<?php

if (isset($_GET['view'])) {
  if (file_exists('vista/' . strtolower($_GET['view']) . '/' . strtolower($_GET['view']) . '_vista.php')) {

    require_once('core/core.php');

    if (isset($_SESSION['usuario_id']) && $_SESSION['usuario_id'] > 0) {

      require_once('vista/perfilmenu/PerfilMenu.class.php');
      $oPerfilMenu = new PerfilMenu();
      require_once('vista/usuario/Usuario.class.php');
      $oUsuario = new Usuario();
      require_once('vista/menu/Menu.class.php');
      $oMenu = new Menu();

      $menu_tit = 'DESCONOCIDO';
      $menu_des = 'Sin descripción';
      $result = $oMenu->informacion_menu_directorio($_GET['view']);
      if ($result['estado'] > 0) {
        $menu_tit = $result['data']['tb_menu_tit'];
        $menu_des = $result['data']['tb_menu_des'];
      }
      $result = NULL;

      $result = $oPerfilMenu->accesso_directorio_perfilmenu(strtolower($_GET['view']), $_SESSION['usuarioperfil_id']);

      $usuario_tem = '';
      $result2 = $oUsuario->mostrarUno(intval($_SESSION['usuario_id']));
      if ($result2['estado'] == 1)
        $usuario_tem = $result2['data']['tb_usuario_tem'];
      $result2 = NULL;


      if ($result['estado'] == 1)
        include('vista/' . strtolower($_GET['view']) . '/index.php');
      else
        header('location: errors/403.html'); //PAGINA DE QUE NO TIENE ACCESSO AQUI: '.$_SESSION['usuarioperfil_id']
    } else
      header('location: ./'); //'MENSAJE DE QUE DEBE INICIAR SESION PARA INGRESAR AL SISTEMA';
  } else {
    header('location: errors/404.html'); //'MENSAJE DE QUE NO EXISTE EL DIRECTORIO QUE SE HA INGRESADO EN EL SISTEMA';
  }
} 
else {
  require_once('core/core.php');
  
  if (isset($_SESSION['usuario_id']) && $_SESSION['usuario_id'] > 0)
    header('location: principal');
  else
    include('vista/login/login.php');
}
