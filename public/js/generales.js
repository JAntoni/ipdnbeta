VISTA_URL = 'vista/';

$(document).ready(function($) {
	var url = window.location+'';
	var res = url.split("/");
	var id = res[res.length - 1];
	$('#'+id).parents('li').addClass('active');
  
  //console.log('cambios 08-08-2023');
});

function modal_hidden_bs_modal(modal_id, tipo){
	//todos los modal ejecutan esta funcion, por ende aplicaremos aqui el ocultar scroll del body
	$('#'+modal_id).draggable({ handle: ".modal-header" });
  
  $('body').addClass('body-hide-scroll');

	$('#'+modal_id).on('hidden.bs.modal', function () {
    if(tipo == 'eliminar')
    	$(this).remove();
    if(tipo == 'limpiar')
    	$(this).parent().empty();

    //despues de cerrar caulquier modal, devolvemos el scroll
    $('body').removeClass('body-hide-scroll');
    $('.modal-backdrop').removeClass('modal-backdrop fade in');
	});
}
function modal_height_auto(modal_id){
  var height = $(window).height() - 190;
  $('#'+modal_id).find(".modal-body").css("max-height", height);
  $('#'+modal_id).find(".modal-body").css("overflow-y", 'auto');
}
function modal_width_auto(modal_id, porcentaje){
  
  $('#'+modal_id).find(".modal-lg").css({'width': porcentaje+'%'});
}
function form_desabilitar_elementos(form_id){
  $('#'+form_id).find('input, textarea').attr('readonly','readonly');
  $('#'+form_id).find('option:not(:selected)').attr('disabled', true);
  $('#'+form_id).find('input[type=radio]').attr('disabled', true);
} 
function permiso_solicitud(usuario_act, creditotipo_id, modulo, div){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"/permiso/permiso_solicitud.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      modulo_id: creditotipo_id,
      modulo: modulo
    }),
    beforeSend: function() {
      
    },
    success: function(data){
      //llamar al formulario de solicitar permiso
      $('#'+div).html(data);
      $('#modal_permiso_solicitud').modal('show');
      modal_hidden_bs_modal('modal_permiso_solicitud', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
function alerta_warning(titulo, mensaje){
  $.confirm({
    title: titulo,
    icon: 'fa fa-info',
    theme: 'material',
    content: mensaje,
    type: 'orange',
    typeAnimated: true,
    buttons: {
      cerrar: function () {}
    }
  });
}
function alerta_success(titulo, mensaje){
  $.confirm({
    title: titulo,
    icon: 'fa fa-check',
    theme: 'material',
    content: mensaje,
    type: 'green',
    typeAnimated: true,
    buttons: {
      cerrar: function () {}
    }
  });
}
function alerta_error(titulo, mensaje){
  $.confirm({
    title: titulo,
    icon: 'fa fa-warning',
    theme: 'material',
    content: mensaje,
    type: 'red',
    typeAnimated: true,
    buttons: {
      cerrar: function () {}
    }
  });
}

function estilos_datatable(id){
  datatable_global = $('#'+id).DataTable({
    "pageLength": 25,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}


function swal_warning(titulo,mensaje,tiempo){
    Swal.fire({
        title: titulo,
        html: mensaje,
        icon: "warning",
        showConfirmButton: true,
        timer: tiempo
        });
}
function swal_success(titulo,mensaje,tiempo){
    Swal.fire({
        title: titulo,
        text: mensaje,
        icon: "success",
        showConfirmButton: true,
        timer: tiempo
        });
}
function swal_error(titulo,mensaje,tiempo){
    Swal.fire({
        title: titulo,
        text: mensaje,
        icon: "error",
        showConfirmButton: true,
        timer: tiempo
        });
}

//notificaciones rápidas
function notificacion_info(mensaje, tiempo){
  toastr.options.timeOut = tiempo;
  toastr.info(mensaje);
}
function notificacion_warning(mensaje, tiempo){
  toastr.options.timeOut = tiempo;
  toastr.warning(mensaje);
}
function notificacion_success(mensaje, tiempo){
  toastr.options.timeOut = tiempo;
  toastr.success(mensaje);
}

//deshabilitar daniel odar 07-03-23 15:22
function disabled(obj){
	Object.keys(obj).forEach(element => {
		var index = parseInt(element);
		if(Number.isInteger(index)){
			//console.log(obj[index].type);
			if(obj[index].type == "text" || obj[index].type == "textarea" || obj[index].type == "number"){
				$(obj[index]).prop('disabled', true);
				$(obj[index]).css("cursor", "text");
			}
			else if(obj[index].type == "button" || obj[index].type == "submit" || obj[index].tagName == "I"){
				$(obj[index]).attr('disabled', 'disabled');
				$(obj[index]).css("cursor", "context-menu");
			}
			else if(obj[index].type == "select-one" || obj[index].type == "checkbox" || obj[index].type == "radio"){
				$(obj[index]).prop('disabled', true);
				$(obj[index]).css("cursor", "context-menu");
        if(obj[index].type == "checkbox"){
          var icheck = $(obj[index]);
          setTimeout(function(){icheck.parent().removeClass("disabled").css('opacity', '75%');}, 150);
        }
			}
			else if(obj[index].tagName == "A"){
        $(obj[index]).attr('disabled', 'disabled');
				$(obj[index]).css("pointer-events", "none");
				$(obj[index]).css("cursor", "context-menu");
        
        if ($(obj[index]).hasClass('ondisabled_deletehref')){
          $(obj[index]).attr('href', '');
        }
			}
		}
	});
}

//daniel odar 08-03-23
function formato_moneda(element, max) {
  var value_max = max == '' ? '999999999.00' : max;
  $('#' + element.id).autoNumeric({
    aSep: ',',
    aDec: '.',
    vMin: '0.00',
    vMax: value_max
  });
}

function solo_letras(string){
  permitidos = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
  letras = true;
  for(contador = 0; contador < string.length; contador++){
    if(permitidos.indexOf(string.substring(contador, contador+1)) <= -1){
      letras = false;
    }
  }
  return letras;
}

function solo_numeros(string){
  permitidos = "0123456789";
  numeros = true;
  for(contador = 0; contador < string.length; contador++){
    if(permitidos.indexOf(string.substring(contador, contador+1)) <= -1){
      numeros = false;
    }
  }
  return numeros;
}

function quitar_comas_miles(valor){
  if(valor === ''){
    return 0;
  } else {
    return parseFloat(valor.replace(/,/g, ""));
  }
}

function rellenar_num_ceros(num, cantidad_limite){
	if(num.length > 0){
		var i=0, completar='';
		var cantidad_falta = cantidad_limite - num.length;
		while(i<cantidad_falta){
			completar+='0';
			i++;
		}
		num = completar+num;
	}
  return num;
}

function rellenar_serie_ceros(txt_serie){
  if (txt_serie.length == 4) {
    return txt_serie;
  }
	if(txt_serie.length > 2){
		var caracter_2 = txt_serie.slice(1,2);
		if(solo_letras(caracter_2)){
			txt_serie = txt_serie.slice(0,2)+'0'+txt_serie.slice(-1);
		} else {
			txt_serie = txt_serie.slice(0,1)+'0'+txt_serie.slice(-2);
		}
	}
	return txt_serie;
}

function removeAccents (str) {
  //se usa al salir de txt (.blur) para quitar caracteres de comillas y especiales
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
  .replaceAll("'", '´')
  .replaceAll('"', '´');
}
