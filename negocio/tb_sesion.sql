-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 11:16:56
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamoserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `sesion_id` int(11) NOT NULL,
  `sesion_fec` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha y hora en que el usuario inicia sesión',
  `usuario_id` int(11) NOT NULL COMMENT 'id del usuario que inicia sesion',
  `sesion_nav` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'informacion de navegador desde donde inicia sesion',
  `sesion_ip` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'informacion de IP de la pc que inicia sesion',
  `sesion_lug` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'informacion del lugar desde donde inicia sesion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`sesion_id`, `sesion_fec`, `usuario_id`, `sesion_nav`, `sesion_ip`, `sesion_lug`) VALUES
(1, '2017-05-17 16:01:34', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(2, '2017-05-17 16:16:19', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(3, '2017-05-17 16:42:27', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(4, '2017-05-17 16:43:21', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(5, '2017-05-17 16:57:27', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(6, '2017-05-17 17:04:30', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(7, '2017-05-17 17:05:55', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(8, '2017-05-17 17:06:24', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(9, '2017-05-17 17:11:08', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(10, '2017-05-17 17:16:25', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(11, '2017-05-17 17:16:33', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(12, '2017-05-17 17:16:33', 1, 'navegador chrome', '192.168.1.45', 'chilayo lambayeque peru'),
(13, '2017-05-17 18:35:04', 2, 'Google Chrome', '::1', ', , '),
(14, '2017-05-17 18:48:22', 2, 'Google Chrome', '::1', ', , '),
(15, '2017-05-17 18:48:24', 2, 'Google Chrome', '::1', ', , '),
(16, '2017-05-17 18:50:50', 2, 'Google Chrome', '::1', ', , '),
(17, '2017-05-17 18:51:35', 2, 'Google Chrome', '::1', ', , '),
(18, '2017-05-17 18:52:34', 2, 'Google Chrome', '::1', ', , '),
(19, '2017-05-17 18:58:17', 2, 'Google Chrome', '::1', ', , '),
(20, '2017-05-18 15:15:31', 2, 'Google Chrome', '::1', ', , '),
(21, '2017-05-18 15:19:16', 2, 'Google Chrome', '::1', ', , '),
(22, '2017-05-18 15:20:47', 2, 'Google Chrome', '::1', ', , '),
(23, '2017-05-18 19:09:02', 2, 'Google Chrome', '::1', ', , '),
(24, '2017-05-18 19:16:42', 2, 'Google Chrome', '::1', ', , '),
(25, '2017-05-18 19:18:28', 16, 'Google Chrome', '::1', ', , '),
(26, '2017-05-19 09:24:33', 2, 'Google Chrome', '::1', ', , '),
(27, '2017-05-19 09:25:07', 16, 'Google Chrome', '::1', ', , '),
(28, '2017-05-19 09:27:29', 2, 'Google Chrome', '::1', ', , '),
(29, '2017-05-19 09:27:43', 16, 'Google Chrome', '::1', ', , '),
(30, '2017-05-19 09:29:14', 2, 'Google Chrome', '::1', ', , '),
(31, '2017-05-19 09:29:26', 16, 'Google Chrome', '::1', ', , '),
(32, '2017-05-19 09:42:15', 16, 'Google Chrome', '::1', ', , '),
(33, '2017-05-19 09:42:35', 16, 'Google Chrome', '::1', ', , '),
(34, '2017-05-19 09:45:01', 16, 'Google Chrome', '::1', ', , '),
(35, '2017-05-19 10:14:01', 2, 'Google Chrome', '::1', ', , '),
(36, '2017-05-19 16:37:53', 2, 'Google Chrome', '::1', ', , '),
(37, '2017-05-19 17:41:03', 16, 'Google Chrome', '::1', ', , '),
(38, '2017-05-19 18:31:16', 2, 'Google Chrome', '::1', ', , '),
(39, '2017-05-20 10:57:55', 2, 'Google Chrome', '::1', ', , '),
(40, '2017-05-20 11:03:27', 21, 'Google Chrome', '192.168.1.39', '0, 0, '),
(41, '2017-05-22 10:00:39', 2, 'Google Chrome', '::1', ', , '),
(42, '2017-05-23 09:38:39', 2, 'Google Chrome', '::1', ', , '),
(43, '2017-05-23 09:47:45', 2, 'Google Chrome', '::1', ', , '),
(44, '2017-05-23 18:37:48', 2, 'Google Chrome', '::1', ', , '),
(45, '2017-05-24 11:10:34', 2, 'Google Chrome', '::1', ', , '),
(46, '2017-05-24 11:20:20', 2, 'Google Chrome', '::1', ', , '),
(47, '2017-05-24 11:20:35', 2, 'Google Chrome', '::1', ', , '),
(48, '2017-05-24 11:21:01', 2, 'Google Chrome', '::1', ', , '),
(49, '2017-05-24 11:21:11', 2, 'Google Chrome', '::1', ', , '),
(50, '2017-05-24 11:35:46', 2, 'Google Chrome', '::1', ', , '),
(51, '2017-05-24 11:52:33', 2, 'Google Chrome', '::1', ', , '),
(52, '2017-05-24 12:12:42', 2, 'Google Chrome', '::1', ', , '),
(53, '2017-05-24 18:12:26', 21, 'Google Chrome', '192.168.1.36', '0, 0, '),
(54, '2017-05-24 18:13:33', 21, 'Google Chrome', '192.168.1.36', '0, 0, '),
(55, '2017-05-25 09:24:48', 2, 'Google Chrome', '::1', ', , '),
(56, '2017-05-25 16:26:03', 2, 'Google Chrome', '::1', ', , '),
(57, '2017-05-26 09:45:47', 2, 'Google Chrome', '::1', ', , '),
(58, '2017-08-31 10:46:22', 2, 'Google Chrome', '::1', ', , '),
(59, '2017-09-01 10:38:45', 2, 'Google Chrome', '::1', ', , '),
(60, '2017-09-01 11:24:03', 2, 'Google Chrome', '::1', ', , '),
(61, '2017-09-01 11:35:01', 2, 'Google Chrome', '::1', ', , '),
(62, '2017-09-01 15:41:56', 16, 'Google Chrome', '::1', ', , '),
(63, '2017-09-02 09:24:16', 16, 'Google Chrome', '::1', ', , '),
(64, '2017-09-04 09:21:24', 16, 'Google Chrome', '::1', ', , '),
(65, '2017-09-05 11:50:24', 16, 'Google Chrome', '::1', ', , '),
(66, '2017-09-05 12:44:32', 16, 'Google Chrome', '::1', ', , '),
(67, '2017-09-05 12:55:55', 16, 'Google Chrome', '::1', ', , '),
(68, '2017-09-06 10:20:37', 16, 'Google Chrome', '::1', ', , '),
(69, '2017-09-06 18:43:06', 16, 'Google Chrome', '::1', ', , '),
(70, '2017-09-07 09:24:32', 16, 'Google Chrome', '::1', ', , '),
(71, '2017-09-08 11:55:45', 16, 'Google Chrome', '::1', ', , '),
(72, '2017-09-08 15:31:45', 2, 'Google Chrome', '::1', ', , '),
(73, '2017-09-08 16:15:25', 2, 'Google Chrome', '::1', ', , '),
(74, '2017-09-09 09:52:01', 2, 'Google Chrome', '::1', ', , '),
(75, '2017-09-11 09:44:00', 2, 'Google Chrome', '::1', ', , '),
(76, '2017-09-11 12:59:57', 2, 'Google Chrome', '::1', ', , '),
(77, '2017-09-12 10:13:29', 2, 'Google Chrome', '::1', ', , '),
(78, '2017-09-12 10:40:27', 2, 'Microsoft Edge', '::1', ', , '),
(79, '2017-09-12 13:00:12', 2, 'Mozilla Firefox', '::1', ', , '),
(80, '2017-09-13 09:49:24', 2, 'Microsoft Edge', '::1', ', , '),
(81, '2017-09-14 09:36:36', 2, 'Microsoft Edge', '::1', ', , '),
(82, '2017-09-14 11:17:08', 16, 'Microsoft Edge', '::1', ', , '),
(83, '2017-09-14 11:53:16', 2, 'Microsoft Edge', '::1', ', , '),
(84, '2017-09-14 12:55:53', 16, 'Microsoft Edge', '::1', ', , '),
(85, '2017-09-14 15:24:08', 2, 'Microsoft Edge', '::1', ', , '),
(86, '2017-09-15 09:42:37', 2, 'Microsoft Edge', '::1', ', , '),
(87, '2017-09-16 09:32:50', 2, 'Microsoft Edge', '::1', ', , '),
(88, '2017-09-16 12:11:40', 2, 'Google Chrome', '::1', ', , '),
(89, '2017-09-16 12:14:45', 2, 'Google Chrome', '::1', ', , '),
(90, '2017-09-16 12:41:23', 16, 'Microsoft Edge', '::1', ', , '),
(91, '2017-09-18 11:25:37', 2, 'Microsoft Edge', '::1', ', , '),
(92, '2017-09-19 10:25:35', 2, 'Microsoft Edge', '::1', ', , '),
(93, '2017-09-20 09:22:08', 2, 'Microsoft Edge', '::1', ', , '),
(94, '2017-09-20 12:36:32', 2, 'Google Chrome', '::1', ', , '),
(95, '2017-09-20 17:22:16', 2, 'Google Chrome', '::1', ', , '),
(96, '2017-09-20 17:47:33', 2, 'Google Chrome', '::1', ', , '),
(97, '2017-09-21 11:04:48', 2, 'Microsoft Edge', '::1', ', , '),
(98, '2017-09-21 11:27:50', 2, 'Google Chrome', '::1', ', , '),
(99, '2017-09-21 11:30:14', 2, 'Mozilla Firefox', '::1', ', , '),
(100, '2017-09-21 11:32:17', 2, 'Microsoft Edge', '::1', ', , '),
(101, '2017-09-22 12:42:08', 2, 'Microsoft Edge', '::1', ', , '),
(102, '2017-09-22 16:04:54', 2, 'Google Chrome', '::1', ', , '),
(103, '2017-09-23 09:55:49', 2, 'Microsoft Edge', '::1', ', , '),
(104, '2017-09-25 10:15:51', 2, 'Microsoft Edge', '::1', ', , '),
(105, '2017-09-26 10:08:52', 2, 'Microsoft Edge', '::1', ', , '),
(106, '2017-09-26 16:44:57', 2, 'Google Chrome', '::1', ', , '),
(107, '2017-09-27 10:01:24', 2, 'Microsoft Edge', '::1', ', , '),
(108, '2017-09-27 11:30:43', 2, 'Google Chrome', '::1', ', , '),
(109, '2017-09-27 11:54:43', 2, 'Microsoft Edge', '::1', ', , '),
(110, '2017-09-28 09:19:37', 2, 'Microsoft Edge', '::1', ', , '),
(111, '2017-09-28 10:20:03', 2, 'Microsoft Edge', '::1', ', , '),
(112, '2017-09-28 17:07:56', 2, 'Google Chrome', '::1', ', , '),
(113, '2017-09-28 17:13:54', 2, 'Microsoft Edge', '::1', ', , '),
(114, '2017-09-29 09:47:35', 2, 'Microsoft Edge', '::1', ', , '),
(115, '2017-09-29 15:15:11', 2, 'Microsoft Edge', '::1', ', , '),
(116, '2017-10-02 09:50:01', 2, 'Microsoft Edge', '::1', ', , '),
(117, '2017-10-03 09:58:16', 2, 'Microsoft Edge', '::1', ', , '),
(118, '2017-10-03 16:40:57', 2, 'Microsoft Edge', '::1', ', , '),
(119, '2017-10-04 09:35:04', 2, 'Microsoft Edge', '::1', ', , '),
(120, '2017-10-04 10:43:56', 2, 'Microsoft Edge', '::1', ', , '),
(121, '2017-10-04 10:44:46', 2, 'Microsoft Edge', '::1', ', , '),
(122, '2017-10-04 18:29:02', 2, 'Microsoft Edge', '::1', ', , '),
(123, '2017-10-05 10:15:23', 2, 'Microsoft Edge', '::1', ', , '),
(124, '2017-10-05 10:46:13', 2, 'Microsoft Edge', '::1', ', , '),
(125, '2017-10-06 09:53:34', 2, 'Microsoft Edge', '::1', ', , '),
(126, '2017-10-07 09:23:48', 2, 'Microsoft Edge', '::1', ', , '),
(127, '2017-10-09 10:03:21', 2, 'Microsoft Edge', '::1', ', , '),
(128, '2017-10-10 12:59:24', 2, 'Microsoft Edge', '::1', ', , '),
(129, '2017-10-11 09:50:12', 2, 'Microsoft Edge', '::1', ', , '),
(130, '2017-10-11 12:23:08', 2, 'Google Chrome', '::1', ', , '),
(131, '2017-10-13 11:01:12', 2, 'Microsoft Edge', '::1', ', , '),
(132, '2017-10-16 11:11:01', 2, 'Microsoft Edge', '::1', ', , '),
(133, '2017-10-17 09:39:58', 2, 'Microsoft Edge', '::1', ', , '),
(134, '2017-10-17 18:43:48', 2, 'Google Chrome', '::1', ', , '),
(135, '2017-10-18 09:39:24', 2, 'Microsoft Edge', '::1', ', , '),
(136, '2017-10-18 09:47:43', 2, 'Google Chrome', '::1', ', , '),
(137, '2017-10-18 09:48:25', 2, 'Google Chrome', '::1', ', , '),
(138, '2017-10-18 09:54:36', 2, 'Google Chrome', '::1', ', , '),
(139, '2017-10-18 09:55:00', 2, 'Google Chrome', '::1', '::1'),
(140, '2017-10-18 18:26:06', 11, 'Google Chrome', '192.168.1.45', '192.168.1.45'),
(141, '2017-10-18 18:47:16', 11, 'Google Chrome', '192.168.1.45', '192.168.1.45'),
(142, '2017-10-19 11:02:38', 2, 'Microsoft Edge', '::1', '::1'),
(143, '2017-10-19 16:29:23', 2, 'Google Chrome', '::1', '::1'),
(144, '2017-10-20 09:43:41', 2, 'Google Chrome', '::1', '::1'),
(145, '2017-10-20 16:51:19', 2, 'Microsoft Edge', '::1', '::1'),
(146, '2017-10-21 09:24:38', 2, 'Microsoft Edge', '::1', '::1'),
(147, '2017-10-23 09:57:27', 2, 'Microsoft Edge', '::1', '::1'),
(148, '2017-10-24 09:29:26', 2, 'Microsoft Edge', '::1', '::1'),
(149, '2017-10-24 12:19:52', 2, 'Google Chrome', '::1', '::1'),
(150, '2017-10-24 16:14:13', 2, 'Google Chrome', '::1', '::1'),
(151, '2017-10-25 09:09:22', 2, 'Microsoft Edge', '::1', '::1'),
(152, '2017-10-26 10:08:01', 2, 'Google Chrome', '::1', '::1'),
(153, '2017-10-26 11:21:46', 2, 'Google Chrome', '192.168.1.48', '192.168.1.48'),
(154, '2017-10-26 16:05:04', 2, 'Google Chrome', '::1', '::1'),
(155, '2017-10-27 09:19:57', 2, 'Google Chrome', '::1', '::1'),
(156, '2017-10-27 10:06:50', 2, 'Google Chrome', '192.168.1.40', '192.168.1.40'),
(157, '2017-10-28 09:55:12', 2, 'Google Chrome', '::1', '::1'),
(158, '2017-10-30 16:09:04', 2, 'Google Chrome', '::1', '::1'),
(159, '2017-10-31 12:36:15', 2, 'Google Chrome', '::1', '::1'),
(160, '2017-11-14 12:36:58', 2, 'Google Chrome', '::1', '::1'),
(161, '2017-11-16 12:39:04', 2, 'Google Chrome', '::1', '::1'),
(162, '2017-12-06 10:38:32', 2, 'Google Chrome', '::1', '::1'),
(163, '2017-12-07 18:56:37', 2, 'Google Chrome', '::1', '::1'),
(164, '2017-12-11 10:32:28', 2, 'Google Chrome', '::1', '::1'),
(165, '2017-12-13 10:36:16', 2, 'Google Chrome', '::1', '::1'),
(166, '2017-12-14 10:36:03', 2, 'Google Chrome', '::1', '::1'),
(167, '2017-12-15 10:46:41', 2, 'Google Chrome', '::1', '::1'),
(168, '2018-01-23 10:30:26', 2, 'Google Chrome', '::1', '::1'),
(169, '2018-01-24 10:43:56', 2, 'Google Chrome', '::1', '::1'),
(170, '2018-01-24 15:35:29', 2, 'Google Chrome', '::1', '::1'),
(171, '2018-01-25 10:50:19', 2, 'Google Chrome', '::1', '::1'),
(172, '2018-01-26 10:42:06', 2, 'Google Chrome', '::1', '::1'),
(173, '2018-01-29 09:58:17', 2, 'Google Chrome', '::1', '::1'),
(174, '2018-01-30 11:14:17', 2, 'Google Chrome', '::1', '::1'),
(175, '2018-01-31 12:37:11', 2, 'Google Chrome', '::1', '::1'),
(176, '2018-02-01 10:15:08', 2, 'Google Chrome', '::1', '::1'),
(177, '2018-02-03 10:59:56', 2, 'Google Chrome', '::1', '::1'),
(178, '2018-02-06 10:51:09', 2, 'Google Chrome', '::1', '::1'),
(179, '2018-02-07 12:35:50', 2, 'Google Chrome', '::1', '::1'),
(180, '2018-02-08 10:06:00', 2, 'Google Chrome', '::1', '::1'),
(181, '2018-02-09 15:33:39', 2, 'Google Chrome', '::1', '::1'),
(182, '2018-02-10 10:27:18', 2, 'Google Chrome', '::1', '::1'),
(183, '2018-02-12 10:02:09', 2, 'Google Chrome', '::1', '::1'),
(184, '2018-02-13 09:27:52', 2, 'Google Chrome', '::1', '::1'),
(185, '2018-02-14 09:38:38', 2, 'Google Chrome', '::1', '::1'),
(186, '2018-02-14 11:15:52', 2, 'Google Chrome', '192.168.1.43', '192.168.1.43'),
(187, '2018-02-15 09:28:16', 2, 'Google Chrome', '::1', '::1'),
(188, '2018-02-16 09:32:19', 2, 'Google Chrome', '::1', '::1'),
(189, '2018-02-17 12:11:04', 2, 'Google Chrome', '::1', '::1'),
(190, '2018-02-19 11:42:35', 2, 'Google Chrome', '::1', '::1'),
(191, '2018-02-19 15:14:27', 2, 'Google Chrome', '::1', '::1'),
(192, '2018-02-20 09:25:26', 2, 'Google Chrome', '::1', '::1'),
(193, '2018-02-21 10:03:50', 2, 'Google Chrome', '::1', '::1'),
(194, '2018-02-21 17:48:12', 16, 'Google Chrome', '::1', '::1'),
(195, '2018-02-23 10:44:11', 2, 'Google Chrome', '::1', '::1'),
(196, '2018-02-23 19:00:20', 2, 'Google Chrome', '192.168.1.41', '192.168.1.41'),
(197, '2018-02-23 19:01:25', 11, 'Google Chrome', '192.168.1.42', '192.168.1.42'),
(198, '2018-02-24 12:08:20', 2, 'Google Chrome', '::1', '::1'),
(199, '2018-02-26 10:19:27', 2, 'Google Chrome', '::1', '::1'),
(200, '2018-02-26 11:13:42', 2, 'Google Chrome', '::1', '::1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`sesion_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sesion`
--
ALTER TABLE `sesion`
  MODIFY `sesion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
