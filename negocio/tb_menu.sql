-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 10:52:56
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamoserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_reg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `menu_nom` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `menu_url` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `menu_tit` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `menu_idp` int(11) NOT NULL,
  `menu_ven` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `menu_des` varchar(200) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion breve del menu',
  `menu_cla` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `menu_ord` int(11) NOT NULL,
  `menu_val` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `menu_dir` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '#' COMMENT 'nombre del directorio al cual tendrán acceso los usuario, con este nombre depende si un usuario tiene acceso o no',
  `menu_ico` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'clase del icono para ser llamado en el menu, ejp: fa fa-home',
  `menu_mos` tinyint(1) NOT NULL COMMENT 'opcion de mostrar u ocultar un menu, debido a los permisos a los directorios, 0 no se muestra, 1 se muestra'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_reg`, `menu_nom`, `menu_url`, `menu_tit`, `menu_idp`, `menu_ven`, `menu_des`, `menu_cla`, `menu_ord`, `menu_val`, `menu_dir`, `menu_ico`, `menu_mos`) VALUES
(1, '2017-09-14 11:30:54', 'Principal', '../administrador/', 'PRINCIPAL', 0, 'Principal', '', 'topfirst', 1, 'L-I-M-E', 'principal', '', 1),
(2, '2017-09-14 11:30:54', 'Mantenimiento', '#', 'MANTENIMIENTO', 0, 'Mantenimiento', 'mantenedor de menús', 'topmenu', 2, 'L-I-M-E', '#', 'fa-gear', 1),
(3, '2017-09-14 11:30:54', 'Créditos', '#', 'CRÉDITOS', 2, 'Créditos', '', 'topmenu', 1, 'L-I-M-E', '#', '', 1),
(4, '2017-09-14 11:30:54', 'Tipo de Créditos', '../creditotipo/', 'TIPO DE CRÉDITOS', 3, 'Tipo de Créditos', '', '', 2, 'L-I-M-E', 'creditotipo', '', 1),
(5, '2017-09-14 11:30:54', 'Tipo de Cuota', '../cuotatipo/', 'TIPO DE CUOTA', 3, 'Tipo de Cuota', '', '', 3, 'L-I-M-E', 'cuotatipo', '', 1),
(6, '2017-09-14 11:30:54', 'Periodo de Cuotas', '../cuotaperiodo/', 'PERIODO DE CUOTAS', 3, 'Periodo de Cuotas', '', '', 4, 'L-I-M-E', 'cuotaperiodo', '', 1),
(7, '2017-09-14 11:30:54', 'Subperiodo de Cuotas', '../cuotasubperiodo/', 'SUBPERIODO DE CUOTAS', 3, 'Subperiodo de Cuotas', '', '', 5, 'L-I-M-E', 'cuotasubperiodo', '', 1),
(8, '2017-09-14 11:30:54', 'Tipos de Garantía', '../garantiatipo/', 'TIPOS DE GARANTÍA', 3, 'Tipos de Garantía', '', '', 7, 'L-I-M-E', 'garantiatipo', '', 1),
(9, '2017-09-14 11:30:54', 'Representante', '../representante/', 'REPRESENTANTE', 3, 'Representante', '', '', 10, 'L-I-M-E', 'representante', '', 1),
(10, '2017-09-14 11:30:54', 'Cuentas para Pagos', '../cuentadeposito/', 'CUENTAS PARA PAGOS', 3, 'Cuentas para Pagos', '', '', 9, 'L-I-M-E', 'cuentadeposito', '', 1),
(11, '2017-09-14 11:30:54', 'Vehículos', '#', 'VEHÍCULOS', 2, 'Vehículos', '', 'topmenu', 2, 'L-I-M-E', '#', '', 1),
(12, '2017-09-14 11:30:54', 'Clases', '../vehiculoclase/', 'CLASES', 11, 'Clases', '', '', 2, 'L-I-M-E', 'vehiculoclase', '', 1),
(13, '2017-09-14 11:30:54', 'Tipos', '../vehiculotipo/', 'TIPOS', 11, 'Tipos', '', '', 3, 'L-I-M-E', 'vehiculotipo', '', 1),
(14, '2017-09-14 11:30:54', 'Marcas', '../vehiculomarca/', 'MARCAS', 11, 'Marcas', '', '', 4, 'L-I-M-E', 'vehiculomarca', '', 1),
(15, '2017-09-14 11:30:54', 'Modelos', '../vehiculomodelo/', 'MODELOS', 11, 'Modelos', '', '', 5, 'L-I-M-E', 'vehiculomodelo', '', 1),
(16, '2017-09-14 11:30:54', 'Adicionales', '../vehiculoadicional/', 'ADICIONALES', 11, 'Adicionales', '', '', 6, 'L-I-M-E', 'vehiculoadicional', '', 1),
(17, '2017-09-14 11:30:54', 'Tipo de Proveedor', '../proveedortipo/', 'TIPO DE PROVEEDOR', 11, 'Tipo de Proveedor', '', '', 7, 'L-I-M-E', 'proveedortipo', '', 1),
(18, '2017-09-14 11:30:54', 'Operaciones', '#', 'OPERACIONES', 2, 'Operaciones', '', 'topmenu', 3, 'L-I-M-E', '#', '', 1),
(19, '2017-09-14 11:30:54', 'Clientes', '../cliente/', 'CLIENTES', 18, 'Clientes', '', '', 1, 'L-I-M-E', 'cliente', '', 1),
(20, '2017-09-14 11:30:54', 'Proveedores', '../proveedor/', 'PROVEEDORES', 18, 'Proveedores', '', '', 2, 'L-I-M-E', 'proveedor', '', 1),
(21, '2017-09-14 11:30:54', 'Caja', '#', 'CAJA', 2, 'Caja', '', 'topmenu', 5, 'L-I-M-E', '#', '', 1),
(22, '2017-09-14 11:30:54', 'Caja', '../caja/', 'CAJA', 21, 'Caja', '', '', 1, 'L-I-M-E', 'caja', '', 1),
(23, '2017-09-14 11:30:54', 'Monedas', '../moneda/', 'MONEDAS', 21, 'Monedas', '', '', 2, 'L-I-M-E', 'moneda', '', 1),
(24, '2017-09-14 11:30:54', 'Tipo de Cambio', '../monedacambio/index.php', 'TIPO DE CAMBIO', 21, 'Tipo de Cambio', 'Mantenimiento de Tipos de Cambio', '', 3, 'L-I-M-E', 'monedacambio', 'fa-circle-o', 1),
(25, '2017-09-14 11:30:54', 'Cuentas - Sub Cuentas', '../cuentas/', 'CUENTAS - SUB CUENTAS', 21, 'Cuentas - Sub Cuentas', 'Mantenimiento de Cuentas y Sub-Cuentas', '', 4, 'L-I-M-E', 'cuenta', 'fa-circle-o', 1),
(26, '2017-09-14 11:30:54', 'Usuarios', '#', 'USUARIOS', 2, 'Usuarios', '', 'topmenu', 6, 'L-I-M-E', '#', '', 1),
(27, '2017-09-14 11:30:54', 'Grupos', '../usuariogrupo/', 'GRUPOS', 26, 'Grupos', 'mantenimiento de grupos de usuario', '', 1, 'L-I-M-E', 'usuariogrupo', 'fa-fr-a', 1),
(28, '2017-09-14 11:30:54', 'Perfiles', '../usuarioperfil/', 'PERFIL DE USUARIO', 26, 'Perfiles', 'Mantenimiento de Perfiles de Usuario', '', 2, 'L-I-M-E', 'usuarioperfil', 'fa-fa-', 1),
(29, '2017-09-14 11:30:54', 'Usuarios', '../usuario/', 'USUARIOS', 26, 'Usuarios', 'mantenimiento de usuarios', '', 3, 'L-I-M-E', 'usuario', 'fa-user', 1),
(30, '2017-09-14 11:30:54', 'Menú', '../menu/', 'MENÚ', 26, 'Menú', 'mantenimiento de menú para el usuario', '', 4, 'L-I-M-E', 'menu', 'fa-list-ul', 1),
(31, '2017-09-14 11:30:54', 'Perfil Menú', '../perfilmenu/', 'PERFIL MENÚ', 26, 'Perfil Menú', '', '', 5, 'L-I-M-E', 'perfilmenu', '', 1),
(32, '2017-09-14 11:30:54', 'Áreas', '../area/', 'ÁREAS', 26, 'Áreas', '', '', 6, 'L-I-M-E', 'area', '', 1),
(33, '2017-09-14 11:30:54', 'Cargos', '../cargo/', 'CARGOS', 26, 'Cargos', '', '', 7, 'L-I-M-E', 'cargo', '', 1),
(34, '2017-09-14 11:30:54', 'Configuración', '#', 'CONFIGURACIÓN', 2, 'Configuración', '', 'topmenu', 8, 'L-I-M-E', '#', '', 1),
(35, '2017-09-14 11:30:54', 'Empresa', '../empresa/', 'EMPRESA', 34, 'Empresa', '', '', 1, 'L-I-M-E', 'empresa', '', 1),
(36, '2017-09-14 11:30:54', 'General', '#', 'GENERAL', 2, 'General', '', 'topmenu', 9, 'L-I-M-E', '#', '', 1),
(37, '2017-09-14 11:30:54', 'Ubigeo', '../ubigeo/ubigeo_vista.php', 'UBIGEO', 36, 'Ubigeo', '', '', 1, 'L-I-M-E', 'ubigeoubigeo_vista.php', '', 1),
(38, '2017-09-14 11:30:54', 'Créditos', '#', 'CRÉDITOS', 0, 'Créditos', '', 'topmenu', 3, 'L-I-M-E', '#', '', 1),
(39, '2017-09-14 11:30:54', 'Créditos - Menores', '../creditomenor/', 'CRÉDITOS - MENORES', 38, 'Créditos - Menores', '', '', 1, 'L-I-M-E', 'creditomenor', '', 1),
(40, '2017-09-14 11:30:54', 'Créditos - Asistencia Vehicular', '../creditoasiveh/', 'CRÉDITOS - ASISTENCIA VEHICULAR', 38, 'Créditos - Asistencia Vehicular', '', '', 2, 'L-I-M-E', 'creditoasiveh', '', 1),
(41, '2017-09-14 11:30:54', 'Créditos - Garantía Vehicular', '../creditogarveh/', 'CRÉDITOS - GARANTÍA VEHICULAR', 38, 'Créditos - Garantía Vehicular', '', '', 3, 'L-I-M-E', 'creditogarveh', '', 1),
(42, '2017-09-14 11:30:54', 'Operaciones', '#', 'OPERACIONES', 0, 'Operaciones', '', 'topmenu', 4, 'L-I-M-E', '#', '', 1),
(43, '2017-09-14 11:30:54', 'Pagos de Cuotas', '../vencimiento/', 'PAGOS DE CUOTAS', 42, 'Pagos de Cuotas', '', '', 1, 'L-I-M-E', 'vencimiento', '', 1),
(44, '2017-09-14 11:30:54', 'Pagos de Mora', '../cobranzamora/', 'PAGOS DE MORA', 42, 'Pagos de Mora', '', '', 2, 'L-I-M-E', 'cobranzamora', '', 1),
(46, '2017-09-14 11:30:54', 'Caja', '#', 'CAJA', 0, 'Caja', '', 'topmenu', 6, 'L-I-M-E', '#', '', 1),
(47, '2017-09-14 11:30:54', 'Consulta de Caja', '../flujocaja/caja_vista.php', 'CONSULTA DE CAJA', 46, 'Consulta de Caja', '', '', 1, 'L-I-M-E', 'flujocajacaja_vista.php', '', 1),
(48, '2017-09-14 11:30:54', 'Ingresos', '../ingreso/', 'INGRESOS', 46, 'Ingresos', '', '', 2, 'L-I-M-E', 'ingreso', '', 1),
(49, '2017-09-14 11:30:54', 'Egresos', '../egreso/', 'EGRESOS', 46, 'Egresos', '', '', 3, 'L-I-M-E', 'egreso', '', 1),
(50, '2017-09-14 11:30:54', 'Cambio de Moneda', '../cajacambio/', 'CAMBIO DE MONEDA', 46, 'Cambio de Moneda', '', '', 4, 'L-I-M-E', 'cajacambio', '', 1),
(51, '2017-09-14 11:30:54', 'Apertura y Cierre de Caja', '../cajaoperacion/', 'APERTURA Y CIERRE DE CAJA', 46, 'Apertura y Cierre de Caja', '', '', 5, 'L-I-M-E', 'cajaoperacion', '', 1),
(52, '2017-09-14 11:30:54', 'Opciones', '#', 'OPCIONES', 0, 'Opciones', '', 'topmenu', 11, 'L-I-M-E', '#', '', 1),
(53, '2017-09-14 11:30:54', 'Modificar Mis Datos', '../usuarios/usuario_datos_vista.php', 'MODIFICAR MIS DATOS', 52, 'Modificar Mis Datos', '', '', 1, 'L-I-M-E', 'perfil', '', 1),
(54, '2017-09-14 11:30:54', '-----------------------', '#', '-----------------------', 3, '-----------------------', '', 'separator', 6, '-', '#', '', 1),
(55, '2017-09-14 11:30:54', '------------------------', '#', '------------------------', 3, '------------------------', '', 'separator', 8, '-', '#', '', 1),
(56, '2017-09-14 11:30:54', 'Disponible para Remate', '../creditomenorgarantia/', 'DISPONIBLE PARA REMATE', 42, 'Disponible para Remate', '', '', 6, 'L-I-M-E', 'creditomenorgarantia', '', 1),
(57, '2017-09-14 11:30:54', 'Impresora', '../impresora/', 'IMPRESORA', 21, 'Impresora', '', '', 5, 'L-I-M-E', 'impresora', '', 1),
(58, '2017-09-14 11:30:54', 'Créditos - Hipotecario', '../creditohipo/', 'CRÉDITOS - HIPOTECARIO', 38, 'Créditos - Hipotecario', '', '', 4, 'L-I-M-E', 'creditohipo', '', 1),
(62, '2017-09-14 11:30:54', 'Cobranza', '#', 'COBRANZA', 0, 'Cobranza', '', '', 9, 'L-I-M-E', '#', '', 1),
(64, '2017-09-14 11:30:54', 'Mis Tareas de Cobranza', '../cobranza/cobranza_mistareas_vista.php', 'MIS TAREAS DE COBRANZA', 62, 'Mis Tareas de Cobranza', '', '', 2, 'L-I-M-E', 'cobranzacobranza_mistareas_vista.php', '', 1),
(65, '2017-09-14 11:30:54', 'Gestión de Cobranza', '../cobranza/', 'GESTIÓN DE COBRANZA', 62, 'Gestión de Cobranza', '', '', 1, 'L-I-M-E', 'cobranza', '', 1),
(66, '2017-09-14 11:30:54', 'Tareas de Cobranza Asignadas', '../cobranza/cobranza_asignadas_vista.php', 'TAREAS DE COBRANZA ASIGNADAS', 62, 'Tareas de Cobranza Asignadas', '', '', 3, 'L-I-M-E', 'cobranzacobranza_asignadas_vista.php', '', 1),
(67, '2017-09-14 11:30:54', 'Formulas de Venta', '../formulaventa/', 'FORMULAS DE VENTA', 3, 'Formulas de Venta', '', '', 11, 'L-I-M-E', 'formulaventa', '', 1),
(68, '2017-09-14 11:30:54', 'Almacén', '../almacen/', 'ALMACEN', 2, 'Almacén', '', '', 10, 'L-I-M-E', 'almacen', '', 1),
(69, '2017-09-14 11:30:54', 'Inventario', '#', 'INVENTARIO', 0, 'Inventario', '', '', 5, 'L-I-M-E', '#', '', 1),
(70, '2017-09-14 11:30:54', 'Garantías en Oficina', '../inventario/', 'GARANTÍAS EN OFICINA', 69, 'Garantías en Oficina', 'Garantías que se encuentran en Oficina', '', 1, 'L-I-M-E', 'oficinainventario', 'fa-circle-o', 1),
(71, '2017-09-14 11:30:54', 'Almacen', '../inventario/garantias_almacen_vista.php', 'ALMACEN', 69, 'Almacen', '', '', 2, 'L-I-M-E', 'inventariogarantias_almacen_vista.php', '', 1),
(72, '2017-09-14 11:30:54', 'Planilla', '#', 'PLANILLA', 0, 'Planilla', '', '', 7, 'L-I-M-E', '#', '', 1),
(73, '2017-09-14 11:30:54', 'Asistencia', '../asistencia/', 'ASISTENCIA', 72, 'Asistencia', '', '', 1, 'L-I-M-E', 'asistencia', '', 1),
(74, '2017-09-14 11:30:54', 'Fórmula Comisión', '../formulaventa/formulacomision_vista.php', 'FÓRMULA COMISIÓN', 3, 'Fórmula Comisión', '', '', 12, 'L-I-M-E', 'formulaventaformulacomision_vista.php', '', 1),
(75, '2017-09-14 11:30:54', 'Aportaciones', '../aportaciones/', 'APORTACIONES', 72, 'Aportaciones', '', '', 2, 'L-I-M-E', 'aportaciones', '', 1),
(76, '2017-09-14 11:30:54', 'Pago en Planilla', '../planilla/', 'PAGO EN PLANILLA', 72, 'Pago en Planilla', '', '', 3, 'L-I-M-E', 'planilla', '', 1),
(77, '2017-09-14 11:30:54', 'Compra Colaboradores', '../creditomenorgarantia/ventainterna_vista.php', 'COMPRA COLABORADORES', 72, 'Compra Colaboradores', '', '', 4, 'L-I-M-E', 'creditomenorgarantiaventainterna_vista.php', '', 1),
(78, '2017-09-14 11:30:54', 'Buscar Vehículo', '../buscaveh/', 'BUSCAR VEHÍCULO', 42, 'Buscar Vehículo', '', '', 3, 'L-I-M-E', 'buscaveh', '', 1),
(79, '2017-09-14 11:30:54', 'Referidos', '#', 'REFERIDOS', 0, 'Referidos', '', '', 8, 'L-I-M-E', '#', '', 1),
(80, '2017-09-14 11:30:54', 'Lista de Referidos', '../referidos/', 'LISTA DE REFERIDOS', 79, 'Lista de Referidos', '', '', 1, 'L-I-M-E', 'referidos', '', 1),
(81, '2017-09-14 11:30:54', 'Medios de Comunicación', '../mediocom/', 'MEDIOS DE COMUNICACIÓN', 18, 'Medios de Comunicación', '', '', 3, 'L-I-M-E', 'mediocom', '', 1),
(82, '2017-09-14 11:30:54', 'Reportes', '#', 'REPORTES', 0, 'Reportes', '', '', 10, 'L-I-M-E', '#', '', 1),
(83, '2017-09-14 11:30:54', 'Generar Reportes', '../reportes/', 'GENERAR REPORTES', 82, 'Generar Reportes', '', '', 1, 'L-I-M-E', '#', '', 1),
(84, '2017-09-14 11:30:54', 'Marketing', '../marketing', 'MARKETING', 2, 'Marketing', 'Medios de comunicacion, metas e inversion', '', 4, 'L-I-M-E', '#', 'fa-circle-o', 1),
(85, '2017-09-14 11:30:54', 'Gastos Vehiculares', '../gastos', 'GASTOS VEHICULARES', 42, 'Gastos Vehiculares', '', '', 4, 'L-I-M-E', '#', '', 1),
(86, '2017-09-14 11:30:54', 'Tipos de Documento', '../documento', 'TIPOS DE DOCUMENTO', 21, 'Tipos de Documento', '', '', 6, 'L-I-M-E', '#', '', 1),
(87, '2017-09-14 11:30:54', 'Historial Usuario', '../lineatiempo', 'HISTORIAL USUARIO', 26, 'Historial Usuario', '', '', 8, 'L-I-M-E', '#', '', 1),
(88, '2017-09-14 11:30:54', 'Stock de Unidades', '../stockunidad', 'STOCK DE UNIDADES', 42, 'Stock de Unidades', '', '', 5, 'L-I-M-E', '#', '', 1),
(89, '2017-09-14 11:30:54', 'Cartera de Clientes', '../cobranza/cobranza_clientes_vista.php', 'CARTERA DE CLIENTES', 62, 'Cartera de Clientes', '', '', 4, 'L-I-M-E', '#', '', 1),
(95, '2017-09-16 11:13:29', 'SOLICITUD PERMISO', 'permiso', 'SOLICITUD PERMISO', 26, 'SOLICITUD PERMISO', 'permisos solicitados por los usuarios', '', 9, '', 'permiso', 'fa-circle-o', 1),
(96, '2017-09-22 12:45:46', 'COLABORADOR', 'colaborador', 'COLABORADOR', 2, 'COLABORADOR', 'Lista de submenús de colaborador', '', 7, '', '#', 'fa-users', 1),
(97, '2017-09-22 12:47:31', 'COLABORADORES', 'colaborador', 'COLABORADORES', 96, 'COLABORADORES', 'Mantenimiento de Colaboradores de la Empresa', '', 1, '', 'colaborador', 'fa-users', 1),
(98, '2017-10-11 16:39:46', 'Clientes por Crédito', '../clientecredito', 'CLIENTES POR CRÉDITO', 62, 'Clientes por Crédito', '', '', 5, 'L-I-M-E', '#', '', 1),
(99, '2017-10-13 16:45:45', 'METAS', 'meta', 'METAS', 84, 'METAS', 'Mantenimiento de metas', '', 1, '', 'meta', 'fa-circle-o', 1),
(100, '2017-10-13 16:46:15', 'INVERSIONES', 'inversion', 'INVERSIONES', 84, 'INVERSIONES', 'Mantenimiento de inversiones', '', 1, '', 'inversion', 'fa-circle-o', 1),
(101, '2017-10-18 12:41:03', 'ZONA REGISTRAL', 'zonaregistral', 'ZONA REGISTRAL', 3, 'ZONA REGISTRAL', 'Mantenimiento de Zonas Registrales', '', 1, '', 'zona', 'fa-circle-o', 1),
(102, '2017-10-27 12:00:53', 'VEHICULO', 'vehiculo', 'VEHICULO', 11, 'VEHICULO', 'Mantenimiento de Vehículos', '', 1, '', 'vehiculo', 'fa-circle-o', 1),
(103, '2017-11-14 12:41:05', 'TARIFARIO', '../tarifario', 'TARIFARIO', 3, 'TARIFARIO', 'Tarifarios por créditos', '', 13, '', 'tarifario', 'fa-circle-o', 1),
(104, '2018-01-23 12:17:44', 'GARANTÍAS', 'garantia', 'GARANTÍAS DE CRE-MENOR', 3, 'GARANTÍAS', 'Mantenimiento de Garantías', '', 1, '', 'garantia', 'fa-circle-o', 1),
(105, '2018-02-01 10:24:41', 'ARCHIVOS', '#', 'ARCHIVOS', 0, 'ARCHIVOS', 'modulo para todos los directorios de imagenes', '', 12, '-', '#', 'fa-circle-o', 0),
(106, '2018-02-01 10:26:27', 'GARANTIAFILE', 'garantiafile', 'GARANTIAFILE', 105, 'GARANTIAFILE', 'carpeta para guardar imágenes de las garantías', '', 1, '', 'garantiafile', 'fa-circle-o', 0),
(107, '2018-02-01 17:03:32', 'Bonos', '../bono', 'BONOS', 26, 'Bonos', '', '', 14, 'L-I-M-E', '#', '', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
