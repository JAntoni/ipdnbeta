-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 10:45:10
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamoserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariogrupo`
--

CREATE TABLE `usuariogrupo` (
  `usuariogrupo_id` int(11) NOT NULL,
  `usuariogrupo_reg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuariogrupo_xac` tinyint(4) NOT NULL,
  `usuariogrupo_nom` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuariogrupo_des` varchar(200) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion del tipo de grupo de usuario',
  `usuariogrupo_mos` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuariogrupo`
--

INSERT INTO `usuariogrupo` (`usuariogrupo_id`, `usuariogrupo_reg`, `usuariogrupo_xac`, `usuariogrupo_nom`, `usuariogrupo_des`, `usuariogrupo_mos`) VALUES
(1, '2017-09-19 17:59:50', 1, 'GERENCIA', 'ahora si genrencia editada', 1),
(2, '2017-09-19 17:59:50', 1, 'VENTAS', '', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuariogrupo`
--
ALTER TABLE `usuariogrupo`
  ADD PRIMARY KEY (`usuariogrupo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuariogrupo`
--
ALTER TABLE `usuariogrupo`
  MODIFY `usuariogrupo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
