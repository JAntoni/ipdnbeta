-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 10:53:11
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamoserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfilmenu`
--

CREATE TABLE `perfilmenu` (
  `perfilmenu_id` int(11) NOT NULL,
  `perfilmenu_reg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `perfilmenu_xac` tinyint(4) NOT NULL,
  `perfilmenu_per` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'permisos',
  `usuarioperfil_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `perfilmenu`
--

INSERT INTO `perfilmenu` (`perfilmenu_id`, `perfilmenu_reg`, `perfilmenu_xac`, `perfilmenu_per`, `usuarioperfil_id`, `menu_id`) VALUES
(1, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 1),
(2, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 2),
(3, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 3),
(4, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 4),
(5, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 5),
(6, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 6),
(7, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 7),
(8, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 54),
(9, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 8),
(10, '2017-09-15 11:32:07', 1, '-', 1, 55),
(11, '2017-09-15 11:32:07', 1, 'L-I-E', 1, 9),
(12, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 10),
(13, '2017-09-15 11:32:07', 0, 'L-I-M-E', 2, 8),
(14, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 46),
(15, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 52),
(16, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 38),
(17, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 42),
(18, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 38),
(19, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 42),
(20, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 46),
(21, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 52),
(22, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 39),
(23, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 40),
(24, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 41),
(25, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 43),
(26, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 44),
(27, '2017-09-15 11:32:07', 0, 'L-I-M-E', 2, 45),
(28, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 47),
(29, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 48),
(30, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 49),
(31, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 50),
(32, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 51),
(33, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 53),
(34, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 39),
(35, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 40),
(36, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 41),
(37, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 43),
(38, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 44),
(39, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 45),
(40, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 47),
(41, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 48),
(42, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 49),
(43, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 50),
(44, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 51),
(45, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 53),
(46, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 11),
(47, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 12),
(48, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 13),
(49, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 14),
(50, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 15),
(51, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 16),
(52, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 17),
(53, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 18),
(54, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 19),
(55, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 20),
(56, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 21),
(57, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 22),
(58, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 23),
(59, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 24),
(60, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 25),
(61, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 26),
(62, '2017-09-15 11:32:07', 1, 'L-I-E', 1, 27),
(63, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 28),
(64, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 29),
(65, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 30),
(66, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 31),
(67, '2017-09-15 11:32:07', 1, 'L-I-E', 1, 32),
(68, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 33),
(69, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 34),
(70, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 35),
(71, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 36),
(72, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 37),
(73, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 38),
(74, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 39),
(75, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 40),
(76, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 41),
(77, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 42),
(78, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 43),
(79, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 44),
(80, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 45),
(81, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 46),
(82, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 47),
(83, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 48),
(84, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 49),
(85, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 50),
(86, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 51),
(87, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 52),
(88, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 53),
(89, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 56),
(90, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 56),
(91, '2017-09-15 11:32:07', 1, 'L-I-M-E', 3, 56),
(92, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 37),
(93, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 38),
(94, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 39),
(95, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 40),
(96, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 41),
(97, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 42),
(98, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 43),
(99, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 44),
(100, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 45),
(101, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 56),
(102, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 46),
(103, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 47),
(104, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 48),
(105, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 49),
(106, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 50),
(107, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 51),
(108, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 52),
(109, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 53),
(110, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 24),
(111, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 21),
(112, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 10),
(113, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 17),
(114, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 16),
(115, '2017-09-15 11:32:07', 1, 'L-I-M-E', 4, 11),
(116, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 57),
(117, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 58),
(118, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 58),
(119, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 59),
(120, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 60),
(121, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 61),
(122, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 59),
(123, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 61),
(124, '2017-09-15 11:32:07', 0, 'L-I-M-E', 2, 21),
(125, '2017-09-15 11:32:07', 0, 'L-I-M-E', 2, 24),
(126, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 2),
(127, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 62),
(128, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 65),
(129, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 64),
(130, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 66),
(131, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 62),
(132, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 65),
(133, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 64),
(134, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 67),
(135, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 68),
(136, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 69),
(137, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 70),
(138, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 71),
(139, '2017-09-15 11:32:07', 0, 'L-I-M-E', 2, 66),
(140, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 69),
(141, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 70),
(142, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 71),
(143, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 72),
(144, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 73),
(145, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 74),
(146, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 75),
(147, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 76),
(148, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 77),
(149, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 78),
(150, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 78),
(151, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 1),
(152, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 38),
(153, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 39),
(154, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 40),
(155, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 41),
(156, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 58),
(157, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 56),
(158, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 42),
(159, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 47),
(160, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 46),
(161, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 69),
(162, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 70),
(163, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 71),
(164, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 79),
(165, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 80),
(166, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 79),
(167, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 80),
(168, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 78),
(169, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 81),
(170, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 18),
(171, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 19),
(172, '2017-09-15 11:32:07', 0, 'L-I-M-E', 2, 25),
(173, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 72),
(174, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 77),
(175, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 3),
(176, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 10),
(177, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 24),
(178, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 21),
(179, '2017-09-15 11:32:07', 0, 'L-I-M-E', 5, 23),
(180, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 43),
(181, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 44),
(182, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 45),
(183, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 48),
(184, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 49),
(185, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 50),
(186, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 51),
(187, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 72),
(188, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 73),
(189, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 75),
(190, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 76),
(191, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 62),
(192, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 65),
(193, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 64),
(194, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 66),
(195, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 52),
(196, '2017-09-15 11:32:07', 1, 'L-I-M-E', 5, 53),
(197, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 82),
(198, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 83),
(199, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 84),
(200, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 85),
(201, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 86),
(202, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 85),
(203, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 87),
(204, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 88),
(205, '2017-09-15 11:32:07', 1, 'L-I-M-E', 1, 89),
(206, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 1),
(207, '2017-09-15 11:32:07', 1, 'L', 2, 4),
(208, '2017-09-15 11:32:07', 1, 'L-I-M-E', 2, 26),
(209, '2017-09-15 11:32:07', 1, 'L', 2, 30),
(210, '2017-09-15 11:32:07', 1, 'L-I-M', 2, 28),
(211, '2017-09-15 11:32:24', 1, 'L-I-M-E', 2, 90),
(212, '2017-09-15 12:53:54', 1, 'L-E', 2, 5),
(216, '2017-09-16 10:30:21', 0, 'I-M', 2, 91),
(217, '2017-09-16 11:02:11', 1, 'L', 1, 91),
(218, '2017-09-16 11:14:18', 1, 'L-I-M-E', 1, 95),
(219, '2017-09-22 12:49:19', 1, 'L-I-M-E', 1, 96),
(220, '2017-09-22 12:49:31', 1, 'L-I-M-E', 1, 97),
(221, '2017-10-11 16:40:27', 1, 'L-I-M-E', 1, 98),
(222, '2017-10-13 16:51:29', 1, 'L-I-M-E', 1, 99),
(223, '2017-10-13 16:52:03', 1, 'L-I-M-E', 1, 100),
(224, '2017-10-18 12:43:58', 1, 'L-I-M-E', 1, 101),
(225, '2017-10-27 12:02:28', 1, 'L-I-M-E', 1, 102),
(226, '2017-11-14 12:44:54', 1, 'L-I-M-E', 1, 103),
(227, '2018-01-23 12:22:11', 1, 'L-I-M-E', 1, 104),
(228, '2018-02-01 10:27:09', 1, 'L-I-M-E', 1, 105),
(229, '2018-02-01 10:27:30', 1, 'L-I-M-E', 1, 106),
(230, '2018-02-02 10:11:28', 1, 'L-I-M-E', 1, 107),
(231, '2018-02-22 12:05:40', 1, 'L-I-M-E', 2, 104),
(232, '2018-02-22 12:08:50', 1, 'L-I-M-E', 2, 106);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `perfilmenu`
--
ALTER TABLE `perfilmenu`
  ADD PRIMARY KEY (`perfilmenu_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `perfilmenu`
--
ALTER TABLE `perfilmenu`
  MODIFY `perfilmenu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
