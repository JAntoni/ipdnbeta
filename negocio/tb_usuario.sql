-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 10:44:33
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamoserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `usuario_reg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_xac` int(11) NOT NULL DEFAULT '1',
  `usuario_use` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_pas` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_cla` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'clave del usuario sin formato MD5, para poder editar o ver',
  `usuario_nom` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_ape` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_ema` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_tel` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_dir` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_dni` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_mod` datetime NOT NULL,
  `usuario_ultvis` datetime NOT NULL,
  `usuario_blo` tinyint(4) NOT NULL,
  `usuario_mos` tinyint(1) NOT NULL DEFAULT '1',
  `usuariogrupo_id` int(11) NOT NULL,
  `usuarioperfil_id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `usuario_tur` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'tipo de turno del colaborador, 1 full time, 2 part time mañana, 3 part time tarde',
  `usuario_ing1` time DEFAULT NULL COMMENT 'hora de ingreso mañana que debe cumplir el colaborador',
  `usuario_sal1` time DEFAULT NULL COMMENT 'hora de salida mañana que debe cumplir el colaborador',
  `usuario_ing2` time DEFAULT NULL COMMENT 'hora ingreso tarde que debe cumplir',
  `usuario_sal2` time DEFAULT NULL COMMENT 'hora de salida tarde que debe cumplir',
  `usuario_per` tinyint(2) NOT NULL DEFAULT '2' COMMENT 'tipo de personal al que pertenece el usuario registrado, 1 jefatura y 2 ventas, por defecto todos son de ventas',
  `usuario_suel` decimal(7,2) NOT NULL COMMENT 'sueldo base que se le pagará al usuario o colaborador',
  `usuario_hor` int(11) NOT NULL COMMENT 'numero de horas que debe cumplir el colaborador para ser pagado',
  `usuario_afp` tinyint(2) NOT NULL COMMENT 'entidad afp al que pertenece el colaborador, de la tabla afps',
  `usuario_eps` tinyint(2) NOT NULL COMMENT 'entidad eps al que pertenece el colaborador, de la tabla afps',
  `usuario_onp` tinyint(2) NOT NULL COMMENT 'id si pertenece a una ONP',
  `usuario_fot` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'public/images/avatar.png' COMMENT 'url donde se guardará la foto del usuario',
  `usuario_tem` varchar(30) COLLATE utf8_spanish_ci NOT NULL COMMENT 'tema para el sistema que elija el usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario_reg`, `usuario_xac`, `usuario_use`, `usuario_pas`, `usuario_cla`, `usuario_nom`, `usuario_ape`, `usuario_ema`, `usuario_tel`, `usuario_dir`, `usuario_dni`, `usuario_mod`, `usuario_ultvis`, `usuario_blo`, `usuario_mos`, `usuariogrupo_id`, `usuarioperfil_id`, `empresa_id`, `usuario_tur`, `usuario_ing1`, `usuario_sal1`, `usuario_ing2`, `usuario_sal2`, `usuario_per`, `usuario_suel`, `usuario_hor`, `usuario_afp`, `usuario_eps`, `usuario_onp`, `usuario_fot`, `usuario_tem`) VALUES
(2, '0000-00-00 00:00:00', 1, 'soporte', '15a10e267709438f6cf814bfabe9be95', '', 'JUAN ANTONIO', 'SALAZAR RAMIREZ', 'srjuan54@gmail.com', '995833506', 'Las Palmeras', '47697061', '2017-07-15 12:06:12', '2018-02-24 09:19:26', 0, 1, 2, 1, 1, 1, NULL, NULL, NULL, NULL, 2, '850.00', 240, 7, 0, 0, 'public/images/colaborador/avatar_usu_2.png', 'skin-blue'),
(11, '0000-00-00 00:00:00', 1, 'egalvez@prestamosdelnortechiclayo.com', '10550943c1cf749d402b0d3d486208a6', '', 'HERBERT ENGEL', 'GALVEZ ROJAS', 'egalvez@prestamosdelnortechiclayo.com', '981562855', '-', '44484240', '2017-03-15 16:33:51', '2018-01-12 11:13:51', 0, 1, 2, 1, 1, 1, '09:00:00', '13:00:00', '15:00:00', '19:00:00', 1, '1500.00', 176, 7, 0, 0, 'public/images/avatar.png', ''),
(15, '2016-08-31 12:23:50', 1, 'cmercado@prestamosdelnortechiclayo.com', '3f47f30ef60f183ebc0a1481083f8514', '', 'CRISTHIAN LUCIO', 'MERCADO LARA', 'cmercado@prestamosdelnortechiclayo.com', '945936439', '', '44223344', '2016-12-21 18:06:19', '2016-12-20 15:37:25', 1, 1, 3, 4, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(16, '2016-08-31 12:25:11', 1, 'lsanchezg@prestamosdelnortechiclayo.com', '8d295b080c3b971e43e254a4e4eb8089', '', 'LUIS ALBERTO', 'SANCHEZ GARAY', 'lsanchezg@prestamosdelnortechiclayo.com', '937009255', '', '45261610', '2018-02-21 17:47:27', '2018-01-18 15:21:29', 0, 1, 3, 2, 1, 1, '09:00:00', '13:00:00', '15:00:00', '19:00:00', 2, '850.00', 240, 4, 0, 10, 'public/images/avatar.png', 'skin-black-light'),
(17, '2016-08-31 12:26:40', 1, 'jorgebriones@prestamosdelnortechiclayo.com', '58ecfb1b6cb53a557a7e83045b9aad9b', '', 'JORGE WILSON', 'BRIONES PLASENCIA', 'jorgebriones@prestamosdelnortechiclayo.com', '984970080', '', '44223344', '2017-05-24 11:48:50', '2017-05-19 11:25:21', 1, 1, 8, 5, 1, 1, '08:30:00', '13:00:00', '15:00:00', '19:00:00', 2, '850.00', 176, 0, 0, 3, 'public/images/avatar.png', ''),
(18, '2016-09-01 15:35:09', 1, 'rfernandezm@prestamosdelnortechiclayo.com', '11b22f1d1a0be1f24f7a91340b02d945', '', 'ROSINA DEL PILAR', 'FERNANDEZ MANZANARES', 'rfernandezm@prestamosdelnortechiclayo.com', '976882074', '', '47137924', '2017-07-27 17:07:24', '2017-07-27 17:07:54', 0, 1, 2, 1, 1, 1, '08:30:00', '12:30:00', '15:00:00', '19:00:00', 2, '1500.00', 176, 0, 0, 0, 'public/images/avatar.png', ''),
(19, '2016-09-13 09:18:45', 1, 'jgonzales@prestamosdelnortechiclayo.com', '7f50201b0e4d24b90bd89922ddbd0d97', '', 'JESSICA JACKELINE', 'GONZALEZ KODZMAN', 'jgonzales@prestamosdelnortechiclayo.com', '945027584', '-', '44801848', '2017-07-17 10:20:04', '2017-07-18 11:12:43', 0, 2, 2, 1, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(20, '2016-09-13 09:19:53', 1, 'mvargas@prestamosdelnortechiclayo.com', 'a69187a21fe665130b07c0ac39d91519', '', 'VICTOR MANUEL', 'VARGAS TORRES', 'mvargas@prestamosdelnortechiclayo.com', '976849994', '-', '44760801', '2017-08-10 15:17:19', '2017-08-17 17:25:34', 0, 2, 2, 1, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(21, '2017-03-04 11:53:38', 1, 'flopez', '1e10020f6cdd45c02ee4ad8898aadfb5', '', 'FREDDY', 'LOPEZ ROJAS', 'fredylr37@gmail.com', '970113254', '', '76555944', '2017-07-07 08:41:11', '2017-07-06 17:21:50', 1, 0, 2, 1, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(23, '2017-04-21 18:46:29', 1, 'contabilidad', 'fcda5f1dc87e4148c8667a7d444ab3c5', '', 'CINTHIA', 'CONTABILIDAD', '', '', '', '', '2017-04-21 19:24:47', '2017-04-21 18:49:17', 1, 0, 6, 5, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(24, '2017-04-22 13:10:54', 1, 'yvelasquez@prestamosdelnortechiclayo.com', 'b1835628cfcb92f9a547bbbf86ac15e4', '', 'YANNLUI EDUARDO', 'VELASQUEZ GÁLVEZ', 'yvelasquez@prestamosdelnortechiclayo.com', '947533372', 'URB. MIRAFLORES MZ. I LOTE 12', '43484640', '2017-05-08 17:30:27', '2017-07-13 11:57:40', 0, 0, 2, 1, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(25, '2017-05-29 18:04:48', 1, 'eteque@prestamosdelnortechiclayo.com', '6fc27e9b58e5053cc1ee541f85d8eb02', '', 'ERICA JHOANA', 'TEQUE CHAPILLIQUEN', 'eteque@prestamosdelnortechiclayo.com', '961972940', 'Jose Olaya N° 551', '47676675', '2017-06-17 10:40:34', '2017-06-03 11:30:22', 1, 0, 6, 5, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(26, '2017-06-02 11:35:53', 1, 'acardoza@prestamosdelnortechiclayo.com', 'e625785e45507fb10c50ca05257766a9', '', 'ANGELO MARTÍN', 'CARDOZA PUCCE', 'acardoza@prestamosdelnortechiclayo.com', '992710620', 'CAL. MARISCAL SUCRE 241, CHICLAYO', '46791652', '2017-11-15 18:25:57', '2017-11-16 10:10:20', 0, 1, 3, 2, 1, 1, '09:00:00', '13:00:00', '15:00:00', '19:00:00', 2, '850.00', 176, 0, 0, 3, 'public/images/avatar.png', ''),
(27, '2017-07-03 10:26:17', 1, 'gzapata@prestamoschiclayo.com', '30b5cfd6c610b1d15a736a17fa3f0d7e', '', 'GLADYS ELENA', 'ZAPATA FIESTAS', 'gzapata@prestamoschiclayo.com', '970440048', 'CALLE MIGUEL GRAU 454', '43997035', '2017-07-07 08:44:52', '2017-07-05 12:33:49', 1, 1, 6, 5, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(28, '2017-07-24 12:14:03', 1, 'jpuicon@prestamosdelnortechiclayo.com', 'c470b025fb3dbf7bf93ab9f8cef416ba', '', 'JOANNA ESTHER', 'PUICON PISFIL', 'jpuicon@prestamosdelnortechiclayo.com', '', '', '', '2017-08-08 09:17:46', '2017-08-08 09:18:50', 0, 1, 6, 5, 1, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(29, '0000-00-00 00:00:00', 0, 'kdiaz', 'diazpepelos', '', 'KLEIN EIDT', 'DIAZ EDIT', 'klein@gmail.com', '995833506', 'dirección de klein', '47697061', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 4, 3, 0, 1, NULL, NULL, NULL, NULL, 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', ''),
(30, '2017-10-04 12:46:26', 1, 'soporte22', '202cb962ac59075b964b07152d234b70', '123', 'PEDRO PRUEBA', 'APELLIDO PRUEBA', 'soporte@gmail.com', '995833506', 'Las Palmeras', '47697061', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 3, 2, 0, 1, '09:00:00', '13:00:00', '15:00:00', '19:00:00', 2, '0.00', 0, 0, 0, 0, 'public/images/avatar.png', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`);
ALTER TABLE `usuario` ADD FULLTEXT KEY `usuario_nom` (`usuario_nom`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
