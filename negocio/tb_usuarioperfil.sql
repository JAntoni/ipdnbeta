-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 10:45:29
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamoserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioperfil`
--

CREATE TABLE `usuarioperfil` (
  `usuarioperfil_id` int(11) NOT NULL,
  `usuarioperfil_reg` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuarioperfil_xac` tinyint(4) NOT NULL,
  `usuarioperfil_nom` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuarioperfil_des` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'breve descripcion para el perfil de un usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarioperfil`
--

INSERT INTO `usuarioperfil` (`usuarioperfil_id`, `usuarioperfil_reg`, `usuarioperfil_xac`, `usuarioperfil_nom`, `usuarioperfil_des`) VALUES
(1, '2017-09-14 12:14:44', 1, 'ADMINISTRADOR', ''),
(2, '2017-09-14 12:14:44', 1, 'VENDEDOR', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarioperfil`
--
ALTER TABLE `usuarioperfil`
  ADD PRIMARY KEY (`usuarioperfil_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarioperfil`
--
ALTER TABLE `usuarioperfil`
  MODIFY `usuarioperfil_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
