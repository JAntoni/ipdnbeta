/* PROCEDIMIENTO PARA INSERTAR vehiculo*/
DROP PROCEDURE IF EXISTS proc_insertar_vehiculo;
DELIMITER $$
CREATE PROCEDURE proc_insertar_vehiculo(
	in _vehiculomarca_id int, in _vehiculomodelo_id int, in _vehiculoclase_id int, 
    in _vehiculotipo_id int, in _vehiculo_tan tinyint, in _galon_id tinyint, 
    in _vehiculo_pla varchar(7), in _vehiculo_sermot varchar(20), in _vehiculo_sercha varchar(200), 
    in _vehiculo_anio year, in _vehiculo_col varchar(50), in _vehiculo_numpas tinyint,
    in _vehiculo_numasi tinyint, in _vehiculo_des text, in _usuario_id int
)
BEGIN
	INSERT INTO tb_vehiculo (
		tb_vehiculomarca_id, tb_vehiculomodelo_id, tb_vehiculoclase_id, 
		tb_vehiculotipo_id, tb_vehiculo_tan, tb_galon_id, 
		tb_vehiculo_pla, tb_vehiculo_sermot, tb_vehiculo_sercha, 
		tb_vehiculo_anio, tb_vehiculo_col, tb_vehiculo_numpas,
		tb_vehiculo_numasi, tb_vehiculo_des, tb_usuario_id
	)
    VALUES (
		_vehiculomarca_id, _vehiculomodelo_id, _vehiculoclase_id, 
		_vehiculotipo_id, _vehiculo_tan, _galon_id, 
		_vehiculo_pla, _vehiculo_sermot, _vehiculo_sercha, 
		_vehiculo_anio, _vehiculo_col, _vehiculo_numpas,
		_vehiculo_numasi, _vehiculo_des, _usuario_id
    );
END$$
DELIMITER ;
/* PROCEDIMIENTO PARA modificar vehiculo*/
DROP PROCEDURE IF EXISTS proc_modificar_vehiculo;
DELIMITER $$
CREATE PROCEDURE proc_modificar_vehiculo(
	in _vehiculo_id int, in _vehiculomarca_id int, in _vehiculomodelo_id int, 
    in _vehiculoclase_id int, in _vehiculotipo_id int, in _vehiculo_tan tinyint, 
    in _galon_id tinyint, in _vehiculo_pla varchar(7), in _vehiculo_sermot varchar(20), 
    in _vehiculo_sercha varchar(200), in _vehiculo_anio year, in _vehiculo_col varchar(50), 
    in _vehiculo_numpas tinyint, in _vehiculo_numasi tinyint, in _vehiculo_des text
)
BEGIN
	UPDATE tb_vehiculo SET
		tb_vehiculomarca_id =_vehiculomarca_id, tb_vehiculomodelo_id =_vehiculomodelo_id, tb_vehiculoclase_id =_vehiculoclase_id, 
		tb_vehiculotipo_id =_vehiculotipo_id, tb_vehiculo_tan =_vehiculo_tan, tb_galon_id =_galon_id, 
		tb_vehiculo_pla =_vehiculo_pla, tb_vehiculo_sermot =_vehiculo_sermot, tb_vehiculo_sercha =_vehiculo_sercha, 
		tb_vehiculo_anio =_vehiculo_anio, tb_vehiculo_col =_vehiculo_col, tb_vehiculo_numpas =_vehiculo_numpas,
		tb_vehiculo_numasi =_vehiculo_numasi, tb_vehiculo_des =_vehiculo_des WHERE tb_vehiculo_id = _vehiculo_id;
END$$
DELIMITER ;

/* -----------------------------------------------------------------------------------------------------*/
/* PROCEDIMIENTO PARA INSERTAR GARANTIA*/
DROP PROCEDURE IF EXISTS proc_insertar_garantia;
DELIMITER $$
CREATE PROCEDURE proc_insertar_garantia(
	in _garantia_xac tinyint, in _credito_id int, in _garantiatipo_id int, 
		in _garantia_can int, in _garantia_pro varchar(150), in _garantia_val decimal(8,2), 
		in _garantia_valtas decimal(8,2), in _garantia_ser varchar(30), in _garantia_kil varchar(2), 
		in _garantia_pes varchar(20), in _garantia_tas varchar(50), in _garantia_det tinytext, 
		in _garantia_web text
)
BEGIN
	INSERT INTO tb_garantia (
		tb_garantia_xac, tb_credito_id, tb_garantiatipo_id, 
		tb_garantia_can, tb_garantia_pro, tb_garantia_val, 
		tb_garantia_valtas, tb_garantia_ser, tb_garantia_kil, 
		tb_garantia_pes, tb_garantia_tas, tb_garantia_det, 
		tb_garantia_web
	)
    VALUES (
		_garantia_xac, _credito_id, _garantiatipo_id, 
		_garantia_can, _garantia_pro, _garantia_val, 
		_garantia_valtas, _garantia_ser, _garantia_kil, 
		_garantia_pes, _garantia_tas, _garantia_det, 
		_garantia_web
    );
END$$
DELIMITER ;

/* PROCEDIMIENTO PARA modificar GARANTIA*/
DROP PROCEDURE IF EXISTS proc_modificar_garantia;
DELIMITER $$
CREATE PROCEDURE proc_modificar_garantia(
	in _garantia_id int,
	in _garantia_xac tinyint, in _credito_id int, in _garantiatipo_id int, 
		in _garantia_can int, in _garantia_pro varchar(150), in _garantia_val decimal(8,2), 
		in _garantia_valtas decimal(8,2), in _garantia_ser varchar(30), in _garantia_kil varchar(2), 
		in _garantia_pes varchar(20), in _garantia_tas varchar(50), in _garantia_det tinytext, 
		in _garantia_web text
)
BEGIN
	UPDATE tb_garantia SET
		tb_garantia_xac = _garantia_xac, tb_credito_id = _credito_id, tb_garantiatipo_id = _garantiatipo_id, 
		tb_garantia_can = _garantia_can, tb_garantia_pro = _garantia_pro, tb_garantia_val = _garantia_val, 
		tb_garantia_valtas = _garantia_valtas, tb_garantia_ser = _garantia_ser, tb_garantia_kil = _garantia_kil, 
		tb_garantia_pes = _garantia_pes, tb_garantia_tas = _garantia_tas, tb_garantia_det = _garantia_det, 
		tb_garantia_web = _garantia_web WHERE tb_garantia_id = _garantia_id;
END$$
DELIMITER ;

/*call proc_modificar_vehiculo(
	4, 1, 1, 
    1, 1, 1, 
    1, 1, 1, 
    1, 1, 1, 
    1, 1, 'asas'
);*/

