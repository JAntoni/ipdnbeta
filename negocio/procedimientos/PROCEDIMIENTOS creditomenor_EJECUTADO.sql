-- FUNCION PARA INSERTAR CREDITOMENOR Y RETORANAR EL ID --
/* PROCEDIMIENTO PARA INSERTAR credito*/
DROP FUNCTION IF EXISTS fn_insertar_creditomenor;
DELIMITER $$
CREATE FUNCTION fn_insertar_creditomenor(
	_credito_usureg int, _cliente_id int, _credito_tip int, 
	_cuotatipo_id int, _moneda_id int, _credito_tipcam decimal(5,3), 
	_credito_preaco decimal(8,2), _credito_int decimal(4,2), _credito_numcuo int, 
	_credito_numcuomax tinyint, _credito_linapr decimal(8,2), _representante_id int, 
	_cuentadeposito_id int, _credito_pla int, _credito_comdes varchar(20), 
	_credito_obs text, _credito_feccre date, _credito_fecdes date, 
	_credito_his text
) RETURNS INT
BEGIN
	INSERT INTO tb_creditomenor(
		tb_credito_usureg, tb_cliente_id, tb_credito_tip, 
		tb_cuotatipo_id, tb_moneda_id, tb_credito_tipcam, 
		tb_credito_preaco, tb_credito_int, tb_credito_numcuo, 
		tb_credito_numcuomax, tb_credito_linapr, tb_representante_id, 
		tb_cuentadeposito_id, tb_credito_pla, tb_credito_comdes, 
		tb_credito_obs, tb_credito_feccre, tb_credito_fecdes, 
		tb_credito_his
    )
    VALUES (
		_credito_usureg, _cliente_id, _credito_tip, 
		_cuotatipo_id, _moneda_id, _credito_tipcam, 
		_credito_preaco, _credito_int, _credito_numcuo, 
		_credito_numcuomax, _credito_linapr, _representante_id, 
		_cuentadeposito_id, _credito_pla, _credito_comdes, 
		_credito_obs, _credito_feccre, _credito_fecdes, 
		_credito_his
    );
    
    RETURN LAST_INSERT_ID();
END$$
DELIMITER ;
/* PROCEDIMIENTO PARA modificar credito*/
DROP PROCEDURE IF EXISTS proc_modificar_creditomenor;
DELIMITER $$
CREATE PROCEDURE proc_modificar_creditomenor(
	in _credito_id int, in _credito_usumod int,
    in _credito_usureg int, in _cliente_id int, in _credito_tip int, 
	in _cuotatipo_id int, in _moneda_id int, in _credito_tipcam decimal(5,3), 
	in _credito_preaco decimal(8,2), in _credito_int decimal(4,2), in _credito_numcuo int, 
	in _credito_numcuomax tinyint, in _credito_linapr decimal(8,2), in _representante_id int, 
	in _cuentadeposito_id int, in _credito_pla int, in _credito_comdes varchar(20), 
	in _credito_obs text, in _credito_feccre date, in _credito_fecdes date
)
BEGIN
	UPDATE tb_credito SET
		tb_credito_usumod = _credito_usumod,
		tb_credito_usureg = _credito_usureg, tb_cliente_id = _cliente_id, tb_credito_tip = _credito_tip, 
		tb_cuotatipo_id = _cuotatipo_id, tb_moneda_id = _moneda_id, tb_credito_tipcam = _credito_tipcam, 
		tb_credito_preaco = _credito_preaco, tb_credito_int = _credito_int, tb_credito_numcuo = _credito_numcuo, 
		tb_credito_numcuomax= _credito_numcuomax, tb_credito_linapr = _credito_linapr, tb_representante_id = _representante_id, 
		tb_cuentadeposito_id = _cuentadeposito_id, tb_credito_pla = _credito_pla, tb_credito_comdes = _credito_comdes, 
		tb_credito_obs = _credito_obs, tb_credito_feccre = _credito_feccre, tb_credito_fecdes = _credito_fecdes
		WHERE tb_credito_id = _credito_id;
END$$
DELIMITER ;