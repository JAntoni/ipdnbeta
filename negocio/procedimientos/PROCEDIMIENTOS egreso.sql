-- FUNCION PARA INSERTAR INGRESO Y RETORANAR EL ID --
/* PROCEDIMIENTO PARA INSERTAR ingreso*/
DROP FUNCTION IF EXISTS fn_insertar_ingreso;
DELIMITER $$
CREATE FUNCTION fn_insertar_ingreso(
	_ingreso_usureg int, _ingreso_fec date, _documento_id int, 
	_ingreso_numdoc varchar(50), _ingreso_det text, _ingreso_imp decimal,  
	_cuenta_id int, _subcuenta_id int, _cliente_id int, 
	_caja_id int, _moneda_id int, _modulo_id int, 
	_ingreso_modide int, _ingreso_fecdep date, _ingreso_numope varchar(200), 
	_ingreso_mondep decimal, _ingreso_comi decimal, _cuentadeposito_id int, 
	_banco_id int, _ingreso_ap tinyint, _ingreso_detex text
) RETURNS INT
BEGIN
	INSERT INTO tb_ingreso(
		tb_ingreso_usureg, tb_ingreso_fec, tb_documento_id, 
		tb_ingreso_numdoc, tb_ingreso_det, tb_ingreso_imp,
		tb_cuenta_id, tb_subcuenta_id, tb_cliente_id, 
		tb_caja_id, tb_moneda_id, tb_modulo_id, 
		tb_ingreso_modide, tb_ingreso_fecdep, tb_ingreso_numope, 
		tb_ingreso_mondep, tb_ingreso_comi, tb_cuentadeposito_id, 
		tb_banco_id, tb_ingreso_ap, tb_ingreso_detex
    )
    VALUES (
		_ingreso_usureg, _ingreso_fec, _documento_id, 
	    _ingreso_numdoc, _ingreso_det, _ingreso_imp,
	    _cuenta_id, _subcuenta_id, _cliente_id, 
	    _caja_id, _moneda_id, _modulo_id, 
	    _ingreso_modide, _ingreso_fecdep, _ingreso_numope, 
	    _ingreso_mondep, _ingreso_comi, _cuentadeposito_id, 
	    _banco_id, _ingreso_ap, _ingreso_detex
    );
    
    RETURN LAST_INSERT_ID();
END$$

-- FUNCION PARA INSERTAR EGRESO Y RETORANAR EL ID --
/* PROCEDIMIENTO PARA INSERTAR egreso*/
DROP FUNCTION IF EXISTS fn_insertar_egreso;
DELIMITER $$
CREATE FUNCTION fn_insertar_egreso(
	_egreso_usureg int, _egreso_fec date, _documento_id int, 
	_egreso_numdoc varchar(50), _egreso_det text, _egreso_imp decimal, 
	_egreso_tipcam decimal, _cuenta_id int, _subcuenta_id int, 
	_proveedor_id int, _cliente_id int, _usuario_id int, 
	_caja_id int, _moneda_id int, _modulo_id int, 
	_egreso_modide int, _empresa_id int, _egreso_ap int
) RETURNS INT
BEGIN
	INSERT INTO tb_egreso(
		tb_egreso_usureg, tb_egreso_fec, tb_documento_id, 
		tb_egreso_numdoc, tb_egreso_det, tb_egreso_imp, 
		tb_egreso_tipcam, tb_cuenta_id, tb_subcuenta_id, 
		tb_proveedor_id, tb_cliente_id, tb_usuario_id, 
		tb_caja_id, tb_moneda_id, tb_modulo_id, 
		tb_egreso_modide, tb_empresa_id, tb_egreso_ap
    )
    VALUES (
		_egreso_usureg, _egreso_fec, _documento_id, 
		_egreso_numdoc, _egreso_det, _egreso_imp, 
		_egreso_tipcam, _cuenta_id, _subcuenta_id, 
		_proveedor_id, _cliente_id, _usuario_id, 
		_caja_id, _moneda_id, _modulo_id, 
		_egreso_modide, _empresa_id, _egreso_ap
    );
    
    RETURN LAST_INSERT_ID();
END$$
DELIMITER ;
/* PROCEDIMIENTO PARA modificar egreso*/
DROP PROCEDURE IF EXISTS proc_modificar_egreso;
DELIMITER $$
CREATE PROCEDURE proc_modificar_egreso(
	in _egreso_id int,
	in _egreso_usumod int, in _egreso_fec date, in _documento_id int, 
	in _egreso_numdoc varchar(50), in _egreso_det text, in _egreso_imp decimal, 
	in _egreso_tipcam decimal, in _cuenta_id int, in _subcuenta_id int, 
	in _proveedor_id int, in _cliente_id int, in _usuario_id int, 
	in _caja_id int, in _moneda_id int, in _modulo_id int, 
	in _egreso_modide int, in _empresa_id int, in _egreso_ap int
)
BEGIN
	UPDATE tb_egreso SET
		tb_egreso_usumod = _egreso_usumod, tb_egreso_fec = _egreso_fec, tb_documento_id = _documento_id, 
		tb_egreso_numdoc = _egreso_numdoc, tb_egreso_det = _egreso_det, tb_egreso_imp = _egreso_imp, 
		tb_egreso_tipcam = _egreso_tipcam, tb_cuenta_id = _cuenta_id, tb_subcuenta_id = _subcuenta_id, 
		tb_proveedor_id = _proveedor_id, tb_cliente_id = _cliente_id, tb_usuario_id = _usuario_id, 
		tb_caja_id = _caja_id, tb_moneda_id = _moneda_id, tb_modulo_id = _modulo_id, 
		tb_egreso_modide = _egreso_modide, tb_empresa_id = _empresa_id, tb_egreso_ap = _egreso_ap
		WHERE tb_egreso_id = _egreso_id;
END$$
DELIMITER ;