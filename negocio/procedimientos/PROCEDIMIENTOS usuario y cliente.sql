
/* PROCEDIMIENTO PARA INSERTAR USUARIO*/
DROP PROCEDURE IF EXISTS proc_insertar_usuario;
DELIMITER $$
CREATE PROCEDURE proc_insertar_usuario(
	in _usuario_use varchar(50), in _usuario_pas varchar(32), in _usuario_nom varchar(50), 
    in _usuario_ape varchar(100), in _usuario_ema varchar(100), in _usuario_tel varchar(12), 
    in _usuario_dir varchar(100), in _usuario_dni varchar(8), in _usuariogrupo_id int, 
    in _usuarioperfil_id int, in _usuario_per tinyint, in _usuario_blo tinyint
)
BEGIN
	INSERT INTO tb_usuario (
		tb_usuario_use, tb_usuario_pas, tb_usuario_nom, 
		tb_usuario_ape, tb_usuario_ema, tb_usuario_tel, 
		tb_usuario_dir, tb_usuario_dni, tb_usuariogrupo_id, 
		tb_usuarioperfil_id, tb_usuario_per, tb_usuario_blo, tb_usuario_cla
	)
    VALUES (
		_usuario_use, MD5(_usuario_pas), _usuario_nom, 
        _usuario_ape, _usuario_ema, _usuario_tel, 
        _usuario_dir, _usuario_dni, _usuariogrupo_id, 
        _usuarioperfil_id, _usuario_per, _usuario_blo, _usuario_pas
    );
END$$
DELIMITER ;

/* PROCEDIMIENTO PARA MODIFICAR USUARIO*/
DROP PROCEDURE IF EXISTS proc_modificar_usuario;
DELIMITER $$
CREATE PROCEDURE proc_modificar_usuario(
	in _usuario_id int, in _usuario_use varchar(50), in _usuario_pas varchar(32), in _usuario_nom varchar(50), 
    in _usuario_ape varchar(100), in _usuario_ema varchar(100), in _usuario_tel varchar(12), 
    in _usuario_dir varchar(100), in _usuario_dni varchar(8), in _usuariogrupo_id int, 
    in _usuarioperfil_id int, in _usuario_per tinyint, in _usuario_blo tinyint
)
BEGIN
	UPDATE tb_usuario SET
		tb_usuario_use =_usuario_use, tb_usuario_pas =MD5(_usuario_pas), tb_usuario_nom =_usuario_nom, 
		tb_usuario_ape =_usuario_ape, tb_usuario_ema =_usuario_ema, tb_usuario_tel =_usuario_tel, 
		tb_usuario_dir =_usuario_dir, tb_usuario_dni =_usuario_dni, tb_usuariogrupo_id =_usuariogrupo_id, 
		tb_usuarioperfil_id =_usuarioperfil_id, tb_usuario_per =_usuario_per, 
        tb_usuario_blo =_usuario_blo, tb_usuario_cla =_usuario_pas 
		WHERE tb_usuario_id = _usuario_id;

END$$
DELIMITER ;


-- FUNCION PARA INSERTAR CLIENTE Y RETORANAR EL ID --
/* PROCEDIMIENTO PARA INSERTAR CLIENTE*/
DROP FUNCTION IF EXISTS fn_insertar_cliente;
DELIMITER $$
CREATE FUNCTION fn_insertar_cliente(
	_cliente_tip tinyint, _cliente_emp tinyint, _cliente_ref tinyint,
  _cliente_seg tinyint, _cliente_vis tinyint, _cliente_des tinyint,
  _cliente_empruc varchar(20), _cliente_emprs varchar(255), _cliente_empdir text,
  _cliente_doc varchar(12), _cliente_nom varchar(100), _cliente_con varchar(100),
	_cliente_tel varchar(12), _cliente_cel varchar(12), _cliente_telref varchar(12), 
  _cliente_protel varchar(255), _cliente_procel varchar(255), _cliente_protelref varchar(255), 
  _cliente_ema varchar(100), _cliente_dir varchar(100), _cliente_fecnac date,
  _cliente_estciv tinyint, _ubigeo_cod varchar(10), _mediocom_id int,
  _creditotipo_id tinyint, _zona_id tinyint, _cliente_numpar varchar(100), 
  _cliente_bien tinyint, _cliente_firm tinyint, _cliente_usureg int
) RETURNS INT
BEGIN
	INSERT INTO tb_cliente (
		tb_cliente_xac,
		tb_cliente_tip, tb_cliente_emp, tb_cliente_ref,
		tb_cliente_seg, tb_cliente_vis, tb_cliente_des,
		tb_cliente_empruc, tb_cliente_emprs, tb_cliente_empdir,
		tb_cliente_doc, tb_cliente_nom, tb_cliente_con,
		tb_cliente_tel, tb_cliente_cel, tb_cliente_telref, 
		tb_cliente_protel, tb_cliente_procel, tb_cliente_protelref, 
		tb_cliente_ema, tb_cliente_dir, tb_cliente_fecnac,
		tb_cliente_estciv, tb_ubigeo_cod, tb_mediocom_id,
		tb_creditotipo_id, tb_zona_id, tb_cliente_numpar, 
		tb_cliente_bien, tb_cliente_firm, tb_cliente_usureg
	)
    VALUES (
		1,
		_cliente_tip, _cliente_emp, _cliente_ref,
		_cliente_seg, _cliente_vis, _cliente_des,
		_cliente_empruc, _cliente_emprs, _cliente_empdir,
		_cliente_doc, _cliente_nom, _cliente_con,
		_cliente_tel, _cliente_cel, _cliente_telref, 
		_cliente_protel, _cliente_procel, _cliente_protelref, 
		_cliente_ema, _cliente_dir, _cliente_fecnac,
		_cliente_estciv, _ubigeo_cod, _mediocom_id,
		_creditotipo_id, _zona_id, _cliente_numpar, 
		_cliente_bien, _cliente_firm, _cliente_usureg
    );
    
    RETURN LAST_INSERT_ID();

END$$
DELIMITER ;

/* PROCEDIMIENTO PARA MODIFICAR CLIENTE*/
DROP PROCEDURE IF EXISTS proc_modificar_cliente;
DELIMITER $$
CREATE PROCEDURE proc_modificar_cliente(
	in _cliente_id int,
	in _cliente_tip tinyint, in _cliente_emp tinyint, in _cliente_ref tinyint,
    in _cliente_seg tinyint, in _cliente_vis tinyint, in _cliente_des tinyint,
  in _cliente_empruc varchar(20), in _cliente_emprs varchar(255), in _cliente_empdir text,
    in _cliente_doc varchar(12), in _cliente_nom varchar(100), in _cliente_con varchar(100),
	in _cliente_tel varchar(12), in _cliente_cel varchar(12), in _cliente_telref varchar(12), 
    in _cliente_protel varchar(255), in _cliente_procel varchar(255), in _cliente_protelref varchar(255), 
    in _cliente_ema varchar(100), in _cliente_dir varchar(100), in _cliente_fecnac date,
    in _cliente_estciv tinyint, in _ubigeo_cod varchar(10), in _mediocom_id int,
    in _creditotipo_id tinyint, in _zona_id tinyint, in _cliente_numpar varchar(100), 
    in _cliente_bien tinyint, in _cliente_firm tinyint, in _cliente_usumod int
)
BEGIN
	UPDATE tb_cliente SET
		tb_cliente_tip = _cliente_tip, tb_cliente_emp = _cliente_emp, tb_cliente_ref = _cliente_ref,
		tb_cliente_seg = _cliente_seg, tb_cliente_vis = _cliente_vis, tb_cliente_des = _cliente_des,
		tb_cliente_empruc = _cliente_empruc, tb_cliente_emprs = _cliente_emprs, tb_cliente_empdir = _cliente_empdir,
		tb_cliente_doc = _cliente_doc, tb_cliente_nom = _cliente_nom, tb_cliente_con = _cliente_con,
		tb_cliente_tel = _cliente_tel, tb_cliente_cel = _cliente_cel, tb_cliente_telref = _cliente_telref, 
		tb_cliente_protel = _cliente_protel, tb_cliente_procel = _cliente_procel, tb_cliente_protelref = _cliente_protelref, 
		tb_cliente_ema = _cliente_ema, tb_cliente_dir = _cliente_dir, tb_cliente_fecnac = _cliente_fecnac,
		tb_cliente_estciv = _cliente_estciv, tb_ubigeo_cod = _ubigeo_cod, tb_mediocom_id = _mediocom_id,
		tb_creditotipo_id = _creditotipo_id, tb_zona_id = _zona_id, tb_cliente_numpar = _cliente_numpar, 
		tb_cliente_bien = _cliente_bien, tb_cliente_firm = _cliente_firm, tb_cliente_usumod = _cliente_usumod 
        WHERE tb_cliente_id = _cliente_id;

END$$
DELIMITER ;