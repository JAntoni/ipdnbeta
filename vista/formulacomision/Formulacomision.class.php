<?php

  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Formulacomision extends Conexion{
    
    public $tb_comision_id;
    public $tb_comision_reg;
    public $tb_comision_per;
    public $tb_creditotipo_id;
    public $tb_creditotipo_asveh;
    public $tb_comision_eve;
    public $tb_comision_for;
    public $tb_comision_est;
    public $tb_comision_mes;
    public $tb_comision_his;
    public $tb_empresa_id;
    
    /* COMISION DETALLE*/
    
    public $tb_comisiondetalle_id;
//    tb_comision_id --> ya se encuentra en la parte de arriba
    public $tb_comisiondetalle_cond1;
    public $tb_comisiondetalle_cond2;
    public $tb_comisiondetalle_porcen;
            
  function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_comision(
                                            tb_comision_reg, 
                                            tb_comision_per, 
                                            tb_creditotipo_id, 
                                            tb_creditotipo_asveh,
                                            tb_comision_eve, 
                                            tb_comision_for, 
                                            tb_comision_est, 
                                            tb_comision_mes, 
                                            tb_comision_his,
                                            tb_empresa_id) 
                                    VALUES (
                                            NOW(),
                                            :tb_comision_per, 
                                            :tb_creditotipo_id, 
                                            :tb_creditotipo_asveh,
                                            :tb_comision_eve, 
                                            :tb_comision_for, 
                                            0, 
                                            :tb_comision_mes, 
                                            :tb_comision_his,
                                            :tb_empresa_id)";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_per", $this->tb_comision_per, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_asveh", $this->tb_creditotipo_asveh, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_eve", $this->tb_comision_eve, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_for", $this->tb_comision_for, PDO::PARAM_INT);
//        $sentencia->bindParam(":tb_comision_est", $this->tb_comision_est, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_mes", $this->tb_comision_mes, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_comision_his", $this->tb_comision_his, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_comision_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_comision_id'] = $tb_comision_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
  function insertar_detalle(){
      $this->dblink->beginTransaction();
      try {
            $sql = "INSERT INTO tb_comisiondetalle(
                                                    tb_comision_id, 
                                                    tb_comisiondetalle_cond1,
                                                    tb_comisiondetalle_cond2,
                                                    tb_comisiondetalle_porcen) 
                                            VALUES (
                                                    :tb_comision_id, 
                                                    :tb_comisiondetalle_cond1,
                                                    :tb_comisiondetalle_cond2,
                                                    :tb_comisiondetalle_porcen)";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_id", $this->tb_comision_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comisiondetalle_cond1", $this->tb_comisiondetalle_cond1, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_comisiondetalle_cond2", $this->tb_comisiondetalle_cond2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_comisiondetalle_porcen", $this->tb_comisiondetalle_porcen, PDO::PARAM_STR);
        
        $result = $sentencia->execute();
        $tb_comisiondetalle_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_comisiondetalle_id'] = $tb_comisiondetalle_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
  function editar(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_comision
                                    set 
                                    tb_comision_per = :tb_comision_per, 
                                    tb_creditotipo_id = :tb_creditotipo_id, 
                                    tb_creditotipo_asveh = :tb_creditotipo_asveh,
                                    tb_comision_eve = :tb_comision_eve,
                                    tb_comision_for = :tb_comision_for,
                                    tb_comision_his = CONCAT(tb_comision_his, :tb_comision_his),
                                    tb_empresa_id=:tb_empresa_id
                            where 
                                    tb_comision_id =:tb_comision_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_per", $this->tb_comision_per, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_asveh", $this->tb_creditotipo_asveh, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_eve", $this->tb_comision_eve, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_for", $this->tb_comision_for, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_his", $this->tb_comision_his, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_id", $this->tb_comision_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  
  function eliminar($comi_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_comision where tb_comision_id =:tb_comision_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  
  function eliminar_comisiondetalle($comi_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_comisiondetalle where tb_comision_id =:tb_comision_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_id",$comi_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }   
  
  function verificar_existente($perso, $cretip, $cre_asveh, $evento, $tipfor, $comi_mes,$comi_id,$empresa_id){
      try {
        $sql = "SELECT * FROM tb_comision 
                where 
                tb_comision_per =:tb_comision_per and 
                tb_creditotipo_id =:tb_creditotipo_id and 
                tb_creditotipo_asveh =:tb_creditotipo_asveh and 
                tb_comision_eve =:tb_comision_eve and 
                tb_comision_for =:tb_comision_for and
                tb_comision_mes =:tb_comision_mes and 
                tb_empresa_id=:tb_empresa_id ";
        
            if(!empty($comi_id))
              $sql.=" and tb_comision_id !=:tb_comision_id";
             
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_per", $perso, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cretip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_asveh", $cre_asveh, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_eve", $evento, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_for", $tipfor, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_mes", $comi_mes, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
        if(!empty($comi_id))
        $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarUno($comi_id){
      try {
         $sql = "SELECT * FROM tb_comision where tb_comision_id =$comi_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
  function mostrar_comisiondetalle($comi_id){
      try {
         $sql = "SELECT * FROM tb_comisiondetalle where tb_comision_id =$comi_id";
    
        $sentencia = $this->dblink->prepare($sql);

        $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
  function listar($comi_mes,$empresa_id){
      try {
         $sql = "SELECT * FROM tb_comision fo
                                inner join tb_creditotipo cre on cre.tb_creditotipo_id = fo.tb_creditotipo_id
                                inner join tb_empresa em on em.tb_empresa_id=fo.tb_empresa_id
                 where 
                                tb_comision_mes = :tb_comision_mes AND fo.tb_empresa_id=:tb_empresa_id order by em.tb_empresa_id,tb_comision_per, fo.tb_creditotipo_id ,tb_comision_eve";
    
        $sentencia = $this->dblink->prepare($sql);

        $sentencia->bindParam(":tb_comision_mes", $comi_mes, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
     
  function vigente($comi_id, $his){
      $this->dblink->beginTransaction();
      try {
          
        $sql = "UPDATE tb_comision set tb_comision_est =1, tb_comision_his = CONCAT(tb_comision_his, :tb_comision_his) where tb_comision_id =:tb_comision_id";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_his", $his, PDO::PARAM_STR);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  function limpiar_estados($perso, $cretip, $cre_asveh, $evento, $comi_mes){
      $this->dblink->beginTransaction();
      try {
          
        $sql = "UPDATE tb_comision set tb_comision_est =0 
                                where   
                                        tb_comision_per =:tb_comision_per and 
                                        tb_creditotipo_id =:tb_creditotipo_id and 
                                        tb_creditotipo_asveh =:tb_creditotipo_asveh and 
                                        tb_comision_eve =:tb_comision_eve and 
                                        tb_comision_mes =:tb_comision_mes";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_comision_per", $perso, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cretip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_asveh", $cre_asveh, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_eve", $evento, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_mes", $comi_mes, PDO::PARAM_STR);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
   
  function mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $comi_mes,$empresa_id){
      try {
        //tb_creditotipo_asveh =:tb_creditotipo_asveh and -> hemos retirado asveh para las formulas
         $sql = "SELECT * FROM 
                        tb_comision co 
                        inner join tb_comisiondetalle cd on cd.tb_comision_id = co.tb_comision_id 
                 where 
                        tb_comision_per =:tb_comision_per and 
                        tb_creditotipo_id =:tb_creditotipo_id and  
                        tb_comision_eve =:tb_comision_eve and 
                        tb_comision_est =:tb_comision_est and 
                        tb_empresa_id =:tb_empresa_id and
                        tb_comision_mes =:tb_comision_mes and :monto between tb_comisiondetalle_cond1 and 
                        tb_comisiondetalle_cond2 limit 1";
         
        $sentencia = $this->dblink->prepare($sql);

        $sentencia->bindParam(":tb_comision_per", $perso, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cretip, PDO::PARAM_INT);
        //$sentencia->bindParam(":tb_creditotipo_asveh", $cre_asveh, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_eve", $evento, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_comision_mes", $comi_mes, PDO::PARAM_STR);
        $sentencia->bindParam(":monto", $monto, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas: persona: ".$perso.' /creditotipo:  '.$cretip.' / evento: '.$evento.' / estado: '.$est.' / mes: '.$comi_mes.' / monto: '.$monto.' / empresa: '.$empresa_id;
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
}
?>