function formulacomision_form(act, idf) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "formulacomision/formulacomision_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            forcomision_id: idf,
            vista: 'formulacomision_tabla',
            comi_mes: $('#cmb_comi_mes').val()
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');

            $('#div_formulacomision_form').html(data);
            $('#modal_registro_formulacomision').modal('show');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('modal_registro_formulacomision', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_registro_formulacomision'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un ancho automatico al modal, al abrirlo
//            modal_width_auto('modal_registro_formulacomision', 75); //funcion encontrada en public/js/generales.js


        },
        complete: function (data) {
        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
//            console.log(data.responseText);
        }
    });
}


function formulacomision_tabla()
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "formulacomision/formulacomision_tabla.php",
        async: true,
        dataType: "html",
        data: ({
//      comi_mes: '01'
            comi_mes: $('#cmb_comi_mes').val(),
            empresa_id: $('#cmb_empresa_id').val()
        }),
        beforeSend: function () {
            $('#formulacomision_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_formulacomision_tabla').html(data);
            $('#formulacomision_mensaje_tbl').hide(300);
        },
        complete: function (data) {
//      $('#formulacomision_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

function vigente_formulacomision(id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿ Realmente desea seleccionar esta fórmula ?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "formulacomision/formulacomision_reg.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: "vigente",
                        forcomision_id: id
                    }),
                    beforeSend: function () {
                        $('#formulacomision_mensaje').html("Cargando...");
                        $('#formulacomision_mensaje').show(100);
                    },
                    success: function (data) {
                        $('#formulacomision_mensaje').html(data.msj);
                    },
                    complete: function () {
                        formulacomision_tabla();
                    }
                });
            },
            no: function () {}
        }
    });
}

function eliminar_formulacomision(id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿ Realmente desea Eliminar esta fórmula ?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "formulacomision/formulacomision_reg.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: "eliminar",
                        forcomision_id: id
                    }),
                    beforeSend: function () {
//                        $('#msj_formulacomision').html("Cargando...");
//                        $('#msj_formulacomision').show(100);
                    },
                    success: function (data) {
                        swal_success("SISTEMA",data.msj,2500);
                    },
                    complete: function () {
                        formulacomision_tabla();
                    }
                });
            },
            no: function () {}
        }
    });
}

function formulacomision_his(idf)
{
  $.ajax({
    type: "POST",
    url: VISTA_URL + "formulacomision/formula_his_form.php",
    async:true,
    dataType: "html",                      
    data: ({
      action: 'historial',
      forcomision_id:  idf
    }),
    beforeSend: function() {
      
//      $('#div_formulacomision_his').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
    success: function(html){
        $('#div_formulacomision_his').html(html);  
            $('#modal_div_formulacomision_his').modal('show');
            modal_hidden_bs_modal('modal_div_formulacomision_his', 'limpiar'); //funcion encontrada en public/js/generales.js

    }
  });
}


$('#cmb_comi_mes,#cmb_empresa_id').change(function () {
    formulacomision_tabla();
});


$(document).ready(function () {
    formulacomision_tabla();
});