/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    var action = $("#action").val();
    var hdd_cretip_id = $("#hdd_cretip_id").val();
    if (action == "editar") {
        cmb_cretip_id(hdd_cretip_id);
    } else {
        cmb_cretip_id(0);
    }
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0',
        vMax: '9999999.99'
    });
    $('.porcentaje').autoNumeric({
        vMin: '0',
        vMax: '100.00'
    });

});

function cmb_cretip_id(id)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditotipo/creditotipo_select.php",
        async: false,
        dataType: "html",
        data: ({
            creditotipo_id: id //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
        }),
        beforeSend: function () {
            $('#cmb_cretip_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_cretip_id').html(html);
        }
    });
}

$('#btn_add').click(function (event) {
    event.preventDefault();
    var tr = $('<tr class="tr_new"></tr>');
    tr.append('<td id="tabla_fila"><input type="text" name="txt_comi_cond1[]" class="moneda form-control input-sm" size="10"></td>');
    tr.append('<td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm" size="10"></td>');
    tr.append('<td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" size="10"></td>');
    tr.append('<td id="tabla_fila"><a href="#add" class="btn_del btn btn-danger btn-xs">-</a></td>');

    $('#tbl_condiciones').append(tr);
    //moneda y porcentaje
    tr.find('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0',
        vMax: '9999999.99'
    });

    tr.find('.porcentaje').autoNumeric({
        vMin: '0',
        vMax: '100.00'
    });

    ///botonesssss
    $('.btn_del').button({
        icons: {primary: "ui-icon-trash"},
        text: false
    });
    $('.btn_del').click(function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });
});

$('#cmb_comi_for').change(function (event) {
    var formu = $(this).val();
    if (parseInt(formu) == 1) {
        $('#btn_add').hide();
        $('.tr_new').remove();
    } else
        $('#btn_add').show();
});
$('.btn_del').click(function (event) {
    event.preventDefault();
    $(this).closest('tr').remove();
});
$('#cmb_cretip_id').change(function (event) {
    var tip = $(this).val();
    if (parseInt(tip) == 2)
        $('#tr_cre_asveh').show();
    else
        $('#tr_cre_asveh').hide();
});


$("#for_forcomision").validate({
    submitHandler: function () {
        var cond1 = $('#txt_comi_cond1').val();
        var cond2 = $('#txt_comi_cond2').val();
        var porc = $('#txt_comi_porcen').val();

        if (cond1 == '' || cond2 == '' || porc == '')
            return false;

        $.ajax({
            type: "POST",
            url: VISTA_URL + "formulacomision/formulacomision_reg.php",
            async: true,
            dataType: "json",
            data: $("#for_forcomision").serialize(),
            beforeSend: function () {
                //$("#div_formulacomision_form" ).dialog( "close" );
//          $('#msj_formulacomision').html("Guardando...");
//          $('#msj_formulacomision').show(100);
            },
            success: function (data) {
                $('#msj_formulacomision').html(data.msj);
                swal_success("SISTEMA", data.msj, 2500);
                if (data.msj != '') {
                    $('#modal_registro_formulacomision').modal('hide');
                    formulacomision_tabla();
                }
            },
            complete: function (data) {
                //$('#msj_formulacomision').html(data.responseText);
                console.log(data);
//          swal_success("SISTEMA",data,8500);
            }
        });
    },
    rules: {
        cmb_cretip_id: {
            required: true
        },
        txt_comi_cond1s: {
            required: true
        },
        txt_comi_cond2: {
            required: true
        },
        txt_comi_porcen: {
            required: true
        }
    },
    messages: {
        cmb_cretip_id: {
            required: 'debe ingresar un dato'
        },
        txt_comi_cond1: {
            required: 'debe ingresar un dato'
        },
        txt_comi_cond2: {
            required: 'debe ingresar un dato'
        },
        txt_comi_porcen: {
            required: 'debe ingresar un dato'
        }
    }
});