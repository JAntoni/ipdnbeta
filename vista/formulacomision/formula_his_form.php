<?php
require_once('../../core/usuario_sesion.php');
require_once("../formulacomision/Formulacomision.class.php");
$oComision = new Formulacomision();

$comi_id = $_POST['forcomision_id'];

$dts = $oComision->mostrarUno($comi_id);
if ($dts['estado'] == 1) {
    $comi_per = $dts['data']['tb_comision_per'];
    $cretip_id = $dts['data']['tb_creditotipo_id'];
    $comi_eve = $dts['data']['tb_comision_eve'];
    $comi_for = $dts['data']['tb_comision_for'];
    $comi_his = $dts['data']['tb_comision_his'];
}

echo $comi_his;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_div_formulacomision_his" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo $titulo; ?></h4>
            </div>
            <form id="form_area" method="post">
                

                <div class="modal-body">
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $comi_his; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="area_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo 'vista/area/area_form.js'; ?>"></script>
