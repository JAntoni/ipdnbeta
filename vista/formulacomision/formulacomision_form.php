<?php 

require_once('../../core/usuario_sesion.php');
require_once("../formulacomision/Formulacomision.class.php");
$oComision = new Formulacomision();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$action = $_POST['action'];
$comi_mes = $_POST['comi_mes'];

if($action == 'editar'){
  $forcomision_id = $_POST['forcomision_id'];
  $dts=$oComision->mostrarUno($forcomision_id);

    if($dts['estado']==1){
        $comi_per  = $dts['data']['tb_comision_per'];
        $cretip_id = $dts['data']['tb_creditotipo_id'];
        $cre_asveh = $dts['data']['tb_creditotipo_asveh']; //1 venta nueva, 4 reventa
        $comi_eve  = $dts['data']['tb_comision_eve'];
        $comi_for  = $dts['data']['tb_comision_for'];
        $empresa_id  = $dts['data']['tb_empresa_id'];
    }

}
//echo 'hasta aki estoy entrando normal ';exit();

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_formulacomision" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000" >INFORMACIÓN DE COMISIONES</h4>
            </div>
            <form id="for_forcomision">
                <input name="action" id="action" type="hidden" value="<?php echo $_POST['action']?>">
                <input name="hdd_forcomision_id" id="hdd_forcomision_id" type="hidden" value="<?php echo $_POST['forcomision_id']?>">
                <input type="hidden" name="hdd_comi_mes" value="<?php echo $comi_mes;?>">
                <input type="hidden" name="hdd_cretip_id" id="hdd_cretip_id" value="<?php echo $cretip_id;?>">

                <div class="modal-body">
                    
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Sede:</label>
                                    <select class="form-control input-sm" id="cmb_empresa_id" name="cmb_empresa_id">
                                        <?php require_once '../empresa/empresa_select.php';?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Personal:</label>
                                    <select name="cmb_comi_per" id="cmb_comi_per" class="form-control input-sm">
                                        <option value="3" <?php if($comi_per == 3) echo 'selected'?>>Administrador de Sede</option>
                                        <option value="2" <?php if($comi_per == 2) echo 'selected'?>>Ventas</option>
                                        <option value="1" <?php if($comi_per == 1) echo 'selected'?>>Jefatura</option>  
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Crédito:</label>
                                    <select name="cmb_cretip_id" id="cmb_cretip_id"  class="form-control input-sm">
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-4"  <?php if($cretip_id !=2) echo 'style="display: none;"'?> id="tr_cre_asveh">
                                    <label>Crédito Asveh:</label>
                                    <select name="cmb_cretip_asveh" id="cmb_cretip_asveh" class="form-control input-sm">
                                        <option value="1" <?php if($cre_asveh == 1) echo 'selected'?>>Venta Nueva</option>
                                        <option value="4" <?php if($cre_asveh == 4) echo 'selected'?>>Re-Veventa</option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>Tipo Evento:</label>
                                    <select name="cmb_comi_eve" class="form-control input-sm">
                                        <option value="1" <?php if($comi_eve == 1) echo 'selected'?>>Poner Crédito</option>
                                        <option value="2" <?php if($comi_eve == 2) echo 'selected'?>>Vender Crédito</option>
                                        <option value="3" <?php if($comi_eve == 3) echo 'selected'?>>Sobre Costo Venta</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Tipo Fórmula:</label>
                                    <select name="cmb_comi_for" id="cmb_comi_for" class="form-control input-sm">
                                        <option value="1" <?php if($comi_for == 1) echo 'selected'?>>Fórmula Fija</option>
                                        <option value="2" <?php if($comi_for == 2) echo 'selected'?>>Fórmula Variable</option>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #006633">INGRESO DE RANGO DE MONTOS</label>
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <table width="100%" id="tbl_condiciones" class="table table-hover">
                                                <tr id="tabla_cabecera" style="text-align: center">
                                                    <th id="tabla_cabecera_fila" style="text-align: center">DESDE</th>
                                                    <th id="tabla_cabecera_fila" style="text-align: center">HASTA</th>
                                                    <th id="tabla_cabecera_fila" style="text-align: center">PORCENTAJE</th>
                                                    <th id="tabla_cabecera_fila" style="text-align: center">ADD/DEL</th>
                                                </tr>
                                                <?php 
                                                  if($action == 'editar'){
                                                    $dts = $oComision->mostrar_comisiondetalle($_POST['forcomision_id']);
                                                    $c = 0;
                                                    if($dts['estado']==1){
                                                        foreach ($dts['data']as$key=>$dt) {
                                                            $display = '';
                                                            if($comi_for == 1)
                                                              $display = 'style="display: none;"';

                                                            if($c == 0){
                                                              echo '
                                                                <tr>
                                                                  <td id="tabla_fila"><input type="text" name="txt_comi_cond1[]" class="moneda form-control input-sm" value="'. mostrar_moneda($dt['tb_comisiondetalle_cond1'],2,',').'"></td>
                                                                  <td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm"  value="'.mostrar_moneda($dt['tb_comisiondetalle_cond2'],2,',').'"></td>
                                                                  <td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" value="'.mostrar_moneda($dt['tb_comisiondetalle_porcen'],2,',').'"></td>
                                                                  <td id="tabla_fila"><a href="#add" id="btn_add" '.$display.' class="btn btn-success btn-xs">Agregar</a></td>
                                                                </tr>';
                                                            }
                                                            else{
                                                              echo '
                                                                <tr class="tr_new">
                                                                  <td id="tabla_fila"><input type="text" name="txt_comi_cond1[]" class="moneda form-control input-sm" value="'.mostrar_moneda($dt['tb_comisiondetalle_cond1'],2,',').'"></td>
                                                                  <td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm" value="'.mostrar_moneda($dt['tb_comisiondetalle_cond2'],2,',').'"></td>
                                                                  <td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" value="'.mostrar_moneda($dt['tb_comisiondetalle_porcen'],2,',').'"></td>
                                                                  <td id="tabla_fila"><a href="#add" class="btn_del btn btn-danger btn-xs">-</a></td>
                                                                </tr>';
                                                            }
                                                            $c ++;
                                                        }
                                                    }
                                                  } 
                                                ?>
                                                <?php if($action != 'editar'): ?>
                                                  <tr>
                                                    <td id="tabla_fila"><input type="text" id="txt_comi_cond1" name="txt_comi_cond1[]" class="moneda form-control input-sm"  size="10"></td>
                                                    <td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm" id="txt_comi_cond2" size="10"></td>
                                                    <td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" id="txt_comi_porcen" size="10"></td>
                                                    <td id="tabla_fila"><a href="#add" id="btn_add" style="display: none;" class="btn btn-success btn-xs">+</a></td>
                                                  </tr>
                                                <?php endif;?>
                                              </table>
  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($action == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Fórmula de Comisión?</h4>
                        </div>
                    <?php endif; ?>
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="formulacomision_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_formulacomision">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/formulacomision/formulacomision_form.js'; ?>"></script>
