<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'formulacomision/Formulacomision.class.php');
    require_once(VISTA_URL . 'empresa/Empresa.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../formulacomision/Formulacomision.class.php');
    require_once('../empresa/Empresa.class.php');
    require_once('../funciones/fechas.php');
}
$oFormula = new Formulacomision();
$oEmpresa=new Empresa();

$comi_mes = $_POST['comi_mes'];
$empresa_id = $_POST['empresa_id'];
$dts = $oFormula->listar($comi_mes,$empresa_id);

if ($dts['estado'] == 0) {
    echo '<center><h2>SIN RESULTADOS</h2></center>';
    exit();
}
?>

<?php if ($dts['estado'] == 1): ?>
<table cellspacing="1" id="tabla_vehiculomarca" class="table table-hover" style="font-family: cambria">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">ID</th>
                <th id="tabla_cabecera_fila">PERSONAL</th>
                <th id="tabla_cabecera_fila">CRÉDITO</th>
                <th id="tabla_cabecera_fila">TIPO EVENTO</th>
                <th id="tabla_cabecera_fila">TIPO DE FÓRMULA</th>
                <th id="tabla_cabecera_fila">SEDE</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th id="tabla_cabecera_fila" width="15%">&nbsp;</th>
            </tr>
        </thead>  
        <tbody> <?php foreach ($dts['data']as $key=>$dt) { ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $dt['tb_comision_id'] ?></td>
                    <td id="tabla_fila">
                        <?php
                        $per = 'JEFATURA';
                        if ($dt['tb_comision_per'] == 2)
                            $per = 'VENTAS';
                        if ($dt['tb_comision_per'] == 3)
                            $per = 'ADMINISTRADOR DE SEDE';
                        echo $per;
                        ?>
                    </td>
                    <td id="tabla_fila">
                        <?php
                        $cre_tip = '';
                        if ($dt['tb_creditotipo_asveh'] == 1)
                            $cre_tip = '(VENTA NUEVA)';
                        if ($dt['tb_creditotipo_asveh'] == 4)
                            $cre_tip = '(RE-VENTA)';

                        echo $dt['tb_creditotipo_nom'] . ' ' . $cre_tip;
                        ?>
                    </td>
                    <td id="tabla_fila">
                        <?php
                        $even = '<span><b>Poner Crédito</b></span>';
                        if ($dt['tb_comision_eve'] == 2)
                            $even = '<span style="color: blue;"><b>Vender garantías</b></span>';
                        if ($dt['tb_comision_eve'] == 3)
                            $even = '<span style="color: brown;"><b>Sobre Costo Ventas</b></span>';
                        echo $even;
                        ?>
                    </td>
                    <td id="tabla_fila">
                        <?php
                        $formu = '<span><b>Fórmula Fija</b></span>';
                        if ($dt['tb_comision_for'] == 2)
                            $formu = '<span style="color: brown;"><b>Fórmula Variable</b></span>';
                        echo $formu;
                        ?>
                    </td>
                    <td id="tabla_fila">
                        <?php
                       $result=$oEmpresa->mostrarUno($dt['tb_empresa_id']);
                       
                        if($result['estado']==1){
                        $nombre_empresa=$result['data']['tb_empresa_nomcom'];
                        }
                           echo $nombre_empresa; 
                        ?>
                    </td>
                    <td id="tabla_fila">
                        <?php
                        $estado = '<b>NO VIGENTE</b>';
                        if ($dt['tb_comision_est'] == 1)
                            $estado = '<strong style="color: green;">VIGENTE</strong>';
                        echo $estado;
                        ?>
                    </td>
                    <td id="tabla_fila" align="center">
                        <a class="btn_editar btn btn-facebook btn-xs" onClick="formulacomision_form('editar', '<?php echo $dt['tb_comision_id'] ?>')" title="Editar"><i class="fa fa-edit"></i></a>
                        <a class="btn_eliminar btn btn-danger btn-xs" onClick="eliminar_formulacomision('<?php echo $dt['tb_comision_id'] ?>')" title="Eliminar"><i class="fa fa-trash"></i></a>

                            <?php
                            if ($dt['tb_comision_est'] == 0) {
                                echo '<a class="btn_vg  btn btn-warning btn-xs" onClick="vigente_formulacomision('.$dt['tb_comision_id'].')" title="Vigente"><i class="fa fa-eye"></i></a>';
                            }
                            ?>
                        <a class="btn_detalle btn btn-success btn-xs" onClick="formulacomision_his('<?php echo $dt['tb_comision_id'] ?>')" title="Detalle"><i class="fa fa-eye"></i></a>
                    </td>
                </tr> <?php
            }
            ?>
        </tbody>

    </table>
<?php endif; ?>