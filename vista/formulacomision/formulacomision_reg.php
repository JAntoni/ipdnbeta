<?php
require_once('../../core/usuario_sesion.php');
require_once("../formulacomision/Formulacomision.class.php");
$oComision = new Formulacomision();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if($_POST['action']=="insertar")
{
  $personal = $_POST['cmb_comi_per']; //tipo de personal, 1 jefatura, 2 ventas
  $cretip = $_POST['cmb_cretip_id']; //tipo de credito, 2 es asiveh
  $cre_asveh = $_POST['cmb_cretip_asveh']; //si es ASIVHE obtenemos el tipo de credito si es venta nueva o reventa
  $evento = $_POST['cmb_comi_eve']; //tipo de evento, 1 poner credito, 2 vender garantias
  $tipfor = $_POST['cmb_comi_for']; //tipo de formula, 1 formula fija, 2 formula variable
  $arr_cond1 = $_POST['txt_comi_cond1']; //array de condiociones primeras
  $arr_cond2 = $_POST['txt_comi_cond2']; //array de condiciones segundas
  $arr_porcen = $_POST['txt_comi_porcen']; //array de porcentajes depende a las condiciones
  $comi_mes = $_POST['hdd_comi_mes']; //mes en que está en vigencia la formaula de comision
  $empresa_id = $_POST['cmb_empresa_id']; 

//  echo '$personal='.$personal.'_$cretip='.$cretip.'_$cre_asveh='.$cre_asveh.'_$evento'.$evento;exit();
  
  if($cretip != 2)
    $cre_asveh = 0;
  $his = '<span>Fórmula de comisión creada por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
//  $data['msj']= 'per: '.$personal.' // cretip: '.$cretip.' // even: '.$evento.' // tipfor: '.$tipfor.' // cond1: '.$arr_cond1[0].' // cre_asveh: '.$cre_asveh.' // comi_mes: '.$comi_mes; echo json_encode($data);exit();
 $dts = $oComision->verificar_existente($personal, $cretip, $cre_asveh, $evento, $tipfor, $comi_mes, '',$empresa_id); //parametro vacio ya que no sera diferente a un id (comision_id)
//  $data['msj'] = 'Fórmula de comisión guarda correctamentebbbbbbbbbbbbbbbbbbbbbbb';echo json_encode($data);exit();

  
  if($dts['estado']==1){
    $data['msj'] = 'Ya existe la fórmula para el mismo Personal y para el mismo Crédito';
    echo json_encode($data);
    exit();
  }
   

  if(count($arr_cond1) == count($arr_cond2) && count($arr_cond1) == count($arr_porcen)){
      
    $oComision->tb_comision_per=$personal; 
    $oComision->tb_creditotipo_id=$cretip;
    $oComision->tb_creditotipo_asveh=$cre_asveh;
    $oComision->tb_comision_eve=$evento;
    $oComision->tb_comision_for=$tipfor;
    $oComision->tb_comision_mes=$comi_mes; 
    $oComision->tb_comision_his=$his;
    $oComision->tb_empresa_id=$empresa_id; 

    $result=$oComision->insertar();
    if(intval($result['estado']) == 1){
        $comi_id = $result['tb_comision_id'];
    }

    $cond = count($arr_cond1);
    for ($i=0; $i < $cond; $i++) { 
        
        $oComision->tb_comision_id=$comi_id;
        $oComision->tb_comisiondetalle_cond1=moneda_mysql($arr_cond1[$i]);
        $oComision->tb_comisiondetalle_cond2=moneda_mysql($arr_cond2[$i]);
        $oComision->tb_comisiondetalle_porcen=moneda_mysql($arr_porcen[$i]);       
        $oComision->insertar_detalle();           
    }
    $data['msj'] = 'Fórmula de comisión guarda correctamente';
  }
  else{
    $data['msj'] = 'Las condiciones y porcentajes no están completos';
  }
  
  echo json_encode($data);
}

if($_POST['action']=="editar")
{
    $comi_id = $_POST['hdd_forcomision_id'];
    $personal = $_POST['cmb_comi_per']; //tipo de personal, 1 jefatura, 2 ventas
    $cretip = $_POST['cmb_cretip_id']; //tipo de credito
    $cre_asveh = $_POST['cmb_cretip_asveh']; //si es ASIVHE obtenemos el tipo de credito si es venta nueva o reventa
    $evento = $_POST['cmb_comi_eve']; //tipo de evento, 1 poner credito, 2 vender garantias
    $tipfor = $_POST['cmb_comi_for']; //tipo de formula, 1 formula fija, 2 formula variable
    $arr_cond1 = $_POST['txt_comi_cond1']; //array de condiociones primeras
    $arr_cond2 = $_POST['txt_comi_cond2']; //array de condiciones segundas
    $arr_porcen = $_POST['txt_comi_porcen']; //array de porcentajes depende a las condiciones
    $comi_mes = $_POST['hdd_comi_mes']; //mes en que está en vigencia la formaula de comision
    $empresa_id = $_POST['cmb_empresa_id'];

  if($cretip != 2)
    $cre_asveh = 0;

  $his = '<span style="color: red;">Fórmula de comisión editada por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
  //echo 'per: '.$personal.' // cretip: '.$cretip.' // even: '.$evento.' // tipfor: '.$tipfor.' // cond1: '.$arr_cond1[0].' // cond2: '.$arr_cond2[0].' // porcen: '.$arr_porcen[0];
  $dts = $oComision->verificar_existente($personal, $cretip, $cre_asveh, $evento, $tipfor, $comi_mes, $comi_id,$empresa_id); //parametro id, lista todos menos el id a modificar


  if($dts['estado']==1){
    $data['msj'] = 'Ya existe la fórmula para el mismo Personal y para el mismo Crédito';
    echo json_encode($data);
    exit();
  }

  if(count($arr_cond1) == count($arr_cond2) && count($arr_cond1) == count($arr_porcen)){
    
    $oComision->tb_comision_per = $personal; 
    $oComision->tb_creditotipo_id = $cretip;
    $oComision->tb_creditotipo_asveh =$cre_asveh;
    $oComision->tb_comision_eve = $evento;
    $oComision->tb_comision_for = $tipfor;
    $oComision->tb_comision_his = $his;
    $oComision->tb_empresa_id =$empresa_id;   
    $oComision->tb_comision_id =$comi_id;   
    $oComision->editar();
    $oComision->eliminar_comisiondetalle($comi_id);

    $cond = count($arr_cond1);
    for ($i=0; $i < $cond; $i++) { 
        $oComision->tb_comision_id=$comi_id;
        $oComision->tb_comisiondetalle_cond1=moneda_mysql($arr_cond1[$i]);
        $oComision->tb_comisiondetalle_cond2=moneda_mysql($arr_cond2[$i]);
        $oComision->tb_comisiondetalle_porcen=moneda_mysql($arr_porcen[$i]);
        $oComision->insertar_detalle();
    }
    $data['msj'] = 'Fórmula de comisión editada correctamente';
  }
  else{
    $data['msj'] = 'Las condiciones y porcentajes no están completos';
  }
  
  echo json_encode($data);
}

if($_POST['action']=="eliminar")
{
  $comi_id = $_POST['forcomision_id'];

  $oComision->eliminar($comi_id);
  $oComision->eliminar_comisiondetalle($comi_id);

  $data['msj'] = 'Se eliminó la fórmula de comisión.';
  echo json_encode($data);
}

if($_POST['action']=="vigente")
{
  $comi_id = $_POST['forcomision_id'];
  $his = '<span style="color: green;">Fórmula de comisión puesta en vigente por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';

  $dts = $oComision->mostrarUno($comi_id);
  
    if($dts['estado']==1){
        $comi_per =  $dts['data']['tb_comision_per'];
        $cretip_id = $dts['data']['tb_creditotipo_id'];
        $cre_asveh = $dts['data']['tb_creditotipo_asveh']; //credido asiveh 1 venta nueva, 4 reventa
        $comi_eve =  $dts['data']['tb_comision_eve'];
        $comi_for =  $dts['data']['tb_comision_for'];
        $comi_mes =  $_POST['tb_comision_mes']; //mes en que está en vigencia la formaula de comision
    }

  $oComision->limpiar_estados($comi_per, $cretip_id, $cre_asveh, $comi_eve, $comi_mes); //poner al evento del credito para el tipo de personal en 0
  $oComision->vigente($comi_id, $his); //cambia de estado a 1->vigente

  $data['msj'] = 'Se ha seleccionado la fórmula vigente.';
  echo json_encode($data);
}
if($_POST['action']=="historial")
{
  $comi_id = $_POST['forcomision_id'];

  $dts = $oComision->mostrarUno($comi_id);
    if($dts['estado']==1){
        $comi_per =  $dts['data']['tb_comision_per'];
        $cretip_id = $dts['data']['tb_creditotipo_id'];
        $comi_eve =  $dts['data']['tb_comision_eve'];
        $comi_for =  $dts['data']['tb_comision_for'];
        $comi_his =  $dts['data']['tb_comision_his'];
     }

  echo $comi_his;
}

?>