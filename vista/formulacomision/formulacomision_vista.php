<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
<?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
            <li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-1">
                        <br>
                        <button class="btn btn-primary btn-sm" onclick="formulacomision_form('insertar', 0)"><i class="fa fa-plus"></i> Nuevo</button>
                    </div>
                    <div class="col-md-2">
                        <label for="cmb_comi_mes">Mes:</label>
                        <select name="cmb_comi_mes" id="cmb_comi_mes" class="form-control input-sm">
                            <?php 
                                $hoy_mes = date('m');
                                $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                                $num = array('01','02','03','04','05','06','07','08','09','10','11','12');
                                $sel = '';

                                for ($i=0; $i < 12 ; $i++) { 
                                    if($num[$i] == $hoy_mes)
                                        echo '<option value="'.$num[$i].'" selected>'.$mes[$i].'</option>';
                                    else
                                        echo '<option value="'.$num[$i].'">'.$mes[$i].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="cmb_comi_mes">Sede:</label>
                        <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm">
                           <?php include_once 'vista/empresa/empresa_select.php';?>
                        </select>
                    </div>
                </div>
                
                
            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="formulacomision_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Comisiones...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <!-- Input para guardar el valor ingresado en el search de la tabla-->
                        <input type="hidden" id="hdd_datatable_fil">

                        <div id="div_formulacomision_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php //require_once('formulamenor_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_formulacomision_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_formulamenor_form">
            </div>
            <div id="div_formulacomision_his">
            </div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
                <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
