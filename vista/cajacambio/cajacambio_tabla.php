<?php

  if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
      require_once(VISTA_URL.'funciones/funciones.php');
      require_once(VISTA_URL.'funciones/fechas.php');
      require_once(VISTA_URL.'cajacambio/Cajacambio.class.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
      require_once('../funciones/funciones.php');
      require_once('../funciones/fechas.php');
      require_once('../cajacambio/Cajacambio.class.php');
  }
  
  $oCajacambio = new Cajacambio();

  $caja_id = (isset($_POST['cmb_fil_caj_id']))? intval($_POST['cmb_fil_caj_id']) : 0;
  $moneda_id = (isset($_POST['cmb_moneda_id']))? intval($_POST['cmb_moneda_id']) : 0;
  $empresa_id = (isset($_POST['cmb_empresa_id']))? intval($_POST['cmb_empresa_id']) : 0;
  $fecha1 = (isset($_POST['txt_fil_fec1']))? fecha_mysql($_POST['txt_fil_fec1']) : date('Y-m-d');
  $fecha2 = (isset($_POST['txt_fil_fec2']))? fecha_mysql($_POST['txt_fil_fec2']) : date('Y-m-d');
  
  $result = $oCajacambio->filtrar_cajacambio($caja_id, $moneda_id, $empresa_id, $fecha1, $fecha2);
  $bandera = intval($result['estado']);
?>
<?php if($bandera == 1): ?>
  <table id="tbl_cajacambio" class="table table-hover">
    <thead>
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">FECHA</th>
        <th id="tabla_cabecera_fila">DESCRIPCION</th>
        <th id="tabla_cabecera_fila">CAJA</th>
        <th id="tabla_cabecera_fila">MONTO</th>
        <th id="tabla_cabecera_fila">TIPO DE CAMBIO</th>
        <th id="tabla_cabecera_fila">CAMBIO</th>
        <th id="tabla_cabecera_fila"></th>
      </tr>
    </thead>
    <tbody> <?php
      foreach ($result['data'] as $key => $value) {
          $credito_id = 'CAV-'.$value['tb_credito_id'];
          if($value['tipo'] == 3)
            $credito_id = 'CGV-'.$value['tb_credito_id'];
          if($value['tb_moneda_id'] == 1)
            $moneda_nombre = 'US$';
          if($value['tb_moneda_id'] == 2)
            $moneda_nombre = 'S/.';
        ?>
        <tr class="even" id="tabla_fila">
          <td id="tabla_fila" align="center"><?php echo $value['tb_cajacambio_id'];?></td>
          <td id="tabla_fila" align="center"><?php echo mostrar_fecha($value['tb_cajacambio_fec']);?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_cajacambio_des'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_caja_nom'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_moneda_nom'] .' '. formato_moneda($value['tb_cajacambio_imp']);?></td>
          <td id="tabla_fila" align="center"><?php echo formato_moneda($value['tb_cajacambio_tipcam']);?></td>
          <td id="tabla_fila" align="center"><?php echo $moneda_nombre .' '. formato_moneda($value['tb_cajacambio_cam']);?></td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-danger btn-xs" href="#" onClick="cajacambio_eliminar(<?php echo $value['tb_cajacambio_id']?>)"><i class="fa fa-trash"></i></a>
          </td>
        </tr> <?php
      }
      $result = null; ?>
    </tbody>
  </table>
<?php endif; ?>

<?php if($bandera != 1): ?>
  <table id="tbl_cajacambio" class="table table-hover">
    <thead>
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">FECHA</th>
        <th id="tabla_cabecera_fila">DESCRIPCION</th>
        <th id="tabla_cabecera_fila">CAJA</th>
        <th id="tabla_cabecera_fila">MONTO</th>
        <th id="tabla_cabecera_fila">TIPO DE CAMBIO</th>
        <th id="tabla_cabecera_fila">CAMBIO</th>
        <th id="tabla_cabecera_fila"></th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
<?php endif; ?>