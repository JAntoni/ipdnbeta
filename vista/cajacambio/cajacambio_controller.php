<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../retencion/Retencion.class.php');
  $oRetencion = new Retencion();
  require_once('../funciones/funciones.php');

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$retencion_cretip = $_POST['cmb_credito_tip'];
		$cliente_id = $_POST['hdd_cliente_id'];
		$credito_id = $_POST['txt_credito_id'];
		$retencion_pla = $_POST['txt_vehiculo_pla'];
		$retencion_mot = $_POST['cmb_retencion_mot'];
		$retencion_deu = (empty($_POST['txt_retencion_deu']))? 0 : moneda_mysql($_POST['txt_retencion_deu']);
		$retencion_kil = (empty($_POST['txt_retencion_kil']))? 0 : moneda_mysql($_POST['txt_retencion_kil']);
		$retencion_det = $_POST['txt_retencion_det'];

		$oRetencion->insertar($retencion_cretip, $cliente_id, $credito_id, $retencion_pla, $retencion_mot, $retencion_deu, $retencion_kil, $retencion_det);
		
		$data['estado'] = 1;
		$data['mensaje'] = 'Retención editada correctamente';
 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$retencion_id = $_POST['hdd_retencion_id'];
		$retencion_cretip = $_POST['cmb_credito_tip'];
		$cliente_id = $_POST['hdd_cliente_id'];
		$credito_id = $_POST['txt_credito_id'];
		$retencion_pla = $_POST['txt_vehiculo_pla'];
		$retencion_mot = $_POST['cmb_retencion_mot'];
		$retencion_deu = (empty($_POST['txt_retencion_deu']))? 0 : moneda_mysql($_POST['txt_retencion_deu']);
		$retencion_kil = (empty($_POST['txt_retencion_kil']))? 0 : moneda_mysql($_POST['txt_retencion_kil']);
		$retencion_det = $_POST['txt_retencion_det'];

		$oRetencion->modificar($retencion_id, $retencion_cretip, $cliente_id, $credito_id, $retencion_pla, $retencion_mot, $retencion_deu, $retencion_kil, $retencion_det);

		$data['estado'] = 1;
		$data['mensaje'] = 'Retención editada correctamente';

		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$retencion_id = intval($_POST['hdd_retencion_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la retencion.';

 		if($oRetencion->eliminar($retencion_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'retencion eliminada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == "credito_cliente"){
		$credito_tip = intval($_POST['credito_tip']);
		$cliente_id = intval($_POST['cliente_id']);

		$tabla = '';
		if($credito_tip == 2)
			$tabla = 'tb_creditoasiveh';
		if($credito_tip == 3)
			$tabla = 'tb_creditogarveh';

		$data['credito_id'] = 0;
		$result = $oRetencion->datos_credito_cliente($tabla, $cliente_id);
			if($result['estado'] == 1){
				$data['credito_id'] = intval($result['data']['tb_credito_id']);
				$data['vehiculo_pla'] = $result['data']['tb_credito_vehpla'];
				$data['vehiculotipo_nom'] = $result['data']['tb_vehiculotipo_nom'];
			}
		$result = NULL;

		echo json_encode($data);
	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>