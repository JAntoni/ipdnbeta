$(document).ready(function() {

  cambio_moneda(2)

  $('#txt_cajcam_imp').focus();

  $( "#txt_cajcam_fec" ).change(function(e) {
    cambio_moneda(2);
    calculo();
  });

  $('#txt_cajcam_imp, #cmb_mon_id, #txt_cajcam_tipcam').change(function(e) {
    calculo();	
  });

  $('.moneda2').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.000',
    vMax: '9999999.999'
  });

	$('#datetimepicker3').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		//startDate: "-0d"
		endDate : new Date()
	});

  $('#txt_cajcam_cam, #txt_cajcam_tipcam').autoNumeric({
    aSep: '',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '1',
    vMax: '50000'
  });

  $("#form_cajacambio").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"cajacambio/cajacambio_reg.php",
				async:true,
				dataType: "json",
				data: $("#form_cajacambio").serialize(),
				beforeSend: function() {
					//alert($("#for_cuopag").serialize());
					$('#msj_cajacambio_tabla').html("Guardando...");
					$('#msj_cajacambio_tabla').show(100);
				},
				success: function(data){				
          if(parseInt(data.cajacambio_id) > 0){
              $('#modal_registro_cajacambio').modal('hide');
              cajacambio_tabla();
              swal_success("SISTEMA",data.cajacambio_id+"   "+data.cajacambio_msj,2000);
              cajacambio_tabla();
              $('#msj_cajacambio_tabla').hide();
          }
          else{
            alerta_error('Error',data.cajacambio_msj);
                  //swal_success("SISTEMA",data.cajacambio_id+"   "+data.cajacambio_msj,6000);
          }
				},
				complete: function(data){
					if(data.statusText != "success"){
						$('#msj_cajacambio_tabla').text(data.responseText);
            
					}
				}
			});
		},
		rules: {
//			txt_cuopag_fec: {
//				required: true,
//				dateITA: true
//			},
			txt_cuopag_mon:{
				required:true
			}
		},
		messages: {
//			txt_cuopag_fec: {
//				required: '*',
//				dateITA: '?'
//			},
			txt_cuopag_mon:{
				required:'*'
			}
		}
	});
});


function cambio_moneda(monid){
  $.ajax({
          type: "POST",
          url: VISTA_URL+"monedacambio/monedacambio_controller.php",
          async:false,
          dataType: "json",
          data: ({
                  action: 'obtener_dato',
                  fecha:	$('#txt_cajcam_fec').val(),
                  mon_id:	monid
          }),
          beforeSend: function(){
                  $('#msj_pagobanco').hide(100);
                  $('#txt_cajcam_tipcam').val('');	
          },
          success: function(data){
                  if(data.moncam_val == null){
                    swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
                  }
                  else
                    $('#txt_cajcam_tipcam').val(data.moncam_val);	
          },
          complete: function(data){			
//                 console.log(data);
          }
        });		
}

function calculo()
	{
		if($('#cmb_mon_id').val() == 2)
		{
			var cambio = Number($('#txt_cajcam_imp').val().replace(/[^0-9\.]+/g,""))*$('#txt_cajcam_tipcam').val();
			$('#txt_cajcam_cam').autoNumeric('set',cambio.toFixed(2));
		}else if($('#cmb_mon_id').val() == 1){
			var cambio = Number($('#txt_cajcam_imp').val().replace(/[^0-9\.]+/g,""))/$('#txt_cajcam_tipcam').val();
      $('#txt_cajcam_cam').autoNumeric('set',cambio.toFixed(2));
		}else{
      $('#txt_cajcam_cam').val('');
    }
	}