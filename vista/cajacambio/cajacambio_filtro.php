<?php
//	$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); //1 ADMIN, 2 COBRADOR, 3 SUPERVISOR
//        $usuario_id = intval($_SESSION['usuario_id']);
//	$usuario_idpa = intval($_SESSION['usuario_idpa']);
//	$empresa_id = intval($_SESSION['empresa_id']);
//$fec1 = date('Y-m-d');
//$fec2 = date('Y-m-d');
?>
<form id="cajacambio_filtro" name="cajacambio_filtro">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label for="">Fecha </label>
        <div class="input-group">
          <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control input-sm" name="txt_fil_fec1" id="txt_fil_fec1" value="<?php echo date('d-m-Y'); ?>" />
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
          <span class="input-group-addon">-</span>
          <div class='input-group date' id='datetimepicker2'>
            <input type='text' class="form-control input-sm" name="txt_fil_fec2" id="txt_fil_fec2" value="<?php echo date('d-m-Y'); ?>" />
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>

        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label for="">Caja</label>
        <select class="form-control input-sm mayus" id="cmb_fil_caj_id" name="cmb_fil_caj_id">
          <?php include VISTA_URL . 'caja/caja_select.php'; ?>
        </select>
      </div>
    </div>
    <div class="col-md-1">
      <div class="form-group">
        <label for="">Moneda</label>
        <select name="cmb_moneda_id" id="cmb_moneda_id" class="form-control input-sm">
          <?php include VISTA_URL . 'moneda/moneda_select.php'; ?>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label for="">Empresa</label>
        <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm">
          <?php include VISTA_URL . 'empresa/empresa_select.php'; ?>
        </select>
      </div>
    </div>

    <div class="col-md-1">
      <div class="form-group">
        <label for=""> </label>
        <div class="input-group">
          <button type="button" class="btn btn-primary btn-xs" onclick="cajacambio_tabla()" title="Filtrar"><i class="fa fa-search"></i></button>
          <!--<span class="input-group-addon"></span>-->
          <!--<button type="button" class="btn btn-success btn-xs" onclick="cajacambio_filtro()" title="Reestablecer"><i class="fa fa-refresh"></i></button>-->
        </div>
      </div>
    </div>

  </div>

</form>