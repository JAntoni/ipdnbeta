<?php

require_once('../../core/usuario_sesion.php');
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../cajacambio/Cajacambio.class.php");
$oCajacambio = new Cajacambio();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");
if($_POST['action_cajacambio']=="insertar")
{
  $response = [
    'estado' => 0,
    'mensaje' => 'Hace falta Datos'
  ];

  if(!empty($_POST['txt_cajcam_fec']) and !empty($_POST['txt_cajcam_imp']))
	{

    if(fecha_mysql($_POST['txt_cajcam_fec']) != date('Y-m-d')){
      $response['cajacambio_msj'] = 'NO PUEDE REGISTRAR MOVIMIENTOS QUE NO SEAN DEL DIA';
      $response['cajacambio_id'] = 0;
      echo json_encode($response); exit();
    }

    try {
      
      $xac=1;
      if($_POST['cmb_mon_id']==1)
      {
        $des = "SOLES A DOLARES";
      }
      if($_POST['cmb_mon_id']==2)
      {
        $des = "DOLARES A SOLES";
      }


      $fecha = fecha_mysql(date('d-m-Y'));
      $caj_id = $_POST['cmb_caj_id'];
			$mod_id = 0; //
			$modide_id = 0; //
                        
			$oCajacambio->tb_cajacambio_usureg= $_SESSION['usuario_id'];
			$oCajacambio->tb_cajacambio_usumod= $_SESSION['usuario_id'];
			$oCajacambio->tb_cajacambio_xac= $xac;
			$oCajacambio->tb_caja_id= $caj_id;
			$oCajacambio->tb_moneda_id= $_POST['cmb_mon_id'];
			$oCajacambio->tb_cajacambio_fec= $fecha;
			$oCajacambio->tb_cajacambio_imp= moneda_mysql($_POST['txt_cajcam_imp']);
			$oCajacambio->tb_cajacambio_tipcam= moneda_mysql($_POST['txt_cajcam_tipcam']);
			$oCajacambio->tb_cajacambio_cam= moneda_mysql($_POST['txt_cajcam_cam']);
			$oCajacambio->tb_cajacambio_des= $des;
			$oCajacambio->tb_modulo_id= $mod_id;
			$oCajacambio->tb_cajacambio_modid= $modide_id;
			$oCajacambio->tb_empresa_id= $_SESSION['empresa_id'];

			$result = $oCajacambio->insertar();
				if(intval($result['estado']) == 1){
					$cajcam_id = $result['cajacambio_id'];
				}
			$result = null;


      $egr_det ='CAMBIO DE MONEDA: ' . $des;
			$cue_id	=23;
			$subcue_id=0;
			$doc_id=9;
			$mod_id=60;
			$numdoc=$mod_id.'-'.$cajcam_id;
			$egr_est='1';
			$pro_id='1';

			$oEgreso->egreso_usureg = $_SESSION['usuario_id'];
			$oEgreso->egreso_usumod= $_SESSION['usuario_id'];
			$oEgreso->egreso_fec = $fecha;
			$oEgreso->documento_id = $doc_id;
			$oEgreso->egreso_numdoc = $numdoc;
			$oEgreso->egreso_det = $egr_det;
			$oEgreso->egreso_imp = moneda_mysql($_POST['txt_cajcam_imp']);
			$oEgreso->egreso_tipcam = moneda_mysql($_POST['txt_cajcam_tipcam']);
			$oEgreso->egreso_est = $egr_est;
			$oEgreso->cuenta_id = $cue_id;
			$oEgreso->subcuenta_id = $subcue_id;
			$oEgreso->proveedor_id = $pro_id;
			$oEgreso->cliente_id = 0;
			$oEgreso->usuario_id = 0;
			$oEgreso->caja_id = $caj_id;
			$oEgreso->moneda_id = $_POST['cmb_mon_id'];
			$oEgreso->modulo_id = $mod_id;
			$oEgreso->egreso_modide = $cajcam_id;
			$oEgreso->empresa_id = $_SESSION['empresa_id'];
			$oEgreso->egreso_ap = 0;

      $oEgreso->insertar();

			$ing_det ='CAMBIO DE MONEDA: ' . $des;
			$doc_id= 8;
			$mod_id=60;
			$numdoc=$mod_id.'-'.$cajcam_id;
			$cue_id	=24;
			$subcue_id=0;
			$ing_est='1';
			//$cli_id=1;
			$cli_id=1;

			if($_POST['cmb_mon_id']== 1) $mon_id = '2';
			if($_POST['cmb_mon_id']== 2) $mon_id = '1';

			$oIngreso->ingreso_usureg = $_SESSION['usuario_id'];
			$oIngreso->ingreso_usumod = $_SESSION['usuario_id'];
			$oIngreso->ingreso_fec = $fecha;
			$oIngreso->documento_id = $doc_id; 
			$oIngreso->ingreso_numdoc = $numdoc;
			$oIngreso->ingreso_det = $ing_det; 
			$oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cajcam_cam']);  
			$oIngreso->cuenta_id = $cue_id;
			$oIngreso->subcuenta_id = $subcue_id; 
			$oIngreso->cliente_id = $cli_id; 
			$oIngreso->caja_id = $caj_id; 
			$oIngreso->moneda_id = $mon_id;
			//valores que pueden ser cambiantes según requerimiento de ingreso
			$oIngreso->modulo_id = $mod_id; 
			$oIngreso->ingreso_modide = $cajcam_id;
			$oIngreso->empresa_id = $_SESSION['empresa_id'];
			$oIngreso->ingreso_fecdep = ''; 
			$oIngreso->ingreso_numope = ''; 
			$oIngreso->ingreso_mondep = 0; 
			$oIngreso->ingreso_comi = 0; 
			$oIngreso->cuentadeposito_id = 0; 
			$oIngreso->banco_id = 0; 
			$oIngreso->ingreso_ap = 0; 
			$oIngreso->ingreso_detex = '';

      $oIngreso->insertar();

      $response['estado'] = 1;
      $response['cajacambio_id'] = $cajcam_id;
      $response['cajacambio_msj'] = 'Se registró el cambio de moneda. ';
    }catch (Exception $e) {
      // Actualiza el mensaje con el error
      $response['cajacambio_msj'] = 'Error: ' . $e->getMessage();
      $response['cajacambio_id'] = 0;
    }
  }
  echo json_encode($response);
}
if($_POST['action_cajacambio']=="eliminar")
{
  $response = [
      'estado' => 0,
      'mensaje' => 'Intentelo nuevamente.'
  ];

	if(!empty($_POST['hdd_cajcam_id']))
	{
    try {
      /**** VALIDAR QUE SEA OPERACION DEL DIA DE HOY****/
      $result3 = $oCajacambio->mostrarUno($_POST['hdd_cajcam_id']);
      if($result3['estado']==1){
        if($result3['data']['tb_cajacambio_fec'] != date('Y-m-d')){
          $response['mensaje'] = 'NO PUEDE ELIMINAR MOVIMIENTOS QUE NO SEAN DE HOY';
          echo json_encode($response); exit();
        }
      }
      //$oCajacambio->modificar_campo($_POST['cajcam_id'],$_SESSION['usuario_id'],'xac','0');
      $oCajacambio->modificar_campo($_POST['hdd_cajcam_id'], 'tb_cajacambio_xac', 0, 'INT');

      $ing_id = 0;
      $mod_id=60;
      $result= $oIngreso->mostrar_por_modulo($mod_id,$_POST['hdd_cajcam_id'],0,0);
        if($result['estado']==1){
          foreach ($result['data'] as $key => $value) {
            $ing_id=$value['tb_ingreso_id'];
            if($ing_id>0)
              $oIngreso->modificar_campo($ing_id,'tb_ingreso_xac', 0, 'INT');
          }
        }
      $result = null;


      $egr_id = 0;
      $mod_id=60;
      $result2= $oEgreso->mostrar_por_modulo($mod_id,$_POST['hdd_cajcam_id'],0,1);
        if($result2['estado']==1){
          foreach ($result2['data'] as $key => $value) {
            $egr_id=$value['tb_egreso_id'];
            if($egr_id>0)
            //$oEgreso->modificar_campo($egr_id,$_SESSION['usuario_id'],'xac','0');
            $oEgreso->modificar_campo($egr_id,'tb_egreso_xac',0,'INT');
          }
        }
      $result2 = null;
        //$egr_id 	=$dt['tb_egreso_id'];

      // Actualiza la respuesta
      $response['estado'] = 1;
      $response['mensaje'] = 'Se realizaron todas las modificaciones. ';
    } catch (Exception $e) {
        // Actualiza el mensaje con el error
        $response['mensaje'] = 'Error: ' . $e->getMessage();
    }
	}
  // Devuelve la respuesta en formato JSON
  echo json_encode($response);
}
?>