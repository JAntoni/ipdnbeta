function cajacambio_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cajacambio/cajacambio_tabla.php",
    async:false,
    dataType: "html",                      
    data: $('#cajacambio_filtro').serialize(),
    beforeSend: function() {
      $('#msj_cajacambio_tabla').html("Cargando datos...");
      $('#msj_cajacambio_tabla').show(100);
      //$('#div_cajacambio_tabla').addClass("ui-state-disabled");
    },
    success: function(html){
      $('#div_cajacambio_tabla').html(html);
      estilos_datatable();
    },
    complete: function(data){
      console.log(data);
      $('#msj_cajacambio_tabla').removeClass("ui-state-disabled");
      $('#msj_cajacambio_tabla').hide(100);
    }
  });
}

function estilos_datatable(){
  datatable_global = $('#tbl_cajacambio').DataTable({
    "pageLength": 25,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [], orderable: false }
    ]
  });

  //datatable_texto_filtrar();
}

function cajacambio_form(usuario_act, cajacambio_id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"cajacambio/cajacambio_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      cajacambio_id: cajacambio_id,
      vista: 'cajacambio_tabla'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_cajacambio_form').html(data);
      	$('#modal_registro_cajacambio').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_cajacambio'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      	modal_hidden_bs_modal('modal_registro_cajacambio', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'cajacambio';
      	var div = 'div_modal_cajacambio_form';
      	permiso_solicitud(usuario_act, cajacambio_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){

		},
		error: function(data){
      alerta_error('Error', data.responseText)
			console.log(data);
		}
	});
}

function cajacambio_eliminar(id) {
  //$.alert('Contenido aquí', 'Titulo aqui');
  $.confirm({
      icon: 'fa fa-trash',
      title: 'Eliminar',
      content: '¿Está seguro de Eliminar?',
      type: 'blue',
      theme: 'material', // 'material', 'bootstrap'
      typeAnimated: true,
      buttons: {
          si: function () {
              //ejecutamos AJAX
              $.ajax({
                type: "POST",
                url: VISTA_URL + "cajacambio/cajacambio_reg.php",
                async: true,
                dataType: "json",
                data: ({
                  action_cajacambio: "eliminar",
                  hdd_cajcam_id: id
                }),
                beforeSend: function () {
//      $('#msj_aportes').html("Guardando...");
//      $('#msj_aportes').show(100);
                },
                success: function (data) {
                  console.log(data);
                  if(data.estado == 1){
                    swal_success("SISTEMA", data.mensaje, 2000);
                    cajacambio_tabla();
                  }else{
                    alerta_error('Error',data.mensaje);
                  }
                },
                complete: function (data) {
                    if (data.status != 200)
                        console.log(data);

                }
              });
          },
          no: function () {}
      }
  });
}

$(document).ready(function() {

  cajacambio_tabla()

  console.log('Perfil menu');

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
});