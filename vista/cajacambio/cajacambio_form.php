<?php
  session_name("ipdnsac");
  session_start();
  require_once('../../core/usuario_sesion.php');

  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../cajacambio/Cajacambio.class.php');
  $oCajacambio = new Cajacambio();
  require_once('../funciones/funciones.php');

  $direc = 'cajacambio';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cajacambio_id = $_POST['cajacambio_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Cajacambio Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Cajacambio';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Cajacambio';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Cajacambio';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en retencion
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cajacambio'; $modulo_id = $cajacambio_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    if($action == "insertar"){
      $fecha=date('d-m-Y');
      $estado='1';
      $caja_id=1;
    }
    //si la accion es modificar, mostramos los datos del cajacambio por su ID
    if(intval($cajacambio_id) > 0){
      $result = $ocajacambio->mostrarUno($cajacambio_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el cajacambio seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        /*else{
          $fecharegistro		=mostrar_fecha_hora($result['data']['tb_cajacambio_fecreg']);
          $fechamodificacion		=mostrar_fecha_hora($result['data']['tb_cajacambio_fecmod']);
          $fecha		=mostrar_fecha($result['data']['tb_cajacambio_fec']);
          $detalle		=$result['data']['tb_cajacambio_det'];
          $importe		=$result['data']['tb_cajacambio_imp'];
          $tipocambio		=$result['data']['tb_cajacambio_tipcam'];
          $caja_id		=$result['data']['tb_caja_id'];
          $moneda_id		=$result['data']['tb_moneda_id'];
          $estado		=$result['data']['tb_cajacambio_est'];
          $usuario_id_reg	=$result['data']['tb_cajacambio_usureg'];
          $usuario_id_mod	=$result['data']['tb_cajacambio_usumod'];

          if($usuario_id_reg>0)
          {
            $resultReg=$oUsuario->mostrarUno($usuario_id_reg);
            if($resultReg['estado'] != 1){
              $usuario_registra_grupo		=$resultReg['data']['tb_usuariogrupo_id'];
              $usuario_registra_grupo_nombre	=$resultReg['data']['tb_usuariogrupo_nom'];
              $usuario_registra_nombre	=$resultReg['data']['tb_usuario_nom'];
              $usuario_registra_apellido_paterno		=$resultReg['data']['tb_usuario_apepat'];
              $usuario_registra_apellido_materno		=$resultReg['data']['tb_usuario_apemat'];
              $usuario_registra_apellido_email		=$resultReg['data']['tb_usuario_ema'];
            
              $usuario_registra="$usuario_registra_nombre $usuario_registra_apellido_paterno $usuario_registra_apellido_materno";
            }
            $resultReg = NULL;
          }
          if($usuario_id_mod>0)
          {
            $resultMod=$oUsuario->mostrarUno($usuario_id_mod);
            if($resultMod['estado'] != 1){
              $usuario_modifica_grupo		=$resultMod['data']['tb_usuariogrupo_id'];
              $usuario_modifica_grupo_nombre	=$resultMod['data']['tb_usuariogrupo_nom'];
              $usuario_modifica_nombre	=$resultMod['data']['tb_usuario_nom'];
              $usuario_modifica_apellido_paterno		=$resultMod['data']['tb_usuario_apepat'];
              $usuario_modifica_apellido_materno		=$resultMod['data']['tb_usuario_apemat'];
              $usuario_modifica_apellido_email		=$resultMod['data']['tb_usuario_ema'];
              
              $usuario_modifica="$usuario_modifica_nombre $usuario_modifica_apellido_paterno $usuario_modifica_apellido_materno";
            }
            $resultMod = NULL;
          }
        }*/
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cajacambio" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cajacambio" method="post">
        <input name="action_cajacambio" id="action_cajacambio" type="hidden" value="<?php echo $_POST['action']?>">
        <input name="hdd_cajcam_id" id="hdd_cajcam_id" type="hidden" value="<?php echo $_POST['cajcam_id']?>">
        <input type="hidden" id="hdd_cajcam_usureg" name="hdd_cajcam_usureg" value="<?php echo $_SESSION['usuario_id']?>">
        <input type="hidden" id="hdd_cajcam_usumod" name="hdd_cajcam_usumod" value="<?php echo $_SESSION['usuario_id']?>">
        <input type="hidden" id="hdd_emp_id" name="hdd_emp_id" value="<?php echo $_SESSION['empresa_id']?>">
        <input type="hidden" id="hdd_action" name="action_cajacambio" value="<?php echo $action?>">
          
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-md-4">
                <label for="cmb_credito_tip" class="control-label">Fecha:</label>
                <div class="input-group">
                  <div class='input-group date' id='datetimepicker3'>
                    <input type='text' class="form-control input-sm" name="txt_cajcam_fec" id="txt_cajcam_fec" value="<?php echo $fecha; ?>" />
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-8">
                <label for="">Caja</label>
                <select class="form-control input-sm mayus" id="cmb_caj_id" name="cmb_caj_id">
                  <?php require('../caja/caja_select.php'); ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="cmb_mon_id" class="control-label">Moneda:</label>
                <select class="form-control" id="cmb_mon_id" name="cmb_mon_id">
                  <option value="0"></option>
                  <option value="1">Soles   -> Dolares</option>
                  <option value="2">Dolares -> Soles</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="txt_cajcam_tipcam" class="control-label">Tipo de Cambio:</label>
                <input type="text" name="txt_cajcam_tipcam" id="txt_cajcam_tipcam" class="form-control input-sm moneda2" value="<?php echo $tipocambio;?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_cajcam_imp" class="control-label">Monto:</label>
                <input type="text" name="txt_cajcam_imp" id="txt_cajcam_imp" class="form-control input-sm moneda2" value="<?php echo $importe;?>">
              </div>
              <div class="form-group col-md-6">
                <label for="txt_cajcam_cam" class="control-label">Cambio:</label>
                <input type="text" name="txt_cajcam_cam" id="txt_cajcam_cam" class="form-control input-sm moneda2" readonly>
              </div>
            </div>
              <?php
              if($action == "insertar"){
              ?>
            <div class="row">
              <div class="form-group col-md-12">
                <label for="txt_info" class="control-label">Responsable:</label>
                <input type="text" name="txt_info" id="txt_info" class="form-control input-sm" value="<?php echo $_SESSION['usuario_nombre'];?>" readonly>
              </div>
            </div>
            <?php
              }elseif ($action == "editar") {
              ?>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="txt_info" class="control-label">Registrado:</label>
                  <input type="text" name="txt_info" id="txt_info" class="form-control input-sm" value="<?php echo $usuario_registra;?>" readonly>
                </div>
                <div class="form-group col-md-6">
                  <label for="txt_info2" class="control-label">Modificado:</label>
                  <input type="text" name="txt_info2" id="txt_info2" class="form-control input-sm" value="<?php echo $usuario_modifica;?>" readonly>
                </div>
              </div>
              <?php
              }
              ?>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cambio?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cajacambio_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cajacambio">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cajacambio">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cajacambio">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cajacambio/cajacambio_form.js?ver=01';?>"></script>
