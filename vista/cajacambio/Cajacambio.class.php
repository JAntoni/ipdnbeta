<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cajacambio extends Conexion{
      
      public $tb_cajacambio_id;
      public $tb_cajacambio_fecreg;
      public $tb_cajacambio_fecmod;
      public $tb_cajacambio_usureg;
      public $tb_cajacambio_usumod;
      public $tb_cajacambio_xac;
      public $tb_caja_id;
      public $tb_moneda_id;
      public $tb_cajacambio_fec;
      public $tb_cajacambio_imp;
      public $tb_cajacambio_tipcam;
      public $tb_cajacambio_cam;
      public $tb_cajacambio_des;
      public $tb_modulo_id;
      public $tb_cajacambio_modid;
      public $tb_empresa_id;
              
      

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cajacambio(
                                        tb_cajacambio_fecreg ,
                                        tb_cajacambio_fecmod ,
                                        tb_cajacambio_usureg ,
                                        tb_cajacambio_usumod ,
                                        tb_cajacambio_xac ,
                                        tb_caja_id ,
                                        tb_moneda_id ,
                                        tb_cajacambio_fec ,
                                        tb_cajacambio_imp ,
                                        tb_cajacambio_tipcam ,
                                        tb_cajacambio_cam ,
                                        tb_cajacambio_des ,	
                                        tb_modulo_id ,	
                                        tb_cajacambio_modid ,	
                                        tb_empresa_id)
                                VALUES(
                                        NOW( ) ,
                                        NOW( ) ,
                                        :tb_cajacambio_usureg ,
                                        :tb_cajacambio_usumod ,
                                        :tb_cajacambio_xac ,
                                        :tb_caja_id ,
                                        :tb_moneda_id ,
                                        :tb_cajacambio_fec ,
                                        :tb_cajacambio_imp ,
                                        :tb_cajacambio_tipcam ,
                                        :tb_cajacambio_cam ,
                                        :tb_cajacambio_des ,	
                                        :tb_modulo_id ,	
                                        :tb_cajacambio_modid ,	
                                        :tb_empresa_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cajacambio_usureg", $this->tb_cajacambio_usureg, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cajacambio_usumod", $this->tb_cajacambio_usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cajacambio_xac", $this->tb_cajacambio_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_caja_id", $this->tb_caja_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cajacambio_fec", $this->tb_cajacambio_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajacambio_imp", $this->tb_cajacambio_imp, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajacambio_tipcam", $this->tb_cajacambio_tipcam, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajacambio_cam", $this->tb_cajacambio_cam, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajacambio_des", $this->tb_cajacambio_des, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_modulo_id", $this->tb_modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cajacambio_modid", $this->tb_cajacambio_modid, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_cajacambio_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['cajacambio_id'] = $tb_cajacambio_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($cajacambio_id, $cajacambio_nom, $cajacambio_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cajacambio SET tb_cajacambio_nom =:cajacambio_nom, tb_cajacambio_des =:cajacambio_des WHERE tb_cajacambio_id =:cajacambio_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cajacambio_id", $cajacambio_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cajacambio_nom", $cajacambio_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cajacambio_des", $cajacambio_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cajacambio_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cajacambio WHERE tb_cajacambio_id =:cajacambio_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cajacambio_id", $cajacambio_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cajacambio_id){
      try {
        $sql = "SELECT * FROM tb_cajacambio WHERE tb_cajacambio_id =:cajacambio_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cajacambio_id", $cajacambio_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cajacambios(){
      try {
        $sql = "SELECT * FROM tb_cajacambio";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function filtrar_cajacambio($caja_id, $moneda_id, $empresa_id, $fecha1, $fecha2){
      $caja = "";
      $moneda = "";
      $empresa = "";
      if($caja_id > 0){
        $caja = " AND cc.tb_caja_id=$caja_id ";
      }
      if($moneda_id > 0){
        $moneda = " AND cc.tb_moneda_id=$moneda_id ";
      }
      if($empresa_id > 0){
        $empresa = " AND cc.tb_empresa_id=$empresa_id ";
      }
      try {
        $sql = "SELECT * 
        FROM tb_cajacambio cc
        INNER JOIN tb_moneda m ON cc.tb_moneda_id = m.tb_moneda_id
        INNER JOIN tb_caja c ON cc.tb_caja_id = c.tb_caja_id
        INNER JOIN tb_empresa e ON cc.tb_empresa_id = e.tb_empresa_id
        WHERE cc.tb_cajacambio_xac=1
        AND DATE_FORMAT(cc.tb_cajacambio_fec, '%Y-%m-%d') BETWEEN :fecha1 AND :fecha2" .$caja .$moneda . $empresa;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay caja cambio registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function modificar_campo($cajacambio_id, $cajacambio_columna, $cajacambio_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($cajacambio_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_cajacambio SET ".$cajacambio_columna." =:cajacambio_valor WHERE tb_cajacambio_id =:cajacambio_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cajacambio_id", $cajacambio_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":cajacambio_valor", $cajacambio_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":cajacambio_valor", $cajacambio_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();
          $this->dblink->commit();
          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrar_cajacambio_modulo($modulo_id, $cajacambio_modid){
      try {
        $sql = "SELECT * FROM tb_cajacambio c INNER JOIN tb_moneda m ON c.tb_moneda_id = m.tb_moneda_id WHERE c.tb_modulo_id =:modulo_id AND c.tb_cajacambio_modid =:cajacambio_modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cajacambio_modid", $cajacambio_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
