<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cregarvehdocumento extends Conexion{
      private $cregarvehdocumento_id;
      private $cregarvehdocumento_nom;

      public function getCregarvehdocumento_id() {
        return $this->cregarvehdocumento_id;
      }
    
      public function setCregarvehdocumento_id($cregarvehdocumento_id){
        $this->cregarvehdocumento_id = $cregarvehdocumento_id;
      }
      
      public function getCregarvehdocumento_nom() {
        return $this->cregarvehdocumento_nom;
      }
    
      public function setCregarvehdocumento_nom($cregarvehdocumento_nom){
        $this->cregarvehdocumento_nom = $cregarvehdocumento_nom;
      }
      
    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO cgarvdoc(cgarvdoc_nom)
          VALUES (:cregarvehdocumento_nom)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cregarvehdocumento_nom", $this->getCregarvehdocumento_nom(), PDO::PARAM_STR);

        $result = $sentencia->execute();
        $cregarvehdocumento_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_cgarvdoc_id '] = $cregarvehdocumento_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE cgarvdoc SET cgarvdoc_nom = :cregarvehdocumento_nom WHERE cgarvdoc_id = :cregarvehdocumento_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cregarvehdocumento_nom", $this->getCregarvehdocumento_nom(), PDO::PARAM_STR);
        $sentencia->bindParam(":cregarvehdocumento_id ", $this->getCregarvehdocumento_id(), PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cregarvehdocumento_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE cgarvdoc SET cgarvdoc_xac=0 WHERE cgarvdoc_id=:cregarvehdocumento_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cregarvehdocumento_id", $cregarvehdocumento_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        echo "VER"; exit();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cregarvehdocumento_id){
      try {
        $sql = "SELECT * FROM cgarvdoc WHERE cgarvdoc_id =:cregarvehdocumento_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cregarvehdocumento_id", $cregarvehdocumento_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cregarvehdocumentos(){
      try {
        $sql = "SELECT * FROM cgarvdoc WHERE cgarvdoc_xac=1;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
