<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cregarvehdocumento/Cregarvehdocumento.class.php');
        $oCregarvehdocumento = new Cregarvehdocumento();

 	$action = $_POST['action'];
        
        $cregarvehdocumento_nom = mb_strtoupper($_POST['txt_cregarvehdocumento_nom'], 'UTF-8');

        $oCregarvehdocumento->setCregarvehdocumento_nom($cregarvehdocumento_nom);
        
 	if($action == 'insertar'){

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Cregarvehdocumento.';
 		if($oCregarvehdocumento->insertar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cregarvehdocumento registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$cregarvehdocumento_id = intval($_POST['hdd_cregarvehdocumento_id']);
                $oCregarvehdocumento->setCregarvehdocumento_id($cregarvehdocumento_id);
 		$data['estado'] = 0;
 		if($oCregarvehdocumento->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cregarvehdocumento modificado correctamente. '.$cregarvehdocumento_nom;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$cregarvehdocumento_id = intval($_POST['hdd_cregarvehdocumento_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Cregarvehdocumento.';

 		if($oCregarvehdocumento->eliminar($cregarvehdocumento_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cregarvehdocumento eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>