<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Cregarvehdocumento.class.php');
  $oCregarvehdocumento = new Cregarvehdocumento();

  $result = $oCregarvehdocumento->listar_cregarvehdocumentos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['cgarvdoc_id'].'</td>
          <td>'.$value['cgarvdoc_nom'].'</td>
          <td align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="cregarvehdocumento_form(\'L\','.$value['cgarvdoc_id'].')"><i class="fa fa-eye"></i> Ver</a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="cregarvehdocumento_form(\'M\','.$value['cgarvdoc_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cregarvehdocumento_form(\'E\','.$value['cgarvdoc_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_cregarvehdocumentos" class="table table-bordered table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th>ID</th>
      <th>Nombre</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
