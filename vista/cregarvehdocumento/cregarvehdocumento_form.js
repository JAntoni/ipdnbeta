
$(document).ready(function(){
  $('#form_cregarvehdocumento').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"cregarvehdocumento/cregarvehdocumento_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cregarvehdocumento").serialize(),
				beforeSend: function() {
					$('#cregarvehdocumento_mensaje').show(400);
					$('#btn_guardar_cregarvehdocumento').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#cregarvehdocumento_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#cregarvehdocumento_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		cregarvehdocumento_tabla();
		      		$('#modal_registro_cregarvehdocumento').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#cregarvehdocumento_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#cregarvehdocumento_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_cregarvehdocumento').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#cregarvehdocumento_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#cregarvehdocumento_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_cregarvehdocumento_nom: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			txt_cregarvehdocumento_nom: {
				required: "Ingrese un nombre para el cregarvehdocumento",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
