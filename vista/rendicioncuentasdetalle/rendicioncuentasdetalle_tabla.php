<?php
if (defined('APP_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}
require_once("../rendicioncuentas/RendicionCuentas.class.php");
$oRendicionCuentas = new RendicionCuentas();
require_once("RendicionCuentasDetalle.class.php");
$oRendicionCuentasDetalle = new RendicionCuentasDetalle();
require_once('../filestorage/Filestorage.class.php');
$oFilestorage = new Filestorage();

$rendicioncuentas_id = $_POST['rendicioncuentas_id'];
$fecha_hoy = date('d-m-Y');

/* FILTROPARA CREDITO MENORES */
$montoegreso = 0;
$total = 0;
$cuenta = $oRendicionCuentas->mostrarUno($rendicioncuentas_id);
if($cuenta['estado'] == 1){
  $montoegreso = $cuenta['data']['tb_egreso_imp'] ;
  $estado_rendicion = $cuenta['data']['tb_rendicioncuenta_estado'] ;
}
$vuelto = $montoegreso;
$result = $oRendicionCuentasDetalle->listar_rendicioncuenta_detalles($rendicioncuentas_id);
if ($result['estado'] == 1): ?>
  <label style="font-family: cambria;color: #006633;">LISTADO DETALLES DE RENDICION</label>
  <table cellspacing="1" class="table table-hover" style="font-family: cambria;font-size:15px">
    <thead>
      <tr id="tabla_cabecera">
        <td id="tabla_cabecera_fila" style="height: 22px"><b>ID</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>MONEDA</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>IMPORTE</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>ESTADO</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>DETALLE</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>ARCHIVO</b></td>
      </tr>
    </thead>
    <tbody>
      <?php
      $total = 0;
      foreach ($result['data'] as $key => $value) {
        $total += $value['tb_rendicioncuenta_detalle_importe'];
        $estado = '';
        if($value['tb_rendicioncuenta_detalle_estado']==1){
            $estado = '<span class="badge bg-orange">POR COBRAR</span>';
        }elseif($value['tb_rendicioncuenta_detalle_estado']==2){
            $estado = '<span class="badge bg-green">PAGADO</span>'; 
        }

        if($value['tb_rendicioncuenta_detalle_moneda'] == 1){
          $moneda = "S/. ";
        }elseif($value['tb_rendicioncuenta_detalle_moneda'] == 2){
          $moneda = "US$. ";
        }
        ?>
        <tr id="tabla_fila">
          <td align="center" id="tabla_fila"><?php echo $value['tb_rendicioncuenta_detalle_id']; ?></td>
          <td align="center" id="tabla_fila"><?php echo $moneda ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_rendicioncuenta_detalle_importe'] ?></td>
          <td align="center" id="tabla_fila"><?php echo $estado ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_rendicioncuenta_detalle_detalle'] ?></td>
          <td align="center" id="tabla_fila">
          <?php if(intval($value['tb_rendicioncuenta_detalle_archivo']) == 0){?>
            <a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="filestorage_form(<?php echo $value['tb_rendicioncuenta_detalle_id'] ?>)" title="SUBIR ARCHIVO"><i class="fa fa-upload"></i></a>
            <?php }else{ 
                $result_file = $oFilestorage->listar_filestorages('rendicioncuentadetalle', $value['tb_rendicioncuenta_detalle_id']);
                if($result_file['estado'] == 1){
                  $url = $result_file['data'][0]['filestorage_url'];
                  $url = "$url";
              ?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="previewFile('<?php echo $url ?>')" title="VER ARCHIVO"><i class="fa fa-eye"></i></a>
            <?php }
            } ?>
          </td>
        </tr>
        <?php
      }
      $vuelto = $montoegreso - $total;
      ?>
    </tbody>
  </table>
  <?php 
  else:
    ?>
    <label style="font-family: cambria;color: #006633;">LISTADO DETALLES DE RENDICION</label>
    <table cellspacing="1" class="table table-hover" style="font-family: cambria;font-size:15px">
      <thead>
        <tr id="tabla_cabecera">
          <td id="tabla_cabecera_fila" style="height: 22px"><b>ID</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>IMPORTE</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>ESTADO</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>DETALLE</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>ARCHIVO</b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="5" class="text-center align-middle "><i>(Sin datos)</i></td>
        </tr>
      </tbody>
    </table>
    <?php 
endif;
$result = null;
$result = $oRendicionCuentas->listar_gv_rendicioncuenta($rendicioncuentas_id);
if ($result['estado'] == 1): ?>
  <label style="font-family: cambria;color: #006633;">LISTADO GV POR RENDICION</label>
  <table cellspacing="1" class="table table-hover" style="font-family: cambria;font-size:15px">
    <thead>
      <tr id="tabla_cabecera">
        <td id="tabla_cabecera_fila" style="height: 22px"><b>ID GV</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>TIPO</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>MONEDA</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>CANTIDAD</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>IMPORTE</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>DETALLE</b></td>
      </tr>
    </thead>
    <tbody>
    <?php
      $montogasto = 0;
      foreach ($result['data'] as $key => $value) {
        $montogasto += $value['tb_gasto_ptl'];
        $tipo = '';
        if($value['tb_gasto_tipgas']==1){
            $tipo = 'Producto';
        }elseif($value['tb_gasto_tipgas']==2){
            $tipo = 'Servicio'; 
        }
        $moneda = '';
        if($value['tb_moneda_id']==1){
            $moneda = 'S/. ';
        }elseif($value['tb_moneda_id']==2){
            $moneda = 'US$. '; 
        }
        ?>
        <tr id="tabla_fila">
          <td align="center" id="tabla_fila"><?php echo $value['tb_gasto_id']; ?></td>
          <td align="center" id="tabla_fila"><?php echo $tipo ?></td>
          <td align="center" id="tabla_fila"><?php echo $moneda ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_gasto_can'] ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_gasto_ptl'] ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_gasto_des'] ?></td>
        </tr>
        <?php
      }
      $vuelto = $vuelto - $montogasto;
      ?>
    </tbody>
    </table>
  <?php
else: ?>
  <table cellspacing="1" class="table table-hover" style="font-family: cambria;font-size:15px">
    <thead>
      <tr id="tabla_cabecera">
        <td id="tabla_cabecera_fila" style="height: 22px"><b>ID GV</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>TIPO</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>PROVEEDOR</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>MONEDA</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>CANTIDAD</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>IMPORTE</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>DETALLE</b></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="6" class="text-center align-middle "><i>(Sin datos)</i></td>
      </tr>
    </tbody>
    </table>
    <?php  
endif;
$result = null;
$result = $oRendicionCuentasDetalle->listar_rendicioncuenta_ingresos($rendicioncuentas_id); 
if ($result['estado'] == 1): ?>
  <label style="font-family: cambria;color: #006633;">LISTADO INGRESOS POR RENDICION</label>
  <table cellspacing="1" class="table table-hover" style="font-family: cambria;font-size:15px">
    <thead>
      <tr id="tabla_cabecera">
        <td id="tabla_cabecera_fila" style="height: 22px"><b>ID INGRESO</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>NUMERO DOC</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>MONEDA</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>IMPORTE</b></td>
        <td id="tabla_cabecera_fila" style="height: 22px"><b>DETALLE</b></td>
      </tr>
    </thead>
    <tbody>
    <?php
      $ingresos = 0;
      foreach ($result['data'] as $key => $value) {
        $ingresos += $value['tb_ingreso_imp'];
        $moneda = '';
        if($value['tb_moneda_id']==1){
            $moneda = 'S/. ';
        }elseif($value['tb_moneda_id']==2){
            $moneda = 'US$. '; 
        }
        ?>
        <tr id="tabla_fila">
          <td align="center" id="tabla_fila"><?php echo $value['tb_ingreso_id']; ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_ingreso_numdoc'] ?></td>
          <td align="center" id="tabla_fila"><?php echo $moneda ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_ingreso_imp'] ?></td>
          <td align="center" id="tabla_fila"><?php echo $value['tb_ingreso_det'] ?></td>
        </tr>
        <?php
      }
      $vuelto = $vuelto - $ingresos;
      ?>
    </tbody>
  </table>
  <?php 
else:
  ?>
    <label style="font-family: cambria;color: #006633;">LISTADO INGRESOS POR RENDICION</label>
    <table cellspacing="1" class="table table-hover" style="font-family: cambria;font-size:15px">
      <thead>
        <tr id="tabla_cabecera">
          <td id="tabla_cabecera_fila" style="height: 22px"><b>ID INGRESO</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>NUMERO DOC</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>MONEDA</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>IMPORTE</b></td>
          <td id="tabla_cabecera_fila" style="height: 22px"><b>DETALLE</b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="5" class="text-center align-middle "><i>(Sin datos)</i></td>
        </tr>
      </tbody>
    </table>
  <?php 
endif;
$result = null;
?>
<div class="col-md-12">
      <ul class="list-group list-group-unbordered">
        <li class="list-group-item" style="border-color: #000000">
            <p>
            <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                      <label for="txt_mont_egreso">Egreso: </label>
                      <input class="form-control input-sm moneda" name="txt_mont_egreso" type="text" id="txt_mont_egreso" value="<?php echo $montoegreso; ?>" readonly>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                      <label for="txt_monto_detalle">Detalle: </label>
                      <input class="form-control input-sm moneda" name="txt_monto_detalle" type="text" id="txt_monto_detalle" value="<?php echo $total; ?>" readonly>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                      <label for="txt_monto_gv">Gasto GV: </label>
                      <input class="form-control input-sm moneda" name="txt_monto_gv" type="text" id="txt_monto_gv" value="<?php echo $montogasto; ?>" readonly>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                      <label for="txt_monto_vuelto">Diferencia: </label>
                      <input class="form-control input-sm moneda" name="txt_monto_vuelto" type="text" id="txt_monto_vuelto" value="<?php echo $vuelto; ?>" readonly>
                  </div>
                </div>
            </div>
            <br>
            <div class="row">
                <?php if(intval($estado_rendicion) == 1 && (intval($vuelto) <= 0)) {?>
                <div class="col-md-3 pull-right">
                  <div class="form-group">
                      <label for="txt_monto_vuelto">Cerrar: </label><br>
                      <a style="margin-top: 3px; margin-left: 2px;" class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="cerrar_rendicion(<?php echo $rendicioncuentas_id; ?>)" title="CERRAR"><i class="fa fa-remove" aria-hidden="true"></i></a>
                  </div>
                </div>
                <?php } ?>
                <?php if(intval($vuelto) > 0) {?>
                <div class="col-md-3 pull-right">
                  <div class="form-group">
                      <label for="txt_monto_vuelto">Accion: </label><br>
                      <a style="margin-top: 3px; margin-left: 2px;" class="btn btn-success btn-xs" href="javascript:void(0)" onclick="ingreso_form('I', 0)" title="DEVOLUCION"><i class="fa fa-money" aria-hidden="true"></i></a>
                  </div>
                </div>
                <?php } ?>
            </div>
            <p>
        </li>
      </ul>
    </div>