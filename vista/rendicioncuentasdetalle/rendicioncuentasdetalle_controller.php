<?php
  require_once('../../core/usuario_sesion.php');
 	require_once('../rendicioncuentasdetalle/RendicionCuentasDetalle.class.php');
  $oRendicionCuentasDetalle = new RendicionCuentasDetalle();
  require_once("../rendicioncuentas/RendicionCuentas.class.php");
  $oRendicionCuentas = new RendicionCuentas();

  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $action = $_POST['action'];

 	if($action == 'insertar'){
    $rendicioncuentas_id = $_POST['hdd_rendicioncuentas_id'];
    $vista = $_POST['hdd_vista'];
    $moneda_id = $_POST['cmb_mon_id'];
    $importe = $_POST['txt_importe'];
    $detalle = $_POST['txt_detalle'];

    $result = $oRendicionCuentas->mostrarUno($rendicioncuentas_id);
    if ($result['estado'] == 1){
      $egreso_importe = $result['data']['tb_egreso_imp'];
    }

    $result = $oRendicionCuentasDetalle->listar_rendicioncuenta_detalles($rendicioncuentas_id);
    if ($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $total += $value['tb_rendicioncuenta_detalle_importe'];
      }
    }

    $total = $total + $importe;

    if($egreso_importe < $total){
      $data['estado'] = 0;
 		  $data['mensaje'] = 'EL IMPORTE EXCEDE EL MONTO A RENDIR';
       echo json_encode($data);
       exit();
    }

    $data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el detalle.';
    //move_filestorageed_file($tempFile, $targetFile); insertamos las imágenes
    $oRendicionCuentasDetalle->rendicioncuenta_detalle_moneda = $moneda_id;
    $oRendicionCuentasDetalle->rendicioncuenta_detalle_importe = moneda_mysql($importe);
    $oRendicionCuentasDetalle->rendicioncuenta_detalle_detalle = $detalle;
    $oRendicionCuentasDetalle->rendicioncuenta_detalle_estado = 2;
    $oRendicionCuentasDetalle->rendicioncuenta_detalle_usureg = $_SESSION['usuario_id'];
    $oRendicionCuentasDetalle->rendicioncuenta_id = $rendicioncuentas_id;

    $return = $oRendicionCuentasDetalle->insertar();

    if($return['estado'] == 1){
      $data['estado'] = 1;
      $data['mensaje'] = 'REGISTRO EXITOSO';
    }

    echo json_encode($data);
 	}
 	else if($action == 'eliminar'){
 		$rendicioncuentasdetalle_id = intval($_POST['hdd_rendicioncuentasdetalle_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el detalle.';

    if($oRendicionCuentasDetalle->eliminar($rendicioncuentasdetalle_id)){
      $data['estado'] = 1;
      $data['mensaje'] = 'Detalle eliminado correctamente.';
    }

 		echo json_encode($data);
 	}
  else if($action == 'listar'){

    $rendicioncuentas_id = $_POST['rendicioncuentas_id'];
    $registros = '';
    $total = 0;
    /** LISTADO DE DETALLES **/
    $result = $oRendicionCuentasDetalle->listar_rendicioncuenta_detalles($rendicioncuentas_id);
    if($result['estado']==1){
        foreach ($result['data'] as $key => $value) {
            $total += $value['tb_rendicioncuenta_detalle_importe'];
            $estado = '';
            if($value['tb_rendicioncuenta_detalle_estado']==1){
                $estado = '<span class="badge bg-orange">POR COBRAR</span>';
            }elseif($value['tb_rendicioncuenta_detalle_estado']==2){
                $estado = '<span class="badge bg-green">CANCELADO</span>';
            }

            $registros = '
            <tr id="tabla_fila" style="height: 20px">
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$value['tb_rendicioncuenta_detalle_id'].'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$value['tb_rendicioncuenta_detalle_importe'].'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$value['tb_rendicioncuenta_detalle_detalle'].'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$estado.'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>BOTON</b></td>
            </tr>';
        }
    }
    /** LISTADO DE DETALLES **/
    $data['estado'] = 1;
    $data['mensaje'] = 'LISTADO';
    $data['registros'] = $registros;
    $data['total'] = $total;

    echo json_encode($data);
  }
  else if($action == 'eliminarxrendicion'){
    $rendicioncuentas_id = intval($_POST['hdd_rendicioncuentas_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar los detalle.';

    if($oRendicionCuentasDetalle->eliminarxrendicion($rendicioncuentas_id)){
      $data['estado'] = 1;
      $data['mensaje'] = 'Detalles eliminados correctamente.';
    }

    echo json_encode($data);
  }
 	else{
	 	echo 'No se ha identificado ningún tipo de transacción para '.$action;
 	}

?>
