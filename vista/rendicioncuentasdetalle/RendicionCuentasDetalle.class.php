<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class RendicionCuentasDetalle extends Conexion{

    public $rendicioncuenta_detalle_id;
    public $rendicioncuenta_detalle_usureg;
    public $rendicioncuenta_detalle_usumod;
    public $rendicioncuenta_detalle_moneda;
    public $rendicioncuenta_detalle_importe;
    public $rendicioncuenta_detalle_detalle;
    public $rendicioncuenta_detalle_estado;
    public $rendicioncuenta_detalle_archivo;
    public $rendicioncuenta_id;

    function insertar() {
      $this->dblink->beginTransaction();
      try {
          $sql = "INSERT INTO tb_rendicioncuenta_detalle(
                                tb_rendicioncuenta_detalle_moneda, 
                                tb_rendicioncuenta_detalle_importe, 
                                tb_rendicioncuenta_detalle_detalle,
                                tb_rendicioncuenta_detalle_estado,
                                tb_rendicioncuenta_id)
                  VALUES (:moneda, 
                          :importe, 
                          :detalle,
                          :estado,
                          :rendicioncuenta_id)";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":moneda", $this->rendicioncuenta_detalle_moneda, PDO::PARAM_INT);
          $sentencia->bindParam(":importe", $this->rendicioncuenta_detalle_importe, PDO::PARAM_STR);
          $sentencia->bindParam(":detalle", $this->rendicioncuenta_detalle_detalle, PDO::PARAM_STR);
          $sentencia->bindParam(":estado", $this->rendicioncuenta_detalle_estado, PDO::PARAM_INT);
          $sentencia->bindParam(":rendicioncuenta_id", $this->rendicioncuenta_id, PDO::PARAM_INT);

          $result['estado'] = $sentencia->execute();
          $result['nuevo'] = $this->dblink->lastInsertId();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }

    function eliminar($rendicioncuenta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_rendicioncuenta_detalle SET tb_rendicioncuenta_detalle_xac=0  WHERE tb_rendicioncuenta_detalle_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function listar_rendicioncuenta_detalles($rendicioncuenta_id){
      try {
        $sql = "SELECT rcd.*
        FROM tb_rendicioncuenta_detalle rcd
        INNER JOIN tb_rendicioncuenta rc ON rcd.tb_rendicioncuenta_id = rc.tb_rendicioncuenta_id
        WHERE rcd.tb_rendicioncuenta_id = :rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay rendiciones";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function modificar_campo($rendicioncuentadetalleid, $rendicioncuentadetalle_columna, $rendicioncuentadetallevalor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($rendicioncuentadetalle_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_rendicioncuenta_detalle SET " . $rendicioncuentadetalle_columna . " = :rendicioncuentadetalle_valor WHERE tb_rendicioncuenta_detalle_id = :rendicioncuentadetalleid;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(':rendicioncuentadetalleid', $rendicioncuentadetalleid, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(':rendicioncuentadetalle_valor', $rendicioncuentadetallevalor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(':rendicioncuentadetalle_valor', $rendicioncuentadetallevalor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function listar_rendicioncuenta_ingresos($rendicioncuenta_id){
      try {
        $sql = "SELECT rci.*, i.*
        FROM tb_rendicioncuenta rc
        INNER JOIN tb_rendicioncuenta_ingreso rci ON rc.tb_rendicioncuenta_id = rci.tb_rendicioncuenta_id
        INNER JOIN tb_ingreso i ON rci.tb_ingreso_id = i.tb_ingreso_id
        WHERE rci.tb_rendicioncuenta_id = :rendicioncuenta_id AND i.tb_ingreso_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay rendiciones";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function agregar_ingresodetalle($rendicioncuentas_id, $ingreso_id, $usuario_id) {
      $this->dblink->beginTransaction();
      try {
          $sql = "INSERT INTO tb_rendicioncuenta_ingreso(
                                tb_rendicioncuenta_id, 
                                tb_ingreso_id, 
                                tb_rendicioncuenta_ingreso_usureg,
                                tb_rendicioncuenta_ingreso_xac)
                  VALUES (
                          :rendicioncuenta_id, 
                          :ingreso_id, 
                          :usureg,
                          1)";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuentas_id, PDO::PARAM_INT);
          $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_INT);
          $sentencia->bindParam(":usureg", $usuario_id, PDO::PARAM_INT);

          $result['estado'] = $sentencia->execute();
          $result['nuevo'] = $this->dblink->lastInsertId();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }

    function eliminarxrendicion($rendicioncuenta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_rendicioncuenta_detalle SET tb_rendicioncuenta_detalle_xac=0  WHERE tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function eliminaringresosxrendicion($rendicioncuenta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_rendicioncuenta_ingreso SET tb_rendicioncuenta_ingreso_xac=0  WHERE tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUnoIngresoxRendicion($rendicioncuenta_id){
      try {
        $sql = "SELECT *
                FROM tb_rendicioncuenta_ingreso
                WHERE tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No existe ingresos por rendicion";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
