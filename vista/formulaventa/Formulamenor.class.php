<?php

  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Formulamenor extends Conexion{
    public $tb_formula_id;
    public $tb_formula_nom;
    public $tb_formula_rec;
    public $tb_formula_pro;
    public $tb_formula_est;
            
    function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_formulaventa(
                    tb_formula_nom, tb_formula_rec, tb_formula_pro) 
                    VALUES (:tb_formula_nom,:tb_formula_rec,:tb_formula_pro)";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_formula_nom", $this->tb_formula_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_formula_rec", $this->tb_formula_rec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_formula_pro", $this->tb_formula_pro, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_formula_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_formula_id'] = $tb_formula_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
  function mostrarUno($formu_id){
      try {
         $sql = "SELECT * FROM tb_formulaventa where tb_formula_id =:tb_formula_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_formula_id", $formu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
        
  function editar(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_formulaventa set 
                                            tb_formula_nom = :tb_formula_nom, 
                                            tb_formula_rec = :tb_formula_rec, 
                                            tb_formula_pro = :tb_formula_pro 
                                     where 
                                            tb_formula_id =:tb_formula_id";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_formula_nom", $this->tb_formula_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_formula_rec", $this->tb_formula_rec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_formula_pro", $this->tb_formula_pro, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_formula_id", $this->tb_formula_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
  function eliminar($formu_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_formulaventa where tb_formula_id =$formu_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_formula_id", $formu_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar(){
      try {
            $sql = "SELECT * FROM tb_formulaventa";

           $sentencia = $this->dblink->prepare($sql);

           $sentencia->execute();

           if ($sentencia->rowCount() > 0) {
             $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
             $retorno["estado"] = 1;
             $retorno["mensaje"] = "exito";
             $retorno["data"] = $resultado;
             $sentencia->closeCursor(); //para libera memoria de la consulta
           }
           else{
             $retorno["estado"] = 0;
             $retorno["mensaje"] = "No hay Comisiones registradas";
             $retorno["data"] = "";
           }

           return $retorno;
         } catch (Exception $e) {
        throw $e;
      }
    }
     
  function vigente($formu_id){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_formulaventa set tb_formula_est =1 where tb_formula_id =:tb_formula_id";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_formula_id",$formu_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $this->dblink->commit();
            return $result; //si es correcto retorna 1
          } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
          }
    }
     
  function limpiar_estados(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_formulaventa set tb_formula_est =0";
            
            $sentencia = $this->dblink->prepare($sql);

            $result = $sentencia->execute();
            $this->dblink->commit();
            return $result; //si es correcto retorna 1
          } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
          }
    }
   
    function mostrar_vigente(){
      try {
            $sql = "SELECT * FROM tb_formulaventa where tb_formula_est =1";

           $sentencia = $this->dblink->prepare($sql);
           $sentencia->execute();

           if ($sentencia->rowCount() > 0) {
             $resultado = $sentencia->fetch();
             $retorno["estado"] = 1;
             $retorno["mensaje"] = "exito";
             $retorno["data"] = $resultado;
             $sentencia->closeCursor(); //para libera memoria de la consulta
           }
           else{
             $retorno["estado"] = 0;
             $retorno["mensaje"] = "No hay Comisiones registradas";
             $retorno["data"] = "";
           }

           return $retorno;
         } catch (Exception $e) {
        throw $e;
      }
    }

}
?>