<?php


if(defined('VISTA_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'formulaventa/Formulamenor.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../../core/usuario_sesion.php');
    require_once('../formulaventa/Formulamenor.class.php');
    require_once('../funciones/fechas.php');
  }

$oFormula = new Formulamenor();
$dts=$oFormula->listar();

?>

<?php if($dts['estado']==1): ?>
  <table cellspacing="1" id="tabla_vehiculomarca" class="table table-hover">
    <thead>
      <tr id="tabla_cabecera"> 
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">NOMBRE</th>
        <th id="tabla_cabecera_fila">RECUPERACIÓN</th>
        <th id="tabla_cabecera_fila">PROYECCIÓN</th>
        <th id="tabla_cabecera_fila">ESTADO</th>
        <th  id="tabla_cabecera_fila"width="16%">&nbsp;</th>
      </tr>
    </thead>  
    <tbody> <?php
        foreach ($dts['data']as $key=>$dt){ ?>
        <tr id="tabla_cabecera_fila">
          <td id="tabla_fila"><?php echo $dt['tb_formula_id']?></td>
          <td id="tabla_fila"><?php echo $dt['tb_formula_nom']?></td>
          <td id="tabla_fila"><?php echo $dt['tb_formula_rec']?></td>
          <td id="tabla_fila"><?php echo $dt['tb_formula_pro']?></td>
          <td id="tabla_fila">
            <?php
              $estado = '<b>NO VIGENTE</b>';
              if($dt['tb_formula_est'] == 1)
                $estado = '<strong style="color: green;">VIGENTE</strong>';
              echo $estado;
            ?>
          </td>
          <td  id="tabla_fila" align="center">
            <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="formulaventa_form('M','<?php echo $dt['tb_formula_id']?>')">Editar</a>
            <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="formulaventa_form('E','<?php echo $dt['tb_formula_id']?>')">Eliminar</a>

            <?php if($dt['tb_formula_est'] == 0){
              echo '<a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="vigente_formulamenor('.$dt['tb_formula_id'].')">Vigente</a>';
            } ?>
          </td>
        </tr> <?php
	} ?>
    </tbody>
  </table>
<?php endif;?>