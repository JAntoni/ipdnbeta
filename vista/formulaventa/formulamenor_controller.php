<?php
require_once('../../core/usuario_sesion.php');
require_once("../formulaventa/Formulamenor.class.php");
$oFormula = new Formulamenor();

if($_POST['action']=="insertar")
{
    $nombre = strtoupper($_POST['txt_formenor_nom']);
    $recuperacion = $_POST['txt_formenor_rec'];
    $proyeccion = $_POST['txt_formenor_pro'];

    $oFormula->tb_formula_nom=$nombre;
    $oFormula->tb_formula_rec=$recuperacion;
    $oFormula->tb_formula_pro=$proyeccion;
    $oFormula->insertar();

    $data['msj'] = 'Se guardó la fórmula para venta de menores.';
    echo json_encode($data);
}

if($_POST['action']=="modificar")
{
  $formula_id = $_POST['hdd_formulamenor_id'];
  $nombre = strtoupper($_POST['txt_formenor_nom']);
  $recuperacion = $_POST['txt_formenor_rec'];
  $proyeccion = $_POST['txt_formenor_pro'];
  
    
    $oFormula->tb_formula_id=$formula_id;
    $oFormula->tb_formula_nom=$nombre;
    $oFormula->tb_formula_rec=$recuperacion;
    $oFormula->tb_formula_pro=$proyeccion;
    $oFormula->editar();

  $data['msj'] = 'Se modificó la fórmula para venta de menores.';
  echo json_encode($data);
}

if($_POST['action']=="eliminar")
{
  $formula_id = $_POST['hdd_formulamenor_id'];
  $oFormula->eliminar($formula_id);

  $data['msj'] = 'Se eliminó la fórmula para venta de menores.';
  echo json_encode($data);
}

if($_POST['action']=="vigente")
{
  $formula_id = $_POST['formenor_id'];
  $oFormula->limpiar_estados(); //asigna todos los estados a 0 -> no vigente
  $oFormula->vigente($formula_id); //cambia de estado a 1->vigente
  
  $data['msj'] = 'Se ha seleccionado la fórmula vigente.';
  echo json_encode($data);
}

?>