<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../formulaventa/Formulamenor.class.php');
  $oFormulamenor = new Formulamenor();
  require_once('../funciones/funciones.php');

  $direc = 'formulaventa';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $formulamenor_id = $_POST['formulamenor_id'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Formula menor Registrado';
}
if ($usuario_action == 'I') {
    $titulo = 'Registrar Formula menor';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Formula menor';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Formula menor';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en formulamenor
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'formulamenor'; $modulo_id = $formulamenor_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del formulamenor por su ID
    if(intval($formulamenor_id) > 0){
      $result = $oFormulamenor->mostrarUno($formulamenor_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el formulamenor seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $tb_formula_nom = $result['data']['tb_formula_nom'];
          $tb_formula_rec = $result['data']['tb_formula_rec'];
          $tb_formula_pro = $result['data']['tb_formula_pro'];
          $tb_formula_est = $result['data']['tb_formula_est'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_formulamenor" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="for_formenor" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_formulamenor_id" value="<?php echo $formulamenor_id;?>">
          
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                  <label for="txt_formenor_nom" class="control-label">Nombre </label>
                  <input type="text" class="form-control input-sm" name="txt_formenor_nom" id="txt_formenor_nom" value="<?php echo $tb_formula_nom?>" size="30" maxlength="50">
                </div>
            </div>
              <p>
            <div class="row">
                <div class="col-md-6">
                  <label for="txt_formenor_nom" class="control-label">Recuperacion </label>
                  <input type="text"  class="form-control input-sm" name="txt_formenor_rec" id="txt_formenor_rec" value="<?php echo $tb_formula_rec?>" size="30" maxlength="50">
                </div>
                <div class="col-md-6">
                  <label for="txt_formenor_nom" class="control-label">Proyección </label>
                  <input type="text"  class="form-control input-sm" name="txt_formenor_pro" id="txt_formenor_pro" value="<?php echo $tb_formula_pro?>" size="30" maxlength="50">
                </div>
            </div>
              <p>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Formula menor?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="formulamenor_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_formulamenor">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_formulamenor">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_formulamenor">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/formulaventa/formulamenor_form.js';?>"></script>
