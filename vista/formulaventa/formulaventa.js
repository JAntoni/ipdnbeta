function formulaventa_form(usuario_act, formulaventa_id){ 
  $.ajax({
        type: "POST",
        url: VISTA_URL+"formulaventa/formulamenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            formulamenor_id: formulaventa_id,
            vista: 'formulaventa'
          }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
              $('#div_formulamenor_form').html(data);
              $('#modal_registro_formulamenor').modal('show');

              //desabilitar elementos del form si es L (LEER)
              if(usuario_act == 'L' || usuario_act == 'E')
                form_desabilitar_elementos('form_formulamenor'); //funcion encontrada en public/js/generales.js

              //funcion js para limbiar el modal al cerrarlo
              modal_hidden_bs_modal('modal_registro_formulamenor', 'limpiar'); //funcion encontrada en public/js/generales.js
              //funcion js para agregar un largo automatico al modal, al abrirlo
              modal_height_auto('modal_registro_formulamenor'); //funcion encontrada en public/js/generales.js
              //funcion js para agregar un ancho automatico al modal, al abrirlo
              modal_width_auto('modal_registro_formulamenor', 75); //funcion encontrada en public/js/generales.js
          }
          else{
            //llamar al formulario de solicitar permiso
            var modulo = 'formulaventa';
            var div = 'div_formulamenor_form';
            permiso_solicitud(usuario_act, formulaventa_id, modulo, div); //funcion ubicada en public/js/permiso.js
          }
        },
        complete: function(data){
            
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
//            console.log(data.responseText);
        }
	});
}

function formulamenor_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"formulaventa/formulamenor_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#formulaventa_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_formulaventa_tabla').html(data);
      $('#formulaventa_mensaje_tbl').hide(300);
      
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#formulaventa_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function vigente_formulamenor(id){
  //$.alert('Contenido aquí', 'Titulo aqui');
  $.confirm({
    icon: 'fa fa-print',
    title: 'Imprimir',
    content: '¿ Realmente desea seleccionar esta fórmula ?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        //ejecutamos AJAX
       $.ajax({
            type: "POST",
            url: VISTA_URL+"formulaventa/formulamenor_controller.php",
            async:true,
            dataType: "json",
            data: ({
              action: "vigente",
              formenor_id: id
            }),
            beforeSend: function() {
              $('#formulamenor_mensaje').html("Cargando...");
              $('#formulamenor_mensaje').show(100);
            },
            success: function(data){
              $('#formulamenor_mensaje').html(data.msj);
            },
            complete: function(){
              formulamenor_tabla();
            }
          });
      },
      no: function () {}
    }
  });
}

$(document).ready(function() {
  formulamenor_tabla();
});