
$(document).ready(function() {
    $('#txt_formenor_rec').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0',
    vMax: '9.99'
  });
    $('#txt_formenor_pro').autoNumeric({
      //aSign: 'S/. ',
      //pSign: 's',
      vMin: '0',
      vMax: '10'
    });
    
    
    $("#for_formenor").validate({
        submitHandler: function() {
            $.ajax({
                    type: "POST",
                    url: VISTA_URL+"formulaventa/formulamenor_controller.php",
                    async:true,
                    dataType: "json",
                    data: $("#for_formenor").serialize(),
                    beforeSend: function() {
                            $("#modal_registro_formulamenor" ).modal('hide');
                            $('#formulamenor_mensaje').html("Guardando...");
                            $('#formulamenor_mensaje').show(100);
                    },
                    success: function(data){						
                        $('#formulamenor_mensaje').html(data.msj);
                        formulamenor_tabla();
                    },
                    complete: function(data){
                        console.log(data);
                    }
                });
		},
		rules: {
			txt_formenor_nom: {
				required: true
			},
                        txt_formenor_rec: {
                          required: true
                        },
                        txt_formenor_pro: {
                          required: true
                        }
                        },
                        messages: {
                                txt_formenor_nom: {
                                        required: '*'
                                },
                        txt_formenor_rec: {
                          required: '*'
                        },
                        txt_formenor_pro: {
                          required: '*'
                        }
                                  }
	});
    
});

