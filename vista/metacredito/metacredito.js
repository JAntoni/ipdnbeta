
$(document).ready(function () {
    
    
    
    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy"
        //startDate: "-0d"
//        endDate: new Date()
    });
    
    
    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_ing_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
        metacredito_tabla();
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_ing_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
        metacredito_tabla();
    });

    metacredito_tabla();
});



function metacredito_form(usuario_act, metacredito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "metacredito/metacredito_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            metacredito_id: metacredito_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_metacredito_form').html(data);
                $('#modal_registro_metacredito').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_metacredito'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_metacredito', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'metacredito';
                var div = 'div_modal_metacredito_form';
                permiso_solicitud(usuario_act, metacredito_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function metacredito_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "metacredito/metacredito_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fecha1: $("#txt_fil_ing_fec1").val(),
            fecha2: $("#txt_fil_ing_fec2").val()
            
        }),
        beforeSend: function () {
            $('#metacredito_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_metacredito_tabla').html(data);
            $('#metacredito_mensaje_tbl').hide(300);
        },
        complete: function (data) {
        },
        error: function (data) {
            $('#metacredito_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

