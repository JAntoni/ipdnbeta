<?php
require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();

require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';

$action = $_POST['action'];
$metacredito_fechafin = fecha_mysql('01-'.$_POST['txt_metacredito_fechafin']);
$dateTime = new DateTime($metacredito_fechafin);
$metacredito_fechaini = fecha_mysql('01-'.$_POST['txt_metacredito_fechaini']);
$metacredito_fechafin = $dateTime->format('Y-m-t');

$oMetacredito->usuario_id = intval($_POST['cmb_usuario_id']); 
$oMetacredito->metacredito_creditos = moneda_mysql($_POST['txt_metacredito_creditos']); 
$oMetacredito->metacredito_cmenor = moneda_mysql($_POST['txt_metacredito_cmenor']); 
$oMetacredito->metacredito_ventacm = moneda_mysql($_POST['txt_metacredito_ventacm']); 
$oMetacredito->metacredito_ventaveh = moneda_mysql($_POST['txt_metacredito_ventaveh']); 
$oMetacredito->metacredito_fechaini = $metacredito_fechaini; 
$oMetacredito->metacredito_fechafin = $metacredito_fechafin; 
$oMetacredito->empresa_id = intval($_POST['cmb_empresa_id']);

if ($action == 'insertar') {

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al guardar el Meta de credito Credito Menor.';

  //? antes de insertar la nueva meta, validamos que el rango de fechas no exista en los ya existentes, para evitar redundancia
  //? como es un insert nuevo, el ID de la meta será = 0, para filtrar todos los ya registrados. TENER EN CUENTA QUE INTERNAMENTE HACE LA VALIDACION POR USUARIO Y EMPRESA QUE ESTAN EN oMeta->usuario..
  $result = $oMetacredito->verificar_meta_existente_fecha_inicio($metacredito_fechaini, $metacredito_fechafin, 0);
    if($result['estado'] == 0){
      //? estado 0 signica que no existe la fecha de inicio en los ya registrados, procedemos a registrar la nueva meta
      if ($oMetacredito->modificar_estado_conjunto_datos(0)) { // modificamos el estado de todos los registros anterioes a FINALIZADO
        $oMetacredito->insertar();
        $data['estado'] = 1;
        $data['mensaje'] = 'Meta registrada correctamente, revisar la lista.';
      }
    }
    else{
      //? retonamos el ID  de la meta encontrada para idicarlo que esa meta es la que debe editar y no registrar una nueva
      $metacredito_id_registrado = $result['data']['tb_metacredito_id'];
      $data['estado'] = 0;
      $data['mensaje'] = 'Ya existe una meta que incluye el MES de Inicio que estás ingresando. El ID  de meta es: '.$metacredito_id_registrado.', Fecha de Inicio: '.mostrar_fecha($result['data']['tb_metacredito_fechaini']).', Fecha Fin: '.mostrar_fecha($result['data']['tb_metacredito_fechafin']);
    }
  $result = NULL;

  echo json_encode($data);
} 
elseif ($action == 'modificar') {
  $metacredito_id = intval($_POST['hdd_metacredito_id']);
  $oMetacredito->metacredito_id = $metacredito_id;

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al modificar el Meta de credito Credito Menor.';

  //? al modificar también verificamos que la fecha ingresada no esté entre los ya existentes, enviamos el id de la meta para listar los diferentes a este ID. TENER EN CUENTA QUE INTERNAMENTE HACE LA VALIDACION POR USUARIO Y EMPRESA QUE ESTAN EN oMeta->usuario..
  $result = $oMetacredito->verificar_meta_existente_fecha_inicio($metacredito_fechaini, $metacredito_fechafin, $metacredito_id);
    if($result['estado'] == 0){
      //? estado 0 signica que no existe la fecha de inicio en los ya registrados, procedemos a editar los datos
      if ($oMetacredito->modificar()){
        $data['estado'] = 1;
        $data['mensaje'] = 'Meta de credito Credito Menor modificado correctamente. ' . $metacredito_des;
      }
    }
    else{
      //? retonamos el ID  de la meta encontrada para idicarlo que esa meta es la que debe editar y no registrar una nueva
      $metacredito_id_registrado = $result['data']['tb_metacredito_id'];
      $data['estado'] = 0;
      $data['mensaje'] = 'Ya existe una meta que incluye el MES de Inicio que estás ingresando. El ID  de meta es: '.$metacredito_id_registrado.', Fecha de Inicio: '.mostrar_fecha($result['data']['tb_metacredito_fechaini']).', Fecha Fin: '.mostrar_fecha($result['data']['tb_metacredito_fechafin']);
    }
  $result = NULL;

  echo json_encode($data);
} 
elseif ($action == 'eliminar') {
  $metacredito_id = intval($_POST['hdd_metacredito_id']);

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al eliminar el Meta de credito Credito Menor.';

  if ($oMetacredito->eliminar($metacredito_id)) {
    $data['estado'] = 1;
    $data['mensaje'] = 'Meta de Colaborador eliminado correctamente';
  }

  echo json_encode($data);
} 
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
?>