<?php

require_once('Metacredito.class.php');
$oMetacredito = new Metacredito();

$metacredito_id = (empty($metacredito_id)) ? 0 : $metacredito_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oMetacredito->listar_metacreditos();
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $selected = '';
        if ($metacredito_id == $value['tb_metacredito_id'])
            $selected = 'selected';

        $option .= '<option value="' . $value['tb_metacredito_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_metacredito_nom'] . '</option>';
    }
}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>