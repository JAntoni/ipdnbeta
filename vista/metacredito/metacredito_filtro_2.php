

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_controlsefuro_filtro" class="form-inline" role="form">
                    <!--<button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</button>-->
                    <a class="btn btn-primary btn-sm" href="javascript:void(0)" onClick="controlseguro_form('insertar', 0)" title="Agregar un Nuevo Registro al Sistema"><i class="fa fa-plus" aria-hidden="true" title="Agregar Comentario"></i></a>
                    <div class="form-group">
                        <input type="hidden" class="form-control input-sm" id="usuariogrupo_id" name="usuariogrupo_id" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">
                    </div>
                    <div class="form-group">
                        <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">
                
                            <?php require_once 'vista/empresa/empresa_select.php';?>
                        </select>
                    </div>
                   <div class="form-group">
                       <select name="cmb_usu_id" id="cmb_usu_id" class="form-control input-sm mayus"></select>
                    </div>
                   <div class="form-group">
                       <select name="cmb_pla_mes" id="cmb_pla_mes" class="form-control input-sm mayus">
                            <?php
                            $hoy_mes = date('m');
                            $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                            $num = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                            $sel = '';

                            for ($i = 0; $i < 12; $i++) {
                                if ($num[$i] == $hoy_mes){
                                    echo '<option value="' . $num[$i] . '" selected>' . $mes[$i] . '</option>';
                                }
                                else{
                                    echo '<option value="' . $num[$i] . '">' . $mes[$i] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <select name="cmb_pla_anio" id="cmb_pla_anio" class="form-control input-sm mayus">
                            <?php
                            $anio = intval(date('Y'));
                            for ($i = 2016; $i < ($anio + 5); $i++) {
                                if ($anio == $i){
                                    echo '<option value="' . $i . '" selected="true">' . $i . '</option>';
                                }
                                else{
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <a id="btn_generar" class="btn btn-primary btn-sm" onclick="planilla_colaborador()" title="Generar Planilla"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                    </div>
                    <?php if($_SESSION['usuariogrupo_id']==2){ ?>
                    <div class="form-group">
                        <a id="btn_generar" class="btn btn-danger btn-sm" onclick="metacredito_form()" title="Agregar Metas de Remate"><i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-info-circle" aria-hidden="true"></i></a>
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>