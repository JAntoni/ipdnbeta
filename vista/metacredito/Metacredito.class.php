<?php
if (defined('APP_URL')) {
  require_once(APP_URL . 'datos/conexion.php');
} else {
  require_once('../../datos/conexion.php');
}

class Metacredito extends Conexion
{

  public $metacredito_id;
  public $usuario_id;
  public $metacredito_creditos;
  public $metacredito_cmenor;
  public $metacredito_ventacm;
  public $metacredito_ventaveh;
  public $metacredito_fechaini;
  public $metacredito_fechafin;
  public $metacredito_estado = 1;
  public $empresa_id;

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {

      $columns = [
        'tb_usuario_id',
        'tb_metacredito_creditos',
        'tb_metacredito_cmenor',
        'tb_metacredito_ventacm',
        'tb_metacredito_ventaveh',
        'tb_metacredito_fechaini',
        'tb_metacredito_fechafin',
        'tb_metacredito_estado',
        'tb_empresa_id'
      ];

      // Lista de placeholders
      $placeholders = implode(',', array_map(function ($column) {
        return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_metacredito (" . implode(',', $columns) . ") VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_metacredito_creditos", $this->metacredito_creditos, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_cmenor", $this->metacredito_cmenor, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_ventacm", $this->metacredito_ventacm, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_ventaveh", $this->metacredito_ventaveh, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_fechaini", $this->metacredito_fechaini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_fechafin", $this->metacredito_fechafin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_estado", $this->metacredito_estado, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_empresa_id", $this->empresa_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_metacredito SET 
          tb_metacredito_creditos =:tb_metacredito_creditos,
          tb_metacredito_cmenor =:tb_metacredito_cmenor,
          tb_metacredito_ventacm =:tb_metacredito_ventacm,
          tb_metacredito_ventaveh =:tb_metacredito_ventaveh,
          tb_metacredito_fechaini =:tb_metacredito_fechaini,
          tb_metacredito_fechafin =:tb_metacredito_fechafin
        WHERE 
          tb_metacredito_id =:tb_metacredito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_metacredito_creditos", $this->metacredito_creditos, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_cmenor", $this->metacredito_cmenor, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_ventacm", $this->metacredito_ventacm, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_ventaveh", $this->metacredito_ventaveh, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_fechaini", $this->metacredito_fechaini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_fechafin", $this->metacredito_fechafin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_metacredito_id", $this->metacredito_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function eliminar($metacredito_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_metacredito SET tb_metacredito_xac = 0 WHERE tb_metacredito_id =:metacredito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":metacredito_id", $metacredito_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  //? cuando la nueva meta es válida, volvemos a todas las metas anteriores a un estado finalizado o no vigente
  function modificar_estado_conjunto_datos($metacredito_estado)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_metacredito SET 
          tb_metacredito_estado =:tb_metacredito_estado
        WHERE 
          tb_usuario_id =:tb_usuario_id AND tb_empresa_id =:tb_empresa_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_metacredito_estado", $metacredito_estado, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_empresa_id", $this->empresa_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  //? validamos si el rango de fechas para la meta no es redundante entre las ya guardadas, ya que no debe repetirse los meses
  //? cuando el registro es nuevo la meta lleva ID = 0; pero cuando es edición la meta si lleva in ID para filtras los extras
  function verificar_meta_existente_fecha_inicio($metacredito_fechaini, $metacredito_fechafin, $metacredito_id)
  {
    try {
      $exep_meta = '';
      if (intval($metacredito_id) > 0)
        $exep_meta = ' AND tb_metacredito_id != ' . intval($metacredito_id);

      $sql = "SELECT * FROM tb_metacredito
      WHERE (:metacredito_fechaini BETWEEN tb_metacredito_fechaini AND tb_metacredito_fechafin OR :metacredito_fechafin BETWEEN tb_metacredito_fechaini AND tb_metacredito_fechafin)
      AND tb_usuario_id =:usuario_id AND tb_metacredito_xac = 1" . $exep_meta;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":metacredito_fechaini", $metacredito_fechaini, PDO::PARAM_STR);
      $sentencia->bindParam(":metacredito_fechafin", $metacredito_fechafin, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontro meta de Crédito registrada";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarUno($metacredito_id)
  {
    try {
      $sql = "SELECT met.*, tb_usuario_nom, tb_empresa_nomcom, 
          DATE_FORMAT(tb_metacredito_fechaini, '%m-%Y') AS 'mes_inicio',
          DATE_FORMAT(tb_metacredito_fechafin, '%m-%Y') AS 'mes_fin'
        FROM tb_metacredito met
        INNER JOIN tb_usuario usu ON usu.tb_usuario_id = met.tb_usuario_id 
        INNER JOIN tb_empresa emp ON emp.tb_empresa_id = met.tb_empresa_id 
        WHERE tb_metacredito_id =:metacredito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":metacredito_id", $metacredito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontro meta de Crédito registrada";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_metacreditos($fecha1, $fecha2)
  {
    try {
      $sql = "SELECT * FROM tb_metacredito met
        INNER JOIN tb_usuario usu ON usu.tb_usuario_id = met.tb_usuario_id 
        WHERE tb_metacredito_xac = 1 AND (tb_metacredito_fechafin BETWEEN :fecha1 AND :fecha2 OR :fecha2 BETWEEN tb_metacredito_fechaini AND tb_metacredito_fechafin) ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontraron metas de créditos registradas ";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function buscarmeta_usuario($usuario_id, $mes, $anio){
    $fecha_busqueda = $anio.'-'.$mes.'-01'; //con el formato de: 2023-08

    try {
      $sql = "SELECT * FROM tb_metacredito 
            WHERE tb_usuario_id =:usuario_id AND :fecha BETWEEN tb_metacredito_fechaini AND tb_metacredito_fechafin
            AND tb_metacredito_xac = 1 ORDER BY tb_metacredito_id DESC LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha", $fecha_busqueda, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontro meta de Crédito registrada";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function sumar_metacredito_usuario_sede($mes, $anio, $usuario_id, $empresa_id){
    try {
      $fecha_busqueda = $anio.'-'.$mes.'-01'; //con el formato de: 2023-08
      $filtro_usuario = '';
      $filtro_empresa = '';
      if(intval($usuario_id) > 0)
        $filtro_usuario = 'AND tb_usuario_id = '.intval($usuario_id);
      if(intval($empresa_id) > 0)
        $filtro_empresa = ' AND tb_empresa_id = '.intval($empresa_id);
      
      $sql = "SELECT
          SUM(tb_metacredito_creditos) AS metacredito_creditos,
          SUM(tb_metacredito_cmenor) AS metacredito_cmenor,
          SUM(tb_metacredito_ventacm) AS metacredito_ventacm,
          SUM(tb_metacredito_ventaveh) AS metacredito_ventaveh
          FROM tb_metacredito 
            WHERE :fecha BETWEEN tb_metacredito_fechaini AND tb_metacredito_fechafin
            AND tb_metacredito_xac = 1 ".$filtro_usuario.$filtro_empresa;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha", $fecha_busqueda, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontro meta de Crédito registrada";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
