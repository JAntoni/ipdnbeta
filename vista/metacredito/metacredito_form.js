function cmb_usuario_id(empresa_id){ 
  var mostrar = 0;
  $.ajax({
    type: "POST",
    url: VISTA_URL+"usuario/cmb_usu_id_select.php",
    async:false,
    dataType: "html",                      
    data: ({
      mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
      empresa_id:empresa_id
    }),
    beforeSend: function() {
      $('#cmb_usuario_id').html('<option value="">Cargando...</option>');
    },
    success: function(html){
      $('#cmb_usuario_id').html(html);
    },
    complete: function (html) {

    }
  });
}

$(document).ready(function () {
  console.log("cambios 33");

  $("#cmb_empresa_id").change(function () {
    var empresa_id = $("#cmb_empresa_id").val();
    cmb_usuario_id(empresa_id);
  });

  $("#datepicker, #datepicker2").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months",
    language: 'es',
    autoclose: true
  });

  $("#txt_metacredito_fechaini").change(function () {
    var input1 = $(this).val();
    var input2 = $("#txt_metacredito_fechafin").val();
    console.log("aqui va el mes11: " + input1);
    console.log("aqui va el mes12: " + input2);

    if (input2 && input1 > input2) {
      alerta_warning(
        "AISO",
        "La fecha final no puede ser menor que la fecha inicial."
      );
      $("#txt_metacredito_fechaini").val("");
    }
  });

  $("#txt_metacredito_fechafin").change(function () {
    var input1 = $("#txt_metacredito_fechaini").val();
    var input2 = $(this).val();
    console.log("aqui va el mes11: " + input1);
    console.log("aqui va el mes12: " + input2);

    if (input1 && input1 > input2) {
      alerta_warning(
        "AISO",
        "La fecha final no puede ser menor que la fecha inicial."
      );
      $("#txt_metacredito_fechafin").val("");
    }
  });

  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "999999999.99",
  });

  $("#form_metacredito").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "metacredito/metacredito_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_metacredito").serialize(),
        beforeSend: function () {
          $("#metacredito_mensaje").show(400);
          $("#btn_guardar_metacredito").prop("disabled", true);
        },
        success: function (data) {
          if(parseInt(data.estado) == 1){
            notificacion_success(data.mensaje);
            metacredito_tabla();
            $('#modal_registro_metacredito').modal('hide');
          }
          else{
            alerta_warning('AVISO', data.mensaje);
            $("#btn_guardar_metacredito").prop("disabled", false);
          }
        },
        complete: function (data) {
          console.log(data);
        },
        error: function (data) {
          alerta_error('ERROR', data.responseText)
        },
      });
    },
    rules: {
      cmb_empresa_id: {
        required: true,
        min: 1,
      },
      cmb_usuario_id: {
        required: true,
        min: 1,
      },
      txt_metacredito_fechaini: {
        required: true
      },
      txt_metacredito_fechafin: {
        required: true
      },
      txt_metacredito_creditos: {
        required: true
      },
      txt_metacredito_cmenor: {
        required: true
      },
      txt_metacredito_ventacm: {
        required: true
      },
      txt_metacredito_ventaveh: {
        required: true
      }
    },
    messages: {
      cmb_empresa_id: {
        required: "Selecciona una sede para la Meta",
        min: "Selecciona una sede para la Meta",
      },
      cmb_usuario_id: {
        required: "Selecciona un colaborador para la Meta",
        min: "Selecciona un colaborador para la Meta",
      },
      txt_metacredito_fechaini: {
        required: "Selecciona un mes de Inicio para la Meta"
      },
      txt_metacredito_fechafin: {
        required: "Selecciona un mes Fin para la Meta"
      },
      txt_metacredito_creditos: {
        required: "Igresa la meta para Créditos Altos"
      },
      txt_metacredito_cmenor: {
        required: "Ingresa la meta para C-Menor"
      },
      txt_metacredito_ventacm: {
        required: "Ingresa la meta para Remate Productos"
      },
      txt_metacredito_ventaveh: {
        required: "Ingresa la meta de Cantidad Vehículos"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });
});
