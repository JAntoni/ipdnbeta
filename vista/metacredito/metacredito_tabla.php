<?php
require_once '../../core/usuario_sesion.php';
require_once('Metacredito.class.php');
$oMetacredito = new Metacredito();

require_once ('../funciones/funciones.php');
require_once ('../funciones/fechas.php');
require_once ('../usuario/Usuario.class.php');
$oUsuario=new Usuario();

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];
$tr = '';

$suma_creditos = 0;
$suma_cmenor = 0;
$suma_ventacm = 0;
$suma_ventaveh = 0;

$result = $oMetacredito->listar_metacreditos(fecha_mysql($fecha1), fecha_mysql($fecha2));
    if ($result['estado'] == 1) {
      foreach ($result['data'] as $key => $value) {
          
        $vigente = '<span class="badge bg-red">FINALIZADO</span>';
        if($value['tb_metacredito_estado'] == 1){
            $vigente = '<span class="badge bg-aqua">ACTIVO</span>';
        }
        
        $suma_creditos += $value['tb_metacredito_creditos'];
        $suma_cmenor += $value['tb_metacredito_cmenor'];
        $suma_ventacm += $value['tb_metacredito_ventacm'];
        $suma_ventaveh += $value['tb_metacredito_ventaveh'];

        $tr .= '<tr>';
        $tr .= '
        <td id="tabla_fila" align="center">' . $value['tb_metacredito_id'] . '</td>
        <td id="tabla_fila" align="center">' . $value['tb_usuario_nom'] . '</td>
        <td id="tabla_fila" align="center">' . mostrar_fecha($value['tb_metacredito_fechaini']) . '</td>
        <td id="tabla_fila" align="center">' . mostrar_fecha($value['tb_metacredito_fechafin']) . '</td>
        <td id="tabla_fila" align="center">' . mostrar_moneda($value['tb_metacredito_creditos']) . '</td>
        <td id="tabla_fila" align="center">' . mostrar_moneda($value['tb_metacredito_cmenor']) . '</td>
        <td id="tabla_fila" align="center">' . mostrar_moneda($value['tb_metacredito_ventacm']) . '</td>
        <td id="tabla_fila" align="center">' . mostrar_moneda($value['tb_metacredito_ventaveh']) . '</td>
        <td id="tabla_fila" align="center">' . $vigente . '</td>
        <td id="tabla_fila" align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="metacredito_form(\'L\',' . $value['tb_metacredito_id'] . ')"><i class="fa fa-eye"></i></a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="metacredito_form(\'M\',' . $value['tb_metacredito_id'] . ')"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="metacredito_form(\'E\',' . $value['tb_metacredito_id'] . ')"><i class="fa fa-trash"></i></a>
        </td>
        ';
        $tr .= '</tr>';
      }
    }
    else {
      echo $result['mensaje'];
    }
$result = null;

$tr .= '
  <tr>
    <td id="tabla_fila" align="center" colspan="4"><b>SUMA TOTAL DE COMISIONES</b></td>
    <td id="tabla_fila" align="center"><b>' . mostrar_moneda($suma_creditos) . '</b></td>
    <td id="tabla_fila" align="center"><b>' . mostrar_moneda($suma_cmenor) . '</b></td>
    <td id="tabla_fila" align="center"><b>' . mostrar_moneda($suma_ventacm) . '</b></td>
    <td id="tabla_fila" align="center"><b>' . mostrar_moneda($suma_ventaveh) . '</b></td>
    <td id="tabla_fila" align="center" colspan="2">
    </td>
  </tr>
';
?>
<table id="tbl_metacreditos" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">USUARIO</th>
            <th id="tabla_cabecera_fila">FECHA INICIO</th>
            <th id="tabla_cabecera_fila">FECHA FIN</th>
            <th id="tabla_cabecera_fila" class="no-sort">META CREDITOS</th>
            <th id="tabla_cabecera_fila" class="no-sort">META C-MENOR</th>
            <th id="tabla_cabecera_fila" class="no-sort">META REMATES</th>
            <th id="tabla_cabecera_fila" class="no-sort">META VENTA VEHICULOS</th>
            <th id="tabla_cabecera_fila" class="no-sort">ESTADO</th>
            <th id="tabla_cabecera_fila" class="no-sort">OPCIONES</th>
        </tr>
    </thead>
    <tbody>
<?php echo $tr; ?>
    </tbody>
</table>
