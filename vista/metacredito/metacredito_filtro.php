

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_controlsefuro_filtro" class="form-inline" role="form">
                    <!--<button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</button>-->
                    <button type="button" class="btn btn-primary btn-sm" onclick="metacredito_form('I', 0)"><i class="fa fa-plus"></i> Nueva Metacredito</button>
                    <div class="form-group">
                        <!--<label for="">Fecha </label>-->
                        <div class="input-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo date('01-m-Y'); ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="input-group-addon">-</span>
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo date('t-m-Y'); ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">

                     </div>
                    <div class="form-group">

                     </div>
                    
                    <div class="form-group">
                        <a href="javascript:void(0)" onClick="metacredito_tabla()" class="btn btn-primary btn-sm" title="Reporte de STR y GPS que estan por vencer y requieren renovación"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>