<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'metacredito';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$metacredito_id = $_POST['metacredito_id'];

$titulo = '';
if ($usuario_action == 'L') {
  $titulo = 'Metacredito Registrado';
} elseif ($usuario_action == 'I') {
  $titulo = 'Registrar Metacredito';
} elseif ($usuario_action == 'M') {
  $titulo = 'Editar Metacredito';
} elseif ($usuario_action == 'E') {
  $titulo = 'Eliminar Metacredito';
} else {
  $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en metacredito
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';
$metacredito_fecha = date('t-m-Y');

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id1 = $_SESSION['usuario_id'];
    $modulo = 'metacredito';
    $modulo_id = $metacredito_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id1, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }

  if (intval($metacredito_id) > 0) {
    $result = $oMetacredito->mostrarUno($metacredito_id);
    if ($result['estado'] == 1) {
      $usuario_id = $result['data']['tb_usuario_id']; 
      $metacredito_creditos = mostrar_moneda($result['data']['tb_metacredito_creditos']); 
      $metacredito_cmenor = mostrar_moneda($result['data']['tb_metacredito_cmenor']);  
      $metacredito_ventacm = mostrar_moneda($result['data']['tb_metacredito_ventacm']); 
      $metacredito_ventaveh = mostrar_moneda($result['data']['tb_metacredito_ventaveh']); 
      $metacredito_fechaini = $result['data']['mes_inicio']; 
      $metacredito_fechafin = $result['data']['mes_fin'];
      $metacredito_estado = $result['data']['tb_metacredito_estado'];
      $empresa_id = $result['data']['tb_empresa_id'];
      $usuario_nom = $result['data']['tb_usuario_nom'];
      $empresa_nom = $result['data']['tb_empresa_nomcom'];
    }
    $result = NULL;
  }
} else {
  $mensaje = $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_metacredito" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo; ?></h4>
        </div>
        <form id="form_metacredito" method="post">
          <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_metacredito_id" value="<?php echo $metacredito_id; ?>">

          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <label>Sede:</label>
                <?php if ($action == "insertar"):?>
                  <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">
                    <?php require_once '../empresa/empresa_select.php'; ?>
                  </select>
                <?php endif; ?>

                <?php if ($action == "modificar"): ?>
                  <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">
                    <option value="<?php echo $empresa_id ?>"><?php echo $empresa_nom; ?></option>
                  </select>
                <?php endif; ?>
              </div>
              <div class="col-md-6">
                <label>Colaborador:</label>
                <?php if ($action == "insertar"):?>
                  <select name="cmb_usuario_id" id="cmb_usuario_id" class="form-control input-sm mayus"></select>
                <?php endif; ?>

                <?php if ($action == "modificar"): ?>
                  <select name="cmb_usuario_id" id="cmb_usuario_id" class="form-control input-sm mayus">
                    <option value="<?php echo $usuario_id ?>"><?php echo $usuario_nom; ?></option>
                  </select>
                <?php endif; ?>
              </div>
            </div>

            <p>
            <div class="row">
              <div class="col-md-6">
                <label>Mes de Inicio:</label>
                <div class="input-group date" id="datepicker">
                  <input type='text' class="form-control input-sm" name="txt_metacredito_fechaini" id="txt_metacredito_fechaini" readonly value="<?php echo $metacredito_fechaini?>">    
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>      
                </div>
              </div>

              <div class="col-md-6">
                <label>Mes Finalizado:</label>
                <div class="input-group date" id="datepicker2">
                  <input type='text' class="form-control input-sm" name="txt_metacredito_fechafin" id="txt_metacredito_fechafin" readonly value="<?php echo $metacredito_fechafin?>">    
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>      
                </div>
              </div>
            </div>
            
            <p></p>
            <table class="table">
              <tr>
                <th class="success">TIPO DE META</th>
                <th class="success">MONTO O CANTIDAD A LOGRAR</th>
              </tr>
              <tr>
                <td><b>Suma Colación: GARVEH + HIPO + ASIVEH</b></td>
                <td>
                  <input type='text' class="form-control input-sm moneda" name="txt_metacredito_creditos" id="txt_metacredito_creditos" value="<?php echo $metacredito_creditos; ?>">
                </td>
              </tr>
              <tr>
                <td><b>Colocación de Crédito Menor</b></td>
                <td>
                  <input type='text' class="form-control input-sm moneda" name="txt_metacredito_cmenor" id="txt_metacredito_cmenor" value="<?php echo $metacredito_cmenor; ?>">
                </td>
              </tr>
              <tr>
                <td><b>Monto Total Productos Vendidos</b></td>
                <td>
                  <input type='text' class="form-control input-sm moneda" name="txt_metacredito_ventacm" id="txt_metacredito_ventacm" value="<?php echo $metacredito_ventacm; ?>">
                </td>
              </tr>
              <tr>
                <td><b>Cantidad Vehiculos Vendidos</b></td>
                <td>
                  <input type='text' class="form-control input-sm moneda" name="txt_metacredito_ventaveh" id="txt_metacredito_ventaveh" value="<?php echo $metacredito_ventaveh; ?>">
                </td>
              </tr>
            </table>
          </div>

          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_metacredito">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_metacredito">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_metacredito">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/metacredito/metacredito_form.js?ver=86326'; ?>"></script>