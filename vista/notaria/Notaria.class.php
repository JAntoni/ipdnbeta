<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');

    ini_set('display_errors', '1');
  class Notaria extends Conexion{
      private $notaria_id;
      private $notaria_nombre;
      private $notaria_razonsocial;
      private $notaria_ruc;
      private $notaria_domiciliofiscal;
      private $notaria_jurisdiccion;
      private $notaria_telefono;
      private $notaria_telefono2;
      private $notaria_email;
      private $notaria_usureg;
      private $notaria_usumod;
      private $notaria_fecmod;
      
    public function getNotaria_id() {
        return $this->notaria_id;
    }
    
    public function setNotaria_id($notaria_id){
        $this->notaria_id = $notaria_id;
    }
      
    public function getNotaria_nombre() {
        return $this->notaria_nombre;
    }
    
    public function setNotaria_nombre($notaria_nombre){
        $this->notaria_nombre = $notaria_nombre;
    }
      
    public function getNotaria_razonsocial() {
        return $this->notaria_razonsocial;
    }
    
    public function setNotaria_razonsocial($notaria_razonsocial){
        $this->notaria_razonsocial = $notaria_razonsocial;
    }
      
    public function getNotaria_ruc() {
        return $this->notaria_ruc;
    }
    
    public function setNotaria_ruc($notaria_ruc){
        $this->notaria_ruc = $notaria_ruc;
    }
      
    public function getNotaria_domiciliofiscal() {
        return $this->notaria_domiciliofiscal;
    }
    
    public function setNotaria_domiciliofiscal($notaria_domiciliofiscal){
        $this->notaria_domiciliofiscal = $notaria_domiciliofiscal;
    }
      
    public function getNotaria_jurisdiccion() {
        return $this->notaria_jurisdiccion;
    }
    
    public function setNotaria_jurisdiccion($notaria_jurisdiccion){
        $this->notaria_jurisdiccion = $notaria_jurisdiccion;
    }
      
    public function getNotaria_telefono() {
        return $this->notaria_telefono;
    }
    
    public function setNotaria_telefono($notaria_telefono){
        $this->notaria_telefono = $notaria_telefono;
    }
      
    public function getNotaria_telefono2() {
        return $this->notaria_telefono2;
    }
    
    public function setNotaria_telefono2($notaria_telefono2){
        $this->notaria_telefono2 = $notaria_telefono2;
    }
      
    public function getNotaria_email() {
        return $this->notaria_email;
    }
    
    public function setNotaria_email($notaria_email){
        $this->notaria_email = $notaria_email;
    }
      
    public function getNotaria_usumod() {
        return $this->notaria_usumod;
    }
    
    public function setNotaria_usumod($notaria_usumod){
        $this->notaria_usumod = $notaria_usumod;
    }
      
      
    public function getNotaria_usureg() {
        return $this->notaria_usureg;
    }
    
    public function setNotaria_usureg($notaria_usureg){
        $this->notaria_usureg = $notaria_usureg;
    }
      
    public function getNotaria_fecmod() {
        return $this->notaria_fecmod;
    }
    
    public function setNotaria_fecmod($notaria_fecmod){
        $this->notaria_fecmod = $notaria_fecmod;
    }

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_notaria(tb_notaria_nombre, tb_notaria_razonsocial, tb_notaria_ruc, tb_notaria_domiciliofiscal, tb_notaria_jurisdiccion, tb_notaria_telefono, tb_notaria_telefono2, tb_notaria_email,tb_notaria_usureg)
          VALUES (:notaria_nombre, :notaria_razonsocial, :notaria_ruc, :notaria_domiciliofiscal,
                  :notaria_jurisdiccion, :notaria_telefono, :notaria_telefono2,
                  :notaria_email, :notaria_usureg)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":notaria_nombre", $this->getNotaria_nombre(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_razonsocial", $this->getNotaria_razonsocial(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_ruc", $this->getNotaria_ruc(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_domiciliofiscal", $this->getNotaria_domiciliofiscal(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_jurisdiccion", $this->getNotaria_jurisdiccion(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_telefono", $this->getNotaria_telefono(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_telefono2", $this->getNotaria_telefono2(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_email", $this->getNotaria_email(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_usureg", $this->getNotaria_usureg(), PDO::PARAM_INT);

        $result = $sentencia->execute();
        $notaria_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result;
        $data['notaria_id'] = $notaria_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_notaria SET 
                tb_notaria_nombre =:notaria_nombre, 
                tb_notaria_razonsocial =:notaria_razonsocial, 
                tb_notaria_ruc =:notaria_ruc,
                tb_notaria_domiciliofiscal =:notaria_domiciliofiscal, 
                tb_notaria_jurisdiccion =:notaria_jurisdiccion, 
                tb_notaria_telefono =:notaria_telefono, 
                tb_notaria_telefono2 =:notaria_telefono2, 
                tb_notaria_email =:notaria_email,
                tb_notaria_usumod =:notaria_usumod,
                tb_notaria_fecmod =:notaria_fecmod
                WHERE tb_notaria_id =:notaria_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":notaria_id", $this->getNotaria_id(), PDO::PARAM_INT);
        $sentencia->bindParam(":notaria_nombre", $this->getNotaria_nombre(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_razonsocial", $this->getNotaria_razonsocial(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_ruc", $this->getNotaria_ruc(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_domiciliofiscal", $this->getNotaria_domiciliofiscal(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_jurisdiccion", $this->getNotaria_jurisdiccion(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_telefono", $this->getNotaria_telefono(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_telefono2", $this->getNotaria_telefono2(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_email", $this->getNotaria_email(), PDO::PARAM_STR);
        $sentencia->bindParam(":notaria_usumod", $this->getNotaria_usumod(), PDO::PARAM_INT);
        $sentencia->bindParam(":notaria_fecmod", $this->getNotaria_fecmod(), PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($notaria_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_notaria SET tb_notaria_xac=0 WHERE tb_notaria_id =:notaria_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":notaria_id", $notaria_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($notaria_id){
      try {
        $sql = "SELECT * FROM tb_notaria WHERE tb_notaria_id =:notaria_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":notaria_id", $notaria_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_notarias(){
      try {
        $sql = "SELECT * FROM tb_notaria WHERE tb_notaria_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
