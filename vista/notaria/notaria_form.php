<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../notaria/Notaria.class.php');
  $oNotaria = new Notaria();
  require_once('../funciones/funciones.php');

  $direc = 'notaria';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $notaria_id = $_POST['notaria_id'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Notaria Registrado';
}
if ($usuario_action == 'I') {
    $titulo = 'Registrar Notaria';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Notaria';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Notaria';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en notaria
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'notaria'; $modulo_id = $notaria_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del notaria por su ID
    if(intval($notaria_id) > 0){
      $result = $oNotaria->mostrarUno($notaria_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el notaria seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $notaria_nombre = $result['data']['tb_notaria_nombre'];
          $notaria_razonsocial = $result['data']['tb_notaria_razonsocial'];
          $notaria_ruc = $result['data']['tb_notaria_ruc'];
          $notaria_domiciliofiscal = $result['data']['tb_notaria_domiciliofiscal'];
          $notaria_jurisdiccion = $result['data']['tb_notaria_jurisdiccion'];
          $notaria_telefono = $result['data']['tb_notaria_telefono'];
          $notaria_telefono2 = $result['data']['tb_notaria_telefono2'];
          $notaria_email = $result['data']['tb_notaria_email'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_notaria" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_notaria" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_notaria_id" value="<?php echo $notaria_id;?>">
          
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_nombre" class="control-label">Nombre notaria</label>
                                      <input type="text" name="txt_notaria_nombre" id="txt_notaria_nombre" class="form-control input-sm mayus" value="<?php echo $notaria_nombre;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_razonsocial" class="control-label">Razon social</label>
                                      <input type="text" name="txt_notaria_razonsocial" id="txt_notaria_razonsocial" class="form-control input-sm" value="<?php echo $notaria_razonsocial;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_ruc" class="control-label">Ruc notaria</label>
                                      <input type="text" name="txt_notaria_ruc" id="txt_notaria_ruc" class="form-control input-sm mayus" value="<?php echo $notaria_ruc;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_domicilio" class="control-label">Domicilio fiscal</label>
                                      <input type="text" name="txt_notaria_domicilio" id="txt_notaria_domicilio" class="form-control input-sm" value="<?php echo $notaria_domiciliofiscal;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_jurisdiccion" class="control-label">Jurisdiccion notaria</label>
                                      <input type="text" name="txt_notaria_jurisdiccion" id="txt_notaria_jurisdiccion" class="form-control input-sm mayus" value="<?php echo $notaria_jurisdiccion;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_email" class="control-label">Correo notaria</label>
                                      <input type="text" name="txt_notaria_email" id="txt_notaria_email" class="form-control input-sm" value="<?php echo $notaria_email;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_telefono" class="control-label">Telefono notaria</label>
                                      <input type="text" name="txt_notaria_telefono" id="txt_notaria_telefono" class="form-control input-sm mayus" value="<?php echo $notaria_telefono;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_notaria_telefono2" class="control-label">Telefono 2 notaria</label>
                                      <input type="text" name="txt_notaria_telefono2" id="txt_notaria_telefono2" class="form-control input-sm" value="<?php echo $notaria_telefono2;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if($action == 'eliminar'):?>
                      <div class="callout callout-warning">
                        <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Notaria?</h4>
                      </div>
                    <?php endif;?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="notaria_mensaje" style="display: none;">
                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_notaria">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_notaria">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_notaria">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/notaria/notaria_form.js';?>"></script>
