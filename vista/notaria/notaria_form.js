
$(document).ready(function(){
  $('#form_notaria').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"notaria/notaria_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_notaria").serialize(),
				beforeSend: function() {
					$('#notaria_mensaje').show(400);
					$('#btn_guardar_notaria').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#notaria_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#notaria_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		notaria_tabla();
		      		$('#modal_registro_notaria').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#notaria_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#notaria_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_notaria').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#notaria_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#notaria_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_notaria_nombre: {
				required: true,
				minlength: 2
			},
			txt_notaria_razonsocial: {
				required: true,
				minlength: 5
			},
			txt_notaria_ruc: {
				required: true,
				minlength: 11
			},
			txt_notaria_domicilio: {
				required: true
			},
			txt_notaria_jurisdiccion: {
				required: true
			},
			txt_notaria_email: {
				required: true
			}
		},
		messages: {
			txt_notaria_nom: {
				required: "Ingrese un nombre para la Notaria",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_notaria_razonsocial: {
				required: "Ingrese una razon social para la Notaria",
				minlength: "La razon social debe tener como mínimo 5 caracteres"
			},
			txt_notaria_ruc: {
				required: "Ingrese un RUC para la Notaria",
				minlength: "El RUC debe tener 11 caracteres"
			},
			txt_notaria_domicilio: {
				required: "Ingrese un domicilio fiscal para la Notaria"
			},
			txt_notaria_jurisdiccion: {
				required: "Ingrese una jurisdiccion para la Notaria"
			},
			txt_notaria_email: {
				required: "Ingrese un correo para la Notaria"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
