<?php
require_once('Notaria.class.php');
$oNotaria = new Notaria();

$notaria_id = (empty($notaria_id))? 0 : $notaria_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0">NOTARIAS</option>';

//PRIMER NIVEL
$result = $oNotaria->listar_notarias();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($notaria_id == $value['tb_notaria_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_notaria_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_notaria_nombre'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>