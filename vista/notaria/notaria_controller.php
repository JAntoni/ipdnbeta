<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../notaria/Notaria.class.php');
  $oNotaria = new Notaria();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

 	$action = $_POST['action'];
        $notaria_nombre = mb_strtoupper($_POST['txt_notaria_nombre'], 'UTF-8');
        $notaria_razonsocial = mb_strtoupper($_POST['txt_notaria_razonsocial'], 'UTF-8');
        $notaria_ruc = $_POST['txt_notaria_ruc'];
        $notaria_domicilio = $_POST['txt_notaria_domicilio'];
        $notaria_jurisdiccion = $_POST['txt_notaria_jurisdiccion'];
        $notaria_email = $_POST['txt_notaria_email'];
        $notaria_telefono = $_POST['txt_notaria_telefono'];
        $notaria_telefono2 = $_POST['txt_notaria_telefono2'];
        $notaria_usureg = $_SESSION['usuario_id'];
        
        $oNotaria->setNotaria_nombre($notaria_nombre);
        $oNotaria->setNotaria_razonsocial($notaria_razonsocial);
        $oNotaria->setNotaria_ruc($notaria_ruc);
        $oNotaria->setNotaria_domiciliofiscal($notaria_domicilio);
        $oNotaria->setNotaria_jurisdiccion($notaria_jurisdiccion);
        $oNotaria->setNotaria_email($notaria_email);
        $oNotaria->setNotaria_telefono($notaria_telefono);
        $oNotaria->setNotaria_telefono2($notaria_telefono2);
        $oNotaria->setNotaria_usureg($notaria_usureg);
  
        
 	if($action == 'insertar'){
//        $data['estado'] = 0;
// 	$data['mensaje'] = '$notaria_nombre='.$notaria_nombre.' $notaria_razonsocial='.$notaria_razonsocial.' $notaria_ruc='.$notaria_ruc.' $notaria_domicilio='.$notaria_domicilio.' $notaria_jurisdiccion='.$notaria_jurisdiccion.' $notaria_email='.$notaria_email.' $notaria_telefono='.$notaria_telefono.' $notaria_telefono2='.$notaria_telefono2.' $notaria_usureg='.$notaria_usureg;
//        echo json_encode($data); exit();
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Notaria.';
 		if($oNotaria->insertar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Notaria registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$notaria_id = intval($_POST['hdd_notaria_id']);
 		$oNotaria->setNotaria_id($notaria_id);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Notaria.';

 		if($oNotaria->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Notaria modificado correctamente. '.$notaria_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$notaria_id = intval($_POST['hdd_notaria_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Notaria.';

 		if($oNotaria->eliminar($notaria_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Notaria eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>