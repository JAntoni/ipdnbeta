<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Notaria.class.php');
  $oNotaria = new Notaria();

  $result = $oNotaria->listar_notarias();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td  id="fila" style="border-left: 1px solid #135896;">'.$value['tb_notaria_id'].'</td>
          <td  id="fila">'.$value['tb_notaria_nombre'].'</td>
          <td  id="fila">'.$value['tb_notaria_razonsocial'].'</td>
          <td  id="fila">'.$value['tb_notaria_ruc'].'</td>
          <td  id="fila">'.$value['tb_notaria_domiciliofiscal'].'</td>
          <td  id="fila">'.$value['tb_notaria_jurisdiccion'].'</td>
          <td  id="fila">'.$value['tb_notaria_telefono'].'</td>
          <td  id="fila">'.$value['tb_notaria_telefono2'].'</td>
          <td  id="fila">'.$value['tb_notaria_email'].'</td>
          <td align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="notaria_form(\'L\','.$value['tb_notaria_id'].')"><i class="fa fa-eye"></i> Ver</a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="notaria_form(\'M\','.$value['tb_notaria_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="notaria_form(\'E\','.$value['tb_notaria_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_notarias" class="table table-bordered table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th id="filacabecera">ID</th>
      <th id="filacabecera">Nombre</th>
      <th id="filacabecera">RazonSocial</th>
      <th id="filacabecera">Ruc</th>
      <th id="filacabecera">Domicilio</th>
      <th id="filacabecera">Jurisdiccion</th>
      <th id="filacabecera">Telefono 1</th>
      <th id="filacabecera">Telefono 2</th>
      <th id="filacabecera">Email</th>
      <th id="filacabecera">Opciones</ th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
