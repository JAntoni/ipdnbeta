var datatable_global_ejecucion;

$(document).ready(function () {
    $('#click').click();
    //js para el fil
    {
        $("#txt_fil_cliente_nom")
        .autocomplete({
            minLength: 1,
            source: function (request, response) {
                $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    { term: request.term },
                    response
                );
            },
            select: function (event, ui) {
                event.preventDefault();
                $("#hdd_fil_cliente_id").val(ui.item.cliente_id);
                parseInt(ui.item.cliente_tip) == 2 ? $('#txt_fil_cliente_nom').val(ui.item.label) : $('#txt_fil_cliente_nom').val(ui.item.value);
                ejecucion_tabla();
            }
        })
        .keyup(function () {
            if ($('#txt_fil_cliente_nom').val() === '') {
                $('#hdd_fil_cliente_id').val('');
                ejecucion_tabla();
            }
        });

        $('#datetimepicker1, #datetimepicker2').datepicker({
            language: 'es',
            autoclose: true,
            format: "dd-mm-yyyy",
            endDate: new Date()
        });

        $("#datetimepicker1").on("change", function (e) {
            var startVal = $('#txt_fil_fec1').val();
            $('#datetimepicker2').data('datepicker').setStartDate(startVal);
        });
        $("#datetimepicker2").on("change", function (e) {
            var endVal = $('#txt_fil_fec2').val();
            $('#datetimepicker1').data('datepicker').setEndDate(endVal);
        });

        $('#cbo_fil_estadoejecucion_cgvsubtipo_id, #txt_fil_encargado_id, #cbo_fil_estadoejecucion').change(function () {
            ejecucion_tabla();
        });

        //daniel odar 17-10-2024, require_once de usuarios, ocultar los que estén con estado(bloqeo) = 1
            $('#txt_fil_encargado_id option[id*="option_cero"], #txt_fil_encargado_id option[data-estado="1"]').remove();
        //
        disabled($('#form_ejecucion_filtro').find('.disabled'));

        $('#cbo_fil_cgvsubtipo_id').on('change', function() {
            if (parseInt($(this).val()) != 0) {
                llenar_cbo_estados($(this).val());
            } else {
                disabled($('#cbo_fil_estadoejecucion_cgvsubtipo_id').html('<option value="0">Todas</option>'));
                $('#div_fil_estadoejecucion_cgvsubtipo_id').css('width', '120px');
            }
            setTimeout(function () {
                ejecucion_tabla();
            }, 200);
        });
    }
    //

    ejecucion_tabla();
});

function ejecucion_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucion/ejecucion_tabla.php",
        async: true,
        dataType: "html",
        data: $('#form_ejecucion_filtro').serialize(),
        beforeSend: function () {
            $('#ejecucion_mensaje_tbl').show(300);
            $('#mensaje_recarga').hide(300);
        },
        success: function (data) {
            $('#div_ejecucion_tabla').html(data);
            $('#ejecucion_mensaje_tbl').hide(300);
            estilos_datatable_ejecucion();
            buscar_alertas('ejecucion', '');
            $('input[type="checkbox"][id*="che_ejec_"]').iCheck({
                checkboxClass: `icheckbox_flat-green`,
                radioClass: `iradio_flat-green`
            });
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            $('#ejecucion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

function estilos_datatable_ejecucion() {
    datatable_global_ejecucion = $('#tbl_ejecucions').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [4, 5, 6, 8, 9], orderable: false}
        ]
    });
    datatable_texto_filtrar_ejecucion();
}

function datatable_texto_filtrar_ejecucion() {
    $('input[aria-controls*="tbl_ejecucions"]')
    .attr('id', 'txt_datatable_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_ejecucion_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_ejecucion_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global_ejecucion.search(text_fil).draw();
    }
};

function btnVolverTabla() {
    $('section#ejecucion_fase').hide();
    $('section#ejecucion_fase, #header_caso').remove();
    $('section#ejecucion_tabla').show();
    $('#header_contenedor').removeAttr('style');
}

function ver_proceso(action, ejecucion_id) {
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'ver_proceso',
            action2: action,
            ejecucion_id: ejecucion_id
        }),
        success: function (data) {
            createHtmlAsync(data.html, 'div#content_wrapper')
                .then(insertedHtml => {
                    ejecucionproceso_document_ready(1);
                })
                .catch(error => {
                    console.error('Error al insertar elementos:', error);
                });
            //
            $('section#ejecucion_tabla').hide();
        }
    });
}

function createHtmlAsync(dataHtml, id_html_element) {
    return new Promise((resolve, reject) => {
        // Agregar el html al contentWrapper
        $(id_html_element).append(dataHtml);

        // Resolvemos la promesa con el html
        resolve(dataHtml);
    });
}

function actualizar_contenido_1fase(ejecucionfase_id) {
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'actualizar_contenido',
            ejecucionfase_id: ejecucionfase_id
        }),
        beforeSend: function () {
            $('li#contenido_faseid_'+ejecucionfase_id).html(
                '<div class="callout callout-info" id="mensaje_ejecucionfase_'+ejecucionfase_id+'">'+
                    '<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>'+
                '</div>'
            ).show(300);
        },
        success: function (data) {
            if (data.estado == 1){
                $('#hdd_ejecucion_reg').val(data.ejecucion_datos_actualizados);
                $('#mensaje_ejecucionfase_'+ejecucionfase_id).hide(300);
                $('#mensaje_ejecucionfase_'+ejecucionfase_id).html('');
                createHtmlAsync(data.html, 'li#contenido_faseid_'+ejecucionfase_id)
                    .then(insertedHtml => {
                        ejecucionproceso_document_ready();
                    })
                    .catch(error => {
                        console.error('Error al insertar elementos:', error);
                    });
                //
            } else {
                $('#mensaje_ejecucionfase_'+ejecucionfase_id).removeClass('callout-info').addClass('callout-danger');
                $('#mensaje_ejecucionfase_'+ejecucionfase_id).html('<h4>Error al recargar datos...</h4>');
            }
        }
    });
}

function actualizar_comentarios(ejecucionfase_id, action2) {
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'actualizar_comentarios',
            ejecucionfase_id: ejecucionfase_id,
            action2: action2
        }),
        beforeSend: function () {
            $('div#comentarios_anteriores_'+ejecucionfase_id).html(
                '<div class="callout callout-info" id="mensaje_comentarios_ejecucionfase_'+ejecucionfase_id+'">'+
                    '<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>'+
                '</div>'
            ).show(300);
        },
        success: function (data) {
            if (data.estado == 1){
                $('#mensaje_comentarios_ejecucionfase_'+ejecucionfase_id).hide(300);
                $('div#comentarios_anteriores_'+ejecucionfase_id).html(data.html);
            } else {
                $('#mensaje_comentarios_ejecucionfase_'+ejecucionfase_id).removeClass('callout-info').addClass('callout-danger');
                $('#mensaje_comentarios_ejecucionfase_'+ejecucionfase_id).html('<h4>Error al recargar datos...</h4>');
            }
        }
    });
}

function historial_form(tabla_nom, tabla_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "historial/historial_form.php",
        async: true,
        dataType: "html",
        data: ({
            tabla_nom: tabla_nom,
            tabla_id: tabla_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_historial_form').html(html);
            $('#modal_'+tabla_nom+'_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('modal_'+tabla_nom+'_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_'+tabla_nom+'_historial_form'); //funcion encontrada en public/js/generales.js
        }
    });
}

function buscar_alertas(modulo_nom, modulo_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "alerta/alerta_controller.php",
        async: true,
        dataType: "json",
        data: ({
            action_alerta: 'buscar_alerta',
            modulo_nom: modulo_nom,
            modulo_id: modulo_id
        }),
        beforeSend: function() {
            $('div#alertas_ejecucion').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>').show(300);
        },
        success: function (data) {
            if (data.estado == 1) {
                $('div#alertas_ejecucion').html(data.html);
            } else {
                $('div#alertas_ejecucion').html('').hide(300);
            }
        }
    });
}

function cambiar_estado_alerta(alerta_id, estado) {
    $.confirm({
        title: 'CONFIRME',
        content: `¿Alerta leída?`,
        type: 'green',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            ACEPTAR: {
                text: 'ACEPTAR',
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'alerta/alerta_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action_alerta: 'cambiar_estado',
                            hdd_alerta_id: alerta_id,
                            estado: estado
                        }),
                        success: function (data) {
                            if (data.estado == 1) {
                                alerta_success("SISTEMA", data.mensaje);
                                buscar_alertas('ejecucion', '');
                            } else {
                                alerta_error("SISTEMA", data.mensaje);
                            }
                        }
                    });
                }
            },
            cancelar: function () {
            }
        }
    });
}

function serialize_form(id_form, extra) {
    var elementos_disabled = $(`#${id_form}`).find('input:disabled, select:disabled, textarea:disabled').removeAttr('disabled');
    var form_serializado = $(`#${id_form}`).serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

function cambiar_estado(ejecucion_id, ejecucionfase_id, valor, action) {
    var tabla = `&tabla=tb_ejecucion`;
    var columna = `&columna=tb_ejecucion_estadoproceso`;
    var valor = `&valor=${valor}`;
    var tipo_dato = `&tipo_dato=INT`;
    var ejecucion_id = `&ejecucion_id=${ejecucion_id}`;
    var ejecucionfase_id = `&ejecucionfase_id=${ejecucionfase_id}`;
    var form_serializado = 'action=modificar_campo'+tabla+columna+valor+tipo_dato+ejecucion_id+ejecucionfase_id;
    $.confirm({
        title: 'CONFIRME',
        content: `<b>¿Desea ${action} el proceso de ejecución legal?</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            ACEPTAR: {
                text: 'ACEPTAR',
                btnClass: 'btn-orange',
                action: function () {
                    modificar_campo('ejecucion_tabla_suspender', form_serializado, '');
                }
            },
            cancelar: function () {
            }
        }
    });
}

function modificar_campo(formulario_nombre, form_serializado, ejecucionfase_id, action2) {
    console.log(formulario_nombre, form_serializado, ejecucionfase_id, action2);
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: form_serializado,
        success: function (data) {
            if (data.estado == 1) {
                alerta_success("SISTEMA", data.mensaje);
                if (formulario_nombre == 'form_persona_vb') {
                    disabled($('#cbo_persona_vb'));
                    $('#persona_vb_guardar, #persona_vb_cancelareditar').hide();
                    $('#persona_vb_editar').show();
                    actualizar_contenido_1fase(ejecucionfase_id);
                } else if (formulario_nombre == 'form_cnllevarnotaria_fecha') {
                    disabled($('#txt_cnllevarnotaria_fecha'));
                    $('#cn_llevarnotaria_guardar, #cn_llevarnotaria_cancelareditar').hide();
                    $('#cn_llevarnotaria_editar').show();
                    actualizar_contenido_1fase(ejecucionfase_id);
                } else if (formulario_nombre == 'form_cndiligencia_fecha') {
                    disabled($('#txt_cndiligencia_fecrecojo'));
                    $('#cn_diligencia_guardar, #cn_diligencia_cancelareditar').hide();
                    $('#cn_diligencia_editar').show();
                    actualizar_contenido_1fase(ejecucionfase_id);
                } else if (formulario_nombre == 'form_esperacliente_fecha' || formulario_nombre == 'form_oficiorecojo' || formulario_nombre == 'form_oficiodescer_recojo' || formulario_nombre == 'form_ofilevordencapt_recojo') {
                    actualizar_contenido_1fase(ejecucionfase_id);
                } else if (formulario_nombre == 'ejecucion_tabla_suspender') {
                    ejecucion_tabla();
                } else if (formulario_nombre == 'ejecucionfasefile_fec_ingreso') {
                    actualizar_contenido_1fase(ejecucionfase_id);
                    completar_tabla_files();
                } else if (formulario_nombre == 'form_procesofasecomentario') {
                    $('#modal_galeriacomentario_form').modal('hide');
                    actualizar_comentarios(ejecucionfase_id, action2);
                }
            } else {
                alerta_error("SISTEMA", data.mensaje);
            }
        },
        complete: function (data) {
            console.log(data);
        }
    });
}

function llenar_cbo_estados(cgvsubtipo_id) {
    if (parseInt(cgvsubtipo_id) > 0) {
        $.ajax({
            type: 'POST',
            url: VISTA_URL + 'ejecucion/estadoejecucion_cgvsubtipo_select.php',
            async: true,
            dataType: 'html',
            data: ({
                cgvsubtipo_id: cgvsubtipo_id,
            }),
            success: function (data) {
                $('#cbo_fil_estadoejecucion_cgvsubtipo_id').html(data);
                $('#div_fil_estadoejecucion_cgvsubtipo_id').css('width', '335px');
            }
        });
        $('#cbo_fil_estadoejecucion_cgvsubtipo_id').removeAttr('disabled');
    }
}

function reasignar_grupo() {
    var id_fallo = '';
    var box_seleccionados = $('#div_ejecucion_tabla').find('input[type="checkbox"][id*="che_ejec_"]:checked');
    if (box_seleccionados.length == 0) {
        alerta_error('Error', 'No seleccionó ningún registro');
        return;
    }
    $.confirm({
        title: 'Cambiar usuario encargado',
        content: `Está cambiando el usuario encargado de este grupo de casos de ejecución<br>`+
            `<label>Asignar a:</label>`+
            `<select id="cbo_usu_encargado_n" name="cbo_usu_encargado_n" class="input-sm form-control">`+
                $('#txt_fil_encargado_id').html()+
            `</select>`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            Confirmar: {
                btnClass: 'btn-blue',
                action: function () {
                    var usuario_n = this.$content.find('#cbo_usu_encargado_n').val();
                    var usuario_n_nombre = this.$content.find('#cbo_usu_encargado_n option:selected').text();
                    if (parseInt(usuario_n) === 0) {
                        alerta_error('Error', 'Elija un usuario');
                        return;
                    }
                    for (let i = 0; i < box_seleccionados.length; i++) {
                        var ejec_id = box_seleccionados[i].id.split('_')[2];
                        var ejec_reg = $('#div_ejecucion_tabla').find(`#hdd_ejecreg_${ejec_id}`).val();
                        
                        $.ajax({
                            type: 'POST',
                            url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
                            async: true,
                            dataType: 'json',
                            data: ({
                                action: 'modificar_campo',
                                tabla: 'tb_ejecucion',
                                columna: 'tb_ejecucion_usuencargado',
                                ejecucion_id: ejec_id,
                                hdd_ejecucion_reg: ejec_reg,
                                ejecucionfase_id: $('#hdd_ejecfaseid_'+ejec_id).val(),
                                valor: usuario_n,
                                tipo_dato: 'INT',
                                usuarionuevo_nombre: usuario_n_nombre
                            }),
                            success: function (data) {
                                if (parseInt(data.estado) != 1) {
                                    id_fallo += "<br>- fallo en id "+ data.ejec_id;
                                }
                            },
                            complete: function (data) {
                                console.table(data.responseText);
                            }
                        });
                    }
                    $.alert("Éxito en la operación"+ id_fallo);
                    setTimeout(function(){
                        ejecucion_tabla();
                    }, 500);
                }
            },
            cancelar: function () {
            }
        }
    });
}

/* GERSON (18-12-24) */
function ver_gastos_ejecucion(ejecucion_id, ejecucion_fase, credito_id, origen){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"ejecucion/ejecucion_gastos.php",
		async:true,
		dataType: "html",                      
		data: ({
			ejecucion_id: ejecucion_id,
			ejecucion_fase: ejecucion_fase,
            credito_id: credito_id,
            origen: origen
		}),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
		success: function(html){
            $('#div_modal_gastos_ejecucion').html(html);		
            $('#modal_gastos_ejecucion').modal('show');
            $('#modal_mensaje').modal('hide');
          
            modal_hidden_bs_modal('modal_gastos_ejecucion', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_height_auto('modal_gastos_ejecucion'); //funcion encontrada en public/js/generales.js
		},
        complete: function (html) {

        }
	});
}
/*  */