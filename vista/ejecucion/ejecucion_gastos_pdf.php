<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
require_once('../../static/tcpdf/tcpdf.php');

require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");


$ejecucion_id = intval($_GET['ejecucion_id']);
$ejecucion_fase = intval($_GET['ejecucion_fase']);
$cre_id = intval($_GET['credito_id']);
$origen = $_GET['origen'];
$usuario_id = $_SESSION['usuario_id'];
$hoy = date('Y-m-d');
$estado = $_GET['estado'];


  $dts1 = null;
  $dts2 = null;
  $dts3 = null;

  $c_total_garveh = 0.00;
  $c_moneda_garveh = '';
  $c_total_adenda = 0.00;
  $c_moneda_adenda = '';
  $c_total_gasto = 0.00;
  $c_moneda_gasto = '';

  $ejecucion_activa = false;

  $credito_id = 0;
  $cliente_id = 0;
  $ejecucionliberacion_id = 0;
  $fec_incautacion = '';
  $fec_fin = '';
  
  if($estado=='liberado'){ // registros antiguos
    $ejecucionliberacion_id = intval($_GET['ejecucionliberacion_id']);
    $ejecucionLiberacion = $oEjecucionfasefile->mostrarUnoEjecucionLineracion($ejecucionliberacion_id);
  }else{
    if($origen == 'ejecucion'){
      $ejecucionLiberacion = $oEjecucionfasefile->existeEjecucionLiberacion($ejecucion_id);
    }elseif($origen == 'credito'){
      $ejecucionLiberacion = $oEjecucionfasefile->existeEjecucionLiberacionXCredito($cre_id);
    }
  }
  
  if($ejecucionLiberacion['estado'] == 1){
    $credito_id = intval($ejecucionLiberacion['data']['tb_credito_id']);
    $fec_incautacion = date('Y-m-d', strtotime($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecreg']));
    if($estado=='liberado'){ // registros antiguos
      $fec_fin = date('Y-m-d', strtotime($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin']));
    }
    $ejecucionliberacion_id = intval($ejecucionLiberacion['data']['tb_ejecucionliberacion_id']);

    $credito = $oEjecucionfasefile->mostrarCreditoXId($credito_id);
    if($credito['estado'] == 1){
      $cliente_id = $credito['data']['tb_cliente_id'] != null || $credito['data']['tb_cliente_id'] > 0 ? intval($credito['data']['tb_cliente_id']) : 0;
    }
    
    if($estado=='liberado'){ // registros antiguos
      
      $dts1 = $oEjecucionfasefile->mostrar_pagos_garveh($cliente_id, $fec_incautacion, $fec_fin);
      $dts2 = $oEjecucionfasefile->mostrar_pagos_adenda($cliente_id, $fec_incautacion, $fec_fin);

      $dts3 = $oEjecucionfasefile->obtenerSumaGastos($cliente_id, $fec_incautacion, $fec_fin);
      if($dts3['estado'] == 1){
        $c_total_gasto = formato_moneda($dts3['data']['total_gasto']);
        if(intval($dts3['data']['tb_moneda_id']) == 1){
          $c_moneda_gasto = 'S/.';
        }else{
          $c_moneda_gasto = '$';
        }
      }

      $ejecucion_activa = true;

    }else{

      // validamos si la ejecución está activa
      if($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin'] == null || $ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin'] == ''){
        if($ejecucionLiberacion['data']['tb_ejecucionliberacion_xac'] == 1){

          $dts1 = $oEjecucionfasefile->mostrar_pagos_garveh($cliente_id, $fec_incautacion, $fec_fin);
          $dts2 = $oEjecucionfasefile->mostrar_pagos_adenda($cliente_id, $fec_incautacion, $fec_fin);

          $dts3 = $oEjecucionfasefile->obtenerSumaGastos($cliente_id, $fec_incautacion, $fec_fin);
          if($dts3['estado'] == 1){
            $c_total_gasto = formato_moneda($dts3['data']['total_gasto']);
            if(intval($dts3['data']['tb_moneda_id']) == 1){
              $c_moneda_gasto = 'S/.';
            }else{
              $c_moneda_gasto = '$';
            }
          }

          $ejecucion_activa = true;
        }
      }
      
    }
    
  }

$title = 'GASTOS DE EJECUCIÓN';
$codigo = "";
//$codigo = 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {

  public function Header() {
    //$image_file = K_PATH_IMAGES.'logo.jpg';
    //$this->Image($image_file, 10, 10, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CGV-'.str_pad($_GET['credito_id'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.prestamosdelnorte.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.prestamosdelnorte.com');
  $pdf->SetKeywords('www.prestamosdelnorte.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  //$pdf->setLanguageArray();

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

  $html = '';

  $html = '
    <style>
      .tb_crono{
        font-size: 16pt;
      }
      td{
        padding-top: 5px;
        padding-bottom: 5px;
      }
    </style>
  ';

  $html.= '
    <table>
      <tr>
        <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 40px; height: 50px;"></td>
        <td colspan="12" style="text-align: center;">
          <b><u>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</u></b>
          <br/>Av. Mariscal Nieto N° 480, Ext. A – 7 Primer Piso
          <br/>Chiclayo, Chiclayo, Lambayeque
        </td>
        <td colspan="3"><br/><br/><b>'.$codigo.'</b></td>
      </tr>
    </table>
    <br/>
    <br/>
    <table>
      <tr>
        <td colspan="5"></td>
        <td colspan="8" style="text-align: center;"></td>
        <td colspan="7">'.get_nombre_dia(date('Y-m-d')).', '.fechaActual(date('Y-m-d')).'</td>
      </tr>
    </table>
    <table>
      <tr>
        <td colspan="5"></td>
        <td colspan="12" style="text-align: center;"><br/><br/><strong><u>GASTOS DE EJECUCIÓN</u></strong></td>
        <td colspan="3"></td>
      </tr>
    </table>
    <br/>';

    $html .= '<br>
    <table width="40%" border="1" cellpadding="3" style="font-size:30px">
      <tr>
        <td width="60%"><b>FECHA DE INCAUTACIÓN</b></td>
        <td>'.mostrar_fecha($fec_incautacion).'</td>
      </tr>
    </table>';

    if($estado=='liberado'){ // registros antiguos
      $html .= '<br>
      <table width="40%" border="1" cellpadding="3" style="font-size:30px">
        <tr>
          <td width="60%"><b>FECHA DE LIBERACIÓN</b></td>
          <td>'.mostrar_fecha($fec_fin).'</td>
        </tr>
      </table>';
    }

  /* GARVEH */
    $html .='
      <p></p>
      <h4 class="box-title">INFORMACIÓN DE CUOTAS GARVEH <strong>CGV-'.$credito_id.'</strong></h4>';
    if ($dts1['estado'] == 1) {
      $num=0;
      $total1 = 0.00;

      $html .= '<table width="100%" border="1" class="tb_crono" cellpadding="3">
        <thead>
          <tr style="font-size:25px; font-weight: bold; background-color: rgb(0, 166, 90); color: black;">
            <th align="center" width="5%">N°</th>
            <th align="center" width="10%">FECHA INGRESO</th>
            <th align="center" width="15%">CUENTA</th>
            <th align="center" width="35%">DETALLE</th>
            <th align="center" width="10%">FECHA CUOTA</th>
            <th align="center" width="10%">N° DE CUOTA</th>
            <th align="center">MONTO</th>
          </tr>
        </thead>
        <tbody>';

            foreach ($dts1['data'] as $key => $dt1) {

              $moneda1 = '';
              if($dt1['tb_moneda_id']==1){
                $moneda1 = 'S/.';
              }else{
                $moneda1 = '$';
              }

              $cuota1 = '';
              if ($dt1['tb_cuotatipo_id'] == 2) {
                $cuota1 = $dt1['tb_cuota_num'] . '/' . $dt1['tb_credito_numcuo'];
              } else {
                $cuota1 = $dt1['tb_cuota_num'] . '/' . $dt1['tb_credito_numcuomax'];
              }
      
              $html .='<tr class="even" style="font-size:22px">
                        <td align="center" width="5%">'.($num+1).'</td>
                        <td align="center" width="10%">'.mostrar_fecha($dt1['tb_ingreso_fec']).'</td>
                        <td align="center" width="15%">'.($dt1['tb_cuentadeposito_ban'] != null ? $dt1['tb_cuentadeposito_ban'] : 'CAJA EFECTIVO').'</td>
                        <td align="left" width="35%">'.$dt1['tb_ingreso_det'].'</td>
                        <td align="center" width="10%">'.mostrar_fecha($dt1['tb_cuota_fec']).'</td>
                        <td align="center" width="10%">'.$cuota1.'</td>
                        <td align="right">'.$moneda1.' '.mostrar_moneda($dt1['tb_ingreso_imp']).'</td>';
            $html .= '</tr>';

            $num++;
            $total1 = $total1 + floatval($dt1['tb_ingreso_imp']);
          }

        $html .='
          </tbody>';
        $html .='
          <tfooter>
              <tr id="tabla_cabecera"style="font-size:25px; font-weight: bold; background-color: rgb(0, 166, 90); color: black;">
                <td id="tabla_cabecera_fila" colspan="6" style="text-align: right;"><strong>TOTAL</strong></td>
                <td id="tabla_cabecera_fila" style="text-align: right;"><strong>'.$moneda1.' '.mostrar_moneda($total1).'</strong></td>
              </tr>
            </tfooter>';
      $html .='</table><br>';
  }else{
    $html .='<p><center><i>SIN RESULTADOS</i></center></p>';
  }
  /* FIN GARVEH */

  /* ADENDAS */
    $html .='
      <p></p>
      <h4 class="box-title">INFORMACIÓN DE CUOTAS ADENDAS</strong></h4>';

    if ($dts2['estado'] == 1) {
      $num=0;
      $total2 = 0.00;

      $html .='<table width="100%" border="1" class="tb_crono" cellpadding="3">
        <thead>
          <tr style="font-size:25px; font-weight: bold; background-color: rgb(19, 88, 150); color: black;">
            <th align="center" width="5%">N°</th>
            <th align="center" width="10%">FECHA INGRESO</th>
            <th align="center" width="15%">CUENTA</th>
            <th align="center" width="27%">DETALLE</th>
            <th align="center" width="10%">ADENDA</th>
            <th align="center" width="10%">FECHA CUOTA</th>
            <th align="center" width="10%">N° DE CUOTA</th>
            <th align="center">MONTO</th>
          </tr>
        </thead>
        <tbody>';

          foreach ($dts2['data'] as $key => $dt2) {

            $moneda2 = '';
            if($dt2['tb_moneda_id']==1){
              $moneda2 = 'S/.';
            }else{
              $moneda2 = '$';
            }

            $cuota2 = '';
            if ($dt2['tb_cuotatipo_id'] == 2) {
              $cuota2 = $dt2['tb_cuota_num'] . '/' . $dt2['tb_credito_numcuo'];
            } else {
              $cuota2 = $dt2['tb_cuota_num'] . '/' . $dt2['tb_credito_numcuomax'];
            }
      
            $html .='<tr class="even" style="font-size:22px">
                      <td align="center" width="5%">'.($num+1).'</td>
                      <td align="center" width="10%">'.mostrar_fecha($dt2['tb_ingreso_fec']).'</td>
                      <td align="center" width="15%">'.($dt2['tb_cuentadeposito_ban'] != null ? $dt2['tb_cuentadeposito_ban'] : 'CAJA EFECTIVO').'</td>
                      <td align="left" width="27%">'.$dt2['tb_ingreso_det'].'</td>
											<td align="center" width="10%">A-CGV-'.$dt2['tb_credito_id'].'</td>
                      <td align="center" width="10%">'.mostrar_fecha($dt2['tb_cuota_fec']).'</td>
                      <td align="center" width="10%">'.$cuota2.'</td>
                      <td align="right">'.$moneda2.' '.mostrar_moneda($dt2['tb_ingreso_imp']).'</td>';
                      
            $html .= '</tr>';

            $num++;
            $total2 = $total2 + floatval($dt2['tb_ingreso_imp']);
          }

          $html .='
          </tbody>';
          $html .='
            <tfooter>
                <tr id="tabla_cabecera"style="font-size:25px; font-weight: bold; background-color: rgb(19, 88, 150); color: black;">
                  <td id="tabla_cabecera_fila" colspan="7" style="text-align: right;"><strong>TOTAL</strong></td>
                  <td id="tabla_cabecera_fila" style="text-align: right;"><strong>'.$moneda2.' '.mostrar_moneda($total2).'</strong></td>
                </tr>
              </tfooter>';
        $html .='</table><br>';
  }else{
    $html .='<p><center><i>SIN RESULTADOS</i></center></p>';
  }
  /* FIN ADENDAS */
    
  /* GASTOS */
  $html .='
  <p></p>
  <h4 class="box-title">INFORMACIÓN OTROS GASTOS</strong></h4>';

  if ($dts3['estado'] == 1) {
    if (floatval($dts3['data']['total_gasto']) > 0) {
      $num3=0;
      $total3 = 0.00;

      $html .='<table width="100%" border="1" class="tb_crono" cellpadding="3">
        <thead>
          <tr style="font-size:25px; font-weight: bold; background-color: rgb(243, 156, 18); color: black;">
            <th align="center" width="5%">N°</th>
            <th align="center" width="61%">GASTO</th>
            <th align="center">MONTO</th>
          </tr>
        </thead>
        <tbody>';
         
            $html .='<tr class="even" style="font-size:22px">
                      <td align="center" width="5%">'.($num3+1).'</td>
                      <td align="left" width="61%">DEVOLUCIÓN TOTAL DEL CLIENTE POR CONCEPTO DE GASTOS</td>
                      <td align="right">'.$c_moneda_gasto.' '.mostrar_moneda($c_total_gasto).'</td>
                    </tr>';
                      
          $html .='
            </tbody>';
            $html .='
              <tfooter>
                  <tr id="tabla_cabecera"style="font-size:25px; font-weight: bold; background-color: rgb(243, 156, 18); color: black;">
                    <td id="tabla_cabecera_fila" colspan="2" style="text-align: right;"><strong>TOTAL</strong></td>
                    <td id="tabla_cabecera_fila" style="text-align: right;"><strong>'.$c_moneda_gasto.' '.mostrar_moneda($c_total_gasto).'</strong></td>
                  </tr>
                </tfooter>';
          $html .='</table><br>';
    }else{
      $html .='<p><center><i>SIN RESULTADOS</i></center></p>';
    }
  }else{
    $html .='<p><center><i>SIN RESULTADOS</i></center></p>';
  }
  /* FIN GASTOS */


// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$title."_".$titulo_moneda.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>

