<?php
session_name("ipdnsac");
session_start();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();

$ejecucion_id = intval($_POST['ejecucion_id']);
$ejecucion_fase = $_POST['ejecucion_fase'];
$credito_id = intval($_POST['credito_id']);
$origen = $_POST['origen'];

$ejecucion_activa = false;

$ejecucionliberacion_id = 0;
$fec_incautacion = '';

$ejecucionLiberacion = $oEjecucionfasefile->existeEjecucionLiberacionXCredito($credito_id);

if($ejecucionLiberacion['estado'] == 1){
	$fec_incautacion = mostrar_fecha(date('Y-m-d', strtotime($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecreg'])));
	$ejecucionliberacion_id = intval($ejecucionLiberacion['data']['tb_ejecucionliberacion_id']);

    if($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin'] == null || $ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin'] == ''){
		if($ejecucionLiberacion['data']['tb_ejecucionliberacion_xac'] == 1){
            $ejecucion_activa = true;
        }
    }
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_gastos_ejecucion_fecha" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">FECHA DE ACTA DE INCAUTACIÓN</h4>
            </div>

            <div class="modal-body">
                <div id="gasto_select" class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Fecha Inicial: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" <?php if($ejecucion_activa){ echo 'disabled="disabled"'; } ?> name="incautacion_fec" id="incautacion_fec" value="<?php if($ejecucion_activa){echo $fec_incautacion;}else{echo date('d-m-Y');} ?>">
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <?php if(!$ejecucion_activa){ ?>
                    <button type="submit" id="btn_guardar_fecha" class="btn btn-success" onclick="registrar_ejecucionliberacion(<?php echo $credito_id; ?>, 'credito')">Guardar</button>
                <?php } ?>
                <button type="button" id="btn_cerrar" class="btn btn-danger" onclick="" data-dismiss="modal">Cerrar</button>
            </div>
            
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('#incautacion_fec').datepicker({
            language: 'es',
            autoclose: true,
            format: "dd-mm-yyyy",
        });
    });

    function registrar_ejecucionliberacion(credito_id, origen) {
        let fecha = $('#incautacion_fec').val();
        $.ajax({
            type: 'POST',
            url: VISTA_URL + 'ejecucionupload/ejecucionupload_controller.php',
            async: true,
            dataType: 'json',
            data: {
                action: 'registrar_ejecucionliberacion',
                fecha: fecha,
                credito_id: credito_id,
                origen: origen
            },
            success: function (data) {
                if (data.estado == 1) {
                    alerta_success("SISTEMA", data.mensaje);
                    $('#modal_gastos_ejecucion_fecha').modal('hide');
                } else {
                    alerta_error("SISTEMA", data.mensaje);
                }
            },
            complete: function (data) {
                console.log(data);
            }
        });
    }

    
</script>