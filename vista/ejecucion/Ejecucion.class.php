<?php
if (defined('APP_URL'))
    require_once APP_URL . 'datos/conexion.php';
else
    require_once '../../datos/conexion.php';
//

class Ejecucion extends Conexion {
    public $id;
    public $cliente_id;
    public $credito_id;
    public $cgarvtipo_id;
    public $cgvsubtipo_id;
    public $estadoejecucion_cgvsubtipo_id;
    public $moneda_id;
    public $tipocambio;
    public $montoliquidacion;
    public $persona_id;
    public $cargo_id;
    public $estadoproceso;
    public $motivo_anulacion;
    public $usuencargado;
    public $fecreg;
    public $usureg;
    public $xac;
    public $revisionpersona_id;
    public $ejecucionfase_incluye_xac = 1;
    public $fecmax_espera;
    public $expediente;
    public $especialista;
    public $juzgado;
    public $demanda_fechora_presentacion;
    public $nombrejuez;
    public $legalizacionpago_id;
    public $cartanotpago_id;
    public $oficiodescer_fecrecojo;
    public $oficiodescer_legalizacionpago_id;
    public $oficiolevanincau_fecrecojo;
    public $oficiolevanincau_legalizacionpago_id;
    public $credito_id_padreorigen;

    public function listar_todos($ejecucion_id, $cliente_id, $credito_id, $fec_ini, $fec_fin, $estadoejecucion_id) {
        $where_opt = '';
        if (!empty($ejecucion_id)) {
            $where_opt .= ' AND ej.tb_ejecucion_id = :param_opc0';
        }
        if (!empty($cliente_id)) {
            $where_opt .= ' AND ej.tb_cliente_id = :param_opc1';
        }
        if (!empty($credito_id)) {
            $where_opt .= ' AND ej.tb_credito_id = :param_opc2';
        }
        if (!empty($fec_ini) && !empty($fec_fin)) {
            $where_opt .= " AND DATE_FORMAT(ej.tb_ejecucion_fecreg, '%Y-%m-%d') BETWEEN :param_fecini and :param_fecfin";
        }
        if (!empty($estadoejecucion_id)) {
            $where_opt .= ' AND ecs.tb_estadoejecucion_id = :param_opc3';
        }
        if (!empty($this->usuencargado)) {
            $where_opt .= ' AND ej.tb_ejecucion_usuencargado = :param_opc4';
        }
        if (!empty($this->cgarvtipo_id)) {
            $where_opt .= ' AND ej.tb_cgarvtipo_id = :param_opc5';
        }
        if (!empty($this->cgvsubtipo_id)) {
            $where_opt .= ' AND ej.tb_cgvsubtipo_id = :param_opc6';
        }
        if (!empty($this->estadoejecucion_cgvsubtipo_id)) {
            $where_opt .= ' AND ej.tb_estadoejecucion_cgvsubtipo_id = :param_opc7';
        }
        if (in_array(strval($this->estadoproceso), array('0', '1', '2', '3'))) {
            $where_opt .= empty($where_opt) ? 'ej.tb_ejecucion_estadoproceso = :param_opc8' : ' AND ej.tb_ejecucion_estadoproceso = :param_opc8';
        }
        try {
            $sql = "SELECT ej.*,
                        cl.tb_cliente_doc, cl.tb_cliente_nom, cl.tb_cliente_empruc, cl.tb_cliente_emprs, cl.tb_cliente_tip,
                        cgv.tb_moneda_id, cgv.tb_credito_vehpla, cgv.tb_credito_reg,
                        cgt.cgarvtipo_nombre2,
                        cs.tb_cgvsubtipo_nom,
                        ecs.tb_estadoejecucion_cgvsubtipo_numorden as estadoejecucion_ordenactual, ecs.tb_estadoejecucion_cgvsubtipo_regresa,
                        ee.tb_estadoejecucion_id, ee.tb_estadoejecucion_nom as estado_actual,
                        us.tb_usuario_nom, us.tb_usuario_ape, us.tb_usuarioperfil_id,
                        c.tb_cargo_nom
                    FROM tb_ejecucion ej
                    INNER JOIN tb_cliente cl ON ej.tb_cliente_id = cl.tb_cliente_id
                    INNER JOIN tb_creditogarveh cgv ON ej.tb_credito_id = cgv.tb_credito_id
                    INNER JOIN cgarvtipo cgt ON ej.tb_cgarvtipo_id = cgt.cgarvtipo_id
                    INNER JOIN tb_cgvsubtipo cs ON ej.tb_cgvsubtipo_id = cs.tb_cgvsubtipo_id
                    INNER JOIN tb_estadoejecucion_cgvsubtipo ecs ON ej.tb_estadoejecucion_cgvsubtipo_id = ecs.tb_estadoejecucion_cgvsubtipo_id
                    INNER JOIN tb_estadoejecucion ee ON ecs.tb_estadoejecucion_id = ee.tb_estadoejecucion_id
                    INNER JOIN tb_usuario us ON ej.tb_ejecucion_usuencargado = us.tb_usuario_id
                    LEFT JOIN tb_cargo c ON ej.tb_cargo_id = c.tb_cargo_id
                    WHERE ej.tb_ejecucion_xac = 1 $where_opt
                    ORDER BY ej.tb_ejecucion_fecreg DESC;";
            //
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($ejecucion_id)) {
                $sentencia->bindParam(':param_opc0', $ejecucion_id, PDO::PARAM_INT);
            }
            if (!empty($cliente_id)) {
                $sentencia->bindParam(':param_opc1', $cliente_id, PDO::PARAM_INT);
            }
            if (!empty($credito_id)) {
                $sentencia->bindParam(':param_opc2', $credito_id, PDO::PARAM_INT);
            }
            if (!empty($fec_ini) && !empty($fec_fin)) {
                $sentencia->bindParam(':param_fecini', $fec_ini, PDO::PARAM_STR);
                $sentencia->bindParam(':param_fecfin', $fec_fin, PDO::PARAM_STR);
            }
            if (!empty($estadoejecucion_id)) {
                $sentencia->bindParam(':param_opc3', $estadoejecucion_id, PDO::PARAM_INT);
            }
            if (!empty($this->usuencargado)) {
                $sentencia->bindParam(':param_opc4', $this->usuencargado, PDO::PARAM_INT);
            }
            if (!empty($this->cgarvtipo_id)) {
                $sentencia->bindParam(':param_opc5', $this->cgarvtipo_id, PDO::PARAM_INT);
            }
            if (!empty($this->cgvsubtipo_id)) {
                $sentencia->bindParam(':param_opc6', $this->cgvsubtipo_id, PDO::PARAM_INT);
            }
            if (!empty($this->estadoejecucion_cgvsubtipo_id)) {
                $sentencia->bindParam(':param_opc7', $this->estadoejecucion_cgvsubtipo_id, PDO::PARAM_INT);
            }
            if (in_array(strval($this->estadoproceso), array('0', '1', '2', '3'))) {
                $sentencia->bindParam(':param_opc8', $this->estadoproceso, PDO::PARAM_INT);
            }
            $sentencia->execute();
            $retorno['cantidad'] = $sentencia->rowCount();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_ejecucion (
                        tb_cliente_id,
                        tb_credito_id,
                        tb_cgarvtipo_id,
                        tb_cgvsubtipo_id,
                        tb_estadoejecucion_cgvsubtipo_id,
                        tb_moneda_id,
                        tb_ejecucion_usuencargado,
                        tb_ejecucion_usureg,
                        tb_credito_id_padreorigen)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        :param6,
                        :param7,
                        :param8)";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->cgarvtipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param3', $this->cgvsubtipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->estadoejecucion_cgvsubtipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param5', $this->moneda_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param6', $this->usuencargado, PDO::PARAM_INT);
            $sentencia->bindParam(':param7', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param8', $this->credito_id_padreorigen, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar_campo($ejecucion_id, $ejecucion_columna, $ejecucion_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($ejecucion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_ejecucion SET " . $ejecucion_columna . " = :ejecucion_valor WHERE tb_ejecucion_id = :ejecucion_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":ejecucion_id", $ejecucion_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":ejecucion_valor", $ejecucion_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":ejecucion_valor", $ejecucion_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function buscar_cgvsubtipo_por($cgvsubtipo_id, $cgarvtipo_id, $cgvsubtipo_adenda) {
        try {
            $sql_add = '';
            if (!empty($cgvsubtipo_id)) {
                $sql_add .= ' AND cgvs.tb_cgvsubtipo_id = :cgvsubtipo_id';
            }
            if (!empty($cgarvtipo_id)) {
                $sql_add .= " AND cgvs.cgarvtipo_id in ($cgarvtipo_id)"; 
            }
            if (is_int($cgvsubtipo_adenda)) {
                $sql_add .= " AND cgvs.tb_cgvsubtipo_adenda = :cgvsubtipo_adenda";
            }
            $sql = "SELECT cgvs.*
                    FROM tb_cgvsubtipo cgvs
                    WHERE cgvs.tb_cgvsubtipo_xac = 1$sql_add;";

            $sentencia = $this->dblink->prepare($sql);
            if (!empty($cgvsubtipo_id)) {
                $sentencia->bindParam(':cgvsubtipo_id', $cgvsubtipo_id, PDO::PARAM_INT);
            }
            if (is_int($cgvsubtipo_adenda)) {
                $sentencia->bindParam(':cgvsubtipo_adenda', $cgvsubtipo_adenda, PDO::PARAM_INT);
            }
            $sentencia->execute();
            $retorno['sql'] = $sql;

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function buscar_estadoejecucion_cgvsubtipo_por($estadoejecucion_cgvsubtipo_id, $cgvsubtipo_id, $estadoejecucion_id, $estadoejecucion_cgvsubtipo_numorden) {
        try {
            $sql_add = '';
            if (!empty($estadoejecucion_cgvsubtipo_id)) {
                $sql_add .= ' AND ejec_sub.tb_estadoejecucion_cgvsubtipo_id = :estadoejecucion_cgvsubtipo_id';
            }
            if (!empty($cgvsubtipo_id)) {
                $sql_add .= ' AND ejec_sub.tb_cgvsubtipo_id = :cgvsubtipo_id';
            }
            if (!empty($estadoejecucion_id)) {
                $sql_add .= ' AND ejec_sub.tb_estadoejecucion_id = :estadoejecucion_id'; 
            }
            if (!empty($estadoejecucion_cgvsubtipo_numorden)) {
                $sql_add .= ' AND ejec_sub.tb_estadoejecucion_cgvsubtipo_numorden = :estadoejecucion_cgvsubtipo_numorden';
            }
            $sql = "SELECT ejec_sub.*, est_eje.tb_estadoejecucion_nom
                    FROM tb_estadoejecucion_cgvsubtipo ejec_sub
                    INNER JOIN tb_estadoejecucion est_eje on ejec_sub.tb_estadoejecucion_id = est_eje.tb_estadoejecucion_id
                    WHERE ejec_sub.tb_estadoejecucion_cgvsubtipo_xac = 1$sql_add;";

            $sentencia = $this->dblink->prepare($sql);
            if (!empty($estadoejecucion_cgvsubtipo_id)) {
                $sentencia->bindParam(':estadoejecucion_cgvsubtipo_id', $estadoejecucion_cgvsubtipo_id, PDO::PARAM_INT);
            }
            if (!empty($cgvsubtipo_id)) {
                $sentencia->bindParam(':cgvsubtipo_id', $cgvsubtipo_id, PDO::PARAM_INT);
            }
            if (!empty($estadoejecucion_id)) {
                $sentencia->bindParam(':estadoejecucion_id', $estadoejecucion_id, PDO::PARAM_INT);
            }
            if (!empty($estadoejecucion_cgvsubtipo_numorden)) {
                $sentencia->bindParam(':estadoejecucion_cgvsubtipo_numorden', $estadoejecucion_cgvsubtipo_numorden, PDO::PARAM_INT);
            }
            $sentencia->execute();
            $retorno['sql'] = $sql;

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar_ejecucionfase($ejecucion_id, $tb_estadoejecucion_cgvsubtipo_id, $tb_ejecucionfase_orden, $tb_ejecucionfase_obs, $tb_ejecucionfase_usureg) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_ejecucionfase (
                        tb_ejecucion_id,
                        tb_estadoejecucion_cgvsubtipo_id,
                        tb_ejecucionfase_orden,
                        tb_ejecucionfase_obs,
                        tb_ejecucionfase_usureg)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4)";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $ejecucion_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $tb_estadoejecucion_cgvsubtipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $tb_ejecucionfase_orden, PDO::PARAM_INT);
            $sentencia->bindParam(':param3', $tb_ejecucionfase_obs, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $tb_ejecucionfase_usureg, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_ejecucionfase_por($ejecucionfase_id, $ejecucion_id, $ejecucionfase_orden, $order_by, $limit) {
        $where_opt = $order = '';
        if (!empty($this->ejecucionfase_incluye_xac)) {
            $where_opt .= 'ef.tb_ejecucionfase_xac = 1';
        }
        if (!empty($ejecucionfase_id)) {
            $where_opt .= empty($where_opt) ? 'ef.tb_ejecucionfase_id = :param_opc0' : ' AND ef.tb_ejecucionfase_id = :param_opc0';
        }
        if (!empty($ejecucion_id)) {
            $where_opt .= empty($where_opt) ? 'ef.tb_ejecucion_id = :param_opc1' : ' AND ef.tb_ejecucion_id = :param_opc1';
        }
        if (!empty($ejecucionfase_orden)) {
            $where_opt .= empty($where_opt) ? 'ef.tb_ejecucionfase_orden = :param_opc2' : ' AND ef.tb_ejecucionfase_orden = :param_opc2';
        }
        if (!empty($order_by)) {
            $order = "ORDER BY $order_by";
        }
        try {
            $sql = "SELECT ef.*,
                        cl.tb_cliente_doc, cl.tb_cliente_nom, cl.tb_cliente_empruc, cl.tb_cliente_emprs, cl.tb_cliente_tip,
                        cs.tb_cgvsubtipo_nom,
                        ecs.tb_estadoejecucion_cgvsubtipo_id as estadoejecucion_idactual, ecs.tb_estadoejecucion_cgvsubtipo_numorden as estadoejecucion_ordenactual, ecs.tb_estadoejecucion_cgvsubtipo_regresa as estadoejecucion_regresa,
                        ee.tb_estadoejecucion_id, ee.tb_estadoejecucion_nom as estado_actual,
                        ecs1.tb_estadoejecucion_cgvsubtipo_id as estadoejecucion_idnext, ecs1.tb_estadoejecucion_cgvsubtipo_numorden as estadoejecucion_ordennext,
                        ee1.tb_estadoejecucion_nom as estado_next,
                        us.tb_usuario_nom, us.tb_usuario_ape, us.tb_usuarioperfil_id
                    FROM tb_ejecucionfase ef
                    INNER JOIN tb_ejecucion ej ON ef.tb_ejecucion_id = ej.tb_ejecucion_id
                    INNER JOIN tb_cliente cl ON ej.tb_cliente_id = cl.tb_cliente_id
                    INNER JOIN tb_cgvsubtipo cs ON ej.tb_cgvsubtipo_id = cs.tb_cgvsubtipo_id
                    INNER JOIN tb_estadoejecucion_cgvsubtipo ecs ON ef.tb_estadoejecucion_cgvsubtipo_id = ecs.tb_estadoejecucion_cgvsubtipo_id
                    INNER JOIN tb_estadoejecucion ee ON ecs.tb_estadoejecucion_id = ee.tb_estadoejecucion_id
                    LEFT JOIN tb_estadoejecucion_cgvsubtipo ecs1 ON (ecs1.tb_cgvsubtipo_id = ej.tb_cgvsubtipo_id AND ecs1.tb_estadoejecucion_cgvsubtipo_numorden = ecs.tb_estadoejecucion_cgvsubtipo_numorden+1)
                    LEFT JOIN tb_estadoejecucion ee1 ON ecs1.tb_estadoejecucion_id = ee1.tb_estadoejecucion_id
                    INNER JOIN tb_usuario us ON ej.tb_ejecucion_usuencargado = us.tb_usuario_id
                    WHERE $where_opt
                    $order $limit;";
            //
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($ejecucionfase_id)) {
                $sentencia->bindParam(':param_opc0', $ejecucionfase_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucion_id)) {
                $sentencia->bindParam(':param_opc1', $ejecucion_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucionfase_orden)) {
                $sentencia->bindParam(':param_opc2', $ejecucionfase_orden, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function modificar_ejecucionfase_campo($ejecucionfase_id, $ejecucionfase_columna, $ejecucionfase_valor, $param_tip, $usuario_id){
        $this->dblink->beginTransaction();
        try {
            if (!empty($ejecucionfase_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_ejecucionfase SET " . $ejecucionfase_columna . " = :ejecucionfase_valor WHERE tb_ejecucionfase_id = :ejecucionfase_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":ejecucionfase_id", $ejecucionfase_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":ejecucionfase_valor", $ejecucionfase_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":ejecucionfase_valor", $ejecucionfase_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function insertar_ejecucionfase_comentario($procesofase_id, $usuario_id, $comentario_des) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_procesofase_comentario (
                        tb_procesofase_id,
                        tb_usuario_id,
                        tb_comentario_des)
                    VALUES (
                        :param0,
                        :param1,
                        :param2);";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $procesofase_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $comentario_des, PDO::PARAM_STR);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_ejecucionfase_comentario_por($procesofase_id, $procesofase_comentario_id) {
        $where_opt = '';
        if (!empty($procesofase_id)) {
            $where_opt .= ' AND pfc.tb_procesofase_id = :param_opc';
        }
        if (!empty($procesofase_comentario_id)) {
            $where_opt .= ' AND pfc.tb_procesofase_comentario_id = :param_opc1';
        }
        try {
            $sql = "SELECT pfc.*,
                        us.tb_usuario_nom, us.tb_usuario_ape, us.tb_usuario_fot,
                        ef.tb_ejecucion_id, ef.tb_ejecucionfase_completado, ej.tb_credito_id
                    FROM tb_procesofase_comentario pfc
                    INNER JOIN tb_usuario us ON pfc.tb_usuario_id = us.tb_usuario_id
                    INNER JOIN tb_ejecucionfase ef ON pfc.tb_procesofase_id = ef.tb_ejecucionfase_id
                    INNER JOIN tb_ejecucion ej ON ef.tb_ejecucion_id = ej.tb_ejecucion_id
                    WHERE pfc.tb_procesofase_comentario_xac = 1 $where_opt
                    ORDER BY pfc.tb_procesofase_comentario_fecreg ASC;";
            //
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($procesofase_id)) {
                $sentencia->bindParam(':param_opc', $procesofase_id, PDO::PARAM_INT);
            }
            if (!empty($procesofase_comentario_id)) {
                $sentencia->bindParam(':param_opc1', $procesofase_comentario_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function listar_estadoejecucion_por($estadoejecucion_id, $xac) {
        $where_opt = '';
        if (!empty($estadoejecucion_id)) {
            $where_opt .= 'ee.tb_estadoejecucion_id = :param_opc0';
        }
        if (in_array(strval($xac), array('0', '1'))) {
            $where_opt .= empty($where_opt) ? 'ee.tb_estadoejecucion_xac = :param_opc1' : ' AND ee.tb_estadoejecucion_xac = :param_opc1';
        }

        if (!empty($where_opt)) {
            $where_opt = 'WHERE '.$where_opt;
        }
        
        try {
            $sql = "SELECT ee.*
                    FROM tb_estadoejecucion ee
                    $where_opt
                    ORDER BY ee.tb_estadoejecucion_id ASC;";
            //
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($estadoejecucion_id)) {
                $sentencia->bindParam(':param_opc0', $estadoejecucion_id, PDO::PARAM_INT);
            }
            if (in_array(strval($xac), array('0', '1'))) {
                $sentencia->bindParam(':param_opc1', $xac, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function comentario_modificar_campo($procesofase_comentario_id, $procesofase_comentario_columna, $procesofase_comentario_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($procesofase_comentario_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_procesofase_comentario SET " . $procesofase_comentario_columna . " = :procesofase_comentario_valor WHERE tb_procesofase_comentario_id = :procesofase_comentario_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":procesofase_comentario_id", $procesofase_comentario_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":procesofase_comentario_valor", $procesofase_comentario_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":procesofase_comentario_valor", $procesofase_comentario_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
