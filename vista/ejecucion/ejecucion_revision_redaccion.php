<?php
require_once '../../core/usuario_sesion.php';
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();

$ejecucionfase_id = $_GET['ejecucionfase_id'];
$usuario_apr = $oEjecucion->listar_ejecucionfase_por($ejecucionfase_id, '', '', 'ef.tb_ejecucionfase_orden DESC', '')['data'][0]['tb_ejecucionfase_usuapr'];

$checked = $comentario = $disabled_textarea = '';
//si hay un usuario que hizo revision y aprobó las redaccion
if (!empty($usuario_apr)) {
    $checked = 'checked';
    $comentario = 'Todo está ok';
    $disabled_textarea = 'disabled';
}
?>
<div>
    <form class="formName">
        <input type="hidden" name="hdd_confirm_vb_disabled" id="hdd_confirm_vb_disabled" value="<?php echo $disabled_textarea; ?>">
        <?php //las personas que pueden dar visto bueno son Sara, Gio, y usuarios con perfil administrador
            if ($_SESSION['usuario_id'] == 71 || $_SESSION['usuario_id'] == 42 || $_SESSION['usuarioperfil_id'] == 1) { ?>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="che_visto_bueno" id="che_visto_bueno" class="flat-green <?php echo $disabled_textarea; ?>" value="1" <?php echo $checked; ?>>&nbsp; <b>TODO ESTA CORRECTO</b>
                    </label>
                </div>
            </div>
        <?php } ?>

        <div class="form-group">
            <label for="txt_nueva_observacion_vb">Observaciones/Correcciones:</label>
            <textarea name="txt_nueva_observacion_vb" id="txt_nueva_observacion_vb" placeholder="EJM: corregir redacción" class="form-control input-sm input-shadow mayus <?php echo $disabled_textarea; ?>" rows="3"><?php echo $comentario; ?></textarea>
        </div>
    </form>
    <script>
        setTimeout(function() {
            $('.flat-green[id*="che_visto_bueno"]').iCheck({
                checkboxClass: `icheckbox_flat-green`,
                radioClass: `iradio_flat-green`
            });
            $('input[id*="che_visto_bueno"]').parents('label').css('padding-left', '8px');
        }, 100);
        $("#che_visto_bueno").on('ifChanged', function() {
            if (this.checked) {
                disabled($('#txt_nueva_observacion_vb').val('Todo está ok'));
            } else {
                $('#txt_nueva_observacion_vb').val('').removeAttr('disabled').removeClass('disabled');
            }
        });
        disabled($('form.formName').find('.disabled'));
        if ($('#hdd_confirm_vb_disabled').val() == 'disabled') {
            disabled($('button.btn_confirm_vb'));
        }
    </script>
</div>
