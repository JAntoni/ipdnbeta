<?php
require_once '../../core/usuario_sesion.php';
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');
require_once('../templates/paginado.php');

require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();

if($_POST['cantidad']==NULL || $_POST['cantidad']==''){
	$pagina=1;
	$inicio=0;
	$cantidad=50;
}else{
	$pagina=$_POST['pagina'];
	$cantidad=$_POST['cantidad'];
	$inicio=($pagina-1)*$cantidad;
}

$ejecucion_id = intval($_POST['ejecucion_id']);
$ejecucion_fase = intval($_POST['ejecucion_fase']);
$cre_id = intval($_POST['credito_id']);
$origen = $_POST['origen'];
$usuario_id = $_SESSION['usuario_id'];
$hoy = date('Y-m-d');

$dts1 = null;
$dts2 = null;
$dts3 = null;

$c_total_garveh = 0.00;
$c_moneda_garveh = '';
$c_total_adenda = 0.00;
$c_moneda_adenda = '';
$c_total_gasto = 0.00;
$c_moneda_gasto = '';

$ejecucion_activa = false;

$credito_id = 0;
$cliente_id = 0;
$ejecucionliberacion_id = 0;
$fec_incautacion = '';
if($origen == 'ejecucion'){
	$ejecucionLiberacion = $oEjecucionfasefile->existeEjecucionLiberacion($ejecucion_id);
}elseif($origen == 'credito'){
	$ejecucionLiberacion = $oEjecucionfasefile->existeEjecucionLiberacionXCredito($cre_id);
	$listaEjecucion = $oEjecucionfasefile->obtenerHistorialEjecucionLiberacion($cre_id); // obtener listado de ejecuciones de incautacion
}
if($ejecucionLiberacion['estado'] == 1){
	$credito_id = intval($ejecucionLiberacion['data']['tb_credito_id']);
	$listaEjecucion = $oEjecucionfasefile->obtenerHistorialEjecucionLiberacion($credito_id); // obtener listado de ejecuciones de incautacion

	$fec_incautacion = date('Y-m-d', strtotime($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecreg']));
	$ejecucionliberacion_id = intval($ejecucionLiberacion['data']['tb_ejecucionliberacion_id']);

	$credito = $oEjecucionfasefile->mostrarCreditoXId($credito_id);
	if($credito['estado'] == 1){
		$cliente_id = $credito['data']['tb_cliente_id'] != null || $credito['data']['tb_cliente_id'] > 0 ? intval($credito['data']['tb_cliente_id']) : 0;
	}

	// validamos si la ejecución está activa
	if($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin'] == null || $ejecucionLiberacion['data']['tb_ejecucionliberacion_fecfin'] == ''){
		if($ejecucionLiberacion['data']['tb_ejecucionliberacion_xac'] == 1){
			$dts1 = $oEjecucionfasefile->mostrar_pagos_garveh($cliente_id, $fec_incautacion);
			$dts2 = $oEjecucionfasefile->mostrar_pagos_adenda($cliente_id, $fec_incautacion);

			$dts1_suma = $oEjecucionfasefile->mostrar_suma_pagos_garveh($cliente_id, $fec_incautacion, '');
			if($dts1_suma['estado'] == 1){
				$c_total_garveh = formato_moneda($dts1_suma['data']['total_garveh']);
				if(intval($dts1_suma['data']['tb_moneda_id']) == 1){
					$c_moneda_garveh = 'S/.';
				}else{
					$c_moneda_garveh = '$';
				}
			}

			$dts2_suma = $oEjecucionfasefile->mostrar_suma_pagos_adenda($cliente_id, $fec_incautacion, '');
			if($dts2_suma['estado'] == 1){
				$c_total_adenda = formato_moneda($dts2_suma['data']['total_adenda']);
				if(intval($dts2_suma['data']['tb_moneda_id']) == 1){
					$c_moneda_adenda = 'S/.';
				}else{
					$c_moneda_adenda = '$';
				}
			}

			$dts3 = $oEjecucionfasefile->obtenerSumaGastos($cliente_id, $fec_incautacion, '');
			if($dts3['estado'] == 1){
				$c_total_gasto = formato_moneda($dts3['data']['total_gasto']);
				if(intval($dts3['data']['tb_moneda_id']) == 1){
					$c_moneda_gasto = 'S/.';
				}else{
					$c_moneda_gasto = '$';
				}
			}

			$ejecucion_activa = true;
		}
	}
}

?>

<br>
<div class="row">

	<div class="col-md-12">
		
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab"><strong>PROCESO DE INCAUTACIÓN ACTUAL</strong></a></li>
				<li class=""><a href="#tab2" data-toggle="tab"><strong>PROCESOS ANTIGUOS</strong></a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab1">

					<div class="row">

						<?php if($ejecucion_activa){ ?>
							<div class="col-md-12" style="text-align: right; padding-bottom: 10px;">
								<button name="btn_volver_tabla" id="btn_volver_tabla" type="button" class="btn btn-sm btn-danger" onclick="generarGastosPDF(<?php echo $ejecucion_id; ?>, <?php echo $ejecucion_fase; ?>, <?php echo $cre_id; ?>, '<?php echo $origen; ?>', <?php echo $usuario_id; ?>, '<?php echo $hoy; ?>', 'incautado', 0)"><i class="fa fa-file-pdf-o"></i> PDF</button>
								<button name="btn_liberacion" id="btn_liberacion" type="button" class="btn btn-sm btn-info" onclick="btnCerrarIncautacion(<?php echo $ejecucionliberacion_id; ?>, '<?php echo $hoy; ?>', 'liberar')"><i class="fa fa-unlock"></i> Liberación</button>
							</div>
						<?php } ?>


						<div class="col-md-3">
							<div class="info-box bg-green" style="min-height: 70px !important;">
								<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
								<div class="info-box-content" style="margin-left: 70px !important;">
									<span class="info-box-text" style="font-size: 12px !important;"><strong>TOTAL GARVEH</strong></span>
									<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_garveh.' '.$c_total_garveh; ?></span>
									<!-- <div class="progress">
										<div class="progress-bar" style="width: 100%"></div>
									</div> -->
									<!-- <span class="progress-description">
										70% Increase in 30 Days
									</span> -->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-box bg-blue" style="min-height: 70px !important;">
								<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
								<div class="info-box-content" style="margin-left: 70px !important;">
									<span class="info-box-text" style="font-size: 12px !important;"><strong>TOTAL ADENDA</strong></span>
									<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_adenda.' '.$c_total_adenda; ?></span>
									<!-- <div class="progress">
										<div class="progress-bar" style="width: 100%"></div>
									</div> -->
									<!-- <span class="progress-description">
										70% Increase in 30 Days
									</span> -->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-box bg-yellow" style="min-height: 70px !important;">
								<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
								<div class="info-box-content" style="margin-left: 70px !important;">
									<span class="info-box-text" style="font-size: 12px !important;"><strong>OTROS GASTOS</strong></span>
									<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_gasto.' '.$c_total_gasto; ?></span>
									<!-- <div class="progress">
										<div class="progress-bar" style="width: 100%"></div>
									</div> -->
									<!-- <span class="progress-description">
										70% Increase in 30 Days
									</span> -->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-box bg-purple" style="min-height: 70px !important;">
								<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
								<div class="info-box-content" style="margin-left: 70px !important;">
									<span class="info-box-text" style="font-size: 12px !important;"><strong>TOTAL PAGADO</strong></span>
									<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_garveh.' '.($c_total_garveh+$c_total_adenda+$c_total_gasto); ?></span>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<!-- <div class="box box-success box-solid"> -->
							<div class="box box-success shadow" style="padding: 9px 10px;">
								<div class="box-header with-border" style="padding: 9px 5px;">
									<h3 class="box-title">INFORMACIÓN DE CUOTAS GARVEH <strong>CGV-<?php echo $credito_id; ?></strong></h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body">

									<?php
										if ($dts1['estado'] == 1) { 
											$num=0;
											$total1 = 0.00;
									?>
										
										<div class="col-md-12">
											<table id="tabla_lista_cuotas" class="table table-hover table-responsive dataTables_wrapper form-inline dt-bootstrap">
												<thead>
													<tr id="tabla_cabecera" style="background-color: #00a65a !important">
														<th id="tabla_cabecera_fila">#</th>
														<th id="tabla_cabecera_fila">FECHA INGRESO</th>
														<th id="tabla_cabecera_fila">CUENTA</th>
														<th id="tabla_cabecera_fila">DETALLE</th>
														<th id="tabla_cabecera_fila">FECHA CUOTA</th>
														<th id="tabla_cabecera_fila">N° CUOTA</th>
														<th id="tabla_cabecera_fila">MONTO</th>
													</tr>
												</thead>  
												
												<tbody>
													<?php 
														foreach ($dts1['data'] as $key => $dt1) {

															$moneda1 = '';
															if($dt1['tb_moneda_id']==1){
																$moneda1 = 'S/.';
															}else{
																$moneda1 = '$';
															}

															$cuota1 = '';
															if ($dt1['tb_cuotatipo_id'] == 2) {
																$cuota1 = $dt1['tb_cuota_num'] . '/' . $dt1['tb_credito_numcuo'];
															} else {
																$cuota1 = $dt1['tb_cuota_num'] . '/' . $dt1['tb_credito_numcuomax'];
															}
															
													?>
														<tr id="tabla_cabecera_fila">
															<td id="tabla_fila"><?php echo $num+1; ?></td>
															<td id="tabla_fila"><?php echo mostrar_fecha($dt1['tb_ingreso_fec']) ?></td>
															<td id="tabla_fila"><?php echo $dt1['tb_cuentadeposito_ban'] != null ? $dt1['tb_cuentadeposito_ban'] : 'CAJA EFECTIVO' ?></td>
															<td id="tabla_fila" style="text-align: left;"><?php echo $dt1['tb_ingreso_det'] ?></td>
															<td id="tabla_fila"><?php echo mostrar_fecha($dt1['tb_cuota_fec']) ?></td>
															<td id="tabla_fila"><?php echo $cuota1 ?></td>
															<td width="15%" id="tabla_fila" style="text-align: right;"><?php echo $moneda1.' '.mostrar_moneda($dt1['tb_ingreso_imp']) ?></td>
														</tr>
													<?php 
															$num++;
															$total1 = $total1 + floatval($dt1['tb_ingreso_imp']);
														}
													?>
												</tbody>
												<tfooter>
													<tr id="tabla_cabecera" style="background-color: #00a65a !important">
														<td id="tabla_cabecera_fila" colspan="6" style="text-align: right;"><strong>TOTAL</strong></td>
														<td id="tabla_cabecera_fila" style="text-align: right;"><strong><?php echo $moneda1.' '.mostrar_moneda($total1) ?></strong></td>
													</tr>
												</tfooter>
											</table>
										</div>
									<?php 
										}else{
											echo '<center><h5>SIN RESULTADOS</h5></center>';
										}
									?>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<!-- <div class="box box-primary box-solid"> -->
							<div class="box box-primary shadow" style="padding: 9px 10px;">
								<div class="box-header with-border" style="padding: 9px 5px;">
									<h3 class="box-title">INFORMACIÓN DE CUOTAS ADENDAS</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body">

									<?php
										if ($dts2['estado'] == 1) { 
											$num=0;
											$total2 = 0.00;
									?>
										
										<div class="col-md-12">
											<table id="tabla_lista_cuotas" class="table table-hover table-responsive dataTables_wrapper form-inline dt-bootstrap">
												<thead>
													<tr id="tabla_cabecera">
														<th id="tabla_cabecera_fila">#</th>
														<th id="tabla_cabecera_fila">FECHA INGRESO</th>
														<th id="tabla_cabecera_fila">CUENTA</th>
														<th id="tabla_cabecera_fila">DETALLE</th>
														<th id="tabla_cabecera_fila">ADENDA</th>
														<th id="tabla_cabecera_fila">FECHA CUOTA</th>
														<th id="tabla_cabecera_fila">N° CUOTA</th>
														<th id="tabla_cabecera_fila">MONTO</th>
													</tr>
												</thead>  
												
												<tbody>
													<?php 
														foreach ($dts2['data'] as $key => $dt2) {
															
															$moneda2 = '';
															if($dt2['tb_moneda_id']==1){
																$moneda2 = 'S/.';
															}else{
																$moneda2 = '$';
															}

															$cuota2 = '';
															if ($dt2['tb_cuotatipo_id'] == 2) {
																$cuota2 = $dt2['tb_cuota_num'] . '/' . $dt2['tb_credito_numcuo'];
															} else {
																$cuota2 = $dt2['tb_cuota_num'] . '/' . $dt2['tb_credito_numcuomax'];
															}
															
													?>
														<tr id="tabla_cabecera_fila">
															<td id="tabla_fila"><?php echo $num+1; ?></td>
															<td id="tabla_fila"><?php echo mostrar_fecha($dt2['tb_ingreso_fec']) ?></td>
															<td id="tabla_fila"><?php echo $dt2['tb_cuentadeposito_ban'] != null ? $dt2['tb_cuentadeposito_ban'] : 'CAJA EFECTIVO' ?></td>
															<td id="tabla_fila" style="text-align: left;"><?php echo $dt2['tb_ingreso_det'] ?></td>
															<td id="tabla_fila">A-CGV-<?php echo $dt2['tb_credito_id'] ?></td>
															<td id="tabla_fila"><?php echo mostrar_fecha($dt2['tb_cuota_fec']) ?></td>
															<td id="tabla_fila"><?php echo $cuota2 ?></td>
															<td width="13%" id="tabla_fila" style="text-align: right;"><?php echo $moneda2.' '.mostrar_moneda($dt2['tb_ingreso_imp']) ?></td>
														</tr>
													<?php 
															$num++;
															$total2 = $total2 + floatval($dt2['tb_ingreso_imp']);
														}
													?>
												</tbody>
												<tfooter>
													<tr id="tabla_cabecera">
														<td id="tabla_cabecera_fila" colspan="7" style="text-align: right;"><strong>TOTAL</strong></td>
														<td id="tabla_cabecera_fila" style="text-align: right;"><strong><?php echo $moneda2.' '.mostrar_moneda($total2) ?></strong></td>
													</tr>
												</tfooter>
											</table>
										</div>
									<?php 
										}else{
											echo '<center><h5>SIN RESULTADOS</h5></center>';
										}
									?>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<!-- <div class="box box-warning box-solid"> -->
							<div class="box box-warning shadow" style="padding: 9px 10px;">
								<div class="box-header with-border" style="padding: 9px 5px;">
									<h3 class="box-title">INFORMACIÓN OTROS GASTOS</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body">

									<?php
										if ($dts3['estado'] == 1) {
											if (floatval($dts3['data']['total_gasto']) > 0) {
												$num3=0;
												$total3 = 0.00;
									?>
										
											<div class="col-md-12">
												<table id="tabla_lista_cuotas" class="table table-hover table-responsive dataTables_wrapper form-inline dt-bootstrap">
													<thead>
														<tr id="tabla_cabecera" style="background-color: #f39c12 !important">
															<th id="tabla_cabecera_fila">#</th>
															<th id="tabla_cabecera_fila">GASTO</th>
															<th id="tabla_cabecera_fila">MONTO</th>
														</tr>
													</thead>  
													
													<tbody>
													
														<tr id="tabla_cabecera_fila">
															<td width="10%" id="tabla_fila">1</td>
															<td id="tabla_fila" style="text-align: left;">DEVOLUCIÓN TOTAL DEL CLIENTE POR CONCEPTO DE GASTOS</td>
															<td width="20%" id="tabla_fila" style="text-align: right;"><?php echo $c_moneda_gasto.' '.$c_total_gasto; ?></td>
														</tr>
													
													</tbody>
													<tfooter>
														<tr id="tabla_cabecera" style="background-color: #f39c12 !important">
															<td id="tabla_cabecera_fila" colspan="2" style="text-align: right;"><strong>TOTAL GASTOS</strong></td>
															<td id="tabla_cabecera_fila" style="text-align: right;"><strong><?php echo $c_moneda_gasto.' '.$c_total_gasto; ?></strong></td>
														</tr>
													</tfooter>
												</table>
											</div>
									<?php
											}else{
												echo '<center><h5>SIN RESULTADOS</h5></center>';
											}
										}else{
											echo '<center><h5>SIN RESULTADOS</h5></center>';
										}
									?>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="tab-pane" id="tab2">
					<div class="row">
						<div class="col-md-12">

							<?php if($listaEjecucion['estado'] == 1){ ?>
								<?php foreach ($listaEjecucion['data'] as $key => $value) { 

										$credito = $oEjecucionfasefile->mostrarCreditoXId($value['tb_credito_id']);
										if($credito['estado'] == 1){
											$cliente_id = $credito['data']['tb_cliente_id'] != null || $credito['data']['tb_cliente_id'] > 0 ? intval($credito['data']['tb_cliente_id']) : 0;
										}

										$fec_incautacion = date('Y-m-d', strtotime($value['tb_ejecucionliberacion_fecreg']));
										$fec_fin = date('Y-m-d', strtotime($value['tb_ejecucionliberacion_fecfin']));


										//$dts1 = $oEjecucionfasefile->mostrar_pagos_garveh($cliente_id, $fec_incautacion);
										//$dts2 = $oEjecucionfasefile->mostrar_pagos_adenda($cliente_id, $fec_incautacion);

										$dts1_suma = $oEjecucionfasefile->mostrar_suma_pagos_garveh($cliente_id, $fec_incautacion, $fec_fin);
										if($dts1_suma['estado'] == 1){
											$c_total_garveh = formato_moneda($dts1_suma['data']['total_garveh']);
											if(intval($dts1_suma['data']['tb_moneda_id']) == 1){
												$c_moneda_garveh = 'S/.';
											}else{
												$c_moneda_garveh = '$';
											}
										}

										$dts2_suma = $oEjecucionfasefile->mostrar_suma_pagos_adenda($cliente_id, $fec_incautacion, $fec_fin);
										if($dts2_suma['estado'] == 1){
											$c_total_adenda = formato_moneda($dts2_suma['data']['total_adenda']);
											if(intval($dts2_suma['data']['tb_moneda_id']) == 1){
												$c_moneda_adenda = 'S/.';
											}else{
												$c_moneda_adenda = '$';
											}
										}

										$dts3 = $oEjecucionfasefile->obtenerSumaGastos($cliente_id, $fec_incautacion, $fec_fin);
										if($dts3['estado'] == 1){
											$c_total_gasto = formato_moneda($dts3['data']['total_gasto']);
											if(intval($dts3['data']['tb_moneda_id']) == 1){
												$c_moneda_gasto = 'S/.';
											}else{
												$c_moneda_gasto = '$';
											}
										}

									?>

									<div class="box box-info box-solid collapsed-box" style="">
										<div class="box-header with-border" style="padding: 9px 5px;">
											<h3 class="box-title">EJECUCIÓN DE INCAUTACIÓN DEL (<?php echo mostrar_fecha($value['tb_ejecucionliberacion_fecini']); ?>) AL (<?php echo mostrar_fecha($value['tb_ejecucionliberacion_fecfin']); ?>)</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
											</div>
										</div>
										<div class="box-body">

											<div class="col-md-12" style="text-align: right; padding-bottom: 10px;">
												<button name="btn_volver_tabla" id="btn_volver_tabla" type="button" class="btn btn-sm btn-danger" onclick="generarGastosPDF(<?php echo $value['tb_ejecucion_id']; ?>, 0, <?php echo $value['tb_credito_id']; ?>, '<?php echo $origen; ?>', <?php echo $usuario_id; ?>, '<?php echo $hoy; ?>', 'liberado', '<?php echo $value['tb_ejecucionliberacion_id']; ?>')"><i class="fa fa-file-pdf-o"></i> PDF</button>
											</div>

											<div class="col-md-3">
												<div class="info-box bg-green" style="min-height: 70px !important;">
													<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
													<div class="info-box-content" style="margin-left: 70px !important;">
														<span class="info-box-text" style="font-size: 12px !important;"><strong>TOTAL GARVEH</strong></span>
														<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_garveh.' '.$c_total_garveh; ?></span>
													</div>
												</div>
											</div>

											<div class="col-md-3">
												<div class="info-box bg-blue" style="min-height: 70px !important;">
													<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
													<div class="info-box-content" style="margin-left: 70px !important;">
														<span class="info-box-text" style="font-size: 12px !important;"><strong>TOTAL ADENDA</strong></span>
														<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_adenda.' '.$c_total_adenda; ?></span>
													</div>
												</div>
											</div>

											<div class="col-md-3">
												<div class="info-box bg-yellow" style="min-height: 70px !important;">
													<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
													<div class="info-box-content" style="margin-left: 70px !important;">
														<span class="info-box-text" style="font-size: 12px !important;"><strong>OTROS GASTOS</strong></span>
														<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_gasto.' '.$c_total_gasto; ?></span>
													</div>
												</div>
											</div>

											<div class="col-md-3">
												<div class="info-box bg-purple" style="min-height: 70px !important;">
													<span class="info-box-icon" style="line-height: 70px !important; height: 70px !important; width: 70px !important;"><i class="fa fa-dollar"></i></span>
													<div class="info-box-content" style="margin-left: 70px !important;">
														<span class="info-box-text" style="font-size: 12px !important;"><strong>TOTAL PAGADO</strong></span>
														<span class="info-box-number" style="font-size: 14px !important;"><?php echo $c_moneda_garveh.' '.($c_total_garveh+$c_total_adenda+$c_total_gasto); ?></span>
													</div>
												</div>
											</div>

										</div>
									</div>

								<?php } ?>
							<?php }else{ ?>
								<center><h5>SIN RESULTADOS</h5></center>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
