<?php
require_once '../../core/usuario_sesion.php';
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();
require_once '../historial/Historial.class.php';
$oHist = new Historial();
require_once '../cartanotarial/Cartanotarial.class.php';
$oCartanotarial = new Cartanotarial();
require_once '../alerta/Alerta.class.php';
$oAlerta = new Alerta();
require_once '../demanda/Demanda.class.php';
$oDemanda = new Demanda();
require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';

$action = $_POST['action'];

if ($action == 'ver_proceso') {
	$data['html'] = armarFases($_POST['ejecucion_id'], $_POST['action2']);
	echo json_encode($data);
}
elseif ($action == 'actualizar_contenido') {
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	$ejecucionfase_reg = $oEjecucion->listar_ejecucionfase_por($ejecucionfase_id, '', '', 'ef.tb_ejecucionfase_orden DESC', '')['data'][0];
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];
	$data['html'] = armarContenido($ejecucion_reg, $ejecucionfase_reg, 'M_ejecucion');
	$data['estado'] = 1;
	$data['ejecucion_datos_actualizados'] = json_encode($ejecucion_reg);
	echo json_encode($data);
}
elseif ($action == 'actualizar_comentarios') {
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	$action2 = $_POST['action2'];
	$data['html'] = armarComentarios($ejecucionfase_id, $action2);
	$data['estado'] = 1;
	echo json_encode($data);
}
elseif ($action == 'comentario') {
	$action2 = $_POST['action2'];
	$action3 = $_POST['action3'];
	if ($action2 == 'insertar') {
		$data['estado'] = 0;
		$data['mensaje'] = 'Error en el registro de comentario. Pide ayuda a sistemas 🙌';

		$ejecucionfase_id = $_POST['ejecucionfase_id'];
		$new_comentario = $_POST['comentario'];
		
		$res_insertar_coment = $oEjecucion->insertar_ejecucionfase_comentario($ejecucionfase_id, $_SESSION['usuario_id'], $new_comentario);

		if ($res_insertar_coment['estado'] == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Se registró el comentario exitosamente';
			$data['html'] = armarComentarios($ejecucionfase_id, $action3);

			if ($_POST['vista'] == 'visto_bueno_carta' || $_POST['vista'] == 'visto_bueno_demanda') {
				$visto_bueno = $_POST['che_visto_bueno'];
				if ($visto_bueno == 'true') {
					//rellenar los campos de aprobado
						$oEjecucion->modificar_ejecucionfase_campo($ejecucionfase_id, 'tb_ejecucionfase_usuapr', $_SESSION['usuario_id'], 'INT', '');
						$oEjecucion->modificar_ejecucionfase_campo($ejecucionfase_id, 'tb_ejecucionfase_fecapr', date("Y-m-d H:i:s"), 'STR', '');
					//
				}
			}
		}
	}
	echo json_encode($data);
}
elseif ($action == 'sgte_fase') {
	$data['estado'] = 0;
	$ejecucionfase_id = $_POST['ejecucionfase_id'];

	//ejecucionfase actual colocar como completado
		$oEjecucion->modificar_ejecucionfase_campo($ejecucionfase_id, 'tb_ejecucionfase_completado', 1, 'INT', '');
	//

	$res_buscar_ejecucionfase = $oEjecucion->listar_ejecucionfase_por($ejecucionfase_id, '', '', 'ef.tb_ejecucionfase_orden DESC', '');
	if ($res_buscar_ejecucionfase['estado'] == 1) {
		$res_buscar_ejecucionfase = $res_buscar_ejecucionfase['data'][0];
		
		//verificar si tiene una sgte fase ya creada
			$oEjecucion->ejecucionfase_incluye_xac = 0;
			$res_sgtefase = $oEjecucion->listar_ejecucionfase_por('', $res_buscar_ejecucionfase['tb_ejecucion_id'], (intval($res_buscar_ejecucionfase['tb_ejecucionfase_orden'])+1), 'ef.tb_ejecucionfase_orden DESC', '');
			if ($res_sgtefase['estado'] != 1) {
				//creamos una nueva ejecucion fase
					$nueva_fase = $oEjecucion->insertar_ejecucionfase($res_buscar_ejecucionfase['tb_ejecucion_id'], $res_buscar_ejecucionfase['estadoejecucion_idnext'], (intval($res_buscar_ejecucionfase['tb_ejecucionfase_orden'])+1), '', $_SESSION['usuario_id']);
					if ($res_buscar_ejecucionfase['estado_next'] == 'TERMINADO') {
						$oEjecucion->modificar_ejecucionfase_campo($nueva_fase['nuevo'], 'tb_ejecucionfase_completado', 1, 'INT', '');
						$oEjecucion->modificar_campo($res_buscar_ejecucionfase['tb_ejecucion_id'], 'tb_ejecucion_estadoproceso', 3, 'INT');
					}
				//
			} else {
				//la siguiente fase que tenía xac 0 pasamos a xac 1
					$oEjecucion->modificar_ejecucionfase_campo($res_sgtefase['data'][0]['tb_ejecucionfase_id'], 'tb_ejecucionfase_xac', 1, 'INT', '');
				//
			}
			$oEjecucion->ejecucionfase_incluye_xac = 1;
		//

		//Modificar estadoejecucion_cgvsubtipo_id si no hay una sgte fase existente
			if ($res_sgtefase['estado'] != 1) {
				$oEjecucion->modificar_campo($res_buscar_ejecucionfase['tb_ejecucion_id'], 'tb_estadoejecucion_cgvsubtipo_id', $res_buscar_ejecucionfase['estadoejecucion_idnext'], 'INT');
			}
			//guardar historial que se avanzó a la sgte fase
				$oHist->setTbHistUsureg($_SESSION['usuario_id']);
				$oHist->setTbHistNomTabla('tb_ejecucionfase');
				$oHist->setTbHistRegmodid($ejecucionfase_id);
				$oHist->setTbHistDet("Avanzó a la siguiente fase en la ejecucion con id {$res_buscar_ejecucionfase['tb_ejecucion_id']} | <b>". date("d-m-Y h:i a").'</b>');
				$oHist->insertar();
			//
		//

		//si el estado actual es redaccion y no hay una sgte fase creada con xac 0, pasar cartas a revision
			if ($res_buscar_ejecucionfase['estado_actual'] == 'CARTAS NOTARIALES: REDACCION' && $res_sgtefase['estado'] != 1) {
				//listar todas las cartas de esa ejecucion
				$res_cartas = $oCartanotarial->listar_todos('', $res_buscar_ejecucionfase['tb_ejecucion_id'], '', '');
				if ($res_cartas['estado'] == 1) {
					foreach ($res_cartas['data'] as $key => $value) {
						//pasarlas a estado en revision
						$oCartanotarial->modificar_campo($value['tb_cartanotarial_id'], 'tb_cnestado_id', 2, 'INT', $_SESSION['usuario_id']);

						$oHist->setTbHistNomTabla('tb_cartanotarial');
						$oHist->setTbHistRegmodid($value['tb_cartanotarial_id']);
						$oHist->setTbHistDet("Pasó la carta a estado de revisión/V°B° | <b>". date("d-m-Y h:i a").'</b>');
						$oHist->insertar();
					}
				}
				unset($res_cartas);
			}
		//si el estado actual es revision, pasar cartas a todo ok
			elseif ($res_buscar_ejecucionfase['estado_actual'] == 'CARTAS NOTARIALES: REVISION/CORRECCIONES') {
				//listar todas las cartas de esa ejecucion
				$res_cartas = $oCartanotarial->listar_todos('', $res_buscar_ejecucionfase['tb_ejecucion_id'], '', '');
				if ($res_cartas['estado'] == 1) {
					foreach ($res_cartas['data'] as $key => $value) {
						//pasarlas a estado en revision
						$oCartanotarial->modificar_campo($value['tb_cartanotarial_id'], 'tb_cnestado_id', 3, 'INT', $_SESSION['usuario_id']);

						$oHist->setTbHistNomTabla('tb_cartanotarial');
						$oHist->setTbHistRegmodid($value['tb_cartanotarial_id']);
						$oHist->setTbHistDet("Carta revisada. Todo Ok. Lista para llevar a notaria | <b>". date("d-m-Y h:i a").'</b>');
						$oHist->insertar();
					}
				}
				unset($res_cartas);
			}
		//si el estado actual es llevar a notaria, pasar cartas a estado diligencia
			elseif ($res_buscar_ejecucionfase['estado_actual'] == 'CARTAS NOTARIALES: LLEVAR A NOTARIA PARA DILIGENCIA') {
				//listar todas las cartas de esa ejecucion
				$res_cartas = $oCartanotarial->listar_todos('', $res_buscar_ejecucionfase['tb_ejecucion_id'], '', '');
				if ($res_cartas['estado'] == 1) {
					foreach ($res_cartas['data'] as $key => $value) {
						//pasarlas a estado en diligencia
						$oCartanotarial->modificar_campo($value['tb_cartanotarial_id'], 'tb_cnestado_id', 5, 'INT', $_SESSION['usuario_id']);

						$oHist->setTbHistNomTabla('tb_cartanotarial');
						$oHist->setTbHistRegmodid($value['tb_cartanotarial_id']);
						$oHist->setTbHistDet("Carta llevada a notaría. Estado en Diligencia | <b>". date("d-m-Y h:i a").'</b>');
						$oHist->insertar();
					}
				}
				unset($res_cartas);
			}
		//si el estado actual es diligencia, pasar cartas a estado diligenciada recogida
			elseif ($res_buscar_ejecucionfase['estado_actual'] == 'CARTAS NOTARIALES: DILIGENCIA') {
				//listar todas las cartas de esa ejecucion
				$res_cartas = $oCartanotarial->listar_todos('', $res_buscar_ejecucionfase['tb_ejecucion_id'], '', '');
				if ($res_cartas['estado'] == 1) {
					foreach ($res_cartas['data'] as $key => $value) {
						//pasarlas a estado en diligencia
						$oCartanotarial->modificar_campo($value['tb_cartanotarial_id'], 'tb_cnestado_id', 6, 'INT', $_SESSION['usuario_id']);

						$oHist->setTbHistNomTabla('tb_cartanotarial');
						$oHist->setTbHistRegmodid($value['tb_cartanotarial_id']);
						$oHist->setTbHistDet("Carta diligenciada y recogida | <b>". date("d-m-Y h:i a").'</b>');
						$oHist->insertar();
					}
				}
				unset($res_cartas);
			}
		//
		$data['estado'] = 1;
		$data['mensaje'] = "AVANZÓ A LA SIGUIENTE FASE";
		$data['html'] = armarFases($res_buscar_ejecucionfase['tb_ejecucion_id'], 'M_ejecucion');
	}
	unset ($res_buscar_ejecucionfase);

	echo json_encode($data);
}
elseif ($action == 'guardar_montoliquidacion_representante') {
	$data['estado'] = 0;
	$data['mensaje'] = 'Error al guardar los datos';

	$ejecucion_id = $_POST['ejecucion_id'];
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	$moneda_id = $_POST['cbo_moneda_pagar'];
	$moneda_nom = $_POST['moneda_nom'];
	$monto_liquidacion = $_POST['txt_monto_liquidacion_cn_redaccion'];
	$representante_ipdn = $_POST['cbo_ipdn_rep_cn_redaccion'];
	$representante_ipdn_nom = $_POST['rep_nom'];
	$cargo_repre_ipdn = $_POST['cargo_repre_id'];

	$res_monto = $oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_montoliquidacion', moneda_mysql($monto_liquidacion), 'STR');
	$res_repre_id = $oEjecucion->modificar_campo($ejecucion_id, 'tb_persona_id', $representante_ipdn, 'INT');
	$res_cargo_id = $oEjecucion->modificar_campo($ejecucion_id, 'tb_cargo_id', $cargo_repre_ipdn, 'INT');

	if ($res_monto > 0 && $res_repre_id > 0 && $res_cargo_id > 0) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Se registraron los datos correctamente';

		//historial
		$oHist->setTbHistUsureg($_SESSION['usuario_id']);
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->setTbHistDet("Guardó el monto de liquidacion ($moneda_nom $monto_liquidacion) y representante IPDN ($representante_ipdn_nom), de la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
		$oHist->insertar();
	}
	echo json_encode($data);
}
elseif ($action == 'guardar_datos_demanda_mesapartes') {
	$data['estado'] = 0;
	$data['mensaje'] = 'Error al guardar los datos';
	
	$ejecucion_id = $_POST['ejecucion_id'];
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	
	$nro_expediente = trim($_POST['txt_ejecucion_nroexpediente']);
	$especialista = strtoupper(trim($_POST['txt_ejecucion_especialista']));
	$juez = strtoupper(trim($_POST['txt_ejecucion_nombrejuez']));
	$juzgado = $_POST['cbo_ejecucion_juzgado'];
	$fecha_pres = $_POST['txt_ejecucion_demanda_fec_pres'];
	$date = DateTime::createFromFormat('g:i A', $_POST['txt_ejecucion_demanda_hora_pres']);
	//$hora_pres = $date->format('H:i:s');
	$hora_pres = '00:00:00';
	
	$res_1 = $oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_expediente', $nro_expediente, 'STR');
	$res_2 = $oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_especialista', $especialista, 'STR');
	$res_3 = $oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_nombrejuez', $juez, 'STR');
	$res_4 = $oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_juzgado', $juzgado, 'INT');
	$res_5 = $oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_demanda_fechora_presentacion', fecha_mysql($fecha_pres).' '.$hora_pres, 'STR');

	if ($res_1 > 0 && $res_2 > 0 && $res_3 > 0 && $res_4 > 0 && $res_5 > 0) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Se registraron los datos correctamente';

		//historial
		$oHist->setTbHistUsureg($_SESSION['usuario_id']);
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->setTbHistDet("Guardó los datos asignados en mesa de partes del poder judicial en la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
		$oHist->insertar();
	}
	echo json_encode($data);
}
elseif ($action == 'modificar_campo') {
	$tabla = $_POST['tabla'];
	$columna = $_POST['columna'];
	$valor = $_POST['valor'];
	$tipo_dato = $_POST['tipo_dato'];
	$ejecucion_id = $_POST['ejecucion_id'];
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	if ($columna == 'tb_ejecucion_oficioincau_fecrecojo' || $columna == 'tb_ejecucion_oficiodescer_fecrecojo' || $columna == 'tb_ejecucion_oficiolevanincau_fecrecojo') {
		$valor_orig = $valor;
		$fecha_hora = explode(' ', $valor);
		$fecha = fecha_mysql($fecha_hora[0]);
		$date = DateTime::createFromFormat('g:i A', $fecha_hora[1] .' '. $fecha_hora[2]);
		$hora = $date->format('H:i:s');
		$valor = $fecha.' '.$hora;
	}
	elseif (strpos($columna, 'fec') > -1) {
		$valor = fecha_mysql($valor);
	}

	if ($tabla == 'tb_ejecucion') {
		$res = $oEjecucion->modificar_campo($ejecucion_id, $columna, $valor, $tipo_dato);

		if ($res == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = "Éxito en la operación";

			$oHist->setTbHistUsureg($_SESSION['usuario_id']);
			$oHist->setTbHistNomTabla('tb_ejecucionfase');
			$oHist->setTbHistRegmodid($ejecucionfase_id);
			if ($columna == 'tb_ejecucion_revisionpersona_id') {
				$oHist->setTbHistDet("Guardó la persona que dará V° B° en las redacciones ({$_POST['persona_vb_nom']}), de la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			elseif ($columna == 'tb_ejecucion_fecmax_espera') {
				$oHist->setTbHistDet("Asignó la fecha máxima (<b>".mostrar_fecha($valor)."</b>) de esperar contacto del cliente, en la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
			} elseif ($columna == 'tb_ejecucion_estadoproceso' && $valor == 0) {
				$oHist->setTbHistDet("Suspendió la ejecución con id $ejecucion_id. | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			elseif ($columna == 'tb_ejecucion_estadoproceso' && $valor == 1) {
				$oHist->setTbHistDet("Reactivó la ejecución con id $ejecucion_id. | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			elseif ($columna == 'tb_ejecucion_oficioincau_fecrecojo') {
				$oHist->setTbHistDet("Asignó la fecha de recojo de oficio de incautación (<b>$valor_orig</b>), en la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			elseif ($columna == 'tb_ejecucion_oficiodescer_fecrecojo') {
				$oHist->setTbHistDet("Asignó la fecha de recojo de oficio de descerraje (<b>$valor_orig</b>), en la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			elseif ($columna == 'tb_ejecucion_oficiolevanincau_fecrecojo') {
				$oHist->setTbHistDet("Asignó la fecha de recojo de oficio de Lev. Orden Captura (<b>$valor_orig</b>), en la ejecucion con id $ejecucion_id | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			elseif ($columna == 'tb_ejecucion_usuencargado') {
				$ejecucion_reg_origin = (array) json_decode($_POST['hdd_ejecucion_reg']);
				$oHist->setTbHistDet("Cambió el usuario encargado de esta ejecucion id $ejecucion_id: {$ejecucion_reg_origin['tb_usuario_nom']} {$ejecucion_reg_origin['tb_usuario_ape']} => <b>{$_POST['usuarionuevo_nombre']}</b> | <b>" . date("d-m-Y h:i a") . '</b>');
			}
			$oHist->insertar();
		} else {
			$data['mensaje'] = "Error";
		}
	}
	elseif ($tabla == 'tb_cartanotarial') {
		if ($columna == 'tb_cartanotarial_fecnotariarecepciona') {
			$mensaje_hist = "Confirmó la fecha en que llevará las cartas a notaría, cartas de la ejecucion con id $ejecucion_id. Fecha <b>$valor</b>";
		} elseif ($columna == 'tb_cartanotarial_fecrecojo') {
			$mensaje_hist = "Guardó la fecha en que debe recoger cartas de notaría, cartas de la ejecucion con id $ejecucion_id. Fecha <b>$valor</b>";
		}

		$res = $oCartanotarial->listar_todos('', $ejecucion_id, '', '');
		if ($res['estado'] == 1) {
			foreach ($res['data'] as $key => $carta) {
				$oCartanotarial->modificar_campo($carta['tb_cartanotarial_id'], $columna, $valor, $tipo_dato, $_SESSION['usuario_id']);
			}

			$data['estado'] = 1;
			$data['mensaje'] = "Éxito en la operación";

			$oHist->setTbHistUsureg($_SESSION['usuario_id']);
			$oHist->setTbHistNomTabla('tb_ejecucionfase');
			$oHist->setTbHistRegmodid($ejecucionfase_id);
			$oHist->setTbHistDet("$mensaje_hist | <b>" . date("d-m-Y h:i a") . '</b>');
			$oHist->insertar();
		}
	}
	elseif ($tabla == 'tb_ejecucionfasefile') {
		if ($columna == 'tb_ejecucionfasefile_incluir_redac') {
			$valor = $valor == 'true' ? 1 : 0;
		}
		$ejecucionfasefile_id = $_POST['tabla_id'];
		$res = $oEjecucionfasefile->modificar_campo($ejecucionfasefile_id, $columna, $valor, $tipo_dato);

		if ($res == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Modificado correctamente';
		}
	}
	elseif ($tabla == 'tb_procesofase_comentario') {
		$res = $oEjecucion->comentario_modificar_campo($_POST['comentario_id'], $columna, $valor, $tipo_dato);
		if ($res == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = "Éxito en la operación";
		} else {
			$data['mensaje'] = "Error";
		}
	}
	unset($res);
	echo json_encode($data);
}
elseif ($action == 'fase_anterior') {
	$ejecucion_id		= $_POST['ejecucion_id'];
	$ejecucionfase_id	= $_POST['ejecucionfase_id'];
	$ejecucion_reg		= $oEjecucion->listar_ejecucionfase_por($ejecucionfase_id, '', '', 'ef.tb_ejecucionfase_orden DESC', '');

	if ($ejecucion_reg['estado'] == 1) {
		$ejecucion_reg			= $ejecucion_reg['data'][0];
		$faseactual_orden		= $ejecucion_reg['tb_ejecucionfase_orden'];
		$ejecucion_anterior_reg	= $oEjecucion->listar_ejecucionfase_por('', $ejecucion_id, ($faseactual_orden-1), 'ef.tb_ejecucionfase_orden DESC', '');
		
		if ($ejecucion_anterior_reg['estado'] == 1) {
			$ejecucion_anterior_reg = $ejecucion_anterior_reg['data'][0];
			
			//a la fase anterior se le cambia el campo completado de 1 a 0
				$oEjecucion->modificar_ejecucionfase_campo($ejecucion_anterior_reg['tb_ejecucionfase_id'], 'tb_ejecucionfase_completado', 0, 'INT', '');
			//

			//modificar la ejecucion actualizar el estadoejecucion_cgvsubtipo si no está en una fase de correccion
				if (strpos($ejecucion_reg['estado_actual'], "CORRECCION") === false) {
					$oEjecucion->modificar_campo($ejecucion_id, 'tb_estadoejecucion_cgvsubtipo_id', $ejecucion_anterior_reg['tb_estadoejecucion_cgvsubtipo_id'], 'INT');
				}

				$oHist->setTbHistUsureg($_SESSION['usuario_id']);
				$oHist->setTbHistNomTabla('tb_ejecucionfase');
				$oHist->setTbHistRegmodid($ejecucionfase_id);
				$oHist->setTbHistDet("Regresó a la fase anterior en la ejecucion con id $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
				$oHist->insertar();
			//

			//condicionar si esta en la revision de cartas
			if ($ejecucion_reg['estado_actual'] == 'CARTAS NOTARIALES: REVISION/CORRECCIONES') {
				//pasar todas las cartas de esa ejecucion a estado de redaccion nuevamente
				$res_cartas = $oCartanotarial->listar_todos('', $ejecucion_id, '', '');
				if ($res_cartas['estado'] == 1) {
					foreach ($res_cartas['data'] as $key => $value) {
						//estado 4: revisada en correccion
						$oCartanotarial->modificar_campo($value['tb_cartanotarial_id'], 'tb_cnestado_id', 4, 'INT', $_SESSION['usuario_id']);

						$oHist->setTbHistNomTabla('tb_cartanotarial');
						$oHist->setTbHistRegmodid($value['tb_cartanotarial_id']);
						$oHist->setTbHistDet("Carta revisada. En correcciones | <b>". date("d-m-Y h:i a").'</b>');
						$oHist->insertar();
					}
				}
				unset($res_cartas);
			}
			
			//modificar la ejecucionfase actual su xac a 0
				$oEjecucion->modificar_ejecucionfase_campo($ejecucionfase_id, 'tb_ejecucionfase_xac', 0, 'INT', '');
			//
			$data['estado'] = 1;
			$data['mensaje'] = 'Se regresó a la fase anterior';
		}
		unset($ejecucion_anterior_reg);
	}
	unset($ejecucion_reg);
	echo json_encode($data);
}
elseif ($action == 'actualizar_vista_fases') {
	$ejecucion_id = $_POST['ejecucion_id'];
	$data['html'] = armar_timeline_fases($ejecucion_id, 'M_ejecucion');
	$data['ejecucion_reg'] = json_encode($oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0]);
	echo json_encode($data);
}
elseif ($action == 'buscar_documento') {
	$nombre_doc = $_POST['nombre_doc'];
	$ejecucion_id = $_POST['ejecucion_id'];
	$ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];
	echo json_encode(buscarUrlDocumento($ejecucion_reg, $nombre_doc));
}
elseif ($action == 'verificar_conteo_dias') {
	$ejecucion_id = $_POST['ejecucion_id'];
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	$estado_ejecucion_id = $_POST['estadoejecucion_id'];
	$data['estado'] = 0;

	if ($estado_ejecucion_id == 11) {
		$res = $oEjecucionfasefile->listar('', $ejecucionfase_id, '', 'not null', 1, '', '', '');
		if ($res['estado'] != 1) {
			$res_ejecucion = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];
			
			$demanda_fec_pres = $res_ejecucion['tb_ejecucion_demanda_fechora_presentacion'];
			$dias_pasaron = restaFechas(fecha_mysql($demanda_fec_pres), date('Y-m-d'));

			$data['estado'] = 1;
			$data['mensaje'] = "<b>Revise la página de CONSULTA DE EXPEDIENTES JUDICIALES</b>. Han pasado <b>$dias_pasaron días</b> desde la presentacion de la demanda en MP del Poder Judicial (".mostrar_fecha($demanda_fec_pres).")";
		} else {
			$ultimo_estado = '';
			foreach ($res['data'] as $key => $documento) {
				if ($documento['tb_ejecucionfasefile_resol_est'] == 2) {
					$ultimo_estado = 'inadmisible';
				}
				if ($documento['tb_ejecucionfasefile_resol_est'] == 1) {
					$ultimo_estado = 'completo';
					$data['mensaje'] = '';
					break;
				}
			}

			if ($ultimo_estado == 'inadmisible') {
				$data['mensaje'] = "La resolución del poder judicial fue ".strtoupper($ultimo_estado);

				//revisar si ya se hizo la presentacion de subsanacion
				$falta_escrito_subsanacion = 1;
				$res1 = $oEjecucionfasefile->listar('', $ejecucionfase_id, '', 'not null', 2, '', 1, '');
				if ($res1['estado'] == 1) {
					$falta_escrito_subsanacion = 0;

					//realizar el conteo de dias sin respuesta al escrito de subsanacion
					$escrito_fec_pres = !empty($res1['data'][0]['tb_ejecucionfasefile_fecnoti']) ? $res1['data'][0]['tb_ejecucionfasefile_fecnoti'] : $res1['data'][0]['upload_reg'];

					$dias_pasaron = restaFechas(fecha_mysql($escrito_fec_pres), date('Y-m-d'));
					$data['mensaje'] = "<b>Revise la página de CONSULTA DE EXPEDIENTES JUDICIALES</b>. Han pasado <b>$dias_pasaron días</b> desde la subida del {$res1['data'][0]['tb_ejecucionfasefile_archivonom']} (".mostrar_fecha($escrito_fec_pres).")";
				}

				if ($falta_escrito_subsanacion == 1) {
					$data['mensaje'] .= '. Falta la redaccion del escrito de subsanación';
				}
				
				unset($res1);
			}
			$data['estado'] = 1;
		}
	}
	elseif ($estado_ejecucion_id == 13) {
		//el conteo de dias en estadoejecucion 13 solo se hace si sube escrito descerraje. lo buscamos
		$res_escrito_desce = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 2, '', 2, '');
		if ($res_escrito_desce['estado'] == 1) {
			//si está subido un pdf de escrito descerraje entonces busquemos una resolucion de respuesta al descerraje(o sea dentro de esta ejecucionfase)
			$resolucion_respuesta = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 1, '', '', '');
			if ($resolucion_respuesta['estado'] != 1) {
				//buscar cuándo se subio el escrito descerraje
				$escrito_desce_fec_upload = $res_escrito_desce['data'][0]['tb_ejecucionfasefile_fecnoti'];
				$dias_pasaron = restaFechas(fecha_mysql($escrito_desce_fec_upload), date('Y-m-d'));
	
				$data['estado'] = 1;
				$data['mensaje'] = "<b>Revise la página de CONSULTA DE EXPEDIENTES JUDICIALES</b>. Han pasado <b>$dias_pasaron días</b> desde la subida de {$res_escrito_desce['data'][0]['tb_ejecucionfasefile_archivonom']} (".mostrar_fecha($escrito_desce_fec_upload).")";
			} else {
				$ultimo_estado = '';
				foreach ($resolucion_respuesta['data'] as $key => $documento) {
					if ($documento['tb_ejecucionfasefile_resol_est'] == 2) {
						$ultimo_estado = 'inadmisible';
					}
					if ($documento['tb_ejecucionfasefile_resol_est'] == 1) {
						$ultimo_estado = 'completo';
						$data['mensaje'] = '';
						break;
					}
				}

				if ($ultimo_estado == 'inadmisible') {
					$data['mensaje'] = "La resolución del poder judicial fue ".strtoupper($ultimo_estado);

					//revisar si ya se hizo la presentacion de subsanacion
					$falta_escrito_subsanacion = 1;
					$res1 = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 2, '', 1, '');
					if ($res1['estado'] == 1) {
						$falta_escrito_subsanacion = 0;

						//realizar el conteo de dias sin respuesta al escrito de subsanacion
						$escrito_fec_pres = !empty($res1['data'][0]['tb_ejecucionfasefile_fecnoti']) ? $res1['data'][0]['tb_ejecucionfasefile_fecnoti'] : $res1['data'][0]['upload_reg'];

						$dias_pasaron = restaFechas(fecha_mysql($escrito_fec_pres), date('Y-m-d'));
						$data['mensaje'] = "<b>Revise la página de CONSULTA DE EXPEDIENTES JUDICIALES</b>. Han pasado <b>$dias_pasaron días</b> desde la subida de {$res1['data'][0]['tb_ejecucionfasefile_archivonom']} (".mostrar_fecha($escrito_fec_pres).")";
					}

					if ($falta_escrito_subsanacion == 1) {
						$data['mensaje'] .= '. Falta la redaccion del escrito de subsanación';
					}
					
					unset($res1);
				}
				$data['estado'] = 1;
			}
		}
	}
	elseif ($estado_ejecucion_id == 14) {
		//buscar si ya se presentó el escrito de lev. ord. incau.
		$res_esc_lev_ord_incau = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 2, '', 4, '');
		if ($res_esc_lev_ord_incau['estado'] == 1) {
			//si está subido un pdf de escrito de lev. ord. incau. entonces busquemos una resolucion de respuesta al escrito(o sea dentro de esta ejecucionfase)
			$resolucion_respuesta = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 1, '', '', '');
			if ($resolucion_respuesta['estado'] != 1) {
				//buscar cuándo se subio el escrito de lev. ord. incau.
				$escrito_lev_orden_incau_fec_upload = $res_esc_lev_ord_incau['data'][0]['tb_ejecucionfasefile_fecnoti'];
				$dias_pasaron = restaFechas(fecha_mysql($escrito_lev_orden_incau_fec_upload), date('Y-m-d'));
	
				$data['estado'] = 1;
				$data['mensaje'] = "<b>Revise la página de CONSULTA DE EXPEDIENTES JUDICIALES</b>. Han pasado <b>$dias_pasaron días</b> desde la subida de {$res_esc_lev_ord_incau['data'][0]['tb_ejecucionfasefile_archivonom']} (".mostrar_fecha($escrito_lev_orden_incau_fec_upload).")";
			} else {
				$ultimo_estado = '';
				foreach ($resolucion_respuesta['data'] as $key => $documento) {
					if ($documento['tb_ejecucionfasefile_resol_est'] == 2) {
						$ultimo_estado = 'inadmisible';
					}
					if ($documento['tb_ejecucionfasefile_resol_est'] == 1) {
						$ultimo_estado = 'completo';
						$data['mensaje'] = '';
						break;
					}
				}

				if ($ultimo_estado == 'inadmisible') {
					$data['mensaje'] = "La resolución del poder judicial fue ".strtoupper($ultimo_estado);

					//revisar si ya se hizo la presentacion de subsanacion
					$falta_escrito_subsanacion = 1;
					$res1 = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 2, '', 1, '');
					if ($res1['estado'] == 1) {
						$falta_escrito_subsanacion = 0;

						//realizar el conteo de dias sin respuesta al escrito de subsanacion
						$escrito_fec_pres = !empty($res1['data'][0]['tb_ejecucionfasefile_fecnoti']) ? $res1['data'][0]['tb_ejecucionfasefile_fecnoti'] : $res1['data'][0]['upload_reg'];

						$dias_pasaron = restaFechas(fecha_mysql($escrito_fec_pres), date('Y-m-d'));
						$data['mensaje'] = "<b>Revise la página de CONSULTA DE EXPEDIENTES JUDICIALES</b>. Han pasado <b>$dias_pasaron días</b> desde la subida de {$res1['data'][0]['tb_ejecucionfasefile_archivonom']} (".mostrar_fecha($escrito_fec_pres).")";
					}

					if ($falta_escrito_subsanacion == 1) {
						$data['mensaje'] .= '. Falta la redaccion del escrito de subsanación';
					}
					
					unset($res1);
				}
				$data['estado'] = 1;
			}
		} else {
			//buscar desde cuando se creó esa fase
			$res_ejecucionfase = $oEjecucion->listar_ejecucionfase_por($ejecucionfase_id, $ejecucion_id, '', '', '');
			if ($res_ejecucionfase['estado'] == 1) {
				$ejecucionfase_fec_creacion = $res_ejecucionfase['data'][0]['tb_ejecucionfase_fecreg'];
				$dias_pasaron = restaFechas(fecha_mysql($ejecucionfase_fec_creacion), date('Y-m-d'));
	
				$data['estado'] = 1;
				$data['mensaje'] = "<b>Cree el Escrito de Levantamiento Orden Incautacion</b>. Han pasado <b>$dias_pasaron días</b> desde la creacion de esta fase. (".mostrar_fecha($ejecucionfase_fec_creacion).")";
			} else {
				$data['estado'] = 0;
				$data['mensaje'] = "<b>No se ha encontrado la ejecucion fase con id $ejecucionfase_id</b>.";
			}
		}
	}

	unset($res);
	echo json_encode($data);
}
elseif ($action == 'verificar_credito_suministro') {
	$data['estado'] = 0;
	$data['mensaje'] = 'No se pudo verificar';
	$credito_id = $_POST['credito_id'];
	require_once '../creditogarveh/Creditogarveh.class.php';
	$oCredito = new Creditogarveh();
	$res1 = $oCredito->mostrarUno($credito_id);

	if (empty($res1['data']['tb_credito_suministro'])) {
		require_once '../usuario/Usuario.class.php';
		$oUsuario = new Usuario();
		$res3 = $oUsuario->mostrarUno($res1['data']['tb_credito_usureg']);
		$data['mensaje'] = '<b>CGV-'.$credito_id.' sin n° suministro</b>. Actualizar en <b>Checklist modulo Credito Garveh</b>. Usuario responsable: '.$res3['data']['tb_usuario_nom'] . ' ' . $res3['data']['tb_usuario_ape'];
	} else {
		$data['estado'] = 1;
		$data['mensaje'] = 'el credito sí tiene número de suministro configurado';
	}

	echo json_encode($data);
}
elseif ($action == 'refresh_vista_galeria') {
	$tabla_nom = $_POST['tabla_nom'];
	$tabla_id = $_POST['tabla_id'];
	$tabla_id2 = $_POST['tabla_id2'];
	$modulo_subnom_orig = $_POST['modulo_subnom'];
	$modulo_subnom = $modulo_subnom_orig == 'comentario_galeria' ? $modulo_subnom_orig.$tabla_id2 : $modulo_subnom_orig;

	$action2 = $_POST['action2'];
	$ejecucionfase_completado = $_POST['ejecucionfase_completado'];

	require_once '../filestorage/Filestorage.class.php';
	$oFilestorage = new Filestorage();

	$res = $oFilestorage->listar_files_modulo_submodulo($tabla_nom, $tabla_id, $modulo_subnom);
	$data['html'] = '';

	if ($res['estado'] == 1) {
		$data['estado'] = 1;

		foreach ($res['data'] as $key => $value) {
			$data['html'] .=
				'<div class="col-xs-4 col-sm-4" style="text-align: center;" id="filestorage_id_'.$value['filestorage_id'].'">'.
				'<img class="img-responsive garantia_img" src="'.$value['filestorage_url'].'" alt="Photo" style="height:73px; width:120px; cursor:pointer;" onclick="carousel(\''.$tabla_nom.'\', '.$tabla_id.', \''.$modulo_subnom_orig.'\', '. $tabla_id2.')">';
				if(($_SESSION['usuario_id'] == 42 || $_SESSION['usuarioperfil_id'] == 1 || $val1['tb_usuario_id'] == $_SESSION['usuario_id']) && ($action2 != 'L_ejecucion' && $ejecucionfase_completado != 1)) {
					$data['html'] .= '<a class="users-list-name" href="javascript:void(0)" onclick="filestorage_eliminar('.$value['filestorage_id']. ", '$tabla_nom', $tabla_id, '$modulo_subnom_orig', $tabla_id2". ')">Eliminar</a><br>';
				}
			$data['html'] .= '</div>';
		}
	} else {
		$data['estado'] = 0;
		$data['mensaje'] = 'No se encontraron archivos';
	}
	echo json_encode($data);
}
else {
	$data['estado'] = 0;
	$data['mensaje'] = 'accion no encontrada';
	echo json_encode($data);
}

function armarFases($ejecucion_id, $action2) {
	global $oEjecucion;
	$buscar_ejecucion = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];

	$cliente_concat = "{$buscar_ejecucion['tb_cliente_doc']} - {$buscar_ejecucion['tb_cliente_nom']}";
	if (intval($buscar_ejecucion['tb_cliente_tip']) == 2) {
		$cliente_concat .= ", {$buscar_ejecucion['tb_cliente_empruc']} - {$buscar_ejecucion['tb_cliente_emprs']}";
	}

	$html_retorno =
		'<section id="ejecucion_fase" class="content" style="margin-top: 121px;">'.
			'<div class="box">'.
				'<div id="all_fases" class="box-body">'.
					'<input type="hidden" name="hdd_ejecucion_reg" id="hdd_ejecucion_reg" value=\''. json_encode($buscar_ejecucion) .'\'>'.
					'<input type="hidden" name="hdd_ejecucion_action2" id="hdd_ejecucion_action2" value="'.$action2.'">'.
					'<input type="hidden" name="ejecucion_id" id="ejecucion_id" value="'. $ejecucion_id .'">'.
					'<input type="hidden" name="usu_encargado_id" id="usu_encargado_id" value="'. $buscar_ejecucion['tb_ejecucion_usuencargado'] .'">'.
					'<input type="hidden" name="usu_encargado_perfil_id" id="usu_encargado_perfil_id" value="'. $buscar_ejecucion['tb_usuarioperfil_id'] .'">'.
					'<input type="hidden" name="hdd_ejecucion_clienteconcat" id="hdd_ejecucion_clienteconcat" value="'. $cliente_concat .'">'.
					'<!--- MESAJES DE CARGANDO -->'.
					'<div class="callout callout-info" id="ejecucion_mensaje_proceso" style="display: none;">'.
						'<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>'.
					'</div>'.
					'<div class="row">'.
						'<!--- CONTENIDO -->'.
						'<ul id="timeline_fases" class="timeline">'.
							armar_timeline_fases($ejecucion_id, $action2) .
						'</ul>'.
					'</div>'.
					'<script type="text/javascript" src="vista/ejecucion/ejecucionproceso.js?ver='.rand().'"></script>'.
				'</div>'.
				'<div id="div_modal_escrpubl_form"></div>'.
				'<div id="div_modal_listadocertificados_form"></div>'.
				'<div id="div_modal_certificadoregveh_form"></div>'.
				'<div id="div_modal_demanda_form"></div>'.
				'<div id="div_modal_demanda_gasto_form"></div>'.
				'<div id="div_modal_listadocartas_form"></div>'.
				'<div id="div_modal_carta_form"></div>'.
				'<div id="div_modal_listadoejecucionfasefiles_form"></div>'.
				'<div id="div_modal_pago_form"></div>'.
				'<div id="div_modal_galeriacomentario_form"></div>'.
				'<div id="div_modal_filestorage_form"></div>'.
				//estos div deben ir al último porque son de subir y ver archivos
				'<div id="div_modal_ejecucionfile_form"></div>'.
				'<div id="div_modal_ejecucion_verpdf_form"></div>'.
				'<div id="div_modal_carousel"></div>'.
				//
			'</div>'.
		'</section>';
	//

	unset($buscar_ejecucion);
	return $html_retorno;
}

function armar_timeline_fases($ejecucion_id, $action2) {
	global $oEjecucion;
	$proceso_all_fases = '';
	$buscar_ejecucion = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];

	//aqui modificar añadir si se desea listar los que tienen xac 0 tendrán una vista diferente
	$oEjecucion->ejecucionfase_incluye_xac = 0;
	$res = $oEjecucion->listar_ejecucionfase_por('', $ejecucion_id, '', 'ef.tb_ejecucionfase_orden DESC', '');
	if ($res['estado'] == 1) {
		foreach ($res['data'] as $key => $val) {
			$ejecucionfase_id = $val['tb_ejecucionfase_id'];
			$comentar = '';
			$title_add_comentarios = '';

			if ($val['tb_ejecucionfase_xac'] == 1) {
				$icon_historial = $val['tb_ejecucionfase_completado'] == 1 ? 'fa-check bg-green' : 'fa-arrow-right bg-yellow';
				
				$proceso_all_fases .=
				'<li style="margin-bottom: 0px">
					<i class="fa '. $icon_historial .'"></i>
					<div class="timeline-item">
						<div class="box box-primary shadow" style="padding: 5px 10px;">
							<div class="box-header with-border" style="padding: 5px 3px;">
								<h3 class="box-title" style="font-size: 25px;">'. $val['estado_actual'] .'</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" style="padding-top: 0px;" data-widget="collapse"><i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
			
							<div class="box-body" style="padding: 5px 5px;">
								<ul style="padding: 0px;" class="products-list product-list-in-box">
									<li class="item" id="contenido_faseid_'.$ejecucionfase_id.'">'.
										armarContenido($buscar_ejecucion, $val, $action2).
									'</li>
								</ul>
							</div>
						</div>
					</div>
				</li>';

				if ($val['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
					//si no está completado y estan editando la ejecucion
					$comentar = '<br>'.
						'<div class="row">'.
							'<div class="col-md-12">'.
								'<textarea id="new_comentario_'.$ejecucionfase_id.'" name="new_comentario_'.$ejecucionfase_id.'" class="form-control input-sm input-shadow" rows="3" placeholder="Inserte un comentario"></textarea>'.
							'</div>'.
						'</div><br>'.
						'<div align="right" class="col-md-12">
							<div class="form-group" style="margin-bottom: 0px;">
								<button type="button" class="btn btn-primary" name="btn_guardar_comentario" id="btn_guardar_comentario" onclick="registrarComentario('.$ejecucionfase_id.', \''.$action2.'\')"><strong>Comentar</strong></button>
							</div>
						</div>';
					//
				}
			} else {
				$title_add_comentarios = ' DE LA FASE '.$val['estado_actual'];
			}

			$comentarios_anteriores = armarComentarios($ejecucionfase_id, $action2);
			if ($comentarios_anteriores != '(no hay comentarios registrados)' || ($val['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion')) {
				//lista de comentarios anteriores y caja para nuevo comentario
				$proceso_all_fases .=
				'<li style="margin-bottom: 20px">
					<div class="timeline-item">
						<div class="box box-primary shadow" style="padding: 5px 10px;">
							<div class="box-header with-border" style="padding: 5px 3px;">
								<h3 class="box-title" style="font-size: 20px;">COMENTARIOS'.$title_add_comentarios.'</h3>
							</div>
			
							<div class="box-body" style="padding: 5px 5px;">
								<ul style="padding: 0px;" class="products-list product-list-in-box">
									<li class="item">'.
										'<div class="row">'.
											'<div id="comentarios_anteriores_'.$ejecucionfase_id.'" class="col-md-12">'.
												$comentarios_anteriores.
											'</div>'.
										'</div>'.
										$comentar.
									'</li>
								</ul>
							</div>
						</div>
					</div>
				</li>';
			}
		}
	}
	unset($res);
	$oEjecucion->ejecucionfase_incluye_xac = 1;
	return $proceso_all_fases;
}

function armarContenido($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$contenido_fase = '';
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];

	$contenido_fase = mostrarUltimoHistorialFase($ejecucionfase_id);

	$datos_rellenados_para_sgte_paso = 0; $mensaje_btn_next = 'SIGUIENTE PASO';
	//contenido de la fase
	if ($ejecucionfase_reg['estado_actual'] == 'ASIGNADO A LEGAL') {
		$datos_rellenados_para_sgte_paso = 1;
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'CARTAS NOTARIALES: REDACCION') {
		$contenido_fase .= contenidoCartaNotarialRedaccion($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, '', 'btn_next');
	}
	elseif($ejecucionfase_reg['estado_actual'] == 'CARTAS NOTARIALES: REVISION/CORRECCIONES') {
		$contenido_fase .= contenidoCartaNotarialRevision($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, '');
	}
	elseif($ejecucionfase_reg['estado_actual'] == 'CARTAS NOTARIALES: LLEVAR A NOTARIA PARA DILIGENCIA') {
		$contenido_fase .= contenidoCartaNotarialLLevarNotaria($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, '');
		$mensaje_btn_next = '✔ CARTA LLEVADA';
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'CARTAS NOTARIALES: DILIGENCIA') {
		$contenido_fase .= contenidoCartaNotarialDiligencia($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, '');
		$mensaje_btn_next = '✔ CARTA RECOGIDA';
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'ESPERANDO CONTACTO CLIENTE') {
		$contenido_fase .= contenidoEsperaContactoCliente($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'DEMANDA: REDACCION') {
		$contenido_fase .= contenidoDemandaRedaccion($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'DEMANDA: REVISION/CORRECCIONES') {
		$contenido_fase .= contenidoDemandaRevision($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'DEMANDA: REGISTRAR GASTOS') {
		$contenido_fase .= contenidoDemandaGastos($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'DEMANDA: EN MESA PARTES DEL PJ') {
		$contenido_fase .= contenidoDemandaMesaPartes($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'INCAUTACION: C.E.J. POR RESPUESTA DE P.J. Y RECOJO OFICIO') {
		$contenido_fase .= contenidoIncautacionRecojoOficio($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
		$mensaje_btn_next = '✔ OFICIO INCAUTACION RECOGIDO';
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'INCAUTACION: GASTOS DEL OFICIO Y CARGO') {
		$contenido_fase .= contenidoIncautacionGastosOficio($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'INCAUTACION: CAPTURA DEL VEHICULO') {
		$contenido_fase .= contenidoIncautacionCaptura($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
	}
	elseif ($ejecucionfase_reg['estado_actual'] == 'LEVANTAMIENTO ORDEN INCAUTACION') {
		$contenido_fase .= contenidoLevantamientoOrdenIncautacion($ejecucion_reg, $ejecucionfase_reg, $action2);
		$datos_rellenados_para_sgte_paso = verificarDatosRellenados($ejecucionfase_reg['estado_actual'], $ejecucion_id, $ejecucionfase_id, 'btn_next');
		$mensaje_btn_next = '✔ PROCESO DE EJECUCION FINALIZADO';
	}

	if($ejecucionfase_reg['tb_ejecucionfase_completado'] == 0 && $ejecucion_reg['tb_ejecucion_estadoproceso'] == 1) {
		$disabled_next_paso = $datos_rellenados_para_sgte_paso == 1 && $action2 == 'M_ejecucion' ? '' : 'disabled';
		$disabled_paso_ant = $datos_rellenados_para_sgte_paso == 1 || $action2 == 'L_ejecucion' ? 'disabled' : '';
		$btn_regresar= $ejecucionfase_reg['estadoejecucion_regresa'] == 1 ? '<button type="button" class="btn btn-danger '.$disabled_paso_ant.'" name="fase_anterior" id="fase_anterior" onclick="fase_anterior('.$ejecucion_id.', '.$ejecucionfase_id.')"><strong>REGRESAR ANTERIOR</strong></button>' : '';

		if ($ejecucionfase_reg['estado_actual'] != 'TERMINADO') {
			$contenido_fase.=
				'<div align="right" class="col-md-12">
					<div class="form-group" style="margin-bottom: 0px">
						'.$btn_regresar.'
						<button type="button" class="btn btn-github '.$disabled_next_paso.'" name="sgte_fase" id="sgte_fase" onclick="sgte_fase('.$ejecucionfase_id.', \''.$ejecucionfase_reg['estado_next'].'\')"><strong>'.$mensaje_btn_next.'</strong></button>
					</div>
				</div>'
			;
		}
	}

	return $contenido_fase;
}

function mostrarUltimoHistorialFase($ejecucionfase_id) {
	global $oHist;
	//mostrar ultimo historial de la fase
		$res_ult_hist = $oHist->mostrar_ultimo_historial('tb_ejecucionfase', $ejecucionfase_id)['data'];
		return
			'<div class="user-block">
				<span class="description" style="margin-left: 0px">'.
					"{$res_ult_hist['tb_usuario_nom']} {$res_ult_hist['tb_usuario_ape']} {$res_ult_hist['tb_hist_det']}".
				'</span>
			</div>';
	//
}

function buscarUrlDocumento($ejecucion_reg, $nombre_item) {
	require_once '../proceso/Proceso.class.php';
	$oProceso = new Proceso();
	require_once '../pdfgarv/Pdfgarv.class.php';
	$oPdfgarv = new Pdfgarv();

	$credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];

	//para saber si un credito viene de "procesos garveh" o de ANTES del modulo de procesos, se hace una consulta en tabla procesos
	$buscar_proceso = $oProceso->mostrarProcesoXCredito($credito_getdocs);
	$return = array();
	
	if ($buscar_proceso['estado'] == 1) {
		$proceso_id = $buscar_proceso['data']['tb_proceso_id'];

		//en la tabla tb_proc_item, columna tb_item_idx = 'escritura_publica' tiene el tb_item_id = 49.
		//Si se desea buscar id mediante consulta usar Proceso->obtenerItemPorIdx($idx)
		//encontrar el item y su url mediante Proceso->obtenerContenidoPorProcesoFaseItem

		if ($nombre_item == 'escritura_publica') {
			$res = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 49)['data'];
		}
		elseif ($nombre_item == 'bol_fac_compra') {
			$res = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 41)['data'];
		}
		$return['tabla'] = 'tb_proc_multimedia';
		$return['url'] = $res['tb_multimedia_url'];
		$return['id_reg'] = $res['tb_multimedia_id'];
	} else {
		if ($ejecucion_reg['tb_cgarvtipo_id'] == 1) {//es PRECONSTITUCION
			if ($nombre_item == 'escritura_publica') {
				$tipogarvdoc_persdetalle_id = $ejecucion_reg['tb_cliente_tip'] == 1 ? 16 : 48;
			}
			elseif ($nombre_item == 'bol_fac_compra') {
				$tipogarvdoc_persdetalle_id = $ejecucion_reg['tb_cliente_tip'] == 1 ? 12 : 44;
			}
		} /* else para las demas tipos de credito garveh */
		elseif ($ejecucion_reg['tb_cgarvtipo_id'] == 2) {//es CONSTITUCION SIN CUSTODIA
			if ($nombre_item == 'escritura_publica') {
				$tipogarvdoc_persdetalle_id = $ejecucion_reg['tb_cliente_tip'] == 1 ? 77 : 106;
			}
		}

		//retorno
		$buscar_documento = $oPdfgarv->mostrarPorDocumento1($credito_getdocs, $tipogarvdoc_persdetalle_id);
		if ($buscar_documento['estado'] == 1) {
			$return['tabla'] = 'pdfgarv';
			$return['url'] = $buscar_documento['data']['tb_pdfgarv_ruta'];
			$return['id_reg'] = $buscar_documento['data']['tb_pdfgarv_id'];
		}
		unset($buscar_documento);
	}
	$return['estado'] = 1;
	unset($buscar_proceso);
	return $return;
}

function contenidoCartaNotarialRedaccion($ejecucion_reg, $ejecucionfase_reg, $action2) {
	require_once '../escriturapublica/Escriturapublica.class.php';
	$oEscritura = new Escriturapublica();
	require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
	$oCertificadoregveh = new Certificadoregistralveh();
	require_once '../upload/Upload.class.php';
	$oUpload = new Upload();
	
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	$monto_liquidacion = empty($ejecucion_reg['tb_ejecucion_montoliquidacion']) ? '' : mostrar_moneda($ejecucion_reg['tb_ejecucion_montoliquidacion']);
	$credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];

	$moneda_id = $ejecucion_reg['tb_moneda_id'];
	$opt_moneda = getMonedaSelect($moneda_id);
	
	//revisar si ya tiene registrado ESCRITURA PUBLICA, CERTIFICADO REGISTRAL VEHICULAR y LIQUIDACION
		$buscar_escritura_credito = $oEscritura->listar_todos('', $credito_getdocs);
		$buscar_certiregveh_ejecucion = $oCertificadoregveh->listar_todos('', '', $ejecucion_id, 1);
		$oUpload->incluye_xac = 0; //para que incluya si hay un registro previo que tiene xac=0
		$buscar_pdf_liquidacion_ejecucion = $oUpload->mostrarUno_modulo($ejecucion_id, 'ejecucion_liquidacion');
	//

	$persona_id = $ejecucion_reg['tb_persona_id']; //no borrar. es necesario para el require_once
	//$opt_representante_ipdn = getCargoPersonaSelect($persona_id, '4, 8', 'actualidad', 'tb_persona_id', 1, 0);
	$opt_representante_ipdn = getCargoPersonaSelect($persona_id, $buscar_escritura_credito['data'][0]['tb_escriturapublica_ipdncargorepres'], 'actualidad', 'tb_persona_id', 1, 0);

	if (!empty($persona_id)){
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 ? '' : 'display: none;';
	} else {
		$style_guardar = '';
		$style_editar = 'display: none;';
	}

	$btn_edit_EP_cls = 'btn-warning'; $btn_edit_EP_icon = 'fa-edit';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
		$action_btn_EP = 'L'; $btn_edit_EP_cls = 'btn-info'; $btn_edit_EP_icon = 'fa-eye';
	} elseif ($buscar_escritura_credito['estado'] == 1) {
		$action_btn_EP = 'M';
	} else {
		$action_btn_EP = 'I';
	}
	$esc_pub_id = $action_btn_EP != 'I' ? $buscar_escritura_credito['data'][0]['tb_escriturapublica_id'] : 0;
	$hist_EP_disabled = $action_btn_EP != 'I' ? '' : 'disabled';

	$hist_pdfliquidacion_disabled = $buscar_pdf_liquidacion_ejecucion['estado'] == 1 ? '' : 'disabled';

	$disabled_editar_datos_ejecucion = 'disabled';
	$disabled_btn_CN = 'disabled';
	if ($buscar_escritura_credito['estado'] == 1 && $buscar_certiregveh_ejecucion['estado'] == 1 && $buscar_pdf_liquidacion_ejecucion['data']['upload_xac'] == 1) {//si no hay datos seteados
		if (empty($ejecucion_reg['tb_persona_id']) && $action2 == 'M_ejecucion') {
			$disabled_editar_datos_ejecucion = '';
		} elseif (!empty($ejecucion_reg['tb_persona_id'])) {
			$disabled_btn_CN = '';
		}
	}

	//iconos para documentos ya subidos
	$icon_escr_publica = $buscar_escritura_credito['estado'] == 1 ? 'fa-check-square' : 'fa-square-o';
	$icon_cert_regveh = $buscar_certiregveh_ejecucion['estado'] == 1 ? 'fa-check-square' : 'fa-square-o';
	$icon_liquidacion = $buscar_pdf_liquidacion_ejecucion['data']['upload_xac'] == 1 ? 'fa-check-square' : 'fa-square-o';
	$icon_carta_notarial = verificarDatosRellenados('CARTAS NOTARIALES: REDACCION', $ejecucion_id, '', 'btn_cartas') == 1 ? 'fa-check-square' : 'fa-square-o';
	$leer_editar_crv = ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') ? 1 : 0;
	$disabled_btn_editar_datos_ejecucion = $action2 == 'L_ejecucion' ? 'disabled' : '';
	$completado_btn_cn = ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') ? 1 : 0;
	
	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>
				setTimeout(function(){
					verificar_credito_suministro();
				}, 300);
			</script>';
	}

	$btn_liquidacion = $action2 == 'L_ejecucion' ? '' :
	'<div class="row">
		<div class="col-md-12">
			<br><a class="btn btn-primary btn-sm ondisabled_deletehref"  target="_blank"  href="https://www.ipdnsac.com/app/modulos/creditogarveh/doc_cronograma_resolver.php?d1='.$ejecucion_reg['tb_credito_id'].'&fecha=no"><i class="fa fa-print"></i> RESOLVER CREDITO</a>
		</div>
	</div>';

	//variante para cada tipo de credito
	$docs_csc = '';//documentos de Cons. S. Custod.
	if (intval($ejecucion_reg['tb_cgarvtipo_id']) == 2) {//constit. s. custod.
		$docs_csc = '<tr>'.
			'<td></td>'.
			'<td style="padding: 4px 1px;">'.
				'<a class="btn btn-info btn-xs" title="LISTADO" onclick="ejecucionfasefile_tabla('.$ejecucionfase_id.', \''.$action2.'\', '.$ejecucionfase_reg['tb_ejecucionfase_completado'].', '.$ejecucionfase_reg['tb_estadoejecucion_id'].')"><i class="fa fa-eye"></i></a>'.
			'</td>'.
			'<td style="padding: 4px 1px;"><b>SUBIR OTROS DOCUMENTOS</b></td>'.
		'</tr>';
	}

	return $btn_liquidacion.
	'<div class="row">'.
		//1. DOCUMENTOS
		'<div class="col-md-4">
			<h3 style="font-size: 18px;">1. DOCUMENTOS</h3>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td><span class="fa '.$icon_escr_publica.' text-green" style="font-size: 15px;"></span></td>
						<td style="padding: 4px 1px;">
							<a id="btnEP_credito_'.$credito_getdocs.'" href="'.buscarUrlDocumento($ejecucion_reg, 'escritura_publica')['url'].'" class="btn btn-info btn-xs ondisabled_deletehref" title="Ver PDF" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
							<button type="button" class="btn '.$btn_edit_EP_cls.' btn-xs" title="Editar Datos" onclick="editar_datos_escr_pub(\''.$action_btn_EP.'\', '.$credito_getdocs.', '.$ejecucionfase_id.')"><i class="fa '.$btn_edit_EP_icon.'"></i></button>
							<a class="btn '.$btn_edit_EP_cls.' btn-xs '.$hist_EP_disabled.'" title="Historial EP" onclick="historial_form(\'tb_escriturapublica\', '.$esc_pub_id.')"><i class="fa fa-history"></i></a>
						</td>
						<td style="padding: 4px 1px;"><b>ESCRITURA PUBLICA</b></td>
					</tr>
					<tr>
						<td><span class="fa '.$icon_cert_regveh.' text-green" style="font-size: 15px;"></span></td>
						<td style="padding: 4px 1px;">
							<button type="button" class="btn btn-info btn-xs" title="Ver registros" onclick="certificado_tabla('.$ejecucion_id.', '.$ejecucionfase_id.', '.$ejecucion_reg['tb_credito_id'].', '.$leer_editar_crv.', \''.$ejecucionfase_reg['estado_actual'].'\')"><i class="fa fa-eye"></i></button>
						</td>
						<td style="padding: 4px 1px;"><b>CERTIFICADO REGISTRAL VEHICULAR</b></td>
					</tr>
					<tr>
						<td><span class="fa '.$icon_liquidacion.' text-green" style="font-size: 15px;"></span></td>
						<td style="padding: 4px 1px;">
							<a class="btn btn-info btn-xs" title="Ver PDF" onclick="'.verPdfoSubirPdfEjecucion($ejecucion_reg, $ejecucionfase_reg, $buscar_pdf_liquidacion_ejecucion, 'ejecucion_liquidacion', $action2).'"><i class="fa fa-file-pdf-o"></i></a>
							<a class="btn btn-info btn-xs '.$hist_pdfliquidacion_disabled.'" title="Historial PDF Liquid. Cred." onclick="historial_form(\'upload\', '.$buscar_pdf_liquidacion_ejecucion['data']['upload_id'].')"><i class="fa fa-history"></i></a>
						</td>
						<td style="padding: 4px 1px;"><b>LIQUIDACION CREDITO</b></td>
					</tr>'.
					$docs_csc.
				'</tbody>
			</table>
		</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		//2. FORMULARIO DE DATOS DE EJECUCION
		'<div class="col-md-4">
			<form id="form_datosejecucion_cn_redaccion" role="form">
				<input type="hidden" name="action_montoliquidacion_representante" id="action_montoliquidacion_representante" value="guardar_montoliquidacion_representante">
				<h3 style="font-size: 18px;">2. DATOS DE EJECUCION</h3>
				<div class="row form-group">
					<div class="col-md-6">
						<div class="two-fields">
							<label for="txt_monto_liquidacion_cn_redaccion">Monto de liquidacion:</label>
							<div class="input-group">
								<select name="cbo_moneda_pagar" id="cbo_moneda_pagar" class="form-control input-sm mayus input-shadow disabled" style="width: 38%; padding: 5px 8px;">'.
									$opt_moneda.
								'</select>
								<input type="text" name="txt_monto_liquidacion_cn_redaccion" id="txt_monto_liquidacion_cn_redaccion" class="form-control input-sm moneda input-shadow '. $disabled_editar_datos_ejecucion .'" style="width: 62%; padding: 5px 5px;" placeholder="0.00" value="'.$monto_liquidacion.'">
							</div>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-11">
						<label for="cbo_ipdn_rep_cn_redaccion">IPDN representante (en la actualidad):</label>
						<select name="cbo_ipdn_rep_cn_redaccion" id="cbo_ipdn_rep_cn_redaccion" class="form-control input-sm input-shadow mayus '. $disabled_editar_datos_ejecucion .'">'.
							$opt_representante_ipdn.
						'</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-11" align="right">
						<a type="button" class="btn btn-success '.$disabled_editar_datos_ejecucion.'" name="cn_redaccion_guardardatos" id="cn_redaccion_guardardatos" onclick="cn_redaccion_guardardatos('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar datos</strong></a>
						<a type="button" class="btn btn-warning '.$disabled_btn_editar_datos_ejecucion.'" name="cn_redaccion_editardatos" id="cn_redaccion_editardatos" onclick="cn_redaccion_editardatosejecucion()" style="'.$style_editar.'"><strong>Editar datos</strong></a>
						<a type="button" class="btn btn-danger" name="cn_redaccion_cancelareditardatos" id="cn_redaccion_cancelareditardatos" onclick="cn_redaccion_cancelareditardatosejecucion()" style="display: none;"><strong>Cancelar Edicion</strong></a>
					</div>
				</div>
			</form>
		</div>'.
		'<div class="col-md-1" style="width: 2%;"></div>'.
		//3. CARTAS NOTARIALES
		'<div class="col-md-2" style="padding: 0px;">
			<h3 style="font-size: 18px;">3. CARTAS NOTARIALES</h3>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td><span class="fa '.$icon_carta_notarial.' text-green" style="font-size: 15px;"></span></td>
						<td><button type="button" class="btn btn-warning btn-xs '.$disabled_btn_CN.'" title="Editar Datos" onclick="carta_not_lista('.$ejecucion_id.', '.$ejecucionfase_id.', '.$completado_btn_cn.', \'cn_redaccion\')">EDITAR</button></td>
						<td><b>CARTAS NOTARIALES</b></td>
					</tr>
				</tbody>
			</table>
		</div>'.
	'</div>'.
	$script;
}

function verPdfoSubirPdfEjecucion($ejecucion_reg, $ejecucionfase_reg, $busqueda_upload, $modulo_nom, $action2) {
	global $oHist;
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	
	$boton_retorno = "ejecucionpdf_form($ejecucion_id, $ejecucionfase_id, {$ejecucion_reg['tb_credito_id']}, 'new', '$modulo_nom', '$action2')";

	if ($busqueda_upload['estado'] == 1) {
		if ($busqueda_upload['data']['upload_xac'] == 1) {
			$ver_editar_pdf = ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') ? 1 : 0;
			$boton_retorno = "verejecucion_pdf_form({$busqueda_upload['data']['upload_id']}, '$modulo_nom', $ejecucionfase_id, $ver_editar_pdf)";
		} else {
			//buscar en el historial cantidad de modificaciones
			$res_cant_mods = $oHist->filtrar_historial_por('upload', $busqueda_upload['data']['upload_id'])['data'];
			$cont = 1;
			foreach($res_cant_mods as $k => $v){
				$contador = substr(trim($v['tb_hist_det']), 3, 3);
				if($contador == 'act'){
					$cont++;
				}
			}
			unset($res_cant_mods);
			$cantmods_uploadid = "'mod$cont-{$busqueda_upload['data']['upload_id']}'";

			$boton_retorno =  "ejecucionpdf_form($ejecucion_id, $ejecucionfase_id, {$ejecucion_reg['tb_credito_id']}, $cantmods_uploadid, '$modulo_nom', '$action2')";
		}
	}
	return $boton_retorno;
}

function getMonedaSelect($mon_id) {
	$moneda_id = $mon_id;
	ob_start();
	require_once '../moneda/moneda_select.php';
	return ob_get_clean();
}

function getCargoPersonaSelect($pers_id, $cargos, $vigencia, $groupby, $show_cargo, $show_telefono) {
	$persona_id = $pers_id; //no borrar. es necesario para el require_once
	$cargo_id = $cargos;//aquí debe ir solo un cargo, que es el cargo elegido al completar la escritura publica
	$vigente = $vigencia;
	$columna_groupby = $groupby;
	$muestra_cargo = $show_cargo;
	$muestra_telefono = $show_telefono;
	ob_start();
	require '../persona/cargopersona_select.php';
	return ob_get_clean();
}

function verificarDatosRellenados ($fase_nom, $ejecucion_id, $ejecucionfase_id, $valor2) {
	require_once '../funciones/fechas.php';
	$return = 0;
	if ($fase_nom == 'CARTAS NOTARIALES: REDACCION') {
		global $oCartanotarial, $oEjecucionfasefile;
		//verificar si ya tiene una carta de repre y una de cliente ya puede pasar sgte paso
		$res_carta = $oCartanotarial->listar_todos('', $ejecucion_id, '', '');
		if ($res_carta['cant_filas'] >= 2) {
			$carta_cliente = $carta_repre_cliente = false;
			foreach ($res_carta['data'] as $key => $value) {
				if ($value['tb_cartanotarial_destinatario'] == 1) {
					$carta_cliente = true;
				} else {
					$carta_repre_cliente = true;
				}
			}
			if ($valor2 == 'btn_cartas') {
				if  ($carta_cliente && $carta_repre_cliente) {
					$return = 1;
				}
			} elseif ($valor2 == 'btn_next') {
				//verificar si se han subido los documentos incluidos en la redaccion
				$res_files_incluidos = $oEjecucionfasefile->listar('', $ejecucionfase_id, '', '', '', '', '', '');
				if ($res_files_incluidos['estado'] == 1) {
					$todos_archivos_subidos = true;
					foreach ($res_files_incluidos['data'] as $key => $value) {
						if (empty($value['upload_id'])) {
							$todos_archivos_subidos = false;
						}
					}

					if ($carta_cliente && $carta_repre_cliente && $todos_archivos_subidos) {
						$return = 1;
					}
				} elseif ($carta_cliente && $carta_repre_cliente) {
					$return = 1;
				}
			}
		}
	}
	elseif ($fase_nom == 'CARTAS NOTARIALES: REVISION/CORRECCIONES' || $fase_nom == 'DEMANDA: REVISION/CORRECCIONES') {
		global $oEjecucion;
		$usuario_apr = $oEjecucion->listar_ejecucionfase_por($ejecucionfase_id, '', '', 'ef.tb_ejecucionfase_orden DESC', '')['data'][0]['tb_ejecucionfase_usuapr'];

		//si hay un usuario que hizo revision y aprobó las redaccion
		if (!empty($usuario_apr)) {
			$return = 1;
		}
	}
	elseif ($fase_nom == 'CARTAS NOTARIALES: LLEVAR A NOTARIA PARA DILIGENCIA' || $fase_nom == 'CARTAS NOTARIALES: DILIGENCIA') {
		global $oCartanotarial, $oHist;
		$columna_verifi = $fase_nom == 'CARTAS NOTARIALES: DILIGENCIA' ? 'tb_cartanotarial_fecrecojo' : 'tb_cartanotarial_fecnotariarecepciona';
		$carta = $oCartanotarial->listar_todos('', $ejecucion_id, '', '')['data'][0];
		$res_hist = $oHist->filtrar_historial_por('tb_ejecucionfase', $ejecucionfase_id);
		if (date('Y-m-d') >= fecha_mysql($carta[$columna_verifi]) && $res_hist['estado'] == 1) {
			$return = 1;
		}
		unset($carta, $res_hist);
	}
	elseif ($fase_nom == 'ESPERANDO CONTACTO CLIENTE') {
		global $oCartanotarial;
		$res_carta = $oCartanotarial->listar_todos('', $ejecucion_id, '', '');
		if ($res_carta['estado'] == 1) {
			$return = 1;
			foreach ($res_carta['data'] as $key => $carta) {
				$estado_entrega = $carta['tb_cnestadoentrega_id'];
				if (empty($estado_entrega)) {
					$return = 0;
				}
			}
		}
		if ($valor2 == 'btn_next') {
			global $oEjecucion;
			$fex_espera_max = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0]['tb_ejecucion_fecmax_espera'];

			if (!empty($fex_espera_max) && date('Y-m-d') >= fecha_mysql($fex_espera_max) && $return == 1) {
				$return = 1;
			} else {
				$return = 0;
			}
		}
	}
	elseif ($fase_nom  == 'DEMANDA: REDACCION') {
		global $oDemanda;

		$res_demanda = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '');
		$return = $res_demanda['estado'];
	}
	elseif ($fase_nom == 'DEMANDA: REGISTRAR GASTOS') {
		global $oDemanda;

		$res = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0];

		if (($valor2 == 'icono_medcau' && !empty($res['tb_demanda_medcau_upload_id'])) ||
			($valor2 == 'icono_dernot' && !empty($res['tb_demanda_dernot_upload_id'])) ||
			($valor2 == 'icono_dilfue' && !empty($res['tb_demanda_dilfue_upload_id'])))
		{
			$return = 1;
		} elseif ($valor2 == 'btn_next' && !empty($res['tb_demanda_medcau_upload_id']) && !empty($res['tb_demanda_dernot_upload_id'])) {
			$return = 1;
			if (intval($res['tb_demanda_incluyedescer']) == 1 && empty($res['tb_demanda_dilfue_upload_id'])) {
				$return = 0;
			}
		}
	}
	elseif ($fase_nom == 'DEMANDA: EN MESA PARTES DEL PJ') {
		global $oDemanda;
		$pdf_scaned = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0]['tb_demanda_doc_scanned'];
		
		if (!empty($pdf_scaned)) {
			$return = 1;
		}
	}
	elseif ($fase_nom == 'INCAUTACION: C.E.J. POR RESPUESTA DE P.J. Y RECOJO OFICIO') {
		global $oEjecucion;
		$oficiorecojo_fecha_hora = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0]['tb_ejecucion_oficioincau_fecrecojo'];

		if (!empty($oficiorecojo_fecha_hora) && fecha_mysql($oficiorecojo_fecha_hora) == date('Y-m-d')) {
			$return = 1;
		}
	}
	elseif ($fase_nom == 'INCAUTACION: GASTOS DEL OFICIO Y CARGO') {
		//buscar si ya se subió el cargo policial del oficio de incautacion
		global $oEjecucionfasefile;
		$res_cargo = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 5, '', '', '');
		
		global $oEjecucion;
		$legalizacionpago_id = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0]['tb_ejecucion_legalizacionpago_id'];
		
		$return = intval($res_cargo['estado'] == 1 && !empty($legalizacionpago_id));
	}
	elseif ($fase_nom == 'INCAUTACION: CAPTURA DEL VEHICULO') {
		global $oEjecucionfasefile;
		//necesitamos un acta de descerraje, o de intervencion y entrega para saber que terminó
		$acta_descerr = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 6, '', '', 3);
		if ($acta_descerr['estado'] == 1) {
			$return = 1;
		} else {
			$acta_interve = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 6, '', '', 1);
			$acta_entrega = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 6, '', '', 2);

			if ($acta_interve['estado'] == 1 && $acta_entrega['estado'] == 1) {
				$return = 1;
			}
		}
	}
	elseif ($fase_nom == 'LEVANTAMIENTO ORDEN INCAUTACION') {
		global $oEjecucionfasefile;
		//necesitamos un cargo policial de lev ord incau
		$oEjecucionfasefile->oficiocargo_tipo = 3;
		$cargopol_levordcapt = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 5, '', '', '');
		if ($cargopol_levordcapt['estado'] == 1) {
			$return = 1;
		}
		$oEjecucionfasefile->oficiocargo_tipo = '';
	}
	return $return;
}

function contenidoCartaNotarialRevision($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	$contenido_fase = '';

	$persona_id = $ejecucion_reg['tb_ejecucion_revisionpersona_id'];
	$opt_persona_revision = getCargoPersonaSelect($persona_id, '10', 'actualidad', 'tb_persona_id', 0, 1);

	if (!empty($persona_id)){
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$cbo_disabled = 'disabled';
		$mensaje_revision_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';

		//obtener info de la persona encargada de revision
		require_once '../persona/Persona.class.php';
		$oPersona = new Persona();

		$res_pers = $oPersona->mostrar_uno_id($persona_id);
		if ($res_pers['estado'] == 1) {
			$persona_nom = $res_pers['data']['tb_persona_nom'];
			$persona_cel = $res_pers['data']['tb_persona_numcel'];
			$persona_info = "$persona_nom ($persona_cel)";
		}
		unset($res_pers);
	} else {
		$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$style_editar = 'display: none;';
		$cbo_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
		$mensaje_revision_disabled = 'disabled';
		$persona_info = $persona_nom = '(falta asignar)';
	}

	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>
				setTimeout(function(){
					verificar_credito_suministro();
				}, 300);
			</script>';
	}
	
	$contenido_fase .=
	'<div class="row">'.
		//1. PERSONA NOTARIA V° B°
		'<div class="col-md-3">
			<form id="form_persona_vb" role="form">
				<h3 style="font-size: 18px;">1. PERSONA DE NOTARIA</h3>
				<div class="row form-group">
					<div class="col-md-11">
						<label for="cbo_persona_vb">Elija quién dará V° B°:</label>
						<select name="cbo_persona_vb" id="cbo_persona_vb" class="form-control input-sm input-shadow mayus '.$cbo_disabled.'">'.
							$opt_persona_revision.
						'</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-11" align="right">
						<a type="button" class="btn btn-success" name="persona_vb_guardar" id="persona_vb_guardar" onclick="persona_vb_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a>
						<a type="button" class="btn btn-warning" name="persona_vb_editar" id="persona_vb_editar" onclick="persona_vb_editar()" style="'.$style_editar.'"><strong>Editar</strong></a>
						<a type="button" class="btn btn-danger" name="persona_vb_cancelareditar" id="persona_vb_cancelareditar" onclick="persona_vb_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>
					</div>
				</div>
			</form>
		</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		//2. COMUNICACION
		'<div class="col-md-3" style="padding: 0px;">
			<h3 style="font-size: 18px;">2. COMUNICACION</h3>
			<div class="row form-group">
				<div class="col-md-12">
					<input type="hidden" name="hdd_persona_vb_nom" id="hdd_persona_vb_nom" value="'.$persona_nom.'">
					<input type="hidden" name="hdd_persona_vb_cel" id="hdd_persona_vb_cel" value="'.$persona_cel.'">
					<b><a href="javascript:void(0)" onclick="mensaje_solicitud_revision(\'carta\')" class="'.$mensaje_revision_disabled.'">Enviar mensaje a '.$persona_info.'</a></b>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6" align="left">
					<a type="button" class="btn btn-info '.$mensaje_revision_disabled.'" onclick="revision_redaccion('.$ejecucionfase_id.', \'carta\', \''.$action2.'\')"><strong>Rspta. de '.$persona_nom.'</strong></a>
				</div>
			</div>
		</div>'.
	'</div>'.
	$script;
	return $contenido_fase;
}

function armarComentarios($ejecucionfase_id, $action2) {
	require_once '../funciones/fechas.php';
	global $oEjecucion;
	//buscar comentarios anteriores
		$comentarios_anteriores= '';
		$res_buscar_coment = $oEjecucion->listar_ejecucionfase_comentario_por($ejecucionfase_id, '');
		if ($res_buscar_coment['estado'] == 1) {
			$res_buscar_coment = $res_buscar_coment['data'];
			$ejecucion_fase_completada = $res_buscar_coment[0]['tb_ejecucionfase_completado'];
			foreach($res_buscar_coment as $key1 => $val1) {
				$galeria_comentario = '';
				if ($_SESSION['usuario_id'] == 42 || $_SESSION['usuarioperfil_id'] == 1 || $val1['tb_usuario_id'] == $_SESSION['usuario_id']) {//condicionar si es el usuario que lo registró o es usuario admin o gio
					$galeria_comentario = ' | <a href="javascript:void(0)" onclick="subir_foto_comentario('.$val1['tb_procesofase_comentario_id'].', \''.$action2.'\', '.$ejecucion_fase_completada.')" style="font-weight:bold;">GALERIA/EDITAR COMENTARIO</a>';
				}
				$comentarios_anteriores .=
				"- {$val1['tb_usuario_nom']} {$val1['tb_usuario_ape']} comentó: ".
				"{$val1['tb_comentario_des']} | ".mostrar_fecha_hora($val1['tb_procesofase_comentario_fecreg']).$galeria_comentario.
				"<br>";
			}
		} else {
			$comentarios_anteriores = '(no hay comentarios registrados)';
		}
		unset($res_buscar_coment);
	//
	return $comentarios_anteriores;
}

function contenidoCartaNotarialLLevarNotaria($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	$contenido_fase = '';
	$lbl_confirmar_fecha = 'display: none;';

	//buscar la carta notarial del cliente
	global $oCartanotarial;
	require_once '../funciones/fechas.php';

	$res_carta = $oCartanotarial->listar_todos('', $ejecucion_id, 1, '')['data'][0];
	$fecha_llevar_notaria = $res_carta['tb_cartanotarial_fecnotariarecepciona'];

	if (!empty($fecha_llevar_notaria)){
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$txt_disabled = 'disabled';

		if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
			$style_alerta = 'display: none;';
		} elseif ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
			global $oHist;
			$res_hist = $oHist->filtrar_historial_por('tb_ejecucionfase', $ejecucionfase_id);
			if (date('Y-m-d') <= fecha_mysql($fecha_llevar_notaria) && $res_hist['estado'] == 1) {
				//revisar si hay alguna alerta ya creada con esa fecha
				$style_alerta = buscar_alerta_ejecucion($ejecucion_id, mostrar_fecha($fecha_llevar_notaria), 'tb_cartanotarial_fecnotariarecepciona');
			} else {
				$style_alerta = 'display: none;';
				$lbl_confirmar_fecha = '';
			}
		}
	} else {
		$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$style_editar = 'display: none;';
		$txt_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
	}

	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>
				setTimeout(function(){
					verificar_credito_suministro();
				}, 300);
			</script>';
	}
	
	$contenido_fase .=
	'<div class="row">'.
		//1. CONFIRMAR FECHA DE LLEVAR CARTA A NOTARIA
		'<div class="col-md-2">
			<form id="form_cnllevarnotaria_fecha" role="form">
				<h3 style="font-size: 18px;">1. CONFIRMAR FECHA:</h3>
				<div class="row form-group">
					<div class="col-md-12">
						<label for="txt_cnllevarnotaria_fecha">Fecha llevar notaria (definitiva):</label>
						<div class="input-group date">
							<input type="text" name="txt_cnllevarnotaria_fecha" id="txt_cnllevarnotaria_fecha" class="form-control input-sm input-shadow '.$txt_disabled.'" placeholder="Fec. llevar Notaria" value="'.mostrar_fecha($fecha_llevar_notaria).'">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-right"><label style="color: blue; '.$lbl_confirmar_fecha.'">Debe confirmar la fecha de llevar a notaria</label></div>
					<div class="col-md-12" align="right">
						<a type="button" class="btn btn-vk" name="cn_llevarnotaria_crear_alerta" id="cn_llevarnotaria_crear_alerta" onclick="crear_alerta('.$ejecucion_id.', '.$ejecucionfase_id.', \''.date('d-m-Y').'\', \'tb_cartanotarial_fecnotariarecepciona\', \''.mostrar_fecha($fecha_llevar_notaria).'\', '.$_SESSION['usuario_id'].')" style="'.$style_alerta.'"><strong>Crear Alerta</strong></a> '.
						'<a type="button" class="btn btn-success" name="cn_llevarnotaria_guardar" id="cn_llevarnotaria_guardar" onclick="cn_llevarnotaria_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a>
						<a type="button" class="btn btn-warning" name="cn_llevarnotaria_editar" id="cn_llevarnotaria_editar" onclick="cn_llevarnotaria_editar()" style="'.$style_editar.'"><strong>Editar</strong></a>
						<a type="button" class="btn btn-danger" name="cn_llevarnotaria_cancelareditar" id="cn_llevarnotaria_cancelareditar" onclick="cn_llevarnotaria_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>
					</div>
				</div>
			</form>
		</div>'.
	'</div>'.
	$script;
	return $contenido_fase;
}

function contenidoCartaNotarialDiligencia($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	//buscar la carta notarial del cliente
	global $oCartanotarial;
	require_once '../funciones/fechas.php';

	$res_carta = $oCartanotarial->listar_todos('', $ejecucion_id, 1, '')['data'][0];
	$fecha_recojo = $res_carta['tb_cartanotarial_fecrecojo'];

	if (!empty($fecha_recojo)){
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$txt_disabled = 'disabled';
		if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
			$style_alerta = 'display: none;';
		} elseif ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
			//revisar si hay alguna alerta ya creada con esa fecha de espera
			$style_alerta = buscar_alerta_ejecucion($ejecucion_id, mostrar_fecha($fecha_recojo), 'tb_cartanotarial_fecrecojo');
		}
	} else {
		$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$style_editar = $style_alerta = 'display: none;';
		$txt_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
	}
	$fecha_recojo = empty($fecha_recojo) ? '' : mostrar_fecha($fecha_recojo);

	$btn_pago = 'pagar(\'I\', 0, 0, \'cartanotgasto\', '.$ejecucion_reg['tb_credito_id'].', '.$ejecucion_id.', '.$ejecucionfase_id.')';
	$class_btn_pago = 'plus'; $tipo_btn_pago = 'primary'; $disabled_btn_pago = '';
	if (!empty($ejecucion_reg['tb_ejecucion_cartanotpago_id'])) {
		//buscar el gastoid
			require_once '../gastopago/Gastopago.class.php';
			$oGastopago = new Gastopago();
			$where[0]['column_name'] = 'gp.tb_gastopago_id';
			$where[0]['param0'] = $ejecucion_reg['tb_ejecucion_cartanotpago_id'];
			$where[0]['datatype'] = 'INT';
			$gasto_id = $oGastopago->listar_todos($where, array(), array())['data'][0]['tb_gasto_idfk'];
		//

		$btn_pago = 'pagar(\'L\', '.$gasto_id.', '.$ejecucion_reg['tb_ejecucion_cartanotpago_id'].')';
		$class_btn_pago = 'eye'; $tipo_btn_pago = 'info';
	} else {
		$disabled_btn_pago = $ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion' ? 'disabled' : '';
	}
	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>
				setTimeout(function(){
					verificar_credito_suministro();
				}, 300);
			</script>';
	}
	
	
	return
	'<div class="row">'.
		//1. CONFIRMAR FECHA DE RECOJO BRINDADA POR NOTARIA
		'<div class="col-md-2">
			<form id="form_cndiligencia_fecha" role="form">
				<h3 style="font-size: 18px;">1. REGISTRAR FECHA:</h3>
				<div class="row form-group">
					<div class="col-md-11">
						<label for="txt_cndiligencia_fecrecojo">Fecha de recojo de carta:</label>
						<div class="input-group date">
							<input type="text" name="txt_cndiligencia_fecrecojo" id="txt_cndiligencia_fecrecojo" class="form-control input-sm input-shadow '.$txt_disabled.'" placeholder="Fec. recojo indicada por notaría" value="'.$fecha_recojo.'">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-11" align="right">
						<a type="button" class="btn btn-vk" name="cn_diligencia_crear_alerta" id="cn_diligencia_crear_alerta" onclick="crear_alerta('.$ejecucion_id.', '.$ejecucionfase_id.', \''.date('d-m-Y').'\', \'tb_cartanotarial_fecrecojo\', \''.$fecha_recojo.'\', '.$_SESSION['usuario_id'].')" style="'.$style_alerta.'"><strong>Crear Alerta</strong></a>
						<a type="button" class="btn btn-success" name="cn_diligencia_guardar" id="cn_diligencia_guardar" onclick="cn_diligencia_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a>
						<a type="button" class="btn btn-warning" name="cn_diligencia_editar" id="cn_diligencia_editar" onclick="cn_diligencia_editar()" style="'.$style_editar.'"><strong>Editar</strong></a>
						<a type="button" class="btn btn-danger" name="cn_diligencia_cancelareditar" id="cn_diligencia_cancelareditar" onclick="cn_diligencia_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>
					</div>
				</div>
			</form>
		</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">2. GASTOS CARTAS NOTARIALES</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-'.$tipo_btn_pago.' btn-xs '.$disabled_btn_pago.'" title="LISTADO" onclick="'.$btn_pago.'"><i class="fa fa-'.$class_btn_pago.'"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>GASTO CARTAS NOTARIALES</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
	'</div>'.
	$script;
}

function contenidoEsperaContactoCliente($ejecucion_reg, $ejecucionfase_reg, $action2) {
	require_once '../funciones/fechas.php';
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	$icon_carta_notarial = verificarDatosRellenados('ESPERANDO CONTACTO CLIENTE', $ejecucion_id, '', 'icono') == 1 ? 'fa-check-square' : 'fa-square-o';
	if ($icon_carta_notarial == 'fa-square-o') {
		//si aun no han completado los datos de la entrega carta notarial verificar si el asesor llenó el num suministro en el crédito
		$script = '';
		if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
			$script = '<script>'.
					'setTimeout(function(){verificar_credito_suministro();}, 300);'.
				'</script>';
		}
	}

	$fecha_max_espera = $ejecucion_reg['tb_ejecucion_fecmax_espera'];
	$recomendada = '';
	if (empty($fecha_max_espera)) {
		$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$style_editar = $style_alerta = 'display: none;';
		$txt_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
		//recomendar una fecha máx espera, buscar la fecha diligencia en la carta
		global $oCartanotarial;
		$res_carta = $oCartanotarial->listar_todos('', $ejecucion_id, 1, '');
		if ($res_carta['estado'] == 1) {
			foreach ($res_carta['data'] as $key => $carta_cliente) {
				if (!empty($carta_cliente['tb_cartanotarial_fecdiligencia'])) {
					$fecha_max_espera = sumarDiasSemana(fecha_mysql($carta_cliente['tb_cartanotarial_fecdiligencia']), 3);
					$recomendada = '<label style="color: red;">Verificar fecha (fue recomendada por sistema)</label>';
					break;
				}
			}
		}
		unset($res_carta);
	} else {
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$txt_disabled = 'disabled';
		if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
			$style_alerta = 'display: none;';
		} elseif ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
			//revisar si hay alguna alerta ya creada con esa fecha de espera
			$style_alerta = buscar_alerta_ejecucion($ejecucion_id, mostrar_fecha($fecha_max_espera), 'tb_ejecucion_fecmax_espera');
		}
	}
	$ver_editar_cn = $ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion' ? 1 : 0;
	
	return
	'<div class="row">'.
		'<div class="col-md-2">
			<form id="form_cndiligencia_fecha" role="form">
				<h3 style="font-size: 18px;">1. DATOS DE DILIGENCIA:</h3>
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><span class="fa '.$icon_carta_notarial.' text-green" style="font-size: 15px;"></span></td>
							<td><button type="button" class="btn btn-warning btn-xs" title="Editar Datos" onclick="carta_not_lista('.$ejecucion_id.', '.$ejecucionfase_id.', '.$ver_editar_cn.', \'esperando_contacto_cliente\')">EDITAR</button></td>
							<td><b>CARTAS NOTARIALES</b></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>'.
		'<div class="col-md-3">
			<form id="form_esperacliente_fecha" role="form">
				<h3 style="font-size: 18px;">2. FECHA DE ESPERA CLIENTE:</h3>
				<div class="row form-group">
					<div class="col-md-11">
						<label for="txt_espera_cliente_fecmax">Fecha máxima de esperar contacto de cliente:</label>
						<div class="input-group date">
							<input type="text" name="txt_espera_cliente_fecmax" id="txt_espera_cliente_fecmax" class="form-control input-sm input-shadow '.$txt_disabled.'" placeholder="Fec. max espera a cliente" value="'.mostrar_fecha($fecha_max_espera).'">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						'.$recomendada.'
					</div>
				</div>
				<div class="row">
					<div class="col-md-11" align="right">
						<a type="button" class="btn btn-vk" name="esperacontacto_crear_alerta" id="esperacontacto_crear_alerta" onclick="crear_alerta('.$ejecucion_id.', '.$ejecucionfase_id.', \''.date('d-m-Y').'\', \'tb_ejecucion_fecmax_espera\', \''.mostrar_fecha($fecha_max_espera).'\', '.$_SESSION['usuario_id'].')" style="'.$style_alerta.'"><strong>Crear Alerta</strong></a>
						<a type="button" class="btn btn-success" name="esperacontacto_fecmax_guardar" id="esperacontacto_fecmax_guardar" onclick="esperacontacto_fecmax_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a>
						<a type="button" class="btn btn-warning" name="esperacontacto_fecmax_editar" id="esperacontacto_fecmax_editar" onclick="esperacontacto_fecmax_editar()" style="'.$style_editar.'"><strong>Editar</strong></a>
						<a type="button" class="btn btn-danger" name="esperacontacto_fecmax_cancelareditar" id="esperacontacto_fecmax_cancelareditar" onclick="esperacontacto_fecmax_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>
					</div>
				</div>
			</form>
		</div>'.
	'</div>'.
	$script;
}

function buscar_alerta_ejecucion($ejecucion_id, $mens1, $mens2) {
	global $oAlerta;//por modulo, modulo_id, donde el mens2 sea la columna deseada, donde el mensaje 1 contenga la fecha
	$res_buscar_alerta = $oAlerta->listar_todos('', 'ejecucion', '', $ejecucion_id, '', $mens1, $mens2, '', '', 'DESC');
	if ($res_buscar_alerta['estado'] == 1) {
		//solo me importa ver el estado de la primer registro que es el más nuevo por el orden descendente de fecreg. los anteriores se supone que ya estan en estado leidos
		//si no está leida: display:none, de lo contrario ''
		return intval($res_buscar_alerta['data'][0]['tb_alerta_estado']) == 1 ? '' : 'display: none';
	} else {
		return '';
	}
}

function contenidoDemandaRedaccion($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	//buscar demanda para esta ejecucion
	global $oDemanda;

	$icon_dts_deman = verificarDatosRellenados('DEMANDA: REDACCION', $ejecucion_id, '', 'icono') == 1 ? 'fa-check-square' : 'fa-square-o';
	$boton_ver = $boton_hist = $boton_elim = $btn_pdf_demanda = '';

	$style_btns = $action2 == 'M_ejecucion' ? '' : ' disabled';

	$btn_add = '<button type="button" class="btn btn-primary btn-xs '.$style_btns.'" title="Registrar" onclick="form_demanda(\'I\', 0, '.$ejecucion_id.', \''.$action2.'\')"> <i class="fa fa-plus"></i></button>';

	$mod_color_comb = 0;
	$deman = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '');
	if ($deman['estado'] == 1) {
		$deman = $deman['data'][0];

		$btn_add = '';
		$boton_ver = '<button type="button" class="btn btn-info btn-xs" title="Ver" onclick="form_demanda(\'L\', '.$deman['tb_demanda_id'].', '.$ejecucion_id.', \''.$action2.'\')"> <i class="fa fa-eye"></i></button>';
		if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
			$boton_elim = '<button type="button" class="btn btn-danger btn-xs'.$style_btns.'" title="Eliminar" onclick="form_demanda(\'E\', '.$deman['tb_demanda_id'].', '.$ejecucion_id.', \''.$action2.'\')"> <i class="fa fa-trash"></i></button>';
		}
		$boton_hist = '<button type="button" class="btn btn-primary btn-xs" title="Historial" onclick="historial_form(\'tb_demanda\', '.$deman['tb_demanda_id'].')"> <i class="fa fa-history"></i></button>';
		$btn_pdf_demanda = '<a type="button" class="btn btn-github btn-xs'.$style_btns.'" title="GENERAR PDF" target="_blank" href="vista/demanda/doc_demanda.php?d1='.$ejecucion_id.'&d2='.$deman['tb_demanda_id'].'&d3=pdf"> <i class="fa fa-file-pdf-o"></i></a>';
		$mod_color_comb = $deman['tb_demanda_cambio'];
	}
	unset($deman);
	$leer_editar_crv = ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') ? 1 : 0;

	return
	'<div class="row">'.
		'<div class="col-md-3">
			<h3 style="font-size: 18px;">1. REVISAR CERTIFICADO</h3>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td style="padding: 4px 1px;" align="center">
							<button type="button" class="btn btn-info btn-xs" title="Ver registros" onclick="certificado_tabla('.$ejecucion_id.', '.$ejecucionfase_id.', '.$ejecucion_reg['tb_credito_id'].', '.$leer_editar_crv.', \''.$ejecucionfase_reg['estado_actual'].'\')"><i class="fa fa-eye"></i></button>
						</td>
						<td style="padding: 4px 1px;"><b>CERTIFICADO REGISTRAL VEHICULAR</b></td>
					</tr>
				</tbody>
			</table>
		</div>'.
		'<input type="hidden" name="hdd_demandared_ejecucionfase_id" id="hdd_demandared_ejecucionfase_id" value="'.$ejecucionfase_id.'">'.
		'<input type="hidden" name="hdd_demandared_mod_color_comb" id="hdd_demandared_mod_color_comb" value="'.$mod_color_comb.'">'.
		'<div class="col-md-3">
			<h3 style="font-size: 18px;">2. DATOS PARA DEMANDA</h3>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td><span class="fa '.$icon_dts_deman.' text-green" style="font-size: 15px;"></span></td>
						<td>
							'.$btn_add.'
							'.$boton_ver.'
							'.$boton_elim.'
							'.$btn_pdf_demanda.'
							'.$boton_hist.'
						</td>
						<td><b>DATOS DEMANDA</b></td>
					</tr>
				</tbody>
			</table>
		</div>'.
	'</div>';
}

function contenidoDemandaRevision($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];
	$contenido_fase = '';

	$mensaje_revision_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
	
	$persona_id = $ejecucion_reg['tb_ejecucion_revisionpersona_id'];
	//obtener info de la persona encargada de revision
		require_once '../persona/Persona.class.php';
		$oPersona = new Persona();
		$res_pers = $oPersona->mostrar_uno_id($persona_id);
		if ($res_pers['estado'] == 1) {
			$persona_nom = $res_pers['data']['tb_persona_nom'];
			$persona_cel = $res_pers['data']['tb_persona_numcel'];
			$persona_info = "$persona_nom ($persona_cel)";
		}
		unset($res_pers);
	//
	
	$contenido_fase .=
	'<div class="row">'.
		//1. COMUNICACION
		'<div class="col-md-3">
			<h3 style="font-size: 18px;">1. COMUNICACION</h3>
			<div class="row form-group">
				<div class="col-md-12">
					<b><a href="javascript:void(0)" onclick="mensaje_solicitud_revision(\'demanda\')" class="'.$mensaje_revision_disabled.'">Enviar mensaje a '.$persona_info.'</a></b>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6" align="left">
					<a type="button" class="btn btn-info '.$mensaje_revision_disabled.'" onclick="revision_redaccion('.$ejecucionfase_id.', \'demanda\', \''.$action2.'\')"><strong>Rspta. de '.$persona_nom.'</strong></a>
				</div>
			</div>
		</div>'.
	'</div>';
	return $contenido_fase;
}

function contenidoDemandaGastos ($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	global $oDemanda;
	$deman = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0];

	$onclick['medcau'] = $action2 == 'L_ejecucion' ? '' : 'form_demanda_gasto(\'I\', '.$deman['tb_demanda_id'].', \'medcau\', '.$ejecucionfase_id.')';
	$icon_dts['medcau'] = verificarDatosRellenados('DEMANDA: REGISTRAR GASTOS', $ejecucion_id, '', 'icono_medcau') == 1 ? 'fa-check-square' : 'fa-square-o';

	$btn_add['medcau'] = $icon_dts['medcau'] == 'fa-check-square' ? '' : '<button type="button" class="btn btn-primary btn-xs" title="Registrar" onclick="'.$onclick['medcau'].'"> <i class="fa fa-plus"></i></button>';
	$boton_ver['medcau'] = $icon_dts['medcau'] != 'fa-check-square' ? '' : '<button type="button" class="btn btn-info btn-xs" title="Ver" onclick="form_demanda_gasto(\'L\', '.$deman['tb_demanda_id'].', \'medcau\', '.$ejecucionfase_id.')"> <i class="fa fa-eye"></i></button>';
	$boton_elim['medcau'] = $icon_dts['medcau'] == 'fa-check-square' && $action2 != 'L_ejecucion' && $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 ? ' <button type="button" class="btn btn-danger btn-xs" title="Eliminar" onclick="form_demanda_gasto(\'E\', '.$deman['tb_demanda_id'].', \'medcau\', '.$ejecucionfase_id.')"> <i class="fa fa-trash"></i></button>' : '';

	$onclick['dernot'] = $action2 == 'L_ejecucion' ? '' : 'form_demanda_gasto(\'I\', '.$deman['tb_demanda_id'].', \'dernot\', '.$ejecucionfase_id.')';
	$icon_dts['dernot'] = verificarDatosRellenados('DEMANDA: REGISTRAR GASTOS', $ejecucion_id, '', 'icono_dernot') == 1 ? 'fa-check-square' : 'fa-square-o';

	$btn_add['dernot'] = $icon_dts['dernot'] == 'fa-check-square' ? '' : '<button type="button" class="btn btn-primary btn-xs" title="Registrar" onclick="'.$onclick['dernot'].'"> <i class="fa fa-plus"></i></button>';
	$boton_ver['dernot'] = $icon_dts['dernot'] != 'fa-check-square' ? '' : '<button type="button" class="btn btn-info btn-xs" title="Ver" onclick="form_demanda_gasto(\'L\', '.$deman['tb_demanda_id'].', \'dernot\', '.$ejecucionfase_id.')"> <i class="fa fa-eye"></i></button>';
	$boton_elim['dernot'] = $icon_dts['dernot'] == 'fa-check-square' && $action2 != 'L_ejecucion' && $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 ? ' <button type="button" class="btn btn-danger btn-xs" title="Eliminar" onclick="form_demanda_gasto(\'E\', '.$deman['tb_demanda_id'].', \'dernot\', '.$ejecucionfase_id.')"> <i class="fa fa-trash"></i></button>' : '';

	$tercer_gasto = '';
	if (intval($deman['tb_demanda_incluyedescer']) == 1) {
		$onclick['dilfue'] = $action2 == 'L_ejecucion' ? '' : 'form_demanda_gasto(\'I\', '.$deman['tb_demanda_id'].', \'dilfue\', '.$ejecucionfase_id.')';
		$icon_dts['dilfue'] = verificarDatosRellenados('DEMANDA: REGISTRAR GASTOS', $ejecucion_id, '', 'icono_dilfue') == 1 ? 'fa-check-square' : 'fa-square-o';

		$btn_add['dilfue'] = $icon_dts['dilfue'] == 'fa-check-square' ? '' : '<button type="button" class="btn btn-primary btn-xs" title="Registrar" onclick="'.$onclick['dilfue'].'"> <i class="fa fa-plus"></i></button>';
		$boton_ver['dilfue'] = $icon_dts['dilfue'] != 'fa-check-square' ? '' : '<button type="button" class="btn btn-info btn-xs" title="Ver" onclick="form_demanda_gasto(\'L\', '.$deman['tb_demanda_id'].', \'dilfue\', '.$ejecucionfase_id.')"> <i class="fa fa-eye"></i></button>';
		$boton_elim['dilfue'] = $icon_dts['dilfue'] == 'fa-check-square' && $action2 != 'L_ejecucion' && $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 ? ' <button type="button" class="btn btn-danger btn-xs" title="Eliminar" onclick="form_demanda_gasto(\'E\', '.$deman['tb_demanda_id'].', \'dilfue\', '.$ejecucionfase_id.')"> <i class="fa fa-trash"></i></button>' : '';

		$tercer_gasto =
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">3. TASA POR DILIGENCIA FUERA DE JUZGADO</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td><span class="fa '.$icon_dts['dilfue'].' text-green" style="font-size: 15px;"></span></td>'.
						'<td>'.
							$btn_add['dilfue'].
							$boton_ver['dilfue'].
							$boton_elim['dilfue'].
						'</td>'.
						'<td><b>GASTO Y DOCUMENTO</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>';
	}

	return
	'<div class="row">'.
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">1. MEDIDA CAUTELAR</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td><span class="fa '.$icon_dts['medcau'].' text-green" style="font-size: 15px;"></span></td>'.
						'<td>'.
							$btn_add['medcau'].
							$boton_ver['medcau'].
							$boton_elim['medcau'].
						'</td>'.
						'<td><b>GASTO Y DOCUMENTO</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">2. DERECHO NOTIFICACION</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td><span class="fa '.$icon_dts['dernot'].' text-green" style="font-size: 15px;"></span></td>'.
						'<td>'.
							$btn_add['dernot'].
							$boton_ver['dernot'].
							$boton_elim['dernot'].
						'</td>'.
						'<td><b>GASTO Y DOCUMENTO</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
		$tercer_gasto.
	'</div>';
}

function contenidoDemandaMesaPartes($ejecucion_reg, $ejecucionfase_reg, $action2) {
	require_once '../funciones/fechas.php';
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	$juzgado_nro = $ejecucion_reg['tb_ejecucion_juzgado'];
	if (empty($juzgado_nro)) {
		$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$style_editar = 'display: none;';
		$campos_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
	} else {
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$campos_disabled = 'disabled';

		$nro_expediente = $ejecucion_reg['tb_ejecucion_expediente'];
		$especialista = $ejecucion_reg['tb_ejecucion_especialista'];
		$nombre_juez = $ejecucion_reg['tb_ejecucion_nombrejuez'];
		$juzgado_selected[1] = $juzgado_nro == 1 ? 'selected' : '';
		$juzgado_selected[2] = $juzgado_nro == 2 ? 'selected' : '';
		$fecha_hora_present = $ejecucion_reg['tb_ejecucion_demanda_fechora_presentacion'];
		$fecha_hora_sep = explode(' ', $fecha_hora_present);
		//$hora = DateTime::createFromFormat('H:i:s', $fecha_hora_sep[1])->format('g:i A');
	}

	//buscar la demanda para saber si ya subieron el doc scaneada
	global $oDemanda;
	$deman = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0];

	$icon_dem_scan = !empty($deman['tb_demanda_doc_scanned']) ? 'fa-check-square' : 'fa-square-o';
	$hist_pdfdem_scan_disabled = 'disabled';
	$btn_pdf = "ejecucionpdf_form($ejecucion_id, $ejecucionfase_id, {$ejecucion_reg['tb_credito_id']}, 'new', 'ejecucion_varios-demscan', '$action2')";

	if (!empty($deman['tb_demanda_doc_scanned'])) {
		$hist_pdfdem_scan_disabled = '';
		$ver_editar_pdf = ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') ? 1 : 0;
		$btn_pdf = "verejecucion_pdf_form({$deman['tb_demanda_doc_scanned']}, 'ejecucion_varios-demscan', $ejecucionfase_id, $ver_editar_pdf)";
	}
	
	return
	'<div class="row">'.
		'<div class="col-md-5">'.
			'<form id="form_demanda_mesapartes" role="form">'.
				'<input type="hidden" name="action_demanda_mesapartes" id="action_demanda_mesapartes" value="guardar_datos_demanda_mesapartes">'.
				'<h3 style="font-size: 18px;">1. DATOS ASIGNADOS EN MP DEL PJ:</h3>'.
				'<div class="row form-group">'.
					'<div class="col-md-6">'.
						'<label for="txt_ejecucion_nombrejuez">Nombre Juez:</label>'.
						'<input type="text" name="txt_ejecucion_nombrejuez" id="txt_ejecucion_nombrejuez" class="form-control input-sm input-shadow mayus '.$campos_disabled.'" placeholder="Nombre del Juez" value="'.$nombre_juez.'">'.
					'</div>'.
					'<div class="col-md-6">'.
						'<label for="txt_ejecucion_especialista">Especialista:</label>'.
						'<input type="text" name="txt_ejecucion_especialista" id="txt_ejecucion_especialista" class="form-control input-sm input-shadow mayus '.$campos_disabled.'" placeholder="Nombres del Especialista legal" value="'.$especialista.'">'.
					'</div>'.
				'</div>'.
				'<div class="row form-group">'.
					'<div class="col-md-3">'.
						'<label for="txt_ejecucion_nroexpediente">Nro expediente:</label>'.
						'<input type="text" name="txt_ejecucion_nroexpediente" id="txt_ejecucion_nroexpediente" class="form-control input-sm input-shadow mayus '.$campos_disabled.'" placeholder="Nro Expediente" value="'.$nro_expediente.'">'.
					'</div>'.
					'<div class="col-md-3">'.
						'<label for="cbo_ejecucion_juzgado">Juzgado:</label>'.
						'<select name="cbo_ejecucion_juzgado" id="cbo_ejecucion_juzgado" class="form-control input-sm input-shadow mayus '.$campos_disabled.'">'.
							'<option value="0">Seleccione</option>'.
							'<option value="1" style="font-weight: bold;" '.$juzgado_selected[1].'>3° Juzgado civil</option>'.
							'<option value="2" style="font-weight: bold;" '.$juzgado_selected[2].'>8° Juzgado civil</option>'.
						'</select>'.
					'</div>'.
					'<div class="col-md-3">'.
						'<label for="txt_ejecucion_demanda_fec_pres">Fecha presentacion:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_ejecucion_demanda_fec_pres" id="txt_ejecucion_demanda_fec_pres" class="form-control input-sm input-shadow mayus '.$campos_disabled.'" placeholder="Fec. presentacion" value="'.mostrar_fecha($fecha_hora_sep[0]).'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'.
						'</div>'.
					'</div>'.
					/* '<div class="col-md-3">'.
						'<label for="txt_ejecucion_demanda_hora_pres">Hora presentacion:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_ejecucion_demanda_hora_pres" id="txt_ejecucion_demanda_hora_pres" class="form-control input-sm input-shadow mayus '.$campos_disabled.'" placeholder="Hora presentacion" value="'.$hora.'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>'.
						'</div>'.
					'</div>'. */
				'</div>'.
				'<div class="row">'.
					'<div class="col-md-12" align="right">'.
						'<a type="button" class="btn btn-success" name="demanda_mesapartes_guardar" id="demanda_mesapartes_guardar" onclick="demanda_mesapartes_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a> '.
						'<a type="button" class="btn btn-warning" name="demanda_mesapartes_editar" id="demanda_mesapartes_editar" onclick="demanda_mesapartes_editar()" style="'.$style_editar.'"><strong>Editar</strong></a> '.
						'<a type="button" class="btn btn-danger" name="demanda_mesapartes_cancelareditar" id="demanda_mesapartes_cancelareditar" onclick="demanda_mesapartes_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>'.
					'</div>'.
				'</div>'.
			'</form>'.
		'</div>'.
		'<div class="col-md-1" style="width: 3%;"></div>'.
		'<div class="col-md-2">'.
			'<h3 style="font-size: 18px;">2. DOCUMENTO</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td><span class="fa '.$icon_dem_scan.' text-green" style="font-size: 15px;"></span></td>'.
						'<td style="padding: 4px 1px;">'.
							'<a class="btn btn-info btn-xs" title="Ver PDF" onclick="'.$btn_pdf.'"><i class="fa fa-file-pdf-o"></i></a> '.
							'<a class="btn btn-info btn-xs '.$hist_pdfdem_scan_disabled.'" title="Historial PDF" onclick="historial_form(\'upload\', '.$deman['tb_demanda_doc_scanned'].')"><i class="fa fa-history"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>DEMANDA ESCANEADA</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
	'</div>';
}

function contenidoIncautacionRecojoOficio($ejecucion_reg, $ejecucionfase_reg, $action2) {
	require_once '../funciones/fechas.php';
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	$fecha_recojo_ofi = $ejecucion_reg['tb_ejecucion_oficioincau_fecrecojo'];
	if (empty($fecha_recojo_ofi)) {
		$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$style_editar = $style_alerta = 'display: none;';
		$txt_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
	} else {
		$fecha_hora_sep = explode(' ', $fecha_recojo_ofi);
		$hora = DateTime::createFromFormat('H:i:s', $fecha_hora_sep[1])->format('g:i A');
		$style_guardar = 'display: none;';
		$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
		$txt_disabled = 'disabled';
		if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
			$style_alerta = 'display: none;';
		} elseif ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
			//revisar si hay alguna alerta ya creada con esa fecha
			$style_alerta = buscar_alerta_ejecucion($ejecucion_id, mostrar_fecha($fecha_hora_sep[0]), 'tb_ejecucion_oficioincau_fecrecojo');
		}
	}

	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>
				setTimeout(function(){
					verificar_conteo_dias('.$ejecucion_id.', '.$ejecucionfase_id.', '.$ejecucionfase_reg['tb_estadoejecucion_id'].');
				}, 300);
			</script>';
	}
	
	$btn_cej = $action2 == 'L_ejecucion' ? '' : '<br><a href="http://cej.pj.gob.pe/cej/" target="_blank" style="font-weight: bold;">CONSULTA EXPEDIENTES JUDICIALES AQUI</a>';

	return
	'<div class="row">'.
		'<div class="col-md-3">'.
			$btn_cej.
			'<h3 style="font-size: 18px;">1. DOCUMENTOS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-info btn-xs" title="LISTADO" onclick="ejecucionfasefile_tabla('.$ejecucionfase_id.', \''.$action2.'\', '.$ejecucionfase_reg['tb_ejecucionfase_completado'].', '.$ejecucionfase_reg['tb_estadoejecucion_id'].')"><i class="fa fa-eye"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>RESOLUCIONES/NOTIFICACIONES/ESCRITOS</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3">'.
			'<form id="form_oficiorecojo" role="form">'.
				'<h3 style="font-size: 18px;">2. FECHA DE RECOJO DE OFICIO:</h3>'.
				'<div class="row form-group">'.
					'<div class="col-md-6">'.
						'<label for="txt_oficiorecojo_fecha">Fecha recojo:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_oficiorecojo_fecha" id="txt_oficiorecojo_fecha" class="form-control input-sm input-shadow '.$txt_disabled.'" placeholder="Fec. recojo oficio" value="'.mostrar_fecha($fecha_hora_sep[0]).'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'.
						'</div>'.
					'</div>'.
					'<div class="col-md-6">'.
						'<label for="txt_oficiorecojo_hora">Hora presentacion:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_oficiorecojo_hora" id="txt_oficiorecojo_hora" class="form-control input-sm input-shadow mayus '.$txt_disabled.'" placeholder="Hora presentacion" value="'.$hora.'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>'.
						'</div>'.
					'</div>'.
				'</div>'.
				'<div class="row">'.
					'<div class="col-md-12" align="right">'.
						'<a type="button" class="btn btn-vk" name="oficiorecojo_crear_alerta" id="oficiorecojo_crear_alerta" onclick="crear_alerta('.$ejecucion_id.', '.$ejecucionfase_id.', \''.date('d-m-Y').'\', \'tb_ejecucion_oficioincau_fecrecojo\', \''.mostrar_fecha($fecha_hora_sep[0]).' '.$hora.'\', '.$_SESSION['usuario_id'].')" style="'.$style_alerta.'"><strong>Crear Alerta</strong></a> '.
						'<a type="button" class="btn btn-success" name="oficiorecojo_guardar" id="oficiorecojo_guardar" onclick="oficiorecojo_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a> '.
						'<a type="button" class="btn btn-warning" name="oficiorecojo_editar" id="oficiorecojo_editar" onclick="oficiorecojo_editar()" style="'.$style_editar.'"><strong>Editar</strong></a> '.
						'<a type="button" class="btn btn-danger" name="oficiorecojo_cancelareditar" id="oficiorecojo_cancelareditar" onclick="oficiorecojo_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>'.
					'</div>'.
				'</div>'.
			'</form>'.
		'</div>'.
	'</div>'.
	$script;
}

function contenidoIncautacionGastosOficio($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	$btn_pago = 'pagar(\'I\', 0, 0, \'legalizaciongasto\', '.$ejecucion_reg['tb_credito_id'].', '.$ejecucion_id.', '.$ejecucionfase_id.')';
	$class_btn_pago = 'plus'; $tipo_btn_pago = 'primary'; $disabled_btn_pago = '';
	if (!empty($ejecucion_reg['tb_ejecucion_legalizacionpago_id'])) {
		require_once '../gastopago/Gastopago.class.php';
		$oGastopago = new Gastopago();

		$where[0]['column_name'] = 'gp.tb_gastopago_id';
		$where[0]['param0'] = $ejecucion_reg['tb_ejecucion_legalizacionpago_id'];
		$where[0]['datatype'] = 'INT';
		$gasto_id = $oGastopago->listar_todos($where, array(), array())['data'][0]['tb_gasto_idfk'];
		$btn_pago = 'pagar(\'L\', '.$gasto_id.', '.$ejecucion_reg['tb_ejecucion_legalizacionpago_id'].')';
		$class_btn_pago = 'eye'; $tipo_btn_pago = 'info';
	} else {
		$disabled_btn_pago = $ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion' ? 'disabled' : '';
	}

	//buscar la demanda para saber si ya subieron el doc scaneada
	$label = '';
	global $oDemanda;
	$deman = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0];
	if (intval($deman['tb_demanda_incluyedescer']) == 1) {
		$label = '/OFICIO DESCERRAJE';
	}

	return
	'<div class="row">'.
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">1. DOCUMENTOS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-info btn-xs" title="LISTADO" onclick="ejecucionfasefile_tabla('.$ejecucionfase_id.', \''.$action2.'\', '.$ejecucionfase_reg['tb_ejecucionfase_completado'].', '.$ejecucionfase_reg['tb_estadoejecucion_id'].')"><i class="fa fa-eye"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>OFICIO INCAUTACION'.$label.'/CARGO POLICIAL</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">2. GASTOS LEGALIZACION COPIAS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-'.$tipo_btn_pago.' btn-xs '.$disabled_btn_pago.'" title="LISTADO" onclick="'.$btn_pago.'"><i class="fa fa-'.$class_btn_pago.'"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>GASTO LEGALIZACION COPIAS OFICIO INCAUTACION</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
	'</div>';
}

function contenidoIncautacionCaptura($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>
				setTimeout(function(){
					verificar_conteo_dias('.$ejecucion_id.', '.$ejecucionfase_id.', '.$ejecucionfase_reg['tb_estadoejecucion_id'].');
				}, 300);
			</script>';
	}

	//revisar si en esta fase se está registrando un descerraje
	$style_columnas_descerraje = 'display: none;';
	global $oEjecucionfasefile;
	$busq_desc = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 1, 1, '', '');
	if ($busq_desc['estado'] == 1) {
		$style_columnas_descerraje = '';

		$fecha_recojo_ofidescer = $ejecucion_reg['tb_ejecucion_oficiodescer_fecrecojo'];
		if (empty($fecha_recojo_ofidescer)) {
			$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
			$style_editar = $style_alerta = 'display: none;';
			$txt_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
		} else {
			$fecha_hora_sep = explode(' ', $fecha_recojo_ofidescer);
			$hora = DateTime::createFromFormat('H:i:s', $fecha_hora_sep[1])->format('g:i A');
			$style_guardar = 'display: none;';
			$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
			$txt_disabled = 'disabled';
			if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
				$style_alerta = 'display: none;';
			} elseif ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
				//revisar si hay alguna alerta ya creada con esa fecha
				$style_alerta = buscar_alerta_ejecucion($ejecucion_id, mostrar_fecha($fecha_hora_sep[0]), 'tb_ejecucion_oficiodescer_fecrecojo');
			}
		}

		$btn_pago = 'pagar(\'I\', 0, 0, \'desce_legalizaciongasto\', '.$ejecucion_reg['tb_credito_id'].', '.$ejecucion_id.', '.$ejecucionfase_id.')';
		$class_btn_pago = 'plus'; $tipo_btn_pago = 'primary'; $disabled_btn_pago = '';
		if (!empty($ejecucion_reg['tb_ejecucion_oficiodescer_legalizacionpago_id'])) {
			require_once '../gastopago/Gastopago.class.php';
			$oGastopago = new Gastopago();

			$where[0]['column_name'] = 'gp.tb_gastopago_id';
			$where[0]['param0'] = $ejecucion_reg['tb_ejecucion_oficiodescer_legalizacionpago_id'];
			$where[0]['datatype'] = 'INT';
			$gasto_id = $oGastopago->listar_todos($where, array(), array())['data'][0]['tb_gasto_idfk'];
			$btn_pago = 'pagar(\'L\', '.$gasto_id.', '.$ejecucion_reg['tb_ejecucion_oficiodescer_legalizacionpago_id'].')';
			$class_btn_pago = 'eye'; $tipo_btn_pago = 'info';
		} else {
			$disabled_btn_pago = $ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion' ? 'disabled' : '';
		}
	}
	
	$btn_cej = $action2 == 'L_ejecucion' ? '' : '<br><a href="http://cej.pj.gob.pe/cej/" target="_blank" style="font-weight: bold;">CONSULTA EXPEDIENTES JUDICIALES AQUI</a>';

	return
	'<div class="row">'.
		'<div class="col-md-3 '.$busq_desc['sql'].'">'.
			$btn_cej.
			'<h3 style="font-size: 18px;">1. DOCUMENTOS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-info btn-xs" title="LISTADO" onclick="ejecucionfasefile_tabla('.$ejecucionfase_id.', \''.$action2.'\', '.$ejecucionfase_reg['tb_ejecucionfase_completado'].', '.$ejecucionfase_reg['tb_estadoejecucion_id'].')"><i class="fa fa-eye"></i></a>'.
						'</td>'.	
						'<td style="padding: 4px 1px;"><b>ACTA INTERVENCION-ENTREGA/ ACTA DESCERRAJE</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3" style="'.$style_columnas_descerraje.'">'.
			'<form id="form_oficiodescer_recojo" role="form">'.
				'<h3 style="font-size: 18px;">2. FECHA Y HORA DE RECOJO DE OFICIO DESCERRAJE:</h3>'.
				'<div class="row form-group">'.
					'<div class="col-md-6">'.
						'<label for="txt_oficiodescer_recojo_fecha">Fecha recojo:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_oficiodescer_recojo_fecha" id="txt_oficiodescer_recojo_fecha" class="form-control input-sm input-shadow '.$txt_disabled.'" placeholder="Fec. recojo oficio" value="'.mostrar_fecha($fecha_hora_sep[0]).'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'.
						'</div>'.
					'</div>'.
					'<div class="col-md-6">'.
						'<label for="txt_oficiodescer_recojo_hora">Hora presentacion:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_oficiodescer_recojo_hora" id="txt_oficiodescer_recojo_hora" class="form-control input-sm input-shadow mayus '.$txt_disabled.'" placeholder="Hora presentacion" value="'.$hora.'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>'.
						'</div>'.
					'</div>'.
				'</div>'.
				'<div class="row">'.
					'<div class="col-md-12" align="right">'.
						'<a type="button" class="btn btn-vk" name="oficiodescerrecojo_crear_alerta" id="oficiodescerrecojo_crear_alerta" onclick="crear_alerta('.$ejecucion_id.', '.$ejecucionfase_id.', \''.date('d-m-Y').'\', \'tb_ejecucion_oficiodescer_fecrecojo\', \''.mostrar_fecha($fecha_hora_sep[0]).' '.$hora.'\', '.$_SESSION['usuario_id'].')" style="'.$style_alerta.'"><strong>Crear Alerta</strong></a> '.
						'<a type="button" class="btn btn-success" name="oficiodescerrecojo_guardar" id="oficiodescerrecojo_guardar" onclick="oficiodescerrecojo_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a> '.
						'<a type="button" class="btn btn-warning" name="oficiodescerrecojo_editar" id="oficiodescerrecojo_editar" onclick="oficiodescerrecojo_editar()" style="'.$style_editar.'"><strong>Editar</strong></a> '.
						'<a type="button" class="btn btn-danger" name="oficiodescerrecojo_cancelareditar" id="oficiodescerrecojo_cancelareditar" onclick="oficiodescerrecojo_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>'.
					'</div>'.
				'</div>'.
			'</form>'.
		'</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3" style="'.$style_columnas_descerraje.'">'.
			'<h3 style="font-size: 18px;">3. GASTOS LEGALIZACION COPIAS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-'.$tipo_btn_pago.' btn-xs '.$disabled_btn_pago.'" title="LISTADO" onclick="'.$btn_pago.'"><i class="fa fa-'.$class_btn_pago.'"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>GASTO LEGALIZACION COPIAS OFICIO DESCERRAJE</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
	'</div>'.
	$script;
}

function contenidoLevantamientoOrdenIncautacion($ejecucion_reg, $ejecucionfase_reg, $action2) {
	$ejecucion_id = $ejecucionfase_reg['tb_ejecucion_id'];
	$ejecucionfase_id = $ejecucionfase_reg['tb_ejecucionfase_id'];

	$script = '';
	if ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion') {
		$script = '<script>setTimeout(function(){verificar_conteo_dias('.$ejecucion_id.', '.$ejecucionfase_id.', '.$ejecucionfase_reg['tb_estadoejecucion_id'].');}, 300);</script>';
	}

	$class_btn_pago = 'plus'; $tipo_btn_pago = 'primary'; $disabled_btn_pago = 'disabled';
	$cls_guardar = 'disabled';
	$style_editar = $style_alerta = 'display: none;';
	$txt_disabled = 'disabled';
	global $oEjecucionfasefile;
	$busq_resol_admis = $oEjecucionfasefile->listar('', $ejecucionfase_id, $ejecucion_id, 'not null', 1, 1, '', '');
	if ($busq_resol_admis['estado'] == 1) {
		$cls_guardar = '';
		$fecha_recojo_ofi_levordencapt = $ejecucion_reg['tb_ejecucion_oficiolevanincau_fecrecojo'];
		if (empty($fecha_recojo_ofi_levordencapt)) {
			$style_guardar = $action2 == 'M_ejecucion' ? '' : 'display: none;';
			$style_editar = $style_alerta = 'display: none;';
			$txt_disabled = $action2 == 'M_ejecucion' ? '' : 'disabled';
		} else {
			$fecha_hora_sep = explode(' ', $fecha_recojo_ofi_levordencapt);
			$hora = DateTime::createFromFormat('H:i:s', $fecha_hora_sep[1])->format('g:i A');
			$style_guardar = 'display: none;';
			$style_editar = $ejecucionfase_reg['tb_ejecucionfase_completado'] != 1 && $action2 == 'M_ejecucion' ? '' : 'display: none;';
			$txt_disabled = 'disabled';
			if ($ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion') {
				$style_alerta = 'display: none;';
			} elseif ($ejecucionfase_reg['tb_ejecucionfase_completado'] != 1) {
				//revisar si hay alguna alerta ya creada con esa fecha
				$style_alerta = buscar_alerta_ejecucion($ejecucion_id, mostrar_fecha($fecha_hora_sep[0]), 'tb_ejecucion_oficiolevanincau_fecrecojo');
			}
		}

		$btn_pago = 'pagar(\'I\', 0, 0, \'levordencapt_legalizaciongasto\', '.$ejecucion_reg['tb_credito_id'].', '.$ejecucion_id.', '.$ejecucionfase_id.')';
		$disabled_btn_pago = '';
		if (!empty($ejecucion_reg['tb_ejecucion_oficiolevanincau_legalizacionpago_id'])) {
			require_once '../gastopago/Gastopago.class.php';
			$oGastopago = new Gastopago();

			$where[0]['column_name'] = 'gp.tb_gastopago_id';
			$where[0]['param0'] = $ejecucion_reg['tb_ejecucion_oficiolevanincau_legalizacionpago_id'];
			$where[0]['datatype'] = 'INT';
			$gasto_id = $oGastopago->listar_todos($where, array(), array())['data'][0]['tb_gasto_idfk'];
			$btn_pago = 'pagar(\'L\', '.$gasto_id.', '.$ejecucion_reg['tb_ejecucion_oficiolevanincau_legalizacionpago_id'].')';
			$class_btn_pago = 'eye'; $tipo_btn_pago = 'info';
		} else {
			$disabled_btn_pago = $ejecucionfase_reg['tb_ejecucionfase_completado'] == 1 || $action2 == 'L_ejecucion' ? 'disabled' : '';
		}
	}
	
	$btn_cej = $action2 == 'L_ejecucion' ? '' : '<br><a href="http://cej.pj.gob.pe/cej/" target="_blank" style="font-weight: bold;">CONSULTA EXPEDIENTES JUDICIALES AQUI</a>';

	return
	'<div class="row">'.
		'<div class="col-md-3">'.
			$btn_cej.
			'<h3 style="font-size: 18px;">1. DOCUMENTOS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-info btn-xs" title="LISTADO" onclick="ejecucionfasefile_tabla('.$ejecucionfase_id.', \''.$action2.'\', '.$ejecucionfase_reg['tb_ejecucionfase_completado'].', '.$ejecucionfase_reg['tb_estadoejecucion_id'].')"><i class="fa fa-eye"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>ESCRITO LEVANTAMIENTO ORDEN INCAUTACION/RESOLUCION/OFICIO</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3">'.
			'<form id="form_ofilevordencapt_recojo" role="form">'.
				'<h3 style="font-size: 18px;">2. FECHA Y HORA DE RECOJO DE OFICIO LEV. ORDEN CAPTURA:</h3>'.
				'<div class="row form-group">'.
					'<div class="col-md-6">'.
						'<label for="txt_ofilevordencapt_recojo_fecha">Fecha recojo:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_ofilevordencapt_recojo_fecha" id="txt_ofilevordencapt_recojo_fecha" class="form-control input-sm input-shadow '.$txt_disabled.'" placeholder="Fec. recojo oficio" value="'.mostrar_fecha($fecha_hora_sep[0]).'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'.
						'</div>'.
					'</div>'.
					'<div class="col-md-6">'.
						'<label for="txt_ofilevordencapt_recojo_hora">Hora presentacion:</label>'.
						'<div class="input-group date">'.
							'<input type="text" name="txt_ofilevordencapt_recojo_hora" id="txt_ofilevordencapt_recojo_hora" class="form-control input-sm input-shadow mayus '.$txt_disabled.'" placeholder="Hora presentacion" value="'.$hora.'">'.
							'<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>'.
						'</div>'.
					'</div>'.
				'</div>'.
				'<div class="row">'.
					'<div class="col-md-12" align="right">'.
						'<a type="button" class="btn btn-vk" name="ofilevordencaptrecojo_crear_alerta" id="ofilevordencaptrecojo_crear_alerta" onclick="crear_alerta('.$ejecucion_id.', '.$ejecucionfase_id.', \''.date('d-m-Y').'\', \'tb_ejecucion_oficiolevanincau_fecrecojo\', \''.mostrar_fecha($fecha_hora_sep[0]).' '.$hora.'\', '.$_SESSION['usuario_id'].')" style="'.$style_alerta.'"><strong>Crear Alerta</strong></a> '.
						'<a type="button" class="btn btn-success '.$cls_guardar.'" name="ofilevordencaptrecojo_guardar" id="ofilevordencaptrecojo_guardar" onclick="ofilevordencaptrecojo_guardar('.$ejecucion_id.', '.$ejecucionfase_id.')" style="'.$style_guardar.'"><strong>Guardar</strong></a> '.
						'<a type="button" class="btn btn-warning" name="ofilevordencaptrecojo_editar" id="ofilevordencaptrecojo_editar" onclick="ofilevordencaptrecojo_editar()" style="'.$style_editar.'"><strong>Editar</strong></a> '.
						'<a type="button" class="btn btn-danger" name="ofilevordencaptrecojo_cancelareditar" id="ofilevordencaptrecojo_cancelareditar" onclick="ofilevordencaptrecojo_cancelareditar()" style="display: none;"><strong>Cancelar Edicion</strong></a>'.
					'</div>'.
				'</div>'.
			'</form>'.
		'</div>'.
		'<div class="col-md-1" style="width: 4%;"></div>'.
		'<div class="col-md-3">'.
			'<h3 style="font-size: 18px;">3. GASTOS LEGALIZACION COPIAS</h3>'.
			'<table class="table table-striped">'.
				'<tbody>'.
					'<tr>'.
						'<td style="padding: 4px 1px;" align="center">'.
							'<a class="btn btn-'.$tipo_btn_pago.' btn-xs '.$disabled_btn_pago.'" title="LISTADO" onclick="'.$btn_pago.'"><i class="fa fa-'.$class_btn_pago.'"></i></a>'.
						'</td>'.
						'<td style="padding: 4px 1px;"><b>GASTO LEGALIZACION COPIAS OFICIO LEV. ORD. CAPTURA</b></td>'.
					'</tr>'.
				'</tbody>'.
			'</table>'.
		'</div>'.
	'</div>'.
	$script;
}
