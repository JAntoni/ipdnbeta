<?php
session_name("ipdnsac");
session_start();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$ejecucion_id = intval($_POST['ejecucion_id']);
$ejecucion_fase = $_POST['ejecucion_fase'];
$credito_id = intval($_POST['credito_id']);
$origen = $_POST['origen'];

?>

<style type="text/css">
    .c_cursor{
      cursor: pointer;
    }
    .hidden {
        display: none;
    }

    #loadingMessage {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: rgba(255, 255, 255, 0.8);
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        z-index: 1000;
    }
</style>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_gastos_ejecucion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">GASTOS DE EJECUCIÓN</h4>
            </div>

            <div class="modal-body">
                <!-- <div id="gasto_select" class="row">
                    <div class="col-md-12">
                        <div class="box box-info shadow-simple col-md-4">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="info-box">
                                        <span class="info-box-icon c_cursor bg-yellow" onclick="gasto_ejecucion_form('incautacion', <?php echo $ejecucion_id; ?>, <?php echo $ejecucion_fase; ?>, <?php echo $credito_id; ?>, '<?php echo $origen; ?>')"><i class="fa fa-car"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Etapa</span>
                                            <span class="info-box-number">INCAUTACIÓN</span>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="info-box">
                                        <span class="info-box-icon c_cursor bg-green" onclick="gasto_ejecucion_form('liberacion', <?php echo $ejecucion_id; ?>, <?php echo $ejecucion_fase; ?>)"><i class="fa fa-unlock"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Etapa</span>
                                            <span class="info-box-number">LIBERACIÓN</span>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div> -->
                <div id="loadingMessage" class="overlay hidden">Cargando...</div>
                <div id="gasto_content" class="row">
                    <!-- <div class="col-md-6" style="text-align: left;">
                        <button name="btn_volver_tabla" id="btn_volver_tabla" type="button" class="btn btn-sm btn-danger" onclick="btnVolverTabla()"><i class="fa fa-arrow-left"></i> Atrás</button>
                    </div> -->
                    <!-- <div class="col-md-6" style="text-align: right;">
                        <button name="btn_liberacion" id="btn_liberacion" type="button" class="btn btn-info" onclick="btnVolverTabla()"><i class="fa fa-unlock"></i> Liberación</button>
                    </div> -->
                    <div class="col-sm-12">
						<div id="div_ejecucion_tabla_incautacion" class="table-responsive dataTables_wrapper form-inline dt-bootstrap content hidden" style="padding-top: 0px"></div>
						<div id="div_ejecucion_tabla_liberacion" class="table-responsive dataTables_wrapper form-inline dt-bootstrap content hidden" style="padding-top: 0px"></div>
					</div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        cargarDatosIncautacion(<?php echo $ejecucion_id; ?>, <?php echo $ejecucion_fase; ?>, <?php echo $credito_id; ?>, '<?php echo $origen; ?>');
    });

    $("#box-widget").activateBox();
    function cargarDatosIncautacion(ejecucion_id, ejecucion_fase, credito_id, origen) {
        $('#loadingMessage').removeClass('hidden');
        $.ajax({
            type: "POST",
            url: VISTA_URL + "ejecucion/ejecucion_gastos_incautacion.php",
            async: true,
            dataType: "html",
            data: {
                ejecucion_id: ejecucion_id,
                ejecucion_fase: ejecucion_fase,
                credito_id: credito_id,
                origen: origen
            },
            success: function (data) {
                $('#div_ejecucion_tabla_incautacion').html(data).removeClass('hidden');
                $('#div_ejecucion_tabla_liberacion').addClass('hidden');
            },
            complete: function (data) {
                $('#loadingMessage').addClass('hidden');
            },
            error: function (data) {
                $('#ejecucion_mensaje_tbl').html('ERROR AL CARGAR DATOS');
            }
        });
    }

    function cargarDatosLiberacion(ejecucion_id, ejecucion_fase) {
        $.ajax({
            url: 'ruta_a_tu_scriptL_php',
            method: 'POST',
            data: { tipo: 'liberacion', ejecucion_id: ejecucion_id, ejecucion_fase: ejecucion_fase },
            success: function(response) {
                $('#div_ejecucion_tabla_liberacion').html(response).removeClass('hidden');
                $('#div_ejecucion_tabla_incautacion').addClass('hidden');
            }
        });
    }

    function btnCerrarIncautacion(ejecucionliberacion_id, valor, action) {
        var tabla = `&tabla=tb_ejecucionliberacion`;
        var columna = `&columna=tb_ejecucionliberacion_fecfin`;
        var valor = `&valor=${valor}`;
        var tipo_dato = `&tipo_dato=STR`;
        var ejecucionliberacion_id = `&ejecucionliberacion_id=${ejecucionliberacion_id}`;
        var form_serializado = 'action=modificar_campo_liberacion'+tabla+columna+valor+tipo_dato+ejecucionliberacion_id;
        $.confirm({
            title: 'CONFIRME',
            content: `<b>¿Desea ${action} el proceso de ejecución legal?</b>`,
            type: 'orange',
            escapeKey: 'close',
            backgroundDismiss: true,
            columnClass: 'small',
            buttons: {
                ACEPTAR: {
                    text: 'ACEPTAR',
                    btnClass: 'btn-orange',
                    action: function () {
                        modificar_campo_liberacion(form_serializado);
                    }
                },
                cancelar: function () {
                }
            }
        });
    }

    function modificar_campo_liberacion(form_serializado) {
        $.ajax({
            type: 'POST',
            url: VISTA_URL + 'ejecucionupload/ejecucionupload_controller.php',
            async: true,
            dataType: 'json',
            data: form_serializado,
            success: function (data) {
                if (data.estado == 1) {
                    alerta_success("SISTEMA", data.mensaje);
                    //disabled($('#cbo_persona_vb'));
                    //$('#div_ejecucion_tabla_incautacion').addClass('hidden');
                    $('#modal_gastos_ejecucion').modal('hide');
                } else {
                    alerta_error("SISTEMA", data.mensaje);
                }
            },
            complete: function (data) {
                console.log(data);
            }
        });
    }

    function generarGastosPDF(ejecucion_id, ejecucion_fase, cre_id, origen, usuario_id, hoy, estado, ejecucionliberacion_id) {


        window.open("vista/ejecucion/ejecucion_gastos_pdf.php?ejecucion_id="+ejecucion_id+"&ejecucion_fase="+ejecucion_fase+"&credito_id="+cre_id+"&origen="+origen+"&usuario_id="+usuario_id+"&hoy="+hoy+"&estado="+estado+"&ejecucionliberacion_id="+ejecucionliberacion_id,"_blank");

    }
</script>