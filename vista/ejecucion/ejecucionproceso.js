var monto_liq_orig, repr_orig, persona_vb_orig, fecha_llevarnotaria_orig, fec_recojo_orig, fec_max_espera_orign, style_alerta_esperacliente_orig, style_alerta_cn_diligencia_orig;
var mesapartes_nroexpediente_orig, mesapartes_espec_origin, mesapartes_juez_origin, mesapartes_juzgado_origin, mesapartes_fecpres_origin, mesapartes_horapres_origin;
var style_alerta_oficiorecojo_orig, oficiorecojo_fec_orig, oficiorecojo_hora_orig;
var style_alerta_oficiodescerrecojo_orig, oficiodescerrecojo_fec_orig, oficiodescerrecojo_hora_orig;
var style_alerta_cn_llevarnotaria_orig;
var style_alerta_ofilevordencaptrecojo_orig, ofilevordencaptrecojo_fec_orig, ofilevordencaptrecojo_hora_orig;

function ejecucionproceso_document_ready(add_header) {
    if (add_header == 1) {
        $('#header_contenedor')
        .append(
            '<div class="container-fluid" id="header_caso">'+
                '<p></p>'+
                '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<div class="box" style="margin-bottom: 10px;">'+
                            '<div class="box-body" id="box_superior" style="background-color: #E7E1A9;">'+
                                '<div class="row">'+
                                    '<div class="col-md-1">'+
                                        '<button name="btn_volver_tabla" id="btn_volver_tabla" type="button" class="btn btn-danger" onclick="btnVolverTabla()"><i class="fa fa-arrow-left"></i> Lista Procesos</button>'+
                                    '</div>'+
                                    '<div class="col-md-9" id="div_mensaje_verificacion">'+
                                        //aqui van las letras de revisar cej
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                        '<span class="badge bg-github" style="white-space: normal;">CLIENTE: '+$('#hdd_ejecucion_clienteconcat').val()+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'
        )
        .css({
            'position': 'fixed',
            'width': '100%',
            'z-index': '1000',
            'padding-right': '50px',
            'background-color': '#ecf0f5'
        });
    }

    $(".moneda").focus(function(){
        formato_moneda(this);
    });
    $('#txt_cnllevarnotaria_fecha, #txt_cndiligencia_fecrecojo, #txt_espera_cliente_fecmax, #txt_oficiorecojo_fecha, #txt_oficiodescer_recojo_fecha, #txt_ofilevordencapt_recojo_fecha').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        startDate: new Date()
    });
    $('#txt_ejecucion_demanda_fec_pres').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });
    $('#txt_ejecucion_demanda_hora_pres, #txt_oficiorecojo_hora, #txt_oficiodescer_recojo_hora, #txt_ofilevordencapt_recojo_hora').timepicker({
        timeFormat: 'HH:mm',
        interval: 5,
        defaultTime: 'now',
        maxTime: '20:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true 
    });

    var sesion_usu_id = parseInt($("#hdd_id_usu_actual").val());
    var sesion_usuperfil_id = parseInt($("#hdd_perfil_usu_actual").val());
    var encargado_usuid = parseInt($("#usu_encargado_id").val());
    var encargado_usuperfil_id = parseInt($("#usu_encargado_perfil_id").val());
    if (sesion_usu_id == encargado_usuid || sesion_usuperfil_id == 1 || sesion_usuperfil_id == 6 || $('#hdd_ejecucion_action2').val() == 'L_ejecucion') {
        disabled($("section#ejecucion_fase").find(".disabled"));
    } else {
        disabled($("section#ejecucion_fase").find("button, a, input, select").not("#btn_volver_tabla, #btn_guardar_comentario"));
    }
}

function registrarComentario(ejecucionfase_id, action2) {
    var comentario = $(`#new_comentario_${ejecucionfase_id}`).val().trim();
    if (comentario == '') {
        alerta_error("SISTEMA", 'El comentario está vacío');
        return;
    }
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'comentario',
            action2: 'insertar',
            action3: action2,
            ejecucionfase_id: ejecucionfase_id,
            comentario: comentario
        }),
        beforeSend: function () {
            $('div#comentarios_anteriores_'+ejecucionfase_id).html(
                '<div class="callout callout-info" id="mensaje_comentarios_ejecucionfase_'+ejecucionfase_id+'">'+
                    '<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>'+
                '</div>'
            ).show(300);
        },
        success: function (data) {
            if (data.estado == 1) {
                alerta_success("SISTEMA", data.mensaje);
                $('#mensaje_comentarios_ejecucionfase_'+ejecucionfase_id).hide(300);
                $('div#comentarios_anteriores_'+ejecucionfase_id).html(data.html);
                $(`#new_comentario_${ejecucionfase_id}`).val('');
            } else {
                alerta_error("SISTEMA", data.mensaje);
            }
        }
    });
}

function editar_comentario() {
    var comentario = encodeURIComponent($('#txt_procesofasecomentario_comentario').val());
    var comentario_id = $('#hdd_procesofasecomentario_id').val();
    var ejecucion_id = $('#ejecucion_id').val();
    var ejecucionfase_id = $('#hdd_ejecucionfase_id').val();

    var action = `action=modificar_campo`;
    var tabla = `&tabla=tb_procesofase_comentario`;
    var columna = `&columna=tb_comentario_des`;
    var valor = `&valor=${comentario}`;
    var tipo_dato = `&tipo_dato=STR`;

    if (comentario == '') {
        alerta_error("SISTEMA", 'El comentario está vacío');
        return;
    }
    disabled($('#btn_editar_comentario'));
    modificar_campo('form_procesofasecomentario', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}&comentario_id=${comentario_id}`, ejecucionfase_id, $('#hdd_procesofasecomentario_action2').val());
}

function sgte_fase(ejecucionfase_id, estado_next) {
    $.confirm({
        title: 'CONFIRME',
        content: `¿ESTÁ SEGURO QUE DESEA PASAR A LA SIGUIENTE FASE <b>"${estado_next}"</b>?`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            ACEPTAR: {
                text: 'ACEPTAR',
                btnClass: 'btn-orange',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action: 'sgte_fase',
                            ejecucionfase_id: ejecucionfase_id
                        }),
                        success: function (data) {
                            if (data.estado == 1) {
                                alerta_success("SISTEMA", data.mensaje);
                                $("section#ejecucion_fase").remove();
                                
                                createHtmlAsync(data.html, 'div#content_wrapper')
                                    .then(insertedHtml => {
                                        ejecucionproceso_document_ready();
                                    })
                                    .catch(error => {
                                        console.error('Error al insertar elementos:', error);
                                    });
                                //
                                $('#mensaje_recarga').show(200);
                            } else {
                                alerta_error("SISTEMA", data.mensaje);
                            }
                        }
                    });
                }
            },
            cancelar: function () {
            }
        }
    });
}

function fase_anterior(ejecucion_id, ejecucionfase_id) {
    $.confirm({
        title: 'CONFIRME',
        content: `¿ESTÁ SEGURO QUE DESEA REGRESAR A LA FASE ANTERIOR?`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            ACEPTAR: {
                text: 'ACEPTAR',
                btnClass: 'btn-orange',
                action: function () {                    
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action: 'fase_anterior',
                            ejecucion_id: ejecucion_id,
                            ejecucionfase_id: ejecucionfase_id
                        }),
                        success: function (data) {
                            console.log(data);
                            $.alert(`<b>${data.mensaje}</b>`);
                            if (data.estado == 1) {
                                actualizar_fases_ejecucion(ejecucion_id);
                            }
                        },
                    });
                }
            },
            cancelar: function () {
            }
        }
    });
}

function actualizar_fases_ejecucion(ejecucion_id) {
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'actualizar_vista_fases',
            ejecucion_id: ejecucion_id
        }),
        beforeSend: function() {
            $('#timeline_fases').html('');
            $('#ejecucion_mensaje_proceso').show(300);
        },
        success: function (data) {
            $('#ejecucion_mensaje_proceso').hide(300);
            $('#timeline_fases').html(data.html);
            ejecucionproceso_document_ready();
            $('#hdd_ejecucion_reg').val(data.ejecucion_reg);
        },
    });
}

function crear_alerta(ejecucion_id, ejecucionfase_id, fecha_mostrara, campo, valor, sesion_usuario_id) {
    var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
    var cliente_nom = '';
    if (parseInt(ejecucion_reg.tb_cliente_tip) == 1) {
        cliente_nom = ejecucion_reg.tb_cliente_nom;
    } else {
        cliente_nom = ejecucion_reg.tb_cliente_emprs;
    }
    var modulo_nom = ''; var mens1 = '';
    if (campo == 'tb_ejecucion_fecmax_espera') {
        modulo_nom = 'ejecucion';
        mens1 = encodeURIComponent('CASO CLIENTE: '+cliente_nom+'. La fecha máxima de esperar el contacto del cliente despues de haber sido notificado notarialmente es '+valor);
    }
    else if (campo == 'tb_cartanotarial_fecrecojo') {
        modulo_nom = 'ejecucion';
        mens1 = encodeURIComponent('CASO CLIENTE: '+cliente_nom+'. La fecha para recoger la Carta notarial del cliente es '+valor);
    }
    else if (campo == 'tb_ejecucion_oficioincau_fecrecojo') {
        modulo_nom = 'ejecucion';
        mens1 = encodeURIComponent('CASO CLIENTE: '+cliente_nom+'. La fecha y hora para recoger el Oficio de Incautación es '+valor);
    }
    else if (campo == 'tb_ejecucion_oficiodescer_fecrecojo') {
        modulo_nom = 'ejecucion';
        mens1 = encodeURIComponent('CASO CLIENTE: '+cliente_nom+'. La fecha y hora para recoger el Oficio de Descerraje es '+valor);
    }
    else if (campo == 'tb_ejecucion_oficiolevanincau_fecrecojo') {
        modulo_nom = 'ejecucion';
        mens1 = encodeURIComponent('CASO CLIENTE: '+cliente_nom+'. La fecha y hora para recoger el Oficio de Levantamiento Orden de Captura es '+valor);
    }
    else if (campo == 'tb_cartanotarial_fecnotariarecepciona') {
        modulo_nom = 'ejecucion';
        mens1 = encodeURIComponent('CASO CLIENTE: '+cliente_nom+'. La fecha para llevar las Cartas notariales a notaria es '+valor);
    }
    var data_form = `action_alerta=insertar&txt_alerta_fecha_mostrar=${fecha_mostrara}&txt_alerta_modulo_nom=${modulo_nom}&txt_alerta_modulo_id=${ejecucion_id}`+
    `&txt_alerta_titulo=RECORDATORIO&txt_alerta_mens1=${mens1}&txt_alerta_mens2=${campo}&cbo_alerta_prioridad=1&cbo_alerta_destinatario=1&cbo_alerta_destinatario_id=${sesion_usuario_id}`;

    $.confirm({
        title: 'CONFIRME',
        content: `¿ESTÁ SEGURO QUE DESEA CREAR UNA ALERTA?`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            ACEPTAR: {
                text: 'ACEPTAR',
                btnClass: 'btn-orange',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: VISTA_URL + "alerta/alerta_controller.php",
                        async: true,
                        dataType: "json",
                        data: data_form,
                        success: function (data) {
                            if (parseInt(data.estado) > 0) {
                                alerta_success("SISTEMA", data.mensaje);
                                if (data.estado == 1) {
                                    $('#mensaje_recarga').show(200);
                                    actualizar_contenido_1fase(ejecucionfase_id);
                                }
                            }
                            else {
                                alerta_error("SISTEMA", data.mensaje);
                            }
                        }
                    });
                }
            },
            cancelar: function () {
            }
        }
    });
}

function ejecucionpdf_form(ejecucion_id, ejecucionfase_id, credito_id, pdf_is, modulo_nom, action2, tabla_id2, nombre_doc, tabla_nom) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucionupload/ejecucionpdf_form.php",
        async: true,
        dataType: "html",
        data: {
            action: 'I',
            ejecucion_id: ejecucion_id,
            pdf_is: pdf_is,//mod or new
            modulo_nom: modulo_nom,
            ejecucionfase_id: ejecucionfase_id,
            credito_id: credito_id,
            action2: action2,
            tabla_id2: tabla_id2,
            nombre_doc: nombre_doc,
            tabla_nom: tabla_nom
        },
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $("#div_modal_ejecucionfile_form").html(html);
            $("#modal_ejecucionupload_form").modal("show");
            $('#modal_mensaje').modal('hide');
            modal_hidden_bs_modal("modal_ejecucionupload_form", "limpiar");
        }
    });
}

function verejecucion_pdf_form(uploadid, modulo_nom, ejecucionfase_id, ejecucionfase_completado, tabla_id2, tabla_nom, archivo_nom) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucionupload/ver_pdfuploaded_form.php",
        async: true,
        dataType: "html",
        data: {
            upload_id: uploadid,
            modulo_nom: modulo_nom,
            ejecucionfase_id: ejecucionfase_id,
            ejecucionfase_completado: ejecucionfase_completado,
            tabla_id2: tabla_id2,
            tabla_nom: tabla_nom,
            archivo_nom: archivo_nom
        },
        beforeSend: function () {
            $("#h3_modal_title").text("Cargando Formulario");
            $("#modal_mensaje").modal("show");
        },
        success: function (data) {
            $("#div_modal_ejecucion_verpdf_form").html(data);
            $("#modal_ejecucion_pdf_ver").modal("show");
            modal_height_auto("modal_ejecucion_pdf_ver"); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal("modal_ejecucion_pdf_ver", "limpiar"); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            $("#modal_mensaje").modal("hide");
        },
        error: function (data) {
            alerta_error("Error", "ERROR!:" + data.responseText); //en generales.js
        },
    });
}

function mensaje_solicitud_revision(documento) {
    var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
    var ahora=new Date();
    var hora=ahora.getHours();

    var doc_a_validar = 'las siguientes cartas';
    if (documento == 'demanda') {
        doc_a_validar = 'la siguiente demanda';
    }
    
    var texto = hora < 12 ? '¡Buenos días' : '¡Buenas tardes';
    texto += ' a todos! Dra. '+$('#hdd_persona_vb_nom').val()+', por favor, le agradecería mucho revisar '+doc_a_validar+', del cliente *'+ejecucion_reg.tb_cliente_nom+'*';
    var texto1 = encodeURIComponent(texto);

    var boton = 
    `<div class="col-md-12">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td align="center">${$('#hdd_persona_vb_cel').val()} 
                        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51${$('#hdd_persona_vb_cel').val()}?text=${texto1}" target="_blank"><i class="fa fa-whatsapp"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>`;
    $.confirm({
        title: 'Mensaje para '+ $('#hdd_persona_vb_nom').val(),
        content: `${texto}<p></p>${boton}`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'medium',
        buttons: {
            cancelar: function () {
            }
        }
    });
}

function revision_redaccion(ejecucionfase_id, documento, action2) {
    $.confirm({
        title: 'REVISION DE LA REDACCION',
        content: 'url:' + VISTA_URL + `ejecucion/ejecucion_revision_redaccion.php?ejecucionfase_id=${ejecucionfase_id}`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'medium',
        buttons: {
            guardar: {
                text: 'Guardar',
                btnClass: 'btn-blue btn_confirm_vb',
                action: function () {
                    var nueva_nota = this.$content.find('#txt_nueva_observacion_vb').val();
                    var todo_ok = this.$content.find('#che_visto_bueno').prop('checked');
                    
                    if (!nueva_nota) {
                        $.alert('<b>Ingrese una observacion</b>');
                        return false;
                    }
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action: 'comentario',
                            action2: 'insertar',
                            action3: action2,
                            ejecucionfase_id: ejecucionfase_id,
                            comentario: nueva_nota,
                            vista: 'visto_bueno_'+documento,
                            che_visto_bueno: todo_ok
                        }),
                        success: function (data) {
                            $.alert(`<b>${data.mensaje}</b>`);
                            actualizar_comentarios(ejecucionfase_id, action2);
                            if (todo_ok) {
                                actualizar_contenido_1fase(ejecucionfase_id);
                            }
                        },
                    });
                }
            },
            cancelar: function () {
            }
        }
    });
}

function ejecucionfasefile_tabla(ejecucionfase_id, action2, concluido, estadoejecucion_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucionupload/ejecucionfasefile_tabla_form.php",
        async: true,
        dataType: "html",
        data: ({
            ejecucionfase_id: ejecucionfase_id,
            action2: action2,
            concluido: concluido,
            estadoejecucion_id: estadoejecucion_id,
            ejecucion_id: $('#ejecucion_id').val()
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_listadoejecucionfasefiles_form').html(html);
            $('#modal_listadoejecucionfasefiles_form').modal('show');
            $('#modal_mensaje').modal('hide');
    
            modal_width_auto('modal_listadoejecucionfasefiles_form', 78);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_listadoejecucionfasefiles_form'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal("modal_listadoejecucionfasefiles_form", "limpiar");
        }
    });
}

function pagar(action, gasto_id, gastopago_id, vista, credito_id, ejecucion_id, ejecucionfase_id, archivo_nom, tabla_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastopago/gastopago_form.php",
        async: true,
        dataType: "html",
        data: {
            action: action,
            gasto_id: gasto_id,
            gastopago_id: gastopago_id,
            vista: vista,
            credito_id: credito_id,
            ejecucion_id: ejecucion_id,
            ejecucionfase_id: ejecucionfase_id,
            archivo_nom: archivo_nom,
            tabla_id: tabla_id
        },
        beforeSend: function () {
            $("#h3_modal_title").text("Cargando Formulario");
            $("#modal_mensaje").modal("show");
        },
        success: function (html) {
            $("#div_modal_pago_form").html(html);
            $("#modal_gastopago_form").modal("show");
            $("#modal_mensaje").modal("hide");

            modal_hidden_bs_modal("modal_gastopago_form", "limpiar"); //funcion encontrada en public/js/generales.js
            modal_width_auto("modal_gastopago_form", 30);
            modal_height_auto("modal_gastopago_form"); //funcion encontrada en public/js/generales.js
        },
    });
}

function verificar_conteo_dias(ejecucion_id, ejecucionfase_id, estadoejecucion_id) {
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'verificar_conteo_dias',
            ejecucion_id: ejecucion_id,
            ejecucionfase_id: ejecucionfase_id,
            estadoejecucion_id: estadoejecucion_id
        }),
        success: function (data) {
            if (data.estado == 1) {
                data.mensaje != '' ? $('#div_mensaje_verificacion').html(`<h3 style="color: #c7254e; margin: 0px; white-space: normal;">${data.mensaje}</h3>`) : $('#div_mensaje_verificacion').html(``);
            } else {
                console.log(data);
            }
        }
    });
}

function verificar_credito_suministro() {
    var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
    var credito_id = ejecucion_reg.tb_credito_id;
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'verificar_credito_suministro',
            credito_id: credito_id
        }),
        success: function (data) {
            if (data.estado == 1) {
                $('#div_mensaje_verificacion').html(``);
            } else {
                data.mensaje != '' ? $('#div_mensaje_verificacion').html(`<h3 style="color: #c7254e; margin: 0px; white-space: normal;">${data.mensaje}</h3>`) : $('#div_mensaje_verificacion').html(``);
            }
        }
    });
}

function subir_foto_comentario(procesofasecomentario_id, action2, ejecucionfase_completado) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucion/galeriacomentario_form.php",
        async: true,
        dataType: "html",
        data: ({
            procesofasecomentario_id: procesofasecomentario_id,
            action2: action2,
            ejecucionfase_completado: ejecucionfase_completado
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_galeriacomentario_form').html(html);
            $('#modal_galeriacomentario_form').modal('show');
            $('#modal_mensaje').modal('hide');
    
            modal_width_auto('modal_galeriacomentario_form', 45);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_galeriacomentario_form'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal("modal_galeriacomentario_form", "limpiar");
        }
    });
}

function ejecucionimg_form(ejecucion_id, ejecucionfase_id, credito_id, img_is, modulo_nom, action2, tabla_id2, nombre_doc, tabla_nom, modulo_subnom) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucionupload/ejecucionimg_form.php",
        async: true,
        dataType: "html",
        data: {
            action: 'I',
            ejecucion_id: ejecucion_id,
            img_is: img_is,//mod or new
            modulo_nom: modulo_nom,
            ejecucionfase_id: ejecucionfase_id,
            credito_id: credito_id,
            action2: action2,
            tabla_id2: tabla_id2,
            nombre_doc: nombre_doc,
            tabla_nom: tabla_nom,
            modulo_subnom: modulo_subnom,
            filestorage_des: encodeURI('Fotos de '+modulo_subnom+', '+tabla_nom+'_id: '+tabla_id2)
        },
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $("#div_modal_ejecucionfile_form").html(html);
            $("#modal_ejecucionupload_form").modal("show");
            $('#modal_mensaje').modal('hide');
            modal_hidden_bs_modal("modal_ejecucionupload_form", "limpiar");
        }
    });
}

function refresh_galeria(tabla_nom, tabla_id, modulo_subnom, tabla_id2, action2, ejecucionfase_completado) {
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'refresh_vista_galeria',
            tabla_nom: tabla_nom,
            tabla_id: tabla_id,
            modulo_subnom: modulo_subnom,
            tabla_id2: tabla_id2,
            action2: action2,
            ejecucionfase_completado: ejecucionfase_completado
        }),
        success: function (data) {
            $('#div_'+tabla_nom+'_'+modulo_subnom).html(data.html);
            if (parseInt(data.estado) == 0) {
                console.log(data.mensaje);
            }
        }
    });
}

function filestorage_eliminar(filestorage_id, tabla_nom, tabla_id, modulo_subnom, tabla_id2) {
    Swal.fire({
        title: '¿DESEA ELIMINAR EL ARCHIVO?',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: '<i class ="fa fa-home"></i> Confirmo!',
        showDenyButton: true,
        denyButtonText: '<i class="fa fa-bank"></i> No',
        confirmButtonColor: '#3b5998',
        denyButtonColor: '#21ba45',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "filestorage/filestorage_controller.php",
                async: true,
                dataType: "json",
                data: ({
                    action: 'eliminar',
                    hdd_filestorage_id: filestorage_id,
                }),
                beforeSend: function () {
                },
                success: function (data) {
                    if (parseInt(data.estado) == 1) {
                        notificacion_success(data.mensaje, 4000)
                        refresh_galeria(tabla_nom, tabla_id, modulo_subnom, tabla_id2, 'M_ejecucion', 0);
                    }
                    else
                        alerta_error('IMPORTANTE', data.mensaje)
                },
                complete: function (data) {
                },
                error: function (data) {
                }
            });
        } else if (result.isDenied) {

        }
    });

}

function carousel(modulo_nom, modulo_id, modulo_subnom, tabla_id2) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "templates/carousel.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: modulo_nom,
            modulo_id: modulo_id,
            modulo_subnom: modulo_subnom,
            tabla_id2: tabla_id2
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Galería');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            $('#div_modal_carousel').html(data);
            $('#modal_carousel_galeria').modal('show');

            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

// 2. FASE DE REDACCION CARTAS NOTARIALES
{
    //2.1.1. ESCRITURA PUBLICA
        function editar_datos_escr_pub(action, credito_id, ejecucionfase_id) {
            var url_EP = $('#btnEP_credito_'+credito_id).attr('href');
            var height = $(window).height() - ($(window).height()*0.21);
            var width = $(window).width() - ($(window).width()*0.61);
            $.ajax({
                type: 'POST',
                url: VISTA_URL + 'escriturapublica/escriturapublica_form.php',
                async: true,
                dataType: 'html',
                data: ({
                    action: action,
                    credito_id: credito_id,
                    ejecucionfase_id: ejecucionfase_id,
                    ejecucion_reg: $('#hdd_ejecucion_reg').val(),
                    url_EP: url_EP,
                    height: height,
                    width: width
                }),
                beforeSend: function () {
                    $('#h3_modal_title').text('Cargando Formulario');
                    $('#modal_mensaje').modal('show');
                },
                success: function (data) {
                    $('#div_modal_escrpubl_form').html(data);
                    $('#modal_escriturapublica_form').modal('show');
                    $('#modal_mensaje').modal('hide');

                    modal_width_auto('modal_escriturapublica_form', 80);
                    modal_height_auto("modal_escriturapublica_form");
                    modal_hidden_bs_modal("modal_escriturapublica_form", "limpiar");
                }
            });
        }
    //

    //2.1.2. CERTIFICADO REG VEH
        function certificado_tabla(ejecucion_id, ejecucionfase_id, credito_id, ejecucionfase_completado, estado_actual) {
            var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
            $.ajax({
                type: "POST",
                url: VISTA_URL + "certificadoregistralveh/certificadoregveh_tabla_form.php",
                async: true,
                dataType: "html",
                data: ({
                    ejecucion_id: ejecucion_id,
                    ejecucionfase_id: ejecucionfase_id,
                    credito_id: credito_id,
                    ejecucionfase_completado: ejecucionfase_completado,
                    estado_actual: estado_actual
                }),
                beforeSend: function () {
                    $('#h3_modal_title').text('Cargando Formulario');
                    $('#modal_mensaje').modal('show');
                },
                success: function (html) {
                    $('#div_modal_listadocertificados_form').html(html);
                    $('#modal_listadocertificadoregveh_form').modal('show');
                    $('#modal_mensaje').modal('hide');
            
                    modal_width_auto('modal_listadocertificadoregveh_form', 50);
                    //funcion js para agregar un largo automatico al modal, al abrirlo
                    modal_height_auto('modal_listadocertificadoregveh_form'); //funcion encontrada en public/js/generales.js
                    modal_hidden_bs_modal("modal_listadocertificadoregveh_form", "limpiar");
                }
            });
        }
    //

    // 2.2. GUARDAR DATOS DE EJECUCION
        {
            function cn_redaccion_guardardatos(ejecucion_id, ejecucionfase_id) {
                var moneda_selected = parseInt($('#cbo_moneda_pagar option:selected').val());
                var moneda_selected_nom = $('#cbo_moneda_pagar option:selected').text();
                var monto_ingresado = $('#txt_monto_liquidacion_cn_redaccion').val();
                var representante_ipdn_selected = parseInt($('#cbo_ipdn_rep_cn_redaccion option:selected').val());
                var representante_ipdn_selected_nom = $('#cbo_ipdn_rep_cn_redaccion option:selected').text();
                var cargo_repre_id = `&cargo_repre_id=`+$('#cbo_ipdn_rep_cn_redaccion option:selected').attr('data_cargoid');

                if (!monto_ingresado || moneda_selected < 1 || representante_ipdn_selected < 1) {
                    alerta_error("SISTEMA", "Faltan datos para rellenar");
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
                    async: true,
                    dataType: 'json',
                    data: serialize_form('form_datosejecucion_cn_redaccion', '&action='+$('#action_montoliquidacion_representante').val()+`&moneda_nom=${moneda_selected_nom}&rep_nom=${representante_ipdn_selected_nom}&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`+cargo_repre_id),
                    success: function (data) {
                        if (data.estado == 1) {
                            alerta_success("SISTEMA", data.mensaje);
                            disabled($('#txt_monto_liquidacion_cn_redaccion, #cbo_ipdn_rep_cn_redaccion'));
                            $('#cn_redaccion_guardardatos, #cn_redaccion_cancelareditardatos').hide();
                            $('#cn_redaccion_editardatos').show();
                            actualizar_contenido_1fase(ejecucionfase_id);
                        } else {
                            alerta_error("SISTEMA", data.mensaje);
                        }
                    }
                });
            }
            function cn_redaccion_editardatosejecucion() {
                $('#cn_redaccion_guardardatos, #cn_redaccion_cancelareditardatos').show();
                $('#cn_redaccion_editardatos').hide();
                $('#txt_monto_liquidacion_cn_redaccion, #cbo_ipdn_rep_cn_redaccion').removeAttr('disabled');
                $('#cn_redaccion_guardardatos').removeAttr('disabled').removeClass('disabled');
                monto_liq_orig = $('#txt_monto_liquidacion_cn_redaccion').val();
                repr_orig = $('#cbo_ipdn_rep_cn_redaccion').val();
            }
            function cn_redaccion_cancelareditardatosejecucion(){
                $('#txt_monto_liquidacion_cn_redaccion').val(monto_liq_orig);
                $('#cbo_ipdn_rep_cn_redaccion').val(repr_orig);
                $('#cn_redaccion_editardatos').show();
                $('#cn_redaccion_guardardatos, #cn_redaccion_cancelareditardatos').hide();
                disabled($('#txt_monto_liquidacion_cn_redaccion, #cbo_ipdn_rep_cn_redaccion'));
            }
        }
    //

    //2.3 CARTAS NOTARIALES
        function carta_not_lista(ejecucion_id, ejecucionfase_id, ejecucionfase_completado, vista) {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "cartanotarial/cartanotarial_tabla_form.php",
                async: true,
                dataType: "html",
                data: ({
                    ejecucion_id: ejecucion_id,
                    ejecucionfase_id: ejecucionfase_id,
                    ejecucionfase_completado: ejecucionfase_completado,
                    vista: vista
                }),
                beforeSend: function () {
                    $('#h3_modal_title').text('Cargando Formulario');
                    $('#modal_mensaje').modal('show');
                },
                success: function (html) {
                    $('#div_modal_listadocartas_form').html(html);
                    $('#modal_listadocartanotarial_form').modal('show');
                    $('#modal_mensaje').modal('hide');
            
                    modal_width_auto('modal_listadocartanotarial_form', 60);
                    //funcion js para agregar un largo automatico al modal, al abrirlo
                    modal_height_auto('modal_listadocartanotarial_form'); //funcion encontrada en public/js/generales.js
                    modal_hidden_bs_modal("modal_listadocartanotarial_form", "limpiar");
                }
            });
        }
    //
}
//

// 3. FASE DE VISTO BUENO A CARTAS NOTARIALES
{
    // 3.1. GUARDAR persona que da V° B°
    {
        function persona_vb_guardar(ejecucion_id, ejecucionfase_id) {
            var persona_vb = parseInt($('#cbo_persona_vb option:selected').val());
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_ejecucion`;
            var columna = `&columna=tb_ejecucion_revisionpersona_id`;
            var valor = `&valor=${persona_vb}`;
            var tipo_dato = `&tipo_dato=INT`;
            var persona_vb_selected_nom = `&persona_vb_nom=${$('#cbo_persona_vb option:selected').text()}`;

            if (persona_vb < 1) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            modificar_campo('form_persona_vb', action+tabla+columna+valor+tipo_dato+persona_vb_selected_nom+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function persona_vb_editar() {
            $('#persona_vb_guardar, #persona_vb_cancelareditar').show();
            $('#persona_vb_editar').hide();
            $('#cbo_persona_vb').removeAttr('disabled');
            $('#persona_vb_guardar').removeAttr('disabled').removeClass('disabled');
            persona_vb_orig = $('#cbo_persona_vb').val();
        }
        function persona_vb_cancelareditar(){
            $('#cbo_persona_vb').val(persona_vb_orig);
            $('#persona_vb_editar').show();
            $('#persona_vb_guardar, #persona_vb_cancelareditar').hide();
            disabled($('#cbo_persona_vb'));
        }
    }
    //
}
//

// 4. FASE DE LLEVAR CARTAS A LA NOTARIA PARA DILIGENCIA
{
    // 4.1. GUARDAR fecha definitiva de llevar la carta
        function cn_llevarnotaria_guardar(ejecucion_id, ejecucionfase_id) {
            var fecha_llevar_definitiva = $('#txt_cnllevarnotaria_fecha').val();
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_cartanotarial`;
            var columna = `&columna=tb_cartanotarial_fecnotariarecepciona`;
            var valor = `&valor=${fecha_llevar_definitiva}`;
            var tipo_dato = `&tipo_dato=STR`;

            if (!fecha_llevar_definitiva) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            modificar_campo('form_cnllevarnotaria_fecha', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function cn_llevarnotaria_editar() {
            style_alerta_cn_llevarnotaria_orig = $('#cn_llevarnotaria_crear_alerta').css('display');
            $('#cn_llevarnotaria_guardar, #cn_llevarnotaria_cancelareditar').show();
            $('#cn_llevarnotaria_editar').hide();
            $('#txt_cnllevarnotaria_fecha').removeAttr('disabled');
            $('#cn_llevarnotaria_guardar').removeAttr('disabled').removeClass('disabled');
            fecha_llevarnotaria_orig = $('#txt_cnllevarnotaria_fecha').val();
        }
        function cn_llevarnotaria_cancelareditar(){
            $('#txt_cnllevarnotaria_fecha').val(fecha_llevarnotaria_orig);
            $('#cn_llevarnotaria_editar').show();
            if (style_alerta_cn_llevarnotaria_orig != 'none') {
                $('#cn_llevarnotaria_crear_alerta').show();
            }
            $('#cn_llevarnotaria_guardar, #cn_llevarnotaria_cancelareditar').hide();
            disabled($('#txt_cnllevarnotaria_fecha'));
        }
    //
}
//

// 5. FASE DE CARTAS EN DILIGENCIA
{
    // 5.1. GUARDAR fecha RECOJO DE la carta
        function cn_diligencia_guardar(ejecucion_id, ejecucionfase_id) {
            var fecha_recojo = $('#txt_cndiligencia_fecrecojo').val();
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_cartanotarial`;
            var columna = `&columna=tb_cartanotarial_fecrecojo`;
            var valor = `&valor=${fecha_recojo}`;
            var tipo_dato = `&tipo_dato=STR`;

            if (!fecha_recojo) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            modificar_campo('form_cndiligencia_fecha', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function cn_diligencia_editar() {
            style_alerta_cn_diligencia_orig = $('#cn_diligencia_crear_alerta').css('display');
            $('#cn_diligencia_guardar, #cn_diligencia_cancelareditar').show();
            $('#cn_diligencia_editar, #cn_diligencia_crear_alerta').hide();
            $('#txt_cndiligencia_fecrecojo').removeAttr('disabled');
            $('#cn_diligencia_guardar').removeAttr('disabled').removeClass('disabled');
            fec_recojo_orig = $('#txt_cndiligencia_fecrecojo').val();
        }
        function cn_diligencia_cancelareditar(){
            $('#txt_cndiligencia_fecrecojo').val(fec_recojo_orig);
            $('#cn_diligencia_editar').show();
            if (style_alerta_cn_diligencia_orig != 'none') {
                $('#cn_diligencia_crear_alerta').show();
            }
            $('#cn_diligencia_guardar, #cn_diligencia_cancelareditar').hide();
            disabled($('#txt_cndiligencia_fecrecojo'));
        }
    //
}
//

// 6. FASE ESPERANDO CONTACTO DE CLIENTE
{
    // 6.1. GUARDAR fecha espera maxima contacto de cliente
        function esperacontacto_fecmax_guardar(ejecucion_id, ejecucionfase_id) {
            var fec_max_espera = $('#txt_espera_cliente_fecmax').val();
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_ejecucion`;
            var columna = `&columna=tb_ejecucion_fecmax_espera`;
            var valor = `&valor=${fec_max_espera}`;
            var tipo_dato = `&tipo_dato=STR`;

            if (!fec_max_espera) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            modificar_campo('form_esperacliente_fecha', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function esperacontacto_fecmax_editar() {
            style_alerta_esperacliente_orig = $('#esperacontacto_crear_alerta').css('display');
            $('#esperacontacto_fecmax_guardar, #esperacontacto_fecmax_cancelareditar').show();
            $('#esperacontacto_fecmax_editar, #esperacontacto_crear_alerta').hide();
            $('#txt_espera_cliente_fecmax').removeAttr('disabled');
            $('#esperacontacto_fecmax_guardar').removeAttr('disabled').removeClass('disabled');
            fec_max_espera_orign = $('#txt_espera_cliente_fecmax').val();
        }
        function esperacontacto_fecmax_cancelareditar(){
            $('#txt_espera_cliente_fecmax').val(fec_max_espera_orign);
            $('#esperacontacto_fecmax_editar').show();
            if (style_alerta_esperacliente_orig != 'none') {
                $('#esperacontacto_crear_alerta').show();
            }
            $('#esperacontacto_fecmax_guardar, #esperacontacto_fecmax_cancelareditar').hide();
            disabled($('#txt_espera_cliente_fecmax'));
        }
    //
}
//

// 7. fase demanda: redaccion
{
    function form_demanda(action, demanda_id, ejecucion_id, action2) {
        var ejecucionfase_id = $('#hdd_demandared_ejecucionfase_id').val();

        var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
        var credito_id = ejecucion_reg.tb_credito_id;
        var cgarvtipo_id = ejecucion_reg.tb_cgarvtipo_id;
        $.ajax({
            type: "POST",
            url: VISTA_URL + "demanda/demanda_form.php",
            async: true,
            dataType: "html",
            data: ({
                action: action,
                demanda_id: demanda_id,
                ejecucion_id: ejecucion_id,
                ejecucionfase_id: ejecucionfase_id,
                action2: action2,
                credito_id: credito_id,
                cgarvtipo_id: cgarvtipo_id
            }),
            beforeSend: function () {
                $('#h3_modal_title').text('Cargando Formulario');
                $('#modal_mensaje').modal('show');
            },
            success: function (html) {
                $('#div_modal_demanda_form').html(html);
                $('#modal_demanda_form').modal('show');
                $('#modal_mensaje').modal('hide');
        
                modal_width_auto('modal_demanda_form', 45);
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_demanda_form'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal("modal_demanda_form", "limpiar");
            }
        });
    }
}
//

// demanda: registrar gastos
{
    function form_demanda_gasto(action, demanda_id, tipo_gasto, ejecucionfase_id) {
        var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
        $.ajax({
            type: "POST",
            url: VISTA_URL + "demanda/demanda_gasto_form.php",
            async: true,
            dataType: "html",
            data: ({
                action: action,
                demanda_id: demanda_id,
                tipo_gasto: tipo_gasto,
                ejecucionfase_id: ejecucionfase_id,
                ejecucion_id: ejecucion_reg.tb_ejecucion_id,
                credito_id: ejecucion_reg.tb_credito_id,
                cliente_nom: ejecucion_reg.tb_cliente_nom
            }),
            beforeSend: function () {
                $('#h3_modal_title').text('Cargando Formulario');
                $('#modal_mensaje').modal('show');
            },
            success: function (html) {
                $('#div_modal_demanda_gasto_form').html(html);
                $('#modal_demanda_gasto_form').modal('show');
                $('#modal_mensaje').modal('hide');
        
                modal_width_auto('modal_demanda_gasto_form', 80);
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_demanda_gasto_form'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal("modal_demanda_gasto_form", "limpiar");
            }
        });
    }
}
//

// demanda: en mesa de partes del pj
{
    function demanda_mesapartes_guardar(ejecucion_id, ejecucionfase_id) {
        if (!$('#txt_ejecucion_nroexpediente').val() || !$('#txt_ejecucion_especialista').val() || !$('#txt_ejecucion_nombrejuez').val() || parseInt($('#cbo_ejecucion_juzgado').val()) < 1 || !$('#txt_ejecucion_demanda_fec_pres').val() /* || !$('#txt_ejecucion_demanda_hora_pres').val() */) {
            alerta_error("SISTEMA", "Faltan datos para rellenar");
            return;
        }
        var ejecucion = `&ejecucion_id=${ejecucion_id}`;
        var ejecucionfase = `&ejecucionfase_id=${ejecucionfase_id}`;
        $.ajax({
            type: 'POST',
            url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
            async: true,
            dataType: 'json',
            data: serialize_form('form_demanda_mesapartes', '&action='+$('#action_demanda_mesapartes').val()+ejecucion+ejecucionfase),
            success: function (data) {
                if (data.estado == 1) {
                    alerta_success("SISTEMA", data.mensaje);
                    disabled($('#txt_ejecucion_nroexpediente, #txt_ejecucion_especialista, #txt_ejecucion_nombrejuez, #cbo_ejecucion_juzgado, #txt_ejecucion_demanda_fec_pres, #txt_ejecucion_demanda_hora_pres'));
                    $('#demanda_mesapartes_guardar, #demanda_mesapartes_cancelareditar').hide();
                    $('#demanda_mesapartes_editar').show();
                    actualizar_contenido_1fase(ejecucionfase_id);
                } else {
                    alerta_error("SISTEMA", data.mensaje);
                }
            }
        });
    }
    function demanda_mesapartes_editar() {
        $('#demanda_mesapartes_guardar, #demanda_mesapartes_cancelareditar').show();
        $('#demanda_mesapartes_editar').hide();
        $('#txt_ejecucion_nroexpediente, #txt_ejecucion_especialista, #txt_ejecucion_nombrejuez, #cbo_ejecucion_juzgado, #txt_ejecucion_demanda_fec_pres, #txt_ejecucion_demanda_hora_pres').removeAttr('disabled');
        $('#demanda_mesapartes_guardar').removeAttr('disabled').removeClass('disabled');
        mesapartes_nroexpediente_orig = $('#txt_ejecucion_nroexpediente').val();
        mesapartes_espec_origin = $('#txt_ejecucion_especialista').val();
        mesapartes_juez_origin = $('#txt_ejecucion_nombrejuez').val();
        mesapartes_juzgado_origin = $('#cbo_ejecucion_juzgado').val();
        mesapartes_fecpres_origin = $('#txt_ejecucion_demanda_fec_pres').val();
        /* mesapartes_horapres_origin = $('#txt_ejecucion_demanda_hora_pres').val(); */
    }
    function demanda_mesapartes_cancelareditar(){
        $('#txt_ejecucion_nroexpediente').val(mesapartes_nroexpediente_orig);
        $('#txt_ejecucion_especialista').val(mesapartes_espec_origin);
        $('#txt_ejecucion_nombrejuez').val(mesapartes_juez_origin);
        $('#cbo_ejecucion_juzgado').val(mesapartes_juzgado_origin);
        $('#txt_ejecucion_demanda_fec_pres').val(mesapartes_fecpres_origin);
        /* $('#txt_ejecucion_demanda_hora_pres').val(mesapartes_horapres_origin); */
        $('#demanda_mesapartes_editar').show();
        $('#demanda_mesapartes_guardar, #demanda_mesapartes_cancelareditar').hide();
        disabled($('#txt_ejecucion_nroexpediente, #txt_ejecucion_especialista, #txt_ejecucion_nombrejuez, #cbo_ejecucion_juzgado, #txt_ejecucion_demanda_fec_pres, #txt_ejecucion_demanda_hora_pres'));
    }
}
//

// incautacion: C.E.J. por resolucion y recojo oficio
{
    // GUARDAR fecha recojo de oficio incautacion
        function oficiorecojo_guardar(ejecucion_id, ejecucionfase_id) {
            var fec_oficiorecojo = $('#txt_oficiorecojo_fecha').val();
            var hora_oficiorecojo = $('#txt_oficiorecojo_hora').val();

            var fec_hora_oficiorecojo = fec_oficiorecojo+' '+hora_oficiorecojo;
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_ejecucion`;
            var columna = `&columna=tb_ejecucion_oficioincau_fecrecojo`;
            var valor = `&valor=${fec_hora_oficiorecojo}`;
            var tipo_dato = `&tipo_dato=STR`;

            if (!fec_oficiorecojo || !hora_oficiorecojo) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            modificar_campo('form_oficiorecojo', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function oficiorecojo_editar() {
            style_alerta_oficiorecojo_orig = $('#oficiorecojo_crear_alerta').css('display');
            $('#oficiorecojo_guardar, #oficiorecojo_cancelareditar').show();
            $('#oficiorecojo_editar, #oficiorecojo_crear_alerta').hide();
            $('#txt_oficiorecojo_fecha, #txt_oficiorecojo_hora').removeAttr('disabled');
            $('#oficiorecojo_guardar').removeAttr('disabled').removeClass('disabled');
            oficiorecojo_fec_orig = $('#txt_oficiorecojo_fecha').val();
            oficiorecojo_hora_orig = $('#txt_oficiorecojo_hora').val();
        }
        function oficiorecojo_cancelareditar(){
            $('#txt_oficiorecojo_fecha').val(oficiorecojo_fec_orig);
            $('#txt_oficiorecojo_hora').val(oficiorecojo_hora_orig);
            $('#oficiorecojo_editar').show();
            if (style_alerta_oficiorecojo_orig != 'none') {
                $('#oficiorecojo_crear_alerta').show();
            }
            $('#oficiorecojo_guardar, #oficiorecojo_cancelareditar').hide();
            disabled($('#txt_oficiorecojo_fecha, #txt_oficiorecojo_hora'));
        }
    //
}
//

// INCAUTACION: CAPTURA DEL VEHICULO
{
    // GUARDAR fecha recojo de oficio DESCERRAJE
        function oficiodescerrecojo_guardar(ejecucion_id, ejecucionfase_id) {
            var fec_oficiodescer_recojo = $('#txt_oficiodescer_recojo_fecha').val();
            var hora_oficiodescer_recojo = $('#txt_oficiodescer_recojo_hora').val();

            var fec_hora_oficiodescer_recojo = fec_oficiodescer_recojo+' '+hora_oficiodescer_recojo;
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_ejecucion`;
            var columna = `&columna=tb_ejecucion_oficiodescer_fecrecojo`;
            var valor = `&valor=${fec_hora_oficiodescer_recojo}`;
            var tipo_dato = `&tipo_dato=STR`;

            if (!fec_oficiodescer_recojo || !hora_oficiodescer_recojo) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            
            modificar_campo('form_oficiodescer_recojo', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function oficiodescerrecojo_editar() {
            style_alerta_oficiodescerrecojo_orig = $('#oficiodescerrecojo_crear_alerta').css('display');
            $('#oficiodescerrecojo_guardar, #oficiodescerrecojo_cancelareditar').show();
            $('#oficiodescerrecojo_editar, #oficiodescerrecojo_crear_alerta').hide();
            $('#txt_oficiodescer_recojo_fecha, #txt_oficiodescer_recojo_hora').removeAttr('disabled');
            $('#oficiodescerrecojo_guardar').removeAttr('disabled').removeClass('disabled');
            oficiodescerrecojo_fec_orig = $('#txt_oficiodescer_recojo_fecha').val();
            oficiodescerrecojo_hora_orig = $('#txt_oficiodescer_recojo_hora').val();
        }
        function oficiodescerrecojo_cancelareditar(){
            $('#txt_oficiodescer_recojo_fecha').val(oficiodescerrecojo_fec_orig);
            $('#txt_oficiodescer_recojo_hora').val(oficiodescerrecojo_hora_orig);
            $('#oficiodescerrecojo_editar').show();
            if (style_alerta_oficiodescerrecojo_orig != 'none') {
                $('#oficiodescerrecojo_crear_alerta').show();
            }
            $('#oficiodescerrecojo_guardar, #oficiodescerrecojo_cancelareditar').hide();
            disabled($('#txt_oficiodescer_recojo_fecha, #txt_oficiodescer_recojo_hora'));
        }
    //
}
//

// LEVANTAMIENTO ORDEN CAPTURA
{
    // GUARDAR fecha recojo de oficio de orden de captura
        function ofilevordencaptrecojo_guardar(ejecucion_id, ejecucionfase_id) {
            var ofilevordencaptrecojo_fec = $('#txt_ofilevordencapt_recojo_fecha').val();
            var ofilevordencaptrecojo_hora = $('#txt_ofilevordencapt_recojo_hora').val();

            var fec_ofilevordencaptrecojo_hora = ofilevordencaptrecojo_fec+' '+ofilevordencaptrecojo_hora;
            var action = `action=modificar_campo`;
            var tabla = `&tabla=tb_ejecucion`;
            var columna = `&columna=tb_ejecucion_oficiolevanincau_fecrecojo`;
            var valor = `&valor=${fec_ofilevordencaptrecojo_hora}`;
            var tipo_dato = `&tipo_dato=STR`;

            if (!ofilevordencaptrecojo_fec || !ofilevordencaptrecojo_hora) {
                alerta_error("SISTEMA", "Faltan datos para rellenar");
                return;
            }
            
            modificar_campo('form_ofilevordencapt_recojo', action+tabla+columna+valor+tipo_dato+`&ejecucion_id=${ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}`, ejecucionfase_id);
        }
        function ofilevordencaptrecojo_editar() {
            style_alerta_ofilevordencaptrecojo_orig = $('#ofilevordencaptrecojo_crear_alerta').css('display');
            $('#ofilevordencaptrecojo_guardar, #ofilevordencaptrecojo_cancelareditar').show();
            $('#ofilevordencaptrecojo_editar, #ofilevordencaptrecojo_crear_alerta').hide();
            $('#txt_ofilevordencapt_recojo_fecha, #txt_ofilevordencapt_recojo_hora').removeAttr('disabled');
            $('#ofilevordencaptrecojo_guardar').removeAttr('disabled').removeClass('disabled');
            ofilevordencaptrecojo_fec_orig = $('#txt_ofilevordencapt_recojo_fecha').val();
            ofilevordencaptrecojo_hora_orig = $('#txt_ofilevordencapt_recojo_hora').val();
        }
        function ofilevordencaptrecojo_cancelareditar(){
            $('#txt_ofilevordencapt_recojo_fecha').val(ofilevordencaptrecojo_fec_orig);
            $('#txt_ofilevordencapt_recojo_hora').val(ofilevordencaptrecojo_hora_orig);
            $('#ofilevordencaptrecojo_editar').show();
            if (style_alerta_ofilevordencaptrecojo_orig != 'none') {
                $('#ofilevordencaptrecojo_crear_alerta').show();
            }
            $('#ofilevordencaptrecojo_guardar, #ofilevordencaptrecojo_cancelareditar').hide();
            disabled($('#txt_ofilevordencapt_recojo_fecha, #txt_ofilevordencapt_recojo_hora'));
        }
    //
}
//

/* $.ajax({
    type: 'POST',
    url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
    async: true,
    dataType: 'json',
    data: ({
        
    }),
    success: function (data) {
        if (data.estado == 1) {
            alerta_success("SISTEMA", data.mensaje);
        } else {
            alerta_error("SISTEMA", data.mensaje);
        }
    }
}); */

/* $.ajax({
    type: "POST",
    url: VISTA_URL + "precredito/precredito_form.php",
    async: true,
    dataType: "html",
    data: ({
        action: action,
        precred_id: precred_id,
        concluido: concluido
    }),
    beforeSend: function () {
        $('#h3_modal_title').text('Cargando Formulario');
        $('#modal_mensaje').modal('show');
    },
    success: function (html) {
        $('#div_modal_precredito_form').html(html);
        $('#modal_precredito_form').modal('show');
        $('#modal_mensaje').modal('hide');

        modal_width_auto('modal_precredito_form', 45);
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_precredito_form'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_demanda_form", "limpiar");
    }
}); */