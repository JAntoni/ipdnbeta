<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();
require_once '../filestorage/Filestorage.class.php';
$oFilestorage = new Filestorage();
$listo = 1;

$usuario_action = $_POST['action'];

$titulo = 'Comentario y Galeria del comentario';
$action = devuelve_nombre_usuario_action($usuario_action);

if ($listo == 1) {
    $procesofasecomentario_id = $_POST['procesofasecomentario_id'];
    $res_comentario = $oEjecucion->listar_ejecucionfase_comentario_por('', $procesofasecomentario_id)['data'][0];

    $action2 = $_POST['action2'];
    $ejecucionfase_completado = $_POST['ejecucionfase_completado'];
    $ejecucion_id = $res_comentario['tb_ejecucion_id'];
    $ejecucionfase_id = $res_comentario['tb_procesofase_id'];
    $credito_id = $res_comentario['tb_credito_id'];
    $modulo_nom = 'ejecucion';
    $tabla_id2 = $res_comentario['tb_procesofase_comentario_id'];
    $nombre_doc = 'Foto de comentario';
    
    $readonly = ''; $disabled = '';
    if (intval($ejecucionfase_completado) == 1 || $action2 == 'L_ejecucion') {
        $readonly = 'readonly';
        $disabled = 'disabled';
    }
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_galeriacomentario_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_procesofasecomentario" method="post">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="action_procesofasecomentario" id="action_procesofasecomentario" value="guardar_comentario">
                                <input type="hidden" name="hdd_ejecucionfase_id" id="hdd_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                                <input type="hidden" name="hdd_procesofasecomentario_id" id="hdd_procesofasecomentario_id" value="<?php echo $procesofasecomentario_id; ?>">
                                <input type="hidden" name="hdd_procesofasecomentario_reg" id="hdd_procesofasecomentario_reg" value='<?php echo json_encode($res_comentario); ?>'>
                                <input type="hidden" name="hdd_procesofasecomentario_action2" id="hdd_procesofasecomentario_action2" value="<?php echo $action2; ?>">
                                
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="txt_procesofasecomentario_comentario">Comentario:</label>
                                        <textarea name="txt_procesofasecomentario_comentario" id="txt_procesofasecomentario_comentario" class="form-control input-sm input-shadow" rows="3" <?php echo $readonly;?>><?php echo $res_comentario['tb_comentario_des']; ?></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" align="right">
                                        <a type="button" onclick="editar_comentario()" class="btn btn-info <?php echo $disabled;?>" id="btn_editar_comentario" name="btn_editar_comentario">Guardar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!-- segunda seccion -->
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-primary <?php echo $disabled;?>" onclick="ejecucionimg_form(<?php echo "$ejecucion_id, $ejecucionfase_id, $credito_id,"; ?> 'new', 'ejecucion', <?php echo "'$action2', $procesofasecomentario_id, '', 'tb_procesofase_comentario', 'comentario_galeria$procesofasecomentario_id'"; ?>)"><i class="fa fa-plus"></i> Subir Imagenes</button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div id="div_ejecucion_comentario_galeria" class="col-md-12">
                                        <!-- script para mostrar imagenes -->
                                        <script>setTimeout(function(){
                                            refresh_galeria(<?php echo "'$modulo_nom', $ejecucion_id, 'comentario_galeria', $procesofasecomentario_id, '$action2', $ejecucionfase_completado" ?>);
                                        }, 200);
                                        setTimeout(function(){
                                            disabled($('#form_procesofasecomentario').find('.disabled'));
                                        }, 300);</script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_procesofasecomentario">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_procesofasecomentario">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
