<?php
require_once './vista/ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();

if (!empty($_POST["cgvsubtipo_id"])){
	$cgvsubtipo_id = $_POST["cgvsubtipo_id"];
}
if (!empty($_POST["cgarvtipo_id"])){
	$cgarvtipo_id = $_POST["cgarvtipo_id"];
}
if (!empty($_POST["cgvsubtipo_adenda"])){
	$cgvsubtipo_adenda = $_POST["cgvsubtipo_adenda"];
}

$option = '<option value="0">Todos</option>';

//PRIMER NIVEL
$result = $oEjecucion->buscar_cgvsubtipo_por('', $cgarvtipo_id, $cgvsubtipo_adenda);
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($cgvsubtipo_id == $value['tb_cgvsubtipo_id'])
			$selected = 'selected';
		//

		$option .= '<option value="'.$value['tb_cgvsubtipo_id'].'" data-cgarvtipo="'.$value['cgarvtipo_id'].'" '.
			'data-cgvsubtipo_adenda="'.$value['tb_cgvsubtipo_adenda'].'" data-cgvsubtipo_xac="'.$value['tb_cgvsubtipo_xac'].'" '.$selected.'>'.$value['tb_cgvsubtipo_nom'].'</option>';
		//
	}
}
unset($result);
//FIN PRIMER NIVEL
echo $option;
