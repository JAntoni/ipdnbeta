<?php
require_once '../../core/usuario_sesion.php';
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();
require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();
require_once '../funciones/fechas.php';

//recibir los filtros
$cliente_id = empty($_POST['hdd_fil_cliente_id']) ? '' : intval($_POST['hdd_fil_cliente_id']);
$fec_ini = empty($_POST['txt_fil_fec1']) ? '' : fecha_mysql($_POST['txt_fil_fec1']);
$fec_fin = empty($_POST['txt_fil_fec2']) ? '' : fecha_mysql($_POST['txt_fil_fec2']);
$encargado_id = empty($_POST['txt_fil_encargado_id']) ? '' : intval($_POST['txt_fil_encargado_id']);
$cgvsubtipo_id = empty($_POST['cbo_fil_cgvsubtipo_id']) ? '' : intval($_POST['cbo_fil_cgvsubtipo_id']);
$estadoejec_cgvsubtipo_id = empty($_POST['cbo_fil_estadoejecucion_cgvsubtipo_id']) ? '' : intval($_POST['cbo_fil_estadoejecucion_cgvsubtipo_id']);
$fil_estado_proceso = $_POST['cbo_fil_estadoejecucion'];
if (!empty($cgvsubtipo_id)) {
    $oEjecucion->cgvsubtipo_id = $cgvsubtipo_id;
}
if (!empty($estadoejec_cgvsubtipo_id)) {
    $oEjecucion->estadoejecucion_cgvsubtipo_id = $estadoejec_cgvsubtipo_id;
}
if (!empty($encargado_id)) {
    $oEjecucion->usuencargado = $encargado_id;
}
if ($fil_estado_proceso != 'all') {
    $oEjecucion->estadoproceso = intval($fil_estado_proceso);
}

$result = $oEjecucion->listar_todos('', $cliente_id, '', $fec_ini, $fec_fin, '');

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        //encontrar el id de su ultima ejecucionfase
        $last_fase = $oEjecucion->listar_ejecucionfase_por('', $value['tb_ejecucion_id'], '', 'ef.tb_ejecucionfase_fecreg DESC', 'LIMIT 1')['data'][0];
        $last_fase_id = $last_fase['tb_ejecucionfase_id'];
        $last_fase_fecha = $last_fase['tb_ejecucionfase_fecreg'];

        $cliente_concat = "{$value['tb_cliente_doc']} - {$value['tb_cliente_nom']}";
        if (intval($value['tb_cliente_tip']) == 2) {
            $cliente_concat .= ", {$value['tb_cliente_empruc']} - {$value['tb_cliente_emprs']}";
        }

        $estado_proceso = ['SUSPENDIDO', 'ACTIVO', 'ANULADO', 'TERMINADO'];
        $juzgado = ['', '3', '8'];
        $nombre_juzgado = empty($value['tb_ejecucion_juzgado']) ? '' : $juzgado[$value['tb_ejecucion_juzgado']] . '° JUZGADO CIVIL';

        $seg_boton = '';
        $action_def = 'L_ejecucion';
        if (intval($value['tb_ejecucion_estadoproceso']) == 1 && ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 42 || $_SESSION['usuario_id'] == $value['tb_ejecucion_usuencargado'])) {
            $action_def = 'M_ejecucion';
            $seg_boton = '<a class="btn btn-danger btn-xs" title="Suspender" onclick="cambiar_estado('.$value['tb_ejecucion_id'].', '.$last_fase_id.', 0, \'suspender\')">Suspender</a>';
        } elseif (intval($value['tb_ejecucion_estadoproceso']) == 0) {
            $seg_boton = '<a class="btn btn-success btn-xs" title="Reactivar" onclick="cambiar_estado('.$value['tb_ejecucion_id'].', '.$last_fase_id.', 1, \'reactivar\')">Reactivar</a>';
        }

        //CONTEO DIAS
            $color_celda = $dias_pasaron = $text_add = '';
            if ($value['tb_ejecucion_estadoproceso'] == 1) {
                if ($value['tb_estadoejecucion_id'] == 11) {
                    $res = $oEjecucionfasefile->listar('', $last_fase_id, '', 'not null', 1, '', '', '');
                    if ($res['estado'] != 1) { //si no hay ningun archivo subido, debe revisar si hay alguna respuesta en pag CEJ
                        $demanda_fec_pres = $value['tb_ejecucion_demanda_fechora_presentacion'];
                        $dias_pasaron = restaFechas(fecha_mysql($demanda_fec_pres), date('Y-m-d'));

                        $text_add = '(Revise C.E.J., pasó ['.$dias_pasaron.'] dias desde la presentacion de demanda)';
                    }
                    else {
                        $ultimo_estado = '';
                        foreach ($res['data'] as $key => $documento) {
                            if ($documento['tb_ejecucionfasefile_resol_est'] == 2) {
                                $ultimo_estado = 'inadmisible';
                            }
                            if ($documento['tb_ejecucionfasefile_resol_est'] == 1) {
                                $ultimo_estado = 'completo';
                                break;
                            }
                        }
                        
                        if ($ultimo_estado == 'inadmisible') {
                            //revisar si ya se hizo la presentacion de subsanacion
                            $falta_escrito_subsanacion = 1;
                            $res1 = $oEjecucionfasefile->listar('', $last_fase_id, '', 'not null', 2, '', 1, '');
                            if ($res1['estado'] == 1) {
                                $falta_escrito_subsanacion = 0;
                
                                //realizar el conteo de dias sin respuesta al escrito de subsanacion
                                $escrito_fec_pres = !empty($res1['data'][0]['tb_ejecucionfasefile_fecnoti']) ? $res1['data'][0]['tb_ejecucionfasefile_fecnoti'] : $res1['data'][0]['upload_reg'];
                                
                                $dias_pasaron = restaFechas(fecha_mysql($escrito_fec_pres), date('Y-m-d'));
                                $text_add = '(Revise C.E.J., pasó ['.$dias_pasaron.'] dias desde la subida de la subsanación)';
                            }
                
                            if ($falta_escrito_subsanacion == 1) {
                                $text_add = '(Redacte escrito de subsanación)';
                                $color_celda = 'background-color: #ff6486';//rojo
                            }
                            
                            unset($res1);
                        }
                    }
                    unset($res);
                }
                elseif ($value['tb_estadoejecucion_id'] == 13) {
                    //el conteo de dias en estadoejecucion 13 solo se hace si sube escrito descerraje. lo buscamos
                    $res_escrito_desce = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], 'not null', 2, '', 2, '');
                    if ($res_escrito_desce['estado'] == 1) {
                        //si está subido un pdf de escrito descerraje entonces busquemos una resolucion de respuesta al descerraje(o sea dentro de esta ejecucionfase)
                        $resolucion_respuesta = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], 'not null', 1, '', '', '');
                        if ($resolucion_respuesta['estado'] != 1) {
                            //buscar cuándo se subio el escrito descerraje
                            $escrito_desce_fec_upload = $res_escrito_desce['data'][0]['tb_ejecucionfasefile_fecnoti'];
                            $dias_pasaron = restaFechas(fecha_mysql($escrito_desce_fec_upload), date('Y-m-d'));
                
                            $text_add = '(Revise C.E.J., pasó ['.$dias_pasaron.'] desde la subida de '.$res_escrito_desce['data'][0]['tb_ejecucionfasefile_archivonom'].')';
                        } else {
                            $ultimo_estado = '';
                            foreach ($resolucion_respuesta['data'] as $key => $documento) {
                                if ($documento['tb_ejecucionfasefile_resol_est'] == 2) {
                                    $ultimo_estado = 'inadmisible';
                                }
                                if ($documento['tb_ejecucionfasefile_resol_est'] == 1) {
                                    $ultimo_estado = 'completo';
                                    break;
                                }
                            }
            
                            if ($ultimo_estado == 'inadmisible') {
                                //revisar si ya se hizo la presentacion de subsanacion
                                $falta_escrito_subsanacion = 1;
                                $res1 = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], 'not null', 2, '', 1, '');
                                if ($res1['estado'] == 1) {
                                    $falta_escrito_subsanacion = 0;
            
                                    //realizar el conteo de dias sin respuesta al escrito de subsanacion
                                    $escrito_fec_pres = !empty($res1['data'][0]['tb_ejecucionfasefile_fecnoti']) ? $res1['data'][0]['tb_ejecucionfasefile_fecnoti'] : $res1['data'][0]['upload_reg'];
            
                                    $dias_pasaron = restaFechas(fecha_mysql($escrito_fec_pres), date('Y-m-d'));
                                    $text_add = "(Revise C.E.J., pasó [$dias_pasaron] días desde la subida de {$res1['data'][0]['tb_ejecucionfasefile_archivonom']})";
                                }
            
                                if ($falta_escrito_subsanacion == 1) {
                                    $text_add = '(Falta Redaccion de Escrito de subsanación)';
                                    $color_celda = 'background-color: #ff6486';//rojo
                                }
                                
                                unset($res1);
                            }
                        }
                    }
                    else {
                        $dias_pasaron = restaFechas(fecha_mysql($last_fase_fecha), date('Y-m-d'));
                        $text_add = '(hace ['.$dias_pasaron.'] días)';
                    }
                }
                elseif ($value['tb_estadoejecucion_id'] == 14) {
                    //buscar si ya se presentó el escrito de lev. ord. incau.
                    $res_esc_lev_ord_incau = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], 'not null', 2, '', 4, '');
                    if ($res_esc_lev_ord_incau['estado'] == 1) {
                        //si está subido un pdf de escrito de lev. ord. incau. entonces busquemos una resolucion de respuesta al escrito(o sea dentro de esta ejecucionfase)
                        $resolucion_respuesta = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], 'not null', 1, '', '', '');
                        if ($resolucion_respuesta['estado'] != 1) {
                            //buscar cuándo se subio el escrito de lev. ord. incau.
                            $escrito_lev_orden_incau_fec_upload = $res_esc_lev_ord_incau['data'][0]['tb_ejecucionfasefile_fecnoti'];
                            $dias_pasaron = restaFechas(fecha_mysql($escrito_lev_orden_incau_fec_upload), date('Y-m-d'));

                            $text_add = '(Revise C.E.J., pasó ['.$dias_pasaron.'] desde la subida de '.$res_esc_lev_ord_incau['data'][0]['tb_ejecucionfasefile_archivonom'].')';
                        } else {
                            $ultimo_estado = '';
                            foreach ($resolucion_respuesta['data'] as $key => $documento) {
                                if ($documento['tb_ejecucionfasefile_resol_est'] == 2) {
                                    $ultimo_estado = 'inadmisible';
                                }
                                if ($documento['tb_ejecucionfasefile_resol_est'] == 1) {
                                    $ultimo_estado = 'completo';
                                    break;
                                }
                            }
            
                            if ($ultimo_estado == 'inadmisible') {
                                //revisar si ya se hizo la presentacion de subsanacion
                                $falta_escrito_subsanacion = 1;
                                $res1 = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], 'not null', 2, '', 1, '');
                                if ($res1['estado'] == 1) {
                                    $falta_escrito_subsanacion = 0;
            
                                    //realizar el conteo de dias sin respuesta al escrito de subsanacion
                                    $escrito_fec_pres = !empty($res1['data'][0]['tb_ejecucionfasefile_fecnoti']) ? $res1['data'][0]['tb_ejecucionfasefile_fecnoti'] : $res1['data'][0]['upload_reg'];
            
                                    $dias_pasaron = restaFechas(fecha_mysql($escrito_fec_pres), date('Y-m-d'));
                                    $text_add = "(Revise C.E.J., pasó [$dias_pasaron] días desde la subida de {$res1['data'][0]['tb_ejecucionfasefile_archivonom']})";
                                }
            
                                if ($falta_escrito_subsanacion == 1) {
                                    $text_add .= '(Falta la redaccion del escrito de subsanación)';
                                    $color_celda = 'background-color: #ff6486';//rojo
                                }
                                
                                unset($res1);
                            }
                        }
                    } else {
                        $dias_pasaron = restaFechas(fecha_mysql($last_fase_fecha), date('Y-m-d'));
                        $text_add = '(Redacte Escrito de Lev. Orden Incau.)';
                    }
                }

                if ($ultimo_estado == 'completo' || !in_array($value['tb_estadoejecucion_id'], [11, 13, 14, 15])) {
                    $dias_pasaron = restaFechas(fecha_mysql($last_fase_fecha), date('Y-m-d'));
                    $text_add = '(hace ['.$dias_pasaron.'] días)';
                }

                if (is_numeric($dias_pasaron)) {
                    $color_celda = 'background-color: #8cd25c';//verde
                    if (in_array($value['tb_estadoejecucion_id'], [1, 2, 3, 4, 5, 12])) {
                        if ($dias_pasaron > 1) {
                            $color_celda = 'background-color: #ff6486';//rojo
                        }
                        elseif ($dias_pasaron > 0) {
                            $color_celda = 'background-color: #ffa141';//naranja
                        }
                    }
                    elseif ($value['tb_estadoejecucion_id'] == 6) {
                        if ($dias_pasaron > 3) {
                            $color_celda = 'background-color: #ff6486';//rojo
                        }
                        elseif ($dias_pasaron == 3) {
                            $color_celda = 'background-color: #ffa141';//naranja
                        }
                        elseif ($dias_pasaron == 2) {
                            $color_celda = 'background-color: #ffe158';//amarillo
                        }
                    }
                    elseif (in_array($value['tb_estadoejecucion_id'], [7, 8, 9])) {
                        if ($dias_pasaron > 0) {
                            $color_celda = 'background-color: #ff6486';//rojo
                        }
                    }
                    elseif ($value['tb_estadoejecucion_id'] == 10) {
                        if ($dias_pasaron == 1) {
                            $color_celda = 'background-color: #ffe158';//amarillo
                        }
                        elseif ($dias_pasaron > 1) {
                            $color_celda = 'background-color: #ff6486';//rojo
                        }
                    }
                    else {
                        if ($dias_pasaron > 6) {
                            $color_celda = 'background-color: #ff6486';//rojo
                        }
                        elseif ($dias_pasaron > 4) {
                            $color_celda = 'background-color: #ffa141';//naranja
                        }
                        elseif ($dias_pasaron > 2) {
                            $color_celda = 'background-color: #ffe158';//amarillo
                        }
                    }
                }
            }
        //

        $checkbox_reasignar = ''; $ejec_reg_hidden = ''; $ejecfase_id_hidden = '';
        if (($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 42) && in_array(intval($value['tb_ejecucion_estadoproceso']), array(0, 1))) { //usuarios admin o gio
            $checkbox_reasignar = '<input type="checkbox" style="margin: 1.5px 0px 0px -12px;" id="che_ejec_'. $value['tb_ejecucion_id'] .'" name="che_ejec_'. $value['tb_ejecucion_id'] .'" value="1">&nbsp; ';
            $ejec_reg_hidden = '<input type="hidden" name="hdd_ejecreg_'. $value['tb_ejecucion_id'] .'" id="hdd_ejecreg_'. $value['tb_ejecucion_id'] .'" value=\''. json_encode($value) .'\'>';
            $ejecfase_id_hidden = '<input type="hidden" name="hdd_ejecfaseid_'. $value['tb_ejecucion_id'] .'" id="hdd_ejecfaseid_'. $value['tb_ejecucion_id'] .'" value=\''. $last_fase_id .'\'>';
        }

        // GERSON (18-12-24) - se agrega botón para ver gastos incurrido en la ejecución
        $gastos_boton = '';
        $gastos_boton = '<a class="btn bg-purple btn-xs" title="Suspender" onclick="ver_gastos_ejecucion('.$value['tb_ejecucion_id'].', '.$last_fase_id.', 0, \'ejecucion\')"><i class="fa fa-money"></i> Gastos</a>';
        //

        $tr .= '<tr>';
        $tr .=
            $ejec_reg_hidden.$ejecfase_id_hidden.
            '<td id="tabla_fila" align="center">' . $checkbox_reasignar. $value['tb_ejecucion_id'] . '</td>'.
            '<td id="tabla_fila" align="center">' . $value['tb_usuario_nom']. ' '. $value['tb_usuario_ape'] . '</td>'.
            '<td id="tabla_fila" align="center">' . mostrar_fecha_hora($value['tb_ejecucion_fecreg']) . '</td>'.
            '<td id="tabla_fila" style="font-weight: bold;">' . $cliente_concat . '</td>'.
            '<td id="tabla_fila" align="center">' . $value['tb_credito_id'] . '</td>'.
            '<td id="tabla_fila" align="center">' . strtoupper($value['tb_credito_vehpla']) . '</td>'.
            '<td id="tabla_fila" align="center">' . $value['tb_cgvsubtipo_nom'] . '</td>'.
            '<td id="tabla_fila" align="center" style="font-weight: bold; '.$color_celda.'">' . $value['estado_actual'] . " $text_add" . '</td>'.
            '<td id="tabla_fila" align="center">' . mostrar_fecha_hora($last_fase_fecha) . '</td>'.
            '<td id="tabla_fila" align="center">' . $value['tb_ejecucion_expediente'] . '</td>'.
            '<td id="tabla_fila" align="center">' . $nombre_juzgado. '</td>'.
            '<td id="tabla_fila" align="center"><span style="font-weight: bold;">'.$estado_proceso[$value['tb_ejecucion_estadoproceso']].'</span></td>'.
            '<td id="tabla_fila" align="center">'.
                '<a class="btn btn-info btn-xs" title="Ver" onclick="ver_proceso(\''.$action_def.'\', '.$value['tb_ejecucion_id'].')">Ver</a> '.
                $seg_boton.' '.
                $gastos_boton.
            '</td>';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
$oEjecucion->usuencargado = null;
$oEjecucion->cgvsubtipo_id = null;
$oEjecucion->estadoejecucion_cgvsubtipo_id = null;
$oEjecucion->estadoproceso = null;
?>
<table id="tbl_ejecucions" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">Id</th>
            <th id="tabla_cabecera_fila" style="width: 7%;">Encargado</th>
            <th id="tabla_cabecera_fila">Fecha Inicio</th>
            <th id="tabla_cabecera_fila" style="width: 23%;">Cliente</th>
            <th id="tabla_cabecera_fila">Credito Id</th>
            <th id="tabla_cabecera_fila">Placa</th>
            <th id="tabla_cabecera_fila">Tipo Credito</th>
            <th id="tabla_cabecera_fila" style="width: 18%;">Fase Actual</th>
            <th id="tabla_cabecera_fila">Fec. Ult Fase</th>
            <th id="tabla_cabecera_fila">Nro Caso (exped)</th>
            <th id="tabla_cabecera_fila">Despacho</th>
            <th id="tabla_cabecera_fila">Estado</th>
            <th id="tabla_cabecera_fila" style="width: 7%;">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>
