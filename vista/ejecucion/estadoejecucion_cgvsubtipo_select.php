<?php
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();

if (!empty($_POST["estadoejecucion_cgvsubtipo_id"])){
	$estadoejecucion_cgvsubtipo_id = $_POST["estadoejecucion_cgvsubtipo_id"];
}
if (!empty($_POST["estadoejecucion_id"])){
	$estadoejecucion_id = $_POST["estadoejecucion_id"];
}
if (!empty($_POST["cgvsubtipo_id"])){
	$cgvsubtipo_id = $_POST["cgvsubtipo_id"];
}

$option = '<option value="0">Todas</option>';

//PRIMER NIVEL
$result = $oEjecucion->buscar_estadoejecucion_cgvsubtipo_por('', $cgvsubtipo_id, $estadoejecucion_id, '');
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($estadoejecucion_cgvsubtipo_id == $value['tb_estadoejecucion_cgvsubtipo_id'])
			$selected = 'selected';

		$option .= '<option value="'.$value['tb_estadoejecucion_cgvsubtipo_id'].'" '.$selected.'>'.$value['tb_estadoejecucion_nom'].'</option>';
	}
}
unset($result);
//FIN PRIMER NIVEL
echo $option;
