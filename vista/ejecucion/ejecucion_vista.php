<!-- Content Wrapper. Contains page content -->
<div id="content_wrapper" class="content-wrapper">
	<input type="hidden" name="hdd_id_usu_actual" id="hdd_id_usu_actual" value="<?php echo $_SESSION['usuario_id']; ?>">
	<input type="hidden" name="hdd_perfil_usu_actual" id="hdd_perfil_usu_actual" value="<?php echo $_SESSION['usuarioperfil_id']; ?>">
	<input type="hidden" name="hdd_nom_usu_actual" id="hdd_nom_usu_actual" value="<?php echo "{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}"; ?>">
	<!-- Content Header (Page header) -->
	<div id="header_contenedor">
		<section class="content-header">
			<h1>
				<?php echo $menu_tit; ?>
				<small><?php echo $menu_des; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Legal</a></li>
				<li class="active"><?php echo ucwords(mb_strtolower($menu_tit)); ?></li>
			</ol>
		</section>
	</div>

	<section id="ejecucion_tabla" class="content">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<label>Filtros</label>
						<div class="panel panel-danger" id="div_filtros_ejecucion">
							<div class="panel-body">
								<?php require_once VISTA_URL . 'ejecucion/ejecucion_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body" style="padding-top: 0px;">
				<div class="row">
					<div class="col-md-12">
						<div id="alertas_ejecucion" class="callout" style="display: none; margin: 0 0 10px 0; border-left-color: gray;">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div id="mensaje_recarga" class="callout callout-info" style="display:none;">
							<h4 id="mensaje_recarga_texto">Debe recargar la tabla, o la página para visualizar algunos cambios</h4>
						</div>
					</div>
				</div>

				<!--- MESAJES DE CARGANDO -->
				<div class="callout callout-info" id="ejecucion_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>

				<div class="row">
					<!-- Input para guardar el valor ingresado en el search de la tabla-->
					<input type="hidden" id="hdd_datatable_ejecucion_fil">

					<div class="col-sm-12">
						<div id="div_ejecucion_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
	<?php require_once VISTA_URL . 'templates/modal_mensaje.php'; ?>

	<!-- aqui vienen los otros section -->
</div>
<div id="div_historial_form"></div>
<!-- GERSON (18-12-24) -->
<div id="div_modal_gastos_ejecucion">
	<!-- INCLUIMOS EL MODAL PARA VISUALIZACION DE GASTOS DE EJECUCION -->
</div>
<!--  -->