<?php
$filtro_fec1 = date('d-m-Y', strtotime('-30 days'));
$filtro_fec2 = date('d-m-Y');

//datos para select de usuarios que pertenezcan al area legal
    $usuario_columna = 'usu.tb_usuarioperfil_id';
    $usuario_valor = 3;//usuarioperfil 3 Legal & AC
    $param_tip = 'INT';
    $empresa_id = 0;
//

//variables para select de cgvsubtipo
    $cgarvtipo_id = '1, 2';
//
?>
<form id="form_ejecucion_filtro">
    <div class="row">
        <div class="col-md-2" style="padding-right: 0px;">
            <label for="txt_fil_cliente_nom">Cliente:</label>
            <input type="text" name="txt_fil_cliente_nom" id="txt_fil_cliente_nom" placeholder="Ingrese N° Documento o Nombre de Cliente" class="form-control input-sm ui-autocomplete-input input-shadow" autocomplete="off" size="45" value>
            <input type="hidden" name="hdd_fil_cliente_id" id="hdd_fil_cliente_id">
        </div>

        <div class="col-md-1" style="padding-right: 0px; width: 255px;">
            <label for="cbo_fil_cgvsubtipo_id">Tipo credito:</label>
            <select name="cbo_fil_cgvsubtipo_id" id="cbo_fil_cgvsubtipo_id" class="input-sm input-shadow form-control" style="padding: 5px 4px;">
                <?php require_once VISTA_URL.'ejecucion/cgvsubtipo_select.php'; ?>
            </select>
        </div>

        <div id="div_fil_estadoejecucion_cgvsubtipo_id" class="col-md-1" style="padding-right: 0px; width: 120px;">
            <label for="cbo_fil_estadoejecucion_cgvsubtipo_id">Fase actual:</label>
            <select name="cbo_fil_estadoejecucion_cgvsubtipo_id" id="cbo_fil_estadoejecucion_cgvsubtipo_id" class="input-sm input-shadow form-control disabled" style="padding: 5px 4px;">
                <option value="0">Todas</option>
            </select>
        </div>

        <div class="col-md-2" style="padding-right: 0px;">
            <label>Fecha de creación entre:</label>
            <div class="input-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control input-sm input-shadow" name="txt_fil_fec1" id="txt_fil_fec1" value="<?php echo $filtro_fec1; ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <span class="input-group-addon">-</span>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control input-sm input-shadow" name="txt_fil_fec2" id="txt_fil_fec2" value="<?php echo $filtro_fec2; ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>

        <div class="col-md-2" style="padding-right: 0px; width: 265px;">
            <label for="txt_fil_encargado_id" style="padding-right: 2px;">Encargado:</label>
            <select name="txt_fil_encargado_id" id="txt_fil_encargado_id" class="input-sm input-shadow form-control">
                <option value="0">Todos los encargados...</option>
                <option value="42">GIORDANA MATILDE GARCIA SANTOS</option>
                <option value="86">MARILIN LISBETH MOLINA AQUINO</option>
                <?php require_once VISTA_URL.'usuario/usuario_select.php'; ?>
            </select>
        </div>

        <div class="col-md-1" style="padding-right: 0px;">
            <label for="cbo_fil_estadoejecucion">Estado proceso:</label>
            <select name="cbo_fil_estadoejecucion" id="cbo_fil_estadoejecucion" class="form-control input-shadow input-sm">
                <option value="all">Todos</option>
                <option value="1">Activo</option>
                <option value="0">Suspendido</option>
                <option value="2">Anulado</option>
                <option value="3">Terminado</option>
            </select>
        </div>

        <!-- botones -->
        <div class="col-md-1" style="padding-top: 22px;">
            <button type="button" id="btn_buscar" class="btn btn-info btn-sm" onclick="ejecucion_tabla()" title="Buscar">
                <i class="fa fa-search icon"></i>
            </button>
            <?php if ($_SESSION['usuario_id'] == 42 || $_SESSION['usuarioperfil_id'] == 1) { ?>
                <a class="btn btn-primary btn-sm" title="Cambiar Encargado" onclick="reasignar_grupo()"><i class="fa fa-exchange">&nbsp;</i><i class="fa fa-user"></i></a>
            <?php } ?>
        </div>
    </div>
</form>
