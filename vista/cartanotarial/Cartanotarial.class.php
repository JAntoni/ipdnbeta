<?php
require_once '../../datos/conexion.php';
//
class Cartanotarial extends Conexion {
    public $id;
    public $destinatario;
    public $direccion_id;
    public $direccion;
    public $ubigeo;
    public $num;
    public $fecnotariarecepciona;
    public $fecdiligencia;
    public $numsuministro;
    public $cnestadoentrega_id;
    public $ejecucion_id;
    public $cnestado_id;
    public $certificadoregveh_id;
    public $fecreg;
    public $usureg;
    public $fecmod;
    public $usumod;
    public $xac;
    public $fecrecojo;
    public $doc_scanned;

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $column_opc = !empty($this->certificadoregveh_id) ? ', tb_certificadoregveh_id' : '';
            $param_opc = !empty($this->certificadoregveh_id) ? ', :param_opc1' : '';

            $sql = "INSERT INTO tb_cartanotarial
                    (
                        tb_cartanotarial_destinatario,
                        tb_direccion_id,
                        tb_cartanotarial_direccion,
                        tb_cartanotarial_ubigeo,
                        tb_cartanotarial_fecnotariarecepciona,
                        tb_ejecucion_id,
                        tb_cnestado_id,
                        tb_cartanotarial_usureg,
                        tb_cartanotarial_usumod $column_opc
                    )
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        1,
                        :param6,
                        :param7 $param_opc
                    )";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->destinatario, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->direccion_id, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->direccion, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->ubigeo, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->fecnotariarecepciona, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->ejecucion_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param6', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param7', $this->usumod, PDO::PARAM_INT);
            if (!empty($this->certificadoregveh_id)) {
                $sentencia->bindParam(':param_opc1', $this->certificadoregveh_id, PDO::PARAM_INT);
            }

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_todos($cartanotarial_id, $ejecucion_id, $tipo_destinatario, $direccion_id) {
        $where_opt = '';
        if (!empty($cartanotarial_id)) {
            $where_opt .= ' AND cn.tb_cartanotarial_id = :param_opc0';
        }
        if (!empty($ejecucion_id)) {
            $where_opt .= ' AND cn.tb_ejecucion_id = :param_opc1';
        }
        if (!empty($tipo_destinatario)) {
            $where_opt .= ' AND cn.tb_cartanotarial_destinatario = :param_opc2';
        }
        if (!empty($direccion_id)) {
            $where_opt .= ' AND cn.tb_direccion_id = :param_opc3';
        }
        
        try {
            $sql = "SELECT cn.*,
                        e.tb_cliente_id, e.tb_persona_id, e.tb_cargo_id, e.tb_ejecucion_montoliquidacion, e.tb_credito_id,
                        ep.tb_escriturapublica_id, ep.tb_escriturapublica_num, ep.tb_escriturapublica_fecha, ep.tb_escriturapublica_clienterepre,
                            ep.tb_escriturapublica_clausulasejecucion, ep.tb_escriturapublica_clausulasrepre,
                        u3.tb_ubigeo_coddep, u3.tb_ubigeo_nom AS departamento,
                        u2.tb_ubigeo_codpro, u2.tb_ubigeo_nom AS provincia,
                        u1.tb_ubigeo_coddis, u1.tb_ubigeo_nom AS distrito,
                        cnee.tb_cnestadoentrega_nom
                    FROM tb_cartanotarial cn
                    INNER JOIN tb_ejecucion e ON (cn.tb_ejecucion_id = e.tb_ejecucion_id)
                    -- INNER JOIN tb_escriturapublica ep ON (e.tb_credito_id = ep.tb_credito_id)
                    INNER JOIN tb_escriturapublica ep ON (
                            ((e.tb_credito_id_padreorigen IS NOT NULL AND e.tb_credito_id_padreorigen != '') AND e.tb_credito_id_padreorigen = ep.tb_credito_id) OR
                            ((e.tb_credito_id_padreorigen IS NULL OR e.tb_credito_id_padreorigen = '') AND e.tb_credito_id = ep.tb_credito_id)
                        )
                    INNER JOIN tb_ubigeo u1 ON (CONCAT(u1.tb_ubigeo_coddep, u1.tb_ubigeo_codpro, u1.tb_ubigeo_coddis) = cn.tb_cartanotarial_ubigeo)
                    INNER JOIN tb_ubigeo u2 ON (u1.tb_ubigeo_coddep=u2.tb_ubigeo_coddep AND u1.tb_ubigeo_codpro=u2.tb_ubigeo_codpro AND u2.tb_ubigeo_coddis='00')
                    INNER JOIN tb_ubigeo u3 ON (u2.tb_ubigeo_coddep=u3.tb_ubigeo_coddep AND u3.tb_ubigeo_codpro = '00' AND u3.tb_ubigeo_coddis = '00')
                    LEFT JOIN tb_cnestadoentrega cnee ON (cn.tb_cnestadoentrega_id = cnee.tb_cnestadoentrega_id)
                    WHERE cn.tb_cartanotarial_xac = 1 $where_opt
                    ORDER BY cn.tb_cartanotarial_fecreg ASC;";
            //
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($cartanotarial_id)) {
                $sentencia->bindParam(':param_opc0', $cartanotarial_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucion_id)) {
                $sentencia->bindParam(':param_opc1', $ejecucion_id, PDO::PARAM_INT);
            }
            if (!empty($tipo_destinatario)) {
                $sentencia->bindParam(':param_opc2', $tipo_destinatario, PDO::PARAM_INT);
            }
            if (!empty($direccion_id)) {
                $sentencia->bindParam(':param_opc3', $direccion_id, PDO::PARAM_STR);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno['cant_filas'] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function modificar_campo($cartanotarial_id, $cartanotarial_columna, $cartanotarial_valor, $param_tip, $usuario_id){
        $this->dblink->beginTransaction();
        try {
            if (!empty($cartanotarial_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql_add = !empty($usuario_id) ? ", tb_cartanotarial_usumod = $usuario_id" : '';
                $sql = "UPDATE tb_cartanotarial SET " . $cartanotarial_columna . " = :cartanotarial_valor$sql_add WHERE tb_cartanotarial_id = :cartanotarial_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cartanotarial_id", $cartanotarial_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":cartanotarial_valor", $cartanotarial_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":cartanotarial_valor", $cartanotarial_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_cnestadoentregas() {
        try {
            $sql = "SELECT cnee.*
                    FROM tb_cnestadoentrega cnee
                    WHERE cnee.tb_cnestadoentrega_xac = 1;";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
