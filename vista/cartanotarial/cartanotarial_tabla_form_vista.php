<?php
require_once '../cartanotarial/Cartanotarial.class.php';
$oCartanotarial = new cartanotarial();
require_once '../persona/Persona.class.php';
$oPersona = new Persona();
require_once '../cliente/Cliente.class.php';
$oCliente = new Cliente();
//utilizar el destinatario para hacer busqueda en 1: $oCliente, o, 2: $oPersona

$lista = '';
$ejecucion_id = intval($_POST['ejecucion_id']);
$ejecucionfase_completado = $_POST['ejecucionfase_completado'];
$vista = $_POST['vista'];
$action2 = $_POST['action2'];
$ejecucionfase_id = $_POST['ejecucionfase_id'];

$res = $oCartanotarial->listar_todos('', $ejecucion_id, '', '');

if ($res['estado'] == 1) {
    foreach ($res['data'] as $key => $carta) {
        $destinatario = $carta['tb_cartanotarial_destinatario'] == 1 ? 'CLIENTE: ' : 'REPRES.: ';
        //buscar de tabla cliente o de tabla persona
        if ($carta['tb_cartanotarial_destinatario'] == 1) {
            $res_cliente = $oCliente->mostrarUno($carta['tb_cliente_id'])['data'];

            $cliente_concat = "{$res_cliente['tb_cliente_doc']} - {$res_cliente['tb_cliente_nom']}";
            if (intval($res_cliente['tb_cliente_tip']) == 2) {
                $cliente_concat = "{$res_cliente['tb_cliente_empruc']} - {$res_cliente['tb_cliente_emprs']}, ". $cliente_concat;
            }
            $destinatario .= $cliente_concat;
            unset($res_cliente);
        } else {
            $res_repre = $oPersona->mostrar_uno_id($carta['tb_escriturapublica_clienterepre'])['data'];
            $repre_nom = $res_repre['tb_persona_doc'].' - '.$res_repre['tb_persona_ape'].' '.$res_repre['tb_persona_nom'];
            $destinatario .= $repre_nom;
            unset($res_repre);
        }
        $ubigeo_nom = "{$carta['distrito']} - {$carta['provincia']} - {$carta['departamento']}";
        $btn_eliminar = $ejecucionfase_completado != 1 && $vista == 'cn_redaccion' ? '<button class="btn btn-danger btn-xs" title="ELIMINAR REGISTRO" onclick="cartanotarial_form(\'E\', '.$carta['tb_cartanotarial_id'].')"><i class="fa fa-trash"></i></button>' : '';
        $btn_modif = $ejecucionfase_completado != 1 && $vista == 'esperando_contacto_cliente' ? ' <button class="btn btn-warning btn-xs" title="EDITAR DATOS" onclick="cartanotarial_form(\'M\', '.$carta['tb_cartanotarial_id'].')"><i class="fa fa-edit"></i></button>' : '';

        $btn_pdf_scan = ''; $btn_generar_pdf = '';
        if ($vista == 'esperando_contacto_cliente') {
            if (!empty($carta['tb_cartanotarial_doc_scanned'])) {
                $ver_editar_pdf = ($ejecucionfase_completado == 1 || $action2 == 'L_ejecucion') ? 1 : 0;
                $btn_pdf_scan = ' <button class="btn btn-primary btn-xs" title="PDF SCANEADO" onclick="'."verejecucion_pdf_form({$carta['tb_cartanotarial_doc_scanned']}, 'ejecucion_varios-cartascan', $ejecucionfase_id, $ver_editar_pdf, {$carta['tb_cartanotarial_id']})".'"><i class="fa fa-upload"></i></button>';
            } else {
                $btn_pdf_scan = ' <button class="btn btn-warning btn-xs" title="SUBIR PDF SCANEADO" onclick="'."ejecucionpdf_form($ejecucion_id, $ejecucionfase_id, {$carta['tb_credito_id']}, 'new', 'ejecucion_varios-cartascan', '$action2', {$carta['tb_cartanotarial_id']})".'"><i class="fa fa-upload"></i></button>';
            }
        } else {
            $btn_generar_pdf = ' <a type="button" class="btn btn-github btn-xs" title="GENERAR PDF" target="_blank" href="javascript:void(0)" onclick="carta_notarial_pdf('.$carta['tb_cartanotarial_id'].')"> <i class="fa fa-file-pdf-o"></i></a>';
        }

        $lista .=
            '<tr id="tabla_cabecera_fila">
                <td id="tabla_fila">'.$carta['tb_cartanotarial_id']. '</td>
                <td id="tabla_fila" align="left">'.$destinatario.'</td>
                <td id="tabla_fila" align="left">'."$ubigeo_nom, {$carta['tb_cartanotarial_direccion']}".'</td>
                <td id="tabla_fila">
                    <button class="btn btn-info btn-xs" title="VER DATOS" onclick="cartanotarial_form(\'L\', '.$carta['tb_cartanotarial_id'].')"><i class="fa fa-eye"></i></button>
                    '.$btn_eliminar.$btn_modif.$btn_pdf_scan.$btn_generar_pdf.'
                </td>
            </tr>';
    }
}
unset($res);
?>
<table id="tbl_cartanotarials" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" style="width: 3%;">Id</th>
            <th id="tabla_cabecera_fila" style="width: 45%;">Destinatario</th>
            <th id="tabla_cabecera_fila" style="width: 40%;">Ubigeo - Direccion</th>
            <th id="tabla_cabecera_fila" style="width: 12%;">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $lista;?>
    </tbody>
</table>
