<?php
//todos los require
	require_once '../../core/usuario_sesion.php';
	require_once '../funciones/fechas.php';
	require_once '../funciones/funciones.php';
	require_once '../cartanotarial/Cartanotarial.class.php';
	$oCarta = new Cartanotarial();
	require_once '../historial/Historial.class.php';
	$oHist = new Historial();
//

//datos estaticos
	$action				= $_POST['action_cartanotarial'];
	$usuario_id			= intval($_SESSION['usuario_id']);
	$data['estado']		= 0;
	$data['mensaje']	= "Error al $action la Carta notarial";
//

//recibir datos y asignar
	$ejecucionfase_id = $_POST['ejecucionfase_id'];
	$destinatario = $_POST['cbo_carta_destinatario'];
	$direccion_id = $_POST['cbo_carta_direccion'];
	$ubigeo = $_POST['cbo_dep'].$_POST['cbo_pro'].$_POST['cbo_dis'];
	$direccion_desc = trim(strtoupper($_POST['direccion_des']));
	if (!is_numeric($direccion_id) && $direccion_id != 'new') {
		$direccion_desc = trim(substr($direccion_desc, 13));
	}
	$certificado_id = $_POST['cbo_certificado_id'];

	$oCarta->destinatario = $destinatario;
	$oCarta->direccion = $direccion_desc;
	$oCarta->ubigeo = $ubigeo;
	$oCarta->fecnotariarecepciona = fecha_mysql($_POST['txt_carta_fechanotariarecepciona']);
	$oCarta->ejecucion_id = $_POST['hdd_carta_ejecucion_id'];
	$oCarta->certificadoregveh_id = $certificado_id;
	$oCarta->usumod = $usuario_id;
//

if ($action == 'insertar') {
	$oCarta->usureg = $usuario_id;

	if ($direccion_id == 'new') {
		//hacer un nuevo registro en tb_direccion
		require_once '../direccion/Direccion.class.php';
		$oDireccion = new Direccion();

		$tabla_id = $_POST['hdd_carta_persona_o_cliente_id'];

		$oDireccion->des		= 'ENVIO DE CARTA NOTARIAL';
		$oDireccion->dir		= $direccion_desc;
		$oDireccion->ubigeo		= $ubigeo;
		$oDireccion->tablanom	= $destinatario == 2 ? "tb_persona" : "tb_cliente";
		$oDireccion->tablaid	= $tabla_id;
		$oDireccion->fecini		= fecha_mysql(date('d-m-Y'));
		$oDireccion->fecfin		= null;
		$oDireccion->usureg		= $usuario_id;

		$direccion_id = $oDireccion->insertar()['nuevo'];
	}
	$oCarta->direccion_id = $direccion_id;
	
	$res_insert = $oCarta->insertar();
	if ($res_insert['estado'] == 1) {
		$data['estado'] = 1;
		$data['mensaje'] = "Carta notarial registrada correctamente";

		//guardar historial tb_hist de la tabla tb_cartanotarial
		$oHist->setTbHistUsureg($usuario_id);
		$oHist->setTbHistNomTabla('tb_cartanotarial');
		$oHist->setTbHistRegmodid($res_insert['nuevo']);
		$oHist->setTbHistDet("Registró los datos de la Carta notarial del proceso Ejecucion id {$oCarta->ejecucion_id} | <b>". date("d-m-Y h:i a").'</b>');
		$oHist->insertar();

		//guardar historial tb_hist de la tabla tb_ejecucionfase
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->insertar();
	}
}
elseif ($action == 'modificar') {
	$fecha_diligencia = $_POST['txt_carta_fecdiligencia'];

	$oCarta->num = trim($_POST['txt_carta_num']);
	$oCarta->fecdiligencia = fecha_mysql($fecha_diligencia);
	$oCarta->numsuministro = strtoupper(trim($_POST['txt_carta_numsuministro']));
	$oCarta->cnestadoentrega_id = $_POST['cbo_carta_estadoentrega'];

	$carta_reg_origin = (array) json_decode($_POST['hdd_carta_reg']);

	$igual[0] = boolval($oCarta->num == $carta_reg_origin['tb_cartanotarial_num']);
	$igual[1] = boolval($oCarta->fecdiligencia == fecha_mysql($carta_reg_origin['tb_cartanotarial_fecdiligencia']));
	$igual[2] = boolval($oCarta->numsuministro == $carta_reg_origin['tb_cartanotarial_numsuministro']);
	$igual[3] = boolval($oCarta->cnestadoentrega_id == $carta_reg_origin['tb_cnestadoentrega_id']);

	$son_iguales = true;
	$i = 0;
	while ($i < count($igual)) {
		if (!$igual[$i]) {
			$son_iguales = false;
		}
		$i++;
	}
	$i = 0;

	if ($son_iguales) {
		$data['estado'] = 2;
		$data['mensaje'] = "● No se realizó ninguna modificacion";
	}
	else {
		$cartanotarial_id = $_POST['hdd_carta_id'];
		$oCarta->id = $cartanotarial_id;

		//hacer cambios e insertar historial
			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('tb_cartanotarial');
			$oHist->setTbHistRegmodid($cartanotarial_id);
			$mensaje = "Modificó la Carta notarial con id $cartanotarial_id:";
			if (!$igual[0] && $oCarta->modificar_campo($cartanotarial_id, 'tb_cartanotarial_num', $oCarta->num, 'STR', $usuario_id) == 1) {
				$mensaje .= "<br> - Cambió el número de la Carta: {$carta_reg_origin['tb_cartanotarial_num']} => <b>{$oCarta->num}</b>";
			}
			if (!$igual[1] && $oCarta->modificar_campo($cartanotarial_id, 'tb_cartanotarial_fecdiligencia', $oCarta->fecdiligencia, 'STR', $usuario_id) == 1) {
				$carta_reg_origin['tb_cartanotarial_fecdiligencia'] = empty($carta_reg_origin['tb_cartanotarial_fecdiligencia']) ? '' : mostrar_fecha($carta_reg_origin['tb_cartanotarial_fecdiligencia']);
				$mensaje .= "<br> - Cambió la fecha de diligencia de la carta: ".$carta_reg_origin['tb_cartanotarial_fecdiligencia']." => <b>$fecha_diligencia</b>";
			}
			if (!$igual[2] && $oCarta->modificar_campo($cartanotarial_id, 'tb_cartanotarial_numsuministro', $oCarta->numsuministro, 'STR', $usuario_id) == 1) {
				$mensaje .= "<br> - Cambió el numero suministro de la carta: {$carta_reg_origin['tb_cartanotarial_numsuministro']} => <b>{$oCarta->numsuministro}</b>";
			}
			if (!$igual[3] && $oCarta->modificar_campo($cartanotarial_id, 'tb_cnestadoentrega_id', $oCarta->cnestadoentrega_id, 'INT', $usuario_id) == 1) {
				$mensaje .= "<br> - Cambió el estado de entrega de la carta: {$carta_reg_origin['tb_cnestadoentrega_nom']} => <b>{$_POST['cn_estadoentrega_nom']}</b>";
			}

			$oHist->setTbHistDet($mensaje);
			$oHist->insertar();
		//

		//guardar historial en la fase
			$oHist->setTbHistNomTabla('tb_ejecucionfase');
			$oHist->setTbHistRegmodid($_POST['hdd_carta_ejecucionfase_id']);
			$oHist->setTbHistDet("Editó los datos del Carta notarial con id {$oCarta->id} | <b>". date("d-m-Y h:i a").'</b>');
			$oHist->insertar();
		//
		$data['mensaje'] = '✔ Carta notarial modificada correctamente';
		$data['estado'] = 1;
		unset($res_modificar);
	}
}
elseif ($action == 'eliminar') {
	$carta_id = $_POST['hdd_carta_id'];

	$res_elim = $oCarta->modificar_campo($carta_id, 'tb_cartanotarial_xac', 0, 'INT', $usuario_id);
	if ($res_elim == 1) {
		$data['estado']	 = 1;
		$data['mensaje'] = "Se eliminó la Carta notarial";

		//guardar historial tb_hist de la tabla tb_cartanotarial
		$oHist->setTbHistUsureg($usuario_id);
		$oHist->setTbHistNomTabla('tb_cartanotarial');
		$oHist->setTbHistRegmodid($carta_id);
		$oHist->setTbHistDet("Eliminó la Carta notarial del proceso Ejecucion id {$oCarta->ejecucion_id} | <b>". date("d-m-Y h:i a").'</b>');
		$oHist->insertar();

		//guardar historial tb_hist de la tabla tb_ejecucionfase
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->insertar();
	}
	unset($res_elim);
}
elseif ($action == 'datos_destinatario') {
	$tipo_destinatario = intval($_POST['tipo_destinatario']);
	$ejecucion_id = $_POST['ejecucion_id'];
	$data['num_filas'] = 1;

	//buscar la ejecucion
	require_once '../ejecucion/Ejecucion.class.php';
	$oEjecucion = new Ejecucion();
	$busq_ejecucion = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];

	if ($tipo_destinatario == 1) { //cliente
		$cliente_id = $busq_ejecucion['tb_cliente_id'];

		require_once '../cliente/Cliente.class.php';
		$oCliente = new Cliente();

		$busq_cliente = $oCliente->mostrarUno($cliente_id)['data'];

		$data['doc_1'] = $busq_cliente['tb_cliente_doc'];
		$data['nombre_1'] = $busq_cliente['tb_cliente_nom'];
		$data['tipo_cli'] = $busq_cliente['tb_cliente_tip'];
		if ($busq_cliente['tb_cliente_tip'] == 2) {
			$data['doc_2'] = $data['doc_1'];
			$data['nombre_2'] = $data['nombre_1'];

			$data['doc_1'] = $busq_cliente['tb_cliente_empruc'];
			$data['nombre_1'] = $busq_cliente['tb_cliente_emprs'];

			$data['num_filas'] = 2;
		}
		$data['tabla_nom'] = 'tb_cliente';
		$data['tabla_id'] = $cliente_id;
	} else { //representante cliente EP
		$credito_getdocs = empty($busq_ejecucion['tb_credito_id_padreorigen']) ? $busq_ejecucion['tb_credito_id'] : $busq_ejecucion['tb_credito_id_padreorigen'];

		require_once '../escriturapublica/Escriturapublica.class.php';
		$oEscritura = new Escriturapublica();

		$cli_repre_id = $oEscritura->listar_todos('', $credito_getdocs)['data'][0]['tb_escriturapublica_clienterepre'];

		require_once '../persona/Persona.class.php';
		$oPersona = new Persona();

		$busq_persona = $oPersona->mostrar_uno_id($cli_repre_id)['data'];

		$data['doc_1'] = $busq_persona['tb_persona_doc'];
		$data['nombre_1'] = $busq_persona['tb_persona_ape'].', '.$busq_persona['tb_persona_nom'];
		$data['tabla_nom'] = 'tb_persona';
		$data['tabla_id'] = $cli_repre_id;
	}
	$data['estado'] = 1;
}
else {
	$data['mensaje'] = "No se encontró la accion que intenta realizar";
}

echo json_encode($data);
