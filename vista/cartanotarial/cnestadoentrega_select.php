<?php
require_once '../cartanotarial/Cartanotarial.class.php';
$oCarta = new Cartanotarial();

$cnestadoentrega_id = (empty($cnestadoentrega_id)) ? 0 : $cnestadoentrega_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0">Seleccione</option>';

//PRIMER NIVEL
$result = $oCarta->listar_cnestadoentregas();
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($cnestadoentrega_id == $value['tb_cnestadoentrega_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_cnestadoentrega_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_cnestadoentrega_nom'] . '</option>';
	}
}
$result = null;
//FIN PRIMER NIVEL
echo $option;
