<?php
error_reporting(E_ERROR | E_PARSE);
define('FPDF_FONTPATH', 'font/');

//todos los require
    require_once '../../static/tcpdf/tcpdf.php';
    require_once '../funciones/funciones.php';
    require_once '../funciones/fechas.php';
    require_once '../cartanotarial/Cartanotarial.class.php';
    $oCarta = new Cartanotarial();
    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscriturapublica = new Escriturapublica();
    require_once '../persona/Persona.class.php';
    $oPersona = new Persona();
    require_once '../cargo/Cargo.class.php';
    $oCargo = new Cargo();
    require_once '../creditogarveh/Creditogarveh.class.php';
    $oCredito = new Creditogarveh();
    require_once '../zona/Zona.class.php';
    $oZona = new Zona();
    require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
    $oCertificado = new Certificadoregistralveh();
    require_once '../ejecucion/Ejecucion.class.php';
    $oEjecucion = new Ejecucion();
    require_once '../ejecucionupload/Ejecucionfasefile.class.php';
    $oEjecucionfasefile = new Ejecucionfasefile();
//

$title = 'CN_CASO_';

class MYPDF extends TCPDF {
    public function Header() {
        $image_file = K_PATH_IMAGES.'logo.jpg';
        $this->Image($image_file, 22, 10, 42, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
    }

    /* public function Footer() {
        $style = array(
            'position' => 'L',
            'align' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'padding' => 0,
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
            'text' => false
            //     'font' => 'helvetica',
            //     'fontsize' => 8,
            //     'stretchtext' => 4
        );

        $this->SetY(-24);
        // Page number
        $this->SetFont('helvetica', '', 9);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Página ' . $this->getAliasNumPage() . ' de ' . $this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');

        $codigo = 'CAV-' . str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);

        $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
        $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');
    } */
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('www.inticap.com');
$pdf->SetSubject('www.inticap.com');
$pdf->SetKeywords('www.inticap.com');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(22, 27, 24); // left top right
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 30);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray();

// ---------------------------------------------------------
// add a page
$pdf->AddPage('P', 'A4');

$style_normal = "font-size: 39.55px; text-align: justify;";
$salto_linea = '<tr><td style="font-size: 25px;"></td></tr>';
$direccion_ipdn = 'Av. Mariscal Nieto Nº 480, Ext. A7, primer piso del Centro Comercial Boulevard';
$horario_atencion_ipdn =
    'lunes a viernes desde las 9:00 a.m. hasta la 1:00 p.m. y desde las 3:00 p.m. hasta las 7:00 p.m. '.
    'y los días sábados desde las 9:00 a.m. hasta la 1:00 p.m.';
//

$cartanotarial_id = $_GET['carta_id'];
$res_carta = $oCarta->listar_todos($cartanotarial_id, '', '', '');
if ($res_carta['estado'] == 1) {
    $carta = $res_carta['data'][0];

    $ejecucion_id = $carta['tb_ejecucion_id'];
    $credito_id = $carta['tb_credito_id'];//id del credito garveh cgv
    $cli_rep = intval($carta['tb_cartanotarial_destinatario']); //1:carta al cliente, 2: carta al representante

    $nro_EP = intval($carta['tb_escriturapublica_num']);
    $fecha_EP = date('d/m/Y', strtotime($carta['tb_escriturapublica_fecha']));
    $clausulas_ejecucion = $carta['tb_escriturapublica_clausulasejecucion'];
    $clausulas_ejecucion = explode(',', $clausulas_ejecucion);
    $clausulas_ejecucion_repre = $carta['tb_escriturapublica_clausulasrepre'];
    $clausulas_ejecucion_repre = explode(',', $clausulas_ejecucion_repre);
    $monto_liquidacion = $carta['tb_ejecucion_montoliquidacion'];
    
    $repre_ipdn = $carta['tb_persona_id'];
    $res_busq_repre_ipdn = $oPersona->mostrar_uno_id($repre_ipdn);
    if ($res_busq_repre_ipdn['estado'] == 1) {
        $res_busq_repre_ipdn = $res_busq_repre_ipdn['data'];

        $nombre_firmante = $res_busq_repre_ipdn['tb_persona_ape'].' '.$res_busq_repre_ipdn['tb_persona_nom'];
        $dni_firmante = $res_busq_repre_ipdn['tb_persona_doc'];
    }
    unset($res_busq_repre_ipdn);

    //hacer una busqueda del nombre cargo en la tabla Cargo
    $cargo_firmante = strtolower(trim($oCargo->mostrarUno($carta['tb_cargo_id'])['data']['tb_cargo_nom']));
    $cargo_firmante = ucwords($cargo_firmante);

    //DATOS DEL REPRESENTANTE CLIENTE
    $res_busq_repre_cli = $oPersona->mostrar_uno_id($carta['tb_escriturapublica_clienterepre']);
    if ($res_busq_repre_cli['estado'] == 1) {
        $res_busq_repre_cli = $res_busq_repre_cli['data'];

        $apellido_representante = $res_busq_repre_cli['tb_persona_ape'];
        $nombre_representante = $res_busq_repre_cli['tb_persona_nom'];
        $dni_representante = $res_busq_repre_cli['tb_persona_doc'];
    }
    unset($res_busq_repre_cli);

    //la direccion y ubigeo del representante del cliente se sacará de su carta notarial
    if ($cli_rep == 1) { //estamos en la carta del cliente por lo tanto debemos buscar la del representante para obtener su direccion y ubigeo
        $carta_repre_cli = $oCarta->listar_todos('', $ejecucion_id, 2, '');

        if ($carta_repre_cli['estado'] == 1) {
            $carta_repre_cli = $carta_repre_cli['data'][0];
            $direccion_representante = $carta_repre_cli['tb_cartanotarial_direccion'];
            $ubigeo_representante = "{$carta_repre_cli['distrito']} − {$carta_repre_cli['provincia']} − {$carta_repre_cli['departamento']}";
        }
        else {
            $direccion_representante = '(REGISTRE LA CARTA DEL REPRESENTANTE CLIENTE)';
            $ubigeo_representante = 'ERROR − ERROR − ERROR';
        }
    } else {
        $direccion_representante = $carta['tb_cartanotarial_direccion'];
        $ubigeo_representante = "{$carta['distrito']} − {$carta['provincia']} − {$carta['departamento']}";
    }
    $ubigeo_representante1 = explode(' − ', $ubigeo_representante);

    //buscar credito garveh para obtener datos del vehiculo
    $credito_datos = $oCredito->mostrarUno($credito_id);
    if ($credito_datos['estado'] == 1) {
        $credito_datos = $credito_datos['data'];

        $carroceria = $credito_datos['tb_vehiculoclase_nom'];
        $categoria = strtoupper($credito_datos['tb_credito_vehcate']);
        $marca =        $credito_datos['tb_vehiculomarca_nom'];
        $modelo =       $credito_datos['tb_vehiculomodelo_nom'];
        $anio =         $credito_datos['tb_credito_vehano'];
        $nro_serie =    strtoupper(trim($credito_datos['tb_credito_vehsercha']));
        $nro_motor =    strtoupper(trim($credito_datos['tb_credito_vehsermot']));
        $placa =        strtoupper(trim($credito_datos['tb_credito_vehpla']));
        $placa =        str_replace("-", "", $placa);
        $zona_reg = strtoupper($oZona->mostrarUno($credito_datos['tb_zonaregistral_id'])['data']['tb_zona_nom']);
    }

    $color = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    $combustible = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    //estos datos se deben obtener del certificado registral vehicular
    $res_certificado = $oCertificado->listar_todos($carta['tb_certificadoregveh_id'], '', '', 1);
    if ($res_certificado['estado'] == 1) {
        $res_certificado = $res_certificado['data'][0];
        $color =        strtoupper(trim($res_certificado['tb_certificadoregveh_color']));
        $combustible =  strtoupper(trim($res_certificado['tb_certificadoregveh_comb']));
    }
    unset($res_certificado);

    $fecha_notaria_recepciona = $carta['tb_cartanotarial_fecnotariarecepciona'];
}
unset($res_carta);

$nro_serie_deletreo = $nro_motor_deletreo = '';
for ($i = 0; $i < strlen($nro_serie); $i++) {
    $letra_num = solo_numeros($nro_serie[$i]) ? numero_letra(intval($nro_serie[$i])) : $nro_serie[$i];
	$nro_serie_deletreo .= $i == 0 ? $letra_num : ', '.$letra_num;
}
for ($i = 0; $i < strlen($nro_motor); $i++) {
    $letra_num = solo_numeros($nro_motor[$i]) ? numero_letra(intval($nro_motor[$i])) : $nro_motor[$i];
	$nro_motor_deletreo .= $i == 0 ? $letra_num : ', '.$letra_num;
}

$cabecera_rep = '';

$tipo_cli           = intval($credito_datos['tb_cliente_tip']);
$cliente_sexo       = $credito_datos['tb_cliente_sexo'];
$cliente_sexo       = empty($cliente_sexo) ? 1 : intval($cliente_sexo);

$res_escritura = $oEscriturapublica->listar_todos('', $credito_getdocs);
//direccion cliente obtener de la escritura publica
if ($cli_rep == 2) {
    if ($res_escritura['estado'] == 1) {
        $direccion_cliente = $res_escritura['data'][0]['tb_escriturapublica_clientedir'];
        $ubigeo_cliente = $res_escritura['data'][0]['tb_escriturapublica_clienteubig'];
        $dep_pro_dis_cliente = $res_escritura['data'][0]['Distrito_cli'].' − '.$res_escritura['data'][0]['Provincia_cli'].' − '.$res_escritura['data'][0]['Departamento_cli'];
    }
} else {
    $direccion_cliente = $carta['tb_cartanotarial_direccion'];
    $ubigeo_cliente = $carta['tb_cartanotarial_ubigeo'];
    $dep_pro_dis_cliente = $carta['distrito'].' − '.$carta['provincia'].' − '.$carta['departamento'];
}

$nombre_abajo_1 = $nombre_abajo = '';
$nombre_arriba = $tipo_cli == 2 ? $credito_datos['tb_cliente_emprs'] : $credito_datos['tb_cliente_nom'];
$nombre_arriba_1 = $tipo_cli == 2 ? "EMPRESA $nombre_arriba" : $nombre_arriba;
$title .= $nombre_arriba;
$title .= $cli_rep == 1 ? '_CLIENTE' : '_REPRESENTANTE';
$pdf->SetTitle($title);

//CONDICIONAL DE CUANDO SE REGISTRE UN CONYUGE
$dirigimos = 'usted';
$su_persona_uds= 'su persona';
$Ud_Uds = 'Ud';
$le_les = 'le';
if ($tipo_cli == 2) {
    $cargo_cliente_empresa = $credito_datos['tb_cliente_empger'];
    $suscrito_entre = 'la empresa que usted representa';
    $nombre_abajo = $credito_datos['tb_cliente_nom'];
    $nombre_abajo_1 =
    '<tr>'.
        '<td colspan="15"></td>'.
        '<td colspan="41" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$nombre_abajo.'</td>'.
    '</tr>'.$salto_linea;
} else {//tipo cliente es natural
    $suscrito_entre = 'usted';
    //verificar cuantos clientes participaron en la Escritura pública
    if ($res_escritura['estado'] == 1) {
        $cliente_ids = explode(',', $res_escritura['data'][0]['tb_cliente_ids']);
        if (count($cliente_ids) > 1) {
            $dirigimos = $su_persona_uds = $suscrito_entre = 'ustedes';
            $le_les = 'les';
            $Ud_Uds = 'Uds';

            //buscar nom cliente asociado (por defecto solo listará 1 cliente asociado)
            require_once '../cliente/Cliente.class.php';
            $oCliente = new Cliente();

            $res_cliente_asoc = $oCliente->mostrarUno($cliente_ids[1]);
            if ($res_cliente_asoc['estado'] == 1) {
                $nombre_abajo = $res_cliente_asoc['data']['tb_cliente_nom'];
            }

            $nombre_abajo_1 =
            '<tr>'.
                '<td colspan="15"></td>'.
                '<td colspan="41" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$nombre_abajo.'</td>'.
            '</tr>'.$salto_linea;
        }
    }
}

//datos de la ejecucion
$res_ejecucion = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '');
if ($res_ejecucion['estado'] == 1) {
    $res_ejecucion = $res_ejecucion['data'][0];
    $moneda_id = intval($res_ejecucion['tb_moneda_id']);//moneda de liquidacion
    $moneda_nom = $moneda_id == 1 ? 'S/. ' : 'US$ ';//nombre de moneda de liquidacion
    $credito_getdocs = empty($res_ejecucion['tb_credito_id_padreorigen']) ? $res_ejecucion['tb_credito_id'] : $res_ejecucion['tb_credito_id_padreorigen'];
}

$tipo_cgv = 'Preconstitución';
if ($res_ejecucion['tb_cgarvtipo_id'] != 1) {
    $tipo_cgv = 'Constitución';
}
$nro_inciso_carta = 1;

//VEHICULO DETALLE. CLAUSULA 3 DE CARTA NOTARIAL.
    $vehiculo_detalle = 
    "CARROCERÍA: <b>$carroceria</b>, ".
    "CATEGORÍA: <b>$categoria</b>, ".
    "MARCA: <b>$marca</b>, ".
    "MODELO: <b>$modelo</b>, ".
    "AÑO DE MODELO: <b>$anio</b>, ".
    "COLOR: <b>$color</b>, ".
    "N° DE SERIE: <b>$nro_serie ($nro_serie_deletreo)</b>, ".
    "N° DE MOTOR: <b>$nro_motor ($nro_motor_deletreo)</b>, ".
    "COMBUSTIBLE: <b>$combustible</b>, ".
    "EL CUAL EN LA ACTUALIDAD MUESTRA COMO PLACA DE RODAJE N° <b>$placa</b> ".
    "DEL REGISTRO DE PROPIEDAD VEHICULAR DE LA $zona_reg";
//

$texto_ordinal = obtenerOrdinales($clausulas_ejecucion);
$texto_ordinal_repre = obtenerOrdinales($clausulas_ejecucion_repre);
if (count($clausulas_ejecucion) > 1) {
    $clausulas_ordinal_EP .= ' las cláusulas ';
} else {
    $clausulas_ordinal_EP .= ' la cláusula ';
}
$clausulas_ordinal_EP .=  $texto_ordinal;

//revisar si hay documentos que incluir en la redaccion junto a la EP
$documentos_referidos_encabezado = 'ESCRITURA PÚBLICA N° '.$nro_EP.' DE FECHA '.$fecha_EP;
$documentos_referidos_narracion = "Escritura Pública de $tipo_cgv de Garantía Mobiliaria N° $nro_EP de fecha $fecha_EP";
$enlace_documentos_referidos_cls_3 = ' y';
$enlace_texto_cn_repre = 'y';

$ejecucionfase_id = $_GET['ejecucionfase_id'];
$oEjecucionfasefile->incluir_redac = 1;
$res_buscar_doc_incluir_redaccion = $oEjecucionfasefile->listar('', $ejecucionfase_id, '', '', 9, '', '', '');
if ($res_buscar_doc_incluir_redaccion['estado'] == 1) {
    $enlace_texto_cn_repre = 'así como';
    $enlace_documentos_referidos_cls_3 = '';
    $i = 1;
    foreach ($res_buscar_doc_incluir_redaccion['data'] as $key => $doc) {
        $enlace_referidos = ',';
        if ($i == $res_buscar_doc_incluir_redaccion['cantidad']) {
            $enlace_referidos = ' y';
        }

        $add = ucfirst(strtolower($doc['tb_ejecucionfasefile_archivonom']));
        if (!empty($doc['tb_ejecucionfasefile_num'])) {
            $add .= ' N° '.$doc['tb_ejecucionfasefile_num'];
        }
        if (!empty($doc['tb_ejecucionfasefile_fecnoti'])) {
            $add .= ' de fecha '.date('d/m/Y', strtotime($doc['tb_ejecucionfasefile_fecnoti']));
        }
        
        $documentos_referidos_encabezado .= strtoupper("$enlace_referidos $add");
        $documentos_referidos_narracion .= "$enlace_referidos $add";
        $i++; $add = '';
    }
    unset($i);
}

$oEjecucionfasefile->incluir_redac = null;

if ($cli_rep == 2) {
    $style_normal = "font-size: 37.8px; text-align: justify;";
    $pdf->SetAutoPageBreak(TRUE, 28);
    if (count($clausulas_ejecucion_repre) > 1) {
        $clausulas_ordinal_EP_1 = "las cláusulas $texto_ordinal_repre";
    } else {
        $clausulas_ordinal_EP_1 = "la $texto_ordinal_repre cláusula";
    }
    if ($tipo_cli == 2) {
        $suscrito_entre_1 = "la empresa $nombre_arriba, debidamente representada por su ". strtolower($cargo_cliente_empresa) ." ". customUcwords($nombre_abajo);
    } else {
        $suscrito_entre_1 = $cliente_sexo == 1 ? "el Sr. $nombre_arriba" : "la Sra. $nombre_arriba";
        if (count($cliente_ids) > 1) {
            $suscrito_entre_1 = "$nombre_arriba y $nombre_abajo";
        }
    }

    $cabecera_rep =
    //TITULO
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="51">'.
                '<u style="font-weight: bold; font-size: 38px" align="center">'.
                    'COMUNICACIÓN NOTARIAL DE EJECUCIÓN DE GARANTÍA MOBILIARIA'.
                '</u>'.
            '</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="32"></td>'.
            '<td colspan="22" style="font-size: 38px" align="right"> Chiclayo, '.fechaActual(fecha_mysql($fecha_notaria_recepciona)).'</td>'.
        '</tr>'.
        $salto_linea.
    //

    //DESTINATARIO REPRESENTANTE, DIRECCION Y REFERENCIAS
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="8" style="font-weight: bold; font-size: 38px">'.
                'SR.'.
            '</td>'.
            '<td colspan="1" style="font-weight: bold; font-size: 38px">:</td>'.
            '<td colspan="3" style="font-weight: bold;"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'."$apellido_representante, $nombre_representante".'</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="8" style="font-weight: bold; font-size: 38px">'.
                'DOMICILIO'.
            '</td>'.
            '<td colspan="1" style="font-weight: bold; font-size: 38px">:</td>'.
            '<td colspan="3" style="font-weight: bold;"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.strtoupper($direccion_representante).'</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="15"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$ubigeo_representante.'</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="8" style="font-weight: bold; font-size: 38px;">'.
                'Ref.'.
            '</td>'.
            '<td colspan="1" style="font-weight: bold; font-size: 38px;">:</td>'.
            '<td colspan="3" style="font-weight: bold;"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$documentos_referidos_encabezado.'</td>'.
        '</tr>'.
        $salto_linea.
    //

    //NARRACION DE CARTA
        '<tr>'.
            '<td colspan="3" style="width: 5.2%;"></td>'.
            '<td colspan="51" style="'.$style_normal.'">'.
                'De nuestra consideración:'.
            '</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="51" style="'.$style_normal.' text-indent: 75px;">'.
                'Por medio del presente, damos cumplimiento a lo establecido en la Ley de '.
                "garantía mobiliaria $enlace_texto_cn_repre en $clausulas_ordinal_EP_1 de la $documentos_referidos_narracion, ".
                "suscrita entre $suscrito_entre_1, con mi representada la Empresa Inversiones y Préstamos del Norte S.A.C.; ".
                'comunicamos a usted en su condición de representante nombrado y con poder otorgado '.
                'según consta en la mencionada Escritura Pública, sobre el requerimiento de pago del saldo '.
                'deudor y ejecución de garantía mobiliaria cursado vía notarial a la persona señalada líneas '.
                'arriba, el mismo que textualmente expresa lo siguiente: '.
            '</td>'.
        '</tr>'.
    //
    '<br><br>';

    $style_normal = "font-size: 37.7px; text-align: justify;";
}


$clausulas =
    '<tr>'.
        '<td colspan="4"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center">'. $nro_inciso_carta .'.</td>'.
        '<td colspan="47" style="'.$style_normal.'">'.
            'Que habiendo incumplido con el pago de su crédito de acuerdo al cronograma '.
            'de pagos hemos procedido a liquidar su saldo deudor, el mismo que al día de '.
            'hoy, asciende al monto de '.$moneda_nom. mostrar_moneda($monto_liquidacion) .' ('.numtoletras($monto_liquidacion, $moneda_id).'), '.
            'incluye capital, intereses, moras, costos, costas, gastos por resolución y ejecución.'.
        '</td>'.
    '</tr>'.
    //
    '<tr>'.
        '<td colspan="4"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center">'. ($nro_inciso_carta + 1) .'.</td>'.
        '<td colspan="47" style="'.$style_normal.'">'.
            "En tal sentido, de conformidad con lo señalado en $clausulas_ordinal_EP de la $documentos_referidos_narracion; ".
            "y en el ejercicio regular de nuestro derecho, $le_les otorgamos el plazo de 03 (tres) ".
            'días hábiles contados a partir de la recepción de la presente carta notarial, '.
            "para acercarse a nuestras oficinas (ubicadas en $direccion_ipdn) a cumplir con el pago ".
            "de la obligación indicada líneas arriba, dentro de nuestros horarios de atención: $horario_atencion_ipdn".
        '</td>'.
    '</tr>'.
    //
    '<tr>'.
        '<td colspan="4"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center">'. ($nro_inciso_carta+2) .'.</td>'.
        '<td colspan="47" style="'.$style_normal.'">'.
            'Caso contrario, de acuerdo a lo convenido y en virtud de lo establecido en la Ley '.
            "N° 28677, Ley de Garantía Mobiliaria,$enlace_documentos_referidos_cls_3 $documentos_referidos_narracion, suscrita por $suscrito_entre y la empresa ".
            "a quien represento; $le_les solicitamos en el plazo de 03 (tres) días hábiles ".
            'contados a partir de recibida la presente carta notarial se sirvan a hacer entrega '.
            "física del bien mueble consiste en $vehiculo_detalle.".
        '</td>'.
    '</tr>'.
    //
    '<tr>'.
        '<td colspan="4"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center">'. ($nro_inciso_carta+3) .'.</td>'.
        '<td colspan="47" style="'.$style_normal.'">'.
            "Entrega que deberá hacer a mi representada en la persona de su $cargo_firmante ".
            "<b>$nombre_firmante</b>, identificado con DNI N° $dni_firmante en la siguiente ".
            "dirección domiciliaria en $direccion_ipdn, distrito de Chiclayo, provincia de Chiclayo ".
            "y departamento de Lambayeque; dentro de nuestros horarios de atención: $horario_atencion_ipdn".
        '</td>'.
    '</tr>'.
    //
    $salto_linea.
    //
    '<tr>'.
        '<td colspan="4"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center"></td>'.
        '<td colspan="47" style="'.$style_normal.'">'.
            "Asimismo, $le_les comunicamos, que de conformidad con lo contenido en la $documentos_referidos_narracion, ".
            "suscrita entre $suscrito_entre y la empresa que represento, luego de haber vencido el ".
            'plazo de 03 (tres) días hábiles de recibida la presente podrá alternativamente:'.
        '</td>'.
    '</tr>'.
    //
    $salto_linea.
    //
    '<tr>'.
        '<td colspan="6" style="width: 11.5%;"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center">'.($nro_inciso_carta+3).'.1.</td>'.
        '<td colspan="46" style="'.$style_normal.' width: 82.4%;">'.
            'Efectuar la ejecución extrajudicial de la garantía mobiliaria (entiéndase '.
            'VENTA DIRECTA DEL BIEN DE ACUERDO AL TÍTULO III, Capítulo único '.
            'referente a la ejecución de la Garantía Mobiliaria contenido en la Ley N° '.
            '28677, Ley de la Garantía Mobiliaria).'.
        '</td>'.
    '</tr>'.
    //
    '<tr>'.
        '<td colspan="6" style="width: 11.5%;"></td>'.
        '<td colspan="3" style="'.$style_normal.'" align="center">'.($nro_inciso_carta+3).'.2.</td>'.
        '<td colspan="46" style="'.$style_normal.' width: 82.4%;">'.
            'Solicitar el representante la adjudicación del bien dejado en garantía.'.
        '</td>'.
    '</tr>';
//

$html .=
    '<table>'.
        //IMAGEN (COMENTADO. CONFIGURADO EN HEADER PARA QUE VAYA EN TODAS LAS PAGINAS)
        /* '<tr>'.
            '<td colspan="27">'.
                '<img src="' . K_PATH_IMAGES . 'logo.jpg' . '" style="with: 5px; height: 44px">'.
            '</td>'.
            '<td colspan="27"></td>'.
        '</tr><br>'. */

        //CABECERA PARA EL REPRESENTANTE
        $cabecera_rep.

        //TITULO CARTA
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="51">'.
                '<u style="font-weight: bold; font-size: 38px" align="center">'.
                    'REQUERIMIENTO DE PAGO DE SALDO DEUDOR Y EJECUCIÓN DE GARANTÍA MOBILIARIA'.
                '</u>'.
            '</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="32"></td>'.
            '<td colspan="22" style="font-size: 38px" align="right"> Chiclayo, '.fechaActual(fecha_mysql($fecha_notaria_recepciona)).'</td>'.
        '</tr>'.
        $salto_linea.

        //DESTINATARIO CLIENTE, DIRECCION Y REFERENCIAS
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="8" style="font-weight: bold; font-size: 38px">'.
                'CLIENTE'.
            '</td>'.
            '<td colspan="1" style="font-weight: bold; font-size: 38px">:</td>'.
            '<td colspan="3" style="font-weight: bold;"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$nombre_arriba_1.'</td>'.
        '</tr>'.
        $salto_linea.$nombre_abajo_1.
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="8" style="font-weight: bold; font-size: 38px">'.
                'DOMICILIO'.
            '</td>'.
            '<td colspan="1" style="font-weight: bold; font-size: 38px">:</td>'.
            '<td colspan="3" style="font-weight: bold;"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.strtoupper($direccion_cliente).'</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="15"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$dep_pro_dis_cliente.'</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="8" style="font-weight: bold; font-size: 38px;">'.
                'REF.'.
            '</td>'.
            '<td colspan="1" style="font-weight: bold; font-size: 38px;">:</td>'.
            '<td colspan="3" style="font-weight: bold;"></td>'.
            '<td colspan="39" style="font-weight: bold; font-size: 38px; text-align: justify;">'.$documentos_referidos_encabezado.'</td>'.
        '</tr>'.
        '<br><br>'.
        
        //NARRACION DE CARTA
        '<tr>'.
            '<td colspan="3" style="width: 5.2%;"></td>'.
            '<td colspan="51" style="'.$style_normal.'">'.
                'De nuestra consideración:'.
            '</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3"></td>'.
            '<td colspan="51" style="'.$style_normal.'">'.
                "Por medio de la presente nos dirigimos a $dirigimos con la finalidad de manifestar$le_les lo siguiente:".
            '</td>'.
        '</tr>'.
        $salto_linea.
            $clausulas.
        $salto_linea.
        
        //FINAL DE LA CARTA
        '<tr>'.
            '<td colspan="3" style="width: 5.2%;"></td>'.
            '<td colspan="51" style="'.$style_normal.' width: 94.9%;">'.
                'Copia de la presente carta, por la misma vía notarial, será cursada también al '.
                'REPRESENTANTE nombrado por ambas partes e inscrita en los Registros Públicos de '.
                "Chiclayo, entre mi representada y $su_persona_uds, a fin de cumplir con lo prescrito por ".
                'la Ley de Garantía Mobiliaria. '.
            '</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3" style="width: 5.2%;"></td>'.
            '<td colspan="51" style="'.$style_normal.' width: 94.9%;">'.
                'NOMBRE DEL REPRESENTANTE PARA EFECTOS DE EJECUCIÓN DE LA '.
                "GARANTÍA MOBILIARIA Y NOTIFICACIÓN POR VÍA NOTARIAL: <u><b>$apellido_representante $nombre_representante, ".
                "IDENTIFICADO CON DNI N° $dni_representante, DOMICILIADO EN $direccion_representante, ".
                "DISTRITO {$ubigeo_representante1[0]}, PROVINCIA {$ubigeo_representante1[1]}, DEPARTAMENTO {$ubigeo_representante1[2]}.</b></u>".
            '</td>'.
        '</tr>'.
        '<br><br>'.
        '<tr>'.
            '<td colspan="3" style="width: 5.2%;"></td>'.
            '<td colspan="51" style="'.$style_normal.' width: 94.9%;">'.
                "Sin otro particular me despido de $Ud_Uds.".
            '</td>'.
        '</tr>'.
        $salto_linea.
        '<tr>'.
            '<td colspan="3" style="width: 5.2%;"></td>'.
            '<td colspan="51" style="'.$style_normal.' width: 94.9%;">'.
                'Atentamente,'.
            '</td>'.
        '</tr>'.
    '</table>';
//fin del html

// set core font
$pdf->SetFont('Arial', '', 10);

// output the HTML content
//$html = mb_convert_encoding($html, 'ISO-8859-1', 'UTF-8');//utf8_decode($html);
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo = $title . ".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
