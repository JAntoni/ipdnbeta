var datatable_global_cartanotarial;

$(document).ready(function () {
    completar_tabla_cartanotarials();
});

function completar_tabla_cartanotarials() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cartanotarial/cartanotarial_tabla_form_vista.php",
        async: true,
        dataType: "html",
        data: ({
            ejecucion_id: $('#hdd_cn_ejecucion_id').val(),
            ejecucionfase_id: $('#hdd_cn_ejecucionfase_id').val(),
            ejecucionfase_completado: $('#hdd_cn_ejecucionfase_completado').val(),
            vista: $('#hdd_cn_vista').val(),
            action2: $('#hdd_ejecucion_action2').val()
        }),
        success: function(data) {
            $('#div_html_tablacartanotarial').html(data);
            setTimeout(function () {
                estilos_datatable_cn();
            }, 300);
        }
    });
}

function estilos_datatable_cn() {
    datatable_global_cartanotarial = $('#tbl_cartanotarials').DataTable({
        "pageLength": 25,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [3], orderable: false}
        ]
    });
    datatable_texto_filtrar_cn();
}

function datatable_texto_filtrar_cn() {
    $('input[aria-controls*="tbl_cartanotarials"]')
    .attr('id', 'txt_datatable_cartanotarial_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_cartanotarial_fil').keyup(function (event) {
        $('#hdd_datatable_cartanotarials_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_cartanotarials_fil').val();
    if (text_fil) {
        $('#txt_datatable_cartanotarial_fil').val(text_fil);
        datatable_global_cartanotarial.search(text_fil).draw();
    }
};

function cartanotarial_form(action, cartanotarial_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cartanotarial/cartanotarial_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            cartanotarial_id: cartanotarial_id,
            ejecucionfase_id: $('#hdd_cn_ejecucionfase_id').val(),
            ejecucion_id: $('#hdd_cn_ejecucion_id').val(),
            vista: $('#hdd_cn_vista').val()
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_carta_form').html(html);
            $('#modal_cartanotarial_form').modal('show');
            $('#modal_mensaje').modal('hide');
    
            modal_width_auto('modal_cartanotarial_form', 50);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_cartanotarial_form'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal("modal_cartanotarial_form", "limpiar");
        }
    });
}

function carta_notarial_pdf(carta_id) {
    window.open(VISTA_URL + "cartanotarial/cartanotarial_pdf.php?carta_id="+carta_id+"&ejecucionfase_id="+$('#hdd_cn_ejecucionfase_id').val(), "_blank");
}