
$(document).ready(function(){
	$('#txt_carta_fechanotariarecepciona').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        startDate: new Date()
    });
	$('#txt_carta_fecdiligencia').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });

	//transiciones de ubigeo
    $('select[id*="cbo_dep"]').change(function (event) {
        var codigo_dep = $(this).val();
        if (parseInt(codigo_dep) == 0) {
            $(`#cbo_pro`).html('');
            $(`#cbo_dis`).html('');
			$('#hdd_codpro').val('');
			$('#hdd_coddis').val('');
        } else {
            cargar_provincia(codigo_dep);
            $(`#cbo_dis`).html('');
        }
    });
    $('select[id*="cbo_pro"]').change(function (event) {
        var codigo_dep = $(`#cbo_dep`).val();
        var codigo_pro = $(this).val();

        if (parseInt(codigo_pro) == 0)
            $(`#cbo_dis`).html('');
        else
            cargar_distrito(codigo_dep, codigo_pro);
    });
    //

	//completar txt destinatario
	$('#cbo_carta_destinatario').change(function(){
		if (parseInt($(this).val()) == 0) {
			limpiar_destinatario();
		} else {
			completar_txt_destinatario(parseInt($(this).val()));
			if (parseInt($(this).val()) == 1) {
				$('div.certificado').show(200);
			} else {
				$('#cbo_certificado_id').val(0);
				$('div.certificado').hide(200);
			}
		}
	});

	$('#cbo_carta_direccion').change(function(){
		if (parseInt($(this).val()) == 0 || $(this).val() === null) {
			$('#cbo_dep').val(0).change();
		} else {
			var ubigeo = $('#cbo_carta_direccion option:selected').attr('data-ubig');
			var codigo_dep = ubigeo.substr(0, 2);
			var codigo_pro = ubigeo.substr(2, 2);
			var codigo_dis = ubigeo.substr(4, 2);
			$('#hdd_codpro').val(codigo_pro);
			$('#hdd_coddis').val(codigo_dis);
			$('#cbo_dep').val(codigo_dep).change();
		}

		if ($.isNumeric($(this).val())) {
			//mas info en https://jqueryvalidation.org/rules/
			//jQuery.validator.format("Please, at least {0} characters are necessary")
			$("#cbo_carta_direccion").rules("add", {
				min: 1,
				messages: {
					min: jQuery.validator.format("Obligatorio*")
				}
			});
		} else {
			$("#cbo_carta_direccion").rules("remove", "min");
		}
	});

	//triggers
	$('select[id*="cbo_carta_destinatario"]').change();

	disabled($("#form_cartanotarial").find(".disabled"));

	var verificado = false, mostrar_mensaje = false;
	$('#form_cartanotarial').validate({
		submitHandler: function () {
			//verificar nro suministro cuando modifique cn de client
			if ($('#action_cartanotarial').val() == 'modificar' && $('#hdd_vista_cartanotarial').val() == 'esperando_contacto_cliente' && parseInt($('#cbo_carta_destinatario option:selected').val()) == 1) {
				var suministro_cliente = $('#hdd_cliente_num_suministro').val();//dato original de la tb_cliente
				var suministro_ingresado = $('#txt_carta_numsuministro').val();
				
				if (parseInt($('#cbo_carta_estadoentrega option:selected').val()) == 3 || parseInt($('#cbo_carta_estadoentrega option:selected').val()) == 4) {
					if (suministro_cliente != suministro_ingresado && !verificado) {
						mostrar_mensaje = true;
						$.confirm({
							title: "SISTEMA",
							content: `<b>¿Desea registrar de todos modos?<br>NO COINCIDE: El N° suministro registrado en el checklist del crédito (${suministro_cliente}) VS el ingresado de la diligencia de notaría (${suministro_ingresado})</b>`,
							type: "orange",
							escapeKey: "close",
							backgroundDismiss: true,
							columnClass: "small",
							buttons: {
								Confirmar: {
									btnClass: "btn-orange",
									action: function () {
										verificado = true;
									},
								},
								cancelar: function () { },
							},
						});
					}
	
					if (!verificado && mostrar_mensaje && suministro_cliente != suministro_ingresado) {
						return;
					}
				}
			}
			
			var ejecucionfase_id = $('#hdd_cn_ejecucionfase_id').val();
			var direccion_des = $('#cbo_carta_direccion option:selected').text();
			if (!$('#txt_carta_nuevadirecc').is(":hidden")) {
				direccion_des = $('#txt_carta_nuevadirecc').val();
				$('#cbo_carta_direccion option:eq(0)').val('new');
			}
			direccion_des = '&direccion_des='+direccion_des;
			var cn_estadoentrega_nom = '&cn_estadoentrega_nom='+ $('#cbo_carta_estadoentrega option:selected').text();
			var datos_extra = '&ejecucionfase_id='+ejecucionfase_id+direccion_des+cn_estadoentrega_nom;

			$.ajax({
				type: "POST",
				url: VISTA_URL + "cartanotarial/cartanotarial_controller.php",
				async: true,
				dataType: "json",
				data: serialize_form('form_cartanotarial', datos_extra),
				beforeSend: function () {
					$('#cartanotarial_mensaje').show(400);
					$('#btn_guardar_cartanotarial').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						disabled($('#form_cartanotarial').find("button[class*='btn-sm'], select, input, textarea"));
						$('#cartanotarial_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#cartanotarial_mensaje').html(`<h4>${data.mensaje}</h4>`);

						setTimeout(function () {
							$('#modal_cartanotarial_form').modal('hide');
							if (data.estado == 1) {
								completar_tabla_cartanotarials();
								actualizar_contenido_1fase(ejecucionfase_id);
							}
						}, 2500);
					}
					else {
						$('#cartanotarial_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#cartanotarial_mensaje').html(`<h4>Alerta: ${data.mensaje}</h4>`);
						$('#btn_guardar_cartanotarial').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#cartanotarial_mensaje').removeClass('callout-info').addClass('callout-danger');
					$('#cartanotarial_mensaje').html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
				}
			});
		},
		rules: {
			cbo_carta_destinatario: {
				min: 1
			},
			txt_carta_nuevadirecc: {
				required: true
			},
			cbo_dep: {
				min: 1
			},
			cbo_pro: {
				min: 1
			},
			cbo_dis: {
				min: 1
			},
			txt_carta_fechanotariarecepciona: {
				required: true
			},
			cbo_certificado_id: {
				min: 1
			},
			cbo_carta_estadoentrega: {
				min: 1
			},
			txt_carta_num: {
				required: true
			},
			txt_carta_fecdiligencia: {
				required: true
			}
		},
		messages: {
			cbo_carta_destinatario: {
				min: "Obligatorio*"
			},
			txt_carta_nuevadirecc: {
				required: "Obligatorio*"
			},
			cbo_dep: {
				min: "Obligatorio*"
			},
			cbo_pro: {
				min: "Obligatorio*"
			},
			cbo_dis: {
				min: "Obligatorio*"
			},
			txt_carta_fechanotariarecepciona: {
				required: "Obligatorio*"
			},
			cbo_certificado_id: {
				min: "Obligatorio*"
			},
			cbo_carta_estadoentrega: {
				min: "Obligatorio*"
			},
			txt_carta_num: {
				required: "Obligatorio*"
			},
			txt_carta_fecdiligencia: {
				required: "Obligatorio*"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	//mensaje antes de eliminar
    if ($.inArray($('#action_cartanotarial').val(), ['leer', 'eliminar']) > -1) {
		disabled($('#form_cartanotarial').find("button[class*='btn-sm'], select, input, textarea, a").not('.disabled'));
        if ($('#action_cartanotarial').val() == 'eliminar') {
            $('#cartanotarial_mensaje').removeClass('callout-info').addClass('callout-warning');
            var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro?</h4>';
            $('#cartanotarial_mensaje').html(mens);
            $('#cartanotarial_mensaje').show();
        }
    }

	$('#cbo_certificado_id').change(function(){
		var antig = $('#cbo_certificado_id option:selected').attr('data-antig');
		parseInt(antig) > 24 ? $('#aviso_antiguo').html(`El certificado tiene ${antig} días de antigüedad`).show() : $('#aviso_antiguo').html(``). hide();
	});
});

//funciones para ubigeo
function cargar_provincia(ubigeo_coddep) {  
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
		async: true,
		dataType: "html",
		data: ({
			ubigeo_coddep: ubigeo_coddep,
			ubigeo_codpro: $('#hdd_codpro').val()
		}),
		beforeSend: function () {
			$(`#cbo_pro`).html('<option>Cargando...</option>');
		},
		success: function (data) {
			$(`#cbo_pro`).html(data).change();
		},
		error: function (data) {
			alert(data.responseText);
		}
	});
}
function cargar_distrito(ubigeo_coddep, ubigeo_codpro) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
		async: true,
		dataType: "html",
		data: ({
			ubigeo_coddep: ubigeo_coddep,
			ubigeo_codpro: ubigeo_codpro,
			ubigeo_coddis: $('#hdd_coddis').val()
		}),
		beforeSend: function () {
			$(`#cbo_dis`).html('<option>Cargando...</option>');
		},
		success: function (data) {
			$(`#cbo_dis`).html(data);
			$(`#cbo_dis option:eq(0)`).val(0);
		},
		error: function (data) {
			alert(data.responseText);
		}
	});
}
//

function completar_txt_destinatario(tipo_destinatario) {
	if (tipo_destinatario == 2)
		$('#btn_add_direcc').hide(150);
	else
		$('#btn_add_direcc').show(150);
	$.ajax({
		type: 'POST',
		url: VISTA_URL + 'cartanotarial/cartanotarial_controller.php',
		async: true,
		dataType: 'json',
		data: ({
			action_cartanotarial: 'datos_destinatario',
			tipo_destinatario: tipo_destinatario,
			ejecucion_id: $('#hdd_carta_ejecucion_id').val()
		}),
		success: function (data) {
			if (data.estado == 1) {
				limpiar_destinatario(tipo_destinatario);
				mostrar_inputs(data);
			}
		}
	});
}

function mostrar_inputs(data){
	$('#hdd_carta_persona_o_cliente_id').val(data.tabla_id);
	$('#txt_carta_nrodoc_destinat').val(data.doc_1);
	$('#txt_carta_nombredestinat').val(data.nombre_1);
	if (data.num_filas == 2) {
		$('.destinat_fila_2').show(150);
		$('#txt_carta_nrodoc_destinat2').val(data.doc_2);
		$('#txt_carta_nombredestinat2').val(data.nombre_2);
	}
	cargar_cbo_direcciones(data.tabla_nom, data.tabla_id, data.tipo_cli);
}

function mostrar_add_direcc() {
	var tipo_destinat = $('#cbo_carta_destinatario').val();
	$('#btn_add_direcc').text('Cancelar').removeClass('btn-github').addClass('btn-warning').attr("onclick",`cancelar_add_direcc(${tipo_destinat})`);
	$('.add_direcc').show(150);
	$('#cbo_carta_direccion').val(0).prop('disabled', 'disabled').change();
}

function cancelar_add_direcc(tipo_destinatario) {
	if ($.inArray($('#action_cartanotarial').val(), ['leer', 'eliminar', 'modificar']) <= -1) {
		$('#cbo_carta_direccion').removeAttr('disabled');
	}
	$('.add_direcc').hide(150);
	$('#txt_carta_nuevadirecc').val('');
	$('#btn_add_direcc').text('Add direccion').removeClass('btn-warning').addClass('btn-github').attr("onclick","mostrar_add_direcc()");
	if (tipo_destinatario == 1) {
		$('#btn_add_direcc').show(150);
	}
}

function limpiar_destinatario(tipo_destinatario) {
	$('#hdd_carta_persona_o_cliente_id').val('');
	$('#txt_carta_nrodoc_destinat, #txt_carta_nombredestinat, #txt_carta_nrodoc_destinat2, #txt_carta_nombredestinat2').val('');
	$('.destinat_fila_2').hide(150);
	cancelar_add_direcc(tipo_destinatario);
	$('#cbo_carta_direccion').html('');
	$('#cbo_dep').val(0).change();
}

function cargar_cbo_direcciones(tabla_nom, tabla_id, tipo_cli) {
	var direccion_id = $('#hdd_carta_direccion_id').val();
	var incluye_direccion_EP = 0, credito_id = 0;
	if (tabla_nom == 'tb_cliente') {
		incluye_direccion_EP = 1;
		credito_id = $('#hdd_carta_creditogetdocs_id').val();//linea a cambiar
	}
	$.ajax({
		type: "POST",
		url: VISTA_URL + "direccion/direccion_select.php",
		async: true,
		dataType: "html",
		data: ({
			tabla_nom: tabla_nom,
			tabla_id: tabla_id,
			vigencia: '',
			direccion_id: direccion_id,
			tipo_cli: tipo_cli,
			incluye_direccion_EP: incluye_direccion_EP,
			credito_getdocs: credito_id
		}),
		beforeSend: function () {
			$(`#cbo_carta_direccion`).html('<option>Cargando...</option>');
		},
		success: function (data) {
			$(`#cbo_carta_direccion`).html(data).change();
		},
		error: function (data) {
			alert(data.responseText);
		}
	});
}