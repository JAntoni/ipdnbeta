<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../cartanotarial/Cartanotarial.class.php';
$oCartanotarial = new Cartanotarial();
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();
require_once '../historial/Historial.class.php';
$oHist = new Historial();
$listo = 1;

$usuario_action = $_POST['action'];

$titulo = '';
$incluye_filtro_estado = 0;//para el select del certificadoregveh
if ($usuario_action == 'I') {
    $titulo = 'Registrar Carta notarial';
    $incluye_filtro_estado = $estado = 1;
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Carta notarial';
} elseif ($usuario_action == 'L') {
    $titulo = 'Carta notarial registrada';
} elseif ($usuario_action == 'M') {
    $titulo = 'Modificar Carta notarial';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action = devuelve_nombre_usuario_action($usuario_action);

if ($listo == 1) {
    $vista = $_POST['vista'];
    $ejecucion_id = $_POST['ejecucion_id'];
    $ejecucionfase_id = $_POST['ejecucionfase_id'];
    $cartanotarial_id = intval($_POST['cartanotarial_id']);
    $ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];
    $credito_id = $ejecucion_reg['tb_credito_id'];
    $credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];
    //buscar si tiene un historial con sgte texto Confirmó la fecha en que llevará las cartas a notaría

    $style_dts_extra = 'display: none'; $disabled_dts_anteriores = $recomendada = '';
    $label_fecha = 'Fecha llevar notaria (provisoria):';

    if (!empty($cartanotarial_id)) {
        $titulo .= ". Id: $cartanotarial_id";
        $res_buscar_cn = $oCartanotarial->listar_todos($cartanotarial_id, '', '', '');

        if ($res_buscar_cn['estado'] == 1) {
            $cn = $res_buscar_cn['data'][0];
            //recoger datos registrados
            $selected_destinat_cli = $cn['tb_cartanotarial_destinatario'] == 1 ? 'selected' : '';
            $selected_destinat_rep = $cn['tb_cartanotarial_destinatario'] == 2 ? 'selected' : '';
            $direccion_id = $cn['tb_direccion_id'];
            $fecha_notaria_recepciona = mostrar_fecha($cn['tb_cartanotarial_fecnotariarecepciona']);
            $certificado_id = $cn['tb_certificadoregveh_id'];
            
            //datos en la fase espera contacto cliente
            $cnestadoentrega_id = $cn['tb_cnestadoentrega_id'];
            $num = $cn['tb_cartanotarial_num'];
            $fec_diligencia = mostrar_fecha($cn['tb_cartanotarial_fecdiligencia']);
            $num_suministro = $cn['tb_cartanotarial_numsuministro'];
        }
        unset($res_buscar_cn);

        $res_ejecucionfase = $oEjecucion->listar_ejecucionfase_por('', $ejecucion_id, '', '', '');
        foreach ($res_ejecucionfase['data'] as $key => $ejecucionfase) {
            $res_hist = $oHist->filtrar_historial_por('tb_ejecucionfase', $ejecucionfase['tb_ejecucionfase_id']);
            if ($res_hist['estado'] == 1) {
                foreach ($res_hist['data'] as $key => $hist) {
                    if (strpos($hist['tb_hist_det'], 'Confirmó la fecha en que llevará las cartas a not') !== false) {
                        $label_fecha = 'Fecha de entrega a notaría:';
                        break;
                    }
                }
            }
        }
    } else {
        $selected_destinat_cli = 'selected';
    }

    if ($vista == 'esperando_contacto_cliente') {
        $style_dts_extra = '';
        $disabled_dts_anteriores = 'disabled';
    }
    if ($vista == 'esperando_contacto_cliente' && empty($cnestadoentrega_id)) {
        //recomendar fecha diligencia
            $fec_diligencia = '';
            require_once '../historial/Historial.class.php';
            $oHist = new Historial();

            $res_carta = $oHist->filtrar_historial_por('tb_cartanotarial', $cartanotarial_id);
            if ($res_carta['estado'] == 1) {
                foreach ($res_carta['data'] as $key => $hist_carta) {
                    if (strpos($hist_carta['tb_hist_det'], 'Estado en Diligencia') > -1) {
                        $fec_diligencia = mostrar_fecha($hist_carta['tb_hist_fecreg']);
                        $recomendada = '<label style="color: red;">Verificar fecha (fue recomendada por sistema)</label>';
                        break;
                    }
                }
            }
            unset($res_carta);
        //
    }

    //Obtener numero suministro
        if ($cn['tb_cartanotarial_destinatario'] == 1) {
            require_once '../creditogarveh/Creditogarveh.class.php';
            $oCredito = new Creditogarveh();
            $res1 = $oCredito->mostrarUno($credito_id);
            $num_suministro_cliente = $res1['data']['tb_credito_suministro'];
            unset($res1);
        }
    //
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_cartanotarial_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_cartanotarial" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action_cartanotarial" id="action_cartanotarial" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_vista_cartanotarial" id="hdd_vista_cartanotarial" value="<?php echo $vista; ?>">
                        <input type="hidden" name="hdd_carta_id" id="hdd_carta_id" value="<?php echo $cartanotarial_id; ?>">
                        <input type="hidden" name="hdd_carta_ejecucion_id" id="hdd_carta_ejecucion_id" value="<?php echo $ejecucion_id; ?>">
                        <input type="hidden" name="hdd_carta_ejecucionfase_id" id="hdd_carta_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                        <input type="hidden" name="hdd_carta_creditogetdocs_id" id="hdd_carta_creditogetdocs_id" value="<?php echo $credito_getdocs; ?>">
                        <input type="hidden" name="hdd_carta_reg" id="hdd_carta_reg" value='<?php echo json_encode($cn); ?>'>

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <label for="cbo_carta_destinatario">Destinatario:</label>
                                    <select name="cbo_carta_destinatario" id="cbo_carta_destinatario" class="form-control mayus input-sm input-shadow <?php echo $disabled_dts_anteriores;?>">
                                        <option value="0">Seleccione</option>
                                        <option value="1" <?php echo $selected_destinat_cli; ?>>Cliente</option>
                                        <option value="2" <?php echo $selected_destinat_rep; ?>>Representante</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <input type="hidden" name="hdd_carta_persona_o_cliente_id" id="hdd_carta_persona_o_cliente_id" value="">
                                    <label for="txt_carta_nrodoc_destinat">Nro Doc:</label>
                                    <input type="text" name="txt_carta_nrodoc_destinat" id="txt_carta_nrodoc_destinat" class="input-sm form-control mayus input-shadow disabled" placeholder="Nro Doc destinatario" value="">
                                </div>

                                <div class="col-md-7">
                                    <label for="txt_carta_nombredestinat">Nombres:</label>
                                    <input type="text" name="txt_carta_nombredestinat" id="txt_carta_nombredestinat" class="input-sm form-control mayus input-shadow disabled" placeholder="Nombres destinatario" value="">
                                </div>
                            </div>

                            <div class="row form-group destinat_fila_2" style="display: none;">
                                <div class="col-md-3"></div>

                                <div class="col-md-2">
                                    <label for="txt_carta_nrodoc_destinat2">Nro Doc:</label>
                                    <input type="text" name="txt_carta_nrodoc_destinat2" id="txt_carta_nrodoc_destinat2" class="input-sm form-control mayus input-shadow disabled" placeholder="Nro Doc destinatario" value="">
                                </div>
                                <div class="col-md-7">
                                    <label for="txt_carta_nombredestinat2">Nombres:</label>
                                    <input type="text" name="txt_carta_nombredestinat2" id="txt_carta_nombredestinat2" class="input-sm form-control mayus input-shadow disabled" placeholder="Nombres destinatario" value="">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-10">
                                    <input type="hidden" name="hdd_carta_direccion_id" id="hdd_carta_direccion_id" value="<?php echo $direccion_id; ?>">
                                    <label for="cbo_carta_direccion">Direccion:</label>
                                    <select name="cbo_carta_direccion" id="cbo_carta_direccion" class="form-control input-sm input-shadow mayus <?php echo $disabled_dts_anteriores;?>"></select>
                                </div>
                                <div class="col-md-1">
                                    <label style="color: white; font-size: 11.5px;">l</label>
                                    <a id="btn_add_direcc" class="btn btn-sm btn-github <?php echo $disabled_dts_anteriores;?>" onclick="mostrar_add_direcc()" title="Añadir dirección">Add direccion</a>
                                </div>
                            </div>

                            <div class="row form-group add_direcc" style="display: none;">
                                <div class="col-md-10">
                                    <input type="text" name="txt_carta_nuevadirecc" id="txt_carta_nuevadirecc" class="input-sm form-control mayus input-shadow" placeholder="Nueva Dirección">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="cbo_dep">Departamento:</label>
                                    <select name="cbo_dep" id="cbo_dep" class="form-control input-sm input-shadow mayus <?php echo $disabled_dts_anteriores;?>">
                                        <?php require_once '../ubigeo/ubigeo_departamento_select.php'; ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <input type="hidden" name="hdd_codpro" id="hdd_codpro" value="">
                                    <label for="cbo_pro">Provincia:</label>
                                    <select name="cbo_pro" id="cbo_pro" class="form-control input-sm input-shadow mayus <?php echo $disabled_dts_anteriores;?>"></select>
                                </div>

                                <div class="col-md-4">
                                    <input type="hidden" name="hdd_coddis" id="hdd_coddis" value="">
                                    <label for="cbo_dis">Distrito:</label>
                                    <select name="cbo_dis" id="cbo_dis" class="form-control input-sm input-shadow mayus <?php echo $disabled_dts_anteriores;?>"></select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="txt_carta_fechanotariarecepciona"><?php echo $label_fecha;?></label>
                                    <div class='input-group date' id='datetimepicker_fec_not_recepciona'>
                                        <input type="text" name="txt_carta_fechanotariarecepciona" id="txt_carta_fechanotariarecepciona" class="form-control input-sm input-shadow mayus <?php echo $disabled_dts_anteriores;?>" placeholder="Fec. Notaria recepciona" value="<?php echo $fecha_notaria_recepciona; ?>">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="col-md-8 certificado">
                                    <label for="cbo_certificado_id">Certificado Reg. Veh para redaccion:</label>
                                    <select name="cbo_certificado_id" id="cbo_certificado_id" class="form-control input-sm input-shadow mayus <?php echo $disabled_dts_anteriores;?>">
                                        <?php require_once '../certificadoregistralveh/certificadoregveh_select.php'; ?>
                                    </select>
                                    <label id="aviso_antiguo" style="display: none; color: red;"></label>
                                </div>
                            </div>

                            <div style="<?php echo $style_dts_extra;?>">
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="cbo_carta_estadoentrega">Estado de recepción:</label>
                                        <select name="cbo_carta_estadoentrega" id="cbo_carta_estadoentrega" class="form-control input-sm input-shadow mayus">
                                            <?php require_once '../cartanotarial/cnestadoentrega_select.php';?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_carta_num">Num. carta:</label>
                                        <input type="text" name="txt_carta_num" id="txt_carta_num" class="form-control input-sm input-shadow mayus" placeholder="EJM: 1012-2024" value="<?php echo $num;?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_carta_fecdiligencia">Fecha de diligencia:</label>
                                        <div class='input-group date'>
                                            <input type="text" name="txt_carta_fecdiligencia" id="txt_carta_fecdiligencia" class="form-control input-sm input-shadow mayus" placeholder="Fec. Diligencia" value="<?php echo $fec_diligencia; ?>">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <?php echo $recomendada;?>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <input type="hidden" name="hdd_cliente_num_suministro" id="hdd_cliente_num_suministro" value="<?php echo $num_suministro_cliente;?>">
                                        <label for="txt_carta_numsuministro">Num. suministro:</label>
                                        <input type="text" name="txt_carta_numsuministro" id="txt_carta_numsuministro" class="form-control input-sm input-shadow mayus" placeholder="Num Suministro" value="<?php echo $num_suministro;?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="cartanotarial_mensaje" style="display: none;">
                            <h4>Cargando...</h4>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_cartanotarial">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_cartanotarial">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cartanotarial/cartanotarial_form.js?ver='.rand(); ?>"></script>
