<?php
require_once '../../core/usuario_sesion.php';
$listo = 1;
$titulo = 'Listado de Cartas notariales';

if ($listo == 1) {
    $ejecucion_id = intval($_POST['ejecucion_id']);
    $ejecucionfase_id = intval($_POST['ejecucionfase_id']);
    $ejecucionfase_completado = $_POST['ejecucionfase_completado'];
    $vista = $_POST['vista'];
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_listadocartanotarial_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo;?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="hdd_cn_ejecucion_id" id="hdd_cn_ejecucion_id" value="<?php echo $ejecucion_id; ?>">
                    <input type="hidden" name="hdd_cn_ejecucionfase_id" id="hdd_cn_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                    <input type="hidden" name="hdd_cn_ejecucionfase_completado" id="hdd_cn_ejecucionfase_completado" value="<?php echo $ejecucionfase_completado; ?>">
                    <input type="hidden" name="hdd_cn_vista" id="hdd_cn_vista" value="<?php echo $vista;?>">

                    <?php if ($ejecucionfase_completado != 1 && $vista == 'cn_redaccion') { ?>
                        <div class="row">
                            <div class="col-md-1">
                                <button onclick="cartanotarial_form('I', 0)" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Nueva</button>
                            </div>
                        </div>
                        <br>
                    <?php } ?>
                    <div class="row">
                        <input type="hidden" id="hdd_datatable_cartanotarials_fil">

                        <div class="col-md-12">
                            <!-- tabla -->
                            <div id="div_html_tablacartanotarial" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cartanotarial/cartanotarial_tabla_form.js?ver='.rand(); ?>"></script>
