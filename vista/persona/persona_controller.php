<?php
//revision de sesion activa
	if (defined('VISTA_URL')) {
		require_once(APP_URL . 'core/usuario_sesion.php');
	} else {
		require_once('../../core/usuario_sesion.php');
	}
//
//todos los require
	require_once('../persona/Persona.class.php');
	$oPersona = new Persona();
	require_once('../persona/Cargopersona.class.php');
	require_once('../direccion/Direccion.class.php');
	require_once('../historial/Historial.class.php');
	$oHist = new Historial();
	require_once('../funciones/fechas.php');
	require_once('../funciones/funciones.php');
//

//datos estaticos
	$fecha_hora			= date('d-m-Y h:i a');
	$data['estado']		= 0;
	$data['mensaje']	= '';
//

//datos POST
	$action			= $_POST['action_persona'];
	$persona_id		= $_POST['hdd_persona_id'];
	$reg_origin		= (array) json_decode($_POST['hdd_persona_reg']);

	$persona_nom	= strtoupper($_POST['txt_persona_nom']);
	$persona_ape	= strtoupper($_POST['txt_persona_ape']);
	$persona_doc	= $_POST['txt_persona_doc'];
	$persona_numcel = $_POST['txt_persona_numcel'];
//

//asignar a Persona
	$oPersona->nom		= $persona_nom;
	$oPersona->ape		= $persona_ape;
	$oPersona->doc		= $persona_doc;
	$oPersona->numcel	= !empty($persona_numcel) ? $persona_numcel : null;
	$oPersona->usumod	= $_SESSION['usuario_id'];
//

//al insertar o modificar se verifica si el nro doc está en un registro de la bd con id diferente
	$res = $oPersona->mostrar_uno_doc($persona_doc, $persona_id);
	$dni_repetido = boolval($res['estado'] == 1);
	if ($dni_repetido) {
		$data['mensaje'] = "EL N° DOCUMENTO $persona_doc YA ESTÁ REGISTRADO EN OTRA PERSONA: {$res['data']['tb_persona_nom']} {$res['data']['tb_persona_ape']}";
		echo json_encode($data); exit();
	}
	unset($res);
//

if ($action == 'insertar') {
	$oPersona->usureg = $_SESSION['usuario_id'];
	$res_insert = $oPersona->insertar();

	if ($res_insert['estado'] == 1) {
		$data['estado']		= 1;
		$data['mensaje']   .= "● " . $res_insert['mensaje'] . ". ID: " . $res_insert['nuevo'];
		$data['nuevo_id']	= $res_insert['nuevo'];

		//aqui viene el registro de cargo
		$count_cargos	= intval($_POST['cantidad_cargos']);
		$arr_cargos		= array();
		for ($i_cargos = 1; $i_cargos <= $count_cargos; $i_cargos++) {
			//opcional
			$fecfin_opc = $_POST["txt_cargopersonafecfin_$i_cargos"];
			
			$oCargopersona = new Cargopersona();
			$oCargopersona->cargo_id	= intval($_POST["cbo_cargopersonacargo_$i_cargos"]);
			$oCargopersona->persona_id	= $res_insert['nuevo'];
			$oCargopersona->fecini		= fecha_mysql($_POST["txt_cargopersonafecini_$i_cargos"]);
			$oCargopersona->fecfin		= !empty($fecfin_opc) ? fecha_mysql($fecfin_opc) : null;
			$oCargopersona->usureg		= $_SESSION['usuario_id'];
			$arr_cargos[$i_cargos]		= $oCargopersona;

			$oCargopersona->insertar();
		}

		//aqui viene el registro de direcciones
		$count_direc	= intval($_POST['cantidad_direcciones']);
		$arr_direc		= array();
		for ($i_direc = 1; $i_direc <= $count_direc; $i_direc++) {
			//opcional
			$desc_opc	= trim(strtoupper($_POST["txt_direcciondesc_$i_direc"]));
			$fecfin_opc = $_POST["txt_direccionfecfin_$i_direc"];
			$depa = $_POST["cbo_direcciondep_$i_direc"];
			$prov = $_POST["cbo_direccionprov_$i_direc"];
			$dist = $_POST["cbo_direcciondist_$i_direc"];

			$oDireccion = new Direccion();
			$oDireccion->des = !empty($desc_opc) ? $desc_opc : null;
			$oDireccion->dir = trim(strtoupper($_POST["txt_direcciondir_$i_direc"]));
			$oDireccion->ubigeo = $depa . "" . $prov . "" . $dist;
			$oDireccion->tablanom = "tb_persona";
			$oDireccion->tablaid = $res_insert['nuevo'];
			$oDireccion->fecini		= fecha_mysql($_POST["txt_cargopersonafecini_$i_direc"]);
			$oDireccion->fecfin		= !empty($fecfin_opc) ? fecha_mysql($fecfin_opc) : null;
			$oDireccion->usureg		= $_SESSION['usuario_id'];
			$arr_direc[$i_direc]		= $oDireccion;

			$oDireccion->insertar();
		}
	}
	unset($res_insert);
}
elseif($action == 'modificar'){
	$igual[0] = boolval($oPersona->nom == $reg_origin['tb_persona_nom']);
	$igual[1] = boolval($oPersona->ape == $reg_origin['tb_persona_ape']);
	$igual[2] = boolval($oPersona->doc == $reg_origin['tb_persona_doc']);
	$igual[3] = boolval($oPersona->numcel == $reg_origin['tb_persona_numcel']);

	$son_iguales = true;
	$i = 0;
	while ($i < count($igual)) {
		if (!$igual[$i]) {
			$son_iguales = false;
		}
		$i++;
	}
	unset($i);

	//REVISION DE CAMBIOS/MODIFICACIONES EN TABLA CARGOS
		$cargos_origin = (array) json_decode($_POST['hdd_cargos_array']);
		$contador = 0;
		while ($contador < count($cargos_origin)) {
			$cargos_origin[$contador] = (array) $cargos_origin[$contador];
			$contador++;
		}
		$count_cargos = intval($_POST['cantidad_cargos']);

		$cargo_eliminado = array();
		$cargo_original_mod = array();
		$fila_corresponde_cargo = array();
		$evaluar_cargo = array();
		$contador = 1;
		while ($contador <= count($cargos_origin)) {
			$cargo_eliminado[$contador] = 1; //está eliminado
			$cargo_original_mod[$contador] = 0;
			$fila_corresponde_cargo[$contador] = null;
			$contador++;
		}

		for ($i = 0; $i < count($cargos_origin); $i++) {
			$contador = 0;
			for ($j = 1; $j <= $count_cargos; $j++) {
				if (intval($cargos_origin[$i]['tb_cargopersona_id']) == intval($_POST["hdd_cargopersonaid_$j"])) {
					$cargo_eliminado[$i + 1] = 0; //no se eliminó el detalle
					$fila_corresponde_cargo[$i + 1] = $j; //fila donde esta el registro

					$evaluar_cargo[$i + 1][0] = boolval($cargos_origin[$i]['tb_cargo_id'] == $_POST["txt_cargopersonacargo_$j"]);
					$evaluar_cargo[$i + 1][1] = boolval($cargos_origin[$i]['tb_cargopersona_fecini'] == $_POST["txt_cargopersonafecini_$j"]);
					$_POST["txt_cargopersonafecfin_$j"] = empty($_POST["txt_cargopersonafecfin_$j"]) ? 'actualidad' : $_POST["txt_cargopersonafecfin_$j"];
					$evaluar_cargo[$i + 1][2] = boolval($cargos_origin[$i]['tb_cargopersona_fecfin'] == $_POST["txt_cargopersonafecfin_$j"]);

					//guardar 1 o 0 para saber si está modificado
					//guardar el numero j para saber cual es la fila donde se modificaron los datos
					while ($contador < count($evaluar_cargo[$i + 1])) {
						if (!$evaluar_cargo[$i + 1][$contador]) {
							$cargo_original_mod[$i + 1] = 1;
						}
						$contador++;
					}
					break;
				}
			}
		}

		$cargos_son_iguales = true;
		if (count($cargos_origin) != $count_cargos) {
			$cargos_son_iguales = false;
		}
		$i = 1;
		while ($i < count($cargos_origin) + 1) {
			if ($cargo_eliminado[$i] == 1 || $cargo_original_mod[$i] == 1) {
				$cargos_son_iguales = false;
			}
			$i++;
		}
	//FIN DE REVISION DE CAMBIOS EN TABLA CARGOS

	//REVISION DE CAMBIOS/MODIFICACIONES EN TABLA DIRECCIONES
		$direcciones_origin = (array) json_decode($_POST['hdd_direcciones_array']);
		$contador = 0;
		while ($contador < count($direcciones_origin)) {
			$direcciones_origin[$contador] = (array) $direcciones_origin[$contador];
			$contador++;
		}
		$count_direcciones = intval($_POST['cantidad_direcciones']);

		$direccion_eliminado = array();
		$direccion_original_mod = array();
		$fila_corresponde_direccion = array();
		$evaluar_direccion = array();
		$contador = 1;
		while ($contador <= count($direcciones_origin)) {
			$direccion_eliminado[$contador] = 1; //está eliminado
			$direccion_original_mod[$contador] = 0;
			$fila_corresponde_direccion[$contador] = null;
			$contador++;
		}

		for ($i = 0; $i < count($direcciones_origin); $i++) {
			$contador = 0;
			for ($j = 1; $j <= $count_direcciones; $j++) {
				if (intval($direcciones_origin[$i]['tb_direccion_id']) == intval($_POST["hdd_direccionid_$j"])) {
					$direccion_eliminado[$i + 1] = 0; //no se eliminó el detalle
					$fila_corresponde_direccion[$i + 1] = $j; //fila donde esta el registro

					$evaluar_direccion[$i + 1][0] = boolval($direcciones_origin[$i]['tb_direccion_des'] == $_POST["txt_direcciondesc_$j"]);
					$evaluar_direccion[$i + 1][1] = boolval($direcciones_origin[$i]['tb_direccion_dir'] == $_POST["txt_direcciondir_$j"]);
					$evaluar_direccion[$i + 1][2] = boolval($direcciones_origin[$i]['cod_departamento'] == $_POST["txt_direcciondep_$j"]);
					$evaluar_direccion[$i + 1][3] = boolval($direcciones_origin[$i]['cod_provincia'] == $_POST["txt_direccionprov_$j"]);
					$evaluar_direccion[$i + 1][4] = boolval($direcciones_origin[$i]['cod_distrito'] == $_POST["txt_direcciondist_$j"]);
					$evaluar_direccion[$i + 1][5] = boolval($direcciones_origin[$i]['tb_direccion_fecini'] == $_POST["txt_direccionfecini_$j"]);
					$_POST["txt_direccionfecfin_$j"] = empty($_POST["txt_direccionfecfin_$j"]) ? 'actualidad' : $_POST["txt_direccionfecfin_$j"];
					$evaluar_direccion[$i + 1][6] = boolval($direcciones_origin[$i]['tb_direccion_fecfin'] == $_POST["txt_direccionfecfin_$j"]);

					//guardar 1 o 0 para saber si está modificado
					//guardar el numero j para saber cual es la fila donde se modificaron los datos
					while ($contador < count($evaluar_direccion[$i + 1])) {
						if (!$evaluar_direccion[$i + 1][$contador]) {
							$direccion_original_mod[$i + 1] = 1;
						}
						$contador++;
					}
					break;
				}
			}
		}

		$direcciones_son_iguales = true;
		if (count($direcciones_origin) != $count_direcciones) {
			$direcciones_son_iguales = false;
		}
		$i = 1;
		while ($i < count($direcciones_origin) + 1) {
			if ($direccion_eliminado[$i] == 1 || $direccion_original_mod[$i] == 1) {
				$direcciones_son_iguales = false;
			}
			$i++;
		}
	//FIN DE REVISION DE CAMBIOS EN TABLA DIRECCIONES

	if ($son_iguales && $cargos_son_iguales && $direcciones_son_iguales) {
		$data['estado'] = 2;
		$data['mensaje'] .= "● No se realizó ninguna modificación";
	}
	else {
		$oPersona->id = $persona_id;

		if (!$son_iguales) {
			$res_modify = $oPersona->modificar();
		}

		//ESPACIO PARA MODIFICAR LOS CARGOS
		$mensaje_cargos = '';
		if(!$cargos_son_iguales){
			for ($i = 1 ; $i <= $count_cargos; $i++) {
				if (intval($_POST["hdd_cargopersonaid_$i"]) == 0) { //ese cargo es nuevo
					//uno a uno
					$oCargopersona = new Cargopersona();
					$oCargopersona->cargo_id = intval($_POST["cbo_cargopersonacargo_$i"]);
					$oCargopersona->persona_id = $persona_id;
					$oCargopersona->fecini = fecha_mysql($_POST["txt_cargopersonafecini_$i"]);
					$oCargopersona->usureg = $_SESSION['usuario_id'];
					$fecfin_cargopersona = $_POST["txt_cargopersonafecfin_$i"];
					if (!empty($fecfin_cargopersona)) {
						$oCargopersona->fecfin = fecha_mysql($fecfin_cargopersona);
					} else {
						$fecfin_cargopersona = 'actualidad';
					}
					$res_insert_cargopersona = $oCargopersona->insertar();

					$mensaje_cargos .= "<br> - Añadió un cargo, fila $i (id cargo_persona {$res_insert_cargopersona['nuevo']}): ". $_POST["hdd_cargopersonacargonom_$i"] . '. Vigencia: ' . $_POST["txt_cargopersonafecini_$i"]." - $fecfin_cargopersona.";
					unset($fecfin_cargopersona, $oCargopersona, $res_insert_cargopersona);
				}
			}

			for ($i = 1 ; $i < count($cargos_origin)+1; $i++) {
				if ($cargo_eliminado[$i] == 1) {
					$oCargopersona = new Cargopersona();
					$oCargopersona->modificar_campo($cargos_origin[($i-1)]['tb_cargopersona_id'], 'tb_cargopersona_xac', 0, 'INT');
					$mensaje_cargos .= "<br> - Eliminó el cargo de la fila $i (id cargo_persona {$cargos_origin[($i-1)]['tb_cargopersona_id']}): ". $cargos_origin[($i-1)]['tb_cargo_nom'].', de vigencia: '. $cargos_origin[($i-1)]['tb_cargopersona_fecini'].' - '. $cargos_origin[($i-1)]['tb_cargopersona_fecfin'] .'.';
				}

				if ($cargo_original_mod[$i] == 1) {
					$oCargopersona = new Cargopersona();
					$oCargopersona->cargo_id = $_POST['txt_cargopersonacargo_'.$fila_corresponde_cargo[$i]];
					$oCargopersona->persona_id = $persona_id;
					$oCargopersona->fecini = fecha_mysql($_POST['txt_cargopersonafecini_'.$fila_corresponde_cargo[$i]]);
					if (!$evaluar_cargo[$i][2]) {
						$oCargopersona->fecfin = fecha_mysql($_POST['txt_cargopersonafecfin_'.$fila_corresponde_cargo[$i]]);
					}
					$oCargopersona->id = $_POST['hdd_cargopersonaid_'.$fila_corresponde_cargo[$i]];
					$oCargopersona->modificar();
					
					$mensaje_cargos .= "<br> - Modificó el cargo de la fila $i (id cargo_persona {$cargos_origin[($i-1)]['tb_cargopersona_id']}):";
					if(!$evaluar_cargo[$i][0])
						$mensaje_cargos .= ' Cargo: ' . $cargos_origin[$i-1]['tb_cargo_nom'] . ' => <b>'. $_POST['hdd_cargopersonacargonom_'.$fila_corresponde_cargo[$i]].'</b>.';
					if(!$evaluar_cargo[$i][1])
						$mensaje_cargos .= ' Fec. inicio: ' . $cargos_origin[$i-1]['tb_cargopersona_fecini'] . ' => <b>'. $_POST['txt_cargopersonafecini_'.$fila_corresponde_cargo[$i]].'</b>.';
					if(!$evaluar_cargo[$i][2])
						$mensaje_cargos .= ' Fec. fin: ' . $cargos_origin[$i-1]['tb_cargopersona_fecfin'] . ' => <b>'. $_POST['txt_cargopersonafecfin_'.$fila_corresponde_cargo[$i]] .'</b>.';

					unset($oCargopersona);
				}
			}
		}

		//ESPACIO PARA MODIFICAR LAS DIRECCIONES
		$mensaje_direcciones = '';
		if(!$direcciones_son_iguales){
			for ($i = 1 ; $i <= $count_direcciones; $i++) {
				if (intval($_POST["hdd_direccionid_$i"]) == 0) {
					$oDireccion = new Direccion();
					$oDireccion->des = trim(strtoupper($_POST["txt_direcciondesc_$i"]));
					$oDireccion->dir = trim(strtoupper($_POST["txt_direcciondir_$i"]));
					$oDireccion->ubigeo = $_POST["cbo_direcciondep_$i"].$_POST["cbo_direccionprov_$i"].$_POST["cbo_direcciondist_$i"];
					$oDireccion->tablanom = 'tb_persona';
					$oDireccion->tablaid = $persona_id;
					$oDireccion->fecini = fecha_mysql($_POST["txt_direccionfecini_$i"]);
					$oDireccion->usureg = $_SESSION['usuario_id'];
					$fecfin_direccion = $_POST["txt_direccionfecfin_$i"];
					if (!empty($fecfin_direccion)) {
						$oDireccion->fecfin = fecha_mysql($fecfin_direccion);
					} else {
						$fecfin_direccion = 'actualidad';
					}

					$res_insert_direccion = $oDireccion->insertar();

					$mensaje_direcciones .= "<br> - Añadió una direccion, fila $i (id tb_direccion {$res_insert_direccion['nuevo']}): {$oDireccion->des} - {$oDireccion->dir}. Ubigeo: {$oDireccion->ubigeo}". '. Vigencia: ' . $_POST["txt_direccionfecini_$i"]." - $fecfin_direccion.";
					unset($fecfin_direccion, $oDireccion, $res_insert_direccion);
				}
			}

			for ($i = 1 ; $i < count($direcciones_origin)+1; $i++) {
				if ($direccion_eliminado[$i] == 1) {
					$oDireccion = new Direccion();
					$oDireccion->modificar_campo($direcciones_origin[($i-1)]['tb_direccion_id'], 'tb_cargopersona_xac', 0, 'INT');
					$mensaje_direcciones .= "<br> - Eliminó la direccion de la fila $i (id tb_direccion {$direcciones_origin[($i-1)]['tb_direccion_id']}): {$direcciones_origin[($i-1)]['tb_direccion_des']} - {$direcciones_origin[($i-1)]['tb_direccion_dir']}. Ubigeo: {$direcciones_origin[($i-1)]['tb_direccion_ubigeo']}".', de vigencia: '. $direcciones_origin[($i-1)]['tb_direccion_fecini'].' - '. $direcciones_origin[($i-1)]['tb_direccion_fecfin'] .'.';
				}

				if ($direccion_original_mod[$i] == 1) {
					$oDireccion = new Direccion();
					
					$mensaje_direcciones .= "<br> - Modificó la direccion de la fila $i (id tb_direccion {$direcciones_origin[($i-1)]['tb_direccion_id']}):";
					if (!$evaluar_direccion[$i][0]) {
						$oDireccion->modificar_campo($_POST['hdd_direccionid_'.$fila_corresponde_direccion[$i]], 'tb_direccion_des', $_POST['txt_direcciondesc_'.$fila_corresponde_direccion[$i]], 'STR');
						$mensaje_direcciones .= ' Descripcion: ' . $direcciones_origin[$i-1]['tb_direccion_des'] . ' => <b>'. $_POST['txt_direcciondesc_'.$fila_corresponde_direccion[$i]].'</b>.';
					}
					if (!$evaluar_direccion[$i][1]) {
						$oDireccion->modificar_campo($_POST['hdd_direccionid_'.$fila_corresponde_direccion[$i]], 'tb_direccion_dir', $_POST['txt_direcciondir_'.$fila_corresponde_direccion[$i]], 'STR');
						$mensaje_direcciones .= ' Direccion: ' . $direcciones_origin[$i-1]['tb_direccion_dir'] . ' => <b>'. $_POST['txt_direcciondir_'.$fila_corresponde_direccion[$i]].'</b>.';
					}
					if (!$evaluar_direccion[$i][2] || !$evaluar_direccion[$i][3] || !$evaluar_direccion[$i][4]) {
						$new_ubigeo = $_POST['txt_direcciondep_'.$fila_corresponde_direccion[$i]] . $_POST['txt_direccionprov_'.$fila_corresponde_direccion[$i]] . $_POST['txt_direcciondist_'.$fila_corresponde_direccion[$i]];
						$oDireccion->modificar_campo($_POST['hdd_direccionid_'.$fila_corresponde_direccion[$i]], 'tb_direccion_ubigeo', $new_ubigeo, 'STR');
						$mensaje_direcciones .= ' Ubigeo: ' . $direcciones_origin[$i-1]['tb_direccion_ubigeo'] . ' => <b>'.$new_ubigeo.'</b>.';
						unset($new_ubigeo);
					}
					if (!$evaluar_direccion[$i][5]) {
						$oDireccion->modificar_campo($_POST['hdd_direccionid_'.$fila_corresponde_direccion[$i]], 'tb_direccion_fecini', fecha_mysql($_POST['txt_direccionfecini_'.$fila_corresponde_direccion[$i]]), 'STR');
						$mensaje_direcciones .= ' Fec. inicio: ' . $direcciones_origin[$i-1]['tb_direccion_fecini'] . ' => <b>'. $_POST['txt_direccionfecini_'.$fila_corresponde_direccion[$i]] .'</b>.';
					}
					if (!$evaluar_direccion[$i][6]) {
						$oDireccion->modificar_campo($_POST['hdd_direccionid_'.$fila_corresponde_direccion[$i]], 'tb_direccion_fecfin', fecha_mysql($_POST['txt_direccionfecfin_'.$fila_corresponde_direccion[$i]]), 'STR');
						$mensaje_direcciones .= ' Fec. fin: ' . $direcciones_origin[$i-1]['tb_direccion_fecfin'] . ' => <b>'. $_POST['txt_direccionfecfin_'.$fila_corresponde_direccion[$i]].'</b>.';
					}

					unset($oDireccion);
				}
			}
		}

		if ($res_modify == 1 || !$cargos_son_iguales || !$direcciones_son_iguales) {
			$data['estado'] = 1;
			$data['mensaje'] .= '● Modificado correctamente';

			//insertar historial
				$oHist->setTbHistUsureg($_SESSION['usuario_id']);
				$oHist->setTbHistNomTabla('tb_persona');
				$oHist->setTbHistRegmodid($persona_id);
				$mensaje = "Modificó la persona con id $persona_id:";
				if (!$son_iguales) {
					if (!$evaluar[0])
						$mensaje .= "<br> - Cambió el nombre: {$reg_origin['tb_persona_nom']} => <b>{$oPersona->nom}</b>";
					if (!$evaluar[1])
						$mensaje .= "<br> - Cambió el apellido: {$reg_origin['tb_persona_ape']} => <b>{$oPersona->ape}</b>";
					if (!$evaluar[2])
						$mensaje .= "<br> - Cambió el nro documento: {$reg_origin['tb_persona_doc']} => <b>{$oPersona->doc}</b>";
					if (!$evaluar[3])
						$mensaje .= "<br> - Cambió el nro de celular: {$reg_origin['tb_persona_numcel']} => <b>{$oPersona->numcel}</b>";
				}
				if (!$cargos_son_iguales)
					$mensaje .= $mensaje_cargos;
				if (!$direcciones_son_iguales)
					$mensaje .= $mensaje_direcciones;
				
				$oHist->setTbHistDet($mensaje);
				$oHist->insertar();
			//
		}
		unset($res_modify);
	}
}
elseif ($action == 'eliminar') {
	$columna = 'tb_persona_xac';
	$valor = 0;
	$tipo_dato = 'INT';
	
	$res_delete = $oPersona->modificar_campo($persona_id, $columna, $valor, $tipo_dato, $_SESSION['usuario_id']);

	if ($res_delete == 1) {
		$data['estado'] = 1;
		$data['mensaje'] = "Se eliminó correctamente";
	} else {
		$data['mensaje'] = "Error";
	}
	unset($res_delete);
}

echo json_encode($data);
