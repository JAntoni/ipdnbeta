<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');
//

class Cargopersona extends Conexion {
    public $id;
    public $cargo_id;
    public $persona_id;
    public $fecini;
    public $fecfin;
    public $usureg;
    public $fecreg;
    public $xac;

    public function listar_todos($persona_id, $cargo_id, $registros_vigentes, $columna_group) {
        //$registros vigentes puede ser "actualidad" o una fecha "2019-01-01"
        $where_opt = $group_opt = '';
        if (!empty($persona_id)) {
            $where_opt .= ' AND cp.tb_persona_id = :param_opc';
        }
        if (!empty($cargo_id)) {
            $where_opt .= " AND cp.tb_cargo_id in ($cargo_id)";
        }
        if (!empty($registros_vigentes)) {
            if ($registros_vigentes == 'actualidad') {
                $where_opt .= ' AND (tb_cargopersona_fecfin IS NULL OR tb_cargopersona_fecfin = "")';
            }
            else {
                $where_opt .=" AND (
                    DATE_FORMAT(cp.tb_cargopersona_fecini, '%Y-%m-%d') <= '$registros_vigentes' AND
                    IF ((cp.tb_cargopersona_fecfin IS NULL OR cp.tb_cargopersona_fecfin = ''), TRUE, DATE_FORMAT(cp.tb_cargopersona_fecfin, '%Y-%m-%d') >= '$registros_vigentes')
                )";
            }
        }
        if (!empty($columna_group)) {
            $group_opt .= " GROUP BY cp.$columna_group";
        }
        try {
            $sql = "SELECT cp.*, c.tb_cargo_nom,
                        p.tb_persona_nom, p.tb_persona_ape, p.tb_persona_doc, p.tb_persona_numcel
                    FROM tb_cargopersona cp
                    INNER JOIN tb_cargo c ON (cp.tb_cargo_id = c.tb_cargo_id)
                    INNER JOIN tb_persona p ON (cp.tb_persona_id = p.tb_persona_id)
                    WHERE cp.tb_cargopersona_xac = 1 $where_opt $group_opt
                    ORDER BY cp.tb_cargopersona_fecreg DESC;";

            $sentencia = $this->dblink->prepare($sql);
            if (!empty($persona_id)) {
                $sentencia->bindParam(':param_opc', $persona_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $column_opc = !empty($this->fecfin) ? ",\ntb_cargopersona_fecfin" : '';
            $param_opc = !empty($this->fecfin) ? ",\n:param_opc1" : '';

            $sql = "INSERT INTO tb_cargopersona (
                        tb_cargo_id,
                        tb_persona_id,
                        tb_cargopersona_fecini,
                        tb_cargopersona_usureg$column_opc)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3$param_opc)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->cargo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->persona_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->fecini, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->usureg, PDO::PARAM_INT);
            if (!empty($this->fecfin)) {
                $sentencia->bindParam(':param_opc1', $this->fecfin, PDO::PARAM_STR);
            }

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar_campo($cargopersona_id, $cargopersona_columna, $cargopersona_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($persona_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_cargopersona SET " . $cargopersona_columna . " = :cargopersona_valor WHERE tb_persona_id = :cargopersona_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(':cargopersona_id', $cargopersona_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(':cargopersona_valor', $cargopersona_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(':cargopersona_valor', $cargopersona_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar() {
        $this->dblink->beginTransaction();
        try {
            $opc = !empty($this->fecfin) ? ",\ntb_cargopersona_fecfin = :param_opc1" : '';

            $sql = "UPDATE tb_cargopersona
              SET
                tb_cargo_id = :param0,
                tb_persona_id = :param1,
                tb_cargopersona_fecini = :param2$opc
              WHERE
                tb_cargopersona_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->cargo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->persona_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->fecini, PDO::PARAM_STR);
            if (!empty($this->fecfin)) {
                $sentencia->bindParam(':param_opc1', $this->fecfin, PDO::PARAM_STR);
            }
            $sentencia->bindParam(':paramx', $this->id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
}
