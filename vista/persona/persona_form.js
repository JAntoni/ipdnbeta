var opciones_cargos = null;
var opciones_departamentos = null;
var no_ocultar = 0;

$(document).ready(function () {
    $('#cbo_cargos').hide();
	$('#cbo_cargos option[value=0]').text('Seleccione');
    opciones_cargos = $("select[id*='cbo_cargos']")[0].innerHTML; //[0] .parentElement .innerHTML

    $('#cbo_departamentos').hide();
	$('#cbo_departamentos option[value=0]').text('Seleccione');
    opciones_departamentos = $("select[id*='cbo_departamentos']")[0].innerHTML; //[0] .parentElement .innerHTML

    $('#cbo_cargo').on('change', function(){
        var cargo_id = parseInt($(this).val());
        if (cargo_id === 9) {
            $('#direc_req').show(100);
        } else {
            $('#direc_req').hide(100);
        }
    });

    $('.num_cel').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '999999999'
    });

    $('#form_persona').validate({
        submitHandler: function(event) {
            var continuar = 1;
            if ($('#action_persona').val() != 'eliminar') {
                //si añade cargo, obligatorio los campos select[id*="cbo_cargopersonacargo"], input[id*="txt_cargopersonafecini"]
                var action_pers = $('#action_persona').val(), cant_cargos = parseInt($('#cantidad_cargos').val());
                var titulo = 'ALERTA', mensaje = '';
                if($('#tbl_detalle_cargos').find('select[id*="cbo_cargopersonacargo"]').length < 1 && action_pers == 'insertar') {
                    continuar = 0;
                    mensaje = 'Debe registrar al menos un cargo para esta persona';
                }
                else if ($(`#cbo_cargopersonacargo_${cant_cargos}`).val() < 1) {
                    continuar = 0;
                    mensaje = 'Debe elegir un cargo para esta persona. En tabla de cargos, fila '+cant_cargos;
                }
                else if ($(`#txt_cargopersonafecini_${cant_cargos}`).length > 0 && $(`#txt_cargopersonafecini_${cant_cargos}`).val().length != 10) {
                    continuar = 0;
                    mensaje = 'Fecha de inicio en cargo está incorrecta. En tabla de cargos, fila '+cant_cargos;
                }
                else if ($(`#cbo_cargopersonacargo_${cant_cargos}`).val() == 9 && $('#tbody_dir').find('tr').length < 1) {
                    continuar = 0;
                    mensaje = 'Debe registrar al menos una direccion para el cargo de "REPRESENTANTE PARA CLIENTES CGV"';
                }
                //verificaciones en tabla de direcciones
                else if ($('#tbl_detalle_dir').find('input[id*="txt_direcciondesc"]').length > 0) {
                    $('#tbl_detalle_dir').find('input[id*="txt_direccion"]').not($('input[id*="txt_direccionfecfin"]')).each(function(){
                        if ($(this).val() == ''){
                            continuar = 0;
                            $(this).addClass('req');
                        } else {
                            $(this).removeClass('req');
                        }
                    });
                    $('#tbl_detalle_dir').find('select[id*="cbo_direccion"]').each(function(){
                        if (parseInt($(this).val()) < 1 || isNaN(parseInt($(this).val()))){
                            continuar = 0;
                            $(this).addClass('req');
                        } else {
                            $(this).removeClass('req');
                        }
                    });
                    if (continuar == 0) {
                        mensaje = 'Complete los campos obligatorios';
                    }
                }
            }

            if (continuar != 1) {
                alertas_error(titulo, mensaje, 'small');
                return;
            }

            $.ajax({
                type: 'POST',
                url: VISTA_URL + 'persona/persona_controller.php',
                async: true,
                dataType: 'json',
                data: serializar_form_persona(),
                beforeSend: function () {
                    $('#persona_mensaje').show(400);
                    $('#btn_guardar_persona').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#persona_mensaje').removeClass('callout-info').addClass('callout-success');
                        $('#persona_mensaje').html(`<h4>${data.mensaje}</h4>`);
                        if ($.inArray($('#action_persona').val(), ['insertar', 'modificar']) > -1) {
                            disabled($('#form_persona').find("button[class*='btn-sm'], select, input, textarea"));
                        }
        
                        setTimeout(function () {
                            $('#modal_persona_form').modal('hide');
                            if (data.estado == 1) {
                                persona_tabla();
                            }
                        }, 2800);
                    }
                    else {
                        console.log(data);
                        $('#persona_mensaje').removeClass('callout-info').addClass('callout-warning');
                        $('#persona_mensaje').html('<h4>Alerta: ' + data.mensaje +'<h4>');
                        $('#btn_guardar_persona').prop('disabled', false);
                    }
                },
                complete: function(data) {
                    console.log(data);
                },
                error: function (data) {
                    $('#persona_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#persona_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_persona_nom: {
                required: true
            },
            txt_persona_ape: {
                required: true
            },
            txt_persona_doc: {
                required: true
            },
            txt_persona_numcel: {
                required: true
            }
        },
        messages: {
            txt_persona_nom: {
                required: 'Obligatorio*'
            },
            txt_persona_ape: {
                required: 'Obligatorio*'
            },
            txt_persona_doc: {
                required: 'Obligatorio*'
            },
            txt_persona_numcel: {
                required: 'Obligatorio*'
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    $('#tbody_dir')
	.on('click', '.remove', function () {
		var filas_detalle = parseInt($('#cantidad_direcciones').val()) - 1;
		$('#cantidad_direcciones').val(filas_detalle);
		var child = $(this).closest('tr').nextAll();
        $('#add_row_dir').removeAttr("style");

		// Iterating across all the rows obtained to change the index
		child.each(function () {
			// Getting <tr> id.
			var id = $(this).attr('id');

			// Gets the row number from <tr> id.
			var dig = parseInt(id.split('_')[3]);

			// Modifying row id.
			$(this).attr('id', `tabla_cabecera_fila_${dig - 1}`);

			//todos los hijos input, select, button se cambiará su id
			$(this).find('input, select, button, i').each(function () {
				//todos los id solo deben tener tres partes, donde la parte 3 es el num de orden empezando desde cero
				id_antiguo = $(this)[0].id.split('_')[2];
				$(this).attr('id', $(this)[0].id.replace(id_antiguo, (dig - 1)));
				$(this).attr('name', $(this)[0].id.replace(id_antiguo, (dig - 1)));
			});
		});

		// Removing the current row.
		$(this).closest('tr').remove();
	});
    
    $('#tbody_cargos')
	.on('click', '.remove', function () {
		var filas_detalle = parseInt($('#cantidad_cargos').val()) - 1;
		$('#cantidad_cargos').val(filas_detalle);
		var child = $(this).closest('tr').nextAll();
        $('#add_row_cargo').removeAttr("style");

		// Iterating across all the rows obtained to change the index
		child.each(function () {
			// Getting <tr> id.
			var id = $(this).attr('id');

			// Gets the row number from <tr> id.
			var dig = parseInt(id.split('_')[3]);

			// Modifying row id.
			$(this).attr('id', `tabla_cabecera_fila_${dig - 1}`);

			//todos los hijos input, select, button se cambiará su id
			$(this).find('input, select, button, i').each(function () {
				//todos los id solo deben tener tres partes, donde la parte 3 es el num de orden empezando desde cero
				id_antiguo = $(this)[0].id.split('_')[2];
				$(this).attr('id', $(this)[0].id.replace(id_antiguo, (dig - 1)));
				$(this).attr('name', $(this)[0].id.replace(id_antiguo, (dig - 1)));
			});
		});

        //eliminar la fila de direccion generada en automatico
        if ($(this).closest('tr').find('select[id*="cbo_cargopersonacargo"]').val() == 9){
            $('button.direcc_repcgv').click();
        }
        // Removing the current row.
		$(this).closest('tr').remove();
	});   

    //COMPLETAR FILAS CARGOS
	var array_cargos = JSON.parse($('#hdd_cargos_array').val());
	var cont = 0;
	if(array_cargos != null){
        while(cont < array_cargos.length){
            nuevo_cargo(0);

            if (parseInt(array_cargos[cont]['tb_cargo_id']) == 10) {
                no_ocultar = 1;
            }
            
            $(`#hdd_cargopersonaid_${cont+1}`).val(array_cargos[cont]['tb_cargopersona_id']);
            $(`td.cargo_select_${cont+1}`).html(`<input type="hidden" name="txt_cargopersonacargo_${cont+1}" id="txt_cargopersonacargo_${cont+1}" value="${array_cargos[cont]['tb_cargo_id']}">` + array_cargos[cont]['tb_cargo_nom']);
            $(`td.cargo_fecini_${cont+1}`).html(`<input type="hidden" name="txt_cargopersonafecini_${cont+1}" id="txt_cargopersonafecini_${cont+1}" value="${array_cargos[cont]['tb_cargopersona_fecini']}">`+array_cargos[cont]['tb_cargopersona_fecini']);
            if (array_cargos[cont]['tb_cargopersona_fecfin'] == 'actualidad') {
                $(`td.cargo_fecfin_${cont+1}`).html(`<input type="text" name="txt_cargopersonafecfin_${cont+1}" id="txt_cargopersonafecfin_${cont+1}" class="form-control input-shadow input-sm" placeholder="puede ser vacío" style="height: 25px;" value="actualidad">`);
            } else {
                $(`td.cargo_fecfin_${cont+1}`).html(`<input type="hidden" name="txt_cargopersonafecfin_${cont+1}" id="txt_cargopersonafecfin_${cont+1}" value="${array_cargos[cont]['tb_cargopersona_fecfin']}">`+array_cargos[cont]['tb_cargopersona_fecfin']);
            }
            cont++;
        }

        $('input[id*="txt_cargopersonafecfin_"]').on('focus', function() {
            if ($(this).val() == 'actualidad'){
                $(this).val('').blur();
                var num = parseInt($(this).attr('id').split('_')[2]);
                var start = $(`#txt_cargopersonafecini_${num}`).val();
                $(this).datepicker({
                    language: 'es',
                    autoclose: true,
                    format: "dd-mm-yyyy",
                    startDate: start,
                    endDate: new Date()
                });
                $(this).focus();
            }
        });
	}

    //COMPLETAR FILAS DIRECCIONES
	var array_direcciones = JSON.parse($('#hdd_direcciones_array').val());
	var cont = 0;
	if (array_direcciones != null) {
        while(cont < array_direcciones.length){
            nueva_direccion(0, 0);
            
            $(`#hdd_direccionid_${cont+1}`).val(array_direcciones[cont]['tb_direccion_id']);
            $(`td.direccion_desc_${cont+1}`).html(`<input type="hidden" name="txt_direcciondesc_${cont+1}" id="txt_direcciondesc_${cont+1}" value="${array_direcciones[cont]['tb_direccion_des']}">` + array_direcciones[cont]['tb_direccion_des']);
            $(`td.direccion_dir_${cont+1}`).html(`<input type="hidden" name="txt_direcciondir_${cont+1}" id="txt_direcciondir_${cont+1}" value="${array_direcciones[cont]['tb_direccion_dir']}">` + array_direcciones[cont]['tb_direccion_dir']);
            $(`td.direccion_dep_${cont+1}`).html(`<input type="hidden" name="txt_direcciondep_${cont+1}" id="txt_direcciondep_${cont+1}" value="${array_direcciones[cont]['cod_departamento']}">` + array_direcciones[cont]['departamento']);
            $(`td.direccion_prov_${cont+1}`).html(`<input type="hidden" name="txt_direccionprov_${cont+1}" id="txt_direccionprov_${cont+1}" value="${array_direcciones[cont]['cod_provincia']}">` + array_direcciones[cont]['provincia']);
            $(`td.direccion_dist_${cont+1}`).html(`<input type="hidden" name="txt_direcciondist_${cont+1}" id="txt_direcciondist_${cont+1}" value="${array_direcciones[cont]['cod_distrito']}">` + array_direcciones[cont]['distrito']);
            $(`td.direccion_fecini_${cont+1}`).html(`<input type="hidden" name="txt_direccionfecini_${cont+1}" id="txt_direccionfecini_${cont+1}" value="${array_direcciones[cont]['tb_direccion_fecini']}">` + array_direcciones[cont]['tb_direccion_fecini']);
            if (array_direcciones[cont]['tb_direccion_fecfin'] == 'actualidad') {
                $(`td.direccion_fecfin_${cont+1}`).html(`<input type="text" name="txt_direccionfecfin_${cont+1}" id="txt_direccionfecfin_${cont+1}" class="form-control input-shadow input-sm" placeholder="puede ser vacío" style="height: 25px;" value="actualidad">`);
            } else {
                $(`td.direccion_fecfin_${cont+1}`).html(`<input type="hidden" name="txt_direccionfecfin_${cont+1}" id="txt_direccionfecfin_${cont+1}" value="${array_direcciones[cont]['tb_direccion_fecfin']}">` + array_direcciones[cont]['tb_direccion_fecfin']);
            }
            cont++;
        }
        
        $('input[id*="txt_direccionfecfin_"]').on('focus', function() {
            if ($(this).val() == 'actualidad'){
                $(this).val('').blur();
                var num = parseInt($(this).attr('id').split('_')[2]);
                var start = $(`#txt_cargopersonafecini_${num}`).val();
                $(this).datepicker({
                    language: 'es',
                    autoclose: true,
                    format: "dd-mm-yyyy",
                    startDate: start,
                    endDate: new Date()
                });
                $(this).focus();
            }
        });
	}

    //mensaje antes de eliminar
    if ($.inArray($('#action_persona').val(), ['leer', 'eliminar']) > -1) {
        $('#form_persona').find("button[class*='btn-sm'], select, input, textarea, a").each(function(){
            disabled($(this));
        });
        if ($('#action_persona').val() == 'eliminar') {
            $('#persona_mensaje').removeClass('callout-info').addClass('callout-warning');
            var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro?</h4>';
            $('#persona_mensaje').html(mens);
            $('#persona_mensaje').show();
        }
    }
});

//funciones para ubigeo
function cargar_provincia(ubigeo_coddep, num_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep
        }),
        beforeSend: function () {
            $(`#cbo_direccionprov_${num_id}`).html('<option>Cargando...</option>');
        },
        success: function (data) {
            $(`#cbo_direccionprov_${num_id}`).html(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function cargar_distrito(ubigeo_coddep, ubigeo_codpro, num_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep,
            ubigeo_codpro: ubigeo_codpro
        }),
        beforeSend: function () {
            $(`#cbo_direcciondist_${num_id}`).html('<option>Cargando...</option>');
        },
        success: function (data) {
            $(`#cbo_direcciondist_${num_id}`).html(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}
//

function serializar_form_persona() {
	var extra = ``;

    var elementos_disabled = $('#form_persona').find('input:disabled, select:disabled, textarea:disabled').removeAttr('disabled');
    var form_serializado = $('#form_persona').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

function nuevo_cargo(val) {
    var cantidad_clicks = parseInt($('#cantidad_cargos').val());
    cantidad_clicks++;
	$('#cantidad_cargos').val(cantidad_clicks);

    var select = '', fecini = '', fecfin = '', boton = '';
    if (val == 1) {
        select = `<input type="hidden" name="hdd_cargopersonacargonom_${cantidad_clicks}" id="hdd_cargopersonacargonom_${cantidad_clicks}">` +
                `<select name="cbo_cargopersonacargo_${cantidad_clicks}" id="cbo_cargopersonacargo_${cantidad_clicks}" class="form-control input-sm input-shadow new" style="height: 25px; padding: 0px 3px 0px 3px;">
                    ${opciones_cargos}
                </select>`;
        fecini = `<input type="text" name="txt_cargopersonafecini_${cantidad_clicks}" id="txt_cargopersonafecini_${cantidad_clicks}" class="form-control input-shadow input-sm" placeholder="obligatorio*" style="height: 25px;">`;
        fecfin = `<input type="text" name="txt_cargopersonafecfin_${cantidad_clicks}" id="txt_cargopersonafecfin_${cantidad_clicks}" class="form-control input-shadow input-sm" placeholder="puede ser vacío" style="height: 25px;">`;
        boton = `<button class="btn btn-sm btn-danger remove fa fa-trash" type="button" title="ELIMINAR"></button>`;
        disabled($('#add_row_cargo'));
    }

    $('#tbody_cargos').
        append(
            `<tr id="tabla_cabecera_fila_${cantidad_clicks}">
                <input type="hidden" name="hdd_cargopersonaid_${cantidad_clicks}" id="hdd_cargopersonaid_${cantidad_clicks}">
                <td id="tabla_fila" class="cargo_select_${cantidad_clicks}">${select}</td>
                <td id="tabla_fila" class="cargo_fecini_${cantidad_clicks}">${fecini}</td>
                <td id="tabla_fila" class="cargo_fecfin_${cantidad_clicks}">${fecfin}</td>
                <td id="tabla_fila" class="cargo_boton_${cantidad_clicks}" align="center">${boton}</td>
            </tr>`
        );
    //

    if (val == 1) {
        //configuracion de fechas
        $(`#txt_cargopersonafecini_${cantidad_clicks}, #txt_cargopersonafecfin_${cantidad_clicks}`).datepicker({
            language: 'es',
            autoclose: true,
            format: "dd-mm-yyyy",
            endDate: new Date()
        });

        $(`#txt_cargopersonafecini_${cantidad_clicks}`).on("change", function (e) {
            var startVal = $(`#txt_cargopersonafecini_${cantidad_clicks}`).val();
            $(`#txt_cargopersonafecfin_${cantidad_clicks}`).data('datepicker').setStartDate(startVal);
        });
        $(`#txt_cargopersonafecfin_${cantidad_clicks}`).on("change", function (e) {
            var endVal = $(`#txt_cargopersonafecfin_${cantidad_clicks}`).val();
            $(`#txt_cargopersonafecini_${cantidad_clicks}`).data('datepicker').setEndDate(endVal);
        });

        //direccion obligatoria en caso elegir representante CGV
        $('select[id*="cbo_cargopersonacargo_"]').change(function(){
            $('input[id*="hdd_cargopersonacargonom_"]').val($('select[id*="cbo_cargopersonacargo_"] option:selected').text());
            if (parseInt($(this).val()) == 9) {
                if ($('button.direcc_repcgv').length < 1) {
                    nueva_direccion(1, 1);
                }

                if (no_ocultar == 0) {
                    $('.num_cel').hide();
                }
            } else if (parseInt($(this).val()) == 10) {
                $('.num_cel').show();
            } else {
                if (no_ocultar == 0) {
                    $('.num_cel').hide();
                }
                if (parseInt($(this).val()) == 0) {
                    $('input[id*="hdd_cargopersonacargonom_"]').val('');
                }
            }
        });
    }
}

function nueva_direccion(val, repcgv) {
    var cantidad_clicks = parseInt($('#cantidad_direcciones').val());
    cantidad_clicks++;
	$('#cantidad_direcciones').val(cantidad_clicks);

    var desc = '', dir= '', dep = '', prov = '', dist = '', fecini = '', fecfin = '', boton = '', class_n = '';
    if (val == 1) {
        if (repcgv) {
            class_n = 'direcc_repcgv';
        }
        desc = `<input type="text" name="txt_direcciondesc_${cantidad_clicks}" id="txt_direcciondesc_${cantidad_clicks}" class="form-control input-shadow input-sm mayus" placeholder="Casa, Trabajo" style="height: 25px;">`;
        dir = `<input type="text" name="txt_direcciondir_${cantidad_clicks}" id="txt_direcciondir_${cantidad_clicks}" class="form-control input-shadow input-sm mayus" placeholder="Av. Calle. Bloque. Piso. Mz. Nro. Referencia" style="height: 25px;">`;
        dep = `<select name="cbo_direcciondep_${cantidad_clicks}" id="cbo_direcciondep_${cantidad_clicks}" class="form-control input-sm input-shadow new" style="height: 25px; padding: 0px 3px 0px 3px;">${opciones_departamentos}</select>`;
        prov = `<select name="cbo_direccionprov_${cantidad_clicks}" id="cbo_direccionprov_${cantidad_clicks}" class="form-control input-sm input-shadow new" style="height: 25px; padding: 0px 3px 0px 3px;"></select>`;
        dist = `<select name="cbo_direcciondist_${cantidad_clicks}" id="cbo_direcciondist_${cantidad_clicks}" class="form-control input-sm input-shadow new" style="height: 25px; padding: 0px 3px 0px 3px;"></select>`;
        fecini = `<input type="text" name="txt_direccionfecini_${cantidad_clicks}" id="txt_direccionfecini_${cantidad_clicks}" class="form-control input-shadow input-sm" placeholder="01-01-23" style="height: 25px;">`;
        fecfin = `<input type="text" name="txt_direccionfecfin_${cantidad_clicks}" id="txt_direccionfecfin_${cantidad_clicks}" class="form-control input-shadow input-sm" placeholder="01-01-23" style="height: 25px;">`;
        boton = `<button name="btn_eliminardireccion_${cantidad_clicks}" id="btn_eliminardireccion_${cantidad_clicks}" class="btn btn-sm btn-danger remove fa fa-trash ${class_n}" type="button" title="ELIMINAR"></button>`;
        disabled($('#add_row_dir'));
    }

    $('#tbody_dir').
        append(
            `<tr id="tabla_cabecera_fila_${cantidad_clicks}">
                <input type="hidden" name="hdd_direccionid_${cantidad_clicks}" id="hdd_direccionid_${cantidad_clicks}">
                <td id="tabla_fila" class="direccion_desc_${cantidad_clicks}">${desc}</td>
                <td id="tabla_fila" class="direccion_dir_${cantidad_clicks}">${dir}</td>
                <td id="tabla_fila" class="direccion_dep_${cantidad_clicks}">${dep}</td>
                <td id="tabla_fila" class="direccion_prov_${cantidad_clicks}">${prov}</td>
                <td id="tabla_fila" class="direccion_dist_${cantidad_clicks}">${dist}</td>
                <td id="tabla_fila" class="direccion_fecini_${cantidad_clicks}">${fecini}</td>
                <td id="tabla_fila" class="direccion_fecfin_${cantidad_clicks}">${fecfin}</td>
                <td id="tabla_fila" align="center">${boton}</td>
            </tr>`
        );
    //

    //transiciones de ubigeo
    $('select[id*="cbo_direcciondep"]').change(function (event) {
        var num = parseInt($(this).attr('id').split('_')[2]);
        var codigo_dep = $(this).val();
        if (parseInt(codigo_dep) == 0) {
            $(`#cbo_direccionprov_${num}`).html('');
            $(`#cbo_direcciondist_${num}`).html('');
        } else {
            cargar_provincia(codigo_dep, num);
            $(`#cbo_direcciondist_${num}`).html('');
        }
    });
    $('select[id*="cbo_direccionprov"]').change(function (event) {
        var num = parseInt($(this).attr('id').split('_')[2]);
        var codigo_dep = $(`#cbo_direcciondep_${num}`).val();
        var codigo_pro = $(this).val();

        if (parseInt(codigo_pro) == 0)
            $(`#cbo_direcciondist_${num}`).html('');
        else
            cargar_distrito(codigo_dep, codigo_pro, num);
    });
    //

    if (val == 1) {
        //configuracion de fechas txt_direccionfecfin_1
        $(`#txt_direccionfecini_${cantidad_clicks}, #txt_direccionfecfin_${cantidad_clicks}`).datepicker({
            language: 'es',
            autoclose: true,
            format: "dd-mm-yyyy",
            endDate: new Date()
        });

        $(`#txt_direccionfecini_${cantidad_clicks}`).on("change", function (e) {
            var startVal = $(`#txt_direccionfecini_${cantidad_clicks}`).val();
            $(`#txt_direccionfecfin_${cantidad_clicks}`).data('datepicker').setStartDate(startVal);
        });
        $(`#txt_direccionfecfin_${cantidad_clicks}`).on("change", function (e) {
            var endVal = $(`#txt_direccionfecfin_${cantidad_clicks}`).val();
            $(`#txt_direccionfecini_${cantidad_clicks}`).data('datepicker').setEndDate(endVal);
        });
    }
}

function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'cerrar',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            cerrar: function () {
            }
        }
    });
}
