var datatable_persona_global;
$(document).ready(function () {
    persona_tabla();
});

function persona_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "persona/persona_tabla.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#persona_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_persona_tabla').html(data);
            $('#persona_mensaje_tbl').hide(300);
            estilos_datatable_persona();
        },
        complete: function (data) {
        },
        error: function (data) {
            $('#persona_mensaje_tbl').html('ERROR AL CARGAR DATOS: ' + data.responseText);
        }
    });
}

function estilos_datatable_persona() {
    datatable_persona_global = $('#tbl_personas').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [2, 3, 4], orderable: false}
        ]
    });
    datatable_persona_texto_filtrar();
}

function datatable_persona_texto_filtrar() {
    $('input[aria-controls*="tbl_personas"]')
    .attr('id', 'txt_datatable_persona_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_persona_fil').keyup(function (event) {
        $('#hdd_datatable_persona_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_persona_fil').val();
    if (text_fil) {
        $('#txt_datatable_persona_fil').val(text_fil);
        datatable_persona_global.search(text_fil).draw();
    }
}

function persona_form(action, persona_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "persona/persona_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            persona_id: persona_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_persona_form').html(html);
            $('#modal_persona_form').modal('show');
            $('#modal_mensaje').modal('hide');

            modal_width_auto('modal_persona_form', 65);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_persona_form'); //funcion encontrada en public/js/generales.js
        }
    });
}

function persona_historial_form(persona_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "persona/persona_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
            persona_id: persona_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_persona_historial_form').html(html);
            $('#div_modal_persona_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_persona_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_persona_historial_form'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
        }
    });
}
