<?php
require_once '../persona/Persona.class.php';
$oPersona = new Persona();

$persona_id = (empty($persona_id))? 0 : $persona_id;

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oPersona->listar_todos();
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($persona_id == $value['tb_persona_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_persona_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_persona_ape'] .' '. $value['tb_persona_nom'] . '</option>';
	}
}
unset($result);
//FIN PRIMER NIVEL
echo $option;
