<?php
//revision de sesion activa
if (defined('VISTA_URL'))
    require_once(APP_URL . 'core/usuario_sesion.php');
else
    require_once('../../core/usuario_sesion.php');
//
//requires
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../persona/Persona.class.php');
    $oPersona = new Persona();
    require_once('../persona/Cargopersona.class.php');
    $oCargopersona = new Cargopersona();
    require_once('../direccion/Direccion.class.php');
    $oDireccion = new Direccion();
    require_once('../ubigeo/Ubigeo.class.php');
    $oUbigeo = new Ubigeo();
//

//datos post
$usuario_action = $_POST['action'];
$persona_id     = intval($_POST['persona_id']);

//datos estaticos
$listo          = 1;
$style_numcel   = 'display: none;';
$action         = devuelve_nombre_usuario_action($usuario_action);

$titulo = '';
//titulo del form
if ($usuario_action == 'I') {
    $titulo = 'Registrar persona';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar persona';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar persona';
} elseif ($usuario_action == 'L') {
    $titulo = 'Persona registrada';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
if ($usuario_action != 'I') {
    $titulo .= ": ID $persona_id";
}
//

if ($listo == 1) {
    if (!empty($persona_id)) {
        $res = $oPersona->mostrar_uno_id($persona_id);

        if ($res['estado'] == 1) {
            $persona = $res['data'];

            $nombres        = $persona['tb_persona_nom'];
            $apellidos      = $persona['tb_persona_ape'];
            $documento      = $persona['tb_persona_doc'];
            $numero_cel     = $persona['tb_persona_numcel'];
            $style_numcel   = empty($numero_cel) ? 'display: none;' : '';

            $res_cargos = $oCargopersona->listar_todos($persona_id, '', '', '');
            if ($res_cargos['estado'] == 1) {
                $res_cargos = $res_cargos['data'];
                for ($i=0; $i < count($res_cargos); $i++) {
                    $res_cargos[$i]['tb_cargopersona_fecini'] = mostrar_fecha($res_cargos[$i]['tb_cargopersona_fecini']);
                    $res_cargos[$i]['tb_cargopersona_fecfin'] = empty($res_cargos[$i]['tb_cargopersona_fecfin']) ? 'actualidad' : mostrar_fecha($res_cargos[$i]['tb_cargopersona_fecfin']);
                }
            }

            $res_direccion = $oDireccion->listar_todos('tb_persona', $persona_id, '');
            if ($res_direccion['estado'] == 1) {
                $res_direccion = $res_direccion['data'];
                for ($i=0; $i < count($res_direccion); $i++) {
                    $res_direccion[$i]['tb_direccion_fecini'] = mostrar_fecha($res_direccion[$i]['tb_direccion_fecini']);
                    $res_direccion[$i]['tb_direccion_fecfin'] = empty($res_direccion[$i]['tb_direccion_fecfin']) ? 'actualidad' : mostrar_fecha($res_direccion[$i]['tb_direccion_fecfin']);

                    $res_ubigeo = $oUbigeo->mostrarUbigeo($res_direccion[$i]['tb_direccion_ubigeo']);
                    if ($res_ubigeo['estado'] == 1) {
                        $res_direccion[$i]['departamento'] = $res_ubigeo['data']['Departamento'];
                        $res_direccion[$i]['provincia'] = $res_ubigeo['data']['Provincia'];
                        $res_direccion[$i]['distrito'] = $res_ubigeo['data']['Distrito'];
                        $res_direccion[$i]['cod_departamento'] = $res_ubigeo['data']['tb_ubigeo_coddep'];
                        $res_direccion[$i]['cod_provincia'] = $res_ubigeo['data']['tb_ubigeo_codpro'];
                        $res_direccion[$i]['cod_distrito'] = $res_ubigeo['data']['tb_ubigeo_coddis'];
                    }
                }
            } else {
                $res_direccion = null;
            }
        }
        unset($res);
    }
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_persona_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_persona" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action_persona" id="action_persona" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_persona_id" id="hdd_persona_id" value="<?php echo $persona_id; ?>">
                        <input type="hidden" name="hdd_persona_reg" id="hdd_persona_reg" value='<?php echo json_encode($persona); ?>'>

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="txt_persona_nom">Nombres:</label>
                                    <input type="text" name="txt_persona_nom" id="txt_persona_nom" placeholder="Nombres" class="form-control mayus input-sm input-shadow" value="<?php echo $nombres; ?>">
                                </div>

                                <div class="col-md-4">
                                    <label for="txt_persona_ape">Apellidos:</label>
                                    <input type="text" name="txt_persona_ape" id="txt_persona_ape" placeholder="Apellidos" class="form-control mayus input-sm input-shadow" value="<?php echo $apellidos; ?>">
                                </div>

                                <div class="col-md-4">
                                    <label for="txt_persona_doc">N° Documento:</label>
                                    <input type="text" name="txt_persona_doc" id="txt_persona_doc" placeholder="DNI o NRO DOC" class="form-control mayus input-sm input-shadow" value="<?php echo $documento; ?>">
                                </div>
                                <!-- oculto -->
                                <select name="cbo_cargos" id="cbo_cargos">
                                    <?php require_once('../cargo/cargo_select.php'); ?>
                                </select>
                                <select name="cbo_departamentos" id="cbo_departamentos">
                                    <?php require_once('../ubigeo/ubigeo_departamento_select.php'); ?>
                                </select>
                            </div>

                            <div class="row form-group num_cel" style="<?php echo $style_numcel; ?>">
                                <div class="col-md-4">
                                    <label for="txt_persona_numcel">Número celular (9 dig):</label>
                                    <input type="text" name="txt_persona_numcel" id="txt_persona_numcel" class="form-control input-sm input-shadow mayus num_cel" placeholder="num cel" value="<?php echo $numero_cel;?>">
                                </div>
                            </div>

                            <div class="row" id="row_cargos" name="row_cargos">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span style="font-weight: bold;">Cargos ocupados:</span>
                                        </div>
                                        <div class="col-md-6" align="right">
                                            <a href="javascript:void(0)" id="add_row_cargo" name="add_row_cargo" onclick="nuevo_cargo(1)"><b class="mayus"><u>Añadir cargo</u></b></a>
                                        </div>
                                    </div>
                                    <input type="hidden" id="hdd_cargos_array" name="hdd_cargos_array" value='<?php echo json_encode($res_cargos) ?>'>
                                    <input type="hidden" name="cantidad_cargos" id="cantidad_cargos" value="0">
                                    <table id="tbl_detalle_cargos" name="tbl_detalle_cargos" class="table table-hover" style="word-wrap:break-word;">
                                        <thead>
                                            <tr id="tabla_cabecera">
                                                <th id="tabla_cabecera_fila" style="width: 40%;">Cargo</th>
                                                <th id="tabla_cabecera_fila" style="width: 25%;">Desde (fec. inicio)</th>
                                                <th id="tabla_cabecera_fila" style="width: 25%;">Hasta (fec. fin)</th>
                                                <th id="tabla_cabecera_fila" style="width: 10%;"> </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_cargos">
                                            <!-- aquí se rellenan los cargos anteriores y actuales -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row direccion">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span style="font-weight: bold;">Direcciones:</span>
                                        </div>
                                        <div class="col-md-6" align="right">
                                            <a href="javascript:void(0)" id="add_row_dir" onclick="nueva_direccion(1, 0)"><b class="mayus"><u>Añadir direccion</u></b></a>
                                        </div>
                                    </div>
                                    <input type="hidden" id="hdd_direcciones_array" name="hdd_direcciones_array" value='<?php echo json_encode($res_direccion) ?>'>
                                    <input type="hidden" name="cantidad_direcciones" id="cantidad_direcciones" value="0">
                                    <table id="tbl_detalle_dir" name="tbl_detalle_dir" class="table table-hover" style="word-wrap:break-word;">
                                        <thead>
                                            <tr id="tabla_cabecera">
                                                <th id="tabla_cabecera_fila" style="width: 11%;">Descripcion</th>
                                                <th id="tabla_cabecera_fila" style="width: 26%;">Direccion</th>
                                                <th id="tabla_cabecera_fila" style="width: 12%;">Departamento</th>
                                                <th id="tabla_cabecera_fila" style="width: 13%;">Provincia</th>
                                                <th id="tabla_cabecera_fila" style="width: 15%;">Distrito</th>
                                                <th id="tabla_cabecera_fila" style="width: 10%;">Desde</th>
                                                <th id="tabla_cabecera_fila" style="width: 10%;">Hasta</th>
                                                <th id="tabla_cabecera_fila" style="width: 3%;"> </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_dir">
                                            <!-- aquí se rellenan los direcciones anteriores y actuales -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="persona_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_persona">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_persona">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/persona/persona_form.js?ver=0000001'; ?>"></script>
