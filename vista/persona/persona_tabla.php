<?php
if (defined('VISTA_URL')) {
    require_once APP_URL . 'core/usuario_sesion.php';
} else {
    require_once '../../core/usuario_sesion.php';
}
require_once '../funciones/fechas.php';
require_once 'Persona.class.php';
$oPersona = new Persona();

$result = $oPersona->listar_todos();

$tr = '';
const TD_TABLAFILA_CENTRADO = '<td id="tabla_fila" align="center">';
const TD_CIERRE = '</td>';

if ($result['estado'] == 1) {
    require_once '../persona/Cargopersona.class.php';
    $oCargopersona = new Cargopersona();
    foreach ($result['data'] as $key => $value) {
        $cargos_activos = '';
        $res_cargosactivos = $oCargopersona->listar_todos($value['tb_persona_id'], '', 'actualidad', '');
        if ($res_cargosactivos['estado'] == 1) {
            foreach ($res_cargosactivos['data'] as $key => $cargoact) {
                $cargos_activos .= empty($cargos_activos) ? $cargoact['tb_cargo_nom'] : ', '.$cargoact['tb_cargo_nom'];
            }
        } else {
            $cargos_activos = 'ninguno';
        }

        $tr .= '<tr>';
        $tr .=
            TD_TABLAFILA_CENTRADO . $value['tb_persona_id'] . TD_CIERRE.
            '<td id="tabla_fila">' . $value['tb_persona_ape'].' '.$value['tb_persona_nom'] . TD_CIERRE.
            TD_TABLAFILA_CENTRADO . $value['tb_persona_doc'] . TD_CIERRE.
            '<td id="tabla_fila">' . $cargos_activos . TD_CIERRE.
            TD_TABLAFILA_CENTRADO.
                '<a class="btn btn-info btn-xs" title="Ver" onclick="persona_form(\'L\', '.$value['tb_persona_id'].')"><i class="fa fa-eye"></i> Ver</a> '.
                '<a class="btn btn-warning btn-xs" title="Editar" onclick="persona_form(\'M\', '.$value['tb_persona_id'].')"><i class="fa fa-edit"></i> Editar</a> '.
                '<a class="btn btn-danger btn-xs" title="Eliminar" onclick="persona_form(\'E\', '.$value['tb_persona_id'].')"><i class="fa fa-trash"></i> Eliminar</a> '.
                '<a class="btn btn-success btn-xs" title="Historial" onclick="persona_historial_form('.$value['tb_persona_id'].')"><i class="fa fa-history"></i> Historial</a>'.
            TD_CIERRE;
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}

?>
<table id="tbl_personas" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">Nombres</th>
            <th id="tabla_cabecera_fila">Documento</th>
            <th id="tabla_cabecera_fila">Cargo actual</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>
