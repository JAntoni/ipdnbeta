<?php
require_once '../persona/Cargopersona.class.php';
$oCargopersona = new Cargopersona();
require_once '../funciones/fechas.php';

$persona_id = (empty($persona_id))? $_POST['persona_id'] : $persona_id;
if (!empty($_POST["cargo_id"])){
	$cargo_id = $_POST["cargo_id"];
} elseif (empty($_POST["cargo_id"]) && empty($cargo_id)) {
	$cargo_id = '';
}

if(empty($_POST["vigente"]) && empty($vigente)){
	$vigente = '';
}
elseif(!empty($vigente)){
	$vigente = $vigente == 'actualidad' ? $vigente : fecha_mysql($vigente);
}
else{
	$vigente = $_POST["vigente"] == 'actualidad' ? $_POST["vigente"] : fecha_mysql($_POST["vigente"]);
}

if (!empty($_POST["columna_groupby"])) {
	$columna_groupby = $_POST["columna_groupby"];
} elseif (empty($_POST["columna_groupby"]) && empty($columna_groupby)) {
	$columna_groupby = '';
}

if (!empty($_POST["muestra_cargo"])) {
	$muestra_cargo = $_POST["muestra_cargo"];
} elseif (empty($_POST["muestra_cargo"]) && empty($muestra_cargo)) {
	$muestra_cargo = 0;
}

if (!empty($_POST["muestra_telefono"])) {
	$muestra_telefono = $_POST["muestra_telefono"];
} elseif (empty($_POST["muestra_telefono"]) && empty($muestra_telefono)) {
	$muestra_telefono = 0;
}

$option = '<option value="0">Seleccione</option>';

//PRIMER NIVEL
$result = $oCargopersona->listar_todos('', $cargo_id, $vigente, $columna_groupby);
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($persona_id == $value['tb_persona_id'])
			$selected = 'selected';
		//

		$add_cargo = '';
		if ($muestra_cargo == 1)
			$add_cargo = ' - '. $value['tb_cargo_nom'];
		//

		$add_telefono = '';
		if ($muestra_telefono == 1)
			$add_telefono = ' - '. $value['tb_persona_numcel'];
		//
		
		$option .= '<option value="'.$value['tb_persona_id'].'" data_cargoid="'.$value['tb_cargo_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_persona_nom'].' '.$value['tb_persona_ape'].' - '.$value['tb_persona_doc'].$add_cargo.$add_telefono.'</option>';
	}
}
unset($result);
//FIN PRIMER NIVEL
echo $option;

