<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');
//

class Persona extends Conexion {
    public $id;
    public $nom;
    public $ape;
    public $doc;
    public $numcel;
    public $fecreg;
    public $usureg;
    public $fecmod;
    public $usumod;
    public $xac;

    function listar_todos() {
        try {
            $sql = "SELECT p.*
                    FROM tb_persona p
                    WHERE p.tb_persona_xac = 1
                    ORDER BY p.tb_persona_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrar_uno_id($persona_id) {
        try {
            $sql = "SELECT p.*
                    FROM tb_persona p
                    WHERE p.tb_persona_id = :param0;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $persona_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrar_uno_doc($documento, $persona_id) {
        try {
            $sql_add = !empty($persona_id) ? "\nAND p.tb_persona_id <> $persona_id" : '';
            $sql = "SELECT p.*
                    FROM tb_persona p
                    WHERE p.tb_persona_xac = 1$sql_add
                    AND p.tb_persona_doc = :param0;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $documento, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $column_opc = !empty($this->numcel) ? ",\ntb_persona_numcel" : '';
            $param_opc = !empty($this->numcel) ? ",\n:param_opc1" : '';

            $sql = "INSERT INTO tb_persona (
                        tb_persona_nom,
                        tb_persona_ape,
                        tb_persona_doc,
                        tb_persona_usureg,
                        tb_persona_usumod$column_opc)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4$param_opc)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->nom, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->ape, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->doc, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->usumod, PDO::PARAM_INT);
            if (!empty($this->numcel)) {
                $sentencia->bindParam(':param_opc1', $this->numcel, PDO::PARAM_STR);
            }

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_campo($persona_id, $persona_columna, $persona_valor, $param_tip, $usuario_id){
        $this->dblink->beginTransaction();
        try {
            if (!empty($persona_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql_add = !empty($usuario_id) ? ", tb_persona_usumod = $usuario_id" : '';
                $sql = "UPDATE tb_persona SET " . $persona_columna . " = :persona_valor$sql_add WHERE tb_persona_id = :persona_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":persona_id", $persona_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":persona_valor", $persona_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":persona_valor", $persona_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_persona
              SET
                tb_persona_nom = :param0,
                tb_persona_ape = :param1,
                tb_persona_doc = :param2,
                tb_persona_numcel = :param3,
                tb_persona_usumod = :param4
              WHERE
                tb_persona_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->nom, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->ape, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->doc, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->numcel, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':paramx', $this->id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
}
