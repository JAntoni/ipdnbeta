var datatable_global;
function mesconta_form(usuario_act, mesconta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "mesconta/mesconta_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            mesconta_id: mesconta_id,
            vista: 'mesconta'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_mesconta_form').html(data);
                $('#modal_registro_mesconta').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_mesconta'); //funcion encontrada en public/js/generales.js

                modal_hidden_bs_modal('modal_registro_mesconta', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_height_auto('modal_registro_mesconta'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_mesconta', 30);
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'mesconta';
                var div = 'div_modal_mesconta_form';
                permiso_solicitud(usuario_act, mesconta_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function mesconta_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "mesconta/mesconta_tabla.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#mesconta_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_mesconta_tabla').html(data);
            $('#mesconta_mensaje_tbl').hide(300);
            estilos_datatable();
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#mesconta_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}
function estilos_datatable() {
    datatable_global = $('#tbl_mescontas').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [3, 4, 5, 6], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}
$(document).ready(function () {
    mesconta_tabla();
});

function mesconta_historial_form(mesconta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "mesconta/mesconta_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
                mesconta_id:mesconta_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_mesconta_historial_form').html(html);
            $('#div_modal_mesconta_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_mesconta_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_mesconta_historial_form'); //funcion encontrada en public/js/generales.js

        },
        complete: function (html) {
//                    console.log(html);
        }
    });
}