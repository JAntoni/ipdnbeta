
$(document).ready(function () {
    $('#mesconta_picker').datepicker({
        language: 'es',
        autoclose: true,
        format: "mm-yyyy",
        maxViewMode: 'años', // Rango de selección máximo
        minViewMode: 'months', // Rango de selección mínimo
        endDate: '+5d'
    })
    .on('changeDate', function(e) {
		//console.log('holaa 👌2');
		actualizar_correlativo(this.value);
	});

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    if( $('#action').val() == 'eliminar' ){
        $('#mesconta_picker').attr('disabled','disabled');
        $(".flat-green").attr("disabled", true);
    }
    if( $('#action').val() == 'insertar' ){
        actualizar_correlativo($('#mesconta_picker').val());
    }

    $('#form_mesconta').validate({
        submitHandler: function () {
            var elementos_disabled = $('#form_mesconta').find('input:disabled, select:disabled').removeAttr('disabled');
            var form_serializado = $('#form_mesconta').serialize();
            elementos_disabled.attr('disabled', 'disabled');
            //console.log(partes[1]+'-'+numero_mes);
            //console.log($("#form_mesconta").serialize());return;
            $.ajax({
                type: "POST",
                url: VISTA_URL + "mesconta/mesconta_controller.php",
                async: true,
                dataType: "json",
                data: form_serializado,
                beforeSend: function () {
                    $('#mesconta_mensaje').show(400);
                    $('#btn_guardar_mesconta').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        console.log(data.estado + ' // '+ data.mensaje);//return;
                        $('#mesconta_mensaje').removeClass('callout-info').addClass('callout-success');
                        $('#mesconta_mensaje').html(data.mensaje);
                        var vista = $('#mesconta_vista').val();
                        
                        setTimeout(function () {
                            if (vista == 'credito')
                            mesconta_tipo_cambio(); //función implementada en cada módulo de los créditos
                        
                            if (vista == 'mesconta' && data.estado != 2)
                                mesconta_tabla();
                            $('#modal_registro_mesconta').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#mesconta_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#mesconta_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_mesconta').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#mesconta_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#mesconta_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            mesconta_picker: {
                required: true
            }
        },
        messages: {
            txt_mesconta_fec: {
                required: "Ingrese el Mes Contable"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});

function actualizar_correlativo(str){
    mes = str.split('-')[0];
    anio = str.split('-')[1];
    $('#txt_mesconta_compracorr').val(`DC${anio}${mes}0001`);
    $('#txt_mesconta_ventacorr').val(`DV${anio}${mes}0001`);
}