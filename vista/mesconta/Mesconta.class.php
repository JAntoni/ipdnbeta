<?php

if (defined('APP_URL')) {
    require_once (APP_URL . 'datos/conexion.php');
} else {
    require_once ('../../datos/conexion.php');
}

class Mesconta extends Conexion{
    private $tb_mesconta_id;
    private $tb_mesconta_fecreg;
    private $tb_mesconta_usureg;
    private $tb_mesconta_fecmod;
    private $tb_mesconta_usumod;
    private $tb_mesconta_anio;
    private $tb_mesconta_mes;
    private $tb_mesconta_aperturado;
    private $tb_mesconta_estadeclarado;
    private $tb_mesconta_xac;
    private $tb_mesconta_compracorrel;
    private $tb_mesconta_ventacorrel;

    /**
     * Get the value of tb_mesconta_id
     */
    public function getTbMescontaId()
    {
        return $this->tb_mesconta_id;
    }

    /**
     * Set the value of tb_mesconta_id
     */
    public function setTbMescontaId($tb_mesconta_id)
    {
        $this->tb_mesconta_id = $tb_mesconta_id;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_fecreg
     */
    public function getTbMescontaFecreg()
    {
        return $this->tb_mesconta_fecreg;
    }

    /**
     * Set the value of tb_mesconta_fecreg
     */
    public function setTbMescontaFecreg($tb_mesconta_fecreg)
    {
        $this->tb_mesconta_fecreg = $tb_mesconta_fecreg;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_usureg
     */
    public function getTbMescontaUsureg()
    {
        return $this->tb_mesconta_usureg;
    }

    /**
     * Set the value of tb_mesconta_usureg
     */
    public function setTbMescontaUsureg($tb_mesconta_usureg)
    {
        $this->tb_mesconta_usureg = $tb_mesconta_usureg;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_fecmod
     */
    public function getTbMescontaFecmod()
    {
        return $this->tb_mesconta_fecmod;
    }

    /**
     * Set the value of tb_mesconta_fecmod
     */
    public function setTbMescontaFecmod($tb_mesconta_fecmod)
    {
        $this->tb_mesconta_fecmod = $tb_mesconta_fecmod;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_usumod
     */
    public function getTbMescontaUsumod()
    {
        return $this->tb_mesconta_usumod;
    }

    /**
     * Set the value of tb_mesconta_usumod
     */
    public function setTbMescontaUsumod($tb_mesconta_usumod)
    {
        $this->tb_mesconta_usumod = $tb_mesconta_usumod;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_anio
     */
    public function getTbMescontaAnio()
    {
        return $this->tb_mesconta_anio;
    }

    /**
     * Set the value of tb_mesconta_anio
     */
    public function setTbMescontaAnio($tb_mesconta_anio)
    {
        $this->tb_mesconta_anio = $tb_mesconta_anio;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_mes
     */
    public function getTbMescontaMes()
    {
        return $this->tb_mesconta_mes;
    }

    /**
     * Set the value of tb_mesconta_mes
     */
    public function setTbMescontaMes($tb_mesconta_mes)
    {
        $this->tb_mesconta_mes = $tb_mesconta_mes;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_aperturado
     */
    public function getTbMescontaAperturado()
    {
        return $this->tb_mesconta_aperturado;
    }

    /**
     * Set the value of tb_mesconta_aperturado
     */
    public function setTbMescontaAperturado($tb_mesconta_aperturado)
    {
        $this->tb_mesconta_aperturado = $tb_mesconta_aperturado;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_estadeclarado
     */
    public function getTbMescontaEstadeclarado()
    {
        return $this->tb_mesconta_estadeclarado;
    }

    /**
     * Set the value of tb_mesconta_estadeclarado
     */
    public function setTbMescontaEstadeclarado($tb_mesconta_estadeclarado)
    {
        $this->tb_mesconta_estadeclarado = $tb_mesconta_estadeclarado;

        return $this;
    }

    /**
     * Get the value of tb_mesconta_xac
     */
    public function getTbMescontaXac()
    {
        return $this->tb_mesconta_xac;
    }

    /**
     * Set the value of tb_mesconta_xac
     */
    public function setTbMescontaXac($tb_mesconta_xac)
    {
        $this->tb_mesconta_xac = $tb_mesconta_xac;

        return $this;
    }

    public function getTbMescontaCompracorrel(){ return $this->tb_mesconta_compracorrel; }
    public function setTbMescontaCompracorrel($tb_mesconta_compracorrel){ $this->tb_mesconta_compracorrel = $tb_mesconta_compracorrel; return $this; }

    public function getTbMescontaVentacorrel(){ return $this->tb_mesconta_ventacorrel; }
    public function setTbMescontaVentacorrel($tb_mesconta_ventacorrel){ $this->tb_mesconta_ventacorrel = $tb_mesconta_ventacorrel; return $this; }

    public function listar_todos($xac){ //si no se envia parametro xac devuelve todos, de lo contrario añade clause WHERE
        try {
            $val = [0, 1]; // valores de xac permitidos
            $sql = "SELECT
                        mc.*, u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot,
                        CASE mc.tb_mesconta_estadeclarado
                        WHEN 1 THEN
                            'SI'
                        ELSE
                            'NO'
                        END AS declarado,
                        CASE mc.tb_mesconta_aperturado
                        WHEN 1 THEN
                            'SI'
                        ELSE
                            'NO'
                        END AS aperturado
                    FROM
                        tb_mesconta mc
                    INNER JOIN tb_usuario u ON mc.tb_mesconta_usureg = u.tb_usuario_id";
            if(in_array($xac, $val)){
                $sql .= " WHERE tb_mesconta_xac = :tb_mesconta_xac ";
            }
                     
            $sql .= " ORDER BY mc.tb_mesconta_anio DESC, mc.tb_mesconta_mes DESC;";
            
            $sentencia = $this->dblink->prepare($sql);
            if(in_array($xac, $val)){
                $sentencia->bindParam(':tb_mesconta_xac', $xac, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipo de cambio contable registrado";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    function mostrarUno($array) {
        //arreglo bidimensional con formato $column_name, $param.$i, $datatype
        try {
            $sql = "SELECT
                        mc.*, u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot
                    FROM
                        tb_mesconta mc
                    INNER JOIN tb_usuario u ON mc.tb_mesconta_usureg = u.tb_usuario_id
                    WHERE ";
            for($i = 0; $i < count($array); $i++){
                if($i > 0){
                    $sql .= " AND ";
                }
                if(stripos($array[$i]['column_name'], 'fec') !== FALSE){ //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
                    $sql .= " DATE_FORMAT(".$array[$i]['column_name'].", '%Y-%m-%d')";
                }
                else{ // de lo contrario solo se coloca el nombre de la columna a filtrar
                    $sql .= $array[$i]['column_name'];
                }
                $sql .= " = :param".$i;
            }
            $sql .= " AND tb_mesconta_xac = 1;";
            
            $sentencia = $this->dblink->prepare($sql);
            for($i = 0; $i < count($array); $i++){
                $_PARAM = strtoupper($array[$i]['datatype'])=='INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
                $sentencia->bindParam(":param".$i, $array[$i]['param'.$i], $_PARAM);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No existe este registro";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function eliminar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_mesconta SET tb_mesconta_xac = 0, tb_mesconta_usumod = :tb_mesconta_usumod WHERE tb_mesconta_id = :tb_mesconta_id ";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_mesconta_usumod", $this->tb_mesconta_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_mesconta_id", $this->tb_mesconta_id, PDO::PARAM_INT);
            $resultado = $sentencia->execute(); //retorna 1 si es correcto

            if($resultado == 1){
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_mesconta
                    SET
                        tb_mesconta_usumod = :param0,
                        tb_mesconta_anio = :param1,
                        tb_mesconta_mes = :param2,
                        tb_mesconta_aperturado = :param3,
                        tb_mesconta_estadeclarado = :param4,
                        tb_mesconta_compracorrel = :param5,
                        tb_mesconta_ventacorrel = :param6
                    WHERE tb_mesconta_id = :param7 ;";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->tb_mesconta_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->tb_mesconta_anio, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->tb_mesconta_mes, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->tb_mesconta_aperturado, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->tb_mesconta_estadeclarado, PDO::PARAM_INT);
            $sentencia->bindParam(':param5', $this->tb_mesconta_compracorrel, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->tb_mesconta_ventacorrel, PDO::PARAM_STR);
            $sentencia->bindParam(':param7', $this->tb_mesconta_id, PDO::PARAM_INT);
            $resultado = $sentencia->execute();

            if($resultado == 1){
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_mesconta(
                        tb_mesconta_usureg,
                        tb_mesconta_usumod,
                        tb_mesconta_anio,
                        tb_mesconta_mes,
                        tb_mesconta_aperturado,
                        tb_mesconta_estadeclarado,
                        tb_mesconta_compracorrel,
                        tb_mesconta_ventacorrel
                    )
                    VALUES(
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        :param6,
                        :param7
                    )";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0',$this->tb_mesconta_usureg,PDO::PARAM_INT);
            $sentencia->bindParam(':param1',$this->tb_mesconta_usumod,PDO::PARAM_INT);
            $sentencia->bindParam(':param2',$this->tb_mesconta_anio,PDO::PARAM_STR);
            $sentencia->bindParam(':param3',$this->tb_mesconta_mes,PDO::PARAM_STR);
            $sentencia->bindParam(':param4',$this->tb_mesconta_aperturado,PDO::PARAM_INT);
            $sentencia->bindParam(':param5',$this->tb_mesconta_estadeclarado,PDO::PARAM_INT);
            $sentencia->bindParam(':param6',$this->tb_mesconta_compracorrel,PDO::PARAM_STR);
            $sentencia->bindParam(':param7',$this->tb_mesconta_ventacorrel,PDO::PARAM_STR);

            $resultado['estado'] = $sentencia->execute();
            $resultado['nuevo'] = $this->dblink->lastInsertId();

            if($resultado['estado'] == 1){
                $this->dblink->commit();
                $resultado['mensaje'] = 'Registrado correctamente';
            }
            else{
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    //  AQUIII REALIZAR UNA FUNCION PARA ACTUALIZAR UN CAMPO ESPECIFICO EN LA TABLA
    function actualizar_columna($array){
        $this->dblink->beginTransaction();
        try {
            $conteo = count($array) - 1;
            $sql = "UPDATE tb_mesconta SET ";
            for($i = 0; $i < $conteo; $i++){
                if($i > 0){
                    $sql .= ", ";
                }
                $sql .= "{$array[$i]['column_name']} = :param{$i}";
            }
            $sql .= " WHERE {$array[$conteo]['column_name']} = :param{$conteo};";
            
            $sentencia = $this->dblink->prepare($sql);
            for($i = 0; $i < count($array); $i++){
                $_PARAM = strtoupper($array[$i]['datatype'])=='INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                $sentencia->bindParam(":param{$i}", $array[$i]["param{$i}"], $_PARAM);
            }
            $resultado = $sentencia->execute();

            if($resultado == 1){
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
    
}

//ALT + FLECHA ARRIBA / ABAJO :  para mover la linea de codigo || SHIFT + SUPR : para eliminar la linea actual
//CTRL + L : para seleccionar la linea
// div#MyID.myClass: abreviatura emmet, para escribir un div con # para id="MyID" - con . para class="myClass"
// $retorno["mensaje"] = $sql; return $retorno; exit();
?>