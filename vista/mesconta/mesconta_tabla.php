<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL . 'mesconta/Mesconta.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../mesconta/Mesconta.class.php');
    require_once('../funciones/fechas.php');
}

$oMesconta = new Mesconta();

$result = $oMesconta->listar_todos(1); //LOS ACTIVOS

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $tr .= '<tr id="tabla_cabecera_fila">';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_mesconta_id'] . '</td>
          <td id="tabla_fila">' . $value['tb_mesconta_anio'] . '</td>
          <td id="tabla_fila">' . date("m", strtotime($value['tb_mesconta_anio'].'-'.$value['tb_mesconta_mes'])) . '</td>
          <td id="tabla_fila">' . $value['tb_usuario_nom'] .' ' . $value['tb_usuario_ape'] . '</td>
          <td id="tabla_fila">' . $value['aperturado'] . '</td>
          <td id="tabla_fila">' . $value['declarado'] . '</td>
          <td id="tabla_fila" align="center">
            <button class="btn btn-info btn-xs" title="Ver"' . " onclick='mesconta_form(\"L\"," . $value['tb_mesconta_id'] . ")'>".'<i class="fa fa-eye"></i> Ver</button>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="mesconta_form(\'M\',' . $value['tb_mesconta_id'] . ')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="mesconta_form(\'E\',' . $value['tb_mesconta_id'] . ')"><i class="fa fa-trash"></i> Eliminar</a>
            <a class="btn btn-success btn-xs" title="Historial" onclick="mesconta_historial_form(' . $value['tb_mesconta_id'] . ')"><i class="fa fa-eye"></i> Historial</a>
          </td>
        ';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
?>
<table id="tbl_mescontas" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">Año</th>
            <th id="tabla_cabecera_fila">Mes</th>
            <th id="tabla_cabecera_fila">Aperturado por</th>
            <th id="tabla_cabecera_fila">Está aperturado</th>
            <th id="tabla_cabecera_fila">Está declarado</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
<?php echo $tr; ?>
    </tbody>
</table>
