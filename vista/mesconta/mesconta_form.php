<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../mesconta/Mesconta.class.php');
  $oMesconta = new Mesconta();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'mesconta';
  $usuarioperfil_id = intval($_SESSION['usuarioperfil_id']);
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $mesconta_id = intval($_POST['mesconta_id']);
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Mes contable Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Mes contable';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Mes contable';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Mes contable';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en mesconta
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'mesconta'; $modulo_id = $mesconta_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del mesconta por su ID
    $mesconta_anio_mes = date('m-Y');
    $aperturado = 1;
    $declarado  = 0;

    if(intval($mesconta_id) > 0){
      $array[0]['column_name'] = 'tb_mesconta_id';
      $array[0]['param0'] = $mesconta_id;
      $array[0]['datatype'] = 'INT';

      $result = $oMesconta->mostrarUno($array);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el mesconta seleccionado, inténtelo nuevamente. '.$result['mensaje'];
          $bandera = 4;
        }
        else{
          $aperturado = intval($result['data']['tb_mesconta_aperturado']);
          $declarado  = intval($result['data']['tb_mesconta_estadeclarado']);
          $mes        = $result['data']['tb_mesconta_mes'];
          $anio       = $result['data']['tb_mesconta_anio'];
          $mesconta_anio_mes = $anio.'-'.$mes;
          $mesconta_anio_mes = date("m-Y", strtotime($mesconta_anio_mes));
          $compra_correlativo = empty($result['data']['tb_mesconta_compracorrel']) && ($action == 'insertar' || $action == 'modificar') ? "DC{$anio}{$mes}0001" : $result['data']['tb_mesconta_compracorrel'];
          $venta_correlativo = empty($result['data']['tb_mesconta_ventacorrel']) && ($action == 'insertar' || $action == 'modificar') ? "DV{$anio}{$mes}0001" : $result['data']['tb_mesconta_ventacorrel'];
        }
      $result = $result['data'];
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_mesconta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document" style="width: 60%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_mesconta" method="post">
          <input type="hidden" id="action" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_mesconta_id" value="<?php echo $mesconta_id;?>">
          <input type="hidden" id="mesconta_vista" name="mesconta_vista" value="<?php echo $vista;?>">
          <input type="hidden" id="hdd_mesconta_registro" name="hdd_mesconta_registro" value='<?php echo json_encode($result);?>'>
          
          <div class="modal-body">
            <?php if ($action == 'eliminar')
              echo '<label style="color: green; font-size: 12px">- No se podrá eliminar si hay documentos registrados en este periodo.</label><p>' ?>
            <div class="col-md-12">
              <div class="tab-content">
                <div class="row form-group">
                  <div class="col-md-5">
                    <label for="mesconta" class="control-label mayus">Mes:</label>
                    <div class="input-group date" id="mescontaDiv">
                      <input type="text" class="form-control" id="mesconta_picker" name="mesconta_picker" placeholder="mm-aaaa" readonly="" value="<?php echo $mesconta_anio_mes;?>" >
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-6">
                    <label for="rad_mesconta_aperturado" class="control-label mayus">Aperturado:</label>
                    <div class="radio">
                      <label><input type="radio" name="rad_mesconta_aperturado" class="flat-green" value="1" <?php if($aperturado==1){echo 'checked';}?>> SI</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="rad_mesconta_aperturado" class="flat-green" value="0" <?php if($aperturado==0){echo 'checked';} ?>> NO</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="rad_mesconta_declarado" class="control-label mayus">Declarado:</label>
                    <div class="radio">
                      <label><input type="radio" name="rad_mesconta_declarado" class="flat-green" value="1" <?php if($declarado==1){echo 'checked';} ?>> SI</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="rad_mesconta_declarado" class="flat-green" value="0" <?php if($declarado==0){echo 'checked';} ?>> NO</label>
                    </div>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-6">
                    <label for="txt_mesconta_compracorr" class="mayus">Correlativo para compras:</label>
                    <input type="text" name="txt_mesconta_compracorr" id="txt_mesconta_compracorr" class="form-control input-sm mayus" placeholder="correlativo compras" value="<?php echo $compra_correlativo;?>">
                  </div>
                  <div class="col-md-6">
                    <label for="txt_mesconta_ventacorr" class="mayus">Correlativo para ventas:</label>
                    <input type="text" name="txt_mesconta_ventacorr" id="txt_mesconta_ventacorr" class="form-control input-sm mayus" placeholder="correlativo ventas" value="<?php echo $venta_correlativo;?>">
                  </div>
                </div>
              </div>
              <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Mes contable?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="mesconta_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
            </div>
          </div>
          
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_mesconta">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_mesconta">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_mesconta">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/mesconta/mesconta_form.js';?>"></script>
