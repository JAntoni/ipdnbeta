<?php
require_once('../../core/usuario_sesion.php');
require_once('../mesconta/Mesconta.class.php');
$oMesconta = new Mesconta();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../compracontadoc/Compracontadoc.class.php');
$oCCD = new Compracontadoc();

$action = $_POST['action'];
$mesconta_id = intval($_POST['hdd_mesconta_id']);
$usuario_id = intval($_SESSION['usuario_id']);
$mesconta_fecha= explode('-',$_POST['mesconta_picker']);
$mesconta_fecha = $mesconta_fecha[1].'-'.$mesconta_fecha[0]; //formato año mes : 2020-01
$data = array();

$oMesconta->setTbMescontaUsumod($usuario_id);

$data['estado'] = 0;
/* $data['mensaje'] = $mesconta_fecha;
echo json_encode($data); exit(); */

if($action == 'eliminar'){
    $oMesconta->setTbMescontaId($mesconta_id);

    $data['mensaje'] = 'Existe un error al eliminar el Mes contable.';

    $registros = $oCCD->buscar_por_mesconta($mesconta_fecha);

    if($registros['estado'] == 1){
        $data['mensaje'] = 'No se puede eliminar el mes contable porque hay documentos en este periodo.';
    }
    else{
        if($oMesconta->eliminar()){
            $data['estado'] = 1;
            $data['mensaje'] = 'Se eliminó el Mes contable';
    
            //insertar historial
            $oHist->setTbHistUsureg($usuario_id);
            $oHist->setTbHistNomTabla('tb_mesconta');
            $oHist->setTbHistRegmodid($mesconta_id);
            $oHist->setTbHistDet('Eliminó el periodo contable del mes de '.$mesconta_fecha);
            $oHist->insertar();
        }
    }
}
else{
    $data['mensaje'] = 'Ocurrió un error al ' . $action . ' el Mes contable.';

    $mesconta_aperturado= intval($_POST['rad_mesconta_aperturado']);
    $mesconta_declarado= intval($_POST['rad_mesconta_declarado']);
    $mesconta_anio = date('Y', strtotime($mesconta_fecha));
    $mesconta_mes = date('m', strtotime($mesconta_fecha));
    $compra_correl = strtoupper($_POST['txt_mesconta_compracorr']);
    $venta_correl = strtoupper($_POST['txt_mesconta_ventacorr']);

    $oMesconta->setTbMescontaAnio($mesconta_anio);
    $oMesconta->setTbMescontaMes($mesconta_mes);
    $oMesconta->setTbMescontaAperturado($mesconta_aperturado);
    $oMesconta->setTbMescontaEstadeclarado($mesconta_declarado);
    $oMesconta->setTbMescontaCompracorrel($compra_correl);
    $oMesconta->setTbMescontaVentacorrel($venta_correl);

    $arreglo[0]['column_name'] = 'tb_mesconta_anio';
    $arreglo[0]['param0'] = $mesconta_anio;
    $arreglo[0]['datatype'] = 'STR';

    $arreglo[1]['column_name'] = 'tb_mesconta_mes';
    $arreglo[1]['param1'] = $mesconta_mes;
    $arreglo[1]['datatype'] = 'STR';
    $result = $oMesconta->mostrarUno($arreglo);
    
    $existe = $result['estado'];
    $registro_coincide = $result['data'];

    if($action == 'insertar'){
        if($existe == 0){
            $oMesconta->setTbMescontaUsureg($usuario_id);
            $res = $oMesconta->insertar();

            if($res['estado'] == 1){
                $data['estado'] = 1;
                $data['mensaje'] = $res['mensaje'];

                //insertar historial
                $oHist->setTbHistUsureg($usuario_id);
                $oHist->setTbHistNomTabla('tb_mesconta');
                $oHist->setTbHistRegmodid($res['nuevo']);
                $oHist->setTbHistDet('Registró el periodo contable del mes de '.$_POST['mesconta_picker']);
                $oHist->insertar();
            }
        }
        else{
            $data['mensaje'] = 'El mes contable para el periodo '.$mesconta_fecha.' ya está registrado, con id: '.$registro_coincide['tb_mesconta_id'];
        }
    }
    elseif($action == 'modificar'){
        $evaluar0 = $mesconta_id == intval($registro_coincide['tb_mesconta_id']) ? true : false;
        
        if($existe == 0 || ($existe == 1 && $evaluar0)){
            $registro_origin = (array) json_decode($_POST['hdd_mesconta_registro']);

            $evaluar1 = $mesconta_anio == $registro_origin['tb_mesconta_anio'] ? true : false;
            $evaluar2 = $mesconta_mes == $registro_origin['tb_mesconta_mes'] ? true : false;
            $evaluar3 = $mesconta_aperturado == intval($registro_origin['tb_mesconta_aperturado']) ? true : false;
            $evaluar4 = $mesconta_declarado == intval($registro_origin['tb_mesconta_estadeclarado']) ? true : false;
            $evaluar5 = $compra_correl == strtoupper($registro_origin['tb_mesconta_compracorrel']);
            $evaluar6 = $venta_correl == strtoupper($registro_origin['tb_mesconta_ventacorrel']);
            
            if( $evaluar1 && $evaluar2 && $evaluar3 && $evaluar4 && $evaluar5 && $evaluar6 ){
                $data['estado'] = 2;
                $data['mensaje'] = "No se realizó ninguna modificacion";
            }
            else{
                $ARRAY[0] = 'NO';
                $ARRAY[1] = 'SI';
                $oMesconta->setTbMescontaId($mesconta_id);
                $res = $oMesconta->modificar();

                if($res == 1){
                    $data['estado'] = 1;
                    $data['mensaje'] = "Se modificó correctamente";

                    //insertar historial
                    $oHist->setTbHistUsureg($usuario_id);
                    $oHist->setTbHistNomTabla('tb_mesconta');
                    $oHist->setTbHistRegmodid($mesconta_id);
                    $mensaje = 'Modificó el periodo contable con id '.$mesconta_id.':';
                    if(!$evaluar1 || !$evaluar2)
                        $mensaje .= '<br>  - Cambió Periodo contable: '.$registro_origin['tb_mesconta_mes'].'-'.$registro_origin['tb_mesconta_anio'].' => <b>'.$mesconta_mes.'-'.$mesconta_anio.'</b>';
                    if(!$evaluar3)
                        $mensaje .= '<br>  - Cambió Apertura del periodo contable: '. $ARRAY[intval($registro_origin['tb_mesconta_aperturado'])].' => <b>'.$ARRAY[$mesconta_aperturado].'</b>';
                    if(!$evaluar4)
                        $mensaje .= '<br>  - Cambió Declarado del periodo contable: '. $ARRAY[intval($registro_origin['tb_mesconta_estadeclarado'])].' => <b>'.$ARRAY[$mesconta_declarado].'</b>';
                    if(!$evaluar5)
                        $mensaje .= "<br>  - Cambió el correlativo de compras: {$registro_origin['tb_mesconta_compracorrel']} => <b> $compra_correl </b>";
                    if(!$evaluar6)
                        $mensaje .= "<br>  - Cambió el correlativo de ventas: {$registro_origin['tb_mesconta_ventacorrel']} => <b> $venta_correl </b>";
                    $oHist->setTbHistDet($mensaje);
                    $oHist->insertar();
                }
            }
        }
        else{
            $data['mensaje'] = 'El mes y año que intenta modificar ya está registrado, con id: '.$registro_coincide['tb_mesconta_id'];
        }
    }
    else{
        $data['estado'] = 0;
        $data['mensaje'] = 'El mes contable para el periodo '.$mesconta_fecha.' ya está registrado, con id: '.$registro_coincide['tb_mesconta_id'];
    }
}

echo json_encode($data);

?>