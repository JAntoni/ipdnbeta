<?php
if(defined('APP_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    require_once (VISTA_URL.'morapago/Morapago.class.php');
}
else{
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once ("../morapago/Morapago.class.php");
}
require_once ("Cobranzamora.class.php");
$oCobranzamora = new Cobranzamora();
$oMorapago = new Morapago();

$fecha_hoy = date('d-m-Y');


?>

<?php
$cre_id_cm=0;
//PAGO DE MORA DE CREDITO MENOR
$dts=$oCobranzamora->filtrar_menor($_POST['hdd_fil_cli_id'],$cre_id_cm,fecha_mysql($fecha_hoy));

if($dts['estado']==1)
{
?>
  <h1 style="text-align:left; padding:10px 0 0 5px;margin-bottom:2px;">CRÉDITO MENOR</h1>
  <table id="tabla_cobranzamoras" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">CLIENTE</th>
                <th id="tabla_cabecera_fila">TIPO</th>
                <th  id="tabla_cabecera_fila"align="center">N° CUOTA</th>
                <?php /*<th align="center">VENCIMIENTO</th>*/ ?>
                <th  id="tabla_cabecera_fila"align="center">MONTO MORA</th>                
                <th id="tabla_cabecera_fila">PAGOS</th>
                <th id="tabla_cabecera_fila">SALDO</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th id="tabla_cabecera_fila"></th>
                <th  id="tabla_cabecera_fila"align="center">CRÉDITO</th>
            </tr>
        </thead>
      <tbody>
      <?php
      foreach($dts['data']as $key=>$dt){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($dt['tb_cuota_fec'])){$vencida = 0;};
      ?>
          <tr id="tabla_cabecera_fila">
          <td id="tabla_fila"><?php echo $dt['tb_cliente_nom']." | ".$dt['tb_cliente_doc'];?></td>
          <td id="tabla_fila"><?php echo $dt['tb_cuotatipo_nom']?></td>
          <td id="tabla_fila" align="center"><?php if($dt['tb_cuotatipo_id']==2){echo $dt['tb_cuota_num'].'/'. $dt['tb_credito_numcuo'];}else{echo $dt['tb_cuota_num'].'/'. $dt['tb_credito_numcuomax'];}?></td>
          <?php /*<td align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrarFecha($dt['tb_cuota_fec'])?></td>*/ ?>
          <?php 
              $pagos_cuota=0;
              $mod_id=1;
              $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id,$dt['tb_cuota_id']);
              if($dts3['estado']==1)
              {
                  foreach($dts3['data']as $key=>$dt3)
                  {
                      $pagos_cuota+=$dt3['tb_ingreso_imp'];
                  }

              }

              $saldo=$dt['tb_cuota_mor']-$pagos_cuota;
          ?>
          <td id="tabla_fila"align="right" style="padding-right:10px;"><?php echo "S/."." ". mostrar_moneda($dt['tb_cuota_mor']);?></td>
          <td id="tabla_fila"align="right" style="padding-right:10px;"><?php echo "S/."." ".mostrar_moneda($pagos_cuota);?></td>
          <td id="tabla_fila"align="right" style="padding-right:10px;"><?php echo "S/."." ".mostrar_moneda($saldo);?></td>
          <td id="tabla_fila">
              <?php 
              $estado="";
              if($dt['tb_cuota_morest']==0)$estado = 'POR COBRAR';
              if($dt['tb_cuota_morest']==2)$estado = 'CANCELADA';
              if($dt['tb_cuota_morest']==3)$estado = 'PAGO PARCIAL';

              echo $estado;

              if($dt['tb_cuotatipo_id']==1)$action='pagar_libre';
              if($dt['tb_cuotatipo_id']==2)$action='pagar_fijo';
              ?>
              
          </td>
          <td id="tabla_fila" align="center"><?php if($dt['tb_cuota_morest']!=2){?>
              <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="morapago_menor_form('<?php echo $action?>','<?php echo $dt['tb_cuota_id']?>')">Pagar</a>
              <?php }?>
              <?php if($dt['tb_cuota_morest']!=2 && $dt['tb_cuota_morest']!=3 && $dt['tb_cuota_mor']>0){?>
                  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="mora_eliminar(<?php echo '1,'.$dt['tb_cuota_id']; ?>)"><i class="fa fa-trash"></i></a>
                <?php }?>
            </td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="creditomenor_pagos('<?php echo $dt['tb_credito_id']?>')">Historial</a>
            <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="creditomenor_form('L','<?php echo $dt['tb_credito_id']?>')">CM-<?php echo str_pad($dt['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a>
          </td>
          </tr>
      <?php
          }
      ?>
      </tbody>
  </table>
<?php
}


$resultgarveh=$oCobranzamora->filtrar_garveh($_POST['hdd_fil_cli_id'],$cre_id_cm,fecha_mysql($fecha_hoy));

if($resultgarveh['estado']==1)
{
?>
<br>
<h1 style="text-align:left; padding:10px 0 0 5px;margin-bottom:2px;">GARANTÍA VEHICULAR</h1>
<table id="tabla_cobranzamoras_garveh" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" align="center">CLIENTE</th>
            <th id="tabla_cabecera_fila" align="center">N° CUOTA</th>
            <!--th align="center">VENCIMIENTO</th-->
            <th id="tabla_cabecera_fila" align="center">MORA</th>
            <th id="tabla_cabecera_fila" align="center">PAGOS</th>
            <th id="tabla_cabecera_fila" align="center">SALDO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila"></th>
            <th id="tabla_cabecera_fila" align="center">CRÉDITO</th>
        </tr>
    </thead>
    <tbody>
    <?php
      foreach($resultgarveh['data']as $key=>$garveh){
        $mora = 0;
        $vencida = 1;
        if(strtotime($fecha_hoy) <= strtotime($garveh['tb_cuota_fec'])){$vencida = 0;};

        $result=$oCobranzamora->mostrarUnoGarveh($garveh['tb_cuota_id']);
        if($result['estado']==1){
            //echo $garveh['tb_cuota_id'];
                $mora = $mora + $result['data']["tb_cuotadetalle_mor"];
        }
      ?>
      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila" align="center"><?php echo $garveh['tb_cliente_nom']." | ".$garveh['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $garveh['tb_cuota_num'].'/'.$garveh['tb_credito_numcuo']?></td>
        <?php
            $pagos_cuota=0;
            $mod_id=2;
            $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id,$garveh['tb_cuotadetalle_id']);
            if($dts3['estado']==1){
                foreach($dts3['data'] as $key=>$dt3){
                    $pagos_cuota+=$dt3['tb_ingreso_imp'];
                }
            }
            $dts3 = null;

            $saldo=$mora-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="center"><?php echo "S/. ".mostrar_moneda($mora);?></td>
        <td id="tabla_fila" align="center"><?php echo "S/. ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="center"><?php echo "S/. ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila" align="center">
            <?php 
                $estado = 'POR COBRAR';
                echo $estado;
                ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="creditogarveh_pagos('<?php echo $garveh['tb_credito_id'] ?>', 3)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="creditogarveh_form('L', '<?php echo $garveh['tb_credito_id'] ?>')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT); ?></a></td>
      </tr>
      <?php
        $cuotadetallegarveh=$oCobranzamora->mostrarUnoGarveh($garveh['tb_cuota_id']);
        if($cuotadetallegarveh['estado']==1){
            $cuotadetalle = $cuotadetallegarveh['data'];
            $vencida = 1;
            if(strtotime($fecha_hoy) <= strtotime($cuotadetalle['tb_cuotadetalle_fec'])){$vencida = 0;};
            if($cuotadetalle['tb_cuotadetalle_mor']>0){
                ?>
                <tr class="even" id="tabla_fila">
                <td id="tabla_fila" align="center" style="padding-left:20px;"><?php echo "-";?></td>
                <td id="tabla_fila" align="center"><?php echo $cuotadetalle['tb_cuotadetalle_num'].'/'.$cuotadetalle['tb_cuota_persubcuo']?></td>
                <?php
                    $pagos_cuotadetalle=0;
                    $mod_id=2;
                    $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id,$cuotadetalle['tb_cuotadetalle_id']);
                    if($dts3['estado']==1){
                        foreach($dts3['data'] as $key=>$dt3){
                            $pagos_cuotadetalle+=$dt3['tb_ingreso_imp'];
                        }
                    }
                    $dts3 = null;

                    //$saldo=$mora-$pagos_cuotadetalle;
                ?>
                <td id="tabla_fila" align="center"><?php echo "S/. ".mostrar_moneda($cuotadetalle['tb_cuotadetalle_mor']);?></td>
                <td id="tabla_fila" align="center"><?php echo "S/. ".mostrar_moneda($pagos_cuotadetalle);?></td>
                <td id="tabla_fila" align="center"><?php echo "S/. ".mostrar_moneda($cuotadetalle['tb_cuotadetalle_mor']-$pagos_cuotadetalle);?></td>
                <td id="tabla_fila" align="center">
                    <?php 
                        $estado="";
                        if($cuotadetalle['tb_cuotadetalle_morest']==1)$estado = 'POR COBRAR';
                        if($cuotadetalle['tb_cuotadetalle_morest']==2)$estado = 'CANCELADA';
                        if($cuotadetalle['tb_cuotadetalle_morest']==3)$estado = 'PAGO PARCIAL';
                        echo $estado;

                        if($cuotadetalle['tb_cuotatipo_id']==3)$action='pagar_libre';
                        if($cuotadetalle['tb_cuotatipo_id']==4)$action='pagar_fijo';

                    ?>
                </td>
                <td id="tabla_fila" align="center">
                    <?php if($cuotadetalle['tb_cuotadetalle_morest']!=2 && $cuotadetalle['tb_cuotadetalle_mor']>0){?>
                        <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="morapago_garveh_form('<?php echo $action;?>','<?php echo $cuotadetalle['tb_cuotadetalle_id']?>')">Pagar</a>
                    <?php }?>
                    <?php if($cuotadetalle['tb_cuotadetalle_morest']!=2 && $cuotadetalle['tb_cuotadetalle_morest']!=3 && $cuotadetalle['tb_cuotadetalle_mor']>0){?>
                  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="mora_eliminar(<?php echo '2,'.$cuotadetalle['tb_cuotadetalle_id']; ?>)"><i class="fa fa-trash"></i></a>
                <?php }?>
                </td>
                <td></td>
                <?php
            }
        }
          }
      ?>
    </tbody>
    </table>
<?php
}
?>
