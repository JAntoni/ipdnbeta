/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('.moneda2').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '99999.00'
	});
	$('.moneda3').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '99999.00'
    });
    $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
    });
    
    
    $("#for_cuopag").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"cobranzamora/morapago_garveh_reg.php",
				async:true,
				dataType: "json",
				data: $("#for_cuopag").serialize(),
				beforeSend: function() {
					//alert($("#for_cuopag").serialize());
					$('#msj_vencimiento').html("Guardando...");
					$('#msj_vencimiento').show(100);
				},
				success: function(data){				
          if(parseInt(data.morpag_id) > 0){
              $('#modal_registro_cobranzamora').modal('hide');
              //cobranzamora_tabla();
              swal_success("SISTEMA",data.morpag_id+"   "+data.morpag_msj,2000);

//                                        if(confirm('¿Desea imprimir voucher de pago de mora?'))
//                                                morapago_imppos_datos(data.cuopag_id);
              var vista=$("#hdd_vista").val();
              console.log(vista);
                  if(vista=="cmb_ven_id")
                  {
                          vista+'.(data.ven_id)';
                  }
                  if(vista=="vencimiento_tabla")
                  {
                      cobranzamora_tabla();
                  }
                  if(vista=="pago_cuota")
                  {
                      //cobranzamora_tabla();
                      console.log("REGISTRO CORRECTO, FALTA COMPROBANTE");
                      
                  }
                  if(vista=="historial_mora")
                  {
                    morapago_pagos_tabla();
                      
                  }

                  ingreso_imppos_datos3(data.ingreso_id, data.empresa_id)
                  
                  $('#msj_vencimiento').hide();
                  $('#modal_registro_cobranzamora').hide();
          }
          else{
                  swal_success("SISTEMA",data.morpag_id+"   "+data.morpag_msj,6000);
          }
				},
				complete: function(data){
					if(data.statusText != "success"){
						$('#msj_menor_form').text(data.responseText);
					}
				}
			});
		},
		rules: {
//			txt_cuopag_fec: {
//				required: true,
//				dateITA: true
//			},
			txt_cuopag_mon:{
				required:true
			}
		},
		messages: {
//			txt_cuopag_fec: {
//				required: '*',
//				dateITA: '?'
//			},
			txt_cuopag_mon:{
				required:'*'
			}
		}
	});
    
    $( "#txt_cuopag_mon" ).blur(function(e) {
        $( "#cmb_mon_id" ).change();
    })

    
    $( "#cmb_mon_id" ).change(function(e) {
  
        var mon_tot = Number($('#txt_cuopag_mon').autoNumeric('get'));
        var tip_cam = Number($('#txt_cuopag_tipcam').val());
        var mon_id = $('#hdd_mon_id').val();
        var res = 0;
  
        if(parseInt($(this).val()) == parseInt(mon_id))
          $('#txt_cuopag_mon').autoNumeric('set',mon_tot);
        else{
          if(parseInt($(this).val()) == 1){
            res = mon_tot * tip_cam;
            $('#txt_cuopag_moncam').autoNumeric('set',res.toFixed(2));
          }
          else{
            res = mon_tot / tip_cam;
            $('#txt_cuopag_moncam').autoNumeric('set',res.toFixed(2));
          }
        }
        console.log(Number($('#txt_cuopag_moncam').autoNumeric('get')));
        div_tipocambio();
      });
    

	  cambio_moneda(2)
});

function cambio_moneda(monid){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"monedacambio/monedacambio_controller.php",
            async:false,
            dataType: "json",
            data: ({
                    action: 'obtener_cambio',
                    fecha:	$('#txt_cuopag_fec').val(),
                    tipo:	"compra"
            }),
            beforeSend: function(){
                    $('#msj_pagobanco').hide(100);
            },
            success: function(data){
                    if(data.estado == 0){
                            $('#txt_cuopag_tipcam').val('');
                            $('#msj_pagobanco').show(100);
                            $('#msj_pagobanco').html('Por favor registre el tipo de cambio del día de la fecha de depósito');
                            swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
                    }
                    else
                            $('#txt_cuopag_tipcam').val(data.moncam_val);	
            },
            complete: function(data){			
//                 console.log(data);
            }
        });		
}

function div_tipocambio()
{
    var mon_select = $('#cmb_mon_id').val();
    var mon_id = $('#hdd_mon_id').val();

    if (mon_id != mon_select) {
        //$('.div_tipocambio').show();
        $('.div_mon_tipocambio').show();
    } else {
        //$('.div_tipocambio').hide();
        $('.div_mon_tipocambio').hide();
    }
}

function ingreso_imppos_datos3(id_ing,empresa_id) {
	var codigo=id_ing;
	window.open("http://ipdnsac.com/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
}