<?php 
require_once('../../core/usuario_sesion.php');

require_once ("Cobranzamora.class.php");
$oVencimiento = new Cobranzamora();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../morapago/Morapago.class.php");
$oMorapago = new Morapago();

require_once("../funciones/fechas.php");
require_once("../funciones/funciones.php");

$vista=$_POST['vista'];


if($_POST['action']=="pagar_fijo" or $_POST['action']=="pagar_libre")
{
	$fecha_hoy = date('d-m-Y');

	$dts=$oCuota->mostrarUno($_POST['cuo_id']);
            if($dts['estado']==1){
		$cre_id=$dts['data']['tb_credito_id'];
		$cuo_fec=mostrar_fecha($dts['data']['tb_cuota_fec']);
		$cuo_num=$dts['data']['tb_cuota_num'];
		$mon_id=$dts['data']['tb_moneda_id'];
		$mon_nom=$dts['data']['tb_moneda_nom'];
		$cuo_cuo=$dts['data']['tb_cuota_mor'];
		$cuo_int=$dts['data']['tb_cuota_int'];
		$cuo_cap=$dts['data']['tb_cuota_cap'];
		$cuo_persubcuo=$dts['data']['tb_cuota_persubcuo'];
            }
            $dts=NULL;
	$fec=$cuo_fec;
	
	$dts=$oCredito->mostrarUno($cre_id);
        
            if($dts['estado']==1){
		$cuotip_id=$dts['data']['tb_cuotatipo_id'];
		$cuotip_nom=$dts['data']['tb_cuotatipo_nom'];
		$cre_int = $dts['data']['tb_credito_int'];
		$cre_numcuo = $dts['data']['tb_credito_numcuo'];
		$cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
		$cli_id=$dts['data']['tb_cliente_id'];
		$cli_doc = $dts['data']['tb_cliente_doc'];
		$cli_nom=$dts['data']['tb_cliente_nom'];
		$cliente=$cli_nom.' | '.$cli_doc;
            }
            
            $dts=NULL;
            $codigo='CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);

	if($cuotip_id==2)$cuota=$cuo_num.'/'.$cre_numcuo.'';
	if($cuotip_id==1)$cuota=$cuo_num.'/'.$cuo_num.' ('.$cre_numcuomax.')';

	$pag_det="PAGO DE MORA $codigo N° CUOTA: $cuota";

	$mod_id='1';//cuota
	$modid=$_POST['cuo_id'];


    //pagos parciales
	$pagos_cuota=0;
    $mod_id=1;
    $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id,$_POST['cuo_id']);
    
    if($dts3['estado']==1){
        foreach($dts3['data']as $key=>$dt3)
        {
             $pagos_cuota+=$dt3['tb_ingreso_imp'];
        }
    }
    
    $pagos=$pagos_cuota;
    $pagos_parciales= mostrar_moneda($pagos);


	$monto_pagar=mostrar_moneda($cuo_cuo-$pagos);

	if($cuotip_id==2)//fijo
	{
		if($cuo_num==$cre_numcuo)
		{
			$vMax=moneda_mysql($monto_pagar);
		}
		else
		{
			$vMax='99999.00';
		}
	}

	if($cuotip_id==1)//libre
	{
		$vMax=moneda_mysql($monto_pagar);
		if($cuo_num==$cre_numcuomax)
		{
			//$ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
			
		}
		else
		{
			//$vMax='99999.00';
		}
	}
}

if($usureg>0)
{
	$rws=$oUsuario->mostrarUno($usureg);
	if($rws['estado']==1){
		$usureg	=$rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
        }
}

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cobranzamora" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">INFORMACIÓN DE MORA A PAGAR  FECHA </h4>
        </div>
            <form id="for_cuopag">
                <input name="action_morapago" id="action_morapago" type="hidden" value="<?php echo $_POST['action']?>">
                <input name="hdd_cuo_id" id="hdd_cuo_id" type="hidden" value="<?php echo $_POST['cuo_id']?>">

                <input name="hdd_cli_id" id="hdd_cli_id" type="hidden" value="<?php echo $cli_id ?>">
                <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id?>">

                <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $mon_id ?>">

                <input name="hdd_mod_id" id="hdd_mod_id" type="hidden" value="<?php echo $mod_id?>">
                <input name="hdd_modid" id="hdd_modid" type="hidden" value="<?php echo $modid?>">

                <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id ?>">
                <input name="hdd_cre_int" id="hdd_cre_int" type="hidden" value="<?php echo $cre_int ?>">
                <input name="hdd_cuo_fec" id="hdd_cuo_fec" type="hidden" value="<?php echo $cuo_fec ?>">
                <input name="hdd_cuo_num" id="hdd_cuo_num" type="hidden" value="<?php echo $cuo_num ?>">
                <input name="hdd_cuo_max" id="hdd_cuo_max" type="hidden" value="<?php echo $cre_numcuomax?>">
                <input name="hdd_cuo_cap" id="hdd_cuo_cap" type="hidden" value="<?php echo $cuo_cap ?>">
                <input name="hdd_vista" id="hdd_vista" type="hidden" value="<?php echo $vista ?>">

                <input name="hdd_pag_det" id="hdd_pag_det" type="hidden" value="<?php echo $pag_det ?>">
                
                <div class="modal-body" style="font-family: cambria">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label" style="text-align: right">Cliente :</label>
                                    <div class="col-md-9">
                                      <?php echo $cliente?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Crédito :</label>
                                    <div class="col-md-9" >
                                     CRÉDITO MENOR - <?php echo $cuotip_nom?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Código :</label>
                                    <div class="col-md-9">
                                     <?php echo $codigo?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">N° de Cuota :</label>
                                    <div class="col-md-9">
                                     <?php echo $cuota.$ultima_cuota;?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Fec. de Vencimiento :</label>
                                    <div class="col-md-9">
                                     <?php echo $cuo_fec; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-md-6 control-label" style="text-align: right">Cuota (<?php echo $mon_nom?>):</label>
                                        <div class="col-md-6">
                                            <input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuodet_cuo" type="text" id="txt_cuodet_cuo" value="<?php echo mostrar_moneda($cuo_cuo)?>" size="15" maxlength="10" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item" style="border-color: #000000">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Total (<?php echo $mon_nom?>):</label>
                                            <div class="col-md-3">
                                                <input style="text-align:right;" class="form-control input-sm moneda" name="txt_cuopag_tot" type="text" id="txt_cuopag_tot" value="<?php echo $monto_pagar;?>" size="15" maxlength="10" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Fecha de Pago:</label>
                                            <div class="col-md-3">
                                            <div class='input-group date' id='ingreso_fil_picker1'>
                                                <input name="txt_cuopag_fec" type="text" class="form-control input-sm mayus" class="fecha" id="txt_cuopag_fec" value="<?php echo $fecha_hoy?>" size="10" maxlength="10" readonly>
                                                <span class="input-group-addon">
                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Monto Pago (<?php echo $mon_nom?>):</label>
                                            <div class="col-md-3">
                                                <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $monto_pagar?>" size="15" maxlength="150" <?php echo $readonly?> >
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guar_cuopag">Guardar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>   
            </form>   
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranzamora/morapago_menor_form.js?ver=121347';?>"></script>