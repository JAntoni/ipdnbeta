<form id="vencimiento_filtro" name="vencimiento_filtro">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Cliente</label>
                <input type="text" class="form-control input-sm mayus" id="txt_fil_cli" name="txt_fil_cli">
                <input type="hidden" id="hdd_fil_cli_id" name="hdd_fil_cli_id" class="form-control input-sm" >
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">&zwj;</label>
              <div class="input-group">
                  <button type="button" class="btn btn-primary btn-sm" onclick="cobranzamora_tabla()" title="Filtrar"><i class="fa fa-search"></i></button>
                  <!--<span class="input-group-addon"></span>-->
                  <button type="button" class="btn btn-success btn-sm" onclick="" title="Reestablecer"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
        </div>
    </div>
</form>