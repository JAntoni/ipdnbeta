/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('.moneda2').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '99999.00'
});
$('.moneda3').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '99999.00'
        });
    $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
    });
    
    
    $("#for_cuopag").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"cobranzamora/morapago_menor_reg.php",
				async:true,
				dataType: "json",
				data: $("#for_cuopag").serialize(),
				beforeSend: function() {
					//alert($("#for_cuopag").serialize());
					$('#msj_vencimiento').html("Guardando...");
					//$('#msj_vencimiento').show(100);
				},
				success: function(data){				
                                    if(parseInt(data.cuopag_id) > 0){
                                        $('#modal_registro_cobranzamora').modal('hide');
                                        swal_success("SISTEMA",data.cuopag_id+"   "+data.cuopag_msj,2000);

//                                        if(confirm('¿Desea imprimir voucher de pago de mora?'))
//                                                morapago_imppos_datos(data.cuopag_id);
                                        var vista=$("#hdd_vista").val();
                                            if(vista=="cmb_ven_id")
                                            {
                                                    vista+'.(data.ven_id)';
                                            }
                                            if(vista=="vencimiento_tabla")
                                            {
                                                cobranzamora_tabla();
                                            }
											if(vista=="historial_mora"){
												morapago_pagos_tabla();
											}
											
											ingreso_imppos_datos3(data.ingreso_id, data.empresa_id)
											
                                            $('#msj_vencimiento').hide();
                                            $('#modal_registro_cobranzamora').hide();
                                    }
                                    else{
                                            swal_success("SISTEMA",data.cuopag_id+"   "+data.cuopag_msj,6000);
                                    }
				},
				complete: function(data){
					if(data.statusText != "success"){
						$('#msj_menor_form').text(data.responseText);
					}
				}
			});
		},
		rules: {
//			txt_cuopag_fec: {
//				required: true,
//				dateITA: true
//			},
			txt_cuopag_mon:{
				required:true
			}
		},
		messages: {
//			txt_cuopag_fec: {
//				required: '*',
//				dateITA: '?'
//			},
			txt_cuopag_mon:{
				required:'*'
			}
		}
	});
    
    
    
});

function ingreso_imppos_datos3(id_ing,empresa_id) {
	var codigo=id_ing;
	window.open("http://ipdnsac.com/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
}