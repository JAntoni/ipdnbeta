<?php

require_once('../../core/usuario_sesion.php');

require_once("../morapago/Morapago.class.php");
$oMorapago = new Morapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$hoy = date('d-m-Y');
$fec_pago = $_POST['txt_cuopag_fec'];

if ($hoy != $fec_pago) {
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $hoy . ', fecha a pagar: ' . $fec_pago;
    echo json_encode($data);
    exit();
}

$numope = trim($_POST['txt_cuopag_numope']);
$monto_validar = formato_moneda($_POST['txt_cuopag_mon']);
$monto_validar_tot = formato_moneda($_POST['txt_cuopag_montot']);
$moneda_deposito_id = intval($_POST['hdd_mon_iddep']); //1 soles, 2 dolares
$moneda_credito_id = intval($_POST['hdd_mon_id']); //1 soles, 2 dolares
$tipocambio_dia = floatval($_POST['txt_cuopag_tipcam']);

$deposito_mon = 0;
$dts = $oDeposito->validar_num_operacion($numope);
if ($dts['estado'] == 1) {
    foreach ($dts['data'] as $key => $dt) {
        if($moneda_deposito_id == 1){
            $deposito_mon += floatval($dt['tb_deposito_mon']);
        }elseif($moneda_deposito_id == 2){
            $deposito_mon += floatval($dt['tb_deposito_mon']) * $tipocambio_dia;
        }

        $deposito_original += floatval($dt['tb_deposito_mon']);
    }
}
$dts = NULL;

if ($deposito_mon == 0) {
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
    echo json_encode($data);
    exit();
}

$dts = $oIngreso->ingresos_totales_num_operacion($numope);
if ($dts['estado'] == 1) {
    $ingreso_importe = floatval($dts['data']['importe']);
    $ingreso_mon_id = intval($dts['data']['tb_moneda_id']);
    if ($ingreso_mon_id == 2) //si el ingreso se hizo en dólares
        $ingreso_importe = moneda_mysql($ingreso_importe * $tipocambio_dia);
}
$dts = NULL;

$monto_usar = $deposito_mon - $ingreso_importe;
$monto_usar = formato_moneda($monto_usar);

if ($monto_validar_tot > $monto_usar) {
    $data['cuopag_id'] = 0;
    if($moneda_deposito_id == 2){
        $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar más de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar_tot) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPÓSITO: S/' . mostrar_moneda($deposito_mon) . " ó US$ " . mostrar_moneda($deposito_original);
    }else{
        $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar más de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar_tot) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPÓSITO: ' . mostrar_moneda($deposito_mon);
    }
    echo json_encode($data);
    exit();
}

$cre_tip = 0;
if ($_POST['hdd_credito'] == "asiveh") {
    require_once("../creditoasiveh/Creditoasiveh.class.php");
    $oCredito = new Creditoasiveh();
    $cre_tip = 2;
}
if ($_POST['hdd_credito'] == "garveh") {
    require_once ("../creditogarveh/Creditogarveh.class.php");
    $oCredito = new Creditogarveh();
    $cre_tip = 3;
}
if ($_POST['hdd_credito'] == "hipo") {
    require_once ("../creditohipo/Creditohipo.class.php");
    $oCredito = new Creditohipo();
    $cre_tip = 4;
}
if ($_POST['hdd_credito'] == "menor") {
    require_once ("../creditomenor/Creditomenor.class.php");
    $oCredito = new Creditomenor();
    $cre_tip = 4;
}

$moneda_depo = ' ? ';
if ($_POST['hdd_mon_iddep'] != '') {
    if ($_POST['hdd_mon_iddep'] == 1)
        $moneda_depo = 'S/.';
    else
        $moneda_depo = 'US$';
}

$moneda_cred = ' ? ';
if ($_POST['hdd_mon_id'] != '') {
    if ($_POST['hdd_mon_id'] == 1)
        $moneda_cred = 'S/.';
    else
        $moneda_cred = 'US$';
}
if($_POST['hdd_credito'] == "asiveh" || $_POST['hdd_credito'] == "garveh" || $_POST['hdd_credito'] == "hipo")
  {
  if(!empty($_POST['txt_cuopag_montot']))
  {
    //$fecha = date('Y-m-d');
    $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
    $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
    $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
    $tipo_caja = 1; //las cajas a tener en cuenta es 1 CAJA y 14 CAJA AP

    $xac=1;

    $oMorapago->tb_morapago_xac=$xac;
    $oMorapago->tb_morapago_usureg=$_SESSION['usuario_id'];
    $oMorapago->tb_morapago_usumod=$_SESSION['usuario_id'];
    $oMorapago->tb_modulo_id=$_POST['hdd_mod_id'];
    $oMorapago->tb_morapago_modid=$_POST['hdd_modid'];
    $oMorapago->tb_morapago_fec=$fecha;
    $oMorapago->tb_moneda_id=1;
    $oMorapago->tb_morapago_mon=moneda_mysql($mon_pagado);
    $oMorapago->tb_cliente_id=$_POST['hdd_cli_id'];

    $result=$oMorapago->insertar();
    if(intval($result['estado']) == 1){
        $cuopag_id = $result['morapago_id'];
    }


    //estado de cuota detalle
    if(moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot))
    {
        $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','2','INT');
        $monto = moneda_mysql($mon_pagado);
        if($monto > 0) $ingresar = 1;
    }

    if(moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot))
    {
        $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','3','INT');
        $monto = moneda_mysql($mon_pagado);
        if($monto > 0) $ingresar = 1;
    }

    if($ingresar == 1)
    {
    //registro de ingreso
        $subcue_id = 0; //
        if($_POST['hdd_credito'] == "asiveh")
        $subcue_id = 68; //CUOTAS asiveh
        if($_POST['hdd_credito'] == "garveh")
        $subcue_id = 69; //CUOTAS garveh
        if($_POST['hdd_credito'] == "hipo")
        $subcue_id = 70; //CUOTAS hipo

        $cue_id = 26; //CUENTA DE MORAS
        $mod_id = 50; //cuota mora
        $ing_det = $_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];

        if(!empty($_POST['txt_cuopag_obs']))
        $ing_det .= '. Obs: '.$_POST['txt_cuopag_obs'];

        $numdoc = $_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = $numdoc;
        $oIngreso->ingreso_det = $ing_det;
        $oIngreso->ingreso_imp = moneda_mysql($monto);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
        $oIngreso->caja_id = $tipo_caja;
        $oIngreso->moneda_id = 1;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $cuopag_id;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];
        $oIngreso->ingreso_fecdep = '';
        $oIngreso->ingreso_numope = '';
        $oIngreso->ingreso_mondep = 0;
        $oIngreso->ingreso_comi = 0;
        $oIngreso->cuentadeposito_id = 0;
        $oIngreso->banco_id = 0;
        $oIngreso->ingreso_ap = 0;
        $oIngreso->ingreso_detex = '';

        $result=$oIngreso->insertar();
        if(intval($result['estado']) == 1){
            $ingr_id = $result['ingreso_id'];
        }

        $oIngreso->ingreso_fecdep=fecha_mysql($_POST['txt_cuopag_fecdep']);
        $oIngreso->ingreso_numope =$_POST['txt_cuopag_numope'];
        $oIngreso->ingreso_mondep=moneda_mysql($_POST['txt_cuopag_mon']);
        $oIngreso->ingreso_comi= moneda_mysql($_POST['txt_cuopag_comi']);
        $oIngreso->cuentadeposito_id= $_POST['cmb_cuedep_id'];
        $oIngreso->banco_id =$_POST['cmb_banco_id'];
        $oIngreso->ingreso_id=$ingr_id;

        $oIngreso->registrar_datos_deposito_banco();

        $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '.mostrar_moneda($monto).' && <b>'.date('d-m-Y h:i a').'</b>';
        $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
        $oLineaTiempo->tb_lineatiempo_det=$line_det;
        $oLineaTiempo->insertar();
    }

    //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
    $subcue_id = 72;//72 es la subcuenta de cuenta en dolares
    if($_POST['hdd_mon_iddep'] == 1)
    $subcue_id = 71; // 71 es cuenta en soles

    $xac = 1;
    $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
    $pro_id = 1;//sistema
    $mod_id = 50;//50 es el modulo_id en ingreso, representa al MORAPAGO, será igual para EGRESO servira para cuando se anule el pago
    $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
    $egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
    $caj_id = 1; //caja id 14 ya que es CAJA AP
    $numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = $fecha;
    $oEgreso->documento_id = 9;//otros egresos
    $oEgreso->egreso_numdoc = $numdoc;
    $oEgreso->egreso_det = $egr_det;
    $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
    $oEgreso->egreso_tipcam = 0;
    $oEgreso->egreso_est = 1;
    $oEgreso->cuenta_id = $cue_id;
    $oEgreso->subcuenta_id = $subcue_id;
    $oEgreso->proveedor_id = $pro_id;
    $oEgreso->cliente_id = 0;
    $oEgreso->usuario_id = 0;
    $oEgreso->caja_id = $caj_id;
    $oEgreso->moneda_id = 1;
    $oEgreso->modulo_id = $mod_id;
    $oEgreso->egreso_modide = $modide;
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0; //si
    $oEgreso->insertar();

    //estado cuota
    $rws = $oCuotadetalle->filtrar($_POST['hdd_modid']);
    if($rws['estado']==1){
        foreach ($rws['data']as $key=>$rw){
            $est = 3;
            if($rw['tb_cuotadetalle_est']!=2)
            break;
            $est = 2;
        }
    }
    $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest',$est,'INT');//pago parcial


    $data['cuopag_id'] = $cuopag_id;
    $data['ingreso_id'] = $ingr_id;
    $data['empresa_id'] = $_SESSION['empresa_id'];
    $data['cuopag_msj'] = 'Se registró el pago correctamente.';
  }
  else
  {
    $data['cuopag_msj']='Intentelo nuevamente.';
  }
    echo json_encode($data);
  } 

if ($_POST['hdd_credito'] == "menor") { //cuotas FIJAS
    
    

    if (!empty($_POST['txt_cuopag_mon'])) {
        //$fecha = date('Y-m-d');
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $tipo_caja = 1; //las cajas a tener en cuenta es 1 CAJA y 14 CAJA AP

        $xac = 1;
        $oMorapago->tb_morapago_xac = $xac;
        $oMorapago->tb_morapago_usureg = $_SESSION['usuario_id'];
        $oMorapago->tb_morapago_usumod = $_SESSION['usuario_id'];
        $oMorapago->tb_modulo_id = $_POST['hdd_mod_id'];
        $oMorapago->tb_morapago_modid = $_POST['hdd_modid'];
        $oMorapago->tb_morapago_fec = $fecha;
        $oMorapago->tb_moneda_id = 1;
        $oMorapago->tb_morapago_mon = moneda_mysql($mon_pagado);
        $oMorapago->tb_cliente_id = $_POST['hdd_cli_id'];
        $result = $oMorapago->insertar();
        if (intval($result['estado']) == 1) {
            $cuopag_id = $result['morapago_id'];
        }
               
        //estado de cuota
        if (moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot)) {
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_morest', '2', 'INT');
            $monto = moneda_mysql($cuopag_tot);
            if ($monto > 0)
                $ingresar = 1;
        }

        if (moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot)) {
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_morest', '3', 'INT');
            $monto = moneda_mysql($mon_pagado);
            if ($monto > 0)
                $ingresar = 1;
        }

        if ($ingresar == 1) {
             
            
            $cue_id = 26; //CUENTA DE MORAS
            $subcue_id = 67; //CUOTAS menor
            $mod_id = 50; //cuota mora
            $ing_det = $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
            if (!empty($_POST['txt_cuopag_obs']))
                $ing_det .= '. Obs: ' . $_POST['txt_cuopag_obs'];

            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuopag_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ing_det;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cue_id;
            $oIngreso->subcuenta_id = $subcue_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = 1;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuopag_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_id = $result['ingreso_id'];
            }

            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
            $oIngreso->banco_id = $_POST['cmb_banco_id'];
            $oIngreso->ingreso_id = $ingr_id;

            $oIngreso->registrar_datos_deposito_banco();

            $line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha hecho la transacción: ' . $ing_det . '. El monto válido fue: S/. ' . mostrar_moneda($monto) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $line_det;
            $oLineaTiempo->insertar();
            
        }

        //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1)
            $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO	
        $pro_id = 1; //sistema
        $mod_id = 50; //50 es el modulo_id en ingreso, representa al MORAPAGO, será igual para EGRESO servira para cuando se anule el pago
        $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuopag_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc;
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($monto);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = 1;
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $oEgreso->insertar();
        
//        $data['cuopag_id'] = 0;
//    $data['cuopag_msj'] = 'hasta aki estoy entrando de manera normal ingreso == '.$cuopag_id;
//    echo json_encode($data);
//    exit();

        $data['cuopag_id'] = $cuopag_id;
        $data['ingreso_id'] = $ingr_id;
        $data['empresa_id'] = $_SESSION['empresa_id'];
        $data['cuopag_msj'] = 'Se registró el pago correctamente.';
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}
?>