<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Cobranzamora extends Conexion{
	
	function filtrar_menor($cli_id,$cre_id,$fec_act)
	{
            $fec_act2=$fec_act;
            try {
		$sql="
		SELECT *
		FROM tb_creditomenor cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 1 AND cu.tb_cuota_est IN(2,3) AND cu.tb_cuota_mor>0 AND cu.tb_cuota_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =$cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =$cre_id";

		$sql.=" GROUP BY c.tb_cliente_nom";
		$sql.=" ORDER BY tb_cuota_fec";

		$sentencia = $this->dblink->prepare($sql);
//                $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
//                $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    

	function filtrar_asiveh($cli_id,$cre_id,$fec_act)
	{
            try {
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cd.tb_cuotadetalle_mor>0 AND cd.tb_cuotadetalle_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = :tb_cliente_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = :tb_credito_id";

		$sql.=" GROUP BY cm.tb_cliente_id";
		$sql.=" ORDER BY tb_cuota_fec";
		//$sql.=" LIMIT 2";
		
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

	function filtrar_garveh($cli_id,$cre_id,$fec_act)
	{
            try{
		$sql="SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE tb_credito_xac=1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_est = 2 AND cd.tb_cuotadetalle_mor>0 AND cd.tb_cuotadetalle_morest <> 2";

    if($cli_id>0)$sql.=" AND cm.tb_cliente_id =$cli_id";
    if($cre_id>0)$sql.=" AND cm.tb_credito_id =$cre_id";

		$sql.=" GROUP BY cm.tb_cliente_id";
		$sql.=" ORDER BY tb_cuota_fec";
		
		$sentencia = $this->dblink->prepare($sql);
                //$sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
                //$sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
	function filtrar_hipo($cli_id,$cre_id,$fec_act)
	{
            try{
		$sql="SELECT *
		FROM tb_creditohipo cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE tb_credito_xac=1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_est = 2 AND cd.tb_cuotadetalle_mor>0 AND cd.tb_cuotadetalle_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = :tb_cliente_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = :tb_credito_id";

		$sql.=" GROUP BY cm.tb_cliente_id";
		$sql.=" ORDER BY tb_cuota_fec";
		
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
	
	function mostrarUnoMenor($id){
            try{
		$sql="SELECT * 
		FROM tb_creditomenor cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetch();
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
	function mostrarUnoAsiveh($id){
            try{
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetch();
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
	function mostrarUnoGarveh($id){
            try{
		$sql="SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
		
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetch();
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
                

	function mostrarUnoHipo($id){
            try{
		$sql="SELECT * 
		FROM tb_creditohipo cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetch();
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

	function mostrarDetalleAsiveh($id, $id2){
            try{
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_id=:tb_cuotadetalle_id and cd.tb_cuotadetalle_est <> 2";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_cuotadetalle_id", $id2, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
	function mostrarDetalleGarveh($id, $id2){
            try{
		$sql="SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_id=:tb_cuotadetalle_id and cd.tb_cuotadetalle_est <> 2";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_cuotadetalle_id", $id2, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

	function mostrarPagadoAsiveh($id){
            try{
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
	function mostrarPagadoGarveh($id){
            try{
		$sql="SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
                
		$sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
}
?>