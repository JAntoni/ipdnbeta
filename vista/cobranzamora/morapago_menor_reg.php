<?php
require_once('../../core/usuario_sesion.php');

require_once("../morapago/Morapago.class.php");
$oMorapago = new Morapago();

require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();

require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();



require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if($_POST['action_morapago']=="pagar_fijo")
{
	$hoy = date('d-m-Y');
	$fec_pago = $_POST['txt_cuopag_fec'];

	if($hoy != $fec_pago){
		$data['cuopag_id'] = 0;
		$data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
		echo json_encode($data);
		exit();
	}


	if(!empty($_POST['txt_cuopag_mon']))
	{
		//$fecha = date('Y-m-d');
		$fecha = fecha_mysql($_POST['txt_cuopag_fec']);

                    $xac=1;		
                    $oMorapago->tb_morapago_xac=$xac; 
                    $oMorapago->tb_morapago_usureg=$_SESSION['usuario_id'];
                    $oMorapago->tb_morapago_usumod=$_SESSION['usuario_id']; 
                    $oMorapago->tb_modulo_id=$_POST['hdd_mod_id'];
                    $oMorapago->tb_morapago_modid=$_POST['hdd_modid']; 
                    $oMorapago->tb_morapago_fec=$fecha;
                    $oMorapago->tb_moneda_id=1; 
                    $oMorapago->tb_morapago_mon=moneda_mysql($_POST['txt_cuopag_mon']);
                    $oMorapago->tb_cliente_id=$_POST['hdd_cli_id'];
                    $result=$oMorapago->insertar();
                        if(intval($result['estado']) == 1){
                            $cuopag_id = $result['morapago_id'];
                    }

		//estado de cuota
		if(moneda_mysql($_POST['txt_cuopag_mon'])==moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','2','INT');
			//registro de mora
			//$oCuota->modificar_campo($_POST['hdd_cuo_id'],'mor',moneda_mysql($_POST['txt_cuopag_mor']));
			//if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'moraut','1');

			$monto=moneda_mysql($_POST['txt_cuopag_tot']);
			if($monto>0)$ingresar=1;
		}

		if(moneda_mysql($_POST['txt_cuopag_mon'])<moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','3','INT');
			
			$monto=moneda_mysql($_POST['txt_cuopag_mon']);
			if($monto>0)$ingresar=1;
		}

		/*if(moneda_mysql($_POST['txt_cuopag_mon'])>moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'morest','2');
			//registro de mora
			//$oCuota->modificar_campo($_POST['hdd_cuo_id'],'mor',moneda_mysql($_POST['txt_cuopag_mor']));
			if($_POST['chk_mor_aut']==1)$oCuotadetalle->modificar_campo($_POST['hdd_cuo_id'],'moraut','1');
			
			$monto=moneda_mysql($_POST['txt_cuopag_tot']);
			if($monto>0)$ingresar=1;

			$abono=moneda_mysql($_POST['txt_cuopag_mon'])-moneda_mysql($_POST['txt_cuopag_tot']);
			if($abono>0)$abonar=1;
		}*/
		

		if($ingresar==1)
		{
		//registro de ingreso

			$cue_id = 26; //CUENTA DE MORAS
			$subcue_id = 67; //SUB CUENTA DE MORAS
			$mod_id = 50; //cuota pago
			$ing_det = $_POST['hdd_pag_det'];
			$numdoc = $_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;
                        
                        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_fec = $fecha; 
                        $oIngreso->documento_id = 8; 
                        $oIngreso->ingreso_numdoc = $numdoc;
                        $oIngreso->ingreso_det = $ing_det; 
                        $oIngreso->ingreso_imp = moneda_mysql($monto);
                        $oIngreso->cuenta_id = $cue_id;
                        $oIngreso->subcuenta_id = $subcue_id; 
                        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                        $oIngreso->caja_id = 1; 
                        $oIngreso->moneda_id = 1;
                        //valores que pueden ser cambiantes según requerimiento de ingreso
                        $oIngreso->modulo_id = $mod_id; 
                        $oIngreso->ingreso_modide = $cuopag_id; 
                        $oIngreso->empresa_id = $_SESSION['empresa_id'];
                        $oIngreso->ingreso_fecdep = ''; 
                        $oIngreso->ingreso_numope = ''; 
                        $oIngreso->ingreso_mondep = 0; 
                        $oIngreso->ingreso_comi = 0; 
                        $oIngreso->cuentadeposito_id = 0; 
                        $oIngreso->banco_id = 0; 
                        $oIngreso->ingreso_ap = 0; 
                        $oIngreso->ingreso_detex = '';

                        $result=$oIngreso->insertar();
                            if(intval($result['estado']) == 1){
                                $ingr_id = $result['ingreso_id'];
                            }

			$line_det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '. mostrar_moneda($monto).' && <b>'.date('d-m-Y h:i a').'</b>';
                        $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                        $oLineaTiempo->tb_lineatiempo_det=$line_det;
                        $oLineaTiempo->insertar();
		}

		$data['cuopag_id']=$cuopag_id;
		$data['ingreso_id'] = $ingr_id;
		$data['empresa_id'] = $_SESSION['empresa_id'];
		$data['cuopag_msj']='Se registró el pago correctamente.';
	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}
//        echo 'estoy entrando al sistemaa'; exit();

if($_POST['action_morapago']=="pagar_libre")
{
	$hoy = date('d-m-Y');
	$fec_pago = $_POST['txt_cuopag_fec'];

	if($hoy != $fec_pago){
		$data['cuopag_id'] = 0;
		$data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
		echo json_encode($data);
		exit();
	}
	
	if(!empty($_POST['txt_cuopag_mon']))
	{
		//$fecha = date('Y-m-d');
		$fecha = fecha_mysql($_POST['txt_cuopag_fec']);

                $xac=1;		
                $oMorapago->tb_morapago_xac=$xac; 
                $oMorapago->tb_morapago_usureg=$_SESSION['usuario_id'];
                $oMorapago->tb_morapago_usumod=$_SESSION['usuario_id']; 
                $oMorapago->tb_modulo_id=$_POST['hdd_mod_id'];
                $oMorapago->tb_morapago_modid=$_POST['hdd_modid']; 
                $oMorapago->tb_morapago_fec=$fecha;
                $oMorapago->tb_moneda_id=1; 
                $oMorapago->tb_morapago_mon=moneda_mysql($_POST['txt_cuopag_mon']);
                $oMorapago->tb_cliente_id=$_POST['hdd_cli_id'];
                $result=$oMorapago->insertar();
                    if(intval($result['estado']) == 1){
                        $cuopag_id = $result['morapago_id'];
                }        

		//estado de cuota
		if(moneda_mysql($_POST['txt_cuopag_mon'])==moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','2','INT');
			//registro de mora
			//$oCuota->modificar_campo($_POST['hdd_cuo_id'],'mor',moneda_mysql($_POST['txt_cuopag_mor']));
			//if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'moraut','1');

			$monto=moneda_mysql($_POST['txt_cuopag_tot']);
			if($monto>0)$ingresar=1;
		}

		if(moneda_mysql($_POST['txt_cuopag_mon'])<moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','3','INT');
			
			$monto=moneda_mysql($_POST['txt_cuopag_mon']);
			if($monto>0)$ingresar=1;
		}

		/*if(moneda_mysql($_POST['txt_cuopag_mon'])>moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'morest','2');
			//registro de mora
			//$oCuota->modificar_campo($_POST['hdd_cuo_id'],'mor',moneda_mysql($_POST['txt_cuopag_mor']));
			if($_POST['chk_mor_aut']==1)$oCuotadetalle->modificar_campo($_POST['hdd_cuo_id'],'moraut','1');
			
			$monto=moneda_mysql($_POST['txt_cuopag_tot']);
			if($monto>0)$ingresar=1;

			$abono=moneda_mysql($_POST['txt_cuopag_mon'])-moneda_mysql($_POST['txt_cuopag_tot']);
			if($abono>0)$abonar=1;
		}*/
		

		if($ingresar==1)
		{
		//registro de ingreso

			$cue_id=26; //CANCEL CUOTAS
			$subcue_id=67; //CUOTAS menor
			$mod_id=50; //cuota pago
			$ing_det=$_POST['hdd_pag_det'];
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                            $oIngreso->ingreso_fec = $fecha; 
                            $oIngreso->documento_id = 8; 
                            $oIngreso->ingreso_numdoc = $numdoc;
                            $oIngreso->ingreso_det = $ing_det; 
                            $oIngreso->ingreso_imp = moneda_mysql($monto);
                            $oIngreso->cuenta_id = $cue_id;
                            $oIngreso->subcuenta_id = $subcue_id; 
                            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                            $oIngreso->caja_id = 1; 
                            $oIngreso->moneda_id = 1;
                            //valores que pueden ser cambiantes según requerimiento de ingreso
                            $oIngreso->modulo_id = $mod_id; 
                            $oIngreso->ingreso_modide = $cuopag_id; 
                            $oIngreso->empresa_id = $_SESSION['empresa_id'];
                            $oIngreso->ingreso_fecdep = ''; 
                            $oIngreso->ingreso_numope = ''; 
                            $oIngreso->ingreso_mondep = 0; 
                            $oIngreso->ingreso_comi = 0; 
                            $oIngreso->cuentadeposito_id = 0; 
                            $oIngreso->banco_id = 0; 
                            $oIngreso->ingreso_ap = 0; 
                            $oIngreso->ingreso_detex = '';

                            $result=$oIngreso->insertar();
                                if(intval($result['estado']) == 1){
                                    $ingr_id = $result['ingreso_id'];
                            }

			$line_det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '. mostrar_moneda($monto).' && <b>'.date('d-m-Y h:i a').'</b>';
                        $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                        $oLineaTiempo->tb_lineatiempo_det=$line_det;
                        $oLineaTiempo->insertar();
		}

		$data['cuopag_id']=$cuopag_id;
		$data['ingreso_id'] = $ingr_id;
		$data['empresa_id'] = $_SESSION['empresa_id'];
		$data['cuopag_msj']='Se registró el pago correctamente.';
	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}

?>