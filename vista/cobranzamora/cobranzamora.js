/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $("#txt_fil_cli").focus();
    $( "#txt_fil_cli" ).autocomplete({
        minLength: 1,
        source: function(request, response){
          $.getJSON(
                  VISTA_URL+"cliente/cliente_autocomplete.php",
                  {term: request.term}, //
                  response
          );
        },
        select: function(event, ui){
          $('#hdd_fil_cli_id').val(ui.item.cliente_id);
          $('#txt_fil_cli').val(ui.item.cliente_nom);
          cobranzamora_tabla();
          event.preventDefault();
          $('#txt_fil_cli').focus();
        }
    }); 
});


function cobranzamora_tabla(){ 
//    console.log("estoy entrando en el sistem hasta aki");return false;
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/vencimiento_tabla.php",
    async:true,
    dataType: "html",                      
    data: $("#vencimiento_filtro").serialize(),
    beforeSend: function() {
      $('#msj_cobranzamora_tabla').html("Cargando datos...");
      $('#msj_cobranzamora_tabla').show(100);
      $('#div_cobranzamora_tabla').addClass("ui-state-disabled");
    },
    success: function(html){
      $('#div_cobranzamora_tabla').html(html);
    },
    complete: function(html){     
//            console.log(html);
      $('#div_cobranzamora_tabla').removeClass("ui-state-disabled");
      $('#msj_cobranzamora_tabla').hide(100);
    }
  });    
}




function creditomenor_pagos(creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditomenor_id,
      hdd_form:'vencimiento'
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditomenor_pagos').html(data);
        $('#modal_creditomenor_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditomenor_pagos', 95);
        modal_height_auto('modal_creditomenor_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditomenor_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function creditomenor_form(usuario_act, creditomenor_id){
  $.ajax({
        type: "POST",
        url: VISTA_URL+"creditomenor/creditomenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditomenor_id: creditomenor_id,
            vista: 'creditomenor'
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
                $('#div_modal_creditomenor_form').html(data);
                $('#modal_registro_creditomenor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if(usuario_act == 'L' || usuario_act == 'E')
                  form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_registro_creditomenor', 95);
                modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
              //llamar al formulario de solicitar permiso
              var modulo = 'creditomenor';
              var div = 'div_modal_creditomenor_form';
              permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){

        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
            console.log(data.responseText);
        }
	});
}


function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}


function morapago_menor_form(act,idf){

    Swal.fire({
      title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
      showDenyButton: true,
      denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
      confirmButtonColor: '#3b5998', 
      denyButtonColor: '#21ba45',
    }).then((result) => {
  
      if (result.isConfirmed) {
        Swal.fire(
          '¡Has elegido pagar en Oficina!',
          '',
          'success'
        )
        cuotamorapago_menor_form(act,idf);
      } else if (result.isDenied) {
        Swal.fire(
          '¡Has elegido pagar en Banco!',
          '',
          'success'
        )
        morapago_banco('menor', idf);

      }
    });
}


function morapago_banco(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_banco.php",
    async:true,
    dataType: "html",                    
    data: ({
      credito: cred,
      cuota_id: idf,
      vista: 'vencimiento_tabla'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function cuotamorapago_menor_form(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_menor_form.php",
    async:true,
    dataType: "html",                    
    data: ({
        action: cred,
        cuo_id:	idf,
        vista:	'vencimiento_tabla'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function cuotamorapago_garveh_form(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_garveh_form.php",
    async:true,
    dataType: "html",                    
    data: ({
        action: cred,
        cuodet_id:	idf,
        vista:	'vencimiento_tabla'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function creditogarveh_form(usuario_act, creditogarveh_id, proceso_id=0) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarveh/creditogarveh_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          creditogarveh_id: creditogarveh_id,
          proceso_id: proceso_id,
          vista: 'credito_tabla'
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
//            console.log(data);
          $('#modal_mensaje').modal('hide');
          if (data != 'sin_datos') {
              $('#div_modal_creditogarveh_form').html(data);
              $('#modal_registro_creditogarveh').modal('show');

              //desabilitar elementos del form si es L (LEER)
              if (usuario_act == 'L'){
                  form_desabilitar_elementos('for_cre');
              } //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_creditogarveh', 'limpiar'); //funcion encontrada en public/js/generales.js
              modal_width_auto('modal_registro_creditogarveh',80);
              modal_height_auto('modal_registro_creditogarveh');
          } else {
              //llamar al formulario de solicitar permiso
              var modulo = 'creditogarveh';
              var div = 'div_modal_creditogarveh_form';
              permiso_solicitud(usuario_act, creditogarveh_id, modulo, div); //funcion ubicada en public/js/permiso.js
          }
      },
      complete: function (data) {
//console.log(data);
      },
      error: function (data) {
          $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
          $('#overlay_modal_mensaje').removeClass('overlay').empty();
          $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
          console.log(data.responseText);
      }
  });
}

function creditogarveh_pagos(creditogarveh_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarveh/creditogarveh_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditogarveh_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditogarveh_pagos').html(data);
        $('#modal_creditogarveh_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditogarveh_pagos', 95);
        modal_height_auto('modal_creditogarveh_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditogarveh_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function morapago_garveh_form(act,idf){

  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      Swal.fire(
        '¡Has elegido pagar en Oficina!',
        '',
        'success'
      )
      cuotamorapago_garveh_form(act,idf);
    } else if (result.isDenied) {
      Swal.fire(
        '¡Has elegido pagar en Banco!',
        '',
        'success'
      )
      morapago_banco('garveh', idf);

    }
  });
}

function mora_eliminar(modulo_id, mora_id){
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Anular',
    content: '¿Desea Anular esta mora?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"cobranzamora/mora_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "eliminar",
            modulo_id: modulo_id,
            mora_id: mora_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Anulando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              alerta_success('Bien', data.mensaje); //en generales.js
              cobranzamora_tabla()
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}