<?php

require_once('../../core/usuario_sesion.php');

require_once("../morapago/Morapago.class.php");
$oMorapago = new Morapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../cajacambio/Cajacambio.class.php');
$oCajacambio = new Cajacambio();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if($_POST['action_morapago']=="pagar_fijo" || $_POST['action_morapago']=="pagar_libre")
{
	$hoy = date('d-m-Y');
	$fec_pago = $_POST['txt_cuopag_fec'];

	if($hoy != $fec_pago){
		$data['morpag_id'] = 0;
		$data['morpag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
		echo json_encode($data);
		exit();
	}
	
	if(!empty($_POST['txt_cuopag_mon']))
	{
        $mon_pagado=moneda_mysql($_POST['txt_cuopag_mon']);
		//$fecha = date('Y-m-d');
		$fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $moneda_credito = 'S/.';
        if ($_POST['hdd_mon_id'] == 2) {
            $moneda_credito = 'US$';
        }

        $moneda_seleccionada = 'S/.';
        if ($_POST['cmb_mon_id'] == 2) {
            $moneda_seleccionada = 'US$';
        }

        $xac=1;

        $oMorapago->tb_morapago_xac=$xac;
        $oMorapago->tb_morapago_usureg=$_SESSION['usuario_id'];
        $oMorapago->tb_morapago_usumod=$_SESSION['usuario_id'];
        $oMorapago->tb_modulo_id=$_POST['hdd_mod_id'];
        $oMorapago->tb_morapago_modid=$_POST['hdd_modid'];
        $oMorapago->tb_morapago_fec=$fecha;
        $oMorapago->tb_moneda_id=1;
        $oMorapago->tb_morapago_mon=$mon_pagado;
        $oMorapago->tb_cliente_id=$_POST['hdd_cli_id'];

        $result=$oMorapago->insertar();
        if(intval($result['estado']) == 1){
            $morpag_id = $result['morapago_id'];
        }

        
        $cuopag_tot=moneda_mysql($_POST['txt_cuopag_tot']);

		//estado de cuota detalle
        if(moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot))
        {
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','2','INT');
            $monto = moneda_mysql($mon_pagado);
            if($monto > 0) $ingresar = 1;
        }
    
        if(moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot))
        {
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','3','INT');
            $monto = moneda_mysql($mon_pagado);
            if($monto > 0) $ingresar = 1;
        }
		

		if($ingresar==1)
		{
		//registro de ingreso
			$cue_id = 26; //CUENTA DE MORAS
			$subcue_id = 69; //SUB CUENTA DE garveh en Mantenimiento->Caja->Cuentas (Parte de Moras)
			$mod_id = 50; //cuota mora
			$ing_det = $_POST['hdd_pag_det'];
			$numdoc = $_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$morpag_id;
            $tipo_caja = 1;

			$oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ing_det;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cue_id;
            $oIngreso->subcuenta_id = $subcue_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = $tipo_caja;
            $oIngreso->moneda_id = 1;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $morpag_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';

            $result=$oIngreso->insertar();
            if(intval($result['estado']) == 1){
                $ingr_id = $result['ingreso_id'];
            }

			$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '.formato_moneda($monto).' && <b>'.date('d-m-Y h:i a').'</b>';
    	    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det=$line_det;
            $oLineaTiempo->insertar();
		}

        if ($_POST['hdd_mon_id'] != $_POST['cmb_mon_id'] and $_POST['txt_cuopag_moncam'] > 0 && $paga_con_ap != 1) {
            //cambio
            $xac = 1;
            if ($_POST['hdd_mon_id'] == 2 and $_POST['cmb_mon_id'] == 1) {
                $descripcion = "DOLARES A SOLES | " . $_POST['hdd_pag_det'];
                $morapago_historial .= 'El pago de mora se hizo con moneda diferente al crédito por eso en en caja hay un cambio de DOLARES A SOLES. Se ingresó ' . $moneda_seleccionada . ' ' . mostrar_moneda($_POST['txt_cuopag_moncam']) . ' y con cambio de moneda el monto pagado fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto_pagado) . ' ||&& ';
            }

            if ($_POST['hdd_mon_id'] == 1 and $_POST['cmb_mon_id'] == 2) {
                $descripcion = "SOLES A DOLARES | " . $_POST['hdd_pag_det'];
                $morapago_historial .= 'El pago de mora se hizo con moneda diferente al crédito por eso en en caja hay un cambio de SOLES A DOLARES. Se ingresó ' . $moneda_seleccionada . ' ' . mostrar_moneda($_POST['txt_cuopag_moncam']) . ' y con cambio de moneda el monto pagado fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto_pagado) . ' ||&& ';
            }

            $caja_id = 1;
            $mod_id = 30;

            $oCajacambio->tb_cajacambio_usureg = $_SESSION['usuario_id'];
            $oCajacambio->tb_cajacambio_usumod = $_SESSION['usuario_id'];
            $oCajacambio->tb_cajacambio_xac = $xac;
            $oCajacambio->tb_caja_id = $caja_id;
            $oCajacambio->tb_moneda_id = $_POST['hdd_mon_id'];
            $oCajacambio->tb_cajacambio_fec = $fecha;
            $oCajacambio->tb_cajacambio_imp = moneda_mysql($mon_pagado);
            $oCajacambio->tb_cajacambio_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
            $oCajacambio->tb_cajacambio_cam = moneda_mysql($_POST['txt_cuopag_moncam']);
            $oCajacambio->tb_cajacambio_des = $descripcion;
            $oCajacambio->tb_modulo_id = $mod_id;
            $oCajacambio->tb_cajacambio_modid = $morpag_id;
            $oCajacambio->tb_empresa_id = $_SESSION['empresa_id'];

            $result = $oCajacambio->insertar();
            if (intval($result['estado']) == 1) {
                $cajcam_id = $result['cajacambio_id'];
            }
            $result = NULL;

            $egreso_detalle = 'CAMBIO DE MONEDA: ' . $descripcion;
            $cuenta_id = 23;
            $subcuenta_id = 0;
            $doc_id = 9;
            $mod_id = 60;
            $numerodocumento = $mod_id . '-' . $cajcam_id;
            $egr_est = 1;
            $pro_id = 1;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = $egr_est;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = $_POST['hdd_mon_id'];
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $cajcam_id;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

            $ingreso_detalle = 'CAMBIO DE MONEDA: ' . $descripcion;
            $doc_id = 8;
            $mod_id = 60;
            $numerodocumento = $mod_id . '-' . $cajcam_id;
            $cuenta_id = 24;
            $subcuenta_id = 0;
            $ing_est = '1';
            //$cli_id=1;
            $cli_id = $_POST['hdd_cli_id'];

            if ($_POST['hdd_mon_id'] == '1')
                $mon_id = '2';
            if ($_POST['hdd_mon_id'] == '2')
                $mon_id = '1';

            //registro de ENTRADA
            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_moncam']);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = $cli_id;
            $oIngreso->caja_id = $caja_id;
            $oIngreso->moneda_id = $mon_id;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cajcam_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $oIngreso->insertar();

            // Registrar historial de la acción
            $oHist->setTbHistUsureg($_SESSION['usuario_id']);
            $oHist->setTbHistNomTabla('tb_morapago');
            $oHist->setTbHistRegmodid($morpag_id);
            $oHist->setTbHistDet($morapago_historial);
            $oHist->insertar();
        }

		//estado cuota
        $rws = $oCuotadetalle->filtrar($_POST['hdd_modid']);
        if($rws['estado']==1){
            foreach ($rws['data']as $key=>$rw){
                $est = 3;
                if($rw['tb_cuotadetalle_est']!=2)
                break;
                $est = 2;
            }
        }
        $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest',$est,'INT');//pago parcial
    
    
        $data['morpag_id'] = $morpag_id;
        $data['ingreso_id'] = $ingr_id;
        $data['empresa_id'] = $_SESSION['empresa_id'];
        $data['morpag_msj'] = 'Se registró el pago correctamente.';
	}
	else
	{
		$data['morpag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}

?>