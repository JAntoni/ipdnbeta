<?php
	session_name("ipdnsac");
  session_start();
	require_once('../cobranzamora/Cobranzamora.class.php');
	require_once('../cuota/Cuota.class.php');
	require_once('../cuota/Cuotadetalle.class.php');
	
	$oCobranzamora = new Cobranzamora();
	$oCuota = new Cuota();
	$oCuotadetalle = new Cuotadetalle();

	require_once('../funciones/funciones.php');
	require_once('../funciones/fechas.php');

	$action = $_POST['action'];
	$mora_tipo = intval($_POST['modulo_id']);
	$mora_id = intval($_POST['mora_id']);
  	//$morapago_modid = intval($_POST['morapago_modid']);
  	$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

  	if($action == 'eliminar' && $mora_id > 0){
		try{
			if($usuariogrupo_id != 2){
				$data['estado'] = 0;
				$data['mensaje'] = 'Solo los usuarios administradores pueden anular los pagos, comunícate con ellos por favor.';
				echo json_encode($data);
				exit();
			}

			$fecha_hoy = date('d-m-Y');


			$cuota_id = 0; //para poder obtener información del crédito

			if ($mora_tipo == 2) {
				$cuotadetalle_est = 1; //1 por cobrar, 2 pagado, 3 pago parcial
		
				$cuotadetalle_id = $mora_id;
				$oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_mor', 0, 'INT');
				$oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_morest', $cuotadetalle_est, 'INT');
		
				$result = $oCuotadetalle->mostrarUno($cuotadetalle_id);
				if ($result['estado'] == 1) {
					$cuota_id = $result['data']['tb_cuota_id'];
				}
				$result = NULL;

				$oCuota->modificar_campo($cuota_id, 'tb_cuota_mor', 0, 'INT');
				$oCuota->modificar_campo($cuota_id, 'tb_cuota_morest', 1, 'INT');
			}

			if ($mora_tipo == 1) {
				$cuota_est = 1; //1 por cobrar, 2 pagado, 3 pago parcial
				$cuota_id = $mora_id;

				$oCuota->modificar_campo($cuota_id, 'tb_cuota_mor', 0, 'INT');
				$oCuota->modificar_campo($cuota_id, 'tb_cuota_morest', 1, 'INT');
			}

			$data['estado'] = 1;
			$data['mensaje'] = 'Mora eliminado correctamente';
		}catch (Exception $e) {
			// Actualiza el mensaje con el error
			$response['mensaje'] = 'Error: ' . $e->getMessage();
			$response['estado'] = 0;
		}
		echo json_encode($data);
  	}
  /*if($action == 'eliminar'){
  	if($usuariogrupo_id != 2){
	    $data['estado'] = 0;
	    $data['mensaje'] = 'Solo los usuarios administradores pueden anular los pagos, comunícate con ellos por favor.';
	    echo json_encode($data);
	    exit();
	  }

	  $result = $oMorapago->mostrar_morapagos_modulo($modulo_id, $morapago_modid);
	  	if($result['estado'] == 1){
	  		foreach ($result['data'] as $key => $value) {
	  			$morapago_id = $value['tb_morapago_id'];
	  			$oMorapago->modificar_campo($morapago_id, 'tb_morapago_xac', 0, 'INT');

	  			$ingreso_valor = 0; $ingreso_modulo_id = 50; $ingreso_modide = $value['tb_morapago_id'];
  				$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $ingreso_modulo_id, $ingreso_modide);
	  		}
	  	}
	  $result = NULL;

	  if($modulo_id == 1){
	  	//la mora es de una cuota, C-MENOR
	  	$cuota_id = $morapago_modid;
	  	$oCuota->modificar_campo($cuota_id, 'tb_cuota_mor', 0, 'INT');
	  }
	  if($modulo_id == 2){
	  	//la mora es de una cuotadetalle, TODOS LOS CREDITOS
	  	$cuotadetalle_id = $morapago_modid;
	  	$oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_mor', 0, 'INT');
	  }

	  $data['estado'] = 1;
	  $data['mensaje'] = 'La mora se ha eliminado correctamente';
	  echo json_encode($data);
  }*/

?>