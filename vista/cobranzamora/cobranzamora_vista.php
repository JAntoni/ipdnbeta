<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Créditos</a></li>
            <li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-primary">
            <div class="box-header">
                    <button class="btn btn-primary btn-sm" onclick=""><i class="fa fa-refresh"></i> Actualizar</button>
            </div>
<!--        </div>
        <div class="box box-default">-->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtrar por</label>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                              <?php include 'vencimiento_filtro.php';?>
                            </div>
                            <div id="msj_vencimiento" class="ui-state-highlight ui-corner-all" style="width:auto; float:left; padding:2px; display:none">
                            
                            </div>
                            <div id="msj_cobranzamora_tabla" class="ui-state-highlight ui-corner-all" style="width:auto; float:left; padding:2px; display:none">

                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="vencimiento_mensaje_tbl" style="display: none;">
                  <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Garantías...</h4>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- Input para guardar el valor ingresado en el search de la tabla-->
                        <input type="hidden" id="hdd_datatable_fil">
                        <div id="div_cobranzamora_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                <?php //require_once('cobranzamora_tabla.php');?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_vencimiento_form">
                    <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            
           
            <div id="div_modal_vencimiento_menor_form">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            
            <div id="div_modal_creditomenor_pagos"></div>
            <div id="div_modal_creditomenor_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE CREDITO-->
            </div>
            <div id="div_modal_creditogarveh_form">
				<!-- INCLUIMOS EL MODAL PARA LECTURA DE CREDITO-->
            </div>
            <div id="div_modal_creditogarveh_pagos">
				<!-- INCLUIMOS EL MODAL PARA LECTURA DE HISTORIAL CREDITO-->
            </div>
            <div id="div_modal_carousel">
                <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
            </div>

            <div id="div_modal_cobranzamora">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DE PAGOS-->
                <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
                <?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
        </div>
    </section>
	<!-- /.content -->
</div>
