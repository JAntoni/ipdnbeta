<?php 

if(defined('APP_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once (VISTA_URL.'cuota/Cuota.class.php');
    require_once (VISTA_URL.'cuota/Cuotadetalle.class.php');
    require_once (VISTA_URL.'morapago/Morapago.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
}
else{
    require_once('../../core/usuario_sesion.php');
    require_once ("../cuota/Cuota.class.php");
    require_once ("../cuota/Cuotadetalle.class.php");
    require_once ("../morapago/Morapago.class.php");
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}

$oCuota = new Cuota();
$oCuotadetalle = new Cuotadetalle();
$oMorapago = new Morapago();

	
$credito = $_POST['credito'];
$cuota_id = $_POST['cuota_id'];


//solo para creditos mayores se consulta a cuotadetalle, para cuota menor es diferente
if($credito != 'menor'){
	$dts=$oCuotadetalle->mostrarUno($cuota_id);
	if($dts['estado']==1){
            $cuo_id=$dts['data']['tb_cuota_id'];
            $fecha = $dts['data']['tb_cuotadetalle_fec'];//para verificar si hay un acuerdopago en esta fecha
            $cuodet_fec=mostrar_fecha($dts['data']['tb_cuotadetalle_fec']);
            $mon_id=$dts['data']['tb_moneda_id'];
            $mon_nom=$dts['data']['tb_moneda_nom'];
            $cuodet_num=$dts['data']['tb_cuotadetalle_num'];
            $cuodet_cuo=$dts['data']['tb_cuotadetalle_mor'];
	}
        $dts=NULL;
	$moneda = 'S/';

	$dts=$oCuota->mostrarUno($cuo_id);
	if($dts['estado']){
            $cre_id=$dts['data']['tb_credito_id'];
            $cuo_num=$dts['data']['tb_cuota_num'];
            $cuo_persubcuo=$dts['data']['tb_cuota_persubcuo'];
            $cuo_int=$dts['data']['tb_cuota_int'];
	}
        $dts=NULL;
	$pagos_cuotadetalle = 0;
	$mod_id = 2;//cuotadetalle

	$dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
	  if($dts3['estado']){
	    foreach($dts3['data']as $key=>$dt3)
	    {
	      $pagos_cuotadetalle += $dt3['tb_ingreso_imp'];
	    }
	  }
  $dts3=NULL;
	$pagos = $pagos_cuotadetalle;
	$pagos_parciales = mostrar_moneda($pagos);
	$monto_pagar = mostrar_moneda($cuodet_cuo-$pagos);
}

if($credito == 'asiveh'){
	require_once ("../creditoasiveh/Creditoasiveh.class.php");
	$oCredito = new Creditoasiveh();
	
	$nombre_credito  = 'ASISTENCIA VEHICULAR - CUOTA FIJA';

	$dts=$oCredito->mostrarUno($cre_id);
	if($dts['estado']==1){
            $cuotip_id = 4; //para asiveh el tipo de cuotas siempre es fijas
            $cre_numcuo = $dts['data']['tb_credito_numcuo'];
            $cre_tip1 = $dts['data']['tb_credito_tip1'];
            $cli_id=$dts['data']['tb_cliente_id'];
            $cli_doc = $dts['data']['tb_cliente_doc'];
            $cli_nom=$dts['data']['tb_cliente_nom'];
            $cliente=$cli_nom.' | '.$cli_doc;
	}
        $dts=NULL;
	$codigo = 'CAV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;
	$cuota =$cuo_num.'/'.$cre_numcuo.' ('.$cuodet_num.'/'.$cuo_persubcuo.')';

	$pag_det = "PAGO DE MORA CRÉDITO $codigo N° CUOTA: $cuota";


	if($cuo_num==$cre_numcuo and $cuodet_num==$cuo_persubcuo)
	{
		$vMax=moneda_mysql($monto_pagar);
	}
	else
	{
		$vMax='99999.00';
	}
}

if($credito == 'garveh') {
	require_once ("../creditogarveh/Creditogarveh.class.php");
	$oCredito = new Creditogarveh();

	$url_mora = 'cuotapago_garveh_reg.php';

	$dts=$oCredito->mostrarUno($cre_id);
    if($dts['estado']==1){
        $cuotip_id=$dts['data']['tb_cuotatipo_id']; //tipo de cuotas 4 cuotas fijas, 3 cuotas libres
        $cuot_subper = $dts['data']['tb_cuotasubperiodo_id'];
        $cuotip_nom=$dts['data']['tb_cuotatipo_nom'];
        $cre_int = $dts['data']['tb_credito_int'];
        $cre_tip = $dts['data']['tb_credito_tip']; //tipo 1 normal, 2 adenda y 3 acuerdo de pago
        $cre_numcuo = $dts['data']['tb_credito_numcuo'];
        $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
        $cli_id=$dts['data']['tb_cliente_id'];
        $cli_tip=$dts['data']['tb_cliente_tip'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom=$dts['data']['tb_cliente_nom'];
        $cliente=$cli_nom.' | '.$cli_doc;
  }
  $dts=NULL;

  $codigo='CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;

  $nom_subper = 'MES';

  if($cuot_subper == 2)
    $nom_subper = 'QUINCENA';

  if($cuot_subper == 3)
    $nom_subper = 'SEMANA';

  //numero de cuotas
  if($cuotip_id==4)
    $cuota = $nom_subper.' '.$cuodet_num.'/'.$cuo_persubcuo.' ('.$cuo_num.'/'.$cre_numcuomax.')';

  if($cuotip_id==3)
    $cuota = $cuo_num.'/'.$cre_numcuomax;

  $pag_det="PAGO DE MORA CRÉDITO $codigo N° CUOTA: $cuota";


  //pagos parciales
  $pagos_cuotadetalle=0;
  $mod_id=2;//cuotadetalle

  $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
    if($dts3['estado']==1){
        foreach($dts3['data']as $key=>$dt3)
      {
        $pagos_cuotadetalle+=$dt3['tb_ingreso_imp'];
      }
    }
   $dts3=NULL;

  $pagos=$pagos_cuotadetalle;
  $pagos_parciales=mostrar_moneda($pagos);
  $monto_pagar=mostrar_moneda($cuodet_cuo-$pagos);

  $pago_minimo = mostrar_moneda($cuo_int - $pagos);
  if($pago_minimo < 0)
    $pago_minimo = 0;

  $nombre_credito  = '';
  if($cuotip_id==4)//fijo
  {
  	$nombre_credito  = 'GARANTÍA VEHICULAR - CUOTA FIJA';
    if($cuo_num==$cre_numcuo)
    {
      $vMax=moneda_mysql($monto_pagar);
    }
    else
    {
      $vMax='99999.00';
    }
    
  }

  if($cuotip_id==3)//libre
  {
  	$nombre_credito  = 'GARANTÍA VEHICULAR - CUOTA LIBRE';

    $vMax=moneda_mysql($monto_pagar);
  }
}

if($credito == 'hipo'){
	require_once ("../creditohipo/Creditohipo.class.php");
	$oCredito = new Creditohipo();

	$url_mora = 'cuotapago_hipo_reg.php';

	$dts=$oCredito->mostrarUno($cre_id);
    if($dts['estado']==1){
        $cuotip_id=$dts['data']['tb_cuotatipo_id'];
        $cuot_subper = $dts['data']['tb_cuotasubperiodo_id'];
        $cuotip_nom=$dts['data']['tb_cuotatipo_nom'];
        $cre_int = $dts['data']['tb_credito_int'];
        $cre_tip = $dts['data']['tb_credito_tip']; //tipo 1 normal, 2 adenda y 3 acuerdo de pago
        $cre_numcuo = $dts['data']['tb_credito_numcuo'];
        $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
        $cli_id=$dts['data']['tb_cliente_id'];
        $cli_tip=$dts['data']['tb_cliente_tip'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom=$dts['data']['tb_cliente_nom'];
        $cliente=$cli_nom.' | '.$cli_doc;
  }
  $dts=NULL;

  $codigo='CH-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;

  $nom_subper = 'MES';

  if($cuot_subper == 2)
    $nom_subper = 'QUINCENA';

  if($cuot_subper == 3)
    $nom_subper = 'SEMANA';

  //numero de cuotas
  if($cuotip_id==4)
    $cuota = $nom_subper.' '.$cuodet_num.'/'.$cuo_persubcuo.' ('.$cuo_num.'/'.$cre_numcuo.')';

  if($cuotip_id==3)
    $cuota = $cuo_num.'/'.$cre_numcuomax;

  $pag_det="PAGO DE MORA DE CRÉDITO $codigo N° CUOTA: $cuota";


  //pagos parciales
  $pagos_cuotadetalle=0;
  $mod_id=2;//cuotadetalle

  $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
   if($dts['estado']==1){
       foreach($dts['data'] as $key=>$dt3)
      {
        $pagos_cuotadetalle+=$dt3['tb_ingreso_imp'];
      }
    }
      $dts=NULL;

  $pagos=$pagos_cuotadetalle;
  $pagos_parciales=mostrar_moneda($pagos);
  $monto_pagar=mostrar_moneda($cuodet_cuo-$pagos);

  $pago_minimo = mostrar_moneda($cuo_int - $pagos);
  if($pago_minimo < 0)
    $pago_minimo = 0;

  $nombre_credito  = '';
  if($cuotip_id==4)//fijo
  {
  	$nombre_credito  = 'CRÉDITO HIPOTECARIO - CUOTA FIJA';

    if($cuo_num==$cre_numcuo)
    {
      $vMax=moneda_mysql($monto_pagar);
    }
    else
    {
      $vMax='99999.00';
    }
    
  }

  if($cuotip_id==3)//libre
  {
  	$nombre_credito  = 'CRÉDITO HIPOTECARIO - CUOTA FIJA';
    $vMax=moneda_mysql($monto_pagar);   
  }
}

if($credito == 'menor'){
	require_once ("../creditomenor/Creditomenor.class.php");
	$oCredito = new Creditomenor();

	$dts=$oCuota->mostrarUno($cuota_id);
	if($dts['estado']==1){
		$cuo_id = $dts['data']['tb_cuota_id'];
		$cre_id=$dts['data']['tb_credito_id'];
		$cuodet_fec= mostrar_fecha($dts['data']['tb_cuota_fec']);
		$cuo_num=$dts['data']['tb_cuota_num'];
		$mon_id=$dts['data']['tb_moneda_id'];
		$mon_nom=$dts['data']['tb_moneda_nom'];
		$cuodet_cuo = moneda_mysql($dts['data']['tb_cuota_mor']);
		$cuo_int=$dts['data']['tb_cuota_int'];
		$cuo_cap=$dts['data']['tb_cuota_cap'];
		$cuo_persubcuo=$dts['data']['tb_cuota_persubcuo'];
                $cuo_per = $dts['data']['tb_cuota_per']; //permiso para poder hacer pagos sobre esta cuota, 0 solo el admin, 1 tienen todos el permiso
        }
        $dts=NULL;
        
	$moneda = 'S/';
	if($mon_id == 2)
		$moneda = 'US$';

	$dts=$oCredito->mostrarUno($cre_id);
	if($dts['estado']==1){
		$cuotip_id=$dts['data']['tb_cuotatipo_id'];
		$cuotip_nom=$dts['data']['tb_cuotatipo_nom'];
		$cre_int = $dts['data']['tb_credito_int'];
		$cre_numcuo = $dts['data']['tb_credito_numcuo'];
		$cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
		$cli_id=$dts['data']['tb_cliente_id'];
		$cli_doc = $dts['data']['tb_cliente_doc'];
		$cli_nom=$dts['data']['tb_cliente_nom'];
		$cliente=$cli_nom.' | '.$cli_doc;
        }
        $dts=NULL;
        
	$codigo='CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);

	if($cuotip_id==2)$cuota=$cuo_num.'/'.$cre_numcuo.'';
	if($cuotip_id==1)$cuota=$cuo_num.'/'.$cuo_num.' ('.$cre_numcuomax.')';

	$pag_det="PAGO DE MORA CRÉDITO $codigo N° CUOTA: $cuota";

  //pagos parciales
	$pagos_cuota=0;
  $mod_id=1;

  $dts3=$oMorapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);

  if($dts3['estado']==1){
    foreach($dts3['data']as $key=>$dt3)
    {
      $pagos_cuota+= moneda_mysql($dt3['tb_ingreso_imp']);
    }
    
    }
    $dts3=NULL;

    $pagos=$pagos_cuota;
    $pagos_parciales= mostrar_moneda($pagos);
    $monto_pagar= mostrar_moneda($cuodet_cuo-$pagos);

  $nombre_credito = '';
	if($cuotip_id==2)//fijo
	{
		$nombre_credito  = 'CRÉDITO MENOR - CUOTA FIJA';

		if($cuo_num==$cre_numcuo)
		{
			$vMax=moneda_mysql($monto_pagar);
		}
		else
		{
			$vMax='99999.00';
		}
		
	}

	if($cuotip_id==1)//libre
	{
		$nombre_credito  = 'CRÉDITO MENOR - CUOTA LIBRE';
		$vMax=moneda_mysql($monto_pagar);
	}
}

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cobranzamora" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">INFORMACIÓN DE MORA A PAGAR  FECHA </h4>
        </div>
        <form id="for_cuopag">
            <input type="hidden" id="hdd_vista" value="<?php echo $_POST['vista']?>">
            <input type="hidden" name="hdd_pag_det" value="<?php echo $pag_det;?>">
            <input type="hidden" name="hdd_mon_id" id="hdd_mon_id" value="1">
            <input type="hidden" name="hdd_credito" value="<?php echo $credito;?>">
            <input type="hidden" name="hdd_mod_id" value="<?php echo $mod_id;?>">
            <input type="hidden" name="hdd_modid" value="<?php echo $cuota_id;?>">
            <input type="hidden" name="hdd_cuo_id" value="<?php echo $cuo_id;?>">
            <input type="hidden" name="hdd_cre_id" value="<?php echo $cre_id;?>">
            <input type="hidden" name="hdd_cli_id" value="<?php echo $cli_id;?>">
            <input type="hidden" name="hdd_cuenta_dep" id="hdd_cuenta_dep">
            <input type="hidden" name="hdd_mon_iddep" id="hdd_mon_iddep">
            <input type="hidden" name="hdd_cuotatip_id" value="<?php echo $cuotip_id;?>">
            <input type="hidden" name="hdd_vMax"  id="hdd_vMax" value="<?php echo $vMax;?>">
            <input type="hidden" name="hidden_cuodet_fec" id="hidden_cuodet_fec" value="<?php echo $cuodet_fec; ?>">
                    <input name="hdd_cuo_num" id="hdd_cuo_num" type="hidden" value="<?php echo $cuo_num ?>">
            
            
            <div class="modal-body" style="font-family: cambria">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Cliente:</label>
                                    <div class="col-md-8">
                                      <?php echo $cliente?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Cambio:</label>
                                    <div class="col-md-5">
                                      <input  class="form-control input-sm" name="txt_cuopag_tipcam" type="text" value="<?php echo $cuopag_tipcam?>" id="txt_cuopag_tipcam" style="text-align:right;font-weight: bold;font-size: 15px" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Crédito:</label>
                                    <div class="col-md-8">
                                      <?php echo $nombre_credito;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Código:</label>
                                    <div class="col-md-8">
                                      <?php echo $codigo?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° de Cuota:</label>
                                    <div class="col-md-8">
                                      <?php echo $cuota.$ultima_cuota;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha de Vencimiento:</label>
                                    <div class="col-md-8">
                                      <?php echo $cuodet_fec; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Mora <?php echo $moneda;?>:</label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control input-sm" name="txt_cuodet_cuo" id="txt_cuodet_cuo" value="<?php echo mostrar_moneda($cuodet_cuo);?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Pagos Parciales:</label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control input-sm" name="txt_cuopag_pagpar" id="txt_cuopag_pagpar" value="<?php echo $pagos_parciales;?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Total Pagar <?php echo $moneda;?>:</label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control input-sm" name="txt_cuopag_tot" id="txt_cuopag_tot" value="<?php echo $monto_pagar;?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° Operación:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control input-sm" id="txt_cuopag_numope" name="txt_cuopag_numope">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha Depósito:</label>
                                    <div class="col-md-8">
                                        <div class='input-group date' id='ingreso_fil_picker1'>
                                            <input type="text" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" class="form-control input-sm" readonly value="<?php echo date('d-m-Y');?>">
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha Validación:</label>
                                    <div class="col-md-8">
                                        <div class='input-group date' id='ingreso_fil_picker2'>
                                            <input type="text" name="txt_cuopag_fec" id="txt_cuopag_fec" class="form-control input-sm" readonly value="<?php echo date('d-m-Y');?>">
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Depósito:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Comisión Banco:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" maxlength="10" value="0.00">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Validar:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" maxlength="10" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° Cuenta:</label>
                                    <div class="col-md-8">
                                        <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                                            <?php // require_once '../cuentadeposito/cuentadeposito_select.php';?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">En Banco:</label>
                                    <div class="col-md-8">
                                        <select name="cmb_banco_id" class="form-control input-sm">
                                            <option value="">--</option>
                                            <option value="1" selected="true">Banco BCP</option>
                                            <option value="2">Banco Interbak</option>
                                            <option value="3">Banco BBVA</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Pagado:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_montot" type="text" id="txt_cuopag_montot" value="0" maxlength="10" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="text-align: right">Observación:</label>
                                    <div class="col-md-10">
                                        <textarea type="text" name="txt_cuopag_obs" rows="2" cols="50" class="form-control input-sm mayus"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="submit" class="btn btn-info" id="btn_guar_cuopag">Guardar</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>         
            </div>
        </form>   
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/cobranzamora/morapago_banco.js?ver=11123333234';?>"></script>