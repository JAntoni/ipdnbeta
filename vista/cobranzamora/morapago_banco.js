     /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    cmb_cuedep_id(0);
    cambio_moneda(1);
    $('.moneda2').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '99999.00'
});
$('.moneda3').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '99999.00'
        });
        
    $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
    }).on('changeDate', function(selected){
        //console.log("BANDERAA");
        //calcular_mora(); la mora será a criterio ahora
        var cuotatip_id = parseInt($('#hdd_cuotatip_id').val())
        //verificar si el n° de operación existe en la fecha indicada
        validar_no_operacion_fecha();
    });

    $("#txt_cuopag_numope").blur(function(){
        // Aquí colocas la acción que quieres realizar
        validar_no_operacion_fecha();
    });
      
    $('#ingreso_fil_picker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
    });
    
    $('#txt_cuopag_fecdep').change(function(event) {
        cambio_moneda(1);
    });
    
     $('#txt_cuopag_mon, #txt_cuopag_comi').change(function(event) {
            var monto_deposito = Number($('#txt_cuopag_mon').autoNumeric('get'));
            var monto_comision = Number($('#txt_cuopag_comi').autoNumeric('get'));
            var cuenta_deposito = $('#cmb_cuedep_id').val();
            var saldo = 0;

            saldo = monto_deposito - monto_comision;

            if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
                    $('#txt_cuopag_montot').autoNumeric('set',saldo.toFixed(2));
                    $('#txt_cuopag_monval').autoNumeric('set',saldo.toFixed(2));
            }
            else{
                    $('#txt_cuopag_montot').autoNumeric('set',0);
                    $('#txt_cuopag_monval').autoNumeric('set',0);
            }

            if(cuenta_deposito)
                    $('#cmb_cuedep_id').change();
    });

$('#cmb_cuedep_id').change(function(event) {
//        console.log("estoy entrando al sistema select ");
        var moneda_sel = $(this).find(':selected').data('moneda'); //este tipo de moneda corresponde al tipo de cuenta seleccionada
        var moneda_id = $('#hdd_mon_id').val(); //esta moneda pertenece al credito
        var monto_validar = Number($('#txt_cuopag_monval').autoNumeric('get')); //es el restanto de lo depositado menos la comision que cobra el banco
        var monto_cambio = Number($('#txt_cuopag_tipcam').val()); //monto del tipo de cambio del día del deposito
        var total_pagado = 0;
        if(parseInt(moneda_sel) == 2 && parseInt(moneda_id) == 1){
                total_pagado = parseFloat(monto_validar * monto_cambio);
                $('#txt_cuopag_montot').autoNumeric('set',total_pagado);
                console.log('dolar a soles monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
        }
        else if(parseInt(moneda_sel) == 1 && parseInt(moneda_id) == 2){
                total_pagado = parseFloat(monto_validar / monto_cambio);
                $('#txt_cuopag_montot').autoNumeric('set',total_pagado);
                console.log('soles a dolares monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
        }
        else
                $('#txt_cuopag_montot').autoNumeric('set',monto_validar);

        var cuenta = $(this).find("option:selected").text();
        $('#hdd_cuenta_dep').val(cuenta);
        $('#hdd_mon_iddep').val(moneda_sel);
    });

    

    
});

function cmb_cuedep_id(ids)
{	
        $.ajax({
                type: "POST",
                url:  VISTA_URL+"cuentadeposito/cuentadeposito_select.php",
                async:false,
                dataType: "html",                      
                data: ({
                        cuedep_id: ids
                }),
                beforeSend: function() {
                        $('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
                },
                success: function(html){
                        $('#cmb_cuedep_id').html(html);
                }
        });
}

function cambio_moneda(monid){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"monedacambio/monedacambio_controller.php",
            async:false,
            dataType: "json",
            data: ({
                    action: 'obtener_cambio',
                    fecha:	$('#txt_cuopag_fecdep').val(),
                    tipo:	"compra"
            }),
            beforeSend: function(){
                    $('#msj_pagobanco').hide(100);
            },
            success: function(data){
                    if(data.estado == 0){
                            $('#txt_cuopag_tipcam').val('');
                            $('#msj_pagobanco').show(100);
                            $('#msj_pagobanco').html('Por favor registre el tipo de cambio del día de la fecha de depósito');
                            swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
                    }
                    else
                            $('#txt_cuopag_tipcam').val(data.moncam_val);	
            },
            complete: function(data){			
//                 console.log(data);
            }
        });		
}

function validar_no_operacion_fecha() {

        var fecha_deposito = $('#txt_cuopag_fecdep').val();
        var num_operacion = $('#txt_cuopag_numope').val();
        var fecha_vencimiento = $('#hidden_cuodet_fec').val();
        var numero_cuota = $('#hdd_cuo_num').val()
        if(num_operacion == ""){
            swal_warning('AVISO', "DEBE REGISTRAR NUMERO DE OPERACION", 6000);
            $('#txt_cuopag_fecdep').val('')
            return false;
        }
        $.ajax({
            type: "POST",
            url: VISTA_URL + "vencimiento/vencimiento_funciones.php",
            async: true,
            dataType: "json",
            data: ({
                funcion: "validar_operacion_fecha",
                fecha_deposito: fecha_deposito,
                num_operacion: num_operacion,
                fecha_vencimiento: fecha_vencimiento,
                numero_cuota: numero_cuota
            }),
            success: function (data) {
                console.log(data);
                if(data.estado == 0){
                    swal_warning('AVISO', data.mensaje, 6000);
                    $('#txt_cuopag_fecdep').val('')
                }
                else {
                    notificacion_success('N° operación y fecha correctos', 6000)
                    $('#cmb_cuedep_id').val(data.cuentaid).change();
                }
                
                $('#hdd_var_op').val(data.mismo_mes) //nos permite mantener la misma fecha para crear las cuotas
    
            },
            complete: function (data) {
                console.log(data)
            }
        });
}

function ingreso_imppos_datos3(id_ing,empresa_id) {
        var codigo=id_ing;
        window.open("http://ipdnsac.com/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
}

$("#for_cuopag").validate({
        submitHandler: function() {
                $.ajax({
                        type: "POST",
                        url: VISTA_URL+"cobranzamora/morapago_banco_reg.php",
                        async:true,
                        dataType: "json",
                        data: $("#for_cuopag").serialize(),
                        beforeSend: function() {
                        },
                        success: function(data){
                                if(parseInt(data.cuopag_id) > 0){
                                    $('#modal_registro_cobranzamora').modal('hide');
                                   swal_success('Sistema',data.cuopag_msj,2000);
                                   //cobranzamora_tabla();
//                                   $('#modal_registro_vencimientomenorbanco').modal('hide');
//                                if(confirm('¿Desea imprimir?'))
                                    //cuotapago_imppos_datos_2(data.cuopag_id);

                                    
                                    var hdd_cuotip_id=$('#hdd_cuotip_id').val();
                                    if(hdd_cuotip_id==1){
                                        generar_nueva_cuota();
                                    }
                                    if(hdd_cuotip_id==3){
                                        generar_nueva_cuota_garveh();
                                    }

                                    var hdd_vista=$('#hdd_vista').val();
                                    if(hdd_vista=='vencimiento_tabla'){
                                        cobranzamora_tabla();
                                    }
                                    if(hdd_vista=="historial_mora"){
                                        morapago_pagos_tabla();
                                    }
                                    
                                    ingreso_imppos_datos3(data.ingreso_id, data.empresa_id)

                                    var acu_id = Number($('#hdd_cuodet_acupag_id').val());
                                    if(acu_id > 0)
                                      cuotapago_asiveh_form('pagar',acu_id);
                                }
                                else{
                                        swal_warning('AVISO',data.cuopag_msj,10000);
                                }	
                        },
                        complete: function(data){
//                            console.log(data);
                                if(data.statusText != "success" || data.statusText != "OK"){
//                                        $('#btn_guar_cuopag').show();
//                                        $('#msj_pagobanco').show(100);
//                                        $('#msj_pagobanco').text('ERROR: ' + data.responseText);
//                                        swal_error('ERROR',data.responseText,10000);
                                        console.log(data);
                                }
                        }
                });
        },
        rules: {
                txt_cuopag_mon:{
                        required:true
                },
                txt_cuopag_tipcam:{
                        required:true
                },
                txt_cuopag_numope: {
                        required:true
                },
                txt_cuopag_comi:{
                        required: true
                },
                txt_cuopag_monval: {
                        required: true
                },
                cmb_cuedep_id: {
                        required: true
                },
                txt_cuopag_montot: {
                        required: true
                }
        },
        messages: {
                txt_cuopag_mon:{
                        required:'* registre el monto pagado'
                },
                txt_cuopag_tipcam:{
                        required:'* registre el tipo de cambio'
                },
                txt_cuopag_numope: {
                        required: '* registre el numero de operacion'
                },
                txt_cuopag_comi:{
                        required: '* registre la comisión del banco'
                },
                txt_cuopag_monval: {
                        required: '* registre el monto a validar'
                },
                cmb_cuedep_id: {
                        required: '* registre la cuenta de deposito'
                },
                txt_cuopag_montot: {
                        required: '* registre el monto total'
                }
        }
});