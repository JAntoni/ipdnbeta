<?php
class cVencimiento{
	
	function filtrar_menor($cli_id,$cre_id,$fec_act)
	{
		$sql="
		SELECT *
		FROM tb_creditomenor cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 1 AND cu.tb_cuota_est IN(2,3) AND cu.tb_cuota_mor>0 AND cu.tb_cuota_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = $cre_id";

		$sql.=" GROUP BY c.tb_cliente_nom";
		$sql.=" ORDER BY tb_cuota_fec";

		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}

	function filtrar_asiveh($cli_id,$cre_id,$fec_act)
	{
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cd.tb_cuotadetalle_mor>0 AND cd.tb_cuotadetalle_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = $cre_id";

		$sql.=" GROUP BY cm.tb_cliente_id";
		$sql.=" ORDER BY tb_cuota_fec";
		//$sql.=" LIMIT 2";
		
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}

	function filtrar_garveh($cli_id,$cre_id,$fec_act)
	{
		$sql="SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE tb_credito_xac=1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_est = 2 AND cd.tb_cuotadetalle_mor>0 AND cd.tb_cuotadetalle_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = $cre_id";

		$sql.=" GROUP BY cm.tb_cliente_id";
		$sql.=" ORDER BY tb_cuota_fec";
		
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	function filtrar_hipo($cli_id,$cre_id,$fec_act)
	{
		$sql="SELECT *
		FROM tb_creditohipo cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE tb_credito_xac=1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_est = 2 AND cd.tb_cuotadetalle_mor>0 AND cd.tb_cuotadetalle_morest <> 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = $cre_id";

		$sql.=" GROUP BY cm.tb_cliente_id";
		$sql.=" ORDER BY tb_cuota_fec";
		
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	
	function mostrarUnoMenor($id){
		$sql="SELECT * 
		FROM tb_creditomenor cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	function mostrarUnoAsiveh($id){
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	function mostrarUnoGarveh($id){
		$sql="SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est = 2";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	function mostrarUnoHipo($id){
		$sql="SELECT * 
		FROM tb_creditohipo cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est = 2";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}

	function mostrarDetalleAsiveh($id, $id2){
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id and cd.tb_cuotadetalle_id=$id2 and cd.tb_cuotadetalle_est <> 2";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	function mostrarDetalleGarveh($id, $id2){
		$sql="SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id and cd.tb_cuotadetalle_id=$id2 and cd.tb_cuotadetalle_est <> 2";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}

	function mostrarPagadoAsiveh($id){
		$sql="SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est = 2";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
	function mostrarPagadoGarveh($id){
		$sql="SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est = 2";
		$oCado = new Cado();
		$rst=$oCado->ejecute_sql($sql);
		return $rst;
	}
}
?>