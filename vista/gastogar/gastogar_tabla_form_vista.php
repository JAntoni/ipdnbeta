<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}
require_once('Gastogar.class.php');
$oGastogar = new Gastogar();

$fecha_hoy = date('Y-m-d');
$arr[0] = 'NO';
$arr[1] = 'SI';

//$column_name, $param.$i, $datatype
$parametros[0]['column_name'] = 'tb_gastogar_xac';
$parametros[0]['param0'] = 1;
$parametros[0]['datatype'] = 'INT';

$parametros[1]['column_name'] = 'tb_garantia_idfk';
$parametros[1]['param1'] = intval($_POST['garantia_id']);
$parametros[1]['datatype'] = 'INT';

$otros_param["orden"]["column_name"] = "gg.tb_gastogar_fecreg";
$otros_param["orden"]["value"] = "DESC";

for ($i = 0; $i < count($parametros); $i++) {
    if (empty($parametros[$i]['param'.$i])) {
        $parametros[$i] = array();
    }
}

$result = $oGastogar->listar_todos($parametros, $otros_param);

$tr = '';
if ($result['estado'] == 1) {
    $contador = 0;
    foreach ($result['data'] as $key => $value) {
        if(empty($value['tb_gastogar_emitedocsunat'])){
            $documentos_sunat = 'no emite';
        } else {
            $documentos_sunat = empty($value['tb_gastogar_documentossunat']) ? '(PENDIENTE DE RECIBIR FACTURA)' : $value['tb_gastogar_documentossunat'];
            $documentos_sunat.= " | ".$value['tb_proveedor_nom'];
        }
        $egreso_empresa = empty($value['tb_empresa_idfk']) ? 'no generó egreso' : explode(" - ", $value["tb_empresa_nomcom"])[1];
        $egreso_fecha = empty($value['tb_empresa_idfk']) ? 'no generó egreso' : mostrar_fecha($value['tb_gastogar_fecha']);
        $forma_pago = 'no generó egreso';
        if($value['tb_gastogar_formapago'] == 1){
            $forma_pago = 'CAJA EFECTIVO';
        } elseif ($value['tb_gastogar_formapago'] == 2) {
            $forma_pago = 'I/E POR BANCO | N° Operacion: '. $value['tb_gastogar_numope'];
        }
        $tr .= '<tr>';
        $tr .= '
        <td id="tabla_fila">' . $value['tb_gastogar_id'] . '</td>
        <td id="tabla_fila">' . $egreso_empresa . '</td>
        <td id="tabla_fila">' . $forma_pago . '</td>
        <td id="tabla_fila">' . $value['tb_moneda_nom'] . ' ' . mostrar_moneda($value['tb_gastogar_monto']) . '</td>
        <td id="tabla_fila">' . $value['tb_falla_nombre'] . ' - ' . $value['tb_gastogar_coment'] . '</td>
        <td id="tabla_fila">' . $egreso_fecha . '</td>
        <td id="tabla_fila">' . $documentos_sunat . '</td>
        <td id="tabla_fila">' . $value['usuario_registro'] . '</td>
        <td id="tabla_fila" align="center">
            <button class="btn btn-info btn-xs" title="Ver" onclick="gasto_form(\'L\', '. intval($_POST['garantia_id']) .", ". intval($value['tb_gastogar_id']).", " . $value['tb_credito_idfk']  .')"><i class="fa fa-eye"></i></button>'."\n";
        if (($value['tb_gastogar_fecha'] == fecha_mysql($fecha_hoy) && intval($_SESSION["usuariogrupo_id"]) == 2) || empty($value['tb_gastogar_fecha'])) {
            $tr.= '<button class="btn btn-warning btn-xs" title="Editar" onclick="gasto_form(\'M\', '. intval($_POST['garantia_id']) .",". intval($value['tb_gastogar_id']) .", {$value['tb_credito_idfk']})\">" . '<i class="fa fa-edit"></i></button>'."\n";
        } elseif ($value['tb_gastogar_fecha'] != $fecha_hoy && $value['tb_gastogar_emitedocsunat'] == 1 && empty($value['tb_gastogar_documentossunat'])){
            $tr.= '<button class="btn btn-warning btn-xs" title="Editar" onclick="gasto_form(\'M_doc\', '. intval($_POST['garantia_id']) .", ". intval($value['tb_gastogar_id']).", " . $value['tb_credito_idfk']  .')"><i class="fa fa-edit"></i></button>'."\n";
        }
        if (($value['tb_gastogar_fecha'] == fecha_mysql($fecha_hoy) && intval($_SESSION["usuariogrupo_id"]) == 2) || empty($value['tb_gastogar_fecha'])) {
            $tr .= '<button class="btn btn-danger btn-xs" title="Eliminar" onclick="gasto_form(\'E\', '. intval($_POST['garantia_id']) . ',' . intval($value['tb_gastogar_id']) . ", {$value['tb_credito_idfk']})\">" . '<i class="fa fa-trash"></i></button>';
        }
        $tr.='
            <button class="btn btn-primary btn-xs" title="Historial" onclick="gastogar_historial_form(' . $value['tb_gastogar_id'] . ')"><i class="fa fa-history"></i></button>
        </td>';
        $tr .= '</tr>';
    }
    $data['estado'] = 1;
} else {
    $tr = $result['mensaje'];
    $data['estado'] = 0;
}
$result = null;
$data['tr'] = $tr;
echo json_encode($data);
?>