var cantidad_focus = 1;

function removeAccents (str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    .replaceAll("'", '´')
    .replaceAll('"', '´');
}

$(document).ready(function () {
    $("#cbo_sede")[0][0].remove();
    $("#cbo_articulo option[value=0]").hide();
    $('#div_anexado').hide();
    $('#div_egresado').hide();

    $('#txt_comentario').blur(function(){
        $(this).val(removeAccents($(this).val()));
    });

    $("#che_emite_docsunat").on('ifChanged', function () {
        if (this.checked) {
            $(".emite_doc_sunat").show(200);
            $('#txt_documento_sunat').focus();
        }
        else {
            $(".emite_doc_sunat").hide(200);
            $("#txt_documento_sunat").val(null);
            limpiar_proveedor();
        }
    });

    $("#che_generar_egreso").on('ifChanged', function () {
        if (this.checked) {
            $(".genera_egreso").show(10);
            $('#cbo_sede').focus();
        }
        else {
            $("#cbo_sede").val($('#hdd_empresa_default').val());
            $('#cbo_forma_egreso').val(1).change();
            $('#che_emite_docsunat').iCheck('uncheck');
            $(".genera_egreso").hide(10);
        }
    });

    $('.moneda').focus(function () {
        formato_moneda(this);
    });

    if ($("#cbo_falla option:selected").val() == 0) {
        $("#col_add_falla").show();
    } else {
        $("#col_add_falla").hide();
    }

    if ($("#cbo_forma_egreso option:selected").val() == 2) {
        $("div .num-ope").show();
    } else {
        $("div .num-ope").hide();
    }

    $("#txt_proveedor_doc,#txt_proveedor_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                VISTA_URL + "proveedor/proveedor_autocomplete.php",
                { term: request.term }, //
                response
            );
        },
        select: function (event, ui) {
            $('#hdd_proveedor_id').val(ui.item.tb_proveedor_id);
            $('#txt_proveedor_doc').val(ui.item.tb_proveedor_doc);
            $('#txt_proveedor_nom').val(ui.item.tb_proveedor_nom);
            event.preventDefault();
            disabled($('input[id*="txt_proveedor_"]'));
            $('#txt_comentario').focus();
        }
    });

    $('#txt_fecha_gasto').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });

    $("#cbo_falla").change(function () {
        var falla_id = $("#cbo_falla option:selected").val();
        if (falla_id == "0") {
            $("#col_add_falla").show(200);
        } else {
            $("#col_add_falla").hide(200);
        }
    });

    $("#cbo_articulo").change(function () {
        var articulo_id = $("#cbo_articulo option:selected").val();
        if (articulo_id == "0") {
            $("#col_add_articulo").show(200);
        } else {
            $("#col_add_articulo").hide(200);
            $('#txt_articulo_nom').val('');
        }
        completar_cbo_fallas(articulo_id);
    });

    $("#cbo_forma_egreso").change(function () {
        var forma_id = $("#cbo_forma_egreso option:selected").val();
        if (forma_id == "2") {
            $("div .num-ope").show(200);
        } else {
            $("div .num-ope").hide(200);
            $('#txt_numope').val('');
        }
    });

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: `icheckbox_flat-green`,
        radioClass: `iradio_flat-green`
    });

    $("#form_gastos").validate({
        submitHandler: function () {

            var form_serializado = serializar_form();
            $.ajax({
                type: "POST",
                url: VISTA_URL + "gastogar/gastogar_controller.php",
                async: true,
                dataType: "json",
                data: form_serializado,
                beforeSend: function () {
                    $("#gastogar_mensaje").html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
                    $("#btn_guardar_gastogar").prop("disabled", true);
                },
                success: function (data) {
                    //console.log(data);
                    if (parseInt(data.estado) > 0) {
                        $("#gastogar_mensaje").removeClass("callout-info callout-danger callout-warning").addClass("callout-success");
                        cantidad_tiempo = data.mensaje.split('<br>').length + 1;
                        cantidad_tiempo = 2400 * cantidad_tiempo;

                        $("#gastogar_mensaje").html(`<h4>${data.mensaje}</h4>`);
                        disabled($('#form_gastos').find("button[class*='btn-sm'], select, input, textarea, checkbox"));

                        setTimeout(function () {
                            //aqui debe ir un $.confirm al ser todo correcto
                            $("#div_modal_gastogar_form").modal("hide");
                            //aqui se deberia actualizar la tabla
                            if ($('#div_gastogar_tabla').html() !== '' && data.estado != 2) {
                                consultar_tabla_gastos();
                            }
                        }, cantidad_tiempo);
                    } else {
                        $("#gastogar_mensaje").removeClass("callout-info").addClass("callout-warning");
                        $("#gastogar_mensaje").html(`<h4>${data.mensaje}</h4>`);
                        $("#btn_guardar_gastogar").prop("disabled", false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $("#gastogar_mensaje").removeClass("callout-info").addClass("callout-danger");
                    $("#gastogar_mensaje").html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
                    $("#btn_guardar_gastogar").prop("disabled", false);
                },
            });
        },
        rules: {
            txt_articulo_nom: {
                required: function(){
                    return parseInt($('#cbo_articulo').val()) == 0;
                },
                minlength: 2
            },
            cbo_articulo: {
                min: -1
            },
            cbo_falla: {
                min: -1
            },
            txt_falla_nom: {
                required: function () {
                    return ($("#cbo_falla option:selected").val() === '0');
                },
                minlength: 2
            },
            txt_fecha_gasto: {
                required: true
            },
            txt_monto: {
                required: true
            },
            cbo_forma_egreso: {
                required: true,
                min: 1
            },
            txt_documento_sunat: {
                required: function () {
                    return $('#action_form_gastogar2').val() == 'modificar_doc';
                }
            },
            txt_proveedor_doc: {
                required: function () {
                    return ($('#che_emite_docsunat').prop('checked') && $('#hdd_proveedor_id').val() == '');
                }
            },
            txt_proveedor_nom: {
                required: function () {
                    return ($('#che_emite_docsunat').prop('checked') && $('#hdd_proveedor_id').val() == '');
                }
            },
            txt_comentario: {
                required: true
            },
            txt_numope: {
                required: function () {
                    return $('#cbo_forma_egreso option:selected').val() == 2;
                }
            }
        },
        messages: {
            txt_articulo_nom: {
                required: "Ingrese nombre de artículo",
                minlength: "Ingrese nombre de artículo"
            },
            cbo_articulo: {
                min: "Seleccione tipo artículo"
            },
            cbo_falla:{
                min: "Seleccione Falla"
            },
            txt_falla_nom: {
                required: "Ingrese la falla",
                minlength: "Ingrese la falla"
            },
            txt_fecha_gasto: {
                required: "Seleccione fecha de gasto"
            },
            txt_monto: {
                required: "Ingrese el monto"
            },
            cbo_forma_egreso: {
                required: "Elegir forma egreso",
                min: "Elegir forma egreso"
            },
            txt_documento_sunat: {
                required: "Ingrese documento (Boleta/Factura)"
            },
            txt_proveedor_doc: {
                required: "Ingrese proveedor"
            },
            txt_proveedor_nom: {
                required: "Ingrese proveedor"
            },
            txt_comentario: {
                required: "Ingrese descripcion"
            },
            txt_numope: {
                required: "Ingrese N° Operación"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            }
            else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0])
            }
            else if( element.parents("#div_cbo_articulo").length == 1 ){
                error.insertAfter(element.parents("#div_cbo_articulo")[0]);
            }
            else if( element.parents("#falla-grp").length == 1 ){
                error.insertAfter(element.parents("#falla-grp")[0]);
            }
            else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    if ($('#action_form_gastogar').val() == 'leer' || $('#action_form_gastogar').val() == 'eliminar') {
        disabled($('#form_gastos').find("button[class*='btn-sm'], select, input, textarea, checkbox"));
        $('#form_gastos').find("input").css("cursor", "text");
        $('#cbo_forma_egreso').val($('#hdd_gastogar_formapago').val()).change();

        if ($('#action_form_gastogar').val() == 'eliminar') {
            $('#gastogar_mensaje').removeClass('callout-info').addClass('callout-warning');
            mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este gasto de la garantía?</h4>';
            $('#gastogar_mensaje').html(mens);
            $('#gastogar_mensaje').show();

            if(parseInt($('#hdd_egreso_id').val()) > 0){
                $('#div_egresado').show()
            }
            if(parseInt($('#hdd_gastoanexadoenccd').val()) == 1){
                $('#div_anexado').show();
            }
        }
    } else {
        if($('#action_form_gastogar2').val() == 'modificar_doc'){
            disabled($('#form_gastos').find("button[class*='btn-sm'], select, input, textarea, checkbox").not('#txt_documento_sunat, #txt_comentario, #che_emite_docsunat'));
        }
        else if ($('#action_form_gastogar').val() == 'modificar') {
            if($('#che_generar_egreso').prop('checked')){
                disabled($('#che_generar_egreso'));
            }
            if(parseInt($('#hdd_gastogar_formapago').val()) > 0){
                $('#cbo_forma_egreso').val($('#hdd_gastogar_formapago').val()).change();
                if($('#che_emite_docsunat').prop('checked')){
                    disabled($('input[id*="txt_proveedor_"]'));
                }
            }

            if(parseInt($('#hdd_egreso_id').val()) > 0){
                $('#div_egresado').show()
            }
            if(parseInt($('#hdd_gastoanexadoenccd').val()) == 1){
                $('#div_anexado').show();
            }
        }
        disabled($('#txt_fecha_gasto'));

        //texto recomendado para detalle
        $('#txt_comentario').focus(function () {
            if (cantidad_focus == 1) {
                var detalle_recomendado = 'Gasto en *CM-' + $('#hdd_credito_id').val() + '*, Garantia id: *' + $('#hdd_garantia_id').val() + '*, ';
                detalle_recomendado += 'por concepto *' + $('#cbo_falla option:selected').text() + '*. ';
                if ($('#txt_monto').val() !== '') {
                    detalle_recomendado += 'Monto: *' + $('#cmb_moneda_id option:selected').text() + $('#txt_monto').val() + '*. ';
                }
                if ($('#che_generar_egreso').prop('checked')) {
                    sede = $('#cbo_sede option:selected').text();
                    sede = sede.split("- ")[1];
                    forma_pago = $('#cbo_forma_egreso option:selected').text();
                    forma_pago = forma_pago.split("Egreso ")[1];
                    if (forma_pago == 'Banco') {
                        forma_pago += ` con N° Operacion ${$('#txt_numope').val()}`;
                    }
                    detalle_recomendado += `Egreso del dia *${$('#txt_fecha_gasto').val()}*, en sede *${sede}*. El medio de pago es por *${forma_pago}*. `;
                    if ($('#che_emite_docsunat').prop('checked')) {
                        if ($('#txt_documento_sunat').val().length < 13){
                            $('#txt_documento_sunat').val('(PENDIENTE DE RECIBIR FACTURA)');
                        }
                        detalle_recomendado += `Proveedor Ruc *${$('#txt_proveedor_doc').val()}*, documento *${$('#txt_documento_sunat').val()}*`;
                    } else {
                        detalle_recomendado += 'No emite boleta/factura';
                    }
                } else {
                    detalle_recomendado += 'No genera egreso';
                }
                $('#txt_comentario').val(detalle_recomendado);
                cantidad_focus++;
            }
        });
    }


    $('#cbo_articulo').selectpicker({
        liveSearch: true,
        maxOptions: 1,
        language: 'ES'
    });
    if ($("#hdd_articulo_id").val() > 0) {
        disabled($('#cbo_articulo, #btn_add_articulo'));
        completar_cbo_fallas($("#hdd_articulo_id").val());
    } else {
        completar_cbo_fallas(0);
    }

    $('button[data-id="cbo_articulo"]').addClass('btn-sm');

    //revision de apertura de caja al dar check en gasto y elegir caja
    $('#cbo_sede').blur(function () {
        verificar_apertura_caja();
    });

    //revision del tipo de cambio del dia registrado
    $('#cmb_moneda_id').blur(function () {
        moneda_id = $('#cmb_moneda_id option:selected').val();
        if (moneda_id == 2) {
            verificar_tipocambio();
        }
        if ($('#che_generar_egreso').prop('checked')) {
            verificar_apertura_caja();
        }
        if ($('#cbo_forma_egreso option:selected').val() == 2) {
            verificar_numope();
        }
    });

    $('#txt_numope').blur(function () {
        if ($('#txt_numope').val() !== '') {
            verificar_numope();
        }
    });
    
    $('#txt_documento_sunat').blur(function () {
        if ($(this).val() !== '') {
            var serie = $.trim($(this).val().split("-")[0]);
            var numer = $.trim($(this).val().split("-")[1]);
            if ($(this).val().indexOf("-") <= -1) {
                alertas_error("Verifique", "Formato de Factura debe incluir un guión", 'small');
                return;
            }
            else if (serie.length > 4 || serie.length < 2  || numer.length > 8 || numer.length < 1) {
                alertas_error("Verifique", "Cantidad de caracteres en la serie y/o numero", 'small');
                return;
            }
            else if (!solo_letras(serie.substring(0, 1))) {
                alertas_error("Verifique", "Formato de Factura, la serie debe incluir una letra al inicio", 'small');
                return;
            }
            else if (!solo_numeros(serie.substring(serie.length - 1))){
                alertas_error("Verifique", "Formato de Factura, la serie debe incluir un número", 'small');
                return;
            }
            else if (!solo_numeros(numer)) {
                alertas_error("Verifique", "Formato de Factura, el número de factura es incorrecto", 'small');
                return;
            }
            $('#txt_documento_sunat').val(rellenar_serie_ceros(serie)+'-'+rellenar_num_ceros(numer, 8));
        }
    });

    disabled($('#form_gastos').find($('.disabled')));
});

function verificar_apertura_caja() {
    const action_original = $('#action_form_gastogar').val();
    $('#action_form_gastogar').val('verificar_apertura_caja');
    var form_serializado = serializar_form();
    $('#action_form_gastogar').val(action_original);
    //console.log(form_serializado); //return;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastogar/gastogar_controller.php",
        async: true,
        dataType: "json",
        data: form_serializado,
        beforeSend: function () {
            $("#gastogar_mensaje").removeClass("callout-warning").addClass("callout-info");
            $("#gastogar_mensaje").html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
            $("#btn_guardar_gastogar").prop("disabled", true);
        },
        success: function (data) {
            $("#btn_guardar_gastogar").prop("disabled", false);
            $("#gastogar_mensaje").hide(400);
            if (data.estado == 0) {
                alertas_error('Verifique', data.mensaje, 'small');
            }
        }
    });
}

function verificar_tipocambio() {
    const action_original = $('#action_form_gastogar').val();
    $('#action_form_gastogar').val('verificar_tipocambio');
    var form_serializado = serializar_form();
    $('#action_form_gastogar').val(action_original);
    //console.log(form_serializado); //return;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastogar/gastogar_controller.php",
        async: true,
        dataType: "json",
        data: form_serializado,
        beforeSend: function () {
            $("#gastogar_mensaje").removeClass("callout-warning").addClass("callout-info");
            $("#gastogar_mensaje").html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
            $("#btn_guardar_gastogar").prop("disabled", true);
        },
        success: function (data) {
            $("#btn_guardar_gastogar").prop("disabled", false);
            $("#gastogar_mensaje").hide(400);
            if (data.estado == 0) {
                alertas_error('Verifique', data.mensaje, 'small');
            }
        }
    });
}

function verificar_numope() {
    const action_original = $('#action_form_gastogar').val();
    $('#action_form_gastogar').val('verificar_numope');
    var form_serializado = serializar_form();
    $('#action_form_gastogar').val(action_original);
    //console.log(form_serializado); //return;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastogar/gastogar_controller.php",
        async: true,
        dataType: "json",
        data: form_serializado,
        beforeSend: function () {
            $("#gastogar_mensaje").removeClass("callout-warning").addClass("callout-info");
            $("#gastogar_mensaje").html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
            $("#btn_guardar_gastogar").prop("disabled", true);
        },
        success: function (data) {
            $("#btn_guardar_gastogar").prop("disabled", false);
            $("#gastogar_mensaje").hide(400);
            if (data.estado != 1) {
                alertas_error('Verifique', data.mensaje, 'small');
            }
        }
    });
}

function serializar_form() {
    var cbo_articulo_enabled = $("#cbo_articulo").is(":disabled") ? 0 : 1;
    var falla_selected_nom = (parseInt($("#cbo_falla").val()) > 0) ? $("#cbo_falla option:selected").text() : $("#txt_falla_nom").val();
    var datos_manuales = `&cbo_articulo_enabled=${cbo_articulo_enabled}&cbo_empresa_selected_nom=${$("#cbo_sede option:selected").text()}&cbo_falla_selected_nom=${falla_selected_nom}`;
    datos_manuales += `&moneda_nom_selected=${$('#cmb_moneda_id option:selected').text()}`;

    var elementos_disabled = $('#form_gastos').find('input:disabled, select:disabled').removeAttr('disabled');
    var form_serializado = $('#form_gastos').serialize() + datos_manuales;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

function añadir_articulo() {
    $('ul.dropdown-menu li[data-original-index=8] > a').css('display', '');
    $('#cbo_articulo').val(0).change();
    $("#col_add_articulo").show(200);
    $('#txt_articulo_nom').focus();
}

function añadir_falla() {
    $('#cbo_falla').val(0);
    $("#col_add_falla").show(200);
    $('#txt_falla_nom').focus();
}

function completar_cbo_fallas(articulo_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "falla/falla_select.php",
        async: true,
        dataType: "html",
        data: ({
            articulo_id: articulo_id,
            falla_id: parseInt($('#hdd_falla_id').val())
        }),
        beforeSend: function () {
            $('#cbo_falla').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cbo_falla').html(data);
            vacio = '<option value="0" style="font-weight: bold;">AÑADIR FALLA</option>';
            if (data == vacio) {
                $("#col_add_falla").show(200);
            }
            else {
                $("#col_add_falla").hide(200);
                $("#cbo_falla option[value=0]").hide();
            }
        },
        complete: function (data) {
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function limpiar_proveedor() {
    $('#hdd_proveedor_id').val('');
    $('#txt_proveedor_doc').val('');
    $('#txt_proveedor_nom').val('');

    $('#txt_proveedor_doc').prop('disabled', false);
    $('#txt_proveedor_nom').prop('disabled', false);
    $('#txt_proveedor_doc').focus();
}

function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            close: function () {
            }
        }
    });
}