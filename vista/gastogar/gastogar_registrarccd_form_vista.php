<?php
require_once('../../core/usuario_sesion.php');
require_once('../monedacambiocontable/Monedacambiocontable.class.php');
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('Gastogar.class.php');
$oGastogar = new Gastogar();
$data["lista"] = "";

$facturas_faltan_registrar = $oGastogar->listar_documentossunatdegastos();

if($facturas_faltan_registrar['estado'] == 1){
    foreach($facturas_faltan_registrar['data'] as $key => $factura){
        $registro['tb_compracontadoc_id'] = 0;
        $registro['tb_proveedor_idfk'] = $factura["tb_proveedor_idfk"];
        $registro['tb_proveedor_doc'] = $factura['tb_proveedor_doc'];
        $registro['tb_proveedor_nom'] = $factura['tb_proveedor_nom'];
        $registro['tb_compracontadoc_serie'] = explode("-", $factura["tb_gastogar_documentossunat"])[0];
        $registro['tb_compracontadoc_numero'] = explode("-", $factura["tb_gastogar_documentossunat"])[1];
        $registro['tb_compracontadoc_condicion'] = 'CONTADO';
        $registro['tb_compracontadoc_fechaemision'] = $factura['tb_gastogar_fecha'];
        $registro['tb_moneda_idfk'] = $factura['tb_moneda_idfk'];
        $tc_reg_val = 1;
        if ($factura['tb_moneda_idfk'] == 2) {
            $oMonedaCambioConta = new Monedacambiocontable();
            $tc_reg = $oMonedaCambioConta->mostrarUno('tb_monedacambiocontable_fecha', $factura['tb_gastogar_fecha'], 'STR');
            if ($tc_reg['estado'] == 1) {
                $tc_reg_id = $tc_reg['data']['tb_monedacambiocontable_id'];
                $tc_reg_val = $tc_reg['data']['tb_monedacambiocontable_comprasunat'];
                
                $registro['tb_monedacambiocontable_idfk'] = $tc_reg_id;
            }
            $oMonedaCambioConta = null;
        }
        $registro['tipocambio_comprasunat'] = $tc_reg_val;

        $data["lista"] .=
        '<li class="item" style="padding-right: 5px;">
            <div class="col-md-9" style="width: 78%; padding-right: 5px;">
                <a href="javascript:void(0)" class="product-title">'. $factura['tb_gastogar_documentossunat'] .' | '. $factura['tb_proveedor_doc'] .' - '. $factura['tb_proveedor_nom'] .'</a>
                <span style="font-size: 11px;">';
        $res = $oGastogar->listar_gastos_por_documentossunat($factura['tb_gastogar_documentossunat'], $factura['tb_proveedor_idfk']);
        $contador = 0;
        foreach ($res['data'] as $key => $gasto) {
            $tipocredito = array(null, 'CM', 'CAV', 'CGV', 'CH');
            $data['lista'] .= "<br>● GASTO EN {$tipocredito[$gasto['tb_creditotipo_idfk']]}-{$gasto['tb_credito_idfk']}, ";
            $data['lista'] .= "GARANTIA ID: {$gasto['tb_garantia_idfk']} - {$gasto['tb_garantia_pro']}, MONTO: {$gasto['tb_moneda_nom']}{$gasto['tb_gastogar_monto']}";

            $detalles[$contador]['tb_compracontadetalle_id'] = 0;
            $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
            $detalles[$contador]['tb_compracontadetalle_monto'] = formato_numero($gasto['tb_gastogar_monto']/1.18);
            $detalles[$contador]['tb_empresa_idfk'] = $gasto['tb_empresa_idfk'];
            $detalles[$contador]['tb_gastogar_idfk'] = $gasto['tb_gastogar_id'];
            $contador++;
        }
        $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
        $contador++;
        $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
        
        $registro['detalles'] = $detalles;
        $detalles = null;

        $data["lista"] .=
                '</span>
            </div>';
        $data["lista"] .=
            '<div class="col-md-3" style="width: 22%">'.
                "<p></p><button class=\"btn bg-aqua btn-xs btn-block\" onclick='compracontadoc_form(\"I\",". json_encode($registro) .")'><i class=\"fa fa-check\"></i> &nbsp; Ok</button>".
            '</div>
        </li>';
        $registro = null;
    }
} else {
    $data["lista"] = "No existen facturas de gastos por registrar";
}

echo json_encode($data);
?>