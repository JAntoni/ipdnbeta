<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../garantia/Garantia.class.php');
$oGar = new Garantia();
require_once('Gastogar.class.php');
$oGastogar = new Gastogar();
$listo = 1;

$usuario_action = $_POST['action'];
$titulo = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar Gasto de la garantia: ';
} elseif ($usuario_action == 'M' || $usuario_action == 'M_doc') {
    if($usuario_action == 'M_doc'){
        $action_2 = 'modificar_doc';
        $usuario_action = 'M';
    }
    $titulo = 'Editar Gasto de la garantia: ';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Gasto de la garantia: ';
} elseif ($usuario_action == 'L') {
    $titulo = 'Gasto registrado de la garantia: ';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action = devuelve_nombre_usuario_action($usuario_action);

$gasto_id = intval($_POST["gasto_id"]);
$garantia_id = intval($_POST["garantia_id"]);
$empresa_id = intval($_SESSION['empresa_id']);
$fecha_gasto = date('d-m-Y');

$garantia_registro = $oGar->mostrarUno($garantia_id)["data"];
$articulo_id = $garantia_registro["tb_articulo_id"];
$creditotipo_id = 1;

if ($gasto_id > 0) {
    $arr[0]["column_name"] = "gg.tb_gastogar_id";
    $arr[0]["param0"] = $gasto_id;
    $arr[0]["datatype"] = "INT";

    $arr[1]["conector"] = "AND";
    $arr[1]["column_name"] = "eg.tb_egreso_xac";
    $arr[1]["param1"] = 1;
    $arr[1]["datatype"] = "INT";

    $inner[0]['alias_columnasparaver'] = 'eg.tb_egreso_det';
    $inner[0]['tipo_union'] = 'LEFT';
    $inner[0]['tabla_alias'] = 'tb_egreso eg';
    $inner[0]['columna_enlace'] = 'gg.tb_egreso_idfk';
    $inner[0]['alias_columnaPK'] = 'eg.tb_egreso_id';

    $inner[1]['alias_columnasparaver'] = 'ing.tb_ingreso_det';
    $inner[1]['tipo_union'] = 'LEFT';
    $inner[1]['tabla_alias'] = 'tb_ingreso ing';
    $inner[1]['columna_enlace'] = 'gg.tb_ingreso_idfk';
    $inner[1]['alias_columnaPK'] = 'ing.tb_ingreso_id';
    //hacer una busqueda del gasto_id para mostrar o editar o eliminar
    $res = $oGastogar->mostrarUno($arr, $inner);
    if ($res["estado"] == 1) {
        $res = $res["data"];
        $falla_id = $res["tb_falla_idfk"];
        $moneda_id = intval($res["tb_moneda_idfk"]);
        $monto_gasto = mostrar_moneda($res["tb_gastogar_monto"]);
        $coment = $res["tb_gastogar_coment"];
        $genera_egreso = ($res['tb_gastogar_estado'] == 1) ? 'checked' : '';
        $ingreso_id = intval($res["tb_ingreso_idfk"]);
        $egreso_id = intval($res["tb_egreso_idfk"]);

        if (!empty($genera_egreso)) {
            $fecha_gasto = mostrar_fecha($res["tb_gastogar_fecha"]);
            $empresa_id = $res["tb_empresa_idfk"];
            $forma_pago = intval($res['tb_gastogar_formapago']);

            if ($forma_pago == 2) {
                $numope = $res['tb_gastogar_numope'];
            }
            $emite_doc_sunat = (intval($res["tb_gastogar_emitedocsunat"]) == 1) ? 'checked' : '';
            if (!empty($emite_doc_sunat)) {
                $doc_sunat = $res["tb_gastogar_documentossunat"];
                $proveedor_id = $res['tb_proveedor_idfk'];
                $proveedor_doc = $res['tb_proveedor_doc'];
                $proveedor_nom = $res['tb_proveedor_nom'];

                require_once('../compracontadetalle/Compracontadetalle.class.php');
                $oCCDetalle = new Compracontadetalle();

                $join[0]['tipo_union'] = 'INNER';
                $join[0]['tabla_alias'] = 'tb_gastogar gg';
                $join[0]['alias_columnasparaver'] = 'gg.*';
                $join[0]['columna_enlace'] = 'ccd.tb_gastogar_idfk';
                $join[0]['alias_columnaPK'] = 'gg.tb_gastogar_id';

                $where[0]['column_name'] = 'ccd.tb_compracontadetalle_xac';
                $where[0]['param0'] = 1;
                $where[0]['datatype'] = 'INT';

                $where[1]['conector'] = 'AND';
                $where[1]['column_name'] = 'ccd.tb_gastogar_idfk';
                $where[1]['param1'] = $gasto_id;
                $where[1]['datatype'] = 'INT';

                $buscar_ccd = $oCCDetalle->mostrarUno($where, $join);
            }
        }
    }
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_gastogar_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" id="modalSizeForm" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4>EN DESARROLLO. PRONTO ACTIVO</h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo . "<b>" . $garantia_registro["tb_garantia_pro"] . "</b>" ?></h4>
                </div>
                <form id="form_gastos" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action_form_gastogar" id="action_form_gastogar" value="<?php echo $action ?>">
                        <input type="hidden" name="action_form_gastogar2" id="action_form_gastogar2" value="<?php echo $action_2 ?>">
                        <input type="hidden" name="hdd_garantia_id" id="hdd_garantia_id" value="<?php echo $garantia_id; ?>">
                        <input type="hidden" name="hdd_garantia_nom" id="hdd_garantia_nom" value="<?php echo $garantia_registro["tb_garantia_pro"] ?>">
                        <input type="hidden" name="hdd_gastogar_id" id="hdd_gastogar_id" value="<?php echo $gasto_id; ?>">
                        <input type="hidden" id="hdd_gastogar_registro" name="hdd_gastogar_registro" value='<?php echo json_encode($res); ?>'>
                        <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $_POST["credito_id"] ?>">
                        <input type="hidden" name="hdd_creditotipo_id" id="hdd_creditotipo_id" value="<?php echo $_POST["credito_tipo"] ?>">
                        <input type="hidden" name="hdd_empresa_default" id="hdd_empresa_default" value="<?php echo $empresa_id; ?>">
                        <input type="hidden" name="hdd_ingreso_id" id="hdd_ingreso_id" value="<?php echo $ingreso_id; ?>">
                        <input type="hidden" name="hdd_egreso_id" id="hdd_egreso_id" value="<?php echo $egreso_id ?>">

                        <div class="box box-primary shadow">

                            <div class="row" id="row_articulo">
                                <input type="hidden" name="hdd_articulo_id" id="hdd_articulo_id" value="<?php echo $articulo_id; ?>">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="cbo_articulo">Tipo artículo:</label>
                                        <select class="form-control mayus input-sm" name="cbo_articulo" id="cbo_articulo">
                                            <?php require_once('../articulo/articulo_select.php'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6" id="col_add_articulo" style="display: none;">
                                    <div class="form-group">
                                        <label for="txt_articulo_nom">Nombre de tipo de artículo:</label>
                                        <input type="text" name="txt_articulo_nom" id="txt_articulo_nom" class="form-control mayus input-sm" placeholder="Nombre del tipo artículo">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <input type="hidden" name="hdd_falla_id" id="hdd_falla_id" value="<?php echo $falla_id; ?>">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="cbo_falla">Concepto/Falla:</label>
                                        <div id="falla-grp" class="input-group">
                                            <select class="form-control mayus input-sm" name="cbo_falla" id="cbo_falla">
                                            </select>
                                            <span class="input-group-btn">
                                                <button id="btn_add_falla" class="btn btn-primary btn-sm" type="button" onclick="añadir_falla()" title="AÑADIR FALLA">
                                                    <span class="fa fa-plus icon"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="col_add_falla" style="display: none;">
                                    <div class="form-group">
                                        <label for="txt_falla_nom">Nombre concepto/Falla:</label>
                                        <input type="text" name="txt_falla_nom" id="txt_falla_nom" class="form-control mayus input-sm" placeholder="Concepto o Falla del artículo">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4 col-md-2">
                                    <div class="form-group">
                                        <label for="cmb_moneda_id" class="control-label">Moneda<?php echo ($action == 'modificar' && $genera_egreso == 'checked' && $action_2 != 'modificar_doc') ? '*' : ''; ?>:</label>
                                        <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id" style="width: 70px;">
                                            <?php require_once('../moneda/moneda_select.php') ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-md-3">
                                    <div class="form-group">
                                        <label for="txt_monto">Monto<?php echo ($action == 'modificar' && $genera_egreso == 'checked' && $action_2 != 'modificar_doc') ? '*' : ''; ?>:</label>
                                        <input type="text" name="txt_monto" id="txt_monto" class="form-control input-sm mayus moneda" placeholder="0.00" value="<?php echo $monto_gasto; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="che_generar_egreso" class="control-label" style="color:white;"></label>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="che_generar_egreso" name="che_generar_egreso" class="flat-green" style="position: relative;" value="1" <?php echo $genera_egreso; ?>><b>&nbsp; Generar egreso</b>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 genera_egreso" <?php if ($genera_egreso != 'checked') echo 'style="display: none;"' ?>>
                                    <label for="txt_fecha_gasto">Fecha de pago:</label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control input-sm" placeholder="DD-MM-AAAA" name="txt_fecha_gasto" id="txt_fecha_gasto" value="<?php echo $fecha_gasto; ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row genera_egreso" <?php if ($genera_egreso != 'checked') echo 'style="display: none;"' ?>>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cbo_sede">Sede del egreso<?php echo ($action == 'modificar' && $genera_egreso == 'checked' && $action_2 != 'modificar_doc') ? '*' : ''; ?>:</label>
                                        <select class="form-control mayus input-sm" name="cbo_sede" id="cbo_sede">
                                            <?php require_once('../empresa/empresa_select.php'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="hidden" name="hdd_gastogar_formapago" id="hdd_gastogar_formapago" value="<?php echo $forma_pago; ?>">
                                    <label for="cbo_forma_egreso">Forma de egreso<?php echo ($action == 'modificar' && $genera_egreso == 'checked' && $action_2 != 'modificar_doc') ? '*' : ''; ?>:</label>
                                    <select name="cbo_forma_egreso" id="cbo_forma_egreso" class="form-control input-sm mayus">
                                        <option value="1">Egreso caja Efectivo</option>
                                        <option value="2">Ingreso/Egreso Banco</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group num-ope" style="display: none;">
                                    <label for="txt_numope">N° Operación<?php echo ($action == 'modificar' && $genera_egreso == 'checked' && $action_2 != 'modificar_doc') ? '*' : ''; ?>:</label>
                                    <input type="text" name="txt_numope" id="txt_numope" class="form-control input-sm mayus" placeholder="Num. operación" value="<?php echo $numope; ?>">
                                </div>
                            </div>

                            <div id="div_egresado" class="row">
                                <div class="col-md-12">
                                    <label style="color: green; font-size: 11px;">
                                        <?php
                                            if($action == "eliminar"){
                                                $mens = "- Se eliminarán también sus movimientos de caja: ";
                                                if($ingreso_id > 0){
                                                    $mens.= "ingreso_id: $ingreso_id, ";
                                                }
                                                if($egreso_id > 0){
                                                    $mens.= "egreso_id: $egreso_id.";
                                                }
                                                echo $mens;
                                            } elseif($action == "modificar") {
                                                $mens = "* Si modifica estos campos, se eliminará lo siguiente (";
                                                if($ingreso_id > 0){
                                                    $mens.= "ingreso_id: $ingreso_id, ";
                                                }
                                                if($egreso_id > 0){
                                                    $mens.= "egreso_id: $egreso_id";
                                                }
                                                $mens .= "), y se generará de nuevo";
                                                echo $mens;
                                            }
                                        ?>
                                    </label>
                                </div>
                            </div>

                            <div class="row genera_egreso" <?php if ($genera_egreso != 'checked') echo 'style="display: none;"' ?>>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="che_emite_docsunat" class="control-label" style="color:white;"></label>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="che_emite_docsunat" name="che_emite_docsunat" class="flat-green" style="position: relative;" value="1" <?php echo $emite_doc_sunat; ?>><b>&nbsp; Emite comprobante<?php echo ($emite_doc_sunat == 'checked' && $action == 'modificar' && $buscar_ccd['estado']==1) ? '**' : ''; ?></b>
                                            </label>
                                        </div>
                                        <input type="hidden" name="hdd_gastoanexadoenccd" id="hdd_gastoanexadoenccd" value="<?php echo $buscar_ccd['estado']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 emite_doc_sunat" <?php if ($emite_doc_sunat != 'checked') echo 'style="display: none;"' ?>>
                                    <div class="form-group">
                                        <label for="txt_documento_sunat">Documento (Serie-Numero)<?php echo ($emite_doc_sunat == 'checked' && $action == 'modificar' && $buscar_ccd['estado'] == 1) ? '**' : ''; ?>:</label>
                                        <input type="text" name="txt_documento_sunat" id="txt_documento_sunat" placeholder="E001-00000001" class="form-control mayus input-sm" value="<?php echo $doc_sunat ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row emite_doc_sunat" <?php if ($emite_doc_sunat != 'checked') echo 'style="display: none;"' ?>>
                                <div class="col-md-5 form-group">
                                    <input type="hidden" id="hdd_proveedor_id" name="hdd_proveedor_id" value='<?php echo $proveedor_id ?>'>
                                    <label for="txt_proveedor_doc">Ruc Proveedor<?php echo ($emite_doc_sunat == 'checked' && $action == 'modificar' && $buscar_ccd['estado'] == 1) ? '**' : ''; ?>:</label>
                                    <input type="text" id="txt_proveedor_doc" name="txt_proveedor_doc" placeholder="Escribe para buscar proveedor" class="form-control mayus input-sm" value="<?php echo $proveedor_doc; ?>">
                                </div>
                                <div class="col-md-6 form-group" style="width: 48%;">
                                    <label for="txt_proveedor_nom">Proveedor<?php echo ($emite_doc_sunat == 'checked' && $action == 'modificar' && $buscar_ccd['estado'] == 1) ? '**' : ''; ?>:</label><br>
                                    <input type="text" id="txt_proveedor_nom" name="txt_proveedor_nom" placeholder="Escribe para buscar proveedor" class="form-control mayus input-sm" value="<?php echo $proveedor_nom ?>">
                                </div>
                                <div class="col-md-1">
                                    <label for="btn-limpiar" style="color:white;">l</label>
                                    <button id="btn-limpiar" class="btn btn-primary btn-sm" type="button" onclick="limpiar_proveedor()" title="LIMPIAR DATOS DEL PROVEEDOR">
                                        <span class="fa fa-eraser icon"></span>
                                    </button>
                                </div>
                            </div>

                            <div id="div_anexado" class="row">
                                <div class="col-md-12">
                                    <label style="color: green; font-size: 11px;">
                                        <?php
                                            if($action == "eliminar"){
                                                echo "- Este documento está anexado en el documento de Id {$buscar_ccd['data']['tb_compracontadoc_idfk']}, en el detalle N° {$buscar_ccd['data']['tb_compracontadetalle_orden']}. Debe limpiar el detalle <a href=\"compracontadoc\" target=\"_blank\">AQUI</a>";
                                            } elseif($action == "modificar") {
                                                echo "** Si quita o modifica estos campos, debe modificar manualmente en el modulo <a href=\"compracontadoc\" target=\"_blank\">Documentos de compra contable</a>, en documento de Id {$buscar_ccd['data']['tb_compracontadoc_idfk']}, dado que está anexado en el detalle {$buscar_ccd['data']['tb_compracontadetalle_orden']}.";
                                            }
                                        ?>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txt_comentario">Descripcion:</label>
                                    <textarea class="form-control mayus input-sm" name="txt_comentario" id="txt_comentario" rows="3" cols="85" placeholder="DESCRIPCION DETALLADA"><?php echo $coment; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="gastogar_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_gastogar">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_gastogar">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gastogar/gastogar_form.js?ver=617856682'; ?>"></script>
<script>
    function adjustModalSizeForm() {
        var modalDialogForm = document.getElementById('modalSizeForm');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogForm.classList.remove('modal-lg');
            modalDialogForm.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogForm.classList.remove('modal-xl');
            modalDialogForm.classList.add('modal-lg');
        }
    }

    adjustModalSizeForm();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizeForm);
</script>