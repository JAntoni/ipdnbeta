<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../gastogar/Gastogar.class.php');
$oGastogar = new Gastogar();
require_once('../garantia/Garantia.class.php');
require_once('../articulo/Articulo.class.php');
require_once('../falla/Falla.class.php');
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../cajaoperacion/Cajaoperacion.class.php');
$oCajaoperacion = new Cajaoperacion();
require_once('../compracontadetalle/Compracontadetalle.class.php');
$oCCDetalle = new Compracontadetalle();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action_form_gastogar'];
$gastogar_id = intval($_POST['hdd_gastogar_id']);
$garantia_idfk = intval($_POST["hdd_garantia_id"]);
$garantia_nom = trim($_POST["hdd_garantia_nom"]);
$usuario_id = intval($_SESSION['usuario_id']);
$fecha_gasto = $_POST["txt_fecha_gasto"];
$estado_pago = intval($_POST['che_generar_egreso']); //si/no genera egreso
$moneda = intval($_POST["cmb_moneda_id"]);
$moneda_nom = $_POST['moneda_nom_selected'];
$monto_gasto = moneda_mysql($_POST["txt_monto"]);
$emite_doc_sunat = intval($_POST["che_emite_docsunat"]);
$registro_origin = (array) json_decode($_POST['hdd_gastogar_registro']);

$oGastogar->tb_gastogar_usumod = $usuario_id;
$data['estado'] = 0; $data['mensaje'] = "";

$caja_aperturada = 0;
$esta_anexado_ccd = 0;
$tipocambio_registrado = 1;
$valor_tipocambio_venta = 1;
$deposito_registrado = 0;

if ($moneda == 2) {
	require_once('../monedacambio/Monedacambio.class.php');
	$oTipcam = new Monedacambio();
	$consulta_tipcam = $oTipcam->consultar(fecha_mysql($fecha_gasto));
	$tipocambio_registrado = $consulta_tipcam['estado'];
	$valor_tipocambio_venta = $tipocambio_registrado == 1 ? $consulta_tipcam['data']['tb_monedacambio_val'] : 0;
}
if ($estado_pago == 1) {
	$sede_id = intval($_POST["cbo_sede"]);
	$sede_nom = strtoupper($_POST['cbo_empresa_selected_nom']);

	$res1 = $oCajaoperacion->mostrar_fecha_hoy(fecha_mysql($fecha_gasto), $sede_id);
	if ($res1['estado'] == 1) {
		$res1 = $res1['data'];
		if ($res1['tb_cajaoperacion_apertura_est'] == 1 && empty($res1['tb_cajaoperacion_cierre_est'])) { //estado de cerrado debe ser 0. si es 1 quiere decir q ya cerraron caja
			$caja_aperturada = 1;
		}
	}

	$forma_pago	= intval($_POST['cbo_forma_egreso']);//si elige I/E por banco
	if($forma_pago == 2){
		$num_operacion = $_POST['txt_numope'];
		$consulta_deposito = $oDeposito->listar_depositos_paragasto($num_operacion);
		$deposito_registrado = $consulta_deposito['estado'];
		$deposito = $consulta_deposito['data'];
		unset($consulta_deposito);

		if ($deposito_registrado == 1) {
			$moneda_deposito_nom = $deposito['tb_moneda_nom'];
			//la moneda del gasto y del deposito deben coincidir
			if($moneda != intval($deposito['tb_moneda_id'])){
				$deposito_registrado = 3;
			} else {
				//verificar que el monto del gasto no sobrepase el monto de deposito
				$importe_ingresos_anteriores = 0;
				$dts = $oIngreso->lista_ingresos_num_operacion($num_operacion);
				if ($dts['estado'] == 1) {
					foreach ($dts['data'] as $key => $dt) {
						$tb_ingreso_imp = moneda_mysql(floatval($dt['tb_ingreso_imp']));
						if($dt['tb_ingreso_id'] != $registro_origin['tb_ingreso_idfk']){
							$importe_ingresos_anteriores += $tb_ingreso_imp;
						}
					}
				}
				unset($dts);

				$monto_deposito = abs($deposito['tb_deposito_mon']);
				$monto_puede_usar = moneda_mysql($monto_deposito - $importe_ingresos_anteriores);

				if ($monto_gasto > $monto_puede_usar) {
					$deposito_registrado = 2;
				}
			}
		}
	}

	if ($gastogar_id > 0) {
		$join[0]['tipo_union'] = 'INNER';
		$join[0]['tabla_alias'] = 'tb_gastogar gg';
		$join[0]['alias_columnasparaver'] = 'gg.*';
		$join[0]['columna_enlace'] = 'ccd.tb_gastogar_idfk';
		$join[0]['alias_columnaPK'] = 'gg.tb_gastogar_id';
	
		$where[0]['column_name'] = 'ccd.tb_compracontadetalle_xac';
		$where[0]['param0'] = 1;
		$where[0]['datatype'] = 'INT';
	
		$where[1]['conector'] = 'AND';
		$where[1]['column_name'] = 'ccd.tb_gastogar_idfk';
		$where[1]['param1'] = $gastogar_id;
		$where[1]['datatype'] = 'INT';
	
		$buscar_ccd = $oCCDetalle->mostrarUno($where, $join);
	
		if ($buscar_ccd['estado'] == 1) {
			$buscar_ccd = $buscar_ccd['data'];
			$esta_anexado_ccd = 1;
		}
	}
}

if($action == 'eliminar'){
	if($estado_pago == 1){
		if($caja_aperturada == 0){
			$data['mensaje'] = "● No puede $action este gasto. Verifique la apertura de caja del día $fecha_gasto en sede $sede_nom";
			echo json_encode($data); exit();
		}
		if($emite_doc_sunat == 1 && $esta_anexado_ccd == 1){
			$data['mensaje'] = "● No puede $action este gasto, porque está anexado en el detalle numero {$buscar_ccd['tb_compracontadetalle_orden']} del documento id {$buscar_ccd['tb_compracontadoc_idfk']} - {$buscar_ccd['tb_gastogar_documentossunat']}";
			echo json_encode($data); exit();
		}
		if($forma_pago == 2){
			//eliminar ingreso
			$oIngreso->ingreso_id = $registro_origin['tb_ingreso_idfk'];
			if($oIngreso->eliminar_2() > 0){
				$data['mensaje'] = "● Ingreso de caja eliminado correctamente. Ingreso id: $oIngreso->ingreso_id. <br>";
				unset($oIngreso->ingreso_id);
			} else {
				$data['mensaje'] = "● No puede $action este gasto. Hubo un error al $action el ingreso de caja con id $oIngreso->ingreso_id.";
				echo json_encode($data); exit();
			}
		}
		//eliminar egreso
		$oEgreso->egreso_id = $registro_origin['tb_egreso_idfk'];
		if ($oEgreso->eliminar_2()) {
			$data['mensaje'] .= "● Egreso eliminado correctamente. Egreso id: $oEgreso->egreso_id. <br>";
		} else {
			$data['mensaje'] .= "● No puede $action este gasto. Hubo un error al $action el egreso de caja con id $oEgreso->egreso_id.";
			echo json_encode($data); exit();
		}
	}
	$oGastogar->tb_gastogar_id = $gastogar_id;
	if ($oGastogar->eliminar()) {
		$data['estado'] = 1;
		$data['mensaje'] .= "● Se eliminó el gasto $gastogar_id de la garantía $garantia_idfk - $garantia_nom";

		//insertar historial
		$oHist->setTbHistUsureg($usuario_id);
		$oHist->setTbHistNomTabla('tb_gastogar');
		$oHist->setTbHistRegmodid($gastogar_id);
		$oHist->setTbHistDet("Eliminó el gasto id: $gastogar_id de la garantia: " . $garantia_idfk . ' - ' . $garantia_nom);
		$oHist->insertar();
	} else {
		$data['mensaje'] .= "● Hubo un error al $action este gasto.";
	}
} elseif($action == 'verificar_apertura_caja'){
	$data['estado'] = $caja_aperturada;
	if($caja_aperturada == 0){
		$data['mensaje'] = "No has aperturado o ya está cerrada la caja de este día $fecha_gasto para la sede $sede_nom";
	}
} elseif ($action == 'verificar_tipocambio') {
	$data['estado'] = $tipocambio_registrado;
	$data['mensaje'] .= "No hay tipo de cambio registrado en el sistema para fecha de $fecha_gasto";
} elseif ($action == 'verificar_numope') {
	//$deposito_registrado 0:no existe el num operacion, 1: negacion de 0 y 2(todo correcto), 2: el monto de gasto es mayor al monto usable del deposito, 3: la moneda de deposito y gasto son diferentes
	$data['estado'] = $deposito_registrado;
	if($deposito_registrado == 0){
		$data['mensaje'] .=  'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor';
	} elseif ($deposito_registrado == 2){
		$data['mensaje'] .= 'El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. Monto ingresado: '.$moneda_nom.mostrar_moneda($monto_gasto).', monto disponible: '.$moneda_deposito_nom.mostrar_moneda($monto_puede_usar).', TOTAL DEL DEPOSITO: '.$moneda_deposito_nom.mostrar_moneda($monto_deposito);
	} elseif ($deposito_registrado == 3){
		$data['mensaje'] .= "La moneda del gasto ($moneda_nom) y la moneda del depósito ($moneda_deposito_nom) son diferentes";
	}
} else {
	if($estado_pago == 1 && $caja_aperturada == 0){
		$data['mensaje'] .= "● No se registró el egreso. Verifique la apertura de caja del día de hoy $fecha_gasto en la sede $sede_nom";
		echo json_encode($data); exit();
	}
	if($tipocambio_registrado == 0){
		$data['mensaje'] .= "● No hay tipo de cambio registrado en el sistema para fecha de $fecha_gasto";
		echo json_encode($data); exit();
	}
	if ($estado_pago == 1 && $forma_pago == 2 && $deposito_registrado != 1) {
		if($deposito_registrado == 0){
			$data['mensaje'] =  '● El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor';
		} elseif ($deposito_registrado == 2){
			$data['mensaje'] = '● El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. Monto ingresado: '.$moneda_nom.mostrar_moneda($monto_gasto).', monto disponible: '.$moneda_deposito_nom.mostrar_moneda($monto_puede_usar).', TOTAL DEL DEPOSITO: '.$moneda_deposito_nom.mostrar_moneda($monto_deposito);
		} elseif ($deposito_registrado == 3){
			$data['mensaje'] = "● La moneda del gasto ($moneda_nom) y la moneda del depósito ($moneda_deposito_nom) son diferentes";
		}
		echo json_encode($data); exit();
	}
	
	$proveedor_id = intval($_POST['hdd_proveedor_id']);
	$oGastogar->tb_gastogar_documentossunat = null;
	$oGastogar->tb_proveedor_idfk = null;
	if($estado_pago == 1){
		$oGastogar->tb_gastogar_fecha			= fecha_mysql($fecha_gasto);
		$oGastogar->tb_empresa_idfk				= $sede_id;
		$oGastogar->tb_gastogar_formapago		= $forma_pago;
		$oGastogar->tb_gastogar_emitedocsunat	= $emite_doc_sunat;

		if ($forma_pago == 2) {
			$oGastogar->tb_gastogar_numope	= $num_operacion;
			//Nos guiamos de modulo (flujocaja -> boton caja banco)
			//BAnco 2: interbank pero no tiene una subcuenta creada.
			//BANCO_ID : 1 = bcp, 3=BBVA, 4=otros bancos
			//SUBCUENTA_ID: 147=BCP, 148=BBVA, (EN LOCAL 181)?=OTROS
			if(is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BCP'))){
				$banco_id = 1;
				$subcuenta_id = 147;
			} elseif (is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BBVA'))){
				$banco_id = 3;
				$subcuenta_id = 148;
			} else {
				$banco_id = 4;
				$subcuenta_id = 183;
			}
		}
		
		if($emite_doc_sunat == 1){
			$factura_ingresada = strtoupper($_POST["txt_documento_sunat"]);

			$oGastogar->tb_gastogar_documentossunat = $factura_ingresada == '(PENDIENTE DE RECIBIR FACTURA)' ? '' : $factura_ingresada;
			$oGastogar->tb_proveedor_idfk = $proveedor_id;
		}
	}

	$asignar_articulo_garantia = intval($_POST["cbo_articulo_enabled"]);
	$articulo_idfk = intval($_POST['cbo_articulo']);
	$falla_idfk = intval($_POST['cbo_falla']);
	$coment = strtoupper(trim($_POST["txt_comentario"]));

	if ($articulo_idfk == 0) {
		$articulo_nombre = trim(strtoupper($_POST['txt_articulo_nom']));
		$oArticulo = new Articulo();
		$oArticulo->tb_articulo_nombre = $articulo_nombre;
		$oArticulo->tb_articulo_usureg = $usuario_id;
		$oArticulo->tb_articulo_usumod = $usuario_id;

		$result = $oArticulo->insertar();
		if ($result['estado'] == 0) {
			$data['mensaje'] = "Error en el registro de articulo $articulo_nombre.";
			echo json_encode($data);
			exit();
		} else {
			$data['mensaje'] = "● Se registró el artículo $articulo_nombre.<br>";
			$articulo_idfk = $result['nuevo'];
		}
	}
	if($asignar_articulo_garantia == 1){
		$oGarantia = new Garantia();
		$resp = $oGarantia->modificar_campo($garantia_idfk, "tb_articulo_id", $articulo_idfk, "INT");

		if($resp == 1){
			$data['mensaje'] .= "● Se asignó tipo de artículo a la garantía.<br>";
		} else {
			$data['mensaje'] = "● Error en la asignacion de articulo a la garantía.";
			echo json_encode($data);
			exit();
		}
	}
	if($falla_idfk == 0){
		$falla_nombre = trim(strtoupper($_POST['txt_falla_nom']));
		$oFalla = new Falla();
		$oFalla->tb_articulo_id = $articulo_idfk;
		$oFalla->tb_falla_nombre = $falla_nombre;
		$oFalla->tb_falla_usureg = $usuario_id;
		$oFalla->tb_falla_usumod = $usuario_id;

		$result = $oFalla->insertar();
		if ($result['estado'] == 0) {
			$data['mensaje'] .= "● Error en el registro de la falla $falla_nombre.";
			echo json_encode($data);
			exit();
		} else {
			$data['mensaje'] .= "● Se registró la falla $falla_nombre.<br>";
			$falla_idfk = $result['nuevo'];
		}
	}

	$oGastogar->tb_credito_idfk		= intval($_POST['hdd_credito_id']);
	$oGastogar->tb_creditotipo_idfk	= intval($_POST['hdd_creditotipo_id']);
	$oGastogar->tb_articulo_id		= $articulo_idfk;
	$oGastogar->tb_falla_idfk		= $falla_idfk;
	$oGastogar->tb_moneda_idfk		= $moneda;
	$oGastogar->tb_gastogar_monto	= $monto_gasto;
	$oGastogar->tb_gastogar_estado	= $estado_pago; //por defecto cero, luego se verifica la caja y se hace egreso y se modifica estado a 1
	$oGastogar->tb_gastogar_coment	= $coment;

	if ($action == 'insertar') {
		$oGastogar->tb_gastogar_usureg = $usuario_id;
		$oGastogar->tb_garantia_idfk = $garantia_idfk;
		
		//si llega aquí es pq caja aperturada o no registra un egreso aun
		if ($estado_pago == 1 && $forma_pago == 1) {
			//intentar el egreso de dinero de caja | si se realiza el egreso -> se registra el gasto.
			$oEgreso->egreso_usureg	= $usuario_id;
			$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
			$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS
			$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
			$oEgreso->egreso_det	= $coment;
			$oEgreso->moneda_id		= $moneda;
			$oEgreso->egreso_tipcam = $valor_tipocambio_venta;
			$oEgreso->egreso_imp	= $monto_gasto;//consultar si los egresos en $ guardan importe en $ o convierten a S/.
			$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
			$oEgreso->cuenta_id		= 16;//cuenta de gastos operativos
			$oEgreso->subcuenta_id	= 54;//subcuenta de OTROS
			$oEgreso->proveedor_id = 1;//por defecto 1: SISTEMA
			if($emite_doc_sunat == 1){
				$oEgreso->proveedor_id = $proveedor_id;
			}
			$oEgreso->cliente_id = 0;//id de cliente solo cuando el egreso se entrega a un cliente.
			$oEgreso->usuario_id = 0;//id de colaborador solo cuando el egreso es para pago a un colaborador.
			$oEgreso->caja_id = 1; // 1: CAJA
			$oEgreso->modulo_id = 0;//se definen en cero segun registro egreso id=52654
			$oEgreso->egreso_modide = 0;//se definen en cero segun registro egreso id=52654
			$oEgreso->empresa_id = $sede_id;
			$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

			$resultado = $oEgreso->insertar();
			if ($resultado['estado'] > 0) {
				$egreso_id = $resultado['egreso_id'];
				$data['mensaje'] .= "● Egreso por Caja efectivo registrado correctamente en sede $sede_nom. Id egreso: $egreso_id.<br>";
				unset($resultado);
			} else {
				$data['mensaje'] .= "● Error en el registro de egreso, intente nuevamente.";
				echo json_encode($data);
				exit();
			}
		} elseif($estado_pago == 1) { //forma pago es 2
			//realizar ingreso y egreso por banco

			$ing_det = "INGRESO DE CAJA POR BANCO = Ingreso desde la siguente cuenta: [ {$deposito['tb_cuentadeposito_nom']} ], la fecha de depósito fue: [ ".mostrar_fecha($deposito['tb_deposito_fec'])." ], el N° de operación fue: [ $num_operacion ]. ";
			$ing_det.= "Se usó [ $moneda_nom$monto_gasto ] de [ {$deposito['tb_moneda_nom']}". abs($deposito['tb_deposito_mon']) ." ], ";
			$ing_det.= "por concepto de $coment";
			//si falla se detiene y sale
			$oIngreso->ingreso_usureg = $usuario_id;
			$oIngreso->ingreso_fec = fecha_mysql($fecha_gasto);
			$oIngreso->documento_id = 8;//OI - OTROS INGRESOS
			$oIngreso->ingreso_numdoc = '';
			$oIngreso->ingreso_det = $ing_det;
			$oIngreso->ingreso_imp = $monto_gasto;
			$oIngreso->cuenta_id = 43;//INGRESO/EGRESO POR BANCO
			$oIngreso->subcuenta_id = $subcuenta_id;
			$oIngreso->cliente_id = 1144;// id del cliente q hace el ingreso por banco: INVERSIONES Y PRESTAMOS DEL NORTE SAC
			$oIngreso->caja_id = 1;//CAJA
			$oIngreso->moneda_id = $moneda;
			//valores que pueden ser cambiantes según requerimiento de ingreso
			$oIngreso->modulo_id = 0;
			$oIngreso->ingreso_modide = 0;
			$oIngreso->empresa_id = $sede_id;
			$oIngreso->ingreso_fecdep = $deposito['tb_deposito_fec'];
			$oIngreso->ingreso_numope = $num_operacion;
			$oIngreso->ingreso_mondep = $monto_gasto;
			$oIngreso->ingreso_comi = 0;
			$oIngreso->cuentadeposito_id = $deposito['tb_cuentadeposito_id'];
			$oIngreso->banco_id = $banco_id;
			$oIngreso->ingreso_ap = 0;//acuerdo de pago, 0
			$oIngreso->ingreso_detex = '';
			
			$result = $oIngreso->insertar();

			if($result['estado'] == 1) { //si el ingreso fue correcto ahora toca hacer el egreso
				$ingreso_id = $result['ingreso_id'];
				$data['mensaje'] .= "● Ingreso por banco registrado correctamente en sede $sede_nom. Id ingreso: $ingreso_id.<br>";
				unset($result);

				$egr_det = "EGRESO DE CAJA POR BANCO de obs: [ $coment ]. Egreso desde la siguiente cuenta [ {$deposito['tb_cuentadeposito_nom']} ], la fecha de depósito fue: [ ".mostrar_fecha($deposito['tb_deposito_fec'])." ], el N° de operación fue: [ $num_operacion ]. ";
				$egr_det.= "Se usó [ $moneda_nom$monto_gasto ] de [ {$deposito['tb_moneda_nom']}". abs($deposito['tb_deposito_mon']) ." ]";
				$oEgreso->egreso_usureg	= $usuario_id;
				$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
				$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS
				$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
				$oEgreso->egreso_det	= $egr_det;
				$oEgreso->moneda_id		= $moneda;
				$oEgreso->egreso_tipcam = $valor_tipocambio_venta;
				$oEgreso->egreso_imp	= $monto_gasto;
				$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
				$oEgreso->cuenta_id		= 16; //cuenta de gastos operativos
				$oEgreso->subcuenta_id	= 54; //subcuenta de OTROS
				$oEgreso->proveedor_id = 1; //por defecto 1: SISTEMA
				if ($emite_doc_sunat == 1) {
					$oEgreso->proveedor_id = $proveedor_id;
				}
				$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
				$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
				$oEgreso->caja_id = 1; // 1: CAJA
				$oEgreso->modulo_id = 0; //se definen en cero segun registro egreso id=52654
				$oEgreso->egreso_modide = 0; //se definen en cero segun registro egreso id=52654
				$oEgreso->empresa_id = $sede_id;
				$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

				$result = $oEgreso->insertar();
				if ($result['estado'] > 0) {
					$egreso_id = $result['egreso_id'];
					$data['mensaje'] .= "● Egreso por Banco registrado correctamente en sede $sede_nom. Id egreso: $egreso_id.<br>";
					unset($result);
				} else {
					$data['mensaje'] .= "● NO SE REGISTRÓ EL GASTO: error en el registro de egreso por banco";
					echo json_encode($data); exit();
				}
			} else {
				$data['mensaje'] .= "● NO SE REGISTRÓ EL GASTO: error en el registro de ingreso por banco";
				echo json_encode($data); exit();
			}
		}
		//si el ingreso/egreso falla se detiene y sale, de lo contrario se registra ingreso/egreso y continúa
		//si el estado de pago es 0 solo se registra gastogar
		$res = $oGastogar->insertar();

		if ($res['estado'] == 1) {
			$data['estado'] = 1;
			$data['mensaje'] .= "● ".$res['mensaje'] . '. Su ID: ' . $res['nuevo'];
			$data['nuevo'] = $res['nuevo'];

			if(!empty($ingreso_id))
				$oGastogar->modificar_campo($res['nuevo'], 'tb_ingreso_idfk', $ingreso_id, 'INT');
			if(!empty($egreso_id))
				$oGastogar->modificar_campo($res['nuevo'], 'tb_egreso_idfk', $egreso_id, 'INT');

			//insertar historial
			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('tb_gastogar');
			$oHist->setTbHistRegmodid($data['nuevo']);
			$oHist->setTbHistDet("Registró un gasto a la garantia $garantia_idfk - $garantia_nom. El id es {$data['nuevo']}. Comentario: $coment");
			$oHist->insertar();
		}
	}
	elseif ($action == "modificar"){
		$evaluar[0] = intval($falla_idfk == $registro_origin["tb_falla_idfk"]);
		$evaluar[1] = intval($moneda == $registro_origin["tb_moneda_idfk"]);
		$evaluar[2] = intval($monto_gasto == $registro_origin["tb_gastogar_monto"]);
		$evaluar[3] = intval($estado_pago == $registro_origin["tb_gastogar_estado"]);

		$evaluar[4] = 1; $evaluar[5] = 1; $evaluar[6] = 1; $evaluar[7] = 1; $evaluar[8] = 1; $evaluar[9] = 1;
		$evaluar[10] = 1; $evaluar[11] = 1; $evaluar[12] = 1; $evaluar[13] = 1;
		if ($estado_pago == 1) {
			$evaluar[4] = intval(fecha_mysql($fecha_gasto) == $registro_origin["tb_gastogar_fecha"]);
			$evaluar[5] = intval($sede_id == $registro_origin["tb_empresa_idfk"]);
			$evaluar[6] = intval($forma_pago == $registro_origin["tb_gastogar_formapago"]);
			$evaluar[7] = intval($num_operacion == $registro_origin["tb_gastogar_numope"]);
			$evaluar[8] = intval($emite_doc_sunat == $registro_origin["tb_gastogar_emitedocsunat"]);
			$evaluar[9] = intval($oGastogar->tb_gastogar_documentossunat == $registro_origin["tb_gastogar_documentossunat"]);
			$evaluar[10] = intval($proveedor_id == intval($registro_origin["tb_proveedor_idfk"]));
			$evaluar[11] = intval($coment == $registro_origin["tb_gastogar_coment"]);
			if(($registro_origin["tb_gastogar_estado"] == 1 && ($evaluar[1] != 1 || $evaluar[2] != 1 || $evaluar[5] != 1 || $evaluar[6] != 1 || $evaluar[7] != 1)) || ($registro_origin["tb_gastogar_estado"] == 0 && $estado_pago == 1)){
				$evaluar[12] = $forma_pago == 1 ? 1 : 0; //ingreso se genera uno nuevo, por lo tanto no son iguales
				$evaluar[13] = 0; //egreso se genera uno nuevo, por lo tanto no son iguales
			}
		}

		$son_iguales = true;
		$i = 0;
		while ($i < count($evaluar)) {
			if ($evaluar[$i] == 0) {
				$son_iguales = false;
			}
			$i++;
		}

		if ($son_iguales) {
			$data['estado'] = 2;
			$data['mensaje'] = "No se realizó ninguna modificacion";
		}
		else{
			$oGastogar->tb_egreso_idfk = $registro_origin['tb_egreso_idfk'];
			$oGastogar->tb_ingreso_idfk = $registro_origin['tb_ingreso_idfk'];
			$oGastogar->tb_gastogar_id = $gastogar_id;
			
			$genera_nuevos_ing_egr = 0;
			//eliminar el ingreso/egreso anterior, generar ing/egreso nuevos
			if(($registro_origin["tb_gastogar_estado"] == 1 && ($evaluar[1] != 1 || $evaluar[2] != 1 || $evaluar[5] != 1 || $evaluar[6] != 1 || $evaluar[7] != 1)) || ($registro_origin["tb_gastogar_estado"] == 0 && $estado_pago == 1)){
				$genera_nuevos_ing_egr = 1;
				$corr = ($registro_origin["tb_gastogar_estado"] == 1) ? "CORRECCION DE " : "";
				if($registro_origin["tb_gastogar_estado"] == 1){
					if($registro_origin["tb_gastogar_formapago"] == 2) {
						//eliminar el ingreso generado anteriormente
						$oIngreso->ingreso_id = $registro_origin['tb_ingreso_idfk'];
						if ($oIngreso->eliminar_2() > 0) {
							$data['mensaje'] = "● Ingreso id: $oIngreso->ingreso_id eliminado correctamente. <br>";
							unset($oIngreso->ingreso_id);
						} else {
							$data['mensaje'] = "● Hubo un error al eliminar el ingreso de caja con id $oIngreso->ingreso_id.";
							echo json_encode($data); exit();
						}
					}
					//eliminar egreso generado anteriormente
					$oEgreso->egreso_id = $registro_origin['tb_egreso_idfk'];
					if ($oEgreso->eliminar_2()) {
						$data['mensaje'] .= "● Egreso id: $oEgreso->egreso_id eliminado correctamente.<br>";
					} else {
						$data['mensaje'] .= "● Hubo un error al eliminar el egreso de caja con id $oEgreso->egreso_id.";
						echo json_encode($data); exit();
					}
				}
				if ($forma_pago == 2) {
					//generar ingreso
					$ing_det = "{$corr}INGRESO DE CAJA POR BANCO = Ingreso desde la siguente cuenta: [ {$deposito['tb_cuentadeposito_nom']} ], la fecha de depósito fue: [ " . mostrar_fecha($deposito['tb_deposito_fec']) . " ], el N° de operación fue: [ $num_operacion ]. ";
					$ing_det .= "Se usó [ $moneda_nom$monto_gasto ] de [ {$deposito['tb_moneda_nom']}" . abs($deposito['tb_deposito_mon']) . " ], ";
					$ing_det .= "por concepto de $coment";
					//si falla se detiene y sale
					$oIngreso->ingreso_usureg = $usuario_id;
					$oIngreso->ingreso_fec = fecha_mysql($fecha_gasto);
					$oIngreso->documento_id = 8; //OI - OTROS INGRESOS
					$oIngreso->ingreso_numdoc = '';
					$oIngreso->ingreso_det = $ing_det;
					$oIngreso->ingreso_imp = $monto_gasto;
					$oIngreso->cuenta_id = 43; //INGRESO/EGRESO POR BANCO
					$oIngreso->subcuenta_id = $subcuenta_id;
					$oIngreso->cliente_id = 1144; // id del cliente q hace el ingreso por banco: INVERSIONES Y PRESTAMOS DEL NORTE SAC
					$oIngreso->caja_id = 1; //CAJA
					$oIngreso->moneda_id = $moneda;
					//valores que pueden ser cambiantes según requerimiento de ingreso
					$oIngreso->modulo_id = 0;
					$oIngreso->ingreso_modide = 0;
					$oIngreso->empresa_id = $sede_id;
					$oIngreso->ingreso_fecdep = $deposito['tb_deposito_fec'];
					$oIngreso->ingreso_numope = $num_operacion;
					$oIngreso->ingreso_mondep = $monto_gasto;
					$oIngreso->ingreso_comi = 0;
					$oIngreso->cuentadeposito_id = $deposito['tb_cuentadeposito_id'];
					$oIngreso->banco_id = $banco_id;
					$oIngreso->ingreso_ap = 0; //acuerdo de pago, 0
					$oIngreso->ingreso_detex = '';

					$result = $oIngreso->insertar();

					if ($result['estado'] == 1) {
						$ingreso_id = $result['ingreso_id'];
						$data['mensaje'] .= "● Ingreso por banco registrado correctamente en sede $sede_nom. Id ingreso: $ingreso_id.<br>";
						$oGastogar->tb_ingreso_idfk = $ingreso_id;
						unset($result);
					} else {
						$data['mensaje'] .= "● Hubo un error al registrar el ingreso por banco en sede $sede_nom.<br>";
						echo json_encode($data); exit();
					}

					//generar egreso			
					$egr_det = "{$corr}EGRESO DE CAJA POR BANCO de obs: [ $coment ]. Egreso desde la siguiente cuenta [ {$deposito['tb_cuentadeposito_nom']} ], la fecha de depósito fue: [ " . mostrar_fecha($deposito['tb_deposito_fec']) . " ], el N° de operación fue: [ $num_operacion ]. ";
					$egr_det .= "Se usó [ $moneda_nom$monto_gasto ] de [ {$deposito['tb_moneda_nom']}" . abs($deposito['tb_deposito_mon']) . " ]";
					$oEgreso->egreso_usureg	= $usuario_id;
					$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
					$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS
					$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
					$oEgreso->egreso_det	= $egr_det;
					$oEgreso->moneda_id		= $moneda;
					$oEgreso->egreso_tipcam = $valor_tipocambio_venta;
					$oEgreso->egreso_imp	= $monto_gasto; //consultar si los egresos en $ guardan importe en $ o convierten a S/.
					$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
					$oEgreso->cuenta_id		= 16; //cuenta de gastos operativos
					$oEgreso->subcuenta_id	= 54; //subcuenta de OTROS
					$oEgreso->proveedor_id = 1; //por defecto 1: SISTEMA
					if ($emite_doc_sunat == 1) {
						$oEgreso->proveedor_id = $proveedor_id;
					}
					$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
					$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
					$oEgreso->caja_id = 1; // 1: CAJA
					$oEgreso->modulo_id = 0; //se definen en cero segun registro egreso id=52654
					$oEgreso->egreso_modide = 0; //se definen en cero segun registro egreso id=52654
					$oEgreso->empresa_id = $sede_id;
					$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

					$result = $oEgreso->insertar();
					if ($result['estado'] == 1) {
						$egreso_id = $result['egreso_id'];
						$data['mensaje'] .= "● Egreso por banco registrado correctamente en sede $sede_nom. Id egreso: $egreso_id.<br>";
						$oGastogar->tb_egreso_idfk = $egreso_id;
						unset($result);
					} else {
						$data['mensaje'] .= "● Hubo un error al registrar el egreso por banco en sede $sede_nom.<br>";
						echo json_encode($data);
						exit();
					}
				} else {
					$oEgreso->egreso_usureg	= $usuario_id;
					$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
					$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS
					$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
					$oEgreso->egreso_det	= $coment;
					$oEgreso->moneda_id		= $moneda;
					$oEgreso->egreso_tipcam = $valor_tipocambio_venta;
					$oEgreso->egreso_imp	= $monto_gasto; //consultar si los egresos en $ guardan importe en $ o convierten a S/.
					$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
					$oEgreso->cuenta_id		= 16; //cuenta de gastos operativos
					$oEgreso->subcuenta_id	= 54; //subcuenta de OTROS
					$oEgreso->proveedor_id = 1; //por defecto 1: SISTEMA
					if ($emite_doc_sunat == 1) {
						$oEgreso->proveedor_id = $proveedor_id;
					}
					$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
					$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
					$oEgreso->caja_id = 1; // 1: CAJA
					$oEgreso->modulo_id = 0; //se definen en cero segun registro egreso id=52654
					$oEgreso->egreso_modide = 0; //se definen en cero segun registro egreso id=52654
					$oEgreso->empresa_id = $sede_id;
					$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

					$resultado = $oEgreso->insertar();
					if ($resultado['estado'] > 0) {
						$egreso_id = $resultado['egreso_id'];
						$oGastogar->tb_egreso_idfk = $egreso_id;
						$data['mensaje'] .= "● Egreso por Caja efectivo registrado correctamente en sede $sede_nom. Id egreso: $egreso_id.<br>";
						unset($resultado);
					} else {
						$data['mensaje'] .= "● Error en el registro de egreso, intente nuevamente.";
						echo json_encode($data); exit();
					}
				}
			}

			$res = $oGastogar->modificar();

			if($res == 1){
				$data['estado'] = 1;
				$data['mensaje'] .= "● Se modificó correctamente este gasto.";
				
				//si se modifica el documento, actualizar egreso (si es por caja banco tambien ingreso)
				if(($evaluar[8] != 1 || $evaluar[9] != 1 || $evaluar[10]) && $genera_nuevos_ing_egr == 0){
					$egreso_detalle_actual = $registro_origin['tb_egreso_det'];
					$texto_a_reemplazar = $registro_origin["tb_gastogar_coment"];
					$nuevo_texto_egreso = str_replace($texto_a_reemplazar, $coment, $egreso_detalle_actual);
					
					if($nuevo_texto_egreso == $egreso_detalle_actual) {
						$data['mensaje'] .= '<br> No se actualizó el egreso/ingreso, solicite el cambio a sistemas';
						echo json_encode($data); exit();
					}
					
					$oEgreso->modificar_campo($registro_origin['tb_egreso_idfk'], 'tb_egreso_det', $nuevo_texto_egreso, 'STR');
					if($forma_pago == 2){
						$nuevo_texto_ingreso = str_replace($texto_a_reemplazar, $coment, $registro_origin['tb_ingreso_det']);
						$oIngreso->modificar_campo($registro_origin['tb_ingreso_idfk'], 'tb_ingreso_det', $nuevo_texto_ingreso, 'STR');
					}
				}

				$ARRAY = array('NO', 'SI');
				$forma_egreso = array(null, 'EGRESO CAJA EFECTIVO', 'INGRESO/EGRESO BANCO');

				//insertar historial
				$oHist->setTbHistUsureg($usuario_id);
				$oHist->setTbHistNomTabla('tb_gastogar');
				$oHist->setTbHistRegmodid($gastogar_id);
				$mensaje = "Modificó el gasto ID $gastogar_id de la garantia " . $garantia_idfk . ' - ' . $garantia_nom . ':';
				if ($evaluar[0] == 0)
					$mensaje .= '<br> - Falla: ' . $registro_origin['tb_falla_nombre'] . ' => <b>' . strtoupper($_POST['cbo_falla_selected_nom']) . '</b>';	
				if ($evaluar[1] == 0)
					$mensaje .= '<br> - Moneda: ' . $registro_origin['tb_moneda_nom'] . ' => <b>' . $moneda_nom . '</b>';
				if ($evaluar[2] == 0)
					$mensaje .= '<br> - Monto: ' . $registro_origin['tb_moneda_nom'] . mostrar_moneda($registro_origin['tb_gastogar_monto']) . ' => <b>' . $moneda_nom . $_POST['txt_monto'] . '</b>';
				if ($evaluar[3] == 0)
					$mensaje .= '<br> - estado de egresado: ' . $ARRAY[$registro_origin['tb_gastogar_estado']] . ' => <b>' . $ARRAY[$estado_pago] . '</b>';
				if ($evaluar[4] == 0)
					$mensaje .= '<br> - fecha del gasto: ' . mostrar_fecha($registro_origin['tb_gastogar_fecha']) . ' => <b>' . $fecha_gasto . '</b>';
				if ($evaluar[5] == 0){
					$mensaje .= '<br> - Sede: ' . $registro_origin['tb_empresa_nomcom'] . ' => <b>' . $sede_nom . '</b>';
				} elseif ($registro_origin["tb_gastogar_estado"] == 1 && ($evaluar[1] != 1 || $evaluar[2] != 1 || $evaluar[5] != 1 || $evaluar[6] != 1 || $evaluar[7] != 1)) {
					$mensaje .= "<br> - Sede: <b>$sede_nom</b>";
				}
				if ($evaluar[6] == 0){
					$mensaje .= '<br> - Forma egreso: ' . $forma_egreso[$registro_origin['tb_gastogar_formapago']] . ' => <b>' . $forma_egreso[$forma_pago] . '</b>';
				} elseif ($registro_origin["tb_gastogar_estado"] == 1 && ($evaluar[1] != 1 || $evaluar[2] != 1 || $evaluar[5] != 1 || $evaluar[6] != 1 || $evaluar[7] != 1)) {
					$mensaje .= "<br> - Forma egreso: <b>{$forma_egreso[$forma_pago]}</b>";
				}
				if ($evaluar[7] == 0){
					$mensaje .= '<br> - Numero operacion: ' . $registro_origin['tb_gastogar_numope'] . ' => <b>' . $num_operacion . '</b>';
				} elseif ($registro_origin["tb_gastogar_estado"] == 1 && ($evaluar[1] != 1 || $evaluar[2] != 1 || $evaluar[5] != 1 || $evaluar[6] != 1 || $evaluar[7] != 1)) {
					$mensaje .= "<br> - Numero operacion: <b>$num_operacion</b>";
				}
				if (($registro_origin["tb_gastogar_estado"] == 1 && ($evaluar[1] != 1 || $evaluar[2] != 1 || $evaluar[5] != 1 || $evaluar[6] != 1 || $evaluar[7] != 1)) || ($registro_origin["tb_gastogar_estado"] == 0 && $estado_pago == 1)) {
					if ($registro_origin['tb_gastogar_estado'] == 1) {
						if ($registro_origin["tb_gastogar_formapago"] == 2) {
							$mensaje .= "<br> - Eliminó el ingreso de caja con id {$registro_origin['tb_ingreso_idfk']}";
						}
						$mensaje .= "<br> - Eliminó el egreso de caja con id {$registro_origin['tb_egreso_idfk']}";
					}
					if ($forma_pago == 2) {
						$mensaje .= "<br> - Registró el ingreso de caja con id $ingreso_id";
					}
					$mensaje .= "<br> - Registró el egreso de caja con id $egreso_id";
				}
				if ($evaluar[8] == 0)
					$mensaje .= '<br> - Cambió la emision de comprobante: ' . $ARRAY[$registro_origin['tb_gastogar_emitedocsunat']] . ' => <b>' . $ARRAY[$emite_doc_sunat] . '</b>';
				if ($evaluar[9] == 0)
					$mensaje .= '<br> - Cambió el documento emitido: ' . $registro_origin["tb_gastogar_documentossunat"] . ' => <b>' . $oGastogar->tb_gastogar_documentossunat . '</b>';
				if ($evaluar[10] == 0){
					$nuevo_prov = $proveedor_id == 0 ? "(vacío)" : "{$_POST['txt_proveedor_doc']} - {$_POST['txt_proveedor_nom']}";
					$antiguo_prov = intval($registro_origin["tb_proveedor_idfk"]) == 0 ? "(vacío)" : "{$registro_origin['tb_proveedor_doc']} - {$registro_origin['tb_proveedor_nom']}";
					$mensaje .= "<br> - Cambió el proveedor del documento: $antiguo_prov => <b> $nuevo_prov </b>";
				}
				if ($evaluar[11] == 0)
					$mensaje .= '<br> - Cambió el comentario: ' . $registro_origin["tb_gastogar_coment"] . ' => <b>' . $coment . '</b>';
				
				$oHist->setTbHistDet($mensaje);
				$oHist->insertar();
			}
		}
	} else {
		$data['mensaje'].= "● No se reconoce la accion";
	}
}

echo json_encode($data);