<?php
require_once('Gastogar.class.php');
$oGastogar = new Gastogar();

$credito_id = (empty($credito_id))? intval($_POST['credito_id']) : intval($credito_id);
$garantia_id = (empty($garantia_id))? intval($_POST['garantia_id']) : intval($garantia_id);
if(empty($_POST["gasto_id"]) && empty($gastogar_id)){
	$gastogar_id = 0;
} elseif (!empty($gastogar_id)){
	$gastogar_id = intval($gastogar_id);
} else {
	$gastogar_id = intval($_POST["gasto_id"]);
}

$option = '';

$parametros[0]['column_name'] = 'tb_gastogar_xac';
$parametros[0]['param0'] = 1;
$parametros[0]['datatype'] = 'INT';

$num_sigue = 1;
if($_POST['vista'] == 'compracontadoc_form'){
	if ($_POST['action'] != 'L') {
		$parametros[$num_sigue]['param1'] = "(tb_gastogar_emitedocsunat = 0 OR (tb_gastogar_emitedocsunat = 1 AND (tb_gastogar_documentossunat <> '' AND tb_gastogar_documentossunat IS NOT NULL)))";
		$num_sigue = 2;
		$parametros[$num_sigue]['column_name'] = 'tb_gastogar_anexado';
		$parametros[$num_sigue]["param$num_sigue"] = 0;
		$parametros[$num_sigue]['datatype'] = 'INT';
		$num_sigue = 3;
	}
}

if(!empty($credito_id) || !empty($garantia_id)){
	$parametros[$num_sigue]['conector'] = 'AND';
	if (!empty($credito_id)) {
		$parametros[$num_sigue]['column_name'] = 'tb_credito_idfk';
		$parametros[$num_sigue]["param$num_sigue"] = $credito_id;
		$parametros[$num_sigue]['datatype'] = 'INT';
	} else {
		$parametros[$num_sigue]['column_name'] = 'tb_garantia_idfk';
		$parametros[$num_sigue]["param$num_sigue"] = $garantia_id;
		$parametros[$num_sigue]['datatype'] = 'INT';
	}
}

$otros["orden"]["column_name"] = 'tb_gastogar_fecreg';
$otros["orden"]["value"] = 'DESC';

//PRIMER NIVEL
$result = $oGastogar->listar_todos($parametros, $otros);

if ($result['estado'] == 1) {
	$option = '<option value="0" style="font-size: 10px">..SELECCIONE..</option>';
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($gastogar_id == $value['tb_gastogar_id'])
			$selected = 'selected';

		//los valores de este option son usados en compracontadoc_elegirgasto.js, no mover
		$option .= '<option value="' . $value['tb_gastogar_id'] . '" style="font-size: 10px" ' . $selected . '>' . $value['tb_moneda_nom'].$value['tb_gastogar_monto'] ." | {$value['tb_garantia_pro']} | {$value['tb_gastogar_coment']} | Proveedor id: {$value['tb_proveedor_idfk']}".'</option>';
	}
}
$result = NULL;

//FIN PRIMER NIVEL
echo $option;
?>