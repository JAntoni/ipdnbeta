<?php
require_once('../funciones/funciones.php');
$producto = strtoupper($_POST['garantia_nom']);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_gastogar_tabla" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" id="modalSizeList" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo "Tabla de gastos en garantia <b>" . $producto . "</b>" ?></h4>
                <input type="hidden" name="hdd_garantia_id" id="hdd_garantia_id" value="<?php echo intval($_POST['garantia_id']); ?>">
            </div>
            <div class="modal-body">
                <table id="tbl_gastogars" class="table table-hover dataTable display">
                    <thead>
                        <tr id="tabla_cabecera">
                            <th id="tabla_cabecera_fila">Id</th>
                            <th id="tabla_cabecera_fila">Sede Egreso</th>
                            <th id="tabla_cabecera_fila">Forma Pago</th>
                            <th id="tabla_cabecera_fila">Monto</th>
                            <th id="tabla_cabecera_fila">Falla - Coment</th>
                            <th id="tabla_cabecera_fila">Fecha</th>
                            <th id="tabla_cabecera_fila">Bol/Fact/Proveedor</th>
                            <th id="tabla_cabecera_fila">Registrado por</th>
                            <th id="tabla_cabecera_fila">Opciones</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_gastogar">
                        <?php //echo $tr; ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gastogar/gastogar.js?ver=6178664523'; ?>"></script>
<script>
    function adjustModalSizeList() {
        var modalDialogList = document.getElementById('modalSizeList');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogList.classList.remove('modal-lg');
            modalDialogList.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogList.classList.remove('modal-xl');
            modalDialogList.classList.add('modal-lg');
        }
    }

    adjustModalSizeList();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizeList);
</script>