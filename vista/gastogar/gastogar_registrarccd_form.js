$(document).ready(function () {
    consultar_datos();
});

function consultar_datos() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastogar/gastogar_registrarccd_form_vista.php",
        async: true,
        dataType: "json",
        success: function (data) {
            $("#lista_gastos_registrarccd").html(data.lista);
        }
    });
}