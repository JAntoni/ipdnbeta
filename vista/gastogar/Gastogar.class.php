<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Gastogar extends Conexion {
    public $tb_gastogar_id;
    public $tb_garantia_idfk;
    public $tb_creditotipo_idfk;
    public $tb_credito_idfk;
    public $tb_articulo_id;
    public $tb_falla_idfk;
    public $tb_gastogar_coment;
    public $tb_gastogar_estado;
    public $tb_gastogar_fecha;
    public $tb_moneda_idfk;
    public $tb_gastogar_monto;
    public $tb_empresa_idfk;
    public $tb_gastogar_formapago;
    public $tb_gastogar_numope;
    public $tb_ingreso_idfk;
    public $tb_egreso_idfk;
    public $tb_gastogar_emitedocsunat;
    public $tb_gastogar_documentossunat;
    public $tb_proveedor_idfk;
    public $tb_gastogar_anexado;
    public $tb_gastogar_usureg;
    public $tb_gastogar_fecreg;
    public $tb_gastogar_usumod;
    public $tb_gastogar_fecmod;
    public $tb_gastogar_xac;

    public function listar_todos($array, $otros_param) {
        //array: bidimensional con formato $column_name, $param.$i, $datatype
        //otros_param: arreglo para funciones diversas como el order by, limit, etc. los keys se definen de acuerdo al uso (ejm: ordenar: ["orden"]["column_name"], ["orden"]["value"])
        try {
            $sql = "SELECT
                gg.*,
                g.tb_garantia_pro,
                m.tb_moneda_nom,
                e.tb_empresa_nomcom,
                a.tb_articulo_nombre,
                f.tb_falla_nombre,
                p.tb_proveedor_doc, p.tb_proveedor_nom,
                CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) AS usuario_registro
              FROM tb_gastogar gg
              INNER JOIN tb_garantia g ON gg.tb_garantia_idfk = g.tb_garantia_id
              INNER JOIN tb_moneda m ON gg.tb_moneda_idfk = m.tb_moneda_id
              LEFT JOIN tb_empresa e ON gg.tb_empresa_idfk = e.tb_empresa_id
              INNER JOIN tb_articulo a ON gg.tb_articulo_id = a.tb_articulo_id
              INNER JOIN tb_falla f ON gg.tb_falla_idfk = f.tb_falla_id
              LEFT JOIN tb_proveedor p ON gg.tb_proveedor_idfk = p.tb_proveedor_id
              INNER JOIN tb_usuario u ON gg.tb_gastogar_usureg = u.tb_usuario_id";
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i])) {
                    $sql .= ($i > 0) ? " AND " : " WHERE ";

                    if (!empty($array[$i]["param$i"]) && empty($array[$i]["column_name"]) && empty($array[$i]["datatype"])) {
                        $sql .= " {$array[$i]["param$i"]}";
                    } else {
                        if (stripos($array[$i]["column_name"], 'fec') !== FALSE) { //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
                            $sql .= " DATE_FORMAT(gg.{$array[$i]["column_name"]}, '%Y-%m-%d')";
                        } else { // de lo contrario solo se coloca el nombre de la columna a filtrar
                            $sql .= "gg.{$array[$i]["column_name"]}";
                        }
                        $sql .= " = :param$i";
                    }
                }
            }
            if (!empty($otros_param["orden"])) {
                $sql .= " ORDER BY {$otros_param["orden"]["column_name"]} {$otros_param["orden"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]["column_name"]) && !empty($array[$i]["datatype"])) {
                    $_PARAM = strtoupper($array[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
                    $sentencia->bindParam(":param$i", $array[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            $retorno["cantidad"] = $sentencia->rowCount();
            $retorno['sql'] = $sql;
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    public function mostrarUno($array, $array1) {
        //array: bidimensional con formato $column_name, $param.$i, $datatype, $conector
        //array1: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
        try {
            $sql = "SELECT
                gg.*,
                g.tb_garantia_pro,
                m.tb_moneda_nom,
                e.tb_empresa_nomcom,
                a.tb_articulo_nombre,
                f.tb_falla_nombre,
                p.tb_proveedor_nom, p.tb_proveedor_doc";
            if (!empty($array1)) {
                for ($j = 0; $j < count($array1); $j++) {
                    $sql .= ",\n" . $array1[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_gastogar gg
              INNER JOIN tb_garantia g ON gg.tb_garantia_idfk = g.tb_garantia_id
              INNER JOIN tb_moneda m ON gg.tb_moneda_idfk = m.tb_moneda_id
              LEFT JOIN tb_empresa e ON gg.tb_empresa_idfk = e.tb_empresa_id
              INNER JOIN tb_articulo a ON gg.tb_articulo_id = a.tb_articulo_id
              INNER JOIN tb_falla f ON gg.tb_falla_idfk = f.tb_falla_id
              LEFT JOIN tb_proveedor p ON gg.tb_proveedor_idfk = p.tb_proveedor_id\n";
            if (!empty($array1)) {
                for ($k = 0; $k < count($array1); $k++) {
                    $sql .= $array1[$k]['tipo_union'] . " JOIN " . $array1[$k]['tabla_alias'] . " ON " . $array1[$k]['columna_enlace'] . " = " . $array1[$k]['alias_columnaPK'] . "\n";
                }
            }
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i])) {
                    $sql .= ($i > 0) ? " {$array[$i]["conector"]} " : "WHERE ";

                    if (!empty($array[$i]["column_name"])) {
                        if (stripos($array[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$array[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$array[$i]["column_name"]}";
                        }

                        if (!empty($array[$i]["datatype"])) {
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " " . $array[$i]["param$i"];
                        }
                    } else {
                        $sql .= " " . $array[$i]["param$i"];
                    }
                }
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]["column_name"]) && !empty($array[$i]["datatype"])) {
                    $_PARAM = strtoupper($array[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $array[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No existe este registro";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $insert_opcional = (!empty($this->tb_gastogar_estado)) ? ",\ntb_gastogar_fecha,\ntb_empresa_idfk,\ntb_gastogar_emitedocsunat,\ntb_gastogar_formapago" : "";
            $insert_opcional .= (!empty($this->tb_gastogar_emitedocsunat)) ? ",\ntb_gastogar_documentossunat,\ntb_proveedor_idfk" : "";
            $insert_opcional .= ($this->tb_gastogar_formapago == 2) ? ",\ntb_gastogar_numope" : "";

            $params_opcional = (!empty($this->tb_gastogar_estado)) ? ",\n:param11,\n:param12,\n:param13,\n:param14" : "";
            $params_opcional .= (!empty($this->tb_gastogar_emitedocsunat)) ? ",\n:param15,\n:param16" : "";
            $params_opcional .= ($this->tb_gastogar_formapago == 2) ? ",\n:param17" : "";

            $sql = "INSERT INTO tb_gastogar (
                tb_garantia_idfk,
                tb_articulo_id,
                tb_falla_idfk,
                tb_moneda_idfk,
                tb_gastogar_monto,
                tb_gastogar_coment,
                tb_gastogar_estado,
                tb_creditotipo_idfk,
                tb_credito_idfk,
                tb_gastogar_usureg,
                tb_gastogar_usumod $insert_opcional)
              VALUES (
                :param0,
                :param1,
                :param2,
                :param3,
                :param4,
                :param5,
                :param6,
                :param7,
                :param8,
                :param9,
                :param10 $params_opcional);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_garantia_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param1", $this->tb_articulo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param2", $this->tb_falla_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param3", $this->tb_moneda_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param4", $this->tb_gastogar_monto, PDO::PARAM_STR);
            $sentencia->bindParam(":param5", $this->tb_gastogar_coment, PDO::PARAM_STR);
            $sentencia->bindParam(":param6", $this->tb_gastogar_estado, PDO::PARAM_INT);
            $sentencia->bindParam(":param7", $this->tb_creditotipo_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param8", $this->tb_credito_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param9", $this->tb_gastogar_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param10", $this->tb_gastogar_usumod, PDO::PARAM_INT);
            if (!empty($this->tb_gastogar_estado)) {
                $sentencia->bindParam(":param11", $this->tb_gastogar_fecha, PDO::PARAM_STR);
                $sentencia->bindParam(":param12", $this->tb_empresa_idfk, PDO::PARAM_INT);
                $sentencia->bindParam(":param13", $this->tb_gastogar_emitedocsunat, PDO::PARAM_INT);
                $sentencia->bindParam(":param14", $this->tb_gastogar_formapago, PDO::PARAM_INT);
            }
            if (!empty($this->tb_gastogar_emitedocsunat)) {
                $sentencia->bindParam(":param15", $this->tb_gastogar_documentossunat, PDO::PARAM_STR);
                $sentencia->bindParam(":param16", $this->tb_proveedor_idfk, PDO::PARAM_INT);
            }
            if ($this->tb_gastogar_formapago == 2) {
                $sentencia->bindParam(":param17", $this->tb_gastogar_numope, PDO::PARAM_STR);
            }

            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Gasto registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    public function modificar() {
        $this->dblink->beginTransaction();
        try {
            $modif_opcional = (!empty($this->tb_gastogar_estado)) ? ",\ntb_gastogar_fecha = :param_opc0,\ntb_empresa_idfk = :param_opc1,\ntb_gastogar_emitedocsunat = :param_opc2,\ntb_gastogar_formapago = :param_opc3,\ntb_egreso_idfk = :param_opc4" : "";
            $modif_opcional .= ",\ntb_gastogar_documentossunat = :param_opc5,\ntb_proveedor_idfk = :param_opc6";
            $modif_opcional .= ($this->tb_gastogar_formapago == 2) ? ",\ntb_gastogar_numope = :param_opc7,\ntb_ingreso_idfk = :param_opc8" : "";

            $sql = "UPDATE tb_gastogar
              SET
                tb_falla_idfk = :param0,
                tb_moneda_idfk = :param1,
                tb_gastogar_monto = :param2,
                tb_gastogar_estado = :param3,
                tb_gastogar_coment = :param4,
                tb_gastogar_usumod = :param5$modif_opcional 
              WHERE
                tb_gastogar_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->tb_falla_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->tb_moneda_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->tb_gastogar_monto, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->tb_gastogar_estado, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->tb_gastogar_coment, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->tb_gastogar_usumod, PDO::PARAM_INT);
            if(!empty($this->tb_gastogar_estado)){
                $sentencia->bindParam(':param_opc0', $this->tb_gastogar_fecha, PDO::PARAM_STR);
                $sentencia->bindParam(':param_opc1', $this->tb_empresa_idfk, PDO::PARAM_INT);
                $sentencia->bindParam(':param_opc2', $this->tb_gastogar_emitedocsunat, PDO::PARAM_INT);
                $sentencia->bindParam(':param_opc3', $this->tb_gastogar_formapago, PDO::PARAM_INT);
                $sentencia->bindParam(':param_opc4', $this->tb_egreso_idfk, PDO::PARAM_INT);
            }
            $sentencia->bindParam(':param_opc5', $this->tb_gastogar_documentossunat, PDO::PARAM_STR);
            $sentencia->bindParam(':param_opc6', $this->tb_proveedor_idfk, PDO::PARAM_INT);
            if($this->tb_gastogar_formapago == 2){
                $sentencia->bindParam(':param_opc7', $this->tb_gastogar_numope, PDO::PARAM_STR);
                $sentencia->bindParam(':param_opc8', $this->tb_ingreso_idfk, PDO::PARAM_INT);
            }
            $sentencia->bindParam(':paramx', $this->tb_gastogar_id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    public function eliminar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_gastogar SET tb_gastogar_xac = 0, tb_gastogar_usumod = :tb_gastogar_usumod WHERE tb_gastogar_id = :tb_gastogar_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_gastogar_usumod", $this->tb_gastogar_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_gastogar_id", $this->tb_gastogar_id, PDO::PARAM_INT);
            $resultado = $sentencia->execute(); //retorna 1 si es correcto

            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_campo($gastogar_id, $gastogar_columna, $gastogar_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($gastogar_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_gastogar SET " . $gastogar_columna . " = :gastogar_valor WHERE tb_gastogar_id = :gastogar_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":gastogar_id", $gastogar_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":gastogar_valor", $gastogar_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":gastogar_valor", $gastogar_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_documentossunatdegastos() {
        try {
            $sql = "SELECT DISTINCT tb_gastogar_documentossunat,tb_gastogar_fecha,
                            tb_proveedor_idfk, tb_proveedor_nom, tb_proveedor_doc,
                            tb_moneda_idfk
                    FROM tb_gastogar
                    INNER JOIN tb_proveedor ON tb_proveedor_idfk = tb_proveedor_id
                    WHERE tb_gastogar_xac = 1
                    AND tb_gastogar_emitedocsunat = 1
                    AND tb_gastogar_anexado = 0
                    AND tb_gastogar_documentossunat IS NOT NULL AND TRIM(tb_gastogar_documentossunat) <> ''
                    GROUP BY tb_proveedor_idfk, tb_gastogar_documentossunat;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            $retorno['sql'] = $sql;
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    public function listar_gastos_por_documentossunat($documento, $prov_id) {
        try {
            $sql = "SELECT
                        gg.*,
                        g.tb_garantia_pro,
                        m.tb_moneda_nom,
                        e.tb_empresa_nomcom,
                        a.tb_articulo_nombre,
                        f.tb_falla_nombre,
                        p.tb_proveedor_doc, p.tb_proveedor_nom,
                        CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) AS usuario_registro
                    FROM tb_gastogar gg
                    INNER JOIN tb_garantia g ON gg.tb_garantia_idfk = g.tb_garantia_id
                    INNER JOIN tb_moneda m ON gg.tb_moneda_idfk = m.tb_moneda_id
                    INNER JOIN tb_empresa e ON gg.tb_empresa_idfk = e.tb_empresa_id
                    INNER JOIN tb_articulo a ON gg.tb_articulo_id = a.tb_articulo_id
                    INNER JOIN tb_falla f ON gg.tb_falla_idfk = f.tb_falla_id
                    INNER JOIN tb_proveedor p ON gg.tb_proveedor_idfk = p.tb_proveedor_id
                    INNER JOIN tb_usuario u ON gg.tb_gastogar_usureg = u.tb_usuario_id
                    WHERE tb_gastogar_xac = 1
                    AND tb_gastogar_emitedocsunat = 1
                    AND tb_gastogar_documentossunat = '$documento'
                    AND tb_proveedor_idfk = $prov_id
                    AND tb_gastogar_anexado = 0
                    ORDER BY tb_gastogar_id DESC;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }
}