<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_registrarccdgastogar_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">REGISTRAR FACTURAS DE GASTOS</h4>
            </div>

            <div class="modal-body">
                <div class="box shadow">
                    <ul id="lista_gastos_registrarccd" name="lista_gastos_registrarccd" class="products-list product-list-in-box">
                    </ul>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gastogar/gastogar_registrarccd_form.js?ver=6132314781'; ?>"></script>