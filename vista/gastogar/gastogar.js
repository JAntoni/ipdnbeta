$(document).ready(function () {
    consultar_tabla_gastos();
});

function gastogar_historial_form(gastogarid){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastogar/gastogar_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
          gastogar_id :gastogarid,
          tabla_nom : "tb_gastogar"
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_gastogar_historial_form').html(html);
            $('#div_modal_gastogar_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');
  
            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_gastogar_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_gastogar_historial_form'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
        }
    });
}

/* daniel odar 02-03-23 */
function gasto_form(action, garantia_id, gasto_id, credito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastogar/gastogar_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            garantia_id: garantia_id,
            gasto_id: gasto_id,
            credito_tipo: 1,
            credito_id: credito_id
        }),
        beforeSend: function () {
            $("#h3_modal_title").text("Cargando Formulario");
            $("#modal_mensaje").modal("show");
        },
        success: function (html) {
            $("#div_gastogar_form").html(html);
            $("#div_modal_gastogar_form").modal("show");
            $("#modal_mensaje").modal("hide");

            modal_hidden_bs_modal("div_modal_gastogar_form", "limpiar"); //funcion encontrada en public/js/generales.js
            modal_width_auto("div_modal_gastogar_form", 40);
            modal_height_auto("div_modal_gastogar_form"); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) { },
    });
}