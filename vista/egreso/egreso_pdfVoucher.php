
<?php

error_reporting(0);

define('FPDF_FONTPATH', 'font/');

require_once('../../static/tcpdf/tcpdf.php');


require_once ("../empresa/Empresa.class.php");
$oEmpresa = new Empresa();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../cliente/Cliente.class.php");
$oCliente= new Cliente();

require_once ("../impresora/Impresora.class.php");
$oImpresora = new Impresora();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

//$cuotapago_id = intval(37801);
//$cuotapago_id = intval(37908);
//$cuotapago_id = intval(37986);
$egreso_id = intval($_GET['id']);
$empresa_id = intval($_GET['empresa_id']);
//$empresa = $_GET['empresa'];
//	$cuotapago_id = intval($_POST['cuotapago']);


//echo '$ingreso_id=='.$ingreso_id.'<br>$empresa_id='.$empresa_id.'<br>';;


$result = $oEmpresa->mostrarUno($empresa_id);
        if($result['estado'] == 1){
                $emp_ruc    =$result['data']['tb_empresa_ruc'];
                $emp_nomcom =$result['data']['tb_empresa_nomcom'];
                $emp_razsoc =$result['data']['tb_empresa_razsoc'];
                $emp_dir    =$result['data']['tb_empresa_dir'];
                $emp_dir2   =$result['data']['tb_empresa_dir2'];
                $emp_tel    =$result['data']['tb_empresa_tel'];
                $emp_ema    =$result['data']['tb_empresa_ema'];
                $emp_fir    =$result['data']['tb_empresa_fir'];		
        }
$result = NULL;

//echo "Empresa=".$emp_ruc;
//exit();


$result = $oEgreso->mostrarUno($egreso_id);
    if($result['estado'] == 1){
        $fecreg		= mostrar_fecha_hora($result['data']['tb_egreso_fecreg']);
        $fecmod		= mostrar_fecha_hora($result['data']['tb_egreso_fecmod']);
        $usureg		=$result['data']['tb_egreso_usureg'];
        $usumod		=$result['data']['tb_egreso_usumod'];
	
	$fec		= mostrar_fecha($result['data']['tb_egreso_fec']);
	$doc_id 	=$result['data']['tb_documento_id'];
	$numdoc		=$result['data']['tb_egreso_numdoc'];
	
	$det		=$result['data']['tb_egreso_det'];
	
	$cue_id		=$result['data']['tb_cuenta_id'];
	$subcue_id	=$result['data']['tb_subcuenta_id'];

	$pro_id		=$result['data']['tb_proveedor_id'];
	$mod_id		=$result['data']['tb_modulo_id'];
        $cliente_id     =$result['data']['tb_cliente_id'];
                
        $cli_nom 	=$result['data']['tb_proveedor_nom'];
        $cli_doc 	=$result['data']['tb_proveedor_doc'];
        $cli_dir 	=$result['data']['tb_proveedor_dir'];       
	$cli_tip 	=$result['data']['tb_proveedor_tip'];
	
	$imp		=$result['data']['tb_egreso_imp'];
	
	$caj_id		=$result['data']['tb_caja_id'];

	$ven_id		=$result['data']['tb_venta_id'];

	$mon_id		=$result['data']['tb_moneda_id'];
	$mon_nom	=$result['data']['tb_moneda_nom'];
	
	$est		=$result['data']['tb_egreso_est'];		
    }
$result = NULL;

if($pro_id==1 && ($mod_id==51)){
    $result = $oCliente->mostrarUno($cliente_id);

    if($result['estado'] == 1){
            $cli_nom 	=$result['data']['tb_cliente_nom'];
            $cli_doc 	=$result['data']['tb_cliente_doc'];
            $cli_dir 	=$result['data']['tb_cliente_dir'];
        }
    $result = NULL;
 }    
    
//impresora
$imp_id=1;
if($imp_id>0)
{
$result = $oImpresora->mostrarUno($imp_id);
        if($result['estado'] == 1){
            $imp_nom 	=$result['data']['tb_impresora_nom'];
            $imp_nomloc =$result['data']['tb_impresora_nomloc'];
            $imp_ser 	=$result['data']['tb_impresora_ser'];
            $imp_url 	=$result['data']['tb_impresora_url'];
            $imp_ip 	=$result['data']['tb_impresora_ip'];	
        }
$result = NULL;
}

//cajero
$result = $oUsuario->mostrarUno($usureg);
        if($result['estado'] == 1){
            $usu_nom	=$result['data']['tb_usuario_nom'];
            $usu_apepat	=$result['data']['tb_usuario_apepat'];
            $usu_apemat	=$result['data']['tb_usuario_apemat'];	
        }
$result = NULL;


//$texto_cajero=substr($usu_nom, 0, 3).substr($usu_apepat, 0, 1).substr($usu_apemat, 0, 1);
$cajero=$usu_nom.' '.$usu_apepat.' '.$usu_apemat;

class MYPDF extends TCPDF {

    public function Header() {
        
    }

}


$contDetalle = strlen($det);
$resta = $contDetalle - 100;
$adicionar = 0;
if($resta > 1){
    $demas = $resta / 35;
    if($demas <= 1)
        $adicionar = 5;
    else {
        $adicionar = $demas * 5;
        $adicionar = round($adicionar);
    }
        
    
}

$heightPDF = 220 + $adicionar; 

$medidas = array(80,300);
//$medidas = array(80,220);

$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);

$pdf->SetAuthor('cristhian.cyga@gmail.com');

$pdf->SetTitle($title);

$pdf->SetSubject('cristhian.cyga@gmail.com');

$pdf->SetKeywords('cristhian.cyga@gmail.com');

// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins

//$pdf->SetMargins(0, 0, 0); // left top right
$pdf->SetMargins(5,2, 5); // left top right

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//set auto page breaks

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
//set some language-dependent strings

$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// add a page

$pdf->AddPage();
//$detalle = 'PAGO DE CUOTA CM-1783 N° CUOTA: 1/1 (2). PAGO ABONADO EN EL BANCO, EN LA SIGUENTE CUENTA: CUENTA IPDN SAC BCP SOLES, LA FECHA DE DEPÓSITO FUE: 27-01-2022, LA FECHA DE VALIDACIÓN FUE: 27-01-2022, EL N° DE OPERACIÓN FUE: 07462152, EL MONTO DEPOSITADO FUE: S/. 400.00, LA COMISIÓN DEL BANCO FUE: S/. 0.00, EL MONTO VALIDADO FUE: S/. 400.00 Y CON CAMBIO DE MONEDA EL MONTO PAGADO A LA CUOTA FUE: S/. 400.00 HAHHA AH H HH TT';
//$contDetalle = strlen($detalle);
$html .= '
                
                <dl>
                <br>
                    <table width="100%">	

                        <tr style="text-align:justify;">

                                <td style="text-align:left; width: 25%;"></td>

                                <td style="text-align:center; width: 50%;"><img src="../../public/images/logopres.png"></td>

                                <td style="text-align:left; width: 25%;"></td>

                        </tr>

                    </table>
                    <br>
                    <dt style="color: black; text-align:center; font-weight: bold; font-size: 10pt;"> ' . $emp_razsoc .'</dt>

                    <dt style="text-align:center; color: black;"></dt>

                    <dt style="text-align:center; color: black;"><b>R.U.C.:</b> ' . $emp_ruc . '</dt>

                    <dt style="text-align:center; color: black;"><b>' . $emp_dir . '</b></dt>

                    <dt style="text-align:center; color: black;">' . $emp_tel . '</dt>

                    <dt style="text-align:center; color: black;"><b>Email:</b> ' . $emp_ema . '</dt>

                    <dt style="text-align:center; color: black;"><b>Website:</b> www.prestamosdelnorte.com</dt>
                    <dt style="text-align:center; color: black;"><b>FECHA:</b> ' . $fecreg . '</dt>
                    <br>
                    <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
                    <dt style="text-align:center;font-family: cambria color: black;"><b>N° DE OPERACION:</b> ' . $numdoc . '</dt>
                    <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
                </dl>
                <dl>
                    <dt style="text-align:justify; color: black;"><b>CLIENTE:</b> ' . $cli_nom . '</dt>
                    <dt style="text-align:justify; color: black;"><b>RUC/DNI:</b> ' . $cli_doc . '</dt>
                    <dt style="text-align:justify; color: black;"><b>DIRECCION:</b> ' . $cli_dir . '</dt>

                    <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
		</dl>
                <dl>
                    <dt style="text-align:justify; color: black;"><b>CONCEPTO:</b> ' . $det . '</dt>';

                        $html .= '<br>
                        <dt style="text-align:justify; color: black;"><b>MONTO:</b> ' . $mon_nom . ' ' . $imp . '</dt>';

                    
                    if ($monto_cambio > 0) {
                        $html .= '<br>
                        <dt style="text-align:justify; color: black;"><b>TIP CAMB:</b> ' . $mon_nom . ' ' . $tipo_cambio . '</dt>
                        <dt style="text-align:justify; color: black;"><b>CAMBIO:</b> ' . $mon_nom . ' ' . $monto_cambio . '</dt>';
                    }

            $html .= '
                <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
                <dt style="text-align:justify; color: black;"><b>REGISTRADO :</b> ' . $cajero . '</dt>
		</dl>';

$pdf->SetFont('helvetica', '', 9);

// output the HTML content

$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

$pdf->lastPage();

$nombre_archivo = "INGRESO_" . $ingreso_id.".pdf";
ob_start();

$pdf->IncludeJS("print();");

$pdf->Output($nombre_archivo, 'I');
?>