function egreso_tipo_mostrar_ocultar() {
    var egresotipo_id = parseInt($('#cmb_egresotipo_id').val());

    if (egresotipo_id == 2) {
        $('.egreso_oro').show(300);
        $('.egreso_serie').hide(300);
    } else if (egresotipo_id == 1) {
        $('.egreso_oro').hide(300);
        $('.egreso_serie').show(300);
    } else {
        $('.egreso_oro').hide(300);
        $('.egreso_serie').hide(300);
    }
}
function egresofile_form(usuario_act, egreso_id, egresofile_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egresofile/egresofile_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            egreso_id: egreso_id,
            egresofile_id: egresofile_id,
            vista: 'egreso'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_egresofile_form').html(data);
                $('#modal_registro_egresofile').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_egresofile'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_egresofile'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_egresofile', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'egresofile';
                var div = 'div_modal_egresofile_form';
                permiso_solicitud(usuario_act, egreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function egresofile_galeria() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egresofile/egresofile_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            egreso_id: $('#hdd_egreso_id').val()
        }),
        beforeSend: function () {
            $('#egresofile_galeria').html('Cargando imágenes...');
        },
        success: function (data) {
            $('#egresofile_galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#egresofile_galeria').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

function Cuenta_select_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuenta/cuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            tipocuenta: 2
        }),
        beforeSend: function () {
            $('#cmb_cue_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_cue_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}
$(document).ready(function () {

    $("#div_rendicion").hide()
    $("#cmb_subcue_id").change()
    $("#cmb_caj_id").val('1');
    $("#cmb_egr_est").val('1');
    Documento_select_form();
    $('#egreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });


    $("#txt_pro_doc,#txt_pro_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "proveedor/proveedor_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            $('#hdd_pro_id').val(ui.item.tb_proveedor_id);
            $('#txt_pro_doc').val(ui.item.tb_proveedor_doc);
            $('#txt_pro_nom').val(ui.item.tb_proveedor_nom);
            event.preventDefault();
            $('#txt_pro_nom').focus();
        }
    });


    egreso_tipo_mostrar_ocultar();

    $('#txt_egreso_can').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0',
        vMax: '50'
    });

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });

    $('#cmb_egresotipo_id').change(function (event) {
        egreso_tipo_mostrar_ocultar();
    });
    
    
$('#txt_egr_det').autocomplete({
		minLength: 1,
		source: VISTA_URL+"egreso/egreso_complete_det.php"
	});

$('#txt_egr_det').change(function(){
        $(this).val($(this).val().toUpperCase());
});

    $('#form_egreso').validate({
        submitHandler: function () {
            if ($("#hdd_pro_id").val()=="") {
                swal_warning("AVISO","Debe Ingresar Un Proveedor para poder continuar",5000);
                return false;
            }
            if ($("#txt_egr_imp").val()=="0.00") {
                swal_warning("AVISO","El importe no debe Ser Igual a S/ 0.00",5000);
                return false;
            }
            $.ajax({
                type: "POST",
                url: VISTA_URL + "egreso/egreso_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_egreso").serialize(),
                beforeSend: function () {
                    $('#egreso_mensaje').show(400);
                    $('#btn_guardar_egreso').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        console.log('data id = '+parseInt(data.cuotaproveedordetalle));
                        $('#egreso_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#egreso_mensaje').html(data.mensaje);
                        swal_success("SISTEMA",data.mensaje,5000);

                        var vista = $('#egreso_vista').val();

                            if (vista == 'egreso' || vista == 'creditomenor')
                                egreso_tabla();
                            /* GERSON (14-01-25) */
                            if (vista == 'permisoanulacion')
                                permisoanulacion_tabla();
                            /*  */
                            if (vista == 'egreso_proveedor'){
                                cuotaproveedor_cronograma(parseInt(data.cuotaproveedordetalle));
                                cuotaproveedor_tabla();
                            }
                            $('#modal_registro_egreso').modal('hide');

                    } else {
                        $('#egreso_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#egreso_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_egreso').prop('disabled', false);
                    }
                },
                complete: function (data) {
//                    console.log('data id = '+parseInt(data.cuotaproveedordetalle));
//                    console.log('ffffffff = '+data.mensaje);
                },
                error: function (data) {
                    
                    $('#egreso_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#egreso_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            cmb_caj_id: {
                required: true,
                min: 1
            },
            cmb_mon_id: {
                required: true,
                min: 1
            },
            cmb_egr_est: {
                required: true,
                min: 1
            },
            cmb_cue_id: {
                required: true,
                min: 1
            },
            txt_pro_doc: {
                required: true,
            },
            hdd_pro_id: {
                required: true,
            },
            cmb_doc_id: {
                required: true,
                min: 1
            },
            txt_egr_imp: {
                required: true,
            },
            txt_egr_det: {
                required: true,
            }
        },
        messages: {
            cmb_caj_id: {
                required: "Debe seleccionar una Caja",
                min: "Debe seleccionar al menos una caja"
            },
            cmb_mon_id: {
                required: "Debe seleccionar una Moneda",
                min: "Debe seleccionar al menos una Moneda"
            },
            cmb_egr_est: {
                required: "Debe seleccionar un estado",
                min: "Debe seleccionar al menos un estado"
            },
            cmb_cue_id: {
                required: "Debe seleccionar una cuenta",
                min: "Debe seleccionar al menos una cuenta"
            },
            txt_pro_doc: {
                required: "Debe ingresar un N° documento",
            },
            hdd_pro_id: {
                required: "Debe ingresar un Proveedor",
            },
            cmb_doc_id: {
                required: "Debe seleccionar un Documento",
                min: "Debe seleccionar al menos un Documento"
            },
            txt_egr_imp: {
                required: "Debe ingresar un Importe de Ingreso",
            },
            txt_egr_det: {
                required: "Debe ingresar un detalle",
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});

function Documento_select_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "documento/documento_select.php",
        async: true,
        dataType: "html",
        data: ({
            documento_tipo: 7,
            vista_documento: 'documento',
            documento_id: 9
        }),
        beforeSend: function () {
            $('#cmb_doc_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_doc_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function SubCuenta_select_form(cuenta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "subcuenta/subcuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            cuenta_id: cuenta_id
//			vista_subcategoria: 'producto'
        }),
        beforeSend: function () {
            $('#cmb_subcue_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_subcue_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$('#cmb_cue_id').change(function () {
    SubCuenta_select_form($('#cmb_cue_id').val());
});

$('#cmb_subcue_id').change(function () {
    if($(this).val() == 66 && $('#cmb_cue_id').val()==25){
        $("#div_rendicion").show()
    }else{
        $("#div_rendicion").hide()
    }
});

/* GERSON (14-01-25) */
// CUANDO LA ANULACION SE HACE DESDE PERMISOANULACION
function permisoanulacion_tabla(){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"permisoanulacion/permisoanulacion_tabla.php",
      async: true,
      dataType: "html",
      data: $('#permisoanulacion_filtro').serialize(),
      beforeSend: function() {
        $('#permisoanulacion_mensaje_tbl').show(300);
      },
      success: function(data){
        $('#div_permisoanulacion_tabla').html(data);
        $('#permisoanulacion_mensaje_tbl').hide(300);
  
        let registros = data.includes("No hay");
        console.log(registros);
        if (!registros) estilos_datatable();
        
      },
      complete: function(data){
        
      },
      error: function(data){
        $('#permisoanulacion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
      }
    });
}
/*  */

$('#cmb_subcue_id').change(function () {
    if(parseInt($(this).val()) == 66 && parseInt($('#cmb_cue_id').val())==25){
        $("#div_rendicion").show()
    }else{
        $("#div_rendicion").hide()
    }
});