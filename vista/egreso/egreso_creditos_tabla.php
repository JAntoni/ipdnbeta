<?php
require_once('../../core/usuario_sesion.php');
require_once('../creditoasiveh/Creditoasiveh.class.php');
$oCredito = new Creditoasiveh();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$cli_id = $_POST['cli_id'];

$dts = $oCredito->listar_creditos_asiveh_garveh_cliente($cli_id);






?>

<?php if ($dts['estado'] == 1) { ?>
    <table class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">TIPO</th>
                <th id="tabla_cabecera_fila">CODIGO</th>
                <th id="tabla_cabecera_fila">MONTO CREDITO</th>
                <th id="tabla_cabecera_fila">SELECCIONAR</th>
            </tr>
        </thead>
        <tbody>
<?php

//echo 'hasta aki estoy entyrando al Sistema 3333'.$dts['estado'];exit();
                foreach($dts['data']as $key=>$dt){

                $tipo = 'ASIVEH';
                $codigo = 'CAV-' . str_pad($dt['tb_credito_id'], 4, "0", STR_PAD_LEFT);

                if ($dt['tipo'] == 3) {
                    $tipo = 'GARVEH';
                    $codigo = 'CGV-' . str_pad($dt['tb_credito_id'], 4, "0", STR_PAD_LEFT);
                }
                if ($dt['tipo'] == 4) {
                    $tipo = 'HIPO';
                    $codigo = 'CH-' . str_pad($dt['tb_credito_id'], 4, "0", STR_PAD_LEFT);
                }

                $moneda = 'S/.';
                if ($dt['tb_moneda_id'] == 2)
                    $moneda = 'US$';
                ?>
                <tr  id="tabla_cabecera_fila">
                    <td id="tabla_fila"><b><?php echo $tipo ?></b></td>
                    <td id="tabla_fila"><?php echo $codigo ?></td>
                    <td id="tabla_fila"><?php echo $moneda . ' ' . mostrar_moneda($dt['tb_credito_preaco']) ?></td>
                    <td id="tabla_fila" align="center">
                        <input type="radio" name="txt_cre_select" class="txt_rad" onclick="credito_selecionado(<?php echo $dt['tipo'] . ',' . $dt['tb_credito_id'] . ',' . $dt['tb_cliente_id'] . ',' . $dt['tb_credito_preaco'] . ',' . $dt['tb_moneda_id'] ?>)"> 
                        &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-facebook btn-xs" onclick="credito_historial(<?php echo $dt['tipo'] . ',' . $dt['tb_credito_id'] . ',' . $dt['tb_cliente_id'] . ',' . $dt['tb_credito_preaco'] . ',' . $dt['tb_moneda_id'] ?>)" title="historial"><i class="fa fa-id-card-o" aria-hidden="true"></i></a> 
                    </td>
                </tr>

                <?php } ?>
        </tbody>
    </table>

    <?php
} else {
    ?>
    <label>El cliente seleccionado no tiene créditos activos</label>
<?php
}?>