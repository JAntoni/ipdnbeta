/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    cambio_moneda_desem(1);
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999999.99'
    });

    $('#egreso_fil_picker11').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $("#txt_seg_cli").autocomplete({
        minLength: 1,
        source: VISTA_URL + "cliente/cliente_autocomplete.php",
        select: function (event, ui) {

            creditos_tabla(ui.item.cliente_id);
            $("#txt_seg_cli").val(ui.item.cliente_nom)
        }
    });

    $("#txt_seg_tipseg").autocomplete({
        minLength: 1,
        source: VISTA_URL + "vehiculoadicional/adicionales_complete.php",
        select: function (event, ui) {
            //console.log('adicional id' + ui.item.id +' / pre_cli: ' + ui.item.precio_cli +' / pre ipdn: '+ui.item.precio_ipdn +' / utilidad: ' + ui.item.utilidad + ' // pro nom: ' +ui.item.prov_nom);
            $('#hdd_seg_tipseg').val(ui.item.id);
            $('#txt_seg_pre_cli').val(parseFloat(ui.item.precio_cli).toFixed(2));
            $('#txt_seg_pre_ipdn').val(parseFloat(ui.item.precio_ipdn).toFixed(2));
            $('#txt_seg_uti').val(parseFloat(ui.item.utilidad).toFixed(2));

            $('#hdd_pre_cli').val(parseFloat(ui.item.precio_cli).toFixed(2));
            $('#hdd_pre_ipdn').val(parseFloat(ui.item.precio_ipdn).toFixed(2));

            $('#hdd_seguro').val(ui.item.prov_nom);
            $('#hdd_moneda_adicional_id').val(ui.item.moneda_id);

            $('#hdd_seg_cre_pre').val(0);
            $(".txt_rad").each(function (i) {
                this.checked = false;
            });
            $('#msj_mon_dis').html('').hide();
        }
    });




 $("#for_egre_seg").validate({
    submitHandler: function() {
//      var pre_cli = Number($('#txt_seg_pre_cli').autoNumericGet());
      var pre_cli = Number($('#txt_seg_pre_cli').autoNumeric('get'));
      var mon_dis = Number($('#hdd_mon_dis').val());

      if(pre_cli > mon_dis){
        alert('El monto a desembolsar es mayor al monto disponible para adicionales y seguros');
        return false;
      }
      $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_adi_seguros_reg.php",
        async:true,
        dataType: "json",
        data: $("#for_egre_seg").serialize(),
        beforeSend: function(){
          $('#msj_egreso').html("Guardando...");
          $('#msj_egreso').show(100);
        },
        success: function(data){
          if(parseInt(data.estado) == 1){
            swal_success("PRESTAMOS DEL NORTE",data.egr_msj,3000);
            $('#modal_registro_egreso_adi').modal('hide');
            egreso_tabla();
          }
          else if(parseInt(data.estado) == 0){
            swal_warning("AVISO",data.egr_msj,3000);
          }
          else{
               swal_error("ERROR: ",'Existe un error al guardar el desembolso, repórtelo.',3000);
          }
        },
        complete: function(data){
          if(data.statusText != 'success'){
            console.log(data);
          }            
        }
      });     
    },
    rules: {
      hdd_seg_cre_id: {
        required: true
      },
      hdd_seg_cli_id: {
        required: true
      },
      txt_seg_tipcam: {
        required: true
      },
      txt_seg_pre_ipdn: {
        required: true
      },
      txt_seg_cli: {
        required: true
      },
      txt_seg_fina:{
        required: true
      }
    },
    messages: {
      hdd_seg_cre_id: {
        required: 'Falta Crédito'
      },
      hdd_seg_cli_id: {
        required: 'Falta el CLiente'
      },
      txt_seg_tipcam: {
        required: 'Registre los cambios de moneda'
      },
      txt_seg_pre_ipdn: {
        required: 'Ingrese el precio IPDN'
      },
      txt_seg_cli: {
        required: 'Seleccione un cliente'
      },
      txt_seg_fina:{
        required: 'Ingrese un detalle del egreso'
      }
    }
  });

});


/* TABLA QUE SE MUESTRA EN EL FORMULARIO DE TABLA DE CRÉDITOS*/

function creditos_tabla(cli) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_creditos_tabla.php",
        async: true,
        dataType: "html",
        data: {
            cli_id: cli
        },
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_creditos_tabla').html(html);
            modal_height_auto('modal_registro_egreso_adi');
        },
        complete: function () {

        }
    });
}

function cambio_moneda_desem(mon) {
    //si vuelve a elegir la misma oneda, todo regresa a como es ORGINALMENTE
    var tip = 'compra';
    if (mon == 2)
        tip = 'venta';

    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_controller.php",
        async: true,
        dataType: "json",
        data: ({
            action: "obtener_cambio",
            fecha: $('#txt_seg_fec').val(),
            tipo: tip
        }),
        beforeSend: function () {
        },
        success: function (data) {
            if (parseInt(data.estado) > 0) {
                $('#txt_seg_tipcam').val('1.00');
                if (mon == 2)
                    $('#txt_seg_tipcam').val(data.moncam_val);
                $('#hdd_tipcam').val(data.moncam_val);
            } else {
                $('#txt_seg_tipcam').val('');
                $('#hdd_tipcam').val('');
                $('#lbl_msj_mon').text('No hay registro de TC para la fecha de deselbolso, regístrelo primero.');
            }
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}

$('#cmb_seg_mon').change(function (event) {
    //var mon_adi = $('#hdd_moneda_adi').val();
    var mon_sel = $(this).val();
    if (mon_sel == 2) {
        cambio_moneda_desem(mon_sel);
    } else {
        $('#cmb_seg_mon').val(1);
    }
    $(".txt_rad").each(function (i) {
        this.checked = false;
    });
    $('#hdd_mon_dis').val(0);
    return false;
});


$('#cmb_credes_mot').change(function () {
    $(".txt_rad").each(function (i) {
        this.checked = false;
    });
});

/*PRECIO DEL CLIENTE O PRECIO DE IPDN*/
$('#txt_seg_pre_cli, #txt_seg_pre_ipdn').change(function (event) {
    var precio_cliente = parseFloat($('#txt_seg_pre_cli').val());
    var precio_ipdn = parseFloat($('#txt_seg_pre_ipdn').val());
        //100.00            90.00
    if (precio_cliente >= precio_ipdn) {
        calcular_utilidad();
    } else {
        $('#txt_seg_pre_ipdn').val("0.00");
        swal_warning("AVISO", "el precio IPDN no puede ser mayor al del Cliente", 3000);
    }

//    calcular_utilidad();
});
/* AL SELECCIONAR LA FECHA*/
$('#txt_seg_fec').change(function (event) {
    var mon = $('#cmb_seg_mon').val();
    cambio_moneda_desem(mon);
    calcular_utilidad();
});



function calcular_utilidad() {
    var pre_cli = Number($('#txt_seg_pre_cli').autoNumeric('get'));
    var pre_ipdn = Number($('#txt_seg_pre_ipdn').autoNumeric('get'));

    var tip_seg = $("#cmb_seg_tipseg option:selected").text();
    var arr_seg = tip_seg.split('-');

    if (arr_seg[0].trim() != 'STR') {
        var uti = 0;
        uti = pre_cli - pre_ipdn;

//        $('#txt_seg_uti').autoNumericSet(uti.toFixed(2));
        $('#txt_seg_uti').autoNumeric('set', uti.toFixed(2));
    } else {
        var pre_cli_ori = Number($('#hdd_pre_cli').val());
        var pre_ipdn_ori = Number($('#hdd_pre_ipdn').val());
        var x = 0;
        x = (pre_cli_ori * pre_ipdn) / pre_ipdn_ori;
        x = x.toFixed(2);
        $('#txt_seg_pre_cli').autoNumericSet(x);

        uti = x - pre_ipdn;
//        $('#txt_seg_uti').autoNumericSet(uti.toFixed(2));
        $('#txt_seg_uti').autoNumeric('set', uti.toFixed(2));
//    console.log('precio cli ori: '+pre_cli_ori + ' // pre ipdn ori: ' + pre_ipdn_ori+' // pre idp modificad: ' + pre_ipdn);

    }
}


function credito_selecionado(tipo, cre_id, cli_id, preaco, moneda_id) {
    $('#hdd_seg_cre_tip').val(tipo);
    $('#hdd_seg_cre_id').val(cre_id);
    $('#hdd_seg_cli_id').val(cli_id);
    $('#hdd_seg_cre_pre').val(preaco);

    var motivo = Number($('#cmb_credes_mot').val());

    if (motivo == 0) {
        swal_success("AVISO", 'Seleccione el motivo de la retención', 2500);
        $(".txt_rad").each(function (i) {
            this.checked = false;
        });
        return false;
    }
    //$('#hdd_moneda_adi').val(moneda_id);

    //$('#cmb_seg_mon').val(moneda_id);
    //$('#cmb_seg_mon').change();
    //consultamos el monto disponible en CAJA SEGUROS para hacer los desembolsos
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_adi_seguros_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: "caja_seguros",
            cre_tip: tipo,
            cre_id: cre_id,
            retencion_mot: motivo,
            mon_id: $('#cmb_seg_mon').val()
        }),
        beforeSend: function () {
            $('#msj_mon_dis').show(100);
            $('#msj_mon_dis').text('Consultando monto disponible...');
        },
        success: function (data) {
            if (parseInt(data.est) > 0) {
                $('#msj_mon_dis').text('Monto disponible en CAJA SEGUROS: ' + data.des);
                $('#hdd_mon_dis').val(data.monto);
            }
        },
        complete: function (data) {
            if (data.statusText != "success")
                $('#msj_mon_dis2').text('Error al consultar monto disponible, reporte este error: ' + data.responseText);
        }
    });
}

function credito_historial(tipo, cre_id, cli_id, preaco, moneda_id) {
    $('#hdd_seg_cre_tip').val(tipo);
    $('#hdd_seg_cre_id').val(cre_id);
    $('#hdd_seg_cli_id').val(cli_id);
    $('#hdd_seg_cre_pre').val(preaco);

    var motivo = Number($('#cmb_credes_mot').val());

    if (motivo == 0) {
        swal_success("AVISO", 'Seleccione el motivo de la retención', 2500);
        $(".txt_rad").each(function (i) {
            this.checked = false;
        });
        return false;
        }
    //consultamos el monto disponible en CAJA SEGUROS para hacer los desembolsos
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_adi_seguros_historial.php",
        async: true,
        dataType: "html",
        data: ({
            cre_tip: tipo,
            cre_id: cre_id,
            retencion_mot: motivo,
            mon_id: $('#cmb_seg_mon').val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_egreso_adi_historial').html(html);
            $('#modal_egreso_adi_historial').modal('show');
//            modal_hidden_bs_modal('modal_egreso_adi_historial', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_egreso_adi_historial'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un ancho automatico al modal, al abrirlo

        },
        complete: function (data) {
//            console.log(data);
        }
    });
}


function cmb_seg_tipseg()
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vehiculoadicional/vehiculoadicional_listar_select.php",
        async: false,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#cmb_seg_tipseg').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_seg_tipseg').html(html);
        }
    });
}