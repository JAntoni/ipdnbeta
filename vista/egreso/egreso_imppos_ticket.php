<?php
require '../../libreriasphp/escpos/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

try 
{
	
	$con="smb://".$_POST['imp_ip']."/".$_POST['imp_nomloc'];
	//$con="smb://localhost/impresora1";
	$connector = new WindowsPrintConnector($con);

	$printer = new Printer($connector);
	//$printer -> pulse();
	//$printer -> feedReverse(1);
	
	$printer -> feed();
	$printer -> feed();

	$printer -> setFont(Printer::MODE_FONT_A);
	$printer -> setJustification(Printer::JUSTIFY_CENTER);

	$printer -> setEmphasis(true);
	$printer -> text("PRESTAMOS DEL NORTE\n");
	$printer -> setEmphasis(false);

	$printer -> text("C.C. Boulevard Plaza Int. J\n");
	$printer -> text("Chiclayo, Chiclayo, Lambayeque\n");
	$printer -> text($_POST['fechareg']."\n");

	$printer -> text("------------------------------------------\n");
	$printer -> text("N° DE OPERACION: ".$_POST['operacion']."\n");
	$printer -> text("------------------------------------------\n");

	$printer -> feed();

	$printer -> setJustification();
	$printer -> text("CLIENTE   : ".$_POST['cliente']."\n");
	$printer -> text("RUC/DNI   : ".$_POST['cliente_doc']."\n");
	$printer -> text("DIRECCION : ".$_POST['cliente_dir']."\n");
	$printer -> feed();
	$printer -> text("CONCEPTO  : ".$_POST['detalle']."\n");

	$printer -> feed();
	$printer -> setEmphasis(true);
	$printer -> text("MONTO     : ".$_POST['moneda'].' '.$_POST['monto']."\n");
	
	if($_POST['monto_cambio']>0)
	{
	$printer -> text("TIP CAMB. : ".$_POST['tipo_cambio']."\n");
	$printer -> text("CAMBIO    : ".$_POST['monto_cambio']."\n");
	}

	$printer -> feed();
	$printer -> text("------------------------------------------\n");
	$printer -> text("REGISTRADO : ".$_POST['cajero']."\n");

	$printer -> setEmphasis(false);

	$printer -> setJustification(Printer::JUSTIFY_CENTER);
	// $printer -> text("------------------------------------------\n");
	// $printer -> text("SON: ".$_POST['monto_letras'].$montext."\n");
	// $printer -> text("------------------------------------------\n");

	$printer -> feed();
	$printer -> setFont(Printer::MODE_FONT_B);
	$printer -> text("www.prestamosdelnortechiclayo.com\n");
	$printer -> feed();
	$printer -> feed();

	$printer -> cut();

	if($_POST['copia']==1):
	$printer -> feed();
	$printer -> feed();

	$printer -> setFont(Printer::MODE_FONT_A);
	$printer -> setJustification(Printer::JUSTIFY_CENTER);

	$printer -> setEmphasis(true);
	$printer -> text("PRESTAMOS DEL NORTE\n");
	$printer -> setEmphasis(false);

	$printer -> text("C.C. Boulevard Plaza Int. J\n");
	$printer -> text("Chiclayo, Chiclayo, Lambayeque\n");
	$printer -> text($_POST['fechareg']."\n");

	$printer -> text("------------------------------------------\n");
	$printer -> text("N° DE OPERACION: ".$_POST['operacion']."\n");
	$printer -> text("------------------------------------------\n");

	$printer -> feed();

	$printer -> setJustification();
	$printer -> text("CLIENTE   : ".$_POST['cliente']."\n");
	$printer -> text("RUC/DNI   : ".$_POST['cliente_doc']."\n");
	$printer -> text("DIRECCION : ".$_POST['cliente_dir']."\n");
	$printer -> feed();
	$printer -> text("CONCEPTO  : ".$_POST['detalle']."\n");

	$printer -> feed();
	$printer -> setEmphasis(true);
	$printer -> text("MONTO     : ".$_POST['moneda'].' '.$_POST['monto']."\n");
	
	if($_POST['monto_cambio']>0)
	{
	$printer -> text("TIP CAMB. : ".$_POST['tipo_cambio']."\n");
	$printer -> text("CAMBIO    : ".$_POST['monto_cambio']."\n");
	}

	$printer -> feed();
	$printer -> text("------------------------------------------\n");
	$printer -> text("REGISTRADO : ".$_POST['cajero']."\n");

	$printer -> setEmphasis(false);
	// $printer -> text("------------------------------------------\n");
	// $printer -> text("SON: ".$_POST['monto_letras'].$montext."\n");
	// $printer -> text("------------------------------------------\n");

	$printer -> feed();
	$printer -> text("FIRMA    : "."\n");
	$printer -> feed();
	$printer -> feed();
	$printer -> feed();
	$printer -> feed();
	$printer -> feed();

	$printer -> setJustification(Printer::JUSTIFY_CENTER);
	$printer -> setFont(Printer::MODE_FONT_B);
	$printer -> text("www.prestamosdelnortechiclayo.com\n");
	$printer -> feed();
	$printer -> feed();

	$printer -> cut();

	endif;

	$printer -> close();
}
catch(Exception $e)
{
	echo "Error de conexión: " . $e -> getMessage() . "\n";
}

?>