<?php
require_once('../../core/usuario_sesion.php');

require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$cre_tip = $_POST['cre_tip'];
$cre_id = $_POST['cre_id'];
$mon_id = $_POST['mon_id'];

$moneda = 'S/.';
if ($mon_id == 2)
    $moneda = 'US$';

//INGRESO TOTAL EN CAJA RETENCION, MONTOS RETENIDOS
$mod_id = 52; // 52 es ingresos por seguros de Asiveh
if ($cre_tip == 3)
    $mod_id = 53; // 53 es ingresos por seguros de Garveh
if ($cre_tip == 4)
    $mod_id = 54; // 54 es ingresos por tramites de HIPO

$modide = $_POST['cre_id'];
$est = 1; // estado del ingreso, 1 vigente
$caj_id = 15; //ID DE LA CAJA RETENCION ES 15
$ingreso_mot = intval($_POST['retencion_mot']);
//$ingreso_mot = 0;

$mon_rete = 0;
$dts = $oIngreso->mostrar_caja_retencion_motivo($mod_id, $modide, $mon_id, $caj_id, $ingreso_mot);

if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $mensaje_ingreso .= '&FilledSmallSquare; ' . $dt['tb_ingreso_det'] . '<br><br>';
    }
}
$dts = NULL;

//EGRESOS DE CAJA RETENCION PARA PAGOS, el modulo_id de egreso de caja seguros es 101, en el modide se guarda el id del credito y para diferenciar de asiveh con garveh es mediante la subcuenta que en EGRESO para Asiveh es 57 y Garveh es 58
$mod_id = 101; //para egresos de caja seguros el modulo_id será 101
$modide = $_POST['cre_id'];
$est = 1;
$caj_id = 15; // caja seguros
$subcue_id = 57; // es la subcuenta para asiveh
if ($cre_tip == 3)
    $subcue_id = 58; // es la subcuenta para garveh
if ($cre_tip == 4)
    $subcue_id = 59; // es la subcuenta para hipotecario
$egreso_mot = intval($_POST['retencion_mot']);

$mon_egre = 0;
$dts = $oEgreso->mostrar_egreso_caja_retencion_motivo($mod_id, $modide, $mon_id, $caj_id, $subcue_id, $egreso_mot);

if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $mensaje .= '&FilledSmallSquare; ' . $dt['tb_egreso_det'] . '<br><br>';
    }
}
$dts = NULL;
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_egreso_adi_historial" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #660066">HISTRORIAL  DE EGRESOS E INGRESOS </h4>
            </div>
            <form id="form_egreso_adi_seguros_historial" method="post">
                <input type="hidden" name="action" value="<?php echo $action; ?>">

                <div class="modal-body">

                    <label style="font-family: cambria;font-weight: bold;color: #00733e">EGRESOS GENERADOS</label>
                    <div class="box box-warning">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $mensaje; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <label style="font-family: cambria;font-weight: bold;color:  #00733e">INGRESOS GENERADOS </label>
                    <div class="box box-warning">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $mensaje_ingreso; ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="egreso_adi_seguros_historial_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>