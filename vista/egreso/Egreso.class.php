<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Egreso extends Conexion{
    public $egreso_id;
    public $egreso_xac = 1;
    public $egreso_usureg;
    public $egreso_usumod;
    public $egreso_fec;
    public $documento_id;
    public $egreso_numdoc;
    public $egreso_det;
    public $egreso_imp;
    public $egreso_tipcam;
    public $egreso_est;
    public $cuenta_id;
    public $subcuenta_id;
    public $proveedor_id;
    public $cliente_id;
    public $usuario_id;
    public $caja_id;
    public $moneda_id;
    public $modulo_id;
    public $egreso_modide;
    public $empresa_id;
    public $egreso_ap; //si es egreso de AP (acuerdo de pago)

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_egreso(
                                    tb_egreso_usureg,tb_egreso_fec,tb_documento_id, 
                                    tb_egreso_numdoc,tb_egreso_det,tb_egreso_imp, 
                                    tb_egreso_tipcam,tb_egreso_est,tb_cuenta_id,tb_subcuenta_id, 
                                    tb_proveedor_id,tb_cliente_id,tb_usuario_id, 
                                    tb_caja_id,tb_moneda_id,tb_modulo_id, 
                                    tb_egreso_modide,tb_empresa_id,tb_egreso_ap)
                            VALUES(
                                    :egreso_usureg, :egreso_fec, :documento_id, 
                                    :egreso_numdoc, UPPER(:egreso_det), :egreso_imp, 
                                    :egreso_tipcam, :egreso_est, :cuenta_id, :subcuenta_id, 
                                    :proveedor_id, :cliente_id, :usuario_id, 
                                    :caja_id, :moneda_id, :modulo_id, 
                                    :egreso_modide, :empresa_id, :egreso_ap)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_usureg", $this->egreso_usureg, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_fec", $this->egreso_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":documento_id", $this->documento_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_numdoc", $this->egreso_numdoc, PDO::PARAM_STR);
        $sentencia->bindParam(":egreso_det", $this->egreso_det, PDO::PARAM_STR);
        $sentencia->bindParam(":egreso_imp", $this->egreso_imp, PDO::PARAM_STR);
        $sentencia->bindParam(":egreso_tipcam", $this->egreso_tipcam, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_est", $this->egreso_est, PDO::PARAM_INT);
        $sentencia->bindParam(":cuenta_id", $this->cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":subcuenta_id", $this->subcuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedor_id", $this->proveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":caja_id", $this->caja_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_modide", $this->egreso_modide, PDO::PARAM_INT);
        $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_ap", $this->egreso_ap, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $egreso_id = $this->dblink->lastInsertId();
        
//        $sql="select tb_documento_abr from tb_documento where tb_documento_id=:tb_documento_id";
//        
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":tb_documento_id", $this->documento_id, PDO::PARAM_INT);
//        $sentencia->execute();
//        $result2 = $sentencia->fetch();
//        $tb_documento_abr = $result2["tb_documento_abr"]." ".$egreso_id;
        
//        echo 'abreviatura de documento : '.$tb_documento_abr;exit();
        $sql="UPDATE tb_egreso SET tb_egreso_numdoc=:tb_egreso_numdoc WHERE tb_egreso_id=:tb_documento_id";
        
        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":tb_egreso_numdoc",$tb_documento_abr, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_egreso_numdoc", $egreso_id, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_documento_id",$egreso_id, PDO::PARAM_INT);
        $sentencia->execute();
        
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['egreso_id'] = $egreso_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    
    
    function modificar(){
        
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_egreso SET
                            tb_egreso_fecmod = NOW( ),
                            tb_egreso_usumod =:egreso_usumod,
                            tb_egreso_fec    =:egreso_fec,
                            tb_documento_id  =:documento_id,
                            tb_egreso_numdoc = :egreso_numdoc,
                            tb_egreso_det    =:egreso_det,
                            tb_egreso_imp    =:egreso_imp,
                            tb_egreso_tipcam =:egreso_tipcam,
                            tb_egreso_est    =:egreso_est,
                            tb_cuenta_id     = :cuenta_id,
                            tb_subcuenta_id  =:subcuenta_id,
                            tb_proveedor_id  =:proveedor_id,
                            tb_cliente_id    =:cliente_id,
                            tb_usuario_id    =:usuario_id,
                            tb_caja_id       =:caja_id ,
                            tb_moneda_id     =:moneda_id,
                            tb_modulo_id     =:modulo_id, 
                            tb_egreso_modide =:egreso_modide,
                            tb_empresa_id    =:empresa_id,
                            tb_egreso_ap     =:egreso_ap
                        WHERE 
                            tb_egreso_id     =:egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_id", $this->egreso_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_usumod", $this->egreso_usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_fec", $this->egreso_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":documento_id", $this->documento_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_numdoc", $this->egreso_numdoc, PDO::PARAM_STR);
        $sentencia->bindParam(":egreso_det", $this->egreso_det, PDO::PARAM_STR);
        $sentencia->bindParam(":egreso_imp", $this->egreso_imp, PDO::PARAM_STR);
        $sentencia->bindParam(":egreso_tipcam", $this->egreso_tipcam, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_est", $this->egreso_est, PDO::PARAM_INT);
        $sentencia->bindParam(":cuenta_id", $this->cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":subcuenta_id", $this->subcuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedor_id", $this->proveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":caja_id", $this->caja_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_modide", $this->egreso_modide, PDO::PARAM_INT);
        $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_ap", $this->egreso_ap, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function eliminar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_egreso SET tb_egreso_xac=0 WHERE tb_egreso_id=:tb_egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_egreso_id", $this->egreso_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function cambiar_xac_por_modulo_modide($valor, $mod_id, $modide){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_egreso SET 
                                    tb_egreso_xac =:tb_egreso_xac 
                            where 
                                    tb_modulo_id =:tb_modulo_id and tb_egreso_modide =:tb_egreso_modide";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_egreso_xac", $valor, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_modulo_id", $mod_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_egreso_modide", $modide, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function mostrarUno($egreso_id){
      try {
        $sql = "SELECT 
                        * 
                FROM 
                        tb_egreso e
                        INNER JOIN tb_cuenta c ON e.tb_cuenta_id = c.tb_cuenta_id
                        INNER JOIN tb_caja cj ON e.tb_caja_id = cj.tb_caja_id
                        INNER JOIN tb_proveedor p ON e.tb_proveedor_id = p.tb_proveedor_id
                        INNER JOIN tb_documento d ON e.tb_documento_id=d.tb_documento_id
                        LEFT JOIN tb_subcuenta sc ON e.tb_subcuenta_id = sc.tb_subcuenta_id
                        INNER JOIN tb_usuario u ON e.tb_egreso_usureg=u.tb_usuario_id
                        INNER JOIN tb_moneda m ON e.tb_moneda_id=m.tb_moneda_id
                WHERE 
                        tb_egreso_xac=1 AND tb_egreso_id =:egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_egresos($fec1,$fec2,$emp_id,$caj_id,$mon_id,$pro_id,$est,$doc_id,$numdoc,$cue_id,$subcue_id,$usuario_id){
        $extension="";
        if($emp_id>0)
            $extension=  $extension." AND e.tb_empresa_id = $emp_id";
        if($caj_id>0)
            $extension=  $extension." AND e.tb_caja_id = $caj_id";
        if($mon_id>0)
            $extension=  $extension." AND e.tb_moneda_id = $mon_id";
        if($pro_id>0)
            $extension=  $extension." AND e.tb_proveedor_id = $pro_id ";
        if($est>0)
            $extension=  $extension." AND e.tb_egreso_est LIKE  $est" ;
        if($doc_id>0)
            $extension=  $extension." AND e.tb_documento_id = $doc_id " ;
        if($numdoc>0)
            $extension=  $extension." AND e.tb_egreso_numdoc LIKE '%$numdoc%' " ;
        if($cue_id>0)
            $extension=  $extension." AND e.tb_cuenta_id = $cue_id " ;
        if($subcue_id>0)
            $extension=  $extension." AND e.tb_subcuenta_id = $subcue_id " ;
        if($usuario_id>0)
            $extension=  $extension." AND e.tb_egreso_usureg = $usuario_id " ;
        
      try {
        $sql = "SELECT 
                        e.*, u.tb_usuario_nom,u.tb_usuario_ape,cj.*,p.*,d.*,sc.*,c.*
                FROM 
                        tb_egreso e
                        INNER JOIN tb_cuenta c ON e.tb_cuenta_id = c.tb_cuenta_id
                        INNER JOIN tb_caja cj ON e.tb_caja_id = cj.tb_caja_id
                        INNER JOIN tb_proveedor p ON e.tb_proveedor_id = p.tb_proveedor_id
                        INNER JOIN tb_documento d ON e.tb_documento_id=d.tb_documento_id
                        LEFT JOIN tb_subcuenta sc ON e.tb_subcuenta_id = sc.tb_subcuenta_id
                        INNER JOIN tb_usuario u ON e.tb_egreso_usureg=u.tb_usuario_id
                WHERE 
                        tb_egreso_xac=1                        
                        AND tb_egreso_fec BETWEEN :fec1 AND :fec2   
                        ".$extension." ORDER BY tb_egreso_fec ";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
//        $sentencia->bindParam(":emp_id", $emp_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Egresos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    function mostrar_por_modulo($modulo_id, $egreso_modide, $moneda_id, $egreso_xac){
      $concatenar = "";
      if (intval($moneda_id) > 0) {
        $concatenar = " AND tb_moneda_id =:moneda_id ";
      }
      try {
        $sql = "SELECT * FROM tb_egreso eg 
              WHERE tb_modulo_id =:modulo_id 
              and tb_egreso_modide =:egreso_modide 
              and tb_egreso_xac =:egreso_xac" . $concatenar;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_modide", $egreso_modide, PDO::PARAM_INT);
        //$sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        if (intval($moneda_id) > 0) {
          $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        }
        $sentencia->bindParam(":egreso_xac", $egreso_xac, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function modificar_campo($egreso_id, $egreso_columna, $egreso_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($egreso_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_egreso SET ".$egreso_columna." =:egreso_valor WHERE tb_egreso_id =:egreso_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":egreso_valor", $egreso_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":egreso_valor", $egreso_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el egreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function total_egresos_fecha($egreso_fec1, $egreso_fec2, $usuario_id, $empresa_id,$moneda_id){
      try {
        $fecha = ''; $usuario = '';
        if(!empty($egreso_fec1) && !empty($egreso_fec2)){
          $fecha = ' AND tb_egreso_fec BETWEEN :egreso_fec1 AND :egreso_fec2';
        }
        if(intval($usuario_id) > 0)
          $usuario = ' AND tb_egreso_usureg ='.intval($usuario_id);

        $sql = "SELECT IFNULL(SUM(tb_egreso_imp),0) AS total_egresos FROM tb_egreso WHERE tb_egreso_xac = 1 AND tb_empresa_id =:empresa_id AND tb_moneda_id=:tb_moneda_id ".$fecha.''.$usuario;

        $sentencia = $this->dblink->prepare($sql);
        if(!empty($egreso_fec1) && !empty($egreso_fec2)){
          $sentencia->bindParam(":egreso_fec1", $egreso_fec1, PDO::PARAM_STR);
          $sentencia->bindParam(":egreso_fec2", $egreso_fec2, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_moneda_id", $moneda_id, PDO::PARAM_INT);
        }
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay egresos registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function total_desembolsos_fecha($egreso_fec1, $egreso_fec2, $usuario_id, $empresa_id,$moneda_id,$cuenta_id,$subcuenta_id){
      try {
        $fecha = ''; $usuario = '';
        if(!empty($egreso_fec1) && !empty($egreso_fec2)){
          $fecha = ' AND tb_egreso_fec BETWEEN :egreso_fec1 AND :egreso_fec2';
        }
        if(intval($usuario_id) > 0)
          $usuario = ' AND tb_egreso_usureg ='.intval($usuario_id);
        
        if(intval($subcuenta_id) > 0)
          $subcuenta = ' AND tb_subcuenta_id ='.intval($subcuenta_id);
        
        $sql = "SELECT IFNULL(SUM(tb_egreso_imp),0) AS total_desembolso FROM tb_egreso WHERE tb_egreso_xac = 1 AND tb_empresa_id =:empresa_id AND tb_moneda_id=:tb_moneda_id AND tb_cuenta_id=:tb_cuenta_id ".$fecha.''.$usuario.' '.$subcuenta;

        $sentencia = $this->dblink->prepare($sql);
        if(!empty($egreso_fec1) && !empty($egreso_fec2)){
          $sentencia->bindParam(":egreso_fec1", $egreso_fec1, PDO::PARAM_STR);
          $sentencia->bindParam(":egreso_fec2", $egreso_fec2, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_moneda_id", $moneda_id, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_cuenta_id", $cuenta_id, PDO::PARAM_INT);
        }
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay egresos registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_egreso_caja_retencion_motivo($mod_id,$modide,$mon_id,$caj_id,$subcue_id, $egreso_mot){
      try {
        $sql = "SELECT * FROM tb_egreso e
			LEFT JOIN tb_subcuenta sc ON e.tb_subcuenta_id = sc.tb_subcuenta_id
			WHERE tb_egreso_xac=1 AND tb_modulo_id=:modulo_id AND e.tb_egreso_modide=:modide 
			AND e.tb_moneda_id =:moneda_id AND tb_caja_id =:caja_id AND e.tb_subcuenta_id =:subcuenta_id 
			AND tb_egreso_mot =:mot";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $mod_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modide", $modide, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
        $sentencia->bindParam(":caja_id", $caj_id, PDO::PARAM_INT);
        $sentencia->bindParam(":subcuenta_id", $subcue_id, PDO::PARAM_INT);
        $sentencia->bindParam(":mot", $egreso_mot, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function anular_egreso_acuerdopago($egreso_valor, $cuotapago_id){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_egreso SET tb_egreso_xac =:egreso_valor WHERE tb_egreso_ap =:cuotapago_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cuotapago_id", $cuotapago_id, PDO::PARAM_INT);
          $sentencia->bindParam(":egreso_valor", $egreso_valor, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el egreso retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function anular_egresos_por_modulo($egreso_valor, $modulo_id, $egreso_modide){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_egreso SET tb_egreso_xac =:egreso_valor WHERE tb_modulo_id =:modulo_id AND tb_egreso_modide =:egreso_modide";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_valor", $egreso_valor, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":egreso_modide", $egreso_modide, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function listar_egresos_filtro($emp_id,$caj_id,$fec1,$fec2,$cue_id,$subcue_id,$doc_id,$numdoc,$pro_id,$est,$mon_id){
      try {
        $sql = "SELECT * 
	FROM tb_egreso e
	INNER JOIN tb_cuenta c ON e.tb_cuenta_id = c.tb_cuenta_id
	INNER JOIN tb_caja cj ON e.tb_caja_id = cj.tb_caja_id
	INNER JOIN tb_proveedor p ON e.tb_proveedor_id = p.tb_proveedor_id
	INNER JOIN tb_documento d ON e.tb_documento_id=d.tb_documento_id
	LEFT JOIN tb_subcuenta sc ON e.tb_subcuenta_id = sc.tb_subcuenta_id
	INNER JOIN tb_usuario u ON e.tb_egreso_usureg=u.tb_usuario_id
	WHERE tb_egreso_xac=1
	AND e.tb_empresa_id=$emp_id 
	AND tb_moneda_id=$mon_id
	AND tb_egreso_fec BETWEEN '$fec1' AND '$fec2' ";
        
        if($caj_id>0){
		$sql = $sql." AND e.tb_caja_id = $caj_id ";
		}
		if($cue_id>0){
		$sql = $sql." AND e.tb_cuenta_id = $cue_id ";
		}
		if($subcue_id>0){
		$sql = $sql." AND e.tb_subcuenta_id = $subcue_id ";
		}
		if($doc_id>0){
		$sql = $sql." AND e.tb_documento_id = $doc_id ";
		}
		if($numdoc!=""){
		$sql = $sql." AND tb_egreso_numdoc LIKE '%$numdoc%' ";
		}
		
		if($pro_id>0){
		$sql = $sql." AND e.tb_proveedor_id = $pro_id ";
		}
		if($est!=''){
		$sql = $sql." AND tb_egreso_est LIKE '$est' ";
		}
		
		$sql = $sql." ORDER BY tb_egreso_fec ";
        
        
        $sentencia = $this->dblink->prepare($sql);
//          $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    	function mostrar_filtro($emp_id,$caj_id,$fec1,$fec2,$cue_id,$subcue_id,$doc_id,$numdoc,$pro_id,$est,$mon_id){
	$sql="SELECT * 
	FROM tb_egreso e
	INNER JOIN tb_cuenta c ON e.tb_cuenta_id = c.tb_cuenta_id
	INNER JOIN tb_caja cj ON e.tb_caja_id = cj.tb_caja_id
	INNER JOIN tb_proveedor p ON e.tb_proveedor_id = p.tb_proveedor_id
	INNER JOIN tb_documento d ON e.tb_documento_id=d.tb_documento_id
	LEFT JOIN tb_subcuenta sc ON e.tb_subcuenta_id = sc.tb_subcuenta_id
	INNER JOIN tb_usuario u ON e.tb_egreso_usureg=u.tb_usuario_id
	WHERE tb_egreso_xac=1
	AND e.tb_empresa_id=$emp_id 
	AND tb_moneda_id=$mon_id
	AND tb_egreso_fec BETWEEN '$fec1' AND '$fec2' ";
	
		if($caj_id>0){
		$sql = $sql." AND e.tb_caja_id = $caj_id ";
		}
		if($cue_id>0){
		$sql = $sql." AND e.tb_cuenta_id = $cue_id ";
		}
		if($subcue_id>0){
		$sql = $sql." AND e.tb_subcuenta_id = $subcue_id ";
		}
		if($doc_id>0){
		$sql = $sql." AND e.tb_documento_id = $doc_id ";
		}
		if($numdoc!=""){
		$sql = $sql." AND tb_egreso_numdoc LIKE '%$numdoc%' ";
		}
		
		if($pro_id>0){
		$sql = $sql." AND e.tb_proveedor_id = $pro_id ";
		}
		if($est!=''){
		$sql = $sql." AND tb_egreso_est LIKE '$est' ";
		}
		
		$sql = $sql." ORDER BY tb_egreso_fec ";
		

	  return $sql;
	}
    
        
            function revisar_pago_colaborar_cuenta($cue_id, $subcue_id, $pro_id, $anio_mes) {
        try {

            $sql = "SELECT * FROM tb_egreso where tb_cuenta_id =:tb_cuenta_id and tb_subcuenta_id =:tb_subcuenta_id and tb_proveedor_id =:tb_proveedor_id and tb_egreso_modide =:tb_egreso_modide and tb_egreso_xac = 1";
            
            $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuenta_id", $cue_id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_subcuenta_id", $subcue_id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_proveedor_id", $pro_id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_egreso_modide", $anio_mes, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay egresos registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    
    function complete_det($emp,$det){
        $filtro = "%".$det."%";
        try {
            $sql="SELECT 
                        DISTINCT(tb_egreso_det) 
                    FROM 
                        tb_egreso
                    WHERE 
                        tb_empresa_id=:tb_empresa_id AND tb_egreso_det like :filtro ORDER BY tb_egreso_det LIMIT 0 , 10";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_empresa_id", $emp, PDO::PARAM_INT);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    
    
//    function egresos_caja_cliente($caja_id, $mon_id, $mod_id, $cli_id){
//        try {
//            $sql = "SELECT * FROM tb_egreso where tb_caja_id =:caja_id and tb_moneda_id =:mon_id and tb_modulo_id =:mod_id and tb_egreso_modide =:cli_id and tb_egreso_xac =1";		
//
//            $sentencia = $this->dblink->prepare($sql);
//            $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);
//            $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
//            $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
//            $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
//            $sentencia->execute();
//
//            if ($sentencia->rowCount() > 0) {
//                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
//                $retorno["estado"] = 1;
//                $retorno["mensaje"] = "exito";
//                $retorno["data"] = $resultado;
//                $sentencia->closeCursor(); //para libera memoria de la consulta
//            } else {
//                $retorno["estado"] = 0;
//                $retorno["mensaje"] = "No hay tipos de crédito registrados";
//                $retorno["data"] = "";
//            }
//
//            return $retorno;
//        } catch (Exception $e) {
//            throw $e;
//        }
//    }

    function egresos_caja_cliente($caja_id, $mon_id, $mod_id, $cli_id){

      try {
        $sql = "SELECT * FROM tb_egreso 
                where tb_caja_id =:caja_id 
                and tb_moneda_id =:mon_id 
                and tb_modulo_id =:mod_id 
                and tb_egreso_modide =:cli_id 
                and tb_egreso_xac =1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);
        $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
        $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Egresos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function total_egresos_cuenta_subcuenta_usuario($egreso_fec1, $egreso_fec2, $usuario_id, $empresa_id, $moneda_id, $cuenta_id, $subcuenta_id){
      try {
        $fecha = ''; $usuario = '';
        if(!empty($egreso_fec1) && !empty($egreso_fec2)){
          $fecha = ' AND tb_egreso_fec BETWEEN :egreso_fec1 AND :egreso_fec2';
        }
        if(intval($usuario_id) > 0)
          $usuario = ' AND tb_egreso_usureg ='.intval($usuario_id);
        
        if(intval($cuenta_id) > 0)
          $cuenta = ' AND tb_cuenta_id ='.intval($cuenta_id);

        if(!empty($subcuenta_id))
          $subcuenta = ' AND tb_subcuenta_id IN ('.$subcuenta_id.')';
        
        $sql = "SELECT IFNULL(SUM(tb_egreso_imp),0) AS total_egresos FROM tb_egreso 
          WHERE tb_egreso_xac = 1 AND tb_empresa_id =:empresa_id AND tb_moneda_id =:tb_moneda_id ".$fecha.''.$usuario.' '.$cuenta.' '.$subcuenta;

        $sentencia = $this->dblink->prepare($sql);
        if(!empty($egreso_fec1) && !empty($egreso_fec2)){
          $sentencia->bindParam(":egreso_fec1", $egreso_fec1, PDO::PARAM_STR);
          $sentencia->bindParam(":egreso_fec2", $egreso_fec2, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_moneda_id", $moneda_id, PDO::PARAM_INT);
        }
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay egresos registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function eliminar_2(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_egreso SET tb_egreso_xac=0 WHERE tb_egreso_id=:tb_egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_egreso_id", $this->egreso_id, PDO::PARAM_INT);

        $sentencia->execute();

        if($sentencia->rowCount() > 0){
          $this->dblink->commit();
          return 1;//si es correcto retorna 1
        } else {
          return 0;
        }

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrar_egresos_pagogasto($egreso_modide){
      try {
        $sql = "SELECT * FROM tb_egreso eg
                LEFT JOIN tb_gastopago gp ON (eg.tb_egreso_id = gp.tb_egreso_idfk)
                WHERE tb_modulo_id = 10
                and tb_egreso_xac = 1
                and tb_egreso_modide =:egreso_modide ORDER BY eg.tb_egreso_fecreg ASC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_modide", $egreso_modide, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

  }
  

?>
