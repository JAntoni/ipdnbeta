<label for="txt_filtro" class="control-label">Informacíon de Egresos</label>
<div class="box box-primary">
    <div class="box-header">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Fecha :</label>
                    <div class='input-group date' id='egreso_fil_picker1'>
                        <input type='text' class="form-control input-sm" name="txt_egr_fec" id="txt_egr_fec" value="<?php echo $egreso_fec;?>" readonly="">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Caja</label>
                        <select class="form-control input-sm" id="cmb_caj_id" name="cmb_caj_id">
                            <?php include '../caja/caja_select.php';?>
                        </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Moneda</label>
                    <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm">
                        <?php include '../moneda/moneda_select.php';?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Estado</label>
                    <select name="cmb_egr_est" id="cmb_egr_est" class="form-control input-sm">
                        <option value="0">-</option>
                        <option value="1"  <?php if($egreso_est==1)echo 'selected'?>>CANCELADO</option>
                        <option value="2"  <?php if($egreso_est==2)echo 'selected'?>>EMITIDO</option>
                    </select>
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Cuenta</label>
                    <select name="cmb_cue_id" id="cmb_cue_id" class="form-control input-sm">
                        <?php include '../cuenta/cuenta_select.php';?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Sub Cuenta</label>
                    <select name="cmb_subcue_id" id="cmb_subcue_id" class="form-control input-sm">
                        <?php include '../subcuenta/subcuenta_select.php';?>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">RUC/DNI</label>
                        <input name="txt_pro_doc" type="text" id="txt_pro_doc" maxlength="11" class="form-control input-sm mayus" value="<?php echo $proveedor_doc;?>">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label for="">ANEXO</label>
                    <input type="text" id="txt_pro_nom" name="txt_pro_nom" class="form-control input-sm mayus" value="<?php echo $proveedor_nombre;?>">
                    <input type="hidden" id="hdd_pro_id" name="hdd_pro_id" class="form-control input-sm mayus" value="<?php echo $proveedor_id;?>">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Documento</label>
                        <select name="cmb_doc_id" id="cmb_doc_id" class="form-control input-sm">

                        </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">N° Doc.</label>
                        <input type="text" id="txt_egr_numdoc" name="txt_egr_numdoc" class="form-control input-sm mayus" value="<?php echo $egreso_numdoc;?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">IMPORTE</label>
                    <input type="text" id="txt_egr_imp" name="txt_egr_imp" class="form-control input-sm mayus moneda" value="<?php echo $egreso_imp;?>" required="">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">DETALLE</label>
                    <textarea name="txt_egr_det" id="txt_egr_det" class="form-control input-sm" required="" ><?php echo $egreso_det;?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input name="chk_imprimir" type="checkbox" id="chk_imprimir" value="1"> 
                    <label for="">Imprimir Documento</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    Responsable : <?php echo $_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape']?>
                </div>
            </div>
        </div>

        <div class="row" id="div_rendicion">
            <div class="col-md-12">
                <div class="form-group">
                    <input name="chk_rendicion" type="checkbox" id="chk_rendicion" value="1" <?php echo $checked;?>> 
                    <label for=""> ¿GENERA RENDICION?</label>
                </div>
            </div>
        </div>
    </div>
</div>

