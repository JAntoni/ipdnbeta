<?php
require_once('../../core/usuario_sesion.php');
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$fecha_hoy = date('d-m-Y');
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_egreso_adi" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">ADICIONALES Y SEGUROS</h4>
            </div>
            <form id="for_egre_seg">
                <input type="hidden" id="action" name="action" value="<?php echo $_POST['action'] ?>">
                <input type="hidden" name="hdd_seg_cre_id" id="hdd_seg_cre_id">
                <input type="hidden" name="hdd_seg_cre_tip" id="hdd_seg_cre_tip">
                <input type="hidden" name="hdd_seg_cli_id" id="hdd_seg_cli_id">
                <input type="hidden" name="hdd_seg_cre_pre" id="hdd_seg_cre_pre">
                <input type="hidden" name="hdd_tipcam" id="hdd_tipcam">
                <input type="hidden" name="hdd_seguro" id="hdd_seguro">
                <input type="hidden" name="hdd_seg_tipseg" id="hdd_seg_tipseg">

                <input type="hidden" id="hdd_pre_cli">
                <input type="hidden" id="hdd_pre_ipdn">
                <input type="hidden" id="hdd_moneda_adicional_id" name="hdd_moneda_adicional_id">



                <div class="modal-body">

                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 col-form-label">Motivo retención :</label>
                                        <div class="col-sm-5">
                                            <select id="cmb_credes_mot" name="cmb_credes_mot" class="form-control input-sm">
                                                <option value="0">Seleccione motivo...</option>
                                                <option value="1">Pagos de Trámites</option>
                                                <option value="2">Pagos de Seguros</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label  for="" class="col-sm-3 col-form-label">Tipo Seguro o Trámite:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="txt_seg_tipseg" id="txt_seg_tipseg" class="form-control input-sm">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 col-form-label">Fecha:</label>
                                        <div class="col-sm-3">
                                            <div class='input-group date' id='egreso_fil_picker11'>
                                                <input type="text" name="txt_seg_fec" id="txt_seg_fec" class="form-control input-sm" readonly value="<?php echo $fecha_hoy; ?>">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <select name="cmb_seg_mon" id="cmb_seg_mon" class="form-control input-sm">
                                                <?php include '../moneda/moneda_select.php'; ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="txt_seg_tipcam" id="txt_seg_tipcam" class="form-control input-sm" readonly value="1.00" style="font-weight: bold;font-size: 17px;text-align: center">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 col-form-label">Cliente:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="txt_seg_cli" id="txt_seg_cli" class="form-control input-sm">
                                        </div>
                                    </div>
<!--                                    <label>Cliente</label>
                                    <input type="text" name="txt_seg_cli" id="txt_seg_cli" class="form-control input-sm">-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 col-form-label">Finalidad:</label>
                                        <div class="col-sm-9">
                                            <textarea name="txt_seg_fina" rows="2" cols="65" placeholder="Ingrese un detalle del egreso"  class="form-control input-sm"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <label style="font-weight: bold;font-family: cambria;color: #0000cc">TABLA DE CRÉDITOS</label>
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="div_creditos_tabla">Busque un cliente para listar sus creditos</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Precio Cliente:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="txt_seg_pre_cli" class="form-control input-sm moneda" id="txt_seg_pre_cli" style="font-size: 15px;font-weight: bold">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Utilidad:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="txt_seg_uti" class="form-control input-sm moneda" id="txt_seg_uti" style="font-size: 15px;font-weight: bold"  readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Precio IPDN:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="txt_seg_pre_ipdn" class="form-control input-sm moneda" id="txt_seg_pre_ipdn" style="font-size: 15px;font-weight: bold">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <label id="lbl_msj_mon" style="color: red;"></label>
                    <input type="hidden" name="hdd_mon_dis" id="hdd_mon_dis">
                    <div id="msj_mon_dis" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($action == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Egreso Adicional?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="egreso_adi_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_egreso_adi">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/egreso/egreso_adi_seguros_form.js?ver=01120245512151210520'; ?>"></script>
