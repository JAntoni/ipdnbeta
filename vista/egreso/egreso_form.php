<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../egreso/Egreso.class.php');
  $oegreso = new egreso();
  require_once('../rendicioncuentas/RendicionCuentas.class.php');
  $oRendicionCuentas = new RendicionCuentas();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'egreso';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $egreso_id = $_POST['egreso_id'];
  $credito_id = intval($_POST['credito_id']);
  $vista = $_POST['vista'];
  $egreso_imp="0.00";
  $egreso_fec=date('d-m-Y');
  $tipocuenta=2;//tipo de documento de egresos

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Egreso Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Egreso';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Egreso';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Egreso';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en egreso
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'egreso'; $modulo_id = $egreso_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del egreso por su ID
    if(intval($egreso_id) > 0){
      $result = $oegreso->mostrarUno($egreso_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el egreso seleccionado, inténtelo nuevamente.';
          $bandera = 4;
          
        }
        else{
          $egreso_tip = $result['data']['tb_egreso_tip'];
          
            $egreso_fec   = mostrar_fecha($result['data']['tb_egreso_fecreg']);
            $documento_id =0;
//            $documento_id =$result['data']['tb_documento_id'];
            $egreso_numdoc=$result['data']['tb_egreso_numdoc'];
            $egreso_det   =$result['data']['tb_egreso_det'];
            $egreso_imp   =$result['data']['tb_egreso_imp'];
            $egreso_est   =$result['data']['tb_egreso_est'];
            $cuenta_id    =$result['data']['tb_cuenta_id'];
            $tipocuenta    =$result['data']['tb_cuenta_tip'];
            $subcuenta_id =$result['data']['tb_subcuenta_id'];
            $proveedor_id =$result['data']['tb_proveedor_id'];
            $proveedor_nombre =$result['data']['tb_proveedor_nom'];
            $proveedor_doc =$result['data']['tb_proveedor_doc'];
            $caja_id      =$result['data']['tb_caja_id'];
            $moneda_id    =$result['data']['tb_moneda_id'];
        }
      $result = NULL;

      $checked = '';
      $rendicion = $oRendicionCuentas->mostrarxEgreso(intval($egreso_id));
      if($rendicion['estado']==1 && intval($rendicion['data']['tb_rendicioncuenta_id'])>0){
        $checked = 'checked';
      }
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
  
  
?>


<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_egreso" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_egreso" method="post">
          <input type="hidden" id="egreso_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_egreso_id" id="hdd_egreso_id" value="<?php echo $egreso_id;?>">
          <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id;?>">

          <div class="modal-body">

              
              <?php include 'egreso_form_vista.php';?>
              
              
            
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Egreso?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="egreso_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_egreso">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_egreso">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_egreso">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<div id="div_modal_egresofile_form"></div>
<script type="text/javascript" src="<?php echo 'vista/egreso/egreso_form.js?ver=111122233';?>"></script>
