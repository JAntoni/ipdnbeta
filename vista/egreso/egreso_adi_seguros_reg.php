<?php

require_once('../../core/usuario_sesion.php');

require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../vehiculoadicional/Vehiculoadicional.class.php');
$oVehiculoadicional = new Vehiculoadicional();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if ($_POST['action'] == 'caja_seguros') {

    $cre_tip = $_POST['cre_tip'];
    $cre_id = $_POST['cre_id'];
    $mon_id = $_POST['mon_id'];

    $moneda = 'S/.';
    if ($mon_id == 2)
        $moneda = 'US$';

    //INGRESO TOTAL EN CAJA RETENCION, MONTOS RETENIDOS
    $mod_id = 52; // 52 es ingresos por seguros de Asiveh
    if ($cre_tip == 3)
        $mod_id = 53; // 53 es ingresos por seguros de Garveh
    if ($cre_tip == 4)
        $mod_id = 54; // 54 es ingresos por tramites de HIPO

    $modide = $_POST['cre_id'];
    $est = 1; // estado del ingreso, 1 vigente
    $caj_id = 15; //ID DE LA CAJA RETENCION ES 15
    $ingreso_mot = intval($_POST['retencion_mot']);

    $mon_rete = 0;
    $dts = $oIngreso->mostrar_caja_retencion_motivo($mod_id, $modide, $mon_id, $caj_id, $ingreso_mot);

    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $mon_rete += floatval($dt['tb_ingreso_imp']);
        }
    }
    $dts = NULL;

    //EGRESOS DE CAJA RETENCION PARA PAGOS, el modulo_id de egreso de caja seguros es 101, en el modide se guarda el id del credito y para diferenciar de asiveh con garveh es mediante la subcuenta que en EGRESO para Asiveh es 57 y Garveh es 58
    $mod_id = 101; //para egresos de caja seguros el modulo_id será 101
    $modide = $_POST['cre_id'];
    $est = 1;
    $caj_id = 15; // caja seguros
    $subcue_id = 57; // es la subcuenta para asiveh
    if ($cre_tip == 3)
        $subcue_id = 58; // es la subcuenta para garveh
    if ($cre_tip == 4)
        $subcue_id = 59; // es la subcuenta para hipotecario
    $egreso_mot = intval($_POST['retencion_mot']);

    $mon_egre = 0;
    $dts = $oEgreso->mostrar_egreso_caja_retencion_motivo($mod_id, $modide, $mon_id, $caj_id, $subcue_id, $egreso_mot);

    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $mon_egre += floatval($dt['tb_egreso_imp']);
        }
    }
    $dts = NULL;

    $mon_disp = $mon_rete - $mon_egre;
    $data['est'] = 1;
    $data['monto'] = $mon_disp;
    $data['des'] = $moneda . ' ' . formato_moneda($mon_disp);

    echo json_encode($data);
    exit();
    //echo 'tipo cre: '.$cre_tip.' // id cred: '.$cre_id.' // moneda: '.$mon_id.' // subcuenta: '.$subcue_id; exit();
}

if ($_POST['action'] == "insertar" && !empty($_POST['hdd_seg_cre_id'])) {

    $hoy = date('d-m-Y');
    $fec_pago = $_POST['txt_seg_fec'];

    if ($hoy != $fec_pago) {
        $data['estado'] = 0;
        $data['egr_msj'] = 'No se puede hacer desembolsos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $hoy . ', fecha a pagar: ' . $fec_pago;
        echo json_encode($data);
        exit();
    }

    $cre_id = $_POST['hdd_seg_cre_id'];
    $tipo = $_POST['hdd_seg_cre_tip'];
    $monto_total = $_POST['hdd_seg_cre_pre'];
    $mon_id = intval($_POST['cmb_seg_mon']);
    $moneda_seguros_id = intval($_POST['hdd_moneda_adicional_id']);

    $moneda = 'S/.';
    $moneda_selec_nombre = 'SOLES';
    $moneda_adi_nombre = 'SOLES';
    if ($mon_id == 2) {
        $moneda = 'US$';
        $moneda_selec_nombre = 'DÓLARES';
    }
    if ($moneda_seguros_id == 2) {
        $moneda_adi_nombre = 'DÓLARES';
    }

    if ($mon_id != $moneda_seguros_id) {
        $data['estado'] = 0;
        $data['egr_msj'] = 'La moneda del seguro o adicional está en ' . $moneda_adi_nombre . ', y está intentando pagar con ' . $moneda_selec_nombre . '. Las monedas deben ser iguales por favor.';
        echo json_encode($data);
        exit();
    }

    $mod_id = 101; // el modulo para egreso de CAJA RETENCION será 101
    $subcue_id = 57; // la subcuenta para Asiveh es 57 en tb_subcuentas
    $codigo = 'CAV-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);

    if ($tipo == 3) {
        $subcue_id = 58; // la subcuenta para Garveh es 58 en tb_subcuentas
        $codigo = 'CGV-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    }
    if ($tipo == 4) {
        $subcue_id = 59; // la subcuenta para Hipo es 59 en tb_subcuentas
        $codigo = 'CH-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    }

    //echo $cre_id.' // '.$tipo.' // '.$monto_total.' // '.$mon_id.' // '.$_POST['txt_seg_pre_ipdn'].' // '.$_POST['hdd_tipcam'].' // '.$_POST['txt_seg_fina'].' // mod: '.$mod_id.' // Subc: '.$subcue_id.' // Subc: '.$codigo.' / '.$_POST['txt_seg_fec']; exit();

    if ($tipo == 2) {
        require_once('../creditoasiveh/Creditoasiveh.class.php');
        $oCredito = new Creditoasiveh();
        $creditotipo_id = 2;
    }
    if ($tipo == 3) {
        require_once('../creditogarveh/Creditogarveh.class.php');
        $oCredito = new Creditogarveh();
        $creditotipo_id = 3;
    }
    if ($tipo == 4) {
        require_once('../creditohipo/Creditohipo.class.php');
        $oCredito = new Creditohipo();
        $creditotipo_id = 4;
    }

    $dts = $oCredito->mostrarUno($cre_id);
    if ($dts['estado'] == 1) {
        $cli_id = $dts['data']['tb_cliente_id'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom = $dts['data']['tb_cliente_nom'];
        $cli_ape = $dts['data']['tb_cliente_ape'];
        $cli_dir = $dts['data']['tb_cliente_dir'];
        $cli_ema = $dts['data']['tb_cliente_ema'];
        $cli_fecnac = $dts['data']['tb_cliente_fecnac'];
        $cli_tel = $dts['data']['tb_cliente_tel'];
        $cli_cel = $dts['data']['tb_cliente_cel'];
        $cli_telref = $dts['data']['tb_cliente_telref'];
        $cre_mon_id = $dts['data']['tb_moneda_id'];
    }

    //EGRESO
    $xac = 1;
    $doc_id = 9;
    $egr_det = 'El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b>  ha realizado un Egreso por <b style="color: #0000cc">'
                .trim('ADICIONALES Y SEGUROS:'.$codigo.' CLIENTE : '.$cli_nom.' '.$cli_ape).'</b> con fecha y hora  <b style="color: #0000cc">'
                .date('d-m-Y h:i:s').'</b> por un Monto de '.$moneda.' <b style="color: #0000cc">'. mostrar_moneda($_POST['txt_seg_pre_cli']).'</b> con el siguiente  Detalle: '.$_POST['txt_seg_fina'];
    $numdoc = $mod_id . '-' . $cre_id;
    $est = 1;
    $cue_id = 19; //creditos
    $pro_id = 1; //proveedor
    $caj_id = 15; //CAJA SEGUROS 



    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = fecha_mysql($_POST['txt_seg_fec']);
    $oEgreso->documento_id = $doc_id;
    $oEgreso->egreso_numdoc = $numdoc;
    $oEgreso->egreso_det = $egr_det;
    $oEgreso->egreso_imp = moneda_mysql($_POST['txt_seg_pre_cli']);
    $oEgreso->egreso_tipcam = $_POST['hdd_tipcam'];
    $oEgreso->egreso_est = $est;
    $oEgreso->cuenta_id = $cue_id;
    $oEgreso->subcuenta_id = $subcue_id;
    $oEgreso->proveedor_id = $pro_id;
    $oEgreso->cliente_id = 0;
    $oEgreso->usuario_id = 0;
    $oEgreso->caja_id = $caj_id;
    $oEgreso->moneda_id = $mon_id;
    $oEgreso->modulo_id = $mod_id;
    $oEgreso->egreso_modide = $cre_id;
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0; //si
    $result = $oEgreso->insertar();

    if (intval($result['estado']) == 1) {
        $egr_id = $result['egreso_id'];
    }
    $result = NULL;
    //guardamos el motivo del egreso para poder hacer el calculo de egreso por motivos
    $egreso_mot = intval($_POST['cmb_credes_mot']);
//    $oEgreso->modificar_campo($egr_id, $_SESSION['usuario_id'], 'mot', $egreso_mot);
    
    $oEgreso->modificar_campo($egr_id, 'tb_egreso_usumod',$_SESSION['usuario_id'], 'INT');
    $oEgreso->modificar_campo($egr_id, 'tb_egreso_mot',$egreso_mot, 'INT');
    $oEgreso->modificar_campo($egr_id, 'tb_egreso_fecmod','NOW( )', 'STR');

    $numdoc = $numdoc . '-' . $egr_id;
//    $oEgreso->modificar_campo($egr_id, $_SESSION['usuario_id'], 'numdoc', $numdoc);
   
    $oEgreso->modificar_campo($egr_id, 'tb_egreso_usumod',$_SESSION['usuario_id'], 'INT');
    $oEgreso->modificar_campo($egr_id, 'tb_egreso_numdoc',$egreso_mot, 'INT');
     $oEgreso->modificar_campo($egr_id, 'tb_egreso_fecmod','NOW( )', 'STR');

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho un Egreso de Caja Seguros (' . $codigo . '): ' . $egr_det . '. El Egreso tiene como N°: ' . $numdoc . '. El monto válido fue: ' . $moneda . ' ' . $_POST['txt_seg_pre_cli'] . ' && <b>' . date('d-m-Y h:i a') . '</b><br>';
    $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det = $line_det;
    $oLineaTiempo->insertar();

    $his = str_replace('&&', '|', $line_det);
    //$oCredito->registro_historial($cre_id, str_replace('&&', '|', $line_det));
    $oCreditolinea->insertar($creditotipo_id, $cre_id, $_SESSION['usuario_id'], $his);

    if ($_POST['txt_seg_uti'] > 0) {
        $mod_id = 52; // 52 es ingresos por seguros de Asiveh
        if ($tipo == 3)
            $mod_id = 53; // 53 es ingresos por seguros de Garveh
        if ($tipo == 4)
            $mod_id = 54; // 53 es ingresos por tramires de HIPO



            
//obtenemos información del tipo de seguro por el cual se está obteniendo una utilidad+
        $dts = $oVehiculoadicional->mostrarUno($_POST['hdd_seg_tipseg']);

        if ($dts['estado'] == 1) {
            $tipo_seguro = $dts['data']['tb_proveedortipo_id']; //tipo de adicional: 1 SOAT, 2 STR, 3 GPS, 4 GAS
        }
        $dts = NULL;

        $subcue_id = 0; //vamos elegir a sub cuenta de UTILIDAD dependiendo del tipo de seguro o adicional
        if ($tipo_seguro == 1)
            $subcue_id = 114; // subcuenta de SOAT
        if ($tipo_seguro == 2)
            $subcue_id = 115; // subcuenta de STR
        if ($tipo_seguro == 3)
            $subcue_id = 113; // subcuenta de GPS
        if ($tipo_seguro == 4)
            $subcue_id = 116; // subcuenta de GAS
        if ($tipo_seguro == 5)
            $subcue_id = 122; // subcuenta de TRAMITES

        $xac = 1;
        $doc_id = 8; ///8 es otros ingresos
        $numdoc = $mod_id . '-' . $cre_id . '-' . $_POST['hdd_seg_tipseg'];
        $est = 1;
        $caja_id = 15;
        $cue_id = 30; //id de la cuenta de UTILIDAD es 30 encontrada en la tabla tb_cuenta

        $det = 'El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b>  ha realizado un Egreso por <b style="color: #0000cc">'
                .trim('ADICIONALES Y SEGUROS:'.$codigo.' CLIENTE : '.$cli_nom.' '.$cli_ape).'</b> con fecha y hora  <b style="color: #0000cc">'
                .date('d-m-Y h:i:s').'</b> por un Monto de '.$moneda.' <b style="color: #0000cc">'. mostrar_moneda($_POST['txt_seg_pre_cli']).'</b> lo cual ha generado una <b style="color: #0000cc">UTILIDAD POR PAGO DE ' . $_POST['hdd_seguro'] . '</b>  DEL CREDITO: <b style="color: #0000cc">' . $codigo.'</b> ';

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = fecha_mysql($_POST['txt_seg_fec']);
        $oIngreso->documento_id = $doc_id;
        $oIngreso->ingreso_numdoc = $numdoc;
        $oIngreso->ingreso_det = $det;
        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_seg_uti']);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = $cli_id;
        $oIngreso->caja_id = $caja_id;
        $oIngreso->moneda_id = $mon_id;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $cre_id;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];
        $oIngreso->ingreso_fecdep = '';
        $oIngreso->ingreso_numope = '';
        $oIngreso->ingreso_mondep = 0;
        $oIngreso->ingreso_comi = 0;
        $oIngreso->cuentadeposito_id = 0;
        $oIngreso->banco_id = 0;
        $oIngreso->ingreso_ap = 0;
        $oIngreso->ingreso_detex = '';
        $result = $oIngreso->insertar();

        if (intval($result['estado']) == 1) {
            $ing_id = $result['ingreso_id'];
        }
        $result = NULL;


        $numdoc = $ing_id . '-' . $mod_id . '-' . $cre_id . '-' . $_POST['hdd_seg_tipseg'];
        //tb_ingreso_fecmod
        //tb_ingreso_usumod
        //numdoc
        //tb_ingreso_mod
//        $oIngreso->modificar_campo($ing_id, $_SESSION['usuario_id'], 'numdoc', $ing_id);
        $oIngreso->modificar_campo($ing_id,'tb_ingreso_usumod',  $_SESSION['usuario_id'], 'INT');
        $oIngreso->modificar_campo($ing_id,'tb_ingreso_numdoc',$ing_id, 'STR');
        //$oIngreso->modificar_campo($ing_id,'tb_ingreso_mot',intval($_POST['cmb_credes_mot']), 'STR');
        $oIngreso->modificar_campo($ing_id,'tb_ingreso_fecmod', 'NOW ()', 'STR');
    }
    $data['estado'] = 1;
    $data['egr_msj'] = 'Se registró desembolso para Adicionales y Seguros.';
    $data['egr_id'] = $egr_id;
} else {
    $data['estado'] = 0;
    $data['egr_msj'] = 'Intentelo nuevamente.';
}
echo json_encode($data);
?>