var datatable_global;
$(document).ready(function () {
    console.log('Modulo egresooss  111111')
    egreso_tabla();
    Documento_select();
    Cuenta_select();
    var empresa_id = $('#cmb_fil_empresa_id').val();

    var usuariogrupo = $('#usuariogrupo_id').val();

    if (usuariogrupo == 2 || usuariogrupo == 6) {
        usuario_select(empresa_id);
    }
    $("#txt_fil_pro").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "proveedor/proveedor_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            console.log(ui);
            $('#hdd_fil_pro_id').val(ui.item.tb_proveedor_id);
            $('#txt_fil_pro').val(ui.item.tb_proveedor_nom);
            egreso_tabla();
            event.preventDefault();
            $('#txt_fil_pro').focus();
        }
    });



    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_egr_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_egr_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });






});



function egreso_form(usuario_act, egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            egreso_id: egreso_id,
            vista: 'egreso'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_egreso_form').html(data);
                $('#modal_registro_egreso').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_egreso'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_egreso'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_egreso', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'egreso';
                var div = 'div_modal_egreso_form';
                permiso_solicitud(usuario_act, egreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}





function egreso_seguros(act) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_adi_seguros_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_egreso_seguros').html(html);
            $('#modal_registro_egreso_adi').modal('show');
            modal_height_auto('modal_registro_egreso_adi');
        }
    });
}



function egreso_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_tabla.php",
        async: true,
        dataType: "html",
        data:
//            ({
                $('#egreso_filtro').serialize(),
//    }),
        beforeSend: function () {
            $('#egreso_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_egreso_tabla').html(data);
            $('#egreso_mensaje_tbl').hide(300);

            let registros = data.includes("No hay");
            console.log(registros);
            if (!registros) estilos_datatable();

        },
        complete: function (data) {

        },
        error: function (data) {
            $('#egreso_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tbl_egresos').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [1, 2, 4, 5, 6], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'egreso', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
//                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function upload_galeria(egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'egreso', //nombre de la tabla a relacionar
            modulo_id: egreso_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function Abrir_imagen(egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_egreso_img').modal('show');
            modal_hidden_bs_modal('modal_egreso_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(egreso_id);
        },
        complete: function (html) {
//            console.log(html);
        }
    });
}

function Documento_select() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "documento/documento_select.php",
        async: true,
        dataType: "html",
        data: ({
            documento_tipo: 7,
            vista_documento: 'documento'
        }),
        beforeSend: function () {
            $('#cmb_fil_doc_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_fil_doc_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function Cuenta_select() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuenta/cuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            tipocuenta: 2
        }),
        beforeSend: function () {
            $('#cmb_fil_cue_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_fil_cue_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}




function SubCuenta_select(cuenta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "subcuenta/subcuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            cuenta_id: cuenta_id
//			vista_subcategoria: 'producto'
        }),
        beforeSend: function () {
            $('#cmb_fil_subcue_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_fil_subcue_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$('#cmb_fil_cue_id').change(function () {
    SubCuenta_select($('#cmb_fil_cue_id').val());
    egreso_tabla();
});

$('#cmb_fil_caj_id').change(function () {
    egreso_tabla();
});
$('#cmb_fil_egr_est').change(function () {
    egreso_tabla();
});
$('#cmb_fil_doc_id').change(function () {
    egreso_tabla();
});
$('#cmb_fil_subcue_id').change(function () {
    egreso_tabla();
});
$('#cmb_moneda_id').change(function () {
    egreso_tabla();
});
$('#cmb_fil_empresa_id').change(function () {
    var empresa_id = $(this).val();
    usuario_select(empresa_id);
    egreso_tabla();
});

function reestablecerValores() {

    let date = new Date()

    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()

    if (month < 10) {
        $("#txt_fil_egr_fec1").val(day + "-0" + month + "-" + year);
        $("#txt_fil_egr_fec2").val(day + "-0" + month + "-" + year);
    } else {
        $("#txt_fil_egr_fec1").val(day + "-" + month + "-" + year);
        $("#txt_fil_egr_fec2").val(day + "-" + month + "-" + year);
    }

    $("#cmb_fil_cue_id").val(0);
    $("#cmb_fil_caj_id").val(0);
    $("#cmb_fil_egr_est").val("");
    $("#cmb_fil_doc_id").val(0);
    $("#cmb_fil_subcue_id").val(0);
    $("#cmb_moneda_id").val(0);
    $("#hdd_fil_pro_id").val("");
    $("#txt_fil_pro").val("");
    $("#txt_fil_egr_numdoc").val("");
    egreso_tabla();
}

$("#txt_fil_egr_fec1,#txt_fil_egr_fec2,#cmb_fil_usuario_id").change(function () {
    egreso_tabla();
});

function egreso_imppos_datos3(id_egr, empresa_id)
{
    var codigo = id_egr;
    window.open("http://ipdnsac.com/ipdnsac/vista/egreso/egreso_ticket.php?egreso_id=" + codigo + "&empresa_id=" + empresa_id);
//  window.open("http://localhost/ipdnsac/vista/egreso/egreso_ticket.php?egreso_id="+codigo+"&empresa_id="+empresa_id);
}

function egreso_imppos_datos(id_egr)
{
    if (confirm("Desea imprimir?"))
    {
        $.ajax({
            type: "POST",
            url: VISTA_URL + "egreso/egreso_imppos_datos.php",
            async: true,
            dataType: "json",
            data: ({egr_id: id_egr}),
            beforeSend: function () {
                //$('#msj_egreso_imp').html("Imprimiendo.");
                //$('#msj_egreso_imp').show(100);
            },
            success: function (data) {
                // $.each(data, function(i, item) {
                //     console.log(item);
                // });
//                            console.log(data);
                egreso_imppos_ticket(data);
            },
            complete: function (data) {
                console.log(data);
            },
            complete: function (data) {
                console.log(data);
            }
        });
    }
}


function egreso_imppos_ticket(datos) {
//    console.log(datos);
//    return 0;
    $.ajax({
        type: "POST",
        url: "http://127.0.0.1/prestamosdelnorte/app/modulos/egreso/egreso_imppos_ticket.php",
        async: true,
        dataType: "html",
        data: datos,
        beforeSend: function () {
            //$('#msj_egreso_imp').html("Imprimiendo...");
            //$('#msj_egreso_imp').show(100);
        },
        success: function (html) {
            $('#msj_egreso_imp').html(html);
        }
    });
}


function usuario_select(empresa_id)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/usuario_select.php",
        async: false,
        dataType: "html",
        data: {
            usuario_columna: 'tb_usuario_ofi',
            usuario_valor: 1, //para que solo muestre usuarios de oficina o que trabajaron en oficina
            param_tip: 'INT',
            empresa_id: empresa_id
        },
        beforeSend: function () {
          $("#cmb_fil_usuario_id").html('<option value="">Cargando...</option>');
        },
        success: function (html) {
          $("#cmb_fil_usuario_id").html(html);
          $("#cmb_fil_usuario_id").selectpicker('refresh');
        },
        complete: function (html) {},
    });
}

/* GERSON (14-01-25) */
function solicitar_anular(egreso_id, credito_id, creditotipo_id){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"permisoanulacion/solicitar_anulacion_form.php",
      async: true,
      dataType: "html",
      data: ({
        modid: egreso_id,
        origen: 4, // 1=cuota(cmenor), 2=cuotadetalle(cgarveh), 3=ingreso, 4=egreso
        credito_id: credito_id,
        creditotipo_id: creditotipo_id
      }),
      beforeSend: function() {
        $('#h3_modal_title').text('Obteniendo datos...');
        $('#modal_mensaje').modal('show');
      },
      success: function(data){
        $('#div_modal_solicitar_anulacion').html(data);
        $('#modal_solicitar_anulacion').modal('show');
  
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_solicitar_anulacion'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_solicitar_anulacion', 'limpiar'); //funcion encontrada en public/js/generales.js
      },
      complete: function(data){
        $('#modal_mensaje').modal('hide');
      },
      error: function(data){
        alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
        console.log(data.responseText);
      }
    });
  }
  /*  */