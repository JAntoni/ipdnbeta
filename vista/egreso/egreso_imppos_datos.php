<?php
require_once('../../core/usuario_sesion.php');

require_once ("../empresa/Empresa.class.php");
$oEmpresa = new Empresa();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();

require_once ("../impresora/Impresora.class.php");
$oImpresora = new Impresora();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
//require_once("../formatos/numletras.php");


$result = $oEmpresa->mostrarUno($_SESSION['empresa_id']);
        if($result['estado'] == 1){
                $emp_ruc    =$result['data']['tb_empresa_ruc'];
                $emp_nomcom =$result['data']['tb_empresa_nomcom'];
                $emp_razsoc =$result['data']['tb_empresa_razsoc'];
                $emp_dir    =$result['data']['tb_empresa_dir'];
                $emp_dir2   =$result['data']['tb_empresa_dir2'];
                $emp_tel    =$result['data']['tb_empresa_tel'];
                $emp_ema    =$result['data']['tb_empresa_ema'];
                $emp_fir    =$result['data']['tb_empresa_fir'];		
        }
$result = NULL;

//echo "Empresa=".$emp_ruc;
//exit();


$result = $oEgreso->mostrarUno($_POST['egr_id']);
    if($result['estado'] == 1){
        $fecreg		= mostrar_fecha_hora($result['data']['tb_egreso_fecreg']);
        $fecmod		= mostrar_fecha_hora($result['data']['tb_egreso_fecmod']);
        $usureg		=$result['data']['tb_egreso_usureg'];
        $usumod		=$result['data']['tb_egreso_usumod'];
	
	$fec		= mostrar_fecha($result['data']['tb_egreso_fec']);
	$doc_id 	=$result['data']['tb_documento_id'];
	$numdoc		=$result['data']['tb_egreso_numdoc'];
	
	$det		=$result['data']['tb_egreso_det'];
	
	$cue_id		=$result['data']['tb_cuenta_id'];
	$subcue_id	=$result['data']['tb_subcuenta_id'];

	$cli_id		=$result['data']['tb_proveedor_id'];
	$cli_nom 	=$result['data']['tb_proveedor_nom'];
	$cli_doc 	=$result['data']['tb_proveedor_doc'];
	$cli_dir 	=$result['data']['tb_proveedor_dir'];
	$cli_tip 	=$result['data']['tb_proveedor_tip'];
	
	$imp		=$result['data']['tb_egreso_imp'];
	
	$caj_id		=$result['data']['tb_caja_id'];

	$ven_id		=$result['data']['tb_venta_id'];

	$mon_id		=$result['data']['tb_moneda_id'];
	$mon_nom	=$result['data']['tb_moneda_nom'];
	
	$est		=$result['data']['tb_egreso_est'];		
    }
$result = NULL;

//impresora
$imp_id=1;
if($imp_id>0)
{
$result = $oImpresora->mostrarUno($imp_id);
        if($result['estado'] == 1){
            $imp_nom 	=$result['data']['tb_impresora_nom'];
            $imp_nomloc =$result['data']['tb_impresora_nomloc'];
            $imp_ser 	=$result['data']['tb_impresora_ser'];
            $imp_url 	=$result['data']['tb_impresora_url'];
            $imp_ip 	=$result['data']['tb_impresora_ip'];	
        }
$result = NULL;
}

//cajero
$result = $oUsuario->mostrarUno($usureg);
        if($result['estado'] == 1){
            $usu_nom	=$result['data']['tb_usuario_nom'];
            $usu_apepat	=$result['data']['tb_usuario_apepat'];
            $usu_apemat	=$result['data']['tb_usuario_apemat'];	
        }
$result = NULL;


//$texto_cajero=substr($usu_nom, 0, 3).substr($usu_apepat, 0, 1).substr($usu_apemat, 0, 1);
$cajero=$usu_nom.' '.$usu_apepat.' '.$usu_apemat;

//------------impresion
$data['imp_nom'] 	=$imp_nom;
$data['imp_nomloc']     =$imp_nomloc;
$data['imp_ser'] 	=$imp_ser;
$data['imp_url'] 	=$imp_url;
$data['imp_ip']	 	=$imp_ip;

$data['emp_razsoc']     =$emp_razsoc;
$data['emp_ruc']	=$emp_ruc;
$data['emp_dir1']	=$emp_dir;
$data['emp_dir2']	=$emp_dir2;

$data['fecha'] 		= $fec;
$data['fechareg'] 	= $fecreg;

$data['operacion'] 	= $numdoc;

$data['cliente'] 	= $cli_nom;
$data['cliente_doc']	= $cli_doc;
$data['cliente_dir']	= $cli_dir;

$data['detalle'] 	= $det;

$data['monto'] 		= $imp;
$data['moneda'] 	= $mon_nom;
$data['total_letras']   =numtoletras($imp,$tb_mon_id);

$data['cajero']		=$cajero;



echo json_encode($data);

function sanear_string($string)
{
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
  
    return $string;
}

?>