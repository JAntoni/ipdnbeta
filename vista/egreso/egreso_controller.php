<?php
  require_once('../../core/usuario_sesion.php');

  require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();
  require_once('../egreso/Egreso.class.php');
  $oEgreso = new Egreso();
  require_once('../ingreso/Ingreso.class.php');
  $oIngreso = new Ingreso();
  require_once('../creditomenor/Creditomenor.class.php');
  $oCredito = new Creditomenor();
  require_once('../creditolinea/Creditolinea.class.php');
  $oCreditolinea = new Creditolinea();
  require_once ('../cuotaproveedor/Cuotaproveedordetalle.class.php');
  $oCuotaproveedordetalle = new Cuotaproveedordetalle();

  require_once('../rendicioncuentas/RendicionCuentas.class.php');
  $oRendicionCuentas = new RendicionCuentas();

  require_once('../permisoanulacion/Permisoanulacion.class.php');
  $oPermisoanulacion = new Permisoanulacion();

  require_once ('../garantia/Garantia.class.php');
  require_once ('../tareainventario/Tareainventario.class.php');
  
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
  
  //echo 'esto es una prueba '.mb_strtoupper($_POST['txt_egr_fec'], 'UTF-8');  exit();
  
  $action = $_POST['action'];
  $creditotipo_id = 0; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
  $usuario_id = intval($_SESSION['usuario_id']);
  $egreso_id = intval($_POST['hdd_egreso_id']);
  //$oEgreso->egreso_id = mb_strtoupper($_POST['txt_cargo_nom'], 'UTF-8');
  //$oEgreso->egreso_xac  = mb_strtoupper($_POST['txt_cargo_nom'], 'UTF-8');
  $oEgreso->egreso_id = intval($_POST['hdd_egreso_id']);
  $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
  $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
  $oEgreso->egreso_fec = fecha_mysql($_POST['txt_egr_fec']);
  $oEgreso->documento_id = mb_strtoupper($_POST['cmb_doc_id'], 'UTF-8');
  $oEgreso->egreso_numdoc = mb_strtoupper($_POST['txt_egr_numdoc'], 'UTF-8');
  $oEgreso->egreso_det = mb_strtoupper($_POST['txt_egr_det'], 'UTF-8');
  $oEgreso->egreso_imp = moneda_mysql($_POST['txt_egr_imp']);
  $oEgreso->egreso_tipcam = 0;
  $oEgreso->egreso_est = mb_strtoupper($_POST['cmb_egr_est'], 'UTF-8');
  $oEgreso->cuenta_id = mb_strtoupper($_POST['cmb_cue_id'], 'UTF-8');
  $oEgreso->subcuenta_id = mb_strtoupper($_POST['cmb_subcue_id'], 'UTF-8');
  
  $oEgreso->proveedor_id = mb_strtoupper($_POST['hdd_pro_id'], 'UTF-8');
  $oEgreso->cliente_id = 0;
  
  $oEgreso->usuario_id = 0;
  $oEgreso->caja_id = mb_strtoupper($_POST['cmb_caj_id'], 'UTF-8');
  $oEgreso->moneda_id = mb_strtoupper($_POST['cmb_mon_id'], 'UTF-8');
  $oEgreso->modulo_id = 0;
  $oEgreso->egreso_modide = 0;
  $oEgreso->empresa_id = $_SESSION['empresa_id'];
  $oEgreso->egreso_ap = 0; //si
    
    
 	if($action == 'insertar'){
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Egreso.';

    $resultado = $oEgreso->insertar();
    //$resultado = 1;
    if($resultado['estado'] > 0){
      if ($_POST['chk_rendicion']) {
        $oRendicionCuentas->egreso_id = $resultado['egreso_id'];
        $oRendicionCuentas->rendicioncuenta_estado = 1;
        $oRendicionCuentas->rendicioncuenta_fec = fecha_mysql(date('Y-m-d'));
        $oRendicionCuentas->rendicioncuenta_usureg = $_SESSION['usuario_id'];
        $oRendicionCuentas->rendicioncuenta_xac = 1;
        $oRendicionCuentas->insertar();
      }

      $data['estado'] = 1;
      $data['mensaje'] = 'Egreso registrado correctamente.';
      $data['datos'] = $resultado;
    }
    $resultado = 0;
    echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Egreso.';

 		if($oEgreso->modificar()){
      $data['estado'] = 1;
      $data['mensaje'] = 'Egreso modificado correctamente.';
      $rendicion = $oRendicionCuentas->mostrarxEgreso(intval($_POST['hdd_egreso_id']));
      if ($_POST['chk_rendicion']) {
        if($rendicion['estado']==1 && intval($rendicion['data']['tb_rendicioncuenta_id'])>0){
          $data['mensaje'] .= 'Rendicion existente.';
        }else{
          $oRendicionCuentas->egreso_id = intval($_POST['hdd_egreso_id']);
          $oRendicionCuentas->rendicioncuenta_estado = 1;
          $oRendicionCuentas->rendicioncuenta_fec = fecha_mysql(date('Y-m-d'));
          $oRendicionCuentas->rendicioncuenta_usureg = $_SESSION['usuario_id'];
          $oRendicionCuentas->rendicioncuenta_xac = 1;
          $oRendicionCuentas->insertar();

          $data['mensaje'] .= 'Se registró la rendicion.';
        }
      }else{
        if($rendicion['estado']==1 && intval($rendicion['data']['tb_rendicioncuenta_id'])>0){
          $oRendicionCuentas->modificar_campo_nuevo(intval($rendicion['data']['tb_rendicioncuenta_id']), intval($_SESSION['usuario_id']), 'xac', 0);
        }
      }
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
        $data['estado'] = 0;
        $data['mensaje'] = 'Existe un error al eliminar el Egreso.';

        /* daniel odar 17-04-2023 */
        //verificar si el egreso está anexado en un gasto
        require_once('../gastogar/Gastogar.class.php');
        $oGastogar = new Gastogar();

        $array_where[0]['column_name'] = 'gg.tb_egreso_idfk';
        $array_where[0]['param0'] = $egreso_id;
        $array_where[0]['datatype'] = 'INT';

        $array_where[1]['conector'] = 'AND';
        $array_where[1]['column_name'] = 'gg.tb_gastogar_xac';
        $array_where[1]['param1'] = 1;
        $array_where[1]['datatype'] = 'INT';

        $array_join = array();
        $res = $oGastogar->mostrarUno($array_where, $array_join);

        if ($res['estado'] == 1) {
            $data['mensaje'] = 'No se puede eliminar el egreso, debido a que está anexado en un gasto | Gasto ID:' . $res['data']['tb_gastogar_id'] . ', en el CM-' . $res['data']['tb_credito_idfk'];
            echo json_encode($data);
            exit();
        }
        $res = null;
        /*  */

        if ($oEgreso->eliminar()) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Egreso eliminado correctamente.';

            $result = $oCuotaproveedordetalle->mostrarUnoEgreso($egreso_id);
            if ($result['estado'] == 1) {
                $cuotadetalle_id = $result['data']['tb_cuotaproveedor_id'];
            }
            $result = NULL;

            $data['cuotaproveedordetalle'] = $cuotadetalle_id;
            $oCuotaproveedordetalle->eliminarEgreso(intval($_POST['hdd_egreso_id']));

            /* GERSON (14-01-25) */
          $permisoanulacion = $oPermisoanulacion->mostrarPermisoanulacionIE($egreso_id, 4);
          if($permisoanulacion['estado'] == 1){
            $oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_est', 2, 'INT');
            $oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_fec_aten', date('Y-m-d h:i:s'), 'STR');
            $oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_user_aten', $_SESSION['usuario_id'], 'INT');
          }
          /*  */
        }

        echo json_encode($data);
 	}
              
  elseif($action == 'insertar_desembolso'){
    $vista = $_POST['vista']; //creditomenor, creditoasive, creditogarveh, creditohipo
    $credito_id = intval($_POST['hdd_credito_id']);
    $desembolsar_max = formato_moneda($_POST['hdd_desembolsar_max']);
    $credito_mon = formato_moneda($_POST['txt_credito_mon']);
    $moneda_id = intval($_POST['cmb_moneda_id']);
    $sigla = retorna_sigla_credito_nombre($vista);
    $credito_cod = $sigla.'-'.str_pad($credito_id, 4, "0", STR_PAD_LEFT);
    $desembolso_obs = mb_strtoupper($_POST['txt_desembolso_obs'], 'UTF-8');

    $result = $oCredito->mostrarUno($credito_id);
      if($result['estado'] == 1){
        $cliente_id = $result['data']['tb_cliente_id'];
        $cliente_nom = $result['data']['tb_cliente_nom'];
        $usu_reg_cre = $result['data']['tb_credito_usureg'];
        $credito_prestamo = formato_moneda($result['data']['tb_credito_preaco']);
      }
    $result = NULL;
    
    $modulo_id = 0; $subcuenta_id = 0;
    $credito_nombre = 'Sin_nombre';

    if(trim($vista) == 'creditomenor'){
      $modulo_id = 51;
      $subcuenta_id = 60;
      $creditotipo_id = 1;
      $credito_nombre = 'Crédito Menor';
    }
    if(trim($vista) == 'creditoasiveh'){
      $modulo_id = 51;
      $creditotipo_id = 2;
      $credito_nombre = 'Crédito ASIVEH';
    }
    if(trim($vista) == 'creditogarveh'){
      $modulo_id = 51;
      $creditotipo_id = 3;
      $credito_nombre = 'Crédito GARVEH';
    }
    if(trim($vista) == 'creditohipo'){
      $modulo_id = 51;
      $creditotipo_id = 4;
      $credito_nombre = 'Crédito HIPOTECARIO';
    }

    //esta función nos permite saber si ya tiene desembolsos previos, para no dejarle desembolsar más
    $total_egresos = 0;
    $result = $oEgreso->mostrar_por_modulo($modulo_id, $credito_id, $moneda_id, 1); // el 1 es por xac = 1 de egreso
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $total_egresos += formato_moneda($value['tb_egreso_imp']);
        }
      }
    $result = NULL;
    
    $saldo_desembolsar = formato_moneda($credito_prestamo - $total_egresos);
    
    if($credito_mon > $saldo_desembolsar){
      $data['estado'] = 0;
      $data['mensaje'] = 'Este crédito ya ha sido desembolsado. Monto a desembolsar: ('.$credito_mon.') / total desembolsos: '.$total_egresos.' / saldo a desembolsar: '.$saldo_desembolsar;

      echo json_encode($data); exit();
    }

    //? A PARTIR DE AQUÍ SE VALIDA SI EL DESEMBOLSO SE REALIZÓ EN DEPÓSITO PARA REALIZAR LAS OPERACIONES: INGRESO Y EGRESO
    $detalle_deposito = '';
    if(isset($_POST['cmb_formadesembolso_id'])){
      $formadesembolso_id = intval($_POST['cmb_formadesembolso_id']);

      if($formadesembolso_id == 2){
        $numero_ope = $_POST['txt_desembolso_numope'];
        $numero_cuenta = $_POST['txt_desembolso_numcuenta'];
        $desembolso_banco = $_POST['txt_desembolso_banco'];
        $ingreso_cuenta_id = intval($_POST['hdd_cuenta_id']);
        $ingreso_subcuenta_id = intval($_POST['cmb_subcuenta_id']);

        $detalle_deposito = 'DEPÓSITO REALIZADO EN LA CUENTA PROPIA DEL CLIENTE, DEPOSITADO EN: '.$desembolso_banco.', CON N° OPERACIÓN: '.$numero_ope.', N° CUENTA: '.$numero_cuenta;

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = fecha_mysql($_POST['txt_credito_fecdes']);
        $oIngreso->documento_id = 8; // otros ingresos
        $oIngreso->ingreso_numdoc = '';
        $oIngreso->ingreso_det = 'INGRESO BANCARIO POR DESEMBOLSO DEL CÉDITO: '.$credito_cod.', INGRESO CON DETALLE: '.$detalle_deposito;
        $oIngreso->ingreso_imp = $credito_mon;  
        $oIngreso->cuenta_id = $ingreso_cuenta_id; // cuenta
        $oIngreso->subcuenta_id = $ingreso_subcuenta_id; // subcuenta
        $oIngreso->cliente_id = $cliente_id;
        $oIngreso->caja_id = 1; 
        $oIngreso->moneda_id = $moneda_id;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = 0; 
        $oIngreso->ingreso_modide = 0; 
        $oIngreso->empresa_id = $_SESSION['empresa_id'];
        $oIngreso->ingreso_fecdep = ''; 
        $oIngreso->ingreso_numope = ''; 
        $oIngreso->ingreso_mondep = 0; 
        $oIngreso->ingreso_comi = 0; 
        $oIngreso->cuentadeposito_id = 0; 
        $oIngreso->banco_id = 0; 
        $oIngreso->ingreso_ap = 0; 
        $oIngreso->ingreso_detex = '';

        $result = $oIngreso->insertar();
      }
    }

    $egreso_numdoc = $modulo_id.'-'.$credito_id;

    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = fecha_mysql($_POST['txt_credito_fecdes']);
    $oEgreso->documento_id = 9; //OE otros egresos
    $oEgreso->egreso_numdoc = $egreso_numdoc; //modulod el credito - ID DEL CREDITO - ID DE EHGRESO
    $oEgreso->egreso_det = "DESEMBOLSO: " .$credito_cod. ", CLIENTE: ".$cliente_nom.'. '.$detalle_deposito;
    $oEgreso->egreso_imp = $credito_mon;
    $oEgreso->egreso_tipcam = 1;
    $oEgreso->egreso_est =1;
    $oEgreso->cuenta_id = 19; //ID 19 es de la cuenta de CREDITOS
    $oEgreso->subcuenta_id = $subcuenta_id;
    $oEgreso->proveedor_id = 1; //el proveedor ID 1, egreso sin proveedor
    $oEgreso->cliente_id = $cliente_id;
    $oEgreso->usuario_id = 0; //el egreso no es para ningún colaborador
    $oEgreso->caja_id = 1; //la cada ID 1 es la caja general
    $oEgreso->moneda_id = $moneda_id; //1 soles, 2 dolares
    $oEgreso->modulo_id = 51;
    $oEgreso->egreso_modide = $credito_id;
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0; //aquí va el cuotapago de un acuerdo de pago

    $result = $oEgreso->insertar();

    $egreso_id = $result['egreso_id'];
    $egreso_numdoc = $egreso_numdoc.'-'.$egreso_id;

    $oEgreso->modificar_campo($egreso_id, 'tb_egreso_numdoc', $egreso_numdoc, 'STR');
    $oCredito->modificar_campo($credito_id, 'tb_credito_comdes', $egreso_numdoc, 'STR');

    //cambio de estado
    if($credito_mon == $saldo_desembolsar){
      $oCredito->modificar_campo($credito_id, 'tb_credito_est', 3, 'INT'); //credito pasa a vigente
      $oCredito->modificar_campo($credito_id, 'tb_credito_fecdes', fecha_mysql($_POST['txt_credito_fecdes']), 'STR');
    }

    //$his = '<span>Se desembolsó un total de S/. '.formato_money($_POST['txt_credes_mon']).', desembolsado por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCredito->registro_historial($_POST['hdd_cre_id'], $his);
    //$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> hizo un desembolso para el Crédito Menor de número: CM-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).', un total de S/. '.formato_money($_POST['txt_credes_mon']).' && <b>'.date('d-m-Y h:i a').'</b>';
    //$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);
    $moneda_nom = 'S/.';
    if($moneda_id == 2) $moneda_nom = 'US$';

    $creditolinea_det = 'He hecho un desembolso del '.$credito_nombre.' número: '.$credito_id.'. Un desembolso de '.$moneda_nom.' '.$_POST['txt_credito_mon'].'. '.$detalle_deposito.'. '.$desembolso_obs;
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);
    
    //si el desembolso fue por un credito menor, es que el cliente llevó el dinero, y la garantía pasa a ser almacenada
    if($creditotipo_id == 1){
      $oGarantia = new Garantia();
      $oGarantia->modificar_campo2($credito_id, 'tb_garantia_almest', 1, 'INT');
      $data['mensaje'] = "Garantía(s) almacenada(s) correctamte. ";
      
      // SECCION PARA REGISTRAR HISTORIAL EN LAS GARANTIAS DE ACUERDO AL CREDITO
      $result = $oGarantia->listar_garantias($credito_id);
      if($result['estado']==1){
          foreach ($result['data'] as $key => $value) {
            $oTareainventario = new Tareainventario();
            $oTareainventario->tb_tarea_det = 'Realiza el almacenaje de la garantía '.$value['tb_garantia_id'].' en '.$value['tb_almacen_nom'];
            $oTareainventario->tb_tarea_usureg = $usu_reg_cre;
            $oTareainventario->tb_garantia_id = $value['tb_garantia_id'];
            $oTareainventario->tb_almacen_id1 = intval($value['tb_almacen_id']);
            $oTareainventario->tb_almacen_id2 = 0;
            $oTareainventario->insertar2();
        }
      }
    }

    $data['estado'] = 1;
    $data['mensaje'] .= "Desembolso registrado correctamente.";
    $data['egreso_id'] = $egreso_id;

    echo json_encode($data);
  }

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>
