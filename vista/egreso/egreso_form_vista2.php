            <div class="row">
              <!-- -->
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_egreso_can" class="control-label">Cantidad</label>
                      <input type="text" name="txt_egreso_can" id="txt_egreso_can" class="form-control input-sm" value="<?php echo $egreso_can;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_egresotipo_id" class="control-label">Tipo Garantía</label>
                      <select id="cmb_egresotipo_id" name="cmb_egresotipo_id" class="form-control input-sm">
                        <?php //require_once('../egresotipo/egresotipo_select.php');?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_egreso_pro" class="control-label">Producto</label>
                  <input type="text" name="txt_egreso_pro" id="txt_egreso_pro" class="form-control input-sm mayus" value="<?php echo $egreso_pro;?>">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_egreso_val" class="control-label">Valor a Prestar</label>
                      <input type="text" name="txt_egreso_val" id="txt_egreso_val" class="form-control input-sm moneda" value="<?php echo $egreso_val;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_egreso_valtas" class="control-label">Valor de Tasación</label>
                      <input type="text" name="txt_egreso_valtas" id="txt_egreso_valtas" class="form-control input-sm moneda" value="<?php echo $egreso_valtas;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group egreso_serie">
                  <label for="txt_egreso_ser" class="control-label">Serie del Producto</label>
                  <input type="text" name="txt_egreso_ser" id="txt_egreso_ser" class="form-control input-sm mayus" value="<?php echo $egreso_ser;?>">
                </div>
                <div class="row egreso_oro" style="display: none;">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_egreso_kil" class="control-label">Kilates</label>
                      <input type="text" name="txt_egreso_kil" id="txt_egreso_kil" class="form-control input-sm moneda" value="<?php echo $egreso_kil;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_egreso_pes" class="control-label">Peso</label>
                      <input type="text" name="txt_egreso_pes" id="txt_egreso_pes" class="form-control input-sm moneda" value="<?php echo $egreso_pes;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group egreso_oro" style="display: none;">
                  <label for="txt_egreso_tas" class="control-label">Tasado por</label>
                  <input type="text" name="txt_egreso_tas" id="txt_egreso_tas" class="form-control input-sm mayus" value="<?php echo $egreso_tas;?>">
                </div>
                <div class="form-group">
                  <label for="txt_egreso_det" class="control-label">Detalle del Producto</label>
                  <textarea name="txt_egreso_det" id="txt_egreso_det" class="form-control input-sm" rows="2" style="resize: vertical;"><?php echo $egreso_det;?></textarea>
                </div>
                <div class="form-group">
                  <label for="txt_egreso_web" class="control-label">Página Web de Referencia</label>
                  <textarea name="txt_egreso_web" id="txt_egreso_web" class="form-control input-sm" rows="2" style="resize: vertical;"><?php echo $egreso_web;?></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <?php
                    if($action == 'insertar')
                      echo '<button type="button" class="btn btn-info" disabled="true">Subir fotos</button>';
                    if($action == 'modificar')
                      echo '<button type="button" class="btn btn-info" onclick="egresofile_form(\'I\', '.$egreso_id.', 0)">Subir fotos</button>';
                  ?>
                </div>
                <?php if($action == 'modificar'):?>
                  <div class="row" id="egresofile_galeria">
                    <?php 
                      require_once('../egresofile/egresofile_galeria.php');
                    ?>
                  </div>
                <?php endif;?>
                <?php if($action == 'insertar'):?>
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>
                      <i class="icon fa fa-info"></i>Mensaje
                    </h4>
                    Puedes subir tus fotos aquí, despúes de guardar la garantía.
                  </div>
                <?php endif;?>
              </div>
            </div>
            <?php if($credito_id == 0):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> Estás intentando guardar una garantía sin un Crédito registrado, revisa antes de proceder por favor. ¿Seguro de proceder?</h4>
              </div>
<?php endif;?>