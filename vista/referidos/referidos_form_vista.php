
<h5 style="font-family: cambria;font-weight: bold;color: #003eff">Notas Adicionales</h5>
<div class="box box-primary">
    <div class="box-header">
        <div class="row">
            <div class="col-md-12">
                <div style="height: 150px; overflow-y: auto;">
                    <?php
                    if (!empty($usu_nom)) {
                        echo '
                <strong>Cliente registrado por: ' . $usu_nom . '</strong><br>
                <hr>';
                    }
                    echo $ref_his;
                    
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<h5 style="font-family: cambria;font-weight: bold;color: #003eff">Cronograma de Seguimiento del Cliente</h5>
<div class="box box-primary">
    <div class="box-header">
        <div class="row">
            <div class="col-md-12">
                <div style="height: 150px; overflow-y: auto; width: 98%">
                    <table id="tbl_crono" class="table table-hover">
                        <tr id="tabla_cabecera">
                            <th id="tabla_cabecera_fila">FECHA</th>
                            <th id="tabla_cabecera_fila">TÍTULO</th>
                            <th id="tabla_cabecera_fila">DETALLE</th>
                            <th></th>
                        </tr>
                        <tbody id="tbl_body">
                            <?php
                            $dts = $oTareaRef->listar_tareas_por_cliente($cli_id);
                            if ($dts['estado'] == 1) {
                                foreach ($dts['data']as $key => $dt) {
                                    echo '
                                <tr id="tabla_cabecera_fila">
                                  <td id="tabla_fila"><b>' . mostrar_fecha($dt['tb_tarearef_fec']) . '</b></td>
                                  <td id="tabla_fila">' . $dt['tb_tarearef_tit'] . '</td>';

                                    if (empty($dt['tb_tarearef_det'])) {
                                        echo '<td id="tabla_fila"><textarea id="txt_detalle' . $dt['tb_tarearef_id'] . '" rows="4" cols="42" placeholder="Ingresa información..."  class="form-control input-sm"></textarea></td>
                                      <td id="tabla_fila" align="center">
                                        <a class="btn btn-facebook btn-sm" id="btn_guardar' . $dt['tb_tarearef_id'] . '" onclick = "guardar_detalle(' . $dt['tb_tarearef_id'] . ')">x</a>
                                      </td>
                                </tr>';
                                    } else {
                                        echo '<td id="tabla_fila">' . $dt['tb_tarearef_det'] . '</td>
                                      <td id="tabla_fila" align="center"><!--a class="btn_eliminar" id="btn_eliminar' . $dt['tb_tarearef_id'] . '" data-taref="' . $dt['tb_tarearef_id'] . '">x</a--></td>
                                  </tr>';
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<h5 style="font-family: cambria;font-weight: bold;color: #003eff">Agregar Nueva Tarea</h5>
<div class="box box-primary">
    <div class="box-header">
        <input type="hidden" name="hdd_cli_id" id="hdd_cli_id" value="<?php echo $cli_id; ?>">
        <input type="hidden" name="hdd_usu_id" id="hdd_usu_id" value="<?php echo $usu_id; ?>">
        <div class="row">
             <div class="col-md-3">
                <div class="form-group">
                    <!--<label>Fecha :</label>-->
                    <div class='input-group date' id='referido_fil_picker1'>
                        <input type="text" name="txt_tarearef_fec" id="txt_tarearef_fec" class="form-control input-sm" value="<?php echo $referido_fec;?>">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <input type="text" name="txt_tarearef_tit" id="txt_tarearef_tit" style="width: 100%" class="form-control input-sm">
            </div>
            <div class="col-md-1">
                <a id="btn_guardar_ref" class="btn btn-facebook btn-sm">ADD</a>
            </div>
        </div>
    </div>
</div>

<h5 style="font-family: cambria;font-weight: bold;color: #003eff">Agregar Nota Adicional:</h5>
<div class="box box-primary">
    <div class="box-header">
        <input type="hidden" name="hdd_cli_id" id="hdd_cli_id" value="<?php echo $cli_id; ?>">
        <input type="hidden" name="hdd_usu_id" id="hdd_usu_id" value="<?php echo $usu_id; ?>">
        <div class="row">

            <div class="col-md-12">
                <textarea name="txt_nota" id="txt_nota" rows="2"  class="form-control input-sm"></textarea>
            </div>

        </div>
    </div>
</div>