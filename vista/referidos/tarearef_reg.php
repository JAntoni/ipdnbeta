<?php
require_once ('../../core/usuario_sesion.php');
require_once ("../referidos/TareaRef.class.php");
$oTareaRef = new TareaRef();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];

if($action == 'insertar_tarea'){
  $oTareaRef->tb_tarearef_fec=fecha_mysql($_POST['fecha']);
  $oTareaRef->tb_tarearef_tit=$_POST['titulo'];
  $oTareaRef->tb_tarearef_det='';
  $oTareaRef->tb_cliente_id=$_POST['cli_id']; 
  $oTareaRef->tb_usuario_id= $_POST['usu_id'];
  $oTareaRef->insertar();

  $data['est'] = 1;
  $data['fecha'] = $_POST['fecha'];
  $data['tit'] = $_POST['titulo'];

  $line_det = '<b>'.$_SESSION['usuario_nom'].'</b> Ha creado una tarea para su Cliente Referido con título: <b>'.$_POST['titulo'].'</b> && <b>'.date('d-m-Y h:i a').'</b>';
  $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
  $oLineaTiempo->tb_lineatiempo_det=$line_det;
  $oLineaTiempo->insertar();

  echo json_encode($data);
}

if($action == 'detalle'){
    
//  $oTareaRef->modificar_columna('tb_tarearef_det', $_POST['det'], $_POST['taref_id']);
  
  $oTareaRef->modificar_campo($_POST['taref_id'],'tb_tarearef_det', $_POST['det'], 'STR');

  $line_det = '<b>'.$_SESSION['usuario_nom'].'</b> Ha registrado una respuesta de su Cliente Referido: <b>'.$_POST['det'].'</b> && <b>'.date('d-m-Y h:i a').'</b>';
  $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
  $oLineaTiempo->tb_lineatiempo_det=$line_det;
  $oLineaTiempo->insertar();

  $data['est'] = 1;

  echo json_encode($data);
}
if($action == 'eliminar'){
  $oTareaRef->eliminar($_POST['taref_id']);

  $data['est'] = 1;

  echo json_encode($data);
}
?>