<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}
//
require_once ("Referidos.class.php");
$oReferido = new Referidos();

$usuario_id = $_SESSION['usuario_id'];
$grupo_id = $_SESSION['usuariogrupo_id'];

$usu_id = $_POST['usu_id'];
$ref_est = $_POST['ref_est'];
$cli_int = $_POST['cli_int'];
$fec1 = $_POST['cli_fec1'];
$fec2 = $_POST['cli_fec2'];

if ($_SESSION['usuariogrupo_id'] != 2)
    $usu_id = $usuario_id;

$dts = $oReferido->filtrar($usu_id, $ref_est, $cli_int, fecha_mysql($fec1), fecha_mysql($fec2));
//$num_rows = mysql_num_rows($dts);
?>

<?php if ($dts['estado'] == 1) { ?>
    <form id="form_referidos">
        <table cellspacing="1" id="tabla_cliente" class="table table-hover">
            <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila">N°</th>
                    <th id="tabla_cabecera_fila">CLIENTE</th>
                    <th id="tabla_cabecera_fila">EMAIL</th>
                    <th id="tabla_cabecera_fila">TELEFONO</th>
                    <th id="tabla_cabecera_fila">CELULAR</th>
                    <th id="tabla_cabecera_fila">ENCARGADO</th>
                    <th id="tabla_cabecera_fila">FEC ASIG.</th>
                    <th id="tabla_cabecera_fila">INTERES</th>
                    <th id="tabla_cabecera_fila">NOTAS</th>
                    <th id="tabla_cabecera_fila">&nbsp;</th>
    <?php
    if ($grupo_id == 2)
        echo '
                <th>ESTADO</th>
                <th></th>';
    ?>
                </tr>
            </thead>
            <tbody>
    <?php
    $cc = 0;
    foreach ($dts['data']as $key=>$dt) {
        $cc++;
        ?>
                    <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila"><?php echo $cc ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_cliente_nom'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_cliente_ema'] ?></td>                     
                        <td id="tabla_fila"><?php echo $dt['tb_cliente_tel'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_cliente_cel'] ?></td>
                        <td id="tabla_fila">
                            <?php
                            if (!empty($dt['tb_usuario_nom']))
                                echo $dt['tb_usuario_nom'];
                            else
                                echo 'Sin Asignar';
                            ?>
                        </td>
                        <td id="tabla_fila">
                            <?php echo mostrar_fecha_hora($dt['tb_referidos_fecasig']); ?>
                        </td>
                        <td id="tabla_fila">
                            <?php
                            if ($dt['tb_cliente_int'] == 1)
                                echo '<b>Crédito Menor</b>';
                            if ($dt['tb_cliente_int'] == 2)
                                echo '<b>Asistencia Vehicular</b>';
                            if ($dt['tb_cliente_int'] == 3)
                                echo '<b>Garantía Vehicular</b>';
                            if ($dt['tb_cliente_int'] == 4)
                                echo '<b>Crédito Hipotecario</b>';
                            ?>
                        </td>
                        <td id="tabla_fila" id="td_ref_<?php echo $dt['tb_referidos_id']; ?>">
                            <?php
                            echo '<a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="referidos_his(' . $dt['tb_referidos_id'] . ')">+</a>';
                            $arr_his = explode('||', $dt['tb_referidos_his']);

                            if (strlen(current($arr_his)) > 100)
                                echo substr(str_replace('style="color: green;"', '', current($arr_his)), 0, 100);
                            else
                                echo str_replace('style="color: green;"', '', current($arr_his));
                            ?>
                        </td>
                        <td id="tabla_fila" align="center" nowrap="nowrap">
                            <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="cliente_form('M', '<?php echo $dt['tb_cliente_id'] ?>')">Ver Cliente</a>
                        </td>
                        <?php if ($grupo_id == 2): ?>
                            <td id="tabla_fila" align="center" nowrap="nowrap">
                                <?php
                                if ($dt['tb_referidos_est'] == 1) {
                                    echo '<span style="color: green;">Aprobado</span>'; //aprobar rechazar, 1 aprobar, 2 rechazar
                                    echo '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="aprobar_rechazar(' . $dt['tb_referidos_id'] . ', 2)"> x </a>';
                                } else {
                                    echo '<span style="color: red;">Pendiente</span>'; //aprobar rechazar, 1 aprobar, 2 rechazar
                                    echo '<a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="aprobar_rechazar(' . $dt['tb_referidos_id'] . ', 1)"> y </a>';
                                }
                                ?>
                            </td>
                            <td id="tabla_fila" align="center">
                                <input type="checkbox" name="check_tarea[]" style="width: 20px; height: 18px;" class ="check_tarea" value="<?php echo $dt['tb_cliente_id'] ?>">
                            </td>
                        <?php endif; ?>
                    </tr><?php
                    }
//                    mysql_free_result($dts);
                    ?>
            </tbody>
        </table>
    </form>
<?php
} else
    echo '<center><h2>SIN RESULTADOS</h2></center>';
?>