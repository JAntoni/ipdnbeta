<?php

require_once ('../../core/usuario_sesion.php');
require_once ("../referidos/Referidos.class.php");
$oReferido = new Referidos();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();



$action = $_POST['action'];

if ($action == 'asignar') {
    $array_ref = $_POST['check_tarea']; //array de clientes seleccionadas
    
//    
    $usu_id = $_POST['usuario_id']; //usuario id
    //echo 'action: '.$action.' // usuario id: '.$usu_id.' /// '.$array_ref[0]; exit();
    $count = count($array_ref);
    for ($i = 0; $i < $count; $i++) {
        $dts = $oReferido->verificar_tarea($array_ref[$i], $usu_id);
        $rows= count($dts['data']);
        
        if ($dts['estado'] == 1) {
            $ref_id = $dts['data']['tb_referidos_id'];
        }
        $dts = NULL;

        $dts = $oUsuario->mostrarUno($usu_id);
        if ($dts['estado'] == 1) {
        $usu_nom = $dts['data']['tb_usuario_nom'];
        $usu_ape = $dts['data']['tb_usuario_ape'];
        $usu_dni = $dts['data']['tb_usuario_dni'];
        }
        $dts = NULL;

        //si hay una tarea de un referido para el mismo cliente, se actualiza su estado a 0 (pendiente)
        if ($ref_id > 0) {
            $his = '<strong style="color: brown;">' . $_SESSION['usuario_nom'] . ' te asignó esta tarea. || ' . date('d-m-Y h:i a') . '</strong><br>';
           
            $oReferido->editar_tarea($array_ref[$i], $usu_id, $ref_id);
            $oReferido->registrar_historial($his, $ref_id);
            
            $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha asignado una tarea de Referidos para el Asesor: <b>' . $usu_nom . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det=$line_det;
            $oLineaTiempo->insertar();
        } else {
            $his = '<strong style="color: brown;">' . $_SESSION['usuario_nom'] . ' te asignó esta tarea. || ' . date('d-m-Y h:i a') . '</strong><br>';
             
            $oReferido->tb_cliente_id=$array_ref[$i];
            $oReferido->tb_usuario_id=$usu_id;
            $oReferido->registrar_tarea();
//echo 'CLIENTE = '.$array_ref[0].' usuario = '.$usu_id.'referido = '.$ref_id;    exit();
            $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha asignado una tarea de Referidos para el Asesor: <b>' . $usu_nom . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det=$line_det;
            $oLineaTiempo->insertar();

//            $dts = $oReferido->ultimoInsert();
//            $dt = mysql_fetch_array($dts);
//            $ref_id = $dt['last_insert_id()'];
//            mysql_free_result($dts);

            $oReferido->registrar_historial($his, $ref_id);
        }
    }
    echo 'Se asignaron las tareas';
}

if ($action == 'insertar_nota') {
    $nota = $_POST['txt_nota'];
    $ref_id = $_POST['hdd_ref_id'];

    $style = '';
    if ($_SESSION['usuariogrupo_id'] != 2)
        $style = 'style="color: blue;"';

    $his = '<strong ' . $style . '>' . $_SESSION['usuario_nom'] . ': ' . $nota . ' || ' . date('d-m-Y h:i a') . '</strong><br>';
    $oReferido->registrar_historial($his, $ref_id);

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Registró una nota para su Cliente Referido: <b>' . $nota . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;
    $oLineaTiempo->insertar();

    $data['est'] = 1;
    $data['msj'] = 'Guardado';

    echo json_encode($data);
}

if ($action == 'accion_ref') {
    $ref_id = $_POST['ref_id'];
    $tipo = $_POST['tipo']; // 1 aprobar, 2 rechazar

    if ($tipo == 2)
        $tipo2 = 0;
    else
        $tipo2 = 1;

//    $oReferido->modificar_columna('tb_referidos_est', $tipo2, $ref_id);
    $oReferido->modificar_campo($ref_id, 'tb_referidos_est', $tipo2,'INT');

    $his = '<strong style="color: green;">' . $_SESSION['usuario_nom'] . ' aprobó esta tarea. || ' . date('d-m-Y h:i a') . '</strong><br>';
    if ($tipo == 2)
        $his = '<strong style="color: red;">' . $_SESSION['usuario_nom'] . ' rechazó esta tarea. || ' . date('d-m-Y h:i a') . '</strong><br>';

    $oReferido->registrar_historial($his, $ref_id);

    echo 'Acción guardada correctamente';
//    $data['est'] = 1;
//    $data['msj'] = 'Acción guardada correctamente';
}

if ($action == 'aprobar_select') {

    $array_cli = $_POST['check_tarea'];
    $tipo2 = 1; //estado resuelto en BD
    
    for ($i = 0; $i < count($array_cli); $i++) {
//        $oReferido->modificar_columna_por_cliente('tb_referidos_est', $tipo2, $array_cli[$i]);
        $oReferido->modificar_campo_cliente($array_cli[$i], 'tb_referidos_est', $tipo2,'INT');
    }


    /*
      $his = '<strong style="color: green;">'.$_SESSION['usuario_nom'].' aprobó esta tarea. || '.date('d-m-Y h:i a').'</strong><br>';
      if($tipo == 2)
      $his = '<strong style="color: red;">'.$_SESSION['usuario_nom'].' rechazó esta tarea. || '.date('d-m-Y h:i a').'</strong><br>';

      $oReferido->registrar_historial($his, $ref_id); */

    echo 'Acción guardada correctamentec select ';
}
?>