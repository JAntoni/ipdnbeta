<?php

require_once ("../../core/usuario_sesion.php");
require_once ("../referidos/Referidos.class.php");
$oReferido = new Referidos();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once('../referidos/TareaRef.class.php');
$oTareaRef = new TareaRef();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuario_nom = $_SESSION['usuario_nom'];
$usuariogrupo_id = $_SESSION['usuariogrupo_id'];
$referido_fec=date('d-m-Y');
$ref_id = $_POST['ref_id'];

$dts = $oReferido->mostrarUno($ref_id);

if ($dts['estado'] == 1) {
    $ref_his = $dts['data']['tb_referidos_his'];
    $cli_id = $dts['data']['tb_cliente_id'];
    $usu_id = $dts['data']['tb_usuario_id'];
}
$dts = NULL;

$dts = $oCliente->mostrarUno($cli_id);
if ($dts['estado'] == 1) {
    $usureg_id = $dts['data']['tb_cliente_usureg'];
}
$dts = NULL;

if ($usureg_id > 0) {
    $dts = $oUsuario->mostrarUno($usureg_id);
    if ($dts['estado'] == 1) {
        $usu_nom = $dts['data']['tb_usuario_nom'];
    }
    $dts = NULL;
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_referido" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Informaión de Historial</h4>
            </div>
            <form id="form_ref_his">
                <input type="hidden" name="action" value="insertar_nota">
                <input type="hidden" name="hdd_ref_id" id="hdd_ref_id" value="<?php echo $ref_id; ?>">
                <input type="hidden" id="hdd_usuario_nom" value="<?php echo $usuario_nom; ?>">
                <input type="hidden" id="hdd_usuariogrupo_id" value="<?php echo $usuariogrupo_id; ?>">

                <div class="modal-body">

                   
                    <?php require_once 'referidos_form_vista.php';?>



                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="referido_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_referido">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="vista/referidos/referidos_form.js?ver=123456"></script>