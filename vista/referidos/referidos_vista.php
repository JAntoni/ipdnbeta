<?php
$fec1 = date('01-m-Y');
$fec2 = date('d-m-Y');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <button class="btn btn-primary btn-sm" onclick="cliente_form('I', 0)"><i class="fa fa-plus"></i> Nueva Cliente</button>
                <!--                <div class="box-header">
                                        <button class="btn btn-primary btn-sm" onclick="ingreso_form('I',0)"><i class="fa fa-plus"></i> Nuevo Ingreso</button>
                                        <button class="btn btn-primary btn-sm" onclick=""><i class="fa fa-refresh"></i> Actualizar</button>
                                </div>-->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtro de inventario de garantias</label>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php include 'referidos_filtro.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="referidos_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_referidos_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('referidos_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_referidos_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_cliente_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
