<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Referidos extends Conexion {

    public $tb_referidos_id;
    public $tb_referidos_fecasig;
    public $tb_cliente_id;
    public $tb_usuario_id;
    public $tb_referidos_est;
    public $tb_referidos_his;

    function registrar_tarea() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_referidos (tb_referidos_fecasig,tb_cliente_id, tb_usuario_id, tb_referidos_est) VALUES (NOW(), :tb_cliente_id, :tb_usuario_id,0)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cliente_id", $this->tb_cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $tb_referidos_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $data['estado'] = $result;
            $data['referidos_id'] = $tb_referidos_id;
            return $data; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrar_historial($his, $ref_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_referidos set 
                                        tb_referidos_his = CONCAT(:tb_referidos_his,tb_referidos_his) 
                                where 
                                        tb_referidos_id =:tb_referidos_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_referidos_his", $his, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_referidos_id", $ref_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function editar_tarea($cli_id, $usu_id, $ref_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_referidos set tb_cliente_id =:tb_cliente_id, tb_usuario_id =:tb_usuario_id, tb_referidos_est =0 where tb_referidos_id =:tb_referidos_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_referidos_id", $ref_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($ref_id) {
        try {
            $sql = "SELECT * FROM tb_referidos where tb_referidos_id =:tb_referidos_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_referidos_id", $ref_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Referidos registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function filtrar($usu_id, $ref_est, $cli_int, $fec1, $fec2) {

        try {
            $sql = "SELECT 
                    cl.*, usu.*,tb_referidos_id, tb_referidos_est, tb_referidos_his, tb_referidos_fecasig 
                FROM 
                    tb_cliente cl left join tb_referidos re on re.tb_cliente_id = cl.tb_cliente_id 
                    left join tb_usuario usu on usu.tb_usuario_id = re.tb_usuario_id 
                where 
                    tb_cliente_ref = 1 and tb_cliente_emp = 0 and tb_cliente_xac = 1 and DATE(tb_cliente_reg) between :fec1 and :fec2";

            if (!empty($usu_id))
                $sql .= ' and re.tb_usuario_id =:tb_usuario_id';
            if ($ref_est != "")
                $sql .= ' and re.tb_referidos_est =:tb_referidos_est';
            if ($cli_int != "")
                $sql .= ' and cl.tb_cliente_int =:tb_cliente_int';

            $sql .= " order by cl.tb_cliente_id desc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
            if (!empty($usu_id))
                $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            if ($ref_est != "")
                $sentencia->bindParam(":tb_referidos_est", $ref_est, PDO::PARAM_INT);
            if ($cli_int != "")
                $sentencia->bindParam(":tb_cliente_int", $cli_int, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Egresos registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function verificar_tarea($cli_id, $usu_id) {
        try {
            $sql = "SELECT * FROM tb_referidos where tb_cliente_id =:tb_cliente_id and tb_usuario_id =:tb_usuario_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay horarios registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function modificar_campo($referidos_id, $referidos_columna, $referidos_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            $param_tip = strtoupper($param_tip);

            if (!empty($referidos_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_referidos SET " . $referidos_columna . " =:referidos_valor WHERE tb_referidos_id =:referidos_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":referidos_id", $referidos_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":referidos_valor", $referidos_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":referidos_valor", $referidos_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_campo_cliente($cliente_id, $referidos_columna, $referidos_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            $param_tip = strtoupper($param_tip);

            if (!empty($referidos_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_referidos SET " . $referidos_columna . " =:referidos_valor WHERE tb_cliente_id =:cliente_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":referidos_valor", $referidos_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":referidos_valor", $referidos_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

}
