<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class TareaRef extends Conexion {

    public $tb_tarearef_id;
    public $tb_tarearef_reg;
    public $tb_tarearef_fec;
    public $tb_tarearef_tit;
    public $tb_tarearef_det;
    public $tb_cliente_id;
    public $tb_usuario_id;

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_tarearef(
                                            tb_tarearef_reg,
                                            tb_tarearef_fec, 
                                            tb_tarearef_tit, 
                                            tb_tarearef_det, 
                                            tb_cliente_id, 
                                            tb_usuario_id) 
                                    VALUES(
                                            NOW(),
                                            :tb_tarearef_fec, 
                                            :tb_tarearef_tit, 
                                            :tb_tarearef_det, 
                                            :tb_cliente_id, 
                                            :tb_usuario_id)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_tarearef_fec", $this->tb_tarearef_fec, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_tarearef_tit", $this->tb_tarearef_tit, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_tarearef_det", $this->tb_tarearef_det, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_cliente_id", $this->tb_cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $tb_tarearef_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $data['estado'] = $result;
            $data['tb_tarearef_id'] = $tb_tarearef_id;
            return $data; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function listar_tareas_por_cliente($cli_id) {
        try {
            $sql = "SELECT * FROM tb_tarearef where tb_cliente_id =:tb_cliente_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Clientes registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function modificar_campo($tarearef_id, $tarearef_columna, $tarearef_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            $param_tip = strtoupper($param_tip);

            if (!empty($tarearef_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_tarearef SET " . $tarearef_columna . " =:tarearef_valor WHERE tb_tarearef_id =:tarearef_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tarearef_id", $tarearef_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":tarearef_valor", $tarearef_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":tarearef_valor", $tarearef_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function seguimiento_cliente_hoy($usu_id){
        try {
            $sql = "SELECT * FROM tb_tarearef tf inner join tb_cliente c on c.tb_cliente_id = tf.tb_cliente_id inner join tb_tarearef ref on ref.tb_cliente_id = tf.tb_cliente_id where (tb_tarearef_det IS NULL OR tb_tarearef_det = '') and tb_tarearef_fec <= NOW() and tb_tarearef_est = 0";
            if ($usu_id > 0)
                $sql .= " and tf.tb_usuario_id =:tb_usuario_id";

            $sentencia = $this->dblink->prepare($sql);
            if ($usu_id > 0)
                $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Clientes registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
      function eliminar(){
    //mejor que no exista el eliminar
  }

}
