/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#referido_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });
    
    
    
    $('#form_ref_his').submit(function(event) {
      event.preventDefault();

      var nota = $('#txt_nota').val().trim();
      if(!nota){
        alert('Ingrese una nota para guardar');
        return false;
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL+"referidos/referidos_reg.php",
        async:true,
        dataType: "json",                      
        data: $('#form_ref_his').serialize(),
        beforeSend: function() {
          $('#div_msj_error').text('Guardando...');
          $('#msj_referidos').hide(100);
        },
        success: function(data){
          if(parseInt(data.est) > 0){
//            $('#msj_referidos').show(100);
//            $('#div_referidos_his').dialog("close");
            $('#modal_registro_referido').modal('hide');
            swal_success("CORRECTO",'Nota adicional agregada para el cliente referido',2500);
            referidos_tabla();
            var ref_id = $('#hdd_ref_id').val();
            var nota = $('#txt_nota').val();
            var user_gru = $('#hdd_usuariogrupo_id').val();
            var user_nom = $('#hdd_usuario_nom').val();
            var strong = '';

            if(parseInt(user_gru) != 2)
              strong = '<strong style="color: blue;">'+user_nom+': '+nota+'</strong>';
            else
              strong = '<strong>'+user_nom+': '+nota+'</strong>';

            $('#td_ref_'+ref_id).find('strong').remove();
            $('#td_ref_'+ref_id).append(strong);
            //referidos_tabla();
          }
          else
            $('#div_msj_error').text('Error al guardar la nota, repórtelo');
        },
        complete: function(data){
          if(data.statusText != "success")
            $('#div_msj_error').text('Error: '+ data.responseText);
        }
      });
    });


});

function guardar_detalle(id) {
    var det = $('#txt_detalle' + id).val().trim();
    if (!det) {
//      alert('Ingresa un detalle para poder guardar');
        swal_warning("AVISO", 'Ingresa un detalle para poder guardar', 3500);
        $('#txt_detalle' + id).focus();
        return false;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/tarearef_reg.php",
        async: true,
        dataType: "json",
        data:
                ({
                    action: 'detalle',
                    taref_id: id,
                    det: $('#txt_detalle' + id).val()
                }),
        beforeSend: function () {
//        $('#div_msj_error').text('Guardando...');
        },
        success: function (data) {
            if (parseInt(data.est) > 0) {
                $('#txt_detalle' + id).attr('disabled', 'disabled');
                $('#btn_guardar' + id).hide();
//                $('#div_msj_error').css('color', 'green').text('Correcto');
//                ;
                swal_success("CORRECTO", "", 2000);
            } else {
//                $('#div_msj_error').text('Error al guardar el detalle, repórtelo');
                swal_error("ERROR", 'Error al guardar el detalle, repórtelo', 3000);
            }
        },
        complete: function (data) {
            if (data.statusText != "success") {
                //            swal_error("ERROR",data.responseText,3000);

            }
//                $('#div_msj_error').text('Error: ' + data.responseText);
        }
    });
}

$('.btn_eliminar').click(function (event) {
    event.preventDefault();
    var id = $(this).data('taref');
    if (!id) {
        alert('No es una tarea válida');
        return false;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/tarearef_reg.php",
        async: true,
        dataType: "json",
        data:
                ({
                    action: 'eliminar',
                    taref_id: id
                }),
        beforeSend: function () {
            $('#div_msj_error').text('Eliminando...');
        },
        success: function (data) {
            if (parseInt(data.est) > 0) {
                $(this).closest('tr').remove();
                $('#div_msj_error').css('color', 'green').text('Correcto');
            } else
                $('#div_msj_error').text('Error al guardar el detalle, repórtelo');
        },
        complete: function (data) {
            if (data.statusText != "success")
                $('#div_msj_error').text('Error: ' + data.responseText);
        }
    });
});


$('#btn_guardar_ref').click(function (event) {
    event.preventDefault();
    var fec = $('#txt_tarearef_fec').val().trim();
    var titu = $('#txt_tarearef_tit').val().trim();

    if (!fec || !titu) {
//        alert('Ingresa una fecha y un título para la tarea por favor.'); 
        swal_warning("AVISO", 'Ingresa una fecha y un título para la tarea por favor.', 3500);
        $('#txt_tarearef_fec').focus();
        return false;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/tarearef_reg.php",
        async: true,
        dataType: "json",
        data:
                ({
                    action: 'insertar_tarea',
                    cli_id: $('#hdd_cli_id').val(),
                    usu_id: $('#hdd_usu_id').val(),
                    fecha: $('#txt_tarearef_fec').val(),
                    titulo: $('#txt_tarearef_tit').val()
                }),
        beforeSend: function () {
//            $('#div_msj_error').text('Guardando...');
        },
        success: function (data) {
            if (parseInt(data.est) > 0) {
//                $('#div_referidos_his').dialog("close");
                $('#modal_registro_referido').modal('hide');
//                modal_hidden_bs_modal('modal_registro_referido', 'limpiar'); //funcion encontrada en public/js/generales.js
                    setTimeout(function(){ 
                    referidos_his($('#hdd_ref_id').val());
                     }, 3000);
                
            } else{
                swal_error("ERROR", 'Error al guardar la tarea, repórtelo', 3500);
            }
//                $('#div_msj_error').text('Error al guardar la tarea, repórtelo');
        },
        complete: function (data) {
            if (data.statusText != "success")
                $('#div_msj_error').text('Error: ' + data.responseText);
        }
    });
});

