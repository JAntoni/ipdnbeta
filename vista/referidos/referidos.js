/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    cmb_usu_id(1);
    referidos_tabla();


    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_ing_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_ing_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });


});

$("#cmb_empresa_id").change(function () {
    cmb_usu_id(1);
    referidos_tabla();
});
$("#cmb_usu_id2,#cmb_ref_est,#cmb_cli_int,#datetimepicker1,#datetimepicker2").change(function () {
    referidos_tabla();
});

function cmb_usu_id(mostrar) {
    var codigo = $("#cmb_empresa_id").val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/cmb_usu_id_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
            empresa_id: codigo
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
            $('#cmb_usu_id2').html(html);
        }
    });
}

function referidos_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/referidos_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            usu_id: $('#cmb_usu_id2').val(),
            ref_est: $('#cmb_ref_est').val(),
            cli_int: $('#cmb_cli_int').val(),
            cli_fec1: $('#txt_fec1').val(),
            cli_fec2: $('#txt_fec2').val()
        }),
        beforeSend: function () {
//            $('#referidos_mensaje_tbl').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
            $('#referidos_mensaje_tbl').show(300);
//            $('#div_referidos_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_referidos_tabla').html(html);
            $('#referidos_mensaje_tbl').hide(300);
            estilos_datatable();
        },
        complete: function (html) {
            $('#div_referidos_tabla').removeClass("ui-state-disabled");
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tabla_cliente').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [1, 2, 3, 4, 5, 6, 7], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}


function aprobar_rechazar(ref, tip) {
    //tip: 1 aprobar, 2 rechazar
    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/referidos_reg.php",
        async: true,
        dataType: "text",
        data: ({
            action: 'accion_ref',
            ref_id: ref,
            tipo: tip
        }),
        beforeSend: function () {

        },
        success: function (data) {
//            $('#msj_referidos').html(data);
            swal_success("SISTEMA", data, 3000);
            referidos_tabla();

        }
    });
}


function cliente_form(usuario_act, cliente_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente_2/cliente_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            cliente_id: cliente_id,
            vista: 'cliente_referido'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_cliente_form').html(data);
                $('#modal_registro_cliente').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_cliente'); //funcion encontrada en public/js/generales.js

                //funcion js para limbiar el modal al cerrarlo
                modal_hidden_bs_modal('modal_registro_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_cliente'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un ancho automatico al modal, al abrirlo
                modal_width_auto('modal_registro_cliente', 75); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'cliente';
                var div = 'div_modal_cliente_form';
                permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}


function aprobar_seleccionados() {
    var usuario = $("#cmb_usu_id").val();

    if(usuario==0){
        swal_warning("AVISO","Debe seleccionar un usuario a quien Asignar el Cliente",6000);
        return false;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/referidos_reg.php",
        async: true,
        dataType: "text",
        data: $('#form_referidos').serialize() + "&action=aprobar_select",
        beforeSend: function () {
//            $('#msj_referidos').show(100);
//            $('#msj_referidos').text('Guardando asignación de tarea de referidos');
        },
        success: function (data) {
//            $('#msj_referidos').text(data);
            swal_success("SISTEMA", data, 5000);
        },
        complete: function (data) {
            if (data.statusText != 'success')
                $('#msj_referidos').text('Error al asignar tarea: ' + data.responseText);
            else
                referidos_tabla();
        }
    });
}

$('#cmb_usu_id').change(function () {
    var usuario_id = $(this).val();
    var nombre = $("option:selected", this).text();
    //console.log(usuario_id);
    if (parseInt(usuario_id) > 0) {
        if (confirm('¿Está seguro de asignar la tarea a: ' + nombre + '?'))
            asignar_tarea(usuario_id);
    }
});


function asignar_tarea(usu) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/referidos_reg.php",
        async: true,
        dataType: "text",
        data: $('#form_referidos').serialize() + "&action=asignar&usuario_id=" + usu,
        beforeSend: function () {
//      $('#msj_referidos').show(100);
//      $('#msj_referidos').text('Guardando asignación de tarea de referidos');
        },
        success: function (data) {
//      $('#msj_referidos').text(data);
            swal_success("SISTEMA", data, 3500);
            referidos_tabla();
        },
        complete: function (data) {
            if (data.statusText != 'success')
                $('#msj_referidos').text('Error al asignar tarea: ' + data.responseText);
            else
                referidos_tabla();
        }
    });
}

function referidos_his(idf)
{
    if (!idf) {
        swal_warning('AVISO', 'No se ha asignada a nadie este Cliente', 4000);
        return false;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "referidos/referidos_form.php",
        async: true,
        dataType: "html",
        data: ({
            ref_id: idf
        }),
        beforeSend: function () {
//      $('#div_referidos_his').empty();
//      $('#div_referidos_his').dialog("open");
//      $('#div_referidos_his').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function (html) {
            $('#div_modal_referidos_form').html(html);
            $('#modal_registro_referido').modal('show');
            modal_hidden_bs_modal('modal_registro_referido', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_registro_referido', 40);
        }
    });
}