<?php 
$empresa_id=$_SESSION['empresa_id'];
?>
<form id="ingreso_filtro" name="ingreso_filtro">

    <div class="row">
        <div class="col-md-1">
            <label id="lbl_tipo_tar">Empresa: </label>
            <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">
                <?php require_once 'vista/empresa/empresa_select.php';?>
            </select>
        </div>
        <div class="col-md-2">
            <label id="lbl_tipo_tar">Usuario: </label>
            <select name="cmb_usu_id2" id="cmb_usu_id2" class="form-control input-sm mayus"></select>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label>Estado:</label>
                <select name="cmb_ref_est" id="cmb_ref_est" class="form-control input-sm mayus">
                    <option value="">--</option>
                    <option value="0">Pendiente</option>
                    <option value="1">Aprobado</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Crédito Intrerés:</label>
                <select name="cmb_cli_int" id="cmb_cli_int" class="form-control input-sm mayus">
                    <option value="">--</option>
                    <option value="1">Crédito Menor</option>
                    <option value="2">Asistencia Vehicular</option>
                    <option value="3">Garantía Vehicular</option>
                    <option value="4">Crédito Hipotecario</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha </label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type="text" id="txt_fec1" name="txt_fec1" value="<?php echo $fec1;?>" class="form-control input-sm mayus">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type="text" id="txt_fec2" name="txt_fec2" value="<?php echo $fec2;?>" class="form-control input-sm mayus">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label id="lbl_tipo_tar" style="font-weight: bold;color: green;">Asignar a: </label>
              <select name="cmb_usu_id" id="cmb_usu_id"  class="form-control input-sm mayus"></select>
        </div>
<!--        <div class="col-md-1">
            <br><br>
          <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="aprobar_seleccionados()">Aprobar</a>  
        </div>-->
    </div>

</form>