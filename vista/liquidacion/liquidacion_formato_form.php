<?php
  require_once('../../core/usuario_sesion.php');
  require_once ("../usuario/Usuario.class.php");
  $oUsuario = new Usuario();
  require_once ("../acuerdopago/Acuerdopago.class.php");
  $oAcuerdopago = new Acuerdopago();
  require_once ("../refinanciar/Refinanciar.class.php");
  $oRefinanciar = new Refinanciar();
  require_once ("../monedacambio/Monedacambio.class.php");
  $oMonedacambio = new Monedacambio();
  require_once ("../ingreso/Ingreso.class.php");
  $oIngreso = new Ingreso();
  require_once ("../egreso/Egreso.class.php");
  $oEgreso = new Egreso();
  require_once('../cuota/Cuotadetalle.class.php');
  $oCuotadetalle = new Cuotadetalle();
  require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();
  require_once ("../cuotapago/Cuotapago.class.php");
  $oCuotapago = new Cuotapago();
  require_once ("../gasto/Gasto.class.php");
  $oGasto = new Gasto();
  require_once("../controlseguro/Poliza.class.php");
  $oPoliza = new Poliza();

  require_once('../liquidacion/Liquidacion.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');
  require_once("../refinanciar/refinanciar_funciones.php"); //para funciones propias de REFINANCIAR

  $oLiquidacion = new Liquidacion();

  $fecha_hoy = date('Y-m-d');

  $vista = $_POST["vista"];

  $credito_id = $_POST['credito_id'];
  $creditotipo_id = $_POST['creditotipo_id']; //tipo de crédito 1 menor, 2 asiveh, 3 garveh, 4 hipo
  $fecha_liquidacion = mostrar_fecha(trim($_POST['fecha_liquidacion'])); //fecha que se ingresa al momento de solicitar una refinanciación
  $fecha_hoy = fecha_mysql($fecha_liquidacion); //date('Y-m-d');
  $TIPO_DE_CAMBIO = $_POST['tipo_cambio'];
  $simbolo_credito = 'SIN-SIMBOLO';
  $cli_id = 0;

  // echo '<h2 style="color: red;">HAS INGRESADO: CRE ID: '.$credito_id.' / TIP: '.$creditotipo_id.' /FEC '.$fecha_liquidacion.' / HOY'.$fecha_hoy.' / CAM: '.$TIPO_DE_CAMBIO.'</h2>';
  // exit();
  // $credito_id = 501;
  // $creditotipo_id = 3;
  // $tabla_credito = 'tb_creditogarveh';
  // $fecha_liquidacion = '08-08-2023';
  // $fecha_hoy = date('Y-m-d');

  if($creditotipo_id == 2){
    require_once ("../creditoasiveh/Creditoasiveh.class.php");
    $oCredito = new Creditoasiveh();
    $simbolo_credito = 'ASV';
    $tabla_credito = 'tb_creditoasiveh';
  }
  if($creditotipo_id == 3){
    require_once ("../creditogarveh/Creditogarveh.class.php");
    $oCredito = new Creditogarveh();
    $simbolo_credito = 'CGV';
    $tabla_credito = 'tb_creditogarveh';
  }
  if($creditotipo_id == 4){
    require_once ("../creditohipo/Creditohipo.class.php");
    $oCredito = new Creditohipo();
    $simbolo_credito = 'CH';
    $tabla_credito = 'tb_creditohipo';
  }

  $validar_fecha = validar_fecha_espanol($fecha_liquidacion);

  if(!$validar_fecha){
    echo '<h2 style="color: red;">POR FAVOR INGRESE UNA FECHA VÁLIDA PARA PODER CALCULAR, RECUERDE QUE EL FORMATO ES COMO EL SIGUIENTE: 01-10-2018 / '.$fecha_liquidacion.'</h2>';
    exit();
  }

  $cre_feccre = date('d-m-Y');
  $cre_fecdes = date('d-m-Y');
  $cre_fecfac = date('d-m-Y');
  $cre_fecent = date('d-m-Y');
  $cre_fecpro = date('d-m-Y');

  $result = $oCredito->mostrarUno($credito_id);
    if($result['estado'] == 1){
      $cre_matriz = $result['data']['tb_credito_idori'];
      $credito_matriz_moneda_id = $result['data']['tb_moneda_id'];
      $credito_preaco = floatval($result['data']['tb_credito_preaco']);
      $credito_fecha_facturacion = $result['data']['tb_credito_fecfac'];
    }
  $result = null;

  $simbolo_moneda_matriz = simbolo_moneda($credito_matriz_moneda_id); //1 de vuelve s/, 2 $usd

  //* DESDE AQUÍ EMPIEZA EL BUCLE CON TODOS LOS CRÉDITOS QUE PERTENECEN AL CREDITO MATRIZ
  $credito_padre_id = $credito_id;
  $array_creditos = array();
  array_push($array_creditos, $credito_id);

  $result = $oCredito->listar_adendas_credito($credito_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        array_push($array_creditos, $value['tb_credito_id']);
      }
    }
  $result = NULL;

  $SUMA_GLOBAL_A_PAGAR = 0;
  $SUMA_TOTAL_GPS = 0; //PARA CAPTAR EL MONTO FALTANTE DE PAGO DEL GPS
  $SUMA_GLOBAL_INCAUTACION = 0;
  $CONTENT_TABS = '';
  $CONTENT_HEADERS = '';
  $TABLA_RESUMEN = '';
  $TEXTO_RECORRIDO = '';
  $conView=0;

  for ($i=0; $i < count($array_creditos); $i++) { 

    $credito_id = $array_creditos[$i];
    $codigo_credito = $simbolo_credito.'-'.str_pad($credito_id, 4, "0", STR_PAD_LEFT);

    $ultima_cuota_pagada_fecha = '';
    $cuota_facturada_pagada = 'NO'; //si existe una cuota facturada y pagada

    $TOTAL_INTERESES_PREVIOS = 0;
    $TOTAL_PAGOS_PREVIOS = 0;

    $TOTAl_INTERESES_POSTERIORES = 0;
    $TOTAl_PAGOS_POSTERIORES = 0;
    $PRORRATEO_TOTAL = 0;

    $CAPITAL_RESTANTE_CAMBIO = 0;
    $INTERESES_CUOTAS_CAMBIO = 0;
    $PRORRATEO_TOTAL_CAMBIO = 0;
    $SUB_TOTAL_PAGAR = 0;

    $result = $oCredito->mostrarUno($credito_id);
      if($result['estado'] == 1){
        $credito_hijo_moneda_id = $result['data']['tb_moneda_id'];
        $cli_id = $result['data']['tb_cliente_id'];
        $credito_preaco = floatval($result['data']['tb_credito_preaco']);
        $credito_fecha_facturacion = $result['data']['tb_credito_fecfac'];
        $cre_cuotip = intval($result['data']['tb_cuotatipo_id']);
        $cre_subper_id = $result['data']['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
        $cre_fecren= mostrar_fecha($result['data']['tb_credito_fecren']); //fecha de vencimiento del SOAT VER: tb_credito_fecsoat  // ESTABA: tb_credito_fecren
        $cre_fecseg= mostrar_fecha($result['data']['tb_credito_fecseg']); //fecha de vencimiento del STR VER: tb_credito_fecstr // ESTABA: tb_credito_fecseg
        $cre_fecgps= mostrar_fecha($result['data']['tb_credito_fecgps']); //fecha de vencimiento del GPS
        $cre_fpagimp = mostrar_fecha($result['data']['tb_credito_pagimp']); //fecha del pago del inpuesto vehicular
        $credito_int = floatval($result['data']['tb_credito_int']);
        $cre_diaspa = intval($result['data']['tb_credito_diapar']);
        $cliente_nom = $result['data']['tb_cliente_nom'];
        $cliente_doc = $result['data']['tb_cliente_doc'];
        if(trim($result['data']['tb_cliente_emprs']) != ''){
          $cliente_nom = $result['data']['tb_cliente_emprs'];
          $cliente_doc = $result['data']['tb_cliente_empruc'];
        }
        $vehiculo_marca = $result['data']['tb_vehiculomarca_nom'];
        $vehiculo_modelo = $result['data']['tb_vehiculomodelo_nom'];
        $vehiculo_placa = $result['data']['tb_credito_vehpla'];

        $credito_numcuo = intval($result['data']['tb_credito_numcuo']); //captamos el número de cuotas del crédito
        $credito_pregps = $result['data']['tb_credito_pregps']; // optenemos el valor del gps guardado en el crédito
      }
    $result = null;

    $simbolo_moneda_hijo = simbolo_moneda($credito_hijo_moneda_id); //1 de vuelve s/, 2 $usd
    $titulo1 = 'ADENDA '.$i;
    $titulo2 = '<b>PRORRATEO</b> DE LA ADENDA:';
    $class_fila = 'class="warning"';
    if($credito_id == $credito_padre_id){
      $titulo1 = 'CREDITO MATRIZ';
      $titulo2 = '<b>PRORRATEO</b> DEL CREDITO MATRIZ:';
      $class_fila = 'class="success"';
    }

    //? PASO 1: IDENTIFICAR LA ULTIMA CUOTA FACTURADA Y PAGADA: obtenemos capital actual y fecha de cuota, esta función lista las cuotas en estado 2 (pagadas) menores a la fecha de HOY
    $result = $oRefinanciar->ultima_cuotadetalle_facturada_pagada($credito_id, $creditotipo_id); //* EVALUAR QUITAR LA FECHA HOY PARA EL ANÁLISIS
      if($result['estado'] == 1){
        $cuota_facturada_pagada = 'SI';
        $ultima_cuota_pagada_fecha = $result['data']['tb_cuotadetalle_fec'];
        $TEXTO_RECORRIDO .= 'El crédito de ID: '.$credito_id.' si tiene una cuota detalle facturada y pagada. ID Cuotadetalle: '.$result['data']['tb_cuotadetalle_id'].', de fecha: '.$result['data']['tb_cuotadetalle_fec'];
      }
      else
        $TEXTO_RECORRIDO .= 'El crédito de ID: '.$credito_id.', no tiene cuotas pagadas';
    $result = NULL;

    if($cuota_facturada_pagada == 'SI'){
      //! PASO 2: LISTAMOS LAS CUOTAS MENORES EN FECHA A LA OBTENIDA: esto para poder sumar todas las cuotas pagadas y determinar el CAPITAL actual adeudado
      //listamos todas las cuotas <= a la cuotadetalle para sumar los pagos realizados
      $prorrateo_cuota = 0;

      $numero_cuotas_pagadas = 0;
      $MONTO_FACTURADO_GPS = 0;
      $valor_una_cuota_gps = 0;

      $result = $oRefinanciar->listar_cuotasdetalle_menores_condicion_fecha($credito_id, $creditotipo_id, $ultima_cuota_pagada_fecha, '<='); 
        if($result['estado'] == 1){
          foreach ($result['data'] as $key => $value) {
            $numero_cuotas_pagadas ++;
            $MONTO_FACTURADO_GPS += $value['tb_cuota_pregps'];
            $valor_una_cuota_gps = $value['tb_cuota_pregps'];

            $TOTAL_INTERESES_PREVIOS += ($value['tb_cuota_int'] + $value['tb_cuota_pro']); //? el prorrateo en la cuota se debe considerar como interés adicional
            $prorrateo_cuota += $value['tb_cuota_pro'];
            //OBTENEMOS LOS PAGOS DE CADA CUOTA PAGADA
            $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']); //2 pago cuotadetalle
              if($result2['estado'] == 1){
                $TOTAL_PAGOS_PREVIOS += $result2['data']['importe_total'];
              }
            $result2 = NULL;
          }
          $TEXTO_RECORRIDO .= '. Se ha encontrado '.count($result['data']).' cuotas pagadas <= a fecha de la última cuota pagada: '.$ultima_cuota_pagada_fecha.', la suma de intereses previos es: '.$TOTAL_INTERESES_PREVIOS.', y el total de pagos previos es: '.$TOTAL_PAGOS_PREVIOS.'. Tiene un total de prorrateo: '.$prorrateo_cuota;
        }
      $result = NULL;

      //* PASO 3: FECHAS VENCIDAS IMPAGAS MAYORES A ULTIMA CUOTA FACTURADA: identificamos las cuotas mayores a la facturada para contarlas y sumar sus intereses
      $cuotas_vencidas_impagas = '';
      $result = $oRefinanciar->listar_cuotasdetalle_impagas_entre_rango($credito_id, $creditotipo_id, $ultima_cuota_pagada_fecha, $fecha_hoy);
        if($result['estado'] == 1){
          foreach ($result['data'] as $key => $value) {
            $TOTAl_INTERESES_POSTERIORES += $value['tb_cuota_int'];
            //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
            $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']); //2 pago cuotadetalle
              if($result2['estado'] == 1){
                $TOTAl_PAGOS_POSTERIORES += $result2['data']['importe_total'];
              }
            $result2 = NULL;

            $cuotas_vencidas_impagas .= '<b>'.mostrar_fecha($value['tb_cuotadetalle_fec']).'</b>, ';
          }

          $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
          $TEXTO_RECORRIDO .= '. Las cuotas vencidas impagas son: '.$cuotas_vencidas_impagas.'. El total de intereses posteriores es: '.$TOTAl_INTERESES_POSTERIORES.', el pago realizado a las cuotas posteriores es: '.$TOTAl_PAGOS_POSTERIORES;
        }
      $result = NULL;

      //TODO PASO 4: CALCULAR EL PRORRATEO DE DÍAS PASADOS HASTA LA FECHA ACTUAL
      $ultima_cuota_facturada_fecha = '';
      $ultima_cuota_facturada_interes_valido = 0;
      $dias_prorrateo = 0;
      $interes_por_dia = 0;
      
      //PUEDE O NO ESTAR PAGADA ESTA CUOTA FACTURADA, SOLO NOS SIRVE PARA CALCULAR UN PRORRATEO, LISTA TODAS LAS CUOTAS MENORES A HOY
      $result = $oRefinanciar->ultima_cuotadetalle_facturada($credito_id, $creditotipo_id);
        if($result['estado'] == 1){
          $ultima_cuota_facturada_fecha = $result['data']['tb_cuotadetalle_fec'];
          $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
          $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
          //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
          $ultima_cuota_facturada_interes_valido = formato_numero($operacion);

          $TEXTO_RECORRIDO .= '. Para calcular el prorrateo se está tomando la ultima fecha facturada: '.$result['data']['tb_cuotadetalle_fec'].', el capital que se usa para esto es: '.$ultima_cuota_facturada_capital_restante.', el interés que se usará para calcular el interés diario es: '.$ultima_cuota_facturada_interes_valido;
        }
      $result = NULL;

      if($ultima_cuota_facturada_fecha != ''){
        list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
        $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
        $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

        $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
        $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
        $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
        $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);

        $TEXTO_RECORRIDO .= '. El interés diario calculado entonces es: '.$interes_por_dia.', resultado de la fórmula: interes valido / dias del mes facturado: '.$ultima_cuota_facturada_interes_valido.'/'.$dias_del_mes_facturado.', total prorrateo hasta hoy: '.$PRORRATEO_TOTAL;
      }
      
      $monto_gps_restante = $valor_una_cuota_gps * ($credito_numcuo - $numero_cuotas_pagadas); //el monto facturado de gps, suma el monto total pagado

      $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $credito_preaco + $TOTAL_INTERESES_PREVIOS + $MONTO_FACTURADO_GPS - $TOTAL_PAGOS_PREVIOS;
      $INTERESES_CUOTAS_IMPAGAS = $TOTAl_INTERESES_POSTERIORES - $TOTAl_PAGOS_POSTERIORES;

      $TEXTO_RECORRIDO .= '. Los calculos finales se realizan con la suma de préstamo del crédito + total intereses previos - total pagos previos: '.$credito_preaco.' + '. $TOTAL_INTERESES_PREVIOS.' - '.$TOTAL_PAGOS_PREVIOS.'. Revisar aquí que la suma de intereses previos también sumen el prorrateo que haya pactado al principio.';

      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA; //? CAPITAL RESTANTE QUE DEBE PAGAR EL CLIENTE, AL TIPO DE CAMBIO MATRIZ
      $INTERESES_CUOTAS_CAMBIO = $INTERESES_CUOTAS_IMPAGAS; //? INTERESES DE CUOTAS VENCIDAS AL TIPO DE CAMBIO MATRIZ
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL; //? PRORRATEO HASTA LA FECHA DE LIQUIDACIÓN
      $GPS_TOTAL_CAMBIO = $monto_gps_restante;

      if($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2){
        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA * $TIPO_DE_CAMBIO;
        $INTERESES_CUOTAS_CAMBIO = $INTERESES_CUOTAS_IMPAGAS * $TIPO_DE_CAMBIO;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL * $TIPO_DE_CAMBIO;
        if(intval($GPS_TOTAL_CAMBIO) > 0)
          $GPS_TOTAL_CAMBIO = $monto_gps_restante * $TIPO_DE_CAMBIO;
      }
      if($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1){
        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA / $TIPO_DE_CAMBIO;
        $INTERESES_CUOTAS_CAMBIO = $INTERESES_CUOTAS_IMPAGAS / $TIPO_DE_CAMBIO;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
        if(intval($GPS_TOTAL_CAMBIO) > 0)
          $GPS_TOTAL_CAMBIO = $monto_gps_restante / $TIPO_DE_CAMBIO;
      }

      $SUB_TOTAL_PAGAR = formato_numero($CAPITAL_RESTANTE_CAMBIO + $INTERESES_CUOTAS_CAMBIO + $PRORRATEO_TOTAL_CAMBIO + $GPS_TOTAL_CAMBIO);
      $SUMA_GLOBAL_A_PAGAR += formato_numero($CAPITAL_RESTANTE_CAMBIO + $INTERESES_CUOTAS_CAMBIO + $PRORRATEO_TOTAL_CAMBIO + $GPS_TOTAL_CAMBIO);

    }
    //? FIN IF DE UNA CUOTA PAGADA = SI
    if($cuota_facturada_pagada == 'NO'){
      //SI NO TIENE UNA CUOTA PAGADA DE FECHA MENOR A HOY, VEAMOS SI TIENE CUOTAS IMPAGAS MENOR A HOY. TAMBIEN PUEDE SER QUE NO TENGA NINGUNA CUOTA FACTURADA, ES DECIR ESTÁ PAGANDO ANTES DE TIEMPO
      $ultima_cuota_facturada_fecha = '';
      $ultima_cuota_facturada_interes_valido = 0;
      $dias_prorrateo = 0;
      $interes_por_dia = 0;

      $result = $oRefinanciar->ultima_cuotadetalle_facturada($credito_id, $creditotipo_id); //PUEDE ESTAR PAGADA O NO
        if($result['estado'] == 1){
          $ultima_cuota_facturada_fecha = $result['data']['tb_cuotadetalle_fec'];
          $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
          $valor_una_cuota_gps = $result['data']['tb_cuota_pregps'];
          //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
          if($ultima_cuota_facturada_capital_restante > 0){
            $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
            $ultima_cuota_facturada_interes_valido = formato_numero($operacion);
          }
          else
            $ultima_cuota_facturada_interes_valido = formato_numero($result['data']['tb_cuota_int']);
        }
      $result = NULL;

      if($ultima_cuota_facturada_fecha != ''){
        //listamos todas las cuotas <= a la cuotadetalle para sumar los pagos realizados
        $todo_cuotas_impagas = '';
        $result = $oRefinanciar->listar_cuotasdetalle_menores_condicion_fecha($credito_id, $creditotipo_id, $ultima_cuota_facturada_fecha, '<='); 
          if($result['estado'] == 1){
            foreach ($result['data'] as $key => $value) {
              $TOTAL_INTERESES_PREVIOS += ($value['tb_cuota_int'] + $value['tb_cuota_pro']); //? el prorrateo en la cuota se debe considerar como interés adicional
              $todo_cuotas_impagas .= '<b>'.mostrar_fecha($value['tb_cuotadetalle_fec']).'</b>, ';
              //OBTENEMOS LOS PAGOS DE CADA CUOTA PAGADA
              $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']); //2 pago cuotadetalle
                if($result2['estado'] == 1){
                  $TOTAL_PAGOS_PREVIOS += $result2['data']['importe_total'];
                }
              $result2 = NULL;
            }

            $todo_cuotas_impagas = substr($todo_cuotas_impagas, 0, -2);
          }
        $result = NULL;

        list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
        $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
        $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

        $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
        $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
        $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
        $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);
        
        $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $credito_preaco + $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS;

        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;

        if($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2){
          $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA * $TIPO_DE_CAMBIO;
          $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL * $TIPO_DE_CAMBIO;
        }
        if($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1){
          $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA / $TIPO_DE_CAMBIO;
          $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
        }

        $SUB_TOTAL_PAGAR = formato_numero($CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO);
        $SUMA_GLOBAL_A_PAGAR += formato_numero($CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO);
      }

      if($ultima_cuota_facturada_fecha == ''){
        //SI NO TIENE FECHAS IMPAGAS VENCIDAS, ENTONCES ES UNA LIQUIDACION ANTES DE TIEMPO
        //CALULAMOS LOS DÍAS PASADOS PARA EL PRORRATEO Y EL INTERÉS VALIDO PARA EL CALCULO
        $result2 = $oRefinanciar->obtener_primera_cuotadetalle($credito_id, $creditotipo_id);
          if($result2['estado'] == 1){
            $valor_una_cuota_gps = $result2['data']['tb_cuota_pregps'];
          }
        $result2 = NULL;

        list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $credito_fecha_facturacion);
        $interes_valido = formato_numero($credito_preaco * $credito_int / 100);
        $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
        $interes_por_dia = formato_numero($interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

        $fecha_facturada_time = new DateTime($credito_fecha_facturacion); //cuantos días han pasado desde la ultima facturada hasta hoy
        $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
        $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
        $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);

        //CALCULAMOS TODOS LOS PAGOS QUE HAYA TENIDO ESTE CRÉDITO SIN CUOTAS FACTURADAS, SUELE PASAR QUE TIENE PAGOS
        $ingreso_total_credito = 0;
        $result2 = $oIngreso->ingreso_total_por_credito_cuotadetalle($credito_id, $creditotipo_id, $credito_hijo_moneda_id);
          if($result2['estado']==1){
            foreach ($result2['data'] as $key => $value) {
              $ingreso_total_credito = floatval($value['importe_total']);
            }
          }
        $result2= null;
        
        $CAPITAL_REAL_RESTANTE = $credito_preaco - $ingreso_total_credito;

        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;

        if($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2){
          $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE * $TIPO_DE_CAMBIO;
          $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL * $TIPO_DE_CAMBIO;
        }
        if($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1){
          $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE / $TIPO_DE_CAMBIO;
          $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
        }

        $SUB_TOTAL_PAGAR = formato_numero($CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO);
        $SUMA_GLOBAL_A_PAGAR += formato_numero($CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO);
      }

      $monto_gps_restante = $valor_una_cuota_gps * $credito_numcuo; //el monto facturado de gps, suma el monto total pagado
      $GPS_TOTAL_CAMBIO = $monto_gps_restante;

      if ($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2 && intval($GPS_TOTAL_CAMBIO) > 0) {
        $GPS_TOTAL_CAMBIO = $monto_gps_restante * $TIPO_DE_CAMBIO;
      }
      if ($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1 && intval($GPS_TOTAL_CAMBIO) > 0) {
        $GPS_TOTAL_CAMBIO = $monto_gps_restante / $TIPO_DE_CAMBIO;
      }

      $SUB_TOTAL_PAGAR += $GPS_TOTAL_CAMBIO;
      $SUMA_GLOBAL_A_PAGAR += $GPS_TOTAL_CAMBIO;
    }

    //? CONSULTAR AQUÍ LOS COSTOS DE INCAUTACIÓN PARA EL GLOBAL A PAGAR
    $gastos_generados = 0;
    $result2 = $oGasto->listar_gastos_credito_tipo($credito_id, $creditotipo_id);
      if($result2['estado'] == 1){
        foreach ($result2['data'] as $key => $value) {
          $gastos = $value['tb_gasto_ptl'] - $value['tb_gasto_devo']; //? DESDE EL MODULO DE GASTOS HAY DEVOLUCIONES QUE SE REALIZAN A CADA GASTO, se debe restar
          if($credito_matriz_moneda_id == 1 && $value['tb_moneda_id'] == 2){
            $gastos = ($value['tb_gasto_ptl'] - $value['tb_gasto_devo']) * $TIPO_DE_CAMBIO;
          }
          if($credito_matriz_moneda_id == 2 && $value['tb_moneda_id'] == 1){
            $gastos = ($value['tb_gasto_ptl'] - $value['tb_gasto_devo']) / $TIPO_DE_CAMBIO;
          }
          $gastos_generados += $gastos;
        }
      }
    $result2 = NULL;

    $cronograma = '';
    $result2 = $oLiquidacion->mostrar_cronograma_credito($credito_id, $creditotipo_id, $tabla_credito);
      if($result2['estado'] == 1){
        foreach ($result2['data'] as $key => $value) {

          $estado = '';
          if($value['tb_cuota_est'] == 2)
            $estado = '<span class="pull-right badge bg-green">Pagada</span>';
          if($value['tb_cuota_est'] == 3)
            $estado = '<span class="pull-right badge bg-aqua">Pago Parcial</span>';
          if($value['tb_cuota_est'] == 1 && strtotime($fecha_hoy) >= strtotime($value['tb_cuota_fec'])){
            $estado = '<span class="pull-right badge bg-red">Vencida</span>';
          }
          
          $cronograma .= '
            <tr>
              <td>'.$value['tb_cuota_num'].'</td>
              <td>'.$value['tb_cuota_fec'].'</td>
              <td align="center">'.$estado.'</td>
              <td>'.$value['tb_cuota_cap'].'</td>
              <td>'.$value['tb_cuota_amo'].'</td>
              <td>'.$value['tb_cuota_int'].'</td>
              <td>'.$value['tb_cuota_pregps'].'</td>
              <td>'.$value['tb_cuota_pro'].'</td>
              <td>'.$value['tb_cuota_cuo'].'</td>
            </tr>
          ';
        }
      }
    $result2 = NULL;

    //? VAMOS A RECOLECTAR LOS DATOS OBTENIDOS PARA GREGARLOS A LOS TABS
    $active = '';
    if($i == 0){
      $active = 'active';
      $TABLA_RESUMEN .= '
        <tr>
          <th>MONEDA</th>
          <td>'.$simbolo_moneda_matriz.' <input type="hidden" id="hdd_credito_moneda_id" name="hdd_credito_moneda_id" value="'.$credito_matriz_moneda_id.'"> </td>
        </tr>
      ';
    }

    $conView++;

    $CONTENT_HEADERS .= '<li class="'.$active.'"><a href="#tab_'.$i.'" data-toggle="tab" aria-expanded="true">'.$titulo1.'</a></li>';
    $CONTENT_TABS .= '
      <div id="tab_'.$i.'" class="tab-pane '.$active.'">
        <div class="row mb-4">
          <!-- SECCION DEL ENCABEZADO: DETALLE DATOS DEL CLIENTE-->
          <div class="col-md-12">
            <div class="container-fluid shadow">
              <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <tbody id="">
                  <tr>
                    <th>N° CONTRARO:</th>
                    <td>'.$codigo_credito.'</td>
                    <th>FECHA EMISIÓN:</th>
                    <td>'.date('d-m-Y h:i').'</td>
                    <th>RESPONSABLE:</th>
                    <td>'.$_SESSION['usuario_nom'].'</td>
                  </tr>
                  <tr>
                    <th>CLIENTE:</th>
                    <td>'.$cliente_nom.'</td>
                    <th>DNI / RUC:</th>
                    <td>'.$cliente_doc.'</td>
                    <th>CALCULADO A LA FECHA:</th>
                    <td>'.$fecha_liquidacion.'</td>
                  </tr>
                  <tr>
                    <th>ESTADO DEL CASO</th>
                    <td></td>
                    <th>FECHA ACTUALIZACION</th>
                    <td></td>
                    <th>GARANTIA / PLACA</th>
                    <td>'.$vehiculo_marca.' '.$vehiculo_modelo.' | '.$vehiculo_placa.'</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
          <!-- ----------------------------------------------------------- -->
        <div class="row mb-4">
          <!-- SECCION DEL CRONOGRAMA -->
          <div class="col-md-9">
            <div class="container-fluid shadow">
              <div class="table-responsive">
                <table id="" class="table table-striped table-bordered display" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>N°</th>
                      <th>FECHAS</th>
                      <th>ESTADO</th>
                      <th>CAPITAL</th>
                      <th>AMORTIZACION</th>
                      <th>INTERES</th>
                      <th>GPS</th>
                      <th>PRORRATEO</th>
                      <th>CUOTA</th>
                    </tr>
                  </thead>
                  <tbody id="">
                    '.$cronograma.'
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- --------------------------------->

          <!-- SECCION DEL DETALLE DEL CREDITO A LIQUIDAR-->
          <div class="col-md-3">
            <div class="container-fluid shadow">
              <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <tbody id="">
                  <tr>
                    <th>MONEDA</th>
                    <td> '.$simbolo_moneda_matriz.' </td>
                  </tr>
                  <tr>
                    <th>CAPITAL</th>
                    <td id="convlabelc1'.$conView.'" class="text-right"> <input type="text" id="txt_capital_'.$conView.'" data-contador="'.$conView.'" class="form-control input-sm text-right moneda txt_capital" onblur="calculateSumaInputs()" value="'.formato_numero($CAPITAL_RESTANTE_CAMBIO).'"/> </td>
                  </tr>
                  <tr>
                    <th>INTERES CUOTAS VENCIDAS</th>
                    <td id="convlabelc2'.$conView.'" class="text-right"> <input type="text" id="txt_cuotas_vencidas_'.$conView.'" data-contador="'.$conView.'" class="form-control input-sm text-right moneda txt_cuotas_vencidas" onblur="calculateSumaInputs()" value="'.formato_numero($INTERESES_CUOTAS_CAMBIO).'"/> </td>
                  </tr>
                  <tr>
                    <th>PRORRATEO</th>
                    <td id="convlabelc3'.$conView.'" class="text-right"> <input type="text" id="txt_prorrateo_'.$conView.'" data-contador="'.$conView.'" class="form-control input-sm text-right moneda txt_prorrateo" onblur="calculateSumaInputs()" value="'.formato_numero($PRORRATEO_TOTAL_CAMBIO).'"/> </td>
                  </tr>
                  <tr>
                    <th>GPS TOTAL</th>
                    <td id="convlabelc4'.$conView.'" class="text-right"> <input type="text" id="txt_gps_total_'.$conView.'" data-contador="'.$conView.'" class="form-control input-sm text-right moneda txt_gps_total" onblur="calculateSumaInputs()" value="'.formato_numero($GPS_TOTAL_CAMBIO).'"/> </td>
                  </tr>
                  <tr>
                    <th>GASTOS ADMINISTRATIVOS / LEGAL</th>
                    <td id="convlabelc5'.$conView.'" class="text-right"> <input type="text" id="txt_gastos_admin_'.$conView.'" data-contador="'.$conView.'" class="form-control input-sm text-right moneda txt_gastos_admin" onblur="calculateSumaInputs()" value="'.formato_numero($gastos_generados).'"/> </td>
                  </tr>
                  <tr style="height: 20px;"></tr>
                  <tr>
                    <th>SUB TOTAL</th>
                    <td class="text-right" id="subtotal_'.$conView.'">'.formato_numero($SUB_TOTAL_PAGAR + $gastos_generados).' </td>
                  </tr>
                  <tr style="height: 20px;"></tr>
                  <tr>
                    <td colspan="2"><button type="button" id="btnexppdf'.$conView.'" class="btn btn-danger" style="width: 100%;" onclick="exportar_liquidacion_formato('.$conView.')">Exportar a PDF</button></td>
                  </tr>
                </tbody>
              </table>
              <input type="hidden" id="hdd_subtotal_'.$conView.'" data-contador="'.$conView.'" class="form-control input-sm text-right moneda" value="'.formato_numero($SUB_TOTAL_PAGAR + $gastos_generados).'"/>
              
              <input type="hidden" id="hdd_subtotal_gastosAdmin" data-contador="'.$conView.'" class="form-control input-sm text-right moneda"/>
              <input type="hidden" id="hdd_subtotal_gpsTotal" data-contador="'.$conView.'" class="form-control input-sm text-right moneda"/>
              <input type="hidden" id="hdd_total_a_pagar" data-contador="'.$conView.'" class="form-control input-sm text-right moneda"/>
            </div>
          </div>
          <!-- --------------------------------->
        </div>
      </div>
    ';
    $TABLA_RESUMEN .= '
      <tr>
        <th>'.$titulo1.' <b>'.$codigo_credito.'</b></th>
        <td class="text-right" id="total_resumen_central_'.$conView.'">'.$SUB_TOTAL_PAGAR.'</td>
      </tr>
    ';
    
    $SUMA_GLOBAL_INCAUTACION += formato_numero($gastos_generados);
    $SUMA_GLOBAL_A_PAGAR += $GASTOS_INCAUTACION;
  } //FIN FOR

  $SUMA_GLOBAL_A_PAGAR += $SUMA_GLOBAL_INCAUTACION;

  $TABLA_RESUMEN .= '
    <tr>
      <th>GASTOS TOTAL ADMINISTRATIVOS</th>
      <td class="text-right" id="subtotalGastosAdminView">'.formato_numero($SUMA_GLOBAL_INCAUTACION).'</td>
    </tr>
    <tr>
      <th>GPS TOTAL</th>
      <td class="text-right" id="subtotalGPSTotalView">'.formato_numero($GPS_TOTAL_CAMBIO).'</td>
    </tr>
    <tr style="height: 20px;"></tr>
    <tr>
      <th>DESCUENTOS</th>
      <td class="text-right">0.00</td>
    </tr>
    <tr style="height: 20px;"></tr>
    <tr>
      <th>TOTAL A PAGAR</th>
      <td class="text-right" id="subtotalResumenView">'.formato_numero($SUMA_GLOBAL_A_PAGAR).'</td>
    </tr>
  ';
  // $AMORTIZACION_TOTAL_PREVIO = 0; //amortización previo
  // $mod_id = $creditotipo_id; //el modulo en INGRESO para amortizacion es 1 menor, 2 asve, 3 garve, 4 hipo
  // $modide = $credito_padre_id; //el modide en ingreso para amortización es el ID  del crédito

  // $result2 = $oIngreso->mostrar_por_modulo($creditotipo_id, $credito_padre_id, $credito_matriz_moneda_id, '1'); // parametro 1 ya que es el estado del ingreso 1 activo
  //   if($result2['estado'] == 1){
  //     foreach ($result2['data'] as $key => $value) {
  //       $AMORTIZACION_TOTAL_PREVIO += floatval($value['tb_ingreso_imp']);
  //     }
  //   }
  // $result2 = NULL;

  // $AMORTIZACION_TOTAL_PREVIO = formato_moneda($AMORTIZACION_TOTAL_PREVIO); //amortización previo a este crédito
  // $TOTAL_A_LIQUIDAR = formato_moneda($SUMA_GLOBAL_A_PAGAR);
  // $SUMA_GLOBAL_A_PAGAR = formato_moneda($SUMA_GLOBAL_A_PAGAR - $AMORTIZACION_TOTAL_PREVIO);
  // $TEXTO_GLOBAL .= '<b>SUMA TOTAL DE CAPITALES + INTERESES:</b> '.$simbolo_moneda_matriz.' '. mostrar_moneda($TOTAL_A_LIQUIDAR);
  // $prorrateo_total = 0;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_liquidacion_formato" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Refinanciar Crédito</h4>
      </div>

      <div class="modal-body">
        <div style="display: none;">
          <?php echo $TEXTO_RECORRIDO;?>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <input type="hidden" id="conView" name="conView" class="form-control input-sm" value="<?php echo $conView;?>">
          </div>
        </div>
        <div class="nav-tabs-custom shadow">
          <ul class="nav nav-tabs">
            <?php echo $CONTENT_HEADERS;?>
            <li class=""><a href="#tab_20" data-toggle="tab" aria-expanded="true">RESUMEN</a></li>
          </ul>

          <div class="tab-content" id="formato_liquidacion">
            <?php echo $CONTENT_TABS;?>

            <div id="tab_20" class="tab-pane">
              <div class="row">
                <div class="col-md-4"></div>
                <!-- SECCION DEL DETALLE DEL CREDITO A LIQUIDAR-->
                <div class="col-md-4">
                  <div class="container-fluid shadow">
                    <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <tbody id="">
                        <?php echo $TABLA_RESUMEN;?>
                        <tr style="height: 20px;"></tr>
                        <tr>
                          <td colspan="2"><button type="button" id="btnexppdf0" class="btn btn-danger" style="width: 100%;" onclick="exportar_liquidacion_formato(0)">Exportar a PDF</button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- --------------------------------->
                <div class="col-md-4">
                  <!-- SI LA VISTA ES GASTO | SE MUESTRAN LOS TRES VALORES A GUARDAR-->
                  <div class="container-fluid shadow">
                    <?php
                      $appenButtonMoments = '';
                      if ($_SESSION['usuariogrupo_id'] == 2 )
                      {
                        if( $vista == 'gasto'){
                          $appenButtonMoments = '<button type="button" id="btnGuardarPreparar" class="btn btn-info btn-block">Guardar datos de ejecución</button>';
                        }
                        elseif( $vista == 'stockunidad'){
                          $appenButtonMoments = '<button type="button" id="btnGuardarMomento02" class="btn btn-success btn-block" onclick="registra_preparar_segundo_momento()">Guardar datos de ejecución - al final de la venta</button>';
                        }
                        else{
                          $appenButtonMoments = '';
                        }
                      }
                      echo 
                      '
                      <table class="table table-striped table-bordered mb-4">
                        <thead>
                          <tr class="bg-info text-blue">
                            <th class="text-center" width="60%"> DESCRIPCI&Oacute;N </th>
                            <th class="text-center"> MONTO </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> CAPITAL </td>
                            <td class="text-right"> <input type="text" id="txt_credito_cap1" name="txt_credito_cap1" class="form-control text-right moneda" readonly/> </td>
                          </tr>
                          <tr>
                            <td> CUOTAS VENCIDAS </td>
                            <td class="text-right"> <input type="text" id="txt_credito_cuoven1" name="txt_credito_cuoven1" class="form-control text-right moneda" readonly/> </td>
                          </tr>
                          <tr>
                            <td> GASTOS </td>
                            <td class="text-right"> <input type="text" id="txt_credito_gasto1" name="txt_credito_gasto1" class="form-control text-right moneda" readonly/> </td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr class="bg-info text-blue">
                            <th> TOTAL </th>
                            <th class="text-right"> <span id="sumat_total_resumen_tree_inputs"></span> </th>
                          </tr>
                        </tfoot>
                      </table>
                      <input type="hidden" id="hdd_vehiculo_placa" name="hdd_vehiculo_placa" class="form-control" value="'.$vehiculo_placa.'" readonly/>
                      <input type="hidden" id="hdd_credito_id" name="hdd_credito_id" class="form-control" value="'.$_POST['credito_id'].'" readonly/>
                      '. $appenButtonMoments;
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/liquidacion/liquidacion_formato.js?ver=0612241801';?>"></script>