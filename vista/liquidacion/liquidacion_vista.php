<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<?php require_once('liquidacion_filtro.php');?>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="liquidacion_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>
				
				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">COMPORTAMIENTO DE PAGO DE LOS CLIENTES</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body liquidacion_tabla">
						<!-- AQUI VAN TODOS LOS TAPS QUE TENGA EL CLIENTE-->
					</div>
				</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
