function exportar_liquidacion_formato(id){
  if( id != 0 ){
    var valor01 = Number($("#txt_capital_"+id).autoNumeric("get"));
    var valor02 = Number($("#txt_cuotas_vencidas_"+id).autoNumeric("get"));
    var valor03 = Number($("#txt_prorrateo_"+id).autoNumeric("get"));
    var valor04 = Number($("#txt_gps_total_"+id).autoNumeric("get"));
    var valor05 = Number($("#txt_gastos_admin_"+id).autoNumeric("get"));
    $("#convlabelc1"+id).empty().html('<label>'+parseFloat(valor01).toFixed(2)+'</label>');
    $("#convlabelc2"+id).empty().html('<label>'+parseFloat(valor02).toFixed(2)+'</label>');
    $("#convlabelc3"+id).empty().html('<label>'+parseFloat(valor03).toFixed(2)+'</label>');
    $("#convlabelc4"+id).empty().html('<label>'+parseFloat(valor04).toFixed(2)+'</label>');
    $("#convlabelc5"+id).empty().html('<label>'+parseFloat(valor05).toFixed(2)+'</label>');

    let timerInterval;
    Swal.fire({
      title: "Mensaje del Sistema",
      html: "Preparando PDF",
      timer: 2000,
      timerProgressBar: true,
      didOpen: () => {
        $("#btnexppdf"+id).prop('disabled', true);
        Swal.showLoading();
        // const timer = Swal.getPopup().querySelector("b");
        timerInterval = setInterval(() => {
          // timer.textContent = `${Swal.getTimerLeft()}`;
        }, 100);
      },
      willClose: () => {
        clearInterval(timerInterval);
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        generarPDFLibrary();
      }
    });
  }
  else
  {
    $("#btnexppdf0").prop('disabled', true);
    generarPDFLibrary();
  }
}
  

function generarPDFLibrary(){
  window.jsPDF = window.jspdf.jsPDF;
  var doc = new jsPDF('l');
  
  // Source HTMLElement or a string containing HTML.
  var elementHTML = document.querySelector("#formato_liquidacion");
  // Ajusta el valor de ancho al 100% de la hoja en modo apaisado
  var pageWidth = doc.internal.pageSize.width;
  var windowHeight = elementHTML.clientHeight * (pageWidth / elementHTML.clientWidth);


  doc.html(elementHTML, {
      callback: function(doc) {
          // Save the PDF
          doc.save('document-html.pdf');
      },
      margin: [10, 10, 10, 10],
      autoPaging: 'text',
      x: 0,
      y: 0,
      width: 250, //target width in the PDF document
      windowWidth: 1200 //window width in CSS pixels
  });
}

$(document).ready(function() {

  console.log('Modal liquidacion 88');

  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999999.00",
  });

  $(".txt_capital").change( function() {
    var contador = $(this).data("contador");
    calcularSumarVertical(contador);
  });

  $(".txt_cuotas_vencidas").change( function() {
    var contador = $(this).data("contador");
    calcularSumarVertical(contador);
  });

  $(".txt_prorrateo").change( function() {
    var contador = $(this).data("contador");
    calcularSumarVertical(contador);
  });

  $(".txt_gps_total").change( function() {
    var contador = $(this).data("contador");
    calcularSumarVertical(contador);
  });

  $(".txt_gastos_admin").change( function() {
    var contador = $(this).data("contador");
    calcularSumarVertical(contador);
  });

  calcularSumarVertical(1);
  
  $("#btnGuardarPreparar").click(function() {
    registrar_preparar();
  });
});

function calcularSumarVertical(contador){
  var suma = 0;
  var capital  = Number($("#txt_capital_"+contador).autoNumeric("get"));
  // console.log('capital: '+capital);
  var cuotas_vencidas = Number($("#txt_cuotas_vencidas_"+contador).autoNumeric("get"));
  // console.log('cuotas_vencidas: '+cuotas_vencidas)
  var prorrateo = Number($("#txt_prorrateo_"+contador).autoNumeric("get"));
  // console.log('prorrateo: '+prorrateo);
  var gps_total = Number($("#txt_gps_total_"+contador).autoNumeric("get"));
  // console.log('gps_total: '+gps_total);
  var gastos_admin = Number($("#txt_gastos_admin_"+contador).autoNumeric("get"));
  // console.log('gastos_admin: '+gastos_admin);

  suma = capital + cuotas_vencidas + prorrateo + gps_total + gastos_admin;
  
  if (suma > 0) {
    $("#hdd_subtotal_"+contador).autoNumeric("set", suma.toFixed(2));
  } else {
    $("#hdd_subtotal_"+contador).autoNumeric("set", 0);
  }

  $("#subtotal_"+contador).empty().html($("#hdd_subtotal_"+contador).val());

  // -------------  steo de totales a laseccion resumen -  parte central

  $("#total_resumen_central_1").empty().html($("#hdd_subtotal_1").val());
  $("#total_resumen_central_"+contador).empty().html($("#hdd_subtotal_"+contador).val());
  
  sumaGastosAdministrativos();
  sumaGPSTotal();
  sumaTotalPagar();
  calculateSumaInputs();
}

function sumaGastosAdministrativos(){  
  var sumaTotal = 0;

  $(".txt_gastos_admin").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaTotal += 0;
    } else {
      sumaTotal += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  // console.log(sumaTotal)

  if (sumaTotal > 0) {
    $("#hdd_subtotal_gastosAdmin").autoNumeric("set", sumaTotal.toFixed(2));
  } else {
    $("#hdd_subtotal_gastosAdmin").autoNumeric("set", 0);
  }

  $("#subtotalGastosAdminView").empty().html($("#hdd_subtotal_gastosAdmin").val());
}

function sumaGPSTotal(){  
  var sumaTotal = 0;

  $(".txt_gps_total").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaTotal += 0;
    } else {
      sumaTotal += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  // console.log(sumaTotal)

  if (sumaTotal > 0) {
    $("#hdd_subtotal_gpsTotal").autoNumeric("set", sumaTotal.toFixed(2));
  } else {
    $("#hdd_subtotal_gpsTotal").autoNumeric("set", 0);
  }

  $("#subtotalGPSTotalView").empty().html($("#hdd_subtotal_gpsTotal").val());
}

function sumaTotalPagar(){
  var total_pagar = 0;
  var creditoMatriz  = Number($("#hdd_subtotal_1").autoNumeric("get"));
  // console.log(creditoMatriz);
  // var subtotal_gastosAdmin  = Number($("#hdd_subtotal_gastosAdmin").autoNumeric("get"));
  // console.log(subtotal_gastosAdmin);
  // var subtotal_gpsTotal  = Number($("#hdd_subtotal_gpsTotal").autoNumeric("get"));
  // console.log(subtotal_gpsTotal);
  // var subtotal_descuentos = 0;

  var getMontoAdendas = 0;
  var validarItems = parseInt($("#conView").val());

  for (var index = 2; index <= validarItems; index++) {
    getMontoAdendas += parseFloat(Number($("#hdd_subtotal_"+index).autoNumeric("get")));
  }

  total_pagar = creditoMatriz + getMontoAdendas;
  
  if (total_pagar > 0) {
    $("#hdd_total_a_pagar").autoNumeric("set", total_pagar.toFixed(2));
  } else {
    $("#hdd_total_a_pagar").autoNumeric("set", 0);
  }
  $("#subtotalResumenView").empty().html($("#hdd_total_a_pagar").val());
}

function calculateSumaInputs() {
  var sumaCapital = 0;
  var sumaInteresCuotaVencida = 0;
  var sumaProrrateo = 0;
  var sumaGPSTotal = 0;
  var sumaGastosAdmin = 0;
  var sumaTotalGastos = 0;
  var sumaTotalCuoVenProrra = 0;
  var sumaTotalResumenTreeInputs = 0;

  $(".txt_capital").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaCapital += 0;
    } else {
      sumaCapital += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  $(".txt_cuotas_vencidas").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaInteresCuotaVencida += 0;
    } else {
      sumaInteresCuotaVencida += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  $(".txt_prorrateo").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaProrrateo += 0;
    } else {
      sumaProrrateo += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  $(".txt_gps_total").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaGPSTotal += 0;
    } else {
      sumaGPSTotal += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  $(".txt_gastos_admin").each(function () {
    if (isNaN(parseFloat(Number($(this).autoNumeric("get"))))) {
      sumaGastosAdmin += 0;
    } else {
      sumaGastosAdmin += parseFloat(Number($(this).autoNumeric("get")));
    }
  });

  sumaTotalGastos = sumaGPSTotal  + sumaGastosAdmin;

  sumaTotalCuoVenProrra = sumaProrrateo + sumaInteresCuotaVencida;

  sumaTotalResumenTreeInputs = sumaTotalResumenTreeInputs + sumaCapital + sumaTotalCuoVenProrra +sumaTotalGastos;

  if (sumaCapital > 0) {
    $("#txt_credito_cap1").autoNumeric("set", sumaCapital.toFixed(2));
  } else {
    $("#txt_credito_cap1").autoNumeric("set", 0);
  }

  if (sumaTotalCuoVenProrra > 0) {
    $("#txt_credito_cuoven1").autoNumeric("set", sumaTotalCuoVenProrra.toFixed(2));
  } else {
    $("#txt_credito_cuoven1").autoNumeric("set", 0);
  }

  if (sumaTotalGastos > 0) {
    $("#txt_credito_gasto1").autoNumeric("set", sumaTotalGastos.toFixed(2));
  } else {
    $("#txt_credito_gasto1").autoNumeric("set", 0);
  }
  
  $("#sumat_total_resumen_tree_inputs").empty().html(sumaTotalResumenTreeInputs.toFixed(2));
  
}

function registrar_preparar() {
  var credito_id          = $("#hdd_credito_id").val()
  var tipo_id             = parseInt($("#hdd_tipo_id").val());
  var numero_placa        = $("#hdd_vehiculo_placa").val();
  var txt_credito_cuoven1 = Number($("#txt_credito_cuoven1").autoNumeric("get"));
  var txt_credito_cap1    = Number($("#txt_credito_cap1").autoNumeric("get"));
  var txt_credito_gasto1  = Number($("#txt_credito_gasto1").autoNumeric("get"));
  var vista               = "gasto";
  var moneda_credito_id   = $("#hdd_credito_moneda_id").val()

  console.log('Resumen cuotas vencidas:'+txt_credito_cuoven1);
  console.log('Resumen capital:'+txt_credito_cap1);
  console.log('Resumen gastos:'+txt_credito_gasto1);
  console.log('Resumen moneda:'+moneda_credito_id);

  $.confirm({
    title: "¡Advertencia!",
    content: "<span>¿Está seguro que desea guardar los datos?</span>",
    type: "blue",
    escapeKey: "close",
    backgroundDismiss: true,
    columnClass: "small",
    buttons: {
      Confirmar: {
        btnClass: "btn-blue",
        action: function () {
          $.ajax({
            type: "POST",
            url: VISTA_URL + "stockunidad/stockunidad_controller.php",
            async: true,
            dataType: "json",
            data: {
              action: "preparar_registrar",
              vista: vista,
              hdd_credito_id: credito_id,
              hdd_tipo_id: 3, //estamos en la carpeta de garveh, por ende es el 3
              hdd_numero_placa: numero_placa,
              txt_credito_cuoven1: txt_credito_cuoven1,
              txt_credito_cap1: txt_credito_cap1,
              txt_credito_gasto1: txt_credito_gasto1,
              txt_moneda_credito_id: moneda_credito_id
            },
            beforeSend: function () {
              // $('#stockunidad_mensaje').hide(300);
              // console.log("cagando...");
            },
            success: function (data) {
              console.log(data);

              if (parseInt(data.estado) > 0) {
                $("#modal_liquidacion_formato").modal("hide");
                notificacion_success(""+data.mensaje,5000);
              } else {
                alerta_warning("Advertencia", data.mensaje);
              }
            },
            complete: function (data) {
              // console.log(data);
            },
            error: function (data) {
              $("#stockunidad_mensaje_tbl").removeClass("callout-info").addClass("callout-danger");
              $("#stockunidad_mensaje_tbl").html("ALERTA DE ERROR: " + data.responseText);
              alerta_error("Advertencia", "" + data.responseText);
            },
          });
        },
      },
      cancelar: function () {},
    },
  });
}

// function datos_json() {
//   var arrayDetalle = new Array();
//   var getcontView = parseInt($("#contView").val());

//   arrayDetalle.splice(0, arrayDetalle.length); /*limpiar el array*/

//   var item    = 0;
//   var valores = "";
//   $("#tbl_resumen_1 tr").each(function () {
//     valores = $(this).find("td").eq(0).html() + ' | ';
//     item = item + 1;

//     var objDetalle = new Object();
//     objDetalle.item = item;
//     objDetalle.valores = valores;
//     arrayDetalle.push(objDetalle);
//   });

//   var jsonDetalle = JSON.stringify(arrayDetalle);

//   console.log(jsonDetalle);
// }