<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Liquidacion extends Conexion{

    function mostrar_cronograma_credito($credito_id, $creditotipo_id, $tabla_credito){
      try {
        $sql = "SELECT
          cuo.tb_cuota_id,
          tb_cuotadetalle_id,
          tb_cuota_num,
          tb_cuota_fec,
          cuo.tb_cuota_cap,
          cuo.tb_cuota_amo,
          cuo.tb_cuota_int,
          cuo.tb_cuota_pro,
          cuo.tb_cuota_cuo,
          cuo.tb_cuota_pregps,
          tb_cuota_est
        FROM ".$tabla_credito." cre
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id =:creditotipo_id)
        INNER JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id
        WHERE tb_cuota_xac = 1 AND cre.tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
