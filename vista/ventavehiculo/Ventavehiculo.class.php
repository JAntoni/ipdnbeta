<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class Ventavehiculo extends Conexion
{

  //estructurar correctamente
  public $ventavehiculo_id;
  public $ventavehiculo_xac;
  public $ventavehiculo_reg;
  public $ventavehiculo_fec;
  public $vehiculo_pla;
  public $moneda_id;
  public $montobase;
  public $montototal;
  public $tipcam;
  public $montosobr;
  public $metpag;
  public $usuario_id;
  public $usuario_id2;
  public $cliente;
  public $credito_id;
  public $creditotipo_id;
  public $vehiculo_est;
  public $empresa_id;
  public $stockunidad;
  public $stockunidad_estado;
  public $ventavehiculo_crenuevoid;

  function consultar_datos_venta_vehiculo($credito_id, $creditotipo_id)
    {
      try
      {
        $sql = "SELECT * FROM tb_ventavehiculo WHERE tb_credito_id = :credito_id AND tb_creditotipo_id =:creditotipo_id AND tb_ventavehiculo_xac = 1";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_ventavehiculo(
          tb_ventavehiculo_xac,
          tb_ventavehiculo_reg,
          tb_ventavehiculo_fec,
          tb_vehiculo_pla,
          tb_moneda_id,
          tb_ventavehiculo_bas,
          tb_ventavehiculo_mon,
          tb_ventavehiculo_tipcam,
          tb_ventavehiculo_sob,
          tb_ventavehiculo_metpag,
          tb_usuario_id,
          tb_usuario_id2,
          tb_cliente_id,
          tb_credito_id,
          tb_creditotipo_id,
          tb_ventavehiculo_est,
          tb_empresa_id,
          tb_stockunidad_id,
          tb_ventavehiculo_crenuevoid) 
          VALUES (
          :ventavehiculo_xac,
          now(),
          :ventavehiculo_fec,
          :vehiculo_pla,
          :moneda_id,
          :ventavehiculo_bas,
          :ventavehiculo_mon,
          :ventavehiculo_tipcam,
          :ventavehiculo_sob,
          :ventavehiculo_metpag,
          :usuario_id,
          :usuario_id2,
          :cliente_id,
          :credito_id,
          :creditotipo_id,
          :ventavehiculo_est,
          :empresa_id,
          :stockunidad_id,
          :ventavehiculo_crenuevoid)";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ventavehiculo_xac", $this->ventavehiculo_xac, PDO::PARAM_INT);
      $sentencia->bindParam(":ventavehiculo_fec", $this->ventavehiculo_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":vehiculo_pla", $this->vehiculo_pla, PDO::PARAM_STR);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ventavehiculo_bas", $this->montobase, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_mon", $this->montototal, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_tipcam", $this->tipcam, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_sob", $this->montosobr, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_metpag", $this->metpag, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_id2", $this->usuario_id2, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ventavehiculo_est", $this->vehiculo_est, PDO::PARAM_INT);
      $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":stockunidad_id", $this->stockunidad, PDO::PARAM_INT);
      $sentencia->bindParam(":ventavehiculo_crenuevoid", $this->ventavehiculo_crenuevoid, PDO::PARAM_INT);
      $result = $sentencia->execute();
      $ventavehiculo_id = $this->dblink->lastInsertId();

      $sql = "UPDATE tb_stockunidad SET tb_stockunidad_est = ? WHERE tb_stockunidad_id = ?";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(1,$this->stockunidad_estado, PDO::PARAM_INT);
      $sentencia->bindParam(2,$this->stockunidad, PDO::PARAM_INT);
      $sentencia->execute();

      $this->dblink->commit();
      $data['estado'] = $result;
      $data['ventavehiculo_id'] = $ventavehiculo_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar($ventavehiculo_id, $credito_id, $credito_vehpla, $ventavehiculo_soatval, $ventavehiculo_strval, $ventavehiculo_gpsval, $ventavehiculo_kil, $ventavehiculo_det, $creditotipo_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ventavehiculo SET 
          tb_credito_id =:credito_id, tb_credito_vehpla =:credito_vehpla, tb_ventavehiculo_soatval =:ventavehiculo_soatval, 
          tb_ventavehiculo_strval =:ventavehiculo_strval, tb_ventavehiculo_gpsval =:ventavehiculo_gpsval, 
          tb_ventavehiculo_kil =:ventavehiculo_kil, tb_ventavehiculo_det =:ventavehiculo_det, 
          tb_creditotipo_id =:creditotipo_id
          WHERE tb_ventavehiculo_id =:ventavehiculo_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ventavehiculo_id", $ventavehiculo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_soatval", $ventavehiculo_soatval, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_strval", $ventavehiculo_strval, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_gpsval", $ventavehiculo_gpsval, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_kil", $ventavehiculo_kil, PDO::PARAM_STR);
      $sentencia->bindParam(":ventavehiculo_det", $ventavehiculo_det, PDO::PARAM_STR);
      $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function eliminar($ventavehiculo_id){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ventavehiculo SET tb_ventavehiculo_xac = 0 WHERE tb_ventavehiculo_id =:ventavehiculo_id -- DELETE FROM tb_ventavehiculo WHERE tb_ventavehiculo_id =:ventavehiculo_id ";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ventavehiculo_id", $ventavehiculo_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function anularVenta_anular($stockunidad_id)
    {
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_ventavehiculo SET tb_ventavehiculo_xac = 0 WHERE tb_stockunidad_id =:stockunidad_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);
        $result = $sentencia->execute();

        $sql = "UPDATE tb_stockunidad SET tb_stockunidad_est = 1 WHERE tb_stockunidad_id =:stockunidad_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);
        $result = $sentencia->execute();

        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
  }

  function anularVenta_validar_exite_venta($vehiculo_pla,$stockunidad_id)
  {
    try
    {
      $sql = "SELECT * FROM tb_ventavehiculo WHERE tb_vehiculo_pla = ? AND tb_stockunidad_id = ?";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(1, $vehiculo_pla, PDO::PARAM_STR);
      $sentencia->bindParam(2, $stockunidad_id, PDO::PARAM_INT);
      $sentencia->execute();
      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"]  = true;
        $retorno["mensaje"] = "exito";
        $retorno["data"]    = $resultado;
        $sentencia->closeCursor();
      } else {
        $retorno["estado"]  = false;
        $retorno["mensaje"] = "No hay resultados";
        $retorno["data"]    = "";
      }
      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  
  function mostrarUnoVehiculoPlaca($vehiculo_pla)
  {
    try
    {
      $sql = "SELECT ven.*, cli.tb_cliente_id, cli.tb_cliente_nom FROM tb_ventavehiculo ven 
        INNER JOIN tb_cliente cli on cli.tb_cliente_id = ven.tb_cliente_id
        WHERE upper(tb_vehiculo_pla) = upper(:vehiculo_pla) AND tb_ventavehiculo_xac = 1 
        ORDER BY tb_ventavehiculo_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":vehiculo_pla", $vehiculo_pla, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay resultados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_datos_stock_placa($credito_id, $credito_vehpla)
  {
    try {
      $sql = "SELECT * FROM tb_ventavehiculo where tb_credito_vehpla =:credito_vehpla and tb_credito_id =:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_ventavehiculo_placa($credito_vehpla)
  {
    try {
      $sql = "SELECT * FROM tb_ventavehiculo where UPPER(tb_credito_vehpla) = UPPER(:credito_vehpla)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_fotos_credito($tabla, $credito_vehpla)
  {
    try {
      $tabla_file = $tabla . 'file';
      $sql = "SELECT cre.tb_credito_id, " . $tabla_file . "_url as img_url FROM $tabla cre inner join $tabla_file fil on fil.tb_credito_id = cre.tb_credito_id where tb_credito_vehpla =:credito_vehpla and tb_credito_xac = 1 limit 5";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function datos_credito_cliente($tabla, $cliente_id)
  {
    try {
      $tipo = 'tb_credito_tip1';
      if ($tabla == 'tb_creditogarveh')
        $tipo = 'tb_credito_tip';
      $sql = "SELECT * 
          FROM $tabla c
          INNER JOIN tb_cliente cli ON c.tb_cliente_id = cli.tb_cliente_id
          LEFT JOIN tb_vehiculomarca vm ON c.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
          LEFT JOIN tb_vehiculomodelo vd ON c.tb_vehiculomodelo_id=vd.tb_vehiculomodelo_id
          LEFT JOIN tb_vehiculoclase vc ON c.tb_vehiculoclase_id=vc.tb_vehiculoclase_id
          LEFT JOIN tb_vehiculotipo vt ON c.tb_vehiculotipo_id=vt.tb_vehiculotipo_id
          WHERE cli.tb_cliente_id = :cliente_id and $tipo in (1,4) and tb_credito_est in (3,4)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //estructura de select es diferente a las de update
  
  function Ultimaventa($vehpla){
      try {
        
        $sql = "SELECT * from tb_ventavehiculo where tb_ventavehiculo_xac=1 and tb_vehiculo_pla=:vehiculo_pla order by tb_ventavehiculo_fec DESC limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculo_pla", $vehpla, PDO::PARAM_STR);
        $sentencia->execute();
  
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay ventas asignadas a este vehiculo";
          $retorno["data"] = "";
        }
  
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function modificar_campo($venta_id, $venta_columna, $venta_valor, $param_tip) {
      $this->dblink->beginTransaction();
      try {
          if (!empty($venta_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

              $sql = "UPDATE tb_ventavehiculo SET " . $venta_columna . " =:ventavehiculo_valor WHERE tb_ventavehiculo_id =:ventavehiculo_id";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":ventavehiculo_id", $venta_id, PDO::PARAM_INT);
              if ($param_tip == 'INT') {
                  $sentencia->bindParam(":ventavehiculo_valor", $venta_valor, PDO::PARAM_INT);
              } else {
                  $sentencia->bindParam(":ventavehiculo_valor", $venta_valor, PDO::PARAM_STR);
              }

              $result = $sentencia->execute();

              $this->dblink->commit();

              return $result; //si es correcto el ingreso retorna 1
          } else
              return 0;
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
  }

}
