<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Pagoplanilla extends Conexion
{
  public $pagoplanilla_id;
  public $usuario_id;
  public $pagoplanilla_suelbruto;
  public $pagoplanilla_suelneto;
  public $pagoplanilla_bono;
  public $pagoplanilla_vacaciones;
  public $pagoplanilla_comisiones;
  public $pagoplanilla_descuentos;
  public $pagoplanilla_quinta;
  public $pagoplanilla_aportacion;
  public $pagoplanilla_montopagado;
  public $pagoplanilla_usureg;
  public $pagoplanilla_mes;
  public $pagoplanilla_anio;

  function insertar(){
    $this->dblink->beginTransaction();
    try {
      $columns = [
        'tb_usuario_id',
        'tb_pagoplanilla_suelbruto',
        'tb_pagoplanilla_suelneto',
        'tb_pagoplanilla_bono',
        'tb_pagoplanilla_vacaciones',
        'tb_pagoplanilla_comisiones',
        'tb_pagoplanilla_descuentos',
        'tb_pagoplanilla_quinta',
        'tb_pagoplanilla_aportacion',
        'tb_pagoplanilla_montopagado',
        'tb_pagoplanilla_usureg',
        'tb_pagoplanilla_mes',
        'tb_pagoplanilla_anio'
      ];

      $placeholders = implode(',', array_map(function ($column) {
        return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_pagoplanilla (" . implode(',', $columns) . ") VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_pagoplanilla_suelbruto", $this->pagoplanilla_suelbruto, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_suelneto", $this->pagoplanilla_suelneto, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_bono", $this->pagoplanilla_bono, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_vacaciones", $this->pagoplanilla_vacaciones, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_comisiones", $this->pagoplanilla_comisiones, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_descuentos", $this->pagoplanilla_descuentos, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_quinta", $this->pagoplanilla_quinta, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_aportacion", $this->pagoplanilla_aportacion, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_montopagado", $this->pagoplanilla_montopagado, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_usureg", $this->pagoplanilla_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_pagoplanilla_mes", $this->pagoplanilla_mes, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_anio", $this->pagoplanilla_anio, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result;

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_pagoplanilla SET 
            tb_pagoplanilla_suelbruto = :tb_pagoplanilla_suelbruto,
            tb_pagoplanilla_suelneto = :tb_pagoplanilla_suelneto,
            tb_pagoplanilla_bono = :tb_pagoplanilla_bono,
            tb_pagoplanilla_vacaciones = :tb_pagoplanilla_vacaciones,
            tb_pagoplanilla_comisiones = :tb_pagoplanilla_comisiones,
            tb_pagoplanilla_descuentos = :tb_pagoplanilla_descuentos,
            tb_pagoplanilla_quinta = :tb_pagoplanilla_quinta,
            tb_pagoplanilla_aportacion = :tb_pagoplanilla_aportacion,
            tb_pagoplanilla_montopagado = :tb_pagoplanilla_montopagado,
            tb_pagoplanilla_mes = :tb_pagoplanilla_mes,
            tb_pagoplanilla_anio = :tb_pagoplanilla_anio 
            WHERE tb_pagoplanilla_id = :tb_pagoplanilla_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_pagoplanilla_suelbruto", $this->pagoplanilla_suelbruto, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_suelneto", $this->pagoplanilla_suelneto, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_bono", $this->pagoplanilla_bono, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_vacaciones", $this->pagoplanilla_vacaciones, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_comisiones", $this->pagoplanilla_comisiones, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_descuentos", $this->pagoplanilla_descuentos, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_quinta", $this->pagoplanilla_quinta, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_aportacion", $this->pagoplanilla_aportacion, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_montopagado", $this->pagoplanilla_montopagado, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_pagoplanilla_mes", $this->pagoplanilla_mes, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_pagoplanilla_anio", $this->pagoplanilla_anio, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_pagoplanilla_id", $this->pagoplanilla_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result;

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function eliminar($tb_pagoplanilla_id)
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_pagoplanilla SET tb_pagoplanilla_xac = 0 WHERE tb_pagoplanilla_id = :tb_pagoplanilla_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_pagoplanilla_id", $tb_pagoplanilla_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result;

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function mostrarUno($tb_pagoplanilla_id)
  {
    try {
      $sql = "SELECT * FROM tb_pagoplanilla pa 
      INNER JOIN tb_usuario usu ON usu.tb_usuario_id = pa.tb_usuario_id 
      WHERE tb_pagoplanilla_id = :tb_pagoplanilla_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_pagoplanilla_id", $tb_pagoplanilla_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para liberar memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontró la planilla";
        $retorno["data"] = "";
      }

      return $retorno;

    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_pagoplanillas($usuario_id)
  {
    try {
      $filtro_usuario = '';
      if (intval($usuario_id) > 0) {
        $filtro_usuario = " AND pa.tb_usuario_id = ". intval($usuario_id);
      }
      
      $sql = "SELECT * FROM tb_pagoplanilla pa
      INNER JOIN tb_usuario usu ON usu.tb_usuario_id = pa.tb_usuario_id 
      WHERE tb_pagoplanilla_xac = 1 $filtro_usuario ORDER BY tb_pagoplanilla_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      //$sentencia->bindParam(":tb_usuario_id", $tb_usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para liberar memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontraron planillas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;

    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_campo($tb_pagoplanilla_id, $tb_pagoplanilla_columna, $tb_pagoplanilla_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($tb_pagoplanilla_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_pagoplanilla SET " . $tb_pagoplanilla_columna . " = :tb_pagoplanilla_valor WHERE tb_pagoplanilla_id = :tb_pagoplanilla_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_pagoplanilla_id", $tb_pagoplanilla_id, PDO::PARAM_INT);
        if ($param_tip == 'INT') {
          $sentencia->bindParam(":tb_pagoplanilla_valor", $tb_pagoplanilla_valor, PDO::PARAM_INT);
        } else {
          $sentencia->bindParam(":tb_pagoplanilla_valor", $tb_pagoplanilla_valor, PDO::PARAM_STR);
        }

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result;

      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  //juan 16-04-2024
  function promedio_sueldo_comisiones_usuario($usuario_id, $mes, $anio){
    try {
      $anio_mes = $anio.$mes;
      $sql = "SELECT ROUND(AVG(tb_pagoplanilla_suelbruto),2) AS promedio_sueldo, ROUND(AVG(tb_pagoplanilla_comisiones),2) AS promedio_comisiones
        FROM (
            SELECT tb_pagoplanilla_suelbruto, tb_pagoplanilla_comisiones
            FROM tb_pagoplanilla 
            WHERE tb_usuario_id =:usuario_id AND CAST(CONCAT(tb_pagoplanilla_anio, tb_pagoplanilla_mes) AS UNSIGNED) < :anio_mes
            ORDER BY CONCAT(tb_pagoplanilla_anio, tb_pagoplanilla_mes) DESC
            LIMIT 6
        ) AS subquery;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":anio_mes", $anio_mes, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para liberar memoria de la consulta
      }
      else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontró la planilla";
        $retorno["data"] = "";
      }

      return $retorno;

    } catch (Exception $e) {
      throw $e;
    }
  }
  
  // juan 07-08-2024
  function listar_pagoplanilla_periodo($usuario_id, $mes, $anio)
  {
    try {
      $sql = "SELECT * FROM tb_pagoplanilla WHERE tb_usuario_id =:usuario_id AND tb_pagoplanilla_mes =:mes AND tb_pagoplanilla_anio =:anio";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para liberar memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se encontraron planillas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;

    } catch (Exception $e) {
      throw $e;
    }
  }
}
?>