<?php
require_once('../../core/usuario_sesion.php');

require_once('../pagoplanilla/Pagoplanilla.class.php');
$oPagoplanilla = new Pagoplanilla();
require_once('../filestorage/Filestorage.class.php');
$oFile = new Filestorage();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$personal_usuario_id = intval($_POST['usuario_id']);
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

$tabla_pagos = '';
$result = $oPagoplanilla->listar_pagoplanillas($personal_usuario_id);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $descripcion = "Boleta de pago: ".$value['tb_usuario_nom'].', periodo: '.nombre_mes(intval($value['tb_pagoplanilla_mes']))." ".$value['tb_pagoplanilla_anio'];
      $filestorage_id = 0;
      $filestorage_url = '';
      $boton_boleta = '';
      $boton_subir = '<a class="btn btn-success btn-xs" onclick="filestorage_form('.$value['tb_pagoplanilla_id'].', \''.$descripcion.'\')"><i class="fa fa-cloud-upload"></i></a>';
      $boton_eliminar_boleta = '';

      $result2 = $oFile->listar_filestorages('pagoplanilla', $value['tb_pagoplanilla_id']);
        if($result2['estado'] == 1){
          foreach ($result2['data'] as $key2 => $value2) {
            $filestorage_id = $value2['filestorage_id'];
            $filestorage_url = $value2['filestorage_url'];
          }
          $boton_boleta = '<a class="btn btn-danger btn-xs" href="'.$filestorage_url.'" target="_blank"><i class="fa fa-file-pdf-o"></i> Boleta</a>';
          $boton_subir = ''; //? SI YA EXISTE UNA BOLETA SUBIDA YA NO SE VUELVE A SUBIR
          if($usuariogrupo_id == 2)
            $boton_eliminar_boleta = '<a class="btn btn-default btn-xs" onclick="filestorage_eliminar('.$filestorage_id.')"><i class="fa fa-trash"></i></a>';
        }
      $result2 = NULL;

      $tabla_pagos .= '
        <tr>
          <td>'.$value['tb_pagoplanilla_id'].'</td>
          <td>'.$value['tb_usuario_nom'].'</td>
          <td>'.$value['tb_pagoplanilla_suelbruto'].'</td>
          <td>'.$value['tb_pagoplanilla_suelneto'].'</td>
          <td>'.$value['tb_pagoplanilla_bono'].'</td>
          <td>'.$value['tb_pagoplanilla_vacaciones'].'</td>
          <td>'.$value['tb_pagoplanilla_comisiones'].'</td>
          <td>'.$value['tb_pagoplanilla_descuentos'].'</td>
          <td>'.$value['tb_pagoplanilla_quinta'].'</td>
          <td>'.$value['tb_pagoplanilla_aportacion'].'</td>
          <td>'.$value['tb_pagoplanilla_montopagado'].'</td>
          <td>'.nombre_mes(intval($value['tb_pagoplanilla_mes'])).'</td>
          <td>'.$value['tb_pagoplanilla_anio'].'</td>
          <td>'.$boton_boleta.'</td>
          <td>
            <a class="btn btn-warning btn-xs" onclick="pagoplanilla_form(\'M\', '.$value['tb_pagoplanilla_id'].')"><i class="fa fa-pencil"></i></a> 
            <a class="btn btn-danger btn-xs" onclick="pagoplanilla_form(\'E\', '.$value['tb_pagoplanilla_id'].')"><i class="fa fa-trash"></i></a> 
            '.$boton_subir.'
            '.$boton_eliminar_boleta.'
          </td>
        </tr>
      ';
    }
    
  }
$result = NULL;
?>
<table class="table table-hover" id="tbl_pagoplanilla" width="100%">
  <thead>
    <tr>
      <th>ID</th>
      <th>Asesor</th>
      <th class="no-sort">S/. Sueldo Bruto</th>
      <th class="no-sort">S/. Sueldo Neto</th>
      <th class="no-sort">S/. Bono</th>
      <th class="no-sort">S/. Vacaciones</th>
      <th class="no-sort">S/. Comisiones</th>
      <th class="no-sort">S/. Descuentos</th>
      <th class="no-sort">S/. Renta Quinta</th>
      <th class="no-sort">S/. Aportaciones</th>
      <th class="no-sort">S/. Monto Depositado</th>
      <th>Mes Periodo</th>
      <th>Año Periodo</th>
      <th class="no-sort">Documento</th>
      <th style="width: 8%;" class="no-sort">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tabla_pagos;?>
  </tbody>
</table>