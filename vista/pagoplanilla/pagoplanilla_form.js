$(document).ready(function () {
  console.log('aaa 333333')

  $('.selectpicker').selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: 'ES'
  });

  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '9999999.99'
  });

  $('#form_pagoplanilla').validate({
    submitHandler: function () {

      $.ajax({
        type: "POST",
        url: VISTA_URL + "pagoplanilla/pagoplanilla_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_pagoplanilla").serialize(),
        beforeSend: function () {
          
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {

            notificacion_success(data.mensaje, 6000);
            swal_success("CONTRATO", data.mensaje, 2500);
            $('#modal_registro_pagoplanilla').modal('hide');
            pagoplanilla_tabla();
          } 
          else {
            alerta_error('Error', data.mensaje);
            console.log(data)
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      pg_planilla_suelbruto: {
        required: true
      },
      pg_planilla_suelneto: {
        required: true
      },
      pg_planilla_bono: {
        required: true
      },
      pg_planilla_vacaciones: {
        required: true
      },
      pg_planilla_comisiones: {
        required: true
      },
      pg_planilla_descuentos: {
        required: true
      },
      pg_planilla_quinta: {
        required: true
      },
      pg_planilla_aportacion: {
        required: true
      },
      pg_planilla_montopagado: {
        required: true
      },
      pg_planilla_mes: {
        required: true
      },
      pg_planilla_anio: {
        required: true
      }
    },
    messages: {
      pg_planilla_suelbruto: {
        required: "Ingresa el sueldo bruto"
      },
      pg_planilla_suelneto: {
        required: "Ingresa el sueldo neto"
      },
      pg_planilla_bono: {
        required: "Ingresa monto de bonos"
      },
      pg_planilla_vacaciones: {
        required: "Ingresa monto de vacaciones"
      },
      pg_planilla_comisiones: {
        required: "Ingresa monto de comisiones"
      },
      pg_planilla_descuentos: {
        required: "Ingresa monto de descuentos"
      },
      pg_planilla_quinta: {
        required: "Ingresa Quinta Categoría"
      },
      pg_planilla_aportacion: {
        required: "Ingresa el monto de aportaciones"
      },
      pg_planilla_montopagado: {
        required: "Ingresa monto final depositado"
      },
      pg_planilla_mes: {
        required: "Selecciona el mes"
      },
      pg_planilla_anio: {
        required: "Selecciona el año"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });
});