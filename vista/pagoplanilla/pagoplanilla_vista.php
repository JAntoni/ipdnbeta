<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
    .label-filter{
      padding-left: 1%;
      padding-right: 1%;
      font-size: 12pt;
    }
    .select-filter{
      width: 10%;
    }
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<div class="panel panel-info">
					<div class="panel-body">
            <form id="form_fil_reportecreditomejorado" class="form-inline" role="form" method="POST">
              <button type="button" class="btn btn-info" onclick="pagoplanilla_form('I', 0)">Nuevo Pago Planilla</button>

              <label for="pg_usuario_id" class="control-label label-filter">Colaborador:</label>
              <div class="form-group select-filter">
                <select class="form-control form-control-sm selectpicker" id="pg_usuario_id" name="pg_usuario_id" data-live-search="true" data-max-options="1" data-size="12">
                  <?php require_once('vista/usuario/usuario_select.php');?>
                </select>
              </div>
              <button type="button" class="btn btn-info" onclick="pagoplanilla_tabla()"><i class="fa fa-search"></i> Buscar</button>
            </form>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="pagoplanilla_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>
				
				<div class="tabla_pagoplanilla">

				</div>

				<div id="div_registro_pagoplanilla">				
				</div>

        <div id="div_filestorage_form">

        </div>
			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>