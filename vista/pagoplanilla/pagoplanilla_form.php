<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../pagoplanilla/Pagoplanilla.class.php');
  $oPagoplanilla = new Pagoplanilla();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'pagoplanilla';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $pagoplanilla_id = $_POST['pagoplanilla_id'];
  $personal_usuario_id = $_POST['usuario_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Contrato del Personal';
  }
  elseif ($usuario_action == 'I') {
      $titulo = 'Registrar Nuevo Contrato';
  } elseif ($usuario_action == 'M') {
      $titulo = 'Editar Contrato del Personal';
  } elseif ($usuario_action == 'E') {
      $titulo = 'Eliminar Contrato del Personal';
  } else {
      $titulo = 'Acción de Usuario Desconocido';
  }

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cliente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'pagoplanilla'; $modulo_id = $pagoplanilla_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $bandera = 4;
          $mensaje =  'No tienes el permiso para la acción que deseas realizar: '.$action;
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cliente por su ID
    $pagoplanilla_tip = 0;

    if(intval($pagoplanilla_id) > 0){
      $result = $oPagoplanilla->mostrarUno($pagoplanilla_id);
        if($result['estado'] == 1){
          $usuario_id = $result['data']['tb_usuario_id'];
          $pagoplanilla_suelbruto = $result['data']['tb_pagoplanilla_suelbruto'];
          $pagoplanilla_suelneto = $result['data']['tb_pagoplanilla_suelneto'];
          $pagoplanilla_bono = $result['data']['tb_pagoplanilla_bono'];
          $pagoplanilla_vacaciones = $result['data']['tb_pagoplanilla_vacaciones'];
          $pagoplanilla_comisiones = $result['data']['tb_pagoplanilla_comisiones'];
          $pagoplanilla_descuentos = $result['data']['tb_pagoplanilla_descuentos'];
          $pagoplanilla_quinta = $result['data']['tb_pagoplanilla_quinta'];
          $pagoplanilla_aportacion = $result['data']['tb_pagoplanilla_aportacion'];
          $pagoplanilla_montopagado = $result['data']['tb_pagoplanilla_montopagado'];
          $pagoplanilla_usureg = $result['data']['tb_pagoplanilla_usureg'];
          $pagoplanilla_mes = $result['data']['tb_pagoplanilla_mes'];
          $pagoplanilla_anio = $result['data']['tb_pagoplanilla_anio'];

          $colaborador = $result['data']['tb_usuario_nom'];
        }
      $result = NULL;
    } 
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_pagoplanilla" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_pagoplanilla" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_pagoplanilla_id" value="<?php echo $pagoplanilla_id;?>">
          <input type="hidden" name="hdd_usuario_id" value="<?php echo $personal_usuario_id;?>">
          <input type="hidden" id="cliente_vista" name="cliente_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <div class="row">
              <?php if($action == 'modificar'):?>
                <div class="col-md-12">
                  <h5>Colaborador: <b><?php echo $colaborador;?></b></h5>
                  <input type="hidden" name="pg_usuario_id" id="pg_usuario_id" class="form-control input-sm moneda" value="<?php echo $usuario_id; ?>">
                </div>
              <?php endif;?>

              <?php if($action == 'insertar'):?>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="pg_usuario_id" class="control-label">Colaborador</label>
                    <select class="form-control form-control-sm selectpicker" id="pg_usuario_id" name="pg_usuario_id" data-live-search="true" data-max-options="1" data-size="12">
                      <?php require_once('../usuario/usuario_select.php');?>
                    </select>
                  </div>
                </div>
              <?php endif;?>
              <!-- Pagoplanilla Suelbruto -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_suelbruto" class="control-label">Sueldo Bruto</label>
                  <input type="text" name="pg_planilla_suelbruto" id="pg_planilla_suelbruto" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_suelbruto; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Suelneto -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_suelneto" class="control-label">Sueldo + Asignación Familiar - Vacaciones</label>
                  <input type="text" name="pg_planilla_suelneto" id="pg_planilla_suelneto" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_suelneto; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Bono -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_bono" class="control-label">Monto de Bonos</label>
                  <input type="text" name="pg_planilla_bono" id="pg_planilla_bono" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_bono; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Vacaciones -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_vacaciones" class="control-label">Monto de Vacaciones</label>
                  <input type="text" name="pg_planilla_vacaciones" id="pg_planilla_vacaciones" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_vacaciones; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Comisiones -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_comisiones" class="control-label">Monto de Comisiones</label>
                  <input type="text" name="pg_planilla_comisiones" id="pg_planilla_comisiones" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_comisiones; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Descuentos -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_descuentos" class="control-label">Monto Adelantos + Faltas + Tardanzas (Descuentos)</label>
                  <input type="text" name="pg_planilla_descuentos" id="pg_planilla_descuentos" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_descuentos; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Quinta -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_quinta" class="control-label">Renta Quinta Categoría</label>
                  <input type="text" name="pg_planilla_quinta" id="pg_planilla_quinta" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_quinta; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Aportacion -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_aportacion" class="control-label">Aportación AFP/ONP</label>
                  <input type="text" name="pg_planilla_aportacion" id="pg_planilla_aportacion" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_aportacion; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Monto Pagado -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_montopagado" class="control-label">Monto Neto Depositado</label>
                  <input type="text" name="pg_planilla_montopagado" id="pg_planilla_montopagado" class="form-control input-sm moneda" value="<?php echo $pagoplanilla_montopagado; ?>">
                </div>
              </div>

              <!-- Pagoplanilla Mes -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_mes" class="control-label">Mes del Periodo</label>
                  <select class="form-control input-sm" name="pg_planilla_mes" id="pg_planilla_mes">
                    <?php echo devuelve_option_nombre_meses($pagoplanilla_mes);?>
                  </select>
                </div>
              </div>

              <!-- Pagoplanilla Anio -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pg_planilla_anio" class="control-label">Año del Periodo</label>
                  <select class="form-control input-sm" name="pg_planilla_anio" id="pg_planilla_anio">
                    <?php echo devuelve_option_anios($pagoplanilla_anio, 4);?>
                  </select>
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Contrato?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cliente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>

          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_pagoplanilla">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/pagoplanilla/pagoplanilla_form.js?ver=1';?>"></script>