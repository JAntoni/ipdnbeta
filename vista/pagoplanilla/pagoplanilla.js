function pagoplanilla_form(usuario_act, pagoplanilla_id) {
  var usuario_id = $('#personal_usuario_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "pagoplanilla/pagoplanilla_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act,
      pagoplanilla_id: pagoplanilla_id,
      usuario_id: usuario_id
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_registro_pagoplanilla").html(data);
        $("#modal_registro_pagoplanilla").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_pagoplanilla"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_pagoplanilla", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        // modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        // //funcion js para agregar un ancho automatico al modal, al abrirlo
        // modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

function pagoplanilla_tabla() {
  var usuario_id = $('#pg_usuario_id').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pagoplanilla/pagoplanilla_tabla.php",
    async: true,
    dataType: "html",
    data: {
      usuario_id: usuario_id
    },
    beforeSend: function () {
    },
    success: function (html) {
      $('.tabla_pagoplanilla').html(html);
      estilos_datatable();
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

function estilos_datatable(){
  datatable_global = $('#tbl_pagoplanilla').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    "bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    scrollY: 700,
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
  });
}

function filestorage_form(pagoplanilla_id, descripcion){
  $.ajax({
      type: "POST",
      url: VISTA_URL + "filestorage/filestorage_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: "I", //insertar
        public_carpeta: 'pdf',
        modulo_nom: 'pagoplanilla',
        modulo_id: pagoplanilla_id,
        filestorage_uniq: '',
        filestorage_des: descripcion
      }),
      beforeSend: function () {
      },
      success: function (data) {
        if (data != 'sin_datos') {
          $('#div_filestorage_form').html(data);
          $('#modal_registro_filestorage').modal('show');
          modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function (data) {
      },
      error: function (data) {
        
      }
  });
}
function filestorage_eliminar(filestorage_id){
  Swal.fire({
    title: '¿DESEA ELIMINAR LA BOLETA DE PAGO?',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class ="fa fa-home"></i> Confirmo!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> No',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "filestorage/filestorage_controller.php",
        async: true,
        dataType: "json",
        data: ({
          action: 'eliminar',
          hdd_filestorage_id: filestorage_id,
        }),
        beforeSend: function () {
        },
        success: function (data) {
          if(parseInt(data.estado) == 1){
            notificacion_success(data.mensaje, 4000)
            pagoplanilla_tabla();
          }
          else
            alerta_error('IMPORTANTE', data.mensaje)
        },
        complete: function (data) {
        },
        error: function (data) {
        }
      });
    } else if (result.isDenied) {
      
    }
  });

}
$(document).ready(function () {
  console.log("Mejorado corre 4444 ");

  pagoplanilla_tabla();

  $("#datetimepicker1, #datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $("#txt_deudadiaria_fec1").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $("#txt_deudadiaria_fec2").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
  });

  $("#personal_usuario_id").change(function () {
    pagoplanilla_perfil();
  });
});