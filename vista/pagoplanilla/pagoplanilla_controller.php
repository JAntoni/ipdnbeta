<?php
require_once('../../core/usuario_sesion.php');

require_once('../pagoplanilla/Pagoplanilla.class.php');
$oPagoplanilla = new Pagoplanilla();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];

$oPagoplanilla->pagoplanilla_id = intval($_POST['hdd_pagoplanilla_id']); 
$oPagoplanilla->usuario_id = intval($_POST['pg_usuario_id']);
$oPagoplanilla->pagoplanilla_suelbruto = moneda_mysql($_POST['pg_planilla_suelbruto']);
$oPagoplanilla->pagoplanilla_suelneto = moneda_mysql($_POST['pg_planilla_suelneto']);
$oPagoplanilla->pagoplanilla_bono = moneda_mysql($_POST['pg_planilla_bono']);
$oPagoplanilla->pagoplanilla_vacaciones = moneda_mysql($_POST['pg_planilla_vacaciones']);
$oPagoplanilla->pagoplanilla_comisiones = moneda_mysql($_POST['pg_planilla_comisiones']);
$oPagoplanilla->pagoplanilla_descuentos = moneda_mysql($_POST['pg_planilla_descuentos']);
$oPagoplanilla->pagoplanilla_quinta = moneda_mysql($_POST['pg_planilla_quinta']);
$oPagoplanilla->pagoplanilla_aportacion = moneda_mysql($_POST['pg_planilla_aportacion']);
$oPagoplanilla->pagoplanilla_montopagado = moneda_mysql($_POST['pg_planilla_montopagado']);
$oPagoplanilla->pagoplanilla_usureg = intval($_SESSION['usuario_id']);
$oPagoplanilla->pagoplanilla_mes = $_POST['pg_planilla_mes'];
$oPagoplanilla->pagoplanilla_anio = $_POST['pg_planilla_anio'];
  
if ($action == 'insertar') {
  $data['estado'] = 0;
  $data['mensaje'] = 'NO SE HA REGISTRADO EL PAGO DE PLANILLA, REVISAR CON SISTEMAS, NO INGRESÓ A NINGUN IG DE LA CONSULTA '.$_POST['pg_planilla_suelbruto'];

  //* ANTES DE REGISTRAR LOS DATOS DEPAGO, VEAMOS SI EXISTE YA UN REGISTRO PARA EL PERIODO REALIZADO
  $result = $oPagoplanilla->listar_pagoplanilla_periodo(intval($_POST['pg_usuario_id']), $_POST['pg_planilla_mes'], $_POST['pg_planilla_anio']);
    if($result['estado'] == 0){
      $oPagoplanilla->insertar();

      $data['estado'] = 1;
      $data['mensaje'] = 'El contrato ha sido registrado correctamente / '.$_POST['pg_planilla_suelbruto'];
    }
    else{
      $data['estado'] = 0;
      $data['mensaje'] = 'EL PAGO DE PLANILLA YA EXISTE PARA EL PERIODO SELECCIONADO '.$_POST['pg_planilla_suelbruto'];
    }
  $result = NULL;

  echo json_encode($data);
}
elseif ($action == 'modificar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El pago planilla ha sido Modificado correctamente';

  $oPagoplanilla->modificar();

  echo json_encode($data);
}
elseif ($action == 'eliminar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El contrato ha sido ELIMINADO correctamente';

  $oPagoplanilla->eliminar($oPagoplanilla->pagoplanilla_id);

  echo json_encode($data);
}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
?>