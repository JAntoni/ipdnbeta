<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Meta extends Conexion{

    function insertar($meta_nom, $meta_tip, $creditotipo_id, $meta_fec1, $meta_fec2, $meta_can, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_meta(tb_meta_nom, tb_meta_tip, tb_creditotipo_id, tb_meta_fec1, tb_meta_fec2, tb_meta_can, tb_usuario_id)
          VALUES (:meta_nom, :meta_tip, :creditotipo_id, :meta_fec1, :meta_fec2, :meta_can, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":meta_nom", $meta_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":meta_tip", $meta_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":meta_fec1", $meta_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":meta_fec2", $meta_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":meta_can", $meta_can, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($meta_id, $meta_nom, $meta_tip, $creditotipo_id, $meta_fec1, $meta_fec2, $meta_can, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_meta SET tb_meta_nom =:meta_nom, tb_meta_tip =:meta_tip, tb_creditotipo_id =:creditotipo_id, tb_meta_fec1 =:meta_fec1, tb_meta_fec2 =:meta_fec2, tb_meta_can =:meta_can, tb_usuario_id =:usuario_id WHERE tb_meta_id =:meta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":meta_id", $meta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":meta_nom", $meta_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":meta_tip", $meta_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":meta_fec1", $meta_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":meta_fec2", $meta_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":meta_can", $meta_can, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($meta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_meta WHERE tb_meta_id =:meta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":meta_id", $meta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($meta_id){
      try {
        $sql = "SELECT * FROM tb_meta me left join tb_creditotipo cretip on cretip.tb_creditotipo_id = me.tb_creditotipo_id left join tb_usuario usu on usu.tb_usuario_id = me.tb_usuario_id WHERE tb_meta_id =:meta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":meta_id", $meta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_metas($meta_mes, $meta_anio){
      try {
        $meta_mes = intval($meta_mes);
        $meta_anio = intval($meta_anio);

        $where_mes = '';
        $where_anio = '';
        if($meta_mes > 0)
          $where_mes = ' AND MONTH(tb_meta_fec1) =:meta_mes';
        if($meta_anio > 0)
          $where_anio = ' AND YEAR(tb_meta_fec1) =:meta_anio';

        $sql = "SELECT * FROM tb_meta me left join tb_creditotipo cretip on cretip.tb_creditotipo_id = me.tb_creditotipo_id left join tb_usuario usu on usu.tb_usuario_id = me.tb_usuario_id WHERE 1=1".$where_mes.''.$where_anio;

        $sentencia = $this->dblink->prepare($sql);
        if($meta_mes > 0)
          $sentencia->bindParam(":meta_mes", $meta_mes, PDO::PARAM_INT);
        if($meta_anio > 0)
          $sentencia->bindParam(":meta_anio", $meta_anio, PDO::PARAM_INT);
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
