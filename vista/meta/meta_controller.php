<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../meta/Meta.class.php');
  $oMeta = new Meta();
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$meta_nom = mb_strtoupper($_POST['txt_meta_nom'], 'UTF-8');
		$meta_tip =	intval($_POST['cmb_meta_tip']);
		$creditotipo_id =	intval($_POST['cmb_creditotipo_id']);
		$meta_fec1 =	fecha_mysql($_POST['txt_meta_fec1']);
		$meta_fec2 =	fecha_mysql($_POST['txt_meta_fec2']);
		$meta_can =	moneda_mysql($_POST['txt_meta_can']);
		$usuario_id = intval($_POST['cmb_usuario_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Meta.';
 		if($oMeta->insertar($meta_nom, $meta_tip, $creditotipo_id, $meta_fec1, $meta_fec2, $meta_can, $usuario_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Meta registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$meta_id = intval($_POST['hdd_meta_id']);
 		$meta_nom = mb_strtoupper($_POST['txt_meta_nom'], 'UTF-8');
 		$meta_tip =	intval($_POST['cmb_meta_tip']);
		$creditotipo_id =	intval($_POST['cmb_creditotipo_id']);
		$meta_fec1 =	fecha_mysql($_POST['txt_meta_fec1']);
		$meta_fec2 =	fecha_mysql($_POST['txt_meta_fec2']);
		$meta_can =	moneda_mysql($_POST['txt_meta_can']);
		$usuario_id = intval($_POST['cmb_usuario_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Meta.';

 		if($oMeta->modificar($meta_id, $meta_nom, $meta_tip, $creditotipo_id, $meta_fec1, $meta_fec2, $meta_can, $usuario_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Meta modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$meta_id = intval($_POST['hdd_meta_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Meta.';

 		if($oMeta->eliminar($meta_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Meta eliminada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>