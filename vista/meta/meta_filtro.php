<?php
	require_once(VISTA_URL.'funciones/funciones.php');
?>
<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-info">
		  <div class="panel-body">
		    <form id="form_perfilmenu_filtro" class="form-inline" role="form">
          <div class="form-group">
            <label for="cmb_meta_mes">Mes: </label>
            <select id="cmb_meta_mes" name="cmb_meta_mes" class="form-control input-sm">
              <?php 
              	$valor_mes = date('m');
              	echo devuelve_option_nombre_meses($valor_mes);
              ?>
            </select>                               
          </div>
          <div class="form-group">
            <label for="cmb_meta_anio">Año: </label>
            <select id="cmb_meta_anio" name="cmb_meta_anio" class="form-control input-sm">
              <?php 
              	$valor_anio = date('Y');
              	$formato = 4;
              	echo devuelve_option_anios($valor_anio, $formato);
              ?>
            </select>                               
          </div>
          <div class="form-group">
            <button class="btn btn-info btn-sm" onclick="meta_tabla()" type="button"><i class="fa fa-search"></i> Ver</button>                              
          </div>
        </form>
		  </div>
		</div>
	</div>
</div>