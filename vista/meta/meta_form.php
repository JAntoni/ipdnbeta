<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../meta/Meta.class.php');
  $oMeta = new Meta();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'meta';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $meta_id = $_POST['meta_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Meta Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Meta';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Meta';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Meta';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en meta
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'meta'; $modulo_id = $meta_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del meta por su ID
    if(intval($meta_id) > 0){
      $result = $oMeta->mostrarUno($meta_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el meta seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $meta_nom = $result['data']['tb_meta_nom'];
          $meta_tip = $result['data']['tb_meta_tip'];
          $creditotipo_id = $result['data']['tb_creditotipo_id'];
          $meta_fec1 = mostrar_fecha($result['data']['tb_meta_fec1']);
          $meta_fec2 = mostrar_fecha($result['data']['tb_meta_fec2']);
          $meta_can = $result['data']['tb_meta_can'];
          $usuario_id = $result['data']['tb_usuario_id'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_meta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_meta" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_meta_id" value="<?php echo $meta_id;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_meta_nom" class="control-label">Nombre de Meta</label>
              <input type="text" name="txt_meta_nom" id="txt_meta_nom" class="form-control input-sm mayus" value="<?php echo $meta_nom;?>">
            </div>
            <div class="form-group">
              <label for="cmb_meta_tip" class="control-label">Tipo de Meta</label>
              <select name="cmb_meta_tip" id="cmb_meta_tip" class="form-control input-sm">
                <option value=""></option>
                <option value="1" <?php if($meta_tip==1) echo 'selected';?>>Referidos</option>
                <option value="2" <?php if($meta_tip==2) echo 'selected';?>>Visitas</option>
                <option value="3" <?php if($meta_tip==3) echo 'selected';?>>Cierres</option>
              </select>
            </div>
            <div class="form-group" id="group_creditotipo" style="display: none;">
              <label for="cmb_creditotipo_id" class="control-label">Tipo de Crédito</label>
              <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm">
                <?php require_once('../creditotipo/creditotipo_select.php');?>
              </select>
            </div>
            <div class="form-group">
              <label for="txt_meta_fec1" class="control-label">Fecha Inicio</label>
              <div class='input-group date' id='datetimepicker1'>
                <input type='text' class="form-control" name="txt_meta_fec1" id="txt_meta_fec1" value="<?php echo $meta_fec1;?>"/>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="txt_meta_fec2" class="control-label">Fecha Fin</label>
              <div class='input-group date' id='datetimepicker2'>
                <input type='text' class="form-control" name="txt_meta_fec2" id="txt_meta_fec2" value="<?php echo $meta_fec2;?>"/>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="txt_meta_can" class="control-label">Cantidad Esperado</label>
              <input type="text" name="txt_meta_can" id="txt_meta_can" class="form-control input-sm" value="<?php echo $meta_can;?>">
            </div>
            <div class="form-group">
              <label for="cmb_usuario_id" class="control-label">Personal Encargado</label>
              <select name="cmb_usuario_id" id="cmb_usuario_id" class="form-control input-sm">
                <?php 
                  $usuario_columna = 'tb_usuario_mos';
                  $usuario_valor = 1; //1 personal en oficinas, 2 personal de almacen, 0 personal externo
                  $param_tip = 'INT';
                  require_once('../usuario/usuario_select.php');
                ?>
              </select>
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Meta?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="meta_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_meta">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_meta">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_meta">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/meta/meta_form.js';?>"></script>
