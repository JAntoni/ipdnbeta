
$(document).ready(function(){
	$('#txt_meta_can').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999.00'
	});
	$('#datetimepicker1, #datetimepicker2').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		startDate: "-0d"
		//endDate : new Date()
	});

	$("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_meta_fec1').val();
   	$('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_meta_fec2').val();
   	$('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  
  $('#cmb_meta_tip').change(function(event) {
  	$('#cmb_creditotipo_id').val(0);
  	
  	if($(this).val() == 3)
  		$('#group_creditotipo').show(300);
  	else
  		$('#group_creditotipo').hide(300);
  });

  $('#form_meta').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"meta/meta_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_meta").serialize(),
				beforeSend: function() {
					$('#meta_mensaje').show(400);
					$('#btn_guardar_meta').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#meta_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#meta_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		meta_tabla();
		      		$('#modal_registro_meta').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#meta_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#meta_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_meta').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#meta_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#meta_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_meta_nom: {
				required: true,
				minlength: 2
			},
			cmb_meta_tip: {
				required: true,
				min: 1
			},
			cmb_creditotipo_id: {
				min: function(e){
					if($('#cmb_meta_tip').val() == 3)
						return 1;
					else
						return '';
				}
			},
			txt_meta_fec1: {
				required: true
			},
			txt_meta_fec2: {
				required: true
			},
			txt_meta_can: {
				required: true
			}
		},
		messages: {
			txt_meta_nom: {
				required: "Ingrese un nombre para el meta",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			cmb_meta_tip: {
				required: "Seleccione el tipo de Meta por favor",
				min: "Seleccione el tipo de Meta por favor"
			},
			cmb_creditotipo_id: {
				min: "Seleccione el tipo de Crédito por favor"
			},
			txt_meta_fec1: {
				required: "Ingrese una fecha de Inicio"
			},
			txt_meta_fec2: {
				required: "Ingrese una fecha de Fin"
			},
			txt_meta_can: {
				required: "Ingre una cantidad por favor"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
