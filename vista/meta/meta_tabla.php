<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'meta/Meta.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../meta/Meta.class.php');
    require_once('../funciones/fechas.php');
  }
  $oMeta = new Meta();

  $meta_mes = (empty($meta_mes))? $_POST['meta_mes'] : $meta_mes;
  $meta_anio = (empty($meta_anio))? $_POST['meta_anio'] : $meta_anio;

?>
<table id="tbl_meta" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Meta</th> 
      <th>Tipo de Meta</th>
      <th>Tipo Crédito</th>
      <th>Fecha Inicio</th>
      <th>Fecha Fin</th>
      <th>Cantidad</th>
      <th>Encargado</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oMeta->listar_metas($meta_mes, $meta_anio);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_meta_id'];?></td>
            <td><?php echo $value['tb_meta_nom'];?></td>
            <td>
              <?php 
                if(intval($value['tb_meta_tip']) == 1)
                  echo '<span class="badge bg-yellow">Referidos</span>';
                elseif(intval($value['tb_meta_tip']) == 2)
                  echo '<span class="badge bg-blue">Visitas</span>';
                elseif(intval($value['tb_meta_tip']) == 3)
                  echo '<span class="badge bg-green">Cierres</span>';
                else
                  echo 'SIN TIPO';
              ?>            
            </td>
            <td><?php echo $value['tb_creditotipo_nom'];?></td>
            <td><?php echo mostrar_fecha($value['tb_meta_fec1']); ?></td>
            <td><?php echo mostrar_fecha($value['tb_meta_fec2']); ?></td>
            <td><?php echo $value['tb_meta_can']; ?></td>
            <td>
              <?php 
                if(intval($value['tb_usuario_id']) == 0)
                  echo '<b>Para Todos</b>';
                else
                  echo $value['tb_usuario_nom'];
              ?>
            </td>
            <td align="center">
              <a class="btn btn-warning btn-xs" title="Editar" onclick="meta_form(<?php echo "'M', ".$value['tb_meta_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="meta_form(<?php echo "'E', ".$value['tb_meta_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
