<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Vehiculomodelo.class.php');
  $oVehiculomodelo = new Vehiculomodelo();

$result = $oVehiculomodelo->listar_vehiculomodelos(0);

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_vehiculomodelo_id'].'</td>
          <td>'.$value['tb_vehiculomarca_nom'].'</td>
          <td>'.$value['tb_vehiculomodelo_nom'].'</td>
          <td>'.$value['tb_vehiculomodelo_des'].'</td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="vehiculomodelo_form(\'M\','.$value['tb_vehiculomodelo_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="vehiculomodelo_form(\'E\','.$value['tb_vehiculomodelo_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
            <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
            
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_vehiculomodelos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Marca Vehículo</th>
      <th>Modelo Vehículo</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
