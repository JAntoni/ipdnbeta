
$(document).ready(function(){
  $('#form_vehiculomodelo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculomodelo/vehiculomodelo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculomodelo").serialize(),
				beforeSend: function() {
					$('#vehiculomodelo_mensaje').show(400);
					$('#btn_guardar_vehiculomodelo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculomodelo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculomodelo_mensaje').html(data.mensaje);
                        $('#modal_registro_vehiculomodelo').modal('hide');
                        var modulo = $("#hdd_modulo").val();
                        if(modulo = 'creditogarveh'){
                            cargar_vehiculomarca(data.marca_id);
                            cargar_vehiculomodelo(data.modelo_id);
                        }
                        if(modulo != 'creditogarveh'){
		      	setTimeout(function(){ 
		      		vehiculomodelo_tabla();
		      		 }, 1000
		      	);
                        }
					}
					else{
		      	$('#vehiculomodelo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculomodelo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculomodelo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculomodelo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculomodelo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_vehiculomodelo_nom: {
				required: true,
				minlength: 1
			},
			txt_vehiculomodelo_des: {
				required: true,
				minlength: 1
			},
			cmb_vehiculomarca_id: {
				required: true,
				min: 1
			}
		},
		messages: {
			txt_vehiculomodelo_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_vehiculomodelo_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			},
			cmb_vehiculomarca_id: {
				required: "Seleccione una Marca",
				min: "Elija una Marca por favor"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
