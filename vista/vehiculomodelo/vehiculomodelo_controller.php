<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculomodelo/Vehiculomodelo.class.php');
  $oVehiculomodelo = new Vehiculomodelo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$vehiculomodelo_nom = mb_strtoupper($_POST['txt_vehiculomodelo_nom'], 'UTF-8');
 		$vehiculomodelo_des = $_POST['txt_vehiculomodelo_des'];
 		$vehiculomarca_id = $_POST['cmb_vehiculomarca_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Modelo de Vehículo.';
                $result = $oVehiculomodelo->insertar($vehiculomodelo_nom, $vehiculomodelo_des, $vehiculomarca_id);
 		if($result['estado']){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Modelo de Vehículo registrado correctamente.';
                        $data['modelo_id'] = $result['vehiculomodelo_id'];
                        $data['marca_id'] = $result['vehiculomarca_id'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$vehiculomodelo_id = intval($_POST['hdd_vehiculomodelo_id']);
 		$vehiculomodelo_nom = mb_strtoupper($_POST['txt_vehiculomodelo_nom'], 'UTF-8');
 		$vehiculomodelo_des = $_POST['txt_vehiculomodelo_des'];
 		$vehiculomarca_id = $_POST['cmb_vehiculomarca_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Modelo de Vehículo.';

 		if($oVehiculomodelo->modificar($vehiculomodelo_id, $vehiculomodelo_nom, $vehiculomodelo_des, $vehiculomarca_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Modelo de Vehículo modificado correctamente. ';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculomodelo_id = intval($_POST['hdd_vehiculomodelo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Modelo de Vehículo.';

 		if($oVehiculomodelo->eliminar($vehiculomodelo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Modelo de Vehículo eliminado correctamente. '.$vehiculomodelo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>