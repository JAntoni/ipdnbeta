<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculomodelo extends Conexion{

    function insertar($vehiculomodelo_nom, $vehiculomodelo_des, $vehiculomarca_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_vehiculomodelo(tb_vehiculomodelo_xac, tb_vehiculomodelo_nom, tb_vehiculomodelo_des, tb_vehiculomarca_id)
          VALUES (1, :vehiculomodelo_nom, :vehiculomodelo_des, :vehiculomarca_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomodelo_nom", $vehiculomodelo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculomodelo_des", $vehiculomodelo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculomarca_id", $vehiculomarca_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $vehiculomodelo_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        
        $data['estado'] = $result;
        $data['vehiculomodelo_id'] = $vehiculomodelo_id;
        $data['vehiculomarca_id'] = $vehiculomarca_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($vehiculomodelo_id, $vehiculomodelo_nom, $vehiculomodelo_des, $vehiculomarca_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculomodelo SET tb_vehiculomodelo_nom =:vehiculomodelo_nom, tb_vehiculomodelo_des =:vehiculomodelo_des, tb_vehiculomarca_id =:vehiculomarca_id WHERE tb_vehiculomodelo_id =:vehiculomodelo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomodelo_id", $vehiculomodelo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculomodelo_nom", $vehiculomodelo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculomodelo_des", $vehiculomodelo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculomarca_id", $vehiculomarca_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculomodelo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_vehiculomodelo WHERE tb_vehiculomodelo_id =:vehiculomodelo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomodelo_id", $vehiculomodelo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculomodelo_id){
      try {
        $sql = "SELECT * FROM tb_vehiculomodelo vehiculomodelo INNER JOIN tb_vehiculomarca vehiculomarca ON vehiculomodelo.tb_vehiculomarca_id = vehiculomarca.tb_vehiculomarca_id WHERE tb_vehiculomodelo_id =:vehiculomodelo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomodelo_id", $vehiculomodelo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculomodelos($vehiculomarca_id){
        $concatenar = '';
        if(intval($vehiculomarca_id)>0){
            $concatenar = ' WHERE vmo.tb_vehiculomarca_id =:vehiculomarca_id';
        }
      try {
        $sql = "SELECT * FROM tb_vehiculomodelo vmo INNER JOIN tb_vehiculomarca vma  
                ON vmo.tb_vehiculomarca_id = vma.tb_vehiculomarca_id".$concatenar;

        $sentencia = $this->dblink->prepare($sql);
        if(intval($vehiculomarca_id)>0){
            $sentencia->bindParam(":vehiculomarca_id", $vehiculomarca_id, PDO::PARAM_INT);
        }
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
