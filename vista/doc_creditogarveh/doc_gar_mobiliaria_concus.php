<?php
require_once('../../vendor/autoload.php');
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\Font;
use PhpOffice\PhpWord\Style\Paragraph;

require_once ("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();
require_once ("../cuentadeposito/Cuentadeposito.class.php");
$oCuentadeposito = new Cuentadeposito();
require_once ("../zona/Zona.class.php");
$oZona = new Zona();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once ("../transferente/Transferente.class.php");
$oTransferente = new Transferente();
require_once ("../externo/Externo.class.php");
$oExterno = new Externo();
require_once ("../cheque/Cheque.class.php");
$oCheque = new Cheque();
require_once("../creditoinicial/Creditoinicial.class.php");
$oCreditoinicial = new Creditoinicial();
require_once ("../representante/Representante.class.php");
$oRepresentante = new Representante();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once("../funciones/operaciones.php");

$tipo_documento = $_GET['d2']; //solo puede ser WORD Y PDF
$credito_id = intval($_GET['d1']);

if(!isset($_GET['d2'])){
  echo 'NO HAS SELECCIONADO SI SERÁ WORD O PDF '.$_GET['d2'].' / '.$tipo_documento; exit();
}

if($tipo_documento == 'pdf')
  require_once('../../static/tcpdf/tcpdf.php');

$title = 'GARANTÍA MOBILIARIA CON CUSTODIA';
$codigo = 'GARMOB-'.str_pad($credito_id, 4, "0", STR_PAD_LEFT);

$result = $oCredito->mostrarUno($credito_id);
  if($result['estado'] == 1){
    $cuotatipo_id = $result['data']['tb_cuotatipo_id']; //? 1,2,3,4 -> los créditos garveh usan los tipos 3 y 4, 3: Libre y 4 Cuota Fija
    $cuotasubperiodo_id = $result['data']['tb_cuotasubperiodo_id']; //? 1 mensual, 2 quincenal, 3 semanal
    $moneda_id = $result['data']['tb_moneda_id'];
    $representante_id = $result['data']['tb_representante_id'];
    $credito_tc = $result['data']['tb_credito_tc'];
    $credito_preaco = $result['data']['tb_credito_preaco'];
    $credito_int = $result['data']['tb_credito_int'];
    $credito_numcuo = $result['data']['tb_credito_numcuo'];
    $credito_linapr = $result['data']['tb_credito_linapr'];
    $credito_numcuomax = $result['data']['tb_credito_numcuomax'];
    $credito_ini = $result['data']['tb_credito_ini'];
    $credito_feccre = mostrar_fecha($result['data']['tb_credito_feccre']);
    $credito_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
    $credito_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
    $credito_fecpro = mostrar_fecha($result['data']['tb_credito_fecpro']);
    $credito_fecent = mostrar_fecha($result['data']['tb_credito_fecent']);
    $credito_obs = trim($result['data']['tb_credito_obs']);
    $credito_est = $result['data']['tb_credito_est'];
    $credito_cargas = trim($result['data']['tb_credito_carg']); //si tuviera cargas el vehículo
    $credito_fecgps = mostrar_fecha($result['data']['tb_credito_fecgps']);
    $credito_fecstr = mostrar_fecha($result['data']['tb_credito_fecstr']);
    $credito_fecsoat = mostrar_fecha($result['data']['tb_credito_fecsoat']);
    $credito_fecvenimp = mostrar_fecha($result['data']['tb_credito_fecvenimp']); //fecha de vencimiento de impuesto vehicular
    $credito_tip = $result['data']['tb_credito_tip']; // tipo del credito 1 es credito normal, 2 es una adenda, 3 ACUERDO PAGO, 4 garantía MINUTA

    $credito_numero_cuo = $credito_numcuo;
    if($cuotatipo_id == 3)
      $credito_numero_cuo = $credito_numcuomax;

    $cliente_id = $result['data']['tb_cliente_id'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_ape = $result['data']['tb_cliente_ape'];
    $cliente_dir = $result['data']['tb_cliente_dir'];
    $cliente_ema = $result['data']['tb_cliente_ema'];
    $cliente_fecnac = $result['data']['tb_cliente_fecnac'];
    $cliente_tel = $result['data']['tb_cliente_tel'];
    $cliente_cel = $result['data']['tb_cliente_cel'];
    $cliente_telref = $result['data']['tb_cliente_telref'];
    $cliente_ubige = $result['data']['tb_ubigeo_cod']; //ubigeo d<b>EL CLIENTE</b>
    $cliente_emp_ubige = $result['data']['tb_ubigeo_cod3']; //ubigeo 3 es para la direccion de la empresa

    //cliente persona juridica
    $cliente_tip = $result['data']['tb_cliente_tip'];
    $cliente_empruc = $result['data']['tb_cliente_empruc']; //ruc de la empresa, persona juridica
    $cliente_emprs = $result['data']['tb_cliente_emprs']; //razon social
    $cliente_empger = $result['data']['tb_cliente_empger']; //tipo de gerente General o Titular
    $cliente_empdir = $result['data']['tb_cliente_empdir']; //dirección de la empresa
    //----------
    $cliente_estciv = $result['data']['tb_cliente_estciv']; // 1 soltero, 2 casado
    $cliente_bien = $result['data']['tb_cliente_bien']; // 1 separacion de bienes, 2 sin separacion
    $cliente_firm = $result['data']['tb_cliente_firm']; // 1 firma solo, 2 firma con conyuge
    $cliente_zon_id = $result['data']['tb_zona_id']; // id de la zona registral donde está el N° de partida
    $cliente_numpar = $result['data']['tb_cliente_numpar']; //numero de partida d<b>EL CLIENTE</b>

    $vehiculo_tipus = $result['data']['tb_credito_tipus']; //tipo de uso 1 taxi, 2 uso particular
    $vehiculo_pre = $result['data']['tb_credito_vehpre'];
    $vehmar_id = $result['data']['tb_vehiculomarca_id'];
    $vehmod_id = $result['data']['tb_vehiculomodelo_id'];
    $vehcla_id = $result['data']['tb_vehiculoclase_id'];
    $vehtip_id = $result['data']['tb_vehiculotipo_id'];
    $vehiculo_pla = $result['data']['tb_credito_vehpla'];
    $vehiculo_mot = $result['data']['tb_credito_vehsermot'];
    $vehiculo_cha = $result['data']['tb_credito_vehsercha'];
    $vehiculo_ano = $result['data']['tb_credito_vehano'];
    $vehiculo_col = $result['data']['tb_credito_vehcol'];
    $vehiculo_numpas = $result['data']['tb_credito_vehnumpas'];
    $vehiculo_numasi = $result['data']['tb_credito_vehnumasi'];
    $vehiculo_kil = $result['data']['tb_credito_vehkil'];
    $vehiculo_zona_id = $result['data']['tb_zonaregistral_id'];

    $gps_pre = formato_numero($result['data']['tb_credito_gpspre']);
    $str_pre = formato_numero($result['data']['tb_credito_strpre']);
    $soa_pre = formato_numero($result['data']['tb_credito_soapre']);
    $gas_pre = formato_numero($result['data']['tb_credito_gaspre']);
    $otr_pre = formato_numero($result['data']['tb_credito_otrpre']);

    $vehiculo_est = $result['data']['tb_credito_vehest'];
    $vehiculo_ent = $result['data']['tb_credito_vehent'];
    $vehiculo_parele = $result['data']['tb_credito_parele'];
    $vehiculo_solreg = $result['data']['tb_credito_solreg'];
    $vehiculo_fecsol = mostrar_fecha($result['data']['tb_credito_fecsol']);

    //vehiculo
    $vehiculo_pre = formato_numero($result['data']['tb_credito_vehpre']);
    $vehiculo_pre = $vehiculo_pre + $gps_pre + $str_pre + $soa_pre + $gas_pre + $otr_pre;

    $vehmar_id = $result['data']['tb_vehiculomarca_id'];
    $vehmar_nom = $result['data']['tb_vehiculomarca_nom'];

    $vehmod_id = $result['data']['tb_vehiculomodelo_id'];
    $vehmod_nom = $result['data']['tb_vehiculomodelo_nom'];

    $vehcla_id = $result['data']['tb_vehiculoclase_id'];
    $vehcla_nom = $result['data']['tb_vehiculoclase_nom'];

    $vehtip_id = $result['data']['tb_vehiculotipo_id'];
    $vehtip_nom = $result['data']['tb_vehiculotipo_nom'];

    $vehiculo_pla = $result['data']['tb_credito_vehpla'];
    $vehiculo_mot = $result['data']['tb_credito_vehsermot'];
    $vehiculo_cha = $result['data']['tb_credito_vehsercha'];
    $vehiculo_ano = $result['data']['tb_credito_vehano'];
    $vehiculo_col = $result['data']['tb_credito_vehcol'];
    $vehiculo_numpas = $result['data']['tb_credito_vehnumpas'];
    $vehiculo_numasi = $result['data']['tb_credito_vehnumasi'];
    $vehiculo_kil = $result['data']['tb_credito_vehkil'];

    $vehiculo_mode = $result['data']['tb_credito_vehmode'];
    $vehiculo_cate = $result['data']['tb_credito_vehcate'];
    $vehiculo_comb = $result['data']['tb_credito_vehcomb'];
    $credito_valtas = $result['data']['tb_credito_valtas'];
    $credito_valrea = $result['data']['tb_credito_valrea'];
    $credito_valgra = $result['data']['tb_credito_valgra'];

    $credito_subgar = $result['data']['tb_credito_subgar'];
    $credito_ini = formato_numero($result['data']['tb_credito_ini']);
    $credito_tipini = intval($result['data']['tb_credito_tipini']); //1 tabla de ingreso, 2 tabla de deposito
    $credito_idini = intval($result['data']['tb_credito_idini']);
    $transferente_id = intval($result['data']['tb_transferente_id']);
    $credito_monedache = intval($result['data']['tb_credito_monedache']);
  }
$result = NULL;

$result = $oRepresentante->mostrarUno($representante_id);
  if($result['estado'] == 1){
    $representante_nom = mb_strtoupper($result['data']['tb_representante_nom'], 'UTF-8');
    $representante_estciv = mb_strtoupper($result['data']['tb_representante_estciv'], 'UTF-8');
    $representante_dni = $result['data']['tb_representante_dni'];
    $representante_dir = mb_strtoupper($result['data']['tb_representante_dir'], 'UTF-8'); //dirección
    $representante_den = mb_strtoupper($result['data']['tb_representante_den'], 'UTF-8'); // denominacion: Gerente, SUb, etc
  }
$result = NULL;
// FIN REPRESENTANTE

list($credito_dia,$credito_mes,$credito_anio) = explode('-', $credito_feccre);

if($moneda_id == 1){
  $moneda = 'S/.';
}else{
  $moneda = 'US$';
}

//ubigeo del cliente
if($cliente_ubige  >0){
  $result = $oUbigeo->mostrarUbigeo($cliente_ubige);
    if($result['estado'] == 1){
      $cliente_dep = $result['data']['Departamento'];
      $cliente_pro = $result['data']['Provincia'];
      $cliente_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

if($cliente_emp_ubige > 0){
  $result = $oUbigeo->mostrarUbigeo($cliente_emp_ubige);
    if($result['estado'] == 1){
      $empresa_dep = $result['data']['Departamento'];
      $empresa_pro = $result['data']['Provincia'];
      $empresa_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

//obtenemos información del cliente, sus nombres, su esposa(o), copropietario o apoderado
$estado_civil = 'SOLTERO(A)';
if($cliente_estciv == 2) $estado_civil = 'CASADO(A)';
if($cliente_estciv == 3) $estado_civil = 'DIVORCIADO(A)';
if($cliente_estciv == 4) $estado_civil = 'VIUDO(A)';

$esposa = ''; $apoderado = '';
$clientes = '<b>el(la) Sr(res) '.$cliente_nom.'</b>, identificado con DNI N° <b>'.$cliente_doc.'</b>, estado civil '.$estado_civil.' con domicilio en <b>'.trim($cliente_dir).'</b>, del Distrito de <b>'.$cliente_dis.'</b>, Provincia de <b>'.$cliente_pro.'</b>, del Departamento de <b>'.$cliente_dep.'</b>';

//si el cliente es casado
if($cliente_estciv == 2){
  $esposa = devolver_datos_esposa_apoderado($cliente_id, $cliente_bien, $cliente_firm, 1); //parametro 1 CONYUGAL
  $clientes = $clientes.' '.$esposa;
}

$apoderado = devolver_datos_esposa_apoderado($cliente_id, $cliente_bien, $cliente_firm, 3); //parametro 3 apoderado

if($apoderado != '')
  $clientes = $clientes.', '.$apoderado;

//verificar si el cliente tiene co-propietarios
$copro = devolver_datos_copropietarios($cliente_id);

if($copro != '')
  $clientes .= ' '.$copro;

$clientes = str_replace(' ;', ';', $clientes);

//LOS DATOS DEL VENDEDOR CAMBIAN CUANDO EL CLIENTE ES PERSONA JURÍDICA
if(intval($cliente_tip) == 2){
  $result = $oZona->mostrarUno($cliente_zon_id);
    if($result['estado'] == 1){
      $cliente_zon_nom = trim($result['data']['tb_zona_nom']);
    }
  $result = NULL;

  $clientes = '<b> '.$cliente_emprs.'</b>, identificada con R.U.C <b>'.$cliente_empruc.'</b>, con domicilio en <b>'.$cliente_empdir.'</b>, del Distrito de <b>'.$empresa_dis.'</b>, Provincia de <b>'.$empresa_pro.'</b>, del Departamento de <b>'.$empresa_dep.'</b>; debidamente representada por su '.$cliente_empger.' el/la <b>Sr(a). '.$cliente_nom.'</b>, identificado con D.N.I. <b>'.$cliente_doc.'</b> según  poderes  inscritos  en  la  Partida  Registral  N° <b>'.$cliente_numpar.'</b> del  Registro  de  Personas Jurídicas  de  la '.$cliente_zon_nom.', con domicilio en <b>'.trim($cliente_dir).'</b>, del Distrito de <b>'.$cliente_dis.'</b>, Provincia de <b>'.$cliente_pro.'</b>, del Departamento de <b>'.$cliente_dep.'</b>';
}

//información de la zona registral
if(intval($vehiculo_zona_id) <= 0)
  $vehiculo_zona_id = 1;

$result = $oZona->mostrarUno($vehiculo_zona_id);
  if($result['estado'] == 1)
    $zona_nom = strtoupper(trim($result['data']['tb_zona_nom']));
$result = NULL;

//detalle de las cuotas del credito nuevo, es decir de la ADENDA 2 ya que es un credito ASIVEH
$result = $oCuotadetalle->mostrar_una_cuotadetalle_por_creditoid($credito_id, 3);
  if($result['estado'] == 1) {
    $cuotadetalle_fec = mostrar_fecha($result['data']['tb_cuotadetalle_fec']);
    $cuotadetalle_cuo = $result['data']['tb_cuotadetalle_cuo'];
  }
$result = NULL;

//CRONOGRAMA SEA FIJA O LIBRE
$cuota_detalle = 'FALTA CRONOGRAMA, PARA ELLO DEBE ESTAR DESEMBOLSADO EL CRÉDITO';
$interes_total = 0;
//* ESTA ES UNA TABLA DE CRONOGRAMA FIJA
if($cuotatipo_id == 4){
  $cuota_detalle = '
    <table cellpadding="2" style="width: 100%; border: 6px #000000 solid;">
      <tr>
        <th align="center" style="background-color: green; color: white;">Fecha de Pago</th>
        <th align="center" width="10%" style="background-color: green; color: white;">N° de cuota</th>
        <th align="center" style="background-color: green; color: white;">Capital al inicio de periodo</th>
        <th align="center" style="background-color: green; color: white;">Amortización</th>
        <th align="center" style="background-color: green; color: white;">Interés Compensatorio</th>
        <th align="center" style="background-color: green; color: white;">IGV</th>
        <th align="center" width="14%" style="background-color: green; color: white;">CUOTA</th>
      </tr>';

  $result = $oCuotadetalle->mostrar_cuotasdetalle_por_creditoid($credito_id, 3);
    if($result['estado'] == 1){
      $num = 1;
      foreach ($result['data'] as $key => $value){

        $interes_igv = formato_numero($value['tb_cuota_int']) - (formato_numero($value['tb_cuota_int']) / 1.18);
        $interes_sin_igv = $value['tb_cuota_int'] - $interes_igv;
        $interes_total += formato_numero($value['tb_cuota_int']);
        
        $cuota_detalle .='
          <tr>
            <td align="center">'.mostrar_fecha($value['tb_cuotadetalle_fec']).'</td>
            <td align="center">'.$num.'</td>
            <td align="center">'.$moneda.mostrar_moneda($value['tb_cuota_cap']).'</td>
            <td align="center">'.$moneda.mostrar_moneda($value['tb_cuota_amo']).'</td>
            <td align="center">'.$moneda_cuo.mostrar_moneda($interes_sin_igv).'</td>
            <td align="center">'.$moneda.mostrar_moneda($interes_igv).'</td>
            <td align="right">'.$moneda.mostrar_moneda($value['tb_cuotadetalle_cuo']).'</td>
          </tr>';
          $num++;
      }
    }
  $result = NULL;

  $cuota_detalle .='</table>';
  $total_pagar_credito = formato_numero($credito_preaco + $interes_total);

  $cuota_detalle .= '
  	<p></p>
    <p align="justify">7. DETALLE:</p>
    <table cellpadding="2" style="width: 100%; border: 6px #000000 solid;">
      <tr>
        <th align="center" style="background-color: green; color: white;">N° de Cuotas</th>
        <th align="center" style="background-color: green; color: white;">Capital</th>
        <th align="center" style="background-color: green; color: white;">Interés Total</th>
        <th align="center" style="background-color: green; color: white;">Total a Pagar en '.$credito_numero_cuo.' meses</th>
      </tr>
      <tr>
        <td align="center">'.$credito_numero_cuo.'</td>
        <td align="center">'.$moneda.mostrar_moneda($credito_preaco).'</td>
        <td align="center">'.$moneda.mostrar_moneda($interes_total).'</td>
        <td align="center">'.$moneda.mostrar_moneda($total_pagar_credito).'</td>
      </tr>
    </table>
    <p></p>
  ';
}
//* EN CASO SELECCIONEN UN CRONOGRAMA DE TIPO LIBRE, LA CUAL NO ESTÁ IMPLEMENTADA
if($cuotatipo_id == 3 && $credito_fecdes){
  $cuota_detalle = '
    <table cellpadding="2" border="1">
      <tr style="background-color:green; color: white;">
        <th align="center">NO TENEMOS UNA IMPLEMENTACIÓN DE CRONOGRAMA LIBRE</th>
      </tr>
    </table>';
}

$moneda_cheque = 'S/.';
if(intval($credito_monedache) == 2)
  $moneda_cheque = 'US$';
// VAMOS A REALIZAR EL CAMBIO DE MONERA EN CASO EL CRÉDITO SEA EN UN TIPO DE MONEDA Y LUEGO EL CHEQUE SEA EN OTRO TIPO
$texto_garantizado = '<b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>';
if(intval($credito_monedache) != intval($moneda_id))
  $texto_garantizado = '<b>'.$moneda_cheque.' '.mostrar_moneda($credito_montoche).' ('.numtoletras($credito_montoche, $credito_monedache).')</b> AL TIPO DE CAMBIO S/. <b>'.$credito_tc_2.'</b> EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>';

/* COPIAAAAR DESDE AQUÍ*/
list($cuota_dia, $cuota_mes, $cuota_anio) = explode('-', $cuotadetalle_fec);
$clausula_adicional = '';
$anexo_adicional = $texto_garantizado;
$iniciales = '';
//OBTENER INFORMACIÓN DE LOS CHEQUES PARA LA MINUTA
$cheque = resaltar_texto('INGRESE REGISTRO DE CHEQUE O CHEQUES');

$result = $oCheque->mostrar_credito_id($credito_id, 3); //3 por el tipo de criedo GARVEH
  $num_cheques = count($result['data']);
  if($num_cheques == 1){
    $cheque_tip = intval($result['data'][0]['tb_cheque_tip']); //1 cheque de gerencia, 2 deposito bancario
    $cheque_tipcam = floatval($result['data'][0]['tb_cheque_tipcam']);
    $cheque_fec = mostrar_fecha($result['data'][0]['tb_cheque_fec']);
    $cheque_moneda_id = intval($result['data'][0]['tb_moneda_id']);
    $cheque_mon = floatval($result['data'][0]['tb_cheque_mon']);
    $cheque_num = $result['data'][0]['tb_cheque_num'];
    $cheque_ban = $result['data'][0]['tb_cheque_ban'];
    $equivalente = '';

    $texto_moneda_che = 'S/.'; 
    if($cheque_moneda_id == 2)
      $texto_moneda_che = 'US$'; 

    if(intval($moneda_id) == 1 && intval($cheque_moneda_id) == 2){
      $monto_equi = $cheque_mon * $cheque_tipcam;
      $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>';
    }
    if(intval($moneda_id) == 2 && intval($cheque_moneda_id) == 1){
      $monto_equi = $cheque_mon / $cheque_tipcam;
      $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>';
    }

    if($cheque_tip == 1){
      $cheque = '01 CHEQUE BANCARIO POR LA SUMA DE <b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA <b>'.$cheque_fec.'</b>, CON N° <b>'.$cheque_num.'</b> Y EN BANCO <b>'.$cheque_ban.'</b>'.$equivalente;
    }
    if($cheque_tip == 2){
      $cheque = '01 DEPÓSITO BANCARIO POR LA SUMA DE <b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA <b>'.$cheque_fec.'</b>, CON N° <b>'.$cheque_num.'</b> Y EN BANCO <b>'.$cheque_ban.'</b>'.$equivalente;
    }
  }

  if($num_cheques > 1){
    $cheque = str_pad($num_cheques, 2, "0", STR_PAD_LEFT).' CHEQUES BANCARIOS POR LA SUMA DE: ';

    foreach ($result['data'] as $key => $value) {
      $cheque_tip = intval($value['tb_cheque_tip']); //1 cheque de gerencia, 2 deposito bancario
      $cheque_tipcam = formato_numero($value['tb_cheque_tipcam']);
      $cheque_fec = mostrar_fecha($value['tb_cheque_fec']);
      $cheque_moneda_id = intval($value['tb_moneda_id']);
      $cheque_mon = formato_numero($value['tb_cheque_mon']);
      $cheque_num = $value['tb_cheque_num'];
      $cheque_ban = $value['tb_cheque_ban'];
      $equivalente = '';

      $texto_moneda_che = 'S/.'; 
      if($cheque_moneda_id == 2)
        $texto_moneda_che = 'US$'; 

      if(intval($moneda_id) == 1 && intval($cheque_moneda_id) == 2){
        $monto_equi = $cheque_mon * $cheque_tipcam;
        $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>';
      }
      if(intval($moneda_id) == 2 && intval($cheque_moneda_id) == 1){
        $monto_equi = $cheque_mon / $cheque_tipcam;
        $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>';
      }

      $cheque .= '<b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA <b>'.$cheque_fec.'</b>, CON N° <b>'.$cheque_num.'</b> Y EN BANCO <b>'.$cheque_ban.'</b>'.$equivalente.', ';
    }
    $cheque = trim($cheque, ', ');
  }
$result = NULL;

if($credito_subgar == 'GARMOB COMPRA TERCERO'){

  $result = $oTransferente->mostrarUno($transferente_id);
    if($result['estado'] == 1){
      $transferente_doc = $result['data']['tb_transferente_doc'];
      $transferente_nom = $result['data']['tb_transferente_nom'];
    }
  $result = NULL;

  $anexo_adicional = ' ENTREGADOS MEDIANTE '.$cheque.'; A NOMBRE DEL TRANSFERENTE '.$transferente_nom.' FINANCIADOS CON LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE SAC, UTILIZADOS PARA LA ADQUISICIÓN DEL VEHICULO MATERIA DE LA PRESENTE GARANTIA MOBILIARIA.';

  $result = $oCreditoinicial->listar_creditoiniciales($credito_id, 3); //3 por el tipo de criedo GARVEH
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $creditoinicial_tipini = intval($value['tb_creditoinicial_tipini']); //1 efectivo, 2 deposito, 3 cuenta externo (efec, depo)
        $creditoinicial_idini = intval($value['tb_creditoinicial_idini']); //tb_ingreso, tb_deposito, tb_externo
        
        if($creditoinicial_tipini == 1){ //ENTREGADO A NUESTRA CAJA EN EFECTIVO
          $result2 = $oIngreso->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $ingreso_fec = mostrar_fecha($result2['data']['tb_ingreso_fec']);
              $ingreso_det = $result2['data']['tb_ingreso_det'];
              $ingreso_imp = $result2['data']['tb_ingreso_imp'];

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $moneda_id).')</b> EN LA FECHA DE <b>'.$ingreso_fec.'</b> ENTREGADO AL TRANSFERENTE EN EFECTIVO, ';
            }
          $result2 = NULL;
        }
        if($creditoinicial_tipini == 2){ //ENTREGADO A NUESTRA CAJA EN DEPOSITO
          $result2 = $oDeposito->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $deposito_fec = mostrar_fecha($result2['data']['tb_deposito_fec']);
              $deposito_num = $result2['data']['tb_deposito_num'];
              $deposito_mon = $result2['data']['tb_deposito_mon'];

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($deposito_mon).' ('.numtoletras($deposito_mon, $moneda_id).')</b> DEPOSITADOS EN CUENTA BANCARIA DEL TRANSFERENTE CON FECHA <b>'.$deposito_fec.'</b>, N° DE OPERACION <b>'.$deposito_num.'</b>, EN BANCO DE CREDITO DEL PERU, ';
            }
          $result2 = NULL;
        }
        if($creditoinicial_tipini == 3){ //ENTREGADO AL TRANSFERENTE, TERCERO
          $result2 = $oExterno->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $externo_tip = $result2['data']['tb_externo_tip']; // 1 efectivo a transferente, 2 deposito a transferente
              $externo_moneda = $result2['data']['tb_moneda_id']; //1 soles, 3 dolares
              $externo_tipcam = floatval($result2['data']['tb_externo_tipcam']);
              $externo_fec = mostrar_fecha($result2['data']['tb_externo_fec']);
              $externo_mon = floatval($result2['data']['tb_externo_mon']);              
              $externo_num = $result2['data']['tb_externo_num'];
              $externo_ban = $result2['data']['tb_externo_ban'];
              $equivalente = '';
              $texto_moneda = 'S/.';
              if(intval($externo_moneda) == 2)
                $texto_moneda = 'US$';

              if(intval($moneda_id) == 1 && intval($externo_moneda) == 2){
                $monto_equi = $externo_mon * $externo_tipcam;
                $equivalente = 'EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$externo_tipcam.'</b>, ';
              }
              if(intval($moneda_id) == 2 && intval($externo_moneda) == 1){
                $monto_equi = $externo_mon / $externo_tipcam;
                $equivalente = 'EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$externo_tipcam.'</b>, ';
              }
              //AHORA VEAMOS SI LA INICIAL AL EXTERNO LE HIZO EN EFECTIVO O DEPOSITO
              if(intval($externo_tip) == 1){
                $iniciales .= '<b>'.$texto_moneda.' '.mostrar_moneda($externo_mon).' ('.numtoletras($externo_mon, $externo_moneda).')</b> '.$equivalente.'EN LA FECHA DE <b>'.$externo_fec.'</b> ENTREGADO AL TRANSFERENTE EN EFECTIVO, ';
              }
              if(intval($externo_tip) == 2){
                $iniciales .= '<b>'.$texto_moneda.' '.mostrar_moneda($externo_mon).' ('.numtoletras($externo_mon, $externo_moneda).')</b> DEPOSITADOS EN CUENTA BANCARIA DEL TRANSFERENTE CON FECHA <b>'.$externo_fec.'</b>, N° DE OPERACION <b>'.$externo_num.'</b>, EN BANCO '.$externo_ban.', ';
              }
            }
          $result2 = NULL;
        }
      }
      $iniciales = trim($iniciales, ', ');
    }
    else
      $iniciales = resaltar_texto('INGRESE REGISTRO DE MONTOS INICIALES');
  $result = NULL;

  $clausula_adicional = ' (EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').', ANTE EL NOTARIO VALDIVIA DEXTRE EL '.resaltar_texto('XX/'.date('m/Y')).'; POR LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($vehiculo_pre).' ('.numtoletras($vehiculo_pre, $moneda_id).')</b>; CANCELANDO LA SUMA DE: '.$iniciales.'; Y CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').'; Y '.$cheque.'; A NOMBRE DEL TRANSFERENTE '.$transferente_nom.' FINANCIADOS CON LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE SAC, GENERANDO LA PRESENTE GARANTÍA MOBILIARIA A FAVOR DE <b>EL ACREEDOR</b>; SEGÚN CLAUSULAS CONTENIDAS EN PRESENTE ESCRITURA PUBLICA DE GARANTIA MOBILIARIA),';
}

if($credito_subgar == 'GARMOB IPDN VENTA' || $credito_subgar == 'GARMOB IPDN VENTA CON RD'){
  $result = $oCreditoinicial->listar_creditoiniciales($credito_id, 3); //3 por el tipo de criedo GARVEH
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $creditoinicial_tipini = intval($value['tb_creditoinicial_tipini']); //1 efectivo, 2 deposito, 3 cuenta externo (efec, depo)
        $creditoinicial_idini = intval($value['tb_creditoinicial_idini']); //tb_ingreso, tb_deposito, tb_externo
        
        if($creditoinicial_tipini == 1){ //ENTREGADO A NUESTRA CAJA EN EFECTIVO
          $result2 = $oIngreso->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $ingreso_fec = mostrar_fecha($result2['data']['tb_ingreso_fec']);
              $ingreso_det = $result2['data']['tb_ingreso_det'];
              $ingreso_imp = $result2['data']['tb_ingreso_imp'];

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $moneda_id).')</b> EN LA FECHA DE <b>'.$ingreso_fec.'</b> ENTREGADOS EN EFECTIVO, ';
            }
          $result2 = NULL;
        }
        if($creditoinicial_tipini == 2){ //ENTREGADO A NUESTRA CAJA EN DEPOSITO
          $result2 = $oDeposito->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $deposito_fec = mostrar_fecha($result2['data']['tb_deposito_fec']);
              $deposito_num = $result2['data']['tb_deposito_num'];
              $deposito_mon = $result2['data']['tb_deposito_mon'];

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($deposito_mon).' ('.numtoletras($deposito_mon, $moneda_id).')</b> DEPOSITADOS EN CUENTA BANCARIA DE <b>EL ACREEDOR</b> CON FECHA <b>'.$deposito_fec.'</b>, N° DE OPERACION <b>'.$deposito_num.'</b>, EN BANCO DE CREDITO DEL PERU, ';
            }
          $result2 = NULL;
        }
      }
      $iniciales = trim($iniciales, ', ');
    }
    else
      $iniciales = resaltar_texto('INGRESE REGISTRO DE MONTOS INICIALES');;
  $result = NULL;

  $clausula_adicional = ' (EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').', ANTE EL NOTARIO VALDIVIA DEXTRE EL '.resaltar_texto('XX/'.date('m/Y')).'; POR LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($vehiculo_pre).' ('.numtoletras($vehiculo_pre, $moneda_id).')</b>; CANCELADOS SEGUN LO ESTIPULADO EN LA MENCIONADA ACTA DE TRANSFERENCIA VEHICULAR DE LA SIGUIENTE MANERA: '.$iniciales.' CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').'; Y EL SALDO ASCENDENTE A LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>, SERÁN CANCELADOS EN UN PLAZO MÁXIMO DE <b>'.$credito_numero_cuo.' CUOTAS</b> MENSUALES ASCENDENTE CADA CUOTA EN LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($cuotadetalle_cuo).' ('.numtoletras($cuotadetalle_cuo, $moneda_id).')</b>, CON FECHA DE VENCIMIENTO CADA CUOTA LOS <b>'.$cuota_dia.' DÍAS</b> DE CADA MES, CONSTITUYENDO EL MENCIONADO SALDO EN UNA GARANTÍA MOBILIARIA A FAVOR DE <b>EL ACREEDOR</b>; SEGÚN CLAUSULAS CONTENIDAS EN PRESENTE ESCRITURA PUBLICA DE GARANTIA MOBILIARIA),';
}

//TERMINA COPIA AQUIIIII
if($tipo_documento == 'pdf'){
  class MYPDF extends TCPDF {

  }
  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
    $pdf->SetTitle($title);
    $pdf->SetSubject('www.prestamosdelnortechiclayo.com');
    $pdf->SetKeywords('www.prestamosdelnortechiclayo.com');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(30, 20, 30);// left top right
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings
    $pdf->setLanguageArray($l);

    // ---------------------------------------------------------

    // add a page
  $pdf->AddPage('P', 'A4');
}

  $html = '
  	<p align="center"><b><u>MINUTA</u></b></p>
    <p align="justify">Señor Notario:</p>
    <p align="justify"><b>SÍRVASE UD. EXTENDER EN SU REGISTRO DE ESCRITURAS PÚBLICAS UNA DE CONSTITUCIÓN DE GARANTÍA MOBILIARIA</b> QUE CELEBRA DE UNA PARTE, '.mb_strtoupper($clientes, 'UTF-8').' EN ADELANTE <b>EL CLIENTE</b>; Y, DE OTRA PARTE, <b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, CON RUC Nº <b>20600752872</b>, CON DOMICILIO EN <b>AV. MARISCAL NIETO N° 480 EXT. A7 PRIMER PISO, DISTRITO DE CHICLAYO, PROVINCIA DE CHICLAYO Y DEPARTAMENTO DE LAMBAYEQUE</b>, DEBIDAMENTE REPRESENTADO POR SU '.$representante_den.' SR. <b>'.$representante_nom.'</b>, IDENTIFICADO CON <b>DNI Nº '.$representante_dni.'</b>, ESTADO CIVIL '.$representante_estciv.' CON DOMICILIO EN <b>'.$representante_dir.'</b>, SEGÚN PODERES INSCRITOS EN <b>LA PARTIDA Nº 11218951</b> DEL REGISTRO DE PERSONAS JURÍDICAS DE LA OFICINA REGISTRAL DE CHICLAYO, A QUIEN EN ADELANTE SOLO SE DENOMINARÁ <b>EL ACREEDOR / EL DEPOSITARIO</b>, EN LOS TÉRMINOS Y CONDICIONES SIGUIENTES:</p>

    <p><b>OBJETO, DECLARACIÓN DE PROPIEDAD Y PLAZO DEL CONTRATO</b></p>
    <p align="justify"><b>PRIMERO: EL CLIENTE</b>, DECLARA QUE MANTIENE ACTUALMENTE UNA O MÁS OBLIGACIONES CREDITICIAS A FAVOR DE <b>EL ACREEDOR</b>, LAS MISMAS QUE SE SEÑALAN EN LA CLÁUSULA SEGUNDA POR LO QUE EN GARANTÍA DE ESTAS OBLIGACIONES <b>EL CLIENTE</b> AFECTA EN CALIDAD DE PRIMERA GARANTÍA MOBILIARIA, EL (LOS) BIEN (ES) QUE EN ADELANTE SE CONOCERÁ (N) COMO EL (LOS) BIEN (ES), AUN EN EL CASO DE SER MÁS DE UNO CUYA DESCRIPCIÓN FIGURA EN EL ANEXO N° 1 QUE FORMA PARTE DEL PRESENTE CONTRATO. </p>

    <p align="justify">SE PRECISA QUE LA GARANTÍA MOBILIARIA QUE <b>EL CLIENTE</b> CONSTITUYE POR ESTE CONTRATO A FAVOR DE <b>EL ACREEDOR</b>, EN RESPALDO SUS OBLIGACIONES, TANTO LAS QUE EXISTEN A LA FECHA DE ESTE CONTRATO COMO DE AQUELLAS QUE PUDIERAN CONTRAER POSTERIORMENTE A FAVOR DE <b>EL ACREEDOR</b>. </p>';

    if($credito_cargas != ""){
      $html .= '<p align="justify"><b>EL CLIENTE</b> DECLARA CON ARREGLO A LA LEY DE GARANTÍA MOBILIARIA Y LA LEY GENERAL DEL SISTEMA FINANCIERO, QUE EL BIEN ES DE SU EXCLUSIVA PROPIEDAD,'.$clausula_adicional.' SOBRE EL VEHÍCULO MATERIA DE LA PRESENTE GARANTÍA MOBILIARIA TIENE SU LIBRE DISPOSICIÓN Y USO Y QUE SOBRE EL MISMO PESA '.$credito_cargas.'. ADICIONALMENTE, <b>EL CLIENTE</b> ASUME COMO OBLIGACIÓN DE NO HACER, EL DE NO TRANSFERIR, DISPONER CEDER, GRAVAR O AFECTAR CON ALGÚN DERECHO EL BIEN QUE AFECTA EN GARANTÍA DE ESTE ACTO A FAVOR DE <b>EL ACREEDOR</b>, SIN EL PREVIO CONSENTIMIENTO ESCRITO Y DE FECHA CIERTA DEL ACREEDOR; EL INCUMPLIMIENTO POR PARTE DEL CLIENTE CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO DE SU PARTE; EN CONSECUENCIA Y EN CASO DE REALIZAR CUALQUIERA DE TALES ACTOS, EL ACREEDOR QUEDARÁ FACULTADO A DAR POR VENCIDOS TODOS LOS PLAZOS ESTABLECIDOS EN FAVOR DEL CLIENTE, QUIEN EN TAL CASO SE OBLIGA A PAGAR TODAS LA DEUDAS Y OBLIGACIONES GARANTIZADAS CON LA PRESENTE GARANTÍA MOBILIARIA, EN CASO DE NO PAGAR TALES DEUDAS, DARÁ LUGAR A QUE EL ACREEDOR EJECUTE LA GARANTÍA, QUE SE CONSTITUYE A FAVOR DEL PRESENTE CONTRATO, CONFORME AL ARTÍCULO 175, INCISOS 4 Y 5 DE LA LEY Nº 26702, DE ACUERDO AL PROCEDIMIENTO DE VENTA EXTRAJUDICIAL SEÑALADA EN LA CLÁUSULA SEXTA, CONFORME AL ARTÍCULO 47º DE LA LEY DE GARANTÍA MOBILIARIA.</p>';
    }
    else{
      $html .= '<p align="justify"><b>EL CLIENTE</b> DECLARA CON ARREGLO A LA LEY DE GARANTÍA MOBILIARIA Y LA LEY GENERAL DEL SISTEMA FINANCIERO, QUE EL BIEN ES DE SU EXCLUSIVA PROPIEDAD,'.$clausula_adicional.' SOBRE EL VEHÍCULO MATERIA DE LA PRESENTE GARANTÍA MOBILIARIA TIENE SU LIBRE DISPOSICIÓN Y USO Y QUE SOBRE EL MISMO NO PESA NINGÚN GRAVAMEN, CARGA, LITIGIO, NI MEDIDA JUDICIAL O EXTRAJUDICIAL QUE LIMITA EN ALGÚN MODO SUS DERECHOS SOBRE SU LIBRE DISPOSICIÓN, ADICIONALMENTE, <b>EL CLIENTE</b> ASUME COMO OBLIGACIÓN DE NO HACER, EL DE NO TRANSFERIR, DISPONER CEDER, GRAVAR O AFECTAR CON ALGÚN DERECHO EL BIEN QUE AFECTA EN GARANTÍA DE ESTE ACTO A FAVOR DE <b>EL ACREEDOR</b>, SIN EL PREVIO CONSENTIMIENTO ESCRITO Y DE FECHA CIERTA <b>DEL ACREEDOR</b>; EL INCUMPLIMIENTO POR PARTE <b>DEL CLIENTE</b> CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO DE SU PARTE; EN CONSECUENCIA Y EN CASO DE REALIZAR CUALQUIERA DE TALES ACTOS, <b>EL ACREEDOR</b> QUEDARÁ FACULTADO A DAR POR VENCIDOS TODOS LOS PLAZOS ESTABLECIDOS EN FAVOR <b>DEL CLIENTE</b>, QUIEN EN TAL CASO SE OBLIGA A PAGAR TODAS LA DEUDAS Y OBLIGACIONES GARANTIZADAS CON LA PRESENTE GARANTÍA MOBILIARIA, EN CASO DE NO PAGAR TALES DEUDAS, DARÁ LUGAR A QUE <b>EL ACREEDOR</b> EJECUTE LA GARANTÍA, QUE SE CONSTITUYE A FAVOR DEL PRESENTE CONTRATO, CONFORME AL ARTÍCULO 175, INCISOS 4 Y 5 DE LA LEY Nº 26702, DE ACUERDO AL PROCEDIMIENTO DE VENTA EXTRAJUDICIAL SEÑALADA EN LA CLÁUSULA SEXTA, CONFORME AL ARTÍCULO 47º DE LA LEY DE GARANTÍA MOBILIARIA.</p>';
    }
  
  $html .='  

    <p><b>OBLIGACIONES GARANTIZADAS:</b></p>

    <p align="justify"><b>SEGUNDO:</b> LA GARANTÍA MOBILIARIA QUE SOBRE EL BIEN OTORGA <b>EL CLIENTE</b> SERVIRÁ DE PRIMERA Y PREFERENCIAL GARANTÍA DE TODAS LAS DEUDAS Y OBLIGACIONES, PRESENTES O FUTURAS, <b>DEL CLIENTE</b> QUE EXPRESAMENTE SE SEÑALA A CONTINUACIÓN, HASTA EL IMPORTE SEÑALADO EN EL ANEXO N° 1 O SU EQUIVALENTE EN OTRAS MONEDAS, MÁS SUS INTERESES, COMISIONES, GASTOS, PRIMAS DE SEGURO PAGADAS POR <b>EL ACREEDOR</b>, LAS COSTAS Y COSTOS PROCESALES, LOS EVENTUALES GASTOS DE CUSTODIA Y CONSERVACIÓN, LAS PENALIDADES Y LA INDEMNIZACIÓN POR DAÑOS Y PERJUICIOS.</p>
      
    <p align="justify">LAS DEUDAS Y OBLIGACIONES QUE QUEDAN RESPALDADAS POR LA PRESENTE GARANTÍA DE PRIMER RANGO QUE SE CONSTITUYE SOBRE EL BIEN, SERÁN LAS QUE DE MODO EXPRESO SE ESPECIFICAN A CONTINUACIÓN:</p>

    <p align="justify">a) EL CRÉDITO A QUE SE REFIERE EN EL ACÁPITE 3 DEL ANEXO N° 1 DE ESTE CONTRATO;</p>
  
    <p align="justify">b) LAS DEUDAS Y OBLIGACIONES ASUMIDAS POR <b>EL CLIENTE</b>, QUE CONSTEN EN CONTRATOS, O EN TÍTULOS VALORES; </p>
  
    <p align="justify">c) LAS DEUDAS Y OBLIGACIONES QUE SEAN O RESULTEN DE CARGO <b>DEL CLIENTE</b>, PROVENIENTE DE CONTRATOS DE CRÉDITO, LÍNEAS DE CRÉDITO, MUTUO, PRÉSTAMO, RECONOCIMIENTO DE DEUDAS U OTRAS MODALIDADES DE CRÉDITO DIRECTO O INDIRECTO O FINANCIAMIENTOS;</p>

    <p align="justify">d) LAS DEUDAS Y OBLIGACIONES QUE SEAN O RESULTANTES DE LA EMISIÓN O CONCESIÓN DE CARTAS FIANZAS, AVALES Y TODO OTRO CRÉDITO INDIRECTO O CONTINGENTE QUE <b>EL ACREEDOR</b> LE TENGA CONCEDIDO O CONCEDA EN EL FUTURO A <b>EL CLIENTE</b>.</p>
      
    <p align="justify">e) LAS DEUDAS Y OBLIGACIONES DISTINTAS A LAS INDICADAS EN LOS ACÁPITES ANTERIORES QUE EN LA FECHA PUEDAN TENER <b>EL CLIENTE</b> FRENTE A <b>EL ACREEDOR</b>, EN CUALQUIERA DE SUS OFICINAS DEL PAÍS, SEAN EN MONEDA NACIONAL O EXTRANJERA, PROVENIENTES DE CUALQUIER CRÉDITO DIRECTO O INDIRECTO, INCLUYENDO AQUELLAS OBLIGACIONES ASUMIDAS ORIGINALMENTE POR <b>EL CLIENTE</b> EN FAVOR DE TERCEROS Y QUE HAYAN SIDO TRANSFERIDAS, CEDIDAS O ENDOSADAS A <b>EL ACREEDOR</b> Y/O QUE CUENTEN CON LA FIANZA O AVAL DE <b>EL CLIENTE</b>.</p>
      
    <p align="justify">LA PRESENTE GARANTÍA MOBILIARIA SERVIRÁ IGUALMENTE DE GARANTÍA DE LAS PROPIAS OBLIGACIONES DEL CLIENTE QUE TENGA A LA FECHA O PUDIESE CONTRAER EN EL FUTURO A FAVOR DE <b>EL ACREEDOR</b>, BAJO LAS MISMAS CONDICIONES Y AMPLITUD SEÑALADAS EN EL PRESENTE CONTRATO.</p>

    <p><b>PERFECCIONAMIENTO DEL CONTRATO Y FACULTAD DE INSPECCIÓN</b></p>

    <p align="justify"><b>TERCERO: EL CLIENTE</b> DECLARA QUE EL BIEN QUE AFECTA EN FAVOR DE <b>EL ACREEDOR</b> QUEDARÁ PERFECCIONADO DESDE LA SUSCRIPCIÓN DE EL PRESENTE DOCUMENTO, CONSERVANDO <b>EL ACREEDOR</b> LA POSESIÓN DEL BIEN, Y ASUMIENDO LA CALIDAD Y LAS OBLIGACIONES DE DEPOSITARIO, CON ARREGLO A LO SEÑALADO EN LA SIGUIENTE CLÁUSULA.</p>
  
    <p align="justify"><b>EL CLIENTE</b> SE OBLIGA FRENTE <b>AL ACREEDOR</b> QUE ANTES DE CONTRATAR CON TERCEROS SOBRE CUALQUIER ASPECTO VINCULADO A LA POSESIÓN, PROPIEDAD Y/O SITUACIÓN JURÍDICA DEL BIEN, <b>EL CLIENTE</b> DEBERÁ PREVIAMENTE COMUNICÁRSELO POR ESCRITO A <b>EL ACREEDOR</b> MEDIANTE CARTA NOTARIAL. EL INCUMPLIMIENTO EN EL ENVÍO DE DICHA INFORMACIÓN DE MANERA OPORTUNA Y SUFICIENTE CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>

    <p align="justify"><b>EL ACREEDOR</b> QUEDA FACULTADO PARA ORDENAR LAS INSPECCIONES LAS VECES QUE CREA CONVENIENTE EN EL LUGAR DONDE SE ENCUENTRE EL BIEN CORRIENDO LOS GASTOS CORRESPONDIENTES POR CUENTA Y CARGO <b>DEL CLIENTE</b>.</p>

    <p align="justify">OBLIGACIONES DEL DEPOSITARIO Y FORMA DE TOMAR LA POSESIÓN DEL BIEN OBJETO DE GARANTÍA MOBILIARIA </p>

    <p align="justify"><b>CUARTO: EL ACREEDOR</b> CONSTITUIDO COMO <b>DEPOSITARIO</b> ASUME LA OBLIGACIÓN DE HACER ENTREGA DE EL (LOS) BIEN (ES) A <b>EL ACREEDOR</b>, A SIMPLE REQUERIMIENTO DE ÉSTE DENTRO DEL PLAZO QUE PARA TAL EFECTO SE SEÑALE A FIN DE FACILITAR SU VENTA, EN ESTE SUPUESTO Y EN OTROS EN LOS QUE SE ESTABLEZCA LA DISPOSICIÓN DE CUMPLIR CON LA ENTREGA DEL  EL (LOS) BIEN (ES), BASTARÁ LA FIRMA LEGALIZADA DE LAS PARTES CON EL DOCUMENTO QUE SE DEJE CONSTANCIA DE LA ENTREGA Y DEL ESTADO Y CARACTERÍSTICAS PRINCIPALES DE EL (LOS) BIEN (ES).</p>

    <p align="justify">EN CASO DE NO CUMPLIRSE CON LA OBLIGACIÓN DE ENTREGA, <b>EL CLIENTE</b> CONCEDE A <b>EL ACREEDOR</b> LA FACULTAD DE TOMAR POSESIÓN DIRECTA DE EL (LOS) BIEN (ES), EN EL LUGAR DONDE SE ENCUENTRE(N), Y A TRAVÉS DE CERRAJERO, SIN PERJUICIO DE ELLO, <b>EL ACREEDOR</b> PODRÁ EJERCER LAS ACCIONES CIVILES Y PENALES QUE HUBIERA LUGAR Y ADOPTAR, A SU ELECCIÓN, CUALQUIERA DE LAS FORMAS DE LA TOMA DE POSESIÓN SEÑALADAS EN EL SEGUNDO Y TERCER PÁRRAFO DEL ARTÍCULO 51 DE LA LEY DE GARANTÍA MOBILIARIA.</p>

    <p align="justify">COMO DEPOSITARIO ASUME LA OBLIGACIÓN DE INFORMAR POR ESCRITO A <b>EL ACREEDOR</b> SOBRE LA SITUACIÓN Y ESTADO DEL (LOS) BIEN(ES), CADA VEZ QUE ESTE ASÍ LO REQUIERA. </p>

    <p align="justify">TODOS LOS GASTOS QUE IRROGUE LA CONSERVACIÓN, CUSTODIA, MANTENIMIENTO, DE EL(OS) BIEN (ES), INFORMES, PRIMAS DE SEGURO, PAGOS DE TRIBUTOS Y CUALQUIER GASTO SERÁ DE CARGO Y CUENTA DEL CLIENTE, YA SEA QUE EL DEPOSITARIO SEA EL MISMO O UN TERCERO DESIGNADO EN COMÚN ACUERDO CON <b>EL ACREEDOR</b>.</p>
  
    <p><b>EXTENSIÓN DE LA GARANTÍA MOBILIARIA</b></p>

    <p align="justify"><b>QUINTO:</b> LA GARANTÍA MOBILIARIA QUE POR ESTE ACTO SE CONSTITUYE SE EXTIENDE A SUS INTEGRANTES Y ACCESORIOS EXISTENTES AL TIEMPO DE LA EJECUCIÓN, Y EVENTUALMENTE, EL PRECIO DE LA ENAJENACIÓN, EL NUEVO BIEN QUE RESULTE DE LA TRANSFORMACIÓN DEL BIEN MUEBLE AFECTADO EN GARANTÍA MOBILIARIA, LA INDEMNIZACIÓN DEL SEGURO QUE SE HUBIESE CONTRATADO Y LA JUSTIPRECIADA EN CASO DE EXPROPIACIÓN; ASIMISMO, COMPRENDE A SUS RENTAS, FRUTOS, MEJORAS Y DEMÁS DERECHOS A LOS QUE DICHO BIEN DE ORIGEN.</p>

    <p><b>TASACIÓN CONVENCIONAL DEL BIEN Y DESIGNACIÓN DE REPRESENTANTES</b></p>

    <p align="justify"><b>SEXTO: EL CLIENTE</b> DECLARA QUE EL VALOR COMERCIAL DEL BIEN OBJETO DEL PRESENTE CONTRATO ASCIENDE A LA SUMA QUE SE SEÑALA EN EL ANEXO N° 1; LAS DOS TERCERAS PARTES DE DICHO VALOR SERVIRÁN DE BASE PARA SU VENTA, NO SIENDO NECESARIA HACER NUEVA TASACIÓN NI ACTUALIZACIÓN ALGUNA, SALVO QUE <b>EL ACREEDOR</b> ASÍ LO ESTIME CONVENIENTE.</p>

    <p align="justify">PARA EFECTOS DE LA VENTA EXTRAJUDICIAL O EJECUCIÓN DE LA GARANTÍA, LAS PARTES OTORGAN PODER ESPECÍFICO A FAVOR DE: ERIK FRANCESC OBIOL ANAYA, IDENTIFICADO CON DNI N° 42417854, DOMICILIADO EN MZ H2 LOTE 28 URBANIZACION DERRAMA MAGISTERIAL EN EL DISTRITO DE CHICLAYO Y PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE, O DE: MANUEL FRANCISCO PORRO RIVADENEIRA, CON DNI N° 16760683, DOMICILIADO EN LA CALLE MANUEL MARIA IZAGA 716-A -SEGUNDO PISO, EN EL DISTRITO DE CHICLAYO, PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE; EN ADELANTE, EL “REPRESENTANTE” (INTERVENCIÓN INDIVIDUAL CUALESQUIERA DE ELLOS); QUIEN(ES) QUEDA(N) AUTORIZADO(S) PARA REALIZAR Y FORMALIZAR LA TRANSFERENCIA DEL BIEN MUEBLE AFECTO DE GARANTÍA MOBILIARIA, MEDIANTE SU VENTA DIRECTA O VENTA EXTRAJUDICIAL EN FAVOR DE TERCEROS INTERESADOS QUE ESTÉN DISPUESTOS A PAGAR EL PRECIO BASE DE REALIZACIÓN ANTES ACORDADO, PUDIENDO CONVOCAR A LOS COMPRADORES DIRECTAMENTE, SIN INTERVENCIÓN DE AUTORIDAD JUDICIAL, NI MARTILLERO O AGENTE ALGUNO; Y DE ESTIMAR NECESARIO ANTE LA FALTA DE INTERESADOS, PODRÁ CONVOCARLOS MEDIANTE AVISO ANTE CUALQUIER MEDIO DE COMUNICACIÓN, DURANTE EL LAPSO QUE <b>EL ACREEDOR</b> CONSIDERE CONVENIENTE.</p>

    
    <p><b>DE LA EJECUCIÓN DEL BIEN</b></p>

    <p align="justify"><b>SÉPTIMO:</b> PRODUCIDO EL INCUMPLIMIENTO DEL CLIENTE, DEL CUAL DEJARÁ CONSTANCIA EL ACREEDOR MEDIANTE CARTA NOTARIAL DIRIGIDA, AL REPRESENTANTE Y A EL CLIENTE; <b>EL ACREEDOR</b> PODRÁ PROCEDER A LA VENTA EXTRAJUDICIAL DEL BIEN AFECTO EN GARANTÍA MOBILIARIA, SI DESPUÉS DE TRANSCURRIDO TRES DÍAS HÁBILES DE RECIBIDA LA CARTA NOTARIAL POR EL CLIENTE, ÉSTE  NO CUMPLE CON PAGAR EL ÍNTEGRO DE SUS OBLIGACIONES CONFORME A LA PRESENTE MINUTA Y/O A CUALQUIER OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL EL CLIENTE ASUMA ALGUNA OBLIGACIÓN O DEUDA CON <b>EL ACREEDOR</b>; SIGUIENDO EL PROCEDIMIENTO ESTIPULADO EN LA CLÁUSULA 47° DE LA LEY DE GARANTIA MOBILIARIA</p>
  
    <p><b>PÉRDIDA DEL VALOR DEL BIEN Y EJECUCIÓN ANTICIPADA DE LA GANTÍA MOBILIARIA.</b></p>

    <p align="justify"><b>OCTAVO:</b> SI EL VALOR DEL BIEN OBJETO DE GARANTÍA MOBILIARIA DESMEJORASE A JUICIO DE <b>EL ACREEDOR</b>, SEA POR DETERIORO, PÉRDIDA DE SU VALOR DE MERCADO O POR CUALQUIER OTRA CAUSA, <b>EL CLIENTE</b> SE OBLIGA, A SIMPLE REQUERIMIENTO DE <b>EL ACREEDOR</b>, A MEJORAR LA GARANTÍA A SATISFACCIÓN DEL MISMO ACREEDOR, EN SU DEFECTO, A DISMINUIR EL MONTO DE SU OBLIGACIÓN AL IMPORTE QUE <b>EL ACREEDOR</b> LE SEÑALE EN CASO DE NO CUMPLIR DENTRO DEL PLAZO QUE <b>EL ACREEDOR</b> FIJE, QUEDARÁ AUTOMÁTICAMENTE RESUELTO EL CONTRATO DE CRÉDITO QUE GARANTIZA ESTA GARANTÍA, ENTENDIÉNDOSE QUE SE DARÁN POR VENCIDOS TODOS LOS PLAZOS ESTABLECIDOS EN FAVOR DE <b>EL ACREEDOR</b> Y EN CONTRA DE <b>EL CLIENTE</b>, SIN NECESIDAD DE FORMALIZAR NI DAR AVISO ALGUNO AL RESPECTO, DE ACUERDO AL ARTÍCULO 1430 DEL CÓDIGO CIVIL; PUDIENDO <b>EL ACREEDOR</b> PROCEDER A LA VENTA O  EJECUCIÓN DE LA GARANTÍA CONFORME A LO ESTIPULADO EN LA CLÁUSULA SEXTA DE ESTE DOCUMENTO, SI NO SE CANCELASE TOTALMENTE LAS OBLIGACIONES ASUMIDAS A FAVOR DE <b>EL ACREEDOR</b>.</p>

    <p align="justify">ASIMISMO, SI <b>EL CLIENTE</b> O, EN SU CASO, EL EVENTUAL ADQUIRENTE, DAÑARA O PUSIERA EN PELIGRO EL BIEN MUEBLE AFECTADO EN GARANTÍA MOBILIARIA, <b>EL ACREEDOR</b> TENDRÁ DERECHO A EXIGIR LA ENTREGA DEL BIEN MUEBLE OBJETO DE LA PRESENTE GARANTÍA Y A PROCEDER A LA EJECUCIÓN DE LA GARANTÍA, CONFORME A LO ESTIPULADO EN LA CLÁUSULA SEXTA DEL PRESENTE CONTRATO.</p>
    
    <p><b>REGLAS DE IMPUTACIÓN</b></p>

    <p align="justify"><b>NOVENO:</b> EL PAGO DERIVADO DE LA EJECUCIÓN DE LA GARANTÍA MOBILIARIA SE IMPUTARÁ A LOS GASTOS Y COMISIONES, LA INDEMNIZACIÓN POR DAÑOS Y PERJUICIOS, LAS PENALIDADES, LAS COSTAS Y COSTOS PROCESALES, LAS PRIMAS DE SEGURO PAGADAS POR <b>EL ACREEDOR</b>, LOS INTERESES QUE DEVENGUE Y EL CAPITAL, EN ESE ORDEN.</p>

    <p><b>ADJUDICACIÓN DEL BIEN POR <b>EL ACREEDOR</b></b></p>
    
    <p align="justify"><b>DÉCIMO:</b> LAS PARTES ACUERDAN QUE EN CASO DE INCUMPLIMIENTO DE CUALQUIERA DE LAS OBLIGACIONES ASUMIDAS POR <b>EL CLIENTE</b>, EN VIRTUD DEL PRESENTE CONTRATO, <b>EL ACREEDOR</b> PODRÁ, ALTERNATIVAMENTE A LA EJECUCIÓN EXTRAJUDICIAL, ADJUDICARSE LA PROPIEDAD DEL BIEN AFECTADO POR GARANTÍA MOBILIARIA, CON TAL FIN, SE ESTABLECE QUE EL VALOR DEL BIEN ES EL INDICADO EN EL ANEXO N° 1 ADJUNTADO AL PRESENTE CONTRATO DE LA GARANTÍA MOBILIARIA, ASIMISMO SE CONVIENE EN OTORGAR PODER ESPECÍFICO E IRREVOCABLE A FAVOR DE ERIK FRANCESC OBIOL ANAYA, IDENTIFICADO CON DNI N° 42417854, DOMICILIADO EN MZ H2 LOTE 28 URBANIZACION DERRAMA MAGISTERIAL EN EL DISTRITO DE CHICLAYO Y PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE; O DE: MANUEL FRANCISCO PORRO RIVADENEIRA, CON DNI N° 16760683 Y DOMICILIADO EN LA CALLE MANUEL MARIA IZAGA 716-A -SEGUNDO PISO, EN EL DISTRITO DE CHICLAYO Y PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE, (INTERVENCIÓN INDIVIDUAL CUALESQUIERA DE ELLOS); PARA QUE EN CASO DE INCUMPLIMIENTO PROCEDA(N) A SUSCRIBIR LA DOCUMENTACIÓN NECESARIA PARA LA TRANSFERENCIA DEL BIEN MUEBLE AFECTO EN GARANTÍA MOBILIARIA.</p>

    <p><b>GASTOS DE FORMALIZACIÓN DEL CONTRATO</b></p>
      
    <p align="justify"><b>DÉCIMO PRIMERO:</b> TODOS LOS GASTOS QUE SE ORIGINEN COMO CONSECUENCIA DEL PRESENTE DOCUMENTO SERÁN CARGADOS POR <b>EL ACREEDOR</b> EN CUALQUIERA DE LAS CUENTAS O DEPÓSITOS QUE <b>EL CLIENTE</b> MANTENGA CON <b>EL ACREEDOR</b>, O QUE AL EFECTO ÉSTE ABRA A SU NOMBRE CON OBLIGACIÓN DEL MISMO DE REEMBOLSAR TALES IMPORTES EL DÍA MISMO EN QUE SE PRODUZCA LOS CARGOS, GENERÁNDOSE EN CASO CONTRARIO LOS INTERESES COMPENSATORIOS Y MORATORIOS LAS TASAS QUE <b>EL ACREEDOR</b> TENGA ESTABLECIDO, HASTA LA FECHA EFECTIVA DE SU PAGO.</p>
    
    <p><b>INFORMACIÓN CREDITICIA DEL CLIENTE</b></p>

    <p align="justify"><b>DÉCIMO SEGUNDO: EL CLIENTE</b> AUTORIZA A <b>EL ACREEDOR</b> A PROPORCIONAR LAS INFORMACIONES RELATIVAS AL INCUMPLIMIENTO DE SUS OBLIGACIONES, A TERCERAS PERSONAS, INCLUSIVE CENTRALES DE RIESGO, PUDIENDO DIFUNDIRSE Y/O COMERCIALIZARSE DICHAS INFORMACIONES CREDITICIAS NEGATIVAS SIN NINGUNA, RESPONSABILIDAD PARA <b>EL ACREEDOR</b>.</p>

    <p align="justify">DEL MISMO MODO, <b>EL ACREEDOR</b> QUEDA AUTORIZADO A VERIFICAR LOS DATOS E INFORMACIONES PROPORCIONADAS POR <b>EL CLIENTE</b>, AUTORIZARLOS E INTERCAMBIARLOS CON OTROS ACREEDORES O TERCEROS (INCLUSIVE CENTRALES DE RIESGO); ASI COMO OBTENER INFORMACIÓN SOBRE SU PATRIMONIO, CUMPLIMIENTO DE SUS PAGOS CON TERCEROS ACREEDORES Y SUS TRANSACCIONES BANCARIAS Y CREDITICIAS EN GENERAL.</p>

    <p align="justify"><b>EL CLIENTE</b> LIBERA A <b>EL ACREEDOR</b> DE TODA RESPONSABILIDAD POR LA DIFUSIÓN Y/O COMERCIALIZACIÓN POR TERCEROS DE DICHAS INFORMACIONES; LIMITÁNDOSE EXCLUSIVAMENTE LA OBLIGACIÓN <b>DEL ACREEDOR</b> A RECTIFICAR INFORMACIONES QUE HAYA PROPORCIONADO, SIEMPRE QUE NO CORRESPONDAN EXACTAMENTE A LA SITUACIÓN <b>EL CLIENTE</b>.</p>
  
    <p><b>COMPETENCIA TERRITORIAL</b></p>

    <p align="justify"><b>DÉCIMO TERCERO:</b> PARA LA VALIDEZ DE TODAS LAS COMUNICACIONES Y NOTIFICACIONES A LAS PARTES CON MOTIVO DE LA EJECUCIÓN DE ESTE CONTRATO, CONVIENE EN SEÑALAR COMO SUS RESPECTIVOS DOMICILIOS LOS INDICADOS EN LA INTRODUCCIÓN Y/O ANEXO N° 1 DE ESTE DOCUMENTO; EL CAMBIO DE DOMICILIO <b>DEL CLIENTE</b> SURTIRÁ EFECTO DESDE LA FECHA CIERTA (CARTA NOTARIAL), DENTRO DE LOS TREINTA DÍAS DE OCURRIDO EL HECHO; Y EL CAMBIO DE DOMICILIO DEL ACREEDOR / DEPOSITARIO SURTIRÁ EFECTO DESDE SU DECLARACIÓN A TRAVÉS DE CUALQUIER MEDIO DE COMUNICACIÓN MASIVA, DENTRO DE LOS TREINTA DÍAS DE OCURRIDO EL HECHO. EL DOMICILIO DE AMBAS PARTES QUE DEBE QUEDAR DENTRO DEL RADIO URBANO DE LA CIUDAD DE CHICLAYO.</p>

    <p align="justify">TODO LITIGIO O CONTROVERSIA, DERIVADOS O RELACIONADOS CON ESTE ACTO JURÍDICO, SERÁ RESUELTO MEDIANTE ARBITRAJE, DE CONFORMIDAD CON LOS REGLAMENTOS ARBITRALES DEL CENTRO DE ARBITRAJE DE LA CÁMARA DE COMERCIO DE LAMBAYEQUE, A CUYAS NORMAS, ADMINISTRACIÓN Y DECISIÓN SE SOMETEN LAS PARTES EN FORMA INCONDICIONAL, DECLARANDO CONOCERLAS Y ACEPTARLAS EN SU INTEGRIDAD.</p>

    <p><b>DISPOSICIONES GENERALES.-</b></p>

    <p align="justify"><b>DÉCIMO CUARTO:</b></p>

    <p align="justify">14.1 LAS CLÁUSULAS Y NUMERALES DE LA PRESENTE MINUTA SON SEPARABLES Y LA NULIDAD, INEFICACIA O INVALIDEZ DE UNA O MÁS DE ELLAS NO PERJUDICARÁ A LAS RESTANTES EN TANTO SE MANTENGA LA ESENCIA DE ESTE ACTO. EN CASO QUE ALGUNA DE LAS CLÁUSULAS O NUMERALES DEL PRESENTE INSTRUMENTO SEA DECLARADA NULA, INEFICAZ O INVÁLIDA, <b>EL CLIENTE</b> HARÁ TODO ESFUERZO RAZONABLE PARA ELABORAR E IMPLEMENTAR UNA SOLUCIÓN LEGALMENTE VÁLIDA QUE LOGRE EL RESULTADO MÁS CERCANO A AQUÉL QUE SE BUSCABA OBTENER CON EL NUMERAL Y/O CLÁUSULA DECLARADA NULA, INEFICAZ O INVÁLIDA.</p>

    <p align="justify">14.2 NINGUNO DE LOS NUMERALES, CLÁUSULAS, TÉRMINOS NI CONDICIONES ESTABLECIDAS EN LA PRESENTE MINUTA, PODRÁN SER MODIFICADOS SIN EL PREVIO CONSENTIMIENTO ESCRITO <b>DEL ACREEDOR</b>. INCLUSO, <b>EL CLIENTE</b> DECLARA QUE LA CONSTITUCIÓN DE GARANTÍA MOBILIARIA CONSTITUIDA EN ESTE ACTO SOBRE EL BIEN, ÚNICA Y EXCLUSIVAMENTE, PODRÁ SER CANCELADA O LEVANTADA POR <b>EL ACREEDOR</b>.</p>

    <p align="justify">14.3 <b>EL CLIENTE</b> SE COMPROMETE A SUBSANAR CUALQUIER OBSERVACIÓN QUE PUDIERA SUFRIR LA SOLICITUD DE INSCRIPCIÓN DE ESTA CONSTITUCIÓN DE GARANTÍA MOBILIARIA EN EL REGISTRO MOBILIARIO DE CONTRATOS, FIJÁNDOSE COMO PLAZO MÁXIMO PARA REALIZAR LA SUBSANACIÓN TREINTA (30) DÍAS CALENDARIO, CONTADOS DESDE LA FECHA DE PRESENTACIÓN DE LA SOLICITUD DE INSCRIPCIÓN; ASI TAMBIÉN SERÁ <b>EL CLIENTE</b> QUIEN CANCELE LOS COSTOS NOTARIALES Y REGISTRALES RESPECTIVOS. DE NO SUBSANAR LA(S) OBSERVACIÓN(ES), <b>EL ACREEDOR</b> QUEDARÁ FACULTADO PARA EXIGIR LA INMEDIATA ENTREGA DEL BIEN A EL DEPOSITARIO, BAJO RESPONSABILIDAD <b>DEL CLIENTE</b> Y DEL DEPOSITARIO. EL INCUMPLIMIENTO <b>DEL CLIENTE</b> DE SUBSANAR EN EL PLAZO PREVISTO (TREINTA (30) DÍAS CALENDARIO) CUALQUIER OBSERVACIÓN REGISTRAL, TAMBIÉN CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>; INICIANDO DE SER EL CASO LA EJECUCIÓN DE LA GARANTIA OTORGADA.</p>


    <p align="justify"><b>PRIMERA CLÁUSULA ADICIONAL:</b> LAS PARTES DEJAN EN EXPRESA CONSTANCIA QUE EN EL PRESENTE ACTO, EN RELACIÓN AL MEDIO DE PAGO, <b>EL ACREEDOR</b> PROCEDE DE CONFORMIDAD CON LA LEY N°28194.</p>

    <p align="justify"><b>SEGUNDA CLÁUSULA ADICIONAL:</b> TODOS LOS GASTOS QUE OCASIONE LA ESCRITURA A LA QUE ESTA MINUTA DÉ LUGAR, UN TESTIMONIO Y UNA COPIA SIMPLE DE LA MISMA, CERTIFICADO DE GRAVAMEN CON LA INSCRIPCIÓN DE LA GARANTÍA DE LOS REGISTROS PÚBLICOS PARA <b>EL ACREEDOR</b>, SERÁN DE CUENTA DE <b>EL CLIENTE</b></p>

    <p align="justify"><b>TERCERA CLÁUSULA ADICIONAL:</b> LAS PARTES INTERVINIENTES EXPRESAN SU CONFORMIDAD CON LOS TÉRMINOS DE LA PRESENTE MINUTA Y LA SUSCRIBEN EN LA CIUDAD DE LA VICTORIA A LOS '.$credito_dia.' DIAS DEL MES DE '.strtolower(nombre_mes($credito_mes)).' DEL '.$credito_anio.'.</p>

    <p align="justify"><b>CUARTA CLÁUSULA ADICIONAL:</b> LAS PARTES DEJAN CONSTANCIA EXPRESA QUE EL(LOS) BIEN(ES) MATERIA DE GARANTÍA MOBILIARIA PERMANECERÁ EN CUSTODIA DE <b>EL ACREEDOR</b> CONFORME A LO ESTABLECIDO EN LA CLÁUSULA CUARTA DE LA PRESENTE MINUTA.</p>

    <p align="justify">USTED SEÑOR NOTARIO SE ENCARGARÁ DE AGREGAR LA INTRODUCCIÓN Y CONCLUSIÓN DE LEY, PARTES A EL/LOS REGISTRO/S PÚBLICOS CORRESPONDIENTES, A LOS EFECTOS DE LA/S INSCRIPCIÓN/ES PERTINENTE/ES.</p>

    <p align="justify">LA VICTORIA, '.$credito_dia.' DE '.strtoupper(nombre_mes($credito_mes)).' DEL '.$credito_anio.'</p>
    
    <p></p>

    <table style="width:100%; text-align: center; margin-left: 80px;">
      <tr>
        <td style="width:30%; text-align: center;">
          <b>_________________________</b><br/>
          <b>EL CLIENTE</b>
        </td>
        <td style="width:40%;"></td>
        <td style="width:30%; text-align: center;">
          <b>_________________________</b><br/>
          <b>EL ACREEDOR</b>
        </td>
      </tr>
    </table>
';

    //nueva hoja
$html2 .='    
    <p align="justify"><b>ANEXO N° 1</b></p>
    <p align="justify"><b>AL CONTRATO DE GARANTÍA MOBILIARIA</b></p>

    <p align="justify">1. DESCRIPCIÓN DEL BIEN AFECTADO EN GARANTIA MOBILIARIA EN FAVOR DE <b>EL ACREEDOR</b> POR <b>EL CLIENTE</b>, QUIEN MANIFIESTA CON CARÁCTER DE DECLARACIÓN JURADA DE ACUERDO A LA LEY N° 26702, QUE ES DE SU PROPIEDAD Y LIBRE DISPOSICIÓN: EL BIEN.- VEHICULO CON PLACA DE RODAJE N° <b>'.$vehiculo_pla.'</b>, CARROCERÍA: <b>'.$vehcla_nom.'</b>, CATEGORÍA: <b>'.$vehiculo_cate.'</b>, MARCA: <b>'.$vehmar_nom.'</b>, MODELO: <b>'.$vehmod_nom.'</b>, AÑO DE MODELO: <b>'.$vehiculo_mode.'</b>, COLOR: <b>'.$vehiculo_col.'</b>, N° DE SERIE: <b>'.$vehiculo_cha.'</b>, N° DE MOTOR: <b>'.$vehiculo_mot.'</b>, COMBUSTIBLE: <b>'.$vehiculo_comb.'</b>, CUYO DOMINIO CORRE INSCRITO EN LA PARTIDA N° <b>'.$vehiculo_parele.'</b> DE REGISTRO DE PROPIEDAD VEHICULAR EN LA '.$zona_nom.'.</p>

    <p align="justify">2. VALOR DE LA TASACIÓN COMERCIAL ACTUALIZADO: <b>US$ '.mostrar_moneda($credito_valtas).' ('.numtoletras($credito_valtas, 2).')</b>.</p>

    <p>3. CRÉDITO GARANTIZADO: '.$anexo_adicional.'</p>

    <p align="justify">4. VALOR DE REALIZACIÓN DEL MERCADO: <b>US$ '.mostrar_moneda($credito_valtas).' ('.numtoletras($credito_valtas, 2).')</b>.</p>

    <p align="justify">5. VALOR DE GRAVAMEN: LA PRESENTE GARANTÍA MOBILIARIA QUE SE CONSTITUYE TIENE UN MONTO O VALOR DE GRAVAMEN ASCENDENTE A LA SUMA DE <b>US$ '.mostrar_moneda($credito_valtas).' ('.numtoletras($credito_valtas, 2).')</b>.</p>

    <p align="justify">6. CRONOGRAMA DE PAGOS:</p>
    '.$cuota_detalle.'

    <p align="justify">USTED SEÑOR NOTARIO SE ENVIARÁ A AGREGAR LA INTRODUCCIÓN Y CONCLUSIÓN DE LE, PARTES A EL /LOS REGISTRO/S PÚBLICOS CORRESPONDIENTES, A LOS EFECTOS DE LA/S INSCRIPCIÓN/ES PERTINENTE/ES.</p>

    <p>LA VICTORIA '.$credito_dia.' DE '.strtoupper(nombre_mes($credito_mes)).' DEL '.$credito_anio.'.</p>
    
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <table style="width:100%; text-align: center; margin-left: 80px;">
      <tr>
        <td style="width:30%; text-align: center;"><b>
          _________________________<br/>
          EL CLIENTE </b>
        </td>
        <td style="width:40%;"></td>
        <td style="width:30%; text-align: center;"><b>
          _________________________<br/>
          EL ACREEDOR </b>
        </td>
      </tr>
    </table>
  ';

if($tipo_documento == 'word'){
  $html = str_replace('&', '&amp;amp;', $html);
  $html2 = str_replace('&', '&amp;amp;', $html2);

  // Crear una instancia de PHPWord
  $phpWord = new PhpWord();

  $phpWord->setDefaultFontName('Arial');
  $phpWord->setDefaultFontSize(8);

  // Definir estilo de párrafo
  $paragraphStyle = array('spaceAfter' => 0, 'spacing' => 0);

  // Crear una nueva sección
  $section1 = $phpWord->addSection();

  // Renderizar el contenido HTML 1
  Html::addHtml($section1, $html, false, false, $paragraphStyle);

  // Insertar un salto de página para la siguiente sección
  $section1->addPageBreak();

  $section2 = $phpWord->addSection();
  // Renderizar el contenido HTML 2
  Html::addHtml($section2, $html2, false, false, $paragraphStyle);

  // Guardar el documento
  $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
  $objWriter->save('con_custodia.docx');
  echo 'Minuta para el cliente: '.$cliente_nom.', generado correctamente...';
  header('Location: ../doc_creditogarveh/con_custodia.docx');
}

if($tipo_documento == 'pdf'){
  $pdf->SetFont('arial', '', 8);
  $pdf->writeHTML($html, true, 0, true, true);

  $pdf->Ln();
  $pdf->AddPage();
  $pdf->writeHTML($html2, true, 0, true, true);

  // set UTF-8 Unicode font
  //$pdf->SetFont('dejavusans', '', 10);

  // output the HTML content
  //$pdf->writeHTML($html, true, 0, true, true);

  // reset pointer to the last page
  $pdf->lastPage();

  // ---------------------------------------------------------

  //Close and output PDF document
  $nombre_archivo=$codigo."_".$title.".pdf";
  $pdf->Output($nombre_archivo, 'I');
}

?>