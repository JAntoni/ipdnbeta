<?php
require_once('../../static/tcpdf/tcpdf.php');
require_once("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once("../representante/Representante.class.php");
$oRepresentante = new Representante();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once("../funciones/operaciones.php");

$credito_id = intval($_GET['d1']);

$title = 'LETRAS DE CAMBIO';
$codigo = 'LETRA-GARMOB-' . str_pad($credito_id, 4, "0", STR_PAD_LEFT);

$result = $oCredito->mostrarUno($credito_id);
if ($result['estado'] == 1) {

  $credito_preaco = $result['data']['tb_credito_preaco'];


  $cliente_id = $result['data']['tb_cliente_id'];
  $cliente_doc = $result['data']['tb_cliente_doc'];
  $cliente_nom = $result['data']['tb_cliente_nom'];
  $cliente_ape = $result['data']['tb_cliente_ape'];
  $cliente_dir = $result['data']['tb_cliente_dir'];
  $cliente_ema = $result['data']['tb_cliente_ema'];
  $cliente_fecnac = $result['data']['tb_cliente_fecnac'];
  $cliente_tel = $result['data']['tb_cliente_tel'];
  $cliente_cel = $result['data']['tb_cliente_cel'];
  $cliente_telref = $result['data']['tb_cliente_telref'];

  //cliente persona juridica
  $cliente_tip = $result['data']['tb_cliente_tip'];
  $cliente_empruc = $result['data']['tb_cliente_empruc']; //ruc de la empresa, persona juridica
  $cliente_emprs = $result['data']['tb_cliente_emprs']; //razon social
  $cliente_empger = $result['data']['tb_cliente_empger']; //tipo de gerente General o Titular
  $cliente_empdir = $result['data']['tb_cliente_empdir']; //dirección de la empresa
  $representante_id = $result['data']['tb_representante_id'];
  $moneda_id = intval($result['data']['tb_moneda_id']);
}
$result = NULL;

$result = $oRepresentante->mostrarUno($representante_id);
if ($result['estado'] == 1) {
  $representante_nom = mb_strtoupper($result['data']['tb_representante_nom'], 'UTF-8');
  $representante_estciv = mb_strtoupper($result['data']['tb_representante_estciv'], 'UTF-8');
  $representante_dni = $result['data']['tb_representante_dni'];
  $representante_dir = mb_strtoupper($result['data']['tb_representante_dir'], 'UTF-8'); //dirección
  $representante_den = mb_strtoupper($result['data']['tb_representante_den'], 'UTF-8'); // denominacion: Gerente, SUb, etc
}
$result = NULL;
// FIN REPRESENTANTE

if ($moneda_id == 1) {
  $moneda = 'S/.';
} else {
  $moneda = 'US$';
}


class MYPDF extends TCPDF {}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
$pdf->SetTitle($title);
$pdf->SetSubject('www.prestamosdelnortechiclayo.com');
$pdf->SetKeywords('www.prestamosdelnortechiclayo.com');


// set default header data

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10); // left top right
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// add a page
$pdf->AddPage('P', 'A4');

$estilos = '
  <style>
    .tb_cab{
      border: 1px solid black;
    }
    .verticalText {
      
      transform: rotate(180deg);
    }
  </style>
';

$cliente = $cliente_nom;
$cliente_documento = $cliente_doc;

if(!empty($cliente_emprs)){
  $cliente = $cliente_emprs;
  $cliente_documento = $cliente_empruc;
}

$letra_vacia = '
  <table width="100%" style="border-bottom: 1px solid black">
    <tr>
      <td width ="20%" border="1" style="font-size:6pt;" align="justify">(1) En caso de mora, esta Letra de Cambio generará las tasas de intereses compensatorios y moratorio más alta que la ley permita a su último Tenedor.<br>(2) El plazo de sus vencimiento podrá ser prorrogado por el Tenedor por el plazo que éste señale, sin que sea necesaria la intervención del obligado principal ni de los solidarios.<br>(3) Su importe debe ser pagado solo en la misma moneda que se expresa este título valor.<br>(4) Esta Letra de Cambio no requiere ser protestada por falta de pago.<br>
        <p><br></p>

        <p width="100" align="center">_______________________<br>
          Aceptante</p>
        <p><br></p>
        <p><br></p>
        <p width="100" align="center">_______________________<br>
          Aceptante</p>
        <p><br></p>
      </td>
      <td width ="80%">
        
        <table width="100%" cellpadding="2" border="1">
          <tr>
            <th align="center" style="font-size:6pt;">NUMERO</th>
            <th align="center" style="font-size:6pt;">REF. DEL GIRADOR</th>
            <th align="center" style="font-size:6pt;">LUGAR DE GIRO</th>
            <th align="center" style="font-size:6pt;">FECHA DE GIRO</th>
            <th align="center" style="font-size:6pt;">VENCIMIENTO</th>
            <th align="center" style="font-size:6pt;">MONEDA E IMPORTE</th>
          </tr>
          <tr>
            <td align="center" style="font-size:6pt;" rowspan="2">1</td>
            <td align="center" style="font-size:6pt;" rowspan="2"></td>
            <td align="center" style="font-size:6pt;" rowspan="2">Chiclayo</td>
            <td align="center" style="font-size:6pt;">DIA / MES / AÑO</td>
            <td align="center" style="font-size:6pt;">DIA / MES / AÑO</td>
            <td align="center" style="font-size:6pt;" rowspan="2">'.$moneda.' '.mostrar_moneda($credito_preaco).'</td>
          </tr>
          <tr>
            <td align="center" style="font-size:6pt;"></td>
            <td align="center" style="font-size:6pt;"></td>
          </tr>
        </table>
        
        <table width="100%" cellpadding="2">
          <tr>
            <td style="font-size:6pt;">Por esta LETRA DE CAMBIO, se servirá(n) pagar incondicionalmente a la Orden de <b>Inversiones y Préstamos del Norte SAC</b>. La cantidad de:</td>
          </tr>
          <tr>
            <td style="font-size:6pt;" border="1"></td>
          </tr>
          <tr>
            <td style="font-size:6pt;">En el siguiente lugar de pago, o con cargo en la cuenta del Banco:</td>
          </tr>
        </table>
        
        <table width="100%" class="tb_cab" cellpadding="2">
          <tr>
            <td style="font-size:6pt;" colspan="5">Girado: <b>'.$cliente.'</b></td>
            <td style="font-size:6pt;" colspan="5" border="1">Importe a debitar en la siguiente cuenta del Banco que se indica</td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">Domicilio: <b>'.$cliente_dir.'</b></td>
            <td style="font-size:6pt;" border="1">BANCO</td>
            <td style="font-size:6pt;" border="1">OFICINA</td>
            <td style="font-size:6pt;" colspan="2" border="1">NUMERO DE CUENTA</td>
            <td style="font-size:6pt;" border="1">D.C.</td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">D.0.I: <b>'.$cliente_documento.'</b></td>
            <td style="font-size:6pt;" border="1" rowspan="2"></td>
            <td style="font-size:6pt;" border="1" rowspan="2"></td>
            <td style="font-size:6pt;" colspan="2" border="1" rowspan="2"></td>
            <td style="font-size:6pt;" border="1" rowspan="2"></td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">Telf: <b>'.$cliente_tel.'</b></td>
          </tr>
        </table>

        <table width="100%" class="tb_cab" cellpadding="2">
          <tr>
            <td style="font-size:6pt;" colspan="5">Fiador: <b></b></td>
            <td style="font-size:6pt; border-left: 1px solid black;" colspan="2">Nombre / Denominación o Razón Social del Girador</td>
            <td colspan="3" style="font-size:8pt;"><b>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</b></td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">Aval Permanente: <b></b></td>
            <td style="font-size:6pt; border-left: 1px solid black;">D.O.I.</td>
            <td colspan="4"><b>20600752872</b></td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">Domicilio: <b></b></td>
            <td style="font-size:6pt; border-left: 1px solid black;"></td>
            <td style="font-size:6pt;" colspan="2" align="center" rowspan="2">
              _______________________<br>
              Firma
            </td>
            <td style="font-size:6pt;" colspan="2" align="center" rowspan="2">
              _______________________<br>
              Firma
            </td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="3">D.O.I: <b></b></td>
            <td style="font-size:6pt;" colspan="2">Telf:</td>
            <td colspan="5" style="font-size:6pt; border-left: 1px solid black;"></td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">Firma: <b></b></td>
            <td colspan="5" style="font-size:6pt; border-left: 1px solid black;"></td>
          </tr>
          <tr>
            <td style="font-size:6pt;" colspan="5">Nombre del Representante: <b></b>
              <p><br></p>
            </td>
            <td colspan="5" style="font-size:6pt; border-left: 1px solid black;">Nombre del Representante(s): <b>Sr. '.$representante_nom.'</b><br>
              D.O.I. <b>'.$representante_dni.'</b>
            </td>
          </tr>
        </table>       
      </td>
    </tr>
  </table>
  <table width="100%">
    <tr width="100%">
      <td style="font-size:6pt;">No escribir ni firmar debajo de esta línea</td>
    </tr>
  </table>
  <p><hr></p>
';

$pdf->SetFont('arial', '', 8);
$pdf->writeHTML($estilos.$letra_vacia, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

//Close and output PDF document
$nombre_archivo = $codigo . "_" . $title . ".pdf";
$pdf->Output($nombre_archivo, 'I');
