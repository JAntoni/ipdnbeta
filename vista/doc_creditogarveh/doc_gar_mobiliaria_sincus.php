<?php
require_once('../../vendor/autoload.php');
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\Font;
use PhpOffice\PhpWord\Style\Paragraph;

require_once ("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();
require_once ("../cuentadeposito/Cuentadeposito.class.php");
$oCuentadeposito = new Cuentadeposito();
require_once ("../zona/Zona.class.php");
$oZona = new Zona();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once ("../transferente/Transferente.class.php");
$oTransferente = new Transferente();
require_once ("../externo/Externo.class.php");
$oExterno = new Externo();
require_once ("../cheque/Cheque.class.php");
$oCheque = new Cheque();
require_once("../creditoinicial/Creditoinicial.class.php");
$oCreditoinicial = new Creditoinicial();
require_once ("../representante/Representante.class.php");
$oRepresentante = new Representante();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once("../funciones/operaciones.php");

$tipo_documento = $_GET['d2']; //solo puede ser WORD Y PDF
$credito_id = intval($_GET['d1']);

if(!isset($_GET['d2'])){
  echo 'NO HAS SELECCIONADO SI SERÁ WORD O PDF '.$_GET['d2'].' / '.$tipo_documento; exit();
}

if($tipo_documento == 'pdf')
  require_once('../../static/tcpdf/tcpdf.php');

$title = 'GARANTÍA MOBILIARIA SIN CUSTODIA';
$codigo = 'GARMOB-'.str_pad($credito_id, 4, "0", STR_PAD_LEFT);

$result = $oCredito->mostrarUno($credito_id);
  if($result['estado'] == 1){
    $cuotatipo_id = $result['data']['tb_cuotatipo_id']; //? 1,2,3,4 -> los créditos garveh usan los tipos 3 y 4, 3: Libre y 4 Cuota Fija
    $cuotasubperiodo_id = $result['data']['tb_cuotasubperiodo_id']; //? 1 mensual, 2 quincenal, 3 semanal
    $moneda_id = $result['data']['tb_moneda_id'];
    $representante_id = $result['data']['tb_representante_id'];
    $credito_tc = $result['data']['tb_credito_tc'];
    $credito_preaco = $result['data']['tb_credito_preaco'];
    $credito_int = $result['data']['tb_credito_int'];
    $credito_numcuo = $result['data']['tb_credito_numcuo'];
    $credito_linapr = $result['data']['tb_credito_linapr'];
    $credito_numcuomax = $result['data']['tb_credito_numcuomax'];
    $credito_ini = $result['data']['tb_credito_ini'];
    $credito_feccre = mostrar_fecha($result['data']['tb_credito_feccre']);
    $credito_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
    $credito_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
    $credito_fecpro = mostrar_fecha($result['data']['tb_credito_fecpro']);
    $credito_fecent = mostrar_fecha($result['data']['tb_credito_fecent']);
    $credito_obs = trim($result['data']['tb_credito_obs']);
    $credito_est = $result['data']['tb_credito_est'];
    $credito_cargas = trim($result['data']['tb_credito_carg']); //si tuviera cargas el vehículo
    $credito_fecgps = mostrar_fecha($result['data']['tb_credito_fecgps']);
    $credito_fecstr = mostrar_fecha($result['data']['tb_credito_fecstr']);
    $credito_fecsoat = mostrar_fecha($result['data']['tb_credito_fecsoat']);
    $credito_fecvenimp = mostrar_fecha($result['data']['tb_credito_fecvenimp']); //fecha de vencimiento de impuesto vehicular
    $credito_tip = $result['data']['tb_credito_tip']; // tipo del credito 1 es credito normal, 2 es una adenda, 3 ACUERDO PAGO, 4 garantía MINUTA

    $credito_numero_cuo = $credito_numcuo;
    if($cuotatipo_id == 3)
      $credito_numero_cuo = $credito_numcuomax;

    $cliente_id = $result['data']['tb_cliente_id'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_ape = $result['data']['tb_cliente_ape'];
    $cliente_dir = $result['data']['tb_cliente_dir'];
    $cliente_ema = $result['data']['tb_cliente_ema'];
    $cliente_fecnac = $result['data']['tb_cliente_fecnac'];
    $cliente_tel = $result['data']['tb_cliente_tel'];
    $cliente_cel = $result['data']['tb_cliente_cel'];
    $cliente_telref = $result['data']['tb_cliente_telref'];
    $cliente_ubige = $result['data']['tb_ubigeo_cod']; //ubigeo d<b>EL CLIENTE</b>
    $cliente_emp_ubige = $result['data']['tb_ubigeo_cod3']; //ubigeo 3 es para la direccion de la empresa

    //cliente persona juridica
    $cliente_tip = $result['data']['tb_cliente_tip'];
    $cliente_empruc = $result['data']['tb_cliente_empruc']; //ruc de la empresa, persona juridica
    $cliente_emprs = $result['data']['tb_cliente_emprs']; //razon social
    $cliente_empger = $result['data']['tb_cliente_empger']; //tipo de gerente General o Titular
    $cliente_empdir = $result['data']['tb_cliente_empdir']; //dirección de la empresa
    //----------
    $cliente_estciv = $result['data']['tb_cliente_estciv']; // 1 soltero, 2 casado
    $cliente_bien = $result['data']['tb_cliente_bien']; // 1 separacion de bienes, 2 sin separacion
    $cliente_firm = $result['data']['tb_cliente_firm']; // 1 firma solo, 2 firma con conyuge
    $cliente_zon_id = $result['data']['tb_zona_id']; // id de la zona registral donde está el N° de partida
    $cliente_numpar = $result['data']['tb_cliente_numpar']; //numero de partida d<b>EL CLIENTE</b>

    $vehiculo_tipus = $result['data']['tb_credito_tipus']; //tipo de uso 1 taxi, 2 uso particular
    $vehiculo_pre = $result['data']['tb_credito_vehpre'];
    $vehmar_id = $result['data']['tb_vehiculomarca_id'];
    $vehmod_id = $result['data']['tb_vehiculomodelo_id'];
    $vehcla_id = $result['data']['tb_vehiculoclase_id'];
    $vehtip_id = $result['data']['tb_vehiculotipo_id'];
    $vehiculo_pla = $result['data']['tb_credito_vehpla'];
    $vehiculo_mot = $result['data']['tb_credito_vehsermot'];
    $vehiculo_cha = $result['data']['tb_credito_vehsercha'];
    $vehiculo_ano = $result['data']['tb_credito_vehano'];
    $vehiculo_col = $result['data']['tb_credito_vehcol'];
    $vehiculo_numpas = $result['data']['tb_credito_vehnumpas'];
    $vehiculo_numasi = $result['data']['tb_credito_vehnumasi'];
    $vehiculo_kil = $result['data']['tb_credito_vehkil'];

    $gps_pre = formato_numero($result['data']['tb_credito_gpspre']);
    $str_pre = formato_numero($result['data']['tb_credito_strpre']);
    $soa_pre = formato_numero($result['data']['tb_credito_soapre']);
    $gas_pre = formato_numero($result['data']['tb_credito_gaspre']);
    $otr_pre = formato_numero($result['data']['tb_credito_otrpre']);

    $vehiculo_est = $result['data']['tb_credito_vehest'];
    $vehiculo_ent = $result['data']['tb_credito_vehent'];
    $vehiculo_parele = $result['data']['tb_credito_parele'];
    $vehiculo_solreg = $result['data']['tb_credito_solreg'];
    $vehiculo_fecsol = mostrar_fecha($result['data']['tb_credito_fecsol']);

    //vehiculo
    $vehiculo_pre = formato_numero($result['data']['tb_credito_vehpre']);
    $vehiculo_pre = $vehiculo_pre + $gps_pre + $str_pre + $soa_pre + $gas_pre + $otr_pre;

    $vehmar_id = $result['data']['tb_vehiculomarca_id'];
    $vehmar_nom = $result['data']['tb_vehiculomarca_nom'];

    $vehmod_id = $result['data']['tb_vehiculomodelo_id'];
    $vehmod_nom = $result['data']['tb_vehiculomodelo_nom'];

    $vehcla_id = $result['data']['tb_vehiculoclase_id'];
    $vehcla_nom = $result['data']['tb_vehiculoclase_nom'];

    $vehtip_id = $result['data']['tb_vehiculotipo_id'];
    $vehtip_nom = $result['data']['tb_vehiculotipo_nom'];

    $vehiculo_pla = $result['data']['tb_credito_vehpla'];
    $vehiculo_mot = $result['data']['tb_credito_vehsermot'];
    $vehiculo_cha = $result['data']['tb_credito_vehsercha'];
    $vehiculo_ano = $result['data']['tb_credito_vehano'];
    $vehiculo_col = $result['data']['tb_credito_vehcol'];
    $vehiculo_numpas = $result['data']['tb_credito_vehnumpas'];
    $vehiculo_numasi = $result['data']['tb_credito_vehnumasi'];
    $vehiculo_kil = $result['data']['tb_credito_vehkil'];
    $vehiculo_zona_id = $result['data']['tb_zonaregistral_id'];

    $vehiculo_mode = $result['data']['tb_credito_vehmode'];
    $vehiculo_cate = $result['data']['tb_credito_vehcate'];
    $vehiculo_comb = $result['data']['tb_credito_vehcomb'];
    $credito_valtas = $result['data']['tb_credito_valtas'];
    $credito_valrea = $result['data']['tb_credito_valrea'];
    $credito_valgra = $result['data']['tb_credito_valgra'];

    $credito_subgar = $result['data']['tb_credito_subgar'];
    $credito_ini = formato_numero($result['data']['tb_credito_ini']);
    $credito_tipini = intval($result['data']['tb_credito_tipini']); //1 tabla de ingreso, 2 tabla de deposito
    $credito_idini = intval($result['data']['tb_credito_idini']);
    $transferente_id = intval($result['data']['tb_transferente_id']);
    $credito_monedache = intval($result['data']['tb_credito_monedache']);
  }
$result = NULL;

$result = $oRepresentante->mostrarUno($representante_id);
  if($result['estado'] == 1){
    $representante_nom = mb_strtoupper($result['data']['tb_representante_nom'], 'UTF-8');
    $representante_estciv = mb_strtoupper($result['data']['tb_representante_estciv'], 'UTF-8');
    $representante_dni = $result['data']['tb_representante_dni'];
    $representante_dir = mb_strtoupper($result['data']['tb_representante_dir'], 'UTF-8'); //dirección
    $representante_den = mb_strtoupper($result['data']['tb_representante_den'], 'UTF-8'); // denominacion: Gerente, SUb, etc
  }
$result = NULL;
// FIN REPRESENTANTE

list($credito_dia,$credito_mes,$credito_anio) = explode('-', $credito_feccre);

if($moneda_id == 1){
  $moneda = 'S/.';
}else{
  $moneda = 'US$';
}

//ubigeo del cliente
if($cliente_ubige  >0){
  $result = $oUbigeo->mostrarUbigeo($cliente_ubige);
    if($result['estado'] == 1){
      $cliente_dep = $result['data']['Departamento'];
      $cliente_pro = $result['data']['Provincia'];
      $cliente_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

if($cliente_emp_ubige > 0){
  $result = $oUbigeo->mostrarUbigeo($cliente_emp_ubige);
    if($result['estado'] == 1){
      $empresa_dep = $result['data']['Departamento'];
      $empresa_pro = $result['data']['Provincia'];
      $empresa_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

//obtenemos información d<b>EL CLIENTE</b>, sus nombres, su esposa(o), copropietario o apoderado
$estado_civil = 'SOLTERO(A)';
if($cliente_estciv == 2) $estado_civil = 'CASADO(A)';
if($cliente_estciv == 3) $estado_civil = 'DIVORCIADO(A)';
if($cliente_estciv == 4) $estado_civil = 'VIUDO(A)';

$esposa = ''; $apoderado = '';
$clientes = '<b>EL(LA) SR(RES) '.$cliente_nom.'</b>, IDENTIFICADO CON DNI N° <b>'.$cliente_doc.'</b>, ESTADO CIVIL '.$estado_civil.' CON DOMICILIO EN <b>'.trim($cliente_dir).'</b>, DEL DISTRITO DE <b>'.$cliente_dis.'</b>, PROVINCIA DE <b>'.$cliente_pro.'</b> Y DEPARTAMENTO DE <b>'.$cliente_dep.'</b>';

//si <b>EL CLIENTE</b> es casado
if($cliente_estciv == 2){
  $esposa = devolver_datos_esposa_apoderado($cliente_id, $cliente_bien, $cliente_firm, 1); //parametro 1 CONYUGAL
  $clientes = $clientes.' '.$esposa;
}

$apoderado = devolver_datos_esposa_apoderado($cliente_id, $cliente_bien, $cliente_firm, 3); //parametro 3 apoderado

if($apoderado != '')
  $clientes = $clientes.', '.$apoderado;

//verificar si <b>EL CLIENTE</b> tiene co-propietarios
$copro = devolver_datos_copropietarios($cliente_id);

if($copro != '')
  $clientes .= ' '.$copro;

$clientes = str_replace(' ;', ';', $clientes);

//LOS DATOS DEL VENDEDOR CAMBIAN CUANDO <b>EL CLIENTE</b> ES PERSONA JURÍDICA
if(intval($cliente_tip) == 2){
  $result = $oZona->mostrarUno($cliente_zon_id);
    if($result['estado'] == 1){
      $cliente_zon_nom = trim($result['data']['tb_zona_nom']);
    }
  $result = NULL;

  $clientes = '<b> '.$cliente_emprs.'</b>, IDENTIFICADA CON R.U.C <b>'.$cliente_empruc.'</b>, CON DOMICILIO EN <b>'.$cliente_empdir.'</b>, DEL DISTRITO DE <b>'.$empresa_dis.'</b>, PROVINCIA DE <b>'.$empresa_pro.'</b>, DEL DEPARTAMENTO DE <b>'.$empresa_dep.'</b>; DEBIDAMENTE REPRESENTADA POR SU '.$cliente_empger.' EL <b>SR(A). '.$cliente_nom.'</b>, IDENTIFICADO CON D.N.I. <b>'.$cliente_doc.'</b> SEGÚN PODERES INSCRITOS EN LA PARTIDA REGISTRAL  N° <b>'.$cliente_numpar.'</b> DEL REIGISTRO DE PERSONAS JURÍDICAS DE LA '.$cliente_zon_nom.', CON DOMICILIO EN <b>'.trim($cliente_dir).'</b>, DEL DISTRITO DE <b>'.$cliente_dis.'</b>, PROVINCIA DE <b>'.$cliente_pro.'</b>, DEL DEPARTAMENTO DE <b>'.$cliente_dep.'</b>';
}

//información de la zona registral
if(intval($vehiculo_zona_id) <= 0)
  $vehiculo_zona_id = 1;

$result = $oZona->mostrarUno($vehiculo_zona_id);
  if($result['estado'] == 1)
    $zona_nom = strtoupper(trim($result['data']['tb_zona_nom']));
$result = NULL;

//detalle de las cuotas del credito nuevo, es decir de la ADENDA 2 ya que es un credito ASIVEH
$result = $oCuotadetalle->mostrar_una_cuotadetalle_por_creditoid($credito_id, 3);
  if($result['estado'] == 1) {
    $cuotadetalle_fec = mostrar_fecha($result['data']['tb_cuotadetalle_fec']);
    $cuotadetalle_cuo = $result['data']['tb_cuotadetalle_cuo'];
  }
$result = NULL;

$result = $oCuota->primera_cuota_por_credito(3, $credito_id);
  if($result['estado'] == 1) {
    $cuota_fec = mostrar_fecha($value['tb_cuota_fec']);
    $cuota_cuo = $value['tb_cuota_cuo'];
    $cuota_int = $value['tb_cuota_int'];
  }
$result = NULL;

$fec_width = 'width="44%"';
if($cuotasubperiodo_id == 1){
  $cols = 1;
  $name_per = "MENSUAL";
  $fec_width = 'width="11%"';
}
if($cuotasubperiodo_id == 2){
  $cols = 2;
  $name_per = "QUINCENAL";
  $fec_width = 'width="22%"';
}
if($cuotasubperiodo_id == 3){
  $cols = 4;
  $name_per = "SEMANAL";
}

//CRONOGRAMA SEA FIJA O LIBRE
$cuota_detalle = 'FALTA CRONOGRAMA, PARA ELLO DEBE ESTAR DESEMBOLSADO EL CRÉDITO';
if($cuotatipo_id == 4){
  //4 ya que es un credito HIPOTECARIO
  $result = $oCuota->filtrar(3, $credito_id); //? 3 representa CREDITO GARVEH
    if($result['estado'] == 1) {
      $cuota_detalle = '
        <table cellpadding="2" style="width: 100%; border: 6px #000000 solid;">
          <tr>
            <th align="center" colspan="'.$cols.'" '.$fec_width.' style="background-color: green; color: white;">Fecha de Pago Acordado</th>
            <th align="center" width="7%" style="background-color: green; color: white;">N° de cuota</th>
            <th align="center" style="background-color: green; color: white;">Capital del Periodo</th>
            <th align="center" style="background-color: green; color: white;">Amortización del Periodo</th>
            <th align="center" width="11%" style="background-color: green; color: white;">Interés Compensatorio del Periodo</th>
            <th align="center" style="background-color: green; color: white;">Prorrateo</th>';
            if($cuosubper_id == 2)
              $cuota_detalle .='<th align="center" width="11%" style="background-color: green; color: white;">CUOTA QUINCENAL</th>';
            if($cuosubper_id == 3)
              $cuota_detalle .='<th align="center" width="11%" style="background-color: green; color: white;">CUOTA SEMANAL</th>';
            $cuota_detalle .='<th align="center" width="11%" style="background-color: green; color: white;">IGV</th>';
            $cuota_detalle .='<th align="center" width="11%" style="background-color: green; color: white;">GPS</th>';
            $cuota_detalle .='<th align="center" width="11%" style="background-color: green; color: white;">CUOTA TOTAL</th>
          </tr>
          ';
      
      $credito_total = 0;

      foreach ($result['data'] as $key => $value) {
        if($value['tb_moneda_id'] == 1){
          $moneda_cuo = 'S/. ';
        }
        if($value['tb_moneda_id'] == 2){
          $moneda_cuo = '$ ';
        }

        $total_adicionales += $value['tb_cuota_pregps'];

        $cuota_detalle .='<tr>';
          $result2 = $oCuotadetalle->filtrar($value['tb_cuota_id']);
            if($result2['estado'] == 1){
              foreach ($result2['data'] as $key => $value2) {
                $cuotadetalle_cuo = $value2['tb_cuotadetalle_cuo'];
                $cuotadetalle_fec = mostrar_fecha($value2['tb_cuotadetalle_fec']);
                  
                $cuota_detalle .= '<td align="center" width="11%">'.$cuotadetalle_fec.'</td>';
              }
            }
          $result2 = NULL;

          $interes_igv = formato_numero($value['tb_cuota_int']) - (formato_numero($value['tb_cuota_int']) / 1.18);
          $interes_sin_igv = $value['tb_cuota_int'] - $interes_igv;

          $cuota_detalle .='
          <td align="center" width="7%">'.$value['tb_cuota_num'].'</td>
          <td align="center">'.$moneda_cuo.mostrar_moneda($value['tb_cuota_cap']).'</td>
          <td align="center">'.$moneda_cuo.mostrar_moneda($value['tb_cuota_amo']).'</td>
          <td align="center" width="11%">'.$moneda_cuo.mostrar_moneda($interes_sin_igv).'</td>
          <td align="center">'.$moneda_cuo.mostrar_moneda($value['tb_cuota_pro']).'</td>';
          if($cuosubper_id == 2)
            $cuota_detalle .='<td width="11%">'.$moneda_cuo.mostrar_moneda($cuotadetalle_cuo).'</td>';
          if($cuosubper_id == 3)
            $cuota_detalle .='<td width="11%">'.$moneda_cuo.mostrar_moneda($cuotadetalle_cuo).'</td>';
          $cuota_detalle .='<td align="right" width="11%">'.$moneda_cuo.mostrar_moneda($interes_igv).'</td>';
          $cuota_detalle .='<td align="right">'.$moneda_cuo.mostrar_moneda($value['tb_cuota_pregps']).'</td>';
          $cuota_detalle .='<td align="right" width="11%">'.$moneda_cuo.mostrar_moneda($value['tb_cuota_cuo']).'</td>
        </tr>';

        $credito_total += $value['tb_cuota_cuo'];
      }
      $cuota_detalle .='
      </table>';
    }
  $result = NULL;

  $interes_total = floatval($credito_total) - floatval($credito_preaco) - floatval($total_adicionales);
  $cuota_detalle .= '
  	<p></p>
    <p align="justify">7. DETALLE:</p>
    <table cellpadding="2" style="width: 100%; border: 6px #000000 solid;">
      <tr>
        <th align="center" style="background-color: green; color: white;">N° de Cuotas</th>
        <th align="center" style="background-color: green; color: white;">Capital</th>
        <th align="center" style="background-color: green; color: white;">Interés Total</th>
        <th align="center" style="background-color: green; color: white;">GPS Total</th>
        <th align="center" style="background-color: green; color: white;">Total a Pagar en '.$credito_numero_cuo.' meses</th>
      </tr>
      <tr>
        <td align="center">'.$credito_numero_cuo.'</td>
        <td align="center">'.$moneda.mostrar_moneda($credito_preaco).'</td>
        <td align="center">'.$moneda.mostrar_moneda($interes_total).'</td>
        <td align="center">'.$moneda.mostrar_moneda($total_adicionales).'</td>
        <td align="center">'.$moneda.mostrar_moneda($credito_total).'</td>
      </tr>
    </table>
  ';
}

//* EN CASO SELECCIONEN UN CRONOGRAMA DE TIPO LIBRE, LA CUAL NO ESTÁ IMPLEMENTADA
if($cuotatipo_id == 3){
  $cuota_detalle = '
    <table cellpadding="2" border="1">
      <tr style="background-color:green; color: white;">
        <th align="center">NO TENEMOS UNA IMPLEMENTACIÓN DE CRONOGRAMA LIBRE</th>
      </tr>
    </table>';
}

$moneda_cheque = 'S/.';
if(intval($credito_monedache) == 2)
  $moneda_cheque = 'US$';

// VAMOS A REALIZAR EL CAMBIO DE MONERA EN CASO EL CRÉDITO SEA EN UN TIPO DE MONEDA Y LUEGO EL CHEQUE SEA EN OTRO TIPO
$texto_garantizado = '<b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>';
if(intval($credito_monedache) != intval($moneda_id))
  $texto_garantizado = '<b>'.$moneda_cheque.' '.mostrar_moneda($credito_montoche).' ('.numtoletras($credito_montoche, $credito_monedache).')</b> AL TIPO DE CAMBIO S/. <b>'.$credito_tc_2.'</b> EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>';

list($cuota_dia, $cuota_mes, $cuota_anio) = explode('-', $cuota_fec);
$clausula_adicional = '';
$anexo_adicional = $texto_garantizado;
$iniciales = '';
//OBTENER INFORMACIÓN DE LOS CHEQUES PARA LA MINUTA
$cheque = resaltar_texto('INGRESE REGISTRO DE CHEQUE O CHEQUES');

$result = $oCheque->mostrar_credito_id($credito_id, 3); //3 por el tipo de criedo GARVEH
  $num_cheques = count($result['data']);
  if($num_cheques == 1){
    $cheque_tip = intval($result['data'][0]['tb_cheque_tip']); //1 cheque de gerencia, 2 deposito bancario
    $cheque_tipcam = floatval($result['data'][0]['tb_cheque_tipcam']);
    $cheque_fec = mostrar_fecha($result['data'][0]['tb_cheque_fec']);
    $cheque_moneda_id = intval($result['data'][0]['tb_moneda_id']);
    $cheque_mon = floatval($result['data'][0]['tb_cheque_mon']);
    $cheque_num = $result['data'][0]['tb_cheque_num'];
    $cheque_ban = $result['data'][0]['tb_cheque_ban'];
    $equivalente = '';

    $texto_moneda_che = 'S/.'; 
    if($cheque_moneda_id == 2)
      $texto_moneda_che = 'US$'; 

    if(intval($moneda_id) == 1 && intval($cheque_moneda_id) == 2){
      $monto_equi = $cheque_mon * $cheque_tipcam;
      $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>,';
    }
    if(intval($moneda_id) == 2 && intval($cheque_moneda_id) == 1){
      $monto_equi = $cheque_mon / $cheque_tipcam;
      $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>,';
    }

    if($cheque_tip == 1){
      //$cheque = '01 CHEQUE BANCARIO POR LA SUMA DE <b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA <b>'.$cheque_fec.'</b>, CON N° <b>'.$cheque_num.'</b> Y EN BANCO <b>'.$cheque_ban.'</b>'.$equivalente;
      $cheque = ' <b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b>, ENTREGADO MEDIANTE 01 CHEQUE DE GERENCIA NO NEGOCIABLE N° '.$cheque_num.', EMITIDO POR EL BANCO <b>'.$cheque_ban.'</b> ASCENDENTE A LA SUMA DE <b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA '.$cheque_fec.','.$equivalente;
    }
    if($cheque_tip == 2){
      $cheque = '01 DEPÓSITO BANCARIO POR LA SUMA DE <b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA <b>'.$cheque_fec.'</b>, CON N° <b>'.$cheque_num.'</b> Y EN BANCO <b>'.$cheque_ban.'</b>'.$equivalente;
    }
  }

  if($num_cheques > 1){
    $cheque = str_pad($num_cheques, 2, "0", STR_PAD_LEFT).' CHEQUES BANCARIOS POR LA SUMA DE: ';

    foreach ($result['data'] as $key => $value) {
      $cheque_tip = intval($value['tb_cheque_tip']); //1 cheque de gerencia, 2 deposito bancario
      $cheque_tipcam = formato_numero($value['tb_cheque_tipcam']);
      $cheque_fec = mostrar_fecha($value['tb_cheque_fec']);
      $cheque_moneda_id = intval($value['tb_moneda_id']);
      $cheque_mon = formato_numero($value['tb_cheque_mon']);
      $cheque_num = $value['tb_cheque_num'];
      $cheque_ban = $value['tb_cheque_ban'];
      $equivalente = '';

      $texto_moneda_che = 'S/.'; 
      if($cheque_moneda_id == 2)
        $texto_moneda_che = 'US$'; 

      if(intval($moneda_id) == 1 && intval($cheque_moneda_id) == 2){
        $monto_equi = $cheque_mon * $cheque_tipcam;
        $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>';
      }
      if(intval($moneda_id) == 2 && intval($cheque_moneda_id) == 1){
        $monto_equi = $cheque_mon / $cheque_tipcam;
        $equivalente = ' EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cheque_tipcam.'</b>';
      }

      $cheque .= '<b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> CON FECHA <b>'.$cheque_fec.'</b>, CON N° <b>'.$cheque_num.'</b> Y EN BANCO <b>'.$cheque_ban.'</b>'.$equivalente.', ';

      if($cheque_tip == 2){
        $cheque .= '<b>'.$texto_moneda_che.' '.mostrar_moneda($cheque_mon).' ('.numtoletras($cheque_mon, $cheque_moneda_id).')</b> DEPOSITADOS A LA CUENTA DEL TRANSFERENTE';
      }
    }
    $cheque = trim($cheque, ', ');
  }
$result = NULL;

if($credito_subgar == 'GARMOB COMPRA TERCERO'){

  $result = $oTransferente->mostrarUno($transferente_id);
    if($result['estado'] == 1){
      $transferente_doc = $result['data']['tb_transferente_doc'];
      $transferente_nom = $result['data']['tb_transferente_nom'];
    }
  $result = NULL;

  $anexo_adicional = $cheque.' A NOMBRE DEL TRANSFERENTE '.$transferente_nom.' FINANCIADOS CON LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE SAC, UTILIZADOS PARA LA ADQUISICIÓN DEL VEHICULO MATERIA DE LA PRESENTE GARANTIA MOBILIARIA; DE CONFORMIDAD Y A SOLICITUD <b>DEL CLIENTE / DEPOSITARIO.</b>';

  $inicial_ingresada = 0;

  $result = $oCreditoinicial->listar_creditoiniciales($credito_id, 3); //3 por el tipo de criedo GARVEH
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $creditoinicial_tipini = intval($value['tb_creditoinicial_tipini']); //1 efectivo, 2 deposito, 3 cuenta externo (efec, depo)
        $creditoinicial_idini = intval($value['tb_creditoinicial_idini']); //tb_ingreso, tb_deposito, tb_externo

        if($creditoinicial_tipini == 1){ //ENTREGADO A NUESTRA CAJA EN EFECTIVO
          $result2 = $oIngreso->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $ingreso_fec = mostrar_fecha($result2['data']['tb_ingreso_fec']);
              $ingreso_det = $result2['data']['tb_ingreso_det'];
              $ingreso_imp = $result2['data']['tb_ingreso_imp'];
              $ingreso_mon_id = $result2['tb_moneda_id'];

              $result3 = $oMonedacambio->consultar(fecha_mysql($ingreso_fec));
                if($result3['estado'] == 1)
                  $cambio = $result3['data']['tb_monedacambio_val'];
              $result3 = NULL;

              $ingreso_moneda = 'S/.';
              if($ingreso_mon_id == 2)
                $ingreso_moneda = 'US$';

              if($ingreso_mon_id == 1 && $moneda_id == 2){
                $ingreso_equivalente = formato_numero($ingreso_imp / $cambio); //sin comas
                $inicial_ingresada += $ingreso_equivalente;

                $iniciales .= '<b>'.$ingreso_moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $ingreso_mon_id).')</b> EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($ingreso_equivalente).' ('.numtoletras($ingreso_equivalente, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cambio.'</b>, EN LA FECHA DE <b>'.$ingreso_fec.'</b> ENTREGADO AL TRANSFERENTE EN EFECTIVO, ';
              }
              if($ingreso_mon_id == 2 && $moneda_id == 1){
                $ingreso_equivalente = formato_numero($ingreso_imp * $cambio); //sin comas
                $inicial_ingresada += $ingreso_equivalente;

                $iniciales .= '<b>'.$ingreso_moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $ingreso_mon_id).')</b> EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($ingreso_equivalente).' ('.numtoletras($ingreso_equivalente, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$cambio.'</b>, EN LA FECHA DE <b>'.$ingreso_fec.'</b> ENTREGADO AL TRANSFERENTE EN EFECTIVO, ';
              }
              if($ingreso_mon_id == $moneda_id){
                $inicial_ingresada += $ingreso_imp;
                $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $moneda_id).')</b> AL TRANSFERENTE EN EFECTIVO, CON ANTERIORIDAD A LA FIRMA DE LA PRESENTE ACTA ';
              }
            }
          $result2 = NULL;
        }
        if($creditoinicial_tipini == 2){ //ENTREGADO A NUESTRA CAJA EN DEPOSITO
          $result2 = $oDeposito->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $deposito_fec = mostrar_fecha($result2['data']['tb_deposito_fec']);
              $deposito_num = $result2['data']['tb_deposito_num'];
              $deposito_mon = $result2['data']['tb_deposito_mon'];
              $inicial_ingresada += $deposito_mon;

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($deposito_mon).' ('.numtoletras($deposito_mon, $moneda_id).')</b> DEPOSITADOS EN CUENTA BANCARIA DEL TRANSFERENTE CON FECHA <b>'.$deposito_fec.'</b>, N° DE OPERACION <b>'.$deposito_num.'</b>, EN BANCO DE CREDITO DEL PERU, ';
            }
          $result2 = NULL;
        }
        if($creditoinicial_tipini == 3){ //ENTREGADO AL TRANSFERENTE, TERCERO
          $result2 = $oExterno->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $externo_tip = $result2['data']['tb_externo_tip']; // 1 efectivo a transferente, 2 deposito a transferente
              $externo_moneda = $result2['data']['tb_moneda_id']; //1 soles, 3 dolares
              $externo_tipcam = floatval($result2['data']['tb_externo_tipcam']);
              $externo_fec = mostrar_fecha($result2['data']['tb_externo_fec']);
              $externo_mon = floatval($result2['data']['tb_externo_mon']);              
              $externo_num = $result2['data']['tb_externo_num'];
              $externo_ban = $result2['data']['tb_externo_ban'];
              $transferente_id2 = intval($result2['data']['tb_transferente_id2']);
              $equivalente = '';
              $texto_moneda = 'S/.';
              if(intval($externo_moneda) == 2)
                $texto_moneda = 'US$';

              $transferente_n2 = '';
              if($transferente_id2 > 0){
                $result3 = $oTransferente->mostrarUno($transferente_id2);
                  if($result3['estado'] == 1){
                    $transferente_doc_t2 = $result3['data']['tb_transferente_doc'];
                    $transferente_nom_t2 = $result3['data']['tb_transferente_nom'];
                  }
                $result3 = NULL;

                $anexo_adicional = ' ENTREGADOS MEDIANTE '.$cheque.'; A NOMBRE DEL TRANSFERENTE '.$transferente_nom.' Y '.$transferente_nom_t2.' Y FINANCIADOS CON LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE SAC, UTILIZADOS PARA LA ADQUISICIÓN DEL VEHICULO MATERIA DE LA PRESENTE GARANTIA MOBILIARIA.';
                $transferente_n2 = ' Y '.$transferente_nom_t2;
              }

              if(intval($moneda_id) == 1 && intval($externo_moneda) == 2){
                $monto_equi = $externo_mon * $externo_tipcam;
                
                $equivalente = 'EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$externo_tipcam.'</b>, ';
              }
              if(intval($moneda_id) == 2 && intval($externo_moneda) == 1){
                $monto_equi = $externo_mon / $externo_tipcam;
                
                $equivalente = 'EQUIVALENTE A <b>'.$moneda.' '.mostrar_moneda($monto_equi).' ('.numtoletras($monto_equi, $moneda_id).')</b>, CONSIDERANDO EL TIPO DE CAMBIO DE <b>'.$externo_tipcam.'</b>, ';
              }
              //AHORA VEAMOS SI LA INICIAL AL EXTERNO LE HIZO EN EFECTIVO O DEPOSITO
              if(intval($externo_tip) == 1){
                $iniciales .= '<b>'.$texto_moneda.' '.mostrar_moneda($externo_mon).' ('.numtoletras($externo_mon, $externo_moneda).')</b> '.$equivalente.' AL TRANSFERENTE CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR EN EFECTIVO';
              }
              if(intval($externo_tip) == 2){
                // $iniciales .= '<b>'.$texto_moneda.' '.mostrar_moneda($externo_mon).' ('.numtoletras($externo_mon, $externo_moneda).')</b> DEPOSITADOS EN CUENTA BANCARIA DEL TRANSFERENTE CON FECHA <b>'.$externo_fec.'</b>, N° DE OPERACION <b>'.$externo_num.'</b>, EN BANCO '.$externo_ban.', ';
                //$inicial_ingresada += $externo_mon;
                $iniciales .= '<b>'.$texto_moneda.' '.mostrar_moneda($externo_mon).' ('.numtoletras($externo_mon, $externo_moneda).')</b> CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR CON RECURSOS PROPIOS <b>DEL CLIENTE</b>';
              }

              $inicial_ingresada += $externo_mon;
              
            }
          $result2 = NULL;
        }
      }
      $iniciales = trim($iniciales, ', ');
    }
    else
      $iniciales = resaltar_texto('INGRESE REGISTRO DE MONTOS INICIALES');;
  $result = NULL;

  // $clausula_adicional = ' EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').', ANTE EL NOTARIO '.resaltar_texto('XXXXXXXXXX').' EL '.resaltar_texto('XX/'.date('m/Y')).'; POR LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_vent).' ('.numtoletras($credito_vent, $moneda_id).')</b>; CANCELANDO LA SUMA DE: '.$iniciales.' Y CON RECURSOS PROPIOS DEL CLIENTE; Y LA DIFERENCIA, LA SUMA DE '.$cheque.' GIRADO A FAVOR DEL TRANSFERENTE '.$transferente_nom.''.$transferente_n2.', Y FINANCIADA CON EL <b>CLIENTE / DEPOSITARIO</b> CON LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE SAC, GENERANDO LA PRESENTE GARANTÍA MOBILIARIA A FAVOR <b>DEL ACREEDOR</b>; SEGÚN CLAUSULAS CONTENIDAS EN PRESENTE ESCRITURA PUBLICA DE GARANTIA MOBILIARIA,';
  $restante = $credito_vent - $inicial_ingresada;
  $clausula_adicional = ' EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').', ANTE EL NOTARIO '.resaltar_texto('XXXXXXXXXX').' EL '.resaltar_texto('XX/'.date('m/Y')).'; POR LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_vent).' ('.numtoletras($credito_vent, $moneda_id).')</b>; CANCELANDO LA SUMA DE: '.$iniciales.'; Y LA DIFERENCIA, LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($restante).' ('.numtoletras($restante, $moneda_id).')</b>, FINANCIADA CON LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE SAC (MEDIANTE DEPOSITO BANCARIO Y/O CHEQUE DE GERENCIA NO NEGOCIABLE, EL CUAL SERÁ INSERTADO EN LA ESCRITURA PÚBLICA QUE ORIGINA LA ELEVACION DE LA PRESENTE MINUTA), GENERANDO O CONSTITUYENDO EL MENCIONADO MUTUO RECONOCIDO EN GARANTÍA SEGÚN CLÁUSULAS CONTENIDAS EN LA PRESENTE ESCRITURA PÚBLICA DE GARANTÍA MOBILIARIA A FAVOR DEL ACREEDOR SOBRE EL BIEN MUEBLE (DESCRITO EN EL ANEXO N° 1); EL CLIENTE DECLARA QUE';

}

if($credito_subgar == 'GARMOB IPDN VENTA' || $credito_subgar == 'GARMOB IPDN VENTA CON RD'){
  $result = $oCreditoinicial->listar_creditoiniciales($credito_id, 3); //3 por el tipo de criedo GARVEH
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $creditoinicial_tipini = intval($value['tb_creditoinicial_tipini']); //1 efectivo, 2 deposito, 3 cuenta externo (efec, depo)
        $creditoinicial_idini = intval($value['tb_creditoinicial_idini']); //tb_ingreso, tb_deposito, tb_externo
        
        if($creditoinicial_tipini == 1){ //ENTREGADO A NUESTRA CAJA EN EFECTIVO
          $result2 = $oIngreso->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $ingreso_fec = mostrar_fecha($result2['data']['tb_ingreso_fec']);
              $ingreso_det = $result2['data']['tb_ingreso_det'];
              $ingreso_imp = $result2['data']['tb_ingreso_imp'];

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $moneda_id).')</b> AL TRANSFERENTE EN EFECTIVO, CON ANTERIORIDAD A LA FIRMA DE LA PRESENTE ACTA ';
              //$iniciales .= '<b>'.$moneda.' '.mostrar_moneda($ingreso_imp).' ('.numtoletras($ingreso_imp, $moneda_id).')</b> EN LA FECHA DE <b>'.$ingreso_fec.'</b> ENTREGADOS EN EFECTIVO, ';
            }
          $result2 = NULL;
        }
        if($creditoinicial_tipini == 2){ //ENTREGADO A NUESTRA CAJA EN DEPOSITO
          $result2 = $oDeposito->mostrarUno($creditoinicial_idini);
            if($result2['estado'] == 1){
              $deposito_fec = mostrar_fecha($result2['data']['tb_deposito_fec']);
              $deposito_num = $result2['data']['tb_deposito_num'];
              $deposito_mon = $result2['data']['tb_deposito_mon'];

              $iniciales .= '<b>'.$moneda.' '.mostrar_moneda($deposito_mon).' ('.numtoletras($deposito_mon, $moneda_id).')</b> DEPOSITADOS EN CUENTA BANCARIA <b>DEL ACREEDOR</b> CON FECHA <b>'.$deposito_fec.'</b>, N° DE OPERACION <b>'.$deposito_num.'</b>, EN BANCO DE CREDITO DEL PERU, ';
            }
          $result2 = NULL;
        }
      }
      $iniciales = trim($iniciales, ', ');
    }
    elseif ($credito_subgar == 'GARMOB IPDN VENTA CON RD') {
      $monto_saldo_venta = formato_numero($credito_vent - $credito_preaco);
    }
    else
      $iniciales = resaltar_texto('INGRESE REGISTRO DE MONTOS INICIALES');;
  $result = NULL;

  $clausula_adicional = ' EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').', ANTE EL NOTARIO '.resaltar_texto('XXXXXXXXXX').' EL '.resaltar_texto('XX/'.date('m/Y')).'; POR LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_vent).' ('.numtoletras($credito_vent, $moneda_id).')</b>; CANCELADO SEGUN LO ESTIPULADO EN LA MENCIONADA ACTA DE TRANSFERENCIA VEHICULAR DE LA SIGUIENTE MANERA: '.$iniciales.' CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').'; Y EL SALDO ASCENDENTE A LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>, SERÁN CANCELADOS EN UN PLAZO MÁXIMO DE <b>'.$credito_numero_cuo.' CUOTAS</b> MENSUALES ASCENDENTE CADA CUOTA EN LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($cuota_cuo).' ('.numtoletras($cuota_cuo, $moneda_id).')</b>, CON FECHA DE VENCIMIENTO CADA CUOTA LOS <b>'.$cuota_dia.' DÍAS</b> DE CADA MES, CONSTITUYENDO EL MENCIONADO SALDO EN UNA GARANTÍA MOBILIARIA A FAVOR <b>DEL ACREEDOR</b>; SEGÚN CLAUSULAS CONTENIDAS EN PRESENTE ESCRITURA PUBLICA DE GARANTIA MOBILIARIA,';

  /*if($credito_subgar == 'GARMOB IPDN VENTA CON RD'){
    $clausula_adicional = ' EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° XXXXX, ANTE EL NOTARIO VALDIVIA DEXTRE EL XX/11/2020; POR LA SUMA DE S/. 30,000.00 (TREINTA MIL CON 00/100 SOLES); CANCELANDO LA SUMA DE: S/ 2,571.71 CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR CON RECURSOS PROPIOS DEL CLIENTE Y LA DIFERENCIA LA SUMA DE S/. 27,428.29 (VEINTISIETE MIL CUATROCIENTOS VEINTIOCHO CON 29/100 SOLES), FINANCIADA CON LA EMPRESA INVERSIONES Y PRESTAMOS DEL NORTE SAC, (DEUDA RECONOCIDA MEDIANTE CONTRATO PRIVADO CON FIRMAS LEGALIZADAS E INSERTADO EN LA ESCRITURA PUBLICA QUE ORIGINA LA ELEVACION DE LA PRESENTE MINUTA) GENERANDO O CONSTITUYENDO LA MENCIONADA DEUDA RECONOCIADA EN GARANTIA SEGÚN CLAUSULAS CONTENIDAS EN LA PRESENTE ESCRITURA PUBLICA DE GARANTIA MOBILIAIRIA A FAVOR DE EL ACREEDOR';
  }*/
}

if($credito_ini <= 0){
  $subdividido = '';
  if($cuosubper_id == 2)
    $subdividido = ' Y SUBDIVIDIDO EN <b>02 CUOTAS</b> QUINCENALES, LOS CUALES SE DETALLAN EN EL ANEXO 01';
  if($cuosubper_id == 3)
    $subdividido = ' Y SUBDIVIDIDO EN <b>04 CUOTAS</b> SEMANALES, LOS CUALES SE DETALLAN EN EL ANEXO 01';
  if($credito_subgar != 'REGULAR'){
    if ($credito_subgar == 'GARMOB IPDN VENTA CON RD') {
      $monto_saldo_venta = formato_numero($credito_vent - $credito_preaco);
    }
    $clausula_adicional = ' EL MISMO QUE HA SIDO ADQUIRIDO MEDIANTE ACTA DE TRANSFERENCIA VEHICULAR N° '.resaltar_texto('XXXXX').', ANTE EL NOTARIO '.resaltar_texto('XXXXXXXXXX').' EL '.resaltar_texto('XX/'.date('m/Y')).'; POR LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_vent).' ('.numtoletras($credito_vent, $moneda_id).')</b>; CANCELANDO LA SUMA DE: <b>'.$moneda.' '.mostrar_moneda($monto_saldo_venta).' ('.numtoletras($monto_saldo_venta, $moneda_id).')</b> CON ANTERIORIDAD A LA FIRMA DEL ACTA DE TRANSFERENCIA VEHICULAR CON RECURSOS PROPIOS DEL CLIENTE Y LA DIFERENCIA LA SUMA DE <b>'.$moneda.' '.mostrar_moneda($credito_preaco).' ('.numtoletras($credito_preaco, $moneda_id).')</b>, FINANCIADA CON LA EMPRESA INVERSIONES Y PRESTAMOS DEL NORTE SAC, (DEUDA RECONOCIDA MEDIANTE CONTRATO PRIVADO CON FIRMAS LEGALIZADAS E INSERTADO EN LA ESCRITURA PUBLICA QUE ORIGINA LA ELEVACION DE LA PRESENTE MINUTA) GENERANDO O CONSTITUYENDO LA MENCIONADA DEUDA RECONOCIDA EN GARANTIA SEGÚN CLAUSULAS CONTENIDAS EN LA PRESENTE ESCRITURA PUBLICA DE GARANTIA MOBILIARIA A FAVOR <b>DEL ACREEDOR</b>;';
  }
}

//SI EL CREDITO TIENE CARGAS, YA NO DEBE DERCIR (PRIMERA GARANTIA), SOLAMENTE LA BORRAMOS
$primera = 'PRIMERA Y PREFERENTE';
if(trim($credito_cargas) != "") $primera = '';

//TERMINA COPIA AQUIIIII
if($tipo_documento == 'pdf'){
  class MYPDF extends TCPDF {

  }
  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
    $pdf->SetTitle($title);
    $pdf->SetSubject('www.prestamosdelnortechiclayo.com');
    $pdf->SetKeywords('www.prestamosdelnortechiclayo.com');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(30, 20, 30);// left top right
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings
    $pdf->setLanguageArray($l);
    // ---------------------------------------------------------
    // add a page
  $pdf->AddPage('P', 'A4');
}

  $html = '

  	<p align="center"><b><u>MINUTA</u></b></p>
    <p></p>
    <p align="justify">Señor Notario:</p>
    <p align="justify"><b>SÍRVASE UD. EXTENDER EN SU REGISTRO DE ESCRITURA PÚBLICAS UNA DE CONSTITUCIÓN DE GARANTÍA MOBILIARIA</b> QUE CELEBRA DE UNA PARTE, '.$clientes.', DE AQUÍ EN ADELANTE <b>EL CLIENTE / EL DEPOSITARIO</b>; Y, DE OTRA PARTE, <b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, CON RUC Nº <b>20600752872</b>, CON DOMICILIO EN <b>AV. MARISCAL NIETO N° 480 EXT. A7 PRIMER PISO, DISTRITO DE CHICLAYO, PROVINCIA DE CHICLAYO Y DEPARTAMENTO DE LAMBAYEQUE</b>, DEBIDAMENTE REPRESENTADO POR SU '.$representante_den.' SR. <b>'.$representante_nom.'</b>, IDENTIFICADO CON <b>DNI Nº '.$representante_dni.'</b>, ESTADO CIVIL '.$representante_estciv.' CON DOMICILIO EN <b>'.$representante_dir.'</b>, SEGÚN PODERES INSCRITOS EN <b>LA PARTIDA Nº 11218951</b> DEL REGISTRO DE PERSONAS JURÍDICAS DE LA OFICINA REGISTRAL DE CHICLAYO, A QUIEN EN ADELANTE SOLO SE DENOMINARÁ <b>EL ACREEDOR</b>, EN LOS TÉRMINOS Y CONDICIONES SIGUIENTES:</p>

    <p><b>OBJETO, DECLARACIÓN DE PROPIEDAD Y PLAZO DEL CONTRATO</b></p>
    <p align="justify"><b>PRIMERO: EL CLIENTE</b>, DECLARA QUE MANTIENE ACTUALMENTE UNA O MÁS OBLIGACIONES CREDITICIAS A FAVOR <b>DEL ACREEDOR</b>, LAS MISMAS QUE SE SEÑALAN EN LA CLÁUSULA SEGUNDA POR LO QUE EN GARANTÍA DE ESTAS OBLIGACIONES EL CLIENTE AFECTA EN CALIDAD DE GARANTÍA MOBILIARIA, EL (LOS) BIEN (ES) QUE EN ADELANTE SE CONOCERÁ (N) COMO EL (LOS) BIEN (ES), AUN EN EL CASO DE SER MÁS DE UNO CUYA DESCRIPCIÓN FIGURA EN EL ANEXO N° 1 QUE FORMA PARTE DEL PRESENTE CONTRATO. </p>

    <p align="justify">SE PRECISA QUE LA GARANTÍA MOBILIARIA QUE <b>EL CLIENTE</b> CONSTITUYE POR ESTE CONTRATO A FAVOR <b>DEL ACREEDOR</b>, EN RESPALDO SUS OBLIGACIONES, TANTO LAS QUE EXISTEN A LA FECHA DE ESTE CONTRATO COMO DE AQUELLAS QUE PUDIERAN CONTRAER POSTERIORMENTE A FAVOR <b>DEL ACREEDOR</b>.</p>';
  
    if($credito_cargas != ""){
      $html .= '<p align="justify"><b>EL CLIENTE</b> DECLARA CON ARREGLO A LA LEY DE GARANTÍA MOBILIARIA Y LA LEY GENERAL DEL SISTEMA FINANCIERO, QUE EL BIEN ES DE SU EXCLUSIVA PROPIEDAD,'.$clausula_adicional.' SOBRE ESTE TIENE SU LIBRE DISPOSICIÓN Y USO Y QUE SOBRE EL MISMO PESA '.resaltar_texto($credito_cargas).'. ADICIONALMENTE, <b>EL CLIENTE</b> ASUME COMO OBLIGACIÓN DE NO HACER, EL DE NO TRANSFERIR, DISPONER, CEDER, GRAVAR O AFECTAR CON ALGÚN DERECHO EL BIEN QUE AFECTA EN GARANTÍA DE ESTE ACTO A FAVOR <b>DEL ACREEDOR</b>, SIN EMBARGO, EN CASO DE REALIZAR CUALQUIERA DE TALES ACTOS, <b>EL ACREEDOR</b> QUEDARÁ FACULTADO A DAR POR VENCIDOS TODOS LOS PLAZOS ESTABLECIDOS EN FAVOR <b>DEL CLIENTE</b>, QUIEN EN TAL CASO SE OBLIGA A PAGAR TODAS LA DEUDAS Y OBLIGACIONES GARANTIZADAS CON LA PRESENTE GARANTÍA MOBILIARIA, EN CASO DE NO PAGAR TALES DEUDAS, DARÁ LUGAR A QUE <b>EL ACREEDOR</b> EJECUTE LA GARANTÍA, QUE SE CONSTITUYE A FAVOR DEL PRESENTE CONTRATO, CONFORME AL ARTÍCULO 175, INCISOS 4 Y 5 DE LA LEY Nº26702, DE ACUERDO AL PROCEDIMIENTO DE VENTA EXTRAJUDICIAL SEÑALADA EN LA CLÁUSULA SEXTA, CONFORME AL ARTÍCULO 47º DE LA LEY DE GARANTÍA MOBILIARIA.</p>';
    }
    else{
      $html .= '<p align="justify"><b>EL CLIENTE</b> DECLARA CON ARREGLO A LA LEY DE GARANTÍA MOBILIARIA Y LA LEY GENERAL DEL SISTEMA FINANCIERO, QUE EL BIEN ES DE SU EXCLUSIVA PROPIEDAD,'.$clausula_adicional.' SOBRE EL BIEN TIENE SU LIBRE DISPOSICIÓN Y USO; SOBRE EL MISMO NO PESA NINGÚN GRAVAMEN, CARGA, LITIGIO, NI MEDIDA JUDICIAL O EXTRAJUDICIAL QUE LIMITE EN ALGÚN MODO SUS DERECHOS SOBRE SU LIBRE DISPOSICIÓN; ADICIONALMENTE, <b>EL CLIENTE</b> ASUME COMO OBLIGACIÓN DE NO HACER, EL DE NO TRANSFERIR, DISPONER, CEDER, GRAVAR O AFECTAR CON ALGÚN DERECHO EL BIEN QUE AFECTA EN GARANTÍA DE ESTE ACTO A FAVOR <b>DEL ACREEDOR</b>, SIN EL PREVIO CONSENTIMIENTO ESCRITO Y DE FECHA CIERTA <b>DEL ACREEDOR</b>; EL INCUMPLIMIENTO POR PARTE <b>DEL CLIENTE</b> CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO DE SU PARTE; EN CONSECUENCIA Y EN CASO DE REALIZAR CUALQUIERA DE TALES ACTOS, <b>EL ACREEDOR</b> QUEDARÁ FACULTADO A DAR POR VENCIDOS TODOS LOS PLAZOS ESTABLECIDOS EN FAVOR <b>DEL CLIENTE</b>, QUIEN EN TAL CASO SE OBLIGA A PAGAR TODAS LA DEUDAS Y OBLIGACIONES GARANTIZADAS CON LA PRESENTE GARANTÍA MOBILIARIA, EN CASO DE NO PAGAR TALES DEUDAS, DARÁ LUGAR A QUE <b>EL ACREEDOR</b> EJECUTE LA GARANTÍA, QUE SE CONSTITUYE A FAVOR DEL PRESENTE CONTRATO, CONFORME AL ARTÍCULO 175, INCISOS 4 Y 5 DE LA LEY Nº 26702, DE ACUERDO AL PROCEDIMIENTO DE VENTA EXTRAJUDICIAL SEÑALADA EN LA CLÁUSULA OCTAVA Y NOVENA, CONFORME AL ARTÍCULO 47º DE LA LEY DE GARANTÍA MOBILIARIA.</p>';
    }

  $html .='
    <p><b>OBLIGACIONES GARANTIZADAS:</b></p>

    <p align="justify"><b>SEGUNDO:</b> LA GARANTÍA MOBILIARIA QUE SOBRE EL BIEN OTORGA <b>EL CLIENTE</b> SERVIRÁ DE '.$primera.' GARANTÍA DE TODAS LAS DEUDAS Y OBLIGACIONES, PRESENTES O FUTURAS, <b>DEL CLIENTE</b> QUE EXPRESAMENTE SE SEÑALAN A CONTINUACIÓN, HASTA POR EL IMPORTE DE GRAVAMEN SEÑALADO EN EL ANEXO N° 1 O SU EQUIVALENTE EN OTRAS MONEDAS. LAS DEUDAS Y OBLIGACIONES QUE QUEDAN RESPALDADAS POR LA PRESENTE GARANTÍA DE PRIMER RANGO QUE SE CONSTITUYE SOBRE EL BIEN, SERÁN LAS QUE DE MODO EXPRESO SE ESPECIFICAN A CONTINUACIÓN:</p>

    <p align="justify">2.1 EL CRÉDITO A QUE SE REFIERE EN EL ACÁPITE 3 DEL ANEXO N° 1 QUE FORMA PARTE INTEGRANTE DE ESTA MINUTA; ASÍ COMO SUS INTERESES, COMISIONES, GASTOS, PRIMAS DE SEGUROS, COSTO -  INSTALACIÓN – MANTENIMIENTO DE GPS, QUE PUDIERA HABER PAGADO <b>EL ACREEDOR</b>, LOS COSTOS, COSTAS PROCESALES, PENALIDADES, TODOS LOS GASTOS DERIVADOS DE LA INCAUTACIÓN DEL BIEN Y DE SU TRANSFERENCIA COMO CONSECUENCIA DE LA EJECUCIÓN DE ESTA GARANTÍA, GASTOS DE TRASLADO, REPARACIÓN, MANTENIMIENTO, CUSTODIA Y CONSERVACIÓN DEL BIEN, HONORARIOS DE ABOGADOS DERIVADOS DEL PROCESO DE EJECUCIÓN JUDICIAL O EXTRAJUDICIAL DE ESTA GARANTÍA, INDEMNIZACIONES POR DAÑOS Y PERJUICIOS QUE EXISTAN O PUEDAN DEVENGARSE O EXISTIR EN EL FUTURO, EL IMPORTE DE LAS INDEMNIZACIONES DE LOS SEGUROS, Y CUALQUIER OTRO CONCEPTO ADICIONAL ESTABLECIDO EN EL PRESENTE ACTO Y/O EN CUALQUIER OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL <b>EL CLIENTE</b> ASUMA ALGUNA OBLIGACIÓN O DEUDA CON <b>EL ACREEDOR</b>.</p>
  
    <p align="justify">2.2 LAS DEUDAS Y OBLIGACIONES QUE ACTUALMENTE TIENE Y PUDIERA TENER EN EL FUTURO <b>EL CLIENTE</b> A FAVOR <b>DEL ACREEDOR</b> PROVENIENTE DE PAGARES Y/O DE CUALQUIER OTRA OBLIGACIÓN DE CUALQUIER NATURALEZA, EN MONEDA NACIONAL O EXTRANJERA.</p>
  
    <p align="justify">2.3 LAS DEUDAS Y OBLIGACIONES QUE SEAN O RESULTEN DE CARGO <b>DEL CLIENTE</b>, PROVENIENTE DE CONTRATOS DE CRÉDITO, LÍNEAS DE CRÉDITO, MUTUO, PRÉSTAMO, RECONOCIMIENTO DE DEUDA  U OTRAS MODALIDADES DE CRÉDITO DIRECTO O INDIRECTO O FINANCIAMIENTOS;</p>

    <p align="justify">2.4 LAS DEUDAS Y OBLIGACIONES RELACIONADAS QUE SEAN O RESULTANTES DE LA EMISIÓN O CONCESIÓN DE CARTAS FIANZAS, AVALES Y TODO OTRO CRÉDITO INDIRECTO O CONTINGENTE QUE <b>EL ACREEDOR</b> LE TENGA CONCEDIDO O CONCEDA EN EL FUTURO A <b>EL CLIENTE</b>.</p>
      
    <p align="justify">2.5 TODAS LAS OBLIGACIONES <b>DEL CLIENTE</b> QUE SEAN CONSECUENCIA DE UNA MODIFICACIÓN DE CUALQUIERA DE LOS TÉRMINOS CONTENIDOS EN ESTA MINUTA, O DE FINANCIAMIENTO, O AMORTIZACIÒN O REFINANCIAMIENTO O DE REPROGRAMACIÒN O LIQUIDACIÓN Y/O EN CUALQUIER OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL <b>EL CLIENTE</b> ASUMA ALGUNA OBLIGACIÓN O DEUDA CON <b>EL ACREEDOR</b>, INCLUYENDO MONTOS DE LAS CUOTAS, PLAZOS DE PAGO, TASAS DE INTERÉS Y CUALQUIER OTRO TÉRMINO Y CONDICIÓN ESTABLECIDO EN VIRTUD A CUALQUIER INSTRUMENTO COMPLEMENTARIO, ADENDA, ENMIENDA, MODIFICACIÓN Y/O ACLARACIÓN A ESTA MINUTA, Y/O A CUALQUIER OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL <b>EL CLIENTE</b> ASUMA ALGUNA OBLIGACIÓN O DEUDA CON <b>EL ACREEDOR</b></p>

    <p align="justify">2.6 LAS OTRAS DEUDAS Y OBLIGACIONES DISTINTAS A LAS INDICADAS EN LOS ACÁPITES ANTERIORES QUE EN LA FECHA PUEDA TENER <b>EL CLIENTE</b> FRENTE A <b>EL ACREEDOR</b>, EN CUALQUIERA DE SUS OFICINAS DEL PAÍS, SEAN EN MONEDA NACIONAL O EXTRANJERA, PROVENIENTES DE CUALQUIER CRÉDITO DIRECTO O INDIRECTO, INCLUYENDO AQUELLAS OBLIGACIONES ASUMIDAS ORIGINALMENTE POR <b>EL CLIENTE</b> EN FAVOR DE TERCEROS Y QUE HAYAN SIDO TRANSFERIDAS, CEDIDAS O ENDOSADAS A <b>EL ACREEDOR</b> Y/O QUE CUENTEN CON LA FIANZA O AVAL <b>DEL CLIENTE</b></p>

    <p align="justify">2.7 EL IMPORTE TOTAL ADEUDADO DE CARGO <b>DEL CLIENTE</b> QUE SE ENCUENTRE EN MORA O PENDIENTE DE PAGO, SERÁ DETERMINADO MEDIANTE LA SUMA DE LAS LIQUIDACIONES DE DICHAS OBLIGACIONES IMPAGAS, HECHAS POR <b>EL ACREEDOR</b> EN UNA FECHA DETERMINADA.</p>

    <p align="justify">2.8 LO DETALLADO EN LOS PÁRRAFOS PRECEDENTES NO ES LIMITATIVO SINO MERAMENTE ENUNCIATIVO, PUES ES EL PROPÓSITO <b>DEL CLIENTE</b> QUE LA CONSTITUCIÓN DE GARANTÍA MOBILIARIA CONSTITUIDA EN ESTE ACTO CUBRA CON TODA AMPLITUD LAS OBLIGACIONES ACTUALES Y FUTURAS QUE COMPETEN <b>AL CLIENTE</b> EN TANTO SEA CLIENTE <b>DEL ACREEDOR</b>.</p>
      
    <p align="justify">LA PRESENTE GARANTÍA MOBILIARIA SERVIRÁ IGUALMENTE DE GARANTÍA DE LAS PROPIAS OBLIGACIONES <b>DEL CLIENTE</b> QUE TENGA A LA FECHA O PUDIESE CONTRAER EN EL FUTURO A FAVOR <b>DEL ACREEDOR</b>, BAJO LAS MISMAS CONDICIONES Y AMPLITUD SEÑALADAS EN EL PRESENTE CONTRATO.</p>

    <p><b>DECLARACIONES / GARANTÍAS / OBLIGACIONES / COMPROMISOS DEL CLIENTE / DEPOSITARIO.-</b></p>

    <p align="justify"><b>TERCERO: EL CLIENTE / DEPOSITARIO</b> DECLARA, GARANTIZA, SE OBLIGA Y COMPROMETE FRENTE AL ACREEDOR QUE:</p>

    <p align="justify">3.1 NO HA OCURRIDO ALGÚN EVENTO NI EXISTE ALGUNA INVESTIGACIÓN, PROCEDIMIENTO O TRÁMITE ADMINISTRATIVO, ARBITRAL, JUDICIAL O EXTRAJUDICIAL QUE:</p>

      <p align="justify">-  AFECTE ADVERSAMENTE, O PUEDA AFECTAR ADVERSAMENTE, SU SITUACIÓN FINANCIERA, EL RESULTADO DE SUS OPERACIONES O EL CUMPLIMIENTO DE LAS OBLIGACIONES A SU CARGO DE ACUERDO CON ESTE INSTRUMENTO;</p>
      <p align="justify">-  LLEVE A UN CAMBIO SIGNIFICATIVO EN EL GIRO DE SU NEGOCIO;</p>
      <p align="justify">-  PONGA EN RIESGO LA VALIDEZ, LEGALIDAD Y EXIGIBILIDAD DE ESTE DOCUMENTO, O;</p>
      <p align="justify">-  IMPLIQUE QUE SE ENCUENTRE EN UN PROCESO DE INSOLVENCIA, PROCESO CONCURSAL O SIMILAR, O QUE SE ENCUENTRE AFECTO A ALGÚN PLAN DE PAGOS O CONVENIO SIMILAR, O SUJETO A ALGÚN OTRO MECANISMO QUE IMPLIQUE LA REESTRUCTURACIÓN O REFINANCIACIÓN DE SUS PASIVOS, O DECLARE LA DISOLUCIÓN, LIQUIDACIÓN O QUIEBRA <b>DEL CLIENTE</b>, O DEBIDO A CUALQUIER CIRCUNSTANCIA PROPIA O AJENA, <b>EL CLIENTE</b> HAYA SUSPENDIDO EL PAGO CORRIENTE DE SUS OBLIGACIONES DEBIDAS Y EXIGIBLES.</p>

    <p align="justify">3.2 NO SE ENCUENTRA SUJETO A LA ADMINISTRACIÓN, CONTROL O INTERVENCIÓN DE TERCEROS, POR ORDEN JUDICIAL O DE AUTORIDAD COMPETENTE.-</p>

    <p align="justify">3.3 LA CELEBRACIÓN DEL PRESENTE ACTO Y EL CUMPLIMIENTO DE SUS OBLIGACIONES:</p>

      <p align="justify">-  NO CAUSARÁ EL INCUMPLIMIENTO DE ALGÚN CONTRATO, ACTO, LÍMITE, RESTRICCIÓN U OBLIGACIÓN ASUMIDA POR <b>EL CLIENTE</b>;</p>
      <p align="justify">-  NO CONTRAVIENE ALGUNA DISPOSICIÓN DEL PACTO SOCIAL O PERSONAL <b>DEL CLIENTE</b>;</p>
      <p align="justify">-  NO OCASIONARÁ ALGUNA VIOLACIÓN, INCUMPLIMIENTO O SITUACIÓN QUE DÉ LUGAR A LA CREACIÓN, ACTIVACIÓN O EJECUCIÓN DE ALGÚN DERECHO, GARANTÍA (DISTINTA A LA CONSTITUCIÓN DE GARANTÍA MOBILIARIA QUE SE CONSTITUYE EN ESTE ACTO) O CARGA, A LA ACELERACIÓN, DERECHOS DE VENCIMIENTO ANTICIPADO, MODIFICACIÓN, TERMINACIÓN, RESOLUCIÓN, RESCISIÓN, CANCELACIÓN, SUSPENSIÓN, IMPOSICIÓN DE OBLIGACIONES ADICIONALES, PÉRDIDA DE ALGÚN DERECHO O A LA NECESIDAD DE REQUERIR EL CONSENTIMIENTO BAJO CUALQUIER CONVENIO, ACTO O CONTRATO DEL CUAL <b>EL CLIENTE</b> SEA PARTE, BENEFICIARIO O POR EL CUAL SE ENCUENTRA DE ALGUNA FORMA OBLIGADO.-</p>

    <p align="justify">3.4 QUE ES (SON), EL (LOS) ÚNICOS PROPIETARIO(S) DEL BIEN. ESTA DECLARACIÓN SE EFECTÚA CON EL CARÁCTER DE DECLARACIÓN JURADA.</p>

    <p align="justify">3.5 EN PARTICULAR, NO UTILIZARÁ, OPERARÁ, DESARROLLARÁ, DESTINARÁ, NI PERMITIRÁ QUE ALGUIEN UTILICE, OPERE, DESARROLLE, DESTINE, DIRECTA O INDIRECTAMENTE, TOTAL O PARCIALMENTE, EL BIEN, EN ALGUNA ACTIVIDAD QUE:</p>

      <p align="justify">-  SEA O PUEDA SER ILÍCITA O CONSIDERADA MINERÍA ILEGAL O EN BENEFICIO, DIRECTO O INDIRECTO, DE ALGUNA ACTIVIDAD ILÍCITA O QUE SEA O PUEDA SER CONSIDERADA MINERÍA ILEGAL; Y/O</p>

      <p align="justify">-  NO CUENTE CON TODOS LOS PERMISOS, AUTORIZACIONES, APROBACIONES Y/O LICENCIAS NECESARIAS DE LAS AUTORIDADES COMPETENTES PARA LLEVARLA A CABO.</p>

    <p align="justify">3.6 SI ALGUNA DE LAS DECLARACIONES O INFORMACIÓN QUE <b>EL CLIENTE</b> HA SUMINISTRADO NO FUESE, O DEJARA DE SER, CIERTA, EXACTA, PRECISA Y COMPLETA DURANTE LA VIGENCIA DEL PRESENTE INSTRUMENTO, O <b>EL CLIENTE</b> SE VEA POSTERIORMENTE INCURSO EN CUALQUIERA DE LAS SITUACIONES DETALLADAS EN LA PRESENTE CLÁUSULA, <b>EL CLIENTE</b> HABRÁ INCURRIDO EN UN EVENTO DE INCUMPLIMIENTO DEL PRESENTE INSTRUMENTO.</p>

    <p align="justify">3.7 <b>EL CLIENTE</b> DECLARA QUE NO PESA CARGA, GRAVAMEN, EMBARGO, MEDIDA CAUTELAR, RESERVA DE PROPIEDAD, NI MEDIDA JUDICIAL O EXTRAJUDICIAL DE NATURALEZA ALGUNA SOBRE EL BIEN, Y QUE A LA FECHA NO SE HA INICIADO CONTRA ÉL O CONTRA TERCEROS ALGÚN PROCEDIMIENTO ARBITRAL, JUDICIAL, EXTRAJUDICIAL O ADMINISTRATIVO QUE DE ALGUNA FORMA PUEDA AFECTAR A EL BIEN O COMPROMETER SU LIBRE TRANSFERENCIA O DISPOSICIÓN. <b>EL CLIENTE</b> SE OBLIGA A INFORMAR POR ESCRITO Y CON FECHA CIERTA A <b>EL ACREEDOR</b> CUALQUIER CAMBIO EN LA SITUACIÓN LEGAL DEL BIEN.</p>

    <p align="justify">3.8 SI EL BIEN ES AFECTADO CON CUALQUIER TIPO DE CARGA, GRAVAMEN, EMBARGO O MEDIDA CAUTELAR O SIMILAR ANTES QUE LA PRESENTE CONSTITUCIÓN DE GARANTÍA SE INSCRIBA, QUE DETERMINE QUE LA PRESENTE CONSTITUCIÓN DE GARANTÍA NO SE INSCRIBA COMO PRIMERA Y PREFERENTE GARANTÍA MOBILIARIA EN EL REGISTRO MOBILIARIO DE CONTRATOS, SIN QUE <b>EL CLIENTE</b> HAYA SUBSANADO TAL SITUACIÓN DENTRO DE LOS QUINCE (15) DÍAS HÁBILES SIGUIENTES DE PRESENTADO EL TÍTULO PARA SU INSCRIPCIÓN, SE HABRÁ CONFIGURADO UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>

    <p align="justify">3.9 <b>EL CLIENTE</b> DECLARA ADEMÁS QUE NO HA SUSCRITO ALGÚN CONTRATO, NI LLEGADO A ALGÚN ACUERDO, DE AFECTACIÓN GLOBAL NI DE SUBORDINACIÓN DE OBLIGACIONES O CONSTITUCIÓN DE GARANTÍAS O GARANTÍAS PREFERENTES CON NINGUNA PERSONA O ENTIDAD Y SE OBLIGA A CUIDAR Y MANTENER LA PREFERENCIA DE LA GARANTÍA QUE POR ESTE ACTO CONSTITUYE A FAVOR <b>DEL ACREEDOR</b>. LA EXISTENCIA AL MOMENTO DE OTORGARSE ESTE DOCUMENTO, O EN EL FUTURO, DE ALGUNO DE LOS SUPUESTOS NEGADOS POR <b>EL CLIENTE</b> EN ESTA DECLARACIÓN, CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO DE SU PARTE.</p>

    <p align="justify">3.10 <b>EL CLIENTE Y EL DEPOSITARIO</b> SE OBLIGAN A NO CEDER, ENTREGAR, TRASPASAR O TRANSFERIR EL PRESENTE INSTRUMENTO, ALGUNO DE SUS DERECHOS U OBLIGACIONES, NI EL BIEN DADO EN GARANTÍA, SIN EL PREVIO CONSENTIMIENTO POR ESCRITO Y CON FECHA CIERTA <b>DEL ACREEDOR</b>; EL INCUMPLIMIENTO POR PARTE <b>DEL CLIENTE Y EL DEPOSITARIO</b> CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO DE SU PARTE Y <b>EL ACREEDOR</b> PODRÁ EJECUTAR LA GARANTÍA MOBILIARIA, DE CONFORMIDAD CON LO SUCRITO ASI COMO LA LEY DE GARANTIAS MOBILIARIAS.</p>

    <p align="justify">3.11 ANTES DE CONTRATAR CON TERCEROS SOBRE CUALQUIER ASPECTO VINCULADO A LA POSESIÓN, PROPIEDAD Y/O SITUACIÓN JURÍDICA DEL BIEN, <b>EL CLIENTE</b> DEBERÁ PREVIAMENTE COMUNICÁRSELO POR ESCRITO A <b>EL ACREEDOR</b> MEDIANTE CARTA NOTARIAL. EL INCUMPLIMIENTO EN EL ENVÍO DE DICHA INFORMACIÓN DE MANERA OPORTUNA Y SUFICIENTE CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>

    <p align="justify">3.12 <b>EL DEPOSITARIO</b> QUEDA DESIGNADO PARA CUIDAR Y SUPERVISAR EL BIEN E INTERVIENE EN EL PRESENTE INSTRUMENTO EN FORMA GRATUITA E IRRENUNCIABLE, ACEPTANDO SU DESIGNACIÓN COMO DEPOSITARIO DEL BIEN. CUALQUIER MODIFICACIÓN DEL LUGAR DONDE SE ENCUENTRE UBICADO <b>EL (LOS) BIEN(ES)</b> DEBERÁ SER COMUNICADA POR ESCRITO Y CON FECHA CIERTA POR EL DEPOSITARIO A <b>EL ACREEDOR</b> DENTRO DE LOS CINCO (5) DÍAS CALENDARIO SIGUIENTES DE OCURRIDO DICHO DESPLAZAMIENTO.</p>

    <p align="justify">3.13 <b>EL CLIENTE / DEPOSITARIO</b>, DECLARAN QUE EL BIEN SE ENCONTRARÁ UBICADO EN EL DOMICILIO DE ELLOS (SEÑALADO LINEAS ARRIBA EN SUS GENERALES DE LEY). EL BIEN PODRÁ SER TRASLADADO SOLO DENTRO DEL TERRITORIO DEL PERÚ PARA CUMPLIR ACTIVIDADES PROPIAS DEL GIRO DE NEGOCIO / ACTIVIDAD <b>DEL CLIENTE</b>, PERO PREVIAMENTE DEBERÁN COMUNICAR POR ESCRITO Y CON FECHA CIERTA A <b>EL ACREEDOR</b> DENTRO DE LOS CINCO (5) DÍAS CALENDARIO SIGUIENTES DE OCURRIDO DICHO DESPLAZAMIENTO; EL TRASLADO DEL BIEN NO LOS LIBERA DE SU OBLIGACIÓN DE CUMPLIMIENTO DE TODAS LAS OBLIGACIONES QUE ASUMEN POR EL PRESENTE INSTRUMENTO, ASÍ COMO QUE SON ENTERAMENTE RESPONSABLES POR DICHO TRASLADO. EN CASO QUE <b>EL ACREEDOR</b> REQUIERA <b>AL CLIENTE</b> LA INFORMACIÓN DE LA UBICACIÓN DEL BIEN, ESTE ÚLTIMO ESTÁ OBLIGADO A BRINDAR DICHA INFORMACIÓN POR ESCRITO A <b>EL ACREEDOR</b> EN UN PLAZO MÁXIMO DE TRES (3) DÍAS HÁBILES.-</p>

    <p align="justify">3.14 <b>EL DEPOSITARIO</b> SE OBLIGA A CAUTELAR EL BIEN, BAJO RESPONSABILIDAD, QUE: </p>

      <p align="justify">-  NINGUNA DE LAS PARTES INTEGRANTES O ACCESORIAS DEL BIEN PUEDEN SER RETIRADAS, SUSTITUIDAS O REEMPLAZADAS POR <b>EL CLIENTE</b> O TERCERO ALGUNO SIN LA PREVIA AUTORIZACIÓN ESCRITA <b>DEL ACREEDOR</b>;</p>

      <p align="justify">-  EL BIEN SEA OBJETO DE UN ADECUADO MANTENIMIENTO A FIN DE EVITAR QUE PUEDA PERDER O DISMINUIR SU VALOR O AFECTAR SU ESTADO DE OPERATIVIDAD; Y</p>

      <p align="justify">-  EL BIEN NO SEA USADO EN ALGUNA ACTIVIDAD ILÍCITA. ASIMISMO, EL DEPOSITARIO, BAJO RESPONSABILIDAD, DEBERÁ PONER EL BIEN A DISPOSICIÓN <b>DEL ACREEDOR</b> A SÓLO REQUERIMIENTO POR ESCRITO <b>DEL ACREEDOR</b>. EN CASO DE EMPLAZAMIENTO JUDICIAL O EXTRAJUDICIAL O ADJUDICACIÓN DIRECTA POR <b>EL ACREEDOR</b>.</p>

    <p align="justify">3.15 ES OBLIGACIÓN <b>DEL CLIENTE</b> COMUNICAR VIA NOTARIAL A <b>EL ACREEDOR</b> Y CONTAR CON LA ACEPTACIÓN EXPRESA Y VIA NOTARIAL <b>DEL ACREEDOR</b>, EN RELACIÓN A LA REMOCIÓN, RENUNCIA O VACANCIA DEL CARGO QUE DESEMPEÑA <b>EL DEPOSITARIO</b>. RECIÉN A PARTIR DEL MOMENTO QUE SE FORMALICE (PREVIA ACEPTACIÓN EXPRESA Y NOTARIAL <b>DEL ACREEDOR</b>) LA DESIGNACIÓN DEL NUEVO DEPOSITARIO DEL BIEN A SATISFACCIÓN <b>DEL ACREEDOR</b>, <b>EL DEPOSITARIO</b> QUEDARÁ LIBERADO DE SUS OBLIGACIONES Y RESPONSABILIDADES DE DEPOSITARIO ESTABLECIDAS EN LA LEY Y EN ESTE INSTRUMENTO. EN TODOS LOS CASOS LA DESIGNACIÓN FORMAL DEL NUEVO DEPOSITARIO DEL BIEN, DEBERÁ REALIZARSE EN EL PLAZO MÁXIMO DE VEINTICUATRO (24) HORAS DE PRODUCIDO EL HECHO QUE, DE ACUERDO A LO SEÑALADO, EXIGE NUEVA DESIGNACIÓN. EL CUAL SIEMPRE SERÁ UNA PERSONA JURIDICA. LA FALTA DE DESIGNACIÓN DEL NUEVO DEPOSITARIO DEL BIEN EN EL PLAZO Y EN LOS TÉRMINOS DE ESTA CLÁUSULA, CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>

    <p align="justify">3.16 <b>EL DEPOSITARIO</b> ASUME LA OBLIGACIÓN DE INFORMAR POR ESCRITO A <b>EL ACREEDOR</b> SOBRE LA SITUACIÓN Y ESTADO DEL BIEN, CADA VEZ QUE ÉSTE ASÍ LO REQUIERA.</p>

    <p align="justify">3.17 TODOS LOS GASTOS QUE IRROGUE LA CONSERVACIÓN, CUSTODIA, MANTENIMIENTO DEL BIEN, INFORMES, PRIMAS DE SEGURO, COSTO, INSTALACIÓN, MANTENIMIENTO DE GPS, PAGO DE TRIBUTOS Y CUALQUIER GASTO SERÁN DE CARGO Y CUENTA <b>DEL CLIENTE</b>, YA SEA QUE <b>EL DEPOSITARIO</b> SEA EL MISMO O UN TERCERO DESIGNADO DE COMÚN ACUERDO O <b>EL ACREEDOR</b>.</p>

    <p align="justify">3.18 <b>EL CLIENTE</b> SE COMPROMETE A SUBSANAR CUALQUIER OBSERVACIÓN QUE PUDIERA SUFRIR LA SOLICITUD DE INSCRIPCIÓN DE ESTA CONSTITUCIÓN DE GARANTÍA MOBILIARIA EN EL REGISTRO MOBILIARIO DE CONTRATOS, FIJÁNDOSE COMO PLAZO MÁXIMO PARA REALIZAR LA SUBSANACIÓN TREINTA (30) DÍAS CALENDARIO, CONTADOS DESDE LA FECHA DE PRESENTACIÓN DE LA SOLICITUD DE INSCRIPCIÓN; ASI TAMBIÉN SERÁ <b>EL CLIENTE</b> QUIEN CANCELE LOS COSTOS NOTARIALES Y REGISTRALES RESPECTIVOS. DE NO SUBSANAR LA(S) OBSERVACIÓN(ES), <b>EL ACREEDOR</b> QUEDARÁ FACULTADO PARA EXIGIR LA INMEDIATA ENTREGA DEL BIEN A <b>EL DEPOSITARIO</b>, BAJO RESPONSABILIDAD <b>DEL CLIENTE Y DEL DEPOSITARIO</b>. EL INCUMPLIMIENTO <b>DEL CLIENTE</b> DE SUBSANAR EN EL PLAZO PREVISTO (TREINTA (30) DÍAS CALENDARIO) CUALQUIER OBSERVACIÓN REGISTRAL, TAMBIÉN CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>

    <p align="justify">3.19 <b>EL CLIENTE</b>, DECLARA ESTAR PLENAMENTE INFORMADO QUE EL BIEN MATERIA DE ESTA MINUTA, DE CONTAR PERMANENTEMENTE CON UN SISTEMA DE GPS (SISTEMA DE POSICIONAMIENTO GLOBAL) A SATISFACCIÓN <b>DEL ACREEDOR</b>, DICHO DISPOSITIVO DEBERÁ PERMANECER VIGENTE DURANTE TODA LA DURACIÓN DEL PRESENTE CONTRATO, ES DECIR <b>EL CLIENTE</b> SE OBLIGA A NO DESINSTALAR EL GPS Y A RENOVARLO ANUALMENTE, ASUMIENDO ESTE MISMO, ÍNTEGRAMENTE TODOS LOS COSTOS NECESARIOS PARA TAL FIN; CASO CONTRARIO <b>EL CLIENTE</b> HABRÁ INCURRIDO EN UN EVENTO DE INCUMPLIMIENTO DEL PRESENTE INSTRUMENTO.</p>

    <p align="justify">PERFECCIONAMIENTO DEL CONTRATO, INFORMACIÓN Y FACULTAD DE INSPECCIÓN</p>
    <p align="justify">CUARTO:</p>

    <p align="justify">4.1 <b>EL CLIENTE</b> DECLARA QUE EL BIEN QUE AFECTA EN FAVOR <b>DEL ACREEDOR</b> QUEDARÁ PERFECCIONADO, DESDE LA SUSCRIPCIÓN DE LA PRESENTE MINUTA, CONSERVANDO <b>EL CLIENTE</b> LA POSESIÓN DEL BIEN, Y ASUMIENDO LA CALIDAD Y LAS OBLIGACIONES DE DEPOSITARIO, CON ARREGLO A LO ESTIPULADO EN LAS CLÁUSULAS DEL PRESENTE INSTRUMENTO.</p>

    <p align="justify">4.2 <b>EL ACREEDOR</b> QUEDA FACULTADO PARA ORDENAR LAS INSPECCIONES LAS VECES QUE CREA CONVENIENTE EN EL LUGAR DONDE SE ENCUENTRE EL BIEN, CORRIENDO LOS GASTOS CORRESPONDIENTES POR CUENTA Y CARGO <b>DEL CLIENTE</b>.</p>

    <p align="justify">4.3 <b>EL ACREEDOR</b> SE ENCUENTRA FACULTADO A INSPECCIONAR EL BIEN EN CUALQUIER MOMENTO, SIN PREVIO AVISO <b>AL CLIENTE O AL DEPOSITARIO</b>, QUIENES AUTORIZAN DE ANTEMANO Y EN FORMA EXPRESA EL ACCESO AL(OS) LUGAR(ES) DONDE SE ENCUENTREN UBICADO EL BIEN. EN CASO QUE, PRODUCTO DE LA INSPECCIÓN, SE COMPRUEBE QUE EL (LOS) BIEN(ES) SE HA(N) DETERIORADO O HA(N) DISMINUIDO SU VALOR DE TAL FORMA QUE, A CRITERIO <b>DEL ACREEDOR</b>, NO CONSTITUYE(N) UNA GARANTÍA ADECUADA PARA EL RESPALDO DE LAS OBLIGACIONES DEL DEUDOR, ÉSTE SE OBLIGA A OTORGAR GARANTÍA COMPLEMENTARIA O UNA NUEVA GARANTÍA A SATISFACCIÓN <b>DEL ACREEDOR</b> DENTRO DE LOS QUINCE (15) DÍAS CALENDARIO SIGUIENTES DE HABER SIDO REQUERIDO POR ESCRITO POR <b>EL ACREEDOR</b>, CASO CONTRARIO, <b>EL ACREEDOR</b> PODRÁ EJECUTAR LA GARANTÍA MOBILIARIA, SEGÚN LO ESTABLECIDO POR EL SEGUNDO PÁRRAFO DEL ARTÍCULO 12º DE LA LEY.</p>

    <p align="justify">4.4 <b>EL ACREEDOR</b> PODRÁ USAR Y COMPARTIR CON SUS AFILIADAS, EMPRESAS VINCULADAS Y DISTRIBUIDORES AUTORIZADOS DE EQUIPOS DE LA MARCA Y/O MODELO DEL BIEN, CUALQUIER INFORMACIÓN QUE HAYA RECOLECTADO <b>DEL CLIENTE</b> Y DE TERCEROS, PARA ADMINISTRAR, IMPLEMENTAR Y EJECUTAR ESTE INSTRUMENTO. <b>EL CLIENTE</b> ADEMÁS AUTORIZA A <b>EL ACREEDOR</b>, SUS AFILIADAS, EMPRESAS VINCULADAS Y DISTRIBUIDORES AUTORIZADOS DE EQUIPOS DE LA MARCA Y/O MODELO DEL BIEN, A ACCEDER A TODA INFORMACIÓN DISPONIBLE EN RELACIÓN AL BIEN, ENTRE LA CUAL SE INCLUYE, PERO NO SE LIMITA A, LA HISTORIA DE SU OPERACIÓN, DE SU ATENCIÓN Y SERVICIO TÉCNICO Y REPUESTOS, LA UBICACIÓN, LAS HORAS DE USO, EL USO DE LÍQUIDOS, LAS EMISIONES, LOS CÓDIGOS DE ERRORES Y DEMÁS DATA PROVENIENTE DEL SISTEMA DE LA MARCA O MODELO DEL BIEN O DE ALGÚN SISTEMA SIMILAR APROBADO POR <b>EL ACREEDOR</b> (EN ADELANTE, EL “SISTEMA”). LA INFORMACIÓN SE PODRÁ USAR PARA:</p>

      <p align="justify">-  ADMINISTRAR, IMPLEMENTAR Y EJECUTAR ESTE INSTRUMENTO;</p>
      <p align="justify">-  EJERCER LOS DERECHOS <b>DEL ACREEDOR</b> ANTE EL INCUMPLIMIENTO <b>DEL CLIENTE</b>, Y;</p>
      <p align="justify">-  SI EL SISTEMA ESTUVIESE INSTALADO AL MOMENTO DE LA ENTREGA DEL BIEN, <b>EL CLIENTE</b> SE OBLIGA A MANTENER EL HARDWARE Y LOS COMPONENTES DEL SOFTWARE DEL SISTEMA EN PERFECTO ESTADO OPERATIVO, EN CONSECUENCIA, EN CASO QUE EL SISTEMA SEA MANIPULADO, O HUBIESE ALGÚN DEFECTO EN SU OPERACIÓN, CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO DE PLENO DERECHO DE ESTE INSTRUMENTO POR PARTE <b>DEL CLIENTE</b>.</p>

    <p align="justify">4.5 <b>EL CLIENTE</b> SE OBLIGA A ENVIAR A <b>EL ACREEDOR</b> CUALQUIER INFORMACIÓN SOBRE EL BIEN, QUE ESTE ÚLTIMO SOLICITE, EN UN PLAZO NO MAYOR A TRES (3) DÍAS HÁBILES DE FORMULADO TAL REQUERIMIENTO. EL INCUMPLIMIENTO EN EL ENVÍO DE DICHA INFORMACIÓN DE MANERA OPORTUNA Y SUFICIENTE, O LA FALSEDAD, IMPRECISIÓN O INEXACTITUD DE LA INFORMACIÓN QUE <b>EL CLIENTE</b> ENVÍE A <b>EL ACREEDOR</b>, CONSTITUIRÁ UN EVENTO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>
  
    <p align="justify">OBLIGACIONES DEL DEPOSITARIO Y FORMA DE TOMAR LA POSESIÓN DEL BIEN OBJETO DE GARANTÍA MOBILIARIA </p>

    <p align="justify"><b>QUINTO: EL CLIENTE</b> CONSTITUIDO COMO DEPOSITARIO ASUME LA OBLIGACIÓN DE HACER ENTREGA DE EL (LOS) BIEN (ES) A <b>EL ACREEDOR</b>, A SIMPLE REQUERIMIENTO DE ESTE DENTRO DEL PLAZO QUE PARA TAL EFECTO SE SEÑALE A FIN DE FACILITAR SU VENTA, EN ESTE SUPUESTO Y EN OTROS EN LOS QUE SE ESTABLEZCA LA DISPOSICIÓN DE CUMPLIR CON LA ENTREGA DE EL (LOS) BIEN (ES), BASTARÁ LA FIRMA LEGALIZADA DE LAS PARTES CON EL DOCUMENTO QUE SE DEJE CONSTANCIA DE LA ENTREGA Y DEL ESTADO Y CARACTERÍSTICAS PRINCIPALES DE EL (LOS) BIEN (ES).</p>

    <p align="justify">EN CASO DE NO CUMPLIRSE CON LA OBLIGACIÓN DE ENTREGA, <b>EL CLIENTE</b> CONCEDE A <b>EL ACREEDOR</b> LA FACULTAD DE TOMAR POSESIÓN DIRECTA DE <b>EL (LOS) BIEN (ES)</b>, EN EL LUGAR DONDE SE ENCUENTRE(N), Y A TRAVÉS DE CERRAJERO, SIN PERJUICIO DE ELLO, <b>EL ACREEDOR</b> PODRÁ EJERCER LAS ACCIONES CIVILES Y PENALES QUE HUBIERA LUGAR Y ADOPTAR, A SU ELECCIÓN, CUALQUIERA DE LAS FORMAS DE LA TOMA DE POSESIÓN SEÑALADAS EN EL SEGUNDO Y TERCER PÁRRAFO DEL ARTÍCULO 51 DE LA LEY DE GARANTÍA MOBILIARIA.</p>

    <p align="justify">COMO DEPOSITARIO ASUME LA OBLIGACIÓN DE INFORMAR POR ESCRITO A <b>EL ACREEDOR</b> SOBRE LA SITUACIÓN Y ESTADO DEL (LOS) BIEN(ES), CADA VEZ QUE ESTE ASÍ LO REQUIERA. CUALQUIER MODIFICACIÓN DEL LUGAR DONDE SE ENCUENTRE UBICADO <b>EL (LOS) BIEN (ES)</b> DEBERÁ SER COMUNICADA POR ESCRITO Y CON FECHA CIERTA <b>EL DEPOSITARIO A EL ACREEDOR</b> DENTRO DE LOS CINCO (5) DÍAS CALENDARIO SIGUIENTES DE OCURRIDO DICHO DESPLAZAMIENTO.</p>

    <p align="justify">TODOS LOS GASTOS QUE IRROGUE LA CONSERVACIÓN, CUSTODIA, MANTENIMIENTO, DE EL(OS) BIEN (ES), INFORMES, PRIMAS DE SEGURO, PAGOS DE TRIBUTOS Y CUALQUIER GASTO SERÁ DE CARGO Y CUENTA <b>DEL CLIENTE</b>, YA SEA QUE <b>EL DEPOSITARIO</b> SEA EL MISMO O UN TERCERO DESIGNADO EN COMÚN ACUERDO CON <b>EL ACREEDOR</b>.</p>

    <p align="justify"><b>SEXTO: MONTO DETERMINADO DEL GRAVAMEN Y EXTENSIÓN DE LA GARANTÍA MOBILIARIA</b></p>

    <p align="justify">6.1 LA CONSTITUCIÓN DE GARANTÍA MOBILIARIA CONSTITUIDA EN EL PRESENTE ACTO SE OTORGA HASTA POR EL ÍNTEGRO DEL VALOR COMERCIAL DEL BIEN Y EQUIPOS QUE LAS CONSTITUYEN, QUE SE ENCUENTRA PRECISADO EN EL ANEXO N° 1 DE ESTE INSTRUMENTO, Y SE EFECTÚA SIN DESPLAZAMIENTO.</p>

    <p align="justify">6.2 LA GARANTÍA MOBILIARIA QUE POR ESTE ACTO SE CONSTITUYE SE EXTIENDE A SUS INTEGRANTES Y ACCESORIOS EXISTENTES AL TIEMPO DE LA EJECUCIÓN, Y EVENTUALMENTE, EL PRECIO DE LA ENAJENACIÓN, EL NUEVO BIEN QUE RESULTE DE LA TRANSFORMACIÓN DEL BIEN MUEBLE AFECTADO EN GARANTÍA MOBILIARIA, LA INDEMNIZACIÓN DEL SEGURO QUE SE HUBIESE CONTRATADO Y LA JUSTIPRECIADA EN CASO DE EXPROPIACIÓN; ASIMISMO, COMPRENDE A SUS RENTAS, FRUTOS, MEJORAS Y DEMÁS DERECHOS A LOS QUE DICHO BIEN DE ORIGEN.</p>
    
    <p align="justify">6.3 ESTA CONSTITUCIÓN DE GARANTÍA MOBILIARIA ES POR PLAZO INDEFINIDO Y SE MANTENDRÁ VIGENTE HASTA EL CUMPLIMIENTO ÍNTEGRO DE TODAS Y CADA UNA DE LAS OBLIGACIONES GARANTIZADAS. EL CLIENTE, A PARTIR DE LA PRIMERA CUOTA CANCELADA, PODRÁ ACERCARSE A LAS OFICINAS DE LA EMPRESA ACREEDORA PARA LIQUIDAR DE MANERA TOTAL EL PRESENTE MUTUO OTORGADO, REALIZANDO LOS DESCUENTOS POR CONCEPTO DE INTERES Y OTROS GASTOS RESPECTIVOS; ASÍ TAMBIÉN, EL CLIENTE, A PARTIR DE LA PRIMERA CUOTA CANCELADA, PODRÁ ACERCARSE A LAS OFICINAS DE LA EMPRESA ACREEDORA PARA REALIZAR AMORTIZACIÓN AL CAPITAL DEL MUTUO OTORGADO, EN CUYO CASO PREVIAS CONCESIONES RECÍPROCAS, SE PROCEDERÁ A ELABORAR UN NUEVO CRONOGRAMA CON LAS VARIACIONES DE CAPITAL, INTERESES Y GASTOS RESPECTIVOS.</p>

    <p align="justify">UNA VEZ PRODUCIDO ESTE CUMPLIMIENTO ÍNTEGRO, EL ACREEDOR, A REQUERIMIENTO DEL CLIENTE, DEBERÁ PROCEDER AL LEVANTAMIENTO Y CANCELACIÓN DE LA PRESENTE GARANTÍA MOBILIARIA. TODOS LOS GASTOS Y COSTOS QUE DICHO LEVANTAMIENTO Y CANCELACIÓN DE GARANTÍA ORIGINE, SERÁN ASUMIDOS POR EL CLIENTE.</p>
    
    <p><b>EVENTOS DE INCUMPLIMIENTO</b></p>
    <p><b>SEPTIMO:</b></p>

    <p align="justify">7.1 SERÁ CONSIDERADO UN EVENTO DE INCUMPLIMIENTO DE LA PRESENTE MINUTA POR PARTE <b>DEL CLIENTE / DEPOSITARIO</b> SI:</p>

      <p align="justify">-  <b>EL CLIENTE / DEPOSITARIO</b> INCUMPLE CUALQUIERA DE LAS GARANTÍAS, OBLIGACIONES Y/O COMPROMISOS ASUMIDOS EN LA PRESENTE MINUTA;</p>
      <p align="justify">-  <b>EL CLIENTE / DEPOSITARIO</b> INCUMPLE CUALQUIER OTRA OBLIGACIÓN, PRESENTE O FUTURA, CON EL ACREEDOR; O, </p>
      <p align="justify">-  <b>EL CIENTE / DEPOSITARIO</b> INCURRE EN CUALQUIERA DE LOS EVENTOS DE INCUMPLIMIENTO PRECISADOS EN ESTE INSTRUMENTO.</p>

    <p align="justify">7.2 ANTE LA OCURRENCIA DE CUALQUIER EVENTO DE INCUMPLIMIENTO DE ESTA MINUTA, LOS PLAZOS DE TODAS LAS CUOTAS ESTABLECIDAS EN EL ANEXO N° 1 DE LA PRESENTE MINUTA U OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL <b>EL CLIENTE</b> ASUMA ALGUNA OBLIGACIÓN O DEUDA CON <b>EL ACREEDOR</b>, SE ACELERARÁN DE MANERA AUTOMÁTICA, DE TAL MANERA QUE TODAS ELLAS SE CONSIDEREN VENCIDAS, Y POR LO TANTO EXIGIBLE EL IMPORTE DE TODAS ELLAS EN CONJUNTO.-</p>

    <p align="justify">7.3 EN EL SUPUESTO SEÑALADO EN EL NUMERAL 7.2 INMEDIATO ANTERIOR, EL CLIENTE TENDRÁ UN PLAZO DE TRES (3) DÍAS HÁBILES, CONTADOS DESDE QUE LAS CUOTAS MENCIONADAS EN EL NUMERAL 7.2 PRECEDENTE QUEDEN ACELERADAS, PARA CANCELAR A EL CLIENTE EL ÍNTEGRO DE LA DEUDA PRODUCTO DE:</p>

      <p align="justify">-  LAS CUOTAS VENCIDAS PENDIENTES DE PAGO Y DE LAS DEMÁS CUOTAS ESTABLECIDAS QUE HAN QUEDADO VENCIDAS COMO CONSECUENCIA DE SU ACELERACIÓN, Y;</p>
      <p align="justify">-  LOS GASTOS, COSTAS, COSTOS, PENALIDADES E INTERESES CORRESPONDIENTES. DE NO PRODUCIRSE LA CANCELACIÓN ÍNTEGRA DE LA DEUDA EN EL PLAZO DE TRES (3) DÍAS HÁBILES ESTABLECIDO, EL ACREEDOR TENDRÁ DERECHO A PROCEDER A LA EJECUCIÓN DE LA PRESENTE GARANTÍA A EFECTOS DE HACERSE COBRO DE SU ACREENCIA, DE ACUERDO A LOS TÉRMINOS DE ESTE DOCUMENTO Y/O DE CUALQUIER OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL EL CLIENTE ASUMA ALGUNA OBLIGACIÓN O DEUDA CON EL ACREEDOR.</p>

    <p align="justify">7.4 SE DEJA CONSTANCIA QUE, ANTE EL INCUMPLIMIENTO DEL CLIENTE DE CUALQUIERA DE LAS OBLIGACIONES GARANTIZADAS, EL ACREEDOR TAMBIÉN PODRÁ TOMAR CONTROL Y POSESIÓN DEL BIEN, ASÍ COMO APAGAR Y/O DISMINUIR LA FUERZA DEL MISMO, SIN NECESIDAD DE ACCIÓN, REQUERIMIENTO Y/O DECLARACIÓN JUDICIAL ALGUNA.</p>

    <p align="justify">7.5 EL CLIENTE EN ESTE ACTO AUTORIZA A EL ACREEDOR A TOMAR POSESIÓN Y RECIBIR EL BIEN, DIRECTA O INDIRECTAMENTE, EN CUALQUIER LUGAR Y UBICACIÓN DONDE ÉSTE SE ENCUENTRE, SI EL ACREEDOR TIENE UNA ORDEN DE INCAUTACIÓN DEL BIEN O MANDATO SIMILAR EXPEDIDO POR LA AUTORIDAD COMPETENTE. LO ESTABLECIDO EN EL PRESENTE PÁRRAFO CONSTITUYE TAMBIÉN UNA INSTRUCCIÓN ANTICIPADA, IRREVOCABLE Y SUFICIENTE DEL CLIENTE HACIA QUIEN TENGA EL BIEN, POR CUALQUIER MOTIVO Y BAJO CUALQUIER CARGO, TEMPORALMENTE O NO, PARA ENTREGAR EL BIEN A EL ACREEDOR, O A QUIEN SEA DESIGNADO, INMEDIATAMENTE SE LE REQUIERA.</p>

    <p><b>TASACIÓN CONVENCIONAL DEL BIEN Y DESIGNACIÓN DE REPRESENTANTES Y DESIGNACIÓN DE REPRESENTANTE Y PROCEDIMIENTO DE VENTA EXTRAJUDICIAL.</b></p>

    <p align="justify"><b>OCTAVO: EL CLIENTE</b> DECLARA QUE EL VALOR COMERCIAL DEL BIEN OBJETO DEL PRESENTE CONTRATO ASCIENDE A LA SUMA QUE SE SEÑALA EN EL ANEXO N° 1 LAS DOS TERCERAS PARTES DE DICHO VALOR SERVIRÁN DE BASE PARA SU VENTA, NO SIENDO NECESARIA HACER NUEVA TASACIÓN NI ACTUALIZACIÓN ALGUNA, SALVO QUE <b>EL ACREEDOR</b> ASÍ LO ESTIME CONVENIENTE.</p>

    <p align="justify">PARA EFECTOS DE LA VENTA O EJECUCIÓN DE LA GARANTÍA, LAS PARTES OTORGAN PODER ESPECÍFICO A FAVOR DE ERIK FRANCESC OBIOL ANAYA, IDENTIFICADO CON DNI N° 42417854, DOMICILIADO EN MZ H2 LOTE 28 URBANIZACION DERRAMA MAGISTERIAL EN EL DISTRITO DE CHICLAYO Y PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE, O DE MANUEL FRANCISCO PORRO RIVADENEIRA, CON DNI N° 16760683 Y DOMICILIADO EN LA CALLE MANUEL MARIA IZAGA 716-A -SEGUNDO PISO, EN EL DISTRITO DE CHICLAYO, PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE; EN ADELANTE, EL “REPRESENTANTE” (INTERVENCIÓN INDIVIDUAL CUALESQUIERA DE ELLOS); QUIEN(ES) QUEDA(N) AUTORIZADO(S) PARA REALIZAR Y FORMALIZAR LA TRANSFERENCIA DEL BIEN MUEBLE AFECTO DE GARANTÍA MOBILIARIA, MEDIANTE SU VENTA DIRECTA EN FAVOR DE TERCEROS INTERESADOS QUE ESTÉN DISPUESTOS A PAGAR EL PRECIO BASE DE REALIZACIÓN ANTES ACORDADO, PUDIENDO CONVOCAR A LOS COMPRADORES DIRECTAMENTE, SIN INTERVENCIÓN DE AUTORIDAD JUDICIAL, NI MARTILLERO O AGENTE ALGUNO; Y DE ESTIMAR NECESARIO ANTE LA FALTA DE INTERESADOS, PODRÁ CONVOCARLOS MEDIANTE AVISO ANTE CUALQUIER MEDIO DE COMUNICACIÓN, DURANTE EL LAPSO QUE <b>EL ACREEDOR</b> CONSIDERE CONVENIENTE.</p>

    <p><b>DE LA EJECUCIÓN DEL BIEN</b></p>
    <p><b>NOVENO:</b></p>

    <p align="justify">9.1. EN CASO DE PRODUCIRSE ALGÚN EVENTO DE INCUMPLIMIENTO A LOS QUE SE HACE REFERENCIA EN LA CLÁUSULA SEPTIMA QUE ANTECEDE, <b>EL ACREEDOR</b> PUEDE OPTAR POR LA EJECUCIÓN DE LA GARANTÍA MOBILIARIA, Y, EN CONSECUENCIA, POR LA VENTA EXTRAJUDICIAL CONTEMPLADA EN LA LEY DE GARANTÍA MOBILIARIA. EN TAL SENTIDO, DEBERÁ CURSAR <b>AL CLIENTE</b>, <b>AL DEPOSITARIO</b> Y AL REPRESENTANTE UNA CARTA NOTARIAL A EFECTOS DE DEJAR CONSTANCIA DEL INCUMPLIMIENTO POR PARTE <b>DEL CLIENTE</b>; EN ADELANTE, EL "AVISO DE INCUMPLIMIENTO".</p>

    <p align="justify">9.2 <b>EL CLIENTE O EL DEPOSITARIO</b>, SEGÚN SEA EL CASO, DENTRO DEL PLAZO DE TRES (3) DÍAS HÁBILES DE RECIBIDO EL AVISO DE INCUMPLIMIENTO, DEBERÁ PAGAR EL ÍNTEGRO DE SUS OBLIGACIONES CONFORME A LA PRESENTE MINUTA Y/O A CUALQUIER OTRO CONTRATO O ACTO JURÍDICO POR MEDIO DEL CUAL <b>EL CLIENTE</b> ASUMA ALGUNA OBLIGACIÓN O DEUDA CON <b>EL ACREEDOR</b>, O ENTREGAR EL BIEN A <b>EL ACREEDOR</b> SIN NECESIDAD DE REQUERIMIENTO ADICIONAL ALGUNO, EN EL LUGAR DONDE INDIQUE ESTE ÚLTIMO. DE PERSISTIR EL INCUMPLIMIENTO Y/O LA FALTA DE ENTREGA DEL BIEN, YA SEA POR PARTE <b>DEL CLIENTE Y/O DEL DEPOSITARIO</b> RESPECTIVAMENTE, ÉSTOS SERÁN RESPONSABLES CIVIL Y PENALMENTE, CONFORME A LO REGULADO POR EL ARTÍCULO 50° DE LA LEY DE GARANTÍA MOBILIARIA.</p>

    <p align="justify">9.3 SI EL BIEN NO ES ENTREGADO CONFORME A LAS CONDICIONES PACTADAS EN EL NUMERAL 9.2 QUE ANTECEDE, <b>EL ACREEDOR</b> PODRÁ ASUMIR DIRECTAMENTE LA POSESIÓN DEL BIEN EN EL(OS) LUGAR(ES) DONDE SE ENCUENTRE ÉSTE Y EN CUALQUIER MOMENTO, CONFORME AL PROCEDIMIENTO REGULADO EN EL SEGUNDO PÁRRAFO DEL ARTÍCULO 51° DE LA LEY DE GARANTÍA MOBILIARIA, CON LA CERTIFICACIÓN NOTARIAL RESPECTIVA EN LA QUE SE DEJARÁ CONSTANCIA DEL ESTADO Y CARACTERÍSTICAS PRINCIPALES DEL BIEN.</p>

    <p align="justify">9.4 SI <b>EL CLIENTE, O EL DEPOSITARIO</b>, ENTREGA(N) VOLUNTARIAMENTE EL BIEN, EN EL ACTA DE ENTREGA CON LA CERTIFICACIÓN NOTARIAL RESPECTIVA, SE DEBERÁ DEJAR CONSTANCIA DEL ESTADO Y CARACTERÍSTICAS EN LAS QUE SE RECIBE EL BIEN.</p>

    <p align="justify">9.5 <b>EL ACREEDOR</b> PODRÁ, ADEMÁS, SOLICITAR AL JUEZ ESPECIALIZADO EN LO CIVIL O AL JUEZ RESPECTIVO UN REQUERIMIENTO JUDICIAL DE INCAUTACIÓN DEL BIEN, CONTANDO CON EL AUXILIO DE LA FUERZA PÚBLICA, SI FUERA NECESARIO, A EFECTOS DE TOMAR POSESIÓN DEL BIEN.</p>

    <p align="justify">9.6 UNA VEZ QUE <b>EL ACREEDOR</b> HAYA TOMADO POSESIÓN DEL BIEN, DARÁ INICIO AL PROCESO DE TRANSFERENCIA O ADJUDICACIÓN DIRECTA.</p>

    <p align="justify">9.7 EL VALOR DEL BIEN PARA EFECTOS DE LA TRANSFERENCIA O ADJUDICACIÓN DIRECTA SERÁ AL PRECIO COMERCIAL QUE TENGA AL TIEMPO DE LA VENTA Y/O DE ACUERDO A UNA NUEVA TASACIÓN QUE EFECTUARÁN PERITOS INSCRITOS EN LA REPEV DE LA SBS, CUIDANDO DE REFLEJAR EL VALOR DE MERCADO DEL BIEN AL MOMENTO DE DECIDIRSE POR LA ALTERNATIVA DE LA ADJUDICACIÓN. EL PRECIO DE VENTA DEL BIEN EFECTUADA POR EL REPRESENTANTE SERÁ LAS DOS TERCERAS PARTES DEL VALOR DEL BIEN SEÑALADO EN EL PRESENTE CONTRATO. EN CASO HAYA TRANSCURRIDO MÁS DE UN AÑO DESDE EL ORTORGAMIENTO DEL CRÉDITO O SI EL ACREEDOR LO ESTIMA CONVENIENTE, ÉSTE PODRÁ OPTAR EN VENDER EL BIEN AL PRECIO COMERCIAL QUE TENGA AL TIEMPO DE LA VENTA O DE ACUERDO A UNA NUEVA TASACIÓN QUE EFECTUARÁ UN PERITO INSCRITO EN LA REPEV DE LA SBS.</p>

    <p align="justify">9.8 LA FORMALIZACIÓN DE LA TRANSFERENCIA DEL BIEN A MÉRITO DE LA ADJUDICACIÓN ESTARÁ A CARGO DEL REPRESENTANTE, CONFORME A LO DISPUESTO POR EL ARTÍCULO 53.6 DE LA LEY DE GARANTÍA MOBILIARIA, QUIEN EXTENDERÁ, ADEMÁS, LOS CONTRATOS, PÓLIZAS DE ADJUDICACIÓN, DOCUMENTOS DE ACREDITACIÓN DE PROPIEDAD Y LOS COMPROBANTES DE PAGO CORRESPONDIENTES, SEGÚN LO ESTABLECIDO EN EL NUMERAL 9.11 DE LA CLÁUSULA NOVENA CONTENIDA EN LA PRESENTE MINUTA.</p>

    <p align="justify">9.9 TRANSCURRIDO EL PLAZO ESTABLECIDO EN LA LEY DE GARANTIA MOBILIARIA, PARA TRANSFERIR EL BIEN INCAUTADO O ENTREGADO A <b>EL ACREEDOR</b> VOLUNTARIAMENTE POR <b>EL CLIENTE</b> O POR EL DEPOSITARIO, SEGÚN SEA EL CASO, SIN QUE SE HAYA PODIDO PROCEDER CON LA VENTA O ADJUDICACIÓN DIRECTA DEL BIEN, <b>EL ACREEDOR</b> PODRÁ SOLICITAR LA EJECUCIÓN JUDICIAL DE LA GARANTÍA MOBILIARIA, EN APLICACIÓN DEL NUMERAL 6 DEL ARTÍCULO 47° DE LA LEY DE GARANTÍA MOBILIARIA.</p>

    <p align="justify">9.10 LA DECISIÓN DE OPTAR, YA SEA POR LA VENTA EXTRAJUDICIAL, LA ADJUDICACIÓN DIRECTA DEL BIEN A <b>EL ACREEDOR</b> O LA EJECUCIÓN JUDICIAL, CORRESPONDERÁ A <b>EL ACREEDOR</b>, QUIEN PODRÁ ELEGIR CUALQUIERA DE ESTAS ALTERNATIVAS CONFORME A LOS PROCEDIMIENTOS REGULADOS EN LA PRESENTE CLÁUSULA EN CASO DE INCUMPLIMIENTO <b>DEL CLIENTE</b>.</p>

    <p align="justify">9.11 EL PAGO DE LAS OBLIGACIONES <b>DEL CLIENTE A EL ACREEDOR</b>, INDEPENDIENTEMENTE QUE SEA CONSECUENCIA DE UNA EJECUCIÓN O NO, SERÁ IMPUTADO PRIMERO A GASTOS, COMISIONES, COSTAS, COSTOS, PENALIDADES E INDEMNIZACIONES, LAS PRIMAS DE SEGURO PAGADAS POR <b>EL ACREEDOR</b>, LUEGO A INTERESES Y FINALMENTE A CAPITAL, EN ESE ORDEN.</p>

    <p><b>DÉCIMO: ASEGURAMIENTO DEL BIEN OBJETO DE GARANTÍA MOBILIARIA.</b></p>

    <p align="justify">10.1 <b>EL CLIENTE</b> TIENE LA FACULTAD DE DECIDIR POR CONTRATAR O NO CON UNA ASEGURADORA LOCAL EN RELACIÒN AL BIEN MATERIA DE LA PRESENTE GARANTIA MOBILIARIA, ES DECIR, DE ASEGURAR O NO EL BIEN MUEBLE; DE OPTAR <b>EL CLIENTE</b> POR NO ASEGURAR EL BIEN, ENTONCES ASUME LA RESPONSABILIDAD TOTAL DE CUALQUIER SINIESTRO Y/O EVENTUALIDAD(ES) Y/O CONTINGENCIA(S) Y/O ACCIDENTE(S) SEAN DOLOSOS Y/O CULPOSOS QUE PUDIERAN SUCEDER EN EL AMBITO LOCAL, REGIONAL, NACIONAL O INTERNACIONAL, YA SEA EN LA VÌA ADMINISTRATIVA, CIVIL Y/O PENAL. SIN QUE <b>EL CLIENTE</b> PUEDA IMPUTÁRLE RESPONSABILIDAD ALGUNA <b>AL ACREEDOR</b>.</p>

    <p align="justify">10.2. SI <b>EL CLIENTE</b> DECIDE CONTRATAR UNA PÓLIZA DE SEGURO CONTRA TODOS LOS RIESGOS, ESTÀ OBLIGADO, EN UN PLAZO MÀXIMO DE 15 DÌAS CALENDARIOS (CONTADOS DESDE QUE SE SUSCRIBE EL CONTRATO CON LA ASEGURADORA), A INFORMAR A <b>EL ACREEDOR</b>, ASÌ COMO ENDOSARLA Y/O NOMBRAR COMO BENEFICIARIO A <b>EL ACREEDOR</b>; CASO CONTRARIO <b>EL CLIENTE</b> HABRÁ INCURRIDO EN UN EVENTO DE INCUMPLIMIENTO DEL PRESENTE INSTRUMENTO.</p>

    <p align="justify">10.2.1. LA PÓLIZA DE SEGURO CONTRATADA EXCLUSIVAMENTE POR <b>EL CLIENTE</b>, DEBE ASEGURAR EL BIEN MATERIA DE GARANTÍA POR UN MONTO NO MENOR AQUEL POR EL CUAL SE CONSTITUYE EL GRAVAMEN (EL CUAL FIGURA EN EL ANEXO Nº 1), Y PARA TODO EL PLAZO PACTADO PARA LA DEVOLUCIÓN DEL MUTUO DINERARIO OTORGADO, SIENDO CONOCIDO POR <b>EL CLIENTE</b> QUE LA CONTRATACIÓN DEL SEGURO SE REALIZA EN FORMA ANUAL, Y CON LA REALIZACIÓN DE LAS RENOVACIONES QUE RESULTEN NECESARIAS, PARA TODO EL PLAZO PACTADO PARA LA DEVOLUCIÓN DE MUTUO DINERARIO OTORGADO.</p>

    <p align="justify">ESTA CONTRATACIÓN TIENE POR FINALIDAD QUE <b>EL ACREEDOR</b> PUEDA COBRAR DIRECTAMENTE EL SEGURO EN CASO DE SINIESTRO, Y APLICARLO, LLEGADO EL CASO, AL PAGO DE LAS OBLIGACIONES QUE GARANTIZA EL BIEN MATERIA DE GARANTÍA, SIN QUE PUEDA IMPUTÁRSELE RESPONSABILIDAD ALGUNA POR LA LIQUIDACIÓN DE LA PÓLIZA Y SIN PERJUICIO DE HACER VALER SUS DERECHOS POR EL SALDO QUE QUEDASE PENDIENTE; <b>EL CLIENTE</b> ACEPTA Y AUTORIZA <b>AL ACREEDOR</b> QUE ESTE IMPORTE (PRODUCTO DE COBRAR EL SEGURO) SEA PRORRATEADO EN FORMA PROPORCIONAL EN CADA CUOTA QUE HAYA PACTADO <b>EL CLIENTE</b> DEVOLVER POR EL MUTUO DINERARIO OTORGADO, Y CON EL MISMO INTERÉS ESTABLECIDO; SIENDO FACULTAD DISCRECIONAL <b>DEL ACREEDOR</b>, EL ESTABLECER EL NÚMERO DE CUOTAS EN LAS CUALES SE LE CARGARÁ EL IMPORTE ANTES MENCIONADO A FIN BRINDAR FACILIDAD DE PAGO A <b>EL CLIENTE</b>.</p>

    <p align="justify">EN EL CASO QUE OCURRIESE UN SINIESTRO EN EL QUE EL BIEN MATERIA DE GARANTÍA SE VIESE PERJUDICADO, Y EL MONTO DE SEGURO CONTRATADO POR <b>EL CLIENTE</b> NO ALCANZASE PARA CUBRIR LAS OBLIGACIONES A CARGO <b>DEL CLIENTE</b>, ENTONCES <b>EL ACREEDOR</b> PODRÁ DEMANDARLO POR EL SALDO INSOLUTO.</p>

    <p align="justify">DE PRESENTARSE UN SINIESTRO QUE OCASIONE LA PÉRDIDA, DESTRUCCIÓN O DAÑO EN EL BIEN MATERIA DE GARANTÍA, <b>EL ACREEDOR</b> COBRARÁ DE LA COMPAÑÍA DE SEGUROS LA INDEMNIZACIÓN CORRESPONDIENTE, CORRIENDO A CARGO <b>DEL CLIENTE</b> EL PAGO DE LA FRANQUICIA O DEDUCIBLE A QUE HAYA LUGAR, PARA LUEGO PROCEDER A APLICAR SU IMPORTE AL PAGO DE TODAS LAS OBLIGACIONES A CARGO <b>DEL CLIENTE</b>, INCLUIDAS LAS NO VENCIDAS; SIN PERJUICIO DE HACER VALER SUS DERECHOS POR EL SALDO QUE QUEDASE PENDIENTE.</p>

    <p align="justify">10.3 ADICIONALMENTE <b>EL CLIENTE</b>, DECLARA ESTAR PLENAMENTE INFORMADO QUE EL BIEN, MATERIA DE ESTA MINUTA, DEBE CONTAR PERMANENTEMENTE CON UN SISTEMA DE GPS (SISTEMA DE POSICIONAMIENTO GLOBAL) A SATISFACCIÓN <b>DEL ACREEDOR</b>, DICHO DISPOSITIVO DEBERÁ PERMANECER VIGENTE DURANTE TODA LA DURACIÓN DEL PRESENTE CONTRATO, ES DECIR <b>EL CLIENTE</b> SE OBLIGA A NO DESINSTALAR EL GPS EN NINGUN CASO Y/O CIRCUNSTANCIA (EL ACREEDOR SERÁ EL TITULAR DEL GPS Y EL ÚNICO QUE PODRÁ DESACTIVARLO O DESINSTALARLO). CASO CONTRARIO <b>EL CLIENTE</b> HABRÁ INCURRIDO EN UN EVENTO DE INCUMPLIMIENTO DEL PRESENTE INSTRUMENTO.</p>

    <p align="justify">ASI TAMBIÈN <b>EL CLIENTE</b>, SE OBLIGA ANTE <b>EL ACREEDOR</b> A BRINDARLE TODAS LAS FACILIDADES, ACCESOS, PERMISOS O AUTORIZACIONES NECESARIAS, PARA LA INSTALACIÒN, MANTENIMIENTO, INSPECCIÒN Y/O REPARACIÒN DEL DISPOSITIVO O SISTEMA DE GPS, CADA VEZ QUE SEA REQUIERIDO O SOLICITADO POR <b>EL ACREEDOR</b>, CUYA NOTIFICACIÓN DIRIGA AL CLIENTE Y/O DEPOSITARIO PUEDE SER POR CUALQUIER MEDIO DE COMUNICACIÒN: VERBAL, WHATSAPP, MENSAJE DE TEXTO, EMAIL O CARTA NOTARIAL. <b>EL CLIENTE</b> AUTORIZA A <b>EL ACREEDOR</b> PARA QUE SOLICITE LA INMOVILIZACIÓN DEL VEHÍCULO A TRAVÉS DEL <b>GPS</b>, QUEDANDO AUTORIZADO A RECOGERLO Y MANTENERLO EN CUSTODIA. CASO CONTRARIO <b>EL CLIENTE</b> HABRÁ INCURRIDO EN UN EVENTO DE INCUMPLIMIENTO DEL PRESENTE INSTRUMENTO.</p>


  	<p><b>PÉRDIDA DEL VALOR DEL BIEN Y EJECUCIÓN ANTICIPADA DE LA GARANTÍA MOBILIARIA.</b></p>

    <p align="justify"><b>DÉCIMO PRIMERO:</b> SI EL VALOR DEL BIEN OBJETO DE GARANTÍA MOBILIARIA DESMEJORASE A JUICIO <b>DEL ACREEDOR</b> , SEA POR DETERIORO, PÉRDIDA DE SU VALOR DE MERCADO O POR CUALQUIER OTRA CAUSA, <b>EL CLIENTE</b> SE OBLIGAN, A SIMPLE REQUERIMIENTO DE  <b>EL ACREEDOR</b>, A MEJORAR LA GARANTÍA A SATISFACCIÓN DEL MISMO ACREEDOR, EN SU DEFECTO, A DISMINUIR EL MONTO DE SU OBLIGACIÓN AL IMPORTE QUE <b>EL ACREEDOR</b> LE SEÑALE EN CASO DE NO CUMPLIR DENTRO DEL PLAZO QUE <b>EL ACREEDOR</b> FIJE, QUEDARÁ AUTOMÁTICAMENTE RESUELTO EL CONTRATO DE CRÉDITO QUE GARANTIZA ESTA GARANTÍA, ENTENDIÉNDOSE QUE SE DARÁN POR VENCIDOS TODOS LOS PLAZOS ESTABLECIDOS EN FAVOR <b>DEL ACREEDOR</b> Y EN CONTRA <b>DEL CLIENTE</b>, SIN NECESIDAD DE FORMALIZAR NI DAR AVISO ALGUNO AL RESPECTO, DE ACUERDO AL ARTÍCULO 1430 DEL CÓDIGO CIVIL; PUDIENDO <b>EL ACREEDOR</b> PROCEDER A LA VENTA O  EJECUCIÓN DE LA GARANTÍA CONFORME A LO ESTIPULADO EN LA CLÁUSULA OCTAVA Y NOVENA DE ESTE DOCUMENTO, SI NO SE CANCELASE TOTALMENTE LAS OBLIGACIONES ASUMIDAS A FAVOR <b>DEL ACREEDOR</b>.</p>

    <p align="justify">ASIMISMO, SI <b>EL CLIENTE</b> O, EN SU CASO, EL EVENTUAL ADQUIRENTE, DAÑARA O PUSIERA EN PELIGRO EL BIEN MUEBLE AFECTADO EN GARANTÍA MOBILIARIA, <b>EL ACREEDOR</b> TENDRÁ DERECHO A EXIGIR SU ENTREGA DE DEPÓSITO A UNA TERCERA PERSONA A PROCEDER A LA EJECUCIÓN DE LA GARANTÍA, CONFORME A LO ESTIPULADO EN LA CLÁUSULA OCTAVA Y NOVENA DEL PRESENTE CONTRATO. </p>
    
    <p><b>REGLAS DE IMPUTACIÓN</b></p>

    <p align="justify"><b>DÉCIMO SEGUNDO:</b> EL PAGO DERIVADO DE LA EJECUCIÓN DE LA GARANTÍA MOBILIARIA SE IMPUTARÁ A LOS GASTOS Y COMISIONES, LA INDEMNIZACIÓN POR DAÑOS Y PERJUICIOS, LAS PENALIDADES, LAS COSTAS Y COSTOS PROCESALES, LAS PRIMAS DE SEGURO PAGADAS POR <b>EL ACREEDOR</b>, LOS INTERESES DE DEVENGUEN Y EL CAPITAL, EN ESE ORDEN.</p>

    <p><b>ADJUDICACIÓN DEL BIEN POR EL ACREEDOR</b></p>
  	
    <p align="justify"><b>DÉCIMO TERCERO:</b> LAS PARTES ACUERDAN QUE EN CASO DE INCUMPLIMIENTO DE CUALQUIERA DE LAS OBLIGACIONES ASUMIDAS POR <b>EL CLIENTE</b>, EN VIRTUD DEL PRESENTE CONTRATO, <b>EL ACREEDOR</b> PODRÁ, ALTERNATIVAMENTE A LA EJECUCIÓN EXTRAJUDICIAL, ADJUDICARSE LA PROPIEDAD DEL BIEN AFECTADO POR GARANTÍA MOBILIARIA, CON TAL FIN, SE ESTABLECE QUE EL VALOR DEL BIEN ES EL INDICADO EN EL ANEXO N° 1 ADJUNTADO AL PRESENTE CONTRATO DE LA GARANTÍA MOBILIARIA, ASIMISMO SE CONVIENE EN OTORGAR PODER ESPECÍFICO A FAVOR DE ERIK FRANCESC OBIOL ANAYA, IDENTIFICADO CON DNI N° 42417854, DOMICILIADO EN MZ H2 LOTE 28 URBANIZACION DERRAMA MAGISTERIAL EN EL DISTRITO DE CHICLAYO Y PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE; O DE: MANUEL FRANCISCO PORRO RIVADENEIRA, CON DNI N° 16760683 Y DOMICILIADO EN LA CALLE MANUEL MARIA IZAGA 716-A -SEGUNDO PISO, EN EL DISTRITO DE CHICLAYO Y PROVINCIA DE CHICLAYO, DEPARTAMENTO DE LAMBAYEQUE, (INTERVENCIÓN INDIVIDUAL CUALESQUIERA DE ELLOS); PARA QUE EN CASO DE INCUMPLIMIENTO PROCEDA(N) A SUSCRIBIR LA DOCUMENTACIÓN NECESARIA PARA LA TRANSFERENCIA DEL BIEN MUEBLE AFECTO EN GARANTÍA MOBILIARIA.</p>

    <p><b>GASTOS DE FORMALIZACIÓN DEL CONTACTO</b></p>
      
    <p align="justify"><b>DÉCIMO CUARTO:</b> TODOS LOS GASTOS QUE SE ORIGINEN COMO CONSECUENCIA DEL PRESENTE DOCUMENTO SERÁN CARGADOS POR <b>EL ACREEDOR</b> EN CUALQUIERA DE LAS CUENTAS O DEPÓSITOS QUE <b>EL CLIENTE</b> MANTENGAN EN <b>EL ACREEDOR</b>, O QUE AL EFECTO ESTA ABRA A SU NOMBRE CON OBLIGACIÓN DEL MISMO DE REEMBOLSAR TALES IMPORTES EL DÍA MISMO EN QUE SE PRODUZCA LOS CARGOS, GENERÁNDOSE EN CASO CONTRARIO LOS INTERESES COMPENSATORIOS Y MORATORIOS A LAS TASAS QUE <b>EL ACREEDOR</b> TENGA ESTABLECIDO, HASTA LA FECHA EFECTIVA DE SU PAGO.</p>

    <p><b>CESIÓN.-</b></p>

    <p align="justify"><b>DÉCIMO QUINTO</b>: EN ESTE ACTO, <b>EL CLIENTE Y EL DEPOSITARIO</b> AUTORIZAN A <b>EL ACREEDOR</b> A CEDER SUS DERECHOS EN LA PRESENTE MINUTA, BIEN SEA TOTAL O PARCIALMENTE.- </p>
    
    <p><b>INFORMACIÓN CREDITICIA DEL CLIENTE</b></p>

    <p align="justify"><b>DÉCIMO SEXTO: EL CLIENTE</b> AUTORIZA A <b>EL ACREEDOR</b> A PROPORCIONAR LAS INFORMACIONES RELATIVAS AL INCUMPLIMIENTO DE SUS OBLIGACIONES, A TERCERAS PERSONAS, INCLUSIVE CENTRALES DE RIESGO, PUDIENDO DIFUNDIRSE Y/O COMERCIALIZARSE DICHAS INFORMACIONES CREDITICIAS NEGATIVAS SIN NINGUNA, RESPONSABILIDAD PARA <b>EL ACREEDOR</b>.</p>

    <p align="justify">DEL MISMO MODO, <b>EL ACREEDOR</b> QUEDA AUTORIZADO A VERIFICAR LOS DATOS E INFORMACIONES PROPORCIONADAS POR EL <b>EL CLIENTE</b>, AUTORIZARLOS E INTERCAMBIARLOS CON OTROS ACREEDORES O TERCEROS (INCLUSIVE CENTRALES DE RIESGO); ASI COMO OBTENER INFORMACIÓN SOBRE SU PATRIMONIO, CUMPLIMIENTO DE SUS PAGOS CON TERCEROS ACREEDORES Y SUS TRANSACCIONES BANCARIAS Y CREDITICIAS EN GENERAL.</p>

    <p align="justify"><b>EL CLIENTE</b> LIBERA A <b>EL ACREEDOR</b> DE TODA RESPONSABILIDAD POR LA DIFUSIÓN Y/O COMERCIALIZACIÓN POR TERCEROS DE DICHAS INFORMACIONES; LIMITÁNDOSE EXCLUSIVAMENTE LA OBLIGACIÓN <b>DEL ACREEDOR</b> A RECTIFICAR INFORMACIONES QUE HAYA PROPORCIONADO, SIEMPRE QUE NO CORRESPONDAN EXACTAMENTE A LA SITUACIÓN <b>EL CLIENTE</b>.</p>
  
  	<p><b>COMPETENCIA TERRITORIAL</b></p>

    <p align="justify"><b>DÉCIMO SEPTIMO:</b> PARA LA VALIDEZ DE TODAS LAS COMUNICACIONES Y NOTIFICACIONES A LAS PARTES CON MOTIVO DE LA EJECUCIÓN DE ESTE CONTRATO, CONVIENE EN SEÑALAR COMO SUS RESPECTIVOS DOMICILIOS LOS INDICADOS EN LA INTRODUCCIÓN Y/O ANEXO N° 1 DE ESTE DOCUMENTO; EL CAMBIO DE DOMICILIO <b>DEL CLIENTE / DEPOSITARIO</b> SURTIRÁ EFECTO DESDE LA FECHA CIERTA (CARTA NOTARIAL), DENTRO DE LOS TREINTA DÍAS DE OCURRIDO EL HECHO; Y EL CAMBIO DE DOMICILIO <b>DEL ACREEDOR</b> SURTIRÁ EFECTO DESDE SU DECLARACIÓN A TRAVÉS DE CUALQUIER MEDIO DE COMUNICACIÓN MASIVA, DENTRO DE LOS TREINTA DÍAS DE OCURRIDO EL HECHO. EL DOMICILIO DE AMBAS PARTES DEBE QUEDAR DENTRO DEL RADIO URBANO DE LA CIUDAD DE CHICLAYO.</p>

    <p align="justify">TODO LITIGIO O CONTROVERSIA, DERIVADOS O RELACIONADOS CON ESTE ACTO JURÍDICO, SERÁ RESUELTO MEDIANTE ARBITRAJE, DE CONFORMIDAD CON LOS REGLAMENTOS ARBITRALES DEL CENTRO DE ARBITRAJE DE LA CÁMARA DE COMERCIO DE LAMBAYEQUE, A CUYAS NORMAS, ADMINISTRACIÓN Y DECISIÓN SE SOMETEN LAS PARTES EN FORMA INCONDICIONAL, DECLARANDO CONOCERLAS Y ACEPTARLAS EN SU INTEGRIDAD.</p>

    <p><b>DISPOSICIONES GENERALES</b></p>
    <p><b>DÉCIMO OCTAVO: </b></p>

    <p align="justify">18.1 LAS CLÁUSULAS Y NUMERALES DE LA PRESENTE MINUTA SON SEPARABLES Y LA NULIDAD, INEFICACIA O INVALIDEZ DE UNA O MÁS DE ELLAS NO PERJUDICARÁ A LAS RESTANTES EN TANTO SE MANTENGA LA ESENCIA DE ESTE ACTO. EN CASO QUE ALGUNA DE LAS CLÁUSULAS O NUMERALES DEL PRESENTE INSTRUMENTO SEA DECLARADA NULA, INEFICAZ O INVÁLIDA, <b>EL CLIENTE Y EL DEPOSITARIO</b> HARÁN TODO ESFUERZO RAZONABLE PARA ELABORAR E IMPLEMENTAR UNA SOLUCIÓN LEGALMENTE VÁLIDA QUE LOGRE EL RESULTADO MÁS CERCANO A AQUÉL QUE SE BUSCABA OBTENER CON EL NUMERAL Y/O CLÁUSULA DECLARADA NULA, INEFICAZ O INVÁLIDA.</p>

    <p align="justify">18.2 NINGUNO DE LOS NUMERALES, CLÁUSULAS, TÉRMINOS NI CONDICIONES ESTABLECIDAS EN LA PRESENTE MINUTA, PODRÁN SER MODIFICADOS SIN EL PREVIO CONSENTIMIENTO ESCRITO <b>DEL ACREEDOR</b>. INCLUSO, <b>EL CLIENTE</b> DECLARA QUE LA CONSTITUCIÓN DE GARANTÍA MOBILIARIA CONSTITUIDA EN ESTE ACTO SOBRE EL BIEN, ÚNICA Y EXCLUSIVAMENTE, PODRÁ SER CANCELADA O LEVANTADA POR <b>EL ACREEDOR</b>.</p>

    <p align="justify">18.3. <b>EL CLIENTE</b> Y EL DEPOSITARIO, OTORGAN A FAVOR DEL APODERADO: TORRES GONZALES OMAR PEDRO, CON DNI Nº 45915322, MANDATO ESPECÍFICO PARA QUE EN SU NOMBRE Y REPRESENTACIÓN, SUSCRIBA EN NOMBRE <b>DEL CLIENTE</b> Y/O DEPOSITARIO CUALQUIER DOCUMENTO PÚBLICO Y/O PRIVADO, SEAN ESTE O ESTOS RECTIFICATORIOS, MODIFICATORIOS, ACLARATORIOS, COMPLEMENTARIOS, CONFIRMATORIOS, QUE SEAN NECESARIOS PARA FORMALIZAR O CONCLUIR LA FORMALIZACIÓN DE LA PRESENTE CONSTITUCIÓN DE GARANTÍA MOBILIARIA Y SU RESPECTIVA INSCRIPCIÓN ANTE LOS REGISTROS PÚBLICOS; EN ESTE SENTIDO, <b>EL APODERADO</b> PODRÁ SUSCRIBIR TODOS LOS DOCUMENTOS ANTES MENCIONADOS QUE SEAN NECESARIOS PARA FORMALIZAR EL PRESENTE ACTO JURÍDICO E INSCRIBIRLO ANTE LOS REGISTROS PÚBLICOS, CONFORME SE HACE REFERENCIA EN EL PRESENTE ITEM. ASI TAMBIÉN <b>EL CLIENTE Y EL DEPOSITARIO</b>, OTORGAN PODER ESPECIAL A <b>EL APODERADO</b> MENCIONADO LINEAS ARRIBA, Y LO FACULTA(N) PARA QUE PUEDA REALIZAR MODIFICACIONES Y/O ACLARACIONES DE CUALESQUIERA DE LAS CLÁUSULAS CONTENIDAS EN LA PRESENTE ESCRITURA DE GARANTÍA MOBILIARIA SUSCRITA POR <b>EL CLIENTE</b> Y EL DEPOSITARIO, PUDIENDO PARA TAL EFECTO SUSCRIBIR CONTRATOS, MINUTAS, ESCRITURAS PÚBLICAS, DECLARACIONES JURADAS, LETRAS Y/O CUALQUIER DOCUMENTO PÚBLICO Y/O PRIVADO NECESARIO PARA MATERIALIZAR LO ENCOMENDADO.</p>


    <p align="justify"><b>PRIMERA CLAUSULA ADICIONAL:</b> LAS PARTES DEJAN EN EXPRESA CONSTANCIA QUE EN EL PRESENTE ACTO, EN RELACION AL MEDIO DE PAGO, <b>EL ACREEDOR</b> PROCEDE DE CONFORMIDAD CON LA LEY N°28194.</p>

    <p align="justify"><b>SEGUNDA CLAUSULA ADICIONAL:</b> TODOS LOS GASTOS QUE OCASIONE LA ESCRITURA A LA QUE ESTA MINUTA DÉ LUGAR, UN TESTIMONIO Y UNA COPIA SIMPLE DE LA MISMA, CERTIFICADO DE GRAVAMEN CON LA INSCRIPCIÓN DE LA GARANTÍA DE LOS REGISTROS PÚBLICOS PARA <b>EL ACREEDOR</b>, SERÁN DE CUENTA <b>DEL CLIENTE</b>.</p>

    <p align="justify"><b>TERCERA CLAUSULA ADICIONAL:</b> LAS PARTES INTERVINIENTES EXPRESAN SU CONFORMIDAD CON LOS TÉRMINOS DE LA PRESENTE MINUTA Y LA SUSCRIBEN EN LA VICTORIA A LOS '.$credito_dia.' DÍAS DEL MES DE '.strtoupper(nombre_mes($credito_mes)).' DEL '.$credito_anio.'. </p>

    <p align="justify">USTED SEÑOR NOTARIO SE ENCARGARÁ DE AGREGAR LA INTRODUCCIÓN Y CONCLUSIÓN DE LEY, PARTES A EL/LOS REGISTRO/S PÚBLICOS CORRESPONDIENTES, A LOS EFECTOS DE LA/S INSCRIPCIÓN/ES PERTINENTE/ES.</p>

    <p align="justify">LA VICTORIA '.$credito_dia.' DE '.strtoupper(nombre_mes($credito_mes)).' DEL '.$credito_anio.'</p>
    
    <p></p>

    <table style="width:100%; text-align: center; margin-left: 80px;">
      <tr>
        <td style="width:30%; text-align: center;">
          <b>_________________________</b><br/>
          <b>EL CLIENTE</b>
        </td>
        <td style="width:40%;"></td>
        <td style="width:30%; text-align: center;">
          <b>_________________________</b><br/>
          <b>EL ACREEDOR</b>
        </td>
      </tr>
    </table>
';

    //nueva hoja
$html2 .='    
    <p align="justify"><b>ANEXO N° 1</b></p>
    <p align="justify"><b>AL CONTRATO DE GARANTÍA MOBILIARIA</b></p>

    <p align="justify">1. DESCRIPCIÓN DEL BIEN AFECTADO EN GARANTIA MOBILIARIA EN FAVOR <b>DEL ACREEDOR</b> POR <b>EL CLIENTE</b>, QUIEN MANIFIESTA CON CARÁCTER DE DECLARACIÓN JURADA DE ACUERDO A LA LEY N° 26702, QUE ES DE SU PROPIEDAD Y LIBRE DISPOSICIÓN: EL BIEN.- VEHICULO CON PLACA DE RODAJE N° <b>'.$vehiculo_pla.'</b>, CARROCERÍA: <b>'.$vehcla_nom.'</b>, CATEGORÍA: <b>'.$vehiculo_cate.'</b>, MARCA: <b>'.$vehmar_nom.'</b>, MODELO: <b>'.$vehmod_nom.'</b>, AÑO DE MODELO: <b>'.$vehiculo_mode.'</b>, COLOR: <b>'.$vehiculo_col.'</b>, N° DE SERIE: <b>'.$vehiculo_cha.'</b>, N° DE MOTOR: <b>'.$vehiculo_mot.'</b>, COMBUSTIBLE: <b>'.$vehiculo_comb.'</b>, CUYO DOMINIO CORRE INSCRITO EN LA PARTIDA N° <b>'.$vehiculo_parele.'</b> DE REGISTRO DE PROPIEDAD VEHICULAR EN LA '.$zona_nom.'.</p>

    <p align="justify">2. VALOR DE LA TASACIÓN COMERCIAL ACTUALIZADO: <b>US$ '.mostrar_moneda($credito_valtas).' ('.numtoletras($credito_valtas, 2).')</b>.</p>

    <p align="justify">3. CRÉDITO GARANTIZADO: '.$anexo_adicional.'</p>

    <p align="justify">4. VALOR DE REALIZACIÓN DEL MERCADO: <b>US$ '.mostrar_moneda($credito_valtas).' ('.numtoletras($credito_valtas, 2).')</b>.</p>

    <p align="justify">5. VALOR DE GRAVAMEN: LA PRESENTE GARANTÍA MOBILIARIA QUE SE CONSTITUYE TIENE UN MONTO O VALOR DE GRAVAMEN ASCENDENTE A LA SUMA DE <b>US$ '.mostrar_moneda($credito_valtas).' ('.numtoletras($credito_valtas, 2).')</b>.</p>

    <p align="justify">6. CRONOGRAMA DE PAGOS:</p>
    '.$cuota_detalle.'
    <p></p>
    <p align="justify"><b>EL CLIENTE</b>, DECLARA EXPRESAMENTE CONOCER LOS IMPORTES DE LOS CONCEPTOS INCLUIDOS EN EL PRESENTE CRONOGRAMA DE PAGOS, EL MISMO QUE HA SIDO ELABORADO DE COMÙN ACUERDO ENTRE LAS PARTES SUSCRIBIENTES Y QUE FORMA PARTE INTEGRANTE DE LA PRESENTE MINUTA. LAS PARTES INTERVINIENTES MANIFIESTAN EXPRESAMENTE CONOCER Y ACEPTAR A SU COMPLETA SATISFACCIÒN DICHO CRONOGRAMA DE PAGOS, EL CUAL ES SUSCRITO POR AMBOS INTERVINIENTES.</p>

    <p>LA VICTORIA '.$credito_dia.' DE '.strtoupper(nombre_mes($credito_mes)).' DEL '.$credito_anio.'.</p>
    
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <table style="width:100%; text-align: center; margin-left: 80px;">
      <tr>
        <td style="width:30%; text-align: center;"><b>
          _________________________<br/>
          EL CLIENTE </b>
        </td>
        <td style="width:40%;"></td>
        <td style="width:30%; text-align: center;"><b>
          _________________________<br/>
          EL ACREEDOR </b>
        </td>
      </tr>
    </table>
  ';

if($tipo_documento == 'word'){
  $html = str_replace('&', '&amp;amp;', $html);
  $html2 = str_replace('&', '&amp;amp;', $html2);

  // Crear una instancia de PHPWord
  $phpWord = new PhpWord();

  $phpWord->setDefaultFontName('Arial');
  $phpWord->setDefaultFontSize(8);

  // Definir estilo de párrafo
  $paragraphStyle = array('spaceAfter' => 0, 'spacing' => 0);

  // Crear una nueva sección
  $section1 = $phpWord->addSection();

  // Renderizar el contenido HTML 1
  Html::addHtml($section1, $html, false, false, $paragraphStyle);

  // Insertar un salto de página para la siguiente sección
  $section1->addPageBreak();

  $section2 = $phpWord->addSection();
  // Renderizar el contenido HTML 2
  Html::addHtml($section2, $html2, false, false, $paragraphStyle);

  // Guardar el documento
  $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
  $objWriter->save('sin_custodia.docx');
  echo 'Minuta para el cliente: '.$cliente_nom.', generado correctamente...';
  header('Location: ../doc_creditogarveh/sin_custodia.docx');
}

if($tipo_documento == 'pdf'){
  $pdf->SetFont('dejavusans', '', 8);
  $pdf->writeHTML($html, true, 0, true, true);

  $pdf->Ln();
  $pdf->AddPage();
  $pdf->writeHTML($html2, true, 0, true, true);

  // reset pointer to the last page
  $pdf->lastPage();

  // ---------------------------------------------------------

  //Close and output PDF document
  $nombre_archivo=$codigo."_".$title.".pdf";
  $pdf->Output($nombre_archivo, 'I');
}
?>