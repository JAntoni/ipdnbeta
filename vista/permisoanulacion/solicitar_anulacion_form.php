<?php
session_name("ipdnsac");
session_start();

require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../permisoanulacion/Permisoanulacion.class.php');
$oPermisoanulacion = new Permisoanulacion();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$origen = $_POST['origen'];
$modid = $_POST['modid'];
// ORIGEN: 1=cuota(cmenor), 2=cuotadetalle(cgarveh), 3=ingreso, 4=egreso
// MODID: 1,2 = cuotapago_id | 3 = ingreso_id | 4 = egreso_id
$credito_id = $_POST['credito_id'];
$creditotipo_id = $_POST['creditotipo_id'];

$accion = $_POST['accion'];

$solicitud = $oPermisoanulacion->mostrarSolicitudAnulacion($modid, $origen);
$flag = false;
$fecha_reg = '';
$usu_reg = '';
$motivo = '';
if($solicitud['estado'] == 1){
    $flag = true;
    $fecha_reg = mostrar_fecha_hora($solicitud['data']['tb_permisoanulacion_fecreg']);
    $usuario = $oUsuario->mostrarUno($solicitud['data']['tb_permisoanulacion_user_reg']);
    if($usuario['estado'] == 1){
        $usu_reg = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
    }
    $motivo = $solicitud['data']['tb_permisoanulacion_des'];
}

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_solicitar_anulacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;">MOTIVO DE ANULACIÓN</h4>
            </div>
            
            <form id="form_solicitar_anulacion" method="post">
                <input name="action" id="action" type="hidden" value="solicitar_anulacion">
                <input name="hdd_modid" id="hdd_modid" type="hidden" value="<?php echo $modid; ?>">
                <input name="hdd_origen" id="hdd_origen" type="hidden" value="<?php echo $origen; ?>">
                <input name="hdd_credito_id" id="hdd_credito_id" type="hidden" value="<?php echo $credito_id; ?>">
                <input name="hdd_creditotipo_id" id="hdd_creditotipo_id" type="hidden" value="<?php echo $creditotipo_id; ?>">

                <div class="modal-body">
                    <?php if($flag){ ?>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <h4>La solicitude de anulación ya fue registrado por <strong><?php echo $usu_reg; ?></strong> el día <strong><?php echo $fecha_reg; ?></strong></h4>
                                <h4><strong>Detalle: </strong><?php echo $motivo; ?></h4>
                            </div>
                        </div>

                    <?php }else{ ?>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label for="cmb_motivo">Motivo:</label>
                                    <select id="cmb_motivo" name="cmb_motivo" class="form-control input-sm">
                                        <?php require_once 'motivo_anulacion_select.php';?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label>Detalle:</label>
                                    <textarea id="txt_detalle" name="txt_detalle" class="form-control input-sm" rows="5"; style="resize: none;" placeholder="Registre el detalle de la anulación..."><?php echo $motivo; ?></textarea>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="solicitar_anulacion_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" <?php if($flag){ echo 'disabled'; } ?> id="btn_guardar_solicitar_anulacion">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/permisoanulacion/permisoanulacion.js?ver=11012024';?>"></script>
