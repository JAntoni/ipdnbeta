<?php
session_name("ipdnsac");
session_start();

if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
    require_once(VISTA_URL . 'permisoanulacion/Permisoanulacion.class.php');
    $oPermisoanulacion = new Permisoanulacion();
    require_once(VISTA_URL . 'usuario/Usuario.class.php');
    $oUsuario = new Usuario();
    require_once(VISTA_URL . 'empresa/Empresa.class.php');
    $oEmpresa = new Empresa();
    require_once(VISTA_URL . 'ingreso/Ingreso.class.php');
	$oIngreso = new Ingreso();
    require_once(VISTA_URL . 'egreso/Egreso.class.php');
	$oEgreso = new Egreso();
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../permisoanulacion/Permisoanulacion.class.php');
    $oPermisoanulacion = new Permisoanulacion();
    require_once('../usuario/Usuario.class.php');
    $oUsuario = new Usuario();
    require_once('../empresa/Empresa.class.php');
    $oEmpresa = new Empresa();
    require_once('../ingreso/Ingreso.class.php');
	$oIngreso = new Ingreso();
    require_once('../egreso/Egreso.class.php');
	$oEgreso = new Egreso();
}

$fec1 = fecha_mysql($_POST['txt_fec1']);
$fec2 = fecha_mysql($_POST['txt_fec2']);
// Sumar 1 día a la fecha $fec2
/* $date = new DateTime($fec2);
$date->modify('+1 day');
$fec2 = $date->format('Y-m-d'); */

$emp_id = intval($_POST['cmb_empresa_id']);
$est = intval($_POST['cmb_est']);
$usuario_id = intval($_POST['cmb_usuario_id']);
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
$fecha_hoy = date('Y-m-d');
$usuario_sesion_id = intval($_SESSION['usuario_id']);

/* if (empty($emp_id)) {
    $emp_id = intval($_SESSION['empresa_id']);
} */

$result = $oPermisoanulacion->listar_solicitudes_anulacion($fec1, $fec2, $emp_id, $est, $usuario_id);

$tr = '';
if ($result['estado'] == 1) {
    $i = 0;
    foreach ($result['data'] as $key => $value) {

        $estado = "";
        $estadobg = "";
        if ($value['tb_permisoanulacion_est'] == 1){
            $estado = "ACTIVO";
            $estadobg = "background-color: #3061F7; color: #FFFFFF;";
        }elseif ($value['tb_permisoanulacion_est'] == 2){
            $estado = "ATENDIDO";
            $estadobg = "background-color: #00BA49; color: #FFFFFF;";
        }elseif ($value['tb_permisoanulacion_est'] == 3){
            $estado = "RECHAZADO";
            $estadobg = "background-color: #F73030; color: #FFFFFF;";
        }

        $usuario_reg = "";
        $usuario = $oUsuario->mostrarUno($value['tb_permisoanulacion_user_reg']);
        if(($usuario['estado'] == 1)){
            $usuario_reg = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
        }

        $empresa_nom = "";
        $empresa = $oEmpresa->mostrarUno($value['tb_empresa_id']);
        if(($empresa['estado'] == 1)){
            $empresa_nom = $empresa['data']['tb_empresa_nomcom'];
        }

        $motivo_nom = "";
        $motivo = $oPermisoanulacion->mostrarMotivoanulacion($value['tb_motivoanulacion_id']);
        if(($motivo['estado'] == 1)){
            $motivo_nom = $motivo['data']['tb_motivoanulacion_des'];
        }

        $cuotapago_monto = 0.00;
        $ingreso_monto = 0.00;
        $egreso_monto = 0.00;
        $monto = '';
        $origen = "";
        if(intval($value['tb_permisoanulacion_origen']) == 1){
            $origen = "CMENOR - PAGO CUOTA";
            $cuotapago = $oPermisoanulacion->mostrarCuotapago($value['tb_permisoanulacion_modid'], 1);
            if($cuotapago['estado'] == 1){
                $cuotapago_monto = floatval($cuotapago['data']['tb_cuotapago_mon']);
                if($cuotapago['data']['tb_moneda_id'] == 1){
                    $monto = 'S/. '.mostrar_moneda($cuotapago_monto);
                }else{
                    $monto = '$ '.mostrar_moneda($cuotapago_monto);
                }
            }
        }elseif ($value['tb_permisoanulacion_origen'] == 2){
            $origen = "GARVEH - PAGO CUOTA DETALLE";
            $cuotapago = $oPermisoanulacion->mostrarCuotapago($value['tb_permisoanulacion_modid'], 2);
            if($cuotapago['estado'] == 1){
                $cuotapago_monto = floatval($cuotapago['data']['tb_cuotapago_mon']);
                if($cuotapago['data']['tb_moneda_id'] == 1){
                    $monto = 'S/. '.mostrar_moneda($cuotapago_monto);
                }else{
                    $monto = '$ '.mostrar_moneda($cuotapago_monto);
                }
            }
        }elseif ($value['tb_permisoanulacion_origen'] == 3){
            $origen = "INGRESO";
            $ingreso = $oPermisoanulacion->mostrarIngreso($value['tb_permisoanulacion_modid']);
            if($ingreso['estado'] == 1){
                $ingreso_monto = floatval($ingreso['data']['tb_ingreso_imp']);
                if($ingreso['data']['tb_moneda_id'] == 1){
                    $monto = 'S/. '.mostrar_moneda($ingreso_monto);
                }else{
                    $monto = '$ '.mostrar_moneda($ingreso_monto);
                }
            }
        }elseif ($value['tb_permisoanulacion_origen'] == 4){
            $origen = "EGRESO";
            $egreso = $oPermisoanulacion->mostrarEgreso($value['tb_permisoanulacion_modid']);
            if($egreso['estado'] == 1){
                $egreso_monto = floatval($egreso['data']['tb_egreso_imp']);
                if($egreso['data']['tb_moneda_id'] == 1){
                    $monto = 'S/. '.mostrar_moneda($egreso_monto);
                }else{
                    $monto = '$ '.mostrar_moneda($egreso_monto);
                }
            }
        }

        $tr .= '<tr  style="font-family: cambria;font-size: 12px;border-color:#135896;">';
        $tr .= '
            <td id="tabla_fila">' . ($i+1) . '</td>
            <td id="tabla_fila">' . mostrar_fecha_hora($value['tb_permisoanulacion_fecreg']) . '</td>
            <td id="tabla_fila">' . $usuario_reg . '</td>
            <td id="tabla_fila">' . $empresa_nom . '</td>
            <td id="tabla_fila">' . $origen . '</td>
            <td id="tabla_fila">' . $motivo_nom . '</td>
            <td id="tabla_fila">' . $value['tb_permisoanulacion_des'] . '</td>
            <td id="tabla_fila" style="text-align: right;">' . $monto . '</td>
            <td align="center" style="'.$estadobg.'">' . $estado . '</td>
            <td id="tabla_fila" style="text-align: center;">';
            if ($value['tb_permisoanulacion_origen'] == 1 && $value['tb_permisoanulacion_est'] == 1){
                $tr .= '<a class="btn btn-primary btn-xs" title="Editar" onclick="creditomenor_pagos(' . $value['tb_credito_id'] . ', 1)"><i class="fa fa-credit-card"></i></a>';
            }elseif($value['tb_permisoanulacion_origen'] == 2 && $value['tb_permisoanulacion_est'] == 1){
                $tr .= '<a class="btn btn-primary btn-xs" title="Editar" onclick="creditogarveh_pagos(' . $value['tb_credito_id'] . ', 3)"><i class="fa fa-credit-card"></i></a>';
            }elseif($value['tb_permisoanulacion_origen'] == 3 && $value['tb_permisoanulacion_est'] == 1){
                $tr .= '<a class="btn btn-primary btn-xs" title="Editar" onclick="ingreso_form(\'E\',' . $value['tb_permisoanulacion_modid'] . ')"><i class="fa fa-credit-card"></i></a>';
            }elseif($value['tb_permisoanulacion_origen'] == 4 && $value['tb_permisoanulacion_est'] == 1){
                $tr .= '<a class="btn btn-primary btn-xs" title="Editar" onclick="egreso_form(\'E\',' . $value['tb_permisoanulacion_modid'] . ')"><i class="fa fa-credit-card"></i></a>';
            }
       
        $tr .= '</td>';

        $tr .= '</tr>';
        $i++;
    }
    $result = null;
} else {
    $tr = '<td colspan="4">' . $result['mensaje'] . '</td>';
    $result = null;
}
?>
<table id="tbl_permisoanulacion" class="table table-hover">
    <thead>
        <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
            <th id="tabla_cabecera_fila">#</th>
            <th id="tabla_cabecera_fila">FECHA SOLICITUD</th>
            <th id="tabla_cabecera_fila">USUARIO SOLICITANTE</th>
            <th id="tabla_cabecera_fila">SEDE</th>
            <th id="tabla_cabecera_fila">ORIGEN</th>
            <th id="tabla_cabecera_fila">MOTIVO</th>
            <th id="tabla_cabecera_fila">DETALLE</th>
            <th id="tabla_cabecera_fila">MONTO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila"></th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>
