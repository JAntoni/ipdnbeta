<form id="permisoanulacion_filtro" name="permisoanulacion_filtro">
    <input type="hidden" class="form-control input-sm" id="usuariogrupo_id" name="usuariogrupo_id" value="<?php echo $_SESSION['usuariogrupo_id'];?>">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha </label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fec1" id="txt_fec1" value="<?php echo date('d-m-Y');?>"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fec2" id="txt_fec2" value="<?php echo date('d-m-Y');?>"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Estado</label>
                <select name="cmb_est" id="cmb_est" class="form-control input-sm">
                    <option value="1">ACTIVOS</option>
                    <option value="2">ATENDIDOS</option>
                    <option value="3">RECHAZADOS</option>
                    <option value="">TODOS</option>
                </select>
            </div>
        </div>
        
        <div class="col-md-2">
                <label for="">Sede :</label>
                <select class="form-control input-sm" id="cmb_empresa_id" name="cmb_empresa_id">
                    <?php require_once VISTA_URL.'empresa/empresa_select.php';?>
                </select>
        </div>
        <div class="col-md-4">
            <label for="">Usuario</label>
            <select name="cmb_usuario_id" id="cmb_usuario_id" class="form-control orm-control-sm selectpicker" data-live-search="true" data-max-options="1" data-size="12">
                <?php require_once VISTA_URL.'usuario/usuario_select.php';?>
                
            </select>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for=""> </label>
                <div class="input-group">
                    <button type="button" class="btn btn-primary btn-sm" onclick="permisoanulacion_tabla()" title="Filtrar"><i class="fa fa-search"></i></button>
                    <button type="button" class="btn btn-success btn-sm" onclick="reestablecerValores()" title="Reestablecer"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </div>
        
    </div>

</form>
