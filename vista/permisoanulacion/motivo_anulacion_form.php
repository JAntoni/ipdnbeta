<?php
session_name("ipdnsac");
session_start();

require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../permisoanulacion/Permisoanulacion.class.php');
$oPermisoanulacion = new Permisoanulacion();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_motivo_anulacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;">MOTIVO DE ANULACIÓN</h4>
            </div>
            
            <div class="modal-body">

                <div class="row">

                    <input type="hidden" name="accion_motivo" id="accion_motivo" value="registrar_motivo">

                    <div class="col-md-6">
                        <div class="form-group" style="width: 100%">
                            <label>Motivo: </label>
                            <div class="input-group" style="width: 100%">
                                <input type="text" class="form-control input-sm" name="motivo_nom" id="motivo_nom" style="width: 100%" placeholder="Motivo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label> </label>
                            <div class="input-group">
                                <button type="button" class="btn btn-primary btn-sm" id="guardar_motivo" name="guardar_motivo" onclick="guardarMotivo()" /><strong><i class="fa fa-save"></i> Añadir</strong></button>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" id="listaMotivos">

                    </div>
                    
                </div>
                
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="solicitar_anulacion_mensaje" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                </div>
            </div>

            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            
        </div>
    </div>
</div>
