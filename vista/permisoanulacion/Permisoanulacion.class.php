<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Permisoanulacion extends Conexion {
    
    function insertar($usuario_id, $empresa_id, $credito_id, $motivo_id, $origen, $modid, $modid2, $moneda_id, $monto, $txt_detalle) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_permisoanulacion (
                                        tb_permisoanulacion_xac, 
                                        tb_permisoanulacion_fecreg,
                                        tb_permisoanulacion_user_reg, 
                                        tb_permisoanulacion_fec_aten, 
                                        tb_permisoanulacion_user_aten, 
                                        tb_empresa_id, 
                                        tb_credito_id, 
                                        tb_motivoanulacion_id, 
                                        tb_permisoanulacion_origen, 
                                        tb_permisoanulacion_modid,
                                        tb_permisoanulacion_modid2,
                                        tb_moneda_id,
                                        tb_permisoanulacion_monto,
                                        tb_permisoanulacion_des,
                                        tb_permisoanulacion_mot,
                                        tb_permisoanulacion_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :usuario_id, 
                                        NULL, 
                                        NULL, 
                                        :empresa_id, 
                                        :credito_id, 
                                        :motivo_id, 
                                        :origen, 
                                        :modid, 
                                        :modid2, 
                                        :moneda_id, 
                                        :monto, 
                                        :txt_detalle, 
                                        NULL, 
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":motivo_id", $motivo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":origen", $origen, PDO::PARAM_INT);
            $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
            $sentencia->bindParam(":modid2", $modid2, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
            $sentencia->bindParam(":monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":txt_detalle", $txt_detalle, PDO::PARAM_STR);
            $result = $sentencia->execute();

            $permisoanulacion_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['permisoanulacion_id'] = $permisoanulacion_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarMotivo($motivo) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_motivoanulacion (
                                        tb_motivoanulacion_xac, 
                                        tb_motivoanulacion_fecreg,
                                        tb_motivoanulacion_des)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :motivo);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":motivo", $motivo, PDO::PARAM_STR);
            $result = $sentencia->execute();

            $motivo_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['motivo_id'] = $motivo_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function lista_motivo_anulacion(){
        try {
          $sql = "SELECT * FROM tb_motivoanulacion WHERE tb_motivoanulacion_xac = 1";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay Motivos de Anulación";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
  
    }

    function listar_solicitudes_anulacion($fec1, $fec2, $emp_id, $est, $usuario_id)
        {
      
        $extension = "";
        if ($emp_id > 0)
        $extension = $extension . " AND tb_empresa_id = $emp_id";
        if ($est != '')
        $extension = $extension . " AND tb_permisoanulacion_est = $est";
        if ($usuario_id > 0)
        $extension = $extension . " AND tb_permisoanulacion_user_reg = $usuario_id ";
    
        try {
        $sql = "SELECT *
                FROM tb_permisoanulacion
                WHERE tb_permisoanulacion_xac=1
                AND DATE(tb_permisoanulacion_fecreg) >= :fec1 
                AND DATE(tb_permisoanulacion_fecreg) <= :fec2 
                " . $extension . " ORDER BY tb_permisoanulacion_fecreg DESC";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
        $sentencia->execute();
    
        if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay Solicitudes registrado";
            $retorno["data"] = "";
        }
    
        return $retorno;
        } catch (Exception $e) {
        throw $e;
        }
    }
    
    function mostrarCuotapago($cuotapago_id, $modulo_id){
        try {
          $sql = "SELECT * FROM tb_cuotapago WHERE tb_cuotapago_id =:cuotapago_id AND tb_modulo_id =:modulo_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cuotapago_id", $cuotapago_id, PDO::PARAM_INT);
          $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay cuotapago registrado";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function mostrarIngreso($ingreso_id){
      try {
        $sql = "SELECT * FROM tb_ingreso WHERE tb_ingreso_id =:ingreso_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay ingreso registrado";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarEgreso($egreso_id){
      try {
        $sql = "SELECT * FROM tb_egreso WHERE tb_egreso_id =:egreso_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay ingreso registrado";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarPermisoanulacion($credito_id, $modulo_id){
      try {
        $sql = "SELECT * FROM tb_permisoanulacion WHERE tb_credito_id =:credito_id AND tb_permisoanulacion_modid =:modulo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay permiso registrado";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarPermisoanulacionIE($modid, $origen){
      try {
        $sql = "SELECT * FROM tb_permisoanulacion WHERE tb_permisoanulacion_modid =:modid AND tb_permisoanulacion_origen =:origen";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
        $sentencia->bindParam(":origen", $origen, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay permiso registrado";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }


    function mostrarPermisoanulacionGarveh($modulo_id2, $modulo_id){
      try {
        $sql = "SELECT * FROM tb_permisoanulacion WHERE tb_permisoanulacion_modid2 =:modulo_id2 AND tb_permisoanulacion_modid =:modulo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id2", $modulo_id2, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay permisos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarMotivoanulacion($motivo_id){
        try {
          $sql = "SELECT * FROM tb_motivoanulacion WHERE tb_motivoanulacion_id =:motivoanulacion_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":motivoanulacion_id", $motivo_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay Motivo registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    
    function mostrarSolicitudAnulacion($modid, $origen){
        try {
          $sql = "SELECT * 
                FROM tb_permisoanulacion 
                WHERE tb_permisoanulacion_modid =:modid 
                AND tb_permisoanulacion_origen =:origen 
                AND tb_permisoanulacion_est = 1 
                AND tb_permisoanulacion_xac = 1";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
          $sentencia->bindParam(":origen", $origen, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay cuotapago registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function modificar_campo_permisoanulacion($permisoanulacion_id, $permisoanulacion_columna, $permisoanulacion_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($permisoanulacion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_permisoanulacion SET " . $permisoanulacion_columna . " =:permisoanulacion_valor WHERE tb_permisoanulacion_id =:permisoanulacion_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":permisoanulacion_id", $permisoanulacion_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":permisoanulacion_valor", $permisoanulacion_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":permisoanulacion_valor", $permisoanulacion_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

}

?>