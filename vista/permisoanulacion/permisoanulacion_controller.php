<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
	require_once('../cliente/Cliente.class.php');
	$oCliente = new Cliente();
	require_once('../usuario/Usuario.class.php');
  	$oUsuario = new Usuario();
	require_once('../ingreso/Ingreso.class.php');
	$oIngreso = new Ingreso();
	require_once('../egreso/Egreso.class.php');
	$oEgreso = new Egreso();
	require_once('../empresa/Empresa.class.php');
	$oEmpresa = new Empresa();
    require_once('../permisoanulacion/Permisoanulacion.class.php');
	$oPermisoanulacion = new Permisoanulacion();

	require_once ("../funciones/funciones.php");
	require_once ("../funciones/fechas.php");

	$fecha_hoy = date('Y-m-d h:i a');

 	$action = $_POST['action'];

 	if($action == 'solicitar_anulacion'){
        $usuario_id = $_SESSION['usuario_id'];
        $empresa_id = $_SESSION['empresa_id'];
 		$modid = intval($_POST['hdd_modid']);
 		$origen = intval($_POST['hdd_origen']);
 		$motivo_id = intval($_POST['cmb_motivo']);
 		$txt_detalle = $_POST['txt_detalle'];
        // ORIGEN: 1=cuota(cmenor), 2=cuotadetalle(cgarveh), 3=ingreso, 4=egreso
        // MODID: 1,2 = cuotapago_id | 3 = ingreso_id | 4 = egreso_id
        $credito_id = intval($_POST['hdd_credito_id']);
        $creditotipo_id = intval($_POST['hdd_creditotipo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la solicitud de anulación.';

        $modid2 = NULL;
        $monto = 0.00;
        $moneda_id = 0;
        if($origen==1 || $origen==2){
            if($origen==1){ // cuota
                $cuotapago = $oPermisoanulacion->mostrarCuotapago($modid, 1);
                if($cuotapago['estado'] == 1){
                    $modid2 = intval($cuotapago['data']['tb_cuotapago_modid']);
					$monto = floatval($cuotapago['data']['tb_cuotapago_mon']);
					$moneda_id = intval($cuotapago['data']['tb_moneda_id']);

                }
            }elseif($origen==2){ // cuotadetalle
                $cuotapago2 = $oPermisoanulacion->mostrarCuotapago($modid, 2);
                if($cuotapago2['estado'] == 1){
                    $modid2 = intval($cuotapago2['data']['tb_cuotapago_modid']);
					$monto = floatval($cuotapag2['data']['tb_cuotapago_mon']);
					$moneda_id = intval($cuotapago2['data']['tb_moneda_id']);
                }
            }
        }elseif($origen==3){ // ingreso
			$ingreso = $oIngreso->mostrarUno($modid);
			if($ingreso['estado'] == 1){
				$monto = floatval($ingreso['data']['tb_ingreso_imp']);
				$moneda_id = intval($ingreso['data']['tb_moneda_id']);
			}
		}elseif($origen==4){ // egreso
			$egreso = $oEgreso->mostrarUno($modid);
			if($egreso['estado'] == 1){
				$monto = floatval($egreso['data']['tb_egreso_imp']);
				$moneda_id = intval($ingreso['data']['tb_moneda_id']);
			}
		}	

		$res= $oPermisoanulacion->insertar($usuario_id, $empresa_id, $credito_id, $motivo_id, $origen, $modid, $modid2, $moneda_id, $monto, $txt_detalle);

 		if($res['estado'] == 1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Solicitud registrada correctamente.';
			$data['credito_id'] = $credito_id; //ultimo id de registro
			$data['creditotipo_id'] = $creditotipo_id;
 		}

 		echo json_encode($data);
    }

    if($action=="registrar_motivo"){
		
		$motivo = ($_POST['motivo'] != '') ? $_POST['motivo'] : NULL;

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al registrar el motivo.';

		$res= $oPermisoanulacion->insertarMotivo($motivo);
 		if(intval($res['estado']) == 1){

 			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';

		}

		echo json_encode($data);

	}

    if($action=="listado_motivo"){

		$motivos = $oPermisoanulacion->lista_motivo_anulacion();

		$html = '';
		$html .= '<div class="col-md-12">
					<table class="table table-hover table-responsive">
						<thead>
							<tr id="tabla_cabecera">
								<th id="tabla_cabecera_fila" width="10%">#</th>
								<th id="tabla_cabecera_fila" width="90%">Motivo</th>
							</tr>
						</thead>';
					
				$html .= '<tbody>';

						if($motivos['estado']==1){
							foreach ($motivos['data'] as $key => $value) {


								$html .= '<tr id="tabla_cabecera_fila">
											<td id="tabla_fila" style="">'.($i+1).'</td>
											<td id="tabla_fila" style="text-align: center;">'.$value['tb_motivoanulacion_des'].'</td>
										</tr>';

								$i++;
							}
						}
						
				$html .= '</tbody>
					</table>
				</div>';

		echo $html;
  	}

?>