<?php
echo $_SESSION['usuario_id'];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Caja</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Permisos de Anulación</a></li>
            <li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
        </ol>
    </section>

	<!-- Main content -->
	<section class="content">
             <?php
                    //vamos a validar que la caja est茅 aperturada para poder hacer permisoanulacion de todo tipo
                    require_once ("vista/funciones/funciones.php");
                    validar_apertura_caja();
                  ?>
            <div class="box box-primary">
                <?php if($_SESSION['usuario_id'] == 2 || $_SESSION['usuario_id'] == 11 || $_SESSION['usuario_id'] == 61){ ?>

                    <div class="box-header">
                            <button class="btn btn-primary btn-sm" onclick="motivoanulacion_form('registrar_motivo')"><i class="fa fa-plus"></i> Nuevo Motivo</button>
                    </div>
                    
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-12">
                                <label for="txt_filtro" class="control-label">Filtro de Solicitudes</label>
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                    <?php include 'permisoanulacion_filtro.php';?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                            <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="permisoanulacion_mensaje_tbl" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Solicitudes...</h4>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Input para guardar el valor ingresado en el search de la tabla-->
                                <input type="hidden" id="hdd_datatable_fil">

                                <div id="div_permisoanulacion_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                        <?php require_once('permisoanulacion_tabla.php');?>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                <?php }else{ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>No tiene permiso para visualizar esta sección.</h3>
                        </div>
                    </div>
                <?php } ?>

                <div id="div_modal_motivo_anulacion">
                    <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
                </div>
                <div id="div_modal_creditomenor_pagos"></div>
                <div id="div_modal_creditogarveh_pagos"></div>
                <div id="div_modal_ingreso_form"></div>
                <div id="div_modal_egreso_form"></div>
                
                <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
                <?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
            </div>
	</section>
	<!-- /.content -->
</div>
