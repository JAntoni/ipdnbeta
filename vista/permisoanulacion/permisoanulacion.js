
$(document).ready(function() {

    permisoanulacion_tabla();
    var empresa_id=$('#cmb_empresa_id').val();
    
    var usuariogrupo=$('#usuariogrupo_id').val();
    
    if(usuariogrupo==2){
        usuario_select(empresa_id);
    }
    
    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });
    permisoanulacion_tabla();
      
    $('#form_solicitar_anulacion').validate({
        submitHandler: function() {
            $.ajax({
            type: "POST",
            url: VISTA_URL+"permisoanulacion/permisoanulacion_controller.php",
            async: true,
            dataType: "json",
            data: $("#form_solicitar_anulacion").serialize(),
            beforeSend: function() {
                $('#btn_guardar_solicitar_anulacion').prop('disabled', true);
            },
            success: function(data){
                if(parseInt(data.estado) == 0){
                    Swal.fire(
                        'Error!',
                        data.mensaje,
                        'error'
                    );
                    $('#btn_guardar_solicitar_anulacion').prop('disabled', false);
                }else
                if(parseInt(data.estado) == 1){
                    $('#modal_solicitar_anulacion').modal('hide');
                    Swal.fire(
                        'Excelente!',
                        data.mensaje,
                        'success'
                    );
                    cuotapago_pagos_tabla(data.credito_id, data.creditotipo_id);
                }
            },
            complete: function(data){
                
            },
                
            });
        },
        rules: {
            txt_detalle: {
                required: true
            },
            cmb_motivo: {
                required: true
            }
        },
        messages: {
            txt_detalle: {
                required: 'Ingrese detalle de anulación'
            },
            cmb_motivo: {
                required: 'Seleccione el motivo'
            }
        }
    });

});

function permisoanulacion_tabla(){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"permisoanulacion/permisoanulacion_tabla.php",
      async: true,
      dataType: "html",
      data: $('#permisoanulacion_filtro').serialize(),
      beforeSend: function() {
        $('#permisoanulacion_mensaje_tbl').show(300);
      },
      success: function(data){
        $('#div_permisoanulacion_tabla').html(data);
        $('#permisoanulacion_mensaje_tbl').hide(300);
  
        let registros = data.includes("No hay");
        console.log(registros);
        if (!registros) estilos_datatable();
        
      },
      complete: function(data){
        
      },
      error: function(data){
        $('#permisoanulacion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
      }
    });
}
/* function estilos_datatable(){
    datatable_global = $('#tbl_permisoanulacion').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
        { targets: [1,2,3,4,5,6,7], orderable: false }
        ]
    });

    datatable_texto_filtrar();
} */

function estilos_datatable(){
    datatable_global = $('#tbl_permisoanulacion').DataTable({
      "pageLength": 50,
      "responsive": true,
      "bScrollCollapse": true,
      "bPaginate": false,
      "bJQueryUI": true,
      //"scrollX": true,
      //"scrollY": "80vh",
      dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
      //dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
      buttons: [
        { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
        { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
        { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
      ],
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
      order: [],
      columnDefs: [
        { targets: 'no-sort', orderable: false }
        ]
      /*drawCallback: function () {
        var sum_desembolsado = $('#tbl_infocorp').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
        var sum_capital_rest_sol = $('#tbl_infocorp').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles
  
        $('#total').html(sum);
      }*/
    });
  
    datatable_texto_filtrar();
}

function datatable_texto_filtrar(){
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function(event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if(text_fil){
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function reestablecerValores(){
    
    let date = new Date()

    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()

    if(month < 10){
      $("#txt_fec1").val(day+"-0"+month+"-"+year);
    $("#txt_fec2").val(day+"-0"+month+"-"+year);
    }else{
      $("#txt_fec1").val(day+"-"+month+"-"+year);
    $("#txt_fec2").val(day+"-"+month+"-"+year);
    }
    
    $("#cmb_est").val("");
    permisoanulacion_tabla();
}

$("#txt_fec1,#txt_fec2,#cmb_usuario_id,#cmb_est,#cmb_empresa_id").change(function(){
    permisoanulacion_tabla();
});

function cuotapago_pagos_tabla(credito_id, creditotipo_id){ // traido para que se recargue la tabla de pagos
    $.ajax({
        type: "POST",
        url: VISTA_URL+"cuotapago/cuotapago_pagos_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            credito_id: credito_id,
            creditotipo_id: creditotipo_id
        }),
        beforeSend: function() {
            $('#creditomenor_pagos_mensaje_tbl').show(300);
        },
        success: function(data){
            $('#creditomenor_pagos_mensaje_tbl').hide(300);
            $('#creditomenor_pagos_tabla').html(data);
        },
        complete: function(data){
            
        },
        error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
        }
    });
}

function motivoanulacion_form(accion){
    $.ajax({
        type: "POST",
        url: VISTA_URL+"permisoanulacion/motivo_anulacion_form.php",
        async: true,
            dataType: "html",
            data: ({
                accion: accion,
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
                $('#div_modal_motivo_anulacion').html(data);
                $('#modal_motivo_anulacion').modal('show');
        
                listadoMotivos();
                
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_motivo_anulacion'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_motivo_anulacion', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
                //llamar al formulario de solicitar permiso
                var modulo = 'ingreso';
                var div = 'div_modal_motivo_anulacion';
                permiso_solicitud(usuario_act, ingreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){
            
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
                    console.log(data.responseText);
        }
    });
}

function usuario_select(empresa_id){ 
    $.ajax({
      type: "POST",
      url: VISTA_URL + "usuario/usuario_select.php",
      async: false,
      dataType: "html",
      data: {
        usuario_columna: 'tb_usuario_ofi',
        usuario_valor: 1, //para que solo muestre usuarios de oficina o que trabajaron en oficina
        param_tip: 'INT',
        empresa_id: empresa_id
      },
      beforeSend: function () {
        $("#cmb_usuario_id").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#cmb_usuario_id").html(html);
        $("#cmb_usuario_id").selectpicker('refresh');
      },
      complete: function (html) {},
    });
}

function listadoMotivos(){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "permisoanulacion/permisoanulacion_controller.php",
		async:true,
		dataType: "html",                      
		data: ({
		  action: 'listado_motivo'
		}),
		beforeSend: function() {
		},
		success: function(html){
			$('#listaMotivos').html(html);
		}
	});
}

function guardarMotivo() {
	$.ajax({
		type: "POST",
		url: VISTA_URL+"permisoanulacion/permisoanulacion_controller.php",
		async: true,
		dataType: "json",
		data: {
			action: $('#accion_motivo').val(),
			motivo: $('#motivo_nom').val(),
		},
		beforeSend: function() {
		},
		success: function(data){
			if(data.estado==1){
				notificacion_success(data.mensaje, 3000);
        		listadoMotivos();
				$('#motivo_nom').val('');
			}else{
				notificacion_warning(data.mensaje, 3000);
			}
		},
		complete: function(data){
		},
			
	});
}

function creditomenor_pagos(creditomenor_id) { // modal de pago de cuotas cmenor
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditomenor/creditomenor_pagos.php",
        async: true,
        dataType: "html",
        data: ({
            credito_id: creditomenor_id,
            hdd_form: 'vencimiento'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_creditomenor_pagos').html(data);
                $('#modal_creditomenor_pagos').modal('show');

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_creditomenor_pagos', 95);
                modal_height_auto('modal_creditomenor_pagos'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_creditomenor_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

function creditogarveh_pagos(creditogarveh_id){ // modal de pago de cuotas garveh
    $.ajax({
      type: "POST",
      url: VISTA_URL+"creditogarveh/creditogarveh_pagos.php",
      async: true,
      dataType: "html",
      data: ({
        credito_id: creditogarveh_id
      }),
      beforeSend: function() {
        $('#h3_modal_title').text('Cargando Formulario');
        $('#modal_mensaje').modal('show');
      },
      success: function(data){
        $('#modal_mensaje').modal('hide');
        if(data != 'sin_datos'){
          $('#div_modal_creditogarveh_pagos').html(data);
          $('#modal_creditogarveh_pagos').modal('show');
  
          //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
          modal_width_auto('modal_creditogarveh_pagos', 95);
          modal_height_auto('modal_creditogarveh_pagos'); //funcion encontrada en public/js/generales.js
          modal_hidden_bs_modal('modal_creditogarveh_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function(data){
  
      },
      error: function(data){
        $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
        $('#overlay_modal_mensaje').removeClass('overlay').empty();
        $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
        console.log(data.responseText);
      }
    });
}

function vencimiento_tabla() {
    if($("#hdd_fil_cli_id").val()=="" || $("#hdd_fil_cli_id").val()==0){
        alerta_error("ERROR", "DEBE SELECCIONAR UN CLIENTE");
        return false;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/vencimiento_tabla.php",
        async: true,
        dataType: "html",
        data: $("#vencimiento_filtro").serialize(),
        beforeSend: function () {
            $('#msj_vencimiento_tabla').html("Cargando datos...");
            $('#msj_vencimiento_tabla').show(100);
            $('#div_vencimiento_tabla').addClass("ui-state-disabled");

            $('#btnSearchComplete').prop('disabled', true);
            $('#btnSearch').prop('disabled', true);

            //$('#div_vencimiento_tabla').hide();
            //$('#div_vencimiento_tabla').html('');
        },
        success: function (html) {
            $('#div_vencimiento_tabla').show(500);
            $('#div_vencimiento_tabla').html(html);
            permisoanulacion_tabla();
        },
        complete: function () {
            $('#btnSearchComplete').prop('disabled', false);
            $('#btnSearch').prop('disabled', false);
            $('#div_vencimiento_tabla').removeClass("ui-state-disabled");
            $('#msj_vencimiento_tabla').hide(100);
        }
    });
}

function cuotapago_anular_garveh(cuotapago_id) {
    $.confirm({
        icon: 'fa fa-remove',
        title: 'Anular',
        content: '¿Desea anular este pago?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "cuotapago/cuotapago_controller_garveh.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: "anular",
                        cuotapago_id: cuotapago_id
                    }),
                    beforeSend: function () {
                        $('#h3_modal_title').text('Anulando...');
                        $('#modal_mensaje').modal('show');
                        $('#body_modal_mensaje').html('Espere');
                    },
                    success: function (data) {
                        if (parseInt(data.estado) == 1) {
                            alerta_success('Exito', data.mensaje);
                            cuotapago_pagos_tabla();

                            var hdd_form = $('#hdd_form').val();
                            if (hdd_form == 'vencimiento') {
                                vencimiento_tabla();
                            }
                        } else {
                            alerta_warning('Alerta', data.mensaje); //en generales.js
                        }
                    },
                    complete: function (data) {
                        $('#modal_mensaje').modal('hide');
                    },
                    error: function (data) {
                        alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
                        //console.log(data.responseText);
                    }
                });
            },
            no: function () {}
        }
    });
}

// PARA INGRESOS
function ingreso_form(usuario_act, ingreso_id){ 
    $.ajax({
        type: "POST",
        url: VISTA_URL+"ingreso/ingreso_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            ingreso_id: ingreso_id,
            vista: 'permisoanulacion'
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
                $('#div_modal_ingreso_form').html(data);
                $('#modal_registro_ingreso').modal('show');
    
            //desabilitar elementos del form si es L (LEER)
            if(usuario_act == 'L' || usuario_act == 'E')
                form_desabilitar_elementos('form_ingreso'); //funcion encontrada en public/js/generales.js
            
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_registro_ingreso'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_ingreso', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
                //llamar al formulario de solicitar permiso
                var modulo = 'ingreso';
                var div = 'div_modal_ingreso_form';
                permiso_solicitud(usuario_act, ingreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){
                
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
                console.log(data.responseText);
        }
    });
}

// PARA EGRESOS
function egreso_form(usuario_act, egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            egreso_id: egreso_id,
            vista: 'permisoanulacion'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_egreso_form').html(data);
                $('#modal_registro_egreso').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_egreso'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_egreso'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_egreso', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'egreso';
                var div = 'div_modal_egreso_form';
                permiso_solicitud(usuario_act, egreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}