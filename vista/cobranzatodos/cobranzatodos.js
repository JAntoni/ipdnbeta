var datatable_global;

function cobranzatodos_tabla(){
  cliente_nom = $('#txt_filtro_cliente').val().trim();
  checkbox_cmenor = 0;

  if($('#txt_filtro_cmenor').is(":checked"))
    checkbox_cmenor = 1;

  if(!cliente_nom)
    $('#hdd_fil_cliente_id').val(0);

  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzatodos/cobranzatodos_tabla.php",
    async: true,
    dataType: "JSON",
    data: ({
      cuota_fec1: $('#txt_filtro_fec1').val(),
      cuota_fec2: $('#txt_filtro_fec2').val(),
      cliente_id: $('#hdd_fil_cliente_id').val(),
      cuota_condicion: $('#cmb_filtro_condicion').val(),
      checkbox_cmenor: checkbox_cmenor,
      vista_modulo: 'cobranzatodos'
    }),
    beforeSend: function() {
      $('#cobranzatodos_mensaje_tbl').show(300);
      $('#span_cargando').text('Cargando Tabla GARVEH...');
      datatable_global.destroy();
      $('#tbl_cobranzatodos_lista').html('')
    },
    success: function(data){
      $('#cobranzatodos_mensaje_tbl').hide(300);
      
      $('#tbl_cobranzatodos_lista').append(data.tabla);
      estilos_datatable();
      $('#tbl_cobranzatodos_resumen').html(data.resumen);
      
      $('#div_segmentado').hide();
      $('#div_segmentado').html('');
      $('.div_listado').show();
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      $('#cobranzatodos_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function cobranzatodos_segmentado(){
  cliente_nom = $('#txt_filtro_cliente').val().trim();
  checkbox_cmenor = 0;

  if($('#txt_filtro_cmenor').is(":checked"))
    checkbox_cmenor = 1;

  if(!cliente_nom)
    $('#hdd_fil_cliente_id').val(0);

  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzatodos/cobranzatodos_segmentado.php",
    async: true,
    dataType: "html",
    data: ({
      cuota_fec1: $('#txt_filtro_fec1').val(),
      cuota_fec2: $('#txt_filtro_fec2').val(),
      cliente_id: $('#hdd_fil_cliente_id').val(),
      cuota_condicion: $('#cmb_filtro_condicion').val(),
      checkbox_cmenor: checkbox_cmenor,
      vista_modulo: 'cobranzatodos'
    }),
    beforeSend: function() {
      $('#cobranzatodos_mensaje_tbl').show(300);
      $('#span_cargando').text('Cargando Tablas .....');
    },
    success: function(data){
      $('#cobranzatodos_mensaje_tbl').hide(300);
      $('.div_listado').hide();
      $('#div_segmentado').show();
      $('#div_segmentado').html(data);
      console.log(data);
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      $('#div_segmentado').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function estilos_datatable(){
  datatable_global = $('#table_cobranzatodos').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    "bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    //scrollY: 700,
    fixedColumns: {
      left: 4
    },
    //"scrollY": "80vh",
    //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
    dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
    buttons: [
      { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
      { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
      { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
      { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
    ],
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
    /*drawCallback: function () {
      var sum_desembolsado = $('#tbl_comportamiento').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
      var sum_capital_rest_sol = $('#tbl_comportamiento').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles

      $('#total').html(sum);
    }*/
  });
}

function opcionesgc_asignar_form(credito_tipo_numero, credito_id, cuotadetalle_id, credito_nombre, credito_subtipo){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_asignar_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: 'I', // PUEDE SER: L, I, M , E
      credito_tipo_numero: credito_tipo_numero,
      credito_id: credito_id,
      cuotadetalle_id: cuotadetalle_id,
      credito_nombre: credito_nombre,
      credito_subtipo: credito_subtipo,
      modulo_nombre: 'cobranzatodos'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_asignar_form').html(data);
      	$('#modal_registro_asignaropciongc').modal('show');

      	modal_hidden_bs_modal('modal_registro_asignaropciongc', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function opcionesgc_timeline(credito_tipo, credito_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_timeline.php",
    async: true,
		dataType: "html",
		data: ({
      credito_tipo: credito_tipo, // PUEDE SER: L, I, M , E
      credito_id: credito_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_timeline').html(data);
      	$('#modal_opcionesgc_timeline').modal('show');

      	modal_hidden_bs_modal('modal_opcionesgc_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
        modal_height_auto('modal_opcionesgc_timeline'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

$(document).ready(function() {
  console.log('cambios al 28-08-2024 -- 2')

  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  estilos_datatable();

  // cobranzatodos_tabla();

  $('#datetimepicker1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  }).on('click', function(ev){
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('#datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  }).on('click', function(ev){
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });

  // $("#datetimepicker1").on("change", function (e) {
  //   var startVal = $('#txt_filtro_fec1').val();
  //   $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  // });
  // $("#datetimepicker2").on("change", function (e) {
  //   var endVal = $('#txt_filtro_fec2').val();
  //   $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  // });

  $("#txt_filtro_cliente").autocomplete({
    minLength: 2,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        {term: request.term}, //
        response
      );
    },
    select: function (event, ui) {
      $('#hdd_fil_cliente_id').val(ui.item.cliente_id);
      $('#txt_filtro_cliente').val(ui.item.cliente_nom);
      
      event.preventDefault();
      $('#txt_filtro_cliente').focus();
    }
});
});