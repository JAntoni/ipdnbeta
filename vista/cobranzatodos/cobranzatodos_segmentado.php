<?php
require_once('../../core/usuario_sesion.php');
require_once('../cuota/Cuota.class.php');
require_once('../ingreso/Ingreso.class.php');
require_once('../cobranzatodos/CobranzaTodos.class.php');
require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$oCobranzatodos = new CobranzaTodos();
$oIngreso = new Ingreso();
$oAsignar = new AsignarOpcionGc();

$cuota_fec1 = date('Y-m-01');
$cuota_fec2 = date('Y-m-d'); // vamos a empezar

$fecha_hoy = date('Y-m-d');
$cuota_fec1 = (isset($_POST['cuota_fec1'])) ? fecha_mysql($_POST['cuota_fec1']) : fecha_mysql($cuota_fec1);
$cuota_fec2 = (isset($_POST['cuota_fec2'])) ? fecha_mysql($_POST['cuota_fec2']) : fecha_mysql($cuota_fec2);
$cliente_id = (isset($_POST['cliente_id'])) ? intval($_POST['cliente_id']) : 0;
$cuota_condicion = (isset($_POST['cuota_condicion'])) ? intval($_POST['cuota_condicion']) : 1;
$checkbox_cmenor = (isset($_POST['checkbox_cmenor'])) ? intval($_POST['checkbox_cmenor']) : 0;

$data_cmenor = '';
$return_mejorado['estado'] = 0;
$return_mejorado['mensaje'] = 'Existe un error al parecer en la carga de datos';
$numero_orden = 0;

$TOTAL_CUOTAS_SOLES = 0;
$TOTAL_CUOTAS_DOLARES = 0;
$TOTAL_COBRADO_SOLES = 0;
$TOTAL_COBRADO_DOLARES = 0;
$SALDO_COBRAR_SOLES = 0;
$SALDO_COBRAR_DOLARES = 0;
$C_MENOR_SOLES = 0;

//? SI EL USUARIO DECIDE AGREGAR CRÉDITO MENOR EN EL LISTADO DE CROBRANZA DETALLADO DE TODA LA DEUDA VENCIDA

  $result = $oCobranzatodos->cobranzatodos_cuotas_facturadas_cmenor($cuota_fec1, $cuota_fec2, $fecha_hoy, '3', $cliente_id, $cuota_condicion);
    if ($result['estado'] == 1) {
      foreach ($result['data'] as $key => $value) {
        $cuo_tipo = 'CUOTA FIJA';
        if ($value['tb_cuotatipo_id'] == 1) {
          $cuo_tipo = 'CUOTA LIBRE';
        }

        $credito_general = 'Crédito MENOR';
        $credito_simbolo = 'MENOR';
        $credito_subtipo1 = '';
        $estado_cuota = 'SIN ESTADO CUOTA';

        if (intval($value['tb_cuota_est']) == 1) $estado_cuota = 'Pendiente';
        if (intval($value['tb_cuota_est']) == 2) $estado_cuota = 'Pagada';
        if (intval($value['tb_cuota_est']) == 3) $estado_cuota = 'Pago Parcial';

        // $capital_desembolsado = $value['tb_credito_preaco'];
        // $capital_restante = $value['tb_cuota_cap']; //si la cuota está impaga o pago parcial debe el capital hasta esa fecha
        // $capital_restante_soles = $capital_restante;
        // $capital_desembolsado_soles = $capital_desembolsado;
        $pago_parcial = 0;
        $numero_orden++;
        $moneda_simbolo = 'S/.';
        $monto_cuota = $value['cuota_real']; //que toma en cuenta el monto de la cuota detalle, en casos de que sea semanal
        $estado_credito = 'Vigente';
        $class_td = 'class="success"';

        $cliente_documento = $value['tb_cliente_doc'];
        $cliente_nombres = $value['tb_cliente_nom'];
        $representante_dni = '';
        $representante_nombres = '';
        $vencimiento = '';
        $ultimopago_fecha = '';

        if ($value['tb_moneda_id'] == 1) {
          $TOTAL_CUOTAS_SOLES += $monto_cuota;
          $C_MENOR_SOLES += $monto_cuota;
        }
        if ($value['tb_moneda_id'] == 2) {
          $moneda_simbolo = 'US$';
          $TOTAL_CUOTAS_DOLARES += $monto_cuota;
        }
        if (intval($value['tb_cuota_est']) == 3) { //todas las cuotas en pago parcial
          $tipo_cuota_pago = 1; // 1 es para pago de cuota
          $cuota_id = intval($value['tb_cuota_id']);

          if (intval($value['tb_cuota_id']) > 0) {
            $tipo_cuota_pago = 1; //1 para pago de cuota
            $cuota_id = intval($value['tb_cuota_id']);
          }

          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
          if ($result2['estado'] == 1) {
            $pago_parcial = $result2['data']['importe_total'];
            if ($value['tb_moneda_id'] == 1)
              $TOTAL_COBRADO_SOLES += $pago_parcial;
            if ($value['tb_moneda_id'] == 2)
              $TOTAL_COBRADO_DOLARES += $pago_parcial;
          }
          $result2 = NULL;
        }

        $saldo_por_pagar = $monto_cuota - $pago_parcial;

        // //! consulta critica para revisar
        // $result2 = $oIngreso->ultima_fecha_que_pago_cliente($value['tb_credito_id'], $value['credito_general'] );
        //   if($result2['estado'] == 1){
        //     $ultimopago_fecha = $result2['data']['ultima_fecha'];
        //     $monto_ultimo_pago = $result2['data']['tb_cuotapago_mon'];
        //   }
        // $result2 = NULL;
        // //! FIN CONSULTA CRITICA PAREA REVISAR

        if (strtotime($fecha_hoy) == strtotime($value['tb_cuota_fec'])) { //cuota vence hoy
          $vencimiento = '<b style="color: blue;">' . mostrar_fecha($value['tb_cuota_fec']) . ' (Vence hoy)</b>';
        }
        if (strtotime($fecha_hoy) < strtotime($value['tb_cuota_fec'])) { //cuota aun no vence
          $resta_fecha = restaFechas($fecha_hoy, $value['tb_cuota_fec']);
          $vencimiento = '<b style="color: green;">' . mostrar_fecha($value['tb_cuota_fec']) . ' (Vence en ' . $resta_fecha . ' días)</b>';
        }
        if (strtotime($fecha_hoy) > strtotime($value['tb_cuota_fec'])) { //todo codigo aquí dentro es para cuiando la cuota está vencida
          $resta_fecha = restaFechas($value['tb_cuota_fec'], $fecha_hoy);
          $vencimiento = '<b style="color: red;">' . mostrar_fecha($value['tb_cuota_fec']) . ' (Venció hace ' . $resta_fecha . ' días)</b>';
        }

        $data_cmenor .= '
              <tr>
                <td>' . $numero_orden . '</td>
                <td>' . $value['tb_credito_id'] . '</td>
                <td>' . $cliente_nombres . '</td>
                <td>' . $representante_nombres . '</td>
                <td>' . $value['tb_cliente_cel'] . '</td>
                <td><b>' . $credito_general . '</b></td>
                <td>' . $credito_subtipo1 . '</td>
                <td>' . $cuo_tipo . '</td>
                <td>' . $moneda_simbolo . '</td>
                <td>' . $monto_cuota . '</td>
                <td>' . $pago_parcial . '</td>
                <td>' . $saldo_por_pagar . '</td>
                <td>' . $vencimiento . '</td>
                <td>' . $value['tb_cuota_num'] . ' / ' . $value['num_cuotas_totales'] . '</td>
                <td>' . $ultimopago_fecha . '</td>
                <td id="td_opciones_' . $value['tb_cuota_id'] . '"></td>
                <td></td>
                <td id="td_comentario_' . $value['tb_cuota_id'] . '"></td>
                <td>
                  
                </td>
              </tr>
            ';
      }

      $data_cmenor .= '
        <tr>
          <td colspan="11" align="right">Total por Pagar <b>Crédito Menor:</b></td>
          <td><b>' . $C_MENOR_SOLES . '</b></td>
          <td colspan="7"></td>
        </tr>
      ';
    }
  $result = NULL;

$estados_creditos = '3'; // 3 vigente, 4 paralizado

$data_asiveh = '';
$cuotas_soles_asiveh = 0;
$cuotas_dolares_asiveh = 0;
$cobrado_soles_asiveh = 0;
$cobrado_dolares_asiveh = 0;
//? ------------------------------------------ LISTA DE CUOTAS SEGÚN RANGO DE ASIVEH -----------------------------------
$result = $oCobranzatodos->cobranzatodos_cuotas_facturadas_tipocredito($cuota_fec1, $cuota_fec2, $fecha_hoy, $estados_creditos, $cliente_id, $cuota_condicion, 2);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $cuo_tipo = 'CUOTA FIJA';
      if ($value['tb_cuotatipo_id'] == 3) {
        $cuo_tipo = 'CUOTA LIBRE';
      }

      $credito_general = 'SIN GENERAL';
      $credito_simbolo = 'REVISAR';
      if (intval($value['credito_general']) == 1) $credito_general = 'Crédito MENOR';
      if (intval($value['credito_general']) == 2) {
        $credito_general = 'Crédito ASIVEH';
        $credito_simbolo = 'ASIVEH';
      }
      if (intval($value['credito_general']) == 3) {
        $credito_general = 'Crédito GARVEH';
        $credito_simbolo = 'GARVEH';
      }
      if (intval($value['credito_general']) == 4) {
        $credito_general = 'Crédito HIPOTECARIO';
        $credito_simbolo = 'HIPOTECA';
      }

      $credito_subtipo1 = 'SIN TIPO CREDITO';
      if ($value['credito_tipo'] == 1) $credito_subtipo1 = $credito_simbolo;
      if ($value['credito_tipo'] == 2) $credito_subtipo1 = $credito_simbolo . " ADENDA";
      if ($value['credito_tipo'] == 3) $credito_subtipo1 = $credito_simbolo . " ACUERDO PAGO";
      if ($value['credito_tipo'] == 4) $credito_subtipo1 = "GARVEH MOBILIARIA";

      if (intval($value['credito_general']) == 4) { // PARA CREDITO HIPOTECARIO CAMBIA SUS SUBTIPOS
        if ($value['credito_tipo'] == 1) $credito_subtipo1 = "GARANTIA INMUEBLE C/V";
        if ($value['credito_tipo'] == 2) $credito_subtipo1 = "HIPOTECA ADENDA";
        if ($value['credito_tipo'] == 3) $credito_subtipo1 = "HIPOTECA ACUERDO PAGO";
        if ($value['credito_tipo'] == 4) $credito_subtipo1 = "HIPOTECARIA";
      }

      $credito_subtipo2 = 'SIN SUBTIPO';
      if ($value['credito_subtipo'] == 1) $credito_subtipo2 = " CREDITO REGULAR";
      if ($value['credito_subtipo'] == 2) $credito_subtipo2 = " CREDITO ESPECÍFICO";
      if ($value['credito_subtipo'] == 3) $credito_subtipo2 = " REPROGRAMADO";
      if ($value['credito_subtipo'] == 4) $credito_subtipo2 = " CUOTA BALON";
      if ($value['credito_subtipo'] == 5) $credito_subtipo2 = " REFINANCIADO AMORTIZADO";
      if ($value['credito_subtipo'] == 6) $credito_subtipo2 = " REFINANCIADO";

      $estado_cuota = 'SIN ESTADO CUOTA';
      if (intval($value['tb_cuota_est']) == 1) $estado_cuota = 'Pendiente';
      if (intval($value['tb_cuota_est']) == 2) $estado_cuota = 'Pagada';
      if (intval($value['tb_cuota_est']) == 3) $estado_cuota = 'Pago Parcial';

      // $capital_desembolsado = $value['tb_credito_preaco'];
      // $capital_restante = $value['tb_cuota_cap']; //si la cuota está impaga o pago parcial debe el capital hasta esa fecha
      // $capital_restante_soles = $capital_restante;
      // $capital_desembolsado_soles = $capital_desembolsado;
      $pago_parcial = 0;
      $numero_orden++;
      $moneda_simbolo = 'S/.';
      $monto_cuota = $value['cuota_real']; //que toma en cuenta el monto de la cuota detalle, en casos de que sea semanal
      $estado_credito = 'Vigente';
      $class_td = 'class="success"';

      $cliente_documento = $value['tb_cliente_doc'];
      $cliente_nombres = $value['tb_cliente_nom'];
      $representante_dni = '';
      $representante_nombres = '';
      $vencimiento = '';

      if ($value['tb_moneda_id'] == 1) {
        $TOTAL_CUOTAS_SOLES += $monto_cuota;
        $cuotas_soles_asiveh += $monto_cuota;
      }
      if ($value['tb_moneda_id'] == 2) {
        $moneda_simbolo = 'US$';
        $TOTAL_CUOTAS_DOLARES += $monto_cuota;
        $cuotas_dolares_asiveh += $monto_cuota;
      }
      if (!empty($value['tb_cliente_emprs'])) {
        $cliente_documento = $value['tb_cliente_empruc'];
        $cliente_nombres = $value['tb_cliente_emprs'];
        $representante_dni = $value['tb_cliente_doc'];
        $representante_nombres = $value['tb_cliente_nom'];
      }
      if (intval($value['tb_cuotadetalle_est']) == 3) { //todas las cuotas en pago parcial
        $tipo_cuota_pago = 1; // 1 es para pago de cuota
        $cuota_id = intval($value['tb_cuota_id']);

        if (intval($value['tb_cuotadetalle_id']) > 0) {
          $tipo_cuota_pago = 2; //2 para pago de cuota detalle
          $cuota_id = intval($value['tb_cuotadetalle_id']);
        }

        $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
        if ($result2['estado'] == 1) {
          $pago_parcial = $result2['data']['importe_total'];
          if ($value['tb_moneda_id'] == 1){
            $TOTAL_COBRADO_SOLES += $pago_parcial;
            $cobrado_soles_asiveh += $pago_parcial;
          }
          if ($value['tb_moneda_id'] == 2){
            $TOTAL_COBRADO_DOLARES += $pago_parcial;
            $cobrado_dolares_asiveh += $pago_parcial;
          }
        }
        $result2 = NULL;
      }

      $saldo_por_pagar = $monto_cuota - $pago_parcial;

      $opcionesgc_nom = $value['tb_opcionesgc_nom'];
      $resultado_nom = $value['resultado_nom'];
      $comentario_des = $value['comentario_des'];
      $fecha_pdp = $value['opciongc_fechapdp']; // esta fecha es la promesa de pago, mediante esta fecha evaluamos si cumplió o no
      $ultimopago_fecha = $value['tb_credito_fecfac']; //el crédito no tiene pago alguno
      if ($value['tb_opcionesgc_id'] == 1 || $value['tb_opcionesgc_id'] == 4)
        $opcionesgc_nom = $value['tb_opcionesgc_nom'] . ' <b>' . mostrar_fecha($fecha_pdp) . '</b>';

      //! consulta critica para revisar
      $result2 = $oIngreso->ultima_fecha_que_pago_cliente($value['tb_credito_id'], $value['credito_general']);
      if ($result2['estado'] == 1) {
        $ultimopago_fecha = $result2['data']['ultima_fecha'];
        $monto_ultimo_pago = $result2['data']['tb_cuotapago_mon'];
      }
      $result2 = NULL;
      //! FIN CONSULTA CRITICA PAREA REVISAR

      //definimos el estado que tendrá la columna resultado que depende de la opción asignada
      if ($value['tb_opcionesgc_id'] == 1) { //la columna de RESULTADO netamente depende de los compromisos de pago, cualquiero otra opción esta debe estar varcía
        //fechas que necesitamos: fecha hoy, fecha de ultimo pago, fecha compromiso de pago
        $fecha_hoy_menos1 = date("Y-m-d", strtotime($fecha_hoy . "- 1 days")); //obtenemos fecha de ayer para comparar si hubo un pago ayer con el compromiso
        if ($ultimopago_fecha == $fecha_pdp) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'SI CUMPLIO', 'STR');
          $resultado_nom = 'SI CUMPLIÓ';
        }
        if (strtotime($fecha_pdp) > strtotime($fecha_hoy_menos1)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'ESPERANDO PDP', 'STR');
          $resultado_nom = 'ESPERANDO PDP';
        }
        if (strtotime($fecha_pdp) < strtotime($fecha_hoy) && strtotime($ultimopago_fecha) < strtotime($fecha_pdp)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'NO CUMPLIO', 'STR');
          $resultado_nom = 'NO CUMPLIÓ';
        }
        if (strtotime($ultimopago_fecha) > strtotime($fecha_pdp)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'ASIGNAR NUEVO PDP', 'STR');
          $resultado_nom = 'ASIGNAR NUEVO PDP';
        }
      } else {
        $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', '', 'STR');
      }

      if (strtotime($fecha_hoy) == strtotime($value['tb_cuotadetalle_fec'])) { //cuota vence hoy
        $vencimiento = '<b style="color: blue;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Vence hoy)</b>';
      }
      if (strtotime($fecha_hoy) < strtotime($value['tb_cuotadetalle_fec'])) { //cuota aun no vence
        $resta_fecha = restaFechas($fecha_hoy, $value['tb_cuotadetalle_fec']);
        $vencimiento = '<b style="color: green;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Vence en ' . $resta_fecha . ' días)</b>';
      }
      if (strtotime($fecha_hoy) > strtotime($value['tb_cuotadetalle_fec'])) { //todo codigo aquí dentro es para cuiando la cuota está vencida
        $resta_fecha = restaFechas($value['tb_cuotadetalle_fec'], $fecha_hoy);
        $vencimiento = '<b style="color: red;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Venció hace ' . $resta_fecha . ' días)</b>';
      }

      $data_asiveh .= '
            <tr>
              <td>' . $numero_orden . '</td>
              <td>' . $value['tb_credito_id'] . '</td>
              <td>' . $cliente_nombres . '</td>
              <td>' . $representante_nombres . '</td>
              <td>' . $value['tb_cliente_cel'] . '</td>
              <td><b>' . $credito_general . '</b></td>
              <td>' . $credito_subtipo1 . '</td>
              <td>' . $cuo_tipo . '</td>
              <td>' . $moneda_simbolo . '</td>
              <td>' . $monto_cuota . '</td>
              <td>' . $pago_parcial . '</td>
              <td>' . $saldo_por_pagar . '</td>
              <td>' . $vencimiento . '</td>
              <td>' . $value['tb_cuota_num'] . ' / ' . $value['num_cuotas_totales'] . '</td>
              <td>' . $ultimopago_fecha . '</td>
              <td id="td_opciones_' . $value['tb_cuotadetalle_id'] . '">' . $opcionesgc_nom . '</td>
              <td>' . $resultado_nom . '</td>
              <td id="td_comentario_' . $value['tb_cuotadetalle_id'] . '">' . $comentario_des . '</td>
              <td>
                <a class="btn btn-info btn-xs" title="Gestion" onclick="opcionesgc_asignar_form(' . $value['credito_general'] . ', ' . $value['tb_credito_id'] . ', ' . $value['tb_cuotadetalle_id'] . ',\'' . $credito_general . '\', \'' . $credito_subtipo1 . '\')"><i class="fa fa-phone"></i></a>
                <a class="btn bg-purple btn-xs" title="Historial" onclick="opcionesgc_timeline(\'' . $credito_general . '\', ' . $value['tb_credito_id'] . ')"><i class="fa fa-list-ul"></i></a>
              </td>
            </tr>
          ';
    }

    $por_cobrar_soles = $cuotas_soles_asiveh - $cobrado_soles_asiveh;
    $por_cobrar_dolares = $cuotas_dolares_asiveh - $cobrado_dolares_asiveh;
    $data_asiveh .= '
        <tr>
          <td colspan="9" align="center">Total Cuotas S/.  <b>'.$cuotas_soles_asiveh.'</b> | Total Cobrado S/. <b>'.$cobrado_soles_asiveh.'</b> | Total Por Cobrar S/. <b>'.$por_cobrar_soles.'</b></td>
          <td colspan="10" align="center">Total Cuotas US$  <b>'.$cuotas_dolares_asiveh.'</b> | Total Cobrado US$  <b>'.$cobrado_dolares_asiveh.'</b> | Total Por Cobrar US$  <b>'.$por_cobrar_dolares.'</b></td>
        </tr>
      ';
  }
$result = NULL;
//? ------------------------------------------ FIN ASIVEH ASIVEH -----------------------------------

$data_garveh = '';
$cuotas_soles_garveh = 0;
$cuotas_dolares_garveh = 0;
$cobrado_soles_garveh = 0;
$cobrado_dolares_garveh = 0;
//* ------------------------------------------ LISTA DE CUOTAS SEGÚN RANGO DE $GARVEHHHHHHHH -----------------------------------
$result = $oCobranzatodos->cobranzatodos_cuotas_facturadas_tipocredito($cuota_fec1, $cuota_fec2, $fecha_hoy, $estados_creditos, $cliente_id, $cuota_condicion, 3);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $cuo_tipo = 'CUOTA FIJA';
      if ($value['tb_cuotatipo_id'] == 3) {
        $cuo_tipo = 'CUOTA LIBRE';
      }

      $credito_general = 'SIN GENERAL';
      $credito_simbolo = 'REVISAR';
      if (intval($value['credito_general']) == 1) $credito_general = 'Crédito MENOR';
      if (intval($value['credito_general']) == 2) {
        $credito_general = 'Crédito ASIVEH';
        $credito_simbolo = 'ASIVEH';
      }
      if (intval($value['credito_general']) == 3) {
        $credito_general = 'Crédito GARVEH';
        $credito_simbolo = 'GARVEH';
      }
      if (intval($value['credito_general']) == 4) {
        $credito_general = 'Crédito HIPOTECARIO';
        $credito_simbolo = 'HIPOTECA';
      }

      $credito_subtipo1 = 'SIN TIPO CREDITO';
      if ($value['credito_tipo'] == 1) $credito_subtipo1 = $credito_simbolo;
      if ($value['credito_tipo'] == 2) $credito_subtipo1 = $credito_simbolo . " ADENDA";
      if ($value['credito_tipo'] == 3) $credito_subtipo1 = $credito_simbolo . " ACUERDO PAGO";
      if ($value['credito_tipo'] == 4) $credito_subtipo1 = "GARVEH MOBILIARIA";

      if (intval($value['credito_general']) == 4) { // PARA CREDITO HIPOTECARIO CAMBIA SUS SUBTIPOS
        if ($value['credito_tipo'] == 1) $credito_subtipo1 = "GARANTIA INMUEBLE C/V";
        if ($value['credito_tipo'] == 2) $credito_subtipo1 = "HIPOTECA ADENDA";
        if ($value['credito_tipo'] == 3) $credito_subtipo1 = "HIPOTECA ACUERDO PAGO";
        if ($value['credito_tipo'] == 4) $credito_subtipo1 = "HIPOTECARIA";
      }

      $credito_subtipo2 = 'SIN SUBTIPO';
      if ($value['credito_subtipo'] == 1) $credito_subtipo2 = " CREDITO REGULAR";
      if ($value['credito_subtipo'] == 2) $credito_subtipo2 = " CREDITO ESPECÍFICO";
      if ($value['credito_subtipo'] == 3) $credito_subtipo2 = " REPROGRAMADO";
      if ($value['credito_subtipo'] == 4) $credito_subtipo2 = " CUOTA BALON";
      if ($value['credito_subtipo'] == 5) $credito_subtipo2 = " REFINANCIADO AMORTIZADO";
      if ($value['credito_subtipo'] == 6) $credito_subtipo2 = " REFINANCIADO";

      $estado_cuota = 'SIN ESTADO CUOTA';
      if (intval($value['tb_cuota_est']) == 1) $estado_cuota = 'Pendiente';
      if (intval($value['tb_cuota_est']) == 2) $estado_cuota = 'Pagada';
      if (intval($value['tb_cuota_est']) == 3) $estado_cuota = 'Pago Parcial';

      // $capital_desembolsado = $value['tb_credito_preaco'];
      // $capital_restante = $value['tb_cuota_cap']; //si la cuota está impaga o pago parcial debe el capital hasta esa fecha
      // $capital_restante_soles = $capital_restante;
      // $capital_desembolsado_soles = $capital_desembolsado;
      $pago_parcial = 0;
      $numero_orden++;
      $moneda_simbolo = 'S/.';
      $monto_cuota = $value['cuota_real']; //que toma en cuenta el monto de la cuota detalle, en casos de que sea semanal
      $estado_credito = 'Vigente';
      $class_td = 'class="success"';

      $cliente_documento = $value['tb_cliente_doc'];
      $cliente_nombres = $value['tb_cliente_nom'];
      $representante_dni = '';
      $representante_nombres = '';
      $vencimiento = '';

      if ($value['tb_moneda_id'] == 1) {
        $TOTAL_CUOTAS_SOLES += $monto_cuota;
        $cuotas_soles_garveh += $monto_cuota;
      }
      if ($value['tb_moneda_id'] == 2) {
        $moneda_simbolo = 'US$';
        $TOTAL_CUOTAS_DOLARES += $monto_cuota;
        $cuotas_dolares_garveh += $monto_cuota;
      }
      if (!empty($value['tb_cliente_emprs'])) {
        $cliente_documento = $value['tb_cliente_empruc'];
        $cliente_nombres = $value['tb_cliente_emprs'];
        $representante_dni = $value['tb_cliente_doc'];
        $representante_nombres = $value['tb_cliente_nom'];
      }
      if (intval($value['tb_cuotadetalle_est']) == 3) { //todas las cuotas en pago parcial
        $tipo_cuota_pago = 1; // 1 es para pago de cuota
        $cuota_id = intval($value['tb_cuota_id']);

        if (intval($value['tb_cuotadetalle_id']) > 0) {
          $tipo_cuota_pago = 2; //2 para pago de cuota detalle
          $cuota_id = intval($value['tb_cuotadetalle_id']);
        }

        $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
        if ($result2['estado'] == 1) {
          $pago_parcial = $result2['data']['importe_total'];
          if ($value['tb_moneda_id'] == 1){
            $TOTAL_COBRADO_SOLES += $pago_parcial;
            $cobrado_soles_garveh += $pago_parcial;
          }
          if ($value['tb_moneda_id'] == 2){
            $TOTAL_COBRADO_DOLARES += $pago_parcial;
            $cobrado_dolares_garveh += $pago_parcial;
          }
        }
        $result2 = NULL;
      }

      $saldo_por_pagar = $monto_cuota - $pago_parcial;

      $opcionesgc_nom = $value['tb_opcionesgc_nom'];
      $resultado_nom = $value['resultado_nom'];
      $comentario_des = $value['comentario_des'];
      $fecha_pdp = $value['opciongc_fechapdp']; // esta fecha es la promesa de pago, mediante esta fecha evaluamos si cumplió o no
      $ultimopago_fecha = $value['tb_credito_fecfac']; //el crédito no tiene pago alguno
      if ($value['tb_opcionesgc_id'] == 1 || $value['tb_opcionesgc_id'] == 4)
        $opcionesgc_nom = $value['tb_opcionesgc_nom'] . ' <b>' . mostrar_fecha($fecha_pdp) . '</b>';

      //! consulta critica para revisar
      $result2 = $oIngreso->ultima_fecha_que_pago_cliente($value['tb_credito_id'], $value['credito_general']);
      if ($result2['estado'] == 1) {
        $ultimopago_fecha = $result2['data']['ultima_fecha'];
        $monto_ultimo_pago = $result2['data']['tb_cuotapago_mon'];
      }
      $result2 = NULL;
      //! FIN CONSULTA CRITICA PAREA REVISAR

      //definimos el estado que tendrá la columna resultado que depende de la opción asignada
      if ($value['tb_opcionesgc_id'] == 1) { //la columna de RESULTADO netamente depende de los compromisos de pago, cualquiero otra opción esta debe estar varcía
        //fechas que necesitamos: fecha hoy, fecha de ultimo pago, fecha compromiso de pago
        $fecha_hoy_menos1 = date("Y-m-d", strtotime($fecha_hoy . "- 1 days")); //obtenemos fecha de ayer para comparar si hubo un pago ayer con el compromiso
        if ($ultimopago_fecha == $fecha_pdp) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'SI CUMPLIO', 'STR');
          $resultado_nom = 'SI CUMPLIÓ';
        }
        if (strtotime($fecha_pdp) > strtotime($fecha_hoy_menos1)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'ESPERANDO PDP', 'STR');
          $resultado_nom = 'ESPERANDO PDP';
        }
        if (strtotime($fecha_pdp) < strtotime($fecha_hoy) && strtotime($ultimopago_fecha) < strtotime($fecha_pdp)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'NO CUMPLIO', 'STR');
          $resultado_nom = 'NO CUMPLIÓ';
        }
        if (strtotime($ultimopago_fecha) > strtotime($fecha_pdp)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'ASIGNAR NUEVO PDP', 'STR');
          $resultado_nom = 'ASIGNAR NUEVO PDP';
        }
      } else {
        $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', '', 'STR');
      }

      if (strtotime($fecha_hoy) == strtotime($value['tb_cuotadetalle_fec'])) { //cuota vence hoy
        $vencimiento = '<b style="color: blue;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Vence hoy)</b>';
      }
      if (strtotime($fecha_hoy) < strtotime($value['tb_cuotadetalle_fec'])) { //cuota aun no vence
        $resta_fecha = restaFechas($fecha_hoy, $value['tb_cuotadetalle_fec']);
        $vencimiento = '<b style="color: green;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Vence en ' . $resta_fecha . ' días)</b>';
      }
      if (strtotime($fecha_hoy) > strtotime($value['tb_cuotadetalle_fec'])) { //todo codigo aquí dentro es para cuiando la cuota está vencida
        $resta_fecha = restaFechas($value['tb_cuotadetalle_fec'], $fecha_hoy);
        $vencimiento = '<b style="color: red;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Venció hace ' . $resta_fecha . ' días)</b>';
      }

      $data_garveh .= '
            <tr>
              <td>' . $numero_orden . '</td>
              <td>' . $value['tb_credito_id'] . '</td>
              <td>' . $cliente_nombres . '</td>
              <td>' . $representante_nombres . '</td>
              <td>' . $value['tb_cliente_cel'] . '</td>
              <td><b>' . $credito_general . '</b></td>
              <td>' . $credito_subtipo1 . '</td>
              <td>' . $cuo_tipo . '</td>
              <td>' . $moneda_simbolo . '</td>
              <td>' . $monto_cuota . '</td>
              <td>' . $pago_parcial . '</td>
              <td>' . $saldo_por_pagar . '</td>
              <td>' . $vencimiento . '</td>
              <td>' . $value['tb_cuota_num'] . ' / ' . $value['num_cuotas_totales'] . '</td>
              <td>' . $ultimopago_fecha . '</td>
              <td id="td_opciones_' . $value['tb_cuotadetalle_id'] . '">' . $opcionesgc_nom . '</td>
              <td>' . $resultado_nom . '</td>
              <td id="td_comentario_' . $value['tb_cuotadetalle_id'] . '">' . $comentario_des . '</td>
              <td>
                <a class="btn btn-info btn-xs" title="Gestion" onclick="opcionesgc_asignar_form(' . $value['credito_general'] . ', ' . $value['tb_credito_id'] . ', ' . $value['tb_cuotadetalle_id'] . ',\'' . $credito_general . '\', \'' . $credito_subtipo1 . '\')"><i class="fa fa-phone"></i></a>
                <a class="btn bg-purple btn-xs" title="Historial" onclick="opcionesgc_timeline(\'' . $credito_general . '\', ' . $value['tb_credito_id'] . ')"><i class="fa fa-list-ul"></i></a>
              </td>
            </tr>
          ';
    }

    $por_cobrar_soles = $cuotas_soles_garveh - $cobrado_soles_garveh;
    $por_cobrar_dolares = $cuotas_dolares_garveh - $cobrado_dolares_garveh;
    $data_garveh .= '
        <tr>
          <td colspan="9" align="center">Total Cuotas S/.  <b>'.$cuotas_soles_garveh.'</b> | Total Cobrado S/. <b>'.$cobrado_soles_garveh.'</b> | Total Por Cobrar S/. <b>'.$por_cobrar_soles.'</b></td>
          <td colspan="10" align="center">Total Cuotas US$  <b>'.$cuotas_dolares_garveh.'</b> | Total Cobrado US$  <b>'.$cobrado_dolares_garveh.'</b> | Total Por Cobrar US$  <b>'.$por_cobrar_dolares.'</b></td>
        </tr>
      ';
  }
$result = NULL;
//* ------------------------------------------ FIN GARVEHHHHHH -----------------------------------

$data_hipo = '';
$cuotas_soles_hipo = 0;
$cuotas_dolares_hipo = 0;
$cobrado_soles_hipo = 0;
$cobrado_dolares_hipo = 0;
//! ------------------------------------------ LISTA DE CUOTAS SEGÚN RANGO DE HIPOOOOOOOOOOOOO -----------------------------------
$result = $oCobranzatodos->cobranzatodos_cuotas_facturadas_tipocredito($cuota_fec1, $cuota_fec2, $fecha_hoy, $estados_creditos, $cliente_id, $cuota_condicion, 4);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $cuo_tipo = 'CUOTA FIJA';
      if ($value['tb_cuotatipo_id'] == 3) {
        $cuo_tipo = 'CUOTA LIBRE';
      }

      $credito_general = 'SIN GENERAL';
      $credito_simbolo = 'REVISAR';
      if (intval($value['credito_general']) == 1) $credito_general = 'Crédito MENOR';
      if (intval($value['credito_general']) == 2) {
        $credito_general = 'Crédito ASIVEH';
        $credito_simbolo = 'ASIVEH';
      }
      if (intval($value['credito_general']) == 3) {
        $credito_general = 'Crédito GARVEH';
        $credito_simbolo = 'GARVEH';
      }
      if (intval($value['credito_general']) == 4) {
        $credito_general = 'Crédito HIPOTECARIO';
        $credito_simbolo = 'HIPOTECA';
      }

      $credito_subtipo1 = 'SIN TIPO CREDITO';
      if ($value['credito_tipo'] == 1) $credito_subtipo1 = $credito_simbolo;
      if ($value['credito_tipo'] == 2) $credito_subtipo1 = $credito_simbolo . " ADENDA";
      if ($value['credito_tipo'] == 3) $credito_subtipo1 = $credito_simbolo . " ACUERDO PAGO";
      if ($value['credito_tipo'] == 4) $credito_subtipo1 = "GARVEH MOBILIARIA";

      if (intval($value['credito_general']) == 4) { // PARA CREDITO HIPOTECARIO CAMBIA SUS SUBTIPOS
        if ($value['credito_tipo'] == 1) $credito_subtipo1 = "GARANTIA INMUEBLE C/V";
        if ($value['credito_tipo'] == 2) $credito_subtipo1 = "HIPOTECA ADENDA";
        if ($value['credito_tipo'] == 3) $credito_subtipo1 = "HIPOTECA ACUERDO PAGO";
        if ($value['credito_tipo'] == 4) $credito_subtipo1 = "HIPOTECARIA";
      }

      $credito_subtipo2 = 'SIN SUBTIPO';
      if ($value['credito_subtipo'] == 1) $credito_subtipo2 = " CREDITO REGULAR";
      if ($value['credito_subtipo'] == 2) $credito_subtipo2 = " CREDITO ESPECÍFICO";
      if ($value['credito_subtipo'] == 3) $credito_subtipo2 = " REPROGRAMADO";
      if ($value['credito_subtipo'] == 4) $credito_subtipo2 = " CUOTA BALON";
      if ($value['credito_subtipo'] == 5) $credito_subtipo2 = " REFINANCIADO AMORTIZADO";
      if ($value['credito_subtipo'] == 6) $credito_subtipo2 = " REFINANCIADO";

      $estado_cuota = 'SIN ESTADO CUOTA';
      if (intval($value['tb_cuota_est']) == 1) $estado_cuota = 'Pendiente';
      if (intval($value['tb_cuota_est']) == 2) $estado_cuota = 'Pagada';
      if (intval($value['tb_cuota_est']) == 3) $estado_cuota = 'Pago Parcial';

      // $capital_desembolsado = $value['tb_credito_preaco'];
      // $capital_restante = $value['tb_cuota_cap']; //si la cuota está impaga o pago parcial debe el capital hasta esa fecha
      // $capital_restante_soles = $capital_restante;
      // $capital_desembolsado_soles = $capital_desembolsado;
      $pago_parcial = 0;
      $numero_orden++;
      $moneda_simbolo = 'S/.';
      $monto_cuota = $value['cuota_real']; //que toma en cuenta el monto de la cuota detalle, en casos de que sea semanal
      $estado_credito = 'Vigente';
      $class_td = 'class="success"';

      $cliente_documento = $value['tb_cliente_doc'];
      $cliente_nombres = $value['tb_cliente_nom'];
      $representante_dni = '';
      $representante_nombres = '';
      $vencimiento = '';

      if ($value['tb_moneda_id'] == 1) {
        $TOTAL_CUOTAS_SOLES += $monto_cuota;
        $cuotas_soles_hipo += $monto_cuota;
      }
      if ($value['tb_moneda_id'] == 2) {
        $moneda_simbolo = 'US$';
        $TOTAL_CUOTAS_DOLARES += $monto_cuota;
        $cuotas_dolares_hipo += $monto_cuota;
      }
      if (!empty($value['tb_cliente_emprs'])) {
        $cliente_documento = $value['tb_cliente_empruc'];
        $cliente_nombres = $value['tb_cliente_emprs'];
        $representante_dni = $value['tb_cliente_doc'];
        $representante_nombres = $value['tb_cliente_nom'];
      }
      if (intval($value['tb_cuotadetalle_est']) == 3) { //todas las cuotas en pago parcial
        $tipo_cuota_pago = 1; // 1 es para pago de cuota
        $cuota_id = intval($value['tb_cuota_id']);

        if (intval($value['tb_cuotadetalle_id']) > 0) {
          $tipo_cuota_pago = 2; //2 para pago de cuota detalle
          $cuota_id = intval($value['tb_cuotadetalle_id']);
        }

        $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
        if ($result2['estado'] == 1) {
          $pago_parcial = $result2['data']['importe_total'];
          if ($value['tb_moneda_id'] == 1){
            $TOTAL_COBRADO_SOLES += $pago_parcial;
            $cobrado_soles_hipo += $pago_parcial;
          }
          if ($value['tb_moneda_id'] == 2){
            $TOTAL_COBRADO_DOLARES += $pago_parcial;
            $cobrado_dolares_hipo += $pago_parcial;
          }
        }
        $result2 = NULL;
      }

      $saldo_por_pagar = $monto_cuota - $pago_parcial;

      $opcionesgc_nom = $value['tb_opcionesgc_nom'];
      $resultado_nom = $value['resultado_nom'];
      $comentario_des = $value['comentario_des'];
      $fecha_pdp = $value['opciongc_fechapdp']; // esta fecha es la promesa de pago, mediante esta fecha evaluamos si cumplió o no
      $ultimopago_fecha = $value['tb_credito_fecfac']; //el crédito no tiene pago alguno
      if ($value['tb_opcionesgc_id'] == 1 || $value['tb_opcionesgc_id'] == 4)
        $opcionesgc_nom = $value['tb_opcionesgc_nom'] . ' <b>' . mostrar_fecha($fecha_pdp) . '</b>';

      //! consulta critica para revisar
      $result2 = $oIngreso->ultima_fecha_que_pago_cliente($value['tb_credito_id'], $value['credito_general']);
      if ($result2['estado'] == 1) {
        $ultimopago_fecha = $result2['data']['ultima_fecha'];
        $monto_ultimo_pago = $result2['data']['tb_cuotapago_mon'];
      }
      $result2 = NULL;
      //! FIN CONSULTA CRITICA PAREA REVISAR

      //definimos el estado que tendrá la columna resultado que depende de la opción asignada
      if ($value['tb_opcionesgc_id'] == 1) { //la columna de RESULTADO netamente depende de los compromisos de pago, cualquiero otra opción esta debe estar varcía
        //fechas que necesitamos: fecha hoy, fecha de ultimo pago, fecha compromiso de pago
        $fecha_hoy_menos1 = date("Y-m-d", strtotime($fecha_hoy . "- 1 days")); //obtenemos fecha de ayer para comparar si hubo un pago ayer con el compromiso
        if ($ultimopago_fecha == $fecha_pdp) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'SI CUMPLIO', 'STR');
          $resultado_nom = 'SI CUMPLIÓ';
        }
        if (strtotime($fecha_pdp) > strtotime($fecha_hoy_menos1)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'ESPERANDO PDP', 'STR');
          $resultado_nom = 'ESPERANDO PDP';
        }
        if (strtotime($fecha_pdp) < strtotime($fecha_hoy) && strtotime($ultimopago_fecha) < strtotime($fecha_pdp)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'NO CUMPLIO', 'STR');
          $resultado_nom = 'NO CUMPLIÓ';
        }
        if (strtotime($ultimopago_fecha) > strtotime($fecha_pdp)) {
          $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', 'ASIGNAR NUEVO PDP', 'STR');
          $resultado_nom = 'ASIGNAR NUEVO PDP';
        }
      } else {
        $oAsignar->modificar_campo_asignar_opciongc($value['tb_credito_id'], $value['tb_cuotadetalle_id'], 'resultado_nom', '', 'STR');
      }

      if (strtotime($fecha_hoy) == strtotime($value['tb_cuotadetalle_fec'])) { //cuota vence hoy
        $vencimiento = '<b style="color: blue;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Vence hoy)</b>';
      }
      if (strtotime($fecha_hoy) < strtotime($value['tb_cuotadetalle_fec'])) { //cuota aun no vence
        $resta_fecha = restaFechas($fecha_hoy, $value['tb_cuotadetalle_fec']);
        $vencimiento = '<b style="color: green;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Vence en ' . $resta_fecha . ' días)</b>';
      }
      if (strtotime($fecha_hoy) > strtotime($value['tb_cuotadetalle_fec'])) { //todo codigo aquí dentro es para cuiando la cuota está vencida
        $resta_fecha = restaFechas($value['tb_cuotadetalle_fec'], $fecha_hoy);
        $vencimiento = '<b style="color: red;">' . mostrar_fecha($value['tb_cuotadetalle_fec']) . ' (Venció hace ' . $resta_fecha . ' días)</b>';
      }

      $data_hipo .= '
            <tr>
              <td>' . $numero_orden . '</td>
              <td>' . $value['tb_credito_id'] . '</td>
              <td>' . $cliente_nombres . '</td>
              <td>' . $representante_nombres . '</td>
              <td>' . $value['tb_cliente_cel'] . '</td>
              <td><b>' . $credito_general . '</b></td>
              <td>' . $credito_subtipo1 . '</td>
              <td>' . $cuo_tipo . '</td>
              <td>' . $moneda_simbolo . '</td>
              <td>' . $monto_cuota . '</td>
              <td>' . $pago_parcial . '</td>
              <td>' . $saldo_por_pagar . '</td>
              <td>' . $vencimiento . '</td>
              <td>' . $value['tb_cuota_num'] . ' / ' . $value['num_cuotas_totales'] . '</td>
              <td>' . $ultimopago_fecha . '</td>
              <td id="td_opciones_' . $value['tb_cuotadetalle_id'] . '">' . $opcionesgc_nom . '</td>
              <td>' . $resultado_nom . '</td>
              <td id="td_comentario_' . $value['tb_cuotadetalle_id'] . '">' . $comentario_des . '</td>
              <td>
                <a class="btn btn-info btn-xs" title="Gestion" onclick="opcionesgc_asignar_form(' . $value['credito_general'] . ', ' . $value['tb_credito_id'] . ', ' . $value['tb_cuotadetalle_id'] . ',\'' . $credito_general . '\', \'' . $credito_subtipo1 . '\')"><i class="fa fa-phone"></i></a>
                <a class="btn bg-purple btn-xs" title="Historial" onclick="opcionesgc_timeline(\'' . $credito_general . '\', ' . $value['tb_credito_id'] . ')"><i class="fa fa-list-ul"></i></a>
              </td>
            </tr>
          ';
    }

    $por_cobrar_soles = $cuotas_soles_hipo - $cobrado_soles_hipo;
    $por_cobrar_dolares = $cuotas_dolares_hipo - $cobrado_dolares_hipo;
    $data_hipo .= '
        <tr>
          <td colspan="9" align="center">Total Cuotas S/.  <b>'.$cuotas_soles_hipo.'</b> | Total Cobrado S/. <b>'.$cobrado_soles_hipo.'</b> | Total Por Cobrar S/. <b>'.$por_cobrar_soles.'</b></td>
          <td colspan="10" align="center">Total Cuotas US$  <b>'.$cuotas_dolares_hipo.'</b> | Total Cobrado US$  <b>'.$cobrado_dolares_hipo.'</b> | Total Por Cobrar US$  <b>'.$por_cobrar_dolares.'</b></td>
        </tr>
      ';
  }
$result = NULL;
//! ------------------------------------------ FIN HIPOOOOOOOOOOOOOOOOO -----------------------------------

$SALDO_COBRAR_SOLES = $TOTAL_CUOTAS_SOLES - $TOTAL_COBRADO_SOLES;
$SALDO_COBRAR_DOLARES = $TOTAL_CUOTAS_DOLARES - $TOTAL_COBRADO_DOLARES;

$resumen = '
    <tr>
      <td>'.mostrar_moneda($TOTAL_CUOTAS_SOLES).'</td>
      <td>'.mostrar_moneda($TOTAL_CUOTAS_DOLARES).'</td>
      <td>'.mostrar_moneda($TOTAL_COBRADO_SOLES).'</td>
      <td>'.mostrar_moneda($TOTAL_COBRADO_DOLARES).'</td>
      <td>'.mostrar_moneda($SALDO_COBRAR_SOLES).'</td>
      <td>'.mostrar_moneda($SALDO_COBRAR_DOLARES).'</td>
    </tr>
  ';
?>

<div class="box box-success shadow">
  <div class="box-header with-border">
    <h3 class="box-title">CUOTAS CRÉDITO MENOR</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>N*</th>
            <th>ID CREDITO</th>
            <th>NOMBRES CLIENTE</th>
            <!--th class="no-sort">DNI / RUC</th>
										<th class="no-sort">DNI REPRESENT.</th -->
            <th>REPRESENTANTE</th>
            <th class="no-sort">TELEFONO</th>
            <th>CREDITO GENERAL</th>
            <th>SUBTIPO CREDITO</th>
            <th>TIPO CUOTA</th>
            <th>MONEDA</th>
            <th>MONTO CUOTA</th>
            <th>PAGO PARCIAL</th>
            <th>SALDO POR PAGAR</th>
            <th>VENCIMIENTO</th>
            <th class="no-sort">N° CUOTA</th>
            <th>FECHA ULT. PAGO</th>
            <th>OPCIONES</th>
            <th>RESULTADO</th>
            <th class="no-sort">COMENTARIOS</th>
            <th class="no-sort">ACTIVIDADES</th>
          </tr>
        </thead>
        <tbody id="tbl_cobranzatodos_lista">
          <?php echo $data_cmenor;?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="box box-info shadow">
  <div class="box-header with-border">
    <h3 class="box-title">CUOTAS CRÉDITO ASISTENCIA VEHICULAR</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>N*</th>
            <th>ID CREDITO</th>
            <th>NOMBRES CLIENTE</th>
            <!--th class="no-sort">DNI / RUC</th>
										<th class="no-sort">DNI REPRESENT.</th -->
            <th>REPRESENTANTE</th>
            <th class="no-sort">TELEFONO</th>
            <th>CREDITO GENERAL</th>
            <th>SUBTIPO CREDITO</th>
            <th>TIPO CUOTA</th>
            <th>MONEDA</th>
            <th>MONTO CUOTA</th>
            <th>PAGO PARCIAL</th>
            <th>SALDO POR PAGAR</th>
            <th>VENCIMIENTO</th>
            <th class="no-sort">N° CUOTA</th>
            <th>FECHA ULT. PAGO</th>
            <th>OPCIONES</th>
            <th>RESULTADO</th>
            <th class="no-sort">COMENTARIOS</th>
            <th class="no-sort">ACTIVIDADES</th>
          </tr>
        </thead>
        <tbody id="tbl_cobranzatodos_lista">
          <?php echo $data_asiveh;?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="box box-warning shadow">
  <div class="box-header with-border">
    <h3 class="box-title">CUOTAS CRÉDITO GARANTÍA VEHICULAR</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>N*</th>
            <th>ID CREDITO</th>
            <th>NOMBRES CLIENTE</th>
            <!--th class="no-sort">DNI / RUC</th>
										<th class="no-sort">DNI REPRESENT.</th -->
            <th>REPRESENTANTE</th>
            <th class="no-sort">TELEFONO</th>
            <th>CREDITO GENERAL</th>
            <th>SUBTIPO CREDITO</th>
            <th>TIPO CUOTA</th>
            <th>MONEDA</th>
            <th>MONTO CUOTA</th>
            <th>PAGO PARCIAL</th>
            <th>SALDO POR PAGAR</th>
            <th>VENCIMIENTO</th>
            <th class="no-sort">N° CUOTA</th>
            <th>FECHA ULT. PAGO</th>
            <th>OPCIONES</th>
            <th>RESULTADO</th>
            <th class="no-sort">COMENTARIOS</th>
            <th class="no-sort">ACTIVIDADES</th>
          </tr>
        </thead>
        <tbody id="tbl_cobranzatodos_lista">
          <?php echo $data_garveh;?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="box box-primary shadow">
  <div class="box-header with-border">
    <h3 class="box-title">CUOTAS CRÉDITO HIPOTECARIOS</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>N*</th>
            <th>ID CREDITO</th>
            <th>NOMBRES CLIENTE</th>
            <!--th class="no-sort">DNI / RUC</th>
										<th class="no-sort">DNI REPRESENT.</th -->
            <th>REPRESENTANTE</th>
            <th class="no-sort">TELEFONO</th>
            <th>CREDITO GENERAL</th>
            <th>SUBTIPO CREDITO</th>
            <th>TIPO CUOTA</th>
            <th>MONEDA</th>
            <th>MONTO CUOTA</th>
            <th>PAGO PARCIAL</th>
            <th>SALDO POR PAGAR</th>
            <th>VENCIMIENTO</th>
            <th class="no-sort">N° CUOTA</th>
            <th>FECHA ULT. PAGO</th>
            <th>OPCIONES</th>
            <th>RESULTADO</th>
            <th class="no-sort">COMENTARIOS</th>
            <th class="no-sort">ACTIVIDADES</th>
          </tr>
        </thead>
        <tbody id="tbl_cobranzatodos_lista">
          <?php echo $data_hipo;?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="box box-info shadow">
  <div class="box-header with-border">
    <h3 class="box-title">RESUMEN DE CUOTAS POR COBRAR Y MONTOS COBRADOS</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table class="table no-margin" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>TOTAL CUOTAS POR COBRAR EN S/.</th>
            <th>TOTAL CUOTAS POR COBRAR EN US$</th>
            <th>TOTAL PAGOS PARCIALES EN S/.</th>
            <th>TOTAL PAGOS PARCIALES EN US$</th>
            <th>SALDO POR COBRAR EN S/.</th>
            <th>SALDO POR COBRAR EN US$</th>
          </tr>
        </thead>
        <tbody id="tbl_cobranzatodos_resumen">
          <?php echo $resumen;?>
        </tbody>
      </table>
    </div>
  </div>
</div>