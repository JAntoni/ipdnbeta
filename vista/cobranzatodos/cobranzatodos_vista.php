<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $menu_tit; ?>
      <small><?php echo $menu_des; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
      <li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
    </ol>
  </section>
  <style type="text/css">
    div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
      font-weight: bold;
    }

    div.dataTables_filter label {
      width: 80%;
    }
  </style>
  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header">
        <?php require_once('cobranzatodos_filtro.php'); ?>
      </div>
      <div class="box-body">
        <!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="cobranzatodos_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>

        <!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
        <div class="div_listado">
          <div class="box box-success shadow">
            <div class="box-header with-border">
              <h3 class="box-title">CUOTAS POR COBRAR SEGÚN EL RANGO DE FECHAS SELECCIONADO</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table id="table_cobranzatodos" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>N*</th>
                      <th>ID CREDITO</th>
                      <th>NOMBRES CLIENTE</th>
                      <!--th class="no-sort">DNI / RUC</th>
                      <th class="no-sort">DNI REPRESENT.</th -->
                      <th>REPRESENTANTE</th>
                      <th class="no-sort">TELEFONO</th>
                      <th>CREDITO GENERAL</th>
                      <th>SUBTIPO CREDITO</th>
                      <th>TIPO CUOTA</th>
                      <th>MONEDA</th>
                      <th>MONTO CUOTA</th>
                      <th>PAGO PARCIAL</th>
                      <th>SALDO POR PAGAR</th>
                      <th>VENCIMIENTO</th>
                      <th class="no-sort">N° CUOTA</th>
                      <th>FECHA ULT. PAGO</th>
                      <th>OPCIONES</th>
                      <th>RESULTADO</th>
                      <th class="no-sort">COMENTARIOS</th>
                      <th class="no-sort">ACTIVIDADES</th>
                    </tr>
                  </thead>
                  <tbody id="tbl_cobranzatodos_lista">

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div id="div_segmentado">
        </div>

        <!-- RESUMEN DE COBROS TOTALES-->
        <div class="box box-info shadow div_listado">
          <div class="box-header with-border">
            <h3 class="box-title">RESUMEN DE CUOTAS POR COBRAR Y MONTOS COBRADOS</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>TOTAL CUOTAS POR COBRAR EN S/.</th>
                    <th>TOTAL CUOTAS POR COBRAR EN US$</th>
                    <th>TOTAL PAGOS PARCIALES EN S/.</th>
                    <th>TOTAL PAGOS PARCIALES EN US$</th>
                    <th>SALDO POR COBRAR EN S/.</th>
                    <th>SALDO POR COBRAR EN US$</th>
                  </tr>
                </thead>
                <tbody id="tbl_cobranzatodos_resumen">

                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
      <div id="div_modal_opcionesgc_asignar_form">
        <!-- INCLUIMOS EL MODAL PARA EL REGISthO DEL TIPO-->
      </div>
      <div id="div_modal_opcionesgc_timeline">
      </div>

      <div id="div_modal_cliente_contacto"></div>
      <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
      <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
    </div>
  </section>
  <!-- /.content -->
</div>