<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class CobranzaTodos extends Conexion{

    function cobranzatodos_cuotas_facturadas_cmenor($fecha_filtro1, $fecha_filtro2, $fecha_hoy, $credito_est, $cliente_id, $cuota_condicion){
      try {
        $filtro_cliente = '';
        if(intval($cliente_id) > 0)
          $filtro_cliente = 'AND cre.tb_cliente_id = '.intval($cliente_id);

        $filtro_condicion = ''; //cuota condicion: 1 mostrar todos, 2 cuotas vencidas, 3 cuotas por vencer
        if(intval($cuota_condicion) == 2) //cuotas vencidas
          $filtro_condicion = "AND tb_cuota_fec <= '".$fecha_hoy."'";
        if(intval($cuota_condicion) == 3) //cuotas por vencer
          $filtro_condicion = "AND tb_cuota_fec > '".$fecha_hoy."'";

        $sql = "
          SELECT 
            cre.tb_credito_id,
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_id,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            (CASE
              WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 2 THEN tb_cuota_cuo
              ELSE tb_cuota_cuo
            END) AS cuota_real

          FROM tb_cuota cuo
          LEFT JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 1)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
  
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1
            AND tb_cuota_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2 AND tb_cuota_est IN(1,3)
            AND tb_credito_est IN (".$credito_est.") ".$filtro_cliente." ".$filtro_condicion."
          ORDER BY 1, tb_cuota_fec
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        //$sentencia->bindParam(":credito_est", $credito_est, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO MENOR";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function cobranzatodos_cuotas_facturadas_rango_fechas($fecha_filtro1, $fecha_filtro2, $fecha_hoy, $credito_est, $cliente_id, $cuota_condicion){
      try {
        $filtro_cliente = '';
        if(intval($cliente_id) > 0)
          $filtro_cliente = 'AND cre.tb_cliente_id = '.intval($cliente_id);

        $filtro_condicion = ''; //cuota condicion: 1 mostrar todos, 2 cuotas vencidas, 3 cuotas por vencer
        if(intval($cuota_condicion) == 2) //cuotas vencidas
          $filtro_condicion = "AND tb_cuotadetalle_fec <= '".$fecha_hoy."'";
        if(intval($cuota_condicion) == 3) //cuotas por vencer
          $filtro_condicion = "AND tb_cuotadetalle_fec > '".$fecha_hoy."'";

        $sql = "
          -- TABLA CREDITO VEHICULAR
          SELECT 
            cre.tb_credito_id,
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_credito_tip AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo, 
            tb_cuotadetalle_est,
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real,
            opc.*, asig.resultado_nom, asig.comentario_des, asig.opciongc_fechapdp

          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN tb_creditogarveh cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 3)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_asignaropciongc asig ON asig.cuotadetalle_id = det.tb_cuotadetalle_id 
          LEFT JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id
  
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND tb_cuotadetalle_xac = 1 AND tb_cuotadetalle_estap = 0 
            AND tb_cuotadetalle_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2 AND tb_cuotadetalle_est IN(1,3)
            AND tb_credito_est IN (".$credito_est.") ".$filtro_cliente." ".$filtro_condicion."
        
        -- UNIMOS CON TABLA DE ASISTENCIA VEHICULAR
        UNION
      
          SELECT 
            cre.tb_credito_id,
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_credito_tip1 AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo, 
            tb_cuotadetalle_est,
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real,
            opc.*, asig.resultado_nom, asig.comentario_des, asig.opciongc_fechapdp
        
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN tb_creditoasiveh cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 2)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_asignaropciongc asig ON asig.cuotadetalle_id = det.tb_cuotadetalle_id 
          LEFT JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id
      
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND tb_cuotadetalle_xac = 1 AND tb_cuotadetalle_estap = 0 
            AND tb_cuotadetalle_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2 AND tb_cuotadetalle_est IN(1,3)
            AND tb_credito_est IN (".$credito_est.") ".$filtro_cliente." ".$filtro_condicion."
      
        -- UNIMOS CON CREDITO HIPOTECARIO
        UNION
  
          SELECT 
            cre.tb_credito_id,
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_credito_tip AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo,
            tb_cuotadetalle_est, 
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real,
            opc.*, asig.resultado_nom, asig.comentario_des, asig.opciongc_fechapdp
        
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN tb_creditohipo cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 4)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_asignaropciongc asig ON asig.cuotadetalle_id = det.tb_cuotadetalle_id 
          LEFT JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id
          
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND tb_cuotadetalle_xac = 1 AND tb_cuotadetalle_estap = 0 
            AND tb_cuotadetalle_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2 AND tb_cuotadetalle_est IN(1,3)
            AND tb_credito_est IN (".$credito_est.") ".$filtro_cliente." ".$filtro_condicion."
        
        ORDER BY 1, tb_cuotadetalle_fec
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        //$sentencia->bindParam(":credito_est", $credito_est, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function cobranzatodos_cuotas_facturadas_tipocredito($fecha_filtro1, $fecha_filtro2, $fecha_hoy, $credito_est, $cliente_id, $cuota_condicion, $creditotipo_id){
      try {
        $filtro_cliente = '';
        if(intval($cliente_id) > 0)
          $filtro_cliente = 'AND cre.tb_cliente_id = '.intval($cliente_id);

        $filtro_condicion = ''; //cuota condicion: 1 mostrar todos, 2 cuotas vencidas, 3 cuotas por vencer
        if(intval($cuota_condicion) == 2) //cuotas vencidas
          $filtro_condicion = "AND tb_cuotadetalle_fec <= '".$fecha_hoy."'";
        if(intval($cuota_condicion) == 3) //cuotas por vencer
          $filtro_condicion = "AND tb_cuotadetalle_fec > '".$fecha_hoy."'";

        $columna_tipo = 'tb_credito_tip';
        if($creditotipo_id == 2)
          $columna_tipo = 'tb_credito_tip1';

        $tabla_credito = 'tb_creditoasiveh';
        if($creditotipo_id == 3)
          $tabla_credito = 'tb_creditogarveh';
        if($creditotipo_id == 4)
          $tabla_credito = 'tb_creditohipo';

        $sql = "
          -- TABLA CREDITO VEHICULAR
          SELECT 
            cre.tb_credito_id,
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.$columna_tipo AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo, 
            tb_cuotadetalle_est,
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real,
            opc.*, asig.resultado_nom, asig.comentario_des, asig.opciongc_fechapdp

          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN $tabla_credito cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id =:creditotipo_id)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_asignaropciongc asig ON asig.cuotadetalle_id = det.tb_cuotadetalle_id 
          LEFT JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id
  
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND tb_cuotadetalle_xac = 1 AND tb_cuotadetalle_estap = 0 
            AND tb_cuotadetalle_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2 AND tb_cuotadetalle_est IN(1,3)
            AND tb_credito_est IN (".$credito_est.") ".$filtro_cliente." ".$filtro_condicion."

          ORDER BY 1, tb_cuotadetalle_fec
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_informacion_credito_cuotadetalle($credito_id, $credito_tipo, $cuotadetalle_id){
      try {
        $tabla = 'tb_creditomenor';
        if(intval($credito_tipo) == 2) $tabla = 'tb_creditoasiveh';
        if(intval($credito_tipo) == 3) $tabla = 'tb_creditogarveh';
        if(intval($credito_tipo) == 4) $tabla = 'tb_creditohipo';

        $sql = "
          SELECT 
            tb_cliente_nom,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cli.tb_cliente_id,
            cre.tb_cuotatipo_id,
            cuo.tb_moneda_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo, 
            tb_cuotadetalle_est,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real

            FROM tb_cuota cuo
            LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
            LEFT JOIN ".$tabla." cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id =:credito_tipo)
            LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id

          WHERE 
            cre.tb_credito_id =:credito_id AND tb_cuotadetalle_id =:cuotadetalle_id
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_tipo", $credito_tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotadetalle_id", $cuotadetalle_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function total_cuotas_impagas_por_credito($fecha_filtro, $credito_tipo, $credito_id){
      try {
        $tabla = 'tb_creditomenor';
        if(intval($credito_tipo) == 2) $tabla = 'tb_creditoasiveh';
        if(intval($credito_tipo) == 3) $tabla = 'tb_creditogarveh';
        if(intval($credito_tipo) == 4) $tabla = 'tb_creditohipo';

        $sql = "
          SELECT 
            cre.tb_credito_id,
            tb_cuota_num,
            cuo.tb_moneda_id,
            tb_cuotadetalle_cuo,
            tb_cuotadetalle_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuotadetalle_est, 
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real
          
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN ".$tabla." cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = :credito_tipo)
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND (tb_cuotadetalle_xac = 1 OR tb_cuotadetalle_xac IS NULL) 
            AND tb_cuotadetalle_est IN(1,3) AND tb_cuotadetalle_fec <= :fecha_filtro AND cre.tb_credito_id = :credito_id
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro", $fecha_filtro, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_tipo", $credito_tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
