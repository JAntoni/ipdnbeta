<?php 
  $fecha = new DateTime();
  $fecha->modify('last day of this month');

  $cuota_fec1 = date('01-m-Y');
  $cuota_fec2 = $fecha->format('d-m-Y');
?>
<style>
  .label-filter{
    padding-left: 1%;
    padding-right: 1%;
    font-size: 12pt;
  }
  .input-filter{
    width: 10%;
  }
</style>
<div class="col-md-12 shadow" style="margin-bottom: -0.8% !Important;">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>Rango de Fechas:</label>
        <div class='input-group'>
          <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $cuota_fec1;?>"/>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
          <span class="input-group-addon">-</span>
          <div class='input-group date' id='datetimepicker2'>
            <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $cuota_fec2;?>"/>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label for="txt_filtro_cliente" class="control-label">Cliente:</label>
        <input type='text' class="form-control input-sm" name="txt_filtro_cliente" id="txt_filtro_cliente"/>
        <input type="hidden" id="hdd_fil_cliente_id"/>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label for="txt_filtro_cliente" class="control-label">Condición de Cuota:</label>
        <select class="form-control input-sm" id="cmb_filtro_condicion" name="cmb_filtro_condicion">
          <option value="1">Mostrar Todo</option>
          <option value="2">Cuotas Vencidas</option>
          <option value="3">Cuotas por Vencer</option>
        </select>
      </div>
    </div>
    <div class="col-md-1">
      <div class="form-group">
        <label for="txt_filtro_cmenor" class="control-label">¿Incluir C-Menor?</label><br>
        <label class="checkbox-inline">
          <b>SI</b> <input type="checkbox" name="txt_filtro_cmenor" id="txt_filtro_cmenor" value="1" class="flat-green" checked>
        </label>
      </div>
    </div>
    <div class="col-md-2">
      <br>
      <button type="button" class="btn btn-success btn-sm" onclick="cobranzatodos_tabla()"><i class="fa fa-search"></i> Vista en Lista</button>
      <button type="button" class="btn btn-primary btn-sm" onclick="cobranzatodos_segmentado()"><i class="fa fa-search"></i> Vista Segmentado</button>
    </div>
  </div>
</div>