<?php
  date_default_timezone_set("America/Lima");
  require_once('../cuota/Cuota.class.php');
  require_once('../ingreso/Ingreso.class.php');
  require_once('../cobranzatodos/CobranzaTodos.class.php');
  require_once('../cobranza/Cobranza.class.php');
  require_once('../creditomenor/Creditomenor.class.php');
  require_once('../creditolinea/Creditolinea.class.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');
  require_once('../monedacambio/Monedacambio.class.php');

  $oCobranzatodos = new CobranzaTodos();
  $oIngreso = new Ingreso();
  $oAsignar = new AsignarOpcionGc();
  $oCambio = new Monedacambio();
  $oCobranza = new Cobranza();
  $oCreditomenor = new Creditomenor();
  $oCreditolinea = new Creditolinea();

  //? VAMOS A ONTENER UNA LISTA DE CUOTAS QUE LLEVAN MÁS DE 30 DÍAS VENCIDOS PARA PODER PASARLOS A REMATE DE MANERA AUTOMÁTICA
  $result = $oCobranza->lista_cuotas_vencidas_30dias();
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $fecha_compromiso = $value['tb_cobranza_fec_compromiso'];
        $fecha_hoy = date('Y-m-d');

        if(strtotime($fecha_hoy) > strtotime($fecha_compromiso)){
          //? AQUI ENTRAN TODAS LAS CUOTAS QUE TIENEN MÁS DE 30 DÍAS VENCIDOS Y NO TIENEN NINGUNA FECHA DE COMPROMISO DE PAGO, ENTONCES PROCEDEMOS A PASARLO A REMATE
          //? 1. PASAMOS EL CRÉDITO A REMATE
          $oCreditomenor->modificar_campo($value['tb_credito_id'], 'tb_credito_est', 5, 'INT');
          $creditolinea_det = 'El crédito ha pasado a Remate de manera AUTOMÁTICA ya que pasó 30 días de cuota vencida';
          $oCreditolinea->insertar(1, $value['tb_credito_id'], 2, $creditolinea_det);

          //? 2. ACTUALIZAMOS EL ESTADO DE LA COBRANZA, PARA QUE YA NO APAREZCA EN PENDIENTE DE PAGO
          $oCobranza->modificar_remate_cobranza($value['tb_cobranza_id'], $fecha_hoy, 2, 4); // 2 = usuario sistemas, 4 estado de cobranza a remate

          echo 'El crédito: '.$value['tb_credito_id'].' está pasando a remate de manera automatica, tiene '.$value['dias_diferencia'].' días vencido </br>';
        }
      }
    }
    else
      echo 'Sin cuotas vencidas más de 30 días';
  $result = NULL;

  $fecha_ultimo = new DateTime();
  $fecha_ultimo->modify('last day of this month');

  $cuota_fec1 = date('2000-01-01'); //fecha más antigua para listar clientes antiguos con deuda
  $cuota_fec2 = $fecha_ultimo->format('Y-m-d');; // fecha fin para el filtro
  $fecha_hoy = date('Y-m-d');
  $cliente_id =  0;
  $cuota_condicion = 2; // cuotas vencidas

  $moneda_cambio = 3.72;
  $result = $oCambio->monedacambio_ultimo_registrado();
    if($result['estado'] == 1){
      $moneda_cambio = floatval($result['data']['tb_monedacambio_val']);
    }
  $result = NULL;

  $data = '';
  $return_mejorado['estado'] = 0;
  $return_mejorado['mensaje'] = 'Existe un error al parecer en la carga de datos';
  $numero_orden = 0;

  $TOTAL_CUOTAS_SOLES = 0;
  $TOTAL_CUOTAS_DOLARES = 0;
  $TOTAL_COBRADO_SOLES = 0;
  $TOTAL_COBRADO_DOLARES = 0;
  $SALDO_COBRAR_SOLES = 0;
  $SALDO_COBRAR_DOLARES = 0;

  // 3 para vigentes, 4 paralizados
  $result = $oCobranzatodos->cobranzatodos_cuotas_facturadas_rango_fechas($cuota_fec1, $cuota_fec2, $fecha_hoy, '3', $cliente_id, $cuota_condicion);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $pago_parcial = 0;
        $numero_orden ++;
        $moneda_simbolo = 'S/.';
        $monto_cuota = $value['cuota_real']; //que toma en cuenta el monto de la cuota detalle, en casos de que sea semanal

        if($value['tb_moneda_id'] == 1){
          $TOTAL_CUOTAS_SOLES += $monto_cuota;
        }
        if($value['tb_moneda_id'] == 2){
          $moneda_simbolo = 'US$';
          $TOTAL_CUOTAS_DOLARES += $monto_cuota;
        }

        if(intval($value['tb_cuotadetalle_est']) == 3){ //todas las cuotas en pago parcial
          $tipo_cuota_pago = 1; // 1 es para pago de cuota
          //$cuota_id = intval($value['tb_cuota_id']);

          if(intval($value['tb_cuotadetalle_id']) > 0){
            $tipo_cuota_pago = 2; //2 para pago de cuota detalle
            $cuota_id = intval($value['tb_cuotadetalle_id']);
          }
          
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
            if($result2['estado'] == 1){
              $pago_parcial = $result2['data']['importe_total'];
              if($value['tb_moneda_id'] == 1)
                $TOTAL_COBRADO_SOLES += $pago_parcial;
              if($value['tb_moneda_id'] == 2)
                $TOTAL_COBRADO_DOLARES += $pago_parcial;
            }
          $result2 = NULL;
        }

        $saldo_por_pagar = $monto_cuota - $pago_parcial;
      }
    }
  $result = NULL;
  
  $SALDO_COBRAR_SOLES = $TOTAL_CUOTAS_SOLES - $TOTAL_COBRADO_SOLES;
  $SALDO_COBRAR_DOLARES = $TOTAL_CUOTAS_DOLARES - $TOTAL_COBRADO_DOLARES;
  
  $DEUDA_ACUMULADA = $SALDO_COBRAR_SOLES + ($SALDO_COBRAR_DOLARES * $moneda_cambio);

  $alerta_deudadiaria = 'NO SE GUARDÓ, YA EXISTE';
  $result = $oAsignar->validar_deudadiaria($fecha_hoy);
    if($result['estado'] == 0){
      //la deuda diaria aún no está guardada, procedemos a guardar
      $oAsignar->insertar_deudadiaria($fecha_hoy, $DEUDA_ACUMULADA);
      $alerta_deudadiaria = 'Deuda Diaria guardada. Se guardó la deuda del día <b>'.$fecha_hoy.', un monto de: <b>'.mostrar_moneda($DEUDA_ACUMULADA).'</b></b>';
    }
  $result = NULL;

  echo 'Deuda acumulada total: '.mostrar_moneda($DEUDA_ACUMULADA).' / con tipo de cambio: ' .$moneda_cambio.' | '.$alerta_deudadiaria;
?>