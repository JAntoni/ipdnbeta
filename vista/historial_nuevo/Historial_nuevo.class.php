<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Historial_nuevo extends Conexion {
    public $id;
    public $tablanom;
    public $regmodid;
    public $columnamod;
    public $valorant;
    public $valornuevo;
    public $fecreg;
    public $usureg;
    public $xac;
    public $det;

    public function listar_todos($where, $join, $otros) {
        try {
            $sql = "SELECT hn.*";
            if (!empty($join)) {
                for ($j = 0; $j < count($join); $j++) {
                    $sql .= ",\n" . $join[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_histnuevo hn\n";
            if (!empty($join)) {
                for ($k = 0; $k < count($join); $k++) {
                    $sql .= $join[$k]['tipo_union'] . " JOIN " . $join[$k]['tabla_alias'] . " ON ";
                    if (!empty($join[$k]['columna_enlace']) && !empty($join[$k]['alias_columnaPK'])) {
                        $sql .= $join[$k]['columna_enlace'] . " = " . $join[$k]['alias_columnaPK'] . "\n";
                    } else {
                        $sql .= $join[$k]['on'] . "\n";
                    }
                }
            }
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i])) {
                    $sql .= ($i > 0) ? "\n{$where[$i]["conector"]} " : "WHERE ";

                    if (!empty($where[$i]["column_name"])) {
                        if (stripos($where[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$where[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$where[$i]["column_name"]}";
                        }

                        if (!empty($where[$i]["datatype"])) {
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " " . $where[$i]["param$i"];
                        }
                    } else {
                        $sql .= $where[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros["group"])) {
                $sql .= "\nGROUP BY {$otros["group"]["column_name"]}";
            }
            if (!empty($otros["orden"])) {
                $sql .= "\nORDER BY {$otros["orden"]["column_name"]} {$otros["orden"]["value"]}";
            }
            if (!empty($otros["limit"])) {
                $sql .= "\nLIMIT {$otros["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i]["column_name"]) && !empty($where[$i]["datatype"])) {
                    $_PARAM = strtoupper($where[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $where[$i]["param$i"], $_PARAM);
                }
            }
            $retorno["sql"] = $sql;
            $sentencia->execute();

            $retorno["row_count"] = $sentencia->rowCount();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } 
            else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $column_opc = !empty($this->det) ? ",\ntb_histnuevo_det" : '';
            $param_opc = !empty($this->det) ? ",\n:param_opc1" : '';
            $sql = "INSERT INTO tb_histnuevo (
                tb_histnuevo_tablanom,
                tb_histnuevo_regmodid,
                tb_histnuevo_columnamod,
                tb_histnuevo_valorant,
                tb_histnuevo_valornuevo,
                tb_histnuevo_usureg$column_opc)
              VALUES (
                :param0,
                :param1,
                :param2,
                :param3,
                :param4,
                :param5$param_opc);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tablanom, PDO::PARAM_STR);
            $sentencia->bindParam(":param1", $this->regmodid, PDO::PARAM_INT);
            $sentencia->bindParam(":param2", $this->columnamod, PDO::PARAM_STR);
            $sentencia->bindParam(":param3", $this->valorant, PDO::PARAM_STR);
            $sentencia->bindParam(":param4", $this->valornuevo, PDO::PARAM_STR);
            $sentencia->bindParam(":param5", $this->usureg, PDO::PARAM_INT);
            if (!empty($this->det)) {
                $sentencia->bindParam(":param_opc1", $this->det, PDO::PARAM_STR);
            }
            
            $resultado['estado'] = $sentencia->execute();
            $resultado['sql'] = $sql;

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Historial registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_campo($histnuevo_id, $histnuevo_columna, $histnuevo_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($histnuevo_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_histnuevo SET " . $histnuevo_columna . " = :histnuevo_valor WHERE tb_histnuevo_id = :histnuevo_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":histnuevo_id", $histnuevo_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":histnuevo_valor", $histnuevo_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":histnuevo_valor", $histnuevo_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
?>
