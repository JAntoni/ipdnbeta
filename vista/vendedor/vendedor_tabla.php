<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'vendedor/Vendedor.class.php');
    require_once(VISTA_URL . 'usuario/Usuario.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../vendedor/Vendedor.class.php');
    require_once('../usuario/Usuario.class.php');
    require_once('../funciones/fechas.php');
}
$oVendedor = new Vendedor();
$oUsuario= new Usuario();
?>
<table id="tbl_vendedors" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">DNI</th> 
            <th id="tabla_cabecera_fila">NOMBRE</th>
            <th id="tabla_cabecera_fila">TELÉFONO</th>
            <th id="tabla_cabecera_fila">DIRECCIÓN</th>
            <th id="tabla_cabecera_fila">CONCESIONARIA</th>
            <th id="tabla_cabecera_fila">REGISTRADO POR</th>
            <th id="tabla_cabecera_fila"></th>
        </tr>
    </thead>
    <tbody>
<?php
//PRIMER NIVEL
$result = $oVendedor->listar_vendedors();
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value):
        
        $usureg=$value['tb_vendedor_usereg'];
    
            $result2=$oUsuario->mostrarUno($usureg);
            if($result2['estado']==1){
                $tb_usuario_nom=$result2['data']['tb_usuario_nom'];
                $tb_usuario_ape=$result2['data']['tb_usuario_ape'];
            }
        ?>
        <tr id="tabla_cabecera_fila" style="font-family: cambria">
                    <td id="tabla_fila"><?php echo $value['tb_vendedor_id']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_vendedor_dni']; ?></td>
                    <td id="tabla_fila" align="right"><?php echo $value['tb_vendedor_nombre']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_vendedor_tel']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_vendedor_dir']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_vendedor_tienda']; ?></td>
                    <td id="tabla_fila"><?php echo '<b>'.$tb_usuario_nom.' '.$tb_usuario_ape.'</b> - '. mostrar_fecha_hora($value['tb_vendedor_reg']); ?></td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-info btn-xs" title="Ver" onclick="vendedor_form(<?php echo "'L', " . $value['tb_vendedor_id']; ?>)"><i class="fa fa-eye"></i></a>
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="vendedor_form(<?php echo "'M', " . $value['tb_vendedor_id']; ?>)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="vendedor_form(<?php echo "'E', " . $value['tb_vendedor_id']; ?>)"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
        <?php
    endforeach;
}
$result = NULL;
?>
    </tbody>
</table>
