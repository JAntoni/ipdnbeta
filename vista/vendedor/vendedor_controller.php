<?php

require_once('../../core/usuario_sesion.php');
require_once('../vendedor/Vendedor.class.php');
$oVendedor = new Vendedor();
require_once('../funciones/fechas.php');

$action = (empty($_POST['action'])) ? $_GET['action'] : $_POST['action'];

$ubigeo_cod = '';

$tb_vendedor_id = $_POST['hdd_vendedor_id'];
$tb_vendedor_dni = $_POST['txt_vendedor_dni'];
$tb_vendedor_nombre = mb_strtoupper($_POST['txt_vendedor_nom']);
$tb_vendedor_dir = mb_strtoupper($_POST['txt_vendedor_dir']);
$tb_vendedor_tel = $_POST['txt_vendedor_tel'];
$tb_vendedor_tienda = mb_strtoupper($_POST['txt_vendedor_tienda']);
$tb_vendedor_usereg=intval($_SESSION['usuario_id']);

$oVendedor->setTb_vendedor_id($tb_vendedor_id);
$oVendedor->setTb_vendedor_dni($tb_vendedor_dni);
$oVendedor->setTb_vendedor_nombre($tb_vendedor_nombre);
$oVendedor->setTb_vendedor_dir($tb_vendedor_dir);
$oVendedor->setTb_vendedor_tel($tb_vendedor_tel);
$oVendedor->setTb_vendedor_tienda($tb_vendedor_tienda);
$oVendedor->setTb_vendedor_usereg($tb_vendedor_usereg);
//+51 934 154 748
//echo '$tb_vendedor_dni='.$tb_vendedor_dni.' - $tb_vendedor_nombre='.$tb_vendedor_nombre.' - $tb_vendedor_dir='.$tb_vendedor_dir.' - $tb_vendedor_tel='.$tb_vendedor_tel.' - $action='.$action;exit();

if ($action == 'insertar') {
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar al Vendedor.';

    $vendedor_id = 0;
    if (trim($tb_vendedor_dni) != '') {
        $result = $oVendedor->verificar_documento_vendedor($tb_vendedor_dni, $vendedor_id);
        $vendedor_existe = $result['estado'];
        $result = NULL;
    }

    if ($vendedor_existe == 1) {
        $data['mensaje'] = 'El documento del Vendedor ya ha sido registrado / ' . $tb_vendedor_dni;
    } else {
        $result =$oVendedor->insertar(); //devuelve un array, estado e ID de Vendedor
        $vendedor_id = $result['vendedor_id'];
//        $result = NULL;
        $data['vendedor_id'] = $vendedor_id;
        $data['estado'] = $result['vendedor_id'];
        $data['mensaje'] = 'Vendedor Registrado correctamente.';
        $data['vendedor_dni'] = $_POST['txt_vendedor_dni'];
        $data['vendedor_nom'] = mb_strtoupper($_POST['txt_vendedor_nom'], 'UTF-8');
        $data['vendedor_tel'] = $_POST['txt_vendedor_tel'];
        $data['vendedor_dir'] = mb_strtoupper($_POST['txt_vendedor_dir']);
    }
    echo json_encode($data);
} elseif ($action == 'modificar') {
//    $vendedor_id = intval($_POST['hdd_vendedor_id']);
//    $oVendedor->vendedor_id = $vendedor_id;

    $tb_vendedor_dni = $_POST['txt_vendedor_dni'];
    if (trim($tb_vendedor_dni) != '') {
        $result = $oVendedor->verificar_documento_vendedor($tb_vendedor_dni, $vendedor_id);
        $vendedor_existe = $result['estado'];
        $result = NULL;
    }

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar al Vendedor.';

    if ($oVendedor->modificar()) {

        $data['estado'] = 1;
        $data['mensaje'] = 'Vendedor modificado correctamente.';
        $data['vendedor_id'] = $vendedor_id;
        $data['vendedor_dni'] = $_POST['txt_vendedor_dni'];
        $data['vendedor_nom'] = mb_strtoupper($_POST['txt_vendedor_nom'], 'UTF-8');
        $data['vendedor_tel'] = $_POST['txt_vendedor_tel'];
        $data['vendedor_dir'] = $_POST['txt_vendedor_dir'];
    }
    echo json_encode($data);
} elseif ($action == 'eliminar') {
//    $vendedor_id = intval($_POST['hdd_vendedor_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar al vendedor.';

    if ($oVendedor->eliminar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Vendedor eliminado correctamente. ' . $vendedor_des;
    }
    echo json_encode($data);
} elseif ($action == 'vendedordetalle') {
    
} else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>
