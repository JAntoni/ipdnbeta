<?php
  require_once ("Vendedor.class.php");
  $oVendedor = new Vendedor();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $vendedor_id;
    var $vendedor_nom;
    var $vendedor_dir;
    var $vendedor_dni;
    var $vendedor_tel;
    var $vendedor_tie;


    function __construct($label, $value, $vendedor_id, $vendedor_nom,$vendedor_dir, $vendedor_dni, $vendedor_tel,$vendedor_tie){ 
      $this->label = $label;
      $this->value = $value;
      $this->vendedor_id = $vendedor_id;
      $this->vendedor_nom = $vendedor_nom;
      $this->vendedor_dir = $vendedor_dir;	
      $this->vendedor_dni = $vendedor_dni;
      $this->vendedor_tel = $vendedor_tel; 
      $this->vendedor_tie = $vendedor_tie; 
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];

  //busco un valor aproximado al dato escrito
  $result = $oVendedor->vendedor_autocomplete($datoBuscar);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    foreach ($result['data'] as $key => $value) {
      array_push(
        $arrayElementos, 
        new ElementoAutocompletar(
                $value["tb_vendedor_dni"].'-'.$value["tb_vendedor_nombre"],
                $value["tb_vendedor_nombre"], 
                $value["tb_vendedor_id"], 
                $value["tb_vendedor_nombre"], 
                $value['tb_vendedor_dir'], 
                $value['tb_vendedor_dni'], 
                $value['tb_vendedor_tel'],
                $value['tb_vendedor_tienda'])
      );
    }

    print_r(json_encode($arrayElementos));
  $result = NULL;
?>