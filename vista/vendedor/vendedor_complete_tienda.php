<?php
require_once('../../core/usuario_sesion.php');
require_once('Vendedor.class.php');
$oVendedor = new Vendedor();

//defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
class ElementoAutocompletar {
   var $value;
   var $label;
   
   function __construct($label, $value){
      $this->label = $label;
      $this->value = $value;
   }
}

//recibo el dato que deseo buscar sugerencias
$datoBuscar = $_GET["term"];

//$emp=$_SESSION['empresa_id'];
$emp=1;
//echo 'hast aki estoy entrando normal al sistema';exit();
//busco un valor aproximado al dato escrito
$rs=$oVendedor->complete_det($datoBuscar);

//creo el array de los elementos sugeridos
$arrayElementos = array();


//bucle para meter todas las sugerencias de autocompletar en el array
if($rs['estado']==1){
    foreach ($rs['data']as $key=>$fila){
       array_push($arrayElementos, new ElementoAutocompletar($fila["tb_vendedor_tienda"], $fila["tb_vendedor_tienda"]));
    }
}
print_r(json_encode($arrayElementos));

?>