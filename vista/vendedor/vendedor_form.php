<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../vendedor/Vendedor.class.php');
$oVendedor = new Vendedor();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'vendedor';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$vendedor_id = $_POST['vendedor_id'];
$vista = $_POST['vista'];

$titulo = '';
if ($usuario_action == 'L')
    $titulo = 'Cliente Registrado';
if ($usuario_action == 'I')
    $titulo = 'Registrar Cliente';
elseif ($usuario_action == 'M')
    $titulo = 'Editar Cliente';
elseif ($usuario_action == 'E')
    $titulo = 'Eliminar Cliente';
else
    $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en vendedor
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos))
        $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
        $usuario_id = $_SESSION['usuario_id'];
        $modulo = 'vendedor';
        $modulo_id = $vendedor_id;
        $tipo_permiso = $usuario_action;
        $estado = 1;

        $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if ($result['estado'] == 1) {
            $bandera = 1; // si tiene el permiso para la accion que desea hacer
        } else {
            $result = NULL;
            echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
            exit();
        }
        $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del vendedor por su ID
    $vendedor_tip = 0;

    if (intval($vendedor_id) > 0) {
        $result = $oVendedor->mostrarUno($vendedor_id);
        if ($result['estado'] != 1) {
            $mensaje = 'No se ha encontrado ningún registro para el vendedor seleccionado, inténtelo nuevamente.';
            $bandera = 4;
        } else {
            $vendedor_dni = $result['data']['tb_vendedor_dni'];
            $vendedor_nom = $result['data']['tb_vendedor_nombre'];
            $vendedor_tel = $result['data']['tb_vendedor_tel'];
            $vendedor_dir = $result['data']['tb_vendedor_dir'];
            $vendedor_tienda = $result['data']['tb_vendedor_tienda'];
        }
        $result = NULL;
    }
} else {
    $mensaje = $result['mensaje'];
    $bandera = 4;
    $result = NULL;
}
?>
<?php if ($bandera == 1): ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vendedor" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo; ?></h4>
                </div>
                <form id="form_vendedor" method="post">
                    <input type="hidden" name="action" value="<?php echo $action; ?>">
                    <input type="hidden" name="hdd_vendedor_id" value="<?php echo $vendedor_id; ?>">
                    <input type="hidden" id="vendedor_vista" value="<?php echo $vista; ?>">

                    <div class="modal-body">
                        <div class="row">
                            <!-- GRUPO 1 DATOS DEL CLIENTE-->
                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading" style="font-family: cambria">
                                        <b>DATOS DEL VENDEDOR</b>
                                    </div>
                                    <div class="panel-body">
                                        <!-- FIN DATOS DE PERSONA JURIDICA-->
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_vendedor_doc" class="control-label">DNI del Vendedor</label>
                                                    <input type="text" name="txt_vendedor_dni" id="txt_vendedor_dni" class="form-control input-sm" value="<?php echo $vendedor_dni; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="txt_vendedor_nom" class="control-label">Nombres del Vendedor</label>
                                                    <input type="text" name="txt_vendedor_nom" id="txt_vendedor_nom" class="form-control input-sm mayus" value="<?php echo $vendedor_nom; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_vendedor_doc" class="control-label">Teléfono</label>
                                                    <input type="text" name="txt_vendedor_tel" id="txt_vendedor_tel" class="form-control input-sm" value="<?php echo $vendedor_tel; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="txt_vendedor_nom" class="control-label">Dirección</label>
                                                    <input type="text" name="txt_vendedor_dir" id="txt_vendedor_dir" class="form-control input-sm mayus" value="<?php echo $vendedor_dir; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="txt_vendedor_nom" class="control-label">Tienda</label>
                                                    <input type="text" name="txt_vendedor_tienda" id="txt_vendedor_tienda" class="form-control input-sm mayus" value="<?php echo $vendedor_tienda; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                        <?php if ($action == 'eliminar'): ?>
                            <div class="callout callout-warning">
                                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cliente?</h4>
                            </div>
                        <?php endif; ?>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="vendedor_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M'): ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_vendedor">Guardar</button>
                            <?php endif; ?>
                            <?php if ($usuario_action == 'E'): ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_vendedor">Aceptar</button>
                            <?php endif; ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($bandera == 4): ?>
    <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_vendedor">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Mensaje Importante</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <p><?php echo $mensaje; ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/vendedor/vendedor_form.js?ver=116436464311'; ?>"></script>
