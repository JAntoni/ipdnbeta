<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Vendedor extends Conexion {

    private $tb_vendedor_id;
    private $tb_vendedor_reg;
    private $tb_vendedor_xac;
    private $tb_vendedor_dni;
    private $tb_vendedor_nombre;
    private $tb_vendedor_dir;
    private $tb_vendedor_tel;
    private $tb_vendedor_tienda;
    private $tb_vendedor_usereg;

    public function getTb_vendedor_id() {
        return $this->tb_vendedor_id;
    }

    public function getTb_vendedor_reg() {
        return $this->tb_vendedor_reg;
    }

    public function getTb_vendedor_xac() {
        return $this->tb_vendedor_xac;
    }

    public function getTb_vendedor_dni() {
        return $this->tb_vendedor_dni;
    }

    public function getTb_vendedor_nombre() {
        return $this->tb_vendedor_nombre;
    }

    public function getTb_vendedor_dir() {
        return $this->tb_vendedor_dir;
    }

    public function getTb_vendedor_tel() {
        return $this->tb_vendedor_tel;
    }

    public function getTb_vendedor_tienda() {
        return $this->tb_vendedor_tienda;
    }

    public function getTb_vendedor_usereg() {
        return $this->tb_vendedor_usereg;
    }

    public function setTb_vendedor_id($tb_vendedor_id){
        $this->tb_vendedor_id = $tb_vendedor_id;
    }

    public function setTb_vendedor_reg($tb_vendedor_reg){
        $this->tb_vendedor_reg = $tb_vendedor_reg;
    }

    public function setTb_vendedor_xac($tb_vendedor_xac){
        $this->tb_vendedor_xac = $tb_vendedor_xac;
    }

    public function setTb_vendedor_dni($tb_vendedor_dni){
        $this->tb_vendedor_dni = $tb_vendedor_dni;
    }

    public function setTb_vendedor_nombre($tb_vendedor_nombre){
        $this->tb_vendedor_nombre = $tb_vendedor_nombre;
    }

    public function setTb_vendedor_dir($tb_vendedor_dir){
        $this->tb_vendedor_dir = $tb_vendedor_dir;
    }

    public function setTb_vendedor_tel($tb_vendedor_tel){
        $this->tb_vendedor_tel = $tb_vendedor_tel;
    }

    public function setTb_vendedor_tienda($tb_vendedor_tienda){
        $this->tb_vendedor_tienda = $tb_vendedor_tienda;
    }

    public function setTb_vendedor_usereg($tb_vendedor_usereg){
        $this->tb_vendedor_usereg = $tb_vendedor_usereg;
    }

    function insertar() {
        
        
//        echo 'DNI='. $this->getTb_vendedor_dni().' - NOMBRE='. $this->getTb_vendedor_nombre();exit();
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_vendedor(
                                    tb_vendedor_dni,
                                    tb_vendedor_nombre,
                                    tb_vendedor_dir,
                                    tb_vendedor_tel,
                                    tb_vendedor_tienda,
                                    tb_vendedor_usereg)
                            VALUES(
                                    :tb_vendedor_dni,
                                    :tb_vendedor_nombre,
                                    :tb_vendedor_dir,
                                    :tb_vendedor_tel,
                                    :tb_vendedor_tienda,
                                    :tb_vendedor_usereg)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_vendedor_dni", $this->getTb_vendedor_dni(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_nombre", $this->getTb_vendedor_nombre(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_dir", $this->getTb_vendedor_dir(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_tel", $this->getTb_vendedor_tel(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_tienda", $this->getTb_vendedor_tienda(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_usereg", $this->getTb_vendedor_usereg(), PDO::PARAM_INT);
            $result = $sentencia->execute();
            $vendedor_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['vendedor_id'] = $vendedor_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_vendedor SET 
                                        tb_vendedor_dni=:tb_vendedor_dni,
                                        tb_vendedor_nombre=:tb_vendedor_nombre,
                                        tb_vendedor_dir=:tb_vendedor_dir,
                                        tb_vendedor_tel=:tb_vendedor_tel,
                                        tb_vendedor_tienda=:tb_vendedor_tienda,
                                        tb_vendedor_usereg=:tb_vendedor_usereg
                                WHERE
                                        tb_vendedor_id=:tb_vendedor_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_vendedor_dni", $this->getTb_vendedor_dni(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_nombre", $this->getTb_vendedor_nombre(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_dir", $this->getTb_vendedor_dir(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_tel", $this->getTb_vendedor_tel(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_tienda", $this->getTb_vendedor_tienda(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vendedor_usereg", $this->getTb_vendedor_usereg(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vendedor_id", $this->getTb_vendedor_id(), PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_vendedor SET 
                                        tb_vendedor_xac=0
                                WHERE
                                        tb_vendedor_id=:tb_vendedor_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_vendedor_id", $this->getTb_vendedor_id(), PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($vendedor_id) {
        try {
            $sql = "SELECT * FROM tb_vendedor WHERE tb_vendedor_id =:vendedor_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":vendedor_id", $vendedor_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_vendedors() {
        try {
            $sql = "SELECT * FROM tb_vendedor WHERE tb_vendedor_xac=1 ORDER BY tb_vendedor_tienda,tb_vendedor_nombre asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function verificar_documento_vendedor($tb_vendedor_dni, $vendedor_id) {
        try {
            if (intval($vendedor_id) > 0) {
                //vendedor está siendo editado
                $sql = "SELECT * FROM tb_vendedor WHERE tb_vendedor_dni !='' AND tb_vendedor_dni =:tb_vendedor_dni AND tb_vendedor_id !=:vendedor_id AND tb_vendedor_xac = 1";
            } else {
                //vendedor a registrar es nuevo
                $sql = "SELECT * FROM tb_vendedor WHERE tb_vendedor_dni !='' AND tb_vendedor_dni =:tb_vendedor_dni AND tb_vendedor_xac = 1";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_vendedor_dni", $tb_vendedor_dni, PDO::PARAM_STR);
            if (intval($vendedor_id) > 0)
                $sentencia->bindParam(":vendedor_id", $vendedor_id, PDO::PARAM_INT);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay vendedors registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function vendedor_autocomplete($dato) {
        try {

            $filtro = "%" . $dato . "%";

            $sql = "SELECT * FROM tb_vendedor WHERE tb_vendedor_xac = 1 AND (tb_vendedor_nombre LIKE :filtro OR tb_vendedor_dni LIKE :filtro)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay vendedors registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function modificar_campo($vendedor_id, $vendedor_columna, $vendedor_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($vendedor_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_vendedor SET " . $vendedor_columna . " =:vendedor_valor WHERE tb_vendedor_id =:vendedor_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":vendedor_id", $vendedor_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":vendedor_valor", $vendedor_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":vendedor_valor", $vendedor_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    function complete_det($det){
        $filtro = "%".$det."%";
        try {
            $sql="  SELECT 
                        DISTINCT(tb_vendedor_tienda) 
                    FROM 
                        tb_vendedor
                    WHERE 
                        tb_vendedor_tienda like :filtro ORDER BY tb_vendedor_tienda LIMIT 0 , 10";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }


}

?>
