function ubigeo_provincia_select(ubigeo_coddep) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep
        }),
        beforeSend: function () {
            $('#cmb_ubigeo_codpro').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_ubigeo_codpro').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}
function ubigeo_distrito_select(ubigeo_coddep, ubigeo_codpro) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep,
            ubigeo_codpro: ubigeo_codpro
        }),
        beforeSend: function () {
            $('#cmb_ubigeo_coddis').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_ubigeo_coddis').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$('#txt_vendedor_tienda').autocomplete({
    minLength: 1,
    source: VISTA_URL+"vendedor/vendedor_complete_tienda.php"
	});
        
$(document).ready(function () {

//    $("#txt_vendedor_nom").autocomplete({
//        minLength: 3,
//        source: function (request, response) {
//            $.getJSON(
//                    VISTA_URL + "vendedor/vendedor_autocomplete.php",
//                    {term: request.term},
//                    response
//                    );
//        },
//        select: function (event, ui) {
//            $('#hdd_vendedor_id').val(ui.item.vendedor_id);
//            $('#txt_vendedor_nom').val(ui.item.vendedor_nom);
//        }
//    });



    $('#form_vendedor').validate({
        submitHandler: function () {

            $.ajax({
                type: "POST",
                url: VISTA_URL + "vendedor/vendedor_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_vendedor").serialize(),
                beforeSend: function () {
                    $('#vendedor_mensaje').show(400);
                    $('#btn_guardar_vendedor').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#vendedor_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#vendedor_mensaje').html(data.mensaje);
                        var vista = $('#vendedor_vista').val();
                        if (vista == 'vendedor')
                            vendedor_tabla();
                        if (vista == 'credito') {
                            llenar_datos_vendedor(data);
                        }
                        setTimeout(function () {
                            $('#modal_registro_vendedor').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#vendedor_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#vendedor_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_vendedor').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#vendedor_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#vendedor_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_vendedor_tip: {
                required: true
            }
            
        },
        messages: {
            txt_vendedor_tip: {
                required: "Seleccione el tipo, Natural o Jurídica"
            }
            
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});
