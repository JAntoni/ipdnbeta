<?php
	session_name("ipdnsac");
  session_start();
	require_once('../tareainventario/Tareainventario.class.php');
  $oTareainventario = new Tareainventario();
  require_once('../funciones/fechas.php');
  //$usuariogrupo_id = $_SESSION['usuariogrupo_id'];

  $garantia_id = intval($_POST['garantia_id']);
  $garantia_nom = $_POST['garantia_nom'];
  $lista = '';

  $result = $oTareainventario->filtrar_tareas_por('ti.tb_garantia_id', $garantia_id, 'INT');//tb_tarea_id

  	if($result['estado'] == 1){
            foreach ($result['data'] as $key => $value) {
                $lista .='
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($value['tb_tarea_fecreg']).'</span>
                              <h3 class="timeline-header"><a>'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</a></h3>
                              <div class="timeline-body">
                                <a class="btn pull-right" href="javascript:void(0)" onClick="Abrir_imagen('.$value["tb_tarea_id"].');"><b>Ver img</b></a>';
                            $lista .= $value['tb_tarea_det'].'  
                              </div>
                            </div>
                          </li>
                ';
            }
  	}
  $result = NULL;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditolinea_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">HISTORIAL DE LA GARANTIA: <?php echo strtolower($garantia_nom) ?> </h4>
      </div>
      <div class="modal-body">
        <ul class="timeline">
				  <?php echo $lista;?>
				</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--<script type="text/javascript" src="<?php //echo 'vista/creditolinea/creditolinea_timeline.js';?>"></script>-->