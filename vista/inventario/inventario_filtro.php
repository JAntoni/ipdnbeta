<form id="inventario_filtro" name="inventario_filtro">
    <div class="row">
        <div class="col-md-2">
            
                <label for="cmb_inven_est">Garantía está en:</label>
                <select name="cmb_inven_est" id="cmb_inven_est"  class="form-control input-sm">
                  <option value="0">TODOS</option>
                  <option value="1">ALMACEN</option>
                  <option value="2">TRANSITO</option>
                  <option value="3">EN OFICINA</option>
                </select>
        </div>
        
        <div class="col-md-2">
            <input type="hidden" id="hdd_almacen_id">
            <label for="cmb_almacen_id">Almacen:</label>
            <select class="form-control input-sm mayus" id="cmb_almacen_id" name="cmb_almacen_id">
                <?php include VISTA_URL.'almacen/almacen_select.php'; ?>
            </select>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label>N° Crédito:</label>
                <input type="text" name="txt_num_credito" id="txt_num_credito" class="form-control input-sm" onKeyUp="if(event.keyCode=='13'){verPaginainventario('1');}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Cliente:</label>
                <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm" onKeyUp="if(event.keyCode=='13'){verPaginainventario('1');}">
                <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" class="form-control input-sm">
				<input type="hidden" value="inventario" id="sufijo" name="sufijo"/>
				<input type="hidden" value="0" id="nroPagina" name="nroPagina"/>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div style="padding-top: 22px">
                    <button type="button" class="btn btn-sm btn-info" onclick="verPaginainventario('1')"><i class="fa fa-search"></i> Buscar</button>
                    <button type="button" class="btn btn-sm bg-maroon" onclick="imprimirCodigoBarrasPDFinventario()"><i class="fa fa-barcode"></i> Lista Cód. Barras</button>
                </div>
            </div>
        </div>
    </div>
    
</form>