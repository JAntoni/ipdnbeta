
var ALMACEN_ID = 0;
var garantia_esta = 0;

$(document).ready(function () {
    garantias_inventario_tabla(ALMACEN_ID);

    $("#txt_num_credito").focus();
    //$("#txt_cliente_nom").focus();
    $("#txt_cliente_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                VISTA_URL + "cliente/cliente_autocomplete.php",
                { term: request.term }, //
                response
            );
        },
        select: function (event, ui) {
            $('#hdd_cliente_id').val(ui.item.cliente_id);
            $('#txt_cliente_nom').val(ui.item.cliente_nom);
            //garantias_inventario_tabla(ALMACEN_ID);
            garantias_inventario_tabla();
            event.preventDefault();
            //$('#txt_cliente_nom').focus();
            $("#txt_num_credito").focus();
        }
    });
    if (window.addEventListener) {
        window.addEventListener("keydown", compruebaTecla, false);
    } else if (document.attachEvent) {
        document.attachEvent("onkeydown", compruebaTecla);
    }
}
);
function compruebaTecla(evt) {
    var tecla = evt.which || evt.keyCode;
    if (tecla == 27) {
        swal.close();
    }
}

//se usa
function garantias_inventario_tabla() {
    /* $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/garantias_inventario_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            estado: $('#cmb_inven_est').val(),
            alm_id: $('#cmb_almacen_id').val(), //esta vista es solo para los garantias que se encuentran en tienda, id de almacen tienda es 1
            cli_id: $('#hdd_cliente_id').val()
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_garantias_inventario_tabla').html(html);
        },
        complete: function (html) {
            //estilos_datatable();
        }
    }); */
    verPaginainventario('1');
}

//se usa 
function verPaginainventario(pag) {
    var pagina = pag;
    var cantidad = '';
    document.getElementById('nroPagina').value = pag;
    sufijo = $('#sufijo').val();
    if (document.getElementById('cboCantidadBusquedainventario')) {
        cantidad = document.getElementById('cboCantidadBusquedainventario').value;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/garantias_inventario_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            estado: $('#cmb_inven_est').val(),
            alm_id: $('#cmb_almacen_id').val(), //esta vista es solo para los garantias que se encuentran en tienda, id de almacen tienda es 1
            cre_id: $('#txt_num_credito').val(),
            cli_id: $('#hdd_cliente_id').val(),
            pagina: pagina,
            cantidad: cantidad
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_garantias_inventario_tabla').html(html);
        },
        complete: function (html) {

        }
    });

}

// en uso para paginado
function solo_numero(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//

// se usa
$('#cmb_inven_est, #cmb_almacen_id').change(function (event) {
    garantias_inventario_tabla();
});

// se usa
/* function estilos_datatable(){
  datatable_global = $('#tabla_inventario').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [1, 2, 3, 5, 6, 7, 8], orderable: false }
    ]
  });

  datatable_texto_filtrar();
} */
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}


//se usa
function garantialinea_timeline(garantia_id, garantia_nom) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/garantias_inventario_reg.php",
        async: true,
        dataType: "html",
        data: ({
            garantia_id: garantia_id,
            garantia_nom: garantia_nom
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_detalle_envios').html(data);
            $('#modal_creditolinea_timeline').modal('show');
            modal_height_auto('modal_creditolinea_timeline'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_creditolinea_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            $('#modal_mensaje').modal('hide');
        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            console.log(data.responseText);
        }
    });
}

function cancelar_envio(id) {
    var vista = 'oficina';
    if (ALMACEN_ID == 2) // 2 es almacén
        vista = 'almacen';

    $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/garantias_inventario_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'cancelar',
            vista: vista,
            gar_id: id
        }),
        beforeSend: function () {
            //      $('#msj_garantias').show(100);
            //      $('#msj_garantias').text("Cancelando envio de garantías...");
        },
        success: function (data) {
            swal_success('SISTEMA', data.msj, 3000);
            garantias_inventario_tabla(ALMACEN_ID);
        },
        complete: function (data) {

            if (data.statusText != 'success') {
                $('#msj_garantias').text(data.responseText);
                //        console.log(data);
            }

        }
    });
}

// se usa
//se usa
function enviar_garantia1(garantia_id, almacen_actual_id, almacen_actual_nom) {
    arreglo = JSON.parse($('#arreglo_almacenes').val());
    var alm_confirm;
    var alm_cancel;
    if (almacen_actual_id == 1) { //esta en boulevard
        alm_confirm = arreglo[1].tb_almacen_nom; // mall
        alm_cancel = arreglo[2].tb_almacen_nom; // almacen casa man
    } else if (almacen_actual_id == 2) { //esta en mall
        alm_confirm = arreglo[0].tb_almacen_nom; //boulevard
        alm_cancel = arreglo[2].tb_almacen_nom; // almacen casa man
    } else { //esta en almacen casa man
        alm_confirm = arreglo[0].tb_almacen_nom; //boulevard
        alm_cancel = arreglo[1].tb_almacen_nom; //mall
    };
    //alert(garantia_id+' - '+almacen_actual_id+' - '+ almacen_actual_nom); return;
    //alert(alm_cancel+' - '+alm_confirm); return;

    Swal.fire({
        title: 'SELECCIONE A QUÉ ALMACEN SE ENVIARÁ EL PRODUCTO',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: alm_confirm,
        showDenyButton: true,
        denyButtonText: alm_cancel,
        confirmButtonColor: '#3b5998',
        denyButtonColor: '#21ba45',
    }).then((result) => {

        var index_alm_destino, alm_destino;

        if (result.isConfirmed) {
            index_alm_destino = arreglo.findIndex(element => element.tb_almacen_nom === alm_confirm);

        } else if (result.isDenied) {
            index_alm_destino = arreglo.findIndex(element => element.tb_almacen_nom === alm_cancel);

        }

        alm_destino = arreglo[index_alm_destino];

        $.ajax({
            type: "POST",
            url: VISTA_URL + "tareainventario/tareainventario_controller.php",
            async: true,
            dataType: "html",
            data: ({
                accion: 'envio',
                alm_destino_id: alm_destino.tb_almacen_id, //aqui va el id del alm. destino
                alm_destino: alm_destino.tb_almacen_nom, //aqui va el nom del alm. destino
                garantia_id: garantia_id, //va el id de la garantia a colocar en transito
                almacen_actual_id: almacen_actual_id, //id del alm. actual
                almacen_actual_nom: almacen_actual_nom //nombre del alm. actual
            }),
            beforeSend: function () {
            },
            success: function (data) {
                Swal.fire(
                    'Confirmado!',
                    "Registraste el envío del producto " + data.valueOf() + " al " + alm_destino.tb_almacen_nom + ". Registra una foto de la garantia enviada.",
                    'success'
                )
                garantias_inventario_tabla();
            },
            complete: function () {
            },
            error: function (data) {
                Swal.fire(
                    'Error!',
                    "Error: " + data.valueOf(),
                    'error'
                )
            }
        });
    });
}

// no se usa creo
function cuotapago_menor_form(act, idf) {
    Swal.fire({
        title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-home"></i> Pago en Oficina!',
        showDenyButton: true,
        denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
        confirmButtonColor: '#3b5998',
        denyButtonColor: '#21ba45',
    }).then((result) => {

        if (result.isConfirmed) {
            Swal.fire(
                '¡Has elegido pagar en Oficina!',
                '',
                'success'
            )
            cuotapago_menor_oficina_form(act, idf);
        } else if (result.isDenied) {
            console.log("Has elegido pagar en banco");
        }
    });
}
// creo que no se usa
function cuotapago_menor_oficina_form(act, idf) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "pagocuota/pagocuota_menor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            cuo_id: idf,
            vista: 'vencimiento_tabla'
        }),
        beforeSend: function () {
            //            $('#h3_modal_title').text('Cargando Formulario');
            //            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            //          $('#modal_mensaje').modal('hide');
            $('#div_modal_pagocuota_menor_form').html(data);
            $('#modal_registro_pagocuotamenor').modal('show');

            modal_height_auto('modal_registro_pagocuotamenor'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            //            console.log(data);
        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
        }
    });
}
//se usa
function recibir_gar(gar_id, alm_id2, alm_nom2, alm_id1, alm_nom1, prod_nom) {

    Swal.fire({
        title: '¿DESEA CONFIRMAR EL RECIBO DEL PRODUCTO ' + gar_id + ' - ' + prod_nom + ' en ' + alm_nom2 + '?',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-check"></i> SI',
        showDenyButton: true,
        denyButtonText: '<i class="fa fa-band"></i> NO',
        confirmButtonColor: '#3b5998',
        denyButtonColor: '#DD3333',
    }).then((result) => {

        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "tareainventario/tareainventario_controller.php",
                async: true,
                dataType: "html",
                data: ({
                    accion: 'recibo',
                    alm_destino: alm_nom2, //aqui va el nombre del alm. que recibe
                    alm_destino_id: alm_id2, //id del almc que recibe
                    garantia_id: gar_id, //va el id de la garantia a colocar en transito
                    almacen_actual_id: alm_id1, //id del alm. de donde viene
                    almacen_actual_nom: alm_nom1 //nombre del alm. de donde viene
                }),
                beforeSend: function () {
                },
                success: function (data) {
                    Swal.fire(
                        'Confirmado!',
                        "Registraste el recibo del producto " + data.valueOf() + ". Registra una foto de la garantia recibida.",
                        'success'
                    )
                    garantias_inventario_tabla();
                },
                complete: function () {
                },
                error: function (data) {
                    Swal.fire(
                        'Error!',
                        "Error: " + data.valueOf(),
                        'error'
                    )
                }
            });
        }
    });
}
//se usa
function cancelar_envio1(gar_id, alm_id2, alm_nom2, alm_id1, alm_nom1, prod_nom) {

    Swal.fire({
        title: '¿DESEA CANCELAR EL RECIBO DEL PRODUCTO ' + gar_id + ' - ' + prod_nom + ' en ' + alm_nom2 + '?',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-check"></i> SI',
        showDenyButton: true,
        denyButtonText: '<i class="fa fa-band"></i> NO',
        confirmButtonColor: '#3b5998',
        denyButtonColor: '#DD3333',
    }).then((result) => {

        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "tareainventario/tareainventario_controller.php",
                async: true,
                dataType: "html",
                data: ({
                    accion: 'cancela',
                    alm_destino: alm_nom2, //aqui va el nombre del alm. donde iba a enviarse
                    alm_destino_id: alm_id2, //id del almc donde iba a enviarse
                    garantia_id: gar_id, //va el id de la garantia a cancelar envio
                    almacen_actual_id: alm_id1, //id del alm. donde se queda
                    almacen_actual_nom: alm_nom1 //nombre del alm. donde se queda
                }),
                beforeSend: function () {
                },
                success: function (data) {
                    Swal.fire(
                        'Confirmado!',
                        "Cancelaste el envío del producto " + data.valueOf() + " al " + alm_nom2,
                        'success'
                    )
                    garantias_inventario_tabla();
                },
                complete: function (data) {
                    //console.log('estoy llegando al controler, y me retorna: '+data.valueOf());
                },
                error: function (data) {
                    Swal.fire(
                        'Error!',
                        "Error: " + data.valueOf(),
                        'error'
                    )
                }
            });
        }
    });

}

// se usa
function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'inventario', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
            //                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

//se usa
function upload_galeria(tareainventario_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'inventario', //nombre de la tabla a relacionar
            modulo_id: tareainventario_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

// se usa
function Abrir_imagen(tareainventario_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/inventario_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_cajaoperacion_img').modal('show');
            modal_hidden_bs_modal('modal_cajaoperacion_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(tareainventario_id);
        },
        complete: function (html) {
        }
    });
}

/* GERSON (03-02-23) */
function imprimirCodigoBarrasPDFinventario() {
    var pagina = document.getElementById('nroPagina').value;
    var cantidad = '';
    //document.getElementById('nroPagina').value=pag;
    sufijo = $('#sufijo').val();
    if (document.getElementById('cboCantidadBusquedainventario')) {
        cantidad = document.getElementById('cboCantidadBusquedainventario').value;
    }
    estado = $('#cmb_inven_est').val();
    alm_id = $('#cmb_almacen_id').val();
    cre_id = $('#txt_num_credito').val();
    cliente_id = $('#hdd_cliente_id').val();
    cliente = $('#txt_cliente_nom').val();

    filtro = 'estado=' + estado + '&alm_id=' + alm_id + '&cre_id=' + cre_id + '&cliente_id=' + cliente_id + '&cliente=' + cliente;

    extra = '';
    if (document.getElementById('cboCantidadBusquedainventario')) {
        extra = '&pagina=' + pagina + '&cantidad=' + document.getElementById('cboCantidadBusquedainventario').value;
    }

    window.open("vista/inventario/pruebaBarras.php?" + filtro + extra, "_blank");
}

function generarCodigoBarras(mod, id) {

    filtro = 'modulo=' + mod + '&id=' + id;

    window.open("vista/inventario/barcodeCredito.php?" + filtro, "_blank");
}
/*  */

/* GERSON (18-04-23) */
function creditomenor_form(usuario_act, creditomenor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditomenor/creditomenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditomenor_id: creditomenor_id,
            vista: 'creditomenor'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_creditomenor_form').html(data);
                $('#modal_registro_creditomenor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_registro_creditomenor', 95);
                modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else {
                //llamar al formulario de solicitar permiso
                var modulo = 'creditomenor';
                var div = 'div_modal_creditomenor_form';
                permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', data.responseText)
            console.log(data);
        }
    });
}

function carousel(modulo_nom, modulo_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "templates/carousel.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: modulo_nom,
            modulo_id: modulo_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Galería');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            $('#div_modal_carousel').html(data);
            $('#modal_carousel_galeria').modal('show');

            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
/*  */