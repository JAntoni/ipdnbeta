<?php
error_reporting(0);

define('FPDF_FONTPATH', 'font/');

require_once('../../static/fpdf/code128.php');

//include('../../static/fpdf/barcode.php');
require_once '../../core/usuario_sesion.php';
require_once ('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');

if($_GET['cantidad']==NULL || $_GET['cantidad']==''){
	$pagina=1;
	$inicio=0;
	$cantidad=12;
}else{
	$pagina=$_GET['pagina'];
	$cantidad=$_GET['cantidad'];
	$inicio=($pagina-1)*$cantidad;
}

$estado = intval($_GET['estado']);
$alm_id = intval($_GET['alm_id']);
$cre_id = intval($_GET['cre_id']);
$cliente_id = intval($_GET['cliente_id']);
$cliente = $_GET['cliente'];

$fecha_hoy = date('d-m-Y');

//$dts = $oGarantia->mostrar_todos_filtro_codigobarras($estado, $alm_id, $cliente_id, $cliente);
$dts = $oGarantia->mostrar_todos_filtro_paginado($estado, $alm_id, $cre_id, $cliente_id, $inicio, $cantidad);
$dts_num = $oGarantia->mostrar_todos_filtro_paginado($estado, $alm_id, $cre_id, $cliente_id, $inicio, $cantidad, true);
$total_pag=ceil($dts_num['data']/$cantidad);

    foreach ($dts['data']as $key => $dt) {
        $code = '';
        //$code = str_pad($dt['tb_garantia_id'], 7, '0', STR_PAD_LEFT)."-".str_pad($dt['tb_credito_id'], 7, '0', STR_PAD_LEFT);
        $code = "CM-".$dt['tb_credito_id'];

        $barcodeText = $code;
        $barcodeType='code128';
        $barcodeDisplay='horizontal';
        $barcodeSize=20;
        $printText=true;
        if($barcodeText != '') {
       
            echo "<div align='center' class='form-group'>";
                echo "<span style='font-size: 13px;'>".substr($dt['tb_garantia_pro'], 0,17)."</span>";
                echo "<div class='form-group'>";
                    echo '<img style="height: 50px; width: 180px;" class="barcode" alt="'.$barcodeText.'" src="../../static/fpdf/barcode.php?text='.$barcodeText.'&codetype='.$barcodeType.'&orientation='.$barcodeDisplay.'&size='.$barcodeSize.'&print='.$printText.'"/>';
                echo '</div>';
                echo "<span style='font-size: 13px;'>".$code."</span>";
            echo '</div>';
            
        }else{
            echo '<div class="alert alert-danger">Introduzca el nombre del producto para generar el código</div>';
        }
    }
    ?>
