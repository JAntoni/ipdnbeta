<?php

error_reporting(0);

define('FPDF_FONTPATH', 'font/');

require_once('../../static/fpdf/code128.php');
//require_once('../../static/fpdf/EAN13.php');

//include('../../static/fpdf/barcode.php');
require_once '../../core/usuario_sesion.php';
require_once ('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');

$estado = intval($_GET['estado']);
$alm_id = intval($_GET['alm_id']);
$cliente = $_GET['cliente'];

$fecha_hoy = date('d-m-Y');

$dts = $oGarantia->mostrar_todos_filtro_codigobarras($estado, $alm_id, 0, $cliente);

//$pdf = new PDF_Code128();
$pdf = new PDF_Code128('P','mm',array(80,220), true, 'UTF-8', false);
//$pdf=new PDF_EAN13();

/* $pdf->AddPage();
$pdf->SetFont('Arial','',10); */

$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
//$pdf->SetFillColor(220,220,220);
$alto=20;
$pdf->Ln(8);
/* $pdf->Cell(135,6,"REPORTE DE PROYECTOS",0,1,'C');

$pdf->SetFont('Arial','B',10);

$pdf->Ln();
$pdf->SetFont('Arial','B',8);
$pdf->Cell(50,8,"PROYECTO",1,0,'C',1); */

$i=5;
$pdf->SetFont('Arial','',7);
$pdf->Ln();
$pag_anterior=$pdf->PageNo();
$pdf->SetFillColor(0,0,0);

foreach ($dts['data']as $key => $dt) {

    $pdf->Cell(8,5*2,'',0,0,'C');

		if($pag_anterior!=$pdf->PageNo()){
			$pag_anterior=$pdf->PageNo();
            $pdf->Ln(12);
            $pdf->Ln();

		}else{
			$pdf->Cell(-8,5,'',0,0,'C');

		}

    if (intval($dt['tb_garantiatipo_id']) != 5) {

        

        $code = '';
        $code = str_pad($dt['tb_garantia_id'], 7, '0', STR_PAD_LEFT)."-".str_pad($dt['tb_credito_id'], 7, '0', STR_PAD_LEFT);

        $pdf->SetFont('Arial','',7);

        $pdf->SetXY(10,$i-5);
        $pdf->Write(5,$dt['tb_garantia_pro']);
        $pdf->Code128(10,$i,$code,50,11);
        $pdf->SetFont('Arial','',7);
        $pdf->SetXY(23,$i+11);
        $pdf->Write(5,$code);

        // FORMATO A4 VERTICAL
        /* $code = '';
        $code = str_pad($dt['tb_garantia_id'], 7, '0', STR_PAD_LEFT)."-".str_pad($dt['tb_credito_id'], 7, '0', STR_PAD_LEFT);

        $pdf->SetFont('Arial','',5);

        //$pdf->Cell(50,$alto,$dt['tb_garantia_pro'],0,0,'L');
		//$pdf->Cell(50,$alto,$pdf->EAN13(80,$i,$code),0,0,'L');
        
        $pdf->SetXY(75,$i-5);
        $pdf->Write(5,$dt['tb_garantia_pro']);
        $pdf->Code128(75,$i,$code,40,8);
        //$pdf->EAN13(80,$i,$code,$dt['tb_garantia_pro']);

        $pdf->SetFont('Arial','',7);
        $pdf->SetXY(75,$i+10);
        $pdf->Write(5,$code); */
        $i = $i + 28;

    }
}

/* $pdf->SetAutoPageBreak('auto',2); 
$pdf->SetDisplayMode(75); */
$pdf->Output();

?>