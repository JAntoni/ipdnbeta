<?php
error_reporting(0);

define('FPDF_FONTPATH', 'font/');

require_once('../../static/fpdf/code128.php');

//include('../../static/fpdf/barcode.php');
require_once '../../core/usuario_sesion.php';
require_once ('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');

$modulo = $_GET['modulo'];
$id = intval($_GET['id']);

if($modulo == 'gestioninventario' || $modulo == 'garantia'){

    $garantia = $oGarantia->mostrarUno($id);
    $credito_id = $garantia['data']['tb_credito_id'];
    $garantia_nom = $garantia['data']['tb_garantia_pro'];

    
    $code = '';
    $code = "CM-".$credito_id;

    $barcodeText = $code;
    $barcodeType='code128';
    $barcodeDisplay='horizontal';
    $barcodeSize=20;
    $printText=true;
    if($barcodeText != '') {

        echo "<div align='center' class='form-group'>";
            echo "<span style='font-size: 10px;'>".substr($garantia_nom, 0,17)."</span>";
            echo "<div class='form-group'>";
                echo '<img style="height: 50px; width: 180px;" class="barcode" alt="'.$barcodeText.'" src="../../static/fpdf/barcode.php?text='.$barcodeText.'&codetype='.$barcodeType.'&orientation='.$barcodeDisplay.'&size='.$barcodeSize.'&print='.$printText.'"/>';
            echo '</div>';
            echo "<span style='font-size: 12px;'>".$code."</span>";
        echo '</div>';
            
    }else{
        echo '<div class="alert alert-danger">Introduzca el nombre del producto para generar el código</div>';
    }

}elseif($modulo == 'creditomenor'){
    
    $garantias = $oGarantia->listar_garantias($id);
  
    if ($garantias['estado'] == 1) {
        foreach ($garantias['data'] as $key => $value):

            $code = '';
            $code = "CM-".$value['tb_credito_id'];

            $barcodeText = $code;
            $barcodeType='code128';
            $barcodeDisplay='horizontal';
            $barcodeSize=20;
            $printText=true;
            if($barcodeText != '') {

                echo "<div align='center' class='form-group'>";
                    echo "<span style='font-size: 13px;'>".substr($value['tb_garantia_pro'], 0,15)."</span>";
                    echo "<div class='form-group'>";
                        echo '<img style="height: 50px; width: 180px;" class="barcode" alt="'.$barcodeText.'" src="../../static/fpdf/barcode.php?text='.$barcodeText.'&codetype='.$barcodeType.'&orientation='.$barcodeDisplay.'&size='.$barcodeSize.'&print='.$printText.'"/>';
                    echo '</div>';
                    echo "<span style='font-size: 13px;'>".$code."</span>";
                echo '</div>';
                    
            }else{
                echo '<div class="alert alert-danger">Introduzca el nombre del producto para generar el código</div>';
            }

        endforeach;
    }

}

?>
