<form id="inventario_filtro" name="inventario_filtro">
    <div class="row">
        <div class="col-md-2">
            
                <label for="cmb_inven_est">Garantía está en:</label>
                <select name="cmb_inven_est" id="cmb_inven_est"  class="form-control input-sm">
                  <option value="0">TODOS</option>
                  <option value="1">ALMACEN</option>
                  <option value="2">TRANSITO</option>
                  <option value="3">EN OFICINA</option>
                </select>
        </div>
        
        <div class="col-md-2">
            <input type="hidden" id="hdd_almacen_id">
            <label for="cmb_almacen_id">Almacen:</label>
            <select class="form-control input-sm mayus" id="cmb_almacen_id" name="cmb_almacen_id">
                <?php include VISTA_URL.'almacen/almacen_select.php'; ?>
            </select>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Cliente:</label>
                <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm">
                <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" class="form-control input-sm">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div style="padding-top: 22px">
                    <button type="button" class="btn btn-sm bg-maroon" onclick="imprimirCodigoBarrasPDF()"><i class="fa fa-barcode"></i> Cód. Barras</button>
                </div>
            </div>
        </div>
    </div>
    
</form>