<?php
require_once '../../core/usuario_sesion.php';
require_once ('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');
require_once('../templates/paginado.php');
require_once ('../tareainventario/Tareainventario.class.php');
$oTareainventario = new TareaInventario();
require_once('../almacen/Almacen.class.php');
$oAlmacen = new Almacen();
$r = $oAlmacen->mostrarTodos1()['data'];

if($_POST['cantidad']==NULL || $_POST['cantidad']==''){
	$pagina=1;
	$inicio=0;
	$cantidad=50;
}else{
	$pagina=$_POST['pagina'];
	$cantidad=$_POST['cantidad'];
	$inicio=($pagina-1)*$cantidad;
}

$estado = intval($_POST['estado']);
$alm_id = intval($_POST['alm_id']);
$cre_id = intval($_POST['cre_id']);
$cli_id = $_POST['cli_id'];



$fecha_hoy = date('d-m-Y');

?>
<input type="hidden" id="usuario_mos" value="<?php echo $_SESSION['usuario_mos']; ?>">
<input type="hidden" id="almacen_id" value="<?php echo $alm_id; ?>">
<input type="hidden" id="arreglo_almacenes" value='<?php echo json_encode($r); ?>'>

<?php 
                                  
//$dts = $oGarantia->mostrar_todos_filtro($estado, $alm_id, $cli_id);
$dts = $oGarantia->mostrar_todos_filtro_paginado($estado, $alm_id, $cre_id, $cli_id, $inicio, $cantidad);
$dts_num = $oGarantia->mostrar_todos_filtro_paginado($estado, $alm_id, $cre_id, $cli_id, $inicio, $cantidad, true);
$total_pag=ceil($dts_num['data']/$cantidad);

if ($dts['estado'] == 1): ?>
    <table id="tabla_inventario" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">PRODUCTO ID</th>
                <th id="tabla_cabecera_fila">CREDITO ID</th>
                <th id="tabla_cabecera_fila">CLIENTE</th>
                <th id="tabla_cabecera_fila">CANT.</th>
                <th id="tabla_cabecera_fila">PRODUCTO</th>
                <th id="tabla_cabecera_fila">DETALLE</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th id="tabla_cabecera_fila">EST. CRÉDITO</th>
                <th id="tabla_cabecera_fila">ALMACEN</th>
                <th  id="tabla_cabecera_fila" width="12%">OPCIONES</th>
            </tr>
        </thead>  
        <tbody> <?php
            foreach ($dts['data'] as $key => $dt) {
                $dt['tb_garantia_pro'] = str_replace("'", "", $dt['tb_garantia_pro']);
                $dt['tb_garantia_pro'] = str_replace('"', '', $dt['tb_garantia_pro']);
                if (intval($dt['tb_garantiatipo_id']) != 5) {
                    ?>
                    <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_id'] ?></td>
                        <td id="tabla_fila"><?php echo 'CM-' . str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_cliente_nom'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_can'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_pro'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_det'] ?></td>
                        <td id="tabla_fila">
                            <?php
                            if ($dt['tb_garantia_almest'] == 1){
                                $estado = '<strong style="color: green;">ALMACENADO</strong>';
                            } elseif ($dt['tb_garantia_almest'] == 2) { //|| $dt['tb_garantia_almest'] == 3
                                $estado = '<strong style="color: red;">TRANSITO</strong>';
                            }  
                            elseif($dt['tb_garantia_almest'] == 0){
                                $estado = '<strong style="color: orange;">EN OFICINA. CREDITO PENDIENTE DE DESEMBOLSO.</strong>';
                            }
                            echo $estado;
                            ?>
                        </td>
                        <!-- GERSON (18-04-23) -->
                        <?php 
                        $est_credito = '';
                        if($dt['tb_credito_est']!=null || $dt['tb_credito_est']>0){
                            if($dt['tb_credito_est']==1){
                                $est_credito = 'PENDIENTE';
                            }elseif($dt['tb_credito_est']==2){
                                $est_credito = 'APROBADO';
                            }elseif($dt['tb_credito_est']==3){
                                $est_credito = 'VIGENTE';
                            }elseif($dt['tb_credito_est']==4){
                                $est_credito = 'LIQUIDADO';
                            }elseif($dt['tb_credito_est']==5){
                                $est_credito = 'REMATE';
                            }elseif($dt['tb_credito_est']==6){
                                $est_credito = 'VENDIDO';
                            }elseif($dt['tb_credito_est']==7){
                                $est_credito = 'LIQUIDADO PENDIENTE DE ENVÍO';
                            }
                        }
                        ?>
                        <td id="tabla_fila"><?php echo $est_credito; ?></td>
                        <!--  -->
                        <td id="tabla_fila"><?php
                        $res = $oTareainventario->ultimo_transito($dt['tb_garantia_id']);
                        $existe_tarea = 0;
                        if($dt['tb_garantia_almest'] == 2){
                            if ($res['estado']==1){
                                $existe_tarea = 1;
                               echo "Del ".$res['data'][0]["almacen1"]. " al ".$res['data'][0]["almacen2"];
                            }
                        } else{
                            echo $dt['tb_almacen_nom'];
                        }
                         ?></td>
                        <td id="tabla_fila">
                            <!-- <a class="btn bg-maroon btn-xs" onClick="generarCodigoBarras(<?php echo "'creditomenor', 10" ?>)"><i class="fa fa-barcode"></i></a> -->
                            <a class="btn bg-maroon btn-xs" onClick="generarCodigoBarras(<?php echo "'gestioninventario', {$dt['tb_garantia_id']}" ?>)"><i class="fa fa-barcode"></i> Cód. Barra</a>
                            <a class="btn btn-info btn-xs" title="Ver" onclick="creditomenor_form(<?php echo "'L', ".$dt['tb_credito_id'];?>)"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-warning btn-xs" onClick="garantialinea_timeline(<?php echo $dt['tb_garantia_id'].", '{$dt['tb_garantia_pro']}'" ?>)">Hist.</a>
                        <?php if((($_SESSION['usuarioperfil_id'] == 8 || intval($_SESSION['usuario_id']) == 16 || intval($_SESSION['usuario_id']) == 11) && $dt['tb_almacen_id']==3) || ($_SESSION['empresa_id'] == $dt['tb_almacen_id'] && $_SESSION['usuarioperfil_id'] != 8)){
                                if ($dt['tb_garantia_almest'] == 1) { ?>
                                    <a class="btn btn-primary btn-xs" onClick="enviar_garantia1(<?php echo $dt['tb_garantia_id'].", {$dt['tb_almacen_id']}, '{$dt['tb_almacen_nom']}'" ?>)">Enviar</a>
                            <?php }
                                elseif($dt['tb_garantia_almest'] == 2 && $_SESSION['usuario_id']== $res['data'][0]["tb_tarea_usureg"]) { ?>
                                <a class="btn btn-danger btn-xs" onClick="cancelar_envio1(<?php echo "{$dt['tb_garantia_id']}, {$res['data'][0]['tb_almacen_id2']}, '{$res['data'][0]['almacen2']}', {$res['data'][0]['tb_almacen_id1']}, '{$res['data'][0]['almacen1']}', '{$dt['tb_garantia_pro']}'" ?>)">Canc. envío</a>
                            <?php }
                            }
                        if($dt['tb_garantia_almest'] == 2 && (($res['data'][0]["tb_almacen_id2"] == $_SESSION['empresa_id'] && $_SESSION['usuarioperfil_id'] != 8) || (($_SESSION['usuarioperfil_id'] == 8 || $_SESSION['usuario_id'] == '11' || $_SESSION['usuario_id'] == '16') && $res['data'][0]["tb_almacen_id2"]==3))){ ?>
                                <a class="btn btn-success btn-xs" onClick="recibir_gar(<?php echo $dt['tb_garantia_id'].", {$res['data'][0]['tb_almacen_id2']}, '{$res['data'][0]['almacen2']}', {$res['data'][0]['tb_almacen_id1']}, '{$res['data'][0]['almacen1']}', '{$dt['tb_garantia_pro']}'" ?>)">Recibir</a>
                            <?php }                                                             //$dt['tb_garantia_id'].", ".$res['data'][0]['tb_almacen_id2'].", '".$res['data'][0]['almacen2']."', ".$res['data'][0]['tb_almacen_id1'].", '".$res['data'][0]['almacen1']."'"
                            if($dt['tb_garantia_almest'] > 0 && $existe_tarea == 1){ ?>
                                <?php if ($fecha_hoy == mostrar_fecha($res['data'][0]['tb_tarea_fecreg']) && $_SESSION['usuario_id']== $res['data'][0]["tb_tarea_usureg"]){ //.$res['data'][0]["tb_tarea_id"].
                                    echo '<div class="btn-group"> 
                                    <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown" title="Opciones de Img"><span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a class="btn" href="javascript:void(0)" onclick="upload_form(\'I\', '.$res['data'][0]["tb_tarea_id"].')"><b>Subir Img</b></a></li>
                                        <li><a class="btn" href="javascript:void(0)" onclick="Abrir_imagen('.$res['data'][0]["tb_tarea_id"].')"><b>Ver Img</b></a></li>
                                    </ul>
                                </div>';
                                } ?>
                            <?php } ?>
                        </td>
                    </tr> <?php
                    $res = null;
                                }
            } //FIN DE WHILE
            ?>
        </tbody>

    </table>
    <div class="box-footer clearfix">
        <?php 
        $fin=$inicio+$cantidad;
        echo getFooterCase($pagina,$total_pag,$dts_num['data'],$cantidad,($inicio+1),$fin,'inventario');
        ?>
    </div>
<?php endif; ?>

