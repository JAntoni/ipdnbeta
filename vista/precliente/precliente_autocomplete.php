<?php
require_once ('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();

class ElementoAutocompletar{
    var $value;
    var $label;
    var $precliente_id;
    var $cliente_id;
    var $precliente_numdoc;
    var $precliente_nombres;
    var $precliente_numcel;
    var $mediocom_id;
    var $precliente_tipodoc;

    function __construct($label, $value, $precliente_id, $cliente_id, $precliente_numdoc, $precliente_nom, $precliente_numcel, $mediocom_id, $precliente_tipodoc){
        $this->label = $label;
        $this->value = $value;
        $this->precliente_id = $precliente_id;
        $this->cliente_id = $cliente_id;
        $this->precliente_numdoc = $precliente_numdoc;
        $this->precliente_nombres = $precliente_nom;
        $this->precliente_numcel = $precliente_numcel;
        $this->mediocom_id = $mediocom_id;
        $this->precliente_tipodoc = $precliente_tipodoc;
    }
}

$datoBuscar = $_GET["term"];

//datos busqueda
    $where[0]['column_name'] = 'tb_precliente_xac';
    $where[0]['param0'] = 1;
    $where[0]['datatype'] = 'INT';

    $where[1]['conector'] = 'AND';
    $where[1]['column_name'] = '(tb_precliente_numdoc';
    $where[1]['param1'] = "LIKE '%$datoBuscar%'";

    $where[2]['conector'] = 'OR';
    $where[2]['column_name'] = 'tb_precliente_nombres';
    $where[2]['param2'] = "LIKE '%$datoBuscar%')";

    $inner[0]['alias_columnasparaver'] = 'mc.tb_mediocom_nom';
    $inner[0]['tipo_union'] = 'LEFT';
    $inner[0]['tabla_alias'] = 'tb_mediocom mc';
    $inner[0]['columna_enlace'] = 'pcl.tb_mediocom_id';
    $inner[0]['alias_columnaPK'] = 'mc.tb_mediocom_id';

    $otros['limit']['value'] = '10';
//
$result = $oPrecliente->listar_todos($where, $inner, $otros);

$arrayElementos = array();
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        array_push(
            $arrayElementos,
            new ElementoAutocompletar(
                $value['tb_precliente_numdoc'] . '-' . $value['tb_precliente_nombres'],
                $value['tb_precliente_numdoc'] . ' - ' . $value['tb_precliente_nombres']. ' - '. $value['tb_precliente_numcel']. ' - '. $value['tb_mediocom_nom'],
                $value['tb_precliente_id'],
                $value['tb_cliente_id'],
                $value['tb_precliente_numdoc'],
                $value['tb_precliente_nombres'],
                $value['tb_precliente_numcel'],
                $value['tb_mediocom_id'],
                $value['tb_precliente_tipodoc']
            )
        );
    }
} else {
    //ahora buscar en tabla cliente
    $result = null;
    $result = $oCliente->cliente_autocomplete($datoBuscar, 0);

    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            array_push(
                $arrayElementos,
                new ElementoAutocompletar(
                    $value['tb_cliente_doc'] . '-' . $value['tb_cliente_nom'],
                    $value['tb_cliente_doc'] . ' - ' . $value['tb_cliente_nom']. ' - '. $value['tb_cliente_tel'] . ' - ' . $value['tb_mediocom_nom'],
                    0,
                    $value['tb_cliente_id'],
                    $value['tb_cliente_doc'],
                    $value['tb_cliente_nom'],
                    $value['tb_cliente_tel'],
                    $value['tb_mediocom_id'],
                    1
                )
            );
        }
    }
}
print_r(json_encode($arrayElementos));
unset($result);