<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
$listo = 1;

$usuario_action = $_POST['action'];
$titulo = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar precliente';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar precliente';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar precliente';
} elseif ($usuario_action == 'L') {
    $titulo = 'Precliente registrado';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action = devuelve_nombre_usuario_action($usuario_action);

$precliente_id = intval($_POST["precliente_id"]);

if ($listo == 1) {
    if (!empty($precliente_id)) {
        $where[0]['column_name'] = 'tb_precliente_id';
        $where[0]['param0'] = $precliente_id;
        $where[0]['datatype'] = 'INT';

        $res = $oPrecliente->listar_todos($where, array(), array());

        if ($res['estado'] == 1) {
            $precliente = $res['data'][0];
            $cliente_id = $precliente['tb_cliente_id'];
            $nro_doc = $precliente['tb_precliente_numdoc'];
            $nombres = $precliente['tb_precliente_nombres'];
            $celular = $precliente['tb_precliente_numcel'];
        }
    }
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_precliente_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_precliente" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action_precliente" id="action_precliente" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $_POST['vista']?>">
                        <input type="hidden" name="hdd_precliente_id" id="hdd_precliente_id" value="<?php echo $precliente_id; ?>">
                        <input type="hidden" name="hdd_precliente_reg" id="hdd_precliente_reg" value='<?php echo json_encode($precliente); ?>'>
                        <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id?>">

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_precliente_nrodocumento">N° Documento:</label>
                                    <input type="text" name="txt_precliente_nrodocumento" id="txt_precliente_nrodocumento" class="input-sm form-control mayus" placeholder="N° documento" value="<?php echo $nro_doc;?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_precliente_nombres">Nombres y Apellidos:</label>
                                    <input type="text" name="txt_precliente_nombres" id="txt_precliente_nombres" class="form-control input-sm mayus" placeholder="nombres" value="<?php echo $nombres;?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <input type="hidden" name="hdd_cliente_tel_orig" id="hdd_cliente_tel_orig" value="<?php echo $celular;?>">
                                <div class="col-md-12">
                                    <label for="txt_precliente_numcel">Numero Celular:</label>
                                    <input type="text" name="txt_precliente_numcel" id="txt_precliente_numcel" class="form-control input-sm mayus" placeholder="numero celular" value="<?php echo $celular;?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <button id="btn_limpiarcliente" type="button" onclick="limpiar_cliente()" class="btn btn-sm btn-warning">Limpiar campos</button>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="precliente_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_precliente">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_precliente">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/precliente/precliente_form.js?ver=61785668'; ?>"></script>