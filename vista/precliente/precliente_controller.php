<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
$oPrecliente->precliente_usumod = $_SESSION['usuario_id'];

$data['estado'] = 0;
$data['mensaje'] = '';

$action = $_POST['action_precliente'];

$cliente_id = $_POST['hdd_cliente_id'];
$nrodocumento = trim(strtoupper($_POST['txt_precliente_nrodocumento']));
$nombres = trim(strtoupper($_POST['txt_precliente_nombres']));
$telefono = trim($_POST['txt_precliente_numcel']);
$tipo_doc = strlen($nrodocumento) > 8 ? '4' : '1';
$precliente_reg = (array) json_decode($_POST['hdd_precliente_reg']);

$oPrecliente->precliente_numdoc = $nrodocumento;
$oPrecliente->precliente_nombres = $nombres;
$oPrecliente->precliente_numcel = $telefono;

if ($action == 'insertar') {
    if (!empty($cliente_id)) {
        $telefono_original = $_POST['hdd_cliente_tel_orig'];
        if ($telefono != $telefono_original) {
            //se debe actualizar el numero telefono del cliente
            $oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');
        }
    }
    
    $oPrecliente->precliente_usureg = $_SESSION['usuario_id'];
    $oPrecliente->cliente_id = $cliente_id;
    $oPrecliente->precliente_tipodoc = $tipo_doc;

    $res = $oPrecliente->insertar();

    if ($res['estado'] == 1) {
        $data['estado'] = 1;
        $data['mensaje'] = $res['mensaje'];
        $oPrecliente->precliente_id = $res['nuevo'];
        $data['registro'] = (array) $oPrecliente;
        $data['cambios'] = 1;
    }
    unset($res);
} elseif ($action == 'modificar') {
    $precliente_id = $_POST['hdd_precliente_id'];
    $oPrecliente->precliente_id = $precliente_id;

    if (intval($precliente_reg['tb_cliente_id']) > 0) {
        $telefono_original = $_POST['hdd_cliente_tel_orig'];
        if ($telefono != $telefono_original) {
            $res = $oPrecliente->modificar_campo($precliente_id, 'tb_precliente_numcel', $telefono, 'STR');
            
            if ($res == 1) {
                $oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');

                $data['estado'] = 1;
                $data['cambios'] = 1;
                $data['mensaje'] = 'Modificado correctamente';
                $data['registro'] = (array) $oPrecliente;
            } else {
                $data['mensaje'] = 'Error al modificar';
            }
        } else {
            $data['estado'] = 1;
            $data['cambios'] = 0;
            $data['mensaje'] = 'Sin cambios';
        }
    } else {
        $igual[0] = boolval($tipo_doc == $precliente_reg['tb_precliente_tipodoc']);
        $igual[1] = boolval($oPrecliente->precliente_numdoc == $precliente_reg['tb_precliente_numdoc']);
        $igual[2] = boolval($oPrecliente->precliente_nombres == $precliente_reg['tb_precliente_nombres']);
        $igual[3] = boolval($oPrecliente->precliente_numcel == $precliente_reg['tb_precliente_numcel']);
        $igual[4] = boolval(intval($cliente_id) == 0);

        $son_iguales = true;
        $i = 0;
        while ($i < count($igual)) {
            if (!$igual[$i]) {
                $son_iguales = false;
            }
            $i++;
        }
        $i = 0;

        if ($son_iguales) {
            $data['estado'] = 1;
            $data['cambios'] = 0;
            $data['mensaje'] = "No se realizó ninguna modificacion";
        } else {
            $oPrecliente->precliente_tipodoc = $tipo_doc;
            if (!empty($cliente_id)) {
                $oPrecliente->cliente_id = $cliente_id;
            }

            $res = $oPrecliente->modificar();
            if ($res == 1) {
                $data['estado'] = 1;
                $data['cambios'] = 1;
                $data['mensaje'] = 'Modificado correctamente';
                $data['registro'] = (array) $oPrecliente;

                if (intval($cliente_id) > 0 && $telefono != $_POST['hdd_cliente_tel_orig']) {
                    $oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');
                }
            } else {
                $data['mensaje'] = 'Error al modificar';
            }
        }
    }
}

echo json_encode($data);
?>