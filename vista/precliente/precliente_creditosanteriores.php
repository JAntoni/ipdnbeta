<?php
require_once('../precredito/Precredito.class.php');
$oPcr = new Precredito();
require_once('../creditomenor/Creditomenor.class.php');
$oCm = new Creditomenor();
require_once('../garantia/Garantia.class.php');
$oGar = new Garantia();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$precli_id = intval($_POST['precli_id']);
$cli_id = intval($_POST['cli_id']);
$action = $_POST['action'];
$precredito_id = intval($_POST['pcr_id']);

$lista['precred'] = 
    '<div class="box box-primary shadow" style="padding: 5px 10px;">'.
        '<div class="box-header with-border" style="padding: 5px 3px;">'.
            '<h3 class="box-title">Solicitudes precredito anteriores</h3>'.
        '</div>'.

        '<div class="box-body" style="padding: 5px 5px;">'.
            '<ul class="products-list product-list-in-box">'.
                'Sin datos'.
            '</ul>'.
        '</div>'.
    '</div>';
//
$lista['cred'] = 
    '<div class="box box-primary shadow" style="padding: 5px 10px;">'.
        '<div class="box-header with-border" style="padding: 5px 3px;">'.
            '<h3 class="box-title">Creditos anteriores</h3>'.
        '</div>'.

        '<div class="box-body" style="padding: 5px 5px;">'.
            '<ul class="products-list product-list-in-box">'.
                'Sin datos'.
            '</ul>'.
        '</div>'.
    '</div>';
//
$creditos_a_excluir = array();

if (!empty($precli_id)) {
    //buscar si existe un precred con xac: 1, que tenga a el id de ese precli, y que no tenga estado 4,5,9,10
        $wh[0]['column_name'] = 'pcr.tb_precred_xac';
        $wh[0]['param0'] = 1;
        $wh[0]['datatype'] = 'INT';

        $wh[1]['conector'] = 'AND';
        $wh[1]['column_name'] = 'pcr.tb_precliente_id';
        $wh[1]['param1'] = $precli_id;
        $wh[1]['datatype'] = 'INT';

        if ($action == 'modificar') {
            $wh[2]['conector'] = 'AND';
            $wh[2]['column_name'] = 'pcr.tb_precred_id';
            $wh[2]['param2'] = "<> $precredito_id";
        }

        $join[0]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
        $join[0]['tipo_union'] = 'LEFT';
        $join[0]['tabla_alias'] = 'tb_usuario us';
        $join[0]['columna_enlace'] = 'pcr.tb_precred_usureg';
        $join[0]['alias_columnaPK'] = 'us.tb_usuario_id';

        $join[1]['alias_columnasparaver'] = 'epcr.tb_estadoprecred_nom';
        $join[1]['tipo_union'] = 'LEFT';
        $join[1]['tabla_alias'] = 'tb_estadoprecred epcr';
        $join[1]['columna_enlace'] = 'pcr.tb_estadoprecred_id';
        $join[1]['alias_columnaPK'] = 'epcr.tb_estadoprecred_id';

        $join[2]['alias_columnasparaver'] = 'tb_empresa_nomcom';
        $join[2]['tipo_union'] = 'LEFT';
        $join[2]['tabla_alias'] = 'tb_empresa em';
        $join[2]['columna_enlace'] = 'pcr.tb_empresa_id';
        $join[2]['alias_columnaPK'] = 'em.tb_empresa_id';

        $join[3]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
        $join[3]['tipo_union'] = 'LEFT';
        $join[3]['tabla_alias'] = 'tb_moneda mon';
        $join[3]['columna_enlace'] = 'pcr.tb_moneda_id';
        $join[3]['alias_columnaPK'] = 'mon.tb_moneda_id';

        $join[4]['alias_columnasparaver'] = 'pcl.tb_cliente_id, pcl.tb_precliente_nombres';
        $join[4]['tipo_union'] = 'LEFT';
        $join[4]['tabla_alias'] = 'tb_precliente pcl';
        $join[4]['columna_enlace'] = 'pcr.tb_precliente_id';
        $join[4]['alias_columnaPK'] = 'pcl.tb_precliente_id';

        $join[5]['alias_columnasparaver'] = 'im.tb_inconclusomotivo_nom';
        $join[5]['tipo_union'] = 'LEFT';
        $join[5]['tabla_alias'] = 'tb_inconclusomotivo im';
        $join[5]['columna_enlace'] = 'pcr.tb_inconclusomotivo_id';
        $join[5]['alias_columnaPK'] = 'im.tb_inconclusomotivo_id';

        $otros['orden']['column_name'] = 'pcr.tb_precred_fecreg';
        $otros['orden']['value'] = 'DESC';
    //

    $res = $oPcr->listar_todos($wh, $join, $otros);
    if ($res['estado'] == 1) {
        $lista['precred'] = '';
        $contador = 0;
        foreach ($res['data'] as $key => $pcr) {
            $colorear = $contador == 0 ? 'background-color: #f7e2c3;' : '';
            $bold = $contador == 0 ? 'font-weight: bold;' : '';
            $estado_motivo = $pcr['tb_estadoprecred_nom'];
            if (in_array(intval($pcr['tb_estadoprecred_id']), array(4, 9))) {
                if (!empty($pcr['tb_credito_id'])) {
                    $cretip_nom = intval($pcr['tb_creditotipo_id']) == 1 ? 'CM' : 'CGV';
                    $estado_motivo .= ' en '.$cretip_nom.'-'.$pcr['tb_credito_id'];
                    if (intval($pcr['tb_estadoprecred_id']) == 4) {
                        $creditos_a_excluir[1][] = $pcr['tb_credito_id'];
                    } else {
                        $creditos_a_excluir[3][] = $pcr['tb_credito_id'];
                    }
                } else {
                    $estado_motivo .= ' <font color="red">(pendiente registrar crédito)</font>';
                }
            } elseif (in_array(intval($pcr['tb_estadoprecred_id']), array(5, 10))) {
                $estado_motivo .= ' - '.$pcr['tb_inconclusomotivo_nom'];
            }

            $lista['precred'] .= 
            '<div class="box box-primary shadow" style="padding: 5px 10px;'. $colorear .'">'.
                '<input type="hidden" name="hdd_precred" value=\''. json_encode($pcr) .'\'>'.
                '<div class="box-header with-border" style="padding: 5px 3px;">'.
                    '<div class="col-md-3" style="padding: 0px">'. mostrar_fecha_hora($pcr['tb_precred_fecreg']) .'</div>'.
                    '<div class="col-md-5" style="padding: 0px; text-align: center;">'. $pcr['asesor'] .'</div>'.
                    '<div class="box-tools pull-right">'.
                        'SEDE: ' . substr($pcr['tb_empresa_nomcom'], 7).
                    '</div>'.
                '</div>'.

                '<div class="box-body" style="padding: 5px 5px;">'.
                    '<ul class="products-list product-list-in-box">'.
                        '<div style="padding: 0px 2px;" class="col-md-3">'. $pcr['tb_precliente_nombres'] .'</div>'.
                        '<div style="padding: 0px 2px;" class="col-md-5">'. $pcr['tb_precred_producto'] .'</div>'.
                        '<div style="padding: 0px 3px; text-align: center; '. $bold .'" class="col-md-2">'. "{$pcr['tb_moneda_nom']} ". mostrar_moneda($pcr['tb_precred_monto']) .'</div>'.
                        '<div style="padding: 0px 5px; '. $bold .'" class="col-md-2">'. $estado_motivo .'. <a style="cursor: pointer;" onclick="notas_precred('. $pcr['tb_precred_id'] .')">Ver notas</a></div>'.
                    '</ul>'.
                '</div>'.
            '</div>';
            $contador++;
        }
    }
    unset($res, $wh, $join, $otros);
}

if (!empty($cli_id)) {
    $extension = "";
    if (!empty($creditos_a_excluir[1])) {
        $extension = " AND tb_credito_id NOT IN (";
        for ($i=0; $i < count($creditos_a_excluir[1]); $i++) {
            $extension .= $i>0 ? ", ". $creditos_a_excluir[1][$i] : $creditos_a_excluir[1][$i];
        }
        $extension .= ")";
    }

    $res = $oCm->listar_por_clienteid($cli_id, $extension);

    if ($res['estado'] == 1) {
        $lista['cred'] = '';
        foreach ($res['data'] as $key => $value) {
            $lista_gar = '';
            $res1 = $oGar->listar_garantias($value['tb_credito_id']);
            if ($res1['estado'] == 1) {
                foreach($res1['data'] as $key1 => $gar) {
                    $lista_gar .= '<div class="row">'.
                                        '<div class="col-md-9">'. $gar['tb_garantia_pro'] .'</div>'.
                                        '<div class="col-md-3">'.
                                            '<a style="cursor: pointer; font-weight: bold;" title="CLICK PARA VER GARANTIA" onclick="garantia_form(\'L\', '. $gar['tb_garantia_id'] .')"> S/. '. mostrar_moneda($gar['tb_garantia_val']) .'</a> Click para ver'.
                                        '</div>'.
                                    '</div>';
                    //
                }
            } else {
                $lista_gar .= '<div class="row">'.
                                    '<div class="col-md-9">'. 'La garantía está eliminada o no existe' .'</div>'.
                                    '<div class="col-md-3"> S/. '. mostrar_moneda($value['tb_credito_preaco']) .'</div>'.
                                '</div>';
            }
            unset($res1);

            $lista['cred'] .= 
                '<div class="box box-primary shadow" style="padding: 5px 10px;">'.
                    '<input type="hidden" name="hdd_cred" value=\''. json_encode($value) .'\'>'.
                    '<div class="box-header with-border" style="padding: 5px 3px;">'.
                        '<div class="col-md-3" style="padding: 0px">'. mostrar_fecha_hora($value['tb_credito_reg']) .'</div>'.
                        '<div class="col-md-5" style="padding: 0px; text-align: center;">'. $value['asesor'] .'</div>'.
                        '<div class="box-tools pull-right">'.
                            'SEDE: ' . substr($value['tb_empresa_nomcom'], 7).
                        '</div>'.
                    '</div>'.

                    '<div class="box-body" style="padding: 5px 5px;">'.
                        '<ul class="products-list product-list-in-box">'.
                            '<div style="padding: 0px 2px;" class="col-md-3">'. $value['tb_cliente_nom'] .'</div>'.
                            '<div style="padding: 0px;" class="col-md-7">';
            //aqui toca hacer la busqueda de todas las garantias de ese CM
            $lista['cred'] .=   $lista_gar;
            //aqui aqui termina listado de garantias
            $lista['cred'].='</div>'.
                            '<div style="padding: 0px 5px;" class="col-md-2" align="center">'.
                                '<a style="cursor: pointer; font-weight: bold;" title="CLICK PARA VER CM" onclick="creditomenor_form(\'L\', 0, '. $value['tb_credito_id'] .')">VER EL CM-'.$value['tb_credito_id'].'</a>'.
                            '</div>'.
                        '</ul>'.
                    '</div>'.
                '</div>';
            //
        }
    }
    unset($res);
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditosanteriores_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo "Precréditos y Créditos anteriores del cliente"; ?></h4>
            </div>
            <form id="form_precredito" method="post">
                <div class="modal-body" style="max-height: 695px; overflow-y: auto;">
                    <h4>PRECREDITOS ANTERIORES</h4>
                    <?php echo $lista['precred'];?>
                    <br>
                    <h4>CREDITOS ANTERIORES</h4>
                    <?php echo $lista['cred'];?>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" id="btn_cerrar" class="btn btn-success" onclick="mostrar_2da_parte()" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
