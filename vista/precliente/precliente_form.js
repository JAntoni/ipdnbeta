$(document).ready(function(){
    $('#txt_precliente_nrodocumento, #txt_precliente_nombres').autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                VISTA_URL + "cliente/cliente_autocomplete.php",
                { term: request.term }, //
                response
            );
        },
        select: function (event, ui) {
            $('#hdd_cliente_id').val(ui.item.cliente_id);
            $('#hdd_cliente_tel_orig, #txt_precliente_numcel').val(ui.item.cliente_tel);
            disabled($('#txt_precliente_nrodocumento').val(ui.item.cliente_doc));
            disabled($('#txt_precliente_nombres').val(ui.item.cliente_nom));
            event.preventDefault();
        }
    });

    if(parseInt($('#hdd_cliente_id').val()) > 0){
        disabled($('#txt_precliente_nrodocumento, #txt_precliente_nombres, #btn_limpiarcliente'));
    }

    $('#form_precliente').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "precliente/precliente_controller.php",
                async: true,
                dataType: "json",
                data: serializar_form_precli(),
                beforeSend: function () {
                    $('#precliente_mensaje').show(400);
                    $('#btn_guardar_precliente').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) == 1) {
                        $('#precliente_mensaje').removeClass('callout-info').addClass('callout-success');
                        $('#precliente_mensaje').html(`<h4>${data.mensaje}</h4>`);
                        if ($.inArray($('#action_precliente').val(), ['insertar', 'modificar']) > -1) {
                            disabled($('#form_precliente').find("button[class*='btn-sm'], select, input, textarea"));
                        }

                        setTimeout(function () {
                            if ($('#hdd_vista').val() == 'precredito' && parseInt(data.cambios) == 1) {
                                llenar_datos_precliente(data.registro);
                            }
                            $('#modal_precliente_form').modal('hide');
                        }, 2800);
                    }
                    else {
                        $('#precliente_mensaje').removeClass('callout-info').addClass('callout-warning');
                        $('#precliente_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_precliente').prop('disabled', false);
                    }
                },
                error: function (data) {
                    $('#precliente_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#precliente_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_precliente_nrodocumento: {
                required: true,
                minlength: 8
            },
            txt_precliente_nombres: {
                required: true
            }
        },
        messages: {
            txt_precliente_nrodocumento: {
                required: 'N° documento*',
                minlength: '8 digitos min*'
            },
            txt_precliente_nombres: {
                required: 'Nombres cliente*'
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});

function limpiar_cliente(){
    $('#hdd_cliente_id, #hdd_cliente_tel_orig, #txt_precliente_nrodocumento, #txt_precliente_nombres, #txt_precliente_numcel')
    .val('')
    .prop('disabled', false);
    $('#txt_precliente_nrodocumento').focus();
}

function serializar_form_precli() {
	var extra = ``;

    var elementos_disabled = $('#form_precliente').find('input:disabled, select:disabled').removeAttr('disabled');
    var form_serializado = $('#form_precliente').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}