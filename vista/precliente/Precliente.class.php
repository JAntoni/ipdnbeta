<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Precliente extends Conexion{
    public $precliente_id;
    public $cliente_id;
    public $precliente_tipodoc;
    public $precliente_numdoc;
    public $precliente_nombres;
    public $precliente_numcel;
    public $mediocom_id;
    public $precliente_fecreg;
    public $precliente_usureg;
    public $precliente_fecmod;
    public $precliente_usumod;
    public $precliente_xac;

    public function listar_todos($where, $join, $otros){
        try {
            $sql = "SELECT pcl.*";
            if (!empty($join)) {
                for ($j = 0; $j < count($join); $j++) {
                    $sql .= ",\n" . $join[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_precliente pcl\n";
            if (!empty($join)) {
                for ($k = 0; $k < count($join); $k++) {
                    $sql .= $join[$k]['tipo_union'] . " JOIN " . $join[$k]['tabla_alias'] . " ON ";
                    if (!empty($join[$k]['columna_enlace']) && !empty($join[$k]['alias_columnaPK'])) {
                        $sql .= $join[$k]['columna_enlace'] . " = " . $join[$k]['alias_columnaPK'] . "\n";
                    } else {
                        $sql .= $join[$k]['on'] . "\n";
                    }
                }
            }
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i])) {
                    $sql .= ($i > 0) ? "\n{$where[$i]['conector']} " : 'WHERE ';

                    if (!empty($where[$i]['column_name'])) {
                        if (stripos($where[$i]['column_name'], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$where[$i]['column_name']}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$where[$i]['column_name']}";
                        }

                        if (!empty($where[$i]['datatype'])) {
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " " . $where[$i]["param$i"];
                        }
                    } else {
                        $sql .= $where[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros["group"])) {
                $sql .= "\nGROUP BY {$otros["group"]["column_name"]}";
            }
            if (!empty($otros["orden"])) {
                $sql .= "\nORDER BY {$otros["orden"]["column_name"]} {$otros["orden"]["value"]}";
            }
            if (!empty($otros["limit"])) {
                $sql .= "\nLIMIT {$otros["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i]["column_name"]) && !empty($where[$i]["datatype"])) {
                    $_PARAM = strtoupper($where[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $where[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            $retorno["sql"] = $sql;
            $retorno["row_count"] = $sentencia->rowCount();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } 
            else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $colum_opc = empty($this->precliente_numcel) ? '' : ",\ntb_precliente_numcel";
            $colum_opc .= empty($this->cliente_id) ? '' : ",\ntb_cliente_id";
            $valor_opc = empty($this->precliente_numcel) ? '' : ",\n:param_opc0";
            $valor_opc .= empty($this->cliente_id) ? '' : ",\n:param_opc1";

            $sql = "INSERT INTO tb_precliente (
                tb_precliente_tipodoc,
                tb_precliente_numdoc,
                tb_precliente_nombres,
                tb_precliente_usureg,
                tb_precliente_usumod,
                tb_mediocom_id$colum_opc)
              VALUES (
                :param0,
                :param1,
                :param2,
                :param3,
                :param4,
                :param5$valor_opc);";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->precliente_tipodoc, PDO::PARAM_STR);
            $sentencia->bindParam(":param1", $this->precliente_numdoc, PDO::PARAM_STR);
            $sentencia->bindParam(":param2", $this->precliente_nombres, PDO::PARAM_STR);
            $sentencia->bindParam(":param3", $this->precliente_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param4", $this->precliente_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(":param5", $this->mediocom_id, PDO::PARAM_INT);
            if (!empty($this->precliente_numcel))
                $sentencia->bindParam(":param_opc0", $this->precliente_numcel, PDO::PARAM_STR);
            if (!empty($this->cliente_id))
                $sentencia->bindParam(":param_opc1", $this->cliente_id, PDO::PARAM_INT);

            $resultado['estado'] = $sentencia->execute();
            $resultado['sql'] = $sql;

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    public function modificar() {
        $this->dblink->beginTransaction();
        try {
            $opc = !empty($this->cliente_id) ? ", tb_cliente_id = :param_opc0" : '';

            $sql = "UPDATE tb_precliente
              SET
                tb_precliente_tipodoc = :param0,
                tb_precliente_numdoc = :param1,
                tb_precliente_nombres = :param2,
                tb_precliente_numcel = :param3,
                tb_precliente_usumod = :param4,
                tb_mediocom_id = :param5$opc
              WHERE
                tb_precliente_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->precliente_tipodoc, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->precliente_numdoc, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->precliente_nombres, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->precliente_numcel, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->precliente_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':param5', $this->mediocom_id, PDO::PARAM_INT);
            if (!empty($this->cliente_id)) {
                $sentencia->bindParam(':param_opc0', $this->cliente_id, PDO::PARAM_INT);
            }
            $sentencia->bindParam(':paramx', $this->precliente_id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_campo($precliente_id, $precliente_columna, $precliente_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($precliente_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_precliente SET " . $precliente_columna . " = :precliente_valor, tb_precliente_usumod = {$_SESSION['usuario_id']} WHERE tb_precliente_id = :precliente_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":precliente_id", $precliente_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":precliente_valor", $precliente_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":precliente_valor", $precliente_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($precliente_id){
        try {
          $sql = "SELECT * FROM tb_precliente WHERE tb_precliente_id =:precliente_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":precliente_id", $precliente_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
      }
}