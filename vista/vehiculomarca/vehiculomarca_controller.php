<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculomarca/Vehiculomarca.class.php');
  $oVehiculomarca = new Vehiculomarca();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$vehiculomarca_nom = mb_strtoupper($_POST['txt_vehiculomarca_nom'], 'UTF-8');
 		$vehiculomarca_des = $_POST['txt_vehiculomarca_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Marca.';
                
                $result = $oVehiculomarca->insertar($vehiculomarca_nom, $vehiculomarca_des);
                        
 		if($result['estado']){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Marca registrado correctamente.';
 			$data['marca_id'] = $result['vehiculomarca_id'];
 		}

 		echo json_encode($data);
 	}
  
 	elseif($action == 'modificar'){
 		$vehiculomarca_id = intval($_POST['hdd_vehiculomarca_id']);
 		$vehiculomarca_nom = mb_strtoupper($_POST['txt_vehiculomarca_nom'], 'UTF-8');
 		$vehiculomarca_des = $_POST['txt_vehiculomarca_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Marca.';

 		if($oVehiculomarca->modificar($vehiculomarca_id, $vehiculomarca_nom, $vehiculomarca_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Marca modificado correctamente. ';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculomarca_id = intval($_POST['hdd_vehiculomarca_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Marca.';

 		if($oVehiculomarca->eliminar($vehiculomarca_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Marca eliminado correctamente. '.$vehiculomarca_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>