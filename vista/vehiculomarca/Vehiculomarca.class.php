<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculomarca extends Conexion{

    function insertar($vehiculomarca_nom, $vehiculomarca_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_vehiculomarca(tb_vehiculomarca_xac, tb_vehiculomarca_nom, tb_vehiculomarca_des)
          VALUES (1, :vehiculomarca_nom, :vehiculomarca_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomarca_nom", $vehiculomarca_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculomarca_des", $vehiculomarca_des, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $vehiculomarca_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result;
        $data['vehiculomarca_id'] = $vehiculomarca_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($vehiculomarca_id, $vehiculomarca_nom, $vehiculomarca_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculomarca SET tb_vehiculomarca_nom =:vehiculomarca_nom, tb_vehiculomarca_des =:vehiculomarca_des WHERE tb_vehiculomarca_id =:vehiculomarca_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomarca_id", $vehiculomarca_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculomarca_nom", $vehiculomarca_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculomarca_des", $vehiculomarca_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculomarca_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_vehiculomarca WHERE tb_vehiculomarca_id =:vehiculomarca_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomarca_id", $vehiculomarca_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculomarca_id){
      try {
        $sql = "SELECT * FROM tb_vehiculomarca WHERE tb_vehiculomarca_id =:vehiculomarca_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomarca_id", $vehiculomarca_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculomarcas(){
      try {
        $sql = "SELECT * FROM tb_vehiculomarca order by tb_vehiculomarca_nom";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
