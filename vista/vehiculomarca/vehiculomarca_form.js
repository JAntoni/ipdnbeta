
$(document).ready(function(){
  $('#form_vehiculomarca').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculomarca/vehiculomarca_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculomarca").serialize(),
				beforeSend: function() {
					$('#vehiculomarca_mensaje').show(400);
					$('#btn_guardar_vehiculomarca').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculomarca_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculomarca_mensaje').html(data.mensaje);
                        $('#modal_registro_vehiculomarca').modal('hide');
                        var modulo = $("#hdd_modulo").val();
                        if(modulo = 'creditogarveh'){
                            cargar_vehiculomarca(data.marca_id);
                        }
                        if(modulo != 'creditogarveh'){
		      	setTimeout(function(){ 
		      		vehiculomarca_tabla();
		      		 }, 1000
		      	);
                        }
					}
					else{
		      	$('#vehiculomarca_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculomarca_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculomarca').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculomarca_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculomarca_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_vehiculomarca_nom: {
				required: true,
				minlength: 2
			},
			txt_vehiculomarca_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_vehiculomarca_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_vehiculomarca_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
