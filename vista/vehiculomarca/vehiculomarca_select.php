<?php
require_once('Vehiculomarca.class.php');
$oVehiculomarca = new Vehiculomarca();

$vehiculomarca_id = (empty($vehiculomarca_id))? intval($_POST['vehiculomarca_id']) : $vehiculomarca_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include
//$vehiculomarca_id = (empty($_POST['vehiculomarca_id']))? 0 : $_POST['vehiculomarca_id']; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0">Seleccione...</option>';

//PRIMER NIVEL
$result = $oVehiculomarca->listar_vehiculomarcas();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($vehiculomarca_id == $value['tb_vehiculomarca_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_vehiculomarca_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_vehiculomarca_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>