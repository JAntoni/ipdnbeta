<?php


if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Vencimiento extends Conexion{
	
 function filtrar_menor($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT *
		FROM tb_creditomenor cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 1 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 AND (cm.tb_credito_est = 3 OR cm.tb_credito_est = 5) and tb_cuota_xac = 1";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";
		//$sql.=" ORDER BY cu.tb_cuota_fec";

		$sql.=" UNION";
		$sql.=" SELECT *
		FROM tb_creditomenor cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 1 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 AND (cm.tb_credito_est = 3 OR cm.tb_credito_est = 5) and tb_cuota_xac = 1";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" OR cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 1 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 AND (cm.tb_credito_est = 3 OR cm.tb_credito_est = 5) and tb_cuota_xac = 1";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" GROUP BY cm.tb_credito_id";
		$sql.=" ORDER BY tb_cuota_fec";
        
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function filtrar_asiveh($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and cm.tb_credito_est NOT IN(1,2,7,8) and cu.tb_cuota_acupag = 0 and cm.tb_credito_tip1 IN(1,4)";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" UNION";
		$sql.=" SELECT *
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and cm.tb_credito_est NOT IN(1,2,7,8) and cu.tb_cuota_acupag = 0 and cm.tb_credito_tip1 IN(1,4)";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" OR cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 and cm.tb_credito_est NOT IN(1,2,7,8) and cu.tb_cuota_acupag = 0 and cm.tb_credito_tip1 IN(1,4)";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";
		
		
		$sql.=" GROUP BY cm.tb_credito_id";
		$sql.=" ORDER BY tb_cuota_fec";
        
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    
    function  mostrarUnoAsiveh($id){
        
      try {
        $sql = "SELECT * 
		FROM tb_creditoasiveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:id and cd.tb_cuotadetalle_est <> 2 and cm.tb_credito_est != 8 AND cd.tb_cuotadetalle_estap != 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    
    function  filtrar_asiveh_acupag($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT * 
                FROM tb_creditoasiveh cm 
                INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and cm.tb_credito_est NOT IN(7,8) and cu.tb_cuota_acupag = 1";

                if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

                $sql.=" UNION";
                $sql.=" SELECT *
                FROM tb_creditoasiveh cm 
                INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec >=:fec_act AND cu.tb_cuota_est = 1 and cm.tb_credito_est NOT IN(7,8) and cu.tb_cuota_acupag = 1";

                if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

                $sql.=" OR cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec >=:fec_act AND cu.tb_cuota_est = 3 and cm.tb_credito_est NOT IN(7,8) and cu.tb_cuota_acupag = 1";

                if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";


                $sql.=" GROUP BY cm.tb_credito_id";
                $sql.=" ORDER BY tb_cuota_fec";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    

    function  filtrar_asiveh_adendas($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT * 
                FROM tb_creditoasiveh cm 
                INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and cm.tb_credito_est NOT IN(1,2,7,8) and cu.tb_cuota_acupag = 0 and cm.tb_credito_tip1 =2";

                if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

                $sql.=" UNION";
                $sql.=" SELECT *
                FROM tb_creditoasiveh cm 
                INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and cm.tb_credito_est NOT IN(1,2,7,8) and cu.tb_cuota_acupag = 0 and cm.tb_credito_tip1 =2";

                if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

                $sql.=" OR cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 2 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 and cm.tb_credito_est NOT IN(1,2,7,8) and cu.tb_cuota_acupag = 0 and cm.tb_credito_tip1 =2";

                if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";


                $sql.=" GROUP BY cm.tb_credito_id";
                $sql.=" ORDER BY tb_cuota_fec";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    

    function  filtrar_garveh($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and cm.tb_credito_est NOT IN(1,2,7,8) AND cm.tb_credito_tip IN(1,4)";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" UNION";
		$sql.=" SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and cm.tb_credito_est NOT IN(1,2,7,8) AND cm.tb_credito_tip IN(1,4)";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";
		
		$sql.=" OR cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 AND cm.tb_credito_tip IN(1,4)";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" GROUP BY cm.tb_credito_id";
		$sql.=" ORDER BY tb_cuota_fec";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    

    function  mostrarUnoGarveh($id){
        
      try {
        $sql = "SELECT * 
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:id and cd.tb_cuotadetalle_est <> 2 AND cd.tb_cuotadetalle_estap != 1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  filtrar_garveh_acupag($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and cm.tb_credito_est NOT IN(7,8) AND cm.tb_credito_tip = 3";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" UNION";
		$sql.=" SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and cm.tb_credito_est NOT IN(7,8) AND cm.tb_credito_tip = 3";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";
		
		$sql.=" OR cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 AND cm.tb_credito_tip = 3";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = :cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = :cre_id";

		$sql.=" GROUP BY cm.tb_credito_id";
		$sql.=" ORDER BY tb_cuota_fec";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function  filtrar_garveh_adendas($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and cm.tb_credito_est NOT IN(1,2,7,8) AND cm.tb_credito_tip = 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = :cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id = :cre_id";

		$sql.=" UNION";
		$sql.=" SELECT *
		FROM tb_creditogarveh cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and cm.tb_credito_est NOT IN(1,2,7,8) AND cm.tb_credito_tip = 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = :cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";
		
		$sql.=" OR cm.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 AND cm.tb_credito_tip = 2";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

		$sql.=" GROUP BY cm.tb_credito_id";
		$sql.=" ORDER BY tb_cuota_fec";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function  filtrar_hipotecario($cli_id,$cre_id,$fec_act){
        
      try {
        $sql = "SELECT *
                FROM tb_creditohipo ch 
                INNER JOIN tb_cliente c ON ch.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON ch.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON ch.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON ch.tb_credito_usureg=ur.tb_usuario_id
                WHERE ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and ch.tb_credito_est NOT IN(1,2,7,8) AND ch.tb_credito_tip in (1,4)";

                if($cli_id>0)$sql.=" AND ch.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND ch.tb_credito_id =:cre_id";

                $sql.=" UNION";
                $sql.=" SELECT *
                FROM tb_creditohipo ch 
                INNER JOIN tb_cliente c ON ch.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON ch.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON ch.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON ch.tb_credito_usureg=ur.tb_usuario_id
                WHERE ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and ch.tb_credito_est NOT IN(1,2,7,8) AND ch.tb_credito_tip in (1,4)";

                if($cli_id>0)$sql.=" AND ch.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND ch.tb_credito_id = :cre_id";

                $sql.=" OR ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 and ch.tb_credito_est NOT IN(1,2,7,8) AND ch.tb_credito_tip in (1,4)";

                if($cli_id>0)$sql.=" AND ch.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND ch.tb_credito_id =:cre_id";

                $sql.=" GROUP BY ch.tb_credito_id";
                $sql.=" ORDER BY tb_cuota_fec";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  mostrarUnoHipo($id){
        
      try {
        $sql = "SELECT * 
                FROM tb_creditohipo hi 
                INNER JOIN tb_cliente c ON hi.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON hi.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON hi.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
                INNER JOIN tb_usuario ur ON hi.tb_credito_usureg=ur.tb_usuario_id
                WHERE cu.tb_cuota_id=:id and cd.tb_cuotadetalle_est <> 2 AND cd.tb_cuotadetalle_estap != 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function filtrar_hipotecario_acupag($cli_id,$cre_id,$fec_act){
        
        try {
            $sql="SELECT *
            FROM tb_creditohipo ch 
            INNER JOIN tb_cliente c ON ch.tb_cliente_id = c.tb_cliente_id
            INNER JOIN tb_moneda m  ON ch.tb_moneda_id = m.tb_moneda_id
            INNER JOIN tb_cuota cu ON ch.tb_credito_id = cu.tb_credito_id
            INNER JOIN tb_usuario ur ON ch.tb_credito_usureg=ur.tb_usuario_id
            WHERE ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and ch.tb_credito_est NOT IN(7,8) AND ch.tb_credito_tip = 3";

            if($cli_id>0)$sql.=" AND ch.tb_cliente_id = :cli_id";
            if($cre_id>0)$sql.=" AND ch.tb_credito_id = :cre_id";

            $sql.=" UNION";
            $sql.=" SELECT *
            FROM tb_creditohipo ch 
            INNER JOIN tb_cliente c ON ch.tb_cliente_id = c.tb_cliente_id
            INNER JOIN tb_moneda m  ON ch.tb_moneda_id = m.tb_moneda_id
            INNER JOIN tb_cuota cu ON ch.tb_credito_id = cu.tb_credito_id
            INNER JOIN tb_usuario ur ON ch.tb_credito_usureg=ur.tb_usuario_id
            WHERE ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and ch.tb_credito_est NOT IN(7,8) AND ch.tb_credito_tip = 3";

            if($cli_id>0)$sql.=" AND ch.tb_cliente_id = :cli_id";
            if($cre_id>0)$sql.=" AND ch.tb_credito_id = :cre_id";

            $sql.=" OR ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 AND ch.tb_credito_tip = 3";

            if($cli_id>0)$sql.=" AND ch.tb_cliente_id = :cli_id";
            if($cre_id>0)$sql.=" AND ch.tb_credito_id = :cre_id";

            $sql.=" GROUP BY ch.tb_credito_id";
            $sql.=" ORDER BY tb_cuota_fec";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function filtrar_hipotecario_adendas($cli_id,$cre_id,$fec_act){
        
        try {
            $sql="SELECT *
                FROM tb_creditohipo ch 
                INNER JOIN tb_cliente c ON ch.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON ch.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON ch.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON ch.tb_credito_usureg=ur.tb_usuario_id
                WHERE ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec < :fec_act AND cu.tb_cuota_est <> 2 and ch.tb_credito_est NOT IN(1,2,7,8) AND ch.tb_credito_tip = 2";

                if($cli_id>0)$sql.=" AND ch.tb_cliente_id =:cli_id";
                if($cre_id>0)$sql.=" AND ch.tb_credito_id =:cre_id";

                $sql.=" UNION";
                $sql.=" SELECT *
                FROM tb_creditohipo ch 
                INNER JOIN tb_cliente c ON ch.tb_cliente_id = c.tb_cliente_id
                INNER JOIN tb_moneda m  ON ch.tb_moneda_id = m.tb_moneda_id
                INNER JOIN tb_cuota cu ON ch.tb_credito_id = cu.tb_credito_id
                INNER JOIN tb_usuario ur ON ch.tb_credito_usureg=ur.tb_usuario_id
                WHERE ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 1 and ch.tb_credito_est NOT IN(1,2,7,8) AND ch.tb_credito_tip = 2";

                if($cli_id>0)$sql.=" AND ch.tb_cliente_id = :cli_id";
                if($cre_id>0)$sql.=" AND ch.tb_credito_id = :cre_id";

                $sql.=" OR ch.tb_credito_xac=1 AND cu.tb_cuota_xac = 1 AND cu.tb_creditotipo_id = 4 AND cu.tb_cuota_fec >= :fec_act AND cu.tb_cuota_est = 3 AND ch.tb_credito_tip = 2 and ch.tb_credito_est NOT IN(1,2,7,8)";

                if($cli_id>0)$sql.=" AND ch.tb_cliente_id = :cli_id";
                if($cre_id>0)$sql.=" AND ch.tb_credito_id = :cre_id";

                $sql.=" GROUP BY ch.tb_credito_id";
                $sql.=" ORDER BY tb_cuota_fec";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    function nro_cuotas_Pendientes($cli_id,$fec_act,$cre_id){
        
      try {
        $sql = "SELECT COUNT(cu.tb_cuota_id) as cuotas
		FROM tb_creditomenor cm 
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id 
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac=1 AND cu.tb_creditotipo_id = 1 AND cu.tb_cuota_fec < :fec_act  AND (cu.tb_cuota_est=1 or cu.tb_cuota_est=3 ) AND (cm.tb_credito_est = 3 OR cm.tb_credito_est = 5) and tb_cuota_xac = 1";

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id =:cli_id";
		if($cre_id>0)$sql.=" AND cm.tb_credito_id =:cre_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fec_act", $fec_act, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito menor registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
}
?>