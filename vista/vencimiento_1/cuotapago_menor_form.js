/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    var pc_nom = $('#hdd_vista_pc_nombre').val();
    var pc_mac = $('#hdd_vista_pc_mac').val();
    $('#hdd_pc_nombre').val(pc_nom);
    $('#hdd_pc_mac').val(pc_mac);
  
  /*CALCULAR LA MORA DEL CREDITO*/
    calcular_mora();
    
    $('.moneda').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999999.00'
	});
        $('.moneda3').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '999999999.00'
});



/*FORMATO DE FECHA PARA QUE NO SEA MAYOR A LA ACTUAL*/
    $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
    });

    $("#for_cuopag").validate({
        submitHandler: function() {
            $.ajax({
                    type: "POST",
                    url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
                    async:true,
                    dataType: "json",
                    data: $("#for_cuopag").serialize(),
                    beforeSend: function() {
//                            $('#msj_vencimiento').html("Guardando...");
//                            $('#msj_vencimiento').show(100);
                    },
                    success: function(data){	
                        if(parseInt(data.cuopag_id) > 0){
                            $('#modal_registro_vencimientomenor').modal('hide');
//                            if(confirm('¿Desea imprimir?'))
//                            cuotapago_imppos_datos_2(data.cuopag_id);
                            
                        
                             $('#msj_vencimiento').hide();
                            $('#msj_cuopag_menor').html(data.cuopag_msj);
//                            swal_success('SISTEMA',data.cuopag_msj,2000);
                            var vista=$("#hdd_vista").val();

                            if(vista=="cmb_ven_id"){
                                vista+"."+(data.ven_id);
                            }
                            var tipo=$("#action_cuotapago").val();
                                if(tipo=="pagar_libre"){
                                    generar_nueva_cuota();
                                }
                                
                            if(vista=="vencimiento_tabla"){
                                vencimiento_tabla();
                            }
                            
                            var codigo=data.cuopag_id;
                            window.open("http://www.ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);
//                            window.open("http://localhost/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);
                            
                            swal_success('SISTEMA',data.cuopag_msj,5000);
                             console.log('tipo = '+data.cuopag_msj);
                            
                           
                        }
                        else{
//                            $('#btn_guar_cuopag').show();
//                            $('#msj_vencimiento').show(100);
//                            $('#msj_vencimiento').text(data.cuopag_msj);
                            swal_warning("AVISO",data.cuopag_msj,3500);
                        }
                    },
                    complete: function(data){
                            if(data.statusText != 'success'){
                                $('#btn_guar_cuopag').show();
//                                $('#msj_vencimiento').text(data.responseText);
//                                console.log(data);
                            }
                    }
			});
		},
		rules: {
			txt_cuopag_mon:{
				required:true
			}
		},
		messages: {
			txt_cuopag_mon:{
				required:'* ingrese un monto'
			}
		}
	});




    
});

function Activar_input_monto_pago_banco(){
    $('#txt_cuopag_mon').attr("readonly",false);
}

function usarSaldo(){
	var num=$('#txt_clicaj_tot').val();
	var cuo=$('#txt_cuodet_cuo').val();

	if(num*1<=cuo*1)
	{
                $('#txt_cuopag_clicaj').autoNumeric('set',num);
	}
	else
	{
                 $('#txt_cuopag_clicaj').autoNumeric('set',cuo);
	}
	
	calculo();
}

function calculo(){
	var cuo=$('#txt_cuodet_cuo').val();
	if($('#txt_cuopag_pagpar').val()>0)
	{
		var pagpar=Number($('#txt_cuopag_pagpar').autoNumeric('get'));
	}
	else
	{
		var pagpar=0;
	}

	if($('#txt_cuopag_clicaj').val()>0)
	{
		var clicaj=$('#txt_cuopag_clicaj').val();
	}
	else
	{
		var clicaj=0;
	}

	var resta1=cuo-pagpar;

	resta1=resta1.toFixed(2);

	var total=resta1-clicaj;
	total=total.toFixed(2);

	if(resta1*1>=clicaj*1)
	{
		//alert(total);
		$('#txt_cuopag_tot').autoNumeric('set',total);
		$('#txt_cuopag_mon').autoNumeric('set',total);
	}
	else
	{
		$('#txt_cuopag_clicaj').autoNumeric('set',resta1);
		$('#txt_cuopag_tot').val('0.00');
		$('#txt_cuopag_mon').val('0.00');
	}
}

function calcular_mora(){	
    $.ajax({
            type: "POST",
            url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
            async:true,
            dataType: "json",                      
            data: ({
                    action: "calcular_mora",
                    fechaPago: $('#txt_cuopag_fec').val(),
                    fecha: $("#hdd_cuo_fec").val(),
                    cre_preaco: $('#hdd_cre_preaco').val()
            }),
            success: function(data){
                    $('#txt_cuopag_mor').val(data.mora);
                    if(parseInt(data.estado) == 0){
                            //no existe tarifario para esas fechas
                            $('#msj_menor_form').html(data.msj);
                            $('#msj_menor_form').show();
                    }
            }, 
            complete: function(data){
            }
    });
}

function comprobar(obj){   
    if (obj.checked){     
       $("#txt_cuopag_mor").attr("readonly",false);
   } 
   else{
        $("#txt_cuopag_mor").attr("readonly",true);
   }     
}
function Editar(obj){   
    if (obj.checked){     
       $("#txt_cuopag_por").attr("readonly",false);
   } 
   else{
        $("#txt_cuopag_por").attr("readonly",true);
   }     
}

function generar_nueva_cuota(){
    
    var vencida=$('#hdd_vencida').val();
    var fecha;
    if(vencida==1){
        fecha=$('#hdd_cuo_fec').val();
    }
    else{
        fecha=$('#txt_cuopag_fec').val();
    }
    
//    console.log('CREDITO ID = '+$('#hdd_cre_id').val());
//    console.log('CUOTA ID = '+$('#hdd_cuo_id').val());
//    console.log('INTERES = '+$('#hdd_cre_int').val());
//    console.log('CUOTAS MAXIMAS = '+$('#hdd_cuo_max').val());
//    console.log('FECHA = '+fecha);
//    console.log('MONEDA = '+$('#hdd_mon_id').val());
  $.ajax({
    type: "POST",
    url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
    async:true,
    dataType: "json",                      
    data: ({
      action: "cuota_nueva",
      cre_id: $('#hdd_cre_id').val(),
      cuo_id: $('#hdd_cuo_id').val(),
      int: $('#hdd_cre_int').val(),
      cuo_max: $('#hdd_cuo_max').val(),
      fecha: fecha,
      mon_id: $('#hdd_mon_id').val()
    }),
    beforeSend: function() {
      $('#msj_vencimiento').html("Guardando...");
      $('#msj_vencimiento').show(100);
    },
    success: function(data){
         console.log('estado = '+data.estado);
      if(data.estado == 1){
        $('#modal_registro_vencimientomenor').modal('hide');
            swal_success('SISTEMA',data.msj,2000);
        $('#msj_vencimiento').hide();
//        var tipo=$("#action_cuotapago").val();
//            if(tipo=="pagar_fijo")
//            vencimiento_tabla();
      }
      else if(data.estado == 2){
        $('#modal_registro_vencimientomenor').modal('hide');
         swal_warning('AVISO',data.msj,5000);
         $('#msj_vencimiento').hide();
      }
      else{
        $('#modal_registro_vencimientomenor').modal('hide');
        swal_warning('AVISO','Error al momento de generar nueva cuota',5000);
      }
    },
    complete: function(data){
      if(data.statusText != 'success'){}
        console.log(data);
    }
  });
}


 $('#txt_cuopag_fec').change(function (){
    calcular_mora(); 
 });