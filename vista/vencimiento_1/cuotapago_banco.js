/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    cmb_cuedep_id(0);

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
     });

      $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
      });
      
      $('#ingreso_fil_picker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
      });

    $('.moneda3').autoNumeric({
            aSep: ',',
            aDec: '.',
            //aSign: 'S/. ',
            //pSign: 's',
            vMin: '0.00',
            vMax: '999999.99'
    });
    $('.moneda4').autoNumeric({
            aSep: ',',
            aDec: '.',
            //aSign: 'S/. ',
            //pSign: 's',
            vMin: '0.00',
            vMax: '99999.00'
    });
    
    var pc_nom = $('#hdd_vista_pc_nombre').val();
    var pc_mac = $('#hdd_vista_pc_mac').val();
    $('#hdd_pc_nombre').val(pc_nom);
    $('#hdd_pc_mac').val(pc_mac);
    

    cambio_moneda(2);
    calcular_mora();
    
    $("#chk_mor_aut").change(function(){
        if($('#chk_mor_aut').is(':checked'))
        {  
                $("#txt_cuopag_mor").prop('readonly', false);
                //$("#txt_cuopag_mor").addClass("moneda2");
        } else {  
                $("#txt_cuopag_mor").prop('readonly', true);
                //$("#txt_cuopag_mor").removeClass("moneda2");
        }
    });
    $('#txt_cuopag_fecdep').change(function(event) {
        cambio_moneda(2);
        calcular_mora();
    });
    
    $('#txt_cuopag_mon, #txt_cuopag_comi').change(function(event) {
            var monto_deposito = Number($('#txt_cuopag_mon').autoNumeric('get'));
            var monto_comision = Number($('#txt_cuopag_comi').autoNumeric('get'));
            var cuenta_deposito = $('#cmb_cuedep_id').val();
            var saldo = 0;

            saldo = monto_deposito - monto_comision;

            if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
                    $('#txt_cuopag_montot').autoNumeric('set',saldo.toFixed(2));
                    $('#txt_cuopag_monval').autoNumeric('set',saldo.toFixed(2));
            }
            else{
                    $('#txt_cuopag_montot').autoNumeric('set',0);
                    $('#txt_cuopag_monval').autoNumeric('set',0);
            }

            if(cuenta_deposito)
                    $('#cmb_cuedep_id').change();
    });

    $('#cmb_cuedep_id').change(function(event) {
//        console.log("estoy entrando al sistema select ");
        var moneda_sel = $(this).find(':selected').data('moneda'); //este tipo de moneda corresponde al tipo de cuenta seleccionada
        var moneda_id = $('#hdd_mon_id').val(); //esta moneda pertenece al credito
        var monto_validar = Number($('#txt_cuopag_monval').autoNumeric('get')); //es el restanto de lo depositado menos la comision que cobra el banco
        var monto_cambio = Number($('#txt_cuopag_tipcam').val()); //monto del tipo de cambio del día del deposito
        var total_pagado = 0;
        if(parseInt(moneda_sel) == 2 && parseInt(moneda_id) == 1){
                total_pagado = parseFloat(monto_validar * monto_cambio);
                $('#txt_cuopag_montot').autoNumeric('set',total_pagado);
                console.log('dolar a soles monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
        }
        else if(parseInt(moneda_sel) == 1 && parseInt(moneda_id) == 2){
                total_pagado = parseFloat(monto_validar / monto_cambio);
                $('#txt_cuopag_montot').autoNumeric('set',total_pagado);
                console.log('soles a dolares monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
        }
        else
                $('#txt_cuopag_montot').autoNumeric('set',monto_validar);

        var cuenta = $(this).find("option:selected").text();
        $('#hdd_cuenta_dep').val(cuenta);
        $('#hdd_mon_iddep').val(moneda_sel);
    });

    
});


function cambio_moneda(monid){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"monedacambio/monedacambio_controller.php",
            async:false,
            dataType: "json",
            data: ({
                    action: 'obtener_dato',
                    fecha:	$('#txt_cuopag_fecdep').val(),
                    mon_id:	monid
            }),
            beforeSend: function(){
                    $('#msj_pagobanco').hide(100);
            },
            success: function(data){
                    if(data.moncam_val == null){
                            $('#txt_cuopag_tipcam').val('');
                            $('#msj_pagobanco').show(100);
                            $('#msj_pagobanco').html('Por favor registre el tipo de cambio del día de la fecha de depósito');
                            swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
                    }
                    else
                            $('#txt_cuopag_tipcam').val(data.moncam_val);	
            },
            complete: function(data){			
//                 console.log(data);
            }
        });		
}


function cmb_cuedep_id(ids)
{	
        $.ajax({
                type: "POST",
                url:  VISTA_URL+"cuentadeposito/cuentadeposito_select.php",
                async:false,
                dataType: "html",                      
                data: ({
                        cuedep_id: ids
                }),
                beforeSend: function() {
                        $('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
                },
                success: function(html){
                        $('#cmb_cuedep_id').html(html);
                }
        });
}

function calcular_mora(){
    
    var url_mora=$('#hidden_url_mora').val();

    var cuodet_fec=$('#hidden_cuodet_fec').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL+"vencimiento/"+url_mora,
        async:true,
        dataType: "json",                   
        data: ({
                action: "calcular_mora",
                fechaPago: $('#txt_cuopag_fecdep').val(),
                fecha: cuodet_fec
        }),
        success: function(data){
                $('#txt_cuopag_mor').val(data.mora);
                if(data.estado!=1){
                    swal_warning('AVISO',data.msj,6000);
                }
        },
        complete: function(data){			
//                console.log(data);               
        }
    });
}


function Activar_input_monto_pago_banco(){
    $('#txt_cuopag_mon').attr("readonly",false);
}


function generar_nueva_cuota(){
    var vencida=$('#hdd_vencida').val();
    var fecha;
    if(vencida==1){
        fecha=$('#hidden_cuodet_fec').val();
    }
    else{
        fecha=$('#txt_cuopag_fec').val();
    }
    
  $.ajax({
    type: "POST",
    url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
    async:true,
    dataType: "json",                      
    data: ({
      action: "cuota_nueva",
      cre_id: $('#hdd_cre_id').val(),
      cuo_id: $('#hdd_cuo_id').val(),
      int: $('#hdd_cre_int').val(),
      cuo_max: $('#hdd_cuo_max').val(),
      fecha: fecha,
      mon_id: $('#hdd_mon_id').val()
    }),
    beforeSend: function() {
      $('#msj_vencimiento').html("Guardando...");
      $('#msj_vencimiento').show(100);
    },
    success: function(data){
      if(data.estado == 1){
        $('#modal_registro_vencimientomenor').modal('hide');
            swal_success('SISTEMA',data.msj,2000);
        $('#msj_vencimiento').hide();
//        var tipo=$("#action_cuotapago").val();
//            if(tipo=="pagar_fijo")
//            vencimiento_tabla();
      }
      else if(data.estado == 2){
        $('#modal_registro_vencimientomenor').modal('hide');
         swal_warning('AVISO',data.msj,5000);
         $('#msj_vencimiento').hide();
      }
      else{
        $('#modal_registro_vencimientomenor').modal('hide');
        swal_warning('AVISO','Error al momento de generar nueva cuota',5000);
      }
    },
    complete: function(data){
//        console.log(data);
      if(data.statusText != 'success')
        console.log(data);
    }
  });
}

function generar_nueva_cuota_garveh(){
    var vencida=$('#hdd_vencida').val();
    var fecha;
    if(vencida==1){
        fecha=$('#hidden_cuodet_fec').val();
    }
    else{
        fecha=$('#txt_cuopag_fec').val();
    }
    
  $.ajax({
    type: "POST",
    url: VISTA_URL+"vencimiento/cuotapago_garveh_reg.php",
    async:true,
    dataType: "json",                      
    data: ({
      action: "cuota_nueva",
      cuodet_id: $('#hdd_modid').val(),
      cre_id: $('#hdd_cre_id').val(),
      cuo_id: $('#hdd_cuo_id').val(),
      int: $('#hdd_cre_int').val(),
      cuo_max: $('#hdd_cuo_max').val(),
      fecha: fecha,
      mon_id: $('#hdd_mon_id').val()
    }),
    beforeSend: function() {
      $('#msj_vencimiento').html("Guardando...");
      $('#msj_vencimiento').show(100);
    },
    success: function(data){
      if(data.estado == 1){
        $('#modal_registro_vencimientomenor').modal('hide');
            swal_success('SISTEMA',data.msj,2000);
        $('#msj_vencimiento').hide();
//        var tipo=$("#action_cuotapago").val();
//            if(tipo=="pagar_fijo")
//            vencimiento_tabla();
      }
      else if(data.estado == 2){
        $('#modal_registro_vencimientomenor').modal('hide');
         swal_warning('AVISO',data.msj,5000);
         $('#msj_vencimiento').hide();
      }
      else{
        $('#modal_registro_vencimientomenor').modal('hide');
        swal_warning('AVISO','Error al momento de generar nueva cuota',5000);
      }
    },
    complete: function(data){
        console.log(data);
      if(data.statusText != 'success')
        console.log(data);
    }
  });
}

$("#for_cuopag").validate({
        submitHandler: function() {
                $.ajax({
                        type: "POST",
                        url: VISTA_URL+"vencimiento/cuotapago_banco_reg.php",
                        async:true,
                        dataType: "json",
                        data: $("#for_cuopag").serialize(),
                        beforeSend: function() {
                        },
                        success: function(data){
                            
                                if(parseInt(data.cuopag_id) > 0){
                                    $('#modal_registro_vencimientomenorbanco').modal('hide');
                                   swal_success('Sistema',data.cuopag_msj,2000);     
//                                if(confirm('¿Desea imprimir?'))
//                                    cuotapago_imppos_datos_2(data.cuopag_id);
                                    

                                    
                                    var hdd_cuotip_id=$('#hdd_cuotip_id').val();
                                    if(hdd_cuotip_id==1){
                                        generar_nueva_cuota();
                                    }
                                    if(hdd_cuotip_id==3){
                                        generar_nueva_cuota_garveh();
                                    }

                                    var hdd_vista=$('#hdd_vista').val();
                                    if(hdd_vista=='vencimiento_tabla'){
                                        vencimiento_tabla();
                                    }
                                    
                                    var codigo=data.cuopag_id;
                                    window.open("http://www.ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);

                                    var acu_id = Number($('#hdd_cuodet_acupag_id').val());
                                    if(acu_id > 0)
                                      cuotapago_asiveh_form('pagar',acu_id);
                                }
                                else{
                                        swal_warning('AVISO',data.cuopag_msj,10000);
                                }	
                        },
                        complete: function(data){
                                if(data.statusText != "success" || data.statusText != "OK"){
//                                        $('#btn_guar_cuopag').show();
//                                        $('#msj_pagobanco').show(100);
//                                        $('#msj_pagobanco').text('ERROR: ' + data.responseText);
//                                        swal_error('ERROR',data.responseText,10000);
//                                        console.log(data);
                                }
                        }
                });
        },
        rules: {
                txt_cuopag_mon:{
                        required:true
                },
                txt_cuopag_tipcam:{
                        required:true
                },
                txt_cuopag_numope: {
                        required:true
                },
                txt_cuopag_comi:{
                        required: true
                },
                txt_cuopag_monval: {
                        required: true
                },
                cmb_cuedep_id: {
                        required: true
                },
                txt_cuopag_montot: {
                        required: true
                }
        },
        messages: {
                txt_cuopag_mon:{
                        required:'* registre el monto pagado'
                },
                txt_cuopag_tipcam:{
                        required:'* registre el tipo de cambio'
                },
                txt_cuopag_numope: {
                        required: '* registre el numero de operacion'
                },
                txt_cuopag_comi:{
                        required: '* registre la comisión del banco'
                },
                txt_cuopag_monval: {
                        required: '* registre el monto a validar'
                },
                cmb_cuedep_id: {
                        required: '* registre la cuenta de deposito'
                },
                txt_cuopag_montot: {
                        required: '* registre el monto total'
                }
        }
});