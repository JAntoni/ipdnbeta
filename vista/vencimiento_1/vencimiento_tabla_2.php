<?php
if(defined('APP_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
}
else{
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}
require_once ("Vencimiento.class.php");
$oPagocuota = new Vencimiento();
//require_once ("../pagocuota/Pagocuota.class.php");
//$oPagocuota = new Pagocuota();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();

$fecha_hoy = date('d-m-Y');



//$values=$oVencimiento->filtrar_menor($_POST['hdd_fil_cli_id'],$cre_id_cm,fecha_mysql($fecha_hoy));
$result = $oPagocuota->filtrar_menor($_POST['hdd_fil_cli_id'],$cre_id_cm,fecha_mysql($fecha_hoy));

//echo 'numero =='.$result;
//exit();
//$num_rows= mysql_num_rows($values);
if($result['estado'] == 1)
{
?>
    <h3 style="color: #000" class="subtitulo_pago" >CRÉDITO MENOR</h3>
    <table cellspacing="1" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">TIPO</th>
            <th id="tabla_cabecera_fila">N° CUOTA</th>
            <th id="tabla_cabecera_fila">VENCIMIENTO</th>
            <th id="tabla_cabecera_fila">CUOTA</th>                
            <th id="tabla_cabecera_fila">PAGOS</th>
            <th id="tabla_cabecera_fila">SALDO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila"></th>
            <th id="tabla_cabecera_fila">CRÉDITO</th>
            </tr>
        </thead>
        <tbody>
        <?php
//        echo 'resulat1   ==='.$result['data'];exit();
        foreach ($result['data'] as $key => $value) {
            $contar=0;
            $vencida = 1;
            $tipo=$value['tb_cuotatipo_id'];
            
            if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}
        ?>
            <tr class="even" id="tabla_fila">
            <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
            <td id="tabla_fila"><?php echo $value['tb_cuotatipo_nom']?></td>
            <td align="center" id="tabla_fila"><?php if($value['tb_cuotatipo_id']==2){echo $value['tb_cuota_num'].'/'. $value['tb_credito_numcuo'];}else{echo $value['tb_cuota_num'].'/'. $value['tb_credito_numcuomax'];}?></td>
            <td align="center" id="tabla_fila" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
            <?php 
                $pagos_cuota=0;
                $mod_id=1;
                $values3=$oCuotapago->mostrar_cuotapago_ingresos_por_tipo_cuota($mod_id,$value['tb_cuota_id']);
//                $num_dts3= mysql_num_rows($values3);
                if($values3['estado'] == 1)
                {
                    foreach ($values3['data'] as $key => $values) {
                        $pagos_cuota+=$values['tb_ingreso_imp'];
                    }
                }
//                mysql_free_result($values3);
                $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
            ?>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
                if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
                if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                if($value['tb_cuotatipo_id']==1)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==2)$action='pagar_fijo';

                ?>
            </td>
            <td align="left" id="tabla_fila"><?php if($value['tb_cuota_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_menor_form('<?php echo $action?>','<?php echo $value['tb_cuota_id']?>')">Pagar</a>
                <?php }?>
             <?php if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){?> 
              <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="cuotapago_menor_form_liq('<?php echo $action?>','<?php echo $value['tb_cuota_id']?>', 1)">Liquidar</a>
             <?php } else{?> 
              <!--<a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="cuotapago_menor_form_liq('<?php echo $action?>','<?php echo $value['tb_cuota_id']?>', 1)">Liquidar</a>-->
             <?php }?> 
              
            <?php if($value['tb_cuotatipo_id']==2):?>
              
              <?php if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){?> 
                <a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="cuotapago_menor_form_liq('<?php echo $action?>','<?php echo $value['tb_cuota_id']?>', 2)">Amortizar</a>
             
              <?php }else{?> 
                <!--<a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="cuotapago_menor_form_liq('<?php echo $action?>','<?php echo $value['tb_cuota_id']?>', 2)">Amortizar</a>-->
              <?php }?>
                
            <?php endif;?>
                
            </td>
            <td align="center" id="tabla_fila">
                <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="creditomenor_pagos('<?php echo $value['tb_credito_id']?>',1)">Historial</a>
                <!--<a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php //echo $value['tb_credito_id']?>','creditomenor')">CM-<?php //echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a>-->
                <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="creditomenor_form('L',<?php echo $value['tb_credito_id'];?>)">CM-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a>
                
                    <?php if($value['tb_cuotatipo_id']==1){ // AHORA TODOS PUEDEN RENOVAR if($_SESSION['usuariogrupo_id'] == 2):?>
                  <a href="javascript:void(0)" class="btn btn-facebook btn-xs" onclick="agregar_cuotas('<?php echo $value['tb_credito_id']?>')">Cu.</a>
                <?php } //endif;?>
              </td>
            </tr>
        <?php
        
            }
        ?>
        </tbody>
    </table>
<?php
}






$result = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'],2, '1,2,3,4'); //ASIVEH de tipo venta nueva y reventa
    if($result['estado'] == 1){
           foreach($result['data'] as $key => $value){
           $nota_det = $value['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
   $result=NULL;

?>
<?php
$result=$oPagocuota->filtrar_asiveh($_POST['hdd_fil_cli_id'],$cre_id_cav,fecha_mysql($fecha_hoy));
if($result['estado'] == 1)
{
?>

<h3 class="subtitulo_pago">ASISTENCIA VEHICULAR</h3>
<table cellspacing="1" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">N° CUOTA</th>
            <th id="tabla_cabecera_fila">VENCIMIENTO</th>
            <th id="tabla_cabecera_fila">CUOTA</th>
            <th id="tabla_cabecera_fila">PAGOS</th>
            <th id="tabla_cabecera_fila">SALDO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila"></th>
            <th id="tabla_cabecera_fila">CRÉDITO</th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($result['data'] as $key => $value){
        $vencida = 1;
        if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;};
    ?>
        <tr class="odd"  id="tabla_fila">
            <td id="tabla_fila">
             <?php 
                echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];
                if($value['tb_cuota_acupag']>0)
                  echo ' <strong style="color: green;">(AP)</strong>';

                if($value['tb_credito_est'] == 4)
                  echo ' <h2 style="color: red;">(PARALIZADO)</h2>';
             ?>        
            </td>
            <td align="center" id="tabla_fila"><?php echo $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo']?></td>
            <td align="center" id="tabla_fila"<?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
            <?php
                $pagos_cuota=0;
                $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
                if($result3['estado'] == 1){
                    foreach($result3['data'] as $key => $value3){
                        $pagos_cuota+=$value3['tb_ingreso_imp'];
                    }
                }
                $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
            ?>
            <td align="right" id="tabla_fila"style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ". mostrar_moneda($value['tb_cuota_cuo']);?></td>
            <td align="right" id="tabla_fila" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
            <td align="right" id="tabla_fila" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
                if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
                if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                ?>
            </td>
            <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',2)">Historial</a></td>
            <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditoasiveh')">CAV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
        </tr>
    <?php
//     echo 'resulat1   ==='.$result['data'];exit();
        $result2=$oPagocuota->mostrarUnoAsiveh($value['tb_cuota_id']);
        if($result2['estado'] == 1){
        foreach($result2['data'] as $key => $value2){
            $vencida = 1;
            if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;};
    ?>
        <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
                $pagos_cuotadetalle=0;
                $mod_id=2;
                $result4=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
                if($result4['estado'] == 1)
                {
                    foreach($result4['data'] as $key => $value4){
                         $pagos_cuotadetalle+=$value4['tb_ingreso_imp'];
                    }
                }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;">
              <?php                 
                if($value2['tb_cuotadetalle_acupag'] > 0)
                  echo '<strong style="color:green;"> AP '.$value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']).'</strong>';
                else
                  echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']);
              ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                ?>
            </td>
            <td id="tabla_fila" align="center">
                <?php if($value['tb_cuota_est']!=2){?>
                    <a class="btn btn-success btn-xs" href="#pagar" onClick="cuotapago_asiveh_form('pagar','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
                <?php }?>
            </td>
            <td id="tabla_fila"></td>
        </tr>
    <?php
        }
    }
        
    }
    ?>
    </tbody>
</table>
<br>
<?php
}
$result=NULL;

?>
<!-- FILTRAR TODOS LOS ACUERDOS DE PAGOS de ASIVEH-->



<?php
$result=$oPagocuota->filtrar_asiveh_acupag($_POST['hdd_fil_cli_id'],$cre_id_cav, fecha_mysql($fecha_hoy));
if($result['estado'] == 1)
{
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 2, '3'); //ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
           $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
//   $result4=NULL;
?>
<h3 class="subtitulo_pago">ACUERDOS DE PAGO CRÉDITO ASCVEH</h3>
<table cellspacing="1" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">N° CUOTA</th>
            <th id="tabla_cabecera_fila">VENCIMIENTO</th>
            <th id="tabla_cabecera_fila">CUOTA</th>
            <th id="tabla_cabecera_fila">PAGOS</th>
            <th id="tabla_cabecera_fila">SALDO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila"></th>
            <th id="tabla_cabecera_fila">CRÉDITO</th>
        </tr>
    </thead>
    <tbody>
    <?php
//    while($value = mysql_fetch_array($values)){
        foreach($result['data'] as $key => $value){
        $vencida = 1;
        if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;};
    ?>
        <tr class="odd" style="font-weight:bold;" id="tabla_fila">
            <td id="tabla_fila">
             <?php 
                echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];
                if($value['tb_cuota_acupag']>0)
                  echo ' <strong style="color: green;">(AP)</strong>';
             ?>        
            </td>
            <td id="tabla_fila" align="center"><?php echo $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
            <?php
                $pagos_cuota=0;
                $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
                if($result3['estado'] == 1)
                {
                    foreach($result3['data'] as $key => $value3){
                        $pagos_cuota+=$value3['tb_ingreso_imp'];
                    }
                }
                $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
                if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
                if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                ?>
            </td>
            <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',2)">Historial</a></td>
            <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditoasiveh')">CAV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
        </tr>
    <?php
        $result2=$oPagocuota->mostrarUnoAsiveh($value['tb_cuota_id']);
        foreach($result2['data'] as $key => $value2){
            $vencida = 1;
            if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;};
    ?>
        <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
                $pagos_cuotadetalle=0;
                $mod_id=2;
                $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
                if($result3['estado'] == 1){
                   foreach($result3['data'] as $key => $value3){
                         $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                    }
                }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;">
              <?php                 
                if($value2['tb_cuotadetalle_acupag'] > 0)
                  echo '<strong style="color:green;"> AP '.$value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']).'</strong>';
                else
                  echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']);
              ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                ?>
            </td>
            <td id="tabla_fila" align="center">
                <?php if($value['tb_cuota_est']!=2){?>
                    <a class="btn btn-success btn-xs" href="#pagar" onClick="cuotapago_asiveh_form('pagar','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
                <?php }?>
            </td>
            <td id="tabla_fila"></td>
        </tr>
    <?php
        }
    }
    ?>
    </tbody>
</table>
<br>
<?php
}
$result=NULL;

?>
<!-- FIN FILTRO DE ACUERDO DE PAGO-->






<!-- FILTRO DE ADENDAS DE ASIVEH-->
<?php
$result=$oPagocuota->filtrar_asiveh_adendas($_POST['hdd_fil_cli_id'],$cre_id_cav,fecha_mysql($fecha_hoy));

if($result['estado'] == 1){
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 2, '2'); //ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
           $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
    
?>
<h3 class="subtitulo_pago">ADENDAS CRÉDITO ASCVEH</h3>
<table cellspacing="1" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila" align="center">N° CUOTA</th>
            <th id="tabla_cabecera_fila" align="center">VENCIMIENTO</th>
            <th id="tabla_cabecera_fila" align="center">CUOTA</th>
            <th id="tabla_cabecera_fila" align="center">PAGOS</th>
            <th id="tabla_cabecera_fila" align="center">SALDO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila"></th>
            <th id="tabla_cabecera_fila">CRÉDITO</th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($result['data'] as $key => $value){
        $vencida = 1;
        if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;};
    ?>
        <tr class="odd" style="font-weight:bold;" id="tabla_fila">
            <td id="tabla_fila">
             <?php 
                echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];
                if($value['tb_cuota_acupag']>0)
                  echo ' <strong style="color: green;">(AP)</strong>';
                if($value['tb_credito_est'] == 4)
                  echo ' <h2 style="color: red;">(PARALIZADO)</h2>';
             ?>        
            </td>
            <td id="tabla_fila" align="center"><?php echo $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
            <?php
                $pagos_cuota=0;
                $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
                if($result3['estado'] == 1){
                    foreach($result3['data'] as $key => $value3)
                    {
                        $pagos_cuota+=$value3['tb_ingreso_imp'];
                    }
                }
                $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
            <td id="tabla_fila" id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
                if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
                if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                ?>
            </td>
            <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',2)">Historial</a></td>
            <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditoasiveh')">CAV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
        </tr>
    <?php
        $result2=$oPagocuota->mostrarUnoAsiveh($value['tb_cuota_id']);
        if($result2['estado'] == 1){
        foreach($result2['data'] as $key => $value2)
                    {
            $vencida = 1;
            if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;};
    ?>
        <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
                $pagos_cuotadetalle=0;
                $mod_id=2;
                $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
                if($result3['estado'] == 1){
                    foreach($result3['data'] as $key => $value3)
                    {
                         $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                    }
                }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;">
              <?php                 
                if($value2['tb_cuotadetalle_acupag'] > 0)
                  echo '<strong style="color:green;"> AP '.$value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']).'</strong>';
                else
                  echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']);
              ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td id="tabla_fila">
                <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;
                ?>
            </td>
            <td id="tabla_fila" align="center">
                <?php if($value['tb_cuota_est']!=2){?>
                    <a class="btn btn-success btn-xs"  href="javascript:void(0)" onClick="cuotapago_asiveh_form('pagar','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagarrrrrr</a>
                <?php }?>
            </td>
            <td id="tabla_fila"></td>
        </tr>
    <?php
        }
    }
    }
    ?>
    </tbody>
</table>
<br>
<?php

}

?>
<!-- FIN FILTRO DE ADENDAS-->






<!-- FILTRO DE GARANTÍAS VEHICULARES GAAAAAAAAAAAAAAARVEEEEEEEEEEEEEEEEEEEEEH-->
<?php
$result=$oPagocuota->filtrar_garveh($_POST['hdd_fil_cli_id'],$cre_id_cgv,fecha_mysql($fecha_hoy));
//$num_rows= mysql_num_rows($values);
 
 if($result['estado'] == 1){
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 3, '1'); //ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
           $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
    
    
?>
<h3 class="subtitulo_pago">CRÉDITO GARANTÍA VEHICULAR</h3>
<table cellspacing="1" class="table table-hover">
  <thead>
      <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">N° CUOTA</th>
      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
      <th id="tabla_cabecera_fila">CUOTA</th>
      <th id="tabla_cabecera_fila">PAGOS</th>
      <th id="tabla_cabecera_fila">SALDO</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila"></th>
      <th id="tabla_cabecera_fila">CRÉDITO</th>
    </tr>
  </thead>
  <tbody>
   <?php
    foreach($result['data'] as $key => $value){
      $vencida = 1;
      if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}; 

      $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
      if($value['tb_cuotatipo_id'] == 3)
        $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?>

      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
        <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
        <?php
          $pagos_cuota=0;
          $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
//          $num_dts3= mysql_num_rows($values3);
          if($result3['estado'==1]){
            foreach($result3['data'] as $key => $value3){
                $pagos_cuota+=$value3['tb_ingreso_imp'];
            }
          }
          $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila">
          <?php 
            $estado="";
            if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
            if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
            if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',3)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditogarveh')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
      </tr>
      <?php
        $result2=$oPagocuota->mostrarUnoGarveh($value['tb_cuota_id']);

        if($result2['estado']==1){
        foreach ($result2['data'] as $key=> $value2){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;}; ?>
          <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
              $pagos_cuotadetalle=0;
              $mod_id=2;
              $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
              if($result3['estado']==1)
              {
              foreach ($result3['data'] as $key=> $value3){
                     $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                }
              }

              $cuota_detalle = $value2['tb_cuotadetalle_cuo'];
              $saldo_detalle = $value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle;
              if($value['tb_cuotatipo_id'] == 3){
                $cuota_detalle = $value2['tb_cuota_int'];
                $saldo_detalle = $value2['tb_cuota_int'] - $pagos_cuotadetalle;
              }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php                 
                echo $value2['tb_moneda_nom']." ".mostrar_moneda($cuota_detalle); ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($saldo_detalle);?></td>
            <td id="tabla_fila">
              <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;

                if($value['tb_cuotatipo_id']==3)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==4)$action='pagar_fijo';
              ?>
            </td>
            <td id="tabla_fila" align="center">
              <?php if($value2['tb_cuotadetalle_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_garveh_form('<?php echo $action?>','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
              <?php }?>
            </td>
            <td id="tabla_fila"></td>
          </tr>
          <?php   
        }
    }
//        mysql_free_result($values2);
    } ?>
  </tbody>
</table>
<br>
<?php
}
//    echo 'hasta si entra todo normal estado === ...   '.$result['estado'];exit();

//mysql_free_result($values);
?>
<!-- FIN GARNTIAS VEHICULARES GAAAAAAAAAAAAAAARVEEEEEEEEEEEEEEEEEEEEEH-->



<!-- FILTRAR AP DE GARVEH AAAAAAAAPPPPPPPPPP-->
<?php
$result=$oPagocuota->filtrar_garveh_acupag($_POST['hdd_fil_cli_id'],$cre_id_cgv,fecha_mysql($fecha_hoy));
//$num_rows= mysql_num_rows($values);

if($result['estado']==1){
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 3, '3'); //ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
           $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
?>
<h3 class="subtitulo_pago">ACUERDOS DE PAGO CRÉDITO GARVEH</h3>
<table cellspacing="1" class="table table-hover">
  <thead>
      <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">N° CUOTA</th>
      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
      <th id="tabla_cabecera_fila">CUOTA</th>
      <th id="tabla_cabecera_fila">PAGOS</th>
      <th id="tabla_cabecera_fila">SALDO</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila"></th>
      <th id="tabla_cabecera_fila">CRÉDITO</th>
    </tr>
  </thead>
  <tbody>
   <?php
   foreach($result['data'] as $key=> $value){
//   foreach($value = mysql_fetch_array($values)){
      $vencida = 1;
      if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}; 

      $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
      if($value['tb_credito_numcuo'] == 0)
        $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?>

      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila" ><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
        <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
        <?php
          $pagos_cuota=0;
          $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
          
          if($result3['estado']==1){
              foreach ($result3['data'] as $key=> $value3){
                $pagos_cuota+=$value3['tb_ingreso_imp'];
            }
          }
          $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila">
          <?php 
            $estado="";
            if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
            if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
            if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',3)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditogarveh')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
      </tr>
      <?php
        $result2=$oPagocuota->mostrarUnoGarveh($value['tb_cuota_id']);
        if($result2['estado']==1){
            foreach ($result2['data'] as $key=>$value2){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;}; ?>
          <tr class="even">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
              $pagos_cuotadetalle=0;
              $mod_id=2;
              $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
              if($result3['estado']==1)
              {
              foreach ($result3['data'] as $key=>$values3)
                {
                     $pagos_cuotadetalle+=$values3['tb_ingreso_imp'];
                }
              }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php                 
                echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']); ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td id="tabla_fila">
              <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;

                if($value['tb_cuotatipo_id']==3)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==4)$action='pagar_fijo';
              ?>
            </td>
            <td id="tabla_fila" align="center">
              <?php if($value2['tb_cuotadetalle_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_garveh_form('<?php echo $action?>','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
              <?php }?>
            </td>
            <td id="tabla_fila"></td>
          </tr>
          <?php   
        }
   }
    } ?>
  </tbody>
</table>
<br>
<?php
}
?>
<!-- FIN AP GARVEH-->



<!-- FILTRAR ADENDAS DE GARVEH ADENNDAAAAAAAAAAAA-->
<?php
$result=$oPagocuota->filtrar_garveh_adendas($_POST['hdd_fil_cli_id'],$cre_id_cgv,fecha_mysql($fecha_hoy));
if($result['estado']==1)
{
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 3, '2');//ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
            $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
?>
<h3 class="subtitulo_pago">ADENDAS CRÉDITO GARVEH</h3>
<table cellspacing="1" class="table table-hover">
  <thead>
      <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">N° CUOTA</th>
      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
      <th id="tabla_cabecera_fila">CUOTA</th>
      <th id="tabla_cabecera_fila">PAGOS</th>
      <th id="tabla_cabecera_fila">SALDO</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila"></th>
      <th id="tabla_cabecera_fila">CRÉDITO</th>
    </tr>
  </thead>
  <tbody>
   <?php
   foreach ($result['data'] as $key=>$value){
      $vencida = 1;
      if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}; 

      $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
      if($value['tb_credito_numcuo'] == 0)
        $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?>

      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
        <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
        <?php
          $pagos_cuota=0;
          $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
          if($result3['estado']==1){
          foreach ($result3['data'] as $key=>$value3)
            {
                $pagos_cuota+=$value3['tb_ingreso_imp'];
            }
          }
          $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila">
          <?php 
            $estado="";
            if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
            if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
            if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',3)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditogarveh')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
      </tr>
      <?php
        $result2=$oPagocuota->mostrarUnoGarveh($value['tb_cuota_id']);

        if($result2['estado']==1){
            foreach ($result2['data'] as $key=>$value2){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;}; ?>
          <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
              $pagos_cuotadetalle=0;
              $mod_id=2;
              $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
              if($result3['estado']==1)
              {
                  foreach ($result3['data'] as $key=>$value3)
                {
                     $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                }
              }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php                 
                echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']); ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td id="tabla_fila">
              <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;

                if($value['tb_cuotatipo_id']==3)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==4)$action='pagar_fijo';
              ?>
            </td>
            <td id="tabla_fila" align="center">
              <?php if($value2['tb_cuotadetalle_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_garveh_form('<?php echo $action?>','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar garveh</a>
              <?php }?>
            </td>
            <td id="tabla_fila"></td>
          </tr>
          <?php   
        }
        }
    } ?>
  </tbody>
</table>
<br>
<?php
}
?>
<!--FIN ADENDAS DE GARVEH -->








<!-- CREDITO HIOTECARIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO-->
<?php
$result=$oPagocuota->filtrar_hipotecario($_POST['hdd_fil_cli_id'],$cre_id_ch,fecha_mysql($fecha_hoy));

if($result['estado']==1)
{
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 4, '1');//ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
            $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
?>
<h3 class="subtitulo_pago">CRÉDITO HIPOTECARIO</h3>
<table cellspacing="1" class="table table-hover">
  <thead>
      <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">N° CUOTA</th>
      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
      <th id="tabla_cabecera_fila">CUOTA</th>
      <th id="tabla_cabecera_fila">PAGOS</th>
      <th id="tabla_cabecera_fila">SALDO</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila"></th>
      <th id="tabla_cabecera_fila">CRÉDITO</th>
    </tr>
  </thead>
  <tbody>
   <?php
   foreach ($result['data'] as $key=>$value){
      $vencida = 1;
      if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}; 

      $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
      if($value['tb_credito_numcuo'] == 0)
        $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?>

      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
        <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
        <?php
          $pagos_cuota=0;
          $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
          if($result3['estado']==1){
              foreach ($result3['data'] as $key=>$values3)
            {
                $pagos_cuota+=$values3['tb_ingreso_imp'];
            }
          }
          $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila">
          <?php 
            $estado="";
            if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
            if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
            if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',4)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditohipo')">CH-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
      </tr>
      <?php
        $result2=$oPagocuota->mostrarUnoHipo($value['tb_cuota_id']);
          if($result2['estado']==1){ 
            foreach($result2['data']as $key=>$value2){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;}; ?>
          <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
              $pagos_cuotadetalle=0;
              $mod_id=2;
              $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
              if($result3['estado']==1)
              {
                foreach($result3['data']as $key=>$value3){
                     $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                }
              }
              $cuota_detalle = $value2['tb_cuotadetalle_cuo'];
              $saldo_detalle = $value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle;
              if($value['tb_cuotatipo_id'] == 3){
                $cuota_detalle = $value2['tb_cuota_int'];
                $saldo_detalle = $value2['tb_cuota_int'] - $pagos_cuotadetalle;
              }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php                 
                echo $value2['tb_moneda_nom']." ".mostrar_moneda($cuota_detalle); ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($saldo_detalle);?></td>
            <td id="tabla_fila">
              <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;

                if($value['tb_cuotatipo_id']==3)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==4)$action='pagar_fijo';
              ?>
            </td>
            <td id="tabla_fila" align="center">
              <?php if($value2['tb_cuotadetalle_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_hipo_form('<?php echo $action?>','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
              <?php }?>
            </td>
            <td id="tabla_fila"></td>
          </tr>
          <?php   
        }
      }
    } ?>
  </tbody>
</table>
<br>
<?php
}
?>
<!--FIN CREDITO HIPOTECARIO000000000000000000000000000000000000000000000-->




<!-- FILTRAR ACUERDOS DE PAGO DE HIOTECARIO APPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP-->
<?php
$result=$oPagocuota->filtrar_hipotecario_acupag($_POST['hdd_fil_cli_id'],$cre_id_ch,fecha_mysql($fecha_hoy));

if($result['estado']==1)
{
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 4, '3');//ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
            $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
?>
<h3 class="subtitulo_pago">ACUERDOS DE PAGO DE CRÉDITO HIPOTECARIO</h3>
<table cellspacing="1" class="table table-hover">
  <thead>
      <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">N° CUOTA</th>
      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
      <th id="tabla_cabecera_fila">CUOTA</th>
      <th id="tabla_cabecera_fila">PAGOS</th>
      <th id="tabla_cabecera_fila">SALDO</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila"></th>
      <th id="tabla_cabecera_fila">CRÉDITO</th>
    </tr>
  </thead>
  <tbody>
   <?php
    foreach($result['data']as $key=>$value){
      $vencida = 1;
      if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}; 

      $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
      if($value['tb_credito_numcuo'] == 0)
        $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?>

      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
        <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
        <?php
          $pagos_cuota=0;
          $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
          if($result3['estado']==1){
            foreach($result3['data']as $key=>$value3)
            {
                $pagos_cuota+=$value3['tb_ingreso_imp'];
            }
          }
          $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila">
          <?php 
            $estado="";
            if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
            if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
            if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',4)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditohipo')">CH-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
      </tr>
      <?php
        $result2=$oPagocuota->mostrarUnoHipo($value['tb_cuota_id']);
        if($result2['estado']==1){
        foreach($result2['data']as $key=>$value2){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;}; ?>
          <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
              $pagos_cuotadetalle=0;
              $mod_id=2;
              $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
              if($result3['estado']==1)
              {
                foreach($result3['estado'] as $key=>$value3)
                {
                     $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                }
              }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php                 
                echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']); ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td id="tabla_fila">
              <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;

                if($value['tb_cuotatipo_id']==3)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==4)$action='pagar_fijo';
              ?>
            </td>
            <td id="tabla_fila" align="center">
              <?php if($value2['tb_cuotadetalle_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_hipo_form('<?php echo $action?>','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
              <?php }?>
            </td>
            <td id="tabla_fila"></td>
          </tr>
          <?php   
        }
    }
    } ?>
  </tbody>
</table>
<br>
<?php
}
?>
<!--FIN ACUERDOS DE PAGOOOOOOOOOOOOOOOOOOOOOOO HIPOTECARIO APPPPPPPPPPPPPPPPPPPPPPPPPPP-->




<!-- FILTRAR ADENDAS DE PAGO DE HIOTECARIO ADENDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD-->
<?php
$result=$oPagocuota->filtrar_hipotecario_adendas($_POST['hdd_fil_cli_id'],$cre_id_ch,fecha_mysql($fecha_hoy));

if($result['estado']==1)
{
?>
<?php
    $result4 = $oCliente->listar_nota_cliente_credito($_POST['hdd_fil_cli_id'], 4, '2');//ASIVEH de tipo ACUERDO DE PAGO
        if($result4['estado'] == 1){
           foreach($result4['data'] as $key => $value4){
            $nota_det = $value4['tb_notacliente_det'];
           echo '<div class="ui-state-highlight ui-corner-all" style="width:auto; padding:2px; display: block; text-align: left;"><h3>'.$nota_det.'</h3></div>';
         }
   //echo 'hasta aki todo normal ';exit();
   }
?>
<h3 class="subtitulo_pago">ADENDAS DE CRÉDITO HIPOTECARIO</h3>
<table cellspacing="1" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">N° CUOTA</th>
      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
      <th id="tabla_cabecera_fila">CUOTA</th>
      <th id="tabla_cabecera_fila">PAGOS</th>
      <th id="tabla_cabecera_fila">SALDO</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila"></th>
      <th id="tabla_cabecera_fila">CRÉDITO</th>
    </tr>
  </thead>
  <tbody>
   <?php
    foreach($result['data'] as $key=>$value){
      $vencida = 1;
      if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])){$vencida = 0;}; 

      $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
      if($value['tb_credito_numcuo'] == 0)
        $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?>

      <tr class="odd" style="font-weight:bold;" id="tabla_fila">
        <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
        <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
        <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
        <?php
          $pagos_cuota=0;
          $result3=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
          if($result3['estado']==1){
            foreach($result3['data'] as $key=>$value3){
                $pagos_cuota+=$value3['tb_ingreso_imp'];
            }
          }
          $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
        ?>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($value['tb_cuota_cuo']);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($pagos_cuota);?></td>
        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".mostrar_moneda($saldo);?></td>
        <td id="tabla_fila">
          <?php 
            $estado="";
            if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
            if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
            if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" href="#his" onClick="credito_historial('<?php echo $value['tb_credito_id']?>',4)">Historial</a></td>
        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="credito_form('editar','<?php echo $value['tb_credito_id']?>','creditohipo')">CH-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></a></td>
      </tr>
      <?php
        $result2=$oPagocuota->mostrarUnoHipo($value['tb_cuota_id']);
        if($result2['estado']==1){
        foreach($result2['data'] as $key=>$value2){
          $vencida = 1;
          if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])){$vencida = 0;}; ?>
          <tr class="even" id="tabla_fila">
            <td id="tabla_fila" style="padding-left:20px;"><?php echo "-";?></td>
            <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo']?></td>
            <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec'])?></td>
            <?php
              $pagos_cuotadetalle=0;
              $mod_id=2;
              $result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$value2['tb_cuotadetalle_id']);
              if($result3['estado']==1){
               foreach($result3['data'] as $key=>$value3){
                     $pagos_cuotadetalle+=$value3['tb_ingreso_imp'];
                }
              }
            ?>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php                 
                echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']); ?>            
            </td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($pagos_cuotadetalle)?></td>
            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom']." ".mostrar_moneda($value2['tb_cuotadetalle_cuo']-$pagos_cuotadetalle);?></td>
            <td>
              <?php 
                $estado="";
                if($value2['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value2['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value2['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                echo $estado;

                if($value['tb_cuotatipo_id']==3)$action='pagar_libre';
                if($value['tb_cuotatipo_id']==4)$action='pagar_fijo';
              ?>
            </td>
            <td id="tabla_fila" align="center">
              <?php if($value2['tb_cuotadetalle_est']!=2){?>
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_hipo_form('<?php echo $action?>','<?php echo $value2['tb_cuotadetalle_id']?>')">Pagar</a>
              <?php }?>
            </td>
            <td id="tabla_fila"></td>
          </tr>
          <?php   
        }
    }
    } ?>
  </tbody>
</table>
<?php
}
?>
<!--FIN ADENDAS DE PAGOOOOOOOOOOOOOOOOOOOOOOO HIPOTECARIO ADEEEEEEENDDDDDDDDDDDDDDDDDDDDDD-->