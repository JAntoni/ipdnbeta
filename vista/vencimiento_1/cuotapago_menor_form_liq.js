/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    $('.numero').autoNumeric({
	aSep: '',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0',
	vMax: '99999'
    });
    $('.moneda3').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.99'
    });
    
    });

function cmb_cuedep_id(ids)
{	
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cuentadeposito/cuentadeposito_select.php",
		async:false,
		dataType: "html",                      
		data: ({
			cuedep_id: ids
		}),
		beforeSend: function() {
			$('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
    },
		success: function(html){
			$('#cmb_cuedep_id').html(html);
		}
	});
}
function calcular_moras(fecha)
{	
	$.ajax({
		type: "POST",
		url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
		async:true,
		dataType: "json",                      
		data: ({
                    action: "calcular_mora",
                    fechaPago: fecha,
                    fecha: $('#hdd_cuo_fec').val(),
                    cre_preaco: $('#hdd_cre_preaco').val()
		}),
		success: function(data){
//			console.log('mora: ' + data.mora)
                    $('#txt_cuopag_mor').val(data.mora);
                    if(parseInt(data.estado) == 0){
                        //no existe tarifario para esas fechas
//                        $('#msj_menor_form').html(data.msj);
                        swal_warning('AVISO',data.msj,6000);
                        $('#msj_menor_form').show();
                    }
                    //alert(data.mora);
		}, 
	});
}

function calcular_mora(fecha)
{
    var cuodet_fec=$('#hidden_cuodet_fec').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
        async:true,
        dataType: "json",                   
        data: ({
                action: "calcular_mora",
                fechaPago: fecha,
                fecha: cuodet_fec,
                fecha: $('#hdd_cuo_fec').val(),
		cre_preaco: $('#hdd_cre_preaco').val()
        }),
        success: function(data){
                $('#txt_cuopag_mor').val(data.mora);
                if(data.estado!=1){
                    swal_warning('AVISO',data.msj,6000);
                }
        },
        complete: function(data){			
//                console.log(data);               
        }
    });
}

function credito_cronograma()
{	
	$.ajax({
		type: "POST",
		url: VISTA_URL+"creditomenor/credito_cronograma.php",
		async:true,
		dataType: "html",
		data: ({
			cre_preaco: $('#txt_preaco_ref').val(),
			cre_int: $('#txt_interes_ref').val(),
			cre_numcuo: $('#txt_numcuo_ref').val(),
			mon_id: 1,
			cre_fecdes: $('#txt_cuopag_fec').val(),
			cuotip_id: 2
		}),
		beforeSend: function() {
			$('#div_credito_cronograma').html('<span>Cargando...</span>');
        },
		success: function(html){
			$('#div_credito_cronograma').html(html);
		}
	});
}

function calcular_precio_acordado()
{
	var forma = parseInt($('#cmb_forma_pag').val())
	var pagado = 0;
	if(forma == 2)
            pagado = Number($('#txt_cuopag_montot').autoNumeric('get'));
	else
            pagado = Number($('#txt_cuopag_mon_ofi').autoNumeric('get'));

	var cuo_int = Number($('#hdd_cuo_int').val());
	var cuo_cap = Number($('#hdd_cuo_cap').val());

	var preaco = cuo_cap - (pagado - cuo_int);
	
	$('#txt_preaco_ref').autoNumeric('set',preaco.toFixed(2));
	credito_cronograma();
}

function calcular_datos_cuota_fechadeposito(fecha)
{
	$.ajax({
		type: "POST",
		url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
		async:true,
		dataType: "JSON",
		data: ({
			action: 'datos_cuota',
			cuopag_fecval: fecha,
			cuota_id: $('#hdd_modid').val()
		}),
		beforeSend: function() {
			
                },
		success: function(data){
//			console.log(data);
			$('#lbl_dias_pasados').text(data.dias_pasados);
			$('#lbl_pago_anticipado').text(data.pago_anticipado);
			//$('#lbl_mon_pag_ant').text(data.mon_pag_ant);
			$('#lbl_cuo_int').text(data.cuo_int);
			$('#lbl_suma_total').text(data.suma_total);
			$('#txt_cuopag_mon_ofi').val(data.total_pagar);

			$('#hdd_cuo_int').val(data.cuo_int);
			$('#hdd_dias_pasados').val(data.dias_pasados);
			$('#hdd_detalle_ext').val(data.detalle_extra);
		},
		complete: function(data){
		}
	});
}

$(function() {
    
//    $('#'+form_id).find('input, textarea').attr('readonly','readonly');
//    $('option:not(:selected)').attr('disabled', true);
//    form_desabilitar_elementos('banco');
    $('#banco').find('input, textarea').attr('readonly','readonly');
//    $('#banco').prop('disabled', true);
  
    $('#cuota_fil_picker1').datepicker({
     language: 'es',
     autoclose: true,
     format: "dd-mm-yyyy",
     //startDate: "-0d"
     endDate : new Date()
    });
    
    $('#txt_cuopag_fecdep').change(function(event) {
        var forma_pago = parseInt($('#cmb_forma_pag').val());
        if(forma_pago == 2){
            var fecha = $('#txt_cuopag_fecdep').val();
            calcular_datos_cuota_fechadeposito(fecha);
            calcular_mora(fecha);
        }
    });
    
    $('#cmb_forma_pag').change(function(event) {
        var forma_pago = parseInt($('#cmb_forma_pag').val());
        if(forma_pago == 1){
            $('#banco').find('input, textarea').attr('readonly','readonly');
        }
        else{
            $('#banco').find('input, textarea').attr('readonly',false);
        }
    });

	var pc_nom = $('#hdd_vista_pc_nombre').val();
        var pc_mac = $('#hdd_vista_pc_mac').val();
        $('#hdd_pc_nombre').val(pc_nom);
        $('#hdd_pc_mac').val(pc_mac);

	cmb_cuedep_id('1');

	var pc_nom = $('#hdd_vista_pc_nombre').val();
        var pc_mac = $('#hdd_vista_pc_mac').val();
        $('#hdd_pc_nombre').val(pc_nom);
        $('#hdd_pc_mac').val(pc_mac);

//        verificar_vencimiento(1);

	$('#txt_cuopag_cuo').focus();

	calcular_mora($('#txt_cuopag_fec').val());

	$('#txt_cuopag_mon, #txt_cuopag_comi').change(function(event) {
		var monto_deposito = Number($('#txt_cuopag_mon').autoNumeric('get'));
		var monto_comision = Number($('#txt_cuopag_comi').autoNumeric('get'));
		var cuenta_deposito = $('#cmb_cuedep_id').val();
		var saldo = 0;

		saldo = monto_deposito - monto_comision;

		if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
			$('#txt_cuopag_montot').autoNumeric('set',saldo.toFixed(2));
			$('#txt_cuopag_monval').autoNumeric('set',saldo.toFixed(2));
		}
		else{
			$('#txt_cuopag_montot').autoNumeric('set',0);
			$('#txt_cuopag_monval').autoNumeric('set',0);
		}

		if(cuenta_deposito)
			$('#cmb_cuedep_id').change();

		var action = $('#hdd_action').val();
		if(action == 'amortizar')
			calcular_precio_acordado();
	});

	$('#cmb_cuedep_id').change(function(event) {
		var moneda_sel = $(this).find(':selected').data('moneda'); //este tipo de moneda corresponde al tipo de cuenta seleccionada
		var moneda_id = $('#hdd_mon_id').val(); //esta moneda pertenece al credito
		var monto_validar = Number($('#txt_cuopag_monval').autoNumeric('get')); //es el restanto de lo depositado menos la comision que cobra el banco
		var monto_cambio = Number($('#txt_cuopag_tipcam').val()); //monto del tipo de cambio del día del deposito
		var total_pagado = 0;
		if(parseInt(moneda_sel) == 2 && parseInt(moneda_id) == 1){
			total_pagado = parseFloat(monto_validar * monto_cambio).toFixed(2);
			$('#txt_cuopag_montot').autoNumeric('set',total_pagado);
			//console.log('dolar a soles monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
		}
		else if(parseInt(moneda_sel) == 1 && parseInt(moneda_id) == 2){
			total_pagado = parseFloat(monto_validar / monto_cambio).toFixed(2);
			$('#txt_cuopag_montot').autoNumeric('set',total_pagado);
			//console.log('soles a dolares monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
		}
		else
			$('#txt_cuopag_montot').autoNumeric('set',monto_validar);

		var cuenta = $(this).find("option:selected").text();
		$('#hdd_cuenta_dep').val(cuenta);
		$('#hdd_mon_iddep').val(moneda_sel);
	});

	$('#cmb_forma_pag').change(function(event) {
		var forma = parseInt($(this).val())
		if(forma == 2){
//                    console.log("Estoy entrando a este campo");
			$('#banco').attr('disabled', false);
			$('#txt_cuopag_mon_ofi').attr('disabled', true);
		}
		else{
			$('#banco').attr('disabled', true);
			$('#txt_cuopag_mon_ofi').attr('disabled', false);
		}
	});

	$('#txt_cuopag_mon_ofi').change(function(event) {
		var action = $('#hdd_action').val();
		if(action == 'amortizar')
			calcular_precio_acordado();
	});

	$('#txt_preaco_ref, #txt_interes_ref, #txt_numcuo_ref').change(function(event) {
		var action = $('#hdd_action').val();
		if(action == 'amortizar')
			credito_cronograma();
	});

	$("#for_cuopag").validate({
		submitHandler: function() {
                $.ajax({
                        type: "POST",
                        url: VISTA_URL+"vencimiento/cuotapago_menor_reg.php",
                        async:true,
                        dataType: "json",
                        data: $("#for_cuopag").serialize(),
                        beforeSend: function() {
//                            $('#msj_vencimiento').html("Guardando...");
//                            $('#msj_vencimiento').show(100);
                        },
                        success: function(data){	
                            if(parseInt(data.cuopag_id) > 0){
                                $('#modal_registro_vencimiento_liquidar').modal('hide');
//                                if(confirm('¿Desea imprimir?'))
//                                cuotapago_imppos_datos_2(data.cuopag_id);
                                
                            
                                
                                vencimiento_tabla();
                                swal_success('SISTEMA',data.cuopag_msj,2000);
                                var codigo=data.cuopag_id;
                            window.open("http://www.ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);
                            }
                            else{
                            $('#btn_guar_cuopag').show();
                            $('#msj_menor_form').show(100);
                            swal_warning('AVISO',data.cuopag_msj,5000);
                          }
                        },
                        complete: function(data){
                            console.log(data);
                            if(data.statusText != 'success'){
                                    $('#btn_guar_cuopag').show();
                                $('#msj_menor_form').text(data.responseText);
                    //            console.log(data);
                            }
                        }
                });
		},
		rules: {
			txt_cuopag_mon:{
				required: function(){
					if(parseInt($('#cmb_forma_pag').val()) == 2)
						return true;
					else
						return false;
				}
			},
			cmb_forma_pag:{
				required: true,
				min: 1
			},
			txt_preaco_ref:{
				required: function(){
					if($('#hdd_action').val() == 'amortizar')
						return true;
					else
						return false;
				}
			},
			txt_interes_ref:{
				required: function(){
					if($('#hdd_action').val() == 'amortizar')
						return true;
					else
						return false;
				}
			},
			txt_numcuo_ref:{
				required: function(){
					if($('#hdd_action').val() == 'amortizar')
						return true;
					else
						return false;
				}
			}
		},
		messages: {
			txt_cuopag_mon:{
				required:'*'
			},
			cmb_forma_pag:{
				required: 'Seleccione la forma de pago por favor'
			},
			txt_preaco_ref:{
				required: 'No puede estar vacío el monto acordado'
			},
			txt_interes_ref:{
				required: 'No puede estar vacío el interés'
			},
			txt_numcuo_ref:{
				required: 'Ingrese el número de cuotas'
			}
		}
	});
});
