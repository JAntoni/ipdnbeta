<?php
require_once('../../core/usuario_sesion.php');

require_once ("Vencimiento.class.php");
$oVencimiento = new Vencimiento();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();

require_once("../funciones/fechas.php");
require_once("../funciones/funciones.php");

$credito = $_POST['credito'];
$cuota_id = $_POST['cuota_id'];
$url_mora = '';


//solo para creditos mayores se consulta a cuotadetalle, para cuota menor es diferente
if($credito != 'menor'){
         $result = $oCuotadetalle->mostrarUno($cuota_id);
        if($result['estado'] == 1){
            $cuo_id=$result['data']['tb_cuota_id'];
            $fecha = $result['data']['tb_cuotadetalle_fec'];//para verificar si hay un acuerdopago en esta fecha
            $cuodet_fec= mostrar_fecha($result['data']['tb_cuotadetalle_fec']);
            $mon_id=$result['data']['tb_moneda_id'];
            $mon_nom=$result['data']['tb_moneda_nom'];
            $cuodet_num=$result['data']['tb_cuotadetalle_num'];
            $cuodet_cuo = floatval($result['data']['tb_cuotadetalle_cuo']);
            $cuodet_acupag=$result['data']['tb_cuotadetalle_acupag'];//si es 1-> es un acuerdo de pago
        }
        $result =null;
	$moneda = 'S/';
	if($mon_id == 2)
		$moneda = 'US$';

	$fec=$cuodet_fec;
	$result=$oCuota->mostrarUno($cuo_id);
         if($result['estado'] == 1){
            $cre_id=$result['data']['tb_credito_id'];
            $cuo_num=$result['data']['tb_cuota_num'];
            $cuo_persubcuo=$result['data']['tb_cuota_persubcuo'];
            $cuo_int=$result['data']['tb_cuota_int'];
         }

	//obetenemos información de acuerdo de pago y de la cuotadetalle
	if($cuodet_acupag == 1){
	  $dts = $oAcuerdopago->informacion_acuerdopago_por_cuotadetalleid($cuota_id);
	    $dt = mysql_fetch_array($dts);
	    $num_cuo = $dt['tb_acuerdopago_numcuo'];//numero de cuotas del acuerdopago
	    $orden_cuodet =$dt['tb_acuerdodetalle_num'];//numero de orden de la cuotadetalle
	    $instancia = $dt['tb_acuerdopago_ins'];//número de instancia del acuerdopago
	  mysql_free_result($dts);
	}

	$pagos_cuotadetalle = 0;
	$mod_id = 2;//cuotadetalle

	$result3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
	  if($result3['estado']==1)
	  {
	    foreach($result3['data'] as $key=>$value3)
	    {
	      $pagos_cuotadetalle += floatval($value3['tb_ingreso_imp']);
	    }
	  }
	$pagos = $pagos_cuotadetalle;
	$pagos_parciales = mostrar_moneda($pagos);
	$monto_pagar = mostrar_moneda($cuodet_cuo-$pagos);
        
        
}



if($credito == 'asiveh'){
	require_once ("../creditoasiveh/Creditoasiveh.class.php");
	$oCredito = new Creditoasiveh();
	
	$url_mora = 'cuotapago_asiveh_reg.php';
	$nombre_credito  = 'ASISTENCIA VEHICULAR - CUOTA FIJA';

	$dts=$oCredito->mostrarUno($cre_id);
            if($dts['estado']==1){
		$cuotip_id = 4; //para asiveh el tipo de cuotas siempre es fijas
		$cre_numcuo = $dts['data']['tb_credito_numcuo'];
                $cre_tip1 = $dts['data']['tb_credito_tip1'];
		$cli_id=$dts['data']['tb_cliente_id'];
		$cli_doc = $dts['data']['tb_cliente_doc'];
		$cli_nom=$dts['data']['tb_cliente_nom'];
		$cliente=$cli_nom.' | '.$cli_doc;
            }
            $dts=NULL;
            $obs_acupag = '';
            $ap = '';
            $des_numero_cuota = 'N° de Cuota:';

        if($cuodet_acupag == 1){
          $obs_acupag = ' (AP) Instancia 0'.$instancia;
          $des_numero_cuota = 'N° de Cuota AP:';
          $ap = "AP";
        }
        $total=1;
        $mensaje="Asistencia vehicular";

	$codigo = 'CAV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;
	$cuota =$cuo_num.'/'.$cre_numcuo.' ('.$cuodet_num.'/'.$cuo_persubcuo.')';

	if($cre_tip1 == 2){
	  $pag_det="PAGO ADENDA A-".$codigo." N° CUOTA: $cuota";
	}else{
	  $pag_det="PAGO DE CUOTA ".$ap." $codigo N° CUOTA: $cuota";
	}

	if($cuo_num==$cre_numcuo and $cuodet_num==$cuo_persubcuo)
	{
		$ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA: ".$monto_pagar."</span>";
		//$vMax=moneda_mysql($monto_pagar);
		$vMax='99999.00';
	}
	else
	{
		$vMax='99999.00';
	}
}

if($credito == 'garveh') {
	require_once ("../creditogarveh/Creditogarveh.class.php");
	$oCredito = new Creditogarveh();

	$url_mora = 'cuotapago_garveh_reg.php';

	$dts=$oCredito->mostrarUno($cre_id);
    if($dts['estado']==1){
        $cuotip_id=$dts['data']['tb_cuotatipo_id']; //tipo de cuotas 4 cuotas fijas, 3 cuotas libres
        $cuot_subper = $dts['data']['tb_cuotasubperiodo_id'];
        $cuotip_nom=$dts['data']['tb_cuotatipo_nom'];
        $cre_int = $dts['data']['tb_credito_int'];
        $cre_tip = $dts['data']['tb_credito_tip']; //tipo 1 normal, 2 adenda y 3 acuerdo de pago
        $cre_numcuo = $dts['data']['tb_credito_numcuo'];
        $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
        $cli_id=$dts['data']['tb_cliente_id'];
        $cli_tip=$dts['data']['tb_cliente_tip'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom=$dts['data']['tb_cliente_nom'];
        $cliente=$cli_nom.' | '.$cli_doc;
    }
        $dts=NULL;
    //CUANDO EL CREDITO ES LIBRE, TAMBIEN HAY MAS CUOTAS GENERADAS, PARA CAMBIARLAS DE ESTADO VERIFICAMOS QUE NUMERO SON Y DE CUANTAS CUOTAS EN TOTAL HAY
    $total_cuotas_garveh = 0;
    $dts = $oCuota->obtener_cuotas_por_credito(3, $cre_id); //listamos las cuotas del credito 3: GARANTIA VEHICULAR
      $total_cuotas_garveh = count($dts['data']);

    $cuota_ultima = 0; //0 si la cuota es la última, a quien la dejaremos para poder generar otra
    if($cuo_num < $total_cuotas_garveh)
      $cuota_ultima = 1; // 1 si esta cuota está antes de la última, entonces al pagarla la desaparecemos de la lista

    //verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
    $vencida = 0; $fecha_hoy = date('d-m-Y');
    if(strtotime($fecha_hoy) > strtotime($cuodet_fec))
      $vencida = 1;

    $obs_acupag = '';
    $ap = '';
    $des_numero_cuota = 'N° de Cuota:';
    if($cuodet_acupag == 1){
      $obs_acupag = ' (AP) Instancia 0'.$instancia;
      $des_numero_cuota = 'N° de Cuota AP:';
      $ap = "AP";
    }
  
  $codigo='CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;
  
  $total=1;
        $mensaje="Garantia vehicular";

  $nom_subper = 'MES';

  if($cuot_subper == 2)
    $nom_subper = 'QUINCENA';

  if($cuot_subper == 3)
    $nom_subper = 'SEMANA';

  //numero de cuotas
  if($cuotip_id==4)
    $cuota = $nom_subper.' '.$cuodet_num.'/'.$cuo_persubcuo.' ('.$cuo_num.'/'.$cre_numcuomax.')';

  if($cuotip_id==3)
    $cuota = $cuo_num.'/'.$cre_numcuomax;

  if($cre_tip == 2){
    $pag_det="PAGO ADENDA A-".$codigo." N° CUOTA: $cuota";
  }else{
    $pag_det="PAGO DE CUOTA ".$ap." $codigo N° CUOTA: $cuota";
  }

  //pagos parciales
  $pagos_cuotadetalle=0;
  $mod_id=2;//cuotadetalle

  $dts3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
    if($dts3['estado']==1){
        foreach($dts3['data']as $key=>$dt3)
      {
        $pagos_cuotadetalle+=$dt3['tb_ingreso_imp'];
      }

    }

  $pagos=$pagos_cuotadetalle;
  $pagos_parciales= mostrar_moneda($pagos);
  $monto_pagar=mostrar_moneda($cuodet_cuo-$pagos);
  
  
  $diasdelmes= date("d-m-Y",strtotime($cuodet_fec."- 1 month")); 
    $diasdelmes=date('t', strtotime($diasdelmes));
    $fecha_inicial=date("d-m-Y",strtotime($cuodet_fec."- 1 month")); 
    $fecha_final=date('Y-m-d');
    $dias = (strtotime($fecha_inicial)-strtotime($fecha_final))/86400;
    $dias = abs($dias); 
    $dias = floor($dias);
    $diastranscurridos= $dias;

  $pago_minimo = mostrar_moneda($cuo_int - $pagos);
  if($pago_minimo < 0)
    $pago_minimo = 0;

  $nombre_credito  = '';
  if($cuotip_id==4)//fijo
  {
  	$nombre_credito  = 'GARANTÍA VEHICULAR - CUOTA FIJA';
    if($cuo_num==$cre_numcuo)
    {
      $ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
      //$vMax = moneda_mysql($monto_pagar);
      $vMax='99999.00';
    }
    else
    {
      $vMax='99999.00';
    }
    
  }

  if($cuotip_id==3)//libre
  {
  	$nombre_credito  = 'GARANTÍA VEHICULAR - CUOTA LIBRE';

    //$vMax=moneda_mysql($monto_pagar);
    $vMax='99999.00';

    if($cuo_num==$cre_numcuomax)
    {
      $ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
      
    }
    else
    {
      //$vMax='99999.00';
    }
    if($vencida == 1)
      $vMax=moneda_mysql($cuo_int-$pagos);
  }
}

if($credito == 'hipo'){
	require_once ("../creditohipo/cCredito.php");
	$oCredito = new cCredito();

	$url_mora = 'cuotapago_hipo_reg.php';

	$dts=$oCredito->mostrarUno($cre_id);
    $dt = mysql_fetch_array($dts);
    $cuotip_id=$dt['tb_cuotatipo_id'];
    $cuot_subper = $dt['tb_cuotasubperiodo_id'];
    $cuotip_nom=$dt['tb_cuotatipo_nom'];
    $cre_int = $dt['tb_credito_int'];
    $cre_tip = $dt['tb_credito_tip']; //tipo 1 normal, 2 adenda y 3 acuerdo de pago
    $cre_numcuo = $dt['tb_credito_numcuo'];
    $cre_numcuomax = $dt['tb_credito_numcuomax'];
    $cli_id=$dt['tb_cliente_id'];
    $cli_tip=$dt['tb_cliente_tip'];
    $cli_doc = $dt['tb_cliente_doc'];
    $cli_nom=$dt['tb_cliente_nom'];
    $cliente=$cli_nom.' | '.$cli_doc;
  mysql_free_result($dts);

  //CUANDO EL CREDITO ES LIBRE, TAMBIEN HAY MAS CUOTAS GENERADAS, PARA CAMBIARLAS DE ESTADO VERIFICAMOS QUE NUMERO SON Y DE CUANTAS CUOTAS EN TOTAL HAY
  $total_cuotas_hipo = 0;
  $dts = $oCuota->obtener_cuotas_por_credito(4, $cre_id); //listamos las cuotas del credito 4: HIPOTECARIO
    $total_cuotas_hipo = mysql_num_rows($dts);
  mysql_free_result($dts);

  $cuota_ultima = 0; //0 si la cuota es la última, a quien la dejaremos para poder generar otra
  if($cuo_num < $total_cuotas_hipo)
    $cuota_ultima = 1; // 1 si esta cuota está antes de la última, entonces al pagarla la desaparecemos de la lista

  //verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
  $vencida = 0; $fecha_hoy = date('d-m-Y');
  if(strtotime($fecha_hoy) > strtotime($cuodet_fec))
    $vencida = 1;
  
  $obs_acupag = '';
  $ap = '';
  $des_numero_cuota = 'N° de Cuota:';
  if($cuodet_acupag == 1){
    $obs_acupag = ' (AP) Instancia 0'.$instancia;
    $des_numero_cuota = 'N° de Cuota AP:';
    $ap = "AP";
  }

  $codigo='CH-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;

  $nom_subper = 'MES';

  if($cuot_subper == 2)
    $nom_subper = 'QUINCENA';

  if($cuot_subper == 3)
    $nom_subper = 'SEMANA';

  //numero de cuotas
  if($cuotip_id==4)
    $cuota = $nom_subper.' '.$cuodet_num.'/'.$cuo_persubcuo.' ('.$cuo_num.'/'.$cre_numcuo.')';

  if($cuotip_id==3)
    $cuota = $cuo_num.'/'.$cre_numcuomax;

  if($cre_tip == 2){
    $pag_det="PAGO ADENDA A-".$codigo." N° CUOTA: $cuota";
  }else{
    $pag_det="PAGO DE CUOTA ".$ap." $codigo N° CUOTA: $cuota";
  }

  //pagos parciales
  $pagos_cuotadetalle=0;
  $mod_id=2;//cuotadetalle

  $dts3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
    $num_dts3= mysql_num_rows($dts3);
    if($num_dts3>0)
    {
      while($dt3 = mysql_fetch_array($dts3))
      {
        $pagos_cuotadetalle+=$dt3['tb_ingreso_imp'];
      }
    }
  mysql_free_result($dts3);

  $pagos=$pagos_cuotadetalle;
  $pagos_parciales=formato_money($pagos);
  $monto_pagar=formato_money($cuodet_cuo-$pagos);

  $pago_minimo = formato_money($cuo_int - $pagos);
  if($pago_minimo < 0)
    $pago_minimo = 0;

  $nombre_credito  = '';
  if($cuotip_id==4)//fijo
  {
  	$nombre_credito  = 'CRÉDITO HIPOTECARIO - CUOTA FIJA';

    if($cuo_num==$cre_numcuo)
    {
      $ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
      //$vMax=moneda_mysql($monto_pagar);
      $vMax='99999.00';
    }
    else
    {
      $vMax='99999.00';
    }
    
  }

  if($cuotip_id==3)//libre
  {
  	$nombre_credito  = 'CRÉDITO HIPOTECARIO - CUOTA LIBRE';

    //$vMax=moneda_mysql($monto_pagar);
    $vMax='99999.00';

    if($cuo_num==$cre_numcuomax)
    {
      $ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
      
    }
    else
    {
      //$vMax='99999.00';
    }
    if($vencida == 1)
      $vMax=moneda_mysql($cuo_int-$pagos);
  }
}

if($credito == 'menor'){
   
	require_once ("../creditomenor/Creditomenor.class.php");
	$oCredito = new Creditomenor();

	$url_mora = 'cuotapago_menor_reg.php';

        $result =$oCuota->mostrarUno($cuota_id);
        if($result['estado'] == 1){
            $cuo_id = $result['data']['tb_cuota_id'];
            $cre_id=$result['data']['tb_credito_id'];
            $cuodet_fec= mostrar_fecha($result['data']['tb_cuota_fec']);
            $cuo_num=$result['data']['tb_cuota_num'];
            $mon_id=$result['data']['tb_moneda_id'];
            $mon_nom=$result['data']['tb_moneda_nom'];
            $cuodet_cuo=$result['data']['tb_cuota_cuo'];
            $cuo_int=$result['data']['tb_cuota_int'];
            $cuo_cap=$result['data']['tb_cuota_cap'];
            $cuo_persubcuo=$result['data']['tb_cuota_persubcuo'];
            $cuo_permiso = $result['data']['tb_cuota_per']; //permiso para poder hacer pagos sobre esta cuota, 0 solo el admin, 1 tienen todos el permiso
        }
        $result =null;
        

	$moneda = 'S/';
	if($mon_id == 2)
		$moneda = 'US$';

	//verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
  $vencida = 0; $fecha_hoy = date('d-m-Y');
  if(strtotime($fecha_hoy) > strtotime($cuodet_fec))
    $vencida = 1;
        
        $result =$oCredito->mostrarUno($cre_id);
        if($result['estado'] == 1){
            $cuotip_id=$result['data']['tb_cuotatipo_id'];
            $cuotip_nom=$result['data']['tb_cuotatipo_nom'];
            $cre_int = $result['data']['tb_credito_int'];
            $cre_numcuo = $result['data']['tb_credito_numcuo'];
            $cre_numcuomax = $result['data']['tb_credito_numcuomax'];
            $cli_id=$result['data']['tb_cliente_id'];
            $cli_doc = $result['data']['tb_cliente_doc'];
            $cli_nom=$result['data']['tb_cliente_nom'];
            $cliente=$cli_nom.' | '.$cli_doc;
        }
        $result =null;
        
        $total=0;
        if($cuotip_id==1) {
            $resultado=$oVencimiento->nro_cuotas_Pendientes($cli_id, fecha_mysql($cuodet_fec),$cre_id);
            if($resultado['estado']=1){
               $total=$resultado['data']['cuotas'];
            }
            if($total>0){
                $mensaje.= 'USTED TIENE '.($total).' CUOTA(S) VENCIDAS.';
                $mensaje.= ' PARA PODER CANCELAR ESTA CUOTA DEBE DE CANCELAR LA(S) CUOTA(S) ANTERIOR(ES)';
            }
            $resultado=NULL;
        }             
	$codigo='CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);

	if($cuotip_id==2)$cuota=$cuo_num.'/'.$cre_numcuo.'';
	if($cuotip_id==1)$cuota=$cuo_num.'/'.$cuo_num.' ('.$cre_numcuomax.')';

	$pag_det="PAGO DE CUOTA $codigo N° CUOTA: $cuota";

  //pagos parciales
	$pagos_cuota=0;
  $mod_id=1;

  $result=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
  if($result['estado']==1)
  {
    foreach($result['data'] as $key=>$value3)
    {
         $pagos_cuota+=$value3['tb_ingreso_imp'];
    }

  }
  $pagos=$pagos_cuota;
  $pagos_parciales= mostrar_moneda($pagos);
  
  
    $diasdelmes= date("d-m-Y",strtotime($cuodet_fec."- 1 month")); //calcula el numero de dias que trae el men
    $diasdelmes=date('t', strtotime($diasdelmes));
    $fecha_inicial=date("d-m-Y",strtotime($cuodet_fec."- 1 month")); 
    $fecha_final=date('d-m-Y');
    $dias = (strtotime($fecha_inicial)-strtotime($fecha_final))/86400;
    $dias = abs($dias); 
    $dias = floor($dias);
    $diastranscurridos= $dias;
    

  $pago_minimo = mostrar_moneda($cuo_int - $pagos);
  if($pago_minimo < 0)
    $pago_minimo = 0;

	$monto_pagar=mostrar_moneda($cuodet_cuo-$pagos);

  $ultimo_pago = 0;
  $nombre_credito = '';
	if($cuotip_id==2)//fijo
	{
		$nombre_credito  = 'CRÉDITO MENOR - CUOTA FIJA';

		if($cuo_num==$cre_numcuo)
		{
      $ultimo_pago = 1;
			$ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
			//$vMax=moneda_mysql($monto_pagar);
			$vMax='99999.00';
		}
		else
		{
			$vMax='99999.00';
		}
		
	}

	if($cuotip_id==1)//libre
	{
		$nombre_credito  = 'CRÉDITO MENOR - CUOTA LIBRE';

		$vMax=moneda_mysql($monto_pagar);
		if($cuo_num==$cre_numcuomax)
		{
      $ultimo_pago = 1;
			$ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";		
		}
		else
		{
			//$vMax='99999.00';
		}
		
	}
}


?>

<?php if($total==0){?>
<!--<?//php if($total>0){?>-->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenorbanco" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">INFORMACIÓN DE CUOTA A PAGAR  FECHA <?php echo 'pago_minimo='.$pago_minimo.' $diasdelmes='.$diasdelmes.' $diastranscurridos='.$diastranscurridos.' Pagos='.$pagos?></h4>
        </div>
        <form id="for_cuopag">
            <input type="hidden" id="hdd_cuo_per" value="<?php echo $cuo_permiso;?>">
            <input type="hidden" id="hdd_vencida" value="<?php echo $vencida?>">
            <input type="hidden" id="hdd_cuotip_id" value="<?php echo $cuotip_id?>">
            <input type="hidden" id="hdd_usuariogrupo" value="<?php echo $_SESSION['usuariogrupo_id'];?>">
            <input type="hidden" id="hdd_cuota_ultima" name="hdd_cuota_ultima" value="<?php echo $cuota_ultima; ?>">

            <input type="hidden" id="hdd_vista" value="<?php echo $_POST['vista']?>">
            <input type="hidden" name="hdd_acupag" id="hdd_acupag" value="<?php echo $cuodet_acupag?>">
            <input type="hidden" name="hdd_pag_det" value="<?php echo $pag_det;?>">
            <input type="hidden" name="hdd_mon_id" id="hdd_mon_id" value="<?php echo $mon_id;?>">
            <input type="hidden" name="hdd_credito" value="<?php echo $credito;?>">
            <input type="hidden" name="hdd_mod_id" value="<?php echo $mod_id;?>">
            <input type="hidden" name="hdd_modid" id="hdd_modid" value="<?php echo $cuota_id;?>">
            <input type="hidden" name="hdd_cuo_id" id="hdd_cuo_id" value="<?php echo $cuo_id;?>">
            <input type="hidden" name="hdd_cre_id" id="hdd_cre_id" value="<?php echo $cre_id;?>">
            <input type="hidden" name="hdd_cli_id" value="<?php echo $cli_id;?>">
            <input type="hidden" name="hdd_cuenta_dep" id="hdd_cuenta_dep">
            <input type="hidden" name="hdd_mon_iddep" id="hdd_mon_iddep">
            <input type="hidden" name="hdd_cuotatip_id" value="<?php echo $cuotip_id;?>">
            <input type="hidden" name="hdd_ultimo_pago" value="<?php echo $ultimo_pago;?>">
            <input type="hidden" name="hidden_url_mora" id="hidden_url_mora" value="<?php echo $url_mora;?>">
            <input type="hidden" name="hidden_cuodet_fec" id="hidden_cuodet_fec" value="<?php echo $cuodet_fec;?>">
            <input name="hdd_cuo_num" id="hdd_cuo_num" type="hidden" value="<?php echo $cuo_num ?>">
            <input name="hdd_cuo_max" id="hdd_cuo_max" type="hidden" value="<?php echo $cre_numcuomax?>">
            <input name="hdd_cre_int" id="hdd_cre_int" type="hidden" value="<?php echo $cre_int ?>">

            <input type="hidden" name="hdd_pc_nombre" id="hdd_pc_nombre">
            <input type="hidden" name="hdd_pc_mac" id="hdd_pc_mac">   
            
            <div class="modal-body" style="font-family: cambria">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Cliente:</label>
                                    <div class="col-md-8">
                                      <?php echo $cliente?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Cambio:</label>
                                    <div class="col-md-5">
                                      <input  class="form-control input-sm" name="txt_cuopag_tipcam" type="text" value="<?php echo $cuopag_tipcam?>" id="txt_cuopag_tipcam" style="text-align:right;font-weight: bold;font-size: 15px" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Crédito:</label>
                                    <div class="col-md-8">
                                      <?php echo $nombre_credito;?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Mora:</label>
                                    <div class="col-md-5">
                                        <input  class="form-control input-sm" style="text-align:right;font-weight: bold;font-size: 15px;color: #cc0000" name="txt_cuopag_mor" type="text" id="txt_cuopag_mor" value="" maxlength="10" readonly>
                                    </div>
                                    <div class="col-md-3"> 
                                      <input name="chk_mor_aut" type="checkbox" id="chk_mor_aut" value="1"> Modificar
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Código:</label>
                                    <div class="col-md-8">
                                      <?php echo $codigo?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° de Cuota:</label>
                                    <div class="col-md-8">
                                      <?php echo $cuota.$ultima_cuota;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha de Vencimiento:</label>
                                    <div class="col-md-8">
                                      <?php echo $cuodet_fec; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Cuota <?php echo $moneda;?>:</label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control input-sm moneda3" name="txt_cuodet_cuo" id="txt_cuodet_cuo" value="<?php echo mostrar_moneda($cuodet_cuo);?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Pagos Parciales:</label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control input-sm moneda3" name="txt_cuopag_pagpar" id="txt_cuopag_pagpar" value="<?php echo $pagos_parciales;?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Total Pagar <?php echo $moneda;?>:</label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control input-sm moneda3" name="txt_cuopag_tot" id="txt_cuopag_tot" value="<?php echo $monto_pagar;?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php //31-01-2022     >       28-02-2022
                            $pago_minimo_hasta_fecha=$pago_minimo;
                            if(strtotime($fecha_hoy) > strtotime($cuodet_fec)){  
//                                $pago_minimo=0.00;
                            }
                                else{
                                   $pago_minimo_hasta_fecha= (moneda_mysql($pago_minimo)/$diasdelmes)*$diastranscurridos-$pagos;
                                }
                                
                                if($cuotip_id==2)//fijo)
                                    $pago_minimo_hasta_fecha=$monto_pagar;
                            ?>
                            <?php if($cuotip_id == 3 || $cuotip_id == 1):?>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label" style="text-align: right">Pago Mínimo <?php echo $moneda;?>:</label>
                                        <div class="col-md-8">
                                          <input type="text" class="form-control input-sm moneda3" name="txt_cuopag_min" id="txt_cuopag_min" value="<?php echo $pago_minimo_hasta_fecha;?>" readonly style="text-align:right;font-weight: bold;font-size: 15px">
                                        </div>
                                    </div>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
                
                <!--Segunda Parte-->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha Depósito:</label>
                                    <div class="col-md-8">
                                        <div class='input-group date' id='ingreso_fil_picker1'>
                                            <input type="text" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" class="form-control input-sm" readonly value="<?php echo date('d-m-Y');?>">
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha Validación:</label>
                                    <div class="col-md-8">
                                        <div class='input-group date' id='ingreso_fil_picker2'>
                                            <input type="text" name="txt_cuopag_fec" id="txt_cuopag_fec" class="form-control input-sm" readonly value="<?php echo date('d-m-Y');?>">
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° Operación:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control input-sm" id="txt_cuopag_numope" name="txt_cuopag_numope">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php 
                        if(strtotime($fecha_hoy) > strtotime($cuodet_fec)){
                            $readonly='readonly=""';
                        }
                        else{
                           $readonly='';
                        }
                        ?>
                        
                        
                        <div class="row">
<!--                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Depósito:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $pago_minimo_hasta_fecha?>" <?php echo $readonly?> >
                                    </div>
                                </div>
                            </div>-->
                            
                            
                            <div class="col-md-4">
                                <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Depósito:</label>
                                <div class="col-md-8">  
                                    <div class="input-group">

                                       <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $pago_minimo_hasta_fecha?>" <?php echo $readonly?> >
                                       <span class="input-group-btn">
                                           <?php
                                           $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
                                           if ($usuariogrupo_id==2) {?>
                                           <button class="btn btn-success btn-sm" type="button"  onClick="Activar_input_monto_pago_banco()" title="Activar Casilla para ingresar Monto">
                                               <span class="fa fa-check-square-o"></span>
                                           </button>
                                           <?php } ?>
                                       </span>
                                   </div>
                               </div>
                            </div>
                            
                            
                            
                            
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Comisión Banco:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" maxlength="10" value="0.00">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Validar:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" maxlength="10" readonly value="<?php echo $pago_minimo_hasta_fecha?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° Cuenta:</label>
                                    <div class="col-md-8">
                                        <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                                            <?php // require_once '../cuentadeposito/cuentadeposito_select.php';?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">En Banco:</label>
                                    <div class="col-md-8">
                                        <select name="cmb_banco_id" class="form-control input-sm">
                                            <option value="">--</option>
                                            <option value="1" selected="true">Banco BCP</option>
                                            <option value="2">Banco Interbak</option>
                                            <option value="3">Banco BBVA</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Pagado:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda4 form-control input-sm" name="txt_cuopag_montot" type="text" id="txt_cuopag_montot" value="<?php echo $pago_minimo_hasta_fecha?>"  maxlength="10" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="text-align: right">Observación:</label>
                                    <div class="col-md-10">
                                        <textarea type="text" name="txt_cuopag_obs" rows="2" cols="50" class="form-control input-sm mayus"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                        <div class="row">
                            <div class="col-md-12">
                                <p>(*) Recuerde: si esta es una cuota libre atrazada, solo debe pagar el monto exacto como indica en el campo: Pago Mínimo,
                                    ese mismo monto debe ingresarlo en el campo: Monto Depósito. Si usted ingresa más, el monto validado le devolverá cero,
                                    por lo que solo debe seguir estas recomendaciones.</p>
                            </div>
                            <div class="col-md-12">
                                <?php 
                                    if($cuotip_id == 1 && $vencida == 1) {
                                      echo '
                                        <br/>
                                        <strong style="font-weight: bold; font-size: 14px; color: red;">Esta cuota ya está vencida, para poder seguir abonando genere nueva cuota o pida al administrador que habilite esta cuota.</strong>
                                        <br/><br>';
//                                      echo '<a href="#nueva" class="btn_accion" id="btn_habilitar" onclick="verificar_vencimiento(2)">Habilitar Pago</a>';

//                                      if($_SESSION['usuariogrupo_id'] == 2)
//                                        echo '<a href="#nueva" style="float: right;" class="btn_accion" onclick="permiso_cuotadetalle(1)">Habilitar Para Vendedores</a>';
                                            }
                                  ?>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div id="msj_pagobanco" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <div class="f1-buttons">
                <?php
                if($cuo_num==$cre_numcuo)
                    {
                    ?>
                    <strong style="font-weight: bold; font-size: 14px; color: red;">Esta es la ultima cuota del credito por lo tando debe Habilitar 2 cuotas mas antes de proceder con el cobro.</strong>
                
                <?php
                    }
                    else{
                        echo '<button type="submit" class="btn btn-info" id="btn_guar_cuopag">Guardar</button>';
                    }
                    ?>
              
                
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>   
        </div>
    </div>
</div>
<?php 

}else { ?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenorbanco" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">MENSAJE IMPORTANTE</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
              <p  style="font-family: cambria;font-size: 15px;color: #cc0000"><?php echo $mensaje;?></p>
          </div>
        </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>         
            </div>
        </div>
    </div>
</div>

<?php } ?>

<script type="text/javascript" src="<?php echo 'vista/vencimiento/cuotapago_banco.js?ver=35232567547757547';?>"></script>