<?php
require_once('../../core/usuario_sesion.php');

require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../tareainventario/Tareainventario.class.php');
$oTarea = new Tareainventario();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../tarifario/Tarifario.class.php");
$oTarifario = new Tarifario();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$nombre_pc = (!empty($_POST['hdd_pc_nombre']))? $_POST['hdd_pc_nombre'] : 'SIN NOMBRE PC';
$mac_pc = (!empty($_POST['hdd_pc_mac']))? $_POST['hdd_pc_mac'] : 'SIN NOMBRE MAC';

$moneda_cre = 'S/.';
if($_POST['hdd_mon_id'] == 2)
  $moneda_cre = 'US$';

$moneda_selec = 'S/.';
if($_POST['cmb_mon_id'] == 2)
  $moneda_selec = 'US$';

$cuopag_his = ''; //registramos todo lo que sucede al pagar una cuota
$cuopag_his .= 'Este pago fue realizado con asistencia del cliente a oficinas, registrado por: '.trim($_SESSION['usuario_nom']).', en la fecha de: '.date('d-m-Y h:i a').', desde la computadora con nombre: '.$nombre_pc.' y MAC: '.$mac_pc.' ||&& ';





$cuota_id=$_POST['hdd_modid'];
$mod_id=1;

$result=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
  if($result['estado']==1)
  {
    foreach($result['data'] as $key=>$value3)
    {
         $pagos_cuota+=formato_moneda($value3['tb_ingreso_imp']);
    }

  }


$resultcuo=$oCuota->mostrarUno($cuota_id);

if($resultcuo['estado']==1){
    $cuopag_tot1 = formato_moneda($resultcuo['data']['tb_cuota_cuo']);
    $cuo_int=formato_moneda($resultcuo['data']['tb_cuota_int']);
    $cuo_fec=$resultcuo['data']['tb_cuota_fec'];
}
    $pagos=$pagos_cuota;
    $cuopag_tot= $cuopag_tot1 - $pagos;
 
    

    
    $diasdelmes= date("d-m-Y",strtotime($cuo_fec."- 1 month")); 
    
    $diasdelmes=date('t', strtotime($diasdelmes));
    
    $fecha_inicial=date("d-m-Y",strtotime($cuo_fec."- 1 month")); 
    $fecha_final=date('d-m-Y');//
    

    $dias = (strtotime($fecha_inicial)-strtotime($fecha_final))/86400;
    $dias = abs($dias); 
    $dias = floor($dias);
    $diastranscurridos= $dias;

   $fecha_hoy = date('d-m-Y');
   $vencida = 0;
    if(strtotime($fecha_hoy) > strtotime($cuo_fec))
    $vencida = 1;
    
 
   $pago_minimo = ($cuo_int - $pagos);
   if($vencida==0){
       if(strtotime($fecha_hoy) > strtotime($cuo_fec)){  
           
       }
        else{
           $pago_minimo= ((($cuo_int/$diasdelmes)*$diastranscurridos)-$pagos);
        }
   }
  if($pago_minimo < 0)
    $pago_minimo = 0;
  
  $monto_pagado_activado_campo= formato_moneda($_POST['txt_cuopag_mon']);
$pago_minimo_mostrado_sistema= formato_moneda($pago_minimo);

  
// echo '$cuopag_tot = '.$cuopag_tot;exit();
 
// $data['cuopag_id'] = 0;
//	$data['cuopag_msj'] = '$cuopag_tot = '.$cuopag_tot.' ANd $pago_minimo='.$pago_minimo .' diastranscurridos= '.$diastranscurridos.' $vencida= '.$vencida;
//	echo json_encode($data);
//	exit();


//echo 'usuario ====  '.$_SESSION['usuario_id'];exit();

if($_POST['action_cuotapago']=="pagar_fijo"){
    
    $hoy = date('d-m-Y');
    $fec_pago = $_POST['txt_cuopag_fec'];

  if($hoy != $fec_pago){
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
    echo json_encode($data);
    exit();
  }

	if(!empty($_POST['txt_cuopag_mon'])){
            $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
            $oCuotapago->tb_cuotapago_xac=1;    
            $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
            $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
            $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
            $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
            $oCuotapago->tb_cuotapago_fec=$fecha; 
            $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
            $oCuotapago->tb_cuotapago_mon=moneda_mysql($_POST['txt_cuopag_mon']);
            $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
            $oCuotapago->tb_cuotapago_moncam=moneda_mysql($_POST['txt_cuopag_moncam']);
            $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($_POST['txt_cuopag_mon']);
            $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
            $oCuotapago->tb_cuotapago_obs='';
            
            $result=$oCuotapago->insertar();
            if(intval($result['estado']) == 1){
                $cuopag_id = $result['tb_cuotapago_id'];
            }
		//estado de cuota
		if(moneda_mysql($_POST['txt_cuopag_mon'])==moneda_mysql($cuopag_tot))
		if(moneda_mysql($_POST['txt_cuopag_mon'])==moneda_mysql($_POST['txt_cuopag_tot']))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
			//registro de mora
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
			if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut','1','INT');
			if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
				$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','1','INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
			}

//			$monto=moneda_mysql($_POST['txt_cuopag_tot']);
			$monto=moneda_mysql($cuopag_tot);
			if($monto>0)$ingresar=1;
		}

//		if(moneda_mysql($_POST['txt_cuopag_mon'])<moneda_mysql($_POST['txt_cuopag_tot']))
		if(moneda_mysql($_POST['txt_cuopag_mon'])<moneda_mysql($cuopag_tot))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','3','INT');
			
			$monto=moneda_mysql($_POST['txt_cuopag_mon']);
			if($monto>0)$ingresar=1;
		}

//		if(moneda_mysql($_POST['txt_cuopag_mon'])>moneda_mysql($_POST['txt_cuopag_tot']))
		if(moneda_mysql($_POST['txt_cuopag_mon'])>moneda_mysql($cuopag_tot))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',2,'INT');
			//registro de mora
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
			if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut','1');
			if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
				$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest',1,'INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
			}
			
//			$monto=moneda_mysql($_POST['txt_cuopag_tot']);
			$monto=moneda_mysql($cuopag_tot);
			if($monto>0)$ingresar=1;

//			$abono=moneda_mysql($_POST['txt_cuopag_mon'])-moneda_mysql($_POST['txt_cuopag_tot']);
			$abono=moneda_mysql($_POST['txt_cuopag_mon'])-moneda_mysql($cuopag_tot);
			if($abono>0)$abonar=1;
		}
                /* MODIFICADO HASTA EL MOMENTO*/
		if($ingresar==1){
		//registro de ingreso
                $cue_id=1; //CANCEL CUOTAS
                $subcue_id=2; //CUOTAS menor
                $mod_id=30; //cuota pago
                $ing_det=$_POST['hdd_pag_det'];
                $numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;
                        
                    $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_fec = $fecha; 
                    $oIngreso->documento_id = 8; 
                    $oIngreso->ingreso_numdoc = $numdoc;
                    $oIngreso->ingreso_det = $ing_det; 
                    $oIngreso->ingreso_imp = moneda_mysql($monto);
                    $oIngreso->cuenta_id = $cue_id;
                    $oIngreso->subcuenta_id = $subcue_id; 
                    $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                    $oIngreso->caja_id = 1; 
                    $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                    //valores que pueden ser cambiantes según requerimiento de ingreso
                    $oIngreso->modulo_id = $mod_id; 
                    $oIngreso->ingreso_modide = $cuopag_id; 
                    $oIngreso->empresa_id = $_SESSION['empresa_id'];
                    $oIngreso->ingreso_fecdep = ''; 
                    $oIngreso->ingreso_numope = ''; 
                    $oIngreso->ingreso_mondep = 0; 
                    $oIngreso->ingreso_comi = 0; 
                    $oIngreso->cuentadeposito_id = 0; 
                    $oIngreso->banco_id = 0; 
                    $oIngreso->ingreso_ap = 0; 
                    $oIngreso->ingreso_detex = '';

                    $oIngreso->insertar();
			$cuopag_his .= 'El ingreso generado en caja tuvo el siguiente detalle: '.$ing_det.' ||&& ';
			$line_det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '.mostrar_moneda($monto).' && <b>'.date('d-m-Y h:i a').'</b>';
                        $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                        $oLineaTiempo->tb_lineatiempo_det=$line_det;
                        $oLineaTiempo->insertar();
		}

		if($abonar==1){
                    $xac=1;
                    $clicaj_tip=1;//ingreso
                    $mod_id=30;//cuota pago
                        
                    $oClientecaja->tb_clientecaja_xac = 1;
                    $oClientecaja->tb_cliente_id=$_POST['hdd_cli_id'];
                    $oClientecaja->tb_credito_id=$_POST['hdd_cre_id'];
                    $oClientecaja->tb_clientecaja_tip=$clicaj_tip; 
                    $oClientecaja->tb_clientecaja_fec=$fecha; 
                    $oClientecaja->tb_moneda_id=$_POST['hdd_mon_id']; 
                    $oClientecaja->tb_clientecaja_mon=moneda_mysql($abono); 
                    $oClientecaja->tb_modulo_id=$mod_id; 
                    $oClientecaja->tb_clientecaja_modid=$cuopag_id; 
                    //abono cliente caja
                    $result=$oClientecaja->insertar();
                    
                    if(intval($result['estado']) == 1){
                        $clicaj_id = $result['tb_clientecaja_id'];
                    }

			$cue_id=21;//saldo a favor
			$subcue_id=62;//saldo a favor
			$mod_id=40;//cliente caja
			$ing_det="ABONO CLIENTE ".$_POST['hdd_pag_det'];
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$clicaj_id;

                        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_fec = $fecha; 
                        $oIngreso->documento_id = 8; 
                        $oIngreso->ingreso_numdoc = $numdoc;
                        $oIngreso->ingreso_det = $ing_det; 
                        $oIngreso->ingreso_imp = moneda_mysql($abono);
                        $oIngreso->cuenta_id = $cue_id;
                        $oIngreso->subcuenta_id = $subcue_id; 
                        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                        $oIngreso->caja_id = 1; 
                        $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                        //valores que pueden ser cambiantes según requerimiento de ingreso
                        $oIngreso->modulo_id = $mod_id; 
                        $oIngreso->ingreso_modide = $clicaj_id; 
                        $oIngreso->empresa_id = $_SESSION['empresa_id'];
                        $oIngreso->ingreso_fecdep = ''; 
                        $oIngreso->ingreso_numope = ''; 
                        $oIngreso->ingreso_mondep = 0; 
                        $oIngreso->ingreso_comi = 0; 
                        $oIngreso->cuentadeposito_id = 0; 
                        $oIngreso->banco_id = 0; 
                        $oIngreso->ingreso_ap = 0; 
                        $oIngreso->ingreso_detex = '';
                        
			$oIngreso->insertar();
			$cuopag_his .= 'El ingreso generado fue mayor a la cuota facturada, por eso a la Caja Cliente fue un saldo a favor de: '.$moneda_cre.' '.mostrar_moneda($abono).' ||&& ';
		}

		//pago con caja cliente
		if(moneda_mysql($_POST['txt_cuopag_clicaj']) > 0){
			//retiro de caja cliente
			$xac=1;
			$clicaj_tip=2;//egreso
			$mod_id=30;//cuota pago
                        
                        $oClientecaja->tb_clientecaja_xac = 1;
                        $oClientecaja->tb_cliente_id=$_POST['hdd_cli_id'];
                        $oClientecaja->tb_credito_id=$_POST['hdd_cre_id'];
                        $oClientecaja->tb_clientecaja_tip=$clicaj_tip; 
                        $oClientecaja->tb_clientecaja_fec=$fecha; 
                        $oClientecaja->tb_moneda_id=$_POST['hdd_mon_id']; 
                        $oClientecaja->tb_clientecaja_mon=moneda_mysql($_POST['txt_cuopag_clicaj']); 
                        $oClientecaja->tb_modulo_id=$mod_id; 
                        $oClientecaja->tb_clientecaja_modid=$cuopag_id; 
                        //abono cliente caja
                        $result=$oClientecaja->insertar();

                        if(intval($result['estado']) == 1){
                        $clicaj_id = $result['tb_clientecaja_id'];
                        }
                        
			//registro de egreso
			$xac=1;
			$cue_id=20;//saldo a favor
			$subcue_id=61;//descuento saldo a favor
			$pro_id=1;//sistema
			$mod_id=40;//cliente caja
			$egr_det="RETIRO CLIENTE ".$_POST['hdd_pag_det'];
			$caj_id=1;
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$clicaj_id;

                        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                        $oEgreso->egreso_fec = $fecha;
                        $oEgreso->documento_id = 9;
                        $oEgreso->egreso_numdoc = $numdoc;
                        $oEgreso->egreso_det = $egr_det;
                        $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_clicaj']);
                        $oEgreso->egreso_tipcam = 0;
                        $oEgreso->egreso_est = 1;
                        $oEgreso->cuenta_id = $cue_id;
                        $oEgreso->subcuenta_id = $subcue_id;
                        $oEgreso->proveedor_id = $pro_id;
                        $oEgreso->cliente_id = 0;
                        $oEgreso->usuario_id = 0;
                        $oEgreso->caja_id = $caj_id;
                        $oEgreso->moneda_id = $_POST['hdd_mon_id'];
                        $oEgreso->modulo_id = $mod_id;
                        $oEgreso->egreso_modide = $clicaj_id;
                        $oEgreso->empresa_id = $_SESSION['empresa_id'];
                        $oEgreso->egreso_ap = 0; //si
                        $oEgreso->insertar();
                        
			//registro de ingreso
			$cue_id=1; //CANCEL CUOTAS
			$subcue_id=2; //CUOTAS menor
			$mod_id=30; //cuota pago
			$ing_det=$_POST['hdd_pag_det']." | CON CAJA CLIENTE";
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                        
                        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_fec = $fecha; 
                        $oIngreso->documento_id = 8; 
                        $oIngreso->ingreso_numdoc = $numdoc;
                        $oIngreso->ingreso_det = $ing_det; 
                        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_clicaj']);
                        $oIngreso->cuenta_id = $cue_id;
                        $oIngreso->subcuenta_id = $subcue_id; 
                        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                        $oIngreso->caja_id = 1; 
                        $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                        //valores que pueden ser cambiantes según requerimiento de ingreso
                        $oIngreso->modulo_id = $mod_id; 
                        $oIngreso->ingreso_modide = $cuopag_id; 
                        $oIngreso->empresa_id = $_SESSION['empresa_id'];
                        $oIngreso->ingreso_fecdep = ''; 
                        $oIngreso->ingreso_numope = ''; 
                        $oIngreso->ingreso_mondep = 0; 
                        $oIngreso->ingreso_comi = 0; 
                        $oIngreso->cuentadeposito_id = 0; 
                        $oIngreso->banco_id = 0; 
                        $oIngreso->ingreso_ap = 0; 
                        $oIngreso->ingreso_detex = '';
			$oIngreso->insertar();

			$cuopag_his .= 'Del saldo a favor del cliente se hizo uso para abonar a esta cuota, fue un total de: '.$moneda_cre.' '.mostrar_moneda($_POST['txt_cuopag_clicaj']).' ||&& ';
		}

    //verificamos si es el ultimo pago, si es el ultimo pago verificamos que se haya pagado todo, para hacer el pedido de todas las garantias vinculadas a ese credito
    $pedidos = '';
    if($_POST['hdd_ultimo_pago'] == 1){
      //ultimo pago 0 aun no, 1 si es ultimo pago, verificamos si en el ultimo pago cancela todo el monto, de ser asi se piden las garantias sino se vuelve esperar el ultimo pago
//      if(moneda_mysql($_POST['txt_cuopag_mon']) >= moneda_mysql($_POST['txt_cuopag_tot'])){
      if(moneda_mysql($_POST['txt_cuopag_mon']) >= moneda_mysql($cuopag_tot)){

      	$cuopag_his .= 'Este pago fue el último, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrea de las garantías. ||&& ';

        //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
        $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est',7,'INT');
        $his = '<span style="color: green;">Se liquidó totalmente el crédito y pasa a LIQUIDADO PENDIENTE DE ENTREGA, registrado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
        $oCredito->registro_historial($_POST['hdd_cre_id'], $his);

        //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
        $result = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito
         

        if($result['estado']==1){

          $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE '.$rows.'. REVISE EN INVENTARIO';
          foreach ($result['data']as $key=>$value) {     
            $gar_id = $value['tb_garantia_id'];
            $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20        
            $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

            $det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho el pedido de la garantía: <b>'.$value['tb_garantia_pro'].'</b> | '.date('d-m-Y h:i a');
            $pedido = '<strong style="color: blue;">Pedido por: '.$_SESSION['usuario_nom'].' a VICTOR MANUEL | '.date('d-m-Y h:i a').'</strong><br>';

            //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
            $result1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');

            if($result1['estado'] == 0){
              //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
              $oTarea->tb_tarea_usureg=$_SESSION['usuario_id'];
              $oTarea->tb_tarea_det=$det;
              $oTarea->tb_usuario_id=$usu_id;
              $oTarea->tb_tarea_usutip=$usu_tip;
              $oTarea->tb_garantia_id=$gar_id;
              $oTarea->tb_tarea_tipo= 'enviar';
              $oTarea->insertar();

              //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
              $oGarantia->modificar_campo($gar_id,'tb_garantia_ped',1,'INT');

              //registramos al usuario que pide la garantía
              $oGarantia->estado_usuario_pedido($gar_id,$pedido);
            }
          }
        }
        else
          $pedidos = ' IPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';
//        mysql_free_result($result);
      }
    }

    $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

		$data['cuopag_id']=$cuopag_id;
		$data['cuopag_msj']='Se registró el pago correctamente.'.$pedidos;
	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}


if($_POST['action_cuotapago']=="pagar_libre")
{
    
    
    if($vencida==1 && ($monto_pagado_activado_campo>$pago_minimo_mostrado_sistema)){
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO Mínimo, no puede pagar mas de lo depositado por que su cuota esta Vencida. Monto ingresado: '.mostrar_moneda($monto_pagado_activado_campo).', monto mínimo: '.($pago_minimo);
    echo json_encode($data);
    exit();
} 
    
//    $data['cuopag_id'] = 0;
//    $data['cuopag_msj'] = 'txt_cuopag_mon= '.$_POST['txt_cuopag_mon'].'  ||  $cuopag_tot = '.$cuopag_tot.'  || $pagos='.$pagos.' || $cuopag_tot1='.$cuopag_tot1;
//    echo json_encode($data);
//    exit();
    
    
    $hoy = date('d-m-Y');
    $fec_pago = $_POST['txt_cuopag_fec'];

//    if($hoy != $fec_pago){
//      $data['cuopag_id'] = 0;
//      $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
//      echo json_encode($data);
//      exit();
//    }

	if(!empty($_POST['txt_cuopag_mon']))
	{
		//$fecha = date('Y-m-d');
		$fecha = fecha_mysql($_POST['txt_cuopag_fec']);

                $xac=1;
                $oCuotapago->tb_cuotapago_xac=$xac;    
                $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
                $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
                $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
                $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
                $oCuotapago->tb_cuotapago_fec=$fecha; 
                $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
                $oCuotapago->tb_cuotapago_mon=moneda_mysql($_POST['txt_cuopag_mon']);
                $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
                $oCuotapago->tb_cuotapago_moncam=moneda_mysql($_POST['txt_cuopag_moncam']);
                $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($_POST['txt_cuopag_mon']);
                $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
                $oCuotapago->tb_cuotapago_obs='';

                $result=$oCuotapago->insertar();
                if(intval($result['estado']) == 1){
                    $cuopag_id = $result['tb_cuotapago_id'];
                }

                $pedidos = '';
		//estado de cuota                 12.5                                  575                                 
//		if(moneda_mysql($_POST['txt_cuopag_mon'])>=moneda_mysql($_POST['txt_cuopag_tot']))
		if(formato_moneda($_POST['txt_cuopag_mon'])>=($cuopag_tot))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',2,'INT');
			//registro de mora
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
			if($_POST['chk_mor_aut']==1)
                            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut',1,'INT');
			if(formato_moneda($_POST['txt_cuopag_mor']) > 0){
				$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest',1,'INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
			}

			$monto=formato_moneda($_POST['txt_cuopag_mon']);
			if($monto>0)$ingresar=1;
			
			$cuopag_his .= 'Este pago fue igual al monto facturado de la cuota, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrega de las garantías. ||&& ';

                    //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
                    $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est',7,'INT');

                    $his = '<span style="color: green;">Se liquidó totalmente el crédito y pasa a LIQUIDADO PENDIENTE DE ENTREGA, registrado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
                    $oCredito->registro_historial($_POST['hdd_cre_id'], $his);

                    //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
                    $result = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito

                if($result['estado']==1){

                  $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE '.$rows.'. REVISE EN INVENTARIO';
                  foreach ($result['data']as $key=>$value) {     
                    $gar_id = $value['tb_garantia_id'];
                    $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20        
                    $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

                    $det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho el pedido de la garantía: <b>'.str_replace("'", "", $value['tb_garantia_pro']).'</b> | '.date('d-m-Y h:i a');
                    $pedido = '<strong style="color: blue;">Pedido por: '.$_SESSION['usuario_nom'].' a VICTOR MANUEL | '.date('d-m-Y h:i a').'</strong><br>';

                    //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
                    $result1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');

          
                if($result1['estado']== 0){
                  //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
                      $oTarea->tb_tarea_usureg=$_SESSION['usuario_id'];
                      $oTarea->tb_tarea_det=$det;
                      $oTarea->tb_usuario_id=$usu_id;
                      $oTarea->tb_tarea_usutip=$usu_tip;
                      $oTarea->tb_garantia_id=$gar_id;
                      $oTarea->tb_tarea_tipo= 'enviar';
                      $oTarea->insertar();
                  //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
                  $oGarantia->modificar_campo($gar_id,'tb_garantia_ped',1,'INT');
                  //registramos al usuario que pide la garantía
                  $oGarantia->estado_usuario_pedido($gar_id,$pedido);

                }
        }
      }
      else
        $pedidos = ' IMPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';
    $result=NULL;
		}
                //estado de cuota                 12.5                                  575
                if(formato_moneda($_POST['txt_cuopag_mon'])<($cuopag_tot))
                {
                    if(formato_moneda($_POST['txt_cuopag_mon'])<($pago_minimo))
                    {
                            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',3,'INT');

                            $monto=formato_moneda($_POST['txt_cuopag_mon']);
                            if($monto>0)$ingresar=1;
                    }
                    if(formato_moneda($_POST['txt_cuopag_mon'])>=($pago_minimo))
                    {
                        //si el pago es mayor que el minimo aun no toma por cancelada la cuota, ya que se tiene que verificar si la fecha de pago ya venció. SI ya venció se genera una nueva cuota y esta cambia a 2 si es que se pagó el mínimo
                        $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',2,'INT');

                        //registro de mora
                        $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
                        if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut',1,'INT');
                        if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
                                $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest',1,'INT');
                                $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
                        }

                        $monto=formato_moneda($_POST['txt_cuopag_mon']);
                        if($monto>0)$ingresar=1;

                        $capital=formato_moneda($cuopag_tot)-formato_moneda($_POST['txt_cuopag_mon']);
                        $generar_cuota=1;
                            //agregamos el monto de amortización que se hace al capital
                        $amorti = formato_moneda($_POST['txt_cuopag_mon']) - ($pago_minimo);
                        if($amorti > 0)
                          $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($amorti));
//                          $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'],moneda_mysql($_POST['txt_cuopag_min']));
                        $suma_pago_actual_y_pagos_previos = $pago_minimo + $pagos;
                        

//                                    $data['cuopag_id']=0;
//                                    $data['cuopag_msj']='$pago_minimo ='.$pago_minimo .' || $pagos = '.$pagos;
//                                    echo json_encode($data);exit();
                        
                          $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'],moneda_mysql($suma_pago_actual_y_pagos_previos)); //sunarle la variable $pagos
                          $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']); //tb_cuota_cuo = a la suma del capital + pagos hechos

                        //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
                        //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
                        $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est',3,'INT');
			}

		}
		if($ingresar==1)
		{
		//registro de ingreso

			$cue_id=1; //CANCEL CUOTAS
			$subcue_id=2; //CUOTAS menor
			$mod_id=30; //cuota pago
			$ing_det=$_POST['hdd_pag_det'];
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_fec = $fecha; 
                        $oIngreso->documento_id = 8; 
                        $oIngreso->ingreso_numdoc = $numdoc;
                        $oIngreso->ingreso_det = $ing_det; 
                        $oIngreso->ingreso_imp = moneda_mysql($monto);
                        $oIngreso->cuenta_id = $cue_id;
                        $oIngreso->subcuenta_id = $subcue_id; 
                        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                        $oIngreso->caja_id = 1; 
                        $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                        //valores que pueden ser cambiantes según requerimiento de ingreso
                        $oIngreso->modulo_id = $mod_id; 
                        $oIngreso->ingreso_modide = $cuopag_id; 
                        $oIngreso->empresa_id = $_SESSION['empresa_id'];
                        $oIngreso->ingreso_fecdep = ''; 
                        $oIngreso->ingreso_numope = ''; 
                        $oIngreso->ingreso_mondep = 0; 
                        $oIngreso->ingreso_comi = 0; 
                        $oIngreso->cuentadeposito_id = 0; 
                        $oIngreso->banco_id = 0; 
                        $oIngreso->ingreso_ap = 0; 
                        $oIngreso->ingreso_detex = '';
			$oIngreso->insertar();

			$cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';

			$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '.mostrar_moneda($monto).' && <b>'.date('d-m-Y h:i a').'</b>';
                        $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                        $oLineaTiempo->tb_lineatiempo_det=$line_det;
                        $oLineaTiempo->insertar();
		}

                    $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago
                    $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_int', moneda_mysql($_POST['txt_cuopag_por']), 'STR');
                    $data['cuopag_id']=$cuopag_id;
                    $data['cuopag_msj']='Se registró el pago correctamente.'.$pedidos;

	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}

if($_POST['action']=="calcular_mora")
{
	$cre_preaco = floatval($_POST['cre_preaco']);

	$data['estado'] = 0;
	$data['msj'] = 'No existe un tarifario para la fecha del crédito, actualícelo por favor para que se pueda calcular la mora.';

	$result = $oTarifario->mostrar_por_fecha_periodo(fecha_mysql($_POST['fecha']));
//		$value = mysql_fetch_array($result);
        if($result['estado'] == 1){
		$interes_moratorio = floatval($result['data']['tb_tarifario_intmor']);
		$custodia = floatval($result['data']['tb_tarifario_cus']);
        }
//	mysql_free_result($result);

	if($interes_moratorio > 0){
		$data['estado'] = 1; //correcto
        $data['msj'] ='El tarifario se encuentra actualizado correctamente';
        
        }
	$interes_capital = $cre_preaco * $interes_moratorio / 100;
	$total_mora = $interes_capital + $custodia;

	$mor_dia = restaFechas($_POST['fecha'], $_POST['fechaPago']);
	$mor = 0;
	if($mor_dia >= 0)
	{
    $mor = $mor_dia * $total_mora;
  }

	$data['mora'] = mostrar_moneda($mor);

	echo json_encode($data);
}

if($_POST['action'] == 'cuota_nueva'){
    
  $cre_id = $_POST['cre_id'];
  $cuo_id = $_POST['cuo_id'];
  $cuo_max = $_POST['cuo_max'];
  $cre_int = $_POST['int'];
  $fecha = $_POST['fecha'];
  $mon_id = $_POST['mon_id'];
  $porcentaje= intval($_POST['cuopag_por']);
  
  
  
    $dts=$oCredito->mostrarUno($cre_id);
     if($dts['estado'] == 1){
            $cre_inte = $dts['data']['tb_credito_int'];
     }
    $dts=NULL;
  
  //el tipo del credito es 1 de menor, eso va en el primer parametro
  $result = $oCuota->filtrar(1,$cre_id);
  
  if($result['estado']==1){
    $num_rows_cuo = count($result['data']);
  }
  $result=NULL;
  

  
  
  
//  if($num_rows_cuo >= $cuo_max){
//    $data['estado'] = 2;
//    $data['msj'] = 'No se puede generar otra cuota ya que el máximo es '.$cuo_max;
//  }
//  else{

    //obtenemos el monto de la cuota con la que fue creada
    $result = $oCuota->mostrarUno($cuo_id);
      if($result['estado']==1){
        $cuota_cap = formato_moneda($result['data']['tb_cuota_cap']); //capital de la cuota
        $cuota_int = formato_moneda($result['data']['tb_cuota_int']); //monto del interes de esa cuota
        $cuota_cuo = formato_moneda($result['data']['tb_cuota_cuo']); //la cuota fue creada con este monto
      }
      $result =NULL;
    //obtenemos todos los ingresos de la cuota, su mod_id es 1 por ser cuota, 2 por ser cuotadetalle
    $result = $oCuotapago->mostrar_cuotatipo_ingreso(1,$cuo_id);
      $pagos_cuo = 0;
      
      if($result['estado']==1){
        foreach ($result['data']as $key=>$value) {
          $pagos_cuo += formato_moneda($value['tb_ingreso_imp']);
        }
      }
      $result =NULL;    
        
      
      $pagos_cuo = formato_moneda($pagos_cuo); //347.41
      $cuota_int = formato_moneda($cuota_int);//347.42
    //actualizamos los estado de la cuota que ya venció, si los pagos pasan al pago minimo la cuota cambia a cancelada sino se queda con el estado anterior
    if($pagos_cuo >= $cuota_int){
      $oCuota->modificar_campo($cuo_id,'tb_cuota_est',2,'INT');
    }

    //generamos otra cuota, para ello obetenemos el nuevo capital que es el mismo capital de la cuota anteior, restado el monto sobrante del pago mínimo
    $capital = $cuota_cap;
    if($pagos_cuo >= $cuota_int){
        
//      $capital = $cuota_cap - ($pagos_cuo - $interesanteriror);
      $capital = $cuota_cap - ($pagos_cuo - $cuota_int);
        //echo $capital.' // '.$cuota_cap.' // '.$pagos_cuo; exit();

    $C = $capital;
    $i = $cre_inte;
    $n = 1; //num cuotas

    $uno = $i / 100;
    $dos = $uno + 1;
    $tres = pow($dos,$n);
    $cuatroA = ($C * $uno) * $tres;
    $cuatroB = $tres - 1;
    $R = $cuatroA / $cuatroB;
    $r_sum = $R*$n;
        //echo $C.' // '.$uno.' // '.$dos.' // '.$tres.' // '.$cuatroA.' // '.$cuatroB; exit();
    
    list($day, $month, $year) = split('-', $fecha);

    for ($j=1; $j <= $n; $j++)
    { 
      if($j>1)
      {
        $C = $C - $amo;
        $int = $C*($i/100);
        $amo = 0;//$R - $int;
      }
      else
      {
        $int = $C*($i/100);
        $amo = 0;//$R - $int;
      } 

      //fecha facturacion
      $month = $month + 1;
      if($month == '13'){
        $month = 1;
        $year = $year + 1;
      }
      $cuota_fecha = validar_fecha_facturacion($day,$month,$year);
          //echo $C.' // '.$amo.' // '.$int.' // '.$R.' // '.fecha_mysql($cuota_fecha); exit();
      $xac=1;
      $cretip_id=1;//credito tipo
      $est=1;
      $cuo_num= $num_rows_cuo + 1;
      $persubcuo = 1;
      $pro=0;
      
     
      
      
      
        $oCuota->cuota_xac=$xac;
        $oCuota->credito_id=$cre_id;
        $oCuota->creditotipo_id=$cretip_id; 
        $oCuota->moneda_id=$mon_id;
        $oCuota->cuota_num=$cuo_num; 
        $oCuota->cuota_fec=fecha_mysql($cuota_fecha);
        $oCuota->cuota_cap=moneda_mysql($C); 
        $oCuota->cuota_amo=moneda_mysql($amo);
        $oCuota->cuota_int=moneda_mysql($int); 
        $oCuota->cuota_cuo=moneda_mysql($R); 
        $oCuota->cuota_pro=$pro; 
        $oCuota->cuota_persubcuo=$persubcuo; 
        $oCuota->cuota_est=$est;
        $oCuota->cuota_acupag=0;
        $oCuota->cuota_interes=moneda_mysql($cre_inte);
        $result=$oCuota->insertar();
            
        if(intval($result['estado']) == 1){
            $cuo_id = $result['cuota_id'];
        }	

      //automatico
      $oCuota->modificar_campo($cuo_id,'tb_cuota_aut',1,'INT');
    }
    
    $data['estado'] = 1;
    $data['msj'] = 'Se ha generado la nueva cuota';
  }
//        }
  echo json_encode($data);
}

if ($_POST['action'] == 'permiso_cuo') {
  $cuo_id = $_POST['cuo_id'];
  $cuo_per = $_POST['cuo_per'];
  $oCuota->modificar_campo($cuo_id,'per',$cuo_per);

  echo 1;
}

if($_POST['action'] == 'liquidar'){

    $hoy = date('d-m-Y');
    $fec_pago = $_POST['txt_cuopag_fec'];

  if($hoy != $fec_pago){
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
    echo json_encode($data);
    exit();
  }

  $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
  $forma_pago = $_POST['cmb_forma_pag'];
  $monto_pagado = 0;
  $total_pagar = moneda_mysql($_SESSION['total_pagar']);
  $pago_banco = '';

  if($forma_pago == 1)
  	$monto_pagado = moneda_mysql($_POST['txt_cuopag_mon_ofi']);
  if($forma_pago == 2){

            $numope = trim($_POST['txt_cuopag_numope']);
            $monto_validar = moneda_mysql($_POST['txt_cuopag_montot']);
            $monto_validar = floatval($monto_validar);

            $deposito_mon = 0;
            $result = $oDeposito->validar_num_operacion($numope);
            if($result['estado']==1){
                foreach ($result['data'] as $key=>$value){
                        $deposito_mon += floatval($value['tb_deposito_mon']);
                }
            }

            if($deposito_mon == 0){
                    $data['cuopag_id'] = 0;
                    $data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
                    echo json_encode($data);
                    exit();
            }

            $result = $oIngreso->ingresos_totales_num_operacion($numope);
                    $ingreso_importe = floatval($value['data']['importe']);

            $monto_usar = $deposito_mon - $ingreso_importe;

            if($monto_validar > $monto_usar){
                    $data['cuopag_id'] = 0;
                    $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar más de lo depositado. Monto ingresado: '.mostrar_moneda($monto_validar).', monto disponible: '.mostrar_moneda($monto_usar).', TOTAL DEL DEPÓSITO: '.mostrar_moneda($deposito_mon);
                    echo json_encode($data);
                    exit();
            }

  	$monto_pagado = moneda_mysql($_POST['txt_cuopag_montot']);
  	$pago_banco = 'Liquidado con abonado en el banco, la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: S/. '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: S/. '.$_POST['txt_cuopag_comi'].', el monto validado fue: S/. '.$_POST['txt_cuopag_montot'];
  }

  if($monto_pagado < $total_pagar){
  	$data['cuopag_id'] = 0;
	  $data['cuopag_msj'] = 'No se puede hacer pagos menores al total a pagar, Monto pagado: S/. '.mostrar_moneda($monto_pagado).', Total a Pagar: S/. '.mostrar_moneda($total_pagar);
	  echo json_encode($data); exit();
  }

	$xac=1;
        $oCuotapago->tb_cuotapago_xac=$xac;    
        $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
        $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
        $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
        $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
        $oCuotapago->tb_cuotapago_fec=$fecha; 
        $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
        $oCuotapago->tb_cuotapago_mon=moneda_mysql($monto_pagado);
        $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
        $oCuotapago->tb_cuotapago_moncam=moneda_mysql($_POST['txt_cuopag_moncam']);
        $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($_POST['txt_cuopag_mon']);
        $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
        $oCuotapago->tb_cuotapago_obs='';

        $result=$oCuotapago->insertar();
        if(intval($result['estado']) == 1){
            $cuopag_id = $result['tb_cuotapago_id'];
        }	

	if($monto_pagado > 0){
		//registro de ingreso
		$cue_id=1; //CANCEL CUOTAS
		$subcue_id=2; //CUOTAS menor
		$mod_id=30; //cuota pago
		$ing_det = $_POST['hdd_pag_det'].'. Crédito liquidado. '.$pago_banco;
		$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;
                
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha; 
                $oIngreso->documento_id = 8; 
                $oIngreso->ingreso_numdoc = $numdoc;
                $oIngreso->ingreso_det = $ing_det; 
                $oIngreso->ingreso_imp = moneda_mysql($monto_pagado);
                $oIngreso->cuenta_id = $cue_id;
                $oIngreso->subcuenta_id = $subcue_id; 
                $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                $oIngreso->caja_id = 1; 
                $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = $mod_id; 
                $oIngreso->ingreso_modide = $cuopag_id; 
                $oIngreso->empresa_id = $_SESSION['empresa_id'];
                $oIngreso->ingreso_fecdep = ''; 
                $oIngreso->ingreso_numope = ''; 
                $oIngreso->ingreso_mondep = 0; 
                $oIngreso->ingreso_comi = 0; 
                $oIngreso->cuentadeposito_id = 0; 
                $oIngreso->banco_id = 0; 
                $oIngreso->ingreso_ap = 0; 
                $oIngreso->ingreso_detex = '';
                
                $result=$oIngreso->insertar();
                if(intval($result['estado']) == 1){
                    $ingreso_id = $result['ingreso_id'];
                }	

		if($forma_pago == 2){
                    
                    $oIngreso->ingreso_fecdep=fecha_mysql($_POST['txt_cuopag_fecdep']);
                    $oIngreso->ingreso_numope =$_POST['txt_cuopag_numope'];
                    $oIngreso->ingreso_mondep=moneda_mysql($_POST['txt_cuopag_montot']);
                    $oIngreso->ingreso_comi= moneda_mysql($_POST['txt_cuopag_comi']);
                    $oIngreso->cuentadeposito_id= $_POST['cmb_cuedep_id'];
                    $oIngreso->banco_id =$_POST['cmb_banco_id'];
                    $oIngreso->ingreso_id=$ingreso_id;

			$oIngreso->registrar_datos_deposito_banco();

			$xac = 1;
			$cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
			$subcue_id = 71; // 71 es cuenta en soles
			$pro_id = 1;//sistema
			$mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
			$modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
			$egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. '.$pago_banco;
			$caj_id = 1; //caja id 14 ya que es CAJA AP
			$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

                        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                        $oEgreso->egreso_fec = $fecha;
                        $oEgreso->documento_id = 9;//otros egresos
                        $oEgreso->egreso_numdoc = $numdoc;
                        $oEgreso->egreso_det = $egr_det;
                        $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
                        $oEgreso->egreso_tipcam = 0;
                        $oEgreso->egreso_est = 1;
                        $oEgreso->cuenta_id = $cue_id;
                        $oEgreso->subcuenta_id = $subcue_id;
                        $oEgreso->proveedor_id = $pro_id;
                        $oEgreso->cliente_id = 0;
                        $oEgreso->usuario_id = 0;
                        $oEgreso->caja_id = $caj_id;
                        $oEgreso->moneda_id = $_POST['hdd_mon_id'];
                        $oEgreso->modulo_id = $mod_id;
                        $oEgreso->egreso_modide = $modide;
                        $oEgreso->empresa_id = $_SESSION['empresa_id'];
                        $oEgreso->egreso_ap = 0; //si
                        $oEgreso->insertar();
		}

		$detalle_extra = 'Días Transcurridos: '.$_POST['hdd_dias_pasados'].' días||'.$_POST['hdd_detalle_ext'].'||Interés del Crédito: '.$_POST['hdd_cre_int'].'%'.'||Interés a la Fecha: S/. '.mostrar_moneda($_SESSION['interes']).'||Suma Total: S/. '.mostrar_moneda($total_pagar);

                $oIngreso->modificar_campo_nuevo($ingreso_id , $_SESSION['usuario_id'], 'detex', $detalle_extra);

                $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '.mostrar_moneda($monto_pagado).' && <b>'.date('d-m-Y h:i a').'</b>';
                $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                $oLineaTiempo->tb_lineatiempo_det=$line_det;
                $oLineaTiempo->insertar();
                }

            $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';
            $cuopag_his .= 'Este pago fue igual al monto facturado de la cuota, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrega de las garantías. ||&& ';
            //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
            $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est',7,'INT');
            $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_fecliq', date('Y-m-d'),'STR');
            $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_monliq', moneda_mysql($_POST['txt_cuopag_mon_ofi']),'STR');

            $his = '<span style="color: green;">Se liquidó totalmente el crédito y pasa a LIQUIDADO PENDIENTE DE ENTREGA, registrado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
            $oCredito->registro_historial($_POST['hdd_cre_id'], $his);

            //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
            $result = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito
            $rows = count($result);
    
            if ($result['estado'] == 1) {

        $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE ' . $rows . '. REVISE EN INVENTARIO';
        foreach ($result['data'] as $key => $value) {
            $gar_id = $value['tb_garantia_id'];
            $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20        
            $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

            $det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho el pedido de la garantía: <b>' . str_replace("'", "", $value['tb_garantia_pro']) . '</b> | ' . date('d-m-Y h:i a');
            $pedido = '<strong style="color: blue;">Pedido por: ' . $_SESSION['usuario_nom'] . ' a VICTOR MANUEL | ' . date('d-m-Y h:i a') . '</strong><br>';

            //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
            $result1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');
            $num_rows = count($result1);

            if ($num_rows == 0) {
                //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
                $oTarea->tb_tarea_usureg=$_SESSION['usuario_id']; 
                $oTarea->tb_tarea_det=$det;
                $oTarea->tb_usuario_id=$usu_id;
                $oTarea->tb_tarea_usutip=$usu_tip;
                $oTarea->tb_garantia_id=$gar_id;
                $oTarea->tb_tarea_tipo='enviar';
                $oTarea->insertar();
                //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
                $oGarantia->modificar_campo($gar_id, 'tb_garantia_ped', 1,'INT');
                //registramos al usuario que pide la garantía
                $oGarantia->estado_usuario_pedido($gar_id, $pedido);
            }
        }
    } else {
        $pedidos = ' IPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';
    }


    if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','1','INT');
            $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
	}
	//modificamos el interes de la cuota, ya que varía de acuerdo al día en que se desea liquidar
	$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_int', moneda_mysql($_SESSION['interes']),'STR');
	//cambiamos a la cuota a un estado de pagado, ya que el monto es mayor que el interés
	$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
	//registro de mora
	$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');

	$oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

	unset($_SESSION['total_pagar']);
	unset($_SESSION['interes']);

	$data['cuopag_id'] = $cuopag_id;
	$data['cuopag_msj'] = 'Crédito liquidado correctamente';
	echo json_encode($data); exit();
}

if($_POST['action'] == 'amortizar'){

    $hoy = date('d-m-Y');
    $fec_pago = $_POST['txt_cuopag_fec'];

    if($hoy != $fec_pago){
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
        echo json_encode($data);
        exit();
    }

//    echo 'esteeeee '.$_POST['hdd_cre_id'];exit();
    
    $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
    $forma_pago = $_POST['cmb_forma_pag'];
    $monto_pagado = 0;
    $cuota_int = floatval($_SESSION['interes']);
    $pago_banco = '';
    
    if ($forma_pago == 1) {
        $monto_pagado = moneda_mysql($_POST['txt_cuopag_mon_ofi']);
    }
    if($forma_pago == 2){

  	$numope = trim($_POST['txt_cuopag_numope']);
        $monto_validar = moneda_mysql($_POST['txt_cuopag_montot']);
        $monto_validar = floatval($monto_validar);

        $deposito_mon = 0;
        $result = $oDeposito->validar_num_operacion($numope);
        if($result['estado']==1){
            foreach ($result['data'] as $key=>$value){
                $deposito_mon += floatval($value['tb_deposito_mon']);
            }
        }

            if($deposito_mon == 0){
                $data['cuopag_id'] = 0;
                $data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
                echo json_encode($data);
                exit();
            }

            $result = $oIngreso->ingresos_totales_num_operacion($numope);
                if($result['estado']==1){
                    $ingreso_importe = floatval($value['importe']);
                }
            $monto_usar = $deposito_mon - $ingreso_importe;

            if($monto_validar > $monto_usar){
                    $data['cuopag_id'] = 0;
                    $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar más de lo depositado. Monto ingresado: '.mostrar_moneda($monto_validar).', monto disponible: '.mostrar_moneda($monto_usar).', TOTAL DEL DEPÓSITO: '.mostrar_moneda($deposito_mon);
                    echo json_encode($data);
                    exit();
            }
		
  	$monto_pagado = moneda_mysql($_POST['txt_cuopag_montot']);
  	$pago_banco = 'Liquidado con abonado en el banco, la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: S/. '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: S/. '.$_POST['txt_cuopag_comi'].', el monto validado fue: S/. '.$_POST['txt_cuopag_montot'];
  }

  if($monto_pagado <= $cuota_int){
  	$data['cuopag_id'] = 0;
	  $data['cuopag_msj'] = 'No se puede hacer pagos menores o iguales al monto de la cuota, Monto pagado: S/. '.mostrar_moneda($monto_pagado).', Monto de la cuota: S/. '.mostrar_moneda($cuota_int);
	  echo json_encode($data); exit();
  }

        $xac=1;
        $oCuotapago->tb_cuotapago_xac=$xac;    
        $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
        $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
        $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
        $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
        $oCuotapago->tb_cuotapago_fec=$fecha; 
        $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
        $oCuotapago->tb_cuotapago_mon=moneda_mysql($monto_pagado);
        $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
        $oCuotapago->tb_cuotapago_moncam=moneda_mysql($_POST['txt_cuopag_moncam']);
        $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($_POST['txt_cuopag_mon']);
        $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
        $oCuotapago->tb_cuotapago_obs='';

        $result=$oCuotapago->insertar();
        if(intval($result['estado']) == 1){
            $cuopag_id = $result['tb_cuotapago_id'];
        }	

	if($monto_pagado > 0){
		//registro de ingreso
		$cue_id=1; //CANCEL CUOTAS
		$subcue_id=2; //CUOTAS menor
		$mod_id=30; //cuota pago
		$ing_det=$_POST['hdd_pag_det'].'. Pago amortizado para refinanciar el crédito. '.$pago_banco;
		$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha; 
                $oIngreso->documento_id = 8; 
                $oIngreso->ingreso_numdoc = $numdoc;
                $oIngreso->ingreso_det = $ing_det; 
                $oIngreso->ingreso_imp = moneda_mysql($monto_pagado);
                $oIngreso->cuenta_id = $cue_id;
                $oIngreso->subcuenta_id = $subcue_id; 
                $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                $oIngreso->caja_id = 1; 
                $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = $mod_id; 
                $oIngreso->ingreso_modide = $cuopag_id; 
                $oIngreso->empresa_id = $_SESSION['empresa_id'];
                $oIngreso->ingreso_fecdep = ''; 
                $oIngreso->ingreso_numope = ''; 
                $oIngreso->ingreso_mondep = 0; 
                $oIngreso->ingreso_comi = 0; 
                $oIngreso->cuentadeposito_id = 0; 
                $oIngreso->banco_id = 0; 
                $oIngreso->ingreso_ap = 0; 
                $oIngreso->ingreso_detex = '';
                
                $result=$oIngreso->insertar();
                if(intval($result['estado']) == 1){
                    $ingreso_id = $result['ingreso_id'];
                }	

		if($forma_pago == 2){
                    
                    $oIngreso->ingreso_fecdep=fecha_mysql($_POST['txt_cuopag_fecdep']);
                    $oIngreso->ingreso_numope =$_POST['txt_cuopag_numope'];
                    $oIngreso->ingreso_mondep=moneda_mysql($_POST['txt_cuopag_montot']);
                    $oIngreso->ingreso_comi= moneda_mysql($_POST['txt_cuopag_comi']);
                    $oIngreso->cuentadeposito_id= $_POST['cmb_cuedep_id'];
                    $oIngreso->banco_id =$_POST['cmb_banco_id'];
                    $oIngreso->ingreso_id=$ingreso_id;

                    $oIngreso->registrar_datos_deposito_banco();

                    $xac = 1;
                    $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
                    $subcue_id = 71; // 71 es cuenta en soles
                    $pro_id = 1;//sistema
                    $mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
                    $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
                    $egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. '.$pago_banco;
                    $caj_id = 1; //caja id 14 ya que es CAJA AP
                    $numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

                    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                    $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                    $oEgreso->egreso_fec = $fecha;
                    $oEgreso->documento_id = 9;//otros egresos
                    $oEgreso->egreso_numdoc = $numdoc;
                    $oEgreso->egreso_det = $egr_det;
                    $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
                    $oEgreso->egreso_tipcam = 0;
                    $oEgreso->egreso_est = 1;
                    $oEgreso->cuenta_id = $cue_id;
                    $oEgreso->subcuenta_id = $subcue_id;
                    $oEgreso->proveedor_id = $pro_id;
                    $oEgreso->cliente_id = 0;
                    $oEgreso->usuario_id = 0;
                    $oEgreso->caja_id = $caj_id;
                    $oEgreso->moneda_id = $_POST['hdd_mon_id'];
                    $oEgreso->modulo_id = $mod_id;
                    $oEgreso->egreso_modide = $modide;
                    $oEgreso->empresa_id = $_SESSION['empresa_id'];
                    $oEgreso->egreso_ap = 0; //si
                    $oEgreso->insertar();
		}

		$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. El monto válido fue: S/. '.mostrar_moneda($monto_pagado).' && <b>'.date('d-m-Y h:i a').'</b>';
                $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                $oLineaTiempo->tb_lineatiempo_det=$line_det;
                $oLineaTiempo->insertar();
	}

	$cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';
	$cuopag_his .= 'Este pago fue igual al monto facturado de la cuota, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrega de las garantías. ||&& ';

        $his = '<span style="color: green;">Se ha amortizado el crédito para ser refinanciado, registrado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
	$oCredito->registro_historial($_POST['hdd_cre_id'], $his);

  
        if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','1','INT');
            $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
	}

	//cambiamos a la cuota a un estado de pagado, ya que el monto es mayor que el interés
	$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
	//registro de mora
	$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');

	$oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

	$result = $oCuota->listar_cuotas_impagas($_POST['hdd_cre_id'], 1);
            if($result['estado']==1){
		foreach ($result['data']as $key=>$value) {
			$cuo_id = $value['tb_cuota_id'];
			$oCuota->modificar_campo($cuo_id, 'tb_cuota_xac', 0,'INT');
		}
            }  
            $result =NULL;
            
	$result = $oCuota->mostrarUno($_POST['hdd_cuo_id']);
            if($result['estado']==1){
		$cuota_cap = floatval($result['data']['tb_cuota_cap']);
            }
        $result =NULL;
        
	$result = $oCuota->listar_cuotas_pagadas($_POST['hdd_cre_id'], 1);
            if($result['estado']==1){
                $num_rows_cuo = count($result['data']);
            } else {
                $num_rows_cuo=0;
            }
        $result =NULL;
                
	$cre_int = floatval($_POST['txt_interes_ref']);

	if($monto_pagado > $cuota_int){
  	$capital = $cuota_cap - ($monto_pagado - $cuota_int);
        
        $amortizar_capital=$monto_pagado - $cuota_int;
        $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_amo', $amortizar_capital, 'STR');//modificamos la nueva amortización que realizó el cliente al capital
        $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_int', $cuota_int, 'STR');//modificamos la nueva amortización que realizó el cliente al capital
        $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3, 'INT');//Cambiamos el estado del crédito por si estaba en remate pase nuevamente a vigente
        
        
//        $data['cuopag_id'] = 0;
//	$data['cuopag_msj'] = '$cuota_cap='.$cuota_cap.' || $monto_pagado='.$monto_pagado.' || $cuota_int='.$cuota_int;
//	echo json_encode($data); exit();
        
        
        

  	$detalle_extra = 'Días Transcurridos: '.$_POST['hdd_dias_pasados'].' días||'.'Interés del Crédito: '.$_POST['hdd_cre_int'].'%'.'||Interés a la Fecha: S/. '.mostrar_moneda($cuota_int).'||Capital Anterior: S/. '.mostrar_moneda($cuota_cap).'||Nuevo Capital: S/. '.mostrar_moneda($capital);
  	$oIngreso->modificar_campo($ingreso_id , $_SESSION['usuario_id'], 'detex', $detalle_extra);

	  $C = $capital;
	  $i = $cre_int;
	  $n = intval($_POST['txt_numcuo_ref']);

	  $uno = $i / 100;
	  $dos = $uno + 1;
	  $tres = pow($dos,$n);
	  $cuatroA = ($C * $uno) * $tres;
	  $cuatroB = $tres - 1;
	  $R = $cuatroA / $cuatroB;
	  $r_sum = $R*$n;
	      //echo $C.' // '.$uno.' // '.$dos.' // '.$tres.' // '.$cuatroA.' // '.$cuatroB; exit();
	  
	  list($day, $month, $year) = split('-', $_POST['txt_cuopag_fec']);

	  for ($j=1; $j <= $n; $j++){ 
	    if($j>1)
	    {
	    	$C = $C - $amo;
				$int = $C*($i/100);
				$amo = $R - $int;
	    }
	    else
	    {
	    	$int = $C*($i/100);
				$amo = $R - $int;
	    } 

	    //fecha facturacion
	    $month = $month + 1;
	    if($month == '13'){
	      $month = 1;
	      $year = $year + 1;
	    }
	    $cuota_fecha = validar_fecha_facturacion($day,$month,$year);
	        //echo $C.' // '.$amo.' // '.$int.' // '.$R.' // '.fecha_mysql($cuota_fecha); exit();
	    $xac=1;
	    $cretip_id=1;//credito tipo
	    $est=1;
	    $cuo_num = $num_rows_cuo + $j;
	    $persubcuo = 1;
            $pro=0;
            
            $oCuota->cuota_xac=$xac;
            $oCuota->credito_id=$_POST['hdd_cre_id'];
            $oCuota->creditotipo_id=$cretip_id; 
            $oCuota->moneda_id=1;
            $oCuota->cuota_num=$cuo_num; 
            $oCuota->cuota_fec=fecha_mysql($cuota_fecha);
            $oCuota->cuota_cap=$C; 
            $oCuota->cuota_amo=$amo;
            $oCuota->cuota_int=$int; 
            $oCuota->cuota_cuo=$R; 
            $oCuota->cuota_pro=$pro; 
            $oCuota->cuota_persubcuo=$persubcuo; 
            $oCuota->cuota_est=$est;
            $oCuota->cuota_acupag=0;
            $oCuota->cuota_interes=$cre_int;
	    $result=$oCuota->insertar();
            
            if(intval($result['estado']) == 1){
                $cuo_id = $result['cuota_id'];
            }	
	    //automatico
	    $oCuota->modificar_campo($cuo_id,'tb_cuota_aut','1','INT');
	  }
	}

	$cuotas_credito = intval($num_rows_cuo) + intval($_POST['txt_numcuo_ref']);
	$oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_numcuo', $cuotas_credito,'INT');

	unset($_SESSION['total_pagar']);
	unset($_SESSION['interes']);

	$data['cuopag_id'] = $cuopag_id;
	$data['cuopag_msj'] = 'Crédito amortizado y refinanciado correctamente';
	echo json_encode($data); exit();
}

if($_POST['action'] == 'datos_cuota'){

	$fecha_validar = $_POST['cuopag_fecval'];
	$cuota_id = $_POST['cuota_id'];

	$result=$oCuota->mostrarUno($cuota_id);
        
        if($result['estado']==1){
            $cre_id=$result['data']['tb_credito_id'];
            $cuo_fec= mostrar_fecha($result['data']['tb_cuota_fec']);
            $cuo_num=$result['data']['tb_cuota_num'];
            $mon_id=$result['data']['tb_moneda_id'];
            $mon_nom=$result['data']['tb_moneda_nom'];
            $cuo_cuo=$result['data']['tb_cuota_cuo'];
            $cuo_int = floatval($result['data']['tb_cuota_int']);
            $cuo_cap = floatval($result['data']['tb_cuota_cap']);
            $cuo_persubcuo=$result['data']['tb_cuota_persubcuo'];
            $cuo_per = $result['data']['tb_cuota_per']; //permiso para poder hacer pagos sobre esta cuota, 0 solo el admin, 1 tienen todos el permiso
        }

	$fec=$cuo_fec;

	//verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
  $vencida = 0;
  if(strtotime($fecha_validar) > strtotime($cuo_fec))
    $vencida = 1;

	$result=$oCredito->mostrarUno($cre_id);
        if($result['estado']==1){
            $cre_preaco = floatval($result['data']['tb_credito_preaco']);
            $cre_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
            $cuotip_id = $result['data']['tb_cuotatipo_id']; //1 libre, 2 fija
            $cuotip_nom=$result['data']['tb_cuotatipo_nom'];
            $cre_int = floatval($result['data']['tb_credito_int']);
            $cre_numcuo = $result['data']['tb_credito_numcuo'];
            $cre_numcuomax = $result['data']['tb_credito_numcuomax'];
            $cli_id=$result['data']['tb_cliente_id'];
            $cli_doc = $result['data']['tb_cliente_doc'];
            $cli_nom=$result['data']['tb_cliente_nom'];
            $cliente=$cli_nom.' | '.$cli_doc;
        }

	$interes_pago_ant = 0;
	$mon_pag_ant = 0;

	if(intval($cuo_num) == 1){
            //cuando se intenta liquidar en la primera cuota
            $dias_pasados = restaFechas($cre_fecdes, $fecha_validar);
            $interes_pago_ant = 0;

            /*
            if(intval($dias_pasados) <= 15){
                    $result = $oTarifario->mostrar_por_fecha_periodo(fecha_mysql($cuo_fec));
                            $value = mysql_fetch_array($result);
                            $interes_pago_ant = floatval($value['tb_tarifario_comant']); //comision pago anticipado
                    mysql_free_result($result);

                    $mon_pag_ant = $interes_pago_ant * $cre_preaco / 100;
            }*/
	}
	else{
		//cuando se quiere liquidar pero después de la primera cuota
		$result = $oCuota->listar_ultima_cuota_pagada_pagoparcial_facturada($cre_id, 1, fecha_mysql($fecha_validar));//parametro 1 creditotipo c-menor
			$num_cuo_ult = count($result);
                        if($result['estado']==1){
                            $cuo_ult_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
                        }
			if($num_cuo_ult == 0)
				$cuo_ult_fec = $fecha_validar;


		if(strtotime($cuo_ult_fec) >= strtotime($fecha_validar))
			$dias_pasados = 0;
		else
			$dias_pasados = restaFechas($cuo_ult_fec, $fecha_validar);
	}

	$pagos_cuota = 0;
	$mod_id = 1;
	$result3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$cuota_id);
		$num_dts3 = mysql_num_rows($result3);
		if($result3['estado']==1){
		  foreach($result3['data']as $key=>$value3)
		  {
		    $pagos_cuota += floatval($value3['tb_ingreso_imp']);
		  }
		}

	$pago_anticipado = 'NO APLICA 0%';
	if($interes_pago_ant > 0)
		$pago_anticipado = 'SI APLICA '.$interes_pago_ant.'%';

	$detalle_extra = 'Pago Anticipado (PA): '.$pago_anticipado.'||Monto (PA): S/. '.mostrar_moneda($interes_pago_ant);

	//CALCULAMOS EL INTERÉS HASTA LA FECHA EN QUE SE ESTÁ LIQUIDANDO
	list($cuo_dia, $cuo_mes, $cuo_anio) = split('-', $cuo_fec);
	$dias_del_mes = cal_days_in_month(CAL_GREGORIAN, $cuo_mes, $cuo_anio);

	if($dias_pasados < $dias_del_mes){
		$cuo_int = floatval($dias_pasados * ($cuo_int / $dias_del_mes));
	}

	$suma_total = $mon_pag_ant + $cuo_int + $cuo_cap;
	$total_pagar = $suma_total - $pagos_cuota;

	$_SESSION['total_pagar'] = moneda_mysql($total_pagar);
	$_SESSION['interes'] = moneda_mysql($cuo_int);

	$data['dias_pasados'] = $dias_pasados;
	$data['pago_anticipado'] = $pago_anticipado;
	$data['mon_pag_ant'] = mostrar_moneda($mon_pag_ant);
	$data['cuo_int'] = mostrar_moneda($cuo_int);
	$data['suma_total'] = mostrar_moneda($suma_total);
	$data['total_pagar'] = mostrar_moneda($total_pagar);
	$data['detalle_extra'] = $detalle_extra;

	echo json_encode($data);
}
?>