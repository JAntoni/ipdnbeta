<?php 
require_once('../../core/usuario_sesion.php');

require_once ("./Vencimiento.class.php");
$oVencimiento = new Vencimiento();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota;
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../tarifario/Tarifario.class.php");
$oTarifario = new Tarifario();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');


if($_POST['action']=="pagar_fijo" or $_POST['action']=="pagar_libre")
{
	$fecha_hoy = date('d-m-Y');
	$dt=$oCuota->mostrarUno($_POST['cuo_id']);
        
        if($dt['estado'] == 1){
                $cre_id=$dt['data']['tb_credito_id'];
		$cuo_fec= mostrar_fecha($dt['data']['tb_cuota_fec']);
		$cuo_num=$dt['data']['tb_cuota_num'];
		$mon_id=$dt['data']['tb_moneda_id'];
		$mon_nom=$dt['data']['tb_moneda_nom'];
		$cuo_cuo=$dt['data']['tb_cuota_cuo'];
		$cuo_int=$dt['data']['tb_cuota_int'];
		$cuo_cap=$dt['data']['tb_cuota_cap'];
		$cuo_persubcuo=$dt['data']['tb_cuota_persubcuo'];
                $cuo_per = $dt['data']['tb_cuota_per']; //p
                $cuota_interes = $dt['data']['tb_cuota_interes']; //p
        }
       
	$fec=$cuo_fec;

	//verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
  $vencida = 0;
  if(strtotime($fecha_hoy) > strtotime($cuo_fec))
    $vencida = 1;

	$dts=$oCredito->mostrarUno($cre_id);
         if($dts['estado'] == 1){
		$cre_preaco = floatval($dts['data']['tb_credito_preaco']);
		$cuotip_id=$dts['data']['tb_cuotatipo_id'];
		$cuotip_nom=$dts['data']['tb_cuotatipo_nom'];
		$cre_int = $dts['data']['tb_credito_int'];
		$cre_numcuo = $dts['data']['tb_credito_numcuo'];
		$cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
		$cli_id=$dts['data']['tb_cliente_id'];
		$cli_doc = $dts['data']['tb_cliente_doc'];
		$cli_nom=$dts['data']['tb_cliente_nom'];
		$cliente=$cli_nom.' | '.$cli_doc;
         }
         
         $total=0;
        if($_POST['action']=="pagar_libre") {
            $result=$oVencimiento->nro_cuotas_Pendientes($cli_id, fecha_mysql($cuo_fec),$cre_id);
            if($result['estado']==1){
               $total=$result['data']['cuotas'];
            }
            if($total>0){
                $mensaje.= 'USTED TIENE '.($total).' CUOTA(S) VENCIDAS.';
                $mensaje.= ' PARA PODER CANCELAR ESTA CUOTA DEBE DE CANCELAR LA(S) CUOTA(S) ANTERIOR(ES)';
            }
            $result=NULL;
        }
         
         
	$codigo='CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
        
//        echo 'cuota tipo nombre '.$cuotip_nom; exit();

	if($cuotip_id==2)$cuota=$cuo_num.'/'.$cre_numcuo.'';
	if($cuotip_id==1)$cuota=$cuo_num.'/'.$cuo_num.' ('.$cre_numcuomax.')';

	$pag_det="PAGO DE CUOTA $codigo N° CUOTA: $cuota";

	$mod_id='1';//cuota
	$modid=$_POST['cuo_id'];


	//cliente caja
	//$dts1=$oClientecaja->filtrar($cli_id,$cre_id,$mon_id); //antes era caja cliente filtrando por credito
	$dts1=$oClientecaja->filtrar_por_cliente_creditomenor($cli_id, $mon_id); //filtramos su caja por cliente de todos sus creditos
    $clientecaja = 0;
    if($dts1['estado']==1){
        foreach($dts1['data'] as $key=>$dt1)
        {
            if($dt1['tb_clientecaja_tip']==1)
            {
            $clientecaja = $clientecaja + $dt1['tb_clientecaja_mon'];
          }
          if($dt1['tb_clientecaja_tip']==2)
          {
            $clientecaja = $clientecaja - $dt1['tb_clientecaja_mon'];
          }
        }
    }
//  mysql_free_result($dts1);

  $cliente_caja = mostrar_moneda($clientecaja);

    //pagos parciales
	$pagos_cuota=0;
    $mod_id=1;
    $dts3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$_POST['cuo_id']);
    if($dts3['estado']==1)
        {
            foreach($dts3['data'] as $key=>$dt3)
            {
                 $pagos_cuota+=$dt3['tb_ingreso_imp'];
            }
        }
    $pagos=$pagos_cuota;
    $pagos_parciales= mostrar_moneda($pagos);
    
    
    
    $diasdelmes= date("d-m-Y",strtotime($cuo_fec."- 1 month")); 
    
    $diasdelmes=date('t', strtotime($diasdelmes));
    
    $fecha_inicial=date("d-m-Y",strtotime($cuo_fec."- 1 month")); 
    $fecha_final=date('d-m-Y');//
    

    $dias = (strtotime($fecha_inicial)-strtotime($fecha_final))/86400;
    $dias = abs($dias); 
    $dias = floor($dias);
    $diastranscurridos= $dias;
    


    
//    $diastranscurridos=dias_pasados($fecha_inicial,$fecha_final);

	$monto_pagar= mostrar_moneda($cuo_cuo-$pagos);
        

  $ultimo_pago = 0;
	if($cuotip_id==2)//fijo
	{
		if($cuo_num==$cre_numcuo)
		{
      $ultimo_pago = 1;
			$ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";
			$vMax=moneda_mysql($monto_pagar);
		}
		else
		{
			$vMax='99999.00';
		}
		
	}

	if($cuotip_id==1)//libre
	{
		$vMax=moneda_mysql($monto_pagar);
		if($cuo_num==$cre_numcuomax)
		{
                    $ultimo_pago = 1;
                    $ultima_cuota=" <span style='color:red;'>ÚLTIMA CUOTA</span>";		
		}
		else
		{
			//$vMax='99999.00';
		}               
	}

}

if($usureg>0)
{
	$rws=$oUsuario->mostrarUno($usureg);
		$usureg	=$rw['data']['tb_usuario_nom']." ".$rw['data']['tb_usuario_ape'];
}
	//verificar si es una liquidación anticipada
	$hoy = date('d--m-Y');
	restaFechas($_POST['fecha'], $_POST['fechaPago']);
?>


<?php if($total==0){?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenor" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">INFORMACIÓN DE CUOTA A PAGAR <?php echo ' $dias = '.$dias;?></h4>
        </div>
        <form id="for_cuopag" method="post">
          
            <input type="hidden" id="hdd_cuo_per" value="<?php echo $cuo_per;?>">
            <input type="hidden" id="hdd_usuariogrupo" value="<?php echo $_SESSION['usuariogrupo_id'];?>">

            <input name="action_cuotapago" id="action_cuotapago" type="hidden" value="<?php echo $_POST['action']?>">
            <input name="hdd_cuo_id" id="hdd_cuo_id" type="hidden" value="<?php echo $_POST['cuo_id']?>">
            <input type="hidden" id="hdd_vencida" value="<?php echo $vencida?>">
            <input type="hidden" id="hdd_cuotip_id" value="<?php echo $cuotip_id?>">

            <input name="hdd_cli_id" id="hdd_cli_id" type="hidden" value="<?php echo $cli_id ?>">
            <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id?>">

            <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $mon_id ?>">

            <input name="hdd_mod_id" id="hdd_mod_id" type="hidden" value="<?php echo $mod_id?>">
            <input name="hdd_modid" id="hdd_modid" type="hidden" value="<?php echo $modid?>">

            <input name="hdd_cre_int" id="hdd_cre_int" type="hidden" value="<?php echo $cre_int ?>">
            <input name="hdd_cuo_fec" id="hdd_cuo_fec" type="hidden" value="<?php echo $cuo_fec ?>">
            <input name="hdd_cuo_num" id="hdd_cuo_num" type="hidden" value="<?php echo $cuo_num ?>">
            <input name="hdd_cuo_max" id="hdd_cuo_max" type="hidden" value="<?php echo $cre_numcuomax?>">
            <input name="hdd_cuo_cap" id="hdd_cuo_cap" type="hidden" value="<?php echo $cuo_cap ?>">

            <input name="hdd_pag_det" id="hdd_pag_det" type="hidden" value="<?php echo $pag_det ?>">
            <input type="hidden" name="hdd_ultimo_pago" id="hdd_ultimo_pago" value="<?php echo $ultimo_pago;?>">
            <input type="hidden" id="hdd_cre_preaco" value="<?php echo $cre_preaco;?>">

            <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $_POST['vista']?>">
            <input type="hidden" name="hdd_pc_nombre" id="hdd_pc_nombre">
            <input type="hidden" name="hdd_pc_mac" id="hdd_pc_mac">

                
        <div class="modal-body">
            
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <!--<div class="col-xs-8 col-xs-8">-->
                            
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label" style="text-align: right">Cliente :</label>
                            <div class="col-md-9">
                              <!--<input type="email" class="form-control" id="inputEmail3" placeholder="Email">-->
                              <?php // echo 'CRISTHIAN JOSSIMAR GALVEZ AGUILAR'?>
                              <?php echo $cliente?>
                            </div>
                        </div>
                        
                        <!--</div>-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Crédito :</label>
                            <div class="col-md-9" >
                             CRÉDITO MENOR - <?php echo $cuotip_nom?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Código :</label>
                            <div class="col-md-9">
                             <?php echo $codigo?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">N° de Cuota :</label>
                            <div class="col-md-9">
                             <?php echo $cuota.$ultima_cuota;?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Fec. de Vencimiento :</label>
                            <div class="col-md-9">
                             <?php echo $cuo_fec; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-6 control-label" style="text-align: right">Cuota (<?php echo $mon_nom?>):</label>
                                <div class="col-md-6">
                                    <input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuodet_cuo" type="text" id="txt_cuodet_cuo" value="<?php echo mostrar_moneda($cuo_cuo)?>" size="15" maxlength="10" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-4 control-label" style="text-align: right">Mora (<?php echo $mon_nom?>):</label>
                                <div class="col-md-4">
                                    <input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuopag_mor" type="text" id="txt_cuopag_mor" value="" size="15" maxlength="10" readonly> 
                                    <!--<input name="chk_mor_aut" type="checkbox" id="chk_mor_aut" value="1"  onChange="comprobar(this);">-->
                                </div>
                                
                                <div class="col-md-1">
                                    <input name="chk_mor_aut" type="checkbox" class="checkbox" id="chk_mor_aut" value="1"  onChange="comprobar(this);">
                                </div>
                                <div class="col-md-3">
                                    <label for="chk_mor_aut">Modificar</label>
                                </div>
                            </div>
                        </div>
                    </div>
                     <?php if($pagos>0):?>
                    <p>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Pagos Parciales (<?php echo $mon_nom?>):</label>
                            <div class="col-md-3">
                                <input style="text-align:right;" class="form-control input-sm moneda" name="txt_cuopag_pagpar" type="text" id="txt_cuopag_pagpar" value="<?php echo $pagos_parciales;?>" size="15" maxlength="10" readonly>
                            </div>
                        </div>
                    </div>
                    <?php endif?>
                    <p>
                    <?php if($clientecaja>0):?> 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-6 control-label" style="text-align: right">Caja Cliente (<?php echo $mon_nom?>):</label>
                                <div class="col-md-6">
                                    <input style="text-align:right;" class="form-control input-sm moneda" name="txt_cuopag_clicaj" type="text" id="txt_cuopag_clicaj" value="<?php //echo $cliente_caja;?>" size="15" maxlength="10" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-5 control-label" style="text-align: right">Caja Cliente (<?php echo $mon_nom?>):</label>
                                <div class="col-md-5">
                                    <input style="text-align:right;" class="form-control input-sm moneda" name="txt_clicaj_tot" type="text" id="txt_clicaj_tot" value="<?php echo $cliente_caja;?>" size="15" maxlength="10" readonly>
                                    <!--<a class="btn_accion" id="usarSaldoBtn" href="#caj" onClick="usarSaldo()">Usar</a>-->
                                </div>
                                <div class="col-md-2">
                                     <a class="btn btn-success btn-sm" id="usarSaldoBtn" onClick="usarSaldo()">Usar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif?>
                    <p></p>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item" style="border-color: #000000">
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Capital (<?php echo $mon_nom?>):</label>
                            <div class="col-md-3">
                                <input style="text-align:right;" class="form-control input-sm moneda" name="txt_capital" type="text" id="txt_capital" value="<?php echo $cuo_cap;?>" size="15" maxlength="10" readonly>
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-2 control-label" style="text-align: right">Interés %:</label>
                                <div class="col-md-2">
                                    <input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuopag_por" type="text" id="txt_cuopag_por" value="<?php echo $cre_int;?>" readonly> 
                                    <!--<input name="chk_mor_aut" type="checkbox" id="chk_mor_aut" value="1"  onChange="comprobar(this);">-->
                                </div>
                                
                                <div class="col-md-1">
                                    <input name="chk_por_aut" type="checkbox" class="checkbox" id="chk_por_aut" value="1"  onChange="Editar(this);" title="Editar porcentaje de interés">
                                </div>
                                
                            </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Interes (<?php echo $mon_nom?>):</label>
                            <div class="col-md-3">
                                <input style="text-align:right;" class="form-control input-sm moneda" name="txt_interes" type="text" id="txt_interes" value="<?php echo $cuo_int;?>" size="15" maxlength="10" readonly>
                            </div>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Total (<?php echo $mon_nom?>):</label>
                            <div class="col-md-3">
                                <input style="text-align:right;" class="form-control input-sm moneda" name="txt_cuopag_tot" type="text" id="txt_cuopag_tot" value="<?php echo $monto_pagar;?>" size="15" maxlength="10" readonly>
                            </div>
                        </div>
                    </div>
                    
                    <?php if($cuotip_id==1):			

                        if($cuo_num==$cre_numcuomax)
                        {

                        }
                        else
                        {
                            $monto_pagar=$cuo_int-$pagos;
                            if($monto_pagar <= 0)
                              $monto_pagar = 0;
                            else
                              $monto_pagar = mostrar_moneda($monto_pagar);
                            
                            
                                if(strtotime($fecha_hoy) > strtotime($cuo_fec)){  }
                                else{
                                   $monto_pagar= mostrar_moneda((($cuo_int/$diasdelmes)*$diastranscurridos)-$pagos);
                                }

                        }
                        ?>
                    <p>
                    <div class="row">
                        <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Pago Mínimo (<?php echo $mon_nom?>):</label>
                                <div class="col-md-3">
                                    <input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuopag_min" type="text" id="txt_cuopag_min" value="<?php echo $monto_pagar;?>" size="15" maxlength="10" readonly>
                                </div>
                                <div class="col-md-6">
                                    por &nbsp;&nbsp;<label style="color: red"><?php echo $diastranscurridos ?></label> &nbsp;&nbsp;dias Transcurridos
                                </div>
                            </div>
                    </div>
                    <?php endif?>
                    <p>
                    <div class="row">
                        <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Fecha de Pago:</label>
                                <div class="col-md-3">
                                <div class='input-group date' id='ingreso_fil_picker1'>
                                    <input name="txt_cuopag_fec" type="text" class="form-control input-sm mayus" class="fecha" id="txt_cuopag_fec" value="<?php echo $fecha_hoy?>" size="10" maxlength="10" readonly>
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                </div>
                            </div>
                    </div>
                    <p>
                        <?php 
                        if(strtotime($fecha_hoy) > strtotime($cuo_fec)){
                            $readonly='readonly=""';
                        }
                        else{
                           $readonly='';
                        }
                        ?>
                    <div class="row">
<!--                        <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Monto Pago (<?php echo $mon_nom?>):</label>
                                <div class="col-md-3">
                                    <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $monto_pagar?>" size="15" maxlength="150" <?php echo $readonly?> >
                                </div>
                        </div>-->
                        
                        
                        <!--<div class="col-md-4">-->
                                <label for="inputEmail3" class="col-md-3 control-label" style="text-align: right">Monto Pago (<?php echo $mon_nom?>):</label>
                                <div class="col-md-3">  
                                    <div class="input-group">

                                       <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $monto_pagar?>" size="15" maxlength="150" <?php echo $readonly?> >
                                       <span class="input-group-btn">
                                           <?php
//                                           $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
//                                           if ($usuariogrupo_id==2) {?>
                                           <button class="btn btn-success btn-sm" type="button"  onClick="Activar_input_monto_pago_banco()" title="Activar Casilla para ingresar Monto">
                                               <span class="fa fa-check-square-o"></span>
                                           </button>
                                           <?php //} ?>
                                       </span>
                                   </div>
                               </div>
                            <!--</div>-->
                        
                        
                        
                        
                        
                    </div>
                </li>
            </ul>
                <div class="row">
                    <div class="col-md-12">    
                        <?php 
                        if($cuotip_id == 1 && $vencida == 1) {
                            echo ' 
                              <br/>
                              <strong style="font-weight: bold; font-size: 14px; color: red;text-align: center">Esta cuota ya está vencida, para poder seguir abonando genere nueva cuota o pida al administrador que habilite esta cuota.</strong>
                              <br/><br>';
                            
                            if($monto_pagar <= 0){
                                
                            }
//                              echo '<a class="btn btn-success" onclick="generar_nueva_cuota()">Generar Nueva Cuota</a>';
//                              echo '<a class="btn btn-success" id="btn_habilitar" onclick="verificar_vencimiento(2)">Habilitar Pago</a>';
//                              echo '<a style="float: right;" class="btn btn-info" onclick="permiso_cuotadetalle(1)">Habilitar Para Vendedores</a>';                              
                        }
                        ?>
                    </div>

                </div>
            <br>
            <div id="msj_menor_form" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none">
                
            </div>
              
            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cuotapago_menor_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          
        
          
      </div>
    </div>
    <div class="modal-footer">
        <div class="f1-buttons">
<!--            <button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>-->
            
             <?php
                if($cuo_num==$cre_numcuo && $cuotip_id == 1)
                    {
                    ?>
                    <strong style="font-weight: bold; font-size: 14px; color: red;">Esta es la ultima cuota del credito por lo tando debe Habilitar 2 cuotas mas antes de proceder con el cobro.</strong>
                
                <?php
                    }
                    else{
                        echo '<button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>';
                    }
                    ?>
            
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
    </div>
    </form>
  </div>


<?php 

}
else { ?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenor" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">MENSAJE IMPORTANTE</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
              <p  style="font-family: cambria;font-size: 15px;color: #cc0000"><?php echo $mensaje;?></p>
          </div>
        </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>         
            </div>
        </div>
    </div>
</div>

<?php } ?>

<script type="text/javascript" src="<?php echo 'vista/vencimiento/cuotapago_menor_form.js?ver=212015245212154154';?>"></script>