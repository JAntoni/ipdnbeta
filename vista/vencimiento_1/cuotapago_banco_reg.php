<?php
require_once('../../core/usuario_sesion.php');

require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../tareainventario/Tareainventario.class.php');
$oTarea = new TareaInventario();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$hoy = date('d-m-Y');
$fec_pago = $_POST['txt_cuopag_fec'];

if($hoy != $fec_pago){
	$data['cuopag_id'] = 0;
	$data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$hoy.', fecha a pagar: '.$fec_pago;
	echo json_encode($data);
	exit();
}

$cuota_id=$_POST['hdd_modid'];
//$mod_id=$_POST['hdd_cuotatip_id'];
$mod_id=$_POST['hdd_mod_id'];

$result=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
  if($result['estado']==1)
  {
    foreach($result['data'] as $key=>$value3)
    {
         $pagos_cuota+=formato_moneda($value3['tb_ingreso_imp']);
    }

  }


$resultcuo=$oCuota->mostrarUno($cuota_id);

if($resultcuo['estado']==1){
    $cuopag_tot1 = $resultcuo['data']['tb_cuota_cuo'];
    $cuo_int= formato_moneda($resultcuo['data']['tb_cuota_int']);
    $cuo_fec=$resultcuo['data']['tb_cuota_fec'];
}
    $pagos=$pagos_cuota;
    $cuopag_tot= $cuopag_tot1 - $pagos;
 
    
    $diasdelmes= date("d-m-Y",strtotime($cuo_fec."- 1 month")); 
    
    $diasdelmes=date('t', strtotime($diasdelmes));
    
    $fecha_inicial=date("d-m-Y",strtotime($cuo_fec."- 1 month")); 
    $fecha_final=date('d-m-Y');//
    

    $dias = (strtotime($fecha_inicial)-strtotime($fecha_final))/86400;
    $dias = abs($dias); 
    $dias = floor($dias);
    $diastranscurridos= $dias;

   $fecha_hoy = date('d-m-Y'); 
   $vencida = 0;
    if(strtotime($fecha_hoy) > strtotime($cuo_fec))
    $vencida = 1;
    
 
   $pago_minimo = ($cuo_int - $pagos);
   if($vencida==0){
       if(strtotime($fecha_hoy) > strtotime($cuo_fec)){  
           
       }
        else{
           $pago_minimo= ((($cuo_int/$diasdelmes)*$diastranscurridos)-$pagos);
        }
   }
  if($pago_minimo < 0)
    $pago_minimo = 0;
  
  
  
  
 
// echo '$cuopag_tot = '.$cuopag_tot;exit();
 
// $data['cuopag_id'] = 0;
//	$data['cuopag_msj'] = '$cuopag_tot = '.$cuopag_tot.' ANd $pago_minimo='.$pago_minimo .' diastranscurridos= '.$diastranscurridos.' $vencida= '.$vencida;
//	echo json_encode($data);
//	exit();




$numope = trim($_POST['txt_cuopag_numope']);
$monto_validar = formato_moneda($_POST['txt_cuopag_mon']); //elimina las comas y devuelve con 2 decimales
$moneda_deposito_id = intval($_POST['hdd_mon_iddep']); //1 soles, 2 dolares
$moneda_credito_id = intval($_POST['hdd_mon_id']); //1 soles, 2 dolares
$tipocambio_dia = floatval($_POST['txt_cuopag_tipcam']);

$deposito_mon = 0;
$dts = $oDeposito->validar_num_operacion($numope);
    if($dts['estado']==1){
        foreach ($dts['data'] as $key=>$dt){
		$deposito_mon += floatval($dt['tb_deposito_mon']);
	}
    }   

if($deposito_mon == 0){
	$data['cuopag_id'] = 0;
	$data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
	echo json_encode($data);
	exit();
}

$ingreso_importe = 0;
$dts = $oIngreso->lista_ingresos_num_operacion($numope);
    if($dts['estado']==1){
	foreach ($dts['data'] as $key=>$dt) {
		$tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
		$ingreso_mon_id = intval($dt['tb_moneda_id']);
		if($ingreso_mon_id == 2) //si el ingreso se hizo en dólares
                    $tb_ingreso_imp = ($tb_ingreso_imp * $tipocambio_dia);
                    $ingreso_importe += $tb_ingreso_imp;
	}
    }    


$monto_usar = $deposito_mon - $ingreso_importe;
$monto_usar = moneda_mysql($monto_usar);
//echo 'tooo esto hay een importes: '.$ingreso_importe.' // depo: '.$deposito_mon.' // validar: '.$monto_validar.' // usar: '.$monto_usar.' // moneda ing: '.$ingreso_mon_id.' // tipo cambio: '.$tipocambio_dia; exit();

if($monto_validar > $monto_usar){
	$data['cuopag_id'] = 0;
	$data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: '.mostrar_moneda($monto_validar).', monto disponible: '.mostrar_moneda($monto_usar).', TOTAL DEL DEPOSITO: '.mostrar_moneda($deposito_mon).' | validar: '.$monto_validar.' | usar: '.$monto_usar.' | ingre: '.$ingreso_importe.' | cambio: '.$tipocambio_dia.' / mond ing: '.$ingreso_mon_id ;
	echo json_encode($data);
	exit();
}


$monto_pagado_activado_campo= moneda_mysql($_POST['txt_cuopag_mon']);
$pago_minimo_mostrado_sistema= moneda_mysql($pago_minimo);




$cre_tip = 0;
if($_POST['hdd_credito'] == "asiveh"){
	require_once("../creditoasiveh/Creditoasiveh.class.php");
	$oCredito = new Creditoasiveh();
	$cre_tip = 2;
}
if($_POST['hdd_credito'] == "garveh"){
	require_once ("../creditogarveh/Creditogarveh.class.php");
	$oCredito = new Creditogarveh();
	$cre_tip = 3;
}
if($_POST['hdd_credito'] == "hipo"){
	require_once ("../creditohipo/Creditohipo.class.php");
	$oCredito = new Creditohipo();
	$cre_tip = 4;
}
if($_POST['hdd_credito'] == "menor"){
	require_once ("../creditomenor/Creditomenor.class.php");
	$oCredito = new Creditomenor();
	$cre_tip = 4;
}

$moneda_depo = ' ? ';
if($_POST['hdd_mon_iddep'] != ''){
	if($_POST['hdd_mon_iddep'] == 1)
		$moneda_depo = 'S/.';
	else
		$moneda_depo = 'US$';
}

$moneda_cred = ' ? ';
if($_POST['hdd_mon_id'] != ''){
	if($_POST['hdd_mon_id'] == 1)
		$moneda_cred = 'S/.';
	else
		$moneda_cred = 'US$';
}

$nombre_pc = (!empty($_POST['hdd_pc_nombre']))? $_POST['hdd_pc_nombre'] : 'SIN NOMBRE PC';
$mac_pc = (!empty($_POST['hdd_pc_mac']))? $_POST['hdd_pc_mac'] : 'SIN NOMBRE MAC';


if($_POST['hdd_cuotatip_id'] == 4 && ($_POST['hdd_credito'] == "asiveh" || $_POST['hdd_credito'] == "garveh" || $_POST['hdd_credito'] == "hipo"))
{

	if(!empty($_POST['txt_cuopag_montot']))
	{
		//$fecha = date('Y-m-d');
		$acuerdo_ap = $_POST['hdd_acupag']; //si el valor es 1 entonces es un acuerdo de pago
		$fecha = fecha_mysql($_POST['txt_cuopag_fec']);
		 //monto total que debe pagar el cliente
		$cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
		$mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
		$tipo_caja = 1; //las cajas a tener en cuenta es 1 CAJA y 14 CAJA AP

		$cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: '.trim($_SESSION['usuario_nom']).', en la fecha de: '.date('d-m-Y h:i a').', desde la computadora con nombre: '.$nombre_pc.' y MAC: '.$mac_pc.' ||&& ';

		if($acuerdo_ap == 1){
			$tipo_caja = 14; //CAJA AP
			$cuopag_his .= 'Esta cuota es de un Acuerdo de Pago, por ende el monto fue directamente a la CAJA AP ||&& ';
		}

		$xac=1;
                $oCuotapago->tb_cuotapago_xac=1;    
                $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
                $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
                $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
                $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
                $oCuotapago->tb_cuotapago_fec=$fecha; 
                $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
                $oCuotapago->tb_cuotapago_mon=moneda_mysql($mon_pagado);
                $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
                $oCuotapago->tb_cuotapago_moncam=0;
                $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($mon_pagado);
                $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
                $oCuotapago->tb_cuotapago_obs=$_POST['txt_cuopag_obs'];

                $result=$oCuotapago->insertar();
                    if(intval($result['estado']) == 1){
                        $cuopag_id = $result['tb_cuotapago_id'];
                    }
		//estado de cuota detalle
		if(moneda_mysql($mon_pagado)==moneda_mysql($cuopag_tot))
		{
			$oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_est','2','INT');
			//registro de mora
			$oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
			if ($_POST['chk_mor_aut'] == 1) {
                            $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_moraut', '1', 'INT');
                         }
                        if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
				$oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','1','INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
			}
			$monto=moneda_mysql($mon_pagado);
			if($monto>0)$ingresar=1;
		}

		if(moneda_mysql($mon_pagado)<moneda_mysql($cuopag_tot))
		{
			$oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_est','3','INT');
			$monto=moneda_mysql($mon_pagado);
			if($monto>0)$ingresar=1;
		}

		if(moneda_mysql($mon_pagado)>moneda_mysql($cuopag_tot))
		{
                    $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_est','2','INT');
                    //registro de mora
                    $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
                if ($_POST['chk_mor_aut'] == 1) {
                    $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_moraut', '1','INT');
                }
                if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
				$oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','1','INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
                }
			$monto=moneda_mysql($cuopag_tot);
			if($monto>0)$ingresar=1;

			$abono = moneda_mysql($mon_pagado)-moneda_mysql($cuopag_tot);
			if($abono>0) $abonar=1;
		}

		//verificar si el pago es de un ACUERDO DE PAGO, PORQUE IRÁ A OTRA CAJA
		if($ingresar == 1)
		{
			//registro de ingreso
			$subcue_id_ingre = 0; //CUOTAS asiveh
			if ($_POST['hdd_credito'] == "asiveh") {
                            $subcue_id_ingre = 1;
                        } //CUOTAS asiveh
			if ($_POST['hdd_credito'] == "garveh") {
                            $subcue_id_ingre = 3;
                        } //CUOTAS garveh
			if ($_POST['hdd_credito'] == "hipo") {
                            $subcue_id_ingre = 4;
                        } //CUOTAS hipo

			$cue_id_ingre = 1; //CANCEL CUOTAS
			$mod_id = 30; //cuota pago
			$ing_det = $_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];

			if (!empty($_POST['txt_cuopag_obs'])) {
                            $ing_det .= '. Obs: ' . $_POST['txt_cuopag_obs'];
                        }

                $numdoc = $_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

            
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha; 
                $oIngreso->documento_id = 8; 
                $oIngreso->ingreso_numdoc = $numdoc;
                $oIngreso->ingreso_det = $ing_det; 
                $oIngreso->ingreso_imp = moneda_mysql($monto);
                $oIngreso->cuenta_id = $cue_id_ingre;
                $oIngreso->subcuenta_id = $subcue_id_ingre; 
                $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                $oIngreso->caja_id = $tipo_caja; 
                $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = $mod_id; 
                $oIngreso->ingreso_modide = $cuopag_id; 
                $oIngreso->empresa_id = $_SESSION['empresa_id'];
                $oIngreso->ingreso_fecdep = ''; 
                $oIngreso->ingreso_numope = ''; 
                $oIngreso->ingreso_mondep = 0; 
                $oIngreso->ingreso_comi = 0; 
                $oIngreso->cuentadeposito_id = 0; 
                $oIngreso->banco_id = 0; 
                $oIngreso->ingreso_ap = 0; 
                $oIngreso->ingreso_detex = '';
            
                $result=$oIngreso->insertar();
                    if(intval($result['estado']) == 1){
                        $ingr_id = $result['ingreso_id'];
                    }
                    
                $oIngreso->ingreso_fecdep=fecha_mysql($_POST['txt_cuopag_fecdep']);
                $oIngreso->ingreso_numope =$_POST['txt_cuopag_numope'];
                $oIngreso->ingreso_mondep=moneda_mysql($_POST['txt_cuopag_mon']);
                $oIngreso->ingreso_comi= moneda_mysql($_POST['txt_cuopag_comi']);
                $oIngreso->cuentadeposito_id= $_POST['cmb_cuedep_id'];
                $oIngreso->banco_id =$_POST['cmb_banco_id'];
                $oIngreso->ingreso_id=$ingr_id;
                
                $oIngreso->registrar_datos_deposito_banco();

                $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';

                $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. && <b>'.date('d-m-Y h:i a').'</b>';

                $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                $oLineaTiempo->tb_lineatiempo_det=$line_det;
                $oLineaTiempo->insertar();

                $ingreso_detalle = $ing_det;
		}
		//si acu_mon es null es porq esta cuota no tiene q pagar un acuerdo de pago, si se paga con monto AP no se abona nada
		if($abonar == 1)
		{
                    //abono cliente caja
                    $xac=1;
                    $clicaj_tip=1;//ingreso
                    $mod_id=30;//cuota pago
                        
                    $oClientecaja->tb_clientecaja_xac = 1;
                    $oClientecaja->tb_cliente_id=$_POST['hdd_cli_id'];
                    $oClientecaja->tb_credito_id=$_POST['hdd_cre_id'];
                    $oClientecaja->tb_clientecaja_tip=$clicaj_tip; 
                    $oClientecaja->tb_clientecaja_fec=$fecha; 
                    $oClientecaja->tb_moneda_id=$_POST['hdd_mon_id']; 
                    $oClientecaja->tb_clientecaja_mon=moneda_mysql($abono); 
                    $oClientecaja->tb_modulo_id=$mod_id; 
                    $oClientecaja->tb_clientecaja_modid=$cuopag_id; 
                    //abono cliente caja
                    $result1=$oClientecaja->insertar();
                    if(intval($result['estado']) == 1){
                        $clicaj_id = $result['tb_clientecaja_id'];
                    }


                    $cue_id=21;//saldo a favor
                    $subcue_id=62;//saldo a favor
                    $mod_id=40;//cliente caja
                    $ing_det="ABONO CLIENTE ".$_POST['hdd_pag_det'];
                    $numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$clicaj_id;

                    
                    $oIngreso->ingreso_usureg=$_SESSION['usuario_id'];
                    $oIngreso->ingreso_usumod=$_SESSION['usuario_id'];
//			$oIngreso->ingreso_xac=1;
                    $oIngreso->ingreso_fec=$fecha;
                    $oIngreso->documento_id=8;
                    $oIngreso->ingreso_numdoc=$numdoc;
                    $oIngreso->ingreso_det=$ing_det;
                    $oIngreso->ingreso_imp=$abono;
//			$oIngreso->ingreso_est=1;
                    $oIngreso->cuenta_id=$cue_id;
                    $oIngreso->subcuenta_id=$subcue_id;
                    $oIngreso->cliente_id=$_POST['hdd_cli_id'];
                    $oIngreso->caja_id=1;
                    $oIngreso->moneda_id=$_POST['hdd_mon_id'];
                    $oIngreso->modulo_id=$mod_id;
                    $oIngreso->ingreso_modide=$clicaj_id;
                    $oIngreso->empresa_id=$_SESSION['empresa_id'];

                    $result=$oIngreso->insertar();
                    if(intval($result['estado']) == 1){
                        $ingr_abono_id = $result['ingreso_id'];
                    }

                    $oIngreso->ingreso_fecdep=fecha_mysql($_POST['txt_cuopag_fecdep']);
                    $oIngreso->ingreso_numope =$_POST['txt_cuopag_numope'];
                    $oIngreso->ingreso_mondep=moneda_mysql($_POST['txt_cuopag_mon']);
                    $oIngreso->ingreso_comi=moneda_mysql($_POST['txt_cuopag_comi']);
                    $oIngreso->cuentadeposito_id= $_POST['cmb_cuedep_id'];
                    $oIngreso->banco_id =$_POST['cmb_banco_id'];
                    $oIngreso->ingreso_id=$ingr_abono_id;

                    $oIngreso->registrar_datos_deposito_banco();

                    $cuopag_his .= 'El ingreso generado fue mayor a la cuota facturada, por eso a la Caja Cliente fue un saldo a favor de: '.$moneda_cre.' '.mostrar_moneda($abono).' ||&& ';
		}

                    //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
                    $subcue_id = 72;//72 es la subcuenta de cuenta en dolares
                    if($_POST['hdd_mon_iddep'] == 1)
			$subcue_id = 71; // 71 es cuenta en soles

                    $xac = 1;
                    $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
                    $pro_id = 1;//sistema
                    $mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
                    $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
                    $egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
                    $caj_id = 1; //caja id 14 ya que es CAJA AP
                    $numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

                    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                    $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                    $oEgreso->egreso_fec = $fecha;
                    $oEgreso->documento_id = 9;//otros egresos
                    $oEgreso->egreso_numdoc = $numdoc;
                    $oEgreso->egreso_det = $egr_det;
                    $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
                    $oEgreso->egreso_tipcam = 0;
                    $oEgreso->egreso_est = 1;
                    $oEgreso->cuenta_id = $cue_id;
                    $oEgreso->subcuenta_id = $subcue_id;
                    $oEgreso->proveedor_id = $pro_id;
                    $oEgreso->cliente_id = 0;
                    $oEgreso->usuario_id = 0;
                    $oEgreso->caja_id = $caj_id;
                    $oEgreso->moneda_id = $_POST['hdd_mon_id'];
                    $oEgreso->modulo_id = $mod_id;
                    $oEgreso->egreso_modide = $clicaj_id;
                    $oEgreso->empresa_id = $_SESSION['empresa_id'];
                    $oEgreso->egreso_ap = 0; //si
                    $oEgreso->insertar();

                    //registrarmos el ingreso del dinero en el tipo de moneda depositado
                    $cue_id_ingre = 7; //cuenta de cancelar cuotas en deposito banco
                    $subcue_id_ingre = 23; //CUOTAS menor
                    if($_POST['hdd_credito'] == "asiveh")
                            $subcue_id_ingre = 22; //CUOTAS asiveh
                    if($_POST['hdd_credito'] == "garveh")
                            $subcue_id_ingre = 24; //CUOTAS garveh
                    if($_POST['hdd_credito'] == "hipo")
                            $subcue_id_ingre = 25; //CUOTAS hipo
                    $numdoc = '';
                
                    $oIngreso->ingreso_usureg=$_SESSION['usuario_id'];
                    $oIngreso->ingreso_usumod=$_SESSION['usuario_id'];
    //			$oIngreso->ingreso_xac=1;
                    $oIngreso->ingreso_fec=$fecha;
                    $oIngreso->documento_id=8;
                    $oIngreso->ingreso_numdoc=$numdoc;
                    $oIngreso->ingreso_det=$ingreso_detalle;
                    $oIngreso->ingreso_imp=moneda_mysql($_POST['txt_cuopag_mon']);
    //			$oIngreso->ingreso_est=1;
                    $oIngreso->cuenta_id=$cue_id_ingre;
                    $oIngreso->subcuenta_id=$subcue_id_ingre;
                    $oIngreso->cliente_id=$_POST['hdd_cli_id'];
                    $oIngreso->caja_id=$tipo_caja;
                    $oIngreso->moneda_id=intval($_POST['hdd_mon_iddep']);
                    $oIngreso->modulo_id=5;
                    $oIngreso->ingreso_modide=$cuopag_id;
                    $oIngreso->empresa_id=$_SESSION['empresa_id'];

                    $result=$oIngreso->insertar();
                    if(intval($result['estado']) == 1){
                        $ingr_id = $result['ingreso_id'];
                    }

                    $oIngreso->modificar_campo($ingr_id, $_SESSION['usuario_id'], 'tb_ingreso_numdoc', $ingr_id,'STR');

                    //despues de ingresar el monto del banco, hay que egresarlo para no alterar la caja

                    $subcue_id = 72;//72 es la subcuenta de cuenta en dolares
                    if($_POST['hdd_mon_iddep'] == 1)
                            $subcue_id = 71; // 71 es cuenta en soles

                    $xac = 1;
                    $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
                    $pro_id = 1;//sistema
                    $mod_id = 5; //el mod id sera 5 que indica deposito en banco
                    $modide = $cuopag_id; //el modid será el id del cuotapago
                    $egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
                    $caj_id = 1; //caja id 14 ya que es CAJA AP
                    $numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$ingr_id;

                    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                    $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                    $oEgreso->egreso_fec = $fecha;
                    $oEgreso->documento_id = 9;//otros egresos
                    $oEgreso->egreso_numdoc = $numdoc;
                    $oEgreso->egreso_det = $egr_det;
                    $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
                    $oEgreso->egreso_tipcam = 0;
                    $oEgreso->egreso_est = 1;
                    $oEgreso->cuenta_id = $cue_id;
                    $oEgreso->subcuenta_id = $subcue_id;
                    $oEgreso->proveedor_id = $pro_id;
                    $oEgreso->cliente_id = 0;
                    $oEgreso->usuario_id = 0;
                    $oEgreso->caja_id = $caj_id;
                    $oEgreso->moneda_id = intval($_POST['hdd_mon_iddep']);
                    $oEgreso->modulo_id = $mod_id;
                    $oEgreso->egreso_modide = $modide;
                    $oEgreso->empresa_id = $_SESSION['empresa_id'];
                    $oEgreso->egreso_ap = 0; //si
                    $oEgreso->insertar();

                    $cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: '.$egr_det.' ||&& ';

                    //estado cuota
                    $rws=$oCuotadetalle->filtrar($_POST['hdd_cuo_id']);

                    if($rws['estado']==1){
                        foreach($rws['data'] as $key=>$rw){
                            $est=3;
                                if($rw['tb_cuotadetalle_est']!=2)break;
                                $est=2;
                        }
                    }

                $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',$est,'INT');//pago parcial

                //si se ha pagado todo el credito, cambia a estado LIQUIDADO
                $dts = $oCredito->mostrarUno($_POST['hdd_cre_id']);
                    if($dts['estado']==1){
                        $num_cuo = $dts['data']['tb_credito_numcuo'];
                    }

                //listamos las cuotas pagadas del crédito
                $dts = $oCuota->listar_cuotas_pagadas($_POST['hdd_cre_id'], $cre_tip);
                    if($dts['estado']==1){
                        $rows = count($dts);
                    }
                //si el numero de cuotas del credito e igual al numero de cuotas pagadas entonces el credito pasa a LIQUIDADO, est = 7
                if($num_cuo == $rows){
                        $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 7,'INT'); //cambiamos a liquidado al estado
                        $liqui = ' | EL crédito ha pasado a liquidado';

   		//registramos el historial
            $his = '<span style="color: green;">El crédito pasó a liquidado ya que se pagó la última cuota. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
            $oCredito->registro_historial($_POST['hdd_cre_id'], $his);
            }

            $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

            $data['cuopag_id'] = $cuopag_id;
            $data['cuopag_msj'] = 'Se registró el pago correctamente.'.$abono_antiguo.' '.$dio_vuelto.$liqui;
	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}




//SI EL CREDITO ES GARVEH O HIPO




if($_POST['hdd_cuotatip_id'] == 3 && ($_POST['hdd_credito'] == "garveh" || $_POST['hdd_credito'] == "hipo"))
{
//    echo 'estoy entrando a la cuota tipo 4';exit();
    
  if(!empty($_POST['txt_cuopag_montot']))
  {
  	$cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
        $cuota_ultima = $_POST['hdd_cuota_ultima']; // 1 al pagarla por completo la desaparecemos de la lista, estado 2 directo
    
    $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: '.trim($_SESSION['usuario_nom']).', en la fecha de: '.date('d-m-Y h:i a').', desde la computadora con nombre: '.$nombre_pc.' y MAC: '.$mac_pc.' ||&& ';

    $xac=1;
    $oCuotapago->tb_cuotapago_xac=1;    
    $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
    $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
    $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
    $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
    $oCuotapago->tb_cuotapago_fec=$fecha; 
    $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
    $oCuotapago->tb_cuotapago_mon=moneda_mysql($mon_pagado);
    $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
    $oCuotapago->tb_cuotapago_moncam=moneda_mysql(0);
    $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($mon_pagado);
    $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
    $oCuotapago->tb_cuotapago_obs=$_POST['txt_cuopag_obs'];

    $result=$oCuotapago->insertar();
        if(intval($result['estado']) == 1){
            $cuopag_id = $result['tb_cuotapago_id'];
        }

    //estado de cuota   $ 8.57                     $ 210.12
    if(moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot))
    {
      $data['cuopag_id'] = 0;
    	$data['cuopag_msj'] = 'Para liquidar todo el crédito, por favor en la parte superior seleccione Refinanciar / Liquidar';

    	echo json_encode($data);
    	exit();
    }
//                  $ 8.57                      $ 210.12
    if(moneda_mysql($mon_pagado)<moneda_mysql($cuopag_tot))
    {
//                    $ 8.57                      $ 0.00
      if(moneda_mysql($mon_pagado) < moneda_mysql($_POST['txt_cuopag_min']))
      {
        $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_est','3','INT');
        //cuando se genera un ingreso entonces la cuota de esa cuotadetalle le cambiamos a estado 3 en pago parcial
        $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',3,'INT');//pago parcial

        $monto = moneda_mysql($mon_pagado);
        if($monto>0)$ingresar=1;

      }
//                    $ 8.57                      $ 0.00
      if(moneda_mysql($mon_pagado) >= moneda_mysql($_POST['txt_cuopag_min']))
      {
        //antes cuando se pagaba mayor al minimo la cuota tenia estado 2, pero el cliente puede seguir avonando en esa misma cuota si es que su fecha aun no vence, entonces para que siga saliendo la cuota le damos estado 3 de pago parcial
        $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_est','2','INT');//SE CAMBIO EL ESTADO DE 3 A 2
        //cuando se genera un ingreso entonces la cuota de esa cuotadetalle le cambiamos a estado 3 en pago parcial
        $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',2,'INT');//pago parcial SE CAMBIO EL ESTADO DE 3 A 2

        //registro de mora
        $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
        if($_POST['chk_mor_aut']==1)$oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_moraut','1','INT');
        if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_morest','1','INT');
            $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
        }

        $monto = moneda_mysql($mon_pagado);
        if($monto>0)$ingresar=1;

        $capital = moneda_mysql($_POST['txt_cuopag_tot']) - moneda_mysql($mon_pagado);
        $amorti = moneda_mysql($_POST['txt_cuopag_montot']) - moneda_mysql($_POST['txt_cuopag_min']);
        //$generar_cuota=1;
        if($cuota_ultima == 1){
          // es decir después de esta cuota hay más generadas ya, por ello es que hay que cambiarla de estado a 2 directamente
          $oCuotadetalle->modificar_campo($_POST['hdd_modid'],'tb_cuotadetalle_est','2','INT');
          $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est',2,'INT');
        }
        
         if($amorti > 0)
            $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($amorti));
            $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'],moneda_mysql($_POST['txt_cuopag_min']));
            $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']);
//         if($capital > 0)
//            $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($capital));
//            $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'],moneda_mysql($_POST['txt_cuopag_min']));
//            $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']);
//
//          //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
//          //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
//          $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3,'INT');
      }
    }

    if($ingresar==1)
    {
    	//registro de ingreso
        $subcue_id = 0; //CUOTAS asiveh
        if($_POST['hdd_credito'] == "garveh")
                $subcue_id = 3; //CUOTAS garveh
        if($_POST['hdd_credito'] == "hipo")
                $subcue_id = 4; //CUOTAS hipo

        $cue_id = 1; //CANCEL CUOTAS
        $mod_id = 30; //cuota pago
        $ing_det = $_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];

        if(!empty($_POST['txt_cuopag_obs']))
                $ing_det .='. Obs: '.$_POST['txt_cuopag_obs'];

        $numdoc = $_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha; 
        $oIngreso->documento_id = 8; 
        $oIngreso->ingreso_numdoc = $numdoc;
        $oIngreso->ingreso_det = $ing_det; 
        $oIngreso->ingreso_imp = moneda_mysql($monto);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id; 
        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
        $oIngreso->caja_id = 1; 
        $oIngreso->moneda_id = $_POST['hdd_mon_id'];
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id; 
        $oIngreso->ingreso_modide = $cuopag_id; 
        $oIngreso->empresa_id = $_SESSION['empresa_id'];
        $oIngreso->ingreso_fecdep = ''; 
        $oIngreso->ingreso_numope = ''; 
        $oIngreso->ingreso_mondep = 0; 
        $oIngreso->ingreso_comi = 0; 
        $oIngreso->cuentadeposito_id = 0; 
        $oIngreso->banco_id = 0; 
        $oIngreso->ingreso_ap = 0; 
        $oIngreso->ingreso_detex = '';
            
        $result=$oIngreso->insertar();
        if(intval($result['estado']) == 1){
            $ingr_id = $result['ingreso_id'];
        }
        $oIngreso->ingreso_fecdep=fecha_mysql($_POST['txt_cuopag_fecdep']);
        $oIngreso->ingreso_numope =$_POST['txt_cuopag_numope'];
        $oIngreso->ingreso_mondep=moneda_mysql($_POST['txt_cuopag_mon']);
        $oIngreso->ingreso_comi= moneda_mysql($_POST['txt_cuopag_comi']);
        $oIngreso->cuentadeposito_id= $_POST['cmb_cuedep_id'];
        $oIngreso->banco_id =$_POST['cmb_banco_id'];
        $oIngreso->ingreso_id=$ingr_id;
                
        $oIngreso->registrar_datos_deposito_banco();   
                        
        $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';
        $ingreso_detalle = $ing_det;
    }

    //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
        $subcue_id = 72;//72 es la subcuenta de cuenta en dolares
        if($_POST['hdd_mon_iddep'] == 1)
                $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1;//sistema
        $mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
        $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
        $egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9;//otros egresos
            $oEgreso->egreso_numdoc = $numdoc;
            $oEgreso->egreso_det = $egr_det;
            $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cue_id;
            $oEgreso->subcuenta_id = $subcue_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caj_id;
            $oEgreso->moneda_id = intval($_POST['hdd_mon_id']);
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $modide;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

        //registrarmos el ingreso del dinero en el tipo de moneda depositado
        $cue_id_ingre = 7; //cuenta de cancelar cuotas en deposito banco
        $subcue_id_ingre = 23; //CUOTAS menor
        if($_POST['hdd_credito'] == "asiveh")
                $subcue_id_ingre = 22; //CUOTAS asiveh
        if($_POST['hdd_credito'] == "garveh")
                $subcue_id_ingre = 24; //CUOTAS garveh
        if($_POST['hdd_credito'] == "hipo")
                $subcue_id_ingre = 25; //CUOTAS hipo
        $numdoc = '';

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha; 
            $oIngreso->documento_id = 8; 
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ingreso_detalle; 
            $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_mon']);
            $oIngreso->cuenta_id = $cue_id_ingre;
            $oIngreso->subcuenta_id = $subcue_id_ingre; 
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
            $oIngreso->caja_id = 1; 
            $oIngreso->moneda_id = intval($_POST['hdd_mon_iddep']);
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = 5; 
            $oIngreso->ingreso_modide = $cuopag_id; 
            $oIngreso->empresa_id = $_SESSION['empresa_id'];

            $result=$oIngreso->insertar();
            if(intval($result['estado']) == 1){
                $ingr_id = $result['ingreso_id'];
            }

        $oIngreso->modificar_campo($ingr_id, $_SESSION['usuario_id'], 'tb_ingreso_numdoc', $ingr_id,'STR');

        //despues de ingresar el monto del banco, hay que egresarlo para no alterar la caja

        $subcue_id = 72;//72 es la subcuenta de cuenta en dolares
        if($_POST['hdd_mon_iddep'] == 1)
                $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1;//sistema
        $mod_id = 5; //el mod id sera 5 que indica deposito en banco
        $modide = $cuopag_id; //el modid será el id del cuotapago
        $egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$ingr_id;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9;//otros egresos
            $oEgreso->egreso_numdoc = $numdoc;
            $oEgreso->egreso_det = $egr_det;
            $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_mon']);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cue_id;
            $oEgreso->subcuenta_id = $subcue_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caj_id;
            $oEgreso->moneda_id = intval($_POST['hdd_mon_iddep']);
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $modide;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

    //la generación de una cuota nueva se hace aparte mediante un botón en el form del pago

        $cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: '.$egr_det.' ||&& ';
        $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

    $data['cuopag_id'] = $cuopag_id;
    $data['cuopag_msj'] = 'Se registró el pago correctamente.';
  }
  else
  {
    $data['cuopag_msj']='Intentelo nuevamente.';
  }
  echo json_encode($data);
}







/* CUOTA FIJAS DE CREDITOS MENORES */

if($_POST['hdd_cuotatip_id'] == 2 && $_POST['hdd_credito'] == "menor") //cuotas FIJAS
{
    
    
       if(($monto_pagado_activado_campo>$cuopag_tot)){
            $data['cuopag_id'] = 0;
            $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO Mínimo, no puede pagar mas de lo depositado. Monto ingresado: '.mostrar_moneda($monto_pagado_activado_campo).', monto mínimo: '.($cuopag_tot).' $pago_minimo_mostrado_sistema= '.$pago_minimo_mostrado_sistema;
            echo json_encode($data);
            exit();
        }
    
    
    
	if(!empty($_POST['txt_cuopag_mon']))
	{
		$cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
		$mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
                $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

                $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: '.trim($_SESSION['usuario_nom']).', en la fecha de: '.date('d-m-Y h:i a').', desde la computadora con nombre: '.$nombre_pc.' y MAC: '.$mac_pc.' ||&& ';
                
                $xac=1;
                $oCuotapago->tb_cuotapago_xac=1;    
                $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
                $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
                $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
                $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
                $oCuotapago->tb_cuotapago_fec=$fecha; 
                $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
                $oCuotapago->tb_cuotapago_mon=moneda_mysql($mon_pagado);
                $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
                $oCuotapago->tb_cuotapago_moncam=0;
                $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($mon_pagado);
                $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
                $oCuotapago->tb_cuotapago_obs=$_POST['txt_cuopag_obs'];

                $result=$oCuotapago->insertar();
                    if(intval($result['estado']) == 1){
                        $cuopag_id = $result['tb_cuotapago_id'];
                    }

		//estado de cuota
		if(moneda_mysql($mon_pagado) == moneda_mysql($cuopag_tot))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
			//registro de mora
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
			if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut','1','INT');
			if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
				$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','1','INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
			}

			$monto = moneda_mysql($cuopag_tot);
			if($monto>0)$ingresar=1;
		}

		if(moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','3','INT');

			$monto = moneda_mysql($mon_pagado);
			if($monto>0)$ingresar=1;
		}

		if(moneda_mysql($mon_pagado) > moneda_mysql($cuopag_tot))
		{
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
			//registro de mora
			$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
			if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut','1','INT');
			if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
				$oCuota->modificar_campo($_POST['hdd_cuo_id'],'morest','1','INT');
				$cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
			}

			$monto = moneda_mysql($cuopag_tot);
			if($monto>0)$ingresar=1;

			$abono = moneda_mysql($mon_pagado)-moneda_mysql($cuopag_tot);
			if($abono>0)$abonar=1;
		}


		if($ingresar == 1)
		{
                    //registro de ingreso
                    $cue_id=1; //CANCEL CUOTAS
                    $subcue_id=2; //CUOTAS menor
                    $mod_id=30; //cuota pago
                    $ing_det = $_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
                    if(!empty($_POST['txt_cuopag_obs']))
                            $ing_det .='. Obs: '.$_POST['txt_cuopag_obs'];
                    $numdoc = $_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                    $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_fec = $fecha; 
                    $oIngreso->documento_id = 8; 
                    $oIngreso->ingreso_numdoc = $numdoc;
                    $oIngreso->ingreso_det = $ing_det; 
                    $oIngreso->ingreso_imp = moneda_mysql($monto);
                    $oIngreso->cuenta_id = $cue_id;
                    $oIngreso->subcuenta_id = $subcue_id; 
                    $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                    $oIngreso->caja_id = 1; 
                    $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                    //valores que pueden ser cambiantes según requerimiento de ingreso
                    $oIngreso->modulo_id = $mod_id; 
                    $oIngreso->ingreso_modide = $cuopag_id; 
                    $oIngreso->empresa_id = $_SESSION['empresa_id'];
                    $oIngreso->ingreso_fecdep = ''; 
                    $oIngreso->ingreso_numope = ''; 
                    $oIngreso->ingreso_mondep = 0; 
                    $oIngreso->ingreso_comi = 0; 
                    $oIngreso->cuentadeposito_id = 0; 
                    $oIngreso->banco_id = 0; 
                    $oIngreso->ingreso_ap = 0; 
                    $oIngreso->ingreso_detex = '';

                    $result=$oIngreso->insertar();
                        if(intval($result['estado']) == 1){
                            $ingr_id = $result['ingreso_id'];
                        }

                    $oIngreso->ingreso_fecdep =fecha_mysql($_POST['txt_cuopag_fecdep']);
                    $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
                    $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
                    $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
                    $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
                    $oIngreso->banco_id = $_POST['cmb_banco_id'];
                    $oIngreso->ingreso_id=$ingr_id;
                    $oIngreso->registrar_datos_deposito_banco();

                    $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';

                    $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha hecho la transacción: '.$ing_det.'. && <b>'.date('d-m-Y h:i a').'</b>';
                    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
                    $oLineaTiempo->tb_lineatiempo_det=$line_det;
                    $oLineaTiempo->insertar();
		}

		if($abonar==1)
		{
			//abono cliente caja
                        $oClientecaja->tb_clientecaja_xac = 1;
                        $oClientecaja->tb_cliente_id=$_POST['hdd_cli_id'];
                        $oClientecaja->tb_credito_id=$_POST['hdd_cre_id'];
                        $oClientecaja->tb_clientecaja_tip=$clicaj_tip; 
                        $oClientecaja->tb_clientecaja_fec=$fecha; 
                        $oClientecaja->tb_moneda_id=$_POST['hdd_mon_id']; 
                        $oClientecaja->tb_clientecaja_mon=moneda_mysql($abono); 
                        $oClientecaja->tb_modulo_id=$mod_id; 
                        $oClientecaja->tb_clientecaja_modid=$cuopag_id; 
                        //abono cliente caja
                        $result1=$oClientecaja->insertar();
                        if(intval($result['estado']) == 1){
                            $clicaj_id = $result['tb_clientecaja_id'];
                        }

			$cue_id=21;//saldo a favor
			$subcue_id=62;//saldo a favor
			$mod_id=40;//cliente caja
			$ing_det="ABONO CLIENTE ".$_POST['hdd_pag_det'];
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$clicaj_id;

                        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_fec = $fecha; 
                        $oIngreso->documento_id = 8; 
                        $oIngreso->ingreso_numdoc = $numdoc;
                        $oIngreso->ingreso_det = $ing_det; 
                        $oIngreso->ingreso_imp = moneda_mysql($abono);
                        $oIngreso->cuenta_id = $cue_id;
                        $oIngreso->subcuenta_id = $subcue_id; 
                        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                        $oIngreso->caja_id = 1; 
                        $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                        //valores que pueden ser cambiantes según requerimiento de ingreso
                        $oIngreso->modulo_id = $mod_id; 
                        $oIngreso->ingreso_modide = $clicaj_id; 
                        $oIngreso->empresa_id = $_SESSION['empresa_id'];

                        $result=$oIngreso->insertar();
                            if(intval($result['estado']) == 1){
                                $ingr_abono_id = $result['ingreso_id'];
                            }
                        $oIngreso->ingreso_fecdep =fecha_mysql($_POST['txt_cuopag_fecdep']);
                        $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
                        $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
                        $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
                        $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
                        $oIngreso->banco_id = $_POST['cmb_banco_id'];
                        $oIngreso->ingreso_id=$ingr_abono_id;

			$oIngreso->registrar_datos_deposito_banco();
			
			$cuopag_his .= 'El ingreso generado fue mayor a la cuota facturada, por eso a la Caja Cliente fue un saldo a favor de: S/. '.mostrar_moneda($abono).' ||&& ';
		}
		//generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
		$subcue_id = 72;//72 es la subcuenta de cuenta en dolares
		if($_POST['hdd_mon_iddep'] == 1)
			$subcue_id = 71; // 71 es cuenta en soles

		$xac = 1;
		$cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
		$pro_id = 1;//sistema
		$mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
		$modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
		$egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
		$caj_id = 1; //caja id 14 ya que es CAJA AP
		$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

                $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                $oEgreso->egreso_fec = $fecha;
                $oEgreso->documento_id = 9;//otros egresos
                $oEgreso->egreso_numdoc = $numdoc;
                $oEgreso->egreso_det = $egr_det;
                $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
                $oEgreso->egreso_tipcam = 0;
                $oEgreso->egreso_est = 1;
                $oEgreso->cuenta_id = $cue_id;
                $oEgreso->subcuenta_id = $subcue_id;
                $oEgreso->proveedor_id = $pro_id;
                $oEgreso->cliente_id = 0;
                $oEgreso->usuario_id = 0;
                $oEgreso->caja_id = $caj_id;
                $oEgreso->moneda_id = $_POST['hdd_mon_id'];
                $oEgreso->modulo_id = $mod_id;
                $oEgreso->egreso_modide = $modide;
                $oEgreso->empresa_id = $_SESSION['empresa_id'];
                $oEgreso->egreso_ap = 0; //si
                $oEgreso->insertar();

		$cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: '.$egr_det.' ||&& ';

    //verificamos si es el ultimo pago, si es el ultimo pago verificamos que se haya pagado todo, para hacer el pedido de todas las garantias vinculadas a ese credito
    $pedidos = '';
    if($_POST['hdd_ultimo_pago'] == 1){
      //ultimo pago 0 aun no, 1 si es ultimo pago, verificamos si en el ultimo pago cancela todo el monto, de ser asi se piden las garantias sino se vuelve esperar el ultimo pago
      if(moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot)){

      	$cuopag_his .= 'Este pago fue el último, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrea de las garantías. ||&& ';

        //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
        $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est','INT');

        //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
        $dts = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito
//          if($dts['estado']==1){
            $rows = count($dts['data']);
//          }
        if($dts['estado']==1){

          $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE '.$rows.'. REVISE EN INVENTARIO';
          foreach ($dts['data']as $key=>$dt) {
            $gar_id = $dt['tb_garantia_id'];
            $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20
            $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

            $det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho el pedido de la garantía: <b>'.$dt['tb_garantia_pro'].'</b> | '.date('d-m-Y h:i a');
            $pedido = '<strong style="color: blue;">Pedido por: '.$_SESSION['usuario_nom'].' a VICTOR MANUEL | '.date('d-m-Y h:i a').'</strong><br>';

            //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
            $dts1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');
              $num_rows = count($dts1['data']);
//            mysql_free_result($dts1);

            if($num_rows == 0){
              //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
                $oTarea->tb_tarea_usureg=$_SESSION['usuario_id'];
                $oTarea->tb_tarea_det=$det;
                $oTarea->tb_usuario_id=$usu_id;
                $oTarea->tb_tarea_usutip=$usu_tip;
                $oTarea->tb_garantia_id =$gar_id;
                $oTarea->tb_tarea_tipo='enviar';
                $oTarea->insertar();

              //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
              $oGarantia->modificar_campo($gar_id,'tb_garantia_ped',1,'INT');

              //registramos al usuario que pide la garantía
              $oGarantia->estado_usuario_pedido($gar_id,$pedido);
            }
          }
        }
        else
          $pedidos = ' IPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';
//        mysql_free_result($dts);
      }
    }
        $oCuotapago->tb_cuotapago_id=$cuopag_id;
        $oCuotapago->tb_cuotapago_his=$cuopag_his;
        $oCuotapago->registro_historial(); //registramos toda la información obtenida del pago

		$data['cuopag_id']=$cuopag_id;
		$data['cuopag_msj']='Se registró el pago correctamente.'.$pedidos;
	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}

/* PAGO DE CRÉDITO MENOR EN CUOTAS LIBRES*/

if($_POST['hdd_cuotatip_id'] == 1 && $_POST['hdd_credito'] == "menor") //cuotas LIBRES
{
    
    if($vencida==1 && ($monto_pagado_activado_campo>$pago_minimo_mostrado_sistema)){
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO Mínimo, no puede pagar mas de lo depositado por que su cuota esta Vencida. Monto ingresado: '.mostrar_moneda($monto_pagado_activado_campo).', monto mínimo: '.mostrar_moneda($pago_minimo);
    echo json_encode($data);
    exit();
}
    
    
    
	if(!empty($_POST['txt_cuopag_mon']))
	{
//            $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
            $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
//            $cuopag_tot = $_POST['txt_cuopag_min'];
            $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

            $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: '.trim($_SESSION['usuario_nom']).', en la fecha de: '.date('d-m-Y h:i a').', desde la computadora con nombre: '.$nombre_pc.' y MAC: '.$mac_pc.' ||&& ';

            $xac=1;
            $oCuotapago->tb_cuotapago_xac=$xac;    
            $oCuotapago->tb_cuotapago_usureg=$_SESSION['usuario_id'];
            $oCuotapago->tb_cuotapago_usumod=$_SESSION['usuario_id'];  
            $oCuotapago->tb_modulo_id=$_POST['hdd_mod_id'];
            $oCuotapago->tb_cuotapago_modid=$_POST['hdd_modid'];
            $oCuotapago->tb_cuotapago_fec=$fecha; 
            $oCuotapago->tb_moneda_id=$_POST['hdd_mon_id'];
            $oCuotapago->tb_cuotapago_mon=moneda_mysql($mon_pagado);
            $oCuotapago->tb_cuotapago_tipcam=moneda_mysql($_POST['txt_cuopag_tipcam']);
            $oCuotapago->tb_cuotapago_moncam=moneda_mysql(0);
            $oCuotapago->tb_cuotapago_pagcon=moneda_mysql($mon_pagado);
            $oCuotapago->tb_cliente_id=$_POST['hdd_cli_id'];
            $oCuotapago->tb_cuotapago_obs=$_POST['txt_cuopag_obs'];
            
            $result=$oCuotapago->insertar();
                if(intval($result['estado']) == 1){
                    $cuopag_id = $result['tb_cuotapago_id'];
                }


            $pedidos = '';
		//estado de cuota 600                       500
		if(moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot))
		{
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
                    //registro de mora
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
                    if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut','1','INT');
                    if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
                            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','1','INT');
                            $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
                    }

                    $monto = moneda_mysql($mon_pagado);
                    if($monto>0)$ingresar=1;

                    $cuopag_his .= 'Este pago fue igual al monto facturado de la cuota, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrega de las garantías. ||&& ';

                    //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
                    $oCredito->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est',2,'INT');// se cambio el 2 por el 7
        
                    //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
                    $dts = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito
                    $rows = count($dts['data']);

                if($rows > 0){

                  $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE '.$rows.'. REVISE EN INVENTARIO';
                  foreach ($dts['data']as $key=>$dt) {
                    $gar_id = $dt['tb_garantia_id'];
                    $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20
                    $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

                    $det = '<b>'.$_SESSION['usuario_nom'].'</b> ha hecho el pedido de la garantía: <b>'.str_replace("'", "", $dt['tb_garantia_pro']).'</b> | '.date('d-m-Y h:i a');
                    $pedido = '<strong style="color: blue;">Pedido por: '.$_SESSION['usuario_nom'].' a VICTOR MANUEL | '.date('d-m-Y h:i a').'</strong><br>';

                    //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
                    $dts1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');
                    $num_rows = count($dts1['data']);
                     /*$num_rows = mysql_num_rows($dts1);
                    mysql_free_result($dts1);*/

                    if($num_rows == 0){
                      $oTarea->tb_tarea_usureg=$_SESSION['usuario_id']; 
                      $oTarea->tb_tarea_det=$det;
                      $oTarea->tb_usuario_id=$usu_id;
                      $oTarea->tb_tarea_usutip= $usu_tip;
                      $oTarea->tb_garantia_id =$gar_id;
                      $oTarea->tb_tarea_tipo='enviar';
                      //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
                      $oTarea->insertar();
                      //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
                      $oGarantia->modificar_campo($gar_id,'tb_garantia_ped',1,'INT');

                      //registramos al usuario que pide la garantía
                      $oGarantia->estado_usuario_pedido($gar_id,$pedido);
                    }
                  }
                }
                else
                  $pedidos = ' IPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';
//                mysql_free_result($dts);
		}
//estado de cuota                   500                       600
		if(moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot))
		{
//                                      500                             200
//                    if(moneda_mysql($mon_pagado) < moneda_mysql($_POST['txt_cuopag_min']))
                    if(moneda_mysql($mon_pagado) < moneda_mysql($pago_minimo))
                    {
                            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','3','INT');

                            $monto = moneda_mysql($mon_pagado);
                            if($monto>0)$ingresar=1;

                    }
//                                  500                                     200
                    if(moneda_mysql($mon_pagado) >= moneda_mysql($pago_minimo))
                    {
                    //si el pago es mayor que el minimo aun no toma por cancelada la cuota, ya que se tiene que verificar si la fecha de pago ya venció. SI ya venció se genera una nueva cuota y esta cambia a 2 si es que se pagó el mínimo
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_est','2','INT');
                    /* SE MODIFICO EL NUMERO 3 POR 2*/
                    //registro de mora
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_mor',moneda_mysql($_POST['txt_cuopag_mor']),'STR');
                     if($_POST['chk_mor_aut']==1)$oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_moraut','1','INT');
                    if(moneda_mysql($_POST['txt_cuopag_mor']) > 0){
                            $oCuota->modificar_campo($_POST['hdd_cuo_id'],'tb_cuota_morest','1','INT');
                            $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. '.mostrar_moneda($_POST['txt_cuopag_mor']).' ||&& ';
                    }

                    $monto = moneda_mysql($mon_pagado);
                    if($monto>0)$ingresar=1;

                        //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
                        //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
                        //$oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3,'INT');

                        $generar_cuota=1;
                        //agregamos el monto de amortización que se hace al capital
                        $amorti = moneda_mysql($_POST['txt_cuopag_mon']) - moneda_mysql($pago_minimo);
                        if($amorti > 0)
                          $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($amorti));
                          $suma_pago_actual_y_pagos_previos = $pago_minimo + $pagos;
                        
                          $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'],moneda_mysql($suma_pago_actual_y_pagos_previos)); 
                          $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']); //tb_cuota_cuo = a la suma del capital + pagos hechos

                        //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
                        //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
                        $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3,'INT');
                        
                        
                    }

		}
		if($ingresar==1)
		{
		//registro de ingreso

			$cue_id=1; //CANCEL CUOTAS
			$subcue_id=2; //CUOTAS menor
			$mod_id=30; //cuota pago
			$ing_det = $_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
			if(!empty($_POST['txt_cuopag_obs']))
				$ing_det .='. Obs: '.$_POST['txt_cuopag_obs'];
			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                        $oIngreso->ingreso_fec = $fecha; 
                        $oIngreso->documento_id = 8; 
                        $oIngreso->ingreso_numdoc = $numdoc;
                        $oIngreso->ingreso_det = $ing_det; 
                        $oIngreso->ingreso_imp = moneda_mysql($monto);
                        $oIngreso->cuenta_id = $cue_id;
                        $oIngreso->subcuenta_id = $subcue_id; 
                        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                        $oIngreso->caja_id = 1; 
                        $oIngreso->moneda_id = $_POST['hdd_mon_id'];
                        //valores que pueden ser cambiantes según requerimiento de ingreso
                        $oIngreso->modulo_id = $mod_id; 
                        $oIngreso->ingreso_modide = $cuopag_id; 
                        $oIngreso->empresa_id = $_SESSION['empresa_id'];

                        $result=$oIngreso->insertar();
                            if(intval($result['estado']) == 1){
                                $ingr_id = $result['ingreso_id'];
                            }
                        $oIngreso->ingreso_fecdep =fecha_mysql($_POST['txt_cuopag_fecdep']);
                        $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
                        $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
                        $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
                        $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
                        $oIngreso->banco_id = $_POST['cmb_banco_id'];
                        $oIngreso->ingreso_id=$ingr_id;
                        
			$oIngreso->registrar_datos_deposito_banco();

			$cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';
		}

		//generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
		$subcue_id = 72;//72 es la subcuenta de cuenta en dolares
		if($_POST['hdd_mon_iddep'] == 1)
			$subcue_id = 71; // 71 es cuenta en soles

		$xac = 1;
		$cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
		$pro_id = 1;//sistema
		$mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
		$modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
		$egr_det ='EGRESO POR: '.$_POST['hdd_pag_det'].'. Pago abonado en el banco, en la siguente cuenta: '.$_POST['hdd_cuenta_dep'].', la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].', la comisión del banco fue: '.$moneda_depo.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_depo.' '.$_POST['txt_cuopag_monval'].' y con cambio de moneda el monto pagado a la cuota fue: '.$moneda_cred.' '.$_POST['txt_cuopag_montot'];
		$caj_id = 1; //caja id 14 ya que es CAJA AP
		$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-'.$cuopag_id;

                $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                $oEgreso->egreso_fec = $fecha;
                $oEgreso->documento_id = 9;//otros egresos
                $oEgreso->egreso_numdoc = $numdoc;
                $oEgreso->egreso_det = $egr_det;
                $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
                $oEgreso->egreso_tipcam = 0;
                $oEgreso->egreso_est = 1;
                $oEgreso->cuenta_id = $cue_id;
                $oEgreso->subcuenta_id = $subcue_id;
                $oEgreso->proveedor_id = $pro_id;
                $oEgreso->cliente_id = 0;
                $oEgreso->usuario_id = 0;
                $oEgreso->caja_id = $caj_id;
                $oEgreso->moneda_id = $_POST['hdd_mon_id'];
                $oEgreso->modulo_id = $mod_id;
                $oEgreso->egreso_modide = $modide;
                $oEgreso->empresa_id = $_SESSION['empresa_id'];
                $oEgreso->egreso_ap = 0; //si
                $oEgreso->insertar();

		$cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: '.$egr_det.' ||&& ';
		$oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

                $data['cuopag_id']=$cuopag_id;
                $data['cuopag_msj']='Se registró el pago correctamente.'.$pedidos;

	}
	else
	{
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}
?>
