/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $("#txt_fil_cli").focus();
    $( "#txt_fil_cli" ).autocomplete({
        minLength: 1,
        source: function(request, response){
          $.getJSON(
                  VISTA_URL+"cliente/cliente_autocomplete.php",
                  {term: request.term}, //
                  response
          );
        },
        select: function(event, ui){
          $('#hdd_fil_cli_id').val(ui.item.cliente_id);
          $('#txt_fil_cli').val(ui.item.cliente_nom);
          vencimiento_tabla();
          event.preventDefault();
          $('#txt_fil_cli').focus();
        }
    }); 
});


function vencimiento_tabla(){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"vencimiento/vencimiento_tabla.php",
    async:true,
    dataType: "html",                      
    data: $("#vencimiento_filtro").serialize(),
    beforeSend: function() {
      $('#msj_vencimiento_tabla').html("Cargando datos...");
      $('#msj_vencimiento_tabla').show(100);
      $('#div_vencimiento_tabla').addClass("ui-state-disabled");
    },
    success: function(html){
      $('#div_vencimiento_tabla').html(html);
    },
    complete: function(){     
      $('#div_vencimiento_tabla').removeClass("ui-state-disabled");
      $('#msj_vencimiento_tabla').hide(100);
    }
  });    
}


/* SE SE SELECCIONA DONDE SE IRÁ A REALIZAR EL PAGO SI ES EN LA MISMA OFICINA O EN ALGUNA ENTIDAD BANCARIA*/
function cuotapago_menor_form(act,idf){

  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Oficina!',1500);
      cuotapago_menor_oficina_form(act,idf);
    } else if (result.isDenied) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Banco!',1500);
      cuotapago_banco('menor', idf);

    }
  });

}

/* SI SE SELECCIONA PAGAR EN OFICINA SE EJECUTA ESTA FUNCIÓN*/
function cuotapago_menor_oficina_form(act,idf){ 
  $.ajax({
        type: "POST",
        url: VISTA_URL+"vencimiento/cuotapago_menor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            cuo_id: idf,
            vista:  'vencimiento_tabla'
        }),
        beforeSend: function() {
        },
        success: function(data){
            $('#div_modal_vencimiento_menor_form').html(data);
            $('#modal_registro_vencimientomenor').modal('show');

            modal_height_auto('modal_registro_vencimientomenor'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_registro_vencimientomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function(data){
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
        }
	});
}

/*SI SE SELECCIONA PAGO EN ENTIDAD BANCARIA SE EJECUTA LA SIGUIENTE FUNCIÓN*/
function cuotapago_banco(cred, idf){
  $.ajax({
        type: "POST",
        url: VISTA_URL+"vencimiento/cuotapago_banco.php",
        async:true,
        dataType: "html",                    
        data: ({
            credito: cred,
            cuota_id: idf,
            vista: 'vencimiento_tabla'
        }),
        beforeSend: function() {

        },
        success: function(html){
            $('#modal_mensaje').modal('hide');
            $('#div_modal_vencimiento_menor_banco_form').html(html);
            $('#modal_registro_vencimientomenorbanco').modal('show');
            modal_height_auto('modal_registro_vencimientomenorbanco');
            modal_hidden_bs_modal('modal_registro_vencimientomenorbanco', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function(data){
//                console.log(data);
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
        }
  });
}

/*SI SE SELECCIONA LIQUIDAR EL PAGO DEL PRESTAMO MENOR*/
function cuotapago_menor_form_liq(act,idf, tipo){
//    console.log("Estoy entrando a Liquidación");
  $.ajax({
    type: "POST",
    url: VISTA_URL+"vencimiento/cuotapago_menor_form_liq.php",
    async:true,
    dataType: "html",                    
    data: ({
      action: act,
      cuo_id: idf,
      vista: 'vencimiento_tabla',
      tipo: tipo
    }),
    beforeSend: function() {
//      $('#msj_vencimiento').hide();
//      $('#div_vencimiento_form').dialog("open");
//      $('#div_vencimiento_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
    success: function(data){
        $('#div_modal_vencimiento_menor_form_liquidar').html(data);
        $('#modal_registro_vencimiento_liquidar').modal('show');

        modal_height_auto('modal_registro_vencimiento_liquidar'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_vencimiento_liquidar', 'limpiar'); //funcion encontrada en public/js/generales.js    
//        console.log(data.responseText);
    },
    complete: function (data) {
//        console.log(data);
    },
    error: function(data){
        $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
        $('#overlay_modal_mensaje').removeClass('overlay').empty();
        $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
    }
  });
}

function agregar_cuotas(cre_id){
  //$.alert('Contenido aquí', 'Titulo aqui');
  $.confirm({
    icon: 'fa fa-print',
    title: 'Imprimir',
    content: '¿Está seguro de agregar 2 cuotas más al crédito?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        //ejecutamos AJAX
        $.ajax({
            type: "POST",
            url: VISTA_URL+"creditomenor/creditomenor_controller.php",
            async:true,
            dataType: "JSON",                      
            data: ({ 
                cre_id: cre_id,
                num_cuo: 2,
                action: 'cuotas'
            }),
            beforeSend: function() {
                $('#msj_vencimiento').show();
                $('#msj_vencimiento').html('Agregando 2 cuotas más al crédito solicitado...');
            },
            success: function(data){    
              if(parseInt(data.estado) == 1){ 
                //$('#msj_vencimiento').html('Se ha agregado 2 cuotas más');
                swal_success("SISTEMA",'Se ha agregado 2 cuotas más',3000);
                vencimiento_tabla();
              }
              else
                //alert('Error agregando más cuotas consulte por favor');
                    swal_warning('AVISO','Error agregando más cuotas consulte por favor',6000);
            },
            complete: function(data){
                $('#msj_vencimiento').hide(100);
              if(data.statusText != "success"){
                $('#msj_vencimiento').html("ERROOOR REVISAR CON SISTEMAS");
                console.log(data);
              }
            }
          });
      },
      no: function () {}
    }
  });
}

//
//function agregar_cuotas(cre_id){
//  if(!confirm('¿Está seguro de agregar 2 cuotas más al crédito?'))
//    return false;
//  $.ajax({
//    type: "POST",
//    url: VISTA_URL+"creditomenor/creditomenor_controller.php",
//    async:true,
//    dataType: "JSON",                      
//    data: ({ 
//      cre_id: cre_id,
//      num_cuo: 2,
//      action: 'cuotas'
//    }),
//    beforeSend: function() {
//      $('#msj_vencimiento').show();
//      $('#msj_vencimiento').html('Agregando 2 cuotas más al crédito solicitado...');
//    },
//    success: function(data){    
//      if(parseInt(data.estado) == 1){ 
////        $('#msj_vencimiento').html('Se ha agregado 2 cuotas más');
//        swal_success("SISTEMA",'Se ha agregado 2 cuotas más',4000);
//        vencimiento_tabla();
//      }
//      else
////        alert('Error agregando más cuotas consulte por favor');
//            swal_warning('AVISO','Error agregando más cuotas consulte por favor',6000);
//    },
//    complete: function(data){
//        $('#msj_vencimiento').hide(100);
//      if(data.statusText != "success"){
//        $('#msj_vencimiento').html("ERROOOR REVISAR CON SISTEMAS");
//        console.log(data);
//      }
//    }
//  });
//}

function creditomenor_pagos(creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditomenor_id,
      hdd_form:'vencimiento'
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditomenor_pagos').html(data);
        $('#modal_creditomenor_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditomenor_pagos', 95);
        modal_height_auto('modal_creditomenor_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditomenor_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
function cuotapago_imppos_datos_2(cuotapago_id){
  //$.alert('Contenido aquí', 'Titulo aqui');
  $.confirm({
    icon: 'fa fa-print',
    title: 'Imprimir',
    content: '¿Desea imprimir voucher?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        //ejecutamos AJAX
        $.ajax({
          type: "POST",
          url: VISTA_URL+"cuotapago/cuotapago_imppos_datos_1.php",
          async: true,
          dataType: "JSON",
          data: ({
            cuotapago_id: cuotapago_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Obteniendo datos...');
            $('#modal_mensaje').modal('show');
          },
          success: function(data){
            cuotapago_imppos_ticket(data);
          },
          complete: function(data){
             
                        console.log("Estoy entrando ------");
            console.log(data);
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
//            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}
function cuotapago_imppos_ticket(datos){  
  $.ajax({
    type: "POST",
    url: "http://127.0.0.1/prestamosdelnorte/app/modulos/cuotapago/cuotapago_imppos_ticket.php",
    async:true,
    dataType: "html",                      
    data: datos,
    beforeSend: function() {
      $('#h3_modal_title').text('Imprimiendo...');
    },
    success: function(html){
      $('#modal_mensaje').modal('hide');
    },
    complete: function(data){
      //console.log(data);
    }
  });
}


function creditomenor_form(usuario_act, creditomenor_id){
  $.ajax({
        type: "POST",
        url: VISTA_URL+"creditomenor/creditomenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditomenor_id: creditomenor_id,
            vista: 'creditomenor'
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
                $('#div_modal_creditomenor_form').html(data);
                $('#modal_registro_creditomenor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if(usuario_act == 'L' || usuario_act == 'E')
                  form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_registro_creditomenor', 95);
                modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
              //llamar al formulario de solicitar permiso
              var modulo = 'creditomenor';
              var div = 'div_modal_creditomenor_form';
              permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){

        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
            console.log(data.responseText);
        }
	});
}


function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}



function cuotapago_garveh_form(act,idf){

  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Oficina!',1500);
      cuotapago_menor_oficina_form(act,idf);
    } else if (result.isDenied) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Banco!',1500);
      cuotapago_banco('garveh', idf);
    }

  });

}

function cuotapago_asiveh_form(act,idf)
{

  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Oficina!',1500);
      //cuotapago_menor_oficina_form(act,idf);
    } else if (result.isDenied) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Banco!',1500);
      cuotapago_banco('asiveh', idf);
    }

  });

}


