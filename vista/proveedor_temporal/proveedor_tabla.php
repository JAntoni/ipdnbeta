<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
if (defined('VISTA_URL')) {
    require_once(VISTA_URL . 'proveedor/Proveedor.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../proveedor/Proveedor.class.php');
    require_once('../funciones/fechas.php');
}
$oProveedor = new Proveedor();
?>
<table id="tbl_proveedor" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">PROVEEDOR</th>
            <th id="tabla_cabecera_fila" width="7%">RUC / DNI</th>
            <th id="tabla_cabecera_fila">DIRECCIÓN</th>
            <th id="tabla_cabecera_fila">CONTACTO</th>
            <th id="tabla_cabecera_fila">TELÉFONO</th>
            <th id="tabla_cabecera_fila">EMAIL</th>
            <th id="tabla_cabecera_fila" width="6%"></th>
        </tr>
    </thead>
    <tbody>
<?php
//PRIMER NIVEL
$result = $oProveedor->listar_proveedores();
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value):
        ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_id']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_nom']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_doc']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_dir']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_con']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_tel']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_proveedor_ema']; ?></td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="proveedor_form(<?php echo "'M', " . $value['tb_proveedor_id']; ?>)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="proveedor_form(<?php echo "'E', " . $value['tb_proveedor_id']; ?>)"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
</table>
