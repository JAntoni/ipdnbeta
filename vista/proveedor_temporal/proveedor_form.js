
$(document).ready(function(){
	
	$('input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#form_proveedor').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"proveedor/proveedor_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_proveedor").serialize(),
				beforeSend: function() {
					$('#proveedor_mensaje').show(400);
					$('#btn_guardar_proveedor').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#proveedor_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#proveedor_mensaje').html(data.mensaje);

		      	var vista = $('#proveedor_vista').val();
		      	setTimeout(function(){
		      		if(vista == 'proveedor')
		      			proveedor_tabla();
		      		$('#modal_registro_proveedor').modal('hide');
					if(vista == 'compracontadoc'){
						llenar_datos_proveedor(data);
					}
		      		 }, 1000
		      	);
					}
					else{
		      	$('#proveedor_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#proveedor_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_proveedor').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#proveedor_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#proveedor_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	txt_proveedor_tip: {
	  		required: true,
				minlength: 1
	  	},
			txt_proveedor_nom: {
				required: true,
				minlength: 2
			},
			txt_proveedor_doc: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 11
			},
			txt_proveedor_dir: {
				required: true,
				minlength: 5
			},
			txt_proveedor_con: {
				required: true,
				minlength: 2
			},
			txt_proveedor_tel: {
				required: true,
				digits: true,
				minlength: 6,
				maxlength: 9
			},
			txt_proveedor_ema: {
				required: false,
				email: true
			}
		},
		messages: {
			txt_proveedor_tip: {
				required: "Seleccione el tipo de Persona",
				minlength: "Seleccione el tipo de Persona"
			},
			txt_proveedor_nom: {
				required: "Ingrese un nombre para el Proveedor",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_proveedor_doc: {
				required: "Ingrese el Documento del Proveedor",
				digits: "Solo números por favor",
				minlength: "El Documento como mínimo debe tener 8 números",
				maxlength: "El Documento como máximo debe tener 11 números"
			},
			txt_proveedor_dir: {
				required: "Ingrese una dirección para el Proveedor",
				minlength: "La dirección debe tener como mínimo 5 caracteres"
			},
			txt_proveedor_con: {
				required: "Ingrese un nombre de contacto para el Proveedor",
				minlength: "El contacto debe tener como mínimo 2 caracteres"
			},
			txt_proveedor_tel: {
				required: "Ingrese el Teléfono del Proveedor",
				digits: "Solo números por favor",
				minlength: "El Teléfono como mínimo debe tener 6 números",
				maxlength: "El Teléfono como máximo debe tener 9 números"
			},
			txt_proveedor_ema: {
				email: "Ingrese un Email válido"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
