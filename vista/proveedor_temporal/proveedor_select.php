<?php
require_once('Proveedor.class.php');
$oProveedor = new Proveedor();

$proveedor_id = (empty($proveedor_id))? 0 : $proveedor_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oProveedor->listar_proveedores();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($proveedor_id == $value['tb_proveedor_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_proveedor_id'].'" '.$selected.'>'.$value['tb_proveedor_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>