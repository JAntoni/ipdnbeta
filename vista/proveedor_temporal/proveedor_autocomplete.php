<?php
  require_once ("Proveedor.class.php");
  $oProveedor= new Proveedor();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $tb_proveedor_id;
    var $tb_proveedor_nom;
    var $tb_proveedor_doc;
    var $tb_proveedor_dir;
    var $tb_proveedor_tel;
    var $tb_proveedor_ema;


    function __construct($label, $value, $tb_proveedor_id, $tb_proveedor_nom, $tb_proveedor_doc,$tb_proveedor_dir,$tb_proveedor_tel,$tb_proveedor_ema){
      $this->label = $label;
      $this->value = $value;
      $this->tb_proveedor_id = $tb_proveedor_id;
      $this->tb_proveedor_nom = $tb_proveedor_nom;
      $this->tb_proveedor_doc = $tb_proveedor_doc;
      $this->tb_proveedor_dir = $tb_proveedor_dir;
      $this->tb_proveedor_tel = $tb_proveedor_tel;
      $this->tb_proveedor_ema = $tb_proveedor_ema;
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $datoBuscar = mb_strtoupper($datoBuscar, 'UTF-8');

  //busco un valor aproximado al dato escrito
  $result = $oProveedor->Proveedor_autocomplete($datoBuscar);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    foreach ($result['data'] as $key => $value) {
      array_push(
        $arrayElementos,
        new ElementoAutocompletar(
          $value["tb_proveedor_doc"].' - '.$value["tb_proveedor_nom"],
          $value["tb_proveedor_doc"].' - '.$value["tb_proveedor_nom"],
          $value["tb_proveedor_id"],
          $value["tb_proveedor_nom"],
          $value["tb_proveedor_doc"],
          $value["tb_proveedor_dir"],
          $value['tb_proveedor_tel'],
          $value['tb_proveedor_ema'])
      );
    }

    print_r(json_encode($arrayElementos));
  $result = NULL;
?>
