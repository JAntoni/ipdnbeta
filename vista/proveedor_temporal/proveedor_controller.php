<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../proveedor/Proveedor.class.php');
  $oProveedor = new Proveedor();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$proveedor_tip = $_POST['txt_proveedor_tip']; //1 natural, 2 juridica
 		$proveedor_nom = mb_strtoupper($_POST['txt_proveedor_nom'], 'UTF-8');
 		$proveedor_doc = $_POST['txt_proveedor_doc'];
 		$proveedor_dir = $_POST['txt_proveedor_dir'];
 		$proveedor_con = $_POST['txt_proveedor_con'];
 		$proveedor_tel = $_POST['txt_proveedor_tel'];
 		$proveedor_ema = $_POST['txt_proveedor_ema'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar al Proveedor.';
		$res= $oProveedor->insertar($proveedor_tip, $proveedor_nom, $proveedor_doc, $proveedor_dir, $proveedor_con, $proveedor_tel, $proveedor_ema);
 		if(intval($res['estado']) == 1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proveedor registrado correctamente.';
			$data['registro'] = $oProveedor->mostrarUno($res['nuevo'])['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$proveedor_id = intval($_POST['hdd_proveedor_id']);
 		$proveedor_tip = $_POST['txt_proveedor_tip']; //1 natural, 2 juridica
 		$proveedor_nom = mb_strtoupper($_POST['txt_proveedor_nom'], 'UTF-8');
 		$proveedor_doc = $_POST['txt_proveedor_doc'];
 		$proveedor_dir = $_POST['txt_proveedor_dir'];
 		$proveedor_con = $_POST['txt_proveedor_con'];
 		$proveedor_tel = $_POST['txt_proveedor_tel'];
 		$proveedor_ema = $_POST['txt_proveedor_ema'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar al Proveedor.';

 		if($oProveedor->modificar($proveedor_id, $proveedor_tip, $proveedor_nom, $proveedor_doc, $proveedor_dir, $proveedor_con, $proveedor_tel, $proveedor_ema)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proveedor modificado correctamente. '.$proveedor_des;
			$data['registro'] = $oProveedor->mostrarUno($proveedor_id)['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$proveedor_id = intval($_POST['hdd_proveedor_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al Proveedor.';

 		if($oProveedor->eliminar($proveedor_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proveedor eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>