<?php
require_once('../../core/usuario_sesion.php');

/* require_once ("Cobranza.class.php");
$oCobranza = new Cobranza(); */
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$vista = $_POST['vista'];
$hdd_vista = $_POST['hddvista'];
$cobranza_id = $_POST['cobranza_id'];
$cuota_id = $_POST['cuota_id'];
$credito_id = $_POST['credito_id'];

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_claverapida" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">CLAVE RAPIDA</h4>
            </div>
            <form id="form_claverapida" method="post">
                <input name="action" id="action" type="hidden" value="clave_rapida">
                <input name="vista" id="vista" type="hidden" value="<?php echo $vista; ?>">
                <input name="hdd_vista" id="hdd_vista" type="hidden" value="<?php echo $hdd_vista; ?>">
                <input name="cobranza_id" id="cobranza_id" type="hidden" value="<?php echo $cobranza_id; ?>">
                <input name="cuota_id" id="cuota_id" type="hidden" value="<?php echo $cuota_id; ?>">
                <input name="credito_id" id="credito_id" type="hidden" value="<?php echo $credito_id; ?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 pb-3">
                            <div class="form-group">
                                <label>Clave:</label>
                                <input type="password" name="txt_clave_rapida" id="txt_clave_rapida" class="form-control input-sm" value="" placeholder="Ingrese clave">
                            </div>
                        </div>

                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_claverapida">Ejecutar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/claverapida/claverapida_form.js?ver==20230321';?>"></script>