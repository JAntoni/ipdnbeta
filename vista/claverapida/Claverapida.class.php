<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Claverapida extends Conexion {

    function mostrarClaveActiva($cod, $usuarioperfil_id){
        try {
            $sql="SELECT * FROM tb_config WHERE tb_config_xac = 1 AND tb_config_codigo=:codigo AND tb_usuarioperfil_id=:usuarioperfil_id";
  
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":codigo", $cod, PDO::PARAM_INT);
            $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
            $sentencia->execute();
  
            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

}

?>