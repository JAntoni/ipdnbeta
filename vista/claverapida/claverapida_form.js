$(document).ready(function() {
    $('#txt_clave_rapida').focus();
    
    //Gerson (23-02-23)
    $("#form_claverapida").validate({
        submitHandler: function () {
            var url = '';
            var vista = $("#vista").val();
            if(vista=='cobranza'){
                url = VISTA_URL + "cobranza/cobranza_controller.php";
            }
            $.ajax({
                type: "POST",
                url: url,
                async: true,
                dataType: "json",
                data: $("#form_claverapida").serialize(),
                beforeSend: function () {
                    //$('#msj_cobranza').html("Guardando...");
                    //$('#msj_cobranza').show(100);
                },
                success: function (data) {
                    if(data.estado==1){
                        if(data.vista=='cobranza'){
                            prorrogaCuotaCobranzaProceso(data.value1, data.value2, data.value3, data.value4)
                        }
                    }else{
                        swal_error("SISTEMA", data.mensaje, 3000);
                    }
                },
                complete: function (data) {
                    if (data.responseJSON.estado==1) {
                        $('#modal_claverapida').modal('hide');
                    }else{
                        swal_error("SISTEMA", data.responseJSON.mensaje, 3000);
                    }
                }

            });
        },
        rules: {
            txt_clave_rapida: {
                required: true
            }
        },
        messages: {
            txt_clave_rapida: {
                required: 'Debe ingresar una clave válida'
            }
        }
    });
    //

});