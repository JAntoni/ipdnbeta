<?php
//revision de sesion activa
	if (defined('VISTA_URL')) {
		require_once APP_URL . 'core/usuario_sesion.php';
	} else {
		require_once '../../core/usuario_sesion.php';
	}
//
//todos los require
	require_once '../ejecucion/Ejecucion.class.php';
	$oEjecucion = new Ejecucion();
	require_once '../ejecucionupload/Ejecucionfasefile.class.php';
	$oEjecucionfasefile = new Ejecucionfasefile();
	require_once '../funciones/fechas.php';
//

$data['estado']	= 0;
$data['mensaje'] = '';
$action	= $_POST['action_ejecucionresumen'];
$fecha_hoy = date('Y-m-d');

if ($action == 'grafico_barras') {
	$oEjecucion->cgarvtipo_id = intval($_POST['cgarvtipo_id']);
	//se establece el nombre de las fases de forma estatica para que tengan nombre resumido
	$data['array_fases'] = [
        'Asign. Legal',
        'Cartas: Redac.',
        'Cartas: Revisión',
        'Cartas: A Not.',
        'Cartas: Dil.',
        'Esp. Cliente',
        'Demanda: Redac.',
        'Demanda: Rev.',
        'Demanda: Gtos.',
        'Demanda: MP',
        'Incaut.: CEJ',
        'Incaut.: Gtos.',
        'Incaut.: Captura Veh.',
        'Levant. Orden',
        'Terminado'
    ];

	$estadoejec_id = empty($_POST['txt_fil_estadoejec_id']) ? '' : intval($_POST['txt_fil_estadoejec_id']);
	$fec_ini = empty($_POST['txt_fil_fec1']) ? '' : fecha_mysql($_POST['txt_fil_fec1']);
	$fec_fin = empty($_POST['txt_fil_fec2']) ? '' : fecha_mysql($_POST['txt_fil_fec2']);
	$encargado_id = empty($_POST['txt_fil_encargado_id']) ? '' : intval($_POST['txt_fil_encargado_id']);
	$orden = empty($_POST['cbo_fil_orden_atraso']) ? '' : intval($_POST['cbo_fil_orden_atraso']);
	$cgvsubtipo_id = empty($_POST['cbo_fil_cgvsubtipo_id']) ? '' : intval($_POST['cbo_fil_cgvsubtipo_id']);
	$estadoejec_cgvsubtipo_id = empty($_POST['cbo_fil_estadoejecucion_cgvsubtipo_id']) ? '' : intval($_POST['cbo_fil_estadoejecucion_cgvsubtipo_id']);
	if (!empty($cgvsubtipo_id)) {
		$oEjecucion->cgvsubtipo_id = $cgvsubtipo_id;
	}
	if (!empty($estadoejec_cgvsubtipo_id)) {
		$oEjecucion->estadoejecucion_cgvsubtipo_id = $estadoejec_cgvsubtipo_id;
	}
	if (!empty($encargado_id)) {
		$oEjecucion->usuencargado = $encargado_id;
	}

	$result = $oEjecucion->listar_todos('', '', '', $fec_ini, $fec_fin, $estadoejec_id);
	$data['estado'] = 1;

	if ($result['estado'] == 1) {
		$estado_proceso = ['SUSPENDIDO', 'ACTIVO', 'ANULADO', 'TERMINADO'];
		$i = 0;
		foreach ($result['data'] as $key => $value) {
			$cliente_concat = "{$value['tb_cliente_doc']} - {$value['tb_cliente_nom']}";
			if (intval($value['tb_cliente_tip']) == 2) {
				$cliente_concat .= ", {$value['tb_cliente_empruc']} - {$value['tb_cliente_emprs']}";
			}

			//buscar su ultima ejecucionfase
				$last_fase = $oEjecucion->listar_ejecucionfase_por('', $value['tb_ejecucion_id'], '', 'ef.tb_ejecucionfase_fecreg DESC', 'LIMIT 1')['data'][0];
				$last_fase_id = $last_fase['tb_ejecucionfase_id'];
				$last_fase_fecha = $last_fase['tb_ejecucionfase_fecreg'];

				$proceso_fase = intval($last_fase['tb_estadoejecucion_cgvsubtipo_id']);
				
				$color = "red";
				$dias_dura = 0;
				$fecha_max = $last_fase_fecha;
				$dias_transcurridos = abs(restaFechas(fecha_mysql($last_fase_fecha), $fecha_hoy));
				
				$proceso_fase -= ((intval($cgvsubtipo_id) - 1) * 15);

				if (in_array($proceso_fase, [1, 2, 3, 4, 5, 12])) {
					$dias_dura = 1;
					// Crear un objeto DateTime con la fecha inicial
					$fecha_obj = new DateTime($last_fase_fecha);
					// Sumar un día
					$fecha_obj->modify('+'.$dias_dura.' day');
					// Obtener la nueva fecha en formato "Y-m-d"
					$fecha_max = $fecha_obj->format('Y-m-d');
					
					if ($fecha_hoy == fecha_mysql($last_fase_fecha) || $dias_transcurridos == $dias_dura) {
						$color = "green";
					} elseif ($dias_transcurridos == 2) {
						$color = "yellow";
					}
				}
				if ($proceso_fase == 6) {
					$dias_dura = 3;
					$fecha_max = sumarDiasSemana($last_fase_fecha, $dias_dura);

					if ($fecha_hoy == fecha_mysql($last_fase_fecha) || $dias_transcurridos < 2) {
						$color = "green";
					} elseif ($dias_transcurridos == 2) {
						$color = "yellow";
					} elseif ($dias_transcurridos == 3) {
						$color = "orange";
					}
				}
				if (in_array($proceso_fase, [7, 8, 9]) && $fecha_hoy == fecha_mysql($last_fase_fecha)) {
					$color = "green";
				}
				if ($proceso_fase == 10) {
					$dias_dura = 1;
					$fecha_max = sumarDiasSemana($last_fase_fecha, $dias_dura);
					
					if ($fecha_hoy == fecha_mysql($last_fase_fecha)) {
						$color = "green";
					} elseif ($dias_transcurridos == 1) {
						$color = "yellow";
					}
				}
				if ($proceso_fase == 11 || $proceso_fase == 14) {
					$dias_dura = 15;
					// Crear un objeto DateTime con la fecha inicial
					$fecha_obj = new DateTime($last_fase_fecha);
					// Sumar un día
					$fecha_obj->modify('+'.$dias_dura.' day');
					// Obtener la nueva fecha en formato "Y-m-d"
					$fecha_max = $fecha_obj->format('Y-m-d');
					
					if ($dias_transcurridos < 4) {
						$color = "green";
					} elseif ($dias_transcurridos < 8) {
						$color = "yellow";
					} elseif ($dias_transcurridos < 12) {
						$color = "orange";
					}
				}
				if ($proceso_fase == 13) {
					//consultar si ha presentado escrito de descerraje
					$result1 = $oEjecucionfasefile->listar('', $last_fase_id, $value['tb_ejecucion_id'], '', 2, '', 2, '');

					if ($result1['estado'] == 1) {
						$dias_dura = 15;
						// Crear un objeto DateTime con la fecha inicial
						$fecha_obj = new DateTime($last_fase_fecha);
						// Sumar un día
						$fecha_obj->modify('+'.$dias_dura.' day');
						// Obtener la nueva fecha en formato "Y-m-d"
						$fecha_max = $fecha_obj->format('Y-m-d');
						
						if ($dias_transcurridos < 4) {
							$color = "green";
						} elseif ($dias_transcurridos < 8) {
							$color = "yellow";
						} elseif ($dias_transcurridos < 12) {
							$color = "orange";
						}
					}
					else {
						$dias_dura = 1;
						// Crear un objeto DateTime con la fecha inicial
						$fecha_obj = new DateTime($last_fase_fecha);
						// Sumar un día
						$fecha_obj->modify('+'.$dias_dura.' day');
						// Obtener la nueva fecha en formato "Y-m-d"
						$fecha_max = $fecha_obj->format('Y-m-d');
						
						if ($fecha_hoy == fecha_mysql($last_fase_fecha)) {
							$color = "green";
						} elseif (abs(restaFechas(fecha_mysql($last_fase_fecha), $fecha_hoy)) == $dias_dura) {
							$color = "yellow";
						}
					}
				}
				if ($proceso_fase == 15) {
					$color = "green";
					$tooltip = '<b>Fec. fin proc.:</b> '.mostrar_fecha($last_fase_fecha);
					$dias_atrasados = -1000;
				} else {
					$dias_atrasados = -$dias_dura + $dias_transcurridos;
					$tooltip = '<b>Fase fec. ini:</b> '.mostrar_fecha($last_fase_fecha).'<br>';
					if ($value['tb_ejecucion_estadoproceso'] == 1) {
						$tooltip .= '<b>Fase fec. max:</b> '.mostrar_fecha($fecha_max).'<br><b>Dias atraso:</b> '.$dias_atrasados;
					} else {
						$tooltip .= '<b>El proceso está '.$estado_proceso[$value['tb_ejecucion_estadoproceso']].'</b>';
						$color = "6699FF";
					}
				}
			//
			$data['array_procesos'][$i] = [$cliente_concat, $proceso_fase, $tooltip, $color, $dias_atrasados];
			$i++;
		}
		$data['cantidad'] = $i;
		$result = null;
		$oEjecucion->usuencargado = null;
		$oEjecucion->cgvsubtipo_id = null;
	}
	else {
		$data['array_procesos'][0] = ['No data', 0, '', '', 0];
		$data['cantidad'] = 0;
	}
	$oEjecucion->cgvsubtipo_id = null;
	$oEjecucion->estadoejecucion_cgvsubtipo_id = null;
	$oEjecucion->usuencargado = null;
}

echo json_encode($data);
