<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Legal</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit)); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<label>Filtros</label>
						<div class="panel panel-danger" id="div_filtros_ejecucionresumen">
							<div class="panel-body">
								<?php require_once VISTA_URL . 'ejecucionresumen/ejecucionresumen_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE CARGANDO -->
				<div class="callout callout-info" id="barrasaño_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div id="chart_div_1" style="height: 300px;"></div>
					</div>
				</div>
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once VISTA_URL . 'templates/modal_mensaje.php'; ?>
		</div>
	</section>
	<!-- /.content -->
</div>
