var array_procesos = [];
var arrayfases = [];
var cantidad = 2;
google.charts.load('current', {'packages':['corechart', 'bar']});

$(document).ready(function () {
    $('#click').click();
    //daniel odar 17-10-2024, require_once de usuarios, ocultar los que estén con estado(bloqeo) = 1
        $('#txt_fil_encargado_id option[id*="option_cero"], #txt_fil_encargado_id option[data-estado="1"], #cbo_fil_cgvsubtipo_id option[value=0]').remove();
    //

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });

    $('#txt_fil_estadoejec_id, #txt_fil_encargado_id').change(function () {
        cargar_data(parseInt($('#cbo_fil_orden_atraso').val()));
    });

    $('#cbo_fil_estadoejecucion_cgvsubtipo_id, #cbo_fil_orden_atraso').on('change', function(){
        cargar_data(parseInt($(this).val()));
        //ordenarPorDiasAtrasados(parseInt($(this).val()));
    });

    disabled($('#form_ejecucionresumen_filtro').find('.disabled'));

    $('#cbo_fil_cgvsubtipo_id').on('change', function() {
        if (parseInt($(this).val()) != 0) {
            llenar_cbo_estados($(this).val());
        } else {
            disabled($('#cbo_fil_estadoejecucion_cgvsubtipo_id').html('<option value="0">Todas</option>'));
            $('#div_fil_estadoejecucion_cgvsubtipo_id').css('width', '120px');
        }
        setTimeout(function () {
            cargar_data();
        }, 200);
    });
});

function cargar_data(orden) {
    if (isNaN(orden)) {
        orden = parseInt($('#cbo_fil_orden_atraso').val());
    }
    var extra = `&action_ejecucionresumen=grafico_barras&cgarvtipo_id=${$('#cbo_fil_cgvsubtipo_id option:selected').attr('data-cgarvtipo')}`;

    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucionresumen/ejecucionresumen_controller.php",
        async: true,
        dataType: "json",
        data: $('#form_ejecucionresumen_filtro').serialize() + extra,
        success: function (data) {
            console.log(data);

            var arraydatos = [];
            if (parseInt(data.estado) == 1) {
                if (parseInt(data.cantidad) > 2) {
                    cantidad = parseInt(data.cantidad);
                }
                for (let i = 0; i < data.array_procesos.length; i++) {
                    // Acceder a cada elemento de resultados y operaciones, según el primer ciclo
                    let cliente = data.array_procesos[i][0];
                    let fase = parseInt(data.array_procesos[i][1]);
                    let tooltip = data.array_procesos[i][2];
                    let color = data.array_procesos[i][3];
                    let dias_atraso = data.array_procesos[i][4];
                    arraydatos.push([cliente, fase, tooltip, color, dias_atraso]);
                }
            } else {
                arraydatos.push([data.array_procesos[0][0],
                    parseInt(data.array_procesos[0][1]),
                    data.array_procesos[0][2],
                    data.array_procesos[0][3],
                    data.array_procesos[0][4]]
                );
            }
            arraydatos.unshift(['Cliente', 'Fase alcanzada', { role: 'tooltip', p: { html: true } }, { role: 'style' }, 'Días atrasados']);
            array_procesos = arraydatos;
            arrayfases = data.array_fases;
            //google.charts.setOnLoadCallback(drawStuffaño);
            $('#chart_div_1').css('height', ((cantidad+1)*100)+'px');
            orden == 0 ? drawStuffaño('chart_div_1') : ordenarPorDiasAtrasados(orden);
        }
    });
}

function drawStuffaño(id_div) {
    var chartDiv = document.getElementById(id_div);
    var cgv_subtipo = $('#cbo_fil_cgvsubtipo_id option:selected').text();

    // Convertimos los datos del array a DataTable
    var dataSinDiasAtrasados = array_procesos.map(row => row.slice(0, 4));
    var data = google.visualization.arrayToDataTable(dataSinDiasAtrasados);

    var classicOptions = {
        title: 'AVANCE DE PROCESOS EJECUCION - '+cgv_subtipo,
        bars: 'horizontal',
        hAxis: {
            title: 'Fases',
            ticks: arrayfases.map(function (fase, i) { return { v: i + 1, f: fase }; }),
            viewWindow: { min: 0.5 },
            minValue: 0.5
        },
        vAxis: { title: 'Clientes' },
        bar: { groupWidth: '40%' },
        annotations: { alwaysOutside: false },
        legend: { position: 'none' },
        tooltip: { isHtml: true }
    };

    function drawClassicChart() {
        var classicChart = new google.visualization.BarChart(chartDiv);
        classicChart.draw(data, classicOptions);
    }

    drawClassicChart();
}

// Función para ordenar por "días atrasados" y redibujar el gráfico
function ordenarPorDiasAtrasados(orden) {
    if (orden == 2) {
        array_procesos.sort(function(a, b) {
            return b[4] - a[4];
        });
    } else if (orden == 1) {
        array_procesos.sort(function(a, b) {
            return a[4] - b[4];
        });
    } else if (orden == 0) {
        cargar_data();
        return;
    }
    drawStuffaño('chart_div_1');
}

function llenar_cbo_estados(cgvsubtipo_id) {
    if (parseInt(cgvsubtipo_id) > 0) {
        $.ajax({
            type: 'POST',
            url: VISTA_URL + 'ejecucion/estadoejecucion_cgvsubtipo_select.php',
            async: true,
            dataType: 'html',
            data: ({
                cgvsubtipo_id: cgvsubtipo_id,
            }),
            success: function (data) {
                $('#cbo_fil_estadoejecucion_cgvsubtipo_id').html(data);
                $('#div_fil_estadoejecucion_cgvsubtipo_id').css('width', '335px');
            }
        });
        $('#cbo_fil_estadoejecucion_cgvsubtipo_id').removeAttr('disabled');
    }
}