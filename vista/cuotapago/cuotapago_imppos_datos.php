<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../cuota/Cuota.class.php');
  require_once('../cuota/Cuotadetalle.class.php');
  require_once('../cuotapago/Cuotapago.class.php');
  require_once('../ingreso/Ingreso.class.php');
  require_once('../clientecaja/Clientecaja.class.php');
  require_once('../empresa/Empresa.class.php');
  require_once('../impresora/Impresora.class.php');

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $oCuota = new Cuota();
  $oCuotadetalle = new Cuotadetalle();
  $oIngreso = new Ingreso();
  $oCuotapago = new Cuotapago();
  $oClientecaja = new Clientecaja();
  $oEmpresa = new Empresa();
  $oImpresora = new Impresora();

	$cuotapago_id = intval($_POST['cuotapago_id']);

	$result = $oEmpresa->mostrarUno($_SESSION['empresa_id']);
		if($result['estado'] == 1){
			$empresa_ruc = $result['data']['tb_empresa_ruc'];
			$empresa_nomcom = $result['data']['tb_empresa_nomcom'];
			$empresa_razsoc = $result['data']['tb_empresa_razsoc'];
			$empresa_dir = $result['data']['tb_empresa_dir'];
			$empresa_dir2 = $result['data']['tb_empresa_dir2'];
			$empresa_tel = $result['data']['tb_empresa_tel'];
			$empresa_ema = $result['data']['tb_empresa_ema'];
			$empresa_fir = $result['data']['tb_empresa_fir'];	
		}
	$result = NULL;

	$result = $oCuotapago->mostrarUno($cuotapago_id);
		if($result['estado'] == 1){
			$cuotapago_reg = mostrar_fecha_hora($result['data']['tb_cuotapago_reg']);
			$usuario_usureg = $result['data']['tb_cuotapago_usureg'];
			$usuario_nom = $result['data']['tb_usuario_nom'];
			
			$modulo_id = $result['data']['tb_modulo_id']; // 1 cuota, 2 cuotadetalle
			$cuotapago_modid = $result['data']['tb_cuotapago_modid']; // id de la cuota o cuotadetalle

			$cuotapago_fec = mostrar_fecha($result['data']['tb_cuotapago_fec']);
	 
			$cuotapago_mon = $result['data']['tb_cuotapago_mon'];
			$cuotapago_tipcam	= $result['data']['tb_cuotapago_tipcam'];
			$cuotapago_moncam = $result['data']['tb_cuotapago_moncam'];
			
			$moneda_id = $result['data']['tb_moneda_id'];
			$moneda_nom = $result['data']['tb_moneda_nom'];
			
			$cuotapago_est = $result['data']['tb_cuotapago_est'];
		}
	$result = NULL;

	//si el modulo_id =1 -> cuota
	if(intval($modulo_id) == 1){
		$result = $oCuota->mostrarUno($cuotapago_modid);
			if($result['estado'] == 1){
				$cuota_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
				$cuota_id = $result['data']['tb_cuota_id'];
				$creditotipo_id = $result['data']['tb_creditotipo_id']; //1 creditomenor, 2 ASIVEH, 3 GARVEH, 4 HIPOTECARIO
				$credito_id = $result['data']['tb_credito_id'];
				$cuota_num = $result['data']['tb_cuota_num'];
				$cuota_cuo = $result['data']['tb_cuota_cuo'];
				$cuota_persubcuo = $result['data']['tb_cuota_persubcuo'];
			}
		$result = NULL;
	}
	//si el modulo_id =2 -> cuotadetalle
	if(intval($modulo_id) == 2){
		$result = $oCuotadetalle->mostrarUno($cuotapago_modid);
			if($result['estado'] == 1){
				$cuota_fec = mostrar_fecha($result['data']['tb_cuotadetalle_fec']);
				$cuota_id = $result['data']['tb_cuota_id'];
				$cuota_num = $result['data']['tb_cuotadetalle_num'];
				$cuota_cuo = $result['data']['tb_cuotadetalle_cuo'];
		    $cuotadetalle_acupag = $result['data']['tb_cuotadetalle_acupag'];//si es 1-> es una cuota acuerdo de pago
		   }
		$result = NULL;

		$result = $oCuota->mostrarUno($cuota_id);
			if($result['estado'] == 1){
				$creditotipo_id = $result['data']['tb_creditotipo_id']; //1 creditomenor, 2 ASIVEH, 3 GARVEH, 4 HIPOTECARIO
				$credito_id = $result['data']['tb_credito_id'];
				$cuota_num = $result['data']['tb_cuota_num'];
				$cuota_cuo = $result['data']['tb_cuota_cuo'];
				$cuota_persubcuo = $result['data']['tb_cuota_persubcuo'];
			}
		$result = NULL;

	  //obtenemos información del acuerdo de pago si es que es una cuota de acuerdopago
	  $observacion_acupag = '';
	  if($cuotadetalle_acupag == 1){
	    $result = $oAcuerdopago->informacion_acuerdopago_por_cuotadetalleid($modid);
	      $instancia = $result['data']['tb_acuerdopago_ins'];//numero de instancia del acuerdopago
	    $result = NULL;

	    $observacion_acupag = ' AP Instancia 0'.$instancia;
	  }
	}

	$ingreso_imp = 0;
	//parametro 30 para modulo id, 30 = cuotapago
	$result = $oIngreso->mostrar_ingresos_modulo(30, $cuotapago_id);
		if($result['estado'] == 1){
			foreach ($result['data'] as $key => $value) {
				$ingreso_detex = $value['tb_ingreso_detex'];
				$ingreso_det = $value['tb_ingreso_det'];
				if($cuotapago_mon <= 0)
					$ingreso_imp = $value['tb_ingreso_imp'];
				break;
			}
		}
	$result = NULL;

	$clientecaja = 0;

	if($creditotipo_id == 1){
		require_once("../creditomenor/Creditomenor.class.php");
		$oCredito = new Creditomenor();

		$result = $oCredito->mostrarUno($credito_id);
			if($result['estado'] == 1){
				$credito_numcuomax = $result['data']['tb_credito_numcuomax'];

				$cliente_tip = $result['data']['tb_cliente_tip'];
                                $cliente_id = $result['data']['tb_cliente_id'];
                                if($cliente_tip==1){
                                    $cliente_doc = $result['data']['tb_cliente_doc'];
                                    $cliente_nom = $result['data']['tb_cliente_nom'];
                                    $cliente_dir = $result['data']['tb_cliente_dir'];
                                }
                                if($cliente_tip==2){
                                    $cliente_doc = $result['data']['tb_cliente_empruc'];
                                    $cliente_nom = $result['data']['tb_cliente_emprs'];
                                    $cliente_dir = $result['data']['tb_cliente_empdir'];
                                }

				$cliente = $cliente_nom.' | '.$cliente_doc;
			}
		$result = NULL;

		$empresa_razsoc = "INVERSIONES Y PRESTAMOS DEL NORTE SAC";
		$empresa_ruc = "20600575679";
		//para credito menor el detalle será el mismo detalle de ingreso
		$detalle = $ingreso_det;
	}

	//impresora, muestra los datos de la impresora por defecto 1
	$result = $oImpresora->mostrarUno(1);
		if($result['estado'] == 1){
			$impresora_nom = $result['data']['tb_impresora_nom'];
			$impresora_nomloc = $result['data']['tb_impresora_nomloc'];
			$impresora_ser = $result['data']['tb_impresora_ser'];
			$impresora_url = $result['data']['tb_impresora_url'];
			$impresora_ip = $result['data']['tb_impresora_ip'];
		}
	$result = NULL;

	$data['imp_nom'] = $impresora_nom;
	$data['imp_nomloc'] = $impresora_nomloc;
	$data['imp_ser'] = $impresora_ser;
	$data['imp_url'] = $impresora_url;
	$data['imp_ip']	 = $impresora_ip;


	$data['emp_razsoc'] = $empresa_razsoc;
	$data['emp_ruc']	= $empresa_ruc;
	$data['emp_dir1']	= $empresa_dir;
	$data['emp_dir2']	= $empresa_dir2;

	$data['cretip_id']	= $creditotipo_id;

	$data['cuota_mon'] = $cuota_cuo;
	$data['ingreso_monto'] = $ingreso_imp; // cuando el valor de anular es = 0, consulta a Ingreso
	$data['cuota_ven']  = $cuota_fec;

	$data['fecha'] = $cuotapago_fec;
	$data['fechareg'] = $cuotapago_reg;

	$data['operacion_id'] = $cuotapago_id;

	$data['cliente'] = $cliente_nom;
	$data['cliente_doc'] = $cliente_doc;
	$data['cliente_dir']	= $cliente_dir;

	$data['detalle'] = $detalle;
	$data['det_ext'] = $ingreso_detex;


	$data['pago_monto'] = $cuotapago_mon;
	$data['tipo_cambio'] = $cuotapago_tipcam;
	$data['pago_cambio'] = $cuotapago_moncam;

	$data['mon_id'] = $moneda_id;
	$data['moneda'] = $moneda_nom;

	$data['cajero']	= $usuario_nom;
	$data['cliente_caj'] = mostrar_moneda($clientecaja);

	echo json_encode($data);

	function sanear_string($string){
	 
	    $string = trim($string);
	 
	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç'),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );
	  
	    return $string;
	}

?>