<?php
  session_name("ipdnsac");
  session_start();
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 admin, 3 ventas

  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'cuota/Cuota.class.php');
    require_once(VISTA_URL.'cuotapago/Cuotapago.class.php');
    require_once(VISTA_URL.'ingreso/Ingreso.class.php');
    require_once(VISTA_URL.'clientecaja/Clientecaja.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../cuota/Cuota.class.php');
    require_once('../cuotapago/Cuotapago.class.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../clientecaja/Clientecaja.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }
  $oCuota = new Cuota();
  $oIngreso = new Ingreso();
  $oCuotapago = new Cuotapago();
  $oClientecaja = new Clientecaja();

  $credito_id = (isset($_POST['credito_id']))? intval($_POST['credito_id']) : $credito_id;
  $creditotipo_id = (isset($_POST['creditotipo_id']))? intval($_POST['creditotipo_id']) : $creditotipo_id;
  $credito_tabla = '';

  if($creditotipo_id == 1) $credito_tabla = 'tb_creditomenor';
  if($creditotipo_id == 2) $credito_tabla = 'tb_creditoasiveh';
  if($creditotipo_id == 3) $credito_tabla = 'tb_creditogarveh';
  if($creditotipo_id == 4) $credito_tabla = 'tb_creditohipo';

  function estado_cuota($cuota_est){
    $estado = '<span class="badge bg-orange">POR COBRAR</span>';
    if($cuota_est == 2)
      $estado = '<span class="badge bg-green">CANCELADO</span>';
    if($cuota_est == 3)
      $estado = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
    return $estado;
  }

  function estado_mora($mora_est, $moneda, $monto_mora){
    $estado = $moneda.' '.mostrar_moneda($monto_mora);
    if($mora_est == 2)
      $estado = '<span class="badge bg-green">Pagada: '.$moneda.' '.mostrar_moneda($monto_mora).'</span>';
    if($mora_est == 3)
      $estado = '<span class="badge bg-aqua">Parcial: '.$moneda.' '.mostrar_moneda($monto_mora).'</span>';
    return $estado;
  }
?>
<table id="tbl_cuota" class="table table-bordered">
  <thead>
    <tr>
      <th>N°</th>
      <th>Fecha</th>
      <th>Cuota</th>
      <th>Estado</th>
      <th>Mora</th>
      <th>Detalle</th>
      <th>Pagos</th>
      <th>Saldo</th>
      <th>Deuda</th>
      <th>Cliente Caja</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $deuda_total = 0; $clientecaja_cuota = 0;
    $result = $oCuota->cuotas_credito($credito_id, $creditotipo_id, $credito_tabla);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];?></td>
            <td><?php echo mostrar_fecha($value['tb_cuota_fec']);?></td>
            <td><?php echo $value['tb_moneda_nom'].' '.mostrar_moneda($value['tb_cuota_cuo']);?></td>
            <td><?php echo estado_cuota($value['tb_cuota_est']);?></td>
            <td>
              <?php 
                echo estado_mora($value['tb_cuota_morest'], $value['tb_moneda_nom'], $value['tb_cuota_mor']); 
              ?>
            </td>
            <td>
              <?php
                $pagos_cuota = 0; $saldo_cuota = 0;

                $modulo_id = 1; $cuotapago_modid = $value['tb_cuota_id'];
                $result2 = $oCuotapago->mostrar_cuotapagos_modulo($modulo_id, $cuotapago_modid);
                  if($result2['estado'] == 1){
                    $numero_pago = 1;
                    foreach ($result2['data'] as $key => $value2): ?>
                      <table class="table" style="border: 2px solid #333; margin-bottom: 0px;">
                        <tr>
                          <th width="40%">
                            <?php 
                              echo '<a href="javascript:void(0)" onclick="cuotapago_imppos_datos3('.$value2['tb_cuotapago_id'].')">Pago '.$numero_pago.'</a>';
                              /* GERSON (12-01-25) */
                              if($_SESSION['usuario_id']==2 || $_SESSION['usuario_id']==11 || $_SESSION['usuario_id']==61){ // Usuarios
                                echo '<a href="javascript:void(0)" onclick="cuotapago_anular('.$value2['tb_cuotapago_id'].')" style="margin-left: 12%;">Anular</a>';
                              }else{
                                echo '<a href="javascript:void(0)" onclick="solicitar_anular('.$value2['tb_cuotapago_id'].','.$credito_id.','.$creditotipo_id.')" style="margin-left: 12%;">Solic. Anular</a>';
                              }
                              /*  */
                            ?>
                          </th>
                          <th width="30%"><?php
                            echo '<a href="javascript:void(0)" onclick="cuotapago_detalle('.$value2['tb_cuotapago_id'].')">Ingreso</a>';?>
                          </th>
                          <th width="30%">Caj Cli</th>
                        </tr>
                        <tr>
                          <td><?php echo mostrar_fecha($value2['tb_cuotapago_fec']).' | '.$value2['tb_moneda_nom'].' '.mostrar_moneda($value2['tb_cuotapago_mon']);?></td>
                          <td>
                            <?php
                              $modulo_id = 30; $ingreso_modide = $value2['tb_cuotapago_id'];
                              $result3 = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
                               if($result3['estado'] == 1){
                                foreach ($result3['data'] as $key => $value3){
                                  echo '<dd>'.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_ingreso_imp']).'</dd>';
                                  $pagos_cuota += formato_numero($value3['tb_ingreso_imp']);
                                }
                               }
                               else
                                echo mostrar_moneda(0);
                              $result3 = NULL;
                            ?>
                          </td>
                          <td>
                            <?php
                              $modulo_id = 30; $clientecaja_modid = $value2['tb_cuotapago_id'];
                              $result3 = $oClientecaja->mostrar_clientecaja_modulo($modulo_id, $clientecaja_modid);
                               if($result3['estado'] == 1){
                                foreach ($result3['data'] as $key => $value3){
                                  if(intval($value3['tb_clientecaja_tip']) == 1){
                                    echo '<dd>+ '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_clientecaja_mon']).'</dd>';
                                    $clientecaja_mas += formato_numero($value3['tb_clientecaja_mon']);
                                  }
                                  if(intval($value3['tb_clientecaja_tip']) == 2){
                                    echo '<dd>- '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_clientecaja_mon']).'</dd>';
                                    $clientecaja_menos += formato_numero($value3['tb_clientecaja_mon']);
                                  }
                                }
                               }
                               else
                                echo $value2['tb_moneda_nom'].' '.mostrar_moneda(0);
                              $result3 = NULL;
                            ?>
                          </td>
                        </tr>
                      </table><?php
                      $numero_pago ++;
                    endforeach;
                  }
                $result2 = NULL;
                $clientecaja_cuota = $clientecaja_mas - $clientecaja_menos;
                $saldo_cuota = formato_numero($value['tb_cuota_cuo']) - formato_numero($pagos_cuota);
                $deuda_total += formato_numero($saldo_cuota);
              ?>
            </td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($pagos_cuota); ?></td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($saldo_cuota); ?></td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($deuda_total); ?></td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($clientecaja_cuota); ?></td>                   
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>