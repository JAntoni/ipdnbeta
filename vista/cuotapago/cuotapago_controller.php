<?php
  require_once('../../core/usuario_sesion.php');
	require_once('../cuotapago/Cuotapago.class.php');
	require_once('../egreso/Egreso.class.php');
	require_once('../ingreso/Ingreso.class.php');
	require_once('../clientecaja/Clientecaja.class.php');
	require_once('../cuota/Cuota.class.php');
	require_once('../cuota/Cuotadetalle.class.php');
	require_once('../cajacambio/Cajacambio.class.php');
	require_once('../creditomenor/Creditomenor.class.php');
	require_once('../creditolinea/Creditolinea.class.php');
	require_once('../permisoanulacion/Permisoanulacion.class.php');

	$oCuotapago = new Cuotapago();
	$oEgreso = new Egreso();
	$oIngreso = new Ingreso();
	$oClientecaja = new Clientecaja();
	$oCuota = new Cuota();
	$oCuotadetalle = new Cuotadetalle();
	$oCajacambio = new Cajacambio();
  $oCredito = new Creditomenor();
	$oCreditolinea = new Creditolinea();
	$oPermisoanulacion = new Permisoanulacion();

	require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

	$action = $_POST['action'];
 	$cuotapago_id = intval($_POST['cuotapago_id']);
 	$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

 	if($action == 'anular' && $cuotapago_id > 0){
		if($usuariogrupo_id != 2){
			$data['estado'] = 0;
			$data['mensaje'] = 'Solo los usuarios administradores pueden anular los pagos, comunícate con ellos por favor.';
			echo json_encode($data);
			exit();
		}

		$fecha_hoy = date('d-m-Y');
		$result = $oCuotapago->mostrarUno($cuotapago_id);
	  	if($result['estado'] == 1){
				$cuota_tipo = $result['data']['tb_modulo_id']; // 1 cuota, 2 cuotadetalle
				$cuotapago_modid = $result['data']['tb_cuotapago_modid']; //id de cuota o cuotadetalle
				$cuotapago_fec = mostrar_fecha($result['data']['tb_cuotapago_fec']);
	  	}
    $result = NULL;

		if($fecha_hoy != $cuotapago_fec){
			$data['estado'] = 0;
			$data['mensaje'] = 'No se puede anular pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: '.$fecha_hoy.', fecha anular: '.$cuotapago_fec;
			echo json_encode($data);
			exit();
		}

		$detalle_ingreso = '';
	  //1. egresos de CAJA AP, si es que se pagó con monto de acuerdo de pago, anulamos los egresos de la caja ap
  	$oEgreso->anular_egreso_acuerdopago(0, $cuotapago_id); //la columna tb_egreso_xac cambia a 0: anulado

  	//2. Anulamos todos los ingresos que pertenezcan al mismo cuotapago_id
  	$ingreso_valor = 0; $modulo_id = 30; $ingreso_modide = $cuotapago_id; // tb_ingreso_xac = 0, modulo 30 de cuotapago

		$result = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
			if($result['estado'] == 1){
				foreach ($result['data'] as $key => $value) {
					if($detalle_ingreso == '')
						$detalle_ingreso = '<b>INGRESO ID: </b>'.$value['tb_ingreso_id'].' / '.$value['tb_ingreso_det'];
					else
						$detalle_ingreso .= ' / <b>INGRESO ID: </b>'.$value['tb_ingreso_id'].' / '.$value['tb_ingreso_det'];
				}
			}
		$result = NULL;

		$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $modulo_id, $ingreso_modide);

  	//3. Cuando el pago es mediante BANCO, se genera un egreso para cuadrar la caja, también se debe anularel egreso tiene como modulo_id = 30 (cuotapago), egreso_modid = cuotapago_id
  	$egreso_valor = 0; $modulo_id = 30; $egreso_modide = $cuotapago_id;
  	$oEgreso->anular_egresos_por_modulo($egreso_valor, $modulo_id, $egreso_modide);

  	//4. Anulamos las cajacliente, en caso se utilizó o en caso se aumentó el monto
  	$modulo_id = 30; $clientecaja_modid = $cuotapago_id;
  	$result = $oClientecaja->mostrar_clientecaja_modulo($modulo_id, $clientecaja_modid);
			if($result['estado'] == 1){
				foreach ($result['data'] as $key => $value) {
					$clientecaja_id = intval($value['tb_clientecaja_id']);
					$oClientecaja->modificar_campo($clientecaja_id, 'tb_clientecaja_xac', 0, 'INT');

					$ingreso_valor = 0; $modulo_id = 40; $ingreso_modide = $clientecaja_id; //40 es el modulo de clientecaja en ingreso
					$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $modulo_id, $ingreso_modide);

					$egreso_valor = 0; $modulo_id = 40; $egreso_modide = $clientecaja_id; //40 es el modulo de clientecaja en egreso
					$oEgreso->anular_egresos_por_modulo($egreso_valor, $modulo_id, $egreso_modide);
				}
			}
  	$result = NULL;

  	$cuota_id = 0; //para poder obtener información del crédito

  	//5. Si el cuotapago pertenece a una cuota
  	if($cuota_tipo == 1){
  		$cuota_est = 1; //1 por cobrar, 2 pagado, 3 pago parcial
  		$modulo_id = $cuota_tipo; // 1 tb_cuota

  		$result = $oCuotapago->mostrar_cuotapago_ingresos_por_tipo_cuota($modulo_id, $cuotapago_modid);
  			if($result['estado'] == 1){
  				//aun sigue teniendo pagos esta cuota
  				$cuota_est = 3; //pago parcial
  			}
  		$result = NULL;

  		$cuota_id = $cuotapago_modid;
  		$oCuota->modificar_campo($cuota_id, 'tb_cuota_est', $cuota_est, 'INT');
                
			$resultado = $oCuota->mostrarUno_CreditoMenor($cuota_id);
				if($resultado['estado']==1){
					$tb_cuota_cap = $resultado['data']['tb_cuota_cap'];
					$tb_credito_id = $resultado['data']['tb_credito_id'];
					$tb_cuota_num = $resultado['data']['tb_cuota_num'];
					$tb_cuotatipo_id = $resultado['data']['tb_cuotatipo_id'];
					$tb_cuota_cuo = $resultado['data']['tb_cuota_cuo'];
				}
			$resultado = NULL;
			
			$resultado = $oCredito->mostrarUno($tb_credito_id);
				if($resultado['estado']==1){
					$tb_credito_int = $resultado['data']['tb_credito_int'];
				}
			$resultado = NULL;
      //echo $tb_cuotatipo_id.' / '.$tb_credito_id.' / '.$tb_cuota_num.' / '.$tb_cuota_cuo, exit(); 
  		/* modificar cuota*/
			$oCuota->modificar_campo($cuota_id, 'tb_cuota_est', $cuota_est, 'INT');
			$creditotipo_id = 1; //1 pertenece a credito menor, 2 asiveh, 3 garveh, 4 hipo
      if($tb_cuotatipo_id == 1){
				//si es cuota Libre
				/* modificar cuota estado*/
				$oCuota->modificar_Estado_Anulacion($tb_credito_id,$tb_cuota_num);
				/* modificar cuota estado*/
				$oCuota->ReestablecerInteres($tb_credito_int, $cuota_id);
      }
      if($tb_cuotatipo_id == 2){//si es cuota Fija
        $oCuota->modificar_Estado_Anulacion_fijas($creditotipo_id, $tb_credito_id,$tb_cuota_num,$tb_cuota_cuo,2);
				$oCuota->modificar_Estado_Anulacion_fijas($creditotipo_id, $tb_credito_id,$tb_cuota_num,$tb_cuota_cuo,1);

				//$oCuota->ReestablecerInteresFija($tb_credito_int, $cuota_id);
				$n = $oCuota->NLetrasNuevaCuota($tb_credito_id);
				$n_valor = count($n['data']);
				$oCredito->ActualizarNcuotas($tb_credito_id,$n_valor);
      }
	  /* GERSON (31-05-23) */
	  $oCuota->modificar_estado_anulacion_cobranza($tb_credito_id,$cuota_id);
	  /*  */
	  /* GERSON (12-01-25) */
	  $permisoanulacion = $oPermisoanulacion->mostrarPermisoanulacion($tb_credito_id,$cuotapago_id);
	  if($permisoanulacion['estado'] == 1){
	  	$oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_est', 2, 'INT');
	  	$oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_fec_aten', date('Y-m-d h:i:s'), 'STR');
	  	$oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_user_aten', $_SESSION['usuario_id'], 'INT');
	  }
	  /*  */
  	}

  	//6. Si el cuotapago pertenece a una cuotadetalle
  	if($cuota_tipo == 2){
  		$cuotadetalle_est = 1; //1 por cobrar, 2 pagado, 3 pago parcial
  		$modulo_id = $cuota_tipo; // 2 tb_cuotadetalle

  		$result = $oCuotapago->mostrar_cuotapago_ingresos_por_tipo_cuota($modulo_id, $cuotapago_modid);
  			if($result['estado'] == 1){
  				//aun sigue teniendo pagos esta cuotadetalle
  				$cuotadetalle_est = 3; //pago parcial
  			}
  		$result = NULL;

  		$cuotadetalle_id = $cuotapago_modid;
  		$oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_est', $cuotadetalle_est, 'INT');

  		$result = $oCuotadetalle->mostrarUno($cuotadetalle_id);
  			if($result['estado'] == 1){
  				$cuota_id = $result['data']['tb_cuota_id'];
  			}
  		$result = NULL;

  		//listamos todos los cuotadetalle que tiene estado 2 o 3
  		$cuota_est = 1;
  		$result = $oCuotadetalle->mostrar_cuotasdetalle_condicion_estado_por_cuota('2,3', $cuota_id);
  			if($result['estado'] == 1) 
  				$cuota_est = 3;
  		$result = NULL;

  		$oCuota->modificar_campo($cuota_id, 'tb_cuota_est', $cuota_est, 'INT');

  	}

  	//7. si el pago se hizo con diferente moneda, tenemos que anular las cajas cambio
  	$modulo_id = 30; $cajacambio_modid = $cuotapago_id;
  	$result = $oCajacambio->mostrar_cajacambio_modulo($modulo_id, $cajacambio_modid);
  		if($result['estado'] == 1){
  			foreach ($$result['data'] as $key => $value) {
  				$cajacambio_id = intval($value['tb_cajacambio_id']);
  				$oCajacambio->modificar_campo($cajacambio_id, 'tb_cajacambio_xac', 0, 'INT');

  				$ingreso_valor = 0; $modulo_id = 60; $ingreso_modide = $cajacambio_id; //60 es el modulo de cajacambio en ingreso
  				$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $modulo_id, $ingreso_modide);

  				$egreso_valor = 0; $modulo_id = 60; $egreso_modide = $cajacambio_id; //60 es el modulo de cajacambio en egreso
  				$oEgreso->anular_egresos_por_modulo($egreso_valor, $modulo_id, $egreso_modide);
  			}
  		}
  	$result = NULL;

  	//8. Si el credito cambió a liquidado, debe pasar a vigente
  	$result = $oCuota->mostrarUno($cuota_id);
  		if($result['estado'] == 1){
  			$creditotipo_id = intval($result['data']['tb_creditotipo_id']);
  			$credito_id = intval($result['data']['tb_credito_id']);
  		}
  	$result = NULL;

  	if($creditotipo_id == 1){
			require_once ("../creditomenor/Creditomenor.class.php");
			$oCredito = new Creditomenor();
		}
		if($creditotipo_id == 2){
			require_once('../creditoasiveh/Creditoasiveh.class.php');
			$oCredito = new Creditoasiveh();
		}
		if($creditotipo_id == 3){
			require_once('../creditogarveh/Creditogarveh.class.php');
			$oCredito = new Creditogarveh();
		}
		if($creditotipo_id == 4){
			require_once('../creditohipo/Creditohipo.class.php');
			$oCredito = new Creditohipo();
		}

		$result = $oCredito->mostrarUno($credito_id);
			if($result['estado'] == 1){
  			$credito_est = intval($result['data']['tb_credito_est']);
  		}
		$result = NULL;
		if($credito_est == 7||$credito_est == 4)
			$oCredito->modificar_campo($credito_id, 'tb_credito_est', 3, 'INT');
		/*	
		$his = '<span style="color: red;">El crédito estuvo en estado LIQUIDADO pero al anular un pago pasa a VIGENTE otra vez. Pago anulado por: <b>'.$_SESSION['usuario_nombre'].'</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    $oCredito->registro_historial($cre_id, $his);*/

  	//9. Finalmente anulamos el cuotapago
  	$oCuotapago->modificar_campo($cuotapago_id, 'tb_cuotapago_xac', 0, 'INT');
		
		//? 10. GUARDAMOS UN HISTORIAL DEL PAGO ANULADO PARA PODER TENER CONTROL DE QUÉ USUARIO REALIZA ESTAS ACCIÓNES
		$creditolinea_det = 'Ha eliminado el pago: '.$detalle_ingreso;
		$oCreditolinea->insertar($creditotipo_id, $credito_id, intval($_SESSION['usuario_id']), $creditolinea_det);

  	$data['estado'] = 1;
	  $data['mensaje'] = 'Cuotapago eliminado correctamente';
	  echo json_encode($data);
 	}
 	
  else{
		$data['estado'] = 0;
		$data['mensaje'] = 'No se reconoce el Action o ID del registro: action: '.$action.', ID: '.$cuotapago_id;
		echo json_encode($data);
		exit();
 	}
?>