<?php
require '../../libreriasphp/escpos/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

try 
{
	// if($_POST['mon_id']=='1'){
	// 	$mon = 'S/. ';
	// 	$montext = " SOLES";
	// }else{
	// 	$mon = 'US$ ';
	// 	$montext = " DOLARES";
	// }
	
	$con="smb://".$_POST['imp_ip']."/".$_POST['imp_nomloc'];
	//$con="smb://localhost/impresora1";
	$connector = new WindowsPrintConnector($con);

	$printer = new Printer($connector);
	//$printer -> pulse();
	//$printer -> feedReverse(1);
	
	$printer -> feed();
	$printer -> feed();

	$printer -> setFont(Printer::MODE_FONT_A);
	$printer -> setJustification(Printer::JUSTIFY_CENTER);

	$printer -> setEmphasis(true);
	$printer -> text($_POST['emp_razsoc']."\n");
	$printer -> setEmphasis(false);

	$printer -> text($_POST['emp_dir1']."\n");
	$printer -> text($_POST['emp_dir2']."\n");
	$printer -> text("FECHA : ".$_POST['fechareg']."\n");

	$printer -> text("------------------------------------------\n");
	$printer -> text("N° DE OPERACION: ".$_POST['operacion_id']."\n");
	$printer -> text("------------------------------------------\n");

	$printer -> feed();

	$printer -> setJustification();
	$printer -> text("CLIENTE   : ".$_POST['cliente']."\n");
	$printer -> text("RUC/DNI   : ".$_POST['cliente_doc']."\n");
	$printer -> text("DIRECCION : ".$_POST['cliente_dir']."\n");
	$printer -> feed();

	$printer -> text("CUOTA       : ".$_POST['moneda']."  ".$_POST['cuota_mon']."\n");
	$printer -> text("VENCIMIENTO : ".$_POST['cuota_ven']."\n");

	$printer -> text("------------------------------------------\n");
	$printer -> text("CONCEPTO  : ".$_POST['detalle']."\n");
	
	if(trim($_POST['det_ext']) != ''){
		$array = explode('||', trim($_POST['det_ext']));

		for ($i=0; $i <count($array) ; $i++) { 
			$printer -> text($array[$i]."\n");
		}
	}
	
	$printer -> feed();
	$printer -> setEmphasis(true);

	if($_POST['pago_monto']>0)
	{
		$printer -> text("MONTO     : ".$_POST['moneda']." ".$_POST['pago_monto']."\n");
	}
	
	if($_POST['pago_cambio']>0)
	{
		$printer -> text("TIP CAMB. : ".$_POST['tipo_cambio']."\n");
		$printer -> text("CAMBIO    : ".$_POST['pago_cambio']."\n");
	}

	if($_POST['ingreso_monto']>0)
	{
		$printer -> text("PAGO C/CAJA CLIENTE : ".$_POST['moneda']."  ".$_POST['ingreso_monto']."\n");
	}
	
	if($_POST['cliente_caj']>0)
	{
		$printer -> feed();
		$printer -> text("SALDO CAJA CLIENTE  : ".$_POST['moneda']."  ".$_POST['cliente_caj']."\n");
	}

	$printer -> feed();

	$printer -> setEmphasis(false);
	$printer -> text("------------------------------------------\n");
	$printer -> text("REGISTRADO : ".$_POST['cajero']."\n");


	$printer -> setJustification(Printer::JUSTIFY_CENTER);
	// $printer -> text("------------------------------------------\n");
	// $printer -> text("SON: ".$_POST['monto_letras'].$montext."\n");
	// $printer -> text("------------------------------------------\n");

	$printer -> feed();
	$printer -> setFont(Printer::MODE_FONT_B);
	$printer -> text("www.prestamosdelnortechiclayo.com\n");
	$printer -> feed();
	$printer -> feed();

	$printer -> cut();

	//hoja 2
	$printer -> feed();
	$printer -> feed();

	$printer -> setFont(Printer::MODE_FONT_A);
	$printer -> setJustification(Printer::JUSTIFY_CENTER);

	$printer -> setEmphasis(true);
	$printer -> text($_POST['emp_razsoc']."\n");
	$printer -> setEmphasis(false);

	$printer -> text($_POST['emp_dir1']."\n");
	$printer -> text($_POST['emp_dir2']."\n");
	$printer -> text("FECHA : ".$_POST['fechareg']."\n");

	$printer -> text("------------------------------------------\n");
	$printer -> text("N° DE OPERACION: ".$_POST['operacion_id']."\n");
	$printer -> text("------------------------------------------\n");

	$printer -> feed();

	$printer -> setJustification();
	$printer -> text("CLIENTE   : ".$_POST['cliente']."\n");
	$printer -> text("RUC/DNI   : ".$_POST['cliente_doc']."\n");
	$printer -> text("DIRECCION : ".$_POST['cliente_dir']."\n");
	$printer -> feed();

	$printer -> text("CUOTA       : ".$_POST['moneda']."  ".$_POST['cuota_mon']."\n");
	$printer -> text("VENCIMIENTO : ".$_POST['cuota_ven']."\n");

	$printer -> text("------------------------------------------\n");
	$printer -> text("CONCEPTO  : ".$_POST['detalle']."\n");
	
	if(trim($_POST['det_ext']) != ''){
		$array = explode('||', trim($_POST['det_ext']));

		for ($i=0; $i <count($array) ; $i++) { 
			$printer -> text($array[$i]."\n");
		}
	}

	$printer -> feed();
	$printer -> setEmphasis(true);

	if($_POST['pago_monto']>0)
	{
		$printer -> text("MONTO     : ".$_POST['moneda']." ".$_POST['pago_monto']."\n");
	}
	
	if($_POST['pago_cambio']>0)
	{
		$printer -> text("TIP CAMB. : ".$_POST['tipo_cambio']."\n");
		$printer -> text("CAMBIO    : ".$_POST['pago_cambio']."\n");
	}

	if($_POST['ingreso_monto']>0)
	{
		$printer -> text("PAGO C/CAJA CLIENTE : ".$_POST['moneda']."  ".$_POST['ingreso_monto']."\n");
	}
	
	if($_POST['cliente_caj']>0)
	{
		$printer -> feed();
		$printer -> text("SALDO CAJA CLIENTE  : ".$_POST['moneda']."  ".$_POST['cliente_caj']."\n");
	}

	$printer -> feed();

	$printer -> setEmphasis(false);
	$printer -> text("------------------------------------------\n");
	$printer -> text("REGISTRADO : ".$_POST['cajero']."\n");

	$printer -> setEmphasis(false);
	// $printer -> text("------------------------------------------\n");
	// $printer -> text("SON: ".$_POST['monto_letras'].$montext."\n");
	// $printer -> text("------------------------------------------\n");

	$printer -> feed();
	$printer -> text("FIRMA    : "."\n");
	$printer -> feed();
	$printer -> feed();
	$printer -> feed();
	$printer -> feed();
	$printer -> feed();

	$printer -> setJustification(Printer::JUSTIFY_CENTER);
	$printer -> setFont(Printer::MODE_FONT_B);
	$printer -> text("www.prestamosdelnortechiclayo.com\n");
	$printer -> feed();
	$printer -> feed();

	$printer -> cut();

	$printer -> close();
}
catch(Exception $e)
{
	echo "Error de conexión: " . $e -> getMessage() . "\n";
}

?>