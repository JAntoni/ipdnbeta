<?php
  require_once('../../core/usuario_sesion.php');
  
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $modulo_nombre = $_POST['modulo_nombre'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Opción de GC Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Asignar Opción o Registrar Comentarios';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Opción de GC ';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Opción de GC ';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php


?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_asignaropciongc" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      <form id="form_opcionesgc_asignar" role="form" method="POST">
        <div class="modal-body">
          <input type="hidden" name="action" value="insertar" />
          <input type="hidden" id="directorio" value="opcionesgc_asignar">

          <input type="hidden" name="hdd_credito_subtipo" value="<?php echo $credito_subtipo;?>">
          <input type="hidden" name="hdd_credito_tipo" value="<?php echo $credito_tipo;?>">
          <input type="hidden" name="hdd_cuota_actual" value="<?php echo $cuota_actual;?>">
          <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
          <input type="hidden" name="hdd_cuotadetalle_id" value="<?php echo $cuotadetalle_id;?>">
          <input type="hidden" name="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" name="hdd_cliente_nom" value="<?php echo $cliente_nom;?>">
          <input type="hidden" name="hdd_cuota_fecha" value="<?php echo $cuota_fecha;?>">
          <input type="hidden" id="hdd_opcionesgc_nom" name="hdd_opcionesgc_nom" value="<?php echo $opcionesgc_nom;?>">

          <label for="txt_filtro" class="control-label" style="font-size: 12pt;">Detalle del cliente a Gestionar</label>
          <div class="panel panel-primary shadow-simple">
            <div class="panel-body">
              <?php if($modulo_nombre == 'reportecreditomejorado'):?>
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>CLiente:</th>
                      <td colspan="3"><?php echo $cliente_nom;?></td>
                    </tr>
                    <tr>
                      <th>Representante:</th>
                      <td colspan="3"><?php echo $representante_nom;?></td>
                    </tr>
                    <tr>
                      <th>Dirección:</th>
                      <td colspan="3"><?php echo $cliente_dir;?></td>
                    </tr>
                    <tr>
                      <th>Cuota Original:</th>
                      <td><?php echo mostrar_moneda($cuota_monto);?></td>
                      <th>Cuota en S/.:</th>
                      <td><?php echo mostrar_moneda($cuota_soles);?></td>
                    </tr>
                    <tr>
                      <th>Fecha Cuota:</th>
                      <td><?php echo $cuota_fecha;?></td>
                      <th>Fecha Ultimo Pago:</th>
                      <td><?php echo $ultimopago_fecha;?></td>
                    </tr>
                    <tr>
                      <th>Inactividad:</th>
                      <td><?php echo $inactividad;?></td>
                      <th>Deuda Acumulada:</th>
                      <td><?php echo mostrar_moneda($deuda_acumulada);?></td>
                    </tr>
                  </tbody>
                </table>
              <?php endif;?>

              <?php if($modulo_nombre == 'cobranzatodos'):?>
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>CLiente:</th>
                      <td colspan="3"><?php echo $cliente_nom;?></td>
                    </tr>
                    <tr>
                      <th>Representante:</th>
                      <td colspan="3"><?php echo $representante_nom;?></td>
                    </tr>
                    <tr>
                      <th>Dirección:</th>
                      <td colspan="3"><?php echo $cliente_dir;?></td>
                    </tr>
                    <tr>
                      <th>Cuota Original:</th>
                      <td><?php echo $simbolo_moneda.' '.mostrar_moneda($cuota_monto);?></td>
                      <th>Fecha Cuota:</th>
                      <td><?php echo $cuota_fecha;?></td>
                    </tr>
                  </tbody>
                </table>
              <?php endif;?>
            </div>
          </div>
          <p></p>
          <p></p>

          <label for="txt_filtro" class="control-label" style="font-size: 12pt;">Asignar Opción para este Crédito</label>
          <div class="panel panel-primary shadow-simple">
            <div class="panel-body">
              <div class="form-group col-md-5">
                <label for="cmb_opcionesgc_id" class="control-label">Opción de Gestión</label>
                <select class="form-control input-sm" id="cmb_opcionesgc_id" name="cmb_opcionesgc_id" style="width: 100%;">
                  <?php require_once('../reportecreditomejorado/opcionesgc_select.php');?>
                </select>
              </div>
              <div class="form-group col-md-3 fecha_pdp" <?php echo $style_pdp_fecha;?> >
                <label for="txt_opciongc_fechapdp" class="control-label">Fecha para Gestión</label>
                <div class='input-group date' id='datetimepicker3'>
                  <input type='text' class="form-control input-sm" name="txt_opciongc_fechapdp" id="txt_opciongc_fechapdp" value="<?php echo $opciongc_fechapdp;?>"/>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
              <div class="form-group col-md-4">
                <label for="cmb_aplicar_todo" class="control-label">¿Aplica a Todo?</label>
                <select class="form-control input-sm" id="cmb_aplicar_todo" name="cmb_aplicar_todo" style="width: 100%;">
                  <option value="1">Solo a esta Cuota</option>
                  <option value="2">Todas las cuotas del CLiente</option>
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="txt_comentario_des" class="control-label">Opcional Puede Dejar un Comentario</label>
                <input class="form-control input-sm" type="text" id="txt_comentario_des" name="txt_comentario_des" value="<?php echo $comentario_des;?>" style="width: 100%;">
              </div>
              <div class="form-group col-md-12">
                <label for="cmb_encargado_id" class="control-label">Puede asignar un Responsable, por defecto será el Gestor de Cobranza:</label>
                <select class="form-control input-sm" id="cmb_encargado_id" name="cmb_encargado_id" style="width: 100%;">
                  <option value="0">Visible para todos los encargados....</option>
                  <option value="11" <?php echo $encargado_id == 11? 'selected' : '';?>>HERBERT ENGEL GALVEZ ROJAS</option>
                  <option value="32" <?php echo $encargado_id == 32? 'selected' : '';?>>CARLOS AUGUSTO CONCEPCIÓN LEON</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_opcionesgc_asignar">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/reportecreditomejorado/opcionesgc_todo.js?ver=2113';?>"></script>