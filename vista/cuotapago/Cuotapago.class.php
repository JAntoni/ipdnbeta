<?php
  if(defined('APP_URL')){
  require_once(APP_URL.'datos/conexion.php');}
  else{
  require_once('../../datos/conexion.php');}
  class Cuotapago extends Conexion{
    public $tb_cuotapago_id;
    public $tb_cuotapago_xac = 1;
    public $tb_cuotapago_reg;
    public $tb_cuotapago_mod;
    public $tb_cuotapago_usureg;
    public $tb_cuotapago_usumod;
    public $tb_modulo_id;
    public $tb_cuotapago_modid; 
    public $tb_cuotapago_fec; 
    public $tb_moneda_id; 
    public $tb_cuotapago_mon; 
    public $tb_cuotapago_tipcam; 
    public $tb_cuotapago_moncam; 
    public $tb_cuotapago_pagcon;
    public $tb_cliente_id; 
    public $tb_cuotapago_est;
    public $tb_cuotapago_obs;
    public $tb_cuotapago_his;
 
    function insertar(){
        $this->dblink->beginTransaction();
            try {
              $sql = "INSERT INTO tb_cuotapago (
                                                tb_cuotapago_xac, 
                                                tb_cuotapago_reg, 
                                                tb_cuotapago_mod, 
                                                tb_cuotapago_usureg,
                                                tb_cuotapago_usumod,  
                                                tb_modulo_id,	
                                                tb_cuotapago_modid,
                                                tb_cuotapago_fec,  
                                                tb_moneda_id,
                                                tb_cuotapago_mon,
                                                tb_cuotapago_tipcam,
                                                tb_cuotapago_moncam,
                                                tb_cuotapago_pagcon,
                                                tb_cliente_id, 
                                                tb_cuotapago_est,
                                                tb_cuotapago_obs)
                                        VALUES (
                                                :tb_cuotapago_xac,
                                                NOW(),
                                                NOW(),
                                                :tb_cuotapago_usureg,
                                                :tb_cuotapago_usumod,  
                                                :tb_modulo_id,	
                                                :tb_cuotapago_modid,
                                                :tb_cuotapago_fec,  
                                                :tb_moneda_id,
                                                :tb_cuotapago_mon,
                                                :tb_cuotapago_tipcam,
                                                :tb_cuotapago_moncam,
                                                :tb_cuotapago_pagcon,
                                                :tb_cliente_id, 
                                                1,
                                                :tb_cuotapago_obs);"; 

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_cuotapago_xac",$this->tb_cuotapago_xac, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_usureg",$this->tb_cuotapago_usureg, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_usumod",$this->tb_cuotapago_usumod, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_modulo_id",$this->tb_modulo_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_modid",$this->tb_cuotapago_modid, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_fec",$this->tb_cuotapago_fec, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_moneda_id",$this->tb_moneda_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_mon",$this->tb_cuotapago_mon, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_cuotapago_tipcam",$this->tb_cuotapago_tipcam, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_moncam",$this->tb_cuotapago_moncam, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_cuotapago_pagcon",$this->tb_cuotapago_pagcon, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_cliente_id",$this->tb_cliente_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotapago_obs",$this->tb_cuotapago_obs, PDO::PARAM_STR);

              $result = $sentencia->execute();
              $cuotapago_id = $this->dblink->lastInsertId();
              $this->dblink->commit();

              $data['estado'] = $result;
              $data['tb_cuotapago_id'] = $cuotapago_id;
              return $data; //si es correcto el ingreso retorna 1

        //        return $result; //si es correcto el ingreso retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
    }
     
    function insertar2(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuotapago (
              tb_cuotapago_xac, tb_credito_id, tb_creditotipo_id, 
              tb_moneda_id, tb_cuotapago_num, tb_cuotapago_fec, 
              tb_cuotapago_cap, tb_cuotapago_amo, tb_cuotapago_int, 
              tb_cuotapago_cuo, tb_cuotapago_pro, tb_cuotapago_persubcuo, 
              tb_cuotapago_est, tb_cuotapago_acupag
            ) 
            VALUES (
              :cuotapago_xac, :credito_id, :creditotipo_id, 
              :moneda_id, :cuotapago_num, :cuotapago_fec, 
              :cuotapago_cap, :cuotapago_amo, :cuotapago_int, 
              :cuotapago_cuo, :cuotapago_pro, :cuotapago_persubcuo, 
              :cuotapago_est, :cuotapago_acupag
            );";

        $sentencia = $this->dblink->prepare($sql);
        // $sentencia->bindParam(":cuotapago_xac", $this->cuotapago_xac, PDO::PARAM_INT);
        // $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_num", $this->cuotapago_num, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_fec", $this->cuotapago_fec, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_cap", $this->cuotapago_cap, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_amo", $this->cuotapago_amo, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_int", $this->cuotapago_int, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_cuo", $this->cuotapago_cuo, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_pro", $this->cuotapago_pro, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_persubcuo", $this->cuotapago_persubcuo, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_est", $this->cuotapago_est, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_acupag", $this->cuotapago_acupag, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $cuotapago_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['cuotapago_id'] = $cuotapago_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_cuotapago(
                :cuotapago_id,
                :cuotapago_xac, :credito_id, :cuotapagotipo_id,
                :cuotapago_can, :cuotapago_pro, :cuotapago_val,
                :cuotapago_valtas, :cuotapago_ser, :cuotapago_kil,
                :cuotapago_pes, :cuotapago_tas, :cuotapago_det,
                :cuotapago_web)";

        $sentencia = $this->dblink->prepare($sql);
        // $sentencia->bindParam(":cuotapago_id", $this->cuotapago_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_xac", $this->cuotapago_xac, PDO::PARAM_INT);
        // $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapagotipo_id", $this->cuotapagotipo_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_can", $this->cuotapago_can, PDO::PARAM_INT);
        // $sentencia->bindParam(":cuotapago_pro", $this->cuotapago_pro, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_val", $this->cuotapago_val, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_valtas", $this->cuotapago_valtas, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_ser", $this->cuotapago_ser, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_kil", $this->cuotapago_kil, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_pes", $this->cuotapago_pes, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_tas", $this->cuotapago_tas, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_det", $this->cuotapago_det, PDO::PARAM_STR);
        // $sentencia->bindParam(":cuotapago_web", $this->cuotapago_web, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cuotapago_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cuotapago WHERE tb_cuotapago_id =:cuotapago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotapago_id", $cuotapago_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cuotapago_id){
      try {
        $sql = "SELECT cuo.*, mon.*, tb_usuario_nom FROM tb_cuotapago cuo 
          INNER JOIN tb_moneda mon ON mon.tb_moneda_id = cuo.tb_moneda_id 
          INNER JOIN tb_usuario usu on usu.tb_usuario_id = cuo.tb_cuotapago_usureg
          WHERE tb_cuotapago_id =:cuotapago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotapago_id", $cuotapago_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_cuotapagos_modulo($modulo_id, $cuotapago_modid){
      try {
        $sql = "SELECT * FROM tb_cuotapago cp INNER JOIN tb_moneda m ON cp.tb_moneda_id = m.tb_moneda_id WHERE tb_cuotapago_xac = 1 AND tb_modulo_id =:modulo_id AND tb_cuotapago_modid =:cuotapago_modid ORDER BY tb_cuotapago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotapago_modid", $cuotapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrar_cuotapago_ingresos_por_tipo_cuota($modulo_id, $cuotapago_modid){
      try {
        $sql = "SELECT tb_cuotapago_id, i.tb_ingreso_id, tb_ingreso_imp
              FROM tb_cuotapago cp 
              INNER JOIN tb_ingreso i ON cp.tb_cuotapago_id=i.tb_ingreso_modide
              INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
              WHERE tb_cuotapago_xac=1 AND tb_ingreso_xac=1
              AND i.tb_modulo_id = 30 AND cp.tb_modulo_id =:modulo_id AND cp.tb_cuotapago_modid =:cuotapago_modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotapago_modid", $cuotapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function modificar_campo($cuotapago_id, $cuotapago_columna, $cuotapago_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($cuotapago_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_cuotapago SET ".$cuotapago_columna." =:cuotapago_valor WHERE tb_cuotapago_id =:cuotapago_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cuotapago_id", $cuotapago_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":cuotapago_valor", $cuotapago_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":cuotapago_valor", $cuotapago_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function mostrar_cuota_ingreso($cuo_id){
      try {
        $sql = "SELECT cd.tb_cuota_id, cd.tb_cuotadetalle_id, tb_ingreso_imp
                FROM tb_cuotadetalle cd 
                INNER JOIN tb_cuotapago cp ON cd.tb_cuotadetalle_id=cp.tb_cuotapago_modid
                INNER JOIN tb_ingreso i ON cp.tb_cuotapago_id=i.tb_ingreso_modide
                INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
                WHERE tb_cuotadetalle_xac=1 AND tb_cuotapago_xac=1 AND tb_ingreso_xac=1
                AND i.tb_modulo_id=30 AND cp.tb_modulo_id=2 AND cd.tb_cuota_id=:cuo_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cuo_id", $cuo_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    
    function mostrar_cuotatipo_ingreso($mod_id,$modid){
      try {
        $sql = "SELECT tb_cuotapago_id, i.tb_ingreso_id, tb_ingreso_imp
                FROM tb_cuotapago cp 
                INNER JOIN tb_ingreso i ON cp.tb_cuotapago_id=i.tb_ingreso_modide
                INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
                WHERE tb_cuotapago_xac=1 AND tb_ingreso_xac=1
                AND i.tb_modulo_id=30 AND cp.tb_modulo_id=:mod_id AND cp.tb_cuotapago_modid=:modid" ;

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
                $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
  
    function registro_historial($cuopag_id, $his){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuotapago set tb_cuotapago_his = CONCAT(tb_cuotapago_his, :his) where tb_cuotapago_id =:cuopag_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuopag_id", $cuopag_id, PDO::PARAM_INT);
        $sentencia->bindParam(":his", $his, PDO::PARAM_STR);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrar_cuotapagos_creditomenor($cuotapago_modid){
      try {
        $sql = "SELECT * FROM tb_cuotapago 
                WHERE tb_cuotapago_xac = 1 
                AND tb_modulo_id =1 
                AND tb_cuotapago_modid =:cuotapago_modid 
                ORDER BY tb_cuotapago_id
                LIMIT 1
                ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotapago_modid", $cuotapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function importe_total_cuotadetalle($cuode_id){
      try {
        $sql = "SELECT SUM(tb_ingreso_imp) as importe 
                FROM tb_cuotadetalle cud inner join tb_cuotapago cp on cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id 
                inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id 
                where cp.tb_modulo_id = 2 
                and ing.tb_ingreso_xac =1 
                and tb_cuotapago_xac=1 
                and ing.tb_modulo_id = 30 
                and cud.tb_cuotadetalle_id =:cuode_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuode_id", $cuode_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function listar_cuotapagos_por_credito_cuotadetalle($cre_id, $tb_credito, $cre_tip){
      try {
        $sql = "SELECT * FROM tb_cuotapago 
                where tb_modulo_id = 2 
                and tb_cuotapago_modid in 
                (select tb_cuotadetalle_id from tb_creditogarveh cre 
                inner join tb_cuota cu on cu.tb_credito_id = cre.tb_credito_id 
                inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
                where cu.tb_creditotipo_id =:cre_tip and cre.tb_credito_id =:cre_id)";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
                $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function mostrar_cuotapago_ingreso($mod_id,$modid){
      try {
        $sql = "SELECT *
          FROM tb_ingreso i
          INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
          WHERE tb_ingreso_xac=1 AND i.tb_modulo_id=:mod_id AND i.tb_ingreso_modide=:modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_cuotapago_egreso($mod_id,$modid){
      try {
        $sql = "SELECT *
          FROM tb_egreso i
          INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
          WHERE tb_egreso_xac=1 AND i.tb_modulo_id=:mod_id AND i.tb_egreso_modide=:modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_cajacambio($id){
      try {
        $sql = "SELECT *
          FROM tb_cajacambio c INNER JOIN tb_moneda m
          ON c.tb_moneda_id = m.tb_moneda_id
          WHERE c.tb_modulo_id='30' AND c.tb_cajacambio_modid=:id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function filtrar($mod_id,$modid){
      try {
        $sql = "SELECT * 
                FROM tb_cuotapago cp 
                INNER JOIN tb_moneda m ON cp.tb_moneda_id=m.tb_moneda_id
                WHERE tb_cuotapago_xac=1
                AND tb_modulo_id=:mod_id
                AND tb_cuotapago_modid=:modid
                ORDER BY tb_cuotapago_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_STR);
                $sentencia->bindParam(":modid", $modid, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function mostrar_clientecaja($id){
      try {
        $sql = "SELECT *
                FROM tb_clientecaja c INNER JOIN tb_moneda m
                ON c.tb_moneda_id = m.tb_moneda_id
                WHERE c.tb_modulo_id='30' AND c.tb_clientecaja_modid=:id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":id", $id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function cambio_de_cliente($cli_id,$cli_id_nuevo){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_cuotapago SET tb_cliente_id =:cli_id_nuevo WHERE tb_cliente_id =:cli_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cli_id_nuevo", $cli_id_nuevo, PDO::PARAM_INT);
          $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function cuotas_pagadas_garveh_hipo($fecha1, $fecha2){
      try {
        $sql = "SELECT
            tb_ingreso_fec,
            ing.tb_cliente_id,
            tb_ingreso_imp,
            ing.tb_moneda_id,
            pag.tb_moneda_id,
            pag.tb_cuotapago_tipcam,
            ROUND(SUM(IF(ing.tb_moneda_id = 2, tb_ingreso_imp * IFNULL(NULLIF(pag.tb_cuotapago_tipcam, 0), 1), tb_ingreso_imp)), 2) AS cuota_real
          FROM tb_ingreso ing
          INNER JOIN tb_cuotapago pag ON (pag.tb_cuotapago_id = ing.tb_ingreso_modide AND ing.tb_modulo_id = 30)
          WHERE tb_ingreso_xac = 1 AND tb_cuenta_id = 1 AND tb_subcuenta_id IN(1, 3, 4) AND tb_ingreso_fec BETWEEN :fecha1 AND :fecha2
          GROUP BY 1 ORDER BY 1 ASC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //Antonio 26-07-2024
    function listar_pagos_rango_creditomenor($pago_fec1, $pago_fec2)
    {
      try {

        $sql = "
          SELECT 

            tb_cuota_id,
            cre.tb_cuotatipo_id,
            DATE_FORMAT(tb_ingreso_fec, '%Y-%m') AS mes_anio,
            MAX(tb_ingreso_fec) as fecha_pago,
            SUM(tb_ingreso_imp) as monto_pagado,
            tb_cuota_est,
            tb_cuota_fec,
            (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real,
            (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_credito_numcuomax ELSE tb_credito_numcuo END) as numero_cuotas,
            tb_cuota_num,
            tb_cuota_cuo,
            tb_cuota_int,
            tb_cuota_cap,
            tb_cuota_amo,
            tb_ingreso_id, 
            ing.tb_moneda_id, 
            pag.tb_cuotapago_tipcam, 
            tb_ingreso_det,
            ing.tb_cliente_id,
            cre.tb_credito_id,
            cli.tb_cliente_nom,
            cli.tb_cliente_emprs,
            cli.tb_cliente_empruc,
            cli.tb_cliente_doc
                      
          FROM tb_ingreso ing 
          INNER JOIN tb_cuotapago pag ON (pag.tb_cuotapago_id = ing.tb_ingreso_modide AND ing.tb_modulo_id = 30)
          INNER JOIN tb_cuota cuo ON (cuo.tb_cuota_id = pag.tb_cuotapago_modid AND pag.tb_modulo_id = 1)
          INNER JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 1) 
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
          WHERE 
            tb_ingreso_xac = 1  
            AND tb_cuotapago_xac = 1 
            AND tb_ingreso_ap = 0 
            AND tb_ingreso_fec BETWEEN :pago_fec1 AND :pago_fec2
          GROUP BY mes_anio, tb_cuota_id ORDER BY 1;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":pago_fec1", $pago_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":pago_fec2", $pago_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_pagos_anticipos_tipo_cuota_menor_fecha($modulo_id, $cuotapago_modid, $fecha_pago){
      try {
        $sql = "SELECT tb_cuotapago_id, i.tb_ingreso_id, tb_ingreso_imp, tb_ingreso_fec
              FROM tb_cuotapago cp 
              INNER JOIN tb_ingreso i ON (cp.tb_cuotapago_id=i.tb_ingreso_modide AND i.tb_modulo_id = 30)
              INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
              WHERE tb_cuotapago_xac = 1 AND tb_ingreso_xac = 1
              AND cp.tb_modulo_id =:modulo_id AND cp.tb_cuotapago_modid =:cuotapago_modid AND tb_ingreso_fec < :fecha_pago";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotapago_modid", $cuotapago_modid, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha_pago", $fecha_pago, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //JUAN 10-09-2024
    function listar_ingresos_por_credito_cuotadetalle($cre_id, $tb_credito, $cre_tip){
      try {
        $sql = "SELECT * FROM tb_cuotapago 
                where tb_modulo_id = 2 
                and tb_cuotapago_modid in 
                (select tb_cuotadetalle_id from tb_creditogarveh cre 
                inner join tb_cuota cu on cu.tb_credito_id = cre.tb_credito_id 
                inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
                where cu.tb_creditotipo_id =:cre_tip and cre.tb_credito_id =:cre_id)";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
                $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Cuotas de Ingreso registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
  }

?>
