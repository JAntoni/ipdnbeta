<?php
require_once('../../core/usuario_sesion.php');

require_once ("../empresa/Empresa.class.php");
$oEmpresa = new Empresa();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();

require_once ("../impresora/Impresora.class.php");
$oImpresora = new Impresora();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
//require_once("../formatos/numletras.php");


$dts=$oEmpresa->mostrarUno($_SESSION['empresa_id']);
if($dts['estado']==1){
	$emp_ruc=$dts['data']['tb_empresa_ruc'];
	$emp_nomcom=$dts['data']['tb_empresa_nomcom'];
	$emp_razsoc=$dts['data']['tb_empresa_razsoc'];
	$emp_dir=$dts['data']['tb_empresa_dir'];
	$emp_dir2=$dts['data']['tb_empresa_dir2'];
	$emp_tel=$dts['data']['tb_empresa_tel'];
	$emp_ema=$dts['data']['tb_empresa_ema'];
	$emp_fir=$dts['data']['tb_empresa_fir'];	
}       
$dts=NULL;
//$cuotapago_id = intval($_POST['cuotapago_id']);
$dts=$oCuotapago->mostrarUno($_POST['cuotapago_id']);
    if($dts['estado']==1){
	$cuotapago_id = $dts['data']['tb_cuotapago_id'];
	$fecreg		= mostrar_fecha_hora($dts['data']['tb_cuotapago_reg']);
	$fecmod		= mostrar_fecha_hora($dts['data']['tb_cuotapago_mod']);
	$usureg		=$dts['data']['tb_cuotapago_usureg'];
	$usumod		=$dts['data']['tb_cuotapago_usumod'];
	
	$mod_id		=$dts['data']['tb_modulo_id'];
	$modid		=$dts['data']['tb_cuotapago_modid'];

	$fec		= mostrar_fecha($dts['data']['tb_cuotapago_fec']);

	$cli_id		=$dts['data']['tb_cliente_id'];
	$cli_nom 	=$dts['data']['tb_cliente_nom'];
	$cli_doc 	=$dts['data']['tb_cliente_doc'];
	$cli_dir 	=$dts['data']['tb_cliente_dir'];
	$cli_tip 	=$dts['data']['tb_cliente_tip'];
	
	$mon		=$dts['data']['tb_cuotapago_mon'];
	$tipcam		=$dts['data']['tb_cuotapago_tipcam'];
	$moncam		=$dts['data']['tb_cuotapago_moncam'];
	
	$mon_id		=$dts['data']['tb_moneda_id'];
	$mon_nom	=$dts['data']['tb_moneda_nom'];
	$cuota_obs      =$dts['data']['tb_cuotapago_obs'];
	$est		=$dts['data']['tb_cuotapago_est'];
}
$dts=NULL;


$cuota_mon; $cuota_id; $fecha_vence;

if($mod_id==1)
{
	$dts=$oCuota->mostrarUno($modid);
	if($dts['estado']==1){
		$cuota_ven = mostrar_fecha($dts['data']['tb_cuota_fec']);
		$cuota_id = $dts['data']['tb_cuota_id'];
		$cretip_id=$dts['data']['tb_creditotipo_id'];
		$cre_id=$dts['data']['tb_credito_id'];
		$cuo_num=$dts['data']['tb_cuota_num'];
		$cuota_mon = $dts['data']['tb_cuota_cuo'];
		$cuota_capital = $dts['data']['tb_cuota_cap'];
		$cuota_amortizacion = $dts['data']['tb_cuota_amo'];
		$cuota_interes = $dts['data']['tb_cuota_int'];
		$cuo_persubcuo=$dts['data']['tb_cuota_persubcuo'];
        }
        $dts=NULL;
}

if($mod_id==2)
{
	$dts=$oCuotadetalle->mostrarUno($modid);
	if($dts['estado']==1){
            $cuota_ven =mostrar_fecha($dts['data']['tb_cuotadetalle_fec']);
            $cuo_id=$dts['data']['tb_cuota_id'];
            $cuodet_fec=mostrar_fecha($dts['data']['tb_cuotadetalle_fec']);
            $mon_id=$dts['data']['tb_moneda_id'];
            $mon_nom=$dts['data']['tb_moneda_nom'];
            $cuodet_num=$dts['data']['tb_cuotadetalle_num'];
            $cuota_mon =$dts['data']['tb_cuotadetalle_cuo'];
            $cuodet_acupag =$dts['data']['tb_cuotadetalle_acupag'];//si es 1-> es una cuota acuerdo de pago
	// 	
        }
        $dts=NULL;
  //obtenemos información del acuerdo de pago si es que es una cuota de acuerdopago
  $obs_acupag = '';
  if($cuodet_acupag == 1){
    $dts = $oAcuerdopago->informacion_acuerdopago_por_cuotadetalleid($modid);
      if($dts['estado']==1){
      $instancia = $dts['data']['tb_acuerdopago_ins'];//numero de instancia del acuerdopago
      }
    $dts=NULL;
    
    $obs_acupag = ' AP Instancia 0'.$instancia;
  }
	$dts=$oCuota->mostrarUno($cuo_id);
	if($dts['estado']==1){
		$cuota_id = $dts['data']['tb_cuota_id'];
		$cretip_id=$dts['data']['tb_creditotipo_id'];
		$cre_id=$dts['data']['tb_credito_id'];
		$cuo_num=$dts['data']['tb_cuota_num'];
		$cuo_cuo = $dts['data']['tb_cuota_cuo'];
		$cuota_capital = $dts['data']['tb_cuota_cap'];
		$cuota_amortizacion = $dts['data']['tb_cuota_amo'];
		$cuota_interes = $dts['data']['tb_cuota_int'];
		$cuo_persubcuo=$dts['data']['tb_cuota_persubcuo'];
        }
        $dts=NULL;
}

$ingreso_monto = 0;

//parametro 30 para modulo id
$dts = $oIngreso->modulo_cuotapago(30,$cuotapago_id);
	if($dts['estado']==1){
	$det_ext = $dts['data']['tb_ingreso_detex'];
	$ingr_det = $dts['data']['tb_ingreso_det'];
        
	if($mon <= 0)
		$ingreso_monto = $dts['data']['tb_ingreso_imp'];
        }


if($cretip_id==1)
{
	require_once ("../creditomenor/Creditomenor.class.php");
	$oCredito = new Creditomenor();

	$dts=$oCredito->mostrarUno($cre_id);
	if($dts['estado']==1){
		// $mon_id=$dt['tb_moneda_id'];
		$cre_numcuo = $dts['data']['tb_credito_numcuomax'];
		$cli_id=$dts['data']['tb_cliente_id'];
		$cli_doc = $dts['data']['tb_cliente_doc'];
		$cli_nom=$dts['data']['tb_cliente_nom'];
		$cliente=$cli_nom.' | '.$cli_doc;
        }
        $dts=NULL;

	$emp_razsoc="INVERSIONES Y PRESTAMOS DEL NORTE SAC";
	$emp_ruc="20600575679";

  $codigo='CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);

  $cuota=$cuo_num.'/'.$cre_numcuo.' ('.$cre_numcuo.')';

	$detalle = "PAGO DE CUOTA $codigo N° CUOTA: $cuota";

	//para credito menor el detalle será el mismo detalle de ingreso
	$detalle = $ingr_det.' | Obs: '.$cuota_obs;
}

$clientecaja = 0;
if($cretip_id==2)
{
	require_once ("../creditoasiveh/Creditoasiveh.class.php");
	$oCredito = new Creditoasiveh();

	$dts=$oCredito->mostrarUno($cre_id);
	if($dts['estado']==1){

		$mon_id 	=$dts['data']['tb_moneda_id'];
		$cre_numcuo =$dts['data']['tb_credito_numcuo'];

		$cli_id 	=$dts['data']['tb_cliente_id'];
		$cli_doc 	=$dts['data']['tb_cliente_doc'];
		$cli_nom 	=$dts['data']['tb_cliente_nom'];

		$cliente 	=$cli_nom.' | '.$cli_doc;

        }
        $dts=NULL;

	$codigo='CAV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).$obs_acupag;

	$cuota="MES ".$cuo_num.'/'.$cre_numcuo.' SEMANA '.$cuodet_num.'/'.$cuo_persubcuo.'';

	$detalle = "ASCVEH $codigo N° CUOTA: $cuota"." | Obs: ".$cuota_obs;

	//$dts1=$oClientecaja->filtrar($cli_id,$cre_id,$mon_id);
	$dts1 = $oClientecaja->filtrar_por_cliente($cli_id,$mon_id, 'tb_creditoasiveh');
	
        if($dts1['estado']==1){
            foreach($dts1['data'] as $key=>$dt1)
            {
                if($dt1['tb_clientecaja_tip']==1)
                {
                        $clientecaja = $clientecaja + $dt1['tb_clientecaja_mon'];
                }
                if($dt1['tb_clientecaja_tip']==2)
                {
                        $clientecaja = $clientecaja - $dt1['tb_clientecaja_mon'];
                }
            }
        }
}

if($cretip_id==3)
{
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();

  $dts=$oCredito->mostrarUno($cre_id);
  if($dts['estado']==1){
    $mon_id   =$dts['data']['tb_moneda_id'];
    $cre_numcuo =$dts['data']['tb_credito_numcuomax'];
    $cuotip_id=$dts['data']['tb_cuotatipo_id'];
    $cuot_subper = $dts['data']['tb_cuotasubperiodo_id'];
    $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
    $representante_nom = $dts['data']['tb_representante_nom'];

    $emp_razsoc = $representante_nom;

    $cli_id   =$dts['data']['tb_cliente_id'];
    $cli_doc  =$dts['data']['tb_cliente_doc'];
    $cli_nom  =$dts['data']['tb_cliente_nom'];

    $cliente  =$cli_nom.' | '.$cli_doc;
}
$dts=NULL;

  $codigo='CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);

  $nom_subper = 'MES';

  if($cuot_subper == 2)
    $nom_subper = 'QUINCENA';

  if($cuot_subper == 3)
    $nom_subper = 'SEMANA';

  //numero de cuotas
  if($cuotip_id==4)
    $cuota = $nom_subper.' '.$cuodet_num.'/'.$cuo_persubcuo.' ('.$cuo_num.'/'.$cre_numcuomax.')';

  if($cuotip_id==3)
    $cuota = $cuo_num.'/'.$cre_numcuomax;

  $detalle = "PAGO DE CUOTA $codigo N° CUOTA: $cuota"." | Obs: ".$cuota_obs;

  //consulta cliente caja
  $dts1 = $oClientecaja->filtrar_por_cliente($cli_id,$mon_id, 'tb_creditogarveh');
    if($dts1['estado']==1){
        foreach($dts1['estado']as $key=>$dt1)
        {
            if($dt1['tb_clientecaja_tip']==1)
            {
                    $clientecaja = $clientecaja + $dt1['tb_clientecaja_mon'];
            }
            if($dt1['tb_clientecaja_tip']==2)
            {
                    $clientecaja = $clientecaja - $dt1['tb_clientecaja_mon'];
            }
        }
    }
}
if($cretip_id==4)
{
  require_once ("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();

  $dts=$oCredito->mostrarUno($cre_id);
  if($dts['estado']==1){

    $mon_id   =$dts['data']['tb_moneda_id'];
    $cre_cuotip = $dts['data']['tb_cuotatipo_id'];
    $cuot_subper = $dts['data']['tb_cuotasubperiodo_id'];
    $cre_numcuo =$dts['data']['tb_credito_numcuo'];
    $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
    $cuotip_id=$dts['data']['tb_cuotatipo_id'];
    $representante_nom = $dts['data']['tb_representante_nom'];
    
    $emp_razsoc = $representante_nom;
    
    $cli_id   =$dts['data']['tb_cliente_id'];
    $cli_doc  =$dts['data']['tb_cliente_doc'];
    $cli_nom  =$dts['data']['tb_cliente_nom'];

    $cliente  =$cli_nom.' | '.$cli_doc;

}
  
  $codigo='CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
  
  $nom_subper = 'MES';

  if($cuot_subper == 2)
    $nom_subper = 'QUINCENA';

  if($cuot_subper == 3)
    $nom_subper = 'SEMANA';

  //numero de cuotas
  if($cuotip_id==4)
    $cuota = $nom_subper.' '.$cuodet_num.'/'.$cuo_persubcuo.' ('.$cuo_num.'/'.$cre_numcuo.')';

  if($cuotip_id==3)
    $cuota = $cuo_num.'/'.$cre_numcuomax;
  

  $detalle = "PAGO DE CUOTA $codigo N° CUOTA: $cuota"." | Obs: ".$cuota_obs;

  //consulta cliente caja
  $dts1 = $oClientecaja->filtrar_por_cliente($cli_id,$mon_id, 'tb_creditohipo');
	
  if($dts1['estado']==1){
    foreach($dts1['data']as $key=>$dt1)
    {
    	if($dt1['tb_clientecaja_tip']==1)
    	{
        	$clientecaja = $clientecaja + $dt1['tb_clientecaja_mon'];
        }
        if($dt1['tb_clientecaja_tip']==2)
        {
        	$clientecaja = $clientecaja - $dt1['tb_clientecaja_mon'];
        }
    }
}
}

//impresora
$imp_id=1;
if($imp_id>0)
{
	$dts=$oImpresora->mostrarUno($imp_id);
	if($dts['estado']==1){
            $imp_nom 	=$dts['data']['tb_impresora_nom'];
            $imp_nomloc =$dts['data']['tb_impresora_nomloc'];
            $imp_ser 	=$dts['data']['tb_impresora_ser'];
            $imp_url 	=$dts['data']['tb_impresora_url'];
            $imp_ip 	=$dts['data']['tb_impresora_ip'];
        }
        $dts=NULL;
}

//cajero
$dts=$oUsuario->mostrarUno($usureg);
    if($dts['estado']==1){
	$usu_nom	=$dts['data']['tb_usuario_nom'];
	$usu_apepat	=$dts['data']['tb_usuario_apepat'];
	$usu_apemat	=$dts['data']['tb_usuario_apemat'];
    }

//$texto_cajero=substr($usu_nom, 0, 3).substr($usu_apepat, 0, 1).substr($usu_apemat, 0, 1);
$cajero=$usu_nom.' '.$usu_apepat.' '.$usu_apemat;


//------------impresion
$data['imp_nom'] 	=$imp_nom;
$data['imp_nomloc'] =$imp_nomloc;
$data['imp_ser'] 	=$imp_ser;
$data['imp_url'] 	=$imp_url;
$data['imp_ip']	 	=$imp_ip;


$data['emp_razsoc'] =   $emp_razsoc;
$data['emp_ruc']	=$emp_ruc;
$data['emp_dir1']	=$emp_dir;
$data['emp_dir2']	=$emp_dir2;

$data['cretip_id']	=$cretip_id;

//$data['cuo_cuo'] 	=$cuo_cuo;
$data['cuota_mon']=$cuota_mon;
$data['cuota_cap']=$cuota_capital;
$data['cuota_amo']=$cuota_amortizacion;
$data['cuota_int']=$cuota_interes;
$data['ingreso_monto']	=$ingreso_monto; // cuando el valor de anular es = 0, consulta a Ingreso
$data['cuota_ven'] 	=$cuota_ven;

$data['fecha'] 			= $fec;
$data['fechareg'] 		= $fecreg;

$data['operacion_id'] 	= $_POST['cuopag_id'];

$data['cliente'] 		= $cli_nom;
$data['cliente_doc']	= $cli_doc;
$data['cliente_dir']	= $cli_dir;

$data['detalle'] 		= $detalle;
$data['det_ext'] = $det_ext;


$data['pago_monto'] 	= $mon;
$data['tipo_cambio'] 	= $tipcam;
$data['pago_cambio'] 	= $moncam;

$data['mon_id'] 		= $mon_id;
$data['moneda'] 		= $mon_nom;

//$data['monto_letras'] 	=numtoletras($mon,$mon_id);

$data['cajero']	=$cajero;
$data['cliente_caj'] = mostrar_moneda($clientecaja);

echo json_encode($data);

function sanear_string($string)
{
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
  
    return $string;
}

?>