<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../garantiatipo/Garantiatipo.class.php');
  $oGarantiatipo = new Garantiatipo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$garantiatipo_nom = mb_strtoupper($_POST['txt_garantiatipo_nom'], 'UTF-8');
 		$garantiatipo_des = $_POST['txt_garantiatipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Tipo de Garantía.';
 		if($oGarantiatipo->insertar($garantiatipo_nom, $garantiatipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Garantía registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$garantiatipo_id = intval($_POST['hdd_garantiatipo_id']);
 		$garantiatipo_nom = mb_strtoupper($_POST['txt_garantiatipo_nom'], 'UTF-8');
 		$garantiatipo_des = $_POST['txt_garantiatipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Tipo de Garantía.';

 		if($oGarantiatipo->modificar($garantiatipo_id, $garantiatipo_nom, $garantiatipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Garantía modificado correctamente. '.$garantiatipo_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$garantiatipo_id = intval($_POST['hdd_garantiatipo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Tipo de Garantía.';

 		if($oGarantiatipo->eliminar($garantiatipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Garantía eliminado correctamente. '.$garantiatipo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>