
$(document).ready(function(){
  $('#form_garantiatipo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"garantiatipo/garantiatipo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_garantiatipo").serialize(),
				beforeSend: function() {
					$('#garantiatipo_mensaje').show(400);
					$('#btn_guardar_garantiatipo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#garantiatipo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#garantiatipo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		garantiatipo_tabla();
		      		$('#modal_registro_garantiatipo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#garantiatipo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#garantiatipo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_garantiatipo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#garantiatipo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#garantiatipo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_garantiatipo_nom: {
				required: true,
				minlength: 2
			},
			txt_garantiatipo_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_garantiatipo_nom: {
				required: "Ingrese un nombre para el Tipo de Crédito",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_garantiatipo_des: {
				required: "Ingrese una descripción para el Tipo de Crédito",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
