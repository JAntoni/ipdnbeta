<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Garantiatipo extends Conexion{

    function insertar($garantiatipo_nom, $garantiatipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_garantiatipo(tb_garantiatipo_xac, tb_garantiatipo_nom, tb_garantiatipo_des)
          VALUES (1, :garantiatipo_nom, :garantiatipo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiatipo_nom", $garantiatipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiatipo_des", $garantiatipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($garantiatipo_id, $garantiatipo_nom, $garantiatipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_garantiatipo SET tb_garantiatipo_nom =:garantiatipo_nom, tb_garantiatipo_des =:garantiatipo_des WHERE tb_garantiatipo_id =:garantiatipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiatipo_id", $garantiatipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiatipo_nom", $garantiatipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiatipo_des", $garantiatipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($garantiatipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_garantiatipo WHERE tb_garantiatipo_id =:garantiatipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiatipo_id", $garantiatipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($garantiatipo_id){
      try {
        $sql = "SELECT * FROM tb_garantiatipo WHERE tb_garantiatipo_id =:garantiatipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiatipo_id", $garantiatipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_garantiatipos(){
      try {
        $sql = "SELECT * FROM tb_garantiatipo WHERE tb_garantiatipo_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
