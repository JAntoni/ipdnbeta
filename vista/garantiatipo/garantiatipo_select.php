<?php
require_once('Garantiatipo.class.php');
$oGarantiatipo = new Garantiatipo();

$garantiatipo_id = (empty($garantiatipo_id))? 0 : $garantiatipo_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oGarantiatipo->listar_garantiatipos();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($garantiatipo_id == $value['tb_garantiatipo_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_garantiatipo_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_garantiatipo_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>