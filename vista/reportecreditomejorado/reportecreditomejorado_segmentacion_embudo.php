<style type="text/css">
  div.dataTables_wrapper div.dataTables_filter input {
    width: 80%;
    font-weight: bold;
    float: right;
  }

  div.dataTables_filter label {
    width: 100%;
    font-size: medium;
  }

  .nav-tabs-custom {
    box-shadow: none;
  }

  /* -------------------------------- */
  .title-teal-with-bakground {
    background-color: #d4ffff;
    color: #39cccc;
    padding: 0.8rem;
  }

  .lateral-item {
    border: 1.5px solid #30bbbb;
    color: #39cccc;
    padding: 5px;
    font-size: 14px;
    margin-right: -10px;
    border-radius: 50%;
    margin-top: -15px;
    margin-bottom: 50px;
  }

  .lateral-item-02 {
    border: 1.5px solid #0073b7;
    color: #0073b7;
    padding: 10px;
    font-size: 15px;
    border-radius: 50%;
    margin-top: -15px;
    margin-bottom: 38px;
  }

  .center-text {
    text-align: center;
    margin: 0px auto;
  }

  .funnel {
    width: 100%;
    margin: 0 auto;
  }

  ul.one {
    margin: 40px 170px;
    padding: 0;
    list-style: none;
    text-align: center;
  }

  .one .funnel-top {
    position: absolute;
    top: -7px;
    left: -180px;
    z-index: 20;
    width: 530px;
    height: 14px;
    background: #ecf0f5;
    border-radius: 100%;
  }

  .one .funnel-bottom {
    position: absolute;
    bottom: -7px;
    left: -0px;
    z-index: 20;
    width: 100%;
    /* 150px */
    height: 16px;
    background: #273445;
    border-radius: 100%;
  }

  .one li {
    font-size: 14px;
    line-height: 70px;
    height: 70px;
    width: 170px;
    position: relative;
    color: #ffffff;
    font-weight: bold;
  }

  .one li span {
    background: rgba(255, 255, 255, 0.3);
    padding: 5px 8px;
    border-radius: 4px;
    margin-left: 5px;
  }

  .one li:before {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0%;
    margin-left: -30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-left: 30px solid transparent;
  }

  .one li:after {
    content: "";
    position: absolute;
    z-index: 10;
    right: 0%;
    margin-left: 30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-right: 30px solid transparent;
  }

  .one li:nth-child(1) {
    background: #919eb1;
  }

  .one li:nth-child(1):before,
  .one li:nth-child(1):after {
    border-top-color: #919eb1;
  }

  .one li:nth-child(1):before {
    width: 180px;
    margin-left: -180px;
  }

  .one li:nth-child(1):after {
    width: 180px;
    margin-right: -180px;
  }

  .one li:nth-child(2) {
    background: #8491a5;
  }

  .one li:nth-child(2):before,
  .one li:nth-child(2):after {
    border-top-color: #8491a5;
  }

  .one li:nth-child(2):before {
    width: 150px;
    margin-left: -150px;
  }

  .one li:nth-child(2):after {
    width: 150px;
    margin-right: -150px;
  }

  .one li:nth-child(3) {
    background: #778599;
  }

  .one li:nth-child(3):before,
  .one li:nth-child(3):after {
    border-top-color: #778599;
  }

  .one li:nth-child(3):before {
    width: 120px;
    margin-left: -120px;
  }

  .one li:nth-child(3):after {
    width: 120px;
    margin-right: -120px;
  }

  .one li:nth-child(4) {
    background: #6d7b8f;
  }

  .one li:nth-child(4):before,
  .one li:nth-child(4):after {
    border-top-color: #6d7b8f;
  }

  .one li:nth-child(4):before {
    width: 90px;
    margin-left: -90px;
  }

  .one li:nth-child(4):after {
    width: 90px;
    margin-right: -90px;
  }

  .one li:nth-child(5) {
    background: #606f84;
  }

  .one li:nth-child(5):before,
  .one li:nth-child(5):after {
    border-top-color: #606f84;
  }

  .one li:nth-child(5):before {
    width: 60px;
    margin-left: -60px;
  }

  .one li:nth-child(5):after {
    width: 60px;
    margin-right: -60px;
  }

  .one li:nth-child(6) {
    background: #536075;
  }

  .one li:nth-child(6):before,
  .one li:nth-child(6):after {
    border-top-color: #536075;
  }

  .one li:nth-child(6):before {
    width: 30px;
    margin-left: -30px;
  }

  .one li:nth-child(6):after {
    width: 30px;
    margin-right: -30px;
  }

  ul.two {
    margin: 40px 170px;
    padding: 0;
    list-style: none;
    text-align: center;
  }

  .two .funnel-top {
    position: absolute;
    top: -7px;
    left: -180px;
    z-index: 20;
    width: 530px;
    height: 14px;
    background: #69F0AE;
    border-radius: 100%;
  }

  .two .funnel-bottom {
    position: absolute;
    bottom: -7px;
    left: -0px;
    z-index: 20;
    width: 100%;
    /* 150px */
    height: 16px;
    background: #B71C1C;
    border-radius: 100%;
  }

  .two li {
    font-size: 14px;
    line-height: 70px;
    height: 70px;
    width: 170px;
    position: relative;
    color: #ffffff;
    font-weight: bold;
  }

  .two li span {
    background: rgba(255, 255, 255, 0.3);
    padding: 5px 8px;
    border-radius: 4px;
    margin-left: 5px;
  }

  .two li:before {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0%;
    margin-left: -30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-left: 30px solid transparent;
  }

  .two li:after {
    content: "";
    position: absolute;
    z-index: 10;
    right: 0%;
    margin-left: 30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-right: 30px solid transparent;
  }

  .two li:nth-child(1) {
    background: #00C853;
  }

  .two li:nth-child(1):before,
  .two li:nth-child(1):after {
    border-top-color: #00C853;
  }

  .two li:nth-child(1):before {
    width: 180px;
    margin-left: -180px;
  }

  .two li:nth-child(1):after {
    width: 180px;
    margin-right: -180px;
  }

  .two li:nth-child(2) {
    background: #64DD17;
  }

  .two li:nth-child(2):before,
  .two li:nth-child(2):after {
    border-top-color: #64DD17;
  }

  .two li:nth-child(2):before {
    width: 150px;
    margin-left: -150px;
  }

  .two li:nth-child(2):after {
    width: 150px;
    margin-right: -150px;
  }

  .two li:nth-child(3) {
    background: #FFC107;
  }

  .two li:nth-child(3):before,
  .two li:nth-child(3):after {
    border-top-color: #FFC107;
  }

  .two li:nth-child(3):before {
    width: 120px;
    margin-left: -120px;
  }

  .two li:nth-child(3):after {
    width: 120px;
    margin-right: -120px;
  }

  .two li:nth-child(4) {
    background: #FB8C00;
  }

  .two li:nth-child(4):before,
  .two li:nth-child(4):after {
    border-top-color: #FB8C00;
  }

  .two li:nth-child(4):before {
    width: 90px;
    margin-left: -90px;
  }

  .two li:nth-child(4):after {
    width: 90px;
    margin-right: -90px;
  }

  .two li:nth-child(5) {
    background: #E53935;
  }

  .two li:nth-child(5):before,
  .two li:nth-child(5):after {
    border-top-color: #E53935;
  }

  .two li:nth-child(5):before {
    width: 60px;
    margin-left: -60px;
  }

  .two li:nth-child(5):after {
    width: 60px;
    margin-right: -60px;
  }

  .two li:nth-child(6) {
    background: #C62828;
  }

  .two li:nth-child(6):before,
  .two li:nth-child(6):after {
    border-top-color: #C62828;
  }

  .two li:nth-child(6):before {
    width: 30px;
    margin-left: -30px;
  }

  .two li:nth-child(6):after {
    width: 30px;
    margin-right: -30px;
  }

  ul.three {
    margin: 40px 170px;
    padding: 0;
    list-style: none;
    text-align: center;
  }

  .three .funnel-top {
    position: absolute;
    top: -7px;
    left: -180px;
    z-index: 20;
    width: 530px;
    height: 14px;
    background: #69F0AE;
    border-radius: 100%;
  }

  .three .funnel-bottom {
    position: absolute;
    bottom: -7px;
    left: -0px;
    z-index: 20;
    width: 100%;
    /* 150px */
    height: 16px;
    background: #B71C1C;
    border-radius: 100%;
  }

  .three li {
    font-size: 14px;
    line-height: 70px;
    height: 70px;
    width: 170px;
    position: relative;
    color: #ffffff;
    font-weight: bold;
  }

  .three li span {
    background: rgba(255, 255, 255, 0.3);
    padding: 5px 8px;
    border-radius: 4px;
    margin-left: 5px;
  }

  .three li:before {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0%;
    margin-left: -30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-left: 30px solid transparent;
  }

  .three li:after {
    content: "";
    position: absolute;
    z-index: 10;
    right: 0%;
    margin-left: 30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-right: 30px solid transparent;
  }

  .three li:nth-child(1) {
    background: #00C853;
  }

  .three li:nth-child(1):before,
  .three li:nth-child(1):after {
    border-top-color: #00C853;
  }

  .three li:nth-child(1):before {
    width: 180px;
    margin-left: -180px;
  }

  .three li:nth-child(1):after {
    width: 180px;
    margin-right: -180px;
  }

  .three li:nth-child(2) {
    background: #64DD17;
  }

  .three li:nth-child(2):before,
  .three li:nth-child(2):after {
    border-top-color: #64DD17;
  }

  .three li:nth-child(2):before {
    width: 150px;
    margin-left: -150px;
  }

  .three li:nth-child(2):after {
    width: 150px;
    margin-right: -150px;
  }

  .three li:nth-child(3) {
    background: #FFC107;
  }

  .three li:nth-child(3):before,
  .three li:nth-child(3):after {
    border-top-color: #FFC107;
  }

  .three li:nth-child(3):before {
    width: 120px;
    margin-left: -120px;
  }

  .three li:nth-child(3):after {
    width: 120px;
    margin-right: -120px;
  }

  .three li:nth-child(4) {
    background: #FB8C00;
  }

  .three li:nth-child(4):before,
  .three li:nth-child(4):after {
    border-top-color: #FB8C00;
  }

  .three li:nth-child(4):before {
    width: 90px;
    margin-left: -90px;
  }

  .three li:nth-child(4):after {
    width: 90px;
    margin-right: -90px;
  }

  .three li:nth-child(5) {
    background: #E53935;
  }

  .three li:nth-child(5):before,
  .three li:nth-child(5):after {
    border-top-color: #E53935;
  }

  .three li:nth-child(5):before {
    width: 60px;
    margin-left: -60px;
  }

  .three li:nth-child(5):after {
    width: 60px;
    margin-right: -60px;
  }

  .three li:nth-child(6) {
    background: #C62828;
  }

  .three li:nth-child(6):before,
  .three li:nth-child(6):after {
    border-top-color: #C62828;
  }

  .three li:nth-child(6):before {
    width: 30px;
    margin-left: -30px;
  }

  .three li:nth-child(6):after {
    width: 30px;
    margin-right: -30px;
  }

  ul.four {
    margin: 40px 278px;
    padding: 0;
    list-style: none;
    text-align: center;
  }

  .four .funnel-top {
    position: absolute;
    top: -7px;
    left: -199px;
    z-index: 20;
    width: 599px;
    height: 14px;
    background: #69F0AE;
    border-radius: 100%;
  }

  .four .funnel-bottom {
    position: absolute;
    bottom: -7px;
    left: -20px;
    z-index: 20;
    width: 240px;
    height: 16px;
    background: #B71C1C;
    border-radius: 100%;
  }

  .four li {
    font-size: 16px;
    line-height: 70px;
    height: 70px;
    width: 200px;
    position: relative;
    color: #ffffff;
    font-weight: bold;
  }

  .four li span {
    background: rgba(255, 255, 255, 0.3);
    padding: 5px 8px;
    border-radius: 4px;
    margin-left: 15px;
  }

  .four li:before {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0%;
    margin-left: -30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-left: 30px solid transparent;
  }

  .four li:after {
    content: "";
    position: absolute;
    z-index: 10;
    right: 0%;
    margin-left: 30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-right: 30px solid transparent;
  }

  .four li:nth-child(1) {
    background: #00C853;
  }

  .four li:nth-child(1):before,
  .four li:nth-child(1):after {
    border-top-color: #00C853;
  }

  .four li:nth-child(1):before {
    width: 200px;
    margin-left: -200px;
  }

  .four li:nth-child(1):after {
    width: 200px;
    margin-right: -200px;
  }

  .four li:nth-child(2) {
    background: #64DD17;
  }

  .four li:nth-child(2):before,
  .four li:nth-child(2):after {
    border-top-color: #64DD17;
  }

  .four li:nth-child(2):before {
    width: 170px;
    margin-left: -170px;
  }

  .four li:nth-child(2):after {
    width: 170px;
    margin-right: -170px;
  }

  .four li:nth-child(3) {
    background: #FFC107;
  }

  .four li:nth-child(3):before,
  .four li:nth-child(3):after {
    border-top-color: #FFC107;
  }

  .four li:nth-child(3):before {
    width: 140px;
    margin-left: -140px;
  }

  .four li:nth-child(3):after {
    width: 140px;
    margin-right: -140px;
  }

  .four li:nth-child(4) {
    background: #FB8C00;
  }

  .four li:nth-child(4):before,
  .four li:nth-child(4):after {
    border-top-color: #FB8C00;
  }

  .four li:nth-child(4):before {
    width: 110px;
    margin-left: -110px;
  }

  .four li:nth-child(4):after {
    width: 110px;
    margin-right: -110px;
  }

  .four li:nth-child(5) {
    background: #E53935;
  }

  .four li:nth-child(5):before,
  .four li:nth-child(5):after {
    border-top-color: #E53935;
  }

  .four li:nth-child(5):before {
    width: 80px;
    margin-left: -80px;
  }

  .four li:nth-child(5):after {
    width: 80px;
    margin-right: -80px;
  }

  .four li:nth-child(6) {
    background: #C62828;
  }

  .four li:nth-child(6):before,
  .four li:nth-child(6):after {
    border-top-color: #C62828;
  }

  .four li:nth-child(6):before {
    width: 50px;
    margin-left: -50px;
  }

  .four li:nth-child(6):after {
    width: 50px;
    margin-right: -50px;
  }

  ul.five {
    margin: 40px 278px;
    padding: 0;
    list-style: none;
    text-align: center;
  }

  .five .funnel-top {
    position: absolute;
    top: -7px;
    left: -199px;
    z-index: 20;
    width: 599px;
    height: 14px;
    background: #69F0AE;
    border-radius: 100%;
  }

  .five .funnel-bottom {
    position: absolute;
    bottom: -7px;
    left: -20px;
    z-index: 20;
    width: 240px;
    height: 16px;
    background: #B71C1C;
    border-radius: 100%;
  }

  .five li {
    font-size: 16px;
    line-height: 70px;
    height: 70px;
    width: 200px;
    position: relative;
    color: #ffffff;
    font-weight: bold;
  }

  .five li span {
    background: rgba(255, 255, 255, 0.3);
    padding: 5px 8px;
    border-radius: 4px;
    margin-left: 15px;
  }

  .five li:before {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0%;
    margin-left: -30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-left: 30px solid transparent;
  }

  .five li:after {
    content: "";
    position: absolute;
    z-index: 10;
    right: 0%;
    margin-left: 30px;
    width: 30px;
    border-top: 70px solid #ccc;
    border-right: 30px solid transparent;
  }

  .five li:nth-child(1) {
    background: #00C853;
  }

  .five li:nth-child(1):before,
  .five li:nth-child(1):after {
    border-top-color: #00C853;
  }

  .five li:nth-child(1):before {
    width: 200px;
    margin-left: -200px;
  }

  .five li:nth-child(1):after {
    width: 200px;
    margin-right: -200px;
  }

  .five li:nth-child(2) {
    background: #64DD17;
  }

  .five li:nth-child(2):before,
  .five li:nth-child(2):after {
    border-top-color: #64DD17;
  }

  .five li:nth-child(2):before {
    width: 170px;
    margin-left: -170px;
  }

  .five li:nth-child(2):after {
    width: 170px;
    margin-right: -170px;
  }

  .five li:nth-child(3) {
    background: #FFC107;
  }

  .five li:nth-child(3):before,
  .five li:nth-child(3):after {
    border-top-color: #FFC107;
  }

  .five li:nth-child(3):before {
    width: 140px;
    margin-left: -140px;
  }

  .five li:nth-child(3):after {
    width: 140px;
    margin-right: -140px;
  }

  .five li:nth-child(4) {
    background: #FB8C00;
  }

  .five li:nth-child(4):before,
  .five li:nth-child(4):after {
    border-top-color: #FB8C00;
  }

  .five li:nth-child(4):before {
    width: 110px;
    margin-left: -110px;
  }

  .five li:nth-child(4):after {
    width: 110px;
    margin-right: -110px;
  }

  .five li:nth-child(5) {
    background: #E53935;
  }

  .five li:nth-child(5):before,
  .five li:nth-child(5):after {
    border-top-color: #E53935;
  }

  .five li:nth-child(5):before {
    width: 80px;
    margin-left: -80px;
  }

  .five li:nth-child(5):after {
    width: 80px;
    margin-right: -80px;
  }

  .five li:nth-child(6) {
    background: #C62828;
  }

  .five li:nth-child(6):before,
  .five li:nth-child(6):after {
    border-top-color: #C62828;
  }

  .five li:nth-child(6):before {
    width: 50px;
    margin-left: -50px;
  }

  .five li:nth-child(6):after {
    width: 50px;
    margin-right: -50px;
  }

  /* -------------------------------- */
</style>
<?php
try {
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oReporteCredito = new ReporteCredito();

  $fecha_hoy = date('Y-m-d');

  // $viewSQL = ''; $viewSQL .= $result['sql']; '.$viewSQL.'
  $tipo_cambio = $_POST['p_tipo_cambio'];

  $resultado_estado = 0;
  $anho = date('Y');
  $mes  = date('m');

  $iniciomes_01 = 0;
  $porcent_inciomes_tramo01 = 0;
  $iniciomes_12 = 0;
  $porcent_inciomes_tramo02 = 0;
  $iniciomes_23 = 0;
  $porcent_inciomes_tramo03 = 0;
  $iniciomes_34 = 0;
  $porcent_inciomes_tramo04 = 0;
  $iniciomes_45 = 0;
  $porcent_inciomes_tramo05 = 0;
  $iniciomes_56 = 0;
  $porcent_inciomes_tramo06 = 0;
  $iniciomes_67 = 0;
  $porcent_inciomes_tramo07 = 0;

  $total_inciomes = 0;
  $total_finmes = 0;

  $result = $oReporteCredito->mostrarUno_tb_reportecreditomejorado_embudo($mes, $anho);
  $resultado_estado = $result['estado'];
  if ($result['estado'] == 1) {
    $iniciomes_01 = floatval($result['data']['inicio_01']);
    $iniciomes_12 = floatval($result['data']['inicio_12']);
    $iniciomes_23 = floatval($result['data']['inicio_23']);
    $iniciomes_34 = floatval($result['data']['inicio_34']);
    $iniciomes_45 = floatval($result['data']['inicio_45']);
    $iniciomes_56 = floatval($result['data']['inicio_56']);
    $iniciomes_67 = floatval($result['data']['inicio_67']);

    $total_inciomes = formato_numero($iniciomes_01 + $iniciomes_12 + $iniciomes_23 + $iniciomes_34 + $iniciomes_45 + $iniciomes_56 + $iniciomes_67);

    if ($total_inciomes > 0) {
      $porcent_inciomes_tramo01 = formato_numero((($iniciomes_01 / $total_inciomes) * 100));
      $porcent_inciomes_tramo02 = formato_numero((($iniciomes_12 / $total_inciomes) * 100));
      $porcent_inciomes_tramo03 = formato_numero((($iniciomes_23 / $total_inciomes) * 100));
      $porcent_inciomes_tramo04 = formato_numero((($iniciomes_34 / $total_inciomes) * 100));
      $porcent_inciomes_tramo05 = formato_numero((($iniciomes_45 / $total_inciomes) * 100));
      $porcent_inciomes_tramo06 = formato_numero((($iniciomes_56 / $total_inciomes) * 100));
      $porcent_inciomes_tramo07 = formato_numero((($iniciomes_67 / $total_inciomes) * 100));
    } else {
      $total_inciomes = formato_numero(0);
    }
  }
  $result = null;

  $data_deuda_actual_tramo01 = '';
  $sum_deuda_actual_tramo01 = 0;
  $porcent_deuda_actual_tramo01 = 0;
  $data_deuda_actual_tramo02 = '';
  $sum_deuda_actual_tramo02 = 0;
  $porcent_deuda_actual_tramo02 = 0;
  $data_deuda_actual_tramo03 = '';
  $sum_deuda_actual_tramo03 = 0;
  $porcent_deuda_actual_tramo03 = 0;
  $data_deuda_actual_tramo04 = '';
  $sum_deuda_actual_tramo04 = 0;
  $porcent_deuda_actual_tramo04 = 0;
  $data_deuda_actual_tramo05 = '';
  $sum_deuda_actual_tramo05 = 0;
  $porcent_deuda_actual_tramo05 = 0;
  $data_deuda_actual_tramo06 = '';
  $sum_deuda_actual_tramo06 = 0;
  $porcent_deuda_actual_tramo06 = 0;
  $data_deuda_actual_tramo07 = '';
  $sum_deuda_actual_tramo07 = 0;
  $porcent_deuda_actual_tramo07 = 0;

  $total_deuda_actual = 0;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 1, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo01 += formato_numero($col20);
      $data_deuda_actual_tramo01 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 2, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo02 += formato_numero($col20);
      $data_deuda_actual_tramo02 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 3, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo03 += formato_numero($col20);
      $data_deuda_actual_tramo03 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 4, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo04 += formato_numero($col20);
      $data_deuda_actual_tramo04 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 5, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo05 += formato_numero($col20);
      $data_deuda_actual_tramo05 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 6, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo06 += formato_numero($col20);
      $data_deuda_actual_tramo06 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 7, 0);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_deuda_actual_tramo07 += formato_numero($col20);
      $data_deuda_actual_tramo07 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $total_deuda_actual = formato_numero($sum_deuda_actual_tramo01 + $sum_deuda_actual_tramo02 + $sum_deuda_actual_tramo03 + $sum_deuda_actual_tramo04 + $sum_deuda_actual_tramo05 + $sum_deuda_actual_tramo06 + $sum_deuda_actual_tramo07);

  if ($total_deuda_actual > 0) {
    $porcent_deuda_actual_tramo01 = formato_numero((($sum_deuda_actual_tramo01 / $total_deuda_actual) * 100));
    $porcent_deuda_actual_tramo02 = formato_numero((($sum_deuda_actual_tramo02 / $total_deuda_actual) * 100));
    $porcent_deuda_actual_tramo03 = formato_numero((($sum_deuda_actual_tramo03 / $total_deuda_actual) * 100));
    $porcent_deuda_actual_tramo04 = formato_numero((($sum_deuda_actual_tramo04 / $total_deuda_actual) * 100));
    $porcent_deuda_actual_tramo05 = formato_numero((($sum_deuda_actual_tramo05 / $total_deuda_actual) * 100));
    $porcent_deuda_actual_tramo06 = formato_numero((($sum_deuda_actual_tramo06 / $total_deuda_actual) * 100));
    $porcent_deuda_actual_tramo07 = formato_numero((($sum_deuda_actual_tramo07 / $total_deuda_actual) * 100));
  } else {
    $total_deuda_actual = formato_numero(0);
  }

  $data_singestion_tramo01 = '';
  $sum_singestion_tramo01 = 0;
  $porcent_singestion_tramo01 = 0;
  $data_singestion_tramo02 = '';
  $sum_singestion_tramo02 = 0;
  $porcent_singestion_tramo02 = 0;
  $data_singestion_tramo03 = '';
  $sum_singestion_tramo03 = 0;
  $porcent_singestion_tramo03 = 0;
  $data_singestion_tramo04 = '';
  $sum_singestion_tramo04 = 0;
  $porcent_singestion_tramo04 = 0;
  $data_singestion_tramo05 = '';
  $sum_singestion_tramo05 = 0;
  $porcent_singestion_tramo05 = 0;
  $data_singestion_tramo06 = '';
  $sum_singestion_tramo06 = 0;
  $porcent_singestion_tramo06 = 0;
  $data_singestion_tramo07 = '';
  $sum_singestion_tramo07 = 0;
  $porcent_singestion_tramo07 = 0;

  $total_singestion = 0;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 1, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo01 += formato_numero($col20);
      $data_singestion_tramo01 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 2, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo02 += formato_numero($col20);
      $data_singestion_tramo02 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 3, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo03 += formato_numero($col20);
      $data_singestion_tramo03 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 4, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo04 += formato_numero($col20);
      $data_singestion_tramo04 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 5, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo05 += formato_numero($col20);
      $data_singestion_tramo05 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 6, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo06 += formato_numero($col20);
      $data_singestion_tramo06 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;
  
  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy, 7, 1);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $col01 = $value['temp_cobranza_id'];
      $col06 = $value['estado_credito'];
      $col11 = $value['representante_nom'];
      $col16 = $value['restante_soles'];
      $col21 = $value['cuota_pago'];
      $col26 = $value['inactividad'];
      $col02 = $value['cuotadetalle_id'];
      $col07 = $value['cliente_id'];
      $col12 = $value['cliente_cel'];
      $col17 = $value['cuota_actual'];
      $col22 = $value['cuota_fecha'];
      $col27 = $value['ultimo_pago'];
      $col03 = $value['credito_id'];
      $col08 = $value['cliente_dni'];
      $col13 = $value['moneda'];
      $col18 = $value['cuota_estado'];
      $col23 = $value['fecha_facturacion'];
      $col28 = $value['deuda_acumulada'];
      $col04 = $value['credito_tipo'];
      $col09 = $value['cliente_nom'];
      $col14 = $value['capital_desembolsado'];
      $col19 = $value['cuota_monto'];
      $col24 = $value['ultimopago_fecha'];
      $col05 = $value['credito_subtipo'];
      $col10 = $value['representante_dni'];
      $col15 = $value['capital_restante'];
      $col20 = $value['cuota_soles'];
      $col25 = $value['dias_atraso'];

      $sum_singestion_tramo07 += formato_numero($col20);
      $data_singestion_tramo07 .= '<tr> <td class="text-center">' . $col03 . '</td> <td>' . $col09 . '</td> <td class="text-center">' . $col26 . '</td> <td class="text-center">' . $col17 . '</td> <td class="text-right">' . mostrar_moneda($col20) . '</td> <td class="text-center"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-phone"></i></button></td> </tr>';
    }
  }
  $result = null;

  $total_singestion = formato_numero($sum_singestion_tramo01 + $sum_singestion_tramo02 + $sum_singestion_tramo03 + $sum_singestion_tramo04 + $sum_singestion_tramo05 + $sum_singestion_tramo06 + $sum_singestion_tramo07);

  if ($total_singestion > 0) {
    $porcent_singestion_tramo01 = formato_numero((($sum_singestion_tramo01 / $total_singestion) * 100));
    $porcent_singestion_tramo02 = formato_numero((($sum_singestion_tramo02 / $total_singestion) * 100));
    $porcent_singestion_tramo03 = formato_numero((($sum_singestion_tramo03 / $total_singestion) * 100));
    $porcent_singestion_tramo04 = formato_numero((($sum_singestion_tramo04 / $total_singestion) * 100));
    $porcent_singestion_tramo05 = formato_numero((($sum_singestion_tramo05 / $total_singestion) * 100));
    $porcent_singestion_tramo06 = formato_numero((($sum_singestion_tramo06 / $total_singestion) * 100));
    $porcent_singestion_tramo07 = formato_numero((($sum_singestion_tramo07 / $total_singestion) * 100));
  } else {
    $total_singestion = formato_numero(0);
  }

  echo
  '<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" style="font-family: cambria;font-weight: bold">
      <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" id="panel1">ACTUAL</a></li>
      <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" id="panel2">COMPARAR</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="container-fluid">
          <div class="row mt-4 mb-4">
            <div class="col-lg-4 col-md-12">
              <div class="container-fluid mb-4">
                <div class="row">
                  <div class="col-xs-1 text-center">
                    <div style="margin-top: 75px;"></div>
                    <div class="lateral-item">0</div>
                    <div class="lateral-item">1</div>
                    <div class="lateral-item">2</div>
                    <div class="lateral-item">3</div>
                    <div class="lateral-item">4</div>
                    <div class="lateral-item">5</div>
                    <div class="lateral-item">6</div>
                    <div class="lateral-item">7</div>
                  </div>
                  <div class="col-xs-11">
                    <div class="funnel leads estimated">
                      <h2 class="center-text"><span class="title-teal-with-bakground rounded">INICIO DEL MES</span></h2>
                      <ul class="one">
                        <li>
                          <div class="funnel-top"></div>
                          ' . mostrar_moneda($iniciomes_01) . ' <span>' . $porcent_inciomes_tramo01 . '</span> &nbsp;%
                        </li>
                        <li>
                          ' . mostrar_moneda($iniciomes_12) . ' <span>' . $porcent_inciomes_tramo02 . '</span> &nbsp;%
                        </li>
                        <li>
                          ' . mostrar_moneda($iniciomes_23) . ' <span>' . $porcent_inciomes_tramo03 . '</span> &nbsp;%
                        </li>
                        <li>
                          ' . mostrar_moneda($iniciomes_34) . ' <span>' . $porcent_inciomes_tramo04 . '</span> &nbsp;%
                        </li>
                        <li>
                          ' . mostrar_moneda($iniciomes_45) . ' <span>' . $porcent_inciomes_tramo05 . '</span> &nbsp;%
                        </li>
                        <li>
                          <div class="funnel-bottom"></div>
                          ' . mostrar_moneda($iniciomes_56) . ' <span>' . $porcent_inciomes_tramo06 . '</span> &nbsp;%
                        </li>
                      </ul>
                      <div class="text-center"> &nbsp; &nbsp;
                        <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">' . mostrar_moneda($iniciomes_67) . '</span> &nbsp;
                        <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">' . $porcent_inciomes_tramo07 . ' %</span>
                      </div>
                    </div>
                    <hr>
                    <div class="mt-4 text-center">
                      <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                      <span class="badge bg-blue p-4" style="font-size: 18px;">S/ ' . mostrar_moneda($total_inciomes) . '</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12">
              <div class="container-fluid mb-4">
                <div class="row">
                  <div class="col-xs-1 text-center">
                    <div style="margin-top: 75px;"></div>
                    <div class="lateral-item">0</div>
                    <div class="lateral-item">1</div>
                    <div class="lateral-item">2</div>
                    <div class="lateral-item">3</div>
                    <div class="lateral-item">4</div>
                    <div class="lateral-item">5</div>
                    <div class="lateral-item">6</div>
                    <div class="lateral-item">7</div>
                  </div>
                  <div class="col-xs-11">
                    <div class="funnel leads estimated">
                      <h2 class="center-text"><span class="title-teal-with-bakground rounded">DEUDA ACTUAL</span></h2>
                      <ul class="two">
                        <li>
                          <div class="funnel-top"></div>
                          ' . mostrar_moneda($sum_deuda_actual_tramo01) . ' <span>' . $porcent_deuda_actual_tramo01 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(2,1,0,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_deuda_actual_tramo02) . ' <span>' . $porcent_deuda_actual_tramo02 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(2,2,0,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_deuda_actual_tramo03) . ' <span>' . $porcent_deuda_actual_tramo03 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(2,3,0,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_deuda_actual_tramo04) . ' <span>' . $porcent_deuda_actual_tramo04 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(2,4,0,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_deuda_actual_tramo05) . ' <span>' . $porcent_deuda_actual_tramo05 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(2,5,0,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          <div class="funnel-bottom"></div>
                          ' . mostrar_moneda($sum_deuda_actual_tramo06) . ' <span>' . $porcent_deuda_actual_tramo06 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(2,6,0,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                      </ul>
                      <div class="text-center"> &nbsp; &nbsp;
                        <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">' . mostrar_moneda($sum_deuda_actual_tramo07) . '</span> &nbsp;
                        <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">' . $porcent_deuda_actual_tramo07 . ' %</span> &nbsp;
                        <a type="button" onclick="listar_fragment_embudo(2,7,0,0)" class="btn btn-outline-new-boton-rounded btn-sm"><i class="fa fa-info-circle"></i></a>
                      </div>
                    </div>
                    <hr>
                    <div class="mt-4 text-center">
                      <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                      <span class="badge bg-blue p-4" style="font-size: 18px;">S/ ' . mostrar_moneda($total_deuda_actual) . '</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12">
              <div class="container-fluid mb-4">
                <div class="row">
                  <div class="col-xs-1 text-center">
                    <div style="margin-top: 75px;"></div>
                    <div class="lateral-item">0</div>
                    <div class="lateral-item">1</div>
                    <div class="lateral-item">2</div>
                    <div class="lateral-item">3</div>
                    <div class="lateral-item">4</div>
                    <div class="lateral-item">5</div>
                    <div class="lateral-item">6</div>
                    <div class="lateral-item">7</div>
                  </div>
                  <div class="col-xs-11">
                    <div class="funnel leads estimated">
                      <h2 class="center-text"><span class="title-teal-with-bakground rounded">SIN GESTIÓN</span></h2>
                      <ul class="three">
                        <li>
                          <div class="funnel-top"></div>
                          ' . mostrar_moneda($sum_singestion_tramo01) . ' <span>' . $porcent_singestion_tramo01 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(3,1,1,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_singestion_tramo02) . ' <span>' . $porcent_singestion_tramo02 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(3,2,1,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_singestion_tramo03) . ' <span>' . $porcent_singestion_tramo03 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(3,3,1,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_singestion_tramo04) . ' <span>' . $porcent_singestion_tramo04 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(3,4,1,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          ' . mostrar_moneda($sum_singestion_tramo05) . ' <span>' . $porcent_singestion_tramo05 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(3,5,1,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                        <li>
                          <div class="funnel-bottom"></div>
                          ' . mostrar_moneda($sum_singestion_tramo06) . ' <span>' . $porcent_singestion_tramo06 . '</span> &nbsp;% <a type="button" onclick="listar_fragment_embudo(3,6,1,0)" class="btn btn-outline-new-boton-rounded btn-xs mt-4 pull-right"><i class="fa fa-info-circle"></i></a>
                        </li>
                      </ul>
                      <div class="text-center"> &nbsp; &nbsp;
                        <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">' . mostrar_moneda($sum_singestion_tramo07) . '</span> &nbsp;
                        <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">' . $porcent_singestion_tramo07 . ' %</span> &nbsp;
                        <a type="button" onclick="listar_fragment_embudo(3,7,1,0)" class="btn btn-outline-new-boton-rounded btn-sm"><i class="fa fa-info-circle"></i></a>
                      </div>
                    </div>
                    <hr>
                    <div class="mt-4 text-center">
                      <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                      <span class="badge bg-blue p-4" style="font-size: 18px;">S/ ' . mostrar_moneda($total_singestion) . '</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
        <div class="container-fluid">
          <div class="row mt-4 mb-4">
            <div class="col-lg-5 col-xs-12"> </div>
            <div class="col-lg-4 col-xs-12">
              <div class="row mb-4">
                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-8">
                  <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
                    <h5 style="margin-left: 10px;" class="mb-4"><b>AÑO</b></h5>
                    <div class="container-fluid">
                      <select name="cmb_comparar_anio" id="cmb_comparar_anio" class="form-control">
                        ' . devuelve_option_anios(date('Y'), 4) . '
                      </select>
                      <p></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-4 col-xs-8">
                  <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
                    <h5 style="margin-left: 10px;" class="mb-4"><b>MES</b></h5>
                    <div class="container-fluid">
                      <select name="cmb_comparar_mes" id="cmb_comparar_mes" class="form-control">
                        <option value="">Seleccione</option>
                        <option value="1">ENERO</option>
                        <option value="2">FEBRERO</option>
                        <option value="3">MARZO</option>
                        <option value="4">ABRIL</option>
                        <option value="5">MAYO</option>
                        <option value="6">JUNIO</option>
                        <option value="7">JULIO</option>
                        <option value="8">AGOSTO</option>
                        <option value="9">SETIEMBRE</option>
                        <option value="10">OCTUBRE</option>
                        <option value="11">NOVIEMBRE</option>
                        <option value="12">DICIEMBRE</option>
                      </select>
                      <p></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2 col-md-1 col-sm-2 col-xs-2 p-4 text-center"> <br>
                  <button type="button" class="btn btn-primary mt-4" onclick="consultar_datos_comparacion_fragment_embudo()"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>
          <div id="lista_fragment_comparacion" class="row mb-4">
            <div class="col-lg-6 col-md-12"> <br>
              <div class="container-fluid mt-4 mb-4">
                <div class="row">
                  <div class="col-xs-1 text-center">
                    <div style="margin-top: 75px;"></div>
                    <div class="lateral-item-02">0</div>
                    <div class="lateral-item-02">1</div>
                    <div class="lateral-item-02">2</div>
                    <div class="lateral-item-02">3</div>
                    <div class="lateral-item-02">4</div>
                    <div class="lateral-item-02">5</div>
                    <div class="lateral-item-02">6</div>
                    <div class="lateral-item-02">7</div>
                  </div>
                  <div class="col-xs-11">
                    <div class="funnel leads estimated">
                      <h2 class="center-text"><span class="title-teal-with-bakground rounded">INICIO DEL MES</span></h2>
                      <ul class="four">
                        <li>
                          <div class="funnel-top"></div>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          <div class="funnel-bottom"></div>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                      </ul>
                      <div class="text-center"> &nbsp; &nbsp;
                        <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">0.00</span> &nbsp;
                        <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">0 %</span> &nbsp;
                        <a type="button" onclick="listar_fragment_embudo(3,7,1,0)" class="btn btn-outline-new-boton-rounded btn-sm"><i class="fa fa-info-circle"></i></a>
                      </div>
                    </div>
                    <hr>
                    <div class="mt-4 text-center">
                      <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                      <span class="badge bg-blue p-4" style="font-size: 18px;">S/ 0.00</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12"> <br>
              <div class="container-fluid mt-4 mb-4">
                <div class="row">
                  <div class="col-xs-1 text-center">
                    <div style="margin-top: 75px;"></div>
                    <div class="lateral-item-02">0</div>
                    <div class="lateral-item-02">1</div>
                    <div class="lateral-item-02">2</div>
                    <div class="lateral-item-02">3</div>
                    <div class="lateral-item-02">4</div>
                    <div class="lateral-item-02">5</div>
                    <div class="lateral-item-02">6</div>
                    <div class="lateral-item-02">7</div>
                  </div>
                  <div class="col-xs-11">
                    <div class="funnel leads estimated">
                      <h2 class="center-text"><span class="title-teal-with-bakground rounded">FINAL DEL MES</span></h2>
                      <ul class="five">
                        <li>
                          <div class="funnel-top"></div>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                        <li>
                          <div class="funnel-bottom"></div>
                          0.00 <span>0</span> &nbsp;%
                        </li>
                      </ul>
                      <div class="text-center"> &nbsp; &nbsp;
                        <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">0.00</span> &nbsp;
                        <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">0 %</span> &nbsp;
                        <a type="button" onclick="listar_fragment_embudo(3,7,1,0)" class="btn btn-outline-new-boton-rounded btn-sm"><i class="fa fa-info-circle"></i></a>
                      </div>
                    </div>
                    <hr>
                    <div class="mt-4 text-center">
                      <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                      <span class="badge bg-blue p-4" style="font-size: 18px;">S/ 0.00</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  ';

  // echo 
    // ' <button type=="button" class="btn btn-info mb-4" onclick="listar_segmentacion_embudo()">Actualizar</button>
    //   <div class="row">
    //     <div class="col-md-4">

    //       <div class="box box-default mb-4">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 0 - 1 </h3>
    //           <div class="box-tools pull-right"> '.$sum_inciomes_tramo01.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_inciomes_tramo01.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_inciomes_tramo01).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-default mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 1 - 2 </h3>
    //           <div class="box-tools pull-right"> '.$sum_inciomes_tramo02.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_inciomes_tramo02.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_inciomes_tramo02).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-default mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 2 - 3 </h3>
    //           <div class="box-tools pull-right"> '.$sum_inciomes_tramo03.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_inciomes_tramo03.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_inciomes_tramo03).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-default mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 3 - 4 </h3>
    //           <div class="box-tools pull-right"> '.$sum_inciomes_tramo04.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_inciomes_tramo04.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_inciomes_tramo04).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-default mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 4 - 5 </h3>
    //           <div class="box-tools pull-right"> '.$sum_inciomes_tramo05.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_inciomes_tramo05.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_inciomes_tramo05).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-default mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 5 - 6 </h3>
    //           <div class="box-tools pull-right"> '.$sum_inciomes_tramo06.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_inciomes_tramo06.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_inciomes_tramo06).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //     </div>
    //     <div class="col-md-4">

    //       <div class="box box-primary mb-4">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 0 - 1 </h3>
    //           <div class="box-tools pull-right"> '.$sum_deuda_actual_tramo01.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_deuda_actual_tramo01.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_deuda_actual_tramo01).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-primary mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 1 - 2 </h3>
    //           <div class="box-tools pull-right"> '.$sum_deuda_actual_tramo02.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_deuda_actual_tramo02.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_deuda_actual_tramo02).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-primary mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 2 - 3 </h3>
    //           <div class="box-tools pull-right"> '.$sum_deuda_actual_tramo03.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_deuda_actual_tramo03.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_deuda_actual_tramo03).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-primary mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 3 - 4 </h3>
    //           <div class="box-tools pull-right"> '.$sum_deuda_actual_tramo04.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_deuda_actual_tramo04.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_deuda_actual_tramo04).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-primary mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 4 - 5 </h3>
    //           <div class="box-tools pull-right"> '.$sum_deuda_actual_tramo05.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_deuda_actual_tramo05.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_deuda_actual_tramo05).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-primary mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 5 - 6 </h3>
    //           <div class="box-tools pull-right"> '.$sum_deuda_actual_tramo06.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_deuda_actual_tramo06.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_deuda_actual_tramo06).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //     </div>
    //     <div class="col-md-4">

    //       <div class="box box-warning mb-4">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 0 - 1 </h3>
    //           <div class="box-tools pull-right"> '.$sum_singestion_tramo01.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_singestion_tramo01.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_singestion_tramo01).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-warning mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 1 - 2 </h3>
    //           <div class="box-tools pull-right"> '.$sum_singestion_tramo02.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_singestion_tramo02.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_singestion_tramo02).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-warning mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 2 - 3 </h3>
    //           <div class="box-tools pull-right"> '.$sum_singestion_tramo03.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_singestion_tramo03.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_singestion_tramo03).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-warning mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 3 - 4 </h3>
    //           <div class="box-tools pull-right"> '.$sum_singestion_tramo04.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_singestion_tramo04.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_singestion_tramo04).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-warning mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 4 - 5 </h3>
    //           <div class="box-tools pull-right"> '.$sum_singestion_tramo05.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_singestion_tramo05.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_singestion_tramo05).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //       <div class="box box-warning mb-4 collapsed-box">
    //         <div class="box-header with-border">
    //           <h3 class="box-title text-blue"> 5 - 6 </h3>
    //           <div class="box-tools pull-right"> '.$sum_singestion_tramo06.'
    //             <button type="button" class="btn btn-box-tool" data-widget="collapse">
    //               <i class="fa fa-plus"></i>
    //             </button>
    //           </div>
    //         </div>
    //         <div class="box-body"> 
    //           <div class="table-responsive">
    //             <table class="table table-striped">
    //               <thead>
    //                 <tr align="center"><td>ID Credito</td> <td>Cliente</td> <td>Inactvidad</td> <td>Numero Cuota</td> <td>Monto</td> <td>LLamar</td> </tr>
    //               </thead>
    //               <tbody>
    //                 '.$data_singestion_tramo06.'
    //               </tbody>
    //               <tfoot>
    //                 <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td class="text-right"><span class="badge bg-blue">'.mostrar_moneda($sum_singestion_tramo06).'</span></td> <td>&nbsp;</td>
    //               </tfoot>
    //             </table>
    //           </div>
    //         </div>
    //       </div>

    //     </div>
    //   </div>
  // ';
} catch (Exception $exc) {
  header("HTTP/1.1 500");
  echo $exc->getMessage();
  exit;
}
?>