<?php
require_once('../../core/usuario_sesion.php');
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$credito_id = $_POST['credito_id'];
$creditotipo = $_POST['creditotipo'];
$titulo = 'Deuda Acumulada por Crédito';

?>
<input type="hidden" id="deuda_credito_id" value="<?php echo $credito_id;?>"/>
<input type="hidden" id="deuda_creditotipo" value="<?php echo $creditotipo;?>"/>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_deuda_acumulada" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo; ?></h4>
      </div>

      <div class="modal-body">
        <div id="chart_div"></div>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/reportecreditomejorado/deuda_acumulada.js?ver=2'; ?>"></script>