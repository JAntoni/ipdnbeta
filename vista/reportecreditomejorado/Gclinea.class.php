<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Gclinea extends Conexion{

    function insertar_gclinea($gclinea_usureg, $credito_tip, $credito_id, $gclinea_det){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_gclinea(tb_gclinea_usureg, tb_credito_tip, tb_credito_id, tb_gclinea_det)
          VALUES (:gclinea_usureg, :credito_tip, :credito_id, :gclinea_det)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gclinea_usureg", $gclinea_usureg, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_tip", $credito_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":gclinea_det", $gclinea_det, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }

    function listar_gclinea($credito_tip, $credito_id){
      try {
        $sql = "SELECT gc.*, tb_usuario_nom, tb_usuario_fot FROM tb_gclinea gc 
          INNER JOIN tb_usuario usu on usu.tb_usuario_id = gc.tb_gclinea_usureg
          WHERE tb_credito_tip =:credito_tip AND tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_tip", $credito_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>