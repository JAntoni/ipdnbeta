<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">

		<?php require_once('reportecreditomejorado_filtro.php'); ?>

		<div class="callout callout-info" id="reportecredito_mensaje_tbl" style="display: none;">
			<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Datos ...</span></h4>
		</div>

		<div class="row mb-4">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h4 class="box-title text-red">Pendiente</h4>
						<h4 id="lbltot07" class="text-red pull-right"></h4>
					</div>
					<!-- <div class="box-body">
						<ul>
							<li class="mt-4">SOLES: <h4 id="lbltot05" class="pull-right"></h4></li>  <br><br>
							<li>DÓLARES a SOLES <h4 id="lbltot06" class="pull-right"></h4></li>
						</ul>
						</div>
					-->
				</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h4 class="box-title text-aqua">Pago Pacial</h4>
						<h4 id="lbltot10" class="text-aqua pull-right"></h4>
					</div>
					<!-- <div class="box-body">
						<ul>
							<li class="mt-4">SOLES: <h4 id="lbltot08" class="pull-right"></h4></li>  <br><br>
							<li>DÓLARES a SOLES <h4 id="lbltot09" class="pull-right"></h4></li>
						</ul>
						</div>
					-->
				</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h4 class="box-title text-green">Cancelado</h4>
						<h4 id="lbltot13" class="text-green pull-right"></h4>
					</div>
					<!-- <div class="box-body">
						<ul>
							<li class="mt-4">SOLES: <h4 id="lbltot11" class="pull-right"></h4></li>  <br><br>
							<li>DÓLARES a SOLES <h4 id="lbltot12" class="pull-right"></h4></li>
						</ul>
						</div>
					-->
				</div>
			</div>
		</div>

		<!-- COMPORTAMIENTO DE PAGO DE CLIENTES -->
		<div class="box box-primary class_comportamiento">
			<div class="box-header with-border">
				<h3 class="box-title text-blue">COMPORTAMIENTO DE PAGO DE LOS CLIENTES</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div id="div_listar" class="dataTables_wrapper form-inline dt-bootstrap"></div> <!-- table-responsive -->
			</div>
		</div>

		<?php
		$fecha = new DateTime();
		$fecha->modify('last day of this month');

		$chart_fec1 = date('01-m-Y');
		$chart_fec2 = $fecha->format('d-m-Y');
		?>

		<!-- GRÁFICO DEUDA ACUMULADA POR DIA -->
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title text-green">DEUDA ACUMULADA DIARIA</h3>
				<form id="form_chart_filtro" class="form-inline pull-right" role="form" style="margin-right: 30px;">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker1'>
							<input type='text' class="form-control input-sm" name="txt_deudadiaria_fec1" id="txt_deudadiaria_fec1" value="<?php echo $chart_fec1; ?>" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<div class='input-group date' id='datetimepicker2'>
							<input type='text' class="form-control input-sm" name="txt_deudadiaria_fec2" id="txt_deudadiaria_fec2" value="<?php echo $chart_fec2; ?>" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group" style="margin-left:9px">
						<a href="javascript:void(0)" onClick="valores_chart_deudadiaria()" class="btn btn-success btn-sm">Buscar</a>
					</div>
				</form>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div id="chart-deudadiaria" style="height: 400px;"> </div>
			</div>
		</div>

		<!-- FILTROS -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">NUEVO REPORTE</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div id="div_segment_embudo"> </div>
			</div>
		</div>

		<!-- REPORTE DE ASISTENCIA VEHICULAR-->
		<div id="div_asiveh_tabla">
			<?php //require_once('reportecredito_asiveh_tabla.php');
			?>
		</div>

		<!-- REPORTE FACTURADO GARANTIA VEHICULAR-->
		<div id="div_garveh_tabla">
			<?php //require_once('reportecredito_garveh_tabla.php');
			?>
		</div>

		<!-- REPORTE FACTURADO GARANTIA HIPOTECARIO-->
		<div id="div_hipo_tabla">
			<?php //require_once('reportecredito_hipo_tabla.php');
			?>
		</div>

		<div id="div_modal_opcionesgc_form"> <!-- INCLUIMOS EL MODAL PARA EL REGISthO DEL TIPO--> </div>

		<div id="div_modal_opcionesgc_timeline"> </div>

		<div id="div_clientenota_form"> </div>

		<div id="div_modal_cliente_contacto"> </div>

		<div id="div_modal_deuda_acumulada"> </div>

		<div class="modal fade" id="modal_clientes_mapa" tabindex="-1">
			<div class="modal-dialog modal-lg modal-simple" style="width: 80%; height: 90%;">
				<div class="modal-content p-3 p-md-5">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">x</span>
						</button>
						<h4 class="modal-title">UBICACION</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<a class="btn btn-danger" href="javascript:void(0)" onclick="borrar_ruta()" title="BORRAR ORDEN" style="margin-left: 10px;"> BORRAR ORDEN <i class="fa fa-ban"></i></a>
							</div>
							<div class="col-md-4">
								<a class="btn btn-success" href="javascript:void(0)" onclick="generarURLGoogleMaps()" title="GENERAR RUTA" style="margin-left: 10px;"> GENERAR RUTA <i class="fa fa-car"></i></a>
							</div>
							<div class="col-md-4">
							</div>
						</div>
						<br>
						<div id="map" style="width: 100%; height: 650px"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal_cronograma_cuotas_cliente" tabindex="-1">
			<div class="modal-dialog modal-lg modal-simple" style="width: 80%; height: 90%;">
				<div class="modal-content p-3 p-md-5">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">x</span>
						</button>
						<h4 class="modal-title">PAGO DE CUOTAS</h4>
					</div>
					<div class="modal-body">
						<div id="div_vencimiento_tabla"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Cerrar
						</button>
					</div>
				</div>
			</div>
		</div>

		<div id="div_info_credito"> </div>

		<div class="modal fade" id="modal_info-grafig-embudo01" tabindex="-1" data-backdrop="static">
			<div class="modal-dialog modal-simple modal-lg" style="float: right;">
				<div class="modal-content p-3 p-md-5">
					<!-- <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">x</span>
						</button>
						<h4 class="modal-title">DETALLE</h4>
					</div> -->
					<div class="modal-body">
						<div id="lista_fragment"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
							Cerrar
						</button>
					</div>
				</div>
			</div>
		</div>

		<div id="div_modal_opcionesgc_asignar_form"> </div>

		<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>

	</section>
	<!-- /.content -->
</div>

<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiNyafdNBeGQnOIeP7W8dIJ13PS6C2Eb0&libraries=places&region=PE&language=es" defer></script>