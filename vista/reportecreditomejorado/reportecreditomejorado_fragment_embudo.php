<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oReporteCredito = new ReporteCredito();

  $fecha_hoy = date('Y-m-d');

  $p_numbergrafic = intval($_POST['p_numbergrafic']);
  $numberfragment = intval($_POST['p_numberfragment']);
  $singestion     = intval($_POST['p_singestion']);
  $iniciomes      = intval($_POST['p_iniciomes']);

  $data = ''; $suma = 0;

  if($p_numbergrafic==1)
    $descrip_title = '<b>INCIO DE MES: &nbsp;</b>';
  elseif($p_numbergrafic==2)
    $descrip_title = '<b>DEUDA ACTUAL: &nbsp;</b>';
  elseif($p_numbergrafic==3)
    $descrip_title = '<b>SIN GESTIÓN: &nbsp;</b>';
  else
    $descrip_title .= 'SIN AGINAR';

  if($numberfragment==1)
    $descrip_title .= 'Rango de 0 - 1';
  elseif($numberfragment==2)
    $descrip_title .= 'Rango de 1 - 2';
  elseif($numberfragment==3)
    $descrip_title .= 'Rango de 2 - 3';
  elseif($numberfragment==4)
    $descrip_title .= 'Rango de 3 - 4';
  elseif($numberfragment==5)
    $descrip_title .= 'Rango de 4 - 5';
  elseif($numberfragment==6)
    $descrip_title .= 'Rango de 5 - 6';
  elseif($numberfragment==7)
    $descrip_title .= 'Rango de 6 - 7';
  else
    $descrip_title .= 'SIN AGINAR';

  $result = $oReporteCredito->listar_segmentacion_embudo($fecha_hoy,$numberfragment,$singestion);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $col01 = $value['temp_cobranza_id'];  $col06 = $value['estado_credito'];      $col11 = $value['representante_nom'];     $col16 = $value['restante_soles'];  $col21 = $value['cuota_pago'];          $col26 = $value['inactividad'];               $col31 = $value['tb_opcionesgc_nom'];
        $col02 = $value['cuotadetalle_id'];   $col07 = $value['cliente_id'];          $col12 = $value['cliente_cel'];           $col17 = $value['cuota_actual'];    $col22 = $value['cuota_fecha'];         $col27 = $value['ultimo_pago'];
        $col03 = $value['credito_id'];        $col08 = $value['cliente_dni'];         $col13 = $value['moneda'];                $col18 = $value['cuota_estado'];    $col23 = $value['fecha_facturacion'];   $col28 = $value['deuda_acumulada'];
        $col04 = $value['credito_tipo'];      $col09 = $value['cliente_nom'];         $col14 = $value['capital_desembolsado'];  $col19 = $value['cuota_monto'];     $col24 = $value['ultimopago_fecha'];    $col29 = $value['asignaropciongc_fecges'];
        $col05 = $value['credito_subtipo'];   $col10 = $value['representante_dni'];   $col15 = $value['capital_restante'];      $col20 = $value['cuota_soles'];     $col25 = $value['dias_atraso'];         $col30 = $value['resultado_nom'];

        $suma += formato_numero($col20);
        $data .= '<tr>
        <td class="text-center">'.$col03.'</td>
        <td>'.$col09.'</td>
        <td class="text-center">'.$col25.'</td>
        <td class="text-center">'.$col26.'</td>
        <td class="text-center">'.$col17.'</td>
        <td class="text-center">'.$col31.'</td>
        <td class="text-center">'.$col30.'</td>
        <td class="text-center">'.$col29.'</td>
        <td class="text-right">'.mostrar_moneda($col20).'</td>
        <td class="text-center"><button type="button" class="btn btn-info btn-xs" onclick="opcionesgc_asignar_form(\'L\', '.$col01.')"><i class="fa fa-phone"></i></button></td>
        </tr>';
      }
    }
  $result = null;
  
  echo 
  '<div class="mb-4 text-right">
    <h4 class="">TOTAL: &nbsp;&nbsp;&nbsp; S/ '.mostrar_moneda($suma).'</span>
  </div>
  <div class="box box-solid box-primary mb-4">
    <div class="box-header with-border">
      <h3 class="box-title text-white">'.$descrip_title.'</h3>
    </div>
    <div class="box-body"> 
      <div class="table-responsive">
        <table class="table table-striped" id="table-fragment">
          <thead>
            <tr align="center">
            <td>ID Credito</td>
            <td>Cliente</td>
            <td>Dias de atraso</td>
            <td>Inactvidad</td>
            <td>Num. Cuota</td>
            <td>Opciones</td>
            <td>Resultado</td>
            <td>Fecha de gestión</td>
            <td>Monto</td>
            <td>LLamar</td>
            </tr>
          </thead>
          <tbody>
            '.$data.'
          </tbody>
        </table>
      </div>
    </div>
  </div>';
?>