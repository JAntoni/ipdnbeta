<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class AsignarOpcionGc extends Conexion{
    public $credito_id;
    public $cuotadetalle_id;
    public $tb_opcionesgc_id;
    public $resultado_nom; 
    public $comentario_des; 
    public $opciongc_fechapdp;
    public $encargado_id;
    public $asignaropciongc_fecges;

    function insertar_asginar_opciongc(){
      $this->dblink->beginTransaction();
      try {
        $columns = [
          'credito_id', 
          'cuotadetalle_id', 
          'tb_opcionesgc_id', 
          'comentario_des', 
          'opciongc_fechapdp',
          'encargado_id',
          'asignaropciongc_fecges'
        ];
  
        // Lista de placeholders
        $placeholders = implode(',', array_map(function ($column) {
            return ':' . $column;
        }, $columns));

        $sql = "INSERT INTO tb_asignaropciongc (" . implode(',', $columns) . ") VALUES ($placeholders)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotadetalle_id", $this->cuotadetalle_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_opcionesgc_id", $this->tb_opcionesgc_id, PDO::PARAM_INT);
        //$sentencia->bindParam(":resultado_nom", $this->resultado_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":comentario_des", $this->comentario_des, PDO::PARAM_STR);
        $sentencia->bindParam(":opciongc_fechapdp", $this->opciongc_fechapdp, PDO::PARAM_STR);
        $sentencia->bindParam(":encargado_id", $this->encargado_id, PDO::PARAM_INT);
        $sentencia->bindParam(":asignaropciongc_fecges", $this->asignaropciongc_fecges, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }

    function insertar_deudadiaria($deudadiaria_fec, $deudadiaria_mon){
      $this->dblink->beginTransaction();
      try {

        $sql = "INSERT INTO tb_deudadiaria (tb_deudadiaria_fec, tb_deudadiaria_mon) VALUES (:deudadiaria_fec, :deudadiaria_mon)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deudadiaria_fec", $deudadiaria_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":deudadiaria_mon", $deudadiaria_mon, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function validar_deudadiaria($deudadiaria_fec){
      try {
        $sql = "SELECT * FROM tb_deudadiaria WHERE tb_deudadiaria_fec =:deudadiaria_fec";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deudadiaria_fec", $deudadiaria_fec, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }
    function listar_deudadiaria($deudadiaria_fec1, $deudadiaria_fec2){
      try {
        $sql = "SELECT * FROM tb_deudadiaria WHERE tb_deudadiaria_fec BETWEEN :deudadiaria_fec1 AND :deudadiaria_fec2 ORDER BY tb_deudadiaria_fec";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deudadiaria_fec1", $deudadiaria_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":deudadiaria_fec2", $deudadiaria_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }

    function listar_creditos_cuotadetalle_cliente($cliente_id, $credito_tip, $fecha_actual, $cuotadetalle_id){
      try {
        $tabla_credito = 'tb_creditoasiveh';
        if(intval($credito_tip) == 3)
          $tabla_credito = 'tb_creditogarveh';
        if(intval($credito_tip) == 4)
          $tabla_credito = 'tb_creditohipo';
        
        $filtro_detalle = '';
        if(intval($cuotadetalle_id) > 0)
          $filtro_detalle = " AND det.tb_cuotadetalle_id = ".intval($cuotadetalle_id);

        $sql = "SELECT
                  cre.tb_credito_id,
                  tb_cuotadetalle_id,
                  tb_cuotadetalle_fec,
                  tb_credito_tip,
                  tb_cuota_num,
                  (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
                    THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
                  ) AS num_cuotas_totales
                FROM $tabla_credito cre 
                INNER JOIN tb_cuota cuo ON (cuo.tb_creditotipo_id =:credito_tip AND cuo.tb_credito_id = cre.tb_credito_id) 
                INNER JOIN tb_cuotadetalle det ON (det.tb_cuota_id = cuo.tb_cuota_id) 
                WHERE tb_credito_xac = 1 AND tb_credito_est IN(3, 4) AND tb_cuotadetalle_fec <=:fecha_actual AND tb_cuotadetalle_est IN(1,3) 
                AND cre.tb_cliente_id =:cliente_id".$filtro_detalle;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_tip", $credito_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha_actual", $fecha_actual, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }

    function modificar_asginar_opciongc(){
      $this->dblink->beginTransaction();
      try {

        $sql = "UPDATE tb_asignaropciongc SET 
                tb_opcionesgc_id =:tb_opcionesgc_id, 
                comentario_des =:comentario_des, 	
                opciongc_fechapdp =:opciongc_fechapdp,
                encargado_id =:encargado_id,
                asignaropciongc_fecges = :asignaropciongc_fecges
                WHERE credito_id =:credito_id AND cuotadetalle_id =:cuotadetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotadetalle_id", $this->cuotadetalle_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_opcionesgc_id", $this->tb_opcionesgc_id, PDO::PARAM_INT);
        $sentencia->bindParam(":comentario_des", $this->comentario_des, PDO::PARAM_STR);
        $sentencia->bindParam(":opciongc_fechapdp", $this->opciongc_fechapdp, PDO::PARAM_STR);
        $sentencia->bindParam(":encargado_id", $this->encargado_id, PDO::PARAM_INT);
        $sentencia->bindParam(":asignaropciongc_fecges", $this->asignaropciongc_fecges, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function listar_asignar_opcionesgc_credito_cuotadetalle($credito_id, $cuotadetalle_id){
      try {
        $sql = "SELECT * FROM tb_asignaropciongc asig
                INNER JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id
                WHERE credito_id =:credito_id AND cuotadetalle_id =:cuotadetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotadetalle_id", $cuotadetalle_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }

    function modificar_campo_asignar_opciongc($credito_id, $cuotadetalle_id, $asignaropciongc_columna, $asignaropciongc_valor, $param_tip) {
      $this->dblink->beginTransaction();
      try {
          if (!empty($asignaropciongc_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

              $sql = "UPDATE tb_asignaropciongc SET " . $asignaropciongc_columna . " =:asignaropciongc_valor WHERE credito_id =:credito_id AND cuotadetalle_id =:cuotadetalle_id";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
              $sentencia->bindParam(":cuotadetalle_id", $cuotadetalle_id, PDO::PARAM_INT);
              if ($param_tip == 'INT')
                  $sentencia->bindParam(":asignaropciongc_valor", $asignaropciongc_valor, PDO::PARAM_INT);
              else
                  $sentencia->bindParam(":asignaropciongc_valor", $asignaropciongc_valor, PDO::PARAM_STR);

              $result = $sentencia->execute();

              $this->dblink->commit();

              return $result; //si es correcto el ingreso retorna 1
          } else
              return 0;
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }
  }

?>