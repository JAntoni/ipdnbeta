<?php
  try 
  {
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  //require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');
  require_once '../creditogarveh/Creditogarveh.class.php';
  $oCreditogarveh = new Creditogarveh();
  $oReporteCredito = new ReporteCredito();
  $oAsignar = new AsignarOpcionGc();

  $fecha_hoy = date('Y-m-d');

  $filter_num_cuotas = intval($_POST['p_filtro_cuotas']);
  $filter_dias_atraso = intval($_POST['p_filtro_dias_atraso']);

  parse_str($_POST["p_array_datos"], $datosFrm);
  $txtnrotitulo = '';//    = $datosFrm["txtnrotitulo"];  
  $opcionesgc_id = intval($datosFrm['fil_cmb_opcionesgc_id']);
  $pdp_dias = $datosFrm['fil_cmb_pdp_dias'];
  $resultado_nom = $datosFrm['fil_cmb_resultado_nom'];
  $encargado_id = intval($datosFrm['fil_cmb_encargado_id']);
  $confirmacion_ejecucion = $opcionesgc_id == 3 ? intval($datosFrm['cbo_ejecucion_confirm']) : 0;
  
  if($opcionesgc_id != 1)
    $pdp_dias = 0; //si no selecciona PDP entonces se debe mostrar todos los compromisos de pago
  $dataextra = 'opc gc: '.$opcionesgc_id.' || pd dias: '.$pdp_dias.' || result: '.$resultado_nom.' || '.$fecha_hoy;

  $SUMA_TOTAL_DESEMBOLSADO = 0;
  $SUMA_TOTAL_CAPITAL_RECUPERAR = 0;
  $SUMA_TOTAL_FACTURADO = 0;
  $SUMA_TOTAL_COBRADO = 0;
  $SUMA_TOTAL_POR_COBRAR = 0;
  $DEUDA_ACUMULADA = 0;

  $total_monto_cuota_soles = 0;
  $total_cuota_pago_soles = 0;
  $total_restante_soles = 0;
  $total_deuda_acumulada_soles = 0;
  $fecha_ultima_pago = "";
  $resaltar_usuario_sin_gestion = "";

  $sum_pendiente_sol    = 0.00;
  $sum_pendiente_dol    = 0.00;
  $sum_total_pendiente  = 0.00;
  $sum_pago_parcial_sol = 0.00;
  $sum_pago_parcial_dol = 0.00;
  $sum_total_pago_parcial = 0.00;
  $sum_cancelado_sol    = 0.00;
  $sum_cancelado_dol    = 0.00;
  $sum_total_cancelado  = 0.00;

  $SUMA_CAPITAL_RECUPERADO = 0;
  $data = "";

  $result = $oReporteCredito->temp_cobranza_lista($opcionesgc_id, $pdp_dias, $resultado_nom, $fecha_hoy, $encargado_id, $confirmacion_ejecucion, $filter_num_cuotas, $filter_dias_atraso);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        
        // $SUMA_TOTAL_DESEMBOLSADO += $capital_desembolsado_soles;
        // $SUMA_TOTAL_CAPITAL_RECUPERAR += $capital_restante_soles ;
        // $SUMA_TOTAL_FACTURADO += $monto_cuota_soles;
        // $SUMA_CAPITAL_RECUPERADO = $SUMA_TOTAL_DESEMBOLSADO - $SUMA_TOTAL_CAPITAL_RECUPERAR;
        $class_td = 'class="text-right success"';
        $class_atraso = 'class="text-center success"';
        $class_inactivo = 'class="text-center success"';
        $class_porcentaje = 'class="text-center danger"';

        $cuota_estado = 'Pendiente';
        if($value['tb_cuotadetalle_est'] == 2)
          $cuota_estado = 'Cancelado';
        if($value['tb_cuotadetalle_est'] == 3)
          $cuota_estado = 'Pago Parcial';

        $cuota_estado = '<span class="badge badge-danger">Pendiente</span>';
        if($value['tb_cuotadetalle_est'] == 2){
          $cuota_estado = '<span class="badge badge-success">Cancelado</span>';
          if( $value['moneda'] == 'S/.')
            $sum_cancelado_sol += formato_moneda($value['cuota_soles']);
          else
            $sum_cancelado_dol += formato_moneda($value['cuota_soles']);
        }else if($value['tb_cuotadetalle_est'] == 3){
          $cuota_estado = '<span class="badge badge-info">Pago Parcial</span>';
          if( $value['moneda'] == 'S/.')
            $sum_pago_parcial_sol += formato_moneda($value['cuota_soles']);
          else
            $sum_pago_parcial_dol += formato_moneda($value['cuota_soles']);
        }else{
          if( $value['moneda'] == 'S/.')
            $sum_pendiente_sol += formato_moneda($value['cuota_soles']);
          else
            $sum_pendiente_dol += formato_moneda($value['cuota_soles']);
        }

        if(strtotime($fecha_hoy) > strtotime($value['cuota_fecha'])){ //todo codigo aquí dentro es para cuiando la cuota está vencida
          $estado_credito = 'Vencido';
          $class_td = 'class="text-right danger"';
        }
        if(intval($value['dias_atraso']) > 20 && intval($value['dias_atraso']) <= 30)
          $class_atraso = 'class="text-center warning"';
        if(intval($value['dias_atraso']) > 30)
          $class_atraso = 'class="text-center danger"';

        if(intval($value['inactividad']) > 20 && intval($value['inactividad']) <= 30)
          $class_inactivo = 'class="text-center warning"';
        if(intval($value['inactividad']) > 30)
          $class_inactivo = 'class="text-center danger"';

        if(intval($value['ultimo_pago']) >= 50 && intval($value['ultimo_pago']) < 80)
          $class_porcentaje = 'class="text-center warning"';
        if(intval($value['ultimo_pago']) >= 80)
          $class_porcentaje = 'class="text-center success"';
        
        //vamos a obtener sus opciones asignadas
        $ultimopago_fecha = $value['ultimopago_fecha'];

        $opcionesgc_nom = $value['tb_opcionesgc_nom'];
        $resultado_nom = $value['resultado_nom'];
        $comentario_des = $value['comentario_des'];
        $fecha_pdp = $value['opciongc_fechapdp']; // esta fecha es la promesa de pago, mediante esta fecha evaluamos si cumplió o no
        if($value['tb_opcionesgc_id'] == 1 || $value['tb_opcionesgc_id'] == 4)
          $opcionesgc_nom = $value['tb_opcionesgc_nom'].' <b>'.mostrar_fecha($fecha_pdp).'</b>';

        if($value['tb_opcionesgc_id'] == 2) { $resaltar_usuario_sin_gestion = '<span class="badge badge-danger">'.$value['tb_usuario_nom'].'</span>'; } else { $resaltar_usuario_sin_gestion = $value['tb_usuario_nom']; }

        //si está en estado de ejecucion y falta aprobar que se distinga en la tabla
        $res_cgv = $oCreditogarveh->mostrarUno($value['credito_id']);
        $ejecucion_por_aprobar = ''; $txt_add = ''; $class_falta_aprobar = '';
        if ($value['credito_tipo'] == 'Crédito GARVEH' && !in_array($value['credito_subtipo'], ['GARVEH ACUERDO PAGO']) && in_array($res_cgv['data']['tb_credito_subgar'], ['REGULAR', 'PRE-CONSTITUCION']) && $value['tb_opcionesgc_id'] == 3 && empty($value['tb_asignaropciongc_usuapr'])) {
          $ejecucion_por_aprobar = 'color: red;';
          $txt_add = ' (Falta confirmar ejecucion)';
          $class_falta_aprobar = 'class="danger"';
        }

        //definimos el estado que tendrá la columna resultado que depende de la opción asignada
        if($value['tb_opcionesgc_id'] == 1){ //la columna de RESULTADO netamente depende de los compromisos de pago, cualquiero otra opción esta debe estar varcía
          //fechas que necesitamos: fecha hoy, fecha de ultimo pago, fecha compromiso de pago
          $fecha_hoy_menos1 = date("Y-m-d", strtotime($fecha_hoy."- 1 days")); //obtenemos fecha de ayer para comparar si hubo un pago ayer con el compromiso
          if($ultimopago_fecha == $fecha_pdp){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'SI CUMPLIO', 'STR');
            $resultado_nom = 'SI CUMPLIÓ';
          }
          if(strtotime($fecha_pdp) > strtotime($fecha_hoy_menos1)){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'ESPERANDO PDP', 'STR');
            $resultado_nom = 'ESPERANDO PDP';
          }
          if(strtotime($fecha_pdp) < strtotime($fecha_hoy) && strtotime($ultimopago_fecha) < strtotime($fecha_pdp)){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'NO CUMPLIO', 'STR');
            $resultado_nom = 'NO CUMPLIÓ';
          }
          if(strtotime($ultimopago_fecha) > strtotime($fecha_pdp)){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'ASIGNAR NUEVO PDP', 'STR');
            $resultado_nom = 'ASIGNAR NUEVO PDP';
          }
        }
        else{
          $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', '', 'STR');
        }

        $DEUDA_ACUMULADA += $value['deuda_acumulada'];

        // --------------------------------------------------------------------------------------------------- //
        
        $viewmoneda = '<span class="badge badge-primary">US$</span>';
        if( $value['moneda'] == 'S/.'){ $viewmoneda = '<span class="badge badge-success">S/.</span>'; }

        $dato_fecha_facturacion = $value['fecha_facturacion'];
        $dato_ultimopago_fecha = $value['ultimopago_fecha'];

        if( $dato_fecha_facturacion == $dato_ultimopago_fecha ) { $fecha_ultima_pago = '<span class="badge badge-danger">('.mostrar_fecha($value['ultimopago_fecha']).')</span>'; } else { $fecha_ultima_pago = mostrar_fecha($value['ultimopago_fecha']); }

        // --------------------------------------------------------------------------------------------------- //

        $total_monto_cuota_soles     += formato_numero($value['cuota_soles']);
        $total_cuota_pago_soles      += formato_numero($value['cuota_pago']);
        $total_restante_soles        += formato_numero($value['restante_soles']);
        $total_deuda_acumulada_soles += formato_numero($value['deuda_acumulada']);

        $data .= '
          <tr>
            <td style="vertical-align: middle;" class="text-center">
              <input type="checkbox" name="txt_cliente_tip" data-client="'.$value['cliente_id'].'" class="flat-yellow" value="'.$value['temp_cobranza_id'].'"> &nbsp;
              <a class="btn btn-info btn-xs" title="Gestion" onclick="opcionesgc_asignar_form(\'I\', '.$value['temp_cobranza_id'].')"><i class="fa fa-phone fa-sm"></i></a> <br> <p></p>
              <a class="btn bg-purple btn-xs" title="Historial" onclick="opcionesgc_timeline(\''.$value['credito_tipo'].'\', '.$value['credito_id'].')"><i class="fa fa-list-ul fa-sm"></i></a> &nbsp;
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cliente_nota(' . $value['credito_id'] . ')"><i class="fa fa-commenting fa-sm" title="COMENTAR"></i></a>
            </td>
            <td style="vertical-align: middle;" class="text-center">'.$value['credito_id'].'</td>
            <td style="vertical-align: middle;"> <button type="button" class="btn btn-info btn-xs" onclick="deuda_acumulada_form('.$value['credito_id'].', \''.$value['credito_tipo'].'\')"><i class="fa fa-bar-chart fa-fw"></i> Ver</button></td>
            <td style="vertical-align: middle;"><a class="text-primary" onclick="pago_cuotas('.$value['cliente_id'].')" style="cursor: pointer;'.$ejecucion_por_aprobar.'"><b>'.$value['cliente_nom'].'</b></a></td>
            <td style="vertical-align: middle;">'.$value['representante_nom'].'</td>
            <td style="vertical-align: middle;">'.$value['cliente_dir'].'</td>
            <td style="vertical-align: middle;">'.$value['credito_subtipo'].'</td>
            <td style="vertical-align: middle;">'.$value['cuota_actual'].'</td>

            <td style="vertical-align: middle;" class="text-center">'.$cuota_estado.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$viewmoneda.'</td>

            <td style="vertical-align: middle;" class="text-right">'.mostrar_moneda($value['capital_desembolsado']).'</td>
            <td style="vertical-align: middle;" class="text-right ">'.mostrar_moneda($value['capital_restante']).'</td>
            <td style="vertical-align: middle;" class="text-right">'.mostrar_moneda($value['restante_soles']).'</td>

            <td style="vertical-align: middle;" class="text-right">'.mostrar_moneda($value['cuota_monto']).'</td>
            <td style="vertical-align: middle;" class="info text-right">'.mostrar_moneda($value['cuota_soles']).'</td>
            <td style="vertical-align: middle;" '.$class_td.'>'.mostrar_moneda($value['cuota_pago']).'</td>

            <td style="vertical-align: middle;">'.mostrar_fecha($value['cuota_fecha']).'</td>
            <td style="vertical-align: middle;">'.$fecha_ultima_pago.'</td>

            <td style="vertical-align: middle;" '.$class_porcentaje.'>'.$value['ultimo_pago'].'</td>
            <td style="vertical-align: middle;" '.$class_atraso.'>'.$value['dias_atraso'].'</td>
            <td style="vertical-align: middle;" '.$class_inactivo.'>'.$value['inactividad'].'</td>
            <td style="vertical-align: middle;" class="text-right">'.mostrar_moneda($value['deuda_acumulada']).'</td>
            <td style="vertical-align: middle;" id="td_opciones_'.$value['cuotadetalle_id'].'" '.$class_falta_aprobar.'>'.$opcionesgc_nom.$txt_add.'</td>
            <td style="vertical-align: middle;">'.$resultado_nom.'</td>
            <td style="vertical-align: middle;" id="td_comentario_'.$value['cuotadetalle_id'].'">'.$comentario_des.'</td>
            <td style="vertical-align: middle;" class="text-center">
              <img src="'.$value['tb_usuario_fot'].'" class="img-circle" alt="img-asesor" width="30"> <br>'.$resaltar_usuario_sin_gestion.'
            </td>
          </tr>
        ';
      }
    }
    else{
      $data = '
        <tr>
          <td colspan="26">'.$result['mensaje'].'</td>
        </tr>
      ';
    }
    $estadoSQL = $result['estado'];
    $mensajeSQL = $result['mensaje'];
    $consultaSQL = $result['sql'];
    $total_resultados = 0;
    if($result['estado'] != 0) $total_resultados = count($result['data']);
  $result = NULL;
  
  $drawtable = '<table id="tbl_comportamiento" class="table table-hover table-borderless"><thead><tr><th class="">ACTIVIDADES</th><th class="text-center">ID CREDITO</th><th class="">DEUDA ACUM.</th><th class=" ">CLIENTE &nbsp;<i class="fa fa-hand-pointer-o" aria-hidden="true"></i><br>INFO. CRÉDITO</th><th class="">REPRESENTANTE</th><th class="">DIRECCIÓN</th><th class=" ">TIPO CREDITO</th><th class=" ">CUOTA ACTUAL</th><th class="">ESTADO CUOTA (TR)</th><th class="">MONEDA</th><th class="">CAPITAL DESEMBOLSADO</th><th class="">CAPITAL RESTANTE</th><th class="">RESTANTE EN S/.</th><th class="">MONTO CUOTA</th><th class="">MONTO CUOTA EN S/.</th><th class="">CUOTA PAGO S/.</th><th class="">FECHA DE CUOTA</th><th class="">FEC. ULTIMO PAGO</th><th class="">% ULTIMO PAGO</th><th class=" ">DIAS ATRASO</th><th class="">INACTIVIDAD</th><th class="">DEUDA ACUMULADA S/.</th><th class=" ">Opciones</th><th class=" ">RESULTADO</th><th class="">COMENTARIOS</th><th class="">ASESOR</th></tr></thead><tbody id="lista_creditos">'.$data.'</tbody><tfoot><tr><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th class="text-right">'.mostrar_moneda($total_restante_soles).'</th><th>&nbsp;</th><th class="text-right">'.mostrar_moneda($total_monto_cuota_soles).'</th><th class="text-right">'.mostrar_moneda($total_cuota_pago_soles).'</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th class="text-right">'.mostrar_moneda($total_deuda_acumulada_soles).'</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th></tr></tfoot></table>'; // class="table table-hover table-borderless dataTable display" cellspacing="0" width="100%">

  $alerta_deudadiaria = '';
  
  $sum_total_pendiente    = formato_numero($sum_pendiente_sol + $sum_pendiente_dol);
  $sum_total_pago_parcial = formato_numero($sum_pago_parcial_sol + $sum_pago_parcial_dol);
  $sum_total_cancelado    = formato_numero($sum_cancelado_sol + $sum_cancelado_dol);
  
  $json['filtros']['txtnrotitulo']                                      = $txtnrotitulo;
  $json['filtros']['opcionesgc_id']                                     = $opcionesgc_id;
  $json['filtros']['pdp_dias']                                          = $pdp_dias;
  $json['filtros']['resultado_nom']                                     = $resultado_nom;
  $json['filtros']['encargado_id']                                      = $encargado_id;
  $json['filtros']['confirmacion_ejecucion']                            = $confirmacion_ejecucion;
  $json['filtros']['filtro_cuotas']                                     = $filter_num_cuotas;
  $json['filtros']['filtro_dias_atraso']                                = $filter_dias_atraso;

  $json['suma_monto_estado']['suma_monto_cuota_soles_pendiente_sol']    = mostrar_moneda($sum_pendiente_sol);
  $json['suma_monto_estado']['suma_monto_cuota_soles_pendiente_dol']    = mostrar_moneda($sum_pendiente_dol);
  $json['suma_monto_estado']['suma_monto_cuota_soles_pendiente']        = mostrar_moneda($sum_total_pendiente);
  $json['suma_monto_estado']['suma_monto_cuota_soles_pago_parcial_sol'] = mostrar_moneda($sum_pago_parcial_sol);
  $json['suma_monto_estado']['suma_monto_cuota_soles_pago_parcial_dol'] = mostrar_moneda($sum_pago_parcial_dol);
  $json['suma_monto_estado']['suma_monto_cuota_soles_pago_parcial']     = mostrar_moneda($sum_total_pago_parcial);
  $json['suma_monto_estado']['suma_monto_cuota_soles_cancelado_sol']    = mostrar_moneda($sum_cancelado_sol);
  $json['suma_monto_estado']['suma_monto_cuota_soles_cancelado_dol']    = mostrar_moneda($sum_cancelado_dol);
  $json['suma_monto_estado']['suma_monto_cuota_soles_cancelado']        = mostrar_moneda($sum_total_cancelado);

  $json['inputs']['suma_total_desembolsado']                            = mostrar_moneda($SUMA_TOTAL_DESEMBOLSADO);
  $json['inputs']['suma_total_capital_recuperar']                       = mostrar_moneda($SUMA_TOTAL_CAPITAL_RECUPERAR);
  $json['inputs']['suma_capital_recuperado']                            = mostrar_moneda($SUMA_CAPITAL_RECUPERADO);
  $json['inputs']['suma_total_facturado']                               = mostrar_moneda($SUMA_TOTAL_FACTURADO);
  $json['inputs']['suma_total_cobrado']                                 = mostrar_moneda($SUMA_TOTAL_COBRADO);
  $json['inputs']['suma_total_por_cobrar']                              = mostrar_moneda($SUMA_TOTAL_POR_COBRAR);
  $json['inputs']['alerta_deudadiaria']                                 = $alerta_deudadiaria;
  $json['inputs']['deuda_acumulada']                                    = $DEUDA_ACUMULADA;

  $json['tabla']['drawtable']                                           = $drawtable;
  $json['tabla']['estado']                                              = $estadoSQL;
  $json['tabla']['mensaje']                                             = $mensajeSQL;
  $json['tabla']['consulta']                                            = $consultaSQL;
  $json['tabla']['total_resultados']                                    = $total_resultados;
  $json['extra']                                                        = $dataextra;

  echo json_encode($json);
  }
  catch (Exception $error) {
    header("HTTP/1.1 500");
    echo $error->getMessage();
  }
?>