<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  $oAsignar = new AsignarOpcionGc();
  require_once('../reportecredito/ReporteCredito.class.php');
  $oReporteCredito = new ReporteCredito();
  require_once('../cobranzatodos/CobranzaTodos.class.php');
  $oCobranzatodos = new CobranzaTodos();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $modulo_nombre = $_POST['modulo_nombre'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Opción de GC Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Asignar Opción o Registrar Comentarios';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Opción de GC ';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Opción de GC ';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  $cuotadetalle_id = 0;
  $credito_id = 0;
  $opcionesgc_id = 0;
  $opciongc_fechapdp = date('d-m-Y'); // por defento fecha actual
  $comentario_des = '';
  $encargado_id = 32; // por defecto el responsable de gestionar este caso será el GESTOR DE COBRANZA: ahora 32 -> carlos augusto
  $style_pdp_fecha = 'style="display: none;"';
  $cliente_id = 0;

  if($modulo_nombre == 'reportecreditomejorado'){
    $temp_cobranza_id = $_POST['temp_cobranza_id'];

    $result = $oReporteCredito->mostrarUnoTempCobranza($temp_cobranza_id);
      if($result['estado'] == 1){
        $cuotadetalle_id = $result['data']['cuotadetalle_id'];
        $credito_id = $result['data']['credito_id'];
        $credito_tipo = $result['data']['credito_tipo'];
        $credito_subtipo = $result['data']['credito_subtipo'];
        $cuota_actual = $result['data']['cuota_actual'];
        $cliente_nom = $result['data']['cliente_nom'];
        $cliente_id = $result['data']['cliente_id'];
        $representante_nom = $result['data']['representante_nom'];
        $cliente_dir = $result['data']['cliente_dir'];
        $cuota_monto = $result['data']['cuota_monto'];
        $cuota_soles = $result['data']['cuota_soles'];
        $cuota_fecha = mostrar_fecha($result['data']['cuota_fecha']);
        $ultimopago_fecha = mostrar_fecha($result['data']['ultimopago_fecha']);
        $inactividad = $result['data']['inactividad'];
        $deuda_acumulada = $result['data']['deuda_acumulada'];
      }
    $result = NULL;
  }

  if($modulo_nombre == 'cobranzatodos'){
    $credito_id = intval($_POST['credito_id']);
    $credito_tipo_numero = intval($_POST['credito_tipo_numero']); // 1 c menor, 2 asiveh, 3 garveh, 4 hipo
    $cuotadetalle_id = intval($_POST['cuotadetalle_id']);
    $credito_subtipo = $_POST['credito_subtipo'];
    $credito_tipo = $_POST['credito_nombre'];

    $result = $oCobranzatodos->mostrar_informacion_credito_cuotadetalle($credito_id, $credito_tipo_numero, $cuotadetalle_id);
      if($result['estado'] == 1){
        $simbolo_moneda = 'S/.';
        $cuota_actual = $result['data']['tb_cuota_num'].' / '.$result['data']['num_cuotas_totales'];
        $cliente_nom = $result['data']['tb_cliente_nom'];
        $cliente_id = $result['data']['tb_cliente_id'];
        $representante_nom = '';
        $cuota_fecha = mostrar_fecha($result['data']['tb_cuotadetalle_fec']);
        $cliente_dir = $result['data']['tb_cliente_dir'];
        $cuota_monto = $result['data']['tb_cuotadetalle_cuo'];
        if(trim($result['data']['tb_cliente_emprs']) != ''){
          $cliente_nom = $result['data']['tb_cliente_emprs'];
          $representante_nom = $result['data']['tb_cliente_nom'];
        }
        if($result['data']['tb_moneda_id'] == 2)
          $simbolo_moneda = 'US$';
      }
    $result = NULL;

  }

  $result = $oAsignar->listar_asignar_opcionesgc_credito_cuotadetalle($credito_id, $cuotadetalle_id);
    if($result['estado'] == 1){
      $opcionesgc_id = $result['data']['tb_opcionesgc_id'];
      $opcionesgc_nom = $result['data']['tb_opcionesgc_nom'];
      $comentario_des = $result['data']['comentario_des'];
      $encargado_id = $result['data']['encargado_id'];
      $usu_apr_ejecucion = $result['data']['tb_asignaropciongc_usuapr'];
      $fecha_apr_ejecucion = $result['data']['tb_asignaropciongc_fecapr'];
      if($opcionesgc_id == 1 || $opcionesgc_id == 4){
        $style_pdp_fecha = '';
        $opciongc_fechapdp = mostrar_fecha($result['data']['opciongc_fechapdp']);
      }
      if($encargado_id == 0)
        $encargado_id = 32; // por defecto el responsable de gestionar este caso será el GESTOR DE COBRANZA: ahora 32 -> carlos augusto
    }
  $result = NULL;

  $result = $oCliente->mostrarUno($cliente_id);
    if($result['estado'] == 1){
      $cliente_cel = str_replace(' ', '', $result['data']['tb_cliente_cel']);
      $cliente_tel = str_replace(' ', '', $result['data']['tb_cliente_tel']);
      $cliente_telref = str_replace(' ', '', $result['data']['tb_cliente_telref']);
      $cliente_procel = $result['data']['tb_cliente_procel'];
      $cliente_protel = $result['data']['tb_cliente_protel'];
      $cliente_protelref = $result['data']['tb_cliente_protelref'];

      $link_celular = $cliente_cel.' 
        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51'.$cliente_cel.'" target="_blank"><i class="fa fa-whatsapp"></i></a>
        <a class="btn btn-social-icon btn-primary btn-sm" href="tel:+51'.$cliente_cel.'"><i class="fa fa-phone"></i></a>';
      $link_telefono = $cliente_tel.' 
        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51'.$cliente_tel.'" target="_blank"><i class="fa fa-whatsapp"></i></a>
        <a class="btn btn-social-icon btn-primary btn-sm" href="tel:+51'.$cliente_tel.'"><i class="fa fa-phone"></i></a>';
      $link_referencial = $cliente_telref.' 
        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51'.$cliente_telref.'" target="_blank"><i class="fa fa-whatsapp"></i></a>
        <a class="btn btn-social-icon btn-primary btn-sm" href="tel:+51'.$cliente_telref.'"><i class="fa fa-phone"></i></a>';
    }
  $result = NULL;

  $btn_confirmar_ejecucion = $arr_btn = $lbl_num_suministro = '';
  //si viene de reportecreditomejorado, y si está en ejecucion, está vacío el usuario de aprobacion de ejecucion, y el perfil es 1 administrador, y si es tipo cred garveh
  require_once('../creditogarveh/Creditogarveh.class.php');
  $oCred = new Creditogarveh();
  require_once '../usuario/Usuario.class.php';
  $oUsuario = new Usuario();
  //verificar si este credito es preconstitucion
  $res1 = $oCred->mostrarUno($credito_id);
  $res3 = $oUsuario->mostrarUno($res1['data']['tb_credito_usureg']);
  $arr_btn['num_suministro'] = $res1['data']['tb_credito_suministro'];
  $arr_btn['usuario_reg'] = $res3['data']['tb_usuario_nom'] . ' ' . $res3['data']['tb_usuario_ape']; //usuario que registró el cgv
  $arr_btn['responsable_asignado_orig'] = $encargado_id;

  //verificar si este credito tiene adendas
  $res2 = $oCred->listar_adendas_credito($credito_id);
  $arr_btn['tiene_adendas'] = $res2['estado'];
  $arr_btn['info_extra'] = 'cant_adendas: '.count($res2['data2']).' | adenda0_moneda: '.$res2['data2'][0]['tb_moneda_id'].' | adenda0_montopreaco: '.$res2['data2'][0]['tb_credito_preaco'].' | incluye_renov_gps: '.$res2['data2'][0]['tb_credito_obs'];

  if ($modulo_nombre == 'reportecreditomejorado' && $opcionesgc_id == 3 && $_SESSION['usuarioperfil_id'] == 1 && $credito_tipo == 'Crédito GARVEH' && !in_array($credito_subtipo, ['GARVEH ACUERDO PAGO'])) {
    if (empty($usu_apr_ejecucion)) {
      $credito_subgar = $res1['data']['tb_credito_subgar'];
      $arr_btn['info_extra'] .= ' | credito_subgar: '.$credito_subgar;

      //si es regular o pre-constitucion se puede crear proceso ejecucion (16-12-2024)
      if (in_array($credito_subgar, ['REGULAR', 'PRE-CONSTITUCION'])) {
        $arr_btn['credito_id'] = $credito_id;
        $arr_btn['credito_id_padreorigen'] = $res1['data']['tb_credito_idori'];
        $arr_btn['cuotadetalle_id'] = $cuotadetalle_id;
        $arr_btn['cliente_id'] = $cliente_id;
        $arr_btn['moneda_id'] = intval($res1['data']['tb_moneda_id']);
        if ($arr_btn['tiene_adendas'] == 0 || (count($res2['data2']) == 1 && $res2['data2'][0]['tb_moneda_id'] == 2 && $res2['data2'][0]['tb_credito_preaco'] == '150.00' && stripos($res2['data2'][0]['tb_credito_obs'], 'renov') !== false && stripos($res2['data2'][0]['tb_credito_obs'], 'gps') !== false)) {
          $btn_confirmar_ejecucion = '<button id="btn_crear_ejecucion" type="button" class="btn btn-github" onclick="confirmar_ejecucion()">CONFIRMAR EJECUCION</button>';
        }
      }
    } else {
      //si ya se aprobó la ejecucion, consultar el encargado de la ejecucion, buscar ejecucion con la fecha de aprobacion y credito_id
      require_once '../ejecucion/Ejecucion.class.php';
      $oEjecucion = new Ejecucion();

      $buscar_ejec = $oEjecucion->listar_todos('', $cliente_id, $credito_id, fecha_mysql($fecha_apr_ejecucion), fecha_mysql($fecha_apr_ejecucion), '');
      if ($buscar_ejec['estado'] == 1) {
        $arr_btn['ejecucion_encargado_id'] = $buscar_ejec['data'][0]['tb_ejecucion_usuencargado'];
        $arr_btn['ejecucion_encargado_nom'] = $buscar_ejec['data'][0]['tb_usuario_nom'].' '.$buscar_ejec['data'][0]['tb_usuario_ape'];
        $arr_btn['ejecucion_id']  = $buscar_ejec['data'][0]['tb_ejecucion_id'];
      }
    }
  }
  //
  if ($modulo_nombre == 'reportecreditomejorado' && $opcionesgc_id == 3 && empty($arr_btn['num_suministro'])) {
    $lbl_num_suministro = '<label style="color: blue; padding-left: 0px;text-align: left;">CGV-'.$credito_id.' sin n° suministro. Actualizar en Checklist modulo Credito Garveh. Usuario responsable: '.$arr_btn['usuario_reg'].'</label>';
  }
  unset($res1, $res2, $res3);

  //datos para select de usuarios que pertenezcan al area legal
    $usuario_columna = 'usu.tb_usuarioperfil_id';
    $usuario_valor = 3;//usuarioperfil 3 Legal & AC
    $param_tip = 'INT';
    $empresa_id = 0;
    if ($modulo_nombre == 'reportecreditomejorado') {
      $usuario_id = $encargado_id;
    }
  //
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_asignaropciongc" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      <form id="form_opcionesgc_asignar" role="form" method="POST">
        <div class="modal-body">
          <input type="hidden" name="action" value="insertar" />
          <input type="hidden" id="directorio" value="opcionesgc_asignar">

          <input type="hidden" name="hdd_credito_subtipo" value="<?php echo $credito_subtipo;?>">
          <input type="hidden" name="hdd_credito_tipo" value="<?php echo $credito_tipo;?>">
          <input type="hidden" name="hdd_cuota_actual" value="<?php echo $cuota_actual;?>">
          <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
          <input type="hidden" name="hdd_cuotadetalle_id" value="<?php echo $cuotadetalle_id;?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" name="hdd_cliente_nom" value="<?php echo $cliente_nom;?>">
          <input type="hidden" name="hdd_cuota_fecha" value="<?php echo $cuota_fecha;?>">
          <input type="hidden" id="hdd_opcionesgc_nom" name="hdd_opcionesgc_nom" value="<?php echo $opcionesgc_nom;?>">

          <label for="txt_filtro" class="control-label" style="font-size: 12pt;">Detalle del cliente a Gestionar</label>
          <div class="panel panel-primary shadow-simple">
            <div class="panel-body">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th><?php echo $cliente_procel;?></th>
                    <th><?php echo $cliente_protel;?></th>
                    <th><?php echo $cliente_protelref;?></th>
                  </tr>
                  <tr>
                    <th><?php echo $link_celular;?></th>
                    <th><?php echo $link_telefono;?></th>
                    <th><?php echo $link_referencial;?></th>
                  </tr>
                  <tr>
                    <td colspan="3" align="center">
                      <button type="button" class="btn btn-info btn-sm" onclick="opcionesgc_contacto()"><i class="fa fa-pencil"></i> Actualizar Números de ser Necesario</button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p></p>
              <?php if($modulo_nombre == 'reportecreditomejorado'):?>
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>CLiente:</th>
                      <td colspan="3"><?php echo $cliente_nom;?></td>
                    </tr>
                    <tr>
                      <th>Representante:</th>
                      <td colspan="3"><?php echo $representante_nom;?></td>
                    </tr>
                    <tr>
                      <th>Dirección:</th>
                      <td colspan="3"><?php echo $cliente_dir;?></td>
                    </tr>
                    <tr>
                      <th>Cuota Original:</th>
                      <td><?php echo mostrar_moneda($cuota_monto);?></td>
                      <th>Cuota en S/.:</th>
                      <td><?php echo mostrar_moneda($cuota_soles);?></td>
                    </tr>
                    <tr>
                      <th>Fecha Cuota:</th>
                      <td><?php echo $cuota_fecha;?></td>
                      <th>Fecha Ultimo Pago:</th>
                      <td><?php echo $ultimopago_fecha;?></td>
                    </tr>
                    <tr>
                      <th>Inactividad:</th>
                      <td><?php echo $inactividad;?></td>
                      <th>Deuda Acumulada:</th>
                      <td><?php echo mostrar_moneda($deuda_acumulada);?></td>
                    </tr>
                  </tbody>
                </table>
              <?php endif;?>

              <?php if($modulo_nombre == 'cobranzatodos'):?>
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>CLiente:</th>
                      <td colspan="3"><?php echo $cliente_nom;?></td>
                    </tr>
                    <tr>
                      <th>Representante:</th>
                      <td colspan="3"><?php echo $representante_nom;?></td>
                    </tr>
                    <tr>
                      <th>Dirección:</th>
                      <td colspan="3"><?php echo $cliente_dir;?></td>
                    </tr>
                    <tr>
                      <th>Cuota Original:</th>
                      <td><?php echo $simbolo_moneda.' '.mostrar_moneda($cuota_monto);?></td>
                      <th>Fecha Cuota:</th>
                      <td><?php echo $cuota_fecha;?></td>
                    </tr>
                  </tbody>
                </table>
              <?php endif;?>
            </div>
          </div>
          <p></p>
          <p></p>

          <label for="txt_filtro" class="control-label" style="font-size: 12pt;">Asignar Opción para este Crédito</label>
          <div class="panel panel-primary shadow-simple">
            <div class="panel-body">
              <div class="form-group col-md-5">
                <label for="cmb_opcionesgc_id" class="control-label">Opción de Gestión</label>
                <select class="form-control input-sm" id="cmb_opcionesgc_id" name="cmb_opcionesgc_id" style="width: 100%;">
                  <?php require_once('../reportecreditomejorado/opcionesgc_select.php');?>
                </select>
              </div>
              <div class="form-group col-md-3 fecha_pdp" <?php echo $style_pdp_fecha;?> >
                <label for="txt_opciongc_fechapdp" class="control-label">Fecha para Gestión</label>
                <div class='input-group date' id='datetimepicker3'>
                  <input type='text' class="form-control input-sm" name="txt_opciongc_fechapdp" id="txt_opciongc_fechapdp" value="<?php echo $opciongc_fechapdp;?>"/>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
              <div class="form-group col-md-4">
                <label for="cmb_aplicar_todo" class="control-label">¿Aplica a Todo?</label>
                <select class="form-control input-sm" id="cmb_aplicar_todo" name="cmb_aplicar_todo" style="width: 100%;">
                  <option value="1">Solo a esta Cuota</option>
                  <option value="2">Todas las cuotas del CLiente</option>
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="txt_comentario_des" class="control-label">Opcional Puede Dejar un Comentario</label>
                <input class="form-control input-sm" type="text" id="txt_comentario_des" name="txt_comentario_des" value="<?php echo $comentario_des;?>" style="width: 100%;">
              </div>
              <div class="form-group col-md-12">
                <label for="cmb_encargado_id" class="control-label">Puede asignar un Responsable, por defecto será el Gestor de Cobranza:</label>
                <select class="form-control input-sm" id="cmb_encargado_id" name="cmb_encargado_id" style="width: 100%;">
                  <option value="0">Visible para todos los encargados....</option>
                  <option value="11" <?php echo $encargado_id == 11? 'selected' : '';?>>HERBERT ENGEL GALVEZ ROJAS</option>
                  <option value="32" <?php echo $encargado_id == 32? 'selected' : '';?>>CARLOS AUGUSTO CONCEPCIÓN LEON</option>
                  <option value="42" <?php echo $encargado_id == 42? 'selected' : '';?>>GIORDANA MATILDE GARCIA SANTOS</option>
                  <option value="86" <?php echo $encargado_id == 86? 'selected' : '';?>>MARILIN LISBETH MOLINA AQUINO</option>
                  <?php require_once '../usuario/usuario_select.php'; ?>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <?php echo $lbl_num_suministro;?>
          <div class="f1-buttons">
            <input type="hidden" name="hdd_arr_ejecucion" id="hdd_arr_ejecucion" value='<?php echo json_encode($arr_btn);?>'>
            <?php echo $btn_confirmar_ejecucion; ?>
            <button type="submit" class="btn btn-info" id="btn_guardar_opcionesgc_asignar">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/reportecreditomejorado/opcionesgc_todo.js?ver='.rand();?>"></script>