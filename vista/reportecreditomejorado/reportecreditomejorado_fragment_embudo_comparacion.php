<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oReporteCredito = new ReporteCredito();

  $fecha_hoy = date('Y-m-d');

  $resultado_estado = 0;
  $anio = intval($_POST['p_anho']);
  $mes  = intval($_POST['p_mes']);

  $iniciomes_01 = 0; $porcent_inciomes_tramo01 = 0;   $finmes_01 = 0; $porcent_finmes_tramo01 = 0;
  $iniciomes_12 = 0; $porcent_inciomes_tramo02 = 0;   $finmes_12 = 0; $porcent_finmes_tramo02 = 0;
  $iniciomes_23 = 0; $porcent_inciomes_tramo03 = 0;   $finmes_23 = 0; $porcent_finmes_tramo03 = 0;
  $iniciomes_34 = 0; $porcent_inciomes_tramo04 = 0;   $finmes_34 = 0; $porcent_finmes_tramo04 = 0;
  $iniciomes_45 = 0; $porcent_inciomes_tramo05 = 0;   $finmes_45 = 0; $porcent_finmes_tramo05 = 0;
  $iniciomes_56 = 0; $porcent_inciomes_tramo06 = 0;   $finmes_56 = 0; $porcent_finmes_tramo06 = 0;
  $iniciomes_67 = 0; $porcent_inciomes_tramo07 = 0;   $finmes_67 = 0; $porcent_finmes_tramo07 = 0;

  $total_inciomes = 0; $total_finmes = 0;

  $result = $oReporteCredito->mostrarUno_tb_reportecreditomejorado_embudo($mes,$anio);
    $resultado_estado = $result['estado'];
    if( $result['estado'] == 1)
    {
      $iniciomes_01 = floatval($result['data']['inicio_01']);   $finmes_01 = floatval($result['data']['fin_01']);
      $iniciomes_12 = floatval($result['data']['inicio_12']);   $finmes_12 = floatval($result['data']['fin_12']);
      $iniciomes_23 = floatval($result['data']['inicio_23']);   $finmes_23 = floatval($result['data']['fin_23']);
      $iniciomes_34 = floatval($result['data']['inicio_34']);   $finmes_34 = floatval($result['data']['fin_34']);
      $iniciomes_45 = floatval($result['data']['inicio_45']);   $finmes_45 = floatval($result['data']['fin_45']);
      $iniciomes_56 = floatval($result['data']['inicio_56']);   $finmes_56 = floatval($result['data']['fin_56']);
      $iniciomes_67 = floatval($result['data']['inicio_67']);   $finmes_67 = floatval($result['data']['fin_67']);

      $total_inciomes = formato_numero($iniciomes_01+$iniciomes_12+$iniciomes_23+$iniciomes_34+$iniciomes_45+$iniciomes_56+$iniciomes_67);
      $total_finmes = formato_numero($finmes_01+$finmes_12+$finmes_23+$finmes_34+$finmes_45+$finmes_56+$finmes_67);

      if( $total_inciomes > 0 ){
        $porcent_inciomes_tramo01 = formato_numero((($iniciomes_01/$total_inciomes)*100));
        $porcent_inciomes_tramo02 = formato_numero((($iniciomes_12/$total_inciomes)*100));
        $porcent_inciomes_tramo03 = formato_numero((($iniciomes_23/$total_inciomes)*100));
        $porcent_inciomes_tramo04 = formato_numero((($iniciomes_34/$total_inciomes)*100));
        $porcent_inciomes_tramo05 = formato_numero((($iniciomes_45/$total_inciomes)*100));
        $porcent_inciomes_tramo06 = formato_numero((($iniciomes_56/$total_inciomes)*100));
        $porcent_inciomes_tramo07 = formato_numero((($iniciomes_67/$total_inciomes)*100));
      } else{
        $total_inciomes = formato_numero(0);
      }

      if( $total_finmes > 0 ){
        $porcent_finmes_tramo01 = formato_numero((($finmes_01/$total_finmes)*100));
        $porcent_finmes_tramo02 = formato_numero((($finmes_12/$total_finmes)*100));
        $porcent_finmes_tramo03 = formato_numero((($finmes_23/$total_finmes)*100));
        $porcent_finmes_tramo04 = formato_numero((($finmes_34/$total_finmes)*100));
        $porcent_finmes_tramo05 = formato_numero((($finmes_45/$total_finmes)*100));
        $porcent_finmes_tramo06 = formato_numero((($finmes_56/$total_finmes)*100));
        $porcent_finmes_tramo07 = formato_numero((($finmes_67/$total_finmes)*100));
      } else{
        $total_finmes = formato_numero(0);
      }
    }
  $result = null;

  if( $resultado_estado == 0)
  {
    echo '<h5>Resultados no encontrados para la búsqueda | Año: '.$anio.' | Mes: '.nombre_mes($mes).'</h5>';
  }
  else
  {
    echo 
    '
    <div id="lista_fragment_comparacion" class="row mb-4">
      <div class="col-lg-6 col-md-12"> <br>
        <div class="container-fluid mt-4 mb-4">
          <div class="row">
            <div class="col-xs-1 text-center">
              <div style="margin-top: 75px;"></div>
              <div class="lateral-item-02">0</div>
              <div class="lateral-item-02">1</div>
              <div class="lateral-item-02">2</div>
              <div class="lateral-item-02">3</div>
              <div class="lateral-item-02">4</div>
              <div class="lateral-item-02">5</div>
              <div class="lateral-item-02">6</div>
              <div class="lateral-item-02">7</div>
            </div>
            <div class="col-xs-11">
              <div class="funnel leads estimated">
                <h2 class="center-text"><span class="title-teal-with-bakground rounded">INICIO DEL MES</span></h2>
                <ul class="four">
                  <li>
                    <div class="funnel-top"></div>
                    '.mostrar_moneda($iniciomes_01).' <span>'.$porcent_inciomes_tramo01.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($iniciomes_12).' <span>'.$porcent_inciomes_tramo02.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($iniciomes_23).' <span>'.$porcent_inciomes_tramo03.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($iniciomes_34).' <span>'.$porcent_inciomes_tramo04.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($iniciomes_45).' <span>'.$porcent_inciomes_tramo05.'</span> &nbsp;%
                  </li>
                  <li>
                    <div class="funnel-bottom"></div>
                    '.mostrar_moneda($iniciomes_56).' <span>'.$porcent_inciomes_tramo06.'</span> &nbsp;%
                  </li>
                </ul>
                <div class="text-center"> &nbsp; &nbsp;
                  <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">'.mostrar_moneda($iniciomes_67).'</span> &nbsp;
                  <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">'.$porcent_inciomes_tramo07.' %</span>
                </div>
              </div>
              <hr>
              <div class="mt-4 text-center">
                <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                <span class="badge bg-blue p-4" style="font-size: 18px;">S/ '.mostrar_moneda($total_inciomes).'</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12"> <br>
        <div class="container-fluid mt-4 mb-4">
          <div class="row">
            <div class="col-xs-1 text-center">
              <div style="margin-top: 75px;"></div>
              <div class="lateral-item-02">0</div>
              <div class="lateral-item-02">1</div>
              <div class="lateral-item-02">2</div>
              <div class="lateral-item-02">3</div>
              <div class="lateral-item-02">4</div>
              <div class="lateral-item-02">5</div>
              <div class="lateral-item-02">6</div>
              <div class="lateral-item-02">7</div>
            </div>
            <div class="col-xs-11">
              <div class="funnel leads estimated">
                <h2 class="center-text"><span class="title-teal-with-bakground rounded">FINAL DEL MES</span></h2>
                <ul class="five">
                  <li>
                    <div class="funnel-top"></div>
                    '.mostrar_moneda($finmes_01).' <span>'.$porcent_finmes_tramo01.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($finmes_12).' <span>'.$porcent_finmes_tramo02.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($finmes_23).' <span>'.$porcent_finmes_tramo03.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($finmes_34).' <span>'.$porcent_finmes_tramo04.'</span> &nbsp;%
                  </li>
                  <li>
                    '.mostrar_moneda($finmes_45).' <span>'.$porcent_finmes_tramo05.'</span> &nbsp;%
                  </li>
                  <li>
                    <div class="funnel-bottom"></div>
                    '.mostrar_moneda($finmes_56).' <span>'.$porcent_finmes_tramo06.'</span> &nbsp;%
                  </li>
                </ul>
                <div class="text-center"> &nbsp; &nbsp;
                  <span class="badge bg-navy" style="font-size: 18px; padding: 0.8rem;">'.mostrar_moneda($finmes_67).'</span> &nbsp;
                  <span class="badge bg-gray" style="font-size: 18px; padding: 0.8rem;">'.$porcent_finmes_tramo07.' %</span>
                </div>
              </div>
              <hr>
              <div class="mt-4 text-center">
                <span class="badge bg-teal p-4" style="font-size: 18px;">Total</span> &nbsp;
                <span class="badge bg-blue p-4" style="font-size: 18px;">S/ '.mostrar_moneda($total_finmes).'</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    ';
  }
?>