<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<?php include(VISTA_URL.'templates/head.php'); ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
	</head>

	<body <?php echo 'class="sidebar-collapse '.CLASE_BODY.' '.$usuario_tem.'"'; ?>>

		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php include(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php include(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php include('reportecreditomejorado_vista.php');; ?>
			<!-- INCLUIR FOOTER-->
				<?php include(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include(VISTA_URL.'templates/script.php'); ?>

		<script type="text/javascript" src="<?php echo VISTA_URL.'reportecreditomejorado/reportecreditomejorado.js?ver=31012025';?>"></script>
	</body>
</html>
