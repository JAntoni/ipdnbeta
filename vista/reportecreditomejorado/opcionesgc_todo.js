function registrar_opciongc(){
  var opcionesgc_nom = $('#txt_opcionesgc_nom').val().trim();
  var opcionesgc_des = $('#txt_opcionesgc_des').val().trim();

  if(!opcionesgc_nom || !opcionesgc_des){
    alerta_warning('AVISO', 'Debes llenar todos los campos');
    return false;
  }

  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_controller.php",
    async: true,
		dataType: "json",
		data: ({
      action: 'insertar',
      opcionesgc_nom: opcionesgc_nom,
      opcionesgc_des: opcionesgc_des
    }),
		beforeSend: function() {
      
		},
		success: function(data){
      if(parseInt(data.estado) == 1){
        notificacion_success('Opción para GC registrado')
        listar_opcionesgc();

        $('#txt_opcionesgc_nom').val('');
        $('#txt_opcionesgc_des').val('');
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function listar_opcionesgc(){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_controller.php",
    async: true,
		dataType: "html",
		data: ({
      action: 'tabla'
    }),
		beforeSend: function() {
		},
		success: function(data){
      $('#table_opcionesgc').html(data)
		},
		complete: function(data){
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}
function opcionesgc_contacto(){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_contacto.php",
    async: true,
		dataType: "html",
		data: ({
      cliente_id: $('#hdd_cliente_id').val()
    }),
		beforeSend: function() {
      
		},
		success: function(data){
      if(data != 'sin_datos'){
        console.log('entra suuu')
      	$('#div_modal_cliente_contacto').html(data);
      	$('#modal_cliente_contacto').modal('show');
      	modal_hidden_bs_modal('modal_cliente_contacto', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function actualizar_numeros(){
  var celular = $('#txt_numero_cel').val();
  var telefono = $('#txt_numero_tel').val();
  var referencial = $('#txt_numero_telref').val();
  var pro_celular = $('#txt_numero_procel').val();
  var pro_telefono = $('#txt_numero_protel').val();
  var pro_referencial = $('#txt_numero_protelref').val();

  var cliente_id = $('#hdd_cliente_id').val();

  if(!celular || !telefono || !referencial || !cliente_id || !pro_celular || !pro_telefono || !pro_referencial){
    alerta_error('IMPORTANTE', 'No puedes ingresar valores vacíos / ' + cliente_id);
    return false;
  }

  $.confirm({
    icon: 'fa fa-pencil',
    title: 'Actualizar',
    content: '¿Está seguro de actualizar los números del cliente?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function () {
        $.ajax({
          type: "POST",
          url: VISTA_URL + "reportecreditomejorado/opcionesgc_asignar_controller.php",
          async: true,
          dataType: "json",
          data: ({
            cliente_id: cliente_id,
            cliente_cel: celular,
            cliente_tel: telefono,
            cliente_telref: referencial,
            cliente_procel: pro_celular,
            cliente_protel: pro_telefono,
            cliente_protelref: pro_referencial,
            action: 'actualizar_numeros'
          }),
          beforeSend: function () {
          },
          success: function (data) {
            $('#modal_cliente_contacto').modal('hide');
            swal_success("SISTEMA", data.mensaje, 2000);
          },
          complete: function (data) {
            if (data.status != 200)
              console.log(data);
          }
        });
      },
      no: function () {}
    }
  });
}

var responsable_id;
$(document).ready(function(){
  $('#datetimepicker3').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    startDate: "-0d"
    //endDate : new Date()
  });

  $('#cmb_encargado_id').change(function(){
    responsable_id = $(this).val();
  });
  $('#cmb_encargado_id').change();

  $('#cmb_opcionesgc_id').change(function(event){
    var opcionesgc_id = parseInt($(this).val());
    var opcionesgc_nom = $('#cmb_opcionesgc_id :selected').text();
    $('#hdd_opcionesgc_nom').val(opcionesgc_nom);

    if(opcionesgc_id == 1 || opcionesgc_id == 4){
      $('.fecha_pdp').show();
      $('#cmb_encargado_id').val(32);
    }
    else if (opcionesgc_id == 3)
      $('#cmb_encargado_id').val(42);
    else{
      $('.fecha_pdp').hide();
      $('#cmb_encargado_id').val(responsable_id);
    }
  });

  directorio = $('#directorio').val(); //opcionesgc, asignaropciongc
  if(directorio == "opcionesgc")
    listar_opcionesgc();

  console.log('corre ya 444: ' + directorio)

  $('#form_opcionesgc_asignar').validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "reportecreditomejorado/opcionesgc_asignar_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_opcionesgc_asignar").serialize(),
        beforeSend: function () {
          
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {
            swal_success('Genial!', data.mensaje, 3000)
            $('#modal_registro_asignaropciongc').modal('hide');

            //retorna los valores de gestión ingresados, para asiganarlo a los td
            $.each(data.cuotadetalle_id, function(index, value) {
              $('#td_opciones_' + value).text(data.td_opciones);
              $('#td_comentario_' + value).text(data.td_comentario);
            });

            console.log(data)
          } 
          else {
            alerta_warning('AVISO', 'Por favor tener en cuenta: ' + data.mensaje)
          }
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      cmb_opcionesgc_id: {
        required: true,
        min: 1
      }
    },
    messages: {
      cmb_opcionesgc_id: {
        required: "Selecciona una opción de gestión para guardar",
        min: "Selecciona una opción de gestión para guardar"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });

  //daniel odar 17-10-2024, require_once de usuarios, ocultar los que estén con estado(bloqeo) = 1
    $('#cmb_encargado_id option[id*="option_cero"], #cmb_encargado_id option[data-estado="1"]').remove();
  //
});

function confirmar_ejecucion() {
  $.confirm({
    title: 'CONFIRME',
    content: `<label style="color: red;">SE CREARÁ UN PROCESO DE EJECUCIÓN LEGAL. ELIJA EL TIPO DE CRÉDITO PARA EJECUTAR:</label><br>`+
            `<label>Tipo de crédito:</label>`+
            `<select id="cbo_tipocredito_ejec" name="cbo_tipocredito_ejec" class="input-sm form-control">`+
                `<option value="0">SELECCIONE</option>`+
                `<option value="1" style="font-weight: bold;">PRECONSTITUCIÓN</option>`+
                `<option value="2" style="font-weight: bold;">CONSTITUCIÓN SIN CUSTODIA</option>`+
            `</select>`
    ,
    type: 'orange',
    escapeKey: 'close',
    backgroundDismiss: true,
    columnClass: 'small',
    buttons: {
      ACEPTAR: {
        text: 'ACEPTAR',
        btnClass: 'btn-orange',
        action: function () {
          var cgv_tipo = this.$content.find('#cbo_tipocredito_ejec').val();
          if (cgv_tipo < 1) {
            $.alert('<b>Elija tipo de crédito para ejecutar</b>');
            return false;
          }
          $.ajax({
            type: 'POST',
            url: VISTA_URL + 'reportecreditomejorado/opcionesgc_asignar_controller.php',
            async: true,
            dataType: 'json',
            data: ({
              action: 'crear_ejecucion',
              arr_ejecucion: $('#hdd_arr_ejecucion').val(),
              usu_encargado: $('#cmb_encargado_id option:selected').text(),
              usu_encargado_id: $('#cmb_encargado_id option:selected').val(),
              cgarvtipo_ejec: cgv_tipo
            }),
            success: function (data) {
              if (data.estado == 1) {
                alerta_success("SISTEMA", data.mensaje);
                $('#btn_crear_ejecucion').hide();
              } else {
                alerta_error("SISTEMA", data.mensaje);
              }
            },
            complete: function (data) {
              console.log(data);
            }
          });
        }
      },
      cancelar: function () {
      }
    }
  });
}