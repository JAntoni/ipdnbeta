<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/Opcionesgc.class.php');
  $oOpcionesgc = new Opcionesgc();
  require_once('../funciones/funciones.php');

  $direc = 'opcionesgc';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $opcionesgc_id = $_POST['opcionesgc_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Opción de GC Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Opción de GC ';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Opción de GC ';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Opción de GC ';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_opcionesgc" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      
      <div class="modal-body">
        <input type="hidden" id="directorio" value="opcionesgc">
        
        <label for="txt_filtro" class="control-label">Registro de nueva Opción</label>
        <div class="panel panel-primary shadow-simple">
          <div class="panel-body">
            <form id="form_opcionesgc" class="row form-inline" role="form">
              <div class="form-group col-md-4">
                <input type='text' class="form-control input-sm mayus" name="txt_opcionesgc_nom" id="txt_opcionesgc_nom" style="width: 100%;" autocomplete="off" placeholder="Nombre de Opción"/>
              </div>
              <div class="form-group col-md-7">
                <input type='text' class="form-control input-sm" name="txt_opcionesgc_des" id="txt_opcionesgc_des" style="width: 100%;" autocomplete="off" placeholder="Breve descripción"/>
              </div>
              <button type="button" class="btn btn-primary btn-sm" onclick="registrar_opciongc()"><i class="fa fa-save"></i></button>
            </form>
          </div>
        </div>
        <p></p>
        <p></p>

        <label for="txt_filtro" class="control-label">Lista de Opciones Registradas</label>
        <div class="panel panel-primary shadow-simple">
          <div class="panel-body">
            <table class="table table-bordered">
              <tbody id="table_opcionesgc"></tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>

    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/reportecreditomejorado/opcionesgc_todo.js?ver=2';?>"></script>