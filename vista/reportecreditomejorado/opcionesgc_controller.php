<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/Opcionesgc.class.php');
  $oOpcionesgc = new Opcionesgc();
  
  $action = $_POST['action'];

  if($action == 'insertar'){
    $opcionesgc_nom = mb_strtoupper($_POST['opcionesgc_nom'], 'UTF-8');
    $opcionesgc_des = $_POST['opcionesgc_des'];

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Usuario Perfil.';

    if($oOpcionesgc->insertar($opcionesgc_nom, $opcionesgc_des)){
      $data['estado'] = 1;
      $data['mensaje'] = 'Usuario Perfil registrado correctamente.';
    }

    echo json_encode($data);
  }
  if($action == 'tabla'){
    $td_opcionesgc = '';
    $result = $oOpcionesgc->listar_opcionesgc();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $td_opcionesgc .= '
            <tr>
              <td>'.$value['tb_opcionesgc_id'].'</td>
              <td>'.$value['tb_opcionesgc_nom'].'</td>
              <td>'.$value['tb_opcionesgc_des'].'</td>
            </tr>';
        }
      }
    $result = NULL;
    
    $tabla = '
      <tr>
        <th>#</th>
        <th>Nombre</th>
        <th>Descripción</th>
      </tr>' . $td_opcionesgc;

    echo $tabla;
  }
?>