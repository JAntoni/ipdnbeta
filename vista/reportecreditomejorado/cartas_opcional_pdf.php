<?php
error_reporting(0);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once('../reportecredito/ReporteCredito.class.php');
$oReporteCredito = new ReporteCredito();

$title = 'Cartas Opcionales';
$codigo = 'CARTAS-000009';

$tipo_carta = trim($_GET['tipo_carta']);
$temp_cobranza_ids = trim($_GET['temp_ids']);
$temp_cobranza_ids = substr($temp_cobranza_ids, 0, -1);

$array_html = [];
$array_codigos = [];
$posicion = 0;

//? DESDE AQUÍ EMPIEZA LA ESTRUCTURA PARA LAS CARTAS DE LOS CLIENTES
$result = $oReporteCredito->temp_cobranza_por_ids($temp_cobranza_ids);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $cliente_nom = $value['cliente_nom'];
      $cliente_dir = $value['cliente_dir'];
      $representante_nom = $value['representante_nom'];
      $dias_atraso = $value['dias_atraso'];
      $tipo_cliente = 1; //persona natural
      $html = '';

      if($representante_nom == '')
        $tipo_cliente = 2; // persona juridica

      //? DOCUMENTO PARA TRAMO DE 1 A 15 DÍAS
      $html = '
        <tr>
          <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
          <td colspan="15" style="text-align: center;"></td>
        </tr>
        <br/><br/>
        <tr>
          <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
        </tr>
        <br/><br/>
        <tr>
          <td colspan="12" style="text-align: center; font-size: 55px;">
            <b><u>REQUERIMIENTO DE PAGO</u></b>
          </td>
        </tr>
        <br/><br/><br/>
        <tr>
          <td colspan="20"><b>'.$cliente_nom.'</b></td>
        </tr>
        <tr>
          <td colspan="20"><b>'.$representante_nom.'</b></td>
        </tr>
        <br/>
        <tr>
          <td colspan="20"><b>'.$cliente_dir.'</b></td>
        </tr>

        <p align="justify"><i>Estimado(a)</i></p>

        <p align="justify"><i>Reciba un cordial saludo; el motivo de la presente comunicación es para informarle que estamos adjuntando un Recordatorio por la OBLIGACIÓN PENDIENTE QUE TIENE CON NUESTRA REPRESENTADA, de acuerdo a resoluciones emitidas por el área de cobranzas hasta la fecha no llegan a un acuerdo de pago favorable para ambas partes, por tal motivo hemos decidido también derivar ESTÁ NOTIFICACIÓN A SU DOMICILIO CONSIGNADO.</i></p>

        <p align="justify"><i>Hacemos de su conocimiento según contrato: El Deudor asume la obligación de pagar el presente préstamo, sus intereses, y penalidades en la misma moneda... En caso de no cancelar las cuotas en el día de su vencimiento, el deudor está obligado a pagar el importe de ellas más los intereses compensatorios y moratorios a las tasas máximas que para sus operaciones crediticias en mora tenga vigente el Banco Central de Reserva del Perú. Estos intereses se devengarán desde el día siguiente a la fecha de vencimiento de la obligación, inclusive hasta el día de su pago efectivo más las penalidades, gastos notariales y judiciales si los hubiere, además de ser reportado negativamente en las centrales de riesgo.</i></p>

        <p align="justify"><i>A fin de evitar incomodidades a su domicilio y a las personas que se encuentran en ella, REQUERIMOS que, en un plazo máximo de 72 horas, cumpla con la cancelación de sus obligaciones vencidas para evitar mayores molestias.</i></p>

        <p align="justify"><i>Cualquier consulta adicional sobre el particular o para convenir una solución a su actual situación, lo instamos a comunicarse con nosotros a la brevedad posible.</i></p>

        <p align="justify"><i>Sin otro particular, quedamos de usted.</i></p>

        <p></p><p></p><p></p>
        <tr>
          <td colspan="20"><b>Atentamente, <br/>
            Normalización de Créditos <br/>
            Celular: 951869875 / 945936439</b>
          </td>
        </tr>
      ';

      $array_html[$posicion] = $html;
      $array_codigos[$posicion] = 'ID-'.$value['temp_cobranza_id'].'-DIAS-'.$dias_atraso;
      $posicion ++;
    }
  }
$result = NULL;

class MYPDF extends TCPDF {

  public function Header() {
  }

  public function Footer() {
    global $array_codigos;
    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, '', 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $this->write1DBarcode($array_codigos[$this->getPage() - 1], 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'Av. Mariscal Nieto # 480 Ext. A-07 1er Piso Centro Comercial Boulevard -  Chiclayo', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  $pdf->setLanguageArray($l);

// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
for($i = 0; $i < count($array_html); $i++){
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTML($array_html[$i], true, 0, true, true);

  $pdf->Ln();
}

// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo = $codigo."_8974".$title.".pdf";
ob_end_clean();

$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+

?>