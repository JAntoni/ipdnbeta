<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Opcionesgc extends Conexion{

    function insertar($opcionesgc_nom, $opcionesgc_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_opcionesgc(tb_opcionesgc_nom, tb_opcionesgc_des)
          VALUES (:opcionesgc_nom, :opcionesgc_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_nom", $opcionesgc_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":opcionesgc_des", $opcionesgc_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function modificar($opcionesgc_id, $opcionesgc_nom, $opcionesgc_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_opcionesgc SET tb_opcionesgc_nom =:opcionesgc_nom, tb_opcionesgc_des =:opcionesgc_des WHERE tb_opcionesgc_id =:opcionesgc_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_id", $opcionesgc_id, PDO::PARAM_INT);
        $sentencia->bindParam(":opcionesgc_nom", $opcionesgc_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":opcionesgc_des", $opcionesgc_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }

    function mostrarUno($opcionesgc_id){
      try {
        $sql = "SELECT * FROM tb_opcionesgc WHERE tb_opcionesgc_id =:opcionesgc_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_id", $opcionesgc_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }
    function listar_opcionesgc(){
      try {
        $sql = "SELECT * FROM tb_opcionesgc WHERE tb_opcionesgc_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>