<?php
$fecha = new DateTime();
$fecha->modify('last day of this month');

$cuota_fec1 = date('01-m-Y');
$cuota_fec2 = $fecha->format('d-m-Y');

//datos para select de usuarios que pertenezcan al area legal
$usuario_columna = 'usu.tb_usuarioperfil_id';
$usuario_valor = 3; //usuarioperfil 3 Legal & AC
$param_tip = 'INT';
$empresa_id = 0;
//
?>

<style>
  .label-filter {
    padding-left: 1%;
    padding-right: 1%;
    font-size: 12pt;
  }

  .select-filter {
    width: 10%;
  }

  .badge-danger {
    background-color: #dc3545;
  }

  .badge-info {
    background-color: #0dcaf0;
  }

  .badge-warning {
    background-color: #ffc107;
  }

  .badge-primary {
    background-color: #568eec;
  }

  .badge-success {
    background-color: #47a447;
  }

  .badge-purple {
    background-color: #6610f2;
  }

  .badge-gray {
    background-color: #6c757d;
  }

  .badge-orange {
    background-color: #fd7e14;
  }

  .btn-outline-new-boton-rounded {
    background-color: #fff !important;
    border: 1px solid #3c8dbc !important;
    color: #2e6da4 !important;
    border-radius: 8px;
    transition: 0.7s;
  }

  .btn-outline-new-boton-rounded:hover {
    background-color: #3c8dbc !important;
    color: #fff !important;
    transition: 0.7s;
  }

  .btn-outline-new-boton-rounded:active {
    background-color: #2e6da4 !important;
    color: #fff !important;
  }

  .btn-outline-new-boton-rounded-active {
    background-color: #3c8dbc !important;
    border-color: 1px solid #fff;
    color: #fff !important;
    border-radius: 5px;
  }
</style>

<!-- FILTROS collapsed-box -->
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title text-primary">INFORMACIÓN</h3>
    <form id="form_chart_filtro" class="form-inline pull-right" role="form" style="margin-right: 30px;">
      <button type="button" class="btn btn-primary btn-sm" onclick="opcionesgc_form('I', 0)"><i class="fa fa-plus"> Opciones de Gestión</i></button>
    </form>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="container-fluid">
      <div class="row mb-4">
        <div class="col-md-9 col-sm-9 col-xs-10">
          <div class="info-box bg-aqua">
            <span class="info-box-icon bg-green" style="cursor: pointer;" onclick="carga_datos_cobranza()"><i class="fa fa-download"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Click en Descargar. Cargar lista de Cuotas para gestionar, esta lista debes ejecutarla cada vez que deseas actualizar las cuotas nuevas</span>
              <span class="info-box-number">Barra de Progreso</span>
              <div class="progress active" style="height: 20px; margin-top: 18px; margin-bottom: -10px;">
                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">40% Complete (success)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-2">
          <div class="spin_progreso" style="display: none;">
            <span class="info-box-icon bg-aqua"><i class="fa fa-refresh fa-spin"></i></span>
          </div>
        </div>
      </div>
      <div class="row mb-4">
        <div class="col-md-10 col-sm-2">
          <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
            <div class="container-fluid" style="padding: 2.4rem ;">
              <form id="form_fil_reportecreditomejorado" class="form-inline" role="form" method="POST">
                <input type='hidden' class="form-control" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $cuota_fec2; ?>" />

                <label for="fil_cmb_opcionesgc_id" class="control-label label-filter">Opción de GC:</label>
                <div class="form-group">
                  <select id="fil_cmb_opcionesgc_id" name="fil_cmb_opcionesgc_id" class="selectpicker form-control" data-live-search="true" data-max-options="1" data-size="12">
                    <?php require(VISTA_URL . 'reportecreditomejorado/opcionesgc_select.php'); ?>
                  </select>
                </div>
                <div id="pdp_dias" style="display: none;">
                  <label for="fil_cmb_pdp_dias" class="control-label label-filter">Elige Día:</label>
                  <div class="form-group select-filter">
                    <select id="fil_cmb_pdp_dias" name="fil_cmb_pdp_dias" class="selectpicker form-control" data-live-search="true" data-max-options="1" data-size="12">
                      <option value="">Todos los días</option>
                      <option value="0">PDP Hoy</option>
                      <option value="1">PDP hasta 1 día</option>
                      <?php
                      for ($i = 2; $i < 61; $i++) {
                        echo '<option value="' . $i . '">PDP hasta ' . $i . ' días</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <label for="fil_cmb_resultado_nom" class="control-label label-filter">Resultado:</label>
                <div class="form-group select-filter">
                  <select id="fil_cmb_resultado_nom" name="fil_cmb_resultado_nom" class="selectpicker form-control" data-live-search="true" data-max-options="1" data-size="12">
                    <option value="">Todos los resultados</option>
                    <option value="SI CUMPLIO">SI CUMPLIO</option>
                    <option value="ESPERANDO PDP">ESPERANDO PDP</option>
                    <option value="NO CUMPLIO">NO CUMPLIO</option>
                    <option value="ASIGNAR NUEVO PDP">ASIGNAR NUEVO PDP</option>
                  </select>
                </div>

                <label for="fil_cmb_encargado_id" class="control-label label-filter">Encargado:</label>
                <div class="form-group">
                  <select id="fil_cmb_encargado_id" name="fil_cmb_encargado_id" class="form-control">
                    <option value="0">Todos los encargados...</option>
                    <option value="11">HERBERT ENGEL GALVEZ ROJAS</option>
                    <option value="32">CARLOS AUGUSTO CONCEPCIÓN LEON</option>
                    <option value="42">GIORDANA MATILDE GARCIA SANTOS</option>
                    <option value="86">MARILIN LISBETH MOLINA AQUINO</option>
                    <?php require_once 'vista/usuario/usuario_select.php'; ?>
                  </select>
                </div>

                <div id="fil_ejecucion" style="display: none;">
                  <label for="cbo_ejecucion_confirm" class="control-label label-filter" style="padding-right: 3px;">Confirmacion:</label>
                  <div class="form-group select-filter">
                    <select id="cbo_ejecucion_confirm" name="cbo_ejecucion_confirm" class="selectpicker form-control">
                      <option value="0">TODOS</option>
                      <option value="1">CONFIRMADOS</option>
                      <option value="2">POR CONFIRMAR</option>
                    </select>
                  </div>
                </div>

                &nbsp; &nbsp; &nbsp; <button type="button" class="btn btn-info" onclick="temp_cobranza_lista()" style="margin-left: 10px;"><i class="fa fa-search"></i> Buscar</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
            <h5 style="margin-left: 10px;" class="mb-4"><b>Generar Cartas:</b></h5>
            <div class="container-fluid">
              <select id="fil_cmb_cartas" name="fil_cmb_cartas" class="form-control">
                <option value="0">Selecciona carta...</option>
                <option value="1">Carta Aviso</option>
                <option value="2">Carta Opcional</option>
              </select>
              <p></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row mb-4">
        <div class="col-md-6">
          <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
            <h5 style="margin-left: 10px;" class="mb-0"><b>Leyenda</b></h5>
            <div class="container-fluid no-padding">
              <div class="row">
                <div class="col-md-4">
                  <ul>
                    <li>
                      <span class="text-black">Cliente &nbsp;<i class="fa fa-hand-pointer-o" aria-hidden="true"></i></span>
                      <h6 class="text-black">Haz clic en el cliente para obtener más información de su crédito. </h6>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <ul>
                    <li>
                      <span class="text-black">Estados de cuota</span>
                      <p></p> <span class="badge badge-danger">Pendiente</span> <span class="badge bg-aqua">Pago Parcial</span> <span class="badge bg-green">Cancelado</span>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <ul>
                    <li>
                      <span class="text-black">FEC. ÚLTIMO PAGO</span> &nbsp; <span class="badge badge-danger">(dia - mes - año)</span>
                      <h6 class="text-black">no tiene la misma fecha de pago del crédito</h6>
                    </li>
                  </ul>
                </div>
                <!--
                  <div class="col-md-3">
                    <ul>
                      <li>
                        <span class="badge badge-danger">nombre de usuario</span>
                        <h6 class="text-black">se debe a que no ha tenido gestión </h6>
                      </li>
                    </ul>
                  </div>
                -->
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
            <h5 style="margin-left: 10px;" class="mb-4"><b>Filtrar por antigüedad de cuota</b></h5>
            <button type="button" id="chkbtn1" style="margin-left: 10px;" class="btn btn-outline-new-boton-rounded btn-outline-new-boton-rounded-active"> Todas </button> &nbsp;
            <button type="button" id="chkbtn2" class="btn btn-outline-new-boton-rounded"> 0 - 3 cuotas </button> &nbsp;
            <button type="button" id="chkbtn3" class="btn btn-outline-new-boton-rounded"> 4 - 9 cuotas </button> &nbsp;
            <button type="button" id="chkbtn4" class="btn btn-outline-new-boton-rounded"> 10 - 18 cuotas </button> &nbsp;
            <button type="button" id="chkbtn5" class="btn btn-outline-new-boton-rounded"> 19 a más </button>
            <input type="hidden" id="filterCuotas" name="filterCuotas" class="form-control" readonly="" value="0">
            <p></p>
          </div>
        </div>
        <div class="col-md-2">
          <div class="bg-info text-blue" style="padding: 8px; border-radius: 5px;">
            <h5 style="margin-left: 10px;" class="mb-4"><b>Filtrar por vencidas acumuladas</b></h5>
            <div class="container-fluid">
              <select name="cmb_filterdiasvenc" id="cmb_filterdiasvenc" class="form-control">
                <option value="0">Todos</option>
                <option value="1">Vigentes de 0 a 1 mes</option>
                <option value="2">Vencidas de 1 a 2 meses</option>
                <option value="3">Vencidas de 2 a 3 meses</option>
                <option value="4">Vencidas de 3 a más</option>
              </select>
              <p></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>