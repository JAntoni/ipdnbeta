var datatable_global;
var array_deudadiaria;
google.charts.load('current', {'packages':['corechart', 'bar']});


function carga_datos_cobranza(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php
  var cuota_fec2 = $('#txt_filtro_fec2').val()
  if(tipo_cambio <= 0){
    alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
    return false;
  }
  $.confirm({
    title: 'Descargar la lista de cuotas por Gestionar',
    icon: 'fa fa-download',
    theme: 'modern',
    content: 'Con esta opción se descargará toda la lista de cuotas de GARVEH, ASIVEH E HIPO para gestionar, se actualiza toda la inforamción al día de hoy, tomará un tiempo en descargar. ¿DESEA DESCARGAR LA DATA ACTUALIZADA?',
    type: 'blue',
    typeAnimated: true,
    buttons: {
      Aceptar: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"reportecredito/reportecredito_garveh_tabla.php", //EJECUTAMOS LA MISMA FUNCION QUE SE ENCUENTRA EN REPORTECREDITO, PARA USAR UN SOLO ARCHIVO
          async: true,
          dataType: "JSON",
          data: ({
            cuota_fec2: cuota_fec2,
            tipo_cambio: tipo_cambio,
            vista_modulo: 'reportecredito_mejorado'
          }),
          beforeSend: function() {
            $('.spin_progreso').show();
            $('#form_creditomenor_filtro').hide();
            $('.info-box-number').text('Barra de progreso hasta la fecha: ' + cuota_fec2);
            porcentaje = 0; segundos = 0;
            var intervalo = setInterval(function() {
              porcentaje += 3;
              segundos ++
      
              $('.progress-bar-striped').css('width', porcentaje+'%')
              console.log('aumenta: ' + porcentaje)
      
              if(segundos == 20)
                clearInterval(intervalo);
            }, 1000);
          },
          success: function(data){
            $('.progress-bar-striped').css('width', '100%')
            $('.spin_progreso').hide();
      
            if(parseInt(data.estado) == 1){
              temp_cobranza_lista();
            }
            else
              alerta_warning('AVISO', data.mensaje)
          },
          complete: function(data){
            //console.log(data)
          },
          error: function(data){
            console.log(data);
            alerta_error('ERROR', data.responseText)
          }
        });
      },
      Cerrar: function (){
      }
    }
  });
}

// --------------------------------------------- //

$('#chkbtn1').click(function (event) {
  event.preventDefault();
  $('#filterCuotas').val('0');
  $('#chkbtn1').addClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn2').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn3').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn4').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn5').removeClass('btn-outline-new-boton-rounded-active');
  setTimeout(() => {
    temp_cobranza_lista();
  }, 500);
});

$('#chkbtn2').click(function (event) {
  event.preventDefault();
  $('#filterCuotas').val('1');
  $('#chkbtn2').addClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn1').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn3').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn4').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn5').removeClass('btn-outline-new-boton-rounded-active');
  setTimeout(() => {
    temp_cobranza_lista();
  }, 500);
});

$('#chkbtn3').click(function (event) {
  event.preventDefault();
  $('#filterCuotas').val('2');
  $('#chkbtn3').addClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn1').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn2').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn4').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn5').removeClass('btn-outline-new-boton-rounded-active');
  setTimeout(() => {
    temp_cobranza_lista();
  }, 500);
});

$('#chkbtn4').click(function (event) {
  event.preventDefault();
  $('#filterCuotas').val('3');
  $('#chkbtn4').addClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn1').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn2').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn3').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn5').removeClass('btn-outline-new-boton-rounded-active');
  setTimeout(() => {
    temp_cobranza_lista();
  }, 500);
});

$('#chkbtn5').click(function (event) {
  event.preventDefault();
  $('#filterCuotas').val('4');
  $('#chkbtn5').addClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn1').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn2').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn3').removeClass('btn-outline-new-boton-rounded-active');
  $('#chkbtn4').removeClass('btn-outline-new-boton-rounded-active');
  setTimeout(() => {
    temp_cobranza_lista();
  }, 500);
});

// --------------------------------------------- //

function temp_cobranza_lista(){
  var filtro_cuotas      = $("#filterCuotas").val();
  var filtro_dias_atraso = $("#cmb_filterdiasvenc").val();

  if( filtro_cuotas == null )
  {
    filtro_cuotas = '0';
  }
  
  if( filtro_dias_atraso == null )
  {
    filtro_dias_atraso = '0';
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "reportecreditomejorado/reportecreditomejorado_tabla.php",
    async: true,
    dataType: "JSON",
    data: ({
      p_array_datos: $('#form_fil_reportecreditomejorado').serialize(),
      p_filtro_cuotas: filtro_cuotas,
      p_filtro_dias_atraso: filtro_dias_atraso
    }), 
    beforeSend: function (before) {
      $("#div_listar").empty();
      $('#span_cargando').text('Cargando datos...');
      $('#reportecredito_mensaje_tbl').show(300);
    },
    success: function (response) {
      console.log(response);
      if(parseInt(response.tabla.estado)==1)
      {
        $('#div_listar').html(response.tabla.drawtable);

        $('input[type="checkbox"].flat-yellow, input[type="radio"].flat-green').iCheck({
          checkboxClass: 'icheckbox_flat-yellow',
          radioClass: 'iradio_flat-yellow'
        });

        $('#lbltot07').html('S/ '+response.suma_monto_estado.suma_monto_cuota_soles_pendiente);
        $('#lbltot10').html('S/ '+response.suma_monto_estado.suma_monto_cuota_soles_pago_parcial);
        $('#lbltot13').html('S/ '+response.suma_monto_estado.suma_monto_cuota_soles_cancelado);
      }
      else // if(parseInt(response.tabla.estado)==0)
      {
        $('#div_listar').html(response.tabla.mensaje);
      }
    },
    complete: function (complete) {
      // estilos_datatable('tbl_comportamiento');
      estilos_datatable_mejorado();
      $('#reportecredito_mensaje_tbl').hide(300);
    },
    error: function (error) {
      $("#div_listar").html(""+error.responseText);
    },
  });
}

function mostrar_sumas_totales(data){
  console.log(data);
  // var suma_total_desembolsado = $('#hdd_suma_total_desembolsado').val();
  // var suma_total_capital_recuperar = $('#hdd_suma_total_capital_recuperar').val();
  // var suma_capital_recuperado = $('#hdd_suma_capital_recuperado').val();
  // var suma_total_facturado = $('#hdd_suma_total_facturado').val();
  // var suma_total_cobrado = $('#hdd_suma_total_cobrado').val();
  // var suma_total_por_cobrar = $('#hdd_suma_total_por_cobrar').val();

  $('#span_desembolsado').text('S/. ' + data.suma_total_desembolsado);
  $('#span_recuperado').text('S/. ' + data.suma_capital_recuperado);
  $('#span_por_recuperar').text('S/. ' + data.suma_total_capital_recuperar);

  $('#span_total_facturado').text('S/. ' + data.suma_total_facturado);
  $('#span_total_cobrado').text('S/. ' + data.suma_total_cobrado);
  $('#span_total_por_cobrar').text('S/. ' + data.suma_total_por_cobrar);
}

function estilos_datatable_mejorado(){
  datatable_global = $('#tbl_comportamiento').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    "bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    scrollY: 700,
    fixedColumns: {
      left: 5,
      right: 1
    },
    //"scrollY": "80vh",
    //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
    dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
    buttons: [
      { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
      { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
      { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
      { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
    ],
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
    /*drawCallback: function () {
      var sum_desembolsado = $('#tbl_comportamiento').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
      var sum_capital_rest_sol = $('#tbl_comportamiento').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles

      $('#total').html(sum);
    }*/
  });
}

function opcionesgc_form(usuario_act, opcionesgc_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      opcionesgc_id: opcionesgc_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_form').html(data);
      	$('#modal_registro_opcionesgc').modal('show');

      	modal_hidden_bs_modal('modal_registro_opcionesgc', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function opcionesgc_asignar_form(usuario_act, temp_cobranza_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_asignar_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      temp_cobranza_id: temp_cobranza_id,
      modulo_nombre: 'reportecreditomejorado'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_asignar_form').html(data);
      	$('#modal_registro_asignaropciongc').modal('show');
        modal_height_auto('modal_registro_asignaropciongc');
      	modal_hidden_bs_modal('modal_registro_asignaropciongc', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function opcionesgc_timeline(credito_tipo, credito_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_timeline.php",
    async: true,
		dataType: "html",
		data: ({
      credito_tipo: credito_tipo, // PUEDE SER: L, I, M , E
      credito_id: credito_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_timeline').html(data);
      	$('#modal_opcionesgc_timeline').modal('show');

      	modal_hidden_bs_modal('modal_opcionesgc_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
        modal_height_auto('modal_opcionesgc_timeline'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function valores_chart_deudadiaria(){
  var deudadiaria_fec1 = $('#txt_deudadiaria_fec1').val();
  var deudadiaria_fec2 = $('#txt_deudadiaria_fec2').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL+"reportecreditomejorado/opcionesgc_asignar_controller.php", //obtenemos los valores para el chart
    async: true,
    dataType: "JSON",
    data:({
      action: 'chart-deudadiaria',
      deudadiaria_fec1 : deudadiaria_fec1,
      deudadiaria_fec2 : deudadiaria_fec2
    }),
    beforeSend: function() {
    },
    success: function(data){
      //console.log(data);
      array_deudadiaria = data;
      google.charts.setOnLoadCallback(drawChart);
    },
    complete: function(data){
      
    },
    error: function(data){
    
    }
  });
}
function drawChart() {
  console.log(array_deudadiaria);
  var data = google.visualization.arrayToDataTable(array_deudadiaria);

  var view = new google.visualization.DataView(data);

  view.setColumns([0, //The "descr column"
    1, //Downlink column
    2,
    {
      calc: "stringify",
      sourceColumn: 1, // Create an annotation column with source column "1"
      type: "string",
      role: "annotation"
    }]
  );

  var columnWrapper = new google.visualization.ChartWrapper({
      chartType: 'ColumnChart',
      containerId: 'chart-deudadiaria',
      dataTable: view
  });

  columnWrapper.draw();
}

function generar_cartas(tipo_carta){
  var validar_combo_generar_cartas = parseInt($('#fil_cmb_cartas').val());
  if(validar_combo_generar_cartas != 0)
  {
    var chkds = $("input[name='txt_cliente_tip']:checkbox");

    if (chkds.is(":checked"))  {
  var temp_ids = '';
  // Selecciona los checkboxes estilizados con iCheck por su atributo "name"
  var checkboxes = $('input[name="txt_cliente_tip"].flat-yellow');

  if (checkboxes.length > 0){
    checkboxes.each(function() {
      if (this.checked) {
        temp_ids += $(this).val() + ',';
      }
    });
  }
  
  var url = '';
  if(tipo_carta == 1)
    url = 'http://ipdnsac.ipdnsac.com/vista/reportecreditomejorado/cartas_cobranza_pdf.php?tipo_carta=' + tipo_carta + '&temp_ids=' + temp_ids;
  if(tipo_carta == 2)
    url = 'http://ipdnsac.ipdnsac.com/vista/reportecreditomejorado/cartas_opcional_pdf.php?tipo_carta=' + tipo_carta + '&temp_ids=' + temp_ids;
  
    //var url = 'http://localhost/ipdnsac/vista/reportecreditomejorado/cartas_cobranza_pdf.php?tipo_carta=' + tipo_carta + '&temp_ids=' + temp_ids;
  // Ahora, valoresSeleccionados contiene los valores de los checkboxes marcados
  window.open(url, '_blank');
  console.log('tipo 33: ' + tipo_carta + ' // ' +temp_ids);
    } else {
      $('#fil_cmb_cartas').val(0);
      swal_warning('Advertencia','Para generar las cartas, debe seleccionar por lo menos uno de los checks de la lista',8000);
      return 0;
    }
  }
  else
  {
    return 0;
  }
}

function pago_cuotas(cliente_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vencimiento/vencimiento_tabla.php",
    async: true,
    dataType: "html",
    data:{
      hdd_fil_cli_id: cliente_id,
      vista: 'reportecreditomejorado'
    },
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
      $('#div_vencimiento_tabla').html('');
      $('#div_vencimiento_tabla').hide();
    },
    success: function (html) {
      $('#div_vencimiento_tabla').html(html);
      $('#div_vencimiento_tabla').show(500);
      $('#modal_mensaje').modal('hide');
    },
    complete: function () {
      $('#modal_cronograma_cuotas_cliente').modal('show');
      modal_width_auto('modal_cronograma_cuotas_cliente', 80);
      modal_height_auto('modal_cronograma_cuotas_cliente');
    }
  });
}

function deuda_acumulada_form(credito_id, creditotipo){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/deuda_acumulada_form.php",
    async: true,
		dataType: "html",
		data: ({
      credito_id: credito_id,
      creditotipo: creditotipo,
      action: 'L'
    }),
		beforeSend: function() {
		},
		success: function(data){
      $('#div_modal_deuda_acumulada').html(data)
      $('#modal_deuda_acumulada').modal('show')

      modal_width_auto('modal_deuda_acumulada', 80);
      modal_hidden_bs_modal('modal_deuda_acumulada', 'limpiar'); //funcion encontrada en public/js/generales.js
		},
		complete: function(data){
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function cliente_nota(idf)
{
	$.ajax({
		type: "POST",
		url: VISTA_URL + "cliente/clientenota_form.php",
		async:true,
		dataType: "html",
		data: ({
			cre_id:	idf,
			cre_tip: 3 //tipo de credito 3: garveh
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_clientenota_form').dialog("open");
			//$('#div_clientenota_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
			$('#div_clientenota_form').html(html);	
      $('#modal_cliente_nota').modal('show');
      modal_hidden_bs_modal('modal_cliente_nota', 'limpiar'); //funcion encontrada en public/js/generales.js
		}
	});
}

function creditogarveh_form(usuario_act, creditogarveh_id, proceso_id=0) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      creditogarveh_id: creditogarveh_id,
      proceso_id: proceso_id,
      vista: 'credito_tabla'
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (data) {
      // console.log(data);
        $('#modal_mensaje').modal('hide');
        if (data != 'sin_datos') {
          $('#div_info_credito').html(data);
          $('#modal_registro_creditogarveh').modal('show');

          //desabilitar elementos del form si es L (LEER)
          if (usuario_act == 'L'){
              form_desabilitar_elementos('for_cre');
          } //funcion encontrada en public/js/generales.js
          modal_hidden_bs_modal('modal_registro_creditogarveh', 'limpiar'); //funcion encontrada en public/js/generales.js
          modal_width_auto('modal_registro_creditogarveh',80);
          modal_height_auto('modal_registro_creditogarveh');
        } else {
          //llamar al formulario de solicitar permiso
          var modulo = 'reportecreditomejorado';
          var div = 'div_modal_creditogarveh_form';
          permiso_solicitud(usuario_act, creditogarveh_id, modulo, div); //funcion ubicada en public/js/permiso.js
        }
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      console.log(data.responseText);
    }
  });
}

function listar_segmentacion_embudo(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val());
  $.ajax({
    type: "POST",
    url: VISTA_URL+"reportecreditomejorado/reportecreditomejorado_segmentacion_embudo.php",
    async: true,
    dataType: "HTML",
    data: ({
      p_tipo_cambio: tipo_cambio
    }),
    beforeSend: function(data) {
      $('#div_segment_embudo').empty();
      $('#reportecredito_mensaje_tbl').show(300);
    },
    success: function(data){
      console.log(data);
      $('#div_segment_embudo').html(data);
      $('#reportecredito_mensaje_tbl').hide(300);
    },
    complete: function(data){
    },
    error: function(error){
      console.log(error);
      $('#reportecredito_mensaje_tbl').html('ERROR: ' + error.responseText);
    }
  });
}

function listar_fragment_embudo(numbergrafic,numberfragment,singestion,iniciomes){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"reportecreditomejorado/reportecreditomejorado_fragment_embudo.php",
    async: true,
    dataType: "HTML",
    data: ({
      p_numbergrafic: numbergrafic,
      p_numberfragment: numberfragment,
      p_singestion: singestion,
      p_iniciomes: iniciomes
    }),
    beforeSend: function(data) {
      console.log('enviando data: '+numbergrafic+'|'+numberfragment+'|'+singestion+'|'+iniciomes);
      $('#modal_mensaje').modal('show');
      $('#lista_fragment').empty();
    },
    success: function(data){
      console.log(data);
      $('#modal_mensaje').modal('hide');
      $('#lista_fragment').html(data);
      $('#modal_info-grafig-embudo01').modal('show');
      modal_height_auto('modal_info-grafig-embudo01');
    },
    complete: function(data){
      // estilos_datatable('table-fragment');
    },
    error: function(error){
      console.log(error);
      $('#reportecredito_mensaje_tbl').html('ERROR: ' + error.responseText);
    }
  });
}

function consultar_datos_comparacion_fragment_embudo(){
  var anio = $("#cmb_comparar_anio").val();
  var mes = $("#cmb_comparar_mes").val();
  
  if($.trim(mes) == "")
  {
    swal_warning("Advertencia","Debe seleccionar un mes a consultar",5000);
    return 0;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"reportecreditomejorado/reportecreditomejorado_fragment_embudo_comparacion.php",
    async: true,
    dataType: "HTML",
    data: ({
      p_anho: anio,
      p_mes: mes
    }),
    beforeSend: function(data) {
      console.log('enviando datos: : '+anio+' | '+mes);
      $('#modal_mensaje').modal('show');
      $('#lista_fragment_comparacion').empty();
    },
    success: function(data){
      console.log(data);
      $('#modal_mensaje').modal('hide');
      $('#lista_fragment_comparacion').html(data);
    },
    complete: function(data){
      
    },
    error: function(error){
      console.log(error);
      $('#reportecredito_mensaje_tbl').html('ERROR: ' + error.responseText);
    }
  });
}

$(document).ready(function() {

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //startDate: "-0d"
    //endDate : new Date()
  });

  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });
  
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_deudadiaria_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);

  });

  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_deudadiaria_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('#fil_cmb_opcionesgc_id').on('change', function() {
    $('#fil_ejecucion').css('display', 'none');
    $('#pdp_dias').css('display', 'none');
    var opcionesgc_id= $( "#fil_cmb_opcionesgc_id option:selected" ).val();
    if(parseInt(opcionesgc_id) == 1 || parseInt(opcionesgc_id) == 4)
      $('#pdp_dias').css('display', 'contents');
    else if (parseInt(opcionesgc_id) == 3){
      //daniel odar 14-10-2024
      $('#fil_ejecucion').css('display', 'contents');
    }
    
      console.log('opcn: ' +  opcionesgc_id);
  });

  $('#fil_cmb_cartas').on('change', function() {
    generar_cartas($(this).val());
  });

  //daniel odar 17-10-2024, require_once de usuarios, ocultar los que estén con estado(bloqeo) = 1
    $('#fil_cmb_encargado_id option[id*="option_cero"], #fil_cmb_encargado_id option[data-estado="1"]').remove();
  //

  $('#cmb_filterdiasvenc').on('change', function() {
    temp_cobranza_lista();
  });

  $('#cmb_filterdiasvenc').on('change', function() {
    temp_cobranza_lista();
  });
  
  temp_cobranza_lista();
  valores_chart_deudadiaria();
  listar_segmentacion_embudo();
});