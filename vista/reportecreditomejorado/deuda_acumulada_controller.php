<?php
require_once('../../core/usuario_sesion.php');
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$credito_id = $_POST['credito_id'];
$creditotipo = $_POST['creditotipo'];

$cuota_fec1 = '2001-01-01'; //todas las cuotas del inicio
$cuota_fec2 = date('Y-m-d');
$creditotipo_id = 0;
$tabla_credito = '';
$credito_est = '3,4,5,6,7,8,9,10';

if ($creditotipo == 'Crédito GARVEH') {
  $creditotipo_id = 3;
  $tabla_credito = 'tb_creditogarveh';
}
if ($creditotipo == 'Crédito ASIVEH') {
  $creditotipo_id = 2;
  $tabla_credito = 'tb_creditoasiveh';
}
if ($creditotipo == 'Crédito HIPOTECARIO') {
  $creditotipo_id = 4;
  $tabla_credito = 'tb_creditohipo';
}

$bandera = 0;
$data_show = '';
$array_dias = array();
$array_cuotas = array();
$array_pagos = array();
$cuota_fecha_inicio = '';

//echo $tabla_credito.' / '.$credito_id.' / '.$creditotipo.' / '.$creditotipo_id.' / '.$cuota_fec1.' / '.$cuota_fec2.' / '.$credito_est; exit();
//? LISTAMOS LAS CUOTAS FACTURADAS DEL CRÉDITO

$result = $oCuota->listar_cuotas_facturadas_credito_id($tabla_credito, $credito_id, $creditotipo_id, $cuota_fec1, $cuota_fec2, $credito_est);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $array_cuotas[mostrar_fecha($value['tb_cuota_fec'])] = formato_numero($value['tb_cuota_cuo']);
      $array_dias[mostrar_fecha($value['tb_cuota_fec'])] = 0;
    }
  }
  else
    $bandera = 1;
$result = NULL;


//? LISTAMOS TODOS LOS PAGOS REALIZADOS EN EL CRÉDITO
if ($bandera == 0) {
  $result = $oIngreso->ingresos_credito_id_rango($cuota_fec1, $cuota_fec2, $tabla_credito, $credito_id, $creditotipo_id);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $array_pagos[mostrar_fecha($value['tb_ingreso_fec'])] = formato_numero($value['tb_ingreso_imp']);
      $array_dias[mostrar_fecha($value['tb_ingreso_fec'])] = 0;
    }
  }
  $result = NULL;
}

if ($bandera == 0) {
  // Ordenar el array por las claves usando uksort y strtotime
  uksort($array_dias, function ($a, $b) {
    // Convertir las fechas en timestamps y compararlas
    return strtotime($a) - strtotime($b);
  });

  $deuda_acumulada = 0;
  foreach ($array_dias as $key => $value) {
    // $data_show .= $key .' / ';
    if (array_key_exists($key, $array_cuotas)) {
      $deuda_acumulada += formato_numero($array_cuotas[$key]);
      $array_dias[$key] = $deuda_acumulada;
    }
    if (array_key_exists($key, $array_pagos)) {
      $deuda_acumulada = formato_numero($deuda_acumulada - $array_pagos[$key]);
      $array_dias[$key] = $deuda_acumulada;
    }
  }

  echo json_encode($array_dias);
}
else{
  echo '';
}