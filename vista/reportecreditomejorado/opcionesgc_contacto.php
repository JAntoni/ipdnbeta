<?php
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();

  $cliente_id = intval($_POST['cliente_id']);

  $result = $oCliente->mostrarUno($cliente_id);
  if($result['estado'] == 1){
    $cliente_cel = str_replace(' ', '', $result['data']['tb_cliente_cel']);
    $cliente_tel = str_replace(' ', '', $result['data']['tb_cliente_tel']);
    $cliente_telref = str_replace(' ', '', $result['data']['tb_cliente_telref']);
    $cliente_procel = $result['data']['tb_cliente_procel'];
    $cliente_protel = $result['data']['tb_cliente_protel'];
    $cliente_protelref = $result['data']['tb_cliente_protelref'];
  }
  $result = NULL;
?>
<div class="modal modal-info fade" tabindex="-1" role="dialog" id="modal_cliente_contacto" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Actualizar Números de Contacto</h4>
      </div>
      <form id="" role="form" method="POST">
        <div class="modal-body">
          <div class="panel panel-primary shadow-simple">
            <div class="panel-body">
              <div class="form-group col-md-4">
                <input class="form-control input-sm" type="text" id="txt_numero_procel" name="txt_numero_procel" value="<?php echo $cliente_procel;?>" style="width: 100%;"><br>
                <input class="form-control input-sm" type="text" id="txt_numero_cel" name="txt_numero_cel" value="<?php echo $cliente_cel;?>" style="width: 100%;">
              </div>
              <div class="form-group col-md-4">
              <input class="form-control input-sm" type="text" id="txt_numero_protel" name="txt_numero_protel" value="<?php echo $cliente_protel;?>" style="width: 100%;"><br>
                <input class="form-control input-sm" type="text" id="txt_numero_tel" name="txt_numero_tel" value="<?php echo $cliente_tel;?>" style="width: 100%;">
              </div>
              <div class="form-group col-md-4">
                <input class="form-control input-sm" type="text" id="txt_numero_protelref" name="txt_numero_protelref" value="<?php echo $cliente_protelref;?>" style="width: 100%;"><br>
                <input class="form-control input-sm" type="text" id="txt_numero_telref" name="txt_numero_telref" value="<?php echo $cliente_telref;?>" style="width: 100%;">
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="button" class="btn btn-info" onclick="actualizar_numeros()">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>