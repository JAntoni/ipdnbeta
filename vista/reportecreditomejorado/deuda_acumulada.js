google.charts.load('current', {'packages':['corechart']});
var array_datos;

function obtener_array_deuda() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "reportecreditomejorado/deuda_acumulada_controller.php",
    async: true,
    dataType: "json",
    data: {
      credito_id: $('#deuda_credito_id').val(),
      creditotipo: $('#deuda_creditotipo').val()
    },
    success: function (data) {
      array_datos = data;
      google.charts.setOnLoadCallback(drawChart);
    },
    complete: function (data) {
    }
  });
}

function drawChart() {
  // Crear un array de arrays a partir del JSON
  var dataArray = [];
  for (var key in array_datos) {
    if (array_datos.hasOwnProperty(key)) {
      dataArray.push([key, parseFloat(array_datos[key])]);
    }
  }

  console.log('datos de la deuda: ')
  console.log(dataArray);
  // Crear la tabla de datos para el gráfico
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Fecha');
  data.addColumn('number', 'Deuda Acumulada');

  // Agregar los datos convertidos
  data.addRows(dataArray);

  // Definir las opciones del gráfico
  var options = {
    title: 'Acumulación de Deuda en el Tiempo',
    hAxis: {title: 'Fechas'},
    vAxis: {title: 'Deuda Acumulada ($)'},
    legend: 'none',  // No mostrar leyenda
    pointSize: 5,    // Tamaño de los puntos
    lineWidth: 2,    // Ancho de la línea
    width: 1500,      // Ancho del gráfico
    height: 400,     // Altura del gráfico
    chartArea: {width: '70%', height: '70%'} // Área del gráfico
  };

  // Crear el gráfico de dispersión y dibujarlo en el elemento con id "chart_div"
  var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}

$(document).ready(function () {
  console.log('deduda al dia 1111');
  obtener_array_deuda();
});
