<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  $oAsignar = new AsignarOpcionGc();
  require_once('../reportecreditomejorado/Gclinea.class.php');
  $oGclinea = new Gclinea();
  require_once('../reportecredito/ReporteCredito.class.php');
  $oReporte = new ReporteCredito();
  require_once('../cuotapago/Cuotapago.class.php');
  $oCuotapago = new Cuotapago();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../ejecucion/Ejecucion.class.php');
  $oEjecucion = new Ejecucion();
  require_once('../funciones/fechas.php');
  
  $action = $_POST['action'];

  if($action == 'insertar'){
    $cuotadetalle_id = intval($_POST['hdd_cuotadetalle_id']);
    $cliente_id = intval($_POST['hdd_cliente_id']);
    $fecha_hoy = date('Y-m-d');
    $credito_id = intval($_POST['hdd_credito_id']);
    $cmb_aplicar_todo = intval($_POST['cmb_aplicar_todo']); // 1 solo aplica a la cuota selaccionada, 2 aplica a todas las cuotas del cliente
    $opcionesgc_nom = $_POST['hdd_opcionesgc_nom'];

    $oAsignar->asignaropciongc_fecges = date('Y-m-d h:i:s');
    $oAsignar->tb_opcionesgc_id = intval($_POST['cmb_opcionesgc_id']);
    $oAsignar->comentario_des = trim($_POST['txt_comentario_des']);
    $oAsignar->opciongc_fechapdp = fecha_mysql($_POST['txt_opciongc_fechapdp']);
    $oAsignar->encargado_id = intval($_POST['cmb_encargado_id']); // 0 todos pueden ver, 11 engel, 32 carlos, 42 Gio
    $opciongc_fechapdp = $_POST['txt_opciongc_fechapdp'];
    if(intval($_POST['cmb_opcionesgc_id']) != 1 && intval($_POST['cmb_opcionesgc_id']) != 4){ //solo para PDP y VISITA se guarda una fecha
      $oAsignar->opciongc_fechapdp = '';
      $opciongc_fechapdp = '';
    }

    /* 
      ! ESTO ES SOLO PARA LOS CASOS EN QUE SE REALIZA UNA GESTION DESDE COBRANZATODOS, EN DONDE HAY CUOTAS NUEVAS QUE NO EXISTE EN EL TEMP COBRANZA, ES POR ELLO QUE PRIMERO
      ! SE DEBE REGISTRAR LA NUEVA CUOTA, Y LUEGO APLICAR LA GESTIÓN DE ACUERDO SI SE ASIGNA A TODOS O SE SOLO UNA CUOTA. 
    */
    $cuota_existe = 'SI';
    $resultPrincipal = $oReporte->temp_cobranza_lista_por_cliente($cliente_id, $cuotadetalle_id);
      if(intval($resultPrincipal['estado']) <= 0)
        $cuota_existe = 'NO';
    $resultPrincipal = NULL;

    if($cuota_existe == 'NO'){
      $result = $oAsignar->listar_asignar_opcionesgc_credito_cuotadetalle($credito_id, $cuotadetalle_id);
        $oAsignar->credito_id = $credito_id;
        $oAsignar->cuotadetalle_id = $cuotadetalle_id;

        if($result['estado'] == 1){
          //? SIGNIFICA QUE YA EXISTE UNA OPCION PARA ESTA CUOTA, PROCEDEMOS A MOFICAR LOS DATOS:
          $oAsignar->modificar_asginar_opciongc();
        }
        else{
          //? SIGNIFICA QUE NO HAY UN REGISTRO PARA ESTA CUOTA, PROCEDEMOS A REGISTRAR UN NUEVO
          $oAsignar->insertar_asginar_opciongc();
        }
      $result = NULL;
      
      if($cmb_aplicar_todo == 1){ //aplica solo a la cuota que no existe
        $data['estado'] = 1;
        $data['mensaje'] = 'NUEVA OPCIÓN. Gestión guardada para este crédito';

        $data['td_opciones'] = trim($opcionesgc_nom.' '.$opciongc_fechapdp);
        $data['td_comentario'] = trim($_POST['txt_comentario_des']); 
        $data['cuotadetalle_id'] = [$cuotadetalle_id];
        $data['encargado_id'] = intval($_POST['cmb_encargado_id']);
        
        echo json_encode($data);
        exit();
      }
    }

    $credito_tipo = $_POST['hdd_credito_tipo'];
    $array_creditos = array("Crédito Menor" => 1,"Crédito ASIVEH" => 2,"Crédito GARVEH" => 3, "Crédito HIPOTECARIO" => 4);
    $credito_tip = intval($array_creditos[$credito_tipo]);
    $usuario_id = intval($_SESSION['usuario_id']);
    $cliente_nom = $_POST['hdd_cliente_nom'];
    $gclinea_usureg = $usuario_id;

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar LA ASIGNACIÓN, REVISAR CON SISTEMAS. CLiente ID: '.$cliente_id.' | cuotadetalle id: '.$cuotadetalle_id;
    $array_cuotadetalle = [];

    //? SI EL CRITERIO DE APLICAR A TODOS LA MISMA GESTIÓN ES SELECCIONADA, LISTAMOS TODAS LAS CUOTAS DEL CRÉDITO PARA APLICAR LA MISMA GESTIÓN
    if($cmb_aplicar_todo == 2)
      $cuotadetalle_id = 0; // si cuota detalle es 0 se lista todas las cuotas del cliente y se le aplica la misma gestión para todas

    $resultPrincipal = $oReporte->temp_cobranza_lista_por_cliente($cliente_id, $cuotadetalle_id);
    $data['mensaje'] .= $resultPrincipal['mensaje'];
      if($resultPrincipal['estado'] == 1){
        foreach ($resultPrincipal['data'] as $key => $value) {
          $oAsignar->credito_id = $value['credito_id'];
          $oAsignar->cuotadetalle_id = $value['cuotadetalle_id'];
          
          $credito_id = $value['credito_id'];
          $cuotadetalle_id = $value['cuotadetalle_id'];
          $cuota_fecha = $value['cuota_fecha'];
          $credito_subtipo = $value['credito_subtipo'];
          $cuota_actual = $value['cuota_actual'];
          array_push($array_cuotadetalle, $cuotadetalle_id);

          $texto_pdp = '';
          if(intval($_POST['cmb_opcionesgc_id']) == 1)
            $texto_pdp = ' con Fecha de Compromiso para el <b>'.$_POST['txt_opciongc_fechapdp'].'</b>';

          $comentario = 'sin comentario adicional.';
          if(trim($_POST['txt_comentario_des']) != '')
            $comentario = 'comentario adicional: <b>'.trim($_POST['txt_comentario_des']).'.</b>';

          //* ANTES DE GUARDAR LA ASIGANACIÓN DE OPCIÓN PARA LA GC, VERIFICAMOS SI YA EXISTE UNA OPCION PARA LA CUOTA, EN CASO YA EXISTA SOLO MODIFICAMOS LOS DATOS
          $result = $oAsignar->listar_asignar_opcionesgc_credito_cuotadetalle($credito_id, $cuotadetalle_id);
            if($result['estado'] == 1){
              //? SIGNIFICA QUE YA EXISTE UNA OPCION PARA ESTA CUOTA, PROCEDEMOS A MOFICAR LOS DATOS:
              if($oAsignar->modificar_asginar_opciongc()){ //modificamos
                $data['estado'] = 1;
                $data['mensaje'] = 'OPCION ACTUALIZADA. Gestión guardada para este crédito';

                $gclinea_det = '<b>Actualizado</b> | He realizado una gestión para el cliente: <b>'.$cliente_nom.'</b> de su <b>'.$credito_subtipo.' ID: '.$credito_id.'</b>, cuota actual: <b>'.$cuota_actual.'</b>, fecha de cuota: <b>'.$cuota_fecha.'</b>. La gestión es de opción: <b>'.$opcionesgc_nom.'</b>'.$texto_pdp.', '.$comentario;
                $oGclinea->insertar_gclinea($gclinea_usureg, $credito_tip, $credito_id, $gclinea_det);

                $arr = (array) json_decode($_POST['hdd_arr_ejecucion']);
                if ($oAsignar->encargado_id != $arr['ejecucion_encargado_id']) {
                  $oEjecucion->modificar_campo($arr['ejecucion_id'], 'tb_ejecucion_usuencargado', $oAsignar->encargado_id, 'INT');
                }
              }
            }
            else{
              //? SIGNIFICA QUE NO HAY UN REGISTRO PARA ESTA CUOTA, PROCEDEMOS A REGISTRAR UN NUEVO
              if($oAsignar->insertar_asginar_opciongc()){ //registramos uno nuevo
                $data['estado'] = 1;
                $data['mensaje'] = 'NUEVA OPCIÓN. Gestión guardada para este crédito';
                
                $gclinea_det = '<b>Nuevo</b> | He realizado una gestión para el cliente: <b>'.$cliente_nom.'</b> de su <b>'.$credito_subtipo.' ID: '.$credito_id.'</b>, cuota actual: <b>'.$cuota_actual.'</b>, fecha de cuota: <b>'.$cuota_fecha.'</b>. La gestión es de opción: <b>'.$opcionesgc_nom.'</b>'.$texto_pdp.', '.$comentario;
                $oGclinea->insertar_gclinea($gclinea_usureg, $credito_tip, $credito_id, $gclinea_det);
              }
            }
          $result = NULL;
        }
      }
    $resultPrincipal = NULL;

    $data['td_opciones'] = trim($opcionesgc_nom.' '.$opciongc_fechapdp);
    $data['td_comentario'] = trim($_POST['txt_comentario_des']); 
    $data['cuotadetalle_id'] = $array_cuotadetalle;
    $data['encargado_id'] = intval($_POST['cmb_encargado_id']);

    echo json_encode($data);
  }
  else if($action == 'chart-deudadiaria'){
    $columnas = array('Deuda Diaria', 'Apertura Deuda S/.', 'Cierre Deuda S/.');
    $array_valores = array();
    $deudadiaria_fec1 = fecha_mysql($_POST['deudadiaria_fec1']);
    $deudadiaria_fec2 = fecha_mysql($_POST['deudadiaria_fec2']);

    $array_cronograma = cronograma_fechas($deudadiaria_fec1, $deudadiaria_fec2);
    $array_deudadiaria = array();
    $array_cobrado = array();

    $result = $oAsignar->listar_deudadiaria($deudadiaria_fec1, $deudadiaria_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          // $nuevo_array = array(mostrar_fecha($value['tb_deudadiaria_fec']), floatval($value['tb_deudadiaria_mon']));
          // $array_valores[] = $nuevo_array;
          $array_deudadiaria[$value['tb_deudadiaria_fec']] = floatval($value['tb_deudadiaria_mon']);
        }
      }
    $result = NULL;

    $result = $oCuotapago->cuotas_pagadas_garveh_hipo($deudadiaria_fec1, $deudadiaria_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $array_cobrado[$value['tb_ingreso_fec']] = floatval($value['cuota_real']);
        }
      }
    $result = NULL;
    
    foreach ($array_cronograma as $fecha) {
      $deuda = floatval($array_deudadiaria[$fecha]);
      $cobrado = floatval($array_cobrado[$fecha]);
      $saldo = 0;

      if($deuda >= $cobrado)
        $saldo = $deuda - $cobrado;
      else
        $saldo = $cobrado;
      
      $nuevo_array = array(mostrar_fecha($fecha), floatval($deuda), floatval($saldo));
      $array_valores[] = $nuevo_array;
    }
    array_unshift($array_valores, $columnas); // para agregar el array de columnas al principio del array completo

    echo json_encode($array_valores);
  }
  else if($action == 'actualizar_numeros'){
    $cliente_id = intval($_POST['cliente_id']);

    $cliente_cel = $_POST['cliente_cel'];
    $cliente_tel = $_POST['cliente_tel'];
    $cliente_telref = $_POST['cliente_telref'];
    $cliente_procel = $_POST['cliente_procel'];
    $cliente_protel = $_POST['cliente_protel'];
    $cliente_protelref = $_POST['cliente_protelref'];

    $oCliente->modificar_campo($cliente_id, 'tb_cliente_cel', $cliente_cel, 'STR');
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $cliente_tel, 'STR');
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_telref', $cliente_telref, 'STR');
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_procel', $cliente_procel, 'STR');
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_protel', $cliente_protel, 'STR');
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_protelref', $cliente_protelref, 'STR');

    $data['estado'] = 1;
    $data['mensaje'] = 'Números de contacto actualizados correctamente';

    echo json_encode($data);
  }
  else if ($action == 'crear_ejecucion'){
    $data['estado']   = 0;
    $data['mensaje']  = "error al $action";
    $cgarvtipo_id = $_POST['cgarvtipo_ejec'];

    $arr = (array) json_decode($_POST['arr_ejecucion']);

    //si el credito actual tiene un idpadre, buscar si hay más padres hasta dar con el original de donde se obtienen la EP y otros documentos
      $credito_id_padreorigen = $arr['credito_id_padreorigen'];
      if (!empty($credito_id_padreorigen)) {
        $oEjecucion->credito_id_padreorigen = $credito_id_padreorigen;
      }
      
      if (!empty($credito_id_padreorigen)) {
        require_once '../creditogarveh/Creditogarveh.class.php';
        $oCredito = new Creditogarveh();
        while (!empty($credito_id_padreorigen)) {//buscar si tiene más padres
          $res_credito_padre = $oCredito->mostrarUno($credito_id_padreorigen);
          $credito_id_padreorigen = ($res_credito_padre['estado'] == 1) ? $res_credito_padre['data']['tb_credito_idori'] : null;

          if (!empty($credito_id_padreorigen)) {
            $oEjecucion->credito_id_padreorigen = $credito_id_padreorigen;
          }
        }
      }
    //

    $oEjecucion->cliente_id     = $arr['cliente_id'];
    $oEjecucion->credito_id     = $arr['credito_id'];
    $oEjecucion->cgarvtipo_id   = $cgarvtipo_id;
    $oEjecucion->cgvsubtipo_id  = $oEjecucion->buscar_cgvsubtipo_por('', $cgarvtipo_id, $arr['tiene_adendas'])['data'][0]['tb_cgvsubtipo_id'];
    $oEjecucion->estadoejecucion_cgvsubtipo_id = $oEjecucion->buscar_estadoejecucion_cgvsubtipo_por('', $oEjecucion->cgvsubtipo_id, '', 1)['data'][0]['tb_estadoejecucion_cgvsubtipo_id'];
    $oEjecucion->moneda_id = $arr['moneda_id'];
    $oEjecucion->usuencargado = $_POST['usu_encargado_id'];
    $oEjecucion->usureg = $_SESSION['usuario_id'];
    $res = $oEjecucion->insertar();

    if ($res['estado'] == 1) {
      $data['estado'] = 1;
      $data['mensaje'] = "Se creó el proceso de ejecución legal. Modulo de Legal->Proceso de Ejecución";

      //se debe actualizar el usuario y fecha de aprobacion
      $oAsignar->modificar_campo_asignar_opciongc($arr['credito_id'], $arr['cuotadetalle_id'], 'tb_asignaropciongc_usuapr', $_SESSION['usuario_id'], 'INT');
      $oAsignar->modificar_campo_asignar_opciongc($arr['credito_id'], $arr['cuotadetalle_id'], 'tb_asignaropciongc_fecapr', date("Y-m-d H:i:s"), 'STR');
      if (intval($oEjecucion->usuencargado) != intval($arr['responsable_asignado_orig'])){
        $oAsignar->modificar_campo_asignar_opciongc($arr['credito_id'], $arr['cuotadetalle_id'], 'encargado_id', intval($oEjecucion->usuencargado), 'INT');
      }

      $ejecucion_id = $res['nuevo'];
      $estadoejecucion_cgvsubtipo_id = $oEjecucion->estadoejecucion_cgvsubtipo_id;
      $ejecucionfase_orden = 1;
      $ejecucionfase_obs = '';
      $ejecucionfase_usureg = $_SESSION['usuario_id'];

      $res1 = $oEjecucion->insertar_ejecucionfase($ejecucion_id, $estadoejecucion_cgvsubtipo_id, $ejecucionfase_orden, $ejecucionfase_obs, $ejecucionfase_usureg);

      require_once('../historial/Historial.class.php');
      $oHistorial = new Historial();
      $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
      $oHistorial->setTbHistNomTabla('tb_ejecucionfase');
      $oHistorial->setTbHistRegmodid($res1['nuevo']);
      $oHistorial->setTbHistDet("Asignó el caso de ejecucion legal a {$_POST['usu_encargado']} | <b>". date("d-m-Y h:i a").'</b>');
      $oHistorial->insertar();
    }

    echo json_encode($data);
  }
  else
    echo 'Ninguna opción de operación detectada: '.$action;
?>