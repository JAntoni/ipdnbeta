<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  $oAsignar = new AsignarOpcionGc();
  require_once('../reportecreditomejorado/Gclinea.class.php');
  $oGclinea = new Gclinea();
  require_once('../funciones/fechas.php');
  
  $action = $_POST['action'];

  if($action == 'insertar'){
    $oAsignar->credito_id = intval($_POST['hdd_credito_id']);
    $oAsignar->cuotadetalle_id = intval($_POST['hdd_cuotadetalle_id']);
    $oAsignar->tb_opcionesgc_id = intval($_POST['cmb_opcionesgc_id']);
    //$oAsignar->resultado_nom = ''; //EL RESULTADO DEPENDE DE LA COLUMNA OPCIONES, PARA DETERMINAR SI SE CUMPLIÓ CON LO GESTIONADO
    $oAsignar->comentario_des = trim($_POST['txt_comentario_des']); 
    $oAsignar->opciongc_fechapdp = fecha_mysql($_POST['txt_opciongc_fechapdp']);
    $oAsignar->encargado_id = intval($_POST['cmb_encargado_id']); // 0 todos pueden ver, 11 engel, 32 carlos

    $opciongc_fechapdp = $_POST['txt_opciongc_fechapdp'];
    if(intval($_POST['cmb_opcionesgc_id']) != 1 && intval($_POST['cmb_opcionesgc_id']) != 4){ //solo para PDP y VISITA se guarda una fecha
      $oAsignar->opciongc_fechapdp = '';
      $opciongc_fechapdp = '';
    }

    $usuario_id = intval($_SESSION['usuario_id']);
    $credito_id = intval($_POST['hdd_credito_id']);
    $cuotadetalle_id = intval($_POST['hdd_cuotadetalle_id']);
    $cliente_nom = $_POST['hdd_cliente_nom'];
    $cuota_fecha = $_POST['hdd_cuota_fecha'];
    $credito_tipo = $_POST['hdd_credito_tipo'];
    $credito_subtipo = $_POST['hdd_credito_subtipo'];
    $cuota_actual = $_POST['hdd_cuota_actual'];
    $opcionesgc_nom = $_POST['hdd_opcionesgc_nom'];
    

    $array_creditos = array("Crédito Menor" => 1,"Crédito ASIVEH" => 2,"Crédito GARVEH" => 3, "Crédito HIPOTECARIO" => 4);
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar LA ASIGNACIÓN, REVISAR CON SISTEMAS.';

    $texto_pdp = '';
    if(intval($_POST['cmb_opcionesgc_id']) == 1)
      $texto_pdp = ' con Fecha de Compromiso para el <b>'.$_POST['txt_opciongc_fechapdp'].'</b>';

    $comentario = 'sin comentario adicional.';
    if(trim($_POST['txt_comentario_des']) != '')
      $comentario = 'comentario adicional: <b>'.trim($_POST['txt_comentario_des']).'.</b>';

    $gclinea_usureg = $usuario_id; 
    $credito_tip = intval($array_creditos[$credito_tipo]);

    //* ANTES DE GUARDAR LA ASIGANACIÓN DE OPCIÓN PARA LA GC, VERIFICAMOS SI YA EXISTE UNA OPCION PARA LA CUOTA, EN CASO YA EXISTA SOLO MODIFICAMOS LOS DATOS
    $result = $oAsignar->listar_asignar_opcionesgc_credito_cuotadetalle($credito_id, $cuotadetalle_id);
      if($result['estado'] == 1){
        //? SIGNIFICA QUE YA EXISTE UNA OPCION PARA ESTA CUOTA, PROCEDEMOS A MOFICAR LOS DATOS:
        if($oAsignar->modificar_asginar_opciongc()){ //modificamos
          $data['estado'] = 1;
          $data['mensaje'] = 'OPCION ACTUALIZADA. Gestión guardada para este crédito';

          $gclinea_det = '<b>Actualizado</b> | He realizado una gestión para el cliente: <b>'.$cliente_nom.'</b> de su <b>'.$credito_subtipo.' ID: '.$credito_id.'</b>, cuota actual: <b>'.$cuota_actual.'</b>, fecha de cuota: <b>'.$cuota_fecha.'</b>. La gestión es de opción: <b>'.$opcionesgc_nom.'</b>'.$texto_pdp.', '.$comentario;
          $oGclinea->insertar_gclinea($gclinea_usureg, $credito_tip, $credito_id, $gclinea_det);
        }
      }
      else{
        //? SIGNIFICA QUE NO HAY UN REGISTRO PARA ESTA CUOTA, PROCEDEMOS A REGISTRAR UN NUEVO
        if($oAsignar->insertar_asginar_opciongc()){ //registramos uno nuevo
          $data['estado'] = 1;
          $data['mensaje'] = 'NUEVA OPCIÓN. Gestión guardada para este crédito';
          
          $gclinea_det = '<b>Nuevo</b> | He realizado una gestión para el cliente: <b>'.$cliente_nom.'</b> de su <b>'.$credito_subtipo.' ID: '.$credito_id.'</b>, cuota actual: <b>'.$cuota_actual.'</b>, fecha de cuota: <b>'.$cuota_fecha.'</b>. La gestión es de opción: <b>'.$opcionesgc_nom.'</b>'.$texto_pdp.', '.$comentario;
          $oGclinea->insertar_gclinea($gclinea_usureg, $credito_tip, $credito_id, $gclinea_det);
        }
      }
    $result = NULL;

    $data['td_opciones'] = trim($opcionesgc_nom.' '.$opciongc_fechapdp);
    $data['td_comentario'] = trim($_POST['txt_comentario_des']); 
    $data['cuotadetalle_id'] = $cuotadetalle_id;
    $data['encargado_id'] = intval($_POST['cmb_encargado_id']);

    echo json_encode($data);
  }
  else if($action == 'chart-deudadiaria'){
    $columnas = array('Deuda Diaria', 'Monto Deuda S/.');
    $array_valores = array();
    $deudadiaria_fec1 = fecha_mysql($_POST['deudadiaria_fec1']);
    $deudadiaria_fec2 = fecha_mysql($_POST['deudadiaria_fec2']);

    $result = $oAsignar->listar_deudadiaria($deudadiaria_fec1, $deudadiaria_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $nuevo_array = array(mostrar_fecha($value['tb_deudadiaria_fec']), floatval($value['tb_deudadiaria_mon']));
          $array_valores[] = $nuevo_array;
        }
      }
    $result = NULL;

    array_unshift($array_valores, $columnas); // para agregar el array de columnas al principio del array completo

    echo json_encode($array_valores);
  }
  else
    echo 'Ninguna opción de operación detectada: '.$action;
?>