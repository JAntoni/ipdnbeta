<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/Gclinea.class.php');
  $oGclinea = new Gclinea();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'opcionesgc';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];

  $array_creditos = array("Crédito Menor" => 1,"Crédito ASIVEH" => 2,"Crédito GARVEH" => 3, "Crédito HIPOTECARIO" => 4);
  $credito_tipo_nombre = $_POST['credito_tipo']; // en formade Crédito Menor, etc
  $credito_id = intval($_POST['credito_id']);
  $credito_tip = intval($array_creditos[$credito_tipo_nombre]);

  $titulo = 'Historial de Gestión del Crédito';

  $timeline = '';
  $result = $oGclinea->listar_gclinea($credito_tip, $credito_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $timeline .= '
          <li>
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($value['tb_gclinea_reg']).'</span>
              <div class="user-block">
                <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="Usuario imagen">
                <span class="username"><a href="#">'.$value['tb_usuario_nom'].'</a></span>
                <span class="description">'.$value['tb_gclinea_det'].'</span>
              </div>
            </div>
          </li>
        ';
      }
    }
  $result = NULL;
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_opcionesgc_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      
      <div class="modal-body">
        <ul class="timeline">
          <?php echo $timeline;?>
        </ul>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>

    </div>
  </div>
</div>