<?php
  //require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../ubigeo/Ubigeo.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oReporteCredito = new ReporteCredito();
  $oAsignar = new AsignarOpcionGc();
  $oUbigeo = new Ubigeo();

  $fecha_hoy = date('Y-m-d');
  $datetime = new DateTime($fecha_hoy);
  $datetime = $datetime->modify('-1 month');
  $fecha_periodo = $datetime->format('m/Y');

  $result = $oReporteCredito->temp_cobranza_infocorp_lista();
    if($result['estado'] == 1){
      $return['estado'] = 1;
      foreach ($result['data'] as $key => $value) {
        $departamento = '';
        $provincia = '';
        $distrito = '';

        $result2 = $oUbigeo->mostrarUbigeo($value['tb_ubigeo_cod']);
          if($result2['estado'] == 1){
            $departamento = $result2['data']['Departamento'];
            $provincia = $result2['data']['Provincia'];
            $distrito = $result2['data']['Distrito'];
          }
        $result2 = NULL;
        
        $tipo_persona = 1; // 1 se refiere a persona natural, 2 persona jurídica
        $tipo_documento = 1; // 1 se refiere a tipo DNI y 2 a tipo RUC
        $num_documento = $value['representante_dni'];
        $nombre_cliente = $value['representante_nom'];
        
        $deuda_vigente_soles = '';
        $deuda_menor_soles = '';
        $deuda_mayor_soles = '';
        $deuda_vigente_dolares = '';
        $deuda_menor_dolares = '';
        $deuda_mayor_dolares = '';
        $deuda_indirecta_avales_soles = '';
        $deuda_indirecta_avales_dolares = '';

        if($value['lista'] == 1){ // lista de clientes como tal, ya sean persona natural o empresa
          $numero_caracteres = strlen(trim($value['cliente_dni']));
          if($numero_caracteres > 8){
            $tipo_documento = 2; //cliente con RUC
            $tipo_persona = 2; // cliente jurídica
          }
          
          $num_documento = $value['cliente_dni'];
          $nombre_cliente = $value['cliente_nom'];

          if(intval($value['dias_atraso']) <= 0){
            if($value['moneda'] == 'S/.')
              $deuda_vigente_soles = $value['capital_restante'];
            else
              $deuda_vigente_dolares = $value['capital_restante'];
          }
          if(intval($value['dias_atraso']) > 0 && intval($value['dias_atraso']) <= 30){
            if($value['moneda'] == 'S/.')
              $deuda_menor_soles = $value['capital_restante'];
            else
              $deuda_menor_dolares = $value['capital_restante'];
          }
          if(intval($value['dias_atraso']) > 30){
            if($value['moneda'] == 'S/.')
              $deuda_mayor_soles = $value['capital_restante'];
            else
              $deuda_mayor_dolares = $value['capital_restante'];
          }
        }

        if($value['lista'] == 2){ //lista de solo representantes de las empresas como clientes
          if($value['moneda'] == 'S/.')
            $deuda_indirecta_avales_soles = $value['capital_restante'];
          else
            $deuda_indirecta_avales_dolares = $value['capital_restante'];
        }

        $calificacion = '';

        if($value['credito_tipo'] == 'Crédito HIPOTECARIO'){
          if(intval($value['dias_atraso']) <= 30)
            $calificacion = 0;
          if(intval($value['dias_atraso']) > 30 && intval($value['dias_atraso']) <= 60)
            $calificacion = 1;
          if(intval($value['dias_atraso']) > 60 && intval($value['dias_atraso']) <= 120)
            $calificacion = 2;
          if(intval($value['dias_atraso']) > 120 && intval($value['dias_atraso']) <= 365)
            $calificacion = 3;
          if(intval($value['dias_atraso']) > 365)
            $calificacion = 4;
        }
        else{
          if(intval($value['dias_atraso']) <= 8)
            $calificacion = 0;
          if(intval($value['dias_atraso']) > 8 && intval($value['dias_atraso']) <= 30)
            $calificacion = 1;
          if(intval($value['dias_atraso']) > 30 && intval($value['dias_atraso']) <= 60)
            $calificacion = 2;
          if(intval($value['dias_atraso']) > 60 && intval($value['dias_atraso']) <= 120)
            $calificacion = 3;
          if(intval($value['dias_atraso']) > 120)
            $calificacion = 4;
        }

        $cliente_cel = trim($value['cliente_cel']);
        $cliente_cel = str_replace(' ', '', $cliente_cel);

        $data .= '
          <tr>
            <td>'.$fecha_periodo.'</td>
            <td>080515</td>
            <td></td>
            <td></td>
            <td></td>
            <td>'.$tipo_documento.'</td>
            <td>'.$num_documento.'</td>
            <td>'.$nombre_cliente.'</td>
            <td></td>
            <td></td>
            <td></td>
            <td>'.$tipo_persona.'</td>
            <td>5</td>
            <td>'.$deuda_vigente_soles.'</td>
            <td></td>
            <td>'.$deuda_menor_soles.'</td>
            <td>'.$deuda_mayor_soles.'</td>
            <td></td>
            <td>'.$deuda_indirecta_avales_soles.'</td>
            <td></td>
            <td></td>
            <td></td>
            <td>'.$deuda_vigente_dolares.'</td>
            <td></td>
            <td>'.$deuda_menor_dolares.'</td>
            <td>'.$deuda_mayor_dolares.'</td>
            <td></td>
            <td>'.$deuda_indirecta_avales_dolares.'</td>
            <td></td>
            <td></td>
            <td></td>
            <td>'.$calificacion.'</td>
            <td>'.$value['dias_atraso'].'</td>
            <td>'.$value['cliente_dir'].'</td>
            <td>'.$distrito.'</td>
            <td>'.$provincia.'</td>
            <td>'.$departamento.'</td>
            <td>'.$cliente_cel.'</td>
          </tr>
        ';
      }
    }
    else{
      $return['estado'] = 0;
      $data = '
        <tr>
          <td colspan="23">'.$result['mensaje'].'</td>
        </tr>
      ';
    }
  $result = NULL;
  
  $alerta_deudadiaria = '';

  $return['tabla'] = $data;
  
  echo json_encode($return);
?>