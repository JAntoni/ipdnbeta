<?php
error_reporting(0);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once('../reportecredito/ReporteCredito.class.php');
$oReporteCredito = new ReporteCredito();

$title = 'Cartas de Cobranza';
$codigo = 'CARTAS-000008';

$tipo_carta = trim($_GET['tipo_carta']);
$temp_cobranza_ids = trim($_GET['temp_ids']);
$temp_cobranza_ids = substr($temp_cobranza_ids, 0, -1);

$array_html = [];
$array_codigos = [];
$posicion = 0;

//? DESDE AQUÍ EMPIEZA LA ESTRUCTURA PARA LAS CARTAS DE LOS CLIENTES
$result = $oReporteCredito->temp_cobranza_por_ids($temp_cobranza_ids);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $cliente_nom = $value['cliente_nom'];
      $cliente_dir = $value['cliente_dir'];
      $representante_nom = $value['representante_nom'];
      $dias_atraso = $value['dias_atraso'];
      $tipo_cliente = 1; //persona natural
      $html = '';

      if($representante_nom == '')
        $tipo_cliente = 2; // persona juridica

      //? DOCUMENTO PARA TRAMO DE 1 A 15 DÍAS
      if($dias_atraso <= 15){
        $html = '
          <tr>
            <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
            <td colspan="15" style="text-align: center;"></td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="12" style="text-align: center; font-size: 55px;">
              <b><u>RECORDATORIO DE PAGO</u></b>
            </td>
          </tr>
          <br/><br/><br/>
          <tr>
            <td colspan="20"><b>'.$cliente_nom.'</b></td>
          </tr>
          <tr>
            <td colspan="20"><b>'.$representante_nom.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20"><b>'.$cliente_dir.'</b></td>
          </tr>

          <p align="justify">Estimado Cliente, Le saludamos cordialmente y le comunicamos que su crédito se muestra vencido, Nos interesa saber el motivo por el cual no ha cumplido con su compromiso de pago, En tal sentido deseamos no causarle mayores molestias y le pedimos pase a nuestras oficinas a informarnos sobre su problema e indicarnos la forma que pagará el importe de su atraso.</p>

          <p align="justify"><i>Cumplimos con informarle que conforme a lo establecido en las cláusulas de su contrato; En caso que incumpliese con el pago de su cuota mensual, pasado (01) UN DÍA de la fecha asignada según su cronograma de pagos, el cliente está obligado a pagar por concepto de penalidad.</i></p>

          <p align="justify">Cualquier consulta adicional sobre el particular o para convenir una solución a su actual situación, lo instamos a comunicarse con nosotros a la brevedad posible.</p>

          <p align="justify">Sin otro particular, quedamos de usted.</p>

          <p></p><p></p><p></p>
          <tr>
            <td colspan="20">Atentamente, <br/>
              Departamento de cobranza <br/>
              Carlos Concepción <br/>
              Celular. 945936439
            </td>
          </tr>
        ';
      }
      //? DOCUMENTO PARA TRAMO DE 16 A 30 DÍAS
      if($dias_atraso >= 16 && $dias_atraso <= 30){
        $html = '
          <tr>
            <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
            <td colspan="15" style="text-align: center;"></td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="12" style="text-align: center; font-size: 55px;">
              <b><u>RECORDATORIO DE PAGO</u></b>
            </td>
          </tr>
          <br/><br/><br/>
          <tr>
            <td colspan="20"><b>'.$cliente_nom.'</b></td>
          </tr>
          <tr>
            <td colspan="20"><b>'.$representante_nom.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20"><b>'.$cliente_dir.'</b></td>
          </tr>

          <p align="justify">Estimado Cliente, Le saludamos cordialmente y le comunicamos que ya tiene <b>'.$dias_atraso.'</b> días de atraso y no ha regularizado aun, recuerde que este atraso le estará generando interés moratorios y penalidades además de ser reportado negativamente en las centrales de riesgo de acuerdo al contrato que firmo al otorgarle el crédito. Por tanto, Sr(a). lo esperamos a la brevedad posible para su pago y evite molestias futuras.</p>

          <p align="justify">Cualquier consulta adicional sobre el particular o para convenir una solución a su actual situación, lo instamos a comunicarse con nosotros. </p>

          <p align="justify">Sin otro particular, quedamos de usted.</p>

          <p></p><p></p><p></p>
          <tr>
            <td colspan="20">Atentamente, <br/>
              Departamento de cobranza <br/>
              Carlos Concepción <br/>
              Celular. 945936439
            </td>
          </tr>
        ';
      }
      //? DOCUMENTO PARA TRAMO DE 31 A 45 DÍAS
      if($dias_atraso >= 31 && $dias_atraso <= 45) {
        $html = '
          <tr>
            <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
            <td colspan="15" style="text-align: center;"></td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="12" style="text-align: center; font-size: 55px;">
              <b><u>INCUMPLIMIENTO DE CONTRATO</u></b>
            </td>
          </tr>
          <br/><br/><br/>
          <tr>
            <td colspan="20"><b>'.$cliente_nom.'</b></td>
          </tr>
          <tr>
            <td colspan="20"><b>'.$representante_nom.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20"><b>'.$cliente_dir.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20">Estimado(a)</td>
          </tr>

          <p align="justify">Reciba un cordial saludo; lo estamos visitando por el motivo de su atraso y para poder llegar a un acuerdo de pago e informarle de algunas facilidades que tenemos para ayudarlo a solucionar su problema; al no tener una respuesta concreta con respecto al cumplimiento de sus obligaciones y de persistir con la actitud renuente al pago nuestra representada estará evaluando la posibilidad de iniciar las acciones legales para recuperar su deuda.</p>

          <p align="justify">Cualquier consulta adicional sobre el particular o para convenir una solución a su actual situación, lo instamos a comunicarse con nosotros a la brevedad posible.</p>

          <p align="justify">Sin otro particular, quedamos de usted.</p>

          <p></p><p></p><p></p>
          <tr>
            <td colspan="20">Atentamente, <br/>
              Departamento de cobranza <br/>
              Celular: 951869875 / 945936439 
            </td>
          </tr>
        ';
      }
      //? DOCUMENTO PARA TRAMO DE 46 A 60 DÍAS
      if($dias_atraso >= 46 && $dias_atraso <= 60){
        $html = '
          <tr>
            <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
            <td colspan="15" style="text-align: center;"></td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="12" style="text-align: center; font-size: 55px;">
              <b><u>INCUMPLIMIENTO DE CONTRATO</u></b>
            </td>
          </tr>
          <br/><br/><br/>
          <tr>
            <td colspan="20"><b>'.$cliente_nom.'</b></td>
          </tr>
          <tr>
            <td colspan="20"><b>'.$representante_nom.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20"><b>'.$cliente_dir.'</b></td>
          </tr>

          <p align="justify">Estimado Cliente, Le hemos enviado recordatorios anteriormente y su cuenta sigue atrasada, Cumplimos con informarle que está incurriendo en <b>Causal de Resolución de Contrato</b>, conforme a lo establecido en las cláusulas de su contrato (… Si el cliente incumpliera con el pago puntual de una o más cuotas, o de cualquier obligación emanada del presente contrato, no estando obligados a respetar dicho periodo de tiempo teniendo el criterio para tomar dicha decisión);  encontrándonos por tanto facultados a dar por vencidos todos los plazos de sus crédito y exigir el pago inmediato del total de sus obligaciones.</p>

          <p align="justify">En tal sentido, REQUERIMOS, que, en el término del día, cumpla con la cancelación de sus obligaciones vencidas para evitar mayores molestias.</p>

          <p align="justify">Cualquier consulta adicional sobre el particular o para convenir una solución a su actual situación, lo instamos a comunicarse con nosotros a la brevedad posible.</p>

          <p align="justify">Sin otro particular, quedamos de usted.</p>

          <p></p><p></p><p></p>
          <tr>
            <td colspan="20">Atentamente, <br/>
              Departamento de cobranza <br/>
              Celular: 951869875 / 945936439 
            </td>
          </tr>
        ';
      }
      //? DOCUMENTO PARA TRAMO DE 61 A 70 DÍAS
      if($dias_atraso >= 61 && $dias_atraso <= 70){
        $html = '
          <tr>
            <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
            <td colspan="15" style="text-align: center;"></td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
          </tr>

          <br/><br/><br/>
          <tr>
            <td colspan="20"><b>'.$cliente_nom.'</b></td>
          </tr>
          <tr>
            <td colspan="20"><b>'.$representante_nom.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20"><b>'.$cliente_dir.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20">Estimado cliente,</td>
          </tr>

          <p align="justify">Reciba un cordial saludo; el motivo de la presente comunicación es para informarle que estamos adjuntando un RECORDATORIO DE PAGO por la OBLIGACIÓN PENDIENTE QUE TIENE CON NUESTRA REPRESENTADA, de acuerdo a resoluciones emitidas por el área de Cobranzas hasta la fecha no llegan a un acuerdo de pago favorable para ambas partes, por tal motivo hemos decidido también derivar ESTÁ NOTIFICACIÓN A SU DOMICILIO CONSIGNADO.</p>

          <p align="justify"><b>Cumplimos con informarle que conforme a lo establecido en las cláusulas de su contrato; En caso que incumpliese con el pago de su cuota mensual, pasado (01) UN DÍA de la fecha asignada según su cronograma de pagos, el cliente está obligado a pagar por concepto de penalidad y el deterioro de su calificación en el sistema financiero, además puede ser Causal de Resolución de Contrato; encontrándonos por tanto facultados a dar por vencidos todos los plazos de su crédito y exigir el pago inmediato del total de sus obligaciones.</b></p>

          <p align="justify">En tal sentido, REQUERIMOS, que, en el término de un plazo de 72 horas, cumpla con la cancelación de sus obligaciones vencidas y evitemos molestias posteriores.</p>

          <p align="justify">Cualquier consulta adicional sobre el particular o para convenir una solución a su actual situación, lo instamos a comunicarse con nosotros a la brevedad posible.</p>

          <p>Sin otro particular, quedamos de usted.</p>

          <p></p><p></p><p></p>
          <tr>
            <td colspan="20">Atentamente, <br/>
              Departamento de cobranza <br/>
              Celular: 951869875 / 945936439 
            </td>
          </tr>
        ';
      }
      //? DOCUMENTO PARA TRAMO DE 71 A MÁS DÍAS
      if($dias_atraso >= 71){
        $html = '
          <tr>
            <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 70px;"></td>
            <td colspan="15" style="text-align: center;"></td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="20" align="rigth">Chiclayo, '.fechaActual(date('Y-m-d')).'</td>
          </tr>
          <br/><br/>
          <tr>
            <td colspan="12" style="text-align: center; font-size: 55px;">
              <b><u>ULTIMO  AVISO  EXTRAJUDICIAL</u></b>
            </td>
          </tr>
          <br/><br/><br/>
          <tr>
            <td colspan="20"><b>'.$cliente_nom.'</b></td>
          </tr>
          <tr>
            <td colspan="20"><b>'.$representante_nom.'</b></td>
          </tr>
          <br/>
          <tr>
            <td colspan="20"><b>'.$cliente_dir.'</b></td>
          </tr>

          <p align="justify">Habiendo hecho caso omiso a nuestros reiterados requerimientos de pago, se está procediendo a TRANSFERIR SU EXPEDIENTE DE CRÉDITO AL ÁREA LEGAL, para que inicie las acciones pertinentes EN CONTRA SUYA Y DE LOS INTERVINIENTES EN EL CRÉDITO, por el adeudo que mantiene con nuestra representada, mencionando como antecedente, que el embargo que perseguimos genera una serie de gastos que la ley señala.</p>

          <p align="justify">En tal sentido, nuestra representada exigirá el PAGO TOTAL QUE LA CUENTA TENGA EN ESE MOMENTO, de acuerdo al artículo 1230 del Código Civil, y de esta forma se procedería a la recuperación del crédito, mediante la ejecución de la GARANTÍA OTORGADA.</p>

          <p align="justify">De lo escrito en los párrafos anteriores, notará que LA LEY ES PRECISA EN SU APLICACIÓN y a efecto de detener el proceso iniciado en su contra, sírvase acudir con esta notificación en UN TERMINO NO MAYOR A 72 HORAS, de no concurrir, ENTENDEREMOS QUE NO LE INTERESA NEGOCIAR SU ADEUDO Y EXIGIREMOS LA ORDEN JUDICIAL PARA PROCEDER CON LA EJECUCION DE LA GARANTIA.</p>

          <p align="justify">Sin otro particular, quedamos de usted.</p>

          <p></p><p></p><p></p>
          <tr>
            <td colspan="20">Atentamente, <br/>
              Departamento de cobranza <br/>
              Celular: 951869875 
            </td>
          </tr>
        ';
      }

      $array_html[$posicion] = $html;
      $array_codigos[$posicion] = 'ID-'.$value['temp_cobranza_id'].'-DIAS-'.$dias_atraso;
      $posicion ++;
    }
  }
$result = NULL;

class MYPDF extends TCPDF {

  public function Header() {
  }

  public function Footer() {
    global $array_codigos;
    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, '', 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $this->write1DBarcode($array_codigos[$this->getPage() - 1], 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'Av. Mariscal Nieto # 480 Ext. A-07 1er Piso Centro Comercial Boulevard -  Chiclayo', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
$pdf->setLanguageArray($l);

// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
for($i = 0; $i < count($array_html); $i++){
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTML($array_html[$i], true, 0, true, true);

  $pdf->Ln();
}

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo = $codigo."_8974".$title.".pdf";
ob_end_clean();

$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+

?>