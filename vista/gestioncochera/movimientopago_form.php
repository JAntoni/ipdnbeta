<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
} else {
  require_once('../../core/usuario_sesion.php');
}
require_once('CocheraMovimiento.class.php');

$oMovimientoPago = new CocheraMovimiento();

require_once '../funciones/fechas.php';

$fecha1 = fecha_mysql($_POST['txt_filtro_fec1']);
$fecha2 = fecha_mysql($_POST['txt_filtro_fec2']);
$fechapago = date('d-m-Y');
$placa = mb_strtoupper($_POST['txt_placa_nom'], 'UTF-8');
$cochera = $_POST['cmb_cochera_id'];

$fechapagar = $_POST['fechapagar'];

$result = $oMovimientoPago->filtrar_movimiento($fecha1, $fecha2, $placa, $cochera);

$tr = '';
$total_monto = 0; // Inicializamos la variable para el monto total

if ($result['estado'] == 1) {
  // foreach ($result['data'] as $key => $value) {
  //   // Verificamos si el vehículo no tiene salida
  //   if (empty($value['tb_cocheramovimiento_fechasalida']) || $value['tb_cocheramovimiento_fechasalida'] == '0000-00-00') {
  //     $fechapagar1= $fechapagar. ' 23:59:59';
  //     $fechaHoraEntrada = strtotime($value['FECHA_HORA_ENTRADA']);
  //     $fechaHoraSalida = strtotime($fechapagar1);

  //     $diferencia_segundos = $fechaHoraSalida - $fechaHoraEntrada;
  //     $criteriohora = $value['CRITERIO'];
  //     $precio = $value['PRECIO'];

  //     if ($diferencia_segundos < 0) {
  //       $diferencia_segundos += 24 * 60 * 60;
  //     }

  //     $diferencia_horas = ceil($diferencia_segundos / (60 * 60));
  //     $num_turno = ceil($diferencia_horas / $criteriohora);
  //     if ($num_turno == 0) {
  //       $num_turno = 1;
  //     }

  //     // Actualización del monto en la BD si no hay salida
  //     $montoupdate = $precio * $num_turno;
  //     $cocheraMovimientoUpdate = $oMovimientoPago->modificar_campo($value['ID'], 'tb_cocheramovimiento_monto', $montoupdate, 'STR');
  //   }
  // }

  // Consultamos los pagos agrupados
  $result2 = $oMovimientoPago->listarPagoAgrupado($fecha1, $fecha2, $cochera);
    if ($result2['estado'] == 1) {
      foreach ($result2['data'] as $key => $value) {
        $monto = $value['monto'];
        $total_monto += (float)$value['monto']; // Sumamos el monto de cada fila al monto total

        // Calculamos la diferencia de días
        $fechaFiltro1 = new DateTime($value['tb_cocheramovimiento_fechaentrada']);
        $fechaPagar = new DateTime($value['tb_cocheramovimiento_fechasalida']);
        if(empty($value['tb_cocheramovimiento_fechasalida']) || $value['tb_cocheramovimiento_fechasalida'] == '0000-00-00')
          $fechaPagar = new DateTime($fecha2);
        $diferenciaDias = $fechaFiltro1->diff($fechaPagar)->days;
        $diferenciaDias += 1;

        $cliente = $value['tb_cliente_nom'];
        if(!empty(trim($value['tb_cliente_emprs'])))
          $cliente = $value['tb_cliente_emprs'].' | '.$value['tb_cliente_nom'];
        // Construcción de la fila de la tabla
        $tr .= '<tr>';
        $tr .= '
          <td>'.$cliente.'</td>
          <td>' . $value['placa'] . '</td>
          <td>' . $value['tb_vehiculomarca_nom'] . ' / '.$value['tb_vehiculomodelo_nom'].'</td>
          <td>'.$value['movimientos'].'</td>
          <td>' . $diferenciaDias . ' ('.$value['tb_cocheramovimiento_fechaentrada'].' | '.$value['tb_cocheramovimiento_fechasalida'].')</td>  <!-- Aquí colocamos la diferencia de días -->
          <td>' . $value['cochera'] . '</td>
          <td>' . $monto . '</td>
        ';
        $tr .= '</tr>';
      }
      // Agregamos la fila del monto total
      $tr .= '<tr>';
      $tr .= '<td colspan="6" style="text-align: right;"><strong>Monto Total:</strong></td>';
      $tr .= '<td><b>' . $total_monto . '</b></td>';
      $tr .= '</tr>';
    } 
    else {
      echo $result2['mensaje'];
      exit();
    }
  $result2 = NULL;
} else {
  echo $result2['mensaje'];
  exit();
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_pago_cochera" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">

        <div class="panel panel-info">
          <div class="panel-body">
            <form id="form_cocheramovimiento_filtro_pagar" class="form-inline" role="form">
              <div class="form-group">
              <input type="hidden" name="hdd_fechapagar" id="hdd_fechapagar" value="<?php echo $fechapagar; ?>">
              <input type="hidden" name="action" id="action" value="pagar">
              <input type="hidden" name="hdd_cochera_id" id="hdd_cochera_id" value="<?php echo $cochera; ?>">
                <div class='input-group date' id='datetimepicker1'>
                  <input type='text' class="form-control input-sm input-shadow" name="txt_filtro_fec1Pago" id="txt_filtro_fec1Pago" value="<?php echo $fecha1; ?> " readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar input-shadow"></span>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                  <input type='text' class="form-control input-sm input-shadow" name="txt_filtro_fec2Pago" id="txt_filtro_fec2Pago" value="<?php echo $fecha2; ?> " readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar input-shadow"></span>
                  </span>
                </div>
              </div>

              <div class="form-group">
                <select name="cbo_metodo_id" id="cbo_metodo_id" class="form-control input-sm mayus input-shadow" >
                  <option value="2">Transferencia bancaria</option>
                  <option value="3" selected>Efectivo</option>
                </select>
              </div>

              <div class="form-group" style="display: none;" id="precio_banco">
                <label>N° Cuenta:</label>
                <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                  <?php require_once '../cuentadeposito/cuentadeposito_select.php'; ?>
                </select>
              </div>

              <div class="form-group" style="display: none;" id="precio_banco3">
                <input type="text" placeholder="Numero de operacion" name="txt_numoperacion" id="txt_numoperacion" class="form-control input-sm">
              </div>

              <div class="form-group" style="display: none;" id="precio_banco2">
                <input style="text-align:right;" placeholder="Monto de depósito" class="form-control input-sm moneda3" name="txt_montodeposito_cochera" type="text" id="txt_montodeposito_cochera">
              </div>

              <div class="form-group" style="display: none;" id="precio_banco_comision">
                <input style="text-align:right;" placeholder="Comisión de Banco" class="form-control input-sm moneda4" name="txt_comisionbanco_cochera" type="text" id="txt_comisionbanco_cochera">
              </div>


              <div class="form-group">
                <div class='input-group date' id='datetimepicker_fechapago'>
                  <input type='text' class="form-control input-sm input-shadow" placeholder="FECHA DE PAGO" name="txt_filtro_fechapago" id="txt_filtro_fechapago value="<?php echo $fechapago; ?>" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar input-shadow"></span>
                  </span>
                </div>
              </div>

            </form>
          </div>
        </div>
        <table id="tbl_cocheras" class="table table-hover">
          <thead>
            <tr>
              <th>CLIENTE</th>
              <th>PLACA</th>
              <th>MARCA / MODELO</th>
              <th>NUMERO DE MOVIMIENTOS</th>
              <th>DIAS TRANSCURRIDOS</th>
              <th>COCHERA</th>
              <th>MONTO A PAGAR</th>
            </tr>
          </thead>
          <tbody>
            <?php echo $tr; ?>
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <div class="f1-buttons">
          <button class="dt-button buttons-copy buttons-html5 btn btn-primary" tabindex="0" aria-controls="tbl_llave" type="button" onclick="CompletarPago()">
            <span><i class="fa fa-money"></i></span> Pagar
          </button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/gestioncochera/movimientopago_form.js'; ?>"></script>
