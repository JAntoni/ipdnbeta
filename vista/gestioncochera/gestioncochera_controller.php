<?php
require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../gestioncochera/GestionCochera.class.php');
$oGestionCochera = new GestionCochera();
require_once('../gestioncochera/CocheraMovimiento.class.php');
$oCocheraMovimiento = new CocheraMovimiento();

require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';

$action = $_POST['action'];

$oCocheraMovimiento->cocheramovimiento_id =intval($_POST['']);

$oGestionCochera->gestioncochera_id = intval($_POST['hdd_gestioncochera_id']);
$oGestionCochera->creditotipo_id = intval($_POST['hdd_gestioncochera_tipocredito']);
$oGestionCochera->cliente_id = intval($_POST['hdd_cliente_id']);
$oGestionCochera->clase_id = intval($_POST['hdd_clase_id']);
$oGestionCochera->modelo_id = intval($_POST['cmb_modelo']);
$oGestionCochera->marca_id = intval($_POST['cmb_marca']);
$oGestionCochera->color_veh = mb_strtoupper($_POST['txt_color_nom'], 'UTF-8');
$oGestionCochera->placa_veh = mb_strtoupper($_POST['txt_placa_nom'], 'UTF-8');
$oGestionCochera->estado_veh = intval($_POST['cmb_estado_id']);
$oGestionCochera->cochera_id = intval($_POST['cmb_cochera_id_form']);
$oGestionCochera->usuario_id = intval($_POST['hdd_usuario_id']);
$oGestionCochera->observacion_veh = trim(strtoupper($_POST['txt_gestioncochera_obs']));
$oGestionCochera->hora_registro = string_hora($_POST['txt_gestioncochera_hora']);
$oGestionCochera->fecha_registro = fecha_mysql($_POST['txt_gestioncochera_fecha']);
$oGestionCochera->tamano_id = intval($_POST['cmb_tamanoveh_id']);
$oGestionCochera->nromotcha =$_POST['hdd_llave_motor'].''.$_POST['hdd_llave_chasis'];
$oGestionCochera->credito_id =$_POST['cmb_credito_id'];

if ($action == 'insertar') {
	/* $data['estado']=0;
	$data['mensaje']=var_export($oGestionCochera);
	echo json_encode($data);
	exit(); */

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al guardar el vehiculo.';

	$registro_existente = $oGestionCochera->verificar_registrousuario();
	if (!$registro_existente) {
		if ($oGestionCochera->insertar()) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Vehiculo registrado correctamente.';
		}
	} else {
		$data['mensaje'] = 'Ya existe un vehículo con la misma placa para este cliente.';
	}

	echo json_encode($data);
} elseif ($action == 'modificar') {

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al modificar los Datos del Vehiculo.';

	if ($oGestionCochera->modificar()) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Datos del Vehiculo modificados correctamente. ';
	}

	echo json_encode($data);
} elseif ($action == 'eliminar') {

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al eliminar los Datos del Vehiculo.';

	if ($oGestionCochera->eliminar()) {

		
		//logica para eliminar movimientos 
		$oCocheraMovimiento->eliminarMovimientoGestion($oGestionCochera->gestioncochera_id);

		$data['estado'] = 1;
		$data['mensaje'] = 'Datos del Vehiculo eliminados correctamente';
	}
	echo json_encode($data);
} else if ($action == 'datos_vehiculo') {
	$tipo_credito = intval($_POST['creditotipo_id']);
	$cliente_id = intval($_POST['cliente_id']);

	$data['estado'] = 1;

	if ($tipo_credito == 1) { // 1 c-menor, 2 -> vehiculo, 3 retención
		require_once('../creditomenor/Creditomenor.class.php');
		$oCredito = new Creditomenor();
	} else {
		require_once('../creditogarveh/Creditogarveh.class.php');
		$oCredito = new Creditogarveh();

		$result = $oCredito->datos_vehiculo_por_cliente($cliente_id); // devuelve una fila con los datos del vehículo del cliente
		if ($result['estado'] == 1) {
			$data['marca'] = $result['data']['tb_vehiculomarca_nom'];
			$data['marca_id'] = $result['data']['tb_vehiculomarca_id'];
			$data['modelo'] = $result['data']['tb_vehiculomodelo_nom'];
			$data['modelo_id'] = $result['data']['tb_vehiculomodelo_id'];
			$data['clase'] = $result['data']['tb_vehiculoclase_nom'];
			$data['clase_id'] = $result['data']['tb_vehiculoclase_id'];
			$data['color'] = $result['data']['tb_credito_vehcol'];
			$data['placa'] = $result['data']['tb_credito_vehpla'];
		}
		$result = NULL;
	}
	echo json_encode($data);
} else if($action == 'pagar'){
    /* $arreglo_carros		= (array) json_decode($_POST['arreglo_carros']); */

    $contador = 0;
    $estado = 'liquidado'; //o 1,2,3,4,5
    for ($i = 0; $i < count($arreglo_carros); $i++) {
        $modelo->modificar_campo();
    }

    $data['estado'] = 1;
    $data['mensaje'] = 'DATOS ENVIADOS CORRECTAMENTE';

    echo json_encode($data);
	
}else if ($action == 'llenardatos') {
	$creditotipo_id = $_POST['creditotipo_id'];
	$credito_id = $_POST['credito_id'];
	
	$data['estado'] = 0;
	$data['mensaje'] = "$creditotipo_id, $credito_id";
	try {
		$res = $oGestionCochera->llenarDatosCreditos($credito_id, $creditotipo_id);
		if (intval($res['estado']) == 1) {
		  $data['estado'] = 1;
		  $data['mensaje'] = 'Datos Obtenidos: ' . $res['data']['clase_vehiculo'] . $res['data']['marca_vehiculo'] . $res['data']['modelo_vehiculo'];

		  $data['clase_vehiculo'] = $res['data']['clase_vehiculo'];
		  $data['placa'] = $res['data']['vehiculo_placa'];
		  $data['marcaid'] = $res['data']['marca_vehiculo'];
		  $data['tipovehiculo'] = $res['data']['tipo_vehiculo'];
		  $data['modeloid'] = $res['data']['modelo_vehiculo'];
		  $data['color'] = $res['data']['color'];
		  $data['motor'] = $res['data']['vehiculo_motor'];
		  $data['chasis'] = $res['data']['vehiculo_chasis'];

		}
	  $res = NULL;
	} catch (Exception $e) {
	  $data['estado'] = 0;
	  $data['mensaje'] = 'Existe un error al obtener datos de la llave: ' . $e->getMessage();
	}
	echo json_encode($data);

}else if ($action == 'BuscarCliente') {
	
	$placa = $_POST['placa'];
	
	$data['estado'] = 0;
	$data['mensaje'] = "$creditotipo_id, $credito_id";
	try {
		$res = $oGestionCochera->llenarCliente($placa);
		if (intval($res['estado']) == 1) {
		  $data['estado'] = 1;
		  $data['mensaje'] = 'Datos Obtenidos: ' . $res['data']['nombrecliente'];
		  $data['nombre'] = $res['data']['nombrecliente'];
		}
	  $res = NULL;
	} catch (Exception $e) {
	  $data['estado'] = 0;
	  $data['mensaje'] = 'Existe un error al obtener datos de cliente: ' . $e->getMessage();
	}
	echo json_encode($data);

  }else {
	$data['estado'] = 0;
	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para' . $action;

	echo json_encode($data);
}
