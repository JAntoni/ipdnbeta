function metodo_mostrar_ocultar() {
  $("#cbo_metodo_id").change(function (event) {
    var metodo = parseInt($(this).val());
    if (metodo == 2) {
      $("#precio_ofi").hide(200);
      $("#precio_banco").show(200);
      $("#precio_banco2").show(200);
      $("#precio_banco3").show(200);
      $("#precio_banco_comision").show(200);
      
    } else {
      // Con depósito solo para clientes externos
      $("#precio_ofi").show(200);
      $("#precio_banco").hide(200);
      $("#precio_banco2").hide(200);
      $("#precio_banco3").hide(200);
      $("#precio_banco_comision").hide(200);
    }
  });
}

$(document).ready(function () {
  metodo_mostrar_ocultar();
  $("#datetimepicker_fechapago").change();

  $("#datetimepicker_fechapago").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });
});
