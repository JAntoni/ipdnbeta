<?php
$total_egresos = 0; // Inicializar la variable total_egresos

if (
  //cambian por usar el serializer
  isset($_POST['txt_filtro_fec1']) &&
  isset($_POST['txt_filtro_fec2']) &&
  isset($_POST['txt_placa_nom']) &&
  isset($_POST['cmb_cochera_id'])
) {
  // Importación de archivos y otras declaraciones necesarias
  if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'gestioncochera/CocheraMovimiento.class.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../gestioncochera/CocheraMovimiento.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  $oCocheraMovimiento = new CocheraMovimiento();
  //cambian por usar el serializer

  // Datos usados para filtrar movimientos
  $fecha1 = fecha_mysql($_POST['txt_filtro_fec1']);
  $fecha2 = fecha_mysql($_POST['txt_filtro_fec2']);
  $placa = mb_strtoupper($_POST['txt_placa_nom'], 'UTF-8');
  $cochera = $_POST['cmb_cochera_id'];

  $fecha_hoy = date('Y-m-d');

  $tr = '';

  $result = $oCocheraMovimiento->filtrar_movimiento($fecha1, $fecha2, $placa, $cochera);
  $test = '';
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $fechacalcular = $value['FECHA_HORA_SALIDA'];
      $fechaSalidaValorAsignado = 1;
      $fechaSalida = $value['FECHA_HORA_SALIDA'];

      // Calcular las horas transcurridas
      if (empty($value['tb_cocheramovimiento_fechasalida']) || $value['tb_cocheramovimiento_fechasalida'] == '0000-00-00') {
        $fechacalcular = $fecha2.' 23:59:00';//date('Y-m-d H:i:s');
        $fechaSalida = $fecha2.' 23:59:00' . '<span class="badge bg-yellow"> NO TIENE FECHA DE SALIDA';
        $fechaSalidaValorAsignado = 0;

        $fechaHoraEntrada = strtotime($value['FECHA_HORA_ENTRADA']);
        $fechaHoraSalida = strtotime($fechacalcular);
        $diferencia_segundos = $fechaHoraSalida - $fechaHoraEntrada;
        $criteriohora = $value['CRITERIO'];
        $precio = $value['PRECIO'];

        //? CUANDO EL CRITERIO ES DE 24 HORAS TRABAJAMOS A RESTAR DÍAS YA NO A RESTAR HORAS, DEBIDO A QUE ES UN SOLO TURNO, NO COMBRAN POR HORAS SINO POR TURNO
        //? POR ELLO EL CALCULO SERÍA EJMP: 01-06-2024 (ENTRADA) - 30-06-2024 (SALIDA) -> SALIDA - ENTRADA = 29 DÍAS + 1 (YA QUE SE CUENTA EL ULTIMO DÍA TAMBIEN)
        
        if(intval($criteriohora >= 24)){
          $fecha_entrada_calcular = $value['tb_cocheramovimiento_fechaentrada'];
          $fecha_salida_calcular = $fecha2;

          // Crear objetos DateTime para las dos fechas
          $datetime_inicio = new DateTime($fecha_entrada_calcular);
          $datetime_fin = new DateTime($fecha_salida_calcular);
          // Calcular la diferencia entre las dos fechas
          $interval = $datetime_inicio->diff($datetime_fin);
          // Obtener el número de días de diferencia
          $diferencia_dias = $interval->days;
          $num_turno = $diferencia_dias + 1; //? PARA QUE EL ULTIMO DÍA TAMBIÉN LO TOME COMO TURNO
        }
        else{
          if ($diferencia_segundos < 0) {
            $diferencia_segundos += 24 * 60 * 60;
          }
          $diferencia_horas = ceil($diferencia_segundos / (60 * 60));
          $num_turno = ceil($diferencia_horas / $criteriohora);
          if ($num_turno == 0) {
            $num_turno = 1;
          }
        }
        //se declara para insertar en la bd, sin el formato de 0.0
        $montoupdate = $precio * $num_turno;
        $cocheraMovimientoUpdate = $oCocheraMovimiento->modificar_campo($value['ID'], 'tb_cocheramovimiento_monto', $montoupdate, 'STR');
        $monto = formato_numero($precio * $num_turno);

        //$test .= ' /// fec hora entrada: '.$fechaHoraEntrada.' / salida: '.$fechaHoraSalida.' / precio: '.$precio.' / nium turno: '.$num_turno;
      } else {
        $fechaHoraEntrada = strtotime($value['FECHA_HORA_ENTRADA']);
        $fechaHoraSalida = strtotime($value['FECHA_HORA_SALIDA']);
        $diferencia_segundos = $fechaHoraSalida - $fechaHoraEntrada;
        $criteriohora = $value['CRITERIO'];
        $precio = $value['PRECIO'];
        if ($diferencia_segundos < 0) {
          $diferencia_segundos += 24 * 60 * 60;
        }
        $diferencia_horas = ceil($diferencia_segundos / (60 * 60));
        $num_turno = ceil($diferencia_horas / $criteriohora);
        if ($num_turno == 0) {
          $num_turno = 1;
        }
        $monto = $value['monto'];

        /* $montoupdate = $precio * $num_turno;
                $cocheraMovimientoUpdate = $oCocheraMovimiento->modificar_campo($value['ID'], 'tb_cocheramovimiento_monto', $montoupdate, 'STR');
                $monto = ($precio * $num_turno) . '.00'; */
      }


      // Determinar la clase CSS basada en el estado
      $estado_class = '';
      if ($value['ESTADO'] == 'PENDIENTE DE PAGO') {
        $estado_class = 'badge bg-red';
      } elseif ($value['ESTADO'] == 'PAGADO') {
        $estado_class = 'badge bg-green';
      }

      $cliente_nom = $value['tb_cliente_nom'];
      if(!empty(trim($value['tb_cliente_emprs'])))
        $cliente_nom = $value['tb_cliente_emprs'].' | '.$value['tb_cliente_nom'];
      // Construir la fila de la tabla con la clase CSS asignada al estado
      $tr .= '<tr>';
      $tr .= '<td>' . $value['ID_VEHICULO'] . '</td>';
      $tr .= '<td>' . $cliente_nom . '</td>';
      $tr .= '<td>' . $value['PLACA'] . '</td>';
      $tr .= '<td>' . $value['MARCA'] . ' / '.$value['MODELO'].'</td>';
      $tr .= '<td>' . $value['CLASE'] . '</td>';
      $tr .= '<td>' . $value['COCHERA'] . '</td>';
      $tr .= '<td>' . $value['FECHA_HORA_ENTRADA'] . '</td>';
      $tr .= '<td>' . $fechaSalida . '</td>';
      $tr .= '<td><span class="' . $estado_class . '">' . $value['ESTADO'] . $test . '</span></td>';
      $tr .= '<td>' . $diferencia_horas . '</td>';
      $tr .= '<td>' . $value['PRECIO'] . '</td>';
      $tr .= '<td>' . $value['CRITERIO'] . '</td>';
      $tr .= '<td>' . $num_turno . '</td>';
      $tr .= '<td><span class="badge bg-green"> S/ ' . $monto . '</td>';
      $tr .= '<td><button class="btn btn-warning btn-xs" onclick="EditarMonto(' . $value['ID'] . ', ' . $fechaSalidaValorAsignado . ')"> <i class="fa fa-edit"></i>Editar</button></td>';
      $tr .= '</tr>';

      if (empty($value['FECHA_HORA_SALIDA'])) {
        // Si no hay fecha de salida, sumar el monto al total_egresos
        $total_egresos += $monto;
      } else {
        // Si hay fecha de salida, sumar el monto registrado al total_egresos
        $total_egresos += $value['monto'];
      }
    }
  } else {
    echo $result['mensaje'];
    exit();
  }
} else {
  // Si falta algún valor, no mostramos nada
  exit();
}
?>

<table id="tbl_cocheramovimientos" class="table table-hover">
  <thead>
    <tr>
      <th>ID VEHICULO</th>
      <th>CLIENTE</th>
      <th class="no-sort">PLACA</th>
      <th class="no-sort">MARCA / MODELO</th>
      <th class="no-sort">TIPO DE VEHICULO</th>
      <th>COCHERA</th>
      <th class="no-sort">FECHA DE INGRESO / INICIO DE FACTURACIÓN</th>
      <th class="no-sort">FECHA DE SALIDA / FIN DE FACTURACIÓN</th>
      <th class="no-sort">ESTADO</th>
      <th class="no-sort">HORAS TRANSCURRIDAS</th>
      <th>PRECIO</th>
      <th class="no-sort">CRITERIO DE HORA</th>
      <th>N° DE TURNO</th>
      <th>MONTO</th>
      <th class="no-sort">OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr; ?>
  </tbody>
  <tfoot>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <td colspan="14">MONTO TOTAL A PAGAR</td>
      <td><?php echo "S/ " . mostrar_moneda($total_egresos); ?></td>
    </tr>
  </tfoot>
</table>
<div style="text-align: center;">
  <input type="hidden" id="hdd_carros_array" name="hdd_carros_array" value='<?php echo json_encode($result['data']) ?>'>
  <button class="dt-button buttons-copy buttons-html5 btn btn-primary" tabindex="0" aria-controls="tbl_llave" type="button" onclick="PagarCochera()">
    <span><i class="fa fa-money"></i></span> Pagar
  </button>
</div>