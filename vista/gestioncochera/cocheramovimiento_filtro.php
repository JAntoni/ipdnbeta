<?php
$filtro_fec1 = date('01-m-Y');
$filtro_fec2 = date('d-m-Y');
$empresa_id = $_SESSION['empresa_id'];
$todasCocheras = 1;
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_cocheramovimiento_filtro" class="form-inline" role="form">

                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control input-sm input-shadow" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $filtro_fec1; ?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar input-shadow"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control input-sm input-shadow" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $filtro_fec2; ?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar input-shadow"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="txt_placa_nom" id="txt_placa_nom" class="form-control input-sm mayus input-shadow" autocomplete="off" size="30" placeholder="Busca una placa aquí" value="<?php echo $placa; ?>">
                    </div>

                    <div class="form-group">
                        <select name="cmb_cochera_id" id="cmb_cochera_id" class="form-control input-sm mayus input-shadow">
                            <?php require_once('vista/cochera/cochera_select.php'); ?>
                        </select>
                    </div>
                    <button type="button" class="btn btn-info btn-sm" onclick="cocheramovimiento_tabla()"><i class="fa fa-search"></i></button>


                    <button class="btn btn-success btn-sm" type="button" onclick="MostrarResumenPagos()">
                        <span><i class="fa fa-money"></i></span> Mostrar Resumen de Pagos
                    </button>

                    </div>
                    
                </form>

            </div>
        </div>
    </div>
</div>