<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../gestioncochera/GestionCochera.class.php');
$oGestionCochera = new GestionCochera();
require_once('../funciones/funciones.php');
require_once '../funciones/fechas.php';

$direc = 'gestioncochera';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$gestioncochera_id = $_POST['gestioncochera_id'];

$cocheramovimiento_id = $_POST[''];

$titulo = '';
if ($usuario_action == 'L') {
    $titulo = 'Vehiculo Registrado';
} elseif ($usuario_action == 'I') {
    $titulo = 'Registrar Vehiculo';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Vehiculo';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Vehiculo';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cochera
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos)) {
        $bandera = 1;
    }

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
        $usuario_id = $_SESSION['usuario_id'];
        $modulo = 'cochera';
        $modulo_id = $gestioncochera_id;
        $tipo_permiso = $usuario_action;
        $estado = 1;

        $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if ($result['estado'] == 1) {
            $bandera = 1; // si tiene el permiso para la accion que desea hacer
        } else {
            $result = NULL;
            echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
            exit();
        }
        $result = NULL;
    }

    $todasCocheras = 1;
    $cochera_id = 0;

    //si la accion es modificar, mostramos los datos del cochera por su ID
    if (intval($gestioncochera_id) > 0) {
        $result = $oGestionCochera->mostrarUno($gestioncochera_id);
        if ($result['estado'] != 1) {
            $mensaje =  'No se ha encontrado ningún registro para el vehiculo seleccionado, inténtelo nuevamente.';
            $bandera = 4;
        } else {
            
            $creditotipo_id = intval($result['data']['tb_creditotipo_id']); //Para mostrar el valor del select Tipo Credito
            $cochera_id = $result['data']['tb_cochera_id']; //Para mostrar el valor del select Cochera
            $todasCocheras = 0;
            $usuario_nom = $result['data']['usuario']; //Para mostrar el valor en el input de Responsable
            $usuario_id = $result['data']['tb_usuario_id']; //Para mostrar el valor del ID del Usuario
            $color_nom = $result['data']['tb_credito_vehcol']; //Para mostrar el valor del input Color
            $placa_nom = $result['data']['tb_credito_vehpla']; //Para mostrar el valor del input Placa
            $cliente_nom = $result['data']['tb_cliente_nom']; //Para mostrar el valor del input Cliente
            $marca_nom = $result['data']['tb_vehiculomarca_nom']; //Para mostrar el valor del input Marca
            $modelo_nom = $result['data']['tb_vehiculomodelo_nom']; //Para mostrar el valor del input Modelo
            $clase_nom = $result['data']['tb_vehiculoclase_nom']; //Para mostrar el valor del input Clase
            $estadollave_obs = $result['data']['tb_cocheramovimiento_obserentrada']; //Para mostrar el valor del input Observacion
            $fecha_ingreso = $result['data']['fecha']; //Para mostrar el valor del input Fecha ingreso
            $hora_ingreso = $result['data']['hora']; //Para mostrar el valor del input Hora ingreso
            $estado_id = $result['data']['tb_gestioncochera_estado']; //Para mostrar el valor del select Estado

            $cliente_id = $result['data']['tb_cliente_id'];

            $tamanoveh_id  = $result['data']['tb_gestioncochera_tamano']; //Para mostrar el valor del select Tamaño

            $creditoid = $result['data']['tb_credito_id'];

        }
        $result = NULL;
    }
} else {
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_gestioncochera" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo; ?></h4>
                </div>
                <form id="form_gestioncochera" method="post" style="max-height: 550px; overflow-x: auto; overflow-y: auto;">
                    <input type="hidden" name="action" value="<?php echo $action; ?>">
                    <input type="hidden" name="hdd_gestioncochera_id" value="<?php echo $gestioncochera_id; ?>">
                    
                    <input type="hidden" name="hdd_cocheramovimiento_id" value="<?php echo $cocheramovimiento_id; ?>">

                    <!-- HIDDEN PARA HACER SELECT DE GESTIONCOCHERA  -->

                    <input type="hidden" name="hdd_clase_id" id="hdd_clase_id" value="<?php echo $clase_vehiculo; ?>">
                    <input type="hidden" name="hdd_llave_motor" id="hdd_llave_motor" value="<?php echo $credito_vehsermot; ?>">
                    <input type="hidden" name="hdd_llave_chasis" id="hdd_llave_chasis" value="<?php echo $credito_vehsercha; ?>">
                    <input type="hidden" name="hdd_llave_placa" id="hdd_llave_placa" value="<?php echo $vehpla; ?>">

                    <input type="hidden" name="hdd_gestioncochera_tipocredito" id="hdd_gestioncochera_tipocredito" value="<?php echo $creditotipo_id; ?>">
                    <input type="hidden" name="hdd_gestioncochera_credito" id="hdd_gestioncochera_credito" value="<?php echo $creditoid; ?>">

                    <input type="hidden" name="hdd_tamaño" id="hdd_tamaño" value="<?php echo $tamanoveh_id; ?>">



                    <div class="modal-body">

                        <div class="box box-primary shadow">
                            <div class="panel shadow-simple">
                                <div class="panel-body">
                                    <div class="row">
                                        <!--  ---------------------------CLIENTE----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="txt_cliente_id" class="control-label">Cliente</label>
                                            <input type="text" name="txt_cliente_id" id="txt_cliente_id" class="form-control input-sm ui-autocomplete-input input-shadow" autocomplete="off" size="48" value="<?php echo $cliente_nom; ?>">
                                            <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">
                                        </div>
                                        <!--  ---------------------------INFORMACION DE CREDITO----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label>Información de crédito:</label>
                                            <select id="cmb_credito_id" name="cmb_credito_id" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="panel shadow-simple">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <!--  ---------------------------PLACA----------------------------- -->
                                        <div class="col-md-4">
                                            <label for="txt_placa_nom" class="control-label">Placa</label>
                                            <input type="text" name="txt_placa_nom" id="txt_placa_nom" class="form-control input-sm mayus input-shadow" autocomplete="off" value="<?php echo $vehpla; ?>">
                                        </div>


                                       

                                        <!--  ---------------------------MARCA----------------------------- -->
                                        <div class="col-md-4">
                                            <label for="cmb_marca" class="control-label">Marca</label>
                                            <select id="cmb_marca" name="cmb_marca" class="form-control">
                                                <?php require_once('../vehiculomarca/vehiculomarca_select.php') ?>
                                            </select>
                                        </div>

                                        <!--  ---------------------------Tipo de Vehiculo----------------------------- -->
                                        <div class="col-md-4">
                                            <label for="cmb_vehiculotipo_id_form" class="control-label">Tipo de Vehiculo</label>
                                            <select id="cmb_vehiculotipo_id_form" name="cmb_vehiculotipo_id_form" class="form-control input-sm">
                                                <?php require_once('../vehiculotipo/vehiculotipo_select.php'); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row form-group">

                                        <!-- txt_tipovehiculo -->

                                        <!--  ---------------------------Modelo de Vehículo----------------------------- -->
                                        <div class="col-md-4">
                                            <label for="cmb_modelo" class="control-label">Modelo de Vehículo</label>
                                            <select id="cmb_modelo" name="cmb_modelo" class="form-control">
                                                <?php require_once('../vehiculomodelo/vehiculomodelo_select.php') ?>
                                            </select>
                                        </div>


                                        <!--  ---------------------------COLOR----------------------------- -->

                                        <div class="col-md-4">
                                            <label for="txt_color_nom" class="control-label">Color</label>
                                            <input type="text" name="txt_color_nom" id="txt_color_nom" class="form-control input-sm mayus input-shadow" autocomplete="off" value="<?php echo $color_nom; ?>">
                                        </div>
                                        <!--  ---------------------------TAMAÑO DE VEHICULO----------------------------- -->
                                        <div class="col-md-4">
                                            <label>Tamaño de Vehiculo:</label>
                                            <select id="cmb_tamanoveh_id" name="cmb_tamanoveh_id" class="form-control input-sm mayus input-shadow">
                                                <option>Seleccione...</option>
                                                <option value="1" <?php if ($tamanoveh_id == 1) echo 'selected'; ?>>Pequeño</option>
                                                <option value="2" <?php if ($tamanoveh_id == 2) echo 'selected'; ?>>Mediano</option>
                                                <option value="3" <?php if ($tamanoveh_id == 3) echo 'selected'; ?>>Grande</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="panel shadow-simple">
                                <div class="panel-body">
                                    <div class="row">
                                        <!--  ---------------------------COCHERA----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="cmb_cochera_id_form" class="control-label">Cochera</label>
                                            <select name="cmb_cochera_id_form" id="cmb_cochera_id_form" class="form-control input-sm mayus input-shadow">
                                                <?php require_once('../cochera/cochera_select.php'); ?>
                                            </select>
                                        </div>
                                        <!--  ---------------------------RESPONSABLE----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="txt_usuario_id" class="control-label">Responsable</label>
                                            <input type="text" name="txt_usuario_id" id="txt_usuario_id" class="form-control input-sm ui-autocomplete-input input-shadow" autocomplete="off" size="48" value="<?php echo $usuario_nom; ?>">
                                            <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>">
                                        </div>
                                        <!--  ---------------------------HORA DE INGRESO----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="txt_gestioncochera_hora" class="control-label">Hora de Ingreso</label>
                                            <input type="time" class="form-control input-sm input-shadow mayus" name="txt_gestioncochera_hora" id="txt_gestioncochera_hora" style="width: 100%;" autocomplete="off" value="<?php echo $hora_ingreso; ?>">
                                        </div>
                                        <!--  ---------------------------FECHA DE INGRESO----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="txt_gestioncochera_fecha" class="control-label">Fecha de Ingreso</label>
                                            <input type='date' class="form-control input-sm input-shadow mayus" name="txt_gestioncochera_fecha" id="txt_gestioncochera_fecha" style="width: 100%;" autocomplete="off" value="<?php echo $fecha_ingreso; ?>">
                                        </div>
                                        <!--  ---------------------------ESTADO----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="cmb_estado_id" class="control-label">Estado:</label>
                                            <select id="cmb_estado_id" name="cmb_estado_id" class="form-control input-sm input-shadow mayus">
                                                <option>Seleccione...</option>
                                                <option value="1" <?php if ($estado_id == 1) echo 'selected'; ?>>Almacenado</option>
                                                <option value="2" <?php if ($estado_id == 2) echo 'selected'; ?>>Mantenimiento</option>
                                                <option value="3" <?php if ($estado_id == 3) echo 'selected'; ?>>Entregado</option>
                                                <option value="4" <?php if ($estado_id == 4) echo 'selected'; ?>>Vendido</option>
                                            </select>
                                        </div>

                                        <!--  ---------------------------OBSERVACION----------------------------- -->
                                        <div class="form-group col-md-6">
                                            <label for="txt_gestioncochera_obs" class="control-label">Observación:</label>
                                            <textarea class="form-control input-sm input-shadow" name="txt_gestioncochera_obs" id="txt_gestioncochera_obs"><?php echo $estadollave_obs ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                        <?php if ($action == 'eliminar') : ?>
                            <div class="callout callout-warning">
                                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro de vehiculo</h4>
                            </div>
                        <?php endif; ?>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="gestioncochera_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_gestioncochera">Guardar</button>
                            <?php endif; ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_gestioncochera">Aceptar</button>
                            <?php endif; ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
    <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_gestioncochera">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Mensaje Importante</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <p><?php echo $mensaje; ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/gestioncochera/gestioncochera_form.js?ver=11112229'; ?>"></script>