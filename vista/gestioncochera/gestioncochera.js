function gestioncochera_form(usuario_act, gestioncochera_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gestioncochera/gestioncochera_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      gestioncochera_id: gestioncochera_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_gestioncochera_form").html(data);
        $("#modal_registro_gestioncochera").modal("show");

        modal_width_auto("modal_registro_gestioncochera", 90);
        modal_height_auto("modal_registro_gestioncochera");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_gestioncochera"); //funcion encontrada en public/js/generales.js
          modal_hidden_bs_modal("modal_registro_gestioncochera", "limpiar"); //funcion encontrada en public/js/generales.js

      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "gestioncochera";
        var div = "div_modal_gestioncochera_form";
        permiso_solicitud(usuario_act, gestioncochera_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

//FUNCION PARA MOSTRAR LA TABLA DE GESTION DE VEHICULOS
function gestioncochera_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gestioncochera/gestioncochera_tabla.php",
    async: true,
    dataType: "html",
    data: {},
    beforeSend: function () {
      $("#gestioncochera_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_gestioncochera_tabla").html(data);
      $("#gestioncochera_mensaje_tbl").hide(300);
      //Ejecutar los estilos de la data al llamar la funcion
      estilos_datatable();
    },
    complete: function (data) {},
    error: function (data) {
      $("#gestioncochera_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DEL MOVIMIENTO " + data.responseText
      );
    },
  });
}

//FUNCION PARA MOSTRAR LA TABLA DE MOVIMIENTOS EN PAR E/S
function cocheramovimiento_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gestioncochera/cocheramovimiento_tabla.php",
    async: true,
    dataType: "html",
    //mando tomo el formulario serializado para evitarme mandar parametros estáticos
    data: $("#form_cocheramovimiento_filtro").serialize(),
    beforeSend: function () {
      $("#cocheramovimiento_mensaje_tbl").show(300);
    },
    success: function (data) {
      console.log(data);
      $("#div_cocheramovimiento_tabla").html(data);
      $("#cocheramovimiento_mensaje_tbl").hide(300);
      //Ejecutar los estilos de la data al llamar la funcion
      estilos_datatable1();
    },
    complete: function (data) {
      console.log(data);
    },
    error: function (data) {
      $("#cocheramovimiento_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DEL MOVIMIENTO " + data.responseText
      );
    },
  });
}

//FUNCION PARA AGREGAR E/S DE VEHICULOS
function agregar_movimiento(
  usuario_act,
  cocheramovimiento_id,
  gestioncochera_id
) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gestioncochera/cocheramovimiento_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cocheramovimiento_id: cocheramovimiento_id,
      gestioncochera_id: gestioncochera_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cocheramovimiento_form").html(data);
        $("#modal_registro_cocheramovimiento").modal("show");
        modal_hidden_bs_modal("modal_registro_cocheramovimiento", "limpiar"); //funcion encontrada en public/js/generales.js
        modal_width_auto("modal_registro_cocheramovimiento", 90);
        modal_height_auto("modal_registro_cocheramovimiento");
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("ERROR", data.responseText);
      console.log(data);
    },
  });
}

//Funcion para los estilos de la tabla de datos
function estilos_datatable() {
  datatable_global = $("#tbl_cocheras").DataTable({
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Página mostrada _PAGE_ de _PAGES_",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    columnDefs: [{ orderable: false, targets: [4, 6] }],
  });
  //Llamar a la función filtrar texto
  datatable_texto_filtrar();
}

//Funcion para los estilos de la tabla de datos
function estilos_datatable1() {
  datatable_global = $("#tbl_cocheramovimientos").DataTable({
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Página mostrada _PAGE_ de _PAGES_",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    columnDefs: [{ targets: "no-sort", orderable: false}],
  });
  //Llamar a la función filtrar texto
  datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
  $(".dataTables_filter input").attr("id", "txt_datatable_fil");
  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });
  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

function Buscarclientexplaca() {
  //aqui definimos la variable-para evitar error(placa is not defined)
  var placa = $("#txt_placa_buscar").val();

  if (!placa) {
    alerta_error("ERROR", "Datos vacíos o incorrectos, intente nuevamente");
    return; // Salimos de la función si el campo está vacío
  }
  
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "gestioncochera/gestioncochera_controller.php",
	  async: true,
	  dataType: "json",
	  data: {
		action: "BuscarCliente",
		//el primero va al controller,el segundo es el parametro
    placa:placa
  
	  },
	  beforeSend: function () {},
	  success: function (data) {
	   console.log(data);
		$("#txt_clientebuscar").val(data.nombre);
	  },
	  complete: function (data) {
		//console.log(data);
	  },
	  error: function (data) {
		alert(data.responseText);
	  },
	});
  }

//Apartado donde se ejecutan ciertas funciones al cargar la pagina
$(document).ready(function () {

  $("#cbo_metodo_id").change();
  

  console.log("cambios 11111");
  //Js para darle funcion a los input de fechas
  $("#datetimepicker1, #datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $("#txt_filtro_fec1").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $("#txt_filtro_fec2").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
  });
  //Ejecutar las funciones que llaman a las tablas
  gestioncochera_tabla();
  cocheramovimiento_tabla();
});


function PagarCochera() {
  Swal.fire({
    title: "Ingresa hasta qué fecha quieres realizar el pago",
    icon: "info",
    html:
      '<input id="swal-input1" class="swal2-input" type="date" placeholder="Selecciona una fecha">',
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: "Confirmar",
    cancelButtonText: "Cancelar",
    confirmButtonColor: "#009900",
    cancelButtonColor: "#ff0000",
  }).then((result) => {
    if (result.isConfirmed) {
      var fechaSeleccionada = document.getElementById("swal-input1").value;
      if (!fechaSeleccionada) {
        alert("Por favor, selecciona una fecha.");
        return;
      }
      var cant_faltan_pagar = $("#tbl_cocheramovimientos").find(".bg-red").length;
      if (cant_faltan_pagar == 0) {
        alerta_warning("ERROR", "Ya se han pagado todos los Vehículos");
        return;
      } else {
        $.ajax({
          type: "POST",
          url: VISTA_URL + "gestioncochera/movimientopago_form.php",
          async: true,
          dataType: "html",
          data: "action=pagar&" + 'fechapagar='+fechaSeleccionada+'&'+ $("#form_cocheramovimiento_filtro").serialize(),
          beforeSend: function () {
            $("#h3_modal_title").text("Cargando Formulario");
            $("#modal_mensaje").modal("show");
          },
          success: function (data) {
            $("#modal_mensaje").modal("hide");
            if (data != "sin_datos") {
              $("#div_modal_cocherapago_form").html(data);
              $("#modal_pago_cochera").modal("show");
              modal_hidden_bs_modal("modal_pago_cochera", "limpiar");
              modal_width_auto("modal_pago_cochera", 90);
              modal_height_auto("modal_pago_cochera");
            }
          },
          complete: function (data) {},
          error: function (data) {
            alerta_error("ERROR", data.responseText);
            console.log(data);
          },
        });
      }
    } else if (result.isDenied) {
      Swal.close();
    }
  });
}

function EditarMonto(id, fecha_salida_tiene_valor) {
  Swal.fire({
    title: "Ingresa el nuevo monto",
    icon: "info",
    input: "text", // Aquí se define el tipo de input que se va a mostrar
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "Confirmar",
    showDenyButton: true,
    denyButtonText: "CANCELAR",
    confirmButtonColor: "#009900",
    denyButtonColor: "#red",
  }).then((result) => {
    if (result.isConfirmed) {
      console.log(result.value);
      // Aquí puedes acceder al valor ingresado por el usuario usando result.value
      var nuevoMonto = result.value;
      var movimiento_id = id;

      // Ejemplo de uso del nuevo monto en una petición AJAX
      $.ajax({
        type: "POST",
        url: VISTA_URL + "gestioncochera/cocheramovimiento_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: "EditarMonto",
          monto: nuevoMonto, // Aquí pasas el nuevo monto como parte de los datos de la petición
          id_movimiento: movimiento_id,
          fecha_salida_tiene_valor: fecha_salida_tiene_valor,
        },
        success: function (data) {
          if (data.estado == 1) {
            alerta_success("EXITO", data.mensaje);
            cocheramovimiento_tabla();
          } else {
            alerta_error("ERROR", data.mensaje);
          }
        },
      });
    } else if (result.isDenied) {
      // Aquí se maneja el caso en que el usuario haya cancelado el input
      Swal.close();
    }
  });
}

//consultar
function CompletarPago() {
  var fechaSeleccionada = $("#hdd_fechapagar").val();
  console.log($("#form_cocheramovimiento_filtro_pagar").serialize());
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pagocochera/pagocochera_controller.php",
    async: true,
    dataType: "json",
    data: $("#form_cocheramovimiento_filtro_pagar").serialize(),
    beforeSend: function () {
      console.log(fechaSeleccionada);
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      console.log(data);
      $("#modal_mensaje").modal("hide");
      if (data.estado == 1) {
        $("#modal_pago_cochera").modal("hide");
        alerta_success("Éxito", data.mensaje);
        cocheramovimiento_tabla();
      } else {
        alerta_error("Error", data.mensaje);
      }
    },
    complete: function (data) {
      console.log(data);
    },
    error: function (data) {
      alerta_error("ERROR", data.responseText);
      console.log(data);
    },
  });
}

function MostrarResumenPagos() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pagocochera/pagocochera_form.php",
    async: true,
    dataType: "html",
    //agregar parametros al serializer
    data: $("#modalresumen_pago_cochera").serialize(), //nombre_variable=valor_variable&nombre_variable=valor_variable&...
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_resumen_pago_cochera").html(data);
        $("#modalresumen_pago_cochera").modal("show");
        modal_hidden_bs_modal("modalresumen_pago_cochera", "limpiar"); //funcion encontrada en public/js/generales.js
        modal_width_auto("modalresumen_pago_cochera", 90);
        modal_height_auto("modalresumen_pago_cochera");
      }
    },
    complete: function (data) {
      console.log($("#modalresumen_pago_cochera").serialize());
    },
    error: function (data) {
      alerta_error("ERROR", data.responseText);
      console.log(data);
    },
  });
}

let nuevoMonto = 0;

function EditarMonto(id, fecha_salida_tiene_valor) {
  Swal.fire({
    title: "Ingresa el nuevo monto",
    icon: "info",
    input: "text", // Aquí se define el tipo de input que se va a mostrar
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "Confirmar",
    showDenyButton: true,
    denyButtonText: "CANCELAR",
    confirmButtonColor: "#009900",
    denyButtonColor: "#red",
  }).then((result) => {
    if (result.isConfirmed) {
      console.log(result.value);
      // Aquí puedes acceder al valor ingresado por el usuario usando result.value
      var nuevoMonto = result.value;
      var movimiento_id = id;

      // Ejemplo de uso del nuevo monto en una petición AJAX
      $.ajax({
        type: "POST",
        url: VISTA_URL + "gestioncochera/cocheramovimiento_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: "EditarMonto",
          monto: nuevoMonto, // Aquí pasas el nuevo monto como parte de los datos de la petición
          id_movimiento: movimiento_id,
          fecha_salida_tiene_valor: fecha_salida_tiene_valor,
        },
        success: function (data) {
          if (data.estado == 1) {
            alerta_success("EXITO", data.mensaje);
            cocheramovimiento_tabla();
          } else {
            alerta_error("ERROR", data.mensaje);
          }
        },
      });
    } else if (result.isDenied) {
      // Aquí se maneja el caso en que el usuario haya cancelado el input
      Swal.close();
    }
  });
}



function EliminarMovimiento(id){
  Swal.fire({
    title: '¿Seguro que desea eliminar este movimiento?',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "SI",
    showDenyButton: true,
    denyButtonText: "NO",
    confirmButtonColor: '#3b5998',
    denyButtonColor: '#21ba45',
}).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "gestioncochera/cocheramovimiento_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: "eliminar", // PUEDE SER: L, I, M , E
          id_movimiento: id,//el primero se manda al controller
        },
        beforeSend: function () {
          $("#h3_modal_title").text("Movimiento Eliminado");
          $("#modal_mensaje").modal("show");
        },
        success: function (data) {
          $("#modal_mensaje").modal("hide");
          cocheramovimiento_tabla();
          
    
          if (parseInt(data.estado) == 1) {
            notificacion_success(data.mensaje);
          } else if (parseInt(data.estado) == 0) {
            console.log(data);
            alerta_error("ERROR", data.mensaje);
          }
        },
        complete: function (data) {},
        error: function (data) {
          alerta_error("ERROR", data.responseText);
          console.log(data);
        },
      });
       
      

    } else if (result.isDenied) {
      swal.close();
    }
});
}
