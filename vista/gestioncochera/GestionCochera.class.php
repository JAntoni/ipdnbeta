<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class GestionCochera extends Conexion
{

  public $gestioncochera_id;
  public $creditotipo_id;
  public $cliente_id;
  public $marca_id;
  public $clase_id;
  public $modelo_id;
  public $color_veh;
  public $placa_veh;
  public $estado_veh;
  public $cochera_id;
  public $usuario_id;
  public $observacion_veh;
  public $hora_registro;
  public $fecha_registro;
  public $tamano_id;
  public $nromotcha;
  public $credito_id;


  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_gestioncochera(tb_gestioncochera_xac,
                                              tb_creditotipo_id, 
                                              tb_cliente_id, 
                                              tb_vehiculoclase_id, 
                                              tb_vehiculomarca_id, 
                                              tb_vehiculomodelo_id,
                                              tb_credito_vehcol, 
                                              tb_gestioncochera_estado, 
                                              tb_cochera_id, 
                                              tb_gestioncochera_tamano,
                                              tb_credito_vehpla,
                                              tb_gestioncochera_vehsermot_chasis,
                                              tb_credito_id
                                              )
                                      VALUES (
                                              1, 
                                              :creditotipo_id, 
                                              :cliente_id, 
                                              :clase_id, 
                                              :marca_id, 
                                              :modelo_id, 
                                              :color_veh, 
                                              :estado_veh, 
                                              :cochera_id, 
                                              :tamano_id,
                                              :placa_veh,
                                              :gestioncochera_vehsermot_chasis,
                                              :tb_credito_id)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_STR);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_STR);
      $sentencia->bindParam(":clase_id", $this->clase_id, PDO::PARAM_STR);
      $sentencia->bindParam(":marca_id", $this->marca_id, PDO::PARAM_STR);
      $sentencia->bindParam(":modelo_id", $this->modelo_id, PDO::PARAM_STR);
      $sentencia->bindParam(":color_veh", $this->color_veh, PDO::PARAM_STR);
      $sentencia->bindParam(":estado_veh", $this->estado_veh, PDO::PARAM_STR);
      $sentencia->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_STR);
      $sentencia->bindParam(":tamano_id", $this->tamano_id, PDO::PARAM_INT);
      $sentencia->bindParam(":placa_veh", $this->placa_veh, PDO::PARAM_INT);
      $sentencia->bindParam(":gestioncochera_vehsermot_chasis", $this->nromotcha, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_id", $this->credito_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $gestioncochera_id = $this->dblink->lastInsertId(); // Obtener el ID generado automáticamente por la base de datos

      $sql2 = "INSERT INTO tb_cocheramovimiento(
                                                  tb_cocheramovimiento_xac, 
                                                  tb_cocheramovimiento_finalidadentrada, 
                                                  tb_cocheramovimiento_destentrada, 
                                                  tb_cocheramovimiento_fechaentrada,
                                                  tb_cocheramovimiento_horaentrada, 
                                                  tb_usuario_id_entrada, 
                                                  tb_cocheramovimiento_obserentrada, 
                                                  tb_gestioncochera_id, 
                                                  tb_movimiento_id_entrada
                                                  ) 
                                            VALUE (
                                                  1, 
                                                  1, 
                                                  :cochera_id, 
                                                  :fecha_registro, 
                                                  :hora_registro, 
                                                  :usuario_id, 
                                                  :observacion_veh, 
                                                  :gestioncochera_id, 
                                                  1
                                                  )";

      $sentencia2 = $this->dblink->prepare($sql2);
      $sentencia2->bindParam(":fecha_registro", $this->fecha_registro, PDO::PARAM_STR);
      $sentencia2->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_STR);
      $sentencia2->bindParam(":hora_registro", $this->hora_registro, PDO::PARAM_STR);
      $sentencia2->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_STR);
      $sentencia2->bindParam(":observacion_veh", $this->observacion_veh, PDO::PARAM_STR);
      $sentencia2->bindParam(":gestioncochera_id", $gestioncochera_id, PDO::PARAM_STR);

      $result2 = $sentencia2->execute();

      $result = $result && $result2; // Comprobar si ambas operaciones fueron exitosas

      $this->dblink->commit();

      return $result ? 1 : 0; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  //Funcion para validar si el usuario y placa ingresado ya existen en la bd 
  function verificar_registrousuario()
  {
    try {
      $sql = "SELECT COUNT(*) as total 
              FROM tb_gestioncochera 
              WHERE tb_cliente_id = :cliente_id 
              AND tb_credito_vehpla = :placa_veh
               AND tb_gestioncochera_xac= 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':cliente_id', $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(':placa_veh', $this->placa_veh, PDO::PARAM_STR);
      $sentencia->execute();
      $result = $sentencia->fetch(PDO::FETCH_ASSOC);
      return $result['total'] > 0;
    } catch (Exception $e) {
      throw $e;
    }
  }
  //Función para extraer el ID del primer movimiento realizado para modificar dicho movimiento
  function extraer_idmovimiento()
  {
    try {
      $sql = " SELECT
                  tb_cocheramovimiento_id
                FROM 
                  tb_cocheramovimiento 
                WHERE 
                  tb_gestioncochera_id = :gestioncochera_id 
                ORDER BY 
                  tb_cocheramovimiento_id ASC 
                LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay vehiculos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }
  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      $result_extraer = $this->extraer_idmovimiento();
      if ($result_extraer["estado"] == 1) {
        $cocheramovimiento_id = $result_extraer["data"][0]["tb_cocheramovimiento_id"];

        $sql = "UPDATE 
                    tb_cocheramovimiento 
                  SET 
                    tb_usuario_id_entrada =:usuario_id, 
                    tb_cocheramovimiento_obserentrada =:cocheramovimiento_obser, 
                    tb_cocheramovimiento_fechaentrada =:cocheramovimiento_fecha, 
                    tb_cocheramovimiento_horaentrada =:cocheramovimiento_hora, 
                    tb_cocheramovimiento_destentrada =:cochera_id
                  WHERE 
                    tb_gestioncochera_id =:gestioncochera_id AND tb_cocheramovimiento_id = :cocheramovimiento_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cocheramovimiento_id", $cocheramovimiento_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cocheramovimiento_hora", $this->hora_registro, PDO::PARAM_STR);
        $sentencia->bindParam(":cocheramovimiento_fecha", $this->fecha_registro, PDO::PARAM_STR);
        $sentencia->bindParam(":cocheramovimiento_obser", $this->observacion_veh, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $sql2 = "UPDATE 
                    tb_gestioncochera
                  SET 
                    tb_cochera_id =:cochera_id,
                    tb_gestioncochera_estado =:gestioncochera_estado
                  WHERE 
                    tb_gestioncochera_id =:gestioncochera_id";

        $sentencia2 = $this->dblink->prepare($sql2);
        $sentencia2->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_INT);
        $sentencia2->bindParam(":gestioncochera_estado", $this->estado_veh, PDO::PARAM_INT);
        $sentencia2->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_INT);

        $result2 = $sentencia2->execute();

        $result = $result && $result2; // Comprobar si ambas operaciones fueron exitosas

        $this->dblink->commit();

        return $result ? 1 : 0; //si es correcto retorna 1
      } else {
        return 0;
      }
    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
      return 0;
    }
  }
  
  function eliminar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE 
                  tb_gestioncochera 
                SET 
                  tb_gestioncochera_xac = 0 
                WHERE 
                  tb_gestioncochera_id = :gestioncochera_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }
  
  function mostrarUno($gestioncochera_id)
  {
    try {
      $sql = "SELECT 
                    gc.*, 
                    c.tb_cliente_nom,
                    vm.tb_vehiculomarca_nom, 
                    vd.tb_vehiculomodelo_nom, 
                    vc.tb_vehiculoclase_nom,
                    u.tb_usuario_id,
                    CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) AS usuario,
                    CONCAT(cm.tb_cocheramovimiento_fechaentrada,'-', cm.tb_cocheramovimiento_horaentrada) AS fecha_hora,
                    cm.tb_cocheramovimiento_fechaentrada AS fecha, 
                    cm.tb_cocheramovimiento_horaentrada AS hora,
                    cm.tb_cocheramovimiento_obserentrada
                FROM tb_gestioncochera gc
                LEFT JOIN tb_cliente c ON gc.tb_cliente_id = c.tb_cliente_id
                LEFT JOIN tb_vehiculomarca vm ON gc.tb_vehiculomarca_id = vm.tb_vehiculomarca_id
                LEFT JOIN tb_vehiculomodelo vd ON gc.tb_vehiculomodelo_id = vd.tb_vehiculomodelo_id
                LEFT JOIN tb_vehiculoclase vc ON gc.tb_vehiculoclase_id = vc.tb_vehiculoclase_id
                LEFT JOIN tb_cocheramovimiento cm ON gc.tb_gestioncochera_id = cm.tb_gestioncochera_id
                LEFT JOIN tb_usuario u ON cm.tb_usuario_id_entrada = u.tb_usuario_id
                WHERE 
                  gc.tb_gestioncochera_id = :gestioncochera_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gestioncochera_id", $gestioncochera_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay cocheras registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }
  function listar_gestioncocheras()
  {
    try {
      $sql = "SELECT 
                  gc.*, 
                  c.tb_cliente_nom,
                  vm.tb_vehiculomarca_nom, 
                  vd.tb_vehiculomodelo_nom, 
                  vc.tb_vehiculoclase_nom,
                  co.tb_cochera_nom,
                  CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) AS usuario,
                  CONCAT(cm.tb_cocheramovimiento_fechaentrada,'-', cm.tb_cocheramovimiento_horaentrada) AS fecha_hora,
                  cm.tb_cocheramovimiento_obserentrada,
                  cm.tb_cocheramovimiento_id,
                  gc.tb_gestioncochera_estado AS estado,
                CASE
                  WHEN gc.tb_gestioncochera_estado = 1 THEN 'Almacenado'
                  WHEN gc.tb_gestioncochera_estado = 2 THEN 'Mantenimiento'
                  WHEN gc.tb_gestioncochera_estado = 3 THEN 'Entregado'
                  WHEN gc.tb_gestioncochera_estado = 4 THEN 'Vendido'
                  WHEN gc.tb_gestioncochera_estado = 5 THEN 'Otro'
                END AS estado
                FROM 
                  tb_gestioncochera gc
                LEFT JOIN tb_cliente c ON gc.tb_cliente_id = c.tb_cliente_id
                LEFT JOIN tb_cochera co ON gc.tb_cochera_id = co.tb_cochera_id
                LEFT JOIN  tb_vehiculomarca vm ON gc.tb_vehiculomarca_id = vm.tb_vehiculomarca_id
                LEFT JOIN tb_vehiculomodelo vd ON gc.tb_vehiculomodelo_id = vd.tb_vehiculomodelo_id
                LEFT JOIN tb_vehiculoclase vc ON gc.tb_vehiculoclase_id = vc.tb_vehiculoclase_id
                LEFT JOIN tb_cocheramovimiento cm ON gc.tb_gestioncochera_id = cm.tb_gestioncochera_id
                LEFT JOIN tb_usuario u ON cm.tb_usuario_id_entrada = u.tb_usuario_id
                WHERE
                  tb_gestioncochera_xac = 1
                GROUP BY 
                  cm.tb_gestioncochera_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay vehiculos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function gestioncochera_autocomplete($dato)
  {
    try {

      $filtro = "%" . $dato . "%";

      $sql = "SELECT *
		            FROM tb_gestioncochera
                WHERE tb_estadollave_motivo LIKE :filtro OR tb_estadollave_tipo LIKE :filtro";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay estados de llave registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarPorPlaca($placa, $estado) {
    try {
      $sql_add ="";
      if ($estado>0) {
        $sql_add .= " AND tb_gestioncochera_estado=:estado ";
      }

      $sql = "SELECT c.* FROM tb_gestioncochera gc
        INNER JOIN tb_cochera c on gc.tb_cochera_id=c.tb_cochera_id
        WHERE tb_credito_vehpla =:placa 
        $sql_add";

      $sentencia = $this->dblink->prepare($sql);
      if ($estado>0) {
        $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);
      }
      $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function cambiarMonto($monto, $movimientoid)
  {
      $this->dblink->beginTransaction();
      try {
          if (is_float($monto)) { // Verificar si $monto es un número de punto flotante
  
              $sql = "UPDATE tb_cocheramovimiento SET monto = :nuevoMonto WHERE tb_cocheramovimiento_id = :movimientoid";
  
              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":nuevoMonto", $monto, PDO::PARAM_STR); // El monto es float, por lo que se usa PDO::PARAM_STR
              $sentencia->bindParam(":movimientoid", $movimientoid, PDO::PARAM_INT);
              $result = $sentencia->execute();
  
              $this->dblink->commit();
  
              return $result; // Si es correcto el ingreso, retorna 1
          } else {
              return 0; // Devolver 0 si $monto no es un número de punto flotante
          }
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
  }

  function buscar_movimiento_sin_salida($gestioncochera_id){
    try {
      $sql = "SELECT * from tb_gestioncochera gc 
      inner join tb_cocheramovimiento cm on gc.tb_gestioncochera_id=cm.tb_gestioncochera_id
      where cm.tb_cocheramovimiento_fechasalida IS NULL
      AND cm.tb_gestioncochera_id=:gestioncochera_id
      LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gestioncochera_id", $gestioncochera_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function llenarDatosCreditos($creditoid, $creditotipo_id){
        
    try {
        if(intval($creditotipo_id) > 1){
            $nombre_tabla = 'tb_creditogarveh';
            if(intval($creditotipo_id) == 2)
            $nombre_tabla = 'tb_creditoasiveh';
            $sql = "SELECT  
            cgv.tb_credito_id as creditoid,
                        cgv.tb_vehiculoclase_id as clase_vehiculo,
                        cgv.tb_vehiculomarca_id as marca_vehiculo,
                        cgv.tb_vehiculotipo_id as tipo_vehiculo,
                        cgv.tb_vehiculomodelo_id as modelo_vehiculo,
                        cgv.tb_credito_vehcol as color,
                        cgv.tb_credito_vehsermot as vehiculo_motor,
                        cgv.tb_credito_vehsercha as vehiculo_chasis,
                        cgv.tb_credito_vehpla as vehiculo_placa   


            from $nombre_tabla cgv
            WHERE 
            cgv.tb_credito_id = :credito_id";
        }
        else{
            $sql = "SELECT 
             gar.tb_credito_id as creditoid,
            gar.tb_vehiculoclase_id as clase_vehiculo,
            gar.tb_vehiculomarca_id as marca_vehiculo,
            gar.tb_garantiatipo_id as tipo_vehiculo,
            gar.tb_vehiculomodelo_id as modelo_vehiculo,
            gar.tb_garantia_vehsermot as vehiculo_motor,
            gar.tb_garantia_vehsercha as vehiculo_chasis,
            gar.tb_garantia_vehpla as vehiculo_placa,
            gar.tb_garantia_vehcol as color

            FROM tb_garantia gar
            INNER JOIN tb_creditomenor cm ON gar.tb_credito_id = cm.tb_credito_id
            WHERE 
            gar.tb_credito_id = :credito_id  AND 
            (gar.tb_garantiatipo_id = 3 and gar.tb_articulo_id=75)";
        }


        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $creditoid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay Estados de llave registrados";
            $retorno["data"] = "";
        }

        return $retorno;
    } catch (Exception $e) {
        throw $e;
    }
}

function llenarCliente($placa){
  try {
          $sql = "SELECT  
          cli.tb_cliente_nom as nombrecliente
          from tb_creditogarveh cgv
          inner join tb_cliente cli on cgv.tb_cliente_id = cli.tb_cliente_id
          WHERE  cgv.tb_credito_vehpla=:placa
          order by tb_credito_reg 
          desc limit 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay placas Registradas a este vehículo";
          $retorno["data"] = "";
      }

      return $retorno;
  } catch (Exception $e) {
      throw $e;
  }
}
}