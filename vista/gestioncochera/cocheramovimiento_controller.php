<?php
require_once('../../core/usuario_sesion.php');
require_once('../gestioncochera/CocheraMovimiento.class.php');
$oCocheraMovimiento = new CocheraMovimiento();

require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';

$action = $_POST['action'];

$oCocheraMovimiento->cocheramovimiento_hora = string_hora($_POST['cocheramovimiento_hora']);
$oCocheraMovimiento->cocheramovimiento_fecha = fecha_mysql($_POST['cocheramovimiento_fecha']);
$oCocheraMovimiento->cocheramovimiento_obs = mb_strtoupper($_POST['cocheramovimiento_obs'], 'UTF-8');
$oCocheraMovimiento->movimiento_id = intval($_POST['movimiento_id']);
$oCocheraMovimiento->finalidad_id = intval($_POST['finalidad_id']);
$oCocheraMovimiento->usuario_id = intval($_POST['usuario_id']);
$oCocheraMovimiento->cocheramovimiento_id = intval($_POST['cocheramovimiento_id']);
$oCocheraMovimiento->gestioncochera_id = intval($_POST['gestioncochera_id']);
$oCocheraMovimiento->cocheramovimiento_dest = mb_strtoupper($_POST['destino_var'], 'UTF-8');

$entrada_salida = intval($_POST['movimiento_id']); //Variable para comparar si es E o S

$data['estado'] = 0;
$data['mensaje'] = 'Existe un error al guardar el Usuario Perfil.';

if ($action == 'insertar') {
    if ($entrada_salida == 1) {
      if ($oCocheraMovimiento->insertar_entrada()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Movimiento registrado correctamente.';
      }
    } 
    elseif ($entrada_salida == 2) {
      $result = $oCocheraMovimiento->mostrarUno($_POST['cocheramovimiento_id']); // Obtener datos de la entrada
        if ($result['estado'] == 1) {
            $cocheramovimiento_fechaentrada = $result['data']['tb_cocheramovimiento_fechaentrada'];
            $cocheramovimiento_horaentrada = $result['data']['tb_cocheramovimiento_horaentrada'];
            $criteriohora = $result['data']['tb_tarifacochera_criteriohora'];
            $precio = $result['data']['PRECIO'];

            // Calcular la diferencia en horas entre entrada y salida
            $fechaHoraEntrada = strtotime($cocheramovimiento_fechaentrada . ' ' . $cocheramovimiento_horaentrada);
            $fechaHoraSalida = strtotime($_POST['cocheramovimiento_fecha'] . ' ' . $_POST['cocheramovimiento_hora']);
            $diferencia_segundos = $fechaHoraSalida - $fechaHoraEntrada;

            if(intval($criteriohora >= 24)){
              $fecha_entrada_calcular = $cocheramovimiento_fechaentrada;
              $fecha_salida_calcular = fecha_mysql($_POST['cocheramovimiento_fecha']);
    
              // Crear objetos DateTime para las dos fechas
              $datetime_inicio = new DateTime($fecha_entrada_calcular);
              $datetime_fin = new DateTime($fecha_salida_calcular);
              // Calcular la diferencia entre las dos fechas
              $interval = $datetime_inicio->diff($datetime_fin);
              // Obtener el número de días de diferencia
              $diferencia_dias = $interval->days;
              $num_turno = $diferencia_dias + 1; //? PARA QUE EL ULTIMO DÍA TAMBIÉN LO TOME COMO TURNO
            }
            else{
              if ($diferencia_segundos < 0) {
                $diferencia_segundos += 24 * 60 * 60; // Ajustar para salidas después de medianoche
              }

              $diferencia_horas = $diferencia_segundos / (60 * 60);

              $num_turno = ceil($diferencia_horas / $criteriohora);
              if ($num_turno == 0) {
                $num_turno = 1;
              }
            }

            $monto = formato_numero($precio * $num_turno);

            if ($oCocheraMovimiento->insertar_salida()) {
              // Guardar el monto calculado en el objeto antes de modificar la gestión cochera
              $oCocheraMovimiento->monto = $monto;

              //intocable
              $oCocheraMovimiento->modificar_gestioncochera();
              $oCocheraMovimiento->modificar_campo($oCocheraMovimiento->cocheramovimiento_id,'tb_cocheramovimiento_monto', $monto,'STR');
              $data['estado'] = 1;
              $data['mensaje'] = 'Movimiento registrado correctamente';
              $data['mensaje2'] = "$diferencia_horas | $criteriohora | $monto | $precio | $num_turno";
              $data['monto'] = $monto; // Añadir el monto calculado a la respuesta
            } else {
                $data['mensaje'] = 'Error al registrar la salida.';
            }
        } else {
            $data['mensaje'] = 'No se encontró el registro de entrada.';
        }
    }

    echo json_encode($data);
}else if ($action == 'EditarMonto') {

  $oCocheraMovimiento->monto = $_POST['monto'];
  $oCocheraMovimiento->cocheramovimiento_id = $_POST['id_movimiento'];
  try {
    if ($oCocheraMovimiento->modificarMonto()) {
      $data['estado'] = 1;
      $data['mensaje'] = 'Monto modificado correctamente';

      $tiene_fecha_salida = intval($_POST['fecha_salida_tiene_valor']);
      if ($tiene_fecha_salida == 0) {
        $oCocheraMovimiento->modificar_campo($oCocheraMovimiento->cocheramovimiento_id, 'tb_cocheramovimiento_fechasalida', date('Y-m-d'), 'STR');
        $oCocheraMovimiento->modificar_campo($oCocheraMovimiento->cocheramovimiento_id, 'tb_cocheramovimiento_horasalida', date("H:i:s"), 'STR');
      }
    } else {
      $data['estado'] = 0;
      $data['mensaje'] = 'Error al modificar';
    }
  } catch (\Throwable $th) {
    $data['estado'] = 0;
    $data['mensaje'] = $th->getMessage();
  }

  echo json_encode($data);
} elseif ($action == 'tabla') {
  $td_cocheramovimiento = '';
  $result = $oCocheraMovimiento->listar_movimientoEntradaSalida();
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $finalidad_entrada = "";
      $finalidad_salida = "";
      $movimiento_entrada = "";
      $movimiento_salida = "";

      switch ($value['tb_cocheramovimiento_finalidadentrada']) {
        case 1:
          $finalidad_entrada = '<span class="badge bg-green">Almacenaje</span>';
          break;
        case 2:
          $finalidad_entrada = '<span class="badge bg-orange">Mantenimiento</span>';
          break;
        case 3:
          $finalidad_entrada = '<span class="badge bg-aqua">Entrega</span>';
          break;
        case 4:
          $finalidad_entrada = '<span class="badge bg-red">Venta</span>';
          break;
        case 5:
          $finalidad_entrada = '<span class="badge bg-purple">Otro</span>';
          break;
        default:
          break;
      }

      switch ($value['tb_cocheramovimiento_finalidadsalida']) {
        case 1:
          $finalidad_salida = '<span class="badge bg-green">Almacenaje</span>';
          break;
        case 2:
          $finalidad_salida = '<span class="badge bg-orange">Mantenimiento</span>';
          break;
        case 3:
          $finalidad_salida = '<span class="badge bg-aqua">Entrega</span>';
          break;
        case 4:
          $finalidad_salida = '<span class="badge bg-red">Venta</span>';
          break;
        case 5:
          $finalidad_salida = '<span class="badge bg-purple">Otro</span>';
          break;
        default:
          break;
      }

      switch ($value['tb_movimiento_id_entrada']) {
        case 1:
          $movimiento_entrada = '<span class="badge bg-green">Entrada</span>';
          break;
        case 2:
          $movimiento_entrada = '<span class="badge bg-green">Salida</span>';
          break;
        default:
          break;
      }

      switch ($value['tb_movimiento_id_salida']) {
        case 1:
          $movimiento_salida = '<span class="badge bg-red">Entrada</span>';
          break;
        case 2:
          $movimiento_salida = '<span class="badge bg-red">Salida</span>';
          break;
        default:
          break;
      }


      $td_cocheramovimiento .= '
            <tr id="tabla_cabecera_fila">
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_id'] . '</td>
              <td id="tabla_fila">' . $movimiento_entrada . '</td>
              <td id="tabla_fila">' . $finalidad_entrada . '</td>
              <td id="tabla_fila">' . $value['DESTINO_ENTRADA'] . '</td>
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_fechaentrada'] . '</td>
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_horaentrada'] . '</td>
              <td id="tabla_fila">' . $value['USUARIO_ENTRADA'] . '</td>
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_obserentrada'] . '</td>
              <td id="tabla_fila">' . $movimiento_salida . '</td>
              <td id="tabla_fila">' . $finalidad_salida . '</td>
              <td id="tabla_fila">' . $value['DESTINO_SALIDA'] . '</td>
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_fechasalida'] . '</td>
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_horasalida'] . '</td>
              <td id="tabla_fila">' . $value['USUARIO_SALIDA'] . '</td>
              <td id="tabla_fila">' . $value['tb_cocheramovimiento_obsersalida'] . '</td>
            </tr>';
    }
  }
  $result = NULL;

  $tabla = '
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">#</th>
        <th id="tabla_cabecera_fila">TIPO DE MOVIMIENTO</th>
        <th id="tabla_cabecera_fila">FINALIDAD DE ENTRADA</th>
        <th id="tabla_cabecera_fila">DESTINO DE ENTRADA</th>
        <th id="tabla_cabecera_fila">FECHA DE ENTRADA</th>
        <th id="tabla_cabecera_fila">HORA DE ENTRADA</th>
        <th id="tabla_cabecera_fila">ENCARGADO DE ENTRADA</th>
        <th id="tabla_cabecera_fila">OBSERVACIÓN DE ENTRADA</th>
        <th id="tabla_cabecera_fila">TIPO DE MOVIMIENTO</th>
        <th id="tabla_cabecera_fila">FINALIDAD DE SALIDA</th>
        <th id="tabla_cabecera_fila">DESTINO DE SALIDA</th>
        <th id="tabla_cabecera_fila">FECHA DE SALIDA</th>
        <th id="tabla_cabecera_fila">HORA DE SALIDA</th>
        <th id="tabla_cabecera_fila">ENCARGADO DE SALIDA</th>
        <th id="tabla_cabecera_fila">OBSERVACIÓN DE SALIDA</th>
      </tr>' . $td_cocheramovimiento;

  echo $tabla;
} elseif ($action == 'eliminar') {
  $cocheramovimiento_id = intval($_POST['id_movimiento']);

  $data['estado'] = 0;

  try {
    if ($oCocheraMovimiento->eliminar($cocheramovimiento_id)) {
      $data['estado'] = 1;
      $data['mensaje'] = 'Movimiento eliminado correctamente';
    }
  } catch (Exception $e) {
    $data['mensaje'] = 'Existe un error al eliminar movimiento' . $e->getMessage();
  }

  echo json_encode($data);
}
