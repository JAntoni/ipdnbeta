<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL .'gestioncochera/GestionCochera.class.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../gestioncochera/GestionCochera.class.php');
  }

  $oGestionCochera = new GestionCochera();

  $result = $oGestionCochera->listar_gestioncocheras();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $estado = "";
      if ($value['estado'] == 'Almacenado'){
        $estado = '<span class="badge bg-green">'.$value['estado'].'</span>';
      }elseif ($value['estado'] == 'Mantenimiento'){
        $estado = '<span class="badge bg-orange">'.$value['estado'].'</span>';
      }elseif($value['estado'] == 'Entregado'){
        $estado = '<span class="badge bg-aqua">'.$value['estado'].'</span>';
      }elseif($value['estado'] == 'Vendido'){
        $estado = '<span class="badge bg-red">'.$value['estado'].'</span>';
      }elseif($value['estado'] == 'Otro'){
        $estado = '<span class="badge bg-purple">'.$value['estado'].'</span>';
      }

      $tr .='<tr id="tabla_cabecera_fila">';
        $tr.='
          <td id="tabla_fila">'.$value['tb_gestioncochera_id'].'</td>
          <td id="tabla_fila">'.$value['tb_cliente_nom'].'</td>
          <td id="tabla_fila">'.$value['tb_credito_vehpla'].'</td>
          <td id="tabla_fila">'.$value['tb_vehiculomarca_nom'].'</td>
          <td id="tabla_fila">'.$value['tb_vehiculomodelo_nom'].'</td>
          <td id="tabla_fila">'.$value['tb_vehiculoclase_nom'].'</td>
          <td id="tabla_fila">'.$value['tb_cochera_nom'].'</td>
          <td id="tabla_fila">'.$value['fecha_hora'].'</td>
          <td id="tabla_fila">'.$value['usuario'].'</td>
          <td id="tabla_fila">'.$estado.'</td>
          <td id="tabla_fila" align="center">
              <a class="btn btn-info btn-xs" title="Ver" onclick="gestioncochera_form(\'L\','.$value['tb_gestioncochera_id'].')"><i class="fa fa-eye"></i> Ver</a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="gestioncochera_form(\'M\','.$value['tb_gestioncochera_id'].')"><i class="fa fa-edit"></i> Editar</a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="gestioncochera_form(\'E\','.$value['tb_gestioncochera_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
              <a class="btn btn-success btn-xs" title="Realizar Movimiento" onclick="agregar_movimiento(\'I\','.$value['tb_cocheramovimiento_id'].','.$value['tb_gestioncochera_id'].')">Movimiento</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_cocheras" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">PLACA</th>
      <th id="tabla_cabecera_fila">MARCA</th>
      <th id="tabla_cabecera_fila">MODELO</th>
      <th id="tabla_cabecera_fila">TIPO DE VEHICULO</th>
      <th id="tabla_cabecera_fila">COCHERA</th>
      <th id="tabla_cabecera_fila">FECHA DE INGRESO</th>
      <th id="tabla_cabecera_fila">RESPONSABLE</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila">OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
