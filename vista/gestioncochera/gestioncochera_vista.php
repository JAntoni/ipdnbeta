<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="gestioncochera_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="panel panel-info">
							<div class="panel-body">
								<form id="form_buscarcliente_placa" class="form-inline" role="form">
									<div class="form-group">
										<input type="text" name="txt_placa_buscar" id="txt_placa_buscar" class="form-control input-sm mayus input-shadow" autocomplete="off" size="30" placeholder="Busca una placa aquí" value="<?php echo $placa; ?>">
									</div>
									<button type="button" class="btn btn-info btn-sm" onclick="Buscarclientexplaca()">
										<i class="fa fa-search"></i>
									</button>
									<div class="form-group">
										<label for="txt_cocheramovimiento_hora" class="control-label" style="text-align: right; margin-left: 20px;">Cliente:</label>
										<input type="text" name="txt_clientebuscar" id="txt_clientebuscar" class="form-control input-sm mayus input-shadow" autocomplete="off" size="30" readonly value="<?php echo $placa; ?>">
									</div>
									<div class="form-group">
										<a class="btn btn-primary btn-sm" onclick="gestioncochera_form('I',0)" style="margin-left: 10px;">
											<i class="fa fa-plus"></i> Nuevo ingreso vehicular
										</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>


				<br>

				<!--Fin Nuevo codigo-->
				<div class="row">
					<div class="col-sm-12">
						<div id="div_gestioncochera_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php //require_once('gestioncochera_tabla.php');
							?>
						</div>
					</div>
				</div>
			</div>

			<div id="div_modal_gestioncochera_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE COCHERAS-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<div id="div_modal_cocheramovimiento_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE COCHERAS-->
			</div>

			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<section class="content">

		<div class="box">
			<div class="box-header with-border" style="text-align: center;">
				<h3 class="box-title" style="font-weight: bold; font-size: 24px; ">LISTA DE MOVIMIENTOS REALIZADOS</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="cocheramovimiento_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="box-header">
					<?php require_once('cocheramovimiento_filtro.php'); ?>
				</div>
				<p></p>
				<div class="row">
					<div class="col-sm-12">
						<div id="div_cocheramovimiento_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php //require_once('cocheramovimiento_tabla.php');
							?>
						</div>
					</div>
				</div>
			</div>

			<div id="div_modal_cocherapago_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE COCHERAS-->
			</div>

			<div id="div_modal_resumen_pago_cochera">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE COCHERAS-->
			</div>

			<div id="div_modal_pagocochera_egreso">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE COCHERAS-->
			</div>

			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>