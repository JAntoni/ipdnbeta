<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../gestioncochera/CocheraMovimiento.class.php');
  $oCocheraMovimiento = new CocheraMovimiento();
  require_once('../funciones/funciones.php');
  
  require_once('../gestioncochera/GestionCochera.class.php');
  $oGestionCochera = new GestionCochera();

  

  $direc = 'gestioncochera';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  //$cocheramovimiento_id = $_POST['cocheramovimiento_id'];
  $gestioncochera_id = $_POST['gestioncochera_id'];

  $cocheramovimiento_id = 0;
  $result = $oCocheraMovimiento->obtenerUltimoIdCocheramovimiento($gestioncochera_id);
    if($result['estado'] == 1){
      $cocheramovimiento_id = $result['data']['tb_cocheramovimiento_id']; 
    }
  $result = NULL;

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Movimiento Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Movimiento ';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Movimiento ';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Movimiento ';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

?>
<style>
  .hide {
    display: none;
  }
</style>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cocheramovimiento" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      
      <div class="modal-body">
        <input type="hidden" id="directorio" value="cocheramovimiento">
        <label for="txt_filtro" class="control-label">Registro de nuevo Movimiento</label>
        <div class="panel shadow-simple">
            <form id="form_cocheramovimiento" method="post">
                <input type="hidden" name="action" value="<?php echo $action;?>">
                <input type="hidden" name="hdd_cocheramovimiento_id" id="hdd_cocheramovimiento_id" value="<?php echo $cocheramovimiento_id;?>">
                <input type="hidden" name="hdd_gestioncochera_id" id="hdd_gestioncochera_id" value="<?php echo $gestioncochera_id;?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-md-6" id="movimiento">
                            <label for="cmb_movimiento_id" class="control-label">Movimiento:</label>
                            <select id="cmb_movimiento_id" name="cmb_movimiento_id" class="form-control input-sm">
                                <option>Seleccione...</option>
                                <option value="1" <?php if ($movimiento_id == 1) echo 'selected'; ?>>Entrada</option>
                                <option value="2" <?php if ($movimiento_id == 2) echo 'selected'; ?>>Salida</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6" id="finalidad">
                            <label for="cmb_finalidad_id" class="control-label">Finalidad:</label>
                            <select id="cmb_finalidad_id" name="cmb_finalidad_id" class="form-control input-sm">
                                <option>Seleccione...</option>
                                <option value="1" <?php if ($finalidad_id == 1) echo 'selected'; ?>>Almacenaje</option>
                                <option value="2" <?php if ($finalidad_id == 2) echo 'selected'; ?>>Mantenimiento</option>
                                <option value="3" <?php if ($finalidad_id == 3) echo 'selected'; ?>>Entrega</option>
                                <option value="4" <?php if ($finalidad_id == 4) echo 'selected'; ?>>Venta</option>
                                <option value="5" <?php if ($finalidad_id == 4) echo 'selected'; ?>>Otro</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 finalidad-div hide" id="finalidad-cochera">
                            <label for="cmb_cochera_id" class="control-label">Cochera</label>
                            <select name="cmb_cochera_id" id="cmb_cochera_id" class="form-control">
                                <?php require_once('../cochera/cochera_select.php'); ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 finalidad-div hide" id="finalidad-taller">
                            <label for="txt_taller_nom" class="control-label">Taller</label>
                            <input type="text" name="txt_taller_nom" id="txt_taller_nom" class="form-control input-sm " autocomplete="off" size="48">
                        </div>
                        <div class="form-group col-md-4 finalidad-div hide" id="finalidad-entrega">
                            <label for="cmb_cliente_id" class="control-label">Cliente</label>
                            <input type="text" name="txt_cliente_id" id="txt_cliente_id" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48">
                            <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" >
                        </div>
                        <div class="form-group col-md-4 finalidad-div hide" id="finalidad-venta">
                            <label for="txt_venta_nom" class="control-label">Comprador</label>
                            <input type="text" name="txt_venta_nom" id="txt_venta_nom" class="form-control input-sm " autocomplete="off" size="48">
                        </div>
                        <div class="form-group col-md-4 finalidad-div hide" id="finalidad-otro">
                            <label for="txt_otro_nom" class="control-label">Otro</label>
                            <input type="text" name="txt_otro_nom" id="txt_otro_nom" class="form-control input-sm " autocomplete="off" size="48">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="txt_cocheramovimiento_hora" class="control-label">Hora del Movimiento</label>
                            <input type="time" class="form-control input-sm" name="txt_cocheramovimiento_hora" id="txt_cocheramovimiento_hora" style="width: 100%;" autocomplete="off" value="<?php echo $hora_ingreso;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txt_cocheramovimiento_fecha" class="control-label">Fecha del Movimiento</label>
                            <input type='date' class="form-control input-sm" name="txt_cocheramovimiento_fecha" id="txt_cocheramovimiento_fecha" style="width: 100%;" autocomplete="off" value="<?php echo $fecha_ingreso;?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="txt_cocheramovimiento_obs" class="control-label">Observación:</label>
                            <textarea class="form-control input-sm" name="txt_cocheramovimiento_obs" id="txt_cocheramovimiento_obs" ><?php echo $estadollave_obs?></textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txt_usuario_id" class="control-label">Responsable del Movimiento</label>
                            <input type="text" name="txt_usuario_id" id="txt_usuario_id" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $usuario_nom;?>">
                            <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">
                        </div>
                        <div class="form-group col-md-2">
                          <br>
                          <button type="button" class="btn btn-primary btn-sm" onclick="registrar_movimiento()"><i class="fa fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <p></p>
        <br>
        <label for="txt_filtro" class="control-label">Lista de Movimientos Realizados</label>
        <div class="panel shadow-simple">
          <div class="panel-body">
            <table class="table table-bordered">
              <tbody id="table_cocheramovimiento"></tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>

    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/gestioncochera/cocheramovimiento_form.js';?>"></script>