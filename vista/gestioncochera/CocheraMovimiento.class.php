<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class CocheraMovimiento extends Conexion
{

  public $cocheramovimiento_hora;
  public $cocheramovimiento_fecha;
  public $cocheramovimiento_obs;
  public $movimiento_id;
  public $finalidad_id;
  public $usuario_id;
  public $cocheramovimiento_id;
  public $gestioncochera_id;
  public $cocheramovimiento_dest;
  public $estado_autocochera;
  public $monto;

  //FUNCION PARA INSERTAR DATOS DE ENTRADA EN LA TABLA COCHERAMOVIMIENTO
  function insertar_entrada()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_cocheramovimiento(
                                                    tb_cocheramovimiento_xac, 
                                                    tb_cocheramovimiento_finalidadentrada, 
                                                    tb_cocheramovimiento_destentrada, 
                                                    tb_cocheramovimiento_fechaentrada,
                                                    tb_cocheramovimiento_horaentrada, 
                                                    tb_usuario_id_entrada, 
                                                    tb_cocheramovimiento_obserentrada, 
                                                    tb_gestioncochera_id, 
                                                    tb_movimiento_id_entrada,
                                                    tb_estado_autocochera) 
                                            VALUE (
                                                    1, 
                                                    1, 
                                                    :cochera_id, 
                                                    :fecha_registro, 
                                                    :hora_registro, 
                                                    :usuario_id, 
                                                    :observacion_veh, 
                                                    :gestioncochera_id, 
                                                    1,
                                                    0)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha_registro", $this->cocheramovimiento_fecha, PDO::PARAM_STR);
      $sentencia->bindParam(":cochera_id", $this->cocheramovimiento_dest, PDO::PARAM_STR);
      $sentencia->bindParam(":hora_registro", $this->cocheramovimiento_hora, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_STR);
      $sentencia->bindParam(":observacion_veh", $this->cocheramovimiento_obs, PDO::PARAM_STR);
      $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  //FUNCION PARA INSERTAR DATOS DE SALIDA EN EL REGISTRO DE ENTRADA CORRESPONDIENTE
  function insertar_salida()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE 
                  tb_cocheramovimiento
                SET 
                  tb_cocheramovimiento_finalidadsalida =:finalidad, 
                  tb_cocheramovimiento_destsalida =:cocheramovimiento_dest, 
                  tb_cocheramovimiento_fechasalida =:cocheramovimiento_fecha, 
                  tb_cocheramovimiento_horasalida =:cocheramovimiento_hora, 
                  tb_usuario_id_salida =:usuario_id, 
                  tb_cocheramovimiento_obsersalida =:cocheramovimiento_obser, 
                  tb_movimiento_id_salida =:movimiento_id
                WHERE
                  tb_gestioncochera_id =:gestioncochera_id 
                AND 
                  tb_movimiento_id_salida IS NULL";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":finalidad", $this->finalidad_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cocheramovimiento_dest", $this->cocheramovimiento_dest, PDO::PARAM_STR);
      $sentencia->bindParam(":cocheramovimiento_fecha", $this->cocheramovimiento_fecha, PDO::PARAM_STR);
      $sentencia->bindParam(":cocheramovimiento_hora", $this->cocheramovimiento_hora, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_STR);
      $sentencia->bindParam(":cocheramovimiento_obser", $this->cocheramovimiento_obs, PDO::PARAM_STR);
      $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_STR);
      $sentencia->bindParam(":movimiento_id", $this->movimiento_id, PDO::PARAM_STR);

      $result = $sentencia->execute();

      //$modificar_gestioncochera = $this->modificar_gestioncochera();

      $this->dblink->commit();
      return $result; //&& $modificar_gestioncochera? 1 : 0; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }
  //FUNCION USADA PARA ACTUALIZAR GESTIONCOCHERA AL MOMENTO DE REALIZAR UN MOVIMIENTO DE SALIDA
  function modificar_gestioncochera()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE 
                    tb_gestioncochera
                  SET
                    tb_gestioncochera_estado =:cocheramovimiento_finalidad,
                    tb_cochera_id = CASE 
                                    WHEN :cocheramovimiento_finalidad = 1 THEN :cochera_id 
                                    ELSE tb_cochera_id
                                    END
                  WHERE tb_gestioncochera_id =:gestioncochera_id";

      $sentencia = $this->dblink->prepare($sql);
      // Transformar la variable cocheramovimiento_dest a int si la finalidad es 1
      $cocheraIdParam = ($this->finalidad_id == 1) ? intval($this->cocheramovimiento_dest) : $this->cocheramovimiento_dest;
      $sentencia->bindParam(":cocheramovimiento_finalidad", $this->finalidad_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cochera_id", $cocheraIdParam, PDO::PARAM_INT);
      $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result ? 1 : 0; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
      return 0;
    }
  }
  //FUNCION PARA QUE NO SE CREE UNA ENTRADA SI AUN HAY UN MOVIMIENTO PENDIENTE
  function verificar_movimientoentrada($gestioncochera_id)
  {
    try {
      $sql = "SELECT COUNT(*) as total 
                FROM tb_cocheramovimiento 
                WHERE tb_gestioncochera_id = :vehiculo_id 
                AND tb_movimiento_id_entrada = 1 AND tb_movimiento_id_salida";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':vehiculo_id', $gestioncochera_id, PDO::PARAM_INT);
      $sentencia->execute();
      $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

      return $resultado['total'] > 0;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarUno($tb_cocheramovimiento_id)
  {
    try {
      $sql = "SELECT cm.*,tcd.tb_tarifacocheradetalle_precio AS PRECIO ,
	      tc.tb_tarifacochera_criteriohora
        FROM tb_cocheramovimiento cm 
        INNER JOIN tb_gestioncochera gc ON cm.tb_gestioncochera_id = gc.tb_gestioncochera_id
        LEFT JOIN tb_cochera co ON gc.tb_cochera_id = co.tb_cochera_id
        LEFT JOIN tb_tarifacochera tc ON co.tb_cochera_id = tc.tb_cochera_id
        LEFT JOIN tb_tarifacocheradetalle tcd ON (tc.tb_tarifacochera_id = tcd.tb_tarifacochera_id and gc.tb_gestioncochera_tamano = tcd.tb_tarifacocheradetalle_tamano)
        WHERE cm.tb_cocheramovimiento_id =:tb_cocheramovimiento_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_cocheramovimiento_id", $tb_cocheramovimiento_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  //FUNCION PARA LISTRAR LOS MOVIMIENTOS DE ENTRADA/SALIDA REALIZADOS EN INTERFAZ HISTORIAL
  function listar_movimientoEntradaSalida()
  {
    try {
      $sql = "SELECT 
                  cm.*,
                  CONCAT(u1.tb_usuario_nom, ' ', u1.tb_usuario_ape) AS USUARIO_ENTRADA,
                  CONCAT(u2.tb_usuario_nom, ' ', u2.tb_usuario_ape) AS USUARIO_SALIDA,
                CASE
                  WHEN cm.tb_cocheramovimiento_finalidadentrada = 1  THEN (SELECT tb_cochera_nom FROM tb_cochera WHERE tb_cochera_id = cm.tb_cocheramovimiento_destentrada)
                  WHEN cm.tb_cocheramovimiento_finalidadentrada = 3  THEN (SELECT tb_cliente_nom FROM tb_cliente WHERE tb_cliente_id = cm.tb_cocheramovimiento_destentrada)
                  ELSE cm.tb_cocheramovimiento_destentrada
                END AS DESTINO_ENTRADA,
                CASE
                  WHEN cm.tb_cocheramovimiento_finalidadsalida = 1  THEN (SELECT tb_cochera_nom FROM tb_cochera WHERE tb_cochera_id = cm.tb_cocheramovimiento_destsalida)
                  WHEN cm.tb_cocheramovimiento_finalidadsalida = 3  THEN (SELECT tb_cliente_nom FROM tb_cliente WHERE tb_cliente_id = cm.tb_cocheramovimiento_destsalida)
                  ELSE cm.tb_cocheramovimiento_destsalida
                END AS DESTINO_SALIDA
                FROM 
                  tb_cocheramovimiento cm 
                LEFT JOIN 
                  tb_usuario u1 ON cm.tb_usuario_id_entrada = u1.tb_usuario_id
                LEFT JOIN 
                  tb_usuario u2 ON cm.tb_usuario_id_salida = u2.tb_usuario_id
                WHERE 
                  tb_cocheramovimiento_xac = 1 AND  tb_gestioncochera_id = :gestioncochera_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gestioncochera_id", $this->gestioncochera_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay movimientos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }
  //FUNCION PARA EL FILTRADO DE DATOS
  function filtrar_movimiento($filtro_fec1, $filtro_fec2, $placa, $cochera){
    if (empty($filtro_fec1) || empty($filtro_fec2)) {
      $retorno["estado"] = 0;
      $retorno["mensaje"] = "Faltan datos necesarios para filtrar los movimientos.";
      $retorno["data"] = "";
      return $retorno;
    }

    // Construir consulta SQL con todas las condiciones
    $sql = "SELECT cm.tb_cocheramovimiento_id AS ID,
      gc.tb_gestioncochera_id AS ID_VEHICULO,
      gc.tb_credito_vehpla AS PLACA,
      vma.tb_vehiculomarca_nom AS MARCA,
      vmo.tb_vehiculomodelo_nom AS MODELO,
      vc.tb_vehiculoclase_nom AS CLASE,
      co.tb_cochera_nom AS COCHERA,
      CONCAT(cm.tb_cocheramovimiento_fechaentrada, ' ', cm.tb_cocheramovimiento_horaentrada) AS FECHA_HORA_ENTRADA,
      CONCAT(cm.tb_cocheramovimiento_fechasalida, ' ', cm.tb_cocheramovimiento_horasalida) AS FECHA_HORA_SALIDA,
      tb_cocheramovimiento_fechaentrada,
      cm.tb_cocheramovimiento_fechasalida,
      tcd.tb_tarifacocheradetalle_precio AS PRECIO,
      tc.tb_tarifacochera_criteriohora AS CRITERIO,
      tcd.tb_tarifacocheradetalle_turno AS TURNO, 
      CASE
      WHEN cm.tb_estado_autocochera = '0' THEN 'PENDIENTE DE PAGO' 
      WHEN cm.tb_estado_autocochera= '1' THEN 'PAGADO' 
      END as ESTADO,
      cm.tb_cocheramovimiento_monto as monto,
      tb_cliente_nom,
      tb_cliente_emprs
    FROM tb_cocheramovimiento cm
    INNER JOIN tb_gestioncochera gc ON cm.tb_gestioncochera_id = gc.tb_gestioncochera_id 
    INNER JOIN tb_cliente cli ON cli.tb_cliente_id = gc.tb_cliente_id
    LEFT JOIN tb_cochera co ON co.tb_cochera_id = cm.tb_cocheramovimiento_destentrada
    LEFT JOIN tb_tarifacochera tc ON co.tb_cochera_id = tc.tb_cochera_id
    LEFT JOIN tb_tarifacocheradetalle tcd ON (tc.tb_tarifacochera_id = tcd.tb_tarifacochera_id and gc.tb_gestioncochera_tamano = tcd.tb_tarifacocheradetalle_tamano)
    LEFT JOIN tb_vehiculoclase vc ON gc.tb_vehiculoclase_id = vc.tb_vehiculoclase_id
    INNER JOIN tb_vehiculomarca vma ON gc.tb_vehiculomarca_id = vma.tb_vehiculomarca_id
    INNER JOIN tb_vehiculomodelo vmo ON gc.tb_vehiculomodelo_id = vmo.tb_vehiculomodelo_id 
    where cm.tb_cocheramovimiento_xac = 1 
    AND cm.tb_cocheramovimiento_fechaentrada BETWEEN :filtro_fec1 AND :filtro_fec2
    ";

    // Verificar si se deben agregar condiciones adicionales
    if (!empty($placa)) {
      $sql .= " AND gc.tb_credito_vehpla = :placa";
    }
    if (!empty($cochera) && $cochera!=0) {
      $sql .= " AND co.tb_cochera_id = :cochera ";
    }

    try {
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro_fec1", $filtro_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":filtro_fec2", $filtro_fec2, PDO::PARAM_STR);
      if (!empty($placa)) {
        $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
      }
      if (!empty($cochera) && $cochera!=0) {
        $sentencia->bindParam(":cochera", $cochera, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); // Para liberar memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay movimientos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function modificarestadoAuto()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_cocheramovimiento SET tb_estado_autocochera =1";

      $sentencia = $this->dblink->prepare($sql);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }


  public function modificarMonto(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_cocheramovimiento SET tb_cocheramovimiento_monto =:monto  WHERE
          tb_cocheramovimiento_id =:cocheramovimiento_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":monto", $this->monto, PDO::PARAM_STR);
      $sentencia->bindParam(":cocheramovimiento_id", $this->cocheramovimiento_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  

  function modificar_campo($movimiento_id, $movimiento_columna, $movimiento_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($movimiento_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
        $sql = "UPDATE tb_cocheramovimiento SET " . $movimiento_columna . " = :movimiento_valor WHERE tb_cocheramovimiento_id = :movimiento_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":movimiento_id", $movimiento_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":movimiento_valor", $movimiento_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":movimiento_valor", $movimiento_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        if ($result == 1)
          $this->dblink->commit();

        return $result;
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  

  public function listarPagoAgrupado($fecha1, $fecha2,$cochera)
  {
    try {
      $sql_add ="";
      if (!empty($cochera) && $cochera!=0) {
        $sql_add .= " AND co.tb_cochera_id = :cochera ";
      }
      $sql = "SELECT
      gc.tb_credito_vehpla AS placa,
      COUNT(cm.tb_cocheramovimiento_fechasalida) AS movimientos,
      GROUP_CONCAT(cm.tb_cocheramovimiento_id) AS movimientos_id,
      gc.tb_gestioncochera_id,
      co.tb_cochera_nom AS cochera,
      co.tb_cochera_id,
      cm.tb_usuario_id_entrada,
      co.tb_proveedor_id,
      SUM(cm.tb_cocheramovimiento_monto) AS monto,
      mar.tb_vehiculomarca_nom,
      mode.tb_vehiculomodelo_nom,
      tb_cliente_nom,
      tb_cliente_emprs,
      tb_cocheramovimiento_fechaentrada,
      tb_cocheramovimiento_fechasalida
  FROM
      tb_cocheramovimiento cm
      INNER JOIN tb_gestioncochera gc ON cm.tb_gestioncochera_id = gc.tb_gestioncochera_id 
      iNNER JOIN tb_cliente cli ON cli.tb_cliente_id = gc.tb_cliente_id
      LEFT JOIN tb_vehiculomarca mar ON mar.tb_vehiculomarca_id = gc.tb_vehiculomarca_id 
      LEFT JOIN tb_vehiculomodelo mode ON mode.tb_vehiculomodelo_id = gc.tb_vehiculomodelo_id
      LEFT JOIN tb_cochera co ON co.tb_cochera_id = cm.tb_cocheramovimiento_destentrada
      LEFT JOIN tb_tarifacochera tc ON co.tb_cochera_id = tc.tb_cochera_id
      LEFT JOIN tb_tarifacocheradetalle tcd ON (tc.tb_tarifacochera_id = tcd.tb_tarifacochera_id AND gc.tb_gestioncochera_tamano = tcd.tb_tarifacocheradetalle_tamano)
  WHERE
      cm.tb_cocheramovimiento_xac = 1 
      AND (cm.tb_cocheramovimiento_fechasalida IS NOT NULL OR cm.tb_cocheramovimiento_fechasalida <> '' OR cm.tb_cocheramovimiento_fechasalida IS NULL)
      AND DATE(cm.tb_cocheramovimiento_fechaentrada) BETWEEN :filtro_fec1 AND :filtro_fec2
      AND cm.tb_estado_autocochera=0
      $sql_add 
  GROUP BY
      gc.tb_gestioncochera_vehsermot_chasis ,
      co.tb_cochera_id;
   ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro_fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":filtro_fec2", $fecha2, PDO::PARAM_STR);
      if ($cochera!=0) {
        $sentencia->bindParam(":cochera", $cochera, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay movimientos que listar";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function obtenerUltimoIdCocheramovimiento($gestioncochera_id){
    try {
      $sql = "SELECT tb_cocheramovimiento_id 
      FROM tb_cocheramovimiento
      WHERE tb_gestioncochera_id = :gestioncochera_id
      ORDER BY tb_cocheramovimiento_id DESC 
      LIMIT 1;
      ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gestioncochera_id", $gestioncochera_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros en la tabla";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function eliminar($cocheramovimiento_id)
  {
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_cocheramovimiento SET tb_cocheramovimiento_xac=0 WHERE tb_cocheramovimiento_id=:cocheramovimiento_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cocheramovimiento_id", $cocheramovimiento_id, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
  }


  function eliminarMovimientoGestion($gestioncochera_id)
  {
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_cocheramovimiento SET tb_cocheramovimiento_xac=0 WHERE tb_gestioncochera_id=:gestioncochera_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":gestioncochera_id", $gestioncochera_id, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
  }



}
