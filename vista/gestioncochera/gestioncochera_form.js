function credito_cliente_select(cliente_id,creditoid) {
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "llave/llave_credito_select.php",
	  async: true,
	  dataType: "html",
	  data: {
		cliente_id: cliente_id, //estos datos se envian con el metodo post
		creditoid: creditoid
	  },
	  beforeSend: function () {
		$("#cmb_credito_id").html("<option>Cargando...</option>");
	  },
	  success: function (data) {
		console.log(data)
		$("#cmb_credito_id").html(data);
		$("#cmb_credito_id").change();

	  },
	  complete: function (data) {
		//console.log(data);
	  },
	  error: function (data) {
		alert(data.responseText);
	  },
	});
  }

  function confDatosVeh(creditoid, creditotipo_id) {
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "gestioncochera/gestioncochera_controller.php",
	  async: true,
	  dataType: "json",
	  data: {
		action: "llenardatos",
		//el primero va al controller,el segundo es el parametro
		creditotipo_id:creditotipo_id,
		credito_id:creditoid
	  },
	  beforeSend: function () {},
	  success: function (data) {
	   console.log(data);
	   console.log(data.placa);
	    $("#hdd_clase_id").val(parseInt(data.clase_vehiculo));
		$("#txt_placa_nom").val(data.placa);
		$("#cmb_marca").val(parseInt(data.marcaid));
		$("#cmb_vehiculotipo_id_form").val(parseInt(data.tipovehiculo));
		$("#cmb_modelo").val(parseInt(data.modeloid));
		$("#txt_color_nom").val(data.color);
		$("#hdd_llave_motor").val(data.motor);
		$("#hdd_llave_chasis").val(data.chasis);
		$("#hdd_llave_placa").val(data.placa);
		
	  },
	  complete: function (data) {
		//console.log(data);
	  },
	  error: function (data) {
		alert(data.responseText);
	  },
	});
  }


$(document).ready(function(){

	confDatosVeh($("#hdd_gestioncochera_credito").val(), $("#hdd_gestioncochera_tipocredito").val());

	credito_cliente_select($("#hdd_cliente_id").val(),$("#hdd_gestioncochera_credito").val());


	$("#cmb_credito_id").change(function (event) {
		$("#hdd_gestioncochera_tipocredito").val(parseInt($('#cmb_credito_id>option:selected').attr("data-tipocredito")));
		var creditotipo_id =$("#hdd_gestioncochera_tipocredito").val();
		var creditoid =$("#cmb_credito_id").val();

		if (parseInt(creditoid) == 0) {
		  $("#txt_placa_nom").val('');
		  $("#cmb_marca").val(0);
		  $("#txt_clase_nom").val(''); 
		  $("#cmb_modelo").val(0);
		  $("#txt_color_nom").val('');
		  $("#cmb_tamanoveh_id").val(0);
	
		} else confDatosVeh(creditoid, creditotipo_id);
	  });

	  $("#cmb_credito_id").change();

	  

	//console.log('uasaios oai sioa sia 3333333')
    //AUTOCOMPLETE CLIENTE
    $("#txt_cliente_id").autocomplete({
		
        minLength: 1,
        source: function(request, response){
            $.getJSON(
            VISTA_URL+"cliente/cliente_autocomplete.php",
            {term: request.term}, //1 solo clientes de la empresa
            response
            );

			
        },
        select: function (event, ui) {
			var tipo_credito = $('#cmb_credito_id option:selected').val();
            $('#txt_cliente_id').val(ui.item.cliente_nom);
            $("#hdd_cliente_id").val(ui.item.cliente_id);
			credito_cliente_select(ui.item.cliente_id,0);
            event.preventDefault();
        }
    })
    .keyup(function () {
        console.log(VISTA_URL+"cliente/cliente_autocomplete.php");
        if ($('#txt_cliente_id').val() === '') {
            $('#hdd_cliente_id').val('');
			$("#txt_placa_nom").val('');
			$("#cmb_marca").val(0);
			$("#txt_clase_nom").val(''); //tamaño de vehículo
			$("#cmb_modelo").val(0);
			$("#txt_color_nom").val('');
			$("#cmb_tamanoveh_id").val(0);

        }
    });
	
	//AUTOCOMPLETE USUARIO
	$("#txt_usuario_id").autocomplete({
        minLength: 1,
        source: function(request, response){
			$.getJSON(
			  VISTA_URL+"usuario/usuario_autocomplete.php",
			  {term: request.term}, //1 solo clientes de la empresa
			  response
			);
		  },
        select: function (event, ui) {
            $('#txt_usuario_id').val(ui.item.tb_usuario_nom + " " + ui.item.tb_usuario_ape);
            $("#hdd_usuario_id").val(ui.item.tb_usuario_id);
            event.preventDefault();
        }
    })
    .keyup(function () {
		console.log(VISTA_URL+"usuario/usuario_autocomplete.php");
        if ($('#txt_usuario_id').val() === '') {
            $('#hdd_usuario_id').val('');
        }
    });



  	$('#form_gestioncochera').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"gestioncochera/gestioncochera_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_gestioncochera").serialize(),
				beforeSend: function() {
					$('#gestioncochera_mensaje').show(400);
					$('#btn_guardar_gestioncochera').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#gestioncochera_mensaje').removeClass('callout-info').addClass('callout-success')
						$('#gestioncochera_mensaje').html(data.mensaje);
						setTimeout(function(){ 
							//recargar tablas
							gestioncochera_tabla();
							cocheramovimiento_tabla();
							
							$('#modal_registro_gestioncochera').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#gestioncochera_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#gestioncochera_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_gestioncochera').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#gestioncochera_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	        $('#gestioncochera_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  	},
	  	rules: {
            
			cmb_cochera_id_form:{
				required: true,
				minlength: 1
			},
			txt_cliente_id: {
				required: true,
				minlength: 1
			},
			txt_marca_nom: {
				required: true,
				minlength: 1
			},
			txt_clase_nom: {
				required: true,
				minlength: 1
			},
			txt_modelo_nom: {
				required: true,
				minlength: 1
			},
			txt_color_nom: {
				required: true,
				minlength: 1
			},
			txt_placa_nom: {
				required: true,
				minlength: 1
			},
			txt_usuario_id: {
				required: true,
				minlength: 1
			},
			txt_gestioncochera_hora: {
				required: true,
				minlength: 1
			},
			txt_gestioncochera_fecha: {
				required: true,
				minlength: 1
			},
			cmb_estado_id: {
				required: true,
				minlength: 1
			},
			txt_gestioncochera_obs: {
				required: true,
				minlength: 5
			},
			cmb_tamanoveh_id: {
				required: true,
				minlength: 1
			},
		},
		messages: {
            
			cmb_cochera_id_form: {
				required: "Seleccione una Cochera",
				min: "Seleccione una Cochera"
			},
			txt_cliente_id: {
				required: "Ingrese el usuario propietario del vehiculo",
				min: "Ingrese el usuario propietario del vehiculo"
			},
			txt_marca_nom: {
				required: "Seleccione la marca del vehiculo ingresado",
				min: "Seleccione la marca del vehiculo ingresado"
			},
			txt_clase_nom: {
				required: "Seleccione la clase del vehiculo ingresado",
				min: "Seleccione la clase del vehiculo ingresado"
			},
			txt_modelo_nom: {
				required: "Seleccione el modelo del vehiculo ingresado",
				min: "Seleccione el modelo del vehiculo ingresado"
			},
			txt_color_nom: {
				required: "Seleccione el color del vehiculo ingresado",
				min: "Seleccione el color del vehiculo ingresado"
			},
			txt_placa_nom: {
				required: "Seleccione la placa del vehiculo ingresado",
				min: "Seleccione la placa del vehiculo ingresado"
			},
			txt_usuario_id: {
				required: "Ingrese el usuario encargado del ingreso",
				min: "Ingrese el usuario encargado del ingreso"
			},
			txt_gestioncochera_hora: {
				required: "Seleccione la hora de ingreso",
				min: "Seleccione la hora de ingreso"
			},
			txt_gestioncochera_fecha: {
				required: "Seleccione la fecha de ingreso",
				min: "Seleccione la fecha de ingreso"
			},
			cmb_estado_id: {
				required: "Seleccione un estado",
				min: "Seleccione un estado"
			},
			txt_gestioncochera_obs: {
				required: "Ingresar una observación realizada al momento del ingreso",
				min: "Ingresar una observación realizada al momento del ingreso"
			},
			cmb_tamanoveh_id: {
				required: "Seleccionar tamaño del vehiculo",
				min: "Seleccionar tamaño del vehiculo"
			},
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});

	
});
