function registrar_movimiento(){
  var cocheramovimiento_hora = $('#txt_cocheramovimiento_hora').val().trim();
  var cocheramovimiento_fecha = $('#txt_cocheramovimiento_fecha').val().trim();
  var cocheramovimiento_obs = $('#txt_cocheramovimiento_obs').val().trim();
  var movimiento_id = $('#cmb_movimiento_id option:selected').val();
  var usuario_id = $('#hdd_usuario_id').val();
  var cocheramovimiento_id = $('#hdd_cocheramovimiento_id').val();
  var gestioncochera_id = $('#hdd_gestioncochera_id').val();
  //Variables para ingresar los datos dependiendo al valor del select finalidad
  var destino_cochera = $('#cmb_cochera_id').val();
  var destino_taller = $('#txt_taller_nom').val().trim();
  var destino_cliente = $('#hdd_cliente_id').val();
  var destino_venta = $('#txt_venta_nom').val().trim();
  var destino_otro = $('#txt_otro_nom').val().trim();
  //Asignar el valor a la varibale que sera guarda en la columna destino segun el tipo de finalidad correspondiente
  var finalidad_id = $('#cmb_finalidad_id option:selected').val();
  var destino_var = '';
  switch (parseInt(finalidad_id)) {
    case 1:
      destino_var = destino_cochera;
      break;
    case 2:
      destino_var = destino_taller;
      break;
    case 3:
      destino_var = destino_cliente;
      break;
    case 4:
      destino_var = destino_venta;
      break;
    case 5:
      destino_var = destino_otro;
      break;
    default:
      break;
  }

  console.log('El destino es:', destino_var)

  
  //Para validar si todos los campos estan llenos
  if(!cocheramovimiento_hora || !cocheramovimiento_fecha || !cocheramovimiento_obs 
      || !movimiento_id || !finalidad_id || !usuario_id ){
    alerta_warning('AVISO', 'Debes llenar todos los campos');
    return false;
  }
  console.log(cocheramovimiento_hora,cocheramovimiento_fecha);
  console.log(cocheramovimiento_id);
  $.ajax({
      type: "POST",
      url: VISTA_URL+"gestioncochera/cocheramovimiento_controller.php",
      async: true,
      dataType: "json",
      data: ({
        action: 'insertar',
        cocheramovimiento_hora: cocheramovimiento_hora,
        cocheramovimiento_fecha: cocheramovimiento_fecha,
        cocheramovimiento_obs: cocheramovimiento_obs,
        movimiento_id: movimiento_id,
        finalidad_id: finalidad_id,
        usuario_id: usuario_id,
        cocheramovimiento_id: cocheramovimiento_id,
        gestioncochera_id: gestioncochera_id,
        destino_var: destino_var,
      }),
      beforeSend: function() {
      
      },
      success: function(data){

          if(parseInt(data.estado) == 1){
            console.log(data.mensaje2);
              notificacion_success(data.mensaje)
              listar_cocheramovimiento();
              $('#txt_cocheramovimiento_hora').val('');
              $('#txt_cocheramovimiento_fecha').val('');
              $('#txt_cocheramovimiento_obs').val('');
              $('#cmb_movimiento_id').val('');
              $('#cmb_finalidad_id').val('');
              $('#txt_usuario_id').val('');
              $('#hdd_usuario_id').val('');
              $('#txt_finalidad_id').val('');
              $('#txt_cliente_id').val('');
              $('#hdd_cliente_id').val('');
              $('#cmb_cochera_id').val('');
              $('#txt_taller_nom').val('');
              $('#txt_venta_nom').val('');
              $('#txt_otro_nom').val('');
              $('#hdd_cocheramovimiento_id').val('');
              $('#hdd_gestioncochera_id').val('');
              $('#modal_registro_cocheramovimiento').modal('hide');
              cocheramovimiento_tabla();
          }
      else{
        console.log(data)
      }
      },
      complete: function(data){
            
      },
      error: function(data){
          alerta_error('ERROR', data.responseText)
          console.log(data);
      }
    });
}

function listar_cocheramovimiento(){
  var gestioncochera_id = $('#hdd_gestioncochera_id').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL+"gestioncochera/cocheramovimiento_controller.php",
    async: true,
    dataType: "html",
    data: ({
      action: 'tabla',
      gestioncochera_id: gestioncochera_id,
    }),
    beforeSend: function() {
    },
    success: function(data){
      $('#table_cocheramovimiento').html(data)
    },
    complete: function(data){
    },
    error: function(data){
      alerta_error('ERROR', data.responseText)
        console.log(data);
    }
  });
}

$(document).ready(function(){

  $('.finalidad-div').addClass('hide');

  //AUTOCOMPLETE CLIENTE
  $("#txt_cliente_id").autocomplete({
    minLength: 1,
    source: function(request, response){
      $.getJSON(
        VISTA_URL+"cliente/cliente_autocomplete.php",
        {term: request.term}, //1 solo clientes de la empresa
        response
      );
    },
    select: function (event, ui) {
      $('#txt_cliente_id').val(ui.item.cliente_nom);
      $("#hdd_cliente_id").val(ui.item.cliente_id);
      event.preventDefault();
    }
  })
  .keyup(function () {
    console.log(VISTA_URL+"cliente/cliente_autocomplete.php");
    if ($('#txt_cliente_id').val() === '') {
      $('#hdd_cliente_id').val('');
    }
  });

  //AUTOCOMPLETE USUARIO
  $("#txt_usuario_id").autocomplete({
    minLength: 1,
    source: function(request, response){
      $.getJSON(
        VISTA_URL+"usuario/usuario_autocomplete.php",
        {term: request.term}, //1 solo clientes de la empresa
        response
      );
    },
    select: function (event, ui) {
      $('#txt_usuario_id').val(ui.item.tb_usuario_nom + " " + ui.item.tb_usuario_ape);
      $("#hdd_usuario_id").val(ui.item.tb_usuario_id);
        event.preventDefault();
    }
  })

  .keyup(function () {
    console.log(VISTA_URL+"usuario/usuario_autocomplete.php");
    if ($('#txt_usuario_id').val() === '') {
      $('#hdd_usuario_id').val('');
    }
  });

  $('#datetimepicker3').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    startDate: "-0d"
    //endDate : new Date()
  });

  $('#cmb_finalidad_id').change(function () {

    $('.finalidad-div').addClass('hide');
    $('#movimiento').removeClass('col-md-4').addClass('col-md-6');
    $('#finalidad').removeClass('col-md-4').addClass('col-md-6');

    var finalidad_id = $('#cmb_finalidad_id option:selected').val();
    switch (parseInt(finalidad_id)) {
      case 1:
          $('#finalidad-cochera').removeClass('hide');
          $('#movimiento').removeClass('col-md-6').addClass('col-md-4');
          $('#finalidad').removeClass('col-md-6').addClass('col-md-4');
          break;
      case 2:
          $('#finalidad-taller').removeClass('hide');
          $('#movimiento').removeClass('col-md-6').addClass('col-md-4');
          $('#finalidad').removeClass('col-md-6').addClass('col-md-4');
          break;
      case 3:
          $('#finalidad-entrega').removeClass('hide');
          $('#movimiento').removeClass('col-md-6').addClass('col-md-4');
          $('#finalidad').removeClass('col-md-6').addClass('col-md-4');
          break;
      case 4:
          $('#finalidad-venta').removeClass('hide');
          $('#movimiento').removeClass('col-md-6').addClass('col-md-4');
          $('#finalidad').removeClass('col-md-6').addClass('col-md-4');
          break;
      case 5:
          $('#finalidad-otro').removeClass('hide');
          $('#movimiento').removeClass('col-md-6').addClass('col-md-4');
          $('#finalidad').removeClass('col-md-6').addClass('col-md-4');
          break;
      default:
          break;
    }
  });
  
  listar_cocheramovimiento();

  $('#cmb_opcionesgc_id').change(function(event){
    var opcionesgc_id = parseInt($(this).val());
    var opcionesgc_nom = $('#cmb_opcionesgc_id :selected').text();
    $('#hdd_opcionesgc_nom').val(opcionesgc_nom);

    if(opcionesgc_id == 1 || opcionesgc_id == 4)
      $('.fecha_pdp').show();
    else
      $('.fecha_pdp').hide();
  });

  directorio = $('#directorio').val(); //opcionesgc, asignaropciongc
  if(directorio == "opcionesgc")
    listar_opcionesgc();

  console.log('corre ya 444: ' + directorio)
});
