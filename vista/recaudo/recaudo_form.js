/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    var empresa_id = $("#cmb_fil_empresa_id").val();
    var action=$("#action_recaudo").val();
    
    if(action=="insertar"){
        cmb_usu_id(empresa_id);
    }
    if(action=="editar"){
//        cmb_usu_id(empresa_id,usuario_id);
    }

    $('.moneda2').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });
    
    //filtro de primera fecha
    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
//        var startVal = $('#txt_recaudo_fec1').val();
//        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
        deuda_tabla();
        recaudo_resumen_tabla();
    });

    $("#datetimepicker2").on("change", function (e) {
//        var endVal = $('#txt_recaudo_fec2').val();
//        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
        deuda_tabla();
        recaudo_resumen_tabla();
    });


    $("#for_recaudo").validate({
        submitHandler: function () {
            var meta1 = $('#meta1').val();
            if (!meta1) {
                alert('Ingrese todos los campos vacíos.');
                return false;
            }

            $.ajax({
                type: "POST",
                url: VISTA_URL+"recaudo/recaudo_reg.php",
                async: true,
                dataType: "json",
                data: $("#for_recaudo").serialize(),
                beforeSend: function () {


                },
                success: function (data) {
                    if (parseInt(data.recaudo_id) > 0) {
                        swal_success("SISTEMA",data.recaudo_msj,2500);
                        $('#modal_registro_recaudo').modal('hide');
                        recaudo_tabla();
                    } else
                        swal_warning("AVISO",data.recaudo_msj,2500);
                },
                complete: function (data) {
                    if (data.statusText != 'success') {
                        $('#msj_recaudo').html(data.responseText);
                        console.log(data.responseText)
                    }

                }
            });
        },
        rules: {
            cmb_usu_id: {
                required: true
            },
            txt_recaudo_fec1: {
                required: true
            },
            txt_recaudo_fec2: {
                required: true
            },
            cmb_mon_id: {
                required: true
            },
            txt_recaudo_meta: {
                required: true
            },
            txt_recaudo_pag: {
                required: true
            }
        },
        messages: {
            cmb_usu_id: {
                required: 'Es obligatorio por favor'
            },
            txt_recaudo_fec1: {
                required: 'Es obligatorio por favor'
            },
            txt_recaudo_fec2: {
                required: 'Es obligatorio por favor'
            },
            cmb_mon_id: {
                required: 'Es obligatorio por favor'
            },
            txt_recaudo_meta: {
                required: 'Es obligatorio por favor'
            },
            txt_recaudo_pag: {
                required: 'Es obligatorio por favor'
            }
        }
    });

});

$("#cmb_fil_empresa_id").change(function (){
    var usuario_id = $("#cmb_fil_empresa_id").val();
    cmb_usu_id(usuario_id);
});


function cmb_usu_id(empresa_id) {
    var mostrar = 0;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/cmb_usu_id_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
            empresa_id: empresa_id,
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
        },
        complete: function (html) {

        }
    });
}

function recaudo_pagar(recaudo_id, pago)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/recaudo_reg.php",
        async: false,
        dataType: "JSON",
        data: ({
            action: 'pagar',
            recaudo_id: recaudo_id,
            pago: pago,
            ingreso_total: $('#hdd_ingreso_total').val()
        }),
        beforeSend: function () {
            $('#msj_recaudo').show(300)
            $('#msj_recaudo').html('<option value="">Cargando...</option>');
        },
        success: function (data) {
            if (parseInt(data.recaudo_id)) {
                $('#msj_recaudo').html(data.recaudo_msj);
                $("#div_recaudo_form").dialog("close");
            }
        }
    });
}


$('#btn_agregar').click(function (event) {
    event.preventDefault();
    var tr = $('<tr class="tr_new"></tr>');
    tr.append('<td><input type="text" id="meta1" name="meta1[]" class="form-control input-sm moneda"></td>');
    tr.append('<td><input type="text" id="meta2" name="meta2[]" class="form-control input-sm moneda"></td>');
    tr.append('<td><input type="text" id="pagar" name="pagar[]" class="form-control input-sm moneda"></td>');
    tr.append('<td><a href="javascript:void(0)" class="btn btn-primary btn-xs btn_del"><i class="fa fa-trash"></i></a></td>');

    $('#tbl_detalle').append(tr);
    //moneda y porcentaje
    tr.find('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0',
        vMax: '9999999.99'
    });
    modal_height_auto('modal_registro_recaudo');
    ///botonesssss
    $('.btn_del').button({
        icons: {primary: "ui-icon-trash"},
        text: false
    });
    $('.btn_del').click(function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });
});