/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    console.log("hasta aki estoy entrando");
    var empresa_id = $("#cmb_fil_empresa_id").val();
    var action = $("#action_mejoragc").val();

    if (action == "insertar") {
        cmb_usu_id(empresa_id);
    }
    if (action == "editar") {
//        cmb_usu_id(empresa_id,usuario_id);
    }

    $('.moneda2').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.00'
    });


    //filtro de primera fecha
    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        deuda_tabla();
        recaudo_resumen_tabla();
    });

    $("#datetimepicker2").on("change", function (e) {
        deuda_tabla();
        recaudo_resumen_tabla();
    });




    $("#for_mejoragc").validate({
        submitHandler: function () {
            var mejoragc_porc = $('#txt_mejoragc_porc').val();
            var action = $('#action_mejoragc').val();

            if (!mejoragc_porc && action == 'insertar') {
                alert('Tiene que insertar al menos un rango');
                return false;
            }
            $.ajax({
                type: "POST",
                url: VISTA_URL + "recaudo/mejoragc_reg.php",
                async: true,
                dataType: "JSON",
                data: $("#for_mejoragc").serialize(),
                beforeSend: function () {
                },
                success: function (data) {
                    if (parseInt(data.mejoragc_id) > 0) {

                        swal_success("SISTEMA", data.mejoragc_msj, 2500);
                        $('#modal_registro_mejoragc').modal('hide');
                        mejoragc_tabla();
                    } else
                        swal_warning("SISTEMA", data.mejoragc_msj, 2500);
                },
                complete: function (data) {
                    if (data.statusText != "success") {
                        $('#msj_mejoragc').html(data.responseText);
                        console.log(data)
                    }

                }
            });
        },
        rules: {
            cmb_usu_id: {
                required: true
            },
            txt_mejoragc_fec1: {
                required: true
            },
            txt_mejoragc_fec2: {
                required: true
            },
            txt_mejoragc_inisol: {
                required: true
            },
            txt_mejoragc_inidol: {
                required: true
            },
            txt_mejoragc_porc: {
                required: true
            }
        },
        messages: {
            cmb_usu_id: {
                required: 'Es obligatorio por favor'
            },
            txt_mejoragc_fec1: {
                required: 'Es obligatorio por favor'
            },
            txt_mejoragc_fec2: {
                required: 'Es obligatorio por favor'
            },
            txt_mejoragc_inisol: {
                required: 'Es obligatorio por favor'
            },
            txt_mejoragc_inidol: {
                required: 'Es obligatorio por favor'
            },
            txt_mejoragc_porc: {
                required: 'Es obligatorio por favor'
            }
        }
    });








});

function cmb_usu_id(empresa_id) {
    var mostrar = 0;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/cmb_usu_id_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
            empresa_id: empresa_id
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
        },
        complete: function (html) {

        }
    });
}

$("#cmb_fil_empresa_id").change(function () {
    var empresa_id = $("#cmb_fil_empresa_id").val();
    cmb_usu_id(empresa_id);
});

function mejoragc_pagar(mejoragc_id, pagar_sol, pagar_dol, porcentaje) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/mejoragc_reg.php",
        async: false,
        dataType: "JSON",
        data: ({
            action: 'pagar',
            mejoragc_id: mejoragc_id,
            mejoragc_logsol: $('#txt_mejoragc_logsol').val(),
            mejoragc_logdol: $('#txt_mejoragc_logdol').val(),
            pagar_sol: pagar_sol,
            pagar_dol: pagar_dol,
            porcentaje: porcentaje
        }),
        beforeSend: function () {

        },
        success: function (data) {
            if (parseInt(data.mejoragc_id)) {
                swal_success("SISTEMA", data.mejoragc_msj, 2500);
                $('#modal_registro_mejoragc').modal('hide');
                mejoragc_tabla();
            }
        }
    });
}

$('#porcentaje').change(function (event) {
    var porc = $(this).autoNumeric('get');
    var inisol = $('#txt_mejoragc_inisol').autoNumeric('get');
    var inidol = $('#txt_mejoragc_inidol').autoNumeric('get');




    var meta_soles = (inisol * porc) / 100;
    meta_soles = inisol - meta_soles;

    var meta_dolares = (inidol * porc) / 100;
    meta_dolares = inidol - meta_dolares;

    meta_soles = parseFloat(meta_soles).toFixed(2);
    meta_dolares = parseFloat(meta_dolares).toFixed(2);

    $('#meta_sol').autoNumeric('set', meta_soles);
    $('#meta_dol').autoNumeric('set', meta_dolares);
});

$('#txt_mejoragc_logsol, #txt_mejoragc_logdol').change(function (event) {
    var logsol = Number($('#txt_mejoragc_logsol').autoNumeric('get'));
    var logdol = Number($('#txt_mejoragc_logdol').autoNumeric('get'));

    var mejoragc_id = $('#hdd_mejoragc_id').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/mejoragc_reg.php",
        async: true,
        dataType: "JSON",
        data: ({
            action: 'logro',
            mejoragc_logsol: logsol,
            mejoragc_logdol: logdol,
            mejoragc_id: mejoragc_id
        }),
        beforeSend: function () {
        },
        success: function (data) {
            if (parseInt(data.estado) > 0) {
                $('#str_mensaje').html(data.mejoragc_msj);
                swal_success("SISTEMA",data.mejoragc_msj,3000);
                $('#btn_gc').attr('onClick', 'mejoragc_pagar(' + mejoragc_id + ', ' + data.pagar_sol + ', ' + data.pagar_dol + ', ' + data.porcentaje + ')');
            } else {
                $('#str_mensaje').html(data.mejoragc_msj);
                swal_warning("AVISO",data.mejoragc_msj,3000);
            }
        },
        complete: function (data) {
            if (data.statusText != "success") {

            }

        }
    });
});



var action = $('#action_mejoragc').val()
//if (action == 'editar')
//    $('#txt_mejoragc_logsol').change();

$('#btn_agregar').click(function (event) {
    event.preventDefault();
    var porcentaje = $('#porcentaje').val();
    var meta_sol = $('#meta_sol').val();
    var meta_dol = $('#meta_dol').val();
    var pagar_sol = $('#pagar_sol').val();
    var pagar_dol = $('#pagar_dol').val();

    if (!porcentaje || !meta_sol || !meta_dol || !pagar_dol) {
        alert('Ingresa todos los campos vacíos');
        return false;
    }

    var tr = $('<tr></tr>');
    tr.append('<td><input type="text" id="txt_mejoragc_porc" name="txt_mejoragc_porc[]" class="form-control input-sm moneda" size="10" value="' + porcentaje + '"></td>');
    tr.append('<td><input type="text" name="txt_mejoragc_metsol[]" class="form-control input-sm moneda" size="10" value="' + meta_sol + '"></td>');
    tr.append('<td><input type="text" name="txt_mejoragc_pagsol[]" class="form-control input-sm moneda" size="10" value="' + pagar_sol + '"></td>');
    tr.append('<td><input type="text" name="txt_mejoragc_metdol[]" class="form-control input-sm moneda" size="10" value="' + meta_dol + '"></td>');
    tr.append('<td><input type="text" name="txt_mejoragc_pagdol[]" class="form-control input-sm moneda" size="10" value="' + pagar_dol + '"></td>');
    tr.append('<td><a class="btn btn-danger btn-xs btn_del" href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>');

    $('#tbl_detalle').append(tr);

    $('#porcentaje').val('0.00');
    $('#meta_sol').val('0.00');
    $('#meta_dol').val('0.00');
    $('#pagar_sol').val('0.00');
    $('#pagar_dol').val('0.00');

    //moneda y porcentaje
    tr.find('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0',
        vMax: '9999999.99'
    });

    ///botonesssss
    $('.btn_del').button({
        icons: {primary: "ui-icon-trash"},
        text: false
    });
    $('.btn_del').click(function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });
});
