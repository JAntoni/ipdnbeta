<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}
class Recaudodetalle extends Conexion{
    public $tb_recaudodetalle_id;
    public $tb_recaudodetalle_reg;
    public $tipo;
    public $meta1;
    public $meta2;
    public $pagar_sol;
    public $pagar_dol;
    public $porcentaje;
    public $modulo_id;
    
    
    function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_recaudodetalle(
                                            tipo, 
                                            meta1, 
                                            meta2, 
                                            pagar_sol, 
                                            pagar_dol, 
                                            porcentaje, 
                                            modulo_id)
                                    VALUES (
                                            :tipo, 
                                            :meta1, 
                                            :meta2, 
                                            :pagar_sol, 
                                            :pagar_dol, 
                                            :porcentaje, 
                                            :modulo_id)";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tipo", $this->tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":meta1", $this->meta1, PDO::PARAM_STR);
        $sentencia->bindParam(":meta2", $this->meta2, PDO::PARAM_STR);
        $sentencia->bindParam(":pagar_sol", $this->pagar_sol, PDO::PARAM_INT);
        $sentencia->bindParam(":pagar_dol", $this->pagar_dol, PDO::PARAM_STR);
        $sentencia->bindParam(":porcentaje", $this->porcentaje, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_recaudodetalle_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_recaudodetalle_id'] = $tb_recaudodetalle_id;
        return $data; //si es correcto el recaudo retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function  mostrarTodos($modulo_id, $tipo){
      try {
         $sql = "SELECT * FROM tb_recaudodetalle WHERE modulo_id =:modulo_id AND tipo =:tipo";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  mostrar_rango_logrado($ingreso, $tipo, $modulo_id){
      try {
         $sql = "SELECT * FROM tb_recaudodetalle WHERE $ingreso BETWEEN meta1 and meta2 and tipo =:tipo AND modulo_id =:modulo_id LIMIT 1;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  mostrar_logrado_mejoragc($logrado_sol, $logrado_dol, $modulo_id){
      try {
         $sql = "(SELECT 1 AS tipo_meta, det1.* FROM tb_recaudodetalle det1 WHERE :logrado_sol <= meta1 and tipo = 2 AND modulo_id =:modulo_id order by meta1, meta2 LIMIT 1) 
		UNION 
		(SELECT 2 AS tipo_meta, det2.* FROM tb_recaudodetalle det2 WHERE :logrado_dol <= meta2 and tipo = 2 AND modulo_id =:modulo_id order by meta1, meta2 LIMIT 1);";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":logrado_sol", $logrado_sol, PDO::PARAM_STR);
        $sentencia->bindParam(":logrado_dol", $logrado_dol, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();
        
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
}
