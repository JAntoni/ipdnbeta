<?php
$fec1 = date('01-m-Y');
$fec2 = date('t-m-Y');
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_creditomenor_filtro" class="form-inline" role="form">
                    <!--<button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</button>-->
                    <a class="btn btn-primary btn-sm" href="javascript:void(0)" onClick="mejoragc_form('insertar',0)"><i class="fa fa-plus"></i> Agregar</a>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker111'>
                            <input type="text" class="form-control input-sm" name="txt_filgc_fec1" id="txt_filgc_fec1" value="<?php echo $fec1?>" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker222'>
                            <input type="text" class="form-control input-sm" name="txt_filgc_fec2" id="txt_filgc_fec2" value="<?php echo $fec2?>" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>