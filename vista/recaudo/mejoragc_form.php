<?php
require_once('../../core/usuario_sesion.php');
require_once ("Mejoragc.class.php");
$oMejoragc = new Mejoragc();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("Recaudodetalle.class.php");
$oDetalle = new Recaudodetalle();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

if ($_POST['action'] == "editar") {
    $dts = $oMejoragc->mostrarUno($_POST['mejoragc_id']);
    if ($dts['estado'] == 1) {
        $usuario_id = $dts['data']['tb_usuario_id'];
        $mejoragc_fec1 = mostrar_fecha($dts['data']['tb_mejoragc_fec1']);
        $mejoragc_fec2 = mostrar_fecha($dts['data']['tb_mejoragc_fec2']);
        $mejoragc_inisol = mostrar_moneda($dts['data']['tb_mejoragc_inisol']);
        $mejoragc_inidol = mostrar_moneda($dts['data']['tb_mejoragc_inidol']);
        $mejoragc_metsol = mostrar_moneda($dts['data']['tb_mejoragc_metsol']);
        $mejoragc_metdol = mostrar_moneda($dts['data']['tb_mejoragc_metdol']);
        $mejoragc_porc = mostrar_moneda($dts['data']['tb_mejoragc_porc']);
        $mejoragc_pag = mostrar_moneda($dts['data']['tb_mejoragc_pag']);
        $mejoragc_logsol = mostrar_moneda($dts['data']['tb_mejoragc_logsol']);
        $mejoragc_logdol = mostrar_moneda($dts['data']['tb_mejoragc_logdol']);
        $mejoragc_est = $dts['data']['tb_mejoragc_est'];
        $empresa_id = $dts['data']['tb_empresa_id'];
    }
    $dts = NULL;
    if ($usuario_id > 0) {
        $dts = $oUsuario->mostrarUno($usuario_id);
        if ($dts['estado'] == 1) {
            $usuario_nombre = $dts['data']['tb_usuario_nom'];
            $usuario_apellido = $dts['data']['tb_usuario_ape'];
        }
        $dts = NULL;
    }

    if ($moneda_id == 2)
        $moneda = 'US$';
}
?>




<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_mejoragc" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">INFORMACIÓN DE MEJORA GC</h4>
            </div>
            <form id="for_mejoragc">
                <input name="action_mejoragc" id="action_mejoragc" type="hidden" value="<?php echo $_POST['action'] ?>">
                <input name="hdd_mejoragc_id" id="hdd_mejoragc_id" type="hidden" value="<?php echo $_POST['mejoragc_id'] ?>">
                <input type="hidden" id="hdd_usuariogrupo_id" value="<?php echo $usuariogrupo_id ?>">
                <input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id ?>">

                <div class="modal-body">
                    <!--<h5 style="font-family: cambria;font-weight: bold;color: #000000">SEGUROS Y UIT</h5>-->
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Sede:</label>
                                    <select class="form-control input-sm" id="cmb_fil_empresa_id" name="cmb_fil_empresa_id">
                                        <?php require_once '../empresa/empresa_select.php'; ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Usuario:</label>
                                    <select class="form-control input-sm" name="cmb_usu_id" id="cmb_usu_id">
                                        <?php
                                        if ($_POST['action'] == "editar") {
                                            echo '<option value="' . $usuario_id . '">' . $usuario_apellido . ' ' . $usuario_nombre . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha 1:</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type="text" name="txt_mejoragc_fec1" id="txt_mejoragc_fec1" class="form-control input-sm fec_pagos" value="<?php echo $mejoragc_fec1 ?>" readonly="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha 2:</label>
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input type="text" name="txt_mejoragc_fec2"  id="txt_mejoragc_fec2" class="form-control input-sm fec_pagos" value="<?php echo $mejoragc_fec2 ?>" readonly="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>  
                            <!--<p>-->
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Inicio Soles:</label>
                                    <input type="text" name="txt_mejoragc_inisol" id="txt_mejoragc_inisol" value="<?php echo ($mejoragc_inisol) ?>" class="form-control input-sm moneda2">
                                </div>
                                <div class="col-md-6">
                                    <label>Inicio Dólares:</label>
                                    <input type="text" name="txt_mejoragc_inidol" id="txt_mejoragc_inidol" value="<?php echo ($mejoragc_inidol) ?>" class="form-control input-sm moneda2">
                                </div>

                            </div> 
                            <p>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item" style="border-color: #000000">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Porcentaje:</label>
                                            <input type="text" id="porcentaje" value="<?php echo mostrar_moneda($mejoragc_porc) ?>" class="form-control input-sm moneda2">
                                        </div>
                                    </div> 
                                    <p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Pagar Soles:</label>
                                            <input type="text" id="pagar_sol" value="<?php echo mostrar_moneda($mejoragc_pag) ?>" class="form-control input-sm moneda2">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Pagar Dólares:</label>
                                            <input type="text" id="pagar_dol" value="<?php echo mostrar_moneda($mejoragc_pagdol) ?>" class="form-control input-sm moneda2">
                                        </div>
                                    </div> 
                                    <p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Meta Soles:</label>
                                            <input type="text" id="meta_sol" value="<?php echo mostrar_moneda($mejoragc_metsol) ?>" class="form-control input-sm moneda2">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Meta Dólares:</label>
                                            <input type="text" id="meta_dol" value="<?php echo mostrar_moneda($mejoragc_metdol) ?>" class="form-control input-sm moneda2">
                                        </div>
                                        <div class="col-md-2">
                                            <label>&nbsp;</label><br>
                                            <a href="javascript:void(0)" class="btn btn-facebook btn-sm" id="btn_agregar" role="button">
                                                Insert
                                            </a>
                                        </div>
                                    </div> 
                                </li>
                            </ul>

                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tbl_detalle" width="100%" class=" table-hover">
                                        <tr id="tabla_cabecera">
                                            <th id="tabla_cabecera_fila">PORCENTAJE</th>
                                            <th id="tabla_cabecera_fila">META S/.</th>
                                            <th id="tabla_cabecera_fila"style="background: rgb(2,184,117)">PAGAR S/.</th>
                                            <th id="tabla_cabecera_fila">META USD</th>
                                            <th id="tabla_cabecera_fila" style="background: rgb(2,184,117)">PAGAR USD</th>
                                            <th id="tabla_cabecera_fila">Opc</th>
                                        </tr>
                                        <?php
                                        if ($_POST['action'] == "editar") {
                                            $dts = $oDetalle->mostrarTodos($_POST['mejoragc_id'], 2);
                                            if ($dts['estado'] == 1) {
                                                foreach ($dts['data']as $key => $dt) {
                                                    echo '
                                                        <tr id="tabla_cabecera_fila tr_' . $dt['tb_recaudodetalle_id'] . '">
                                                            <td id="tabla_fila"><b>' . mostrar_moneda($dt['porcentaje']) . '%</b></td>
                                                            <td id="tabla_fila"><b>S/. ' . mostrar_moneda($dt['meta1']) . '</b></td>
                                                            <td id="tabla_fila" style="background: rgb(2,184,117);"><b>S/. ' . mostrar_moneda($dt['pagar_sol']) . '</b></td>
                                                            <td id="tabla_fila"><b>$ ' . mostrar_moneda($dt['meta2']) . '</b></td>
                                                            <td id="tabla_fila" style="background: rgb(2,184,117)"><b>S/. ' . mostrar_moneda($dt['pagar_dol']) . '</b></td>
                                                            <td id="tabla_fila">-</td>
                                                        </tr>';
                                                }
                                            }
                                        }
                                        ?>
                                    </table>

                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($_POST['action'] == "editar"): ?>
                                        <fieldset>
                                            <legend>Logrado en la Fecha</legend>
                                            <table width="100%" class="table">
                                                <tr id="tabla_cabecera_fila">
                                                    <td id="tabla_fila">
                                                        <label>Logrado Soles:</label>
                                                        <input type="text" name="txt_mejoragc_logsol" id="txt_mejoragc_logsol" value="<?php echo ($mejoragc_logsol) ?>" class="form-control input-sm moneda2">
                                                    </td>
                                                    <td id="tabla_fila">
                                                        <label>Logrado Dólares:</label>
                                                        <input type="text" name="txt_mejoragc_logdol" id="txt_mejoragc_logdol" value="<?php echo ($mejoragc_logdol) ?>" class="form-control input-sm moneda2">
                                                    </td>
                                                    <td id="tabla_fila">
                                                    </td>
                                                    <td  id="tabla_fila" align="right">
                                                        <?php if ($usuariogrupo_id == 2 && $mejoragc_est == 0): ?>
                                                            <br>
                                                            <a id="btn_gc" class="btn btn-success btn-xs" href="javascript:void(0)" onClick="mejoragc_pagar('<?php echo $_POST['mejoragc_id'] ?>')" title="Pagar"><i class="fa fa-money"></i></a>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>

                                            <strong style="color: blue;" id="str_mensaje"></strong>

                                            <?php if ($mejoragc_est == 1): ?>
                                                <h3 style="color: red;">Esta Meta ya está pagada</h3>
                                            <?php endif; ?>
                                        </fieldset>
                                    <?php endif; ?>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($_POST['action'] == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="seguro_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_seguro">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/recaudo/mejoragc_form.js?ver=0003665645560024564'; ?>"></script>
