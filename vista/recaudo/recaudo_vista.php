<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $menu_tit; ?>
      <small><?php echo $menu_des; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
      <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header">
        <!--<button class="btn btn-primary btn-sm" onclick="recaudo_form('I', 0)"><i class="fa fa-plus"></i> Nueva Area</button>-->
        <?php include 'recaudo_filtro.php'; ?>
      </div>
      <div class="box-body">
        <div class="callout callout-info" id="recaudo_mensaje_tbl" style="display: none;">
        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
      </div>

      <div class="row">
        <div class="col-md-12">
          <center><label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #000000;font-size: 16px">RESUMEN DE GESTIÓN DE COBRANZA</label></center>
          <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">COMO INICIA LA DEUDA</label>
          
          <div class="panel panel-primary">
            <div class="panel-body">
              <div id="div_deuda_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
              </div>
              <div class="row" id="div_recaudo_resumen_tabla">
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="row mt-4">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border text-center">
              <h3 class="box-title text-bold" id="h3_titulo_revers_seg">Titulo</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="" id="load_tabla_reverssegment" style="margin: auto; text-align: center; display:none;">
                <h4>
                  <p>Cargando datos ...</p>
                  <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>
                </h4>
              </div>
              <div id="box_tabla_reverssegment"></div>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border" style="text-align: center;">
              <h3 class="box-title" id="h3_titulo_vigentes" style="font-weight: bold;">FACTURADO SEGMENTADO POR CUOTAS DESDE EL ... HASTA ...</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="" id="load_tabla_rsegmentado" style="margin: auto; text-align: center; display:none;">
                <h4>
                  <p>Cargando datos de Cobro...</p>
                  <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>
                </h4>
              </div>
              <div id="box_tabla_vigentes"></div>
            </div>
          </div>
        </div>
        
        <div class="col-md-12">
          <!-- REPORTE FACTURADO EN RANGO DE FECHAS-->
          <div class="box box-success">
            <div class="box-header with-border" style="text-align: center;">
              <h3 class="box-title" id="h3_titulo_vencidas" style="font-weight: bold;">FACTURADO VENCIDO HASTA LA FECHA</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="" id="load_tabla_vencidas" style="margin: auto; text-align: center; display:none;">
                <h4>
                  <p>Cargando datos de Cobro...</p>
                  <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>
                </h4>
              </div>

              <div id="box_tabla_vencidas"></div>
            </div>
          </div>
        </div>
      </div>

      <p>
      <div class="row">
        <div class="col-md-12">
          <center><label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #000000;font-size: 16px">RECAUDO DE COBRANZA</label></center>
          <!--<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">COMO INICIA LA DEUDA</label>-->
          <!--<button class="btn btn-primary btn-sm" onclick="recaudo_form('insertar')"><i class="fa fa-plus"></i>Agregar</button>-->
          <?php include_once 'recaudo_filtro_cobranza.php';?>
          <p>
          <div class="panel panel-primary">
            <div class="panel-body">
              <div id="div_recaudo_tabla" class="contenido_tabla">
              </div>
            </div>
          </div>
        </div>
      </div>
      <p>
      <div class="row">
        <div class="col-md-12">
          <center><label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #000000;font-size: 16px">METAS DE DISMINUCIÓN DE GESTIÓN DE COBRANZA</label></center>
          <!--<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">COMO INICIA LA DEUDA</label>-->
          <!--<button class="btn btn-primary btn-sm" onclick="recaudo_form('insertar')"><i class="fa fa-plus"></i>Agregar</button>-->
          <?php include_once 'recaudo_filtro_mejoragc.php';?>
          <p>
          <div class="panel panel-primary">
            <div class="panel-body">
              <div id="div_mejoragc_tabla" class="contenido_tabla">
              </div>
            </div>
          </div>
        </div>
      </div>
                
      <!--- MESAJES DE GUARDADO -->
      <div class="callout callout-info" id="recaudo_mensaje_tbl" style="display: none;">
        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
      </div>

    </div>
    <div id="div_modal_recaudo_form">
        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
    </div>
    <div id="div_mejoragc_form">
        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
    </div>
    <div id="div_modal_recaudo_cliente_form">
        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
    </div>
    <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
    <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
    
  </section>
    <!-- /.content -->
</div>
