<?php
require_once('../../core/usuario_sesion.php');
require_once ("Recaudo.class.php");
$oRecaudo = new Recaudo();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("Recaudodetalle.class.php");
$oDetalle = new Recaudodetalle();
require_once ('../usuario/Usuario.class.php');
$oUsuario= new Usuario();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

if ($_POST['action'] == "editar") {
    $dts = $oRecaudo->mostrarUno($_POST['recaudo_id']);

    if ($dts['estado'] == 1) {
        $usuario_id = $dts['data']['tb_usuario_id'];
        $recaudo_fec1 = mostrar_fecha($dts['data']['tb_recaudo_fec1']);
        $recaudo_fec2 = mostrar_fecha($dts['data']['tb_recaudo_fec2']);
        $moneda_id = $dts['data']['tb_moneda_id'];
        $recaudo_meta = mostrar_moneda($dts['data']['tb_recaudo_meta']);
        $recaudo_log = mostrar_moneda($dts['data']['tb_recaudo_log']);
        $recaudo_pag = mostrar_moneda($dts['data']['tb_recaudo_pag']);
        $recaudo_est = $dts['data']['tb_recaudo_est'];
        $empresa_id = $dts['data']['tb_empresa_id'];
    }
    $dts = NULL;
    
    if($usuario_id>0){
        $dts =$oUsuario->mostrarUno($usuario_id);
        if ($dts['estado'] == 1) {
            $usuario_nombre = $dts['data']['tb_usuario_nom'];
            $usuario_apellido = $dts['data']['tb_usuario_ape'];
        }
        $dts = NULL;
    }

    $moneda = 'S/.';
    if ($moneda_id == 2)
        $moneda = 'US$';

//ingresos de pago cuotas en asistencia a oficinas
    //soles 
    $dts = $oIngreso->suma_ingresos_cuenta(1, fecha_mysql($recaudo_fec1), fecha_mysql($recaudo_fec2), 1);
    if ($dts['estado'] == 1) {
        $ingreso_total = ($dts['data']['ingreso_total']);
    }
    $dts = NULL;
    //DOLARES AL TIPO DE CAMBIO DEL DÍA
    $dts = $oIngreso->suma_ingresos_cuenta(1, fecha_mysql($recaudo_fec1), fecha_mysql($recaudo_fec2), 2);
    if ($dts['estado'] == 1) {
        $ingreso_total += ($dts['data']['ingreso_total']);
    }
    $dts = NULL;
//----------FIN PAGO EN OFICINAS--------------------//

    /* INGRESOS CON PAGOS EN EL BANCO, DEBE TOMARSE EL RANGO DE FECHA DEL DEPÓSITO
      $dts = $oIngreso->suma_ingresos_cuenta_numoperacion(1, fecha_mysql($recaudo_fec1), fecha_mysql($recaudo_fec2), 1);
      $dt = mysql_fetch_array($dts);
      $ingreso_total += formato_moneda($dt['ingreso_total']);
      mysql_free_result($dts);

      //DOLARES AL TIPO DE CAMBIO DEL DÍA
      $dts = $oIngreso->suma_ingresos_cuenta_numoperacion(1, fecha_mysql($recaudo_fec1), fecha_mysql($recaudo_fec2), 2);
      $dt = mysql_fetch_array($dts);
      $ingreso_total += formato_moneda($dt['ingreso_total']);
      mysql_free_result($dts);

      //----------FIN PAGO EN BANCOS-------------------- */
    $dts = $oDetalle->mostrar_rango_logrado($ingreso_total, 1, $_POST['recaudo_id']); // 1 de tipo recaudo
    $sobrante = count($dts['data']);
    if ($dts['estado'] == 1) {
        $pago_total = floatval($dts['data']['pagar_sol']);
        $recaudodetalle_id = $dts['data']['tb_recaudodetalle_id'];
    }
    $dts = NULL;
    //$sobrante = formato_moneda($ingreso_total - $recaudo_meta);
}
//                    echo 'HASTA AKI TODO NORMAL ENTssdsadsadRANDO ';exit();
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_recaudo" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">INFORMACIÓN DE RECAUDO</h4>
            </div>
            <form id="for_recaudo">
                <input name="action_recaudo" id="action_recaudo" type="hidden" value="<?php echo $_POST['action'] ?>">
                <input name="hdd_recaudo_id" id="hdd_recaudo_id" type="hidden" value="<?php echo $_POST['recaudo_id'] ?>">
                <input type="hidden" id="hdd_usuariogrupo_id" value="<?php echo $usuariogrupo_id ?>">

                <div class="modal-body">
                    <!--<h5 style="font-family: cambria;font-weight: bold;color: #000000">SEGUROS Y UIT</h5>-->
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Sede:</label>
                                    <select class="form-control input-sm" id="cmb_fil_empresa_id" name="cmb_fil_empresa_id">
                                        <?php require_once '../empresa/empresa_select.php'; ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Usuario:</label>
                                    <select class="form-control input-sm" name="cmb_usu_id" id="cmb_usu_id">
                                        <?php 
                                        if($_POST['action']=="editar"){
                                            echo '<option value="'.$usuario_id.'">'.$usuario_apellido.' '.$usuario_nombre.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha 1:</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type="text" name="txt_recaudo_fec1" id="txt_recaudo_fec1" class="form-control input-sm" value="<?php echo $recaudo_fec1?>">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha 2:</label>
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input type="text" name="txt_recaudo_fec2"  id="txt_recaudo_fec2" class="form-control input-sm" value="<?php echo $recaudo_fec2?>">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>  
                            <p>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Moneda:</label>
                                    <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm">
                                        <option value=""></option>
                                        <option value="1" <?php if($moneda_id == 1) echo 'selected'; else echo 'selected';?>>S/.</option>
                                        <option value="2" <?php if($moneda_id == 2) echo 'selected';?>>US$</option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0)" class="btn btn-facebook btn-sm" id="btn_agregar"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>
                                </div>
                            </div>
                             <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tbl_detalle" width="100%" border="1" style="border-collapse: collapse;" class="table-hover">
                                        <tr id="tabla_cabecera">
                                            <th id="tabla_cabecera_fila">DESDE</th>
                                            <th id="tabla_cabecera_fila">HASTA</th>
                                            <th id="tabla_cabecera_fila">PAGAR</th>
                                            <th id="tabla_cabecera_fila">OPC</th>
                                        </tr>
                                    <?php
                                    if ($_POST['action'] == "editar") {
                                        $dts = $oDetalle->mostrarTodos($_POST['recaudo_id'], 1);
                                        if($dts['estado']==1){
                                            foreach ($dts['data']as$Key=>$dt) {
                                                $style = '';
                                                if ($recaudodetalle_id == $dt['tb_recaudodetalle_id'])
                                                    $style = 'style ="background: red;"';
                                                echo '
                                                    <tr id="tabla_cabecera_fila tr_' . $dt['tb_recaudodetalle_id'] . '" ' . $style . '>
                                                        <td id="tabla_fila"><b>S/. ' . mostrar_moneda($dt['meta1']) . '</b></td>
                                                        <td id="tabla_fila"><b>S/. ' . mostrar_moneda($dt['meta2']) . '</b></td>
                                                        <td id="tabla_fila"><b>S/. ' . mostrar_moneda($dt['pagar_sol']) . '</b></td>
                                                        <td id="tabla_fila">-</td>
                                                    </tr>';
                                            }
                                        }
                                    }
                                    ?>
                                    </table>
                                </div>
                            </div>
                            <p>
                            <div>
                                <?php if($_POST['action']=="editar"):?>
                                            <fieldset>
                                                    <legend  style="font-family: cambria;font-weight: bold;color: #000000;font-size: 12px">LOGRADO EN LA FECHA</legend>
                                                    <table width="100%">
                                                        <tr id="tabla_fila" style="height: 30px">
                                                                    <td id="tabla_fila" align="center">
                                                                        <strong><?php echo $moneda.' '. mostrar_moneda($ingreso_total); ?></strong>
                                                                            <input type="hidden" id="hdd_ingreso_total" value="<?php echo $ingreso_total;?>" >
                                                                    </td>
                                                                    <td id="tabla_fila" align="center">
                                                                            //<?php
//                                                                                    if($sobrante == 0)
//                                                                                            echo '<img src="../../images/iconos/dead.ico" width="50" height="50">';
//                                                                                    if($sobrante < 0)
//                                                                                            echo '<img src="../../images/iconos/dead.ico" width="50" height="50">';
//                                                                                    if($sobrante > 0)
//                                                                                            echo '<img src="../../images/iconos/happy.ico" width="50" height="50">';
//                                                                            ?>
                                                                    </td>
                                                                    <td id="tabla_fila" align="center">
                                                                            <?php echo 'Pagar <b>S/. '.$pago_total.'</b>';?>
                                                                    </td>
                                                                    <td  id="tabla_fila" align="center">
                                                                            <?php if($sobrante > 0 && $usuariogrupo_id == 2 && $recaudo_est != 1): ?>
                                                                        <a class="btn btn-success btn-xs"  onClick="recaudo_pagar(<?php echo $_POST['recaudo_id'].','.$pago_total?>)" title="Pagar"><i class="fa fa-money"></i></a>
                                                                            <?php endif; ?>
                                                                    </td>
                                                            </tr>
                                                    </table>
                                                    <?php if($recaudo_est == 1):?>
                                                            <h3 style="color: red;">Esta Meta ya está pagada</h3>
                                                    <?php endif;?>
                                            </fieldset>
                                    <?php endif; ?>
                                
                            </div>
                             
                        </div>
                    </div>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($_POST['action'] == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="seguro_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_seguro">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/recaudo/recaudo_form.js?ver=11222'; ?>"></script>

