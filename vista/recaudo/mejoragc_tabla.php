<?php
require_once('../../core/usuario_sesion.php');
require_once ("Mejoragc.class.php");
$oMejoragc = new Mejoragc();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

$fecha1 = $_POST['fec1'];
$fecha2 = $_POST['fec2'];

$dts = $oMejoragc->mostrarTodos(fecha_mysql($fecha1), fecha_mysql($fecha2));
?>


<table cellspacing="1" id="tabla_mejoragc" class="table-hover" style="width:100%">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">COLABORADOR</th>
            <th id="tabla_cabecera_fila">FECHA INICIO</th>
            <th id="tabla_cabecera_fila">FECHA FIN</th>
            <th id="tabla_cabecera_fila">INICIO SOLES</th>
            <th id="tabla_cabecera_fila">INICIO DÓLARES</th>
            <th id="tabla_cabecera_fila">PORCENTAJE LOGRADO</th>
            <th id="tabla_cabecera_fila">LOGRADO SOLES</th>
            <th id="tabla_cabecera_fila">LOGRADO DÓLARES</th>
            <th id="tabla_cabecera_fila">PAGAR</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
        </tr>
    </thead>
    <?php if ($dts['estado'] == 1) { ?>  
        <tbody> <?php 
            foreach ($dts['data']as $Key=>$dt) { ?>
                <tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila"><?php echo $dt['tb_mejoragc_id'].'  holaaaa' ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_usuario_nom'] . ' ' . $dt['tb_usuario_ape'] ?></td>
                    <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_mejoragc_fec1']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_mejoragc_fec2']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_mejoragc_inisol']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_mejoragc_inidol']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_mejoragc_porc']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_mejoragc_logsol']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_mejoragc_logdol']) ?></td>
                    <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_mejoragc_pag']) ?></td>
                    <td id="tabla_fila" align="center">
                        <?php if ($dt['tb_mejoragc_est'] == 1) echo '<b style="color: red;">Pag.</b>'; ?>

                        <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="mejoragc_form('editar', '<?php echo $dt['tb_mejoragc_id'] ?>')" title="Ver"><i class="fa fa-eye"></i></a>
                        <?php if ($usuariogrupo_id == 2): ?>
                            <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="eliminar_mejoragc('<?php echo $dt['tb_mejoragc_id'] ?>')" title="Eliminar"><i class="fa fa-trash"></i></a>
                        <?php endif; ?>
                    </td>
                </tr><?php
                    }
                    ?>
        </tbody>
    <?php 
            }
            $dts =NULL;
    ?>
</table>