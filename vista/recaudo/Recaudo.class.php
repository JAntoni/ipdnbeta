<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Recaudo extends Conexion{
    public $tb_recaudo_id;
    public $tb_recaudo_reg;
    public $tb_usuario_id;
    public $tb_recaudo_fec1;
    public $tb_recaudo_fec2;
    public $tb_moneda_id;
    public $tb_recaudo_meta;
    public $tb_recaudo_log;
    public $tb_recaudo_pag;
    public $tb_recaudo_est;
    public $tb_empresa_id;
    
    function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_recaudo(
                                            tb_usuario_id, 
                                            tb_recaudo_fec1, 
                                            tb_recaudo_fec2, 
                                            tb_moneda_id, 
                                            tb_recaudo_meta, 
                                            tb_recaudo_pag,
                                            tb_empresa_id) 
                                    VALUES (
                                            :tb_usuario_id,
                                            :tb_recaudo_fec1,
                                            :tb_recaudo_fec2, 
                                            :tb_moneda_id,
                                            :tb_recaudo_meta,
                                            :tb_recaudo_pag,
                                            :tb_empresa_id)"; 
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_recaudo_fec1", $this->tb_recaudo_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_fec2", $this->tb_recaudo_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_recaudo_meta", $this->tb_recaudo_meta, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_pag", $this->tb_recaudo_pag, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_recaudo_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_recaudo_id'] = $tb_recaudo_id;
        return $data; //si es correcto el recaudo retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_recaudo SET  
                                            tb_usuario_id =:tb_usuario_id , 
                                            tb_recaudo_fec1 = :tb_recaudo_fec1, 
                                            tb_recaudo_fec2 = :tb_recaudo_fec2, 
                                            tb_moneda_id = :tb_moneda_id,
                                            tb_recaudo_meta = :tb_recaudo_meta,
                                            tb_recaudo_pag = :tb_recaudo_pag 
                                        WHERE  
                                            tb_recaudo_id =:tb_recaudo_id;";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_recaudo_fec1", $this->tb_recaudo_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_fec2", $this->tb_recaudo_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_recaudo_meta", $this->tb_recaudo_meta, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_pag", $this->tb_recaudo_pag, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_id", $this->tb_recaudo_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($recaudo_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_recaudo WHERE tb_recaudo_id = :tb_recaudo_id"; 

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_recaudo_id", $recaudo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarTodos($fecha1, $fecha2){
      try {
         $sql = "SELECT * FROM tb_recaudo re INNER JOIN tb_usuario us on us.tb_usuario_id = re.tb_usuario_id WHERE tb_recaudo_fec1 >= :tb_recaudo_fec1 and tb_recaudo_fec2 <= :tb_recaudo_fec2";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_recaudo_fec1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_fec2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  mostrarUno($id){
      try {
         $sql="SELECT * FROM tb_recaudo WHERE tb_recaudo_id=:tb_recaudo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_recaudo_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($recaudo_id, $recaudo_columna, $recaudo_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($recaudo_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_recaudo SET " . $recaudo_columna . " =:recaudo_valor WHERE tb_recaudo_id =:recaudo_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":recaudo_id", $recaudo_id, PDO::PARAM_INT);
                if ($param_tip == 'INT') {
                    $sentencia->bindParam(":recaudo_valor", $recaudo_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":recaudo_valor", $recaudo_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el recaudo retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    function pagar_mes_usuario($usuario_id, $fecha1, $fecha2){
      try {
         $sql = "SELECT IFNULL(SUM(tb_recaudo_pag),0) AS pago_recaudo FROM tb_recaudo WHERE tb_recaudo_est = 1 AND tb_recaudo_fec1 >= :tb_recaudo_fec1 
                AND tb_recaudo_fec2 <= :tb_recaudo_fec2 AND tb_usuario_id = :tb_usuario_id"; 
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_recaudo_fec1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_recaudo_fec2", $fecha2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*sucede que ahora las metas de recaudacion ya no empiezan desde el 01 del mes y terminan el 31 del mes, sino que pueden empezar el 02 o 03 del mes y finalizar el 02 o 03
    del otro mes, por ejemplo: empieza la meta el 01-07-2022 y termina el 01-08-2022, luego la otra meta empieza el 02-08-2022 y termina el 31-08-2022
    para ello filtramos 1 registro de la fecha de inicio por mes y año y obtenemos la fecha de inicio y fecha de fin de esa meta de recaudo
    */
    function obtener_fecha_inicio_fin_por_anio_mes($anio_mes){
      try {
         $sql = "SELECT * FROM tb_recaudo WHERE LEFT(tb_recaudo_fec1, 7) =:anio_mes limit 1"; 
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":anio_mes", $anio_mes, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
}
