<?php

require_once('../../core/usuario_sesion.php');
require_once ("Mejoragc.class.php");
$oMejoragc = new Mejoragc();
require_once("Recaudodetalle.class.php");
$oDetalle = new Recaudodetalle();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

if($_POST['action_mejoragc']=="insertar")
{
	$usuario_id = intval($_POST['cmb_usu_id']);
	$mejoragc_fec1 = fecha_mysql($_POST['txt_mejoragc_fec1']);
	$mejoragc_fec2 = fecha_mysql($_POST['txt_mejoragc_fec2']);
	$mejoragc_inisol = moneda_mysql($_POST['txt_mejoragc_inisol']);
	$mejoragc_inidol = moneda_mysql($_POST['txt_mejoragc_inidol']);
        $tb_empresa_id = intval($_POST['cmb_fil_empresa_id']);

	$mejoragc_porc = array_filter($_POST['txt_mejoragc_porc']);
	$mejoragc_pagsol = array_filter($_POST['txt_mejoragc_pagsol']);
	$mejoragc_pagdol = array_filter($_POST['txt_mejoragc_pagdol']);
	$mejoragc_metsol = array_filter($_POST['txt_mejoragc_metsol']);
	$mejoragc_metdol = array_filter($_POST['txt_mejoragc_metdol']);
	$tipo = 2; // tipo 2 tipo de MEJORA GC 
        
	$oMejoragc->tb_usuario_id = $usuario_id; 
	$oMejoragc->tb_mejoragc_fec1 = $mejoragc_fec1; 
	$oMejoragc->tb_mejoragc_fec2 = $mejoragc_fec2; 
	$oMejoragc->tb_mejoragc_inisol = $mejoragc_inisol; 
	$oMejoragc->tb_mejoragc_inidol = $mejoragc_inidol; 
	$oMejoragc->tb_mejoragc_metsol = 0;
	$oMejoragc->tb_mejoragc_metdol = 0; 
	$oMejoragc->tb_mejoragc_porc = 0;
	$oMejoragc->tb_mejoragc_pag=0;
        $oMejoragc->tb_empresa_id = $tb_empresa_id;

	if(count($mejoragc_porc) == count($mejoragc_pagsol) && count($mejoragc_porc) == count($mejoragc_metsol)){

		$result = $oMejoragc->insertar();
		
                if (intval($result['estado']) == 1) {
                    $mejoragc_id = $result['tb_mejoragc_id'];
                }

		$cond = count($mejoragc_porc);
                for ($i=0; $i < $cond; $i++) { 
                    $oDetalle->tipo=$tipo; 
                    $oDetalle->meta1=moneda_mysql($mejoragc_metsol[$i]); 
                    $oDetalle->meta2= moneda_mysql($mejoragc_metdol[$i]);
                    $oDetalle->pagar_sol= moneda_mysql($mejoragc_pagsol[$i]); 
                    $oDetalle->pagar_dol=moneda_mysql($mejoragc_pagdol[$i]); 
                    $oDetalle->porcentaje= moneda_mysql($mejoragc_porc[$i]);
                    $oDetalle->modulo_id=$mejoragc_id;
                  $oDetalle->insertar();
                }

		$data['mejoragc_id'] = $mejoragc_id;
		$data['mejoragc_msj'] = 'Se registró la meta de GC correctamente.';
	}
	else{
		$data['mejoragc_id'] = 0;
    $data['mejoragc_msj'] = 'Las campos de metas no están completos';
  }

  echo json_encode($data);
}

if($_POST['action_mejoragc']=="editar")
{
	$usuario_id = intval($_POST['cmb_usu_id']);// usuario a cumplir meta
	$mejoragc_fec1 = fecha_mysql($_POST['txt_mejoragc_fec1']); // fecha de inicio
	$mejoragc_fec2 = fecha_mysql($_POST['txt_mejoragc_fec2']); // fecha de fin
	$mejoragc_inisol = moneda_mysql($_POST['txt_mejoragc_inisol']);// inicio soles
	$mejoragc_inidol = moneda_mysql($_POST['txt_mejoragc_inidol']); // inicio dolares

	$mejoragc_logsol = moneda_mysql($_POST['txt_mejoragc_logsol']); // monto logrado en soles
	$mejoragc_logdol = moneda_mysql($_POST['txt_mejoragc_logdol']); // monto logrado en dolares
	$mejoragc_id = intval($_POST['hdd_mejoragc_id']);
	$tb_empresa_id = intval($_POST['cmb_fil_empresa_id']);// empresa a registrar
        
	$oMejoragc->tb_usuario_id = $usuario_id;
	$oMejoragc->tb_mejoragc_fec1 = $mejoragc_fec1;
	$oMejoragc->tb_mejoragc_fec2 =$mejoragc_fec2;
	$oMejoragc->tb_mejoragc_inisol = $mejoragc_inisol;
	$oMejoragc->tb_mejoragc_inidol = $mejoragc_inidol;
	$oMejoragc->tb_mejoragc_logsol = $mejoragc_logsol;
	$oMejoragc->tb_mejoragc_logdol = $mejoragc_logdol;
	$oMejoragc->tb_mejoragc_id = $mejoragc_id;
	$oMejoragc->tb_empresa_id = $tb_empresa_id;

	$oMejoragc->modificar();
	
	$data['mejoragc_id'] = $mejoragc_id;
	$data['mejoragc_msj'] = 'Se modificó la Meta de GC correctamente.';

	echo json_encode($data);
}

if($_POST['action']=="pagar"){
	$mejoragc_id = intval($_POST['mejoragc_id']);
	$mejoragc_logsol = moneda_mysql($_POST['mejoragc_logsol']);//monto logrado en soles
	$mejoragc_logdol = moneda_mysql($_POST['mejoragc_logdol']); // monto logrado en dolares
	$pagar_sol = moneda_mysql($_POST['pagar_sol']);
	$pagar_dol = moneda_mysql($_POST['pagar_dol']);
	$porcentaje = moneda_mysql($_POST['porcentaje']);

	$pagar = $pagar_sol + $pagar_dol;

	$oMejoragc->modificar_campo($mejoragc_id, 'tb_mejoragc_est', 1,'INT'); //estado 1, el monto se paga a PLANILLA
	$oMejoragc->modificar_campo($mejoragc_id, 'tb_mejoragc_logsol', $mejoragc_logsol,'STR'); //estado 1, el monto se paga a PLANILLA
	$oMejoragc->modificar_campo($mejoragc_id, 'tb_mejoragc_logdol', $mejoragc_logdol,'STR'); //estado 1, el monto se paga a PLANILLA
	$oMejoragc->modificar_campo($mejoragc_id, 'tb_mejoragc_pag', $pagar,'STR');
	$oMejoragc->modificar_campo($mejoragc_id, 'tb_mejoragc_porc', $porcentaje,'STR');

	$data['mejoragc_id'] = $mejoragc_id;
	$data['mejoragc_msj'] = 'Se ha registrado el pago correctamente';

	echo json_encode($data);
}

if($_POST['action'] == "logro"){
	$mejoragc_id = intval($_POST['mejoragc_id']);
	$mejoragc_logsol = moneda_mysql($_POST['mejoragc_logsol']);
	$mejoragc_logdol = moneda_mysql($_POST['mejoragc_logdol']);

	$pagar_sol = 0; $pagar_dol = 0;
	$dts = $oDetalle->mostrar_logrado_mejoragc($mejoragc_logsol, $mejoragc_logdol, $mejoragc_id);       
		if($dts['estado']== 1){
			foreach ($dts['data']as $key=>$dt) {
				if($dt['tipo_meta'] == 1){
					//pago en soles
					$detalle_sol_id = $dt['tb_recaudodetalle_id'];
					$porcentaje = $dt['porcentaje'];
					$pagar_sol = floatval($dt['pagar_sol']);
				}
				if($dt['tipo_meta'] == 2){
					//pago en dolares
					$detalle_dol_id = $dt['tb_recaudodetalle_id'];
					$porcentaje = $dt['porcentaje'];
					$pagar_dol = floatval($dt['pagar_dol']);
				}
			}
			
			$pagar = $pagar_sol + $pagar_dol;

			$data['estado'] = 1;
			$data['detalle_sol_id'] = $detalle_sol_id;
			$data['detalle_dol_id'] = $detalle_dol_id;
			$data['porcentaje'] = $porcentaje;
			$data['pagar_sol'] = $pagar_sol;
			$data['pagar_dol'] = $pagar_dol;
			$data['mejoragc_msj'] = 'Genial se ha logrado la meta. Se pagará S/. '. mostrar_moneda($pagar);
		}
		else{
			$data['estado'] = 0;
			$data['pagar'] = 0;
			$data['mejoragc_msj'] = 'Lamentable no se cumplió ninguna meta este mes';
		}
	echo json_encode($data);
}

if($_POST['action']=="eliminar"){
	$mejoragc_id = intval($_POST['mejoragc_id']);

	$oMejoragc->eliminar($mejoragc_id);

	echo "Se eliminó la menta correctamente";
}

?>