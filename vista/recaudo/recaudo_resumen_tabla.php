<?php
require_once('../../core/usuario_sesion.php');
require_once ("Recaudo.class.php");
$oRecaudo = new Recaudo();
require_once ('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

$fecha1 = fecha_mysql($_POST['fec1']);
$fecha2 = fecha_mysql($_POST['fec2']);


//---------------- CREDITO ASIVEHHHHH --------------------------------------------------------------------//
$suma_asiveh = 0;
$tabla = 'tb_creditoasiveh';
$cre_tip = 2;
$subtotal_asiveh_vigentes = 0;
//PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE ASIVEH fecha_detalle entre fec1 y fec2
$dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_asiveh_vigentes += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_asiveh_vigentes += $ingreso_imp;
    }
}
$dts = NULL;
$tabla = 'tb_creditoasiveh';
$cre_tip = 2;
$subtotal_asiveh_vencidas = 0;
//PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
$dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_asiveh_vencidas += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_asiveh_vencidas += $ingreso_imp;
    }
}
$dts = NULL;
$suma_asiveh = $subtotal_asiveh_vigentes + $subtotal_asiveh_vencidas;
//----------------------------------------------- FIN ASIVEH ----------------------------------------//
//---------------- CREDITO GARVEH --------------------------------------------------------------------//
$suma_garveh = 0;
$tabla = 'tb_creditogarveh';
$cre_tip = 3;
$subtotal_garveh_vigentes = 0;
//PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE ASIVEH fecha_detalle entre fec1 y fec2
$dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_garveh_vigentes += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_garveh_vigentes += $ingreso_imp;
    }
}
$dts2 = NULL;

$tabla = 'tb_creditogarveh';
$cre_tip = 3;
$subtotal_garveh_vencidas = 0;
//PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
$dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_garveh_vencidas += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_garveh_vencidas += $ingreso_imp;
    }
}
$dts = NULL;

$suma_garveh = $subtotal_garveh_vigentes + $subtotal_garveh_vencidas;
//----------------------------------------------- FIN ASIVEH ----------------------------------------//
//---------------- CREDITO HIPOOO --------------------------------------------------------------------//
$suma_hipo = 0;
$tabla = 'tb_creditohipo';
$cre_tip = 4;
$subtotal_hipo_vigentes = 0;
//PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE ASIVEH fecha_detalle entre fec1 y fec2
$dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_hipo_vigentes += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_hipo_vigentes += $ingreso_imp;
    }
}
$dts = NULL;

$tabla = 'tb_creditohipo';
$cre_tip = 4;
$subtotal_hipo_vencidas = 0;
//PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
$dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_hipo_vencidas += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_hipo_vencidas += $ingreso_imp;
    }
}
$dts = NULL;

$suma_hipo = $subtotal_hipo_vigentes + $subtotal_hipo_vencidas;
//----------------------------------------------- FIN HIIPOOOOOO ----------------------------------------//
//-------------------------------- CREDITO MENOR -------------------------------
$suma_menor = 0;
$tabla = 'tb_creditomenor';
$cre_tip = 1;
$subtotal_menor_vigentes = 0;
//PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE MENOR fecha_detalle entre fec1 y fec2
$dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_menor_vigentes += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_menor_vigentes += $ingreso_imp;
    }
}
$dts = NULL;

$tabla = 'tb_creditomenor';
$cre_tip = 1;
$subtotal_menor_vencidas = 0;
//PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
$dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $subtotal_menor_vencidas += $ingreso_imp * $tipo_cambio;
        else
            $subtotal_menor_vencidas += $ingreso_imp;
    }
}
$dts = NULL;

$suma_menor = $subtotal_menor_vigentes + $subtotal_menor_vencidas;

//----------------------------- FIIIN MENOR -------------------------------

$total_vigentes = $subtotal_asiveh_vigentes + $subtotal_garveh_vigentes + $subtotal_hipo_vigentes + $subtotal_menor_vigentes;
$total_vencidas = $subtotal_asiveh_vencidas + $subtotal_garveh_vencidas + $subtotal_hipo_vencidas + $subtotal_menor_vencidas;

$global = $total_vigentes + $total_vencidas;

//------------------------------------------------------ LIQUIDACIONES--------------------------

/*
  liquidacion ASIVE: 110,
  Liquidacion GARVEH: 111,
  Liquidacion HIPO: 112
  Liquidación MENOR: 109
 */
//-------------------------   ASIVEHHH -----------------------------
$liquidacion_asiveh = 0;
$dts = $oIngreso->liquidacion_total_tipo_credito(110, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $liquidacion_asiveh += $ingreso_imp * $tipo_cambio;
        else
            $liquidacion_asiveh += $ingreso_imp;
    }
}
$dts = NULL;
//-------------------------   GARVEHHHH  -----------------------------
$liquidacion_garveh = 0;
$dts = $oIngreso->liquidacion_total_tipo_credito(111, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $liquidacion_garveh += $ingreso_imp * $tipo_cambio;
        else
            $liquidacion_garveh += $ingreso_imp;
    }
}
$dts = NULL;
//-------------------------   HIPOOOOOOOOO  -----------------------------
$liquidacion_hipo = 0;
$dts = $oIngreso->liquidacion_total_tipo_credito(112, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $liquidacion_hipo += $ingreso_imp * $tipo_cambio;
        else
            $liquidacion_hipo += $ingreso_imp;
    }
}
$dts = NULL;

//-------------------------   MENOOOOOOOOOOOORRRRRRRRR  -----------------------------
$liquidacion_menor = 0;
$dts = $oIngreso->liquidacion_total_tipo_credito(109, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $liquidacion_menor += $ingreso_imp * $tipo_cambio;
        else
            $liquidacion_menor += $ingreso_imp;
    }
}
$dts = NULL;
// ---------------------------------- fin liquidacion
//-------------------------------------------- AMORTIZACIONES --------------------
/*
  amortizacion asiveh: 106,
  amortizacion garveh: 107,
  amortizacion hipo: 108
  amortización menor: 105
 */
//-------------------------   ASIVEHHHH  -----------------------------
$amortizacion_asiveh = 0;
$dts = $oIngreso->amortizacion_total_tipo_credito(106, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $amortizacion_asiveh += $ingreso_imp * $tipo_cambio;
        else
            $amortizacion_asiveh += $ingreso_imp;
    }
}
$dts = NULL;

//-------------------------   GARVEHHHH  -----------------------------
$amortizacion_garveh = 0;
$dts = $oIngreso->amortizacion_total_tipo_credito(107, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $amortizacion_garveh += $ingreso_imp * $tipo_cambio;
        else
            $amortizacion_garveh += $ingreso_imp;
    }
}
$dts = NULL;

//-------------------------   HIPOOOOOOO  -----------------------------
$amortizacion_hipo = 0;
$dts = $oIngreso->amortizacion_total_tipo_credito(108, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $amortizacion_hipo += $ingreso_imp * $tipo_cambio;
        else
            $amortizacion_hipo += $ingreso_imp;
    }
}
$dts = NULL;

//-------------------------   MENOOOOOOOOOOORRRRRRR  -----------------------------
$amortizacion_menor = 0;
$dts = $oIngreso->amortizacion_total_tipo_credito(105, $fecha1, $fecha2);
if ($dts['estado'] == 1) {
    foreach ($dts['data']as $key => $dt) {
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_monedacambio_val'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2)
            $amortizacion_menor += $ingreso_imp * $tipo_cambio;
        else
            $amortizacion_menor += $ingreso_imp;
    }
}
$dts = NULL;
// ---------------------------------- fin amortizacion
//echo 'hasta aki todowwwwwwwwww normal';exit();

$total_liquidacion = $liquidacion_asiveh + $liquidacion_garveh + $liquidacion_hipo + $liquidacion_menor;
$total_amortizacion = $amortizacion_asiveh + $amortizacion_garveh + $amortizacion_hipo + $amortizacion_menor;
$global_liqui_amor = $total_liquidacion + $total_amortizacion;
?>
<div class="col-md-6">
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">CUOTAS VENCIDAS / CUOTAS VIGENTES</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <table cellspacing="1" class="table table-striped table-bordered dataTable display">
        <thead>
          <tr>
            <th>TIPO CRÉDITO</th>
            <th>MONTO VIGENTES</th>
            <th>MONTO VENCIDAS</th>
            <th>SUBTOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a onclick="recaudo_form_cliente('asistencia')">Crédito ASIVEH</a></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_asiveh_vigentes); ?></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_asiveh_vencidas); ?></td>
            <td>S/ <?php echo mostrar_moneda($suma_asiveh); ?></td>
          </tr>
          <tr>
            <td><a onclick="recaudo_form_cliente('garantia')">Crédito GARVEH</a></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_garveh_vigentes); ?></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_garveh_vencidas); ?></td>
            <td>S/ <?php echo mostrar_moneda($suma_garveh); ?></td>
          </tr>
          <tr>
            <td><a onclick="recaudo_form_cliente('hipotecario')">Crédito HIPO</a></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_hipo_vigentes); ?></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_hipo_vencidas); ?></td>
            <td>S/ <?php echo mostrar_moneda($suma_hipo); ?></td>
          </tr>
          <tr>
            <td><a onclick="recaudo_form_cliente('menor')">Crédito MENOR</a></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_menor_vigentes); ?></td>
            <td>S/ <?php echo mostrar_moneda($subtotal_menor_vencidas); ?></td>
            <td>S/ <?php echo mostrar_moneda($suma_menor); ?></td>
          </tr>
          <tr>
            <td>TOTAL</td>
            <td>S/ <?php echo mostrar_moneda($total_vigentes); ?></td>
            <td>S/ <?php echo mostrar_moneda($total_vencidas); ?></td>
            <td>S/ <?php echo mostrar_moneda($global); ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="col-md-6">
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">AMORTIZACIÓN / LIQUIDACIÓN</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <table cellspacing="1" class="table table-striped table-bordered dataTable display">
        <thead>
          <tr>
            <th>TIPO CRÉDITO</th>
            <th>MONTO LIQUIDACION</th>
            <th>MONTO AMORTIZACION</th>
            <th>SUBTOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a onclick="recaudo_form_cliente('asistencialiquidado')">Crédito ASIVEH</a></td>
            <td>S/ <?php echo mostrar_moneda($liquidacion_asiveh); ?></td>
            <td>S/ <?php echo mostrar_moneda($amortizacion_asiveh); ?></td>
            <td>S/ <?php echo mostrar_moneda($liquidacion_asiveh + $amortizacion_asiveh); ?></td>
          </tr>
          <tr>
            <td><a onclick="recaudo_form_cliente('garantialiquidado')">Crédito GARVEH</a></td>
            <td>S/ <?php echo mostrar_moneda($liquidacion_garveh); ?></td>
            <td>S/ <?php echo mostrar_moneda($amortizacion_garveh); ?></td>
            <td>S/ <?php echo mostrar_moneda($liquidacion_garveh + $amortizacion_garveh); ?></td>
          </tr>
          <tr>
            <td><a onclick="recaudo_form_cliente('hipotecarioliquidado')">Crédito HIPO</a></td>
            <td>S/ <?php echo mostrar_moneda($liquidacion_hipo); ?></td>
            <td>S/ <?php echo mostrar_moneda($amortizacion_hipo); ?></td>
            <td>S/ <?php echo mostrar_moneda($liquidacion_hipo + $amortizacion_hipo); ?></td>
          </tr>
            <tr>
            <td>TOTAL</td>
            <td>S/ <?php echo mostrar_moneda($total_liquidacion); ?></td>
            <td>S/ <?php echo mostrar_moneda($total_amortizacion); ?></td>
            <td>S/ <?php echo mostrar_moneda($global_liqui_amor); ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>