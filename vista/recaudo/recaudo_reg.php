<?php

require_once('../../core/usuario_sesion.php');
require_once ("Recaudo.class.php");
$oRecaudo = new Recaudo();
require_once("Recaudodetalle.class.php");
$oDetalle = new Recaudodetalle();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

if ($_POST['action_recaudo'] == "insertar") {
    $oRecaudo->tb_usuario_id = intval($_POST['cmb_usu_id']);
    $oRecaudo->tb_recaudo_fec1 = fecha_mysql($_POST['txt_recaudo_fec1']);
    $oRecaudo->tb_recaudo_fec2 = fecha_mysql($_POST['txt_recaudo_fec2']);
    $oRecaudo->tb_moneda_id = intval($_POST['cmb_mon_id']);
    $oRecaudo->tb_recaudo_meta = moneda_mysql($_POST['txt_recaudo_meta']);
    $oRecaudo->tb_recaudo_pag = moneda_mysql($_POST['txt_recaudo_pag']);
    $oRecaudo->tb_empresa_id = intval($_POST['cmb_fil_empresa_id']);

    $meta1 = array_filter($_POST['meta1']);
    $meta2 = array_filter($_POST['meta2']);
    $pagar = array_filter($_POST['pagar']);
    $tipo = 1; // tipo 1 tiupo de RECAUDACIÓN 

    if (count($meta1) == count($meta2) && count($meta1) == count($pagar)) {
        $result = $oRecaudo->insertar();

        if (intval($result['estado']) == 1) {
            $recaudo_id = $result['tb_recaudo_id'];
        }

        $cond = count($meta1);
        for ($i = 0; $i < $cond; $i++) {
            $oDetalle->tipo=$tipo; 
            $oDetalle->meta1=moneda_mysql($meta1[$i]); 
            $oDetalle->meta2= moneda_mysql($meta2[$i]);
            $oDetalle->pagar_sol= moneda_mysql($pagar[$i]); 
            $oDetalle->pagar_dol=0; 
            $oDetalle->porcentaje=0;
            $oDetalle->modulo_id=$recaudo_id;
            $oDetalle->insertar();
        }
        $data['recaudo_id'] = $recaudo_id;
        $data['recaudo_msj'] = 'Metas para recaudo registrado correctamente.';
    } else {
        $data['recaudo_id'] = 0;
        $data['recaudo_msj'] = 'Las campos de metas no están completos';
    }

    echo json_encode($data);
}

if ($_POST['action_recaudo'] == "editar") {
    $oRecaudo->tb_usuario_id = intval($_POST['cmb_usu_id']);
    $oRecaudo->tb_recaudo_fec1 = fecha_mysql($_POST['txt_recaudo_fec1']);
    $oRecaudo->tb_recaudo_fec2 = fecha_mysql($_POST['txt_recaudo_fec2']);
    $oRecaudo->tb_moneda_id = intval($_POST['cmb_mon_id']);
    $oRecaudo->tb_recaudo_meta = moneda_mysql($_POST['txt_recaudo_meta']);
    $oRecaudo->tb_recaudo_pag = moneda_mysql($_POST['txt_recaudo_pag']);
    $oRecaudo->tb_recaudo_id = intval($_POST['hdd_recaudo_id']);    

    $oRecaudo->modificar();

    $data['recaudo_id'] = intval($_POST['hdd_recaudo_id']); 
    $data['recaudo_msj'] = 'Se registró el Recaudo correctamente.';

    echo json_encode($data);
}

if ($_POST['action'] == "pagar") {
    $recaudo_id = intval($_POST['recaudo_id']);
    $pago = floatval($_POST['pago']);
    $ingreso_total = floatval($_POST['ingreso_total']);

    $oRecaudo->modificar_campo($recaudo_id, 'tb_recaudo_est', 1,'INT'); //estado 1, el monto se paga a PLANILLA
    $oRecaudo->modificar_campo($recaudo_id, 'tb_recaudo_pag', $pago,'STR'); //total a PAGAR
    $oRecaudo->modificar_campo($recaudo_id, 'tb_recaudo_log', $ingreso_total,'STR'); //estado 1, el monto se paga a PLANILLA

    $data['recaudo_id'] = $recaudo_id;
    $data['recaudo_msj'] = 'Se ha registrado el pago correctamente .' . $_POST['pago'];

    echo json_encode($data);
}

if ($_POST['action'] == "eliminar") {
    $recaudo_id = intval($_POST['recaudo_id']);

    $oRecaudo->eliminar($recaudo_id);

    echo "Se eliminó la menta correctamente";
}
?>