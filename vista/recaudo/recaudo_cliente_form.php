<?php
require_once ('../../core/usuario_sesion.php');
require_once ('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once ('../funciones/funciones.php');
require_once ('../funciones/fechas.php');

$tipo = $_POST['action'];

$cre_tip = 2;

$tablas = "";
$fecha1 = fecha_mysql($_POST['fecha1']);
$fecha2 = fecha_mysql($_POST['fecha2']);

//echo 'fecha 1='.$fecha1.' and fecha 2='.$fecha2;exit();
//$fecha1 = date('Y-m-01');
//$fecha2 = date('Y-m-t');
$subtotal_asiveh_vigentes = 0;
$moneda = "S/.";

//? TABALA DE PAGOS PARA ASISTENCIA VEHICULAR
if ($tipo == 'asistencia') {
  $tabla = 'tb_creditoasiveh';
  $dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      $credito = "VIGENTE";
      foreach ($dts['data']as $key => $dt) {
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          $subtotal_asiveh_vigentes += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } else {
          $subtotal_asiveh_vigentes += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }
        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuotadetalle_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
              </tr>';
          }
        $resu = NULL;
      }

      $tablas .= '
        <tr id="tabla_cabecera_fila" style="height:25px;">
          <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VIGENTE</b></td>
          <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($subtotal_asiveh_vigentes) . '</b></td>
        </tr>';
    }
  $dts = NULL;

  $dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      $credito = "VENCIDA";
      foreach ($dts['data']as $key => $dt) {
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          $subtotal_asiveh_vencidas += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } 
        else {
          $subtotal_asiveh_vencidas += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }
        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuotadetalle_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
              </tr>';
          }
        $resu = NULL;
      }
      $tablas .= '
        <tr id="tabla_cabecera_fila" style="height:25px;">
          <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VENCIDO</b></td>
          <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($subtotal_asiveh_vencidas) . '</b></td>
      </tr>';
    }
  $dts = NULL;

  $tablas .= '
    <tr id="tabla_cabecera_fila" style="height:25px;">
      <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
      <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($subtotal_asiveh_vigentes + $subtotal_asiveh_vencidas) . '</b></td>
  </tr>';
}

//todo TABLA DE PAGOS PARA GARANTIA VEHICULAR
elseif ($tipo == 'garantia') {

  $suma_garveh = 0;
  $tabla = 'tb_creditogarveh';
  $cre_tip = 3;
  $subtotal_garveh_vigentes = 0;
  //PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE ASIVEH fecha_detalle entre fec1 y fec2
  $dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      foreach ($dts['data']as $key => $dt) {
        $credito = "VIGENTE";
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          $subtotal_garveh_vigentes += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } 
        else {
          $subtotal_garveh_vigentes += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }

        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuotadetalle_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
              </tr>';
          }
        $resu = NULL;
        }
        $tablas .= '
          <tr id="tabla_cabecera_fila" style="height:25px;">
            <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VIGENTES</b></td>
            <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($subtotal_garveh_vigentes) . '</b></td>
        </tr>';
    }
  $dts = NULL;

  $subtotal_garveh_vencidas = 0;
  //PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
  $dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      foreach ($dts['data']as $key => $dt) {
        $credito = "VENCIDA";
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          $subtotal_garveh_vencidas += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } 
        else {
          $subtotal_garveh_vencidas += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }

        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuotadetalle_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
              </tr>';
          }
        $resu = NULL;
        }
        $tablas .= '
          <tr id="tabla_cabecera_fila" style="height:25px;">
            <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VENCIDO</b></td>
            <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($subtotal_garveh_vencidas) . '</b></td>
        </tr>';
    }
  $dts = NULL;
  $tablas .= '
    <tr id="tabla_cabecera_fila" style="height:25px;">
      <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
      <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($subtotal_garveh_vigentes + $subtotal_garveh_vencidas) . '</b></td>
  </tr>';
} 

//! TABLA DE PAGOS PARA CREDITO HIPOTECARIO
elseif ($tipo == 'hipotecario') {

  $tabla = 'tb_creditohipo';
  $cre_tip = 4;
  $subtotal_hipo_vigentes = 0;
  //PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE ASIVEH fecha_detalle entre fec1 y fec2
  $dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      foreach ($dts['data']as $key => $dt) {
        $credito = "VIGENTE";
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          $subtotal_hipo_vigentes += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } 
        else {
          $subtotal_hipo_vigentes += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }
        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuotadetalle_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
              </tr>';
          }
        $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
            <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VIGENTE</b></td>
            <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($subtotal_hipo_vigentes) . '</b></td>
        </tr>';
    }
  $dts = NULL;

  $subtotal_hipo_vencidas = 0;
  //PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
  $dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      foreach ($dts['data']as $key => $dt) {
        $credito = "VENCIDA";
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = $dt['tb_cuotapago_tipcam'];
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          $subtotal_hipo_vencidas += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } else {
          $subtotal_hipo_vencidas += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }
        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuotadetalle_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
              </tr>';
          }
        $resu = NULL;
        }
        $tablas .= '
          <tr id="tabla_cabecera_fila" style="height:25px;">
            <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VENCIDO</b></td>
            <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($subtotal_hipo_vencidas) . '</b></td>
        </tr>';
    }
  $dts = NULL;
  $tablas .= '
    <tr id="tabla_cabecera_fila" style="height:25px;">
      <td id="tabla_fila" colspan="9" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
      <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($subtotal_hipo_vigentes + $subtotal_hipo_vencidas) . '</b></td>
  </tr>';
} 

//* TABLA DE PAGOS PARA CREDITO MENOR
elseif ($tipo == 'menor') {
  $tabla = 'tb_creditomenor';
  $cre_tip = 1;
  $subtotal_menor_vigentes = 0;
  $subtotal_menor_extra = 0;
  //PAGO DE CUOTAS FACTURADAS EN EL MISMO MES EN QUE SON PAGADAS, DE MENOR fecha_detalle entre fec1 y fec2
  $dts = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      foreach ($dts['data']as $key => $dt) {
        $credito = "VIGENTE";
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = 1.00;
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          //$subtotal_menor_vigentes += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } else {
          //$subtotal_menor_vigentes += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }

        $pago_real = 0; $pago_extra = 0;

        if($ingreso_imp >= $dt['cuota_real']){
          $pago_real = $dt['cuota_real'];
          $pago_extra = $ingreso_imp - $dt['cuota_real'];
          $subtotal_menor_vigentes += $pago_real;
          $subtotal_menor_extra += $pago_extra;
        }
        if($ingreso_imp < $dt['cuota_real']){
          $pago_real = $ingreso_imp;
          $pago_extra = 0;
          $subtotal_menor_vigentes += $pago_real;
          $subtotal_menor_extra += $pago_extra;
        }
        
        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '
              <tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuota_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($dt['cuota_real']) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($pago_real) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($pago_extra) . '</td>
              </tr>';
          }
        $resu = NULL;
      }
      $tablas .= '
        <tr id="tabla_cabecera_fila" style="height:25px;">
          <td id="tabla_fila" colspan="10" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VIGENTE</b></td>
          <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' .mostrar_moneda($subtotal_menor_vigentes). '</b></td>
          <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' .mostrar_moneda($subtotal_menor_extra). '</b></td>
      </tr>';
    }
  $dts = NULL;

  $subtotal_menor_vencidas = 0;
  $subtotal_menor_extra_vencida = 0;
  //PAGO DE CUOTAS FACTURADAS VENCIDAS, EJEMPLO: FACTURADA EN AGOSTO, PERO PAGADA EN SEPTIEMBRE, fec_detalle < fec1
  $dts = $oIngreso->ingreso_credito_rango_fecha_vencida($fecha1, $fecha2, $tabla, $cre_tip);
    if ($dts['estado'] == 1) {
      foreach ($dts['data']as $key => $dt) {
        $credito = "VENCIDA";
        $tb_cliente_id = ($dt['tb_cliente_id']);
        $ingreso_imp = ($dt['tb_ingreso_imp']);
        $tipo_cambio = 1.00;
        $tb_ingreso_fec = $dt['tb_ingreso_fec'];
        $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

        if ($moneda_id == 2) {
          //$subtotal_menor_vencidas += $ingreso_imp * $tipo_cambio;
          $moneda = "$$ ";
        } 
        else {
          //$subtotal_menor_vencidas += $ingreso_imp;
          $tipo_cambio = 1.00;
          $moneda = "S/. ";
        }

        $pago_real = 0; $pago_extra = 0;

        if($ingreso_imp >= $dt['cuota_real']){
          $pago_real = $dt['cuota_real'];
          $pago_extra = $ingreso_imp - $dt['cuota_real'];
          $subtotal_menor_vencidas += $pago_real;
          $subtotal_menor_extra_vencida += $pago_extra;
        }
        if($ingreso_imp < $dt['cuota_real']){
          $pago_real = $ingreso_imp;
          $pago_extra = 0;
          $subtotal_menor_vencidas += $pago_real;
          $subtotal_menor_extra_vencida += $pago_extra;
        }

        $resu = $oCliente->mostrarUno($tb_cliente_id);
          if ($resu['estado'] == 1) {
            $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                <td id="tabla_fila">' . mostrar_fecha($dt['tb_cuota_fec']) . '</td>
                <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                <td id="tabla_fila">' . ($credito) . '</td>
                <td id="tabla_fila">' . ($moneda) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($dt['cuota_real']) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($pago_real) . '</td>
                <td id="tabla_fila">' . mostrar_moneda($pago_extra) . '</td>
              </tr>';
          }
        $resu = NULL;
      }
      $tablas .= '
        <tr id="tabla_cabecera_fila" style="height:25px;">
          <td id="tabla_fila" colspan="10" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO VENCIDO</b></td>
          <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' .mostrar_moneda($subtotal_menor_vencidas) . '</b></td>
          <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' .mostrar_moneda($subtotal_menor_extra_vencida) . '</b></td>
      </tr>';
    }
  $dts = NULL;
  
  $RECAUDO_TOTAL_MENOR = $subtotal_menor_vigentes + $subtotal_menor_vencidas + $subtotal_menor_extra + $subtotal_menor_extra_vencida;
  $tablas .= '
    <tr id="tabla_cabecera_fila" style="height:25px;">
      <td id="tabla_fila" colspan="10" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
      <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($subtotal_menor_vigentes + $subtotal_menor_vencidas) . '</b></td>
      <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($subtotal_menor_extra + $subtotal_menor_extra_vencida) . '</b></td>
    </tr>
    <tr id="tabla_cabecera_fila" style="height:25px;">
      <td id="tabla_fila" colspan="11" style="font-family: cambria;font-weight: bold;color: #000000;background: #00c0ef"><b>RECAUDACION TOTAL DE CREDITO MENOR</b></td>
      <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #00c0ef"><b>' . mostrar_moneda($RECAUDO_TOTAL_MENOR) . '</b></td>
    </tr>';
} 

//TABLAS PARA MOSTRAR LAS LIQUIDACIONES DE LOS CRÉDITOS
elseif ($tipo == 'asistencialiquidado') {

    $liquidacion_asiveh = 0;
    $dts = $oIngreso->liquidacion_total_tipo_credito(110, $fecha1, $fecha2);
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $credito = "LIQUIDADO";
            $tb_cliente_id = ($dt['tb_cliente_id']);
            $ingreso_imp = ($dt['tb_ingreso_imp']);
            $tipo_cambio = $dt['tb_monedacambio_val'];
            $tb_ingreso_fec = $dt['tb_ingreso_fec'];
            $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

            if ($moneda_id == 2) {
                $liquidacion_asiveh += $ingreso_imp * $tipo_cambio;
                $moneda = "$$ ";
            } else {
                $liquidacion_asiveh += $ingreso_imp;
                $tipo_cambio = 1.00;
                $moneda = "S/. ";
            }
            $resu = $oCliente->mostrarUno($tb_cliente_id);
            if ($resu['estado'] == 1) {
                $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                    <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                    <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                    <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                    <td id="tabla_fila">' . ($credito) . '</td>
                    <td id="tabla_fila">' . ($moneda) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
                 </tr>';
            }
            $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                        <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO LIQUIDADO</b></td>
                        <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($liquidacion_asiveh) . '</b></td>
                    </tr>';
    }
    $dts = NULL;

    $amortizacion_asiveh = 0;
    $dts = $oIngreso->amortizacion_total_tipo_credito(106, $fecha1, $fecha2);
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $credito = "AMORTIZADO";
            $tb_cliente_id = ($dt['tb_cliente_id']);
            $ingreso_imp = ($dt['tb_ingreso_imp']);
            $tipo_cambio = $dt['tb_monedacambio_val'];
            $tb_ingreso_fec = $dt['tb_ingreso_fec'];
            $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

            if ($moneda_id == 2) {
                $amortizacion_asiveh += $ingreso_imp * $tipo_cambio;
                $moneda = "$$ ";
            } else {
                $amortizacion_asiveh += $ingreso_imp;
                $tipo_cambio = 1.00;
                $moneda = "S/. ";
            }
            $resu = $oCliente->mostrarUno($tb_cliente_id);
            if ($resu['estado'] == 1) {
                $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                    <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                    <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                    <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                    <td id="tabla_fila">' . ($credito) . '</td>
                    <td id="tabla_fila">' . ($moneda) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
                 </tr>';
            }
            $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                        <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO LIQUIDADO</b></td>
                        <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($amortizacion_asiveh) . '</b></td>
                    </tr>';
    }
    $dts = NULL;
    $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
                    <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($liquidacion_asiveh + $amortizacion_asiveh) . '</b></td>
                </tr>';
} 
elseif ($tipo == 'garantialiquidado') {
    $liquidacion_garveh = 0;
    $dts = $oIngreso->liquidacion_total_tipo_credito(111, $fecha1, $fecha2);
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $credito = "LIQUIDADO";
            $tb_cliente_id = ($dt['tb_cliente_id']);
            $ingreso_imp = ($dt['tb_ingreso_imp']);
            $tipo_cambio = $dt['tb_monedacambio_val'];
            $tb_ingreso_fec = $dt['tb_ingreso_fec'];
            $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

            if ($moneda_id == 2) {
                $liquidacion_garveh += $ingreso_imp * $tipo_cambio;
                $moneda = "$$ ";
            } else {
                $liquidacion_garveh += $ingreso_imp;
                $tipo_cambio = 1.00;
                $moneda = "S/. ";
            }
            $resu = $oCliente->mostrarUno($tb_cliente_id);
            if ($resu['estado'] == 1) {
                $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                    <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                    <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                    <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                    <td id="tabla_fila">' . ($credito) . '</td>
                    <td id="tabla_fila">' . ($moneda) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
                 </tr>';
            }
            $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                        <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO LIQUIDADO</b></td>
                        <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($liquidacion_garveh) . '</b></td>
                    </tr>';
    }
    $dts = NULL;

    $amortizacion_garveh = 0;
    $dts = $oIngreso->amortizacion_total_tipo_credito(107, $fecha1, $fecha2);
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $credito = "AMORTIZADO";
            $tb_cliente_id = ($dt['tb_cliente_id']);
            $ingreso_imp = ($dt['tb_ingreso_imp']);
            $tipo_cambio = $dt['tb_monedacambio_val'];
            $tb_ingreso_fec = $dt['tb_ingreso_fec'];
            $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

            if ($moneda_id == 2) {
                $amortizacion_garveh += $ingreso_imp * $tipo_cambio;
                $moneda = "$$ ";
            } else {
                $amortizacion_garveh += $ingreso_imp;
                $tipo_cambio = 1.00;
                $moneda = "S/. ";
            }
            $resu = $oCliente->mostrarUno($tb_cliente_id);
            if ($resu['estado'] == 1) {
                $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                    <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                    <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                    <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                    <td id="tabla_fila">' . ($credito) . '</td>
                    <td id="tabla_fila">' . ($moneda) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
                 </tr>';
            }
            $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                        <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO AMORTIZADO</b></td>
                        <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($amortizacion_garveh) . '</b></td>
                    </tr>';
    }
    $dts = NULL;
    $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
                    <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($liquidacion_garveh + $amortizacion_garveh) . '</b></td>
                </tr>';
} 
elseif ($tipo == 'hipotecarioliquidado') {
    $liquidacion_hipo = 0;
    $dts = $oIngreso->liquidacion_total_tipo_credito(112, $fecha1, $fecha2);
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $credito = "LIQUIDADO";
            $tb_cliente_id = ($dt['tb_cliente_id']);
            $ingreso_imp = ($dt['tb_ingreso_imp']);
            $tipo_cambio = $dt['tb_monedacambio_val'];
            $tb_ingreso_fec = $dt['tb_ingreso_fec'];
            $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

            if ($moneda_id == 2) {
                $liquidacion_hipo += $ingreso_imp * $tipo_cambio;
                $moneda = "$$ ";
            } else {
                $liquidacion_hipo += $ingreso_imp;
                $tipo_cambio = 1.00;
                $moneda = "S/. ";
            }
            $resu = $oCliente->mostrarUno($tb_cliente_id);
            if ($resu['estado'] == 1) {
                $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                    <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                    <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                    <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                    <td id="tabla_fila">' . ($credito) . '</td>
                    <td id="tabla_fila">' . ($moneda) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
                 </tr>';
            }
            $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                        <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO LIQUIDADO</b></td>
                        <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($liquidacion_hipo) . '</b></td>
                    </tr>';
    }
    $dts = NULL;

    $amortizacion_hipo = 0;
    $dts = $oIngreso->amortizacion_total_tipo_credito(108, $fecha1, $fecha2);
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $credito = "AMORTIZADO";
            $tb_cliente_id = ($dt['tb_cliente_id']);
            $ingreso_imp = ($dt['tb_ingreso_imp']);
            $tipo_cambio = $dt['tb_monedacambio_val'];
            $tb_ingreso_fec = $dt['tb_ingreso_fec'];
            $moneda_id = intval($dt['tb_moneda_id']); //1 soles, 2 dolares

            if ($moneda_id == 2) {
                $amortizacion_hipo += $ingreso_imp * $tipo_cambio;
                $moneda = "$$ ";
            } else {
                $amortizacion_hipo += $ingreso_imp;
                $tipo_cambio = 1.00;
                $moneda = "S/. ";
            }
            $resu = $oCliente->mostrarUno($tb_cliente_id);
            if ($resu['estado'] == 1) {
                $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila">' . $dt['tb_ingreso_id'] . '</td>
                    <td id="tabla_fila">' . $resu['data']['tb_cliente_id'] . '</td>
                    <td id="tabla_fila" align="right"><b>' . $resu['data']['tb_cliente_nom'] . '</b></td>
                    <td id="tabla_fila">' . mostrar_fecha($tb_ingreso_fec) . '</td>
                    <td id="tabla_fila">' . ($credito) . '</td>
                    <td id="tabla_fila">' . ($moneda) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($tipo_cambio) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp) . '</td>
                    <td id="tabla_fila">' . mostrar_moneda($ingreso_imp * $tipo_cambio) . '</td>
                 </tr>';
            }
            $resu = NULL;
        }
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                        <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>MONTO AMORTIZADO</b></td>
                        <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #ffff66"><b>' . mostrar_moneda($amortizacion_hipo) . '</b></td>
                    </tr>';
    }
    $dts = NULL;
        $tablas .= '<tr id="tabla_cabecera_fila" style="height:25px;">
                    <td id="tabla_fila" colspan="8" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>SUB TOTAL</b></td>
                    <td id="tabla_fila" style="font-family: cambria;font-weight: bold;color: #000000;background: #99ff99"><b>' . mostrar_moneda($liquidacion_hipo + $amortizacion_hipo) . '</b></td>
                </tr>';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_recaudo_cliente" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
      </div>
      <form id="for_recaudo">
        <input name="action_recaudo" id="action_recaudo" type="hidden" value="<?php echo $_POST['action'] ?>">
        <div class="modal-body">
          <!--<h5 style="font-family: cambria;font-weight: bold;color: #000000">SEGUROS Y UIT</h5>-->
          <div class="box box-primary">
            <div class="box-header">
              <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">Cliente de <?php echo $tipo . ' = ' . $fecha1 . ' = ' . $fecha2 ?></h4>
            </div>
            <div class="box-body">
              <table width="100%" class="table">
                <tr id="tabla_cabecera">
                  <th id="tabla_cabecera_fila">INGRESO</th>
                  <th id="tabla_cabecera_fila">ID CLIENTE</th>
                  <th id="tabla_cabecera_fila">CLIENTE</th>
                  <th id="tabla_cabecera_fila">FECHA DE CUOTA</th>
                  <th id="tabla_cabecera_fila">FECHA DE PAGO</th>
                  <th id="tabla_cabecera_fila">CUOTA</th>
                  <th id="tabla_cabecera_fila">MONEDA</th>
                  <th id="tabla_cabecera_fila">TIPO CAMBIO</th>
                  <?php if($tipo != 'menor'):?>
                    <th id="tabla_cabecera_fila">MONTO</th>
                    <th id="tabla_cabecera_fila">MONTO TOTAL</th>
                  <?php endif;?>
                  <?php if($tipo == 'menor'):?>
                    <th id="tabla_cabecera_fila">VALOR DE CUOTA</th>
                    <th id="tabla_cabecera_fila">MONTO PAGADO</th>
                    <th id="tabla_cabecera_fila">PAGO DE CUOTA REAL</th>
                    <th id="tabla_cabecera_fila">MONTO AMORTIZADO O LIQUIDADO</th>
                  <?php endif;?>
                </tr>
                <?php echo $tablas ?>
              </table>
            </div>
          </div>
          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="seguro_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_seguro">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/recaudo/recaudo_form.js?ver=11222'; ?>"></script>

