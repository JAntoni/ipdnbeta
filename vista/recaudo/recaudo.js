$(document).ready(function () {
    validarTazaCambio();
    deuda_tabla();
    recaudo_resumen_tabla();
    recaudo_tabla();
    mejoragc_tabla();

    //filtro de primera fecha
    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        //endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {

        deuda_tabla();
        recaudo_resumen_tabla();
        resumenfacturado_vigentes_tabla();

        listar_reversiones_segmentadas_tabla();
    });

    $("#datetimepicker2").on("change", function (e) {

        deuda_tabla();
        recaudo_resumen_tabla();
        resumenfacturado_vigentes_tabla();

        listar_reversiones_segmentadas_tabla();
    });

    //filtro de segunda fecha
    $('#datetimepicker11, #datetimepicker22').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        //endDate: new Date()
    });
    $("#datetimepicker11").on("change", function (e) {

        recaudo_tabla();
    });
    $("#datetimepicker22").on("change", function (e) {

        recaudo_tabla();
    });

    //filtro de tercera fecha
    $('#datetimepicker111, #datetimepicker222').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        //endDate: new Date()
    });
    $("#datetimepicker111").on("change", function (e) {
        mejoragc_tabla();
    });
    $("#datetimepicker222").on("change", function (e) {
        mejoragc_tabla();
    });

    listar_reversiones_segmentadas_tabla();
    resumenfacturado_vigentes_tabla();
});

function validarTazaCambio(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php
  if(tipo_cambio <= 0){
    alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
    return false;
  }
}

function listar_reversiones_segmentadas_tabla(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php
  var fecha01 = $('#txt_resumen_fec1').val();
  var fecha02 = $('#txt_resumen_fec2').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL+"resumenfacturado/resumenfacturado_rsegmentado_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha01: fecha01,
      fecha02: fecha02,
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function() {
      $('#load_tabla_reverssegment').show(300);
      $('#h3_titulo_revers_seg').text('FACTURADO SEGMENTADO POR CUOTAS DESDE EL' + fecha01 + ' HASTA ' + fecha02);
    },
    success: function(data){
      $('#load_tabla_reverssegment').hide(300);
      $('#box_tabla_reverssegment').html(data);
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}

function resumenfacturado_vigentes_tabla(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php
  var fecha_inicio = $('#txt_resumen_fec1').val();
  var fecha_fin = $('#txt_resumen_fec2').val();

  // if(tipo_cambio <= 0){
  //   alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
  //   return false;
  // }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"resumenfacturado/resumenfacturado_vigentes_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha_inicio: $('#txt_resumen_fec1').val(),
      fecha_fin: $('#txt_resumen_fec2').val(),
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function() {
      $('#load_tabla_vigentes').show(300);
      $('#h3_titulo_vigentes').text('FACTURADO CUOTAS VIGENTES DE FECHAS ENTRE ' + fecha_inicio + ' AL ' + fecha_fin);
    },
    success: function(data){
      $('#load_tabla_vigentes').hide(300);
      $('#box_tabla_vigentes').html(data);

      resumenfacturado_vencidas_tabla();
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}

function resumenfacturado_vencidas_tabla(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php
  var fecha_inicio = $('#txt_resumen_fec1').val();

  // if(tipo_cambio <= 0){
  //   alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
  //   return false;
  // }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"resumenfacturado/resumenfacturado_vencidas_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha_inicio: $('#txt_resumen_fec1').val(),
      fecha_fin: $('#txt_resumen_fec2').val(),
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function() {
      $('#load_tabla_vencidas').show(300);
      $('#h3_titulo_vencidas').text('FACTURADO CUOTAS VENCIDAS DE FECHAS MENORES A ' + fecha_inicio);
    },
    success: function(data){
      $('#load_tabla_vencidas').hide(300);
      $('#box_tabla_vencidas').html(data);
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}

function deuda_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deuda/deuda_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_resumen_fec1').val(),
            fec2: $('#txt_resumen_fec2').val()
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_deuda_tabla').html(html);
        },
        complete: function () {
            $('#div_deuda_tabla').removeClass("ui-state-disabled");
        }
    });
}

function recaudo_resumen_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/recaudo_resumen_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_resumen_fec1').val(),
            fec2: $('#txt_resumen_fec2').val()
        }),
        beforeSend: function () {
            $('#recaudo_mensaje_tbl').show(300);
        },
        success: function (html) {
            $('#recaudo_mensaje_tbl').hide(300);
            $('#div_recaudo_resumen_tabla').html(html);
        },
        complete: function () {
        }
    });
}

function recaudo_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/recaudo_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_fil_fec1').val(),
            fec2: $('#txt_fil_fec2').val()
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_recaudo_tabla').html(html);
        },
        complete: function () {
        }
    });
}

function recaudo_form(act, idf) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/recaudo_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            recaudo_id: idf,
            vista: 'recaudo_tabla'
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_modal_recaudo_form').html(html);
            $('#modal_registro_recaudo').modal('show');
            modal_hidden_bs_modal('modal_registro_recaudo', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_height_auto('modal_registro_recaudo');
        }
    });
}

function recaudo_form_cliente(action) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/recaudo_cliente_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            fecha1: $("#txt_resumen_fec1").val(),
            fecha2: $("#txt_resumen_fec2").val()
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_modal_recaudo_cliente_form').html(html);
            $('#modal_registro_recaudo_cliente').modal('show');
            modal_hidden_bs_modal('modal_registro_recaudo_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_height_auto('modal_registro_recaudo_cliente');
            modal_width_auto('modal_registro_recaudo_cliente', 60);
        }
    });
}

function eliminar_recaudo(recaudo_id) {
    if (confirm("Realmente desea eliminar?")) {
        $.ajax({
            type: "POST",
            url: VISTA_URL + "recaudorecaudo/recaudo_reg.php",
            async: true,
            dataType: "html",
            data: ({
                action: "eliminar",
                recaudo_id: recaudo_id
            }),
            beforeSend: function () {
                $('#msj_recaudo').html("Cargando...");
                $('#msj_recaudo').show(100);
            },
            success: function (html) {
                $('#msj_recaudo').html(html);
            },
            complete: function () {
                recaudo_tabla();
            }
        });
    }
}

function eliminar_recaudo(recaudo_id) {
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿Está seguro de Eliminar?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "recaudo/recaudo_reg.php",
                    async: true,
                    dataType: "html",
                    data: ({
                        action: "eliminar",
                        recaudo_id: recaudo_id
                    }),
                    beforeSend: function () {
                    },
                    success: function (html) {
                        swal_success("SISTEMA", html, 2500);
                    },
                    complete: function () {
                        recaudo_tabla();
                    }
                });
            },
            no: function () {}
        }
    });
}

//--------------------------------FUNCIONES DE MEJORA DE GC ------------------------
function mejoragc_form(act, idf){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/mejoragc_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            mejoragc_id: idf,
            vista: 'mejoragc_tabla'
        }),
        beforeSend: function () {
            $('#msj_mejoragc').hide();
        },
        success: function (html) {
            $('#div_mejoragc_form').html(html);
            $('#modal_registro_mejoragc').modal('show');
            modal_hidden_bs_modal('modal_registro_mejoragc', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_height_auto('modal_registro_mejoragc');
        }
    });
}

function mejoragc_tabla(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "recaudo/mejoragc_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_filgc_fec1').val(),
            fec2: $('#txt_filgc_fec2').val()
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_mejoragc_tabla').html(html);
        },
        complete: function () {
        }
    });
}


function eliminar_mejoragc(mejoragc_id) {
  $.confirm({
    icon: 'fa fa-print',
    title: 'Imprimir',
    content: '¿Está seguro de Eliminar?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function () {
        //ejecutamos AJAX
        $.ajax({
          type: "POST",
          url: VISTA_URL + "recaudo/mejoragc_reg.php",
          async: true,
          dataType: "html",
          data: ({
              action: "eliminar",
              mejoragc_id: mejoragc_id
          }),
          beforeSend: function () {

          },
          success: function (html) {
            swal_success("SISTEMA", html, 2500);
          },
          complete: function () {
              mejoragc_tabla();
          }
        });
      },
      no: function () {}
    }
  });
}