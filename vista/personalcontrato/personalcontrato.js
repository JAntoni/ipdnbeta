function personalcontrato_perfil() {
  var usuario_id = $("#personal_usuario_id").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "personalcontrato/personalcontrato_perfil.php",
    async: true,
    dataType: "html",
    data: {
      usuario_id: usuario_id,
    },
    beforeSend: function () {},
    success: function (data) {
      $(".perfil_personal").html(data);
      personalcontrato_tabla();
    },
    complete: function (data) {},
  });
}

function personalcontrato_tabla() {
  var usuario_id = $("#personal_usuario_id").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "personalcontrato/personalcontrato_tabla.php",
    async: true,
    dataType: "html",
    data: {
      usuario_id: usuario_id,
    },
    beforeSend: function () {},
    success: function (data) {
      $(".tablas_personal").html(data);
    },
    complete: function (data) {},
  });
}

function personalcontrato_form(usuario_act, personalcontrato_id) {
  var usuario_id = $('#personal_usuario_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "personalcontrato/personalcontrato_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act,
      personalcontrato_id: personalcontrato_id,
      usuario_id: usuario_id
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_registro_personalcontrato").html(data);
        $("#modal_registro_personalcontrato").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_personalcontrato"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_personalcontrato", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        // modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        // //funcion js para agregar un ancho automatico al modal, al abrirlo
        // modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

function vacaciones_form(usuario_act, vacaciones_id) {
  var usuario_id = $('#personal_usuario_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "vacaciones/vacaciones_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act,
      vacaciones_id: vacaciones_id,
      usuario_id: usuario_id,
      vista: 'personalcontrato'
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_modal_vacaciones_form").html(data);
        $("#modal_registro_vacaciones").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_vacaciones"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_vacaciones", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        // modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        // //funcion js para agregar un ancho automatico al modal, al abrirlo
        // modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

function bono_form(usuario_act, bono_id) {
  var usuario_id = $('#personal_usuario_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "bono/bono_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act,
      bono_id: bono_id,
      usuario_id: usuario_id,
      vista: 'personalcontrato'
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_modal_bono_form").html(data);
        $("#modal_registro_bono").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_bono"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_bono", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        // modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        // //funcion js para agregar un ancho automatico al modal, al abrirlo
        // modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

function upload_formpdf(usuario_act, personalcontrato_id) {
  upload_uniq = $('#hdd_upload_uniq').val();
  
  $.ajax({
    type: "POST",
    url: VISTA_URL + "uploadpdf/upload_form.php",
    async: true,
    dataType: "html",
    data: ({
        action: usuario_act, // PUEDE SER: L, I, M , E
        upload_id: 0,
        upload_uniq: upload_uniq,
        modulo_nom: 'personalcontrato', //nombre de la tabla a relacionar
        modulo_id: personalcontrato_id
    }),
    beforeSend: function () {
        $('#h3_modal_title').text('Cargando Formulario');
        $('#modal_mensaje').modal('show');
    },
    success: function (data) {
      $('#modal_mensaje').modal('hide');
      if (data != 'sin_datos') {
        $('#div_modal_uploadpdf_form').html(data);
        $('#modal_registro_upload').modal('show');

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {

    },
    error: function (data) {
      alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
    }
  });
}

function descuento_form(usuario_act, descuento_id) {
  var usuario_id = $('#personal_usuario_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "descuento/descuento_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act,
      descuento_id: descuento_id,
      usuario_id: usuario_id,
      vista: 'personalcontrato'
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_descuento_form").html(data);
        $("#modal_registro_descuento").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_vacaciones"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_descuento", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

$(document).ready(function () {
  console.log("Mejorado corre 4444 ");

  $("#datetimepicker1, #datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $("#txt_deudadiaria_fec1").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $("#txt_deudadiaria_fec2").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
  });

  $("#personal_usuario_id").change(function () {
    personalcontrato_perfil();
  });
});
