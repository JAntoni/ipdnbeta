<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class Personalcontrato extends Conexion
{

  public $personalcontrato_id;
  public $personalcontrato_tipo;
  public $usuario_id;
  public $personalcontrato_fecini;
  public $personalcontrato_fecfin;
  public $personalcontrato_tiptur;
  public $seguro_id;
  public $personalcontrato_numcuenta;
  public $personalcontrato_horas;
  public $personalcontrato_ing1;
  public $personalcontrato_sal1;
  public $personalcontrato_ing2;
  public $personalcontrato_sal2;
  public $cargo_id;
  public $personalcontrato_sueldo;
  public $personalcontrato_asigfamilia;
  public $personalcontrato_inde;
  public $personalcontrato_des;

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $columns = [
        'tb_personalcontrato_tipo',
        'tb_usuario_id',
        'tb_personalcontrato_fecini',
        'tb_personalcontrato_fecfin',
        'tb_personalcontrato_tiptur',
        'tb_seguro_id',
        'tb_personalcontrato_numcuenta',
        'tb_personalcontrato_horas',
        'tb_personalcontrato_ing1',
        'tb_personalcontrato_sal1',
        'tb_personalcontrato_ing2',
        'tb_personalcontrato_sal2',
        'tb_cargo_id',
        'tb_personalcontrato_sueldo',
        'tb_personalcontrato_asigfamilia',
        'tb_personalcontrato_inde',
        'tb_personalcontrato_des'
      ];

      // Lista de placeholders
      $placeholders = implode(',', array_map(function ($column) {
        return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_personalcontrato (" . implode(',', $columns) . ") VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_personalcontrato_tipo", $this->personalcontrato_tipo, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_fecini", $this->personalcontrato_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_fecfin", $this->personalcontrato_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_tiptur", $this->personalcontrato_tiptur, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_seguro_id", $this->seguro_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_numcuenta", $this->personalcontrato_numcuenta, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_horas", $this->personalcontrato_horas, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_ing1", $this->personalcontrato_ing1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_sal1", $this->personalcontrato_sal1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_ing2", $this->personalcontrato_ing2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_sal2", $this->personalcontrato_sal2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_cargo_id", $this->cargo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_sueldo", $this->personalcontrato_sueldo, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_asigfamilia", $this->personalcontrato_asigfamilia, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_inde", $this->personalcontrato_inde, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_des", $this->personalcontrato_des, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_personalcontrato SET 
            tb_personalcontrato_tipo = :tb_personalcontrato_tipo,
            tb_personalcontrato_fecini = :tb_personalcontrato_fecini,
            tb_personalcontrato_fecfin = :tb_personalcontrato_fecfin,
            tb_personalcontrato_tiptur = :tb_personalcontrato_tiptur,
            tb_seguro_id = :tb_seguro_id,
            tb_personalcontrato_numcuenta = :tb_personalcontrato_numcuenta,
            tb_personalcontrato_horas = :tb_personalcontrato_horas,
            tb_personalcontrato_ing1 = :tb_personalcontrato_ing1,
            tb_personalcontrato_sal1 = :tb_personalcontrato_sal1,
            tb_personalcontrato_ing2 = :tb_personalcontrato_ing2,
            tb_personalcontrato_sal2 = :tb_personalcontrato_sal2,
            tb_cargo_id = :tb_cargo_id,
            tb_personalcontrato_sueldo = :tb_personalcontrato_sueldo,
            tb_personalcontrato_asigfamilia = :tb_personalcontrato_asigfamilia,
            tb_personalcontrato_inde = :tb_personalcontrato_inde,
            tb_personalcontrato_des = :tb_personalcontrato_des 
            WHERE tb_personalcontrato_id = :tb_personalcontrato_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_personalcontrato_tipo", $this->personalcontrato_tipo, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_fecini", $this->personalcontrato_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_fecfin", $this->personalcontrato_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_tiptur", $this->personalcontrato_tiptur, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_seguro_id", $this->seguro_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_numcuenta", $this->personalcontrato_numcuenta, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_horas", $this->personalcontrato_horas, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_ing1", $this->personalcontrato_ing1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_sal1", $this->personalcontrato_sal1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_ing2", $this->personalcontrato_ing2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_sal2", $this->personalcontrato_sal2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_cargo_id", $this->cargo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_sueldo", $this->personalcontrato_sueldo, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_asigfamilia", $this->personalcontrato_asigfamilia, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_inde", $this->personalcontrato_inde, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_personalcontrato_des", $this->personalcontrato_des, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_personalcontrato_id", $this->personalcontrato_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function eliminar($personalcontrato_id)
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_personalcontrato SET tb_personalcontrato_xac = 0 WHERE tb_personalcontrato_id = :tb_personalcontrato_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_personalcontrato_id", $personalcontrato_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function mostrarUno($personalcontrato_id)
  {
    try {
      $sql = "SELECT * FROM tb_personalcontrato WHERE tb_personalcontrato_id =:personalcontrato_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":personalcontrato_id", $personalcontrato_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_contratos($usuario_id)
  {
    try {
      $sql = "SELECT * FROM tb_personalcontrato per
        LEFT JOIN tb_cargo car ON car.tb_cargo_id = per.tb_cargo_id 
        LEFT JOIN tb_seguro seg ON seg.tb_seguro_id = per.tb_seguro_id
        WHERE tb_usuario_id =:usuario_id AND tb_personalcontrato_xac = 1 ORDER BY tb_personalcontrato_fecini DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_uno_condicion($usuario_id, $primero_ultimo, $tipos)
  {
    try {
      $ordenar = 'DESC LIMIT 1'; //por defecto obtenemos el ultimo registrado
      $filtro_tipo = '';

      if ($tipos != '')
        $filtro_tipo = 'AND tb_personalcontrato_tipo IN (' . $tipos . ')';
      if ($primero_ultimo == 'primero')
        $ordenar = 'ASC LIMIT 1';

      $sql = "SELECT * FROM tb_personalcontrato per 
          INNER JOIN tb_usuario usu ON usu.tb_usuario_id = per.tb_usuario_id 
          INNER JOIN tb_empresa emp ON emp.tb_empresa_id = usu.tb_empresa_id 
          INNER JOIN tb_usuarioperfil perfil ON perfil.tb_usuarioperfil_id = usu.tb_usuarioperfil_id 
          INNER JOIN tb_usuariogrupo grupo ON grupo.tb_usuariogrupo_id = usu.tb_usuariogrupo_id 
          LEFT JOIN tb_cargo car ON car.tb_cargo_id = per.tb_cargo_id 
          LEFT JOIN tb_seguro seg ON seg.tb_seguro_id = per.tb_seguro_id
          WHERE per.tb_usuario_id =:usuario_id AND tb_personalcontrato_xac = 1 " . $filtro_tipo . " ORDER BY tb_personalcontrato_fecini " . $ordenar;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
      echo $sql;
    }
  }

  function mostrar_vigencia_personal($usuario_id, $fecha_vigente)
  {
    try {

      $sql = "SELECT * FROM tb_personalcontrato per 
          INNER JOIN tb_usuario usu ON usu.tb_usuario_id = per.tb_usuario_id 
          INNER JOIN tb_empresa emp ON emp.tb_empresa_id = usu.tb_empresa_id 
          LEFT JOIN tb_cargo car ON car.tb_cargo_id = per.tb_cargo_id 
          LEFT JOIN tb_seguro seg ON seg.tb_seguro_id = per.tb_seguro_id
          WHERE per.tb_usuario_id =:usuario_id AND tb_personalcontrato_tipo IN(1,2,3) 
          AND :fecha_vigente BETWEEN DATE_FORMAT(tb_personalcontrato_fecini, '%Y-%m-01') AND tb_personalcontrato_fecfin AND tb_personalcontrato_xac = 1 
          ORDER BY tb_personalcontrato_id DESC LIMIT 1;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha_vigente", $fecha_vigente, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
      echo $sql;
    }
  }

  function modificar_campo($personalcontrato_id, $personalcontrato_columna, $personalcontrato_valor, $param_tip){
    $this->dblink->beginTransaction();
    try {
      if (!empty($personalcontrato_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_personalcontrato SET " . $personalcontrato_columna . " =:personalcontrato_valor WHERE tb_personalcontrato_id =:personalcontrato_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":personalcontrato_id", $personalcontrato_id, PDO::PARAM_INT);
        if ($param_tip == 'INT') {
          $sentencia->bindParam(":personalcontrato_valor", $personalcontrato_valor, PDO::PARAM_INT);
        } else {
          $sentencia->bindParam(":personalcontrato_valor", $personalcontrato_valor, PDO::PARAM_STR);
        }

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
}
