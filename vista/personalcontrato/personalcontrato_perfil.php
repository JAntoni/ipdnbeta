<?php
require_once('../../core/usuario_sesion.php');

require_once('../personalcontrato/Personalcontrato.class.php');
$oPersonal = new Personalcontrato();
require_once('../principal/Chart.class.php');
$oChart = new Chart();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$personal_usuario_id = $_POST['usuario_id'];

$tabla_contratos = '';
//? tipos: 1->Inicio Labores, 2->Extensión de Contrato, 3->Ascenso del Colaborador, 4->Finalización de Contrato, 5->Renuncia Voluntaria, 6->Despido por la Empresa
$usuario_nom = 'Sin Nombre Aun';
$cargo_nom = 'Sin Cargo Aun';
$usuario_foto = 'public/images/avatar.png';

$result = $oPersonal->mostrar_uno_condicion($personal_usuario_id, 'primero', ''); //? obtener el primer registro de contratos sin especificar el tipo
  //echo '<h3> // ----- '.$result['mensaje'].'</h3>';
  if($result['estado'] == 1){
    $usuario_nom = ucwords(strtolower($result['data']['tb_usuario_nom'].' '.$result['data']['tb_usuario_ape']));
    $empresa_nom = $result['data']['tb_empresa_nomcom'];
    $inicio_labores = fechaActual($result['data']['tb_personalcontrato_fecini']);
    $usuario_tel = $result['data']['tb_usuario_tel'];
    $personalcontrato_tiptur = intval($result['data']['tb_personalcontrato_tiptur']);
    $usuarioperfil_nom = $result['data']['tb_usuarioperfil_nom'];
    $usuariogrupo_nom = $result['data']['tb_usuariogrupo_nom'];
    $usuario_foto = $result['data']['tb_usuario_fot'];

    $tipo_turno = 'REVISAR SIN TURNO';
    if($personalcontrato_tiptur == 1)
      $tipo_turno = 'Full Time';
    if($personalcontrato_tiptur == 2)
      $tipo_turno = 'Part Time - Mañana';
    if($personalcontrato_tiptur == 3)
      $tipo_turno = 'Part Time - Tarde';
  }
$result = NULL;

$result = $oPersonal->mostrar_uno_condicion($personal_usuario_id, 'ultimo', '1,2,3'); //? obtener el ULTIMO registro indicando que sea de inicio, extension o ascenso
  //echo '<h3> // ----- '.$result['mensaje'].'</h3>';
  if($result['estado'] == 1){
    $cargo_nom = $result['data']['tb_cargo_nom'];
  }
$result = NULL;

//? VAMOS A OBTENER EL MONTO DE COLOCACIONES DE CADA TIPO DE SERVICIO QUE SE OFRECE EN LA EMPRESA
$fecha_inicio = '2012-01-01'; // consultamos todo el tiempo de la empresa
$fecha_fin = date('Y-m-d'); //colocaciones hasta la fecha actual

$total_logrado_creditos = 0;
$result = $oChart->listar_creditos_asiveh_garveh_hipo($fecha_inicio, $fecha_fin, $personal_usuario_id, 0); //EMPRESA = 0, consultamos de todas las sedes
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $total_logrado_creditos += $value['prestamo_soles'];
    }
  }
$result = NULL;

//? ________________________________________________________________________________________________________________________________
$total_logrado_cmenor = 0;
$result = $oChart->listar_cmenor_colocacion($fecha_inicio, $fecha_fin, $personal_usuario_id, 0);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $total_logrado_cmenor += $value['tb_garantia_val'];
    }
  }
$result = NULL;

//? ________________________________________________________________________________________________________________________________
$total_logrado_ventacm = 0;
$result = $oChart->listar_reporte_venta_garantias(0, $fecha_inicio, $fecha_fin, $personal_usuario_id);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $total_logrado_ventacm += $value['total_vendido'];
    }
  }
$result = NULL;

//* VAMOS A OBTENER LA VIGENCIA DEL COLABORADOR, SI SIGUE TRABAJANDO O NO
$fecha_finaliza = 'Ya finalizó sus labores';
$estado_personal = 'INACTIVO';

$result = $oPersonal->mostrar_vigencia_personal($personal_usuario_id, $fecha_fin);
  if($result['estado'] == 1){
    $fecha_finaliza = 'Finaliza el: <b>'.fechaActual($result['data']['tb_personalcontrato_fecfin']).'</b>';
    $estado_personal = 'VIGENTE';
  }
$result = NULL;
?>
<div class="box box-widget widget-user shadow">
  <div class="widget-user-header bg-aqua-active">
    <h3 class="widget-user-username"><?php echo $usuario_nom;?></h3>
    <h5 class="widget-user-desc"><?php echo '<b>'.$cargo_nom.'</b> | Estado del Colaborador: <b>'.$estado_personal.'</b> | '.$fecha_finaliza?></h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="<?php echo $usuario_foto;?>" alt="Foto Colaborador">
  </div>
  <div class="box-footer">
    <div class="row">
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header"><?php echo 'S/. '.mostrar_moneda($total_logrado_cmenor);?></h5>
          <span class="description-text">Creditos Menores</span>
        </div>
      </div>
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header"><?php echo 'S/. '.mostrar_moneda($total_logrado_creditos);?></h5>
          <span class="description-text">Creditos Vehiculares | Hipotecas</span>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="description-block">
          <h5 class="description-header"><?php echo 'S/. '.mostrar_moneda($total_logrado_ventacm);?></h5>
          <span class="description-text">Venta de Remates</span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="panel-body shadow" style="margin-top: 15px;">
  <div class="row">
    <div class="col-md-4"><h4 class=""><b>Inicio Labores:</b> <?php echo $inicio_labores;?></h4></div>
    <div class="col-md-4"><h4 class=""><b>Sede de Labores:</b> <?php echo $empresa_nom;?></h4></div>
    <div class="col-md-4"><h4 class=""><b>Telefono:</b> <?php echo $usuario_tel;?></h4></div>
  </div>
  <div class="row">
    <div class="col-md-4"><h4 class=""><b>Tipo Jornada:</b> <?php echo $tipo_turno;?></h4></div>
    <div class="col-md-4"><h4 class=""><b>Perfil Usuario:</b> <?php echo $usuarioperfil_nom;?></h4></div>
    <div class="col-md-4"><h4 class=""><b>Grupo Usuario:</b> <?php echo $usuariogrupo_nom;?></h4></div>
  </div>
</div>

<div class="panel-body shadow" style="margin-top: 15px;">
  <div class="row">
    <div class="col-md-3"><button type="button" class="btn btn-block btn-info" onclick="personalcontrato_form('I', 0)">Nuevo Contrato</button></div>
    <div class="col-md-3"><button type="button" class="btn btn-block btn-success" onclick="vacaciones_form('I', 0)">Vacaciones</button></div>
    <div class="col-md-3"><button type="button" class="btn btn-block btn-warning" onclick="bono_form('insertar', 0)">Bonos</button></div>
    <div class="col-md-3"><button type="button" class="btn btn-block btn-danger" onclick="descuento_form('I', 0)">Descuentos y Obligaciones</button></div>
  </div>
</div>