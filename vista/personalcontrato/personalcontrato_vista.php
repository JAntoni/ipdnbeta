<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<div class="panel panel-info">
					<div class="panel-body">
						<select class="form-control form-control-sm selectpicker" id="personal_usuario_id" data-live-search="true" data-max-options="1" data-size="12">
							<?php require_once(VISTA_URL.'usuario/usuario_select.php');?>
						</select>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="personalcontrato_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>
				
				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">FICHA DEL COLABORADOR</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>

					<div class="box-body">
						<div class="perfil_personal">
							<div class="alert alert-info alert-dismissible">
								<h4><i class="icon fa fa-info"></i> Importante</h4>
								Selecciona un usuario para poder ver la ficha y gestionar las opciones disponibles.
							</div>
						</div>

						<div class="tablas_personal">

						</div>
						
					</div>
				</div>	
				
				<div id="div_registro_personalcontrato">				
				</div>

				<div id="div_modal_vacaciones_form">
				</div>
				
				<div id="div_modal_bono_form">
				</div>

				<div id="div_modal_uploadpdf_form"></div>

				<div id="div_descuento_form"></div>
			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
