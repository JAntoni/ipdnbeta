$(document).ready(function () {
  console.log('aaa 2222')

  $('#datetimepicker1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //endDate : new Date()
  });

  $('#datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //endDate : new Date()
  });

  $('#cmb_personalcontrato_tipo').change(function(){
    var tipo = parseInt($(this).val());

    if(tipo == 1 || tipo == 2 || tipo == 3)
      $('.personal_ocultar').show(300);
    else
      $('.personal_ocultar').hide(300);
  });
  $('#cmb_personalcontrato_inde').change(function(){
    var inde = parseInt($(this).val());

    if(inde == 1)
      $('.fecha_finaliza').hide(300);
    else
      $('.fecha_finaliza').show(300);
  });
  

  $('#form_personalcontrato').validate({
    submitHandler: function () {

      $.ajax({
        type: "POST",
        url: VISTA_URL + "personalcontrato/personalcontrato_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_personalcontrato").serialize(),
        beforeSend: function () {
          
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {

            notificacion_success(data.mensaje, 6000);
            swal_success("CONTRATO", data.mensaje, 2500);
            $('#modal_registro_personalcontrato').modal('hide');
            personalcontrato_tabla();
          } 
          else {
            alerta_error('Error', data.mensaje);
            console.log(data)
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      cmb_personalcontrato_tipo: {
        required: true
      },
      txt_personalcontrato_fecini: {
        required: true
      },
      cmb_personalcontrato_tiptur: {
        required: function(){
          if(parseInt($('#cmb_personalcontrato_tipo').val()) == 1 || parseInt($('#cmb_personalcontrato_tipo').val()) == 2 || parseInt($('#cmb_personalcontrato_tipo').val()) == 3)
            return true;
          else
            return false;
        }
      },
      cmb_seguro_id: {
        required: function(){
          if(parseInt($('#cmb_personalcontrato_tipo').val()) == 1 || parseInt($('#cmb_personalcontrato_tipo').val()) == 2 || parseInt($('#cmb_personalcontrato_tipo').val()) == 3)
            return true;
          else
            return false;
        }
      },
      txt_personalcontrato_horas: {
        required: function(){
          if(parseInt($('#cmb_personalcontrato_tipo').val()) == 1 || parseInt($('#cmb_personalcontrato_tipo').val()) == 2 || parseInt($('#cmb_personalcontrato_tipo').val()) == 3)
            return true;
          else
            return false;
        }
      },
      cmb_cargo_id: {
        required: function(){
          if(parseInt($('#cmb_personalcontrato_tipo').val()) == 1 || parseInt($('#cmb_personalcontrato_tipo').val()) == 2 || parseInt($('#cmb_personalcontrato_tipo').val()) == 3)
            return true;
          else
            return false;
        }
      },
      txt_personalcontrato_sueldo: {
        required: function(){
          if(parseInt($('#cmb_personalcontrato_tipo').val()) == 1 || parseInt($('#cmb_personalcontrato_tipo').val()) == 2 || parseInt($('#cmb_personalcontrato_tipo').val()) == 3)
            return true;
          else
            return false;
        }
      },
      txt_personalcontrato_des: {
        required: true,
        minlength: 5
      }
    },
    messages: {
      cmb_personalcontrato_tipo: {
        required: "Selecciona un tipo para este Contrato"
      },
      txt_personalcontrato_fecini: {
        required: "Ingresa una fecha válida"
      },
      cmb_personalcontrato_tiptur: {
        required: "Selecciona un tipo de jornada"
      },
      cmb_seguro_id: {
        required: "Selecciona un tipo de aportación de ley"
      },
      txt_personalcontrato_horas: {
        required: "Ingresa las horas a laborar de manera mensual"
      },
      cmb_cargo_id: {
        required: "Selecciona el cargo del colaborador"
      },
      txt_personalcontrato_sueldo: {
        required: "Ingresa el sueldo base al colaborador"
      },
      txt_personalcontrato_des: {
        required: "Breve descripción",
        minlength: "Breve descripción"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });
});