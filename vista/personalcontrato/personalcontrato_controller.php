<?php
require_once('../../core/usuario_sesion.php');

require_once('../personalcontrato/Personalcontrato.class.php');
$oPersonal = new Personalcontrato();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once('../funciones/fechas.php');

$action = $_POST['action'];

$oPersonal->personalcontrato_id = intval($_POST['hdd_personalcontrato_id']); 
$oPersonal->personalcontrato_tipo = intval($_POST['cmb_personalcontrato_tipo']); 
$oPersonal->usuario_id = intval($_POST['hdd_usuario_id']);  
$oPersonal->personalcontrato_fecini = fecha_mysql($_POST['txt_personalcontrato_fecini']);
$oPersonal->personalcontrato_tiptur = intval($_POST['cmb_personalcontrato_tiptur']); 
$oPersonal->seguro_id = intval($_POST['cmb_seguro_id']); 
$oPersonal->personalcontrato_numcuenta = $_POST['txt_personalcontrato_numcuenta']; 
$oPersonal->personalcontrato_horas = intval($_POST['txt_personalcontrato_horas']); 
$oPersonal->personalcontrato_ing1 = $_POST['txt_personalcontrato_ing1'];
$oPersonal->personalcontrato_sal1 = $_POST['txt_personalcontrato_sal1'];;
$oPersonal->personalcontrato_ing2 = $_POST['txt_personalcontrato_ing2'];;
$oPersonal->personalcontrato_sal2 = $_POST['txt_personalcontrato_sal2'];;
$oPersonal->cargo_id = intval($_POST['cmb_cargo_id']); 
$oPersonal->personalcontrato_sueldo = $_POST['txt_personalcontrato_sueldo']; 
$oPersonal->personalcontrato_asigfamilia = $_POST['txt_personalcontrato_asigfamilia'];
$oPersonal->personalcontrato_inde = intval($_POST['cmb_personalcontrato_inde']); 
$oPersonal->personalcontrato_des = $_POST['txt_personalcontrato_des'];

if($_POST['txt_personalcontrato_fecfin'] == '')
  $oPersonal->personalcontrato_fecfin = fecha_mysql($_POST['txt_personalcontrato_fecini']); // si no tiene fecha de fin, le damos la misma fecha de inicio
if(intval($_POST['cmb_personalcontrato_inde']) == 1) 
  $oPersonal->personalcontrato_fecfin = date('2030-12-31'); // si es indefinido, le damos una fecha máxima
if($_POST['txt_personalcontrato_fecfin'] != '')
  $oPersonal->personalcontrato_fecfin = fecha_mysql($_POST['txt_personalcontrato_fecfin']); // le otorgamos su fecha tal cual

if ($action == 'insertar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El contrato ha sido registrado correctamente';

  //? 1. Antes de realizar un insert, vamos a verificar si se está culminando el contrato del colaborador, mediante el tipo
  if(intval($_POST['cmb_personalcontrato_tipo']) > 3){ //* quiere decir que no es un ascenso ni extensión de contrato, es una finalizacion
    //? 2. Obtenemos el ultimo ascenso o extención de contrato
    $result = $oPersonal->mostrar_uno_condicion(intval($_POST['hdd_usuario_id']), 'ultimo', '1,2,3'); //? obtener el ULTIMO registro indicando que sea de inicio, extension o ascenso
      if($result['estado'] == 1){
        $personalcontrato_id = $result['data']['tb_personalcontrato_id'];
        //? 3. finalizamos la fecha de contrato a la ultima extensión o ascenso del colaborador con la fecha de este fin de contrato
        $oPersonal->modificar_campo($personalcontrato_id, 'tb_personalcontrato_fecfin', fecha_mysql($_POST['txt_personalcontrato_fecini']), 'STR');
        $data['mensaje'] .= ' / Se ha modificado la fecha fin del contrato anterior';
      }
    $result = NULL;

    //? 4. Vamos a actualizar sus permisos de usuario para el acceso al sistema, los cuales serían dos modificaciones: cambia a tb_usuario_estado = 1 INACTIVO, tb_usuario_ofi = 0
    $oUsuario->modificar_campo(intval($_POST['hdd_usuario_id']), 'tb_usuario_estado', 1, 'INT'); // estado 1 INACTIVO, 0 ACTIVO
    $oUsuario->modificar_campo(intval($_POST['hdd_usuario_id']), 'tb_usuario_ofi', 0, 'INT'); // 0 no trabaja en oficina, ya no genera calendario
  }

  $oPersonal->insertar();

  echo json_encode($data);
}
elseif ($action == 'modificar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El contrato ha sido Modificado correctamente';

  $oPersonal->modificar();

  echo json_encode($data);
}
elseif ($action == 'eliminar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El contrato ha sido ELIMINADO correctamente';

  $oPersonal->eliminar($oPersonal->personalcontrato_id);

  echo json_encode($data);
}
elseif ($action == 'contratos_vencidos') {

  $fecha_actual = date('Y-m-d');
  $por_vencer = array();
  $vencidos = array();

  //* VAMOS A LISTAR TODOS LOS USUARIOS QUE TRABAJAN EN OFICINA, DE ELLOS VAMOS A OBTENER SU ULTIMO CONTRATO ACTUALIZADO
  $result = $oUsuario->listar_trabajadores_oficina(); // lista los usuario en tb_usuario_ofi = 1
    if($result['estado'] == 1){
      $contador_x = 0;
      $contador_y = 0;
      foreach ($result['data'] as $key => $value) {
        $colaborador = $value['tb_usuario_nom'].' '.$value['tb_usuario_ape'];

        //* VAMOS A OBTENER SU ULTIMO CONTRATO ACTUALIZADO
        $result2 = $oPersonal->mostrar_uno_condicion($value['tb_usuario_id'], 'ultimo', '1,2,3'); //* obtener el ULTIMO registro indicando que sea de inicio, extension o ascenso
          if($result2['estado'] == 1){
            $fecha_fin_contrato = $result2['data']['tb_personalcontrato_fecfin'];

            //? ESTA CONDICION INDICA QUE EL CONTRATO HA VENCIDO
            if(strtotime($fecha_actual) > strtotime($fecha_fin_contrato)){
              $dias_vencido = restaFechas($fecha_fin_contrato, $fecha_actual); // actual - fin contrato devuelve días

              $v2['id'] = $value['tb_usuario_id'];
              $v2['colaborador'] = $colaborador;
              $v2['fin_contrato'] = $fecha_fin_contrato;
              $v2['vencimiento'] = $dias_vencido;
              $vencidos[$contador_x] = $v2;
              $contador_x ++;
            }
            //? ESTA CONDICIÓN INDCA QUE EL CONTRATO AUN NO VENCE, VEAMOS SI TIENE 5 DÍAS O MENOS POR VENCER
            if(strtotime($fecha_actual) <= strtotime($fecha_fin_contrato)){
              $dias_por_vencer = restaFechas($fecha_actual, $fecha_fin_contrato); // fin contrato - fecha actual

              if($dias_por_vencer <= 5){
                $v1['id'] = $value['tb_usuario_id'];
                $v1['colaborador'] = $colaborador;
                $v1['fin_contrato'] = $fecha_fin_contrato;
                $v1['vencimiento'] = $dias_por_vencer;
                $por_vencer[$contador_y] = $v1;
                $contador_y ++;
              }
            }
          }
        $result2 = NULL;
      }
    }
  $result = NULL;

  $data['estado'] = 1;
  $data['por_vencer'] = $por_vencer;
  $data['vencidos'] = $vencidos;
  //$data['mensaje'] = 'Vacaciones Registradas correctamente';

  echo json_encode($data);
}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
?>