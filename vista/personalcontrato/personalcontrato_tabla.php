<?php
require_once('../../core/usuario_sesion.php');

require_once('../personalcontrato/Personalcontrato.class.php');
$oPersonal = new Personalcontrato();
require_once('../upload/Upload.class.php');
$oUpload = new Upload();
require_once('../funciones/fechas.php');

$personal_usuario_id = $_POST['usuario_id'];

$tabla_contratos = '';
$result = $oPersonal->listar_contratos($personal_usuario_id);
  if($result['estado'] == 1){
    $tipo_contrato = ['Inicio Labores', 'Extensión de Contrato', 'Ascenso del Colaborador', 'Finalización de Contrato', 'Renuncia Voluntaria', 'Despido por la Empresa'];
    foreach ($result['data'] as $key => $value) {
      $tipo_numero = intval($value['tb_personalcontrato_tipo']);
      $contrato_nombre = '';
      if($tipo_numero > 0)
        $contrato_nombre = $tipo_contrato[$tipo_numero - 1];

      $tipo_jornada = '';
      if($value['tb_personalcontrato_tiptur'] == 1)
        $tipo_jornada = 'Full Time';
      if($value['tb_personalcontrato_tiptur'] == 2)
        $tipo_jornada = 'Part Time Mañana';
      if($value['tb_personalcontrato_tiptur'] == 3)
        $tipo_jornada = 'Part Time Tarde';

      //? buscamos los contratos subidos para este detalle
      $lista_contratos = '';
      $result2 = $oUpload->listar_uploads('personalcontrato', $value['tb_personalcontrato_id']);
        if($result2['estado'] == 1){
          foreach ($result2['data'] as $key => $value2) {
            $lista_contratos .= '<a class="btn btn-danger btn-xs" href="'.$value2['upload_url'].'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
          }
        }
      $result2 = NULL;

      $tabla_contratos .= '
        <tr>
          <td>'.$value['tb_personalcontrato_id'].'</td>
          <td>'.mostrar_fecha($value['tb_personalcontrato_fecini']).'</td>
          <td>'.mostrar_fecha($value['tb_personalcontrato_fecfin']).'</td>
          <td>'.$value['tb_cargo_nom'].'</td>
          <td>'.$contrato_nombre.'</td>
          <td>'.$value['tb_personalcontrato_sueldo'].'</td>
          <td>'.$tipo_jornada.'</td>
          <td>'.$value['tb_personalcontrato_horas'].'</td>
          <td>'.$value['tb_seguro_nom'].'</td>
          <td>'.$value['tb_personalcontrato_asigfamilia'].'</td>
          <td>'.$value['tb_personalcontrato_des'].'</td>
          <td>'.$lista_contratos.'</td>
          <td>
            <a class="btn btn-info btn-xs" onclick="personalcontrato_form(\'L\', '.$value['tb_personalcontrato_id'].')"><i class="fa fa-eye"></i></a> 
            <a class="btn btn-warning btn-xs" onclick="personalcontrato_form(\'M\', '.$value['tb_personalcontrato_id'].')"><i class="fa fa-pencil"></i></a> 
            <a class="btn btn-danger btn-xs" onclick="personalcontrato_form(\'E\', '.$value['tb_personalcontrato_id'].')"><i class="fa fa-trash"></i></a> 
            <a class="btn btn-success btn-xs" onclick="upload_formpdf(\'I\', '.$value['tb_personalcontrato_id'].')"><i class="fa fa-cloud-upload"></i></a>
          </td>
        </tr>
      ';
    }
    
  }
$result = NULL;
?>

<div class="row">
  <!-- TABLA PARA HISTORIAL DE CAMBIOS LABORALES-->
  <div class="col-md-12" style="margin-top: 10px;">
    <div class="box box-info shadow-simple">
      <div class="box-header"><h3 class="box-title">Historial de Contratos y Cambios Laborales</h3></div>
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>ID</th>
              <th>Fecha Inicio</th>
              <th>Fecha Fin</th>
              <th>Cargo</th>
              <th>Tipo Contrato</th>
              <th>Sueldo</th>
              <th>Tipo Jornada</th>
              <th>Horas a Laborar</th>
              <th>Aportación</th>
              <th>Asignación Familiar</th>
              <th style="width: 20%;">Descripción</th>
              <th style="width: 6%;">PDF</th>
              <th style="width: 8%;">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php echo $tabla_contratos;?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- TABLA PARA HISTORIAL DE VACACIONES-->
  <div class="col-md-6" style="margin-top: 10px;">
    <div class="box box-info shadow-simple">
      <div class="box-header"><h3 class="box-title">Historial de Vacaciones</h3></div>
      <div class="box-body max-servicios">
        <?php require_once('../vacaciones/vacaciones_tabla.php');?>
      </div>
    </div>
  </div>

  <!-- TABLA PARA HISTORIAL DE BONOS-->
  <div class="col-md-6" style="margin-top: 10px;">
    <div class="box box-info shadow-simple">
      <div class="box-header"><h3 class="box-title">Historial de Bonos</h3></div>
      <div class="box-body max-servicios">
        <?php require_once('../bono/bono_tabla_simple.php');?> 
      </div>
    </div>
  </div>

  <!-- TABLA PARA HISTORIAL DE PENALIDADES-->
  <div class="col-md-6" style="margin-top: 10px;">
    <div class="box box-info shadow-simple">
      <div class="box-header"><h3 class="box-title">Historial de Descuentos u Obligaciones</h3></div>
      <div class="box-body max-servicios">
        <?php require_once('../descuento/descuento_tabla.php');?>  
      </div>
    </div>
  </div>
</div>