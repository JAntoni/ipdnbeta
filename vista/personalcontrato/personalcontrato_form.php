<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../personalcontrato/Personalcontrato.class.php');
  $oPersonal = new Personalcontrato();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'personalcontrato';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $personalcontrato_id = $_POST['personalcontrato_id'];
  $personal_usuario_id = $_POST['usuario_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Contrato del Personal';
  }
  elseif ($usuario_action == 'I') {
      $titulo = 'Registrar Nuevo Contrato';
  } elseif ($usuario_action == 'M') {
      $titulo = 'Editar Contrato del Personal';
  } elseif ($usuario_action == 'E') {
      $titulo = 'Eliminar Contrato del Personal';
  } else {
      $titulo = 'Acción de Usuario Desconocido';
  }

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cliente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'personalcontrato'; $modulo_id = $personalcontrato_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $bandera = 4;
          $mensaje =  'No tienes el permiso para la acción que deseas realizar: '.$action;
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cliente por su ID
    $personalcontrato_tip = 0;

    if(intval($personalcontrato_id) > 0){
      $result = $oPersonal->mostrarUno($personalcontrato_id);
        if($result['estado'] == 1){
          $personalcontrato_tipo = $result['data']['tb_personalcontrato_tipo']; 
          $usuario_id = $result['data']['tb_usuario_id'];
          $personalcontrato_fecini = mostrar_fecha($result['data']['tb_personalcontrato_fecini']); 
          $personalcontrato_fecfin = mostrar_fecha($result['data']['tb_personalcontrato_fecfin']); 
          $personalcontrato_tiptur = $result['data']['tb_personalcontrato_tiptur']; 
          $seguro_id = $result['data']['tb_seguro_id']; 
          $personalcontrato_numcuenta = $result['data']['tb_personalcontrato_numcuenta']; 
          $personalcontrato_horas = $result['data']['tb_personalcontrato_horas'];
          $personalcontrato_ing1 = $result['data']['tb_personalcontrato_ing1'];;
          $personalcontrato_sal1 = $result['data']['tb_personalcontrato_sal1'];;
          $personalcontrato_ing2 = $result['data']['tb_personalcontrato_ing2'];;
          $personalcontrato_sal2 = $result['data']['tb_personalcontrato_sal2'];;
          $cargo_id = $result['data']['tb_cargo_id']; 
          $personalcontrato_sueldo = $result['data']['tb_personalcontrato_sueldo']; 
          $personalcontrato_asigfamilia = $result['data']['tb_personalcontrato_asigfamilia'];
          $personalcontrato_inde = $result['data']['tb_personalcontrato_inde']; 
          $personalcontrato_des = $result['data']['tb_personalcontrato_des'];
        }
      $result = NULL;
    } 
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_personalcontrato" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_personalcontrato" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_personalcontrato_id" value="<?php echo $personalcontrato_id;?>">
          <input type="hidden" name="hdd_usuario_id" value="<?php echo $personal_usuario_id;?>">
          <input type="hidden" id="cliente_vista" name="cliente_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_personalcontrato_tipo" class="control-label">Tipo o Estado</label>
                  <select class="form-control input-sm" id="cmb_personalcontrato_tipo" name="cmb_personalcontrato_tipo">
                    <option value="">Selecciona...</option>
                    <option value="1" <?php if($personalcontrato_tipo == 1) echo 'selected';?> >Inicio de Labores</option>
                    <option value="2" <?php if($personalcontrato_tipo == 2) echo 'selected';?> >Extensión o Cambio de Puesto</option>
                    <option value="3" <?php if($personalcontrato_tipo == 3) echo 'selected';?> >Ascenso de Colaborador</option>
                    <option value="4" <?php if($personalcontrato_tipo == 4) echo 'selected';?> >Finalización de Contrato</option>
                    <option value="5" <?php if($personalcontrato_tipo == 5) echo 'selected';?> >Renuncia Voluntaria</option>
                    <option value="6" <?php if($personalcontrato_tipo == 6) echo 'selected';?> >Despido al Colaborador</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_personalcontrato_inde" class="control-label">¿Personal Indefinido?</label>
                  <select class="form-control input-sm" id="cmb_personalcontrato_inde" name="cmb_personalcontrato_inde">
                    <option value="0" <?php if($personalcontrato_inde == 0) echo 'selected';?>>NO</option>
                    <option value="1" <?php if($personalcontrato_inde == 1) echo 'selected';?> >SI</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_fecini" class="control-label">Fecha de Inicio</label>
                  <div class="input-group date" id="datetimepicker1">
                    <input type="text" name="txt_personalcontrato_fecini" id="txt_personalcontrato_fecini" class="form-control input-sm" value="<?php echo $personalcontrato_fecini;?>">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group personal_ocultar fecha_finaliza">
                  <label for="txt_personalcontrato_fecfin" class="control-label">Fecha de Finalización</label>
                  <div class="input-group date" id="datetimepicker2">
                    <input type="text" name="txt_personalcontrato_fecfin" id="txt_personalcontrato_fecfin" class="form-control input-sm" value="<?php echo $personalcontrato_fecfin;?>">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row personal_ocultar">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_personalcontrato_tiptur" class="control-label">Tipo de Jornada</label>
                  <select class="form-control input-sm" id="cmb_personalcontrato_tiptur" name="cmb_personalcontrato_tiptur">
                    <option value="">Selecciona...</option>
                    <option value="1" <?php if($personalcontrato_tiptur == 1) echo 'selected';?> >Full Time</option>
                    <option value="2" <?php if($personalcontrato_tiptur == 2) echo 'selected';?> >Part Time Mañana</option>
                    <option value="3" <?php if($personalcontrato_tiptur == 3) echo 'selected';?> >Part Time Tarde</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_seguro_id" class="control-label">Aportaciones AFP/ONP</label>
                  <select class="form-control input-sm" id="cmb_seguro_id" name="cmb_seguro_id">
                    <?php
                      require_once('../aportaciones/cmb_seguro_id.php');
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row personal_ocultar">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_numcuenta" class="control-label">N° de Cuenta</label>
                  <input type="text" name="txt_personalcontrato_numcuenta" id="txt_personalcontrato_numcuenta" class="form-control input-sm" value="<?php echo $personalcontrato_numcuenta;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_horas" class="control-label">Horas a Cumplir</label>
                  <input type="text" name="txt_personalcontrato_horas" id="txt_personalcontrato_horas" class="form-control input-sm" value="<?php echo $personalcontrato_horas;?>">
                </div>
              </div>
            </div>
            <div class="row personal_ocultar">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_ing1" class="control-label">Hora Ingreso Mañana</label>
                  <input type="time" name="txt_personalcontrato_ing1" id="txt_personalcontrato_ing1" class="form-control input-sm" value="<?php echo $personalcontrato_ing1;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_sal1" class="control-label">Hora Salida Mañana</label>
                  <input type="time" name="txt_personalcontrato_sal1" id="txt_personalcontrato_sal1" class="form-control input-sm" value="<?php echo $personalcontrato_sal1;?>">
                </div>
              </div>
            </div>
            <div class="row personal_ocultar">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_ing2" class="control-label">Hora Ingreso Tarde</label>
                  <input type="time" name="txt_personalcontrato_ing2" id="txt_personalcontrato_ing2" class="form-control input-sm" value="<?php echo $personalcontrato_ing2;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_personalcontrato_sal2" class="control-label">Hora Salida Tarde</label>
                  <input type="time" name="txt_personalcontrato_sal2" id="txt_personalcontrato_sal2" class="form-control input-sm" value="<?php echo $personalcontrato_sal2;?>">
                </div>
              </div>
            </div>
            <div class="row personal_ocultar">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="cmb_cargo_id" class="control-label">Cargo</label>
                  <select class="form-control input-sm" id="cmb_cargo_id" name="cmb_cargo_id">
                    <?php
                      require_once('../cargo/cargo_select.php');
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_personalcontrato_sueldo" class="control-label">Sueldo Base</label>
                  <input type="text" name="txt_personalcontrato_sueldo" id="txt_personalcontrato_sueldo" class="form-control input-sm" value="<?php echo $personalcontrato_sueldo;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_personalcontrato_asigfamilia" class="control-label">Asignación Familiar</label>
                  <input type="text" name="txt_personalcontrato_asigfamilia" id="txt_personalcontrato_asigfamilia" class="form-control input-sm" value="<?php echo $personalcontrato_asigfamilia;?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_personalcontrato_des" class="control-label">Descripción del Contrato o Cambio Laboral</label>
                  <textarea class="form-control input-sm" id="txt_personalcontrato_des" name="txt_personalcontrato_des"><?php echo $personalcontrato_des;?></textarea>
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Contrato?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cliente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>

          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_personalcontrato">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/personalcontrato/personalcontrato_form.js?ver=1';?>"></script>
