<?php
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
echo dirname(__FILE__); exit();
require_once dirname(__FILE__) . '/../../libreriasphp/phpexcel/Classes/PHPExcel.php';



//
$nombre_archivo = "Gestión de Cobranza por Asesor";

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("srjuan54@gmail.com")
	->setLastModifiedBy("srjuan54@gmail.com")
	->setTitle("Reporte")
	->setSubject("Reporte")
	->setDescription("Reporte generado por srjuan54@gmail.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
  'font' => array(
    'name'  => 'Arial',
    'bold'  => true,
    'size'  =>8,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);
  
  $fecha1 = $_GET['fecha1'];
  $fecha2 = $_GET['fecha2'];

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
  
  $c = 2;
  $titulo = "Gestión de Cobranza por Usuario, fechas: ".$fecha1." y ".$fecha2;
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:H$c");
  $objPHPExcel->getActiveSheet()->setCellValue("A$c",$titulo);

  $c = $c + 1;

  $titulosColumnas = array(
    'ASESOR',
    'CLIENTES',
    'FACTURACIÓN S/.',
    'FACTURACIÓN US$',
    'CUOTAS'
  );

  $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
  ;

  $objPHPExcel->getActiveSheet()->getStyle("A$c:E$c")->applyFromArray($estiloTituloColumnas);

  $c = $c+1;
  $TOTAL_SOLES = 0; $TOTAL_DOLARES = 0; $TOTAL_CUOTAS = 0; $TOTAL_CLIENTES = 0; $ARRAY_CLIENTES = array();
  //listamos todos los clientes de cada usuario, los usuarios son 4
  
    
  $objPHPExcel->getActiveSheet(0)
    ->setCellValue('A'.$c, 'TOTAL')
    ->setCellValue('B'.$c, $TOTAL_CLIENTES.' CLIENTES')
    ->setCellValue('C'.$c, $TOTAL_SOLES)
    ->setCellValue('D'.$c, $TOTAL_DOLARES)
    ->setCellValue('E'.$c, $TOTAL_CUOTAS)
  ;

  $objPHPExcel->getActiveSheet(0)->getStyle('C'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet(0)->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet(0)->getStyle('E'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

  for($z = 'A'; $z <= 'E'; $z++){
    $objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
  }

  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('Gestion de Cobranza');

//-------------------------------------------------------------------------------------------------------------------------------------------
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;