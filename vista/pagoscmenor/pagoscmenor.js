var datatable_global;

function pagoscmenor_tabla(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php

  $.ajax({
    type: "POST",
    url: VISTA_URL+"pagoscmenor/pagoscmenor_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      pago_fec1: $('#txt_filtro_fec1').val(),
      pago_fec2: $('#txt_filtro_fec2').val()
    }),
    beforeSend: function() {
      $('#pagoscmenor_mensaje_tbl').show(300);
      $('#span_cargando').text('Cargando Tabla CREDITO MENOR...');
      datatable_global.destroy();
    },
    success: function(data){
      $('#pagoscmenor_mensaje_tbl').hide(300);
      
      $('.tabla_creditomenor').html(data);
      estilos_datatable();
      
    },
    complete: function(data){
      console.log(data)
    },
    error: function(data){
      $('#pagoscmenor_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}



function mostrar_sumas_totales(data){
  console.log(data);
  // var suma_total_desembolsado = $('#hdd_suma_total_desembolsado').val();
  // var suma_total_capital_recuperar = $('#hdd_suma_total_capital_recuperar').val();
  // var suma_capital_recuperado = $('#hdd_suma_capital_recuperado').val();
  // var suma_total_facturado = $('#hdd_suma_total_facturado').val();
  // var suma_total_cobrado = $('#hdd_suma_total_cobrado').val();
  // var suma_total_por_cobrar = $('#hdd_suma_total_por_cobrar').val();

  $('#span_desembolsado').text('S/. ' + data.suma_total_desembolsado);
  $('#span_recuperado').text('S/. ' + data.suma_capital_recuperado);
  $('#span_por_recuperar').text('S/. ' + data.suma_total_capital_recuperar);

  $('#span_total_facturado').text('S/. ' + data.suma_total_facturado);
  $('#span_total_cobrado').text('S/. ' + data.suma_total_cobrado);
  $('#span_total_por_cobrar').text('S/. ' + data.suma_total_por_cobrar);
}


function estilos_datatable(){
  datatable_global = $('#tbl_creditomenor').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    //"bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    //"scrollY": "80vh",
    //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
    dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
    buttons: [
      { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
      { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
      { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
      { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
    ],
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
    /*drawCallback: function () {
      var sum_desembolsado = $('#tbl_comportamiento').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
      var sum_capital_rest_sol = $('#tbl_comportamiento').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles

      $('#total').html(sum);
    }*/
  });
}

$(document).ready(function() {
  console.log('Perfil menu 666');

  estilos_datatable();

  pagoscmenor_tabla();

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
});