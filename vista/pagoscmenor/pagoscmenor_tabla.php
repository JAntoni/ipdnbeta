<?php
  //require_once('../../core/usuario_sesion.php');
  date_default_timezone_set("America/Lima");
  require_once('../cuota/Cuota.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');
  require_once('../monedacambio/Monedacambio.class.php');
  require_once('../ingreso/Ingreso.class.php');
  require_once('../cuotapago/Cuotapago.class.php');

  $oCambio = new Monedacambio();
  $oCuota = new Cuota();
  $oIngreso = new Ingreso();
  $oCuotapago = new Cuotapago();

  $fecha_hoy = date('Y-m-d');

  $fecha_ultimo = new DateTime();
  $fecha_ultimo->modify('last day of this month');

  $pago_fec1 = date('01-01-Y');
  $pago_fec2 = $fecha_ultimo->format('Y-m-d');

  $pago_fec1 = (isset($_POST['pago_fec1']))? fecha_mysql($_POST['pago_fec1']) : fecha_mysql($pago_fec1);
  $pago_fec2 = (isset($_POST['pago_fec2']))? fecha_mysql($_POST['pago_fec2']) : fecha_mysql($pago_fec2);
  $tipo_cambio = (isset($_POST['tipo_cambio']))? floatval($_POST['tipo_cambio']) : $moneda_cambio; //tipo cambio del día
  $vista_modulo = (isset($_POST['vista_modulo']))? $_POST['vista_modulo'] : 'pagoscmenor';

  $data = '';

  $SUMA_TOTAL_PAGADO = 0;
  $SUMA_TOTAL_UTILIDAD = 0;
  $SUMA_TOTAL_IGV = 0;
  $SUMA_TOTAL_INTERES = 0;

  // 3 para vigentes, 4 paralizados
  $result = $oCuotapago->listar_pagos_rango_creditomenor($pago_fec1, $pago_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        // $SUMA_TOTAL_DESEMBOLSADO += $capital_desembolsado_soles;
        // $SUMA_TOTAL_CAPITAL_RECUPERAR += $capital_restante_soles ;
        // $SUMA_TOTAL_FACTURADO += $monto_cuota_soles;
        // $SUMA_CAPITAL_RECUPERADO = $SUMA_TOTAL_DESEMBOLSADO - $SUMA_TOTAL_CAPITAL_RECUPERAR;
        $moneda_simbolo = 'S/.';
        $estado_mensaje = '<span class="badge bg-red">Pago Parcial</span>';
        $tipo_cuota = '<span style="color: blue;"><b>Libre</b></span>';
        $estado_valor = intval($value['tb_cuota_est']);
        $monto_amortizado = $value['tb_cuota_amo'];

        if(intval($value['tb_moneda_id']) == 2)
          $moneda_simbolo = 'US$';
        if(intval($value['tb_cuota_est']) == 2)
          $estado_mensaje = '<span class="badge bg-green">Pagado</span>';
        if(intval($value['tb_cuotatipo_id']) == 2)
          $tipo_cuota = '<span style="color: black;"><b>Fija</b></span>';

        $valor_cuota = formato_numero($value['cuota_real']);
        $monto_pagado = formato_numero($value['monto_pagado']);
        $fecha_para_anticipos = $value['mes_anio'].'-01'; //? es decir buscará todos los pagos de esta cuota pero del mes anterior a este mes, ya que esta agrupado por mes

        //? BUSQUEMOS PAGOS ANTERIORES A LA FECHA DE ESTE MES, LLAMADOS ANTICIPOS PARA SUMARLOS AL PAGO DE ESTE MES Y VERIFICAR SI LA CUOTA YA ESTARÍA PAGADA EN ESTE PERIODO
        $modulo_id = 1; //? QUE REPRESENTA 1 A PAGO DE CUOTA, 2 PAGO DE CUOTA DETALLE EN LA TABLA TB_CUOTAPAGO
        $cuotapago_modid = $value['tb_cuota_id']; //? EN LA TABLA TB_CUOTAPAGO SE GUARDA EN MODID EL ID DE LA CUOTA O EL ID DE LA CUOTA DETALLE
        $fecha_pago = $value['fecha_pago']; //? DE ESTA ULTIMA FECHA DE PAGO VAMOS A BUSCAR INGRESOS MENORES A ESTA FECHA, LO QUE INDICA ANTICIPOS
        $pagos_anticipados = 0;

        //-- SOLO CONSULTAR ANTICIPOS DE PAGOS MENORES A LA CUOTA PERO QUE ESTÁN EN ESTADO PAGO, PORQUE CONSULTAR POR CADA UNO ES DEMASIADO
        if($monto_pagado < $valor_cuota && intval($value['tb_cuota_est']) == 2){
          $result2 = $oCuotapago->mostrar_pagos_anticipos_tipo_cuota_menor_fecha($modulo_id, $cuotapago_modid, $fecha_para_anticipos);
            if(intval($result2['estado']) == 1){
              foreach ($result2['data'] as $key => $value2) {
                $pagos_anticipados += formato_numero($value2['tb_ingreso_imp']);
              }
            }
          $result2 = NULL;

          $suma_pago_anticipos = formato_numero($pagos_anticipados + $monto_pagado);

          if($suma_pago_anticipos < $valor_cuota){
            $estado_mensaje = '<span class="badge bg-red">Pago Parcial</span>';
            $estado_valor = 3;
          }
        }

        $utilidad = 0;
        $igv = 0;
        $interes_total = 0;

        if($estado_valor == 2){
          $interes_total = formato_numero($value['tb_cuota_int']);
          $utilidad = formato_numero($interes_total / 1.18);
          $igv = formato_numero($interes_total - $utilidad);

          if($monto_pagado > $valor_cuota)
            $monto_amortizado = formato_numero($monto_pagado - $valor_cuota);
        }

        $SUMA_TOTAL_PAGADO += $monto_pagado;
        $SUMA_TOTAL_UTILIDAD += $utilidad;
        $SUMA_TOTAL_IGV += $igv;
        $SUMA_TOTAL_INTERES += $interes_total;

        $data .= '
          <tr>
            <td>'.$value['tb_credito_id'].'</td>
            <td>'.$value['tb_cliente_nom'].'</td>
            <td>'.$value['tb_cliente_doc'].'</td>
            <td>'.$value['mes_anio'].'</td>
            <td>'.mostrar_fecha($value['tb_cuota_fec']).'</td>
            <td>'.mostrar_fecha($value['fecha_pago']).'</td>
            <td>'.$value['tb_cuota_num'].' / '.$value['numero_cuotas'].' / '.$tipo_cuota.'</td>
            <td>'.$value['tb_cuota_cap'].'</td>
            <td>'.$monto_amortizado.'</td>
            <td>'.$value['cuota_real'].'</td>
            <td>'.$value['monto_pagado'].'</td>
            <td>'.$pagos_anticipados.'</td>
            <td>'.$utilidad.'</td>
            <td>'.$igv.'</td>
            <td>'.$interes_total.'</td>
            <td>'.$estado_mensaje.'</td>
          </tr>
        ';
      }
    }
  $result = NULL;
  
?>
  <!-- CREA LA CABECERA DE LA TABLA CON LAS COLUMNAS QUE SE MOSTRARÁt -->
  <table id="tbl_creditomenor" class="table table-hover" style="width: 100%;">
    <thead>
      <tr>
        <th>ID CRÉDITO</th>
        <th>CLIENTE</th>
        <th class="no-sort">DOCUMENTO</th>
        <th class="no-sort">MES PERIODO</th>
        <th class="no-sort">FECHA CUOTA</th>
        <th class="no-sort">FECHA PAGO</th>
        <th class="no-sort">N° CUOTA</th>
        <th>CAPITAL</th>
        <th>AMORTIZACION</th>
        <th>VALOR DE CUOTA</th>
        <th>MONTO PAGADO</th>
        <th class="no-sort">ANTICIPOS</th>
        <th>UTILIDAD</th>
        <th class="no-sort">IGV</th>
        <th>INTERÉS TOTAL</th>
        <th class="no-sort">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $data; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="10"><b>SUMAS TOTALES</b></td>
        <td><b><?php echo $SUMA_TOTAL_PAGADO;?></b></td>
        <td></td>
        <td><b><?php echo $SUMA_TOTAL_UTILIDAD;?></b></td>
        <td><b><?php echo $SUMA_TOTAL_IGV;?></b></td>
        <td><b><?php echo $SUMA_TOTAL_INTERES;?></b></td>
        <td></td>
      </tr>
    </tfoot>
  </table>
