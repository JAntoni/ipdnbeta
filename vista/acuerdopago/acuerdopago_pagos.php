<?php
//session_start();
require_once('../../core/usuario_sesion.php');
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$acu_id = $_POST['acu_id'];
$num_ins = $_POST['num_ins'];
$cre_tip = $_POST['cre_tip'];
$link_pdf = 'https://www.ipdnsac.com/app/modulos/acuerdopago/acuerdopago_instancia0' . $num_ins . '.php?ins=0' . $num_ins . '&acu=' . $acu_id . '&tip=' . $cre_tip;

$result = $oAcuerdopago->mostrarUno($acu_id);
if ($result['estado'] == 1) {
  $cuode_ids = $result['data']['tb_cuotas_ids'];
  $acu_mon = $result['data']['tb_acuerdopago_mon']; //monto total del acuerdo de pago
  $string_ids = $result['data']['tb_cuotas_ids']; //ids de las cuotas vencidas
  $moneda_id = $result['data']['tb_moneda_id'];
}
$result = null;

$simbolo = 'S/.';
if ($moneda_id == 2)
  $simbolo = 'US$';
//monto pagado hasta el momento de las cuotas acuerdo de pago, si su suma es igual a $acu_mon, entonces el acuerdopago está termianda
$result = $oAcuerdopago->total_importe_cuotas_acuerdopago($acu_id);
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $importe_total = $value['importe'];
  }
}
$result = null;

$array_ids = explode(',', $string_ids);
$cant = count($array_ids);
/*
if($importe_total > 0){
	for ($i=0; $i < $cant; $i++) {

		$dts = $oCuotadetalle->mostrarUno($array_ids[$i]);
			$dt = mysql_fetch_array($dts);
			$mon_cuo = $dt['tb_cuotadetalle_cuo'];
		mysql_free_result($dts);

		if($importe_total > $mon_cuo){
			//cancelamos por completo la cuotadetalle
			$oCuotadetalle->modificar_campo($array_ids[$i],'est',2);
			$importe_total = $importe_total - $mon_cuo;
		}
		elseif ($importe_total == $mon_cuo) {
			//cancelamos por completo la cuotadetalle
			$oCuotadetalle->modificar_campo($array_ids[$i],'est',2);
			break;
		}
		else{
			//la cuota vencida está en pago parcial, estado = 3
			$oCuotadetalle->modificar_campo($array_ids[$i],'est',3);
			break;
		}

	}
}
//echo $det_cuo_ids.' / '.$.' / '.$cre_id.' / '.$cli_id.' / '.$mon_id;
*/
$importe = $importe_total;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_acuerdopago_pagos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">ACUERDO DE PAGOS</h4>
      </div>
      <div id="for_acuerdopago">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-6 col-lg-6">
                      <fieldset>
                        <legend>Cuotas vencidas o por vencer</legend>
                        <table class="table table-bordered table-hover">
                          <thead>
                            <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                              <th>ID CUOT DE.</th>
                              <th>FECHA VEN.</th>
                              <th>MONTO</th>
                              <th>ESTADO</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $dts = $oCuotadetalle->mostrar_cuotadetalle_in($cuode_ids);
                            if ($dts['estado'] == 1) {
                              foreach ($dts['data'] as $key => $dt) {

                                $dts2 = $oCuotapago->importe_total_cuotadetalle($dt['tb_cuotadetalle_id']);
                                if ($dts2['estado'] == 1) {
                                  $cuode_imp = $dts2['data']['importe'];
                                }
                                $dts2 = null;

                                $saldo = $dt['tb_cuotadetalle_cuo'] - $cuode_imp;
                            ?>
                                <tr class="even">
                                  <td><?php echo $dt['tb_cuotadetalle_id']; ?></td>
                                  <td><?php echo mostrar_fecha($dt['tb_cuotadetalle_fec']) ?></td>
                                  <td><?php echo $simbolo . ' ' . formato_moneda($saldo) ?></td>
                                  <td>
                                    <?php
                                    if ($importe_total < $acu_mon) {
                                      if ($importe > $saldo) {
                                        echo "CANCELADO";
                                        $importe = $importe - $saldo;
                                      } elseif ($importe == $saldo) {
                                        echo "CANCELADO";
                                        $importe = $importe - $saldo;
                                      } elseif ($importe < $saldo && $importe > 0) {
                                        echo "PAGO PARCIAL";
                                        $importe = 0;
                                      } else
                                        echo "POR COBRAR";
                                    } else
                                      echo "CANCELADO";
                                    ?>
                                  </td>
                                </tr>
                            <?php
                              }
                            }
                            $dts = null;
                            ?>
                          </tbody>
                          <tr class="even">
                            <td colspan="2">Total Acuerdo:</td>
                            <td><?php echo $simbolo . ' ' . formato_moneda($acu_mon) ?></td>
                            <td></td>
                          </tr>
                        </table>
                      </fieldset>
                    </div>
                    <div class="col-md-6 col-lg-6">
                      <fieldset>
                        <legend>Pago de los Acuerdos de Pago</legend>
                        <table class="table table-bordered table-hover">
                          <thead>
                            <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                              <th>ID CUOT DE.</th>
                              <th>FECHA PAGO</th>
                              <th>MONTO</th>
                              <th>IMPORTE</th>
                              <th>ESTADO</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $dts = $oAcuerdopago->listar_cuotas_en_acuerdopago($acu_id);
                            if ($dts['estado'] == 1) {
                              foreach ($dts['data'] as $key => $dt) {
                            ?>
                                <tr class="even">
                                  <td><?php echo $dt['tb_cuotadetalle_id']; ?></td>
                                  <td><?php echo mostrar_fecha($dt['tb_cuotadetalle_fec']) ?></td>
                                  <td><?php echo $simbolo . ' ' . formato_moneda($dt['tb_cuotadetalle_cuo']) ?></td>
                                  <td><?php echo $simbolo . ' ' . formato_moneda($dt['importe']) ?></td>
                                  <td>
                                    <?php
                                    if ($dt['tb_cuotadetalle_est'] == 1)
                                      echo 'POR COBRAR';
                                    if ($dt['tb_cuotadetalle_est'] == 2)
                                      echo 'CANCELADO';
                                    if ($dt['tb_cuotadetalle_est'] == 3)
                                      echo 'PAGO PARCIAL';
                                    ?>
                                  </td>
                                </tr>
                            <?php
                              }
                            }
                            $dts = null;
                            ?>
                          </tbody>
                          <tr class="even">
                            <td colspan="3">Importe Total:</td>
                            <td><?php echo $simbolo . ' ' . formato_moneda($importe_total) ?></td>
                            <td></td>
                          </tr>
                        </table>
                      </fieldset>
                      <div>
                        <a href="<?php echo $link_pdf ?>" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Imprimir Instancia <?php echo '0' . $num_ins; ?></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>