<?php
//session_start();
require_once('../../core/usuario_sesion.php');
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../cajacambio/Cajacambio.class.php");
$oCajacambio = new Cajacambio();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");
//SELECT * FROM `tb_cuotapago` where tb_modulo_id = 2 and tb_cuotapago_modid in (select tb_cuotadetalle_id from tb_creditoasiveh cre inner join tb_cuota cu on cu.tb_credito_id = cre.tb_credito_id inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id where cre.tb_credito_id = 167)
$acu_id = $_POST['acu_id'];
$cre_tip = $_POST['cre_tip'];
$motivo = $_POST['motivo'];

if($cre_tip == 2){
  require_once("../creditoasiveh/Creditoasiveh.class.php");
  $oCredito = new Creditoasiveh();
}
if($cre_tip == 3){
  require_once("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
}
if($cre_tip == 4){
  require_once("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
}

$dts = $oAcuerdopago->mostrarUno($acu_id);
if($dts['estado']==1){
  $cre_id = $dts['data']['tb_credito_id'];
  $cuode_ids = $dts['data']['tb_cuotas_ids'];
  $cre_nuevo = $dts['data']['tb_credito_nuevo'];//id del credito nuevo en base al orginal
}
$dts=null;

//devolvemos a su estado normal a las cuotasdetalle, es decir les quitamos la opción de que se pagará mediante acuerdo de pago
$array_ids = explode(',', $cuode_ids);
$cant = count($array_ids);

//parámetro cero porque ya no se pagará mediante AP
for ($i=0; $i < $cant; $i++) { 
  $oCuotadetalle->modificar_campo($array_ids[$i], 'tb_cuotadetalle_estap', 0, 'INT');
}

//cambiamos a resuelto el estado del AP, parametro 2 ya que pasa a resuelto
$oAcuerdopago->resolver_acuerdopago($_SESSION['usuario_id'], $acu_id, 2);

//cambiamos el estado de aplicar monto a 1: disponible para ser aplicado
$oAcuerdopago->modificar_campo($acu_id, 'apl', 1, 'INT');

//al CREDITO ACUERDO DE PAGO le cambiamos de estado a RESUELTO, GUARDANDO HISTORIAL DE TODO
$est = 8;
$oCredito->aprobar($cre_nuevo, $_SESSION['usuario_id'], $est);

//registramos el historial para el credito AP
$his = '<span style="color: #F56B02;">Crédito Resuelto por: <b>'.$_SESSION['usuario_nombre'].'</b>. Detalle: Este crédito AP se resolvió ya que su monto pagado total se aplicó para saldar cuotas vencidas. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
$oCredito->registro_historial($cre_nuevo, $his);

//registramos historial para el crédito matriz
$his = '<span>El AP (ID: '.$cre_nuevo.') de este crédito fue aplicado por: <b>'.$_SESSION['usuario_nombre'].'</b>. Detalle: El crédito AP se resolvió ya que su monto pagado total se aplicó para saldar cuotas vencidas. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
$oCredito->registro_historial($cre_id, $his);

$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha aplicado un Acuerdo de Pago del Crédito ASIVEH de número: CAV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'. El Acuerdo de Pago tiene como número AP-CAV-'.str_pad($cre_nuevo, 4, "0", STR_PAD_LEFT).' && <b>'.date('d-m-Y h:i a').'</b>';
$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
$oLineaTiempo->tb_lineatiempo_det = $line_det;
$oLineaTiempo->insertar();
//en cuota, modificar segun el credito_id, parametro 0 al xac y 2 ya que el credito es de tipo ASIVEH
//$oCuota->modificar_xac_por_credito($cre_nuevo, 0, $cre_tip);

//en cuotadetalle modificar el xac según las cuotas, parametro 0 al xac y 2 ya que el credito es de tipo ASIVEH
//$oCuotadetalle->modificar_xac_por_credito($cre_nuevo, 0, $cre_tip);

$data['cre_nuevo']=$cre_nuevo;
$data['estado']=1;
$data['msj']='El crédito está listo para que sus montos sean aplicados.';
echo json_encode($data);
?>