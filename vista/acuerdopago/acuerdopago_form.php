<?php
require_once('../../core/usuario_sesion.php');

require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");


$fecha_hoy = date('d-m-Y', strtotime('+10 day'));

$acu_id = $_POST['acu_id'];
$action = $_POST['action'];
if ($action == 'editar') {
  $result = $oAcuerdopago->mostrarUno($acu_id);
  if ($result['estado'] == 1) {
    $cre_id = $result['data']['tb_credito_id'];
    $cre_tip = $result['data']['tb_credito_tip']; //tipo de credito 2 asive, 3 garveh, 4 hipo
    $fecreg = $result['data']['tb_acuerdopago_fecreg'];
    $fecmod = $result['data']['tb_acuerdopago_fecmod'];
    $motivo = $result['data']['tb_acuerdopago_mot'];
    $num_cuo = $result['data']['tb_acuerdopago_numcuo']; //numero de cuotas para el acuerdo
    $mon_acu = $result['data']['tb_acuerdopago_mon']; //monto total, es la suma de todas las vencidas
    $cli_id = $result['data']['tb_cliente_id']; //id del cliente
    $mon_id = $result['data']['tb_moneda_id']; //tipo de moneda
    $interes = $result['data']['tb_acuerdopago_int']; //interés aplicado a los montos del acuerdo
    $periodo = $result['data']['tb_acuerdopago_per']; //periodo, semana, quincena o mensual
    $estado = $result['data']['tb_acuerdopago_xac']; //si xac es 0 activo, si es 1 anulado, si 2 resuelto
    $usureg = $result['data']['tb_acuerdopago_usureg'];
    $usumod = $result['data']['tb_acuerdopago_usumod'];
    $det_cuo_ids = $result['data']['tb_cuota_ids']; //ids de las cuotas vencidas
    $fec_acu = mostrar_fecha($result['data']['tb_acuerdopago_fec']); //fecha de inicio del acuerdo de pago                   
  }
  $result = null;

  $result = $oUsuario->mostrarUno($usureg);
  if ($result['estado'] == 1) {
    $usu_nom = $result['data']['tb_usuario_nom'];
    $usu_ape = $result['data']['tb_usuario_ape'];
  }
  $result = null;

  $det_usureg = 'Registrado por: ' . $usu_nom . ' ' . $usu_ape . ' / ' . mostrar_fecha_hora($fecreg);
  $det_usumod = '';

  if ($estado == 1 || $estado == 2) {
    $result = $oUsuario->mostrarUno($usumod);
    if ($result['estado'] == 1) {
      $usu_nom = $result['data']['tb_usuario_nom'];
      $usu_ape = $result['data']['tb_usuario_ape'];
    }
    $result = null;

    $det_usumod = 'Anulado por: ' . $usu_nom . ' ' . $usu_ape . ' / ' . mostrar_fecha_hora($fecreg);

    if ($estado == 2)
      $det_usumod = 'Resuelto por: ' . $usu_nom . ' ' . $usu_ape . ' / ' . mostrar_fecha_hora($fecreg);
  }
}
if ($action == 'insertar') {
  $cre_id = $_POST['hdd_his_cre_id']; //id del crédito
  $mon_acu = $_POST['hdd_his_mon_acu']; //suma total del monto para el acuerdo
  $cli_id = $_POST['hdd_his_cli_id']; // id del cliente
  $mon_id = $_POST['hdd_his_mone_id']; //moneda id
  $cre_tip = $_POST['hdd_cre_tip']; //tipo de credito, 2 asiveh, 3 garveh y 4 hipo
  $cuotip_id = $_POST['hdd_cuotip_id']; //tipo cuota 3 LIBRE, 4 FIJA
  $cre_numcuomax = $_POST['hdd_cre_numcuomax']; //cuotas maximas cuando es LIBRE
  $det_cuo_ids = implode(',', $_POST['chk_cre_acuerdo']); //ids de detalle cuotas
  $fec_acu = $fecha_hoy;
}

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_acuerdo" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">ACUERDO DE PAGOS</h4>
      </div>
      <form id="for_acuerdopago">
        <input type="hidden" name="hdd_acu_id" id="hdd_acu_id" value="<?php echo $acu_id; ?>">
        <input type="hidden" name="hdd_action" id="hdd_action" value="<?php echo $action; ?>">
        <input type="hidden" name="hdd_cre_id" id="hdd_cre_id" value="<?php echo $cre_id; ?>">
        <input type="hidden" name="hdd_cre_tip" id="hdd_cre_tip" value="<?php echo $cre_tip; ?>">
        <input type="hidden" name="hdd_cli_id" id="hdd_cli_id" value="<?php echo $cli_id ?>">
        <input type="hidden" name="hdd_mon_id" id="hdd_mon_id" value="<?php echo $mon_id ?>">
        <input type="hidden" name="hdd_cuo_ids" id="hdd_cuo_id" value="<?php echo $det_cuo_ids ?>">
        <input type="hidden" name="hdd_cuotip_id" id="hdd_cuotip_id" value="<?php echo $cuotip_id ?>">
        <input type="hidden" name="hdd_cre_numcuomax" id="hdd_cre_numcuomax" value="<?php echo $cre_numcuomax ?>">
        <input type="hidden" name="hdd_his_mon_acu" id="hdd_his_mon_acu" value="<?php echo $mon_acu ?>">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 col-lg-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Datos del Acuerdo</h3>
                        </div>
                        <div class="box-body no-padding">
                          <div class="row">
                            <div class="col-md-4 col-lg-6">
                              <div class="form-group">
                                <label for="txt_acu_mon" class="control-label">Monto :</label>
                                <input type="text" name="txt_acu_mon" id="txt_acu_mon" class="form-control input-sm moneda2" value="<?php echo formato_moneda($mon_acu) ?>">
                              </div>
                            </div>
                            <div class="col-md-4 col-lg-6">
                              <div class="form-group">
                                <label for="cmb_acu_peri" class="control-label">Periodo :</label>
                                <select class="form-control input-sm" name="cmb_acu_peri" id="cmb_acu_peri">
                                  <!--option value="1" <?php if ($periodo == 1) echo 'selected' ?>>SEMANAL</!--option>
                                                <option-- value="2" <?php if ($periodo == 2) echo 'selected' ?>>QUINCENAL</option-->
                                  <option value="1" <?php if ($periodo == 1) echo 'selected' ?>>MENSUAL</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                              <div class="form-group">
                                <label for="">Fecha Inicio :</label>
                                <div class="input-group">
                                  <div class='input-group date' id='datetimepicker1111'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_acu_fec" id="txt_acu_fec" value="<?php echo mostrar_fecha($fec_acu); ?>" readonly />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!--div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for="txt_acu_mon" class="control-label">Interés :</label>
                                            <input type="text" name="txt_acu_int" id="txt_acu_int" class="form-control input-sm moneda2" value="<?php if (isset($interes)) echo formato_moneda($interes);
                                                                                                                                                else echo 0 ?>">                   
                                        </div>
                                    </div-->
                            <div class="col-md-4 col-lg-4">
                              <div class="form-group">
                                <label for="txt_acu_mon" class="control-label">Cuotas :</label>
                                <input type="text" name="txt_acu_numcuo" id="txt_acu_numcuo" class="form-control input-sm" value="<?php echo $num_cuo ?>">
                              </div>
                            </div>
                            <div class="col-md-2 col-lg-2">
                              <div class="form-group" style="padding-top: 20px">
                                <a href="#ca" id="btn_calcular_acu" class="btn btn-info" onclick="generar_tabla_acuerdo()">Calcular</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="msj_acuerdopago" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Tabla de Acuerdos de Pago</h3>
                        </div>
                        <div class="box-body no-padding">
                          <div id="div_acu_tbl_generado"></div>
                        </div>
                      </div>
                      <label><?php echo $det_usureg ?></label>
                      <br />
                      <label><?php echo $det_usumod ?></label>
                      <br />
                      <label><?php if (!empty($motivo)) echo 'Motivo: ' . $motivo ?></label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/acuerdopago/acuerdopago_form.js?ver=437433'; ?>"></script>