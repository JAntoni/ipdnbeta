<?php
//session_start();
require_once('../../core/usuario_sesion.php');
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once("../cajacambio/Cajacambio.class.php");
$oCajacambio = new Cajacambio();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
//SELECT * FROM `tb_cuotapago` where tb_modulo_id = 2 and tb_cuotapago_modid in (select tb_cuotadetalle_id from tb_creditoasiveh cre inner join tb_cuota cu on cu.tb_credito_id = cre.tb_credito_id inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id where cre.tb_credito_id = 167)
$acu_id = $_POST['acu_id'];
$cre_tip = $_POST['cre_tip'];
$motivo = $_POST['motivo'];

$result = $oAcuerdopago->mostrarUno($acu_id);
if ($result['estado'] == 1) {
  $cre_id = $result['data']['tb_credito_id'];
  $cuode_ids = $result['data']['tb_cuotas_ids'];
  $cre_nuevo = $result['data']['tb_credito_nuevo']; //id del credito nuevo en base al orginal
}
$result = null;

$credito_nom = 'CREDITO SIN TIPO';
$AP_SIN = 'AP SIN SIMBOLO';
$tb_credito = '';

if ($cre_tip == 2) {
  require_once("../creditoasiveh/Creditoasiveh.class.php");
  $oCredito = new Creditoasiveh();
  $credito_nom = 'Crédito ASIVEH de número: CAV';
  $AP_SIN = 'AP-CAV';
  $tb_credito = 'tb_creditoasiveh';
}
if ($cre_tip == 3) {
  require_once("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
  $credito_nom = 'Crédito GARVEH de número: CGV';
  $AP_SIN = 'AP-CGV';
  $tb_credito = 'tb_creditogarveh';
}
if ($cre_tip == 4) {
  require_once("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
  $credito_nom = 'Crédito HIPO de número: HIPO';
  $AP_SIN = 'AP-HIPO';
  $tb_credito = 'tb_creditohipo';
}

$result = $oCuotapago->listar_cuotapagos_por_credito_cuotadetalle($cre_nuevo, 0, $cre_tip);

//$num_rows = mysql_num_rows($dts);
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $cuopag_id = $value['tb_cuotapago_id'];
    //anulamos todos los ingresos de las cuotas de pago de este AP
    $result1 = $oCuotapago->mostrarUno($cuopag_id);
    if ($result1['estado'] == 1) {
      $cuota_tipo = $result1['data']['tb_modulo_id'];
      $cuota_id = $result1['data']['tb_cuotapago_modid'];
    }
    $result1 = null;
    //INGRESOS
    $mod_id = 30;
    $result2 = $oCuotapago->mostrar_cuotapago_ingreso($mod_id, $cuopag_id);
    if ($result2['estado'] == 1) {
      foreach ($result2['data'] as $key => $value2) {
        $oIngreso->modificar_campo($value2['tb_ingreso_id'], 'tb_ingreso_xac', 0, 'INT');
      }
    }
    $result2 = null;
    $data['cre_nuevo'] = $cre_nuevo;

    //CLIENTE CAJA
    $result3 = $oCuotapago->mostrar_clientecaja($cuopag_id);
    if ($result3['estado'] == 1) {
      foreach ($result3['data'] as $key => $value3) {
        $oClientecaja->modificar_campo($value3['tb_clientecaja_id'], 'tb_clientecaja_xac', 0, 'INT');
        //INGRESOS
        $mod_id = 40;
        $dts3 = $oCuotapago->mostrar_cuotapago_ingreso($mod_id, $value3['tb_clientecaja_id']);
        if ($dts3['estado'] == 1) {
          foreach ($dts3['data'] as $key => $dt3) {
            $oIngreso->modificar_campo($dt3['tb_ingreso_id'], 'tb_ingreso_xac', 0, 'INT');
          }
        }
        $dts3 = null;
        //EGRESOS
        $mod_id = 40;
        $dts3 = $oCuotapago->mostrar_cuotapago_egreso($mod_id, $value3['tb_clientecaja_id']);
        if ($dts3['estado'] == 1) {
          foreach ($dts3['data'] as $key => $dt3) {
            $oEgreso->modificar_campo($dt3['tb_egreso_id'], 'tb_egreso_xac', 0, 'INT');
          }
        }
        $dts3 = null;
      }
    }
    $result3 = null;


    $oCuotapago->modificar_campo($cuopag_id, 'tb_cuotapago_xac', 0, 'INT');
    //estado cuota
    if ($cuota_tipo == 1) {
      $mod_id = $cuota_tipo;
      $dts3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
      if ($dts3['estado'] == 1) {
        $num_dts3 = count($dts3['data']);
      }
      $dts3 = null;
      $est = 1;
      if ($num_dts3 > 0) {
        $est = 3;
      }
      $oCuota->modificar_campo($cuota_id, 'tb_cuota_est', $est, 'INT');
      //mora
      $oCuota->modificar_campo($cuota_id, 'tb_cuota_mor', moneda_mysql(0), 'STR');
      $oCuota->modificar_campo($cuota_id, 'tb_cuota_moraut', 0, 'INT');
    }
    if ($cuota_tipo == 2) {
      $cuotadetalle_id = $cuota_id;
      $dts1 = $oCuotadetalle->mostrarUno($cuotadetalle_id);
      if ($dts1['estado'] == 1) {
        $cuota_id = $dts1['data']['tb_cuota_id'];
      }
      $dts1 = null;
      //estado cuotadetalle
      $mod_id = $cuota_tipo;
      $dts3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuotadetalle_id);
      if ($dts3['estado'] == 1) {
        $num_dts3 = count($dts3['data']);
      }
      $dts3 = null;
      $est = 1;
      if ($num_dts3 > 0) {
        $est = 3;
      }
      $oCuota->modificar_campo($cuota_id, 'tb_cuota_est', $est, 'INT');
      $oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_estap', 0, 'INT'); // 0 porque deja de ser pagada POR AP
      //estado cuota
      $rws = $oCuotadetalle->filtrar($cuota_id);
      if ($rws['estado'] == 1) {
        $num_cuo = count($rws['data']);
        foreach ($rws['data'] as $key => $rw) {
          $est = 1;
          if ($rw['tb_cuotadetalle_est'] != 1) {
            $est = 3;
            break;
          }
        }
      }
      $rws = null;
      $oCuota->modificar_campo($cuota_id, 'tb_cuota_est', $est, 'INT');
      //mora
      $oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_mor', 0, 'STR');
      $oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_moraut', 0, 'INT');
    }
    //CAJA CAMBIO
    $dts2 = $oCuotapago->mostrar_cajacambio($cuopag_id);
    if ($dts2['estado'] == 1) {
      foreach ($dts2['data'] as $key => $value2) {
        $oCajacambio->modificar_campo($value2['tb_cajacambio_id'], 'tb_cajacambio_xac', 0, 'INT');


        //INGRESOS
        $mod_id = 60;
        $est = "";
        $dts3 = $oIngreso->mostrar_por_modulo($mod_id, $value2['tb_cajacambio_id'], '', $est);
        if ($dts3['estado'] == 1) {
          foreach ($dts3['data'] as $key => $value3) {
            $ing_id   = $value3['tb_ingreso_id'];
          }
        }
        $dts3 = null;
        if ($ing_id > 0) {
          $oIngreso->modificar_campo($ing_id, $_SESSION['usuario_id'], 'tb_ingreso_xac', '0');
        }


        // //EGRESOS
        $mod_id = 60;
        $est = "";
        $dts3 = $oEgreso->mostrar_por_modulo($mod_id, $value2['tb_cajacambio_id'], '', $est);
        if ($dts3['estado'] == 1) {
          foreach ($dts3['data'] as $key => $value3) {
            $egr_id   = $value3['tb_egreso_id'];
          }
        }
        $dts3 = null;
        if ($egr_id > 0) {
          $oEgreso->modificar_campo($egr_id, $_SESSION['usuario_id'], 'tb_egreso_xac', '0');
        }
      }
    }
    $dts2 = null;
  }
}
$result = null;

//devolvemos a su estado normal a las cuotasdetalle, es decir les quitamos la opción de que se pagará mediante acuerdo de pago
$array_ids = explode(',', $cuode_ids);
$cant = count($array_ids);
//parámetro cero porque ya no se pagará mediante AP
for ($i = 0; $i < $cant; $i++) {
  $oCuotadetalle->modificar_campo($array_ids[$i], 'tb_cuotadetalle_estap', 0, 'INT');
}


$result = $oCredito->mostrarUno($cre_id);
if ($result['estado'] == 1) {
  $cliente_nom = $result['data']['tb_cliente_nom'];
}
$result = null;

//cambiamos de estado al crédito, cuota y cuotas detalle para que no se listen
$oCredito->modificar_campo($cre_nuevo, 'tb_credito_xac', 0, 'INT');
//en cuota, modificar segun el credito_id, parametro 2 ya que el credito es de tipo ASIVEH
$oCuota->modificar_xac_por_credito($cre_nuevo, 0, $cre_tip);
//en cuotadetalle modificar el xac según las cuotas, parametro 2 ya que el credito es de tipo ASIVEH
$oCuotadetalle->modificar_xac_por_credito($cre_nuevo, 0, 2);
//anulamos e acuerdo de pago, xac = 1
$data['cre_nuevo'] = $cliente_nom;

$oAcuerdopago->anular_acuerdopago($acu_id, 1, $_SESSION['usuario_id'], $motivo);

$line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha anulado un Acuerdo de Pago del ' . $credito_nom . '-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT) . ', CLIENTE: ' . $cliente_nom . '. El Acuerdo de Pago tiene como número ' . $AP_SIN . '-' . str_pad($cre_nuevo, 4, "0", STR_PAD_LEFT) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
$oLineaTiempo->tb_lineatiempo_det = $line_det;
$oLineaTiempo->insertar();

$data['cre_nuevo'] = $cre_nuevo;
$data['estado'] = 1;
$data['his_msj'] = 'Se anuló correctamente.';
echo json_encode($data);
