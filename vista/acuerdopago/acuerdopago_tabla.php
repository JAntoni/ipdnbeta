<?php
	//session_start();
	require_once('../../core/usuario_sesion.php');
	require_once ("../acuerdopago/Acuerdopago.class.php");
	$oAcuerdopago = new Acuerdopago();

	require_once ("../funciones/funciones.php");

	$cre_id = $_POST['cre_id'];
	$cre_tip = $_POST['cre_tip'];

	$result = $oAcuerdopago->listar_por_credito_tipo($cre_id, $cre_tip);
	//$num_rows = mysql_num_rows($dts);
        if($result['estado']==1){
            $num_rows = count($result['data']);
        }
?>

<?php
	if($num_rows > 0){
?>
	<table id="cuota-table" class="table table-bordered table-hover">
		<thead>
			<tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
				<th id="filacabecera">FECHA GENERADA</th>
				<th id="filacabecera">INSTANCIA</th>
				<th id="filacabecera">MONTO</th>
				<th id="filacabecera">N° CUOTAS VEN.</th>
				<th id="filacabecera">INTERES</th>
				<th id="filacabecera">PERIODO</th>
				<th id="filacabecera">PAGA EN</th>
				<th id="filacabecera">ESTADO</th>
				<th id="filacabecera">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php
			foreach ($result['data'] as $key => $value) {
				$array = explode(',',$value['tb_cuotas_ids']);
				$cant_cuo = count($array);
				$simb = 'S/.';
				if($value['tb_moneda_id'] == 2)
					$simb = 'US$';
		?>
			<tr class="filaTabla">
				<td><?php echo $value['tb_acuerdopago_fecreg'] ?></td>
				<td><?php echo $value['tb_acuerdopago_ins'] ?></td>
                                <td><?php echo $simb.' '. formato_moneda($value['tb_acuerdopago_mon']); ?></td>
				<td><?php echo $cant_cuo.' Cuotas' ?></td>
				<td><?php echo '%'.$value['tb_acuerdopago_int'] ?></td>
				<td><?php
					if($value['tb_acuerdopago_per'] == 1)
						echo 'Semanal';
					elseif($value['tb_acuerdopago_per'] == 2) {
						echo 'Quincenal';
					}
					else {
						echo 'Mensual';
					}
					
				?></td>
				<td><?php echo $value['tb_acuerdopago_numcuo'].' cuotas' ?></td>
				<td>
				 <?php
				 	$apli = $value['tb_acuerdopago_apl']; //si apl tiene como valor 1 es porque su monto fue aplicado

				 	if($value['tb_acuerdopago_xac'] == 0)
				 		echo 'Activo';
				 	elseif ($value['tb_acuerdopago_xac'] == 1)
				 		echo 'Anulado';
				 	else{
				 		if($apli == 1)
				 			echo 'Resuelto / Aplicado';
				 		else
				 			echo 'Resuelto';
				 	}
				 ?>
				</td>
				<td>
					<a class="btn btn-info btn-xs" onclick="acuerdopago_form('editar',<?php echo $value['tb_acuerdopago_id'] ?>)" title="VER"><i class="fa fa-eye"></i></a>
				 <?php
				 	if($value['tb_acuerdopago_xac'] != 1){
						echo '
							<a class="btn btn-facebook btn-xs" onclick="acuerdopago_pagos('.$value['tb_acuerdopago_id'].','.$value['tb_acuerdopago_ins'].')" title="PAGOS"><i class="fa fa-money"></i></a>';
						if(($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6) && $value['tb_acuerdopago_xac'] == 0){
							echo'
								<a class="btn btn-danger btn-xs" onclick="acuerdopago_anular_form('.$value['tb_acuerdopago_id'].')" title="ANULAR"><i class="fa fa-trash"></i></a>';
							if($value['tb_acuerdopago_apr'] == 0)
								echo'
                                                                <a class="btn btn-facebook btn-xs" onclick="acuerdopago_aplicar('.$value['tb_acuerdopago_id'].')" title="APLICAR"><i class="fa fa-history"></i></a>';
						}
				 	}
				 	if($value['tb_acuerdopago_apr'] == 1 && $_SESSION['usuariogrupo_id'] == 2){
				 		echo'
							<a class="btn btn-success btn-xs" onclick="acuerdopago_aprobar('.$value['tb_acuerdopago_id'].','.$cre_tip.')" title="APROBAR"><i class="fa fa-check-square-o"></i></a>
						';
				 	}
				 ?>
				</td>
			</tr>
		<?php
			}
			$result = null;
		?>
		</tbody>
	</table>
<?php
	}
	else{
?>
	<label>No se ha generado acuerdos de pago para este crédito</label>
<?php
	}
?>