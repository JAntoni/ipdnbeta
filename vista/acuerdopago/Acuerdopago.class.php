<?php

  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Acuerdopago extends Conexion{
    public $tb_acuerdopago_id;
    public $tb_acuerdopago_fecreg;
    public $tb_acuerdopago_fec;
    public $tb_acuerdopago_fecmod;
    public $tb_acuerdopago_usureg;
    public $tb_acuerdopago_usumod;
    public $tb_acuerdopago_mot;
    public $tb_credito_id;
    public $tb_credito_nuevo;
    public $tb_cliente_id;
    public $tb_acuerdopago_mon;
    public $tb_acuerdopago_per;
    public $tb_acuerdopago_int;
    public $tb_acuerdopago_numcuo;
    public $tb_acuerdopago_ins;
    public $tb_cuotas_ids;
    public $tb_moneda_id;
    public $tb_acuerdopago_xac;
    public $tb_acuerdopago_apl;
    public $tb_credito_tip;
    public $tb_acuerdopago_apr;
    
    function insertar(){
        $this->dblink->beginTransaction();
            try {
              $sql = "INSERT into 
                        tb_acuerdopago(
					tb_acuerdopago_fecreg,
					tb_acuerdopago_fec,
					tb_credito_id,
					tb_credito_tip,
					tb_cliente_id,
					tb_acuerdopago_mon,
					tb_acuerdopago_per,
					tb_acuerdopago_int,
					tb_acuerdopago_numcuo,
					tb_acuerdopago_ins,
					tb_cuotas_ids,
					tb_moneda_id,
                                        tb_acuerdopago_usureg,
                                        tb_acuerdopago_apr) 
                                values (
                                        NOW(),
                                        :tb_acuerdopago_fec,
					:tb_credito_id,
					:tb_credito_tip,
					:tb_cliente_id,
					:tb_acuerdopago_mon,
					:tb_acuerdopago_per,
					:tb_acuerdopago_int,
					:tb_acuerdopago_numcuo,
					:tb_acuerdopago_ins,
					:tb_cuotas_ids,
					:tb_moneda_id,
                                        :tb_acuerdopago_usureg,
                                        1)";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_acuerdopago_fec",$this->tb_acuerdopago_fec, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_credito_id",$this->tb_credito_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_credito_tip",$this->tb_credito_tip, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cliente_id",$this->tb_cliente_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_acuerdopago_mon",$this->tb_acuerdopago_mon, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_acuerdopago_per",$this->tb_acuerdopago_per, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_acuerdopago_int",$this->tb_acuerdopago_int, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_acuerdopago_numcuo",$this->tb_acuerdopago_numcuo, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_acuerdopago_ins",$this->tb_acuerdopago_ins, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_cuotas_ids",$this->tb_cuotas_ids, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_moneda_id",$this->tb_moneda_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_acuerdopago_usureg",$this->tb_acuerdopago_usureg, PDO::PARAM_INT);
              

              $result = $sentencia->execute();
              $acuerdopago_id = $this->dblink->lastInsertId();
              $this->dblink->commit();

              $data['estado'] = $result;
              $data['tb_acuerdopago_id'] = $acuerdopago_id;
              return $data; //si es correcto el ingreso retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
    }
    
    function listar_por_credito_tipo($cre_id, $cre_tip){
      try {
        $sql = "SELECT * from tb_acuerdopago where tb_credito_id =:tb_credito_id and tb_credito_tip =:tb_credito_tip";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  
    
    function mostrarUno($acu_id){
      try {
        $sql = "SELECT * FROM `tb_acuerdopago` ac inner join tb_cliente c on c.tb_cliente_id = ac.tb_cliente_id where tb_acuerdopago_id =:tb_acuerdopago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function agregar_cuotasdetalle_acuerdopago($acu_id,$cud_id,$num){
        $this->dblink->beginTransaction();
            try {
              $sql = "INSERT INTO tb_acuerdodetalle (
                                                    tb_acuerdopago_id,
                                                    tb_cuotadetalle_id,
                                                    tb_acuerdodetalle_num) 
                                            values(
                                                    :tb_acuerdopago_id,
                                                    :tb_cuotadetalle_id,
                                                    :tb_acuerdodetalle_num)";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_acuerdopago_id",$acu_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cuotadetalle_id",$cud_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_acuerdodetalle_num",$num, PDO::PARAM_INT);

              $result = $sentencia->execute();
              $tb_acuerdopago_id = $this->dblink->lastInsertId();
              $this->dblink->commit();

              $data['estado'] = $result;
              $data['tb_acuerdopago_id'] = $tb_acuerdopago_id;
              return $data; //si es correcto el ingreso retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
    }

    function listar_cuotas_monto_acuerdopago($acu_id){
      try {
        $sql = "SELECT ac.tb_acuerdopago_id, cud.* FROM `tb_acuerdopago` ac inner join tb_acuerdodetalle acd on acd.tb_acuerdopago_id = ac.tb_acuerdopago_id inner join tb_cuotadetalle cud on cud.tb_cuotadetalle_id = acd.tb_cuotadetalle_id where ac.tb_acuerdopago_id =:tb_acuerdopago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  

    function listar_cuotas_en_acuerdopago($acu_id){
        try{
            $sql = "SELECT cud.tb_cuotadetalle_fec,cud.tb_cuotadetalle_id,cud.tb_cuotadetalle_cuo,cud.tb_cuotadetalle_est,(case when tb_cuotapago_xac =1 then SUM(tb_ingreso_imp) else 0 end) as importe,tb_cuotapago_xac,tb_ingreso_xac FROM tb_acuerdodetalle acud  
                    left join tb_cuotadetalle cud on acud.tb_cuotadetalle_id = cud.tb_cuotadetalle_id
                    left join tb_cuotapago cp on cp.tb_cuotapago_modid = acud.tb_cuotadetalle_id
                    left join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id
                    where acud.tb_acuerdopago_id =:tb_acuerdopago_id GROUP by 2";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  

    function total_importe_cuotas_acuerdopago($acu_id){
        try{
            $sql = "SELECT 
                            IFNULL(SUM(ing.tb_ingreso_imp),0) as importe
                    FROM 
                            tb_cuotadetalle cud inner join tb_acuerdodetalle acud on acud.tb_cuotadetalle_id = cud.tb_cuotadetalle_id 
                            inner join tb_cuotapago cp on cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id 
                            inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id 
                    where 
                            cp.tb_modulo_id = 2 and acud.tb_acuerdopago_id =:tb_acuerdopago_id and cp.tb_cuotapago_xac = 1 and tb_ingreso_xac = 1";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  

    function siguiente_numero_instancia($cre_id, $cre_tip){
        try{
            $sql ="SELECT count(*) as num_ins FROM `tb_acuerdopago` where tb_credito_id =:tb_credito_id and tb_credito_tip =:tb_credito_tip and tb_acuerdopago_xac != 1";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_credito_tip", $cre_tip, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  

    function informacion_acuerdopago_por_cuotadetalleid($cud_id){
        try{
            $sql =" SELECT 
                            ac.*,tb_cuotadetalle_id,tb_acuerdodetalle_num 
                    from 
                            tb_acuerdopago ac
                            inner join tb_acuerdodetalle ad on  ad.tb_acuerdopago_id=ac.tb_acuerdopago_id 
                    where 
                            tb_cuotadetalle_id =:tb_cuotadetalle_id";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuotadetalle_id", $cud_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  
    
    function anular_acuerdopago($acu_id,$valor,$usu,$motivo){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_acuerdopago set 
                                        tb_acuerdopago_xac =:tb_acuerdopago_xac,
                                        tb_acuerdopago_usumod =:tb_acuerdopago_usumod,
                                        tb_acuerdopago_mot = :tb_acuerdopago_mot,
                                        tb_acuerdopago_fecmod =NOW() 
                                    where 
                                        tb_acuerdopago_id =:tb_acuerdopago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_acuerdopago_xac", $valor, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_acuerdopago_usumod", $usu, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_acuerdopago_mot", $motivo, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function agregar_nuevo_credito_al_original($acu_id,$cre_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_acuerdopago set tb_credito_nuevo =:tb_credito_nuevo where tb_acuerdopago_id =:tb_acuerdopago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_nuevo", $cre_id, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function resolver_acuerdopago($usu, $acu_id, $valor){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_acuerdopago set 
                                        tb_acuerdopago_fecmod =NOW(), 
                                        tb_acuerdopago_usumod =:tb_acuerdopago_usumod,
                                        tb_acuerdopago_xac =:tb_acuerdopago_xac 
                                where 
                                        tb_acuerdopago_id =:tb_acuerdopago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_acuerdopago_usumod", $usu, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_acuerdopago_xac", $valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar_acuerdopago_credito_xac($cre_id, $cre_tip,$valor, $cli_id){
        try{
            $sql ="SELECT 
                            * 
                    FROM 
                            `tb_acuerdopago` 
                    where 
                            tb_credito_id =:tb_credito_id and tb_credito_tip =:tb_credito_tip and tb_acuerdopago_xac =:tb_acuerdopago_xac and tb_cliente_id =:tb_cliente_id";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_credito_tip", $cre_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_acuerdopago_xac", $valor, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  
    
    function mostrar_por_creditonuevo($cre_id, $cre_tip){
        try{
            $sql ="SELECT * FROM `tb_acuerdopago` where tb_credito_nuevo =:tb_credito_nuevo and tb_credito_tip =:tb_credito_tip";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_nuevo", $cre_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_credito_tip", $cre_tip, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  
    
    function filtrar_por_cuotadetale($cuodetid){
        try{
            $sql ="SELECT * FROM tb_acuerdopago WHERE tb_cuotas_ids LIKE '%$cuodetid%' and tb_acuerdopago_xac <> 1 "; //buscando todos a excepcion de anulados(<>1).
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  
    
    function modificar_campo($acu_id, $cuotapago_columna, $cuotapago_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($cuotapago_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_acuerdopago SET `tb_acuerdopago_$cuotapago_columna` =:tb_acuerdopago WHERE tb_acuerdopago_id =:tb_acuerdopago_id"; 
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_acuerdopago_id", $acu_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":tb_acuerdopago", $cuotapago_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":tb_acuerdopago", $cuotapago_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar_por_credito_aplicar($cre_id, $cre_tip, $cli_id){
        try{
            $sql = "SELECT * from tb_acuerdopago where tb_credito_id =$cre_id AND tb_credito_tip =$cre_tip AND tb_acuerdopago_apl = 1 AND tb_cliente_id =$cli_id order by tb_credito_id";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    } 
    
    function listar_ap_aplicar_por_cliente($cre_tip, $cli_id){
        try{
            $sql = "SELECT * from tb_acuerdopago where tb_credito_tip =$cre_tip AND tb_acuerdopago_apl = 1 AND tb_cliente_id =$cli_id order by tb_credito_id";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }  
}
