$(document).ready(function () {
  acuerdopago_form_tabla();

  $("#datetimepicker1111").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  $("#datetimepicker1111").on("change", function (e) {
    var startVal = $("#txt_acu_fec").val();
    $("#datetimepicker1111").data("datepicker").setStartDate(startVal);
  });

  $("#cmb_acu_peri").change(function (event) {
    generar_tabla_acuerdo();
  });

  $("#for_acuerdopago").validate({
    submitHandler: function () {
      var acuerdo = Number($("#hdd_his_mon_acu").val()); //monto total para el acuerdo
      var act = $("#hdd_action").val().trim();
      var acu_cuotas = Number($("#txt_acu_numcuo").val()); //cuotas supuestas a dar
      var cuo_disponible = Number($("#hdd_num_rows_acu").val()); // numero de filas

      if ((isNaN(acuerdo) || acuerdo <= 0) && act != "editar") {
        alert("Selecione las cuotas para generar un acuerdo");
        return false;
      }
      if (cuo_disponible < acu_cuotas) {
        alert(
          "No se puede generar " +
            acu_cuotas +
            " cuotas para " +
            cuo_disponible +
            " fechas."
        );
        $("#txt_acu_numcuo").focus();
        return false;
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL + "acuerdopago/acuerdopago_reg.php",
        async: true,
        dataType: "json",
        data: $("#for_acuerdopago").serialize(),
        beforeSend: function () {
          //                    $('#msj_credito_historial').html('Guardando acuerdo de pago...');
          //                    $('#msj_credito_historial').show();
          //                    $('#msj_acuerdopago').text('Guardando...');
          //                    $('#msj_acuerdopago').show();
        },
        success: function (data) {
          if (parseInt(data.estado) == 1) {
            alerta_success("EXITO", data.msj);
            $("#modal_creditogarveh_acuerdo").modal("hide");
            $("#msj_credito_historial").html("Acuerdo de pago generado.");
            acuerdopago_tabla();
            //credito_historial_tabla();
            //generar pdf
          } else if (parseInt(data.estado) == 2) {
            alerta_warning("ERROR", data.msj);
          } else {
            //$('#msj_acuerdopago').html('Error: ' + data);
            //console.log(data);
            alerta_error("ERROR", data.msj);
          }
        },
        complete: function (data) {
          console.log(data);
          //                    if (data.statusText != "success") {
          //                        alert('Reporte este error al guardar un acuerdo de pago');
          //                        //console.log(data.responseText);
          //
          //                    }
        },
      });
    },
    rules: {
      hdd_cre_id: {
        required: true,
      },
      hdd_cli_id: {
        required: true,
      },
      hdd_mon_acu: {
        required: true,
      },
      cmb_acu_peri: {
        required: true,
      },
      txt_acu_numcuo: {
        required: true,
      },
    },
    messages: {
      hdd_cre_id: {
        required: "No es un credito válido",
      },
      hdd_cli_id: {
        required: "NO es un cliente válido",
      },
      hdd_mon_acu: {
        required: "No se selecciono cuotas vencidas",
      },
      cmb_acu_peri: {
        required: "Elija un periodo",
      },
      txt_acu_numcuo: {
        required: "Ingrese número de cuotas",
      },
    },
  });
});

function generar_tabla_acuerdo() {
  if ($("#txt_acu_numcuo").val() <= 0) {
    $("#txt_acu_numcuo").focus();
    return false;
  }
  $.ajax({
    type: "POST",
    url: VISTA_URL + "acuerdopago/acuerdopago_generar_tabla_acuerdo.php",
    async: true,
    dataType: "html",
    data: $("#for_acuerdopago").serialize(),
    beforeSend: function () {
      //$('#div_acuerdopago_form').dialog('open');
    },
    success: function (html) {
      //console.log(html);
      //$('#lng_tabla_acu').text('Tabla de Acuerdos de Pago');
      $("#div_acu_tbl_generado").html(html);
    },
    complete: function (data) {
      //console.log(data);
    },
  });
}

function acuerdopago_form_tabla() {
  if ($("#hdd_acu_id").val().trim() == "") {
    return false;
  }
  $.ajax({
    type: "POST",
    url: VISTA_URL + "acuerdopago/acuerdopago_form_tabla.php",
    async: true,
    dataType: "html",
    data: {
      acu_id: $("#hdd_acu_id").val(),
    },
    beforeSend: function () {
      //$('#div_acuerdopago_form').dialog('open');
    },
    success: function (html) {
      //console.log(html);
      $("#lng_tabla_acu").text("Tabla de Acuerdos de Pago");
      $("#div_acu_tbl_generado").html(html);
    },
    complete: function () {},
  });
}
