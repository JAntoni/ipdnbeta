$(document).ready(function () {
    $('#for_anular_acu').submit(function (event) {
        event.preventDefault();
        if ($('#txt_motivo').val() == '') {
            $('#txt_motivo').focus();
            return false;
        }
        $.ajax({
            type: "POST",
            url: VISTA_URL +"acuerdopago/acuerdopago_anular.php",
            async: true,
            dataType: "json",
            data: ({
                acu_id: $('#acu_id').val(),
                motivo: $('#txt_motivo').val(),
                cre_tip: $('#hdd_cre_tip').val()
            }),
            beforeSend: function () {
//                $('#msj_credito_historial').html('Anulando acuerdo de pago...');
//                $('#msj_credito_historial').show();
            },
            success: function (data) {
                if (parseInt(data.estado) > 0) {
                    alerta_success("EXITO",data.his_msj);
                    $('#modal_anular_acuerdopago').modal('hide');
                    acuerdopago_tabla();
//                    $('#for_anular_acu').trigger('reset');
                    
                } else {
//                    $('#lbl_error_ac').text('error: ' + data.his_msj);
                    alerta_error("ERROR",data.his_msj);
                    console.log(data);
                }
            },
            complete: function (data) {
                //console.log(data);
//                if (data.statusText != "success") {
//                    alert('Reporte este error a sistemas: Error al anular acuerdo de pago');
//                    $('#div_acuerdopago_anular_form').html(data.responseText);
//                    //console.log(data);
//                }
            }
        });
    });

});
