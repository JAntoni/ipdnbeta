<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../../core/usuario_sesion.php');
require_once("Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$acu_id = $_POST['acu_id'];
$num_rows = 0;
if (isset($acu_id)) {
	//echo $cre_id.' / '.$num_cuo.' / '.$monto;
	$result = $oAcuerdopago->listar_cuotas_monto_acuerdopago($acu_id);
	if ($result['estado'] == 1) {
		$num_rows = count($result['data']);
	}
}
if ($num_rows > 0) {
?>
	<input type="hidden" name="hdd_num_rows_acu" id="hdd_num_rows_acu" value="<?php echo $num_rows; ?>">
	<table class="table table-bordered table-hover" border="0">
		<thead>
			<tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
				<th id="filacabecera">ID CUO. DET.</th>
				<th id="filacabecera">FECHA DE PAGO</th>
				<th id="filacabecera">MONTO ACUERDO</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if ($result['estado'] == 1) {
				foreach ($result['data'] as $key => $value) {
					$monto_pagar = $value['tb_cuotadetalle_cuo'];
					$simbolo = 'S./';
					if ($value['tb_moneda_id'] == 2)
						$simbolo = 'US$';
			?>
					<tr class="filaTabla">
						<td><?php echo $value['tb_cuotadetalle_id'] ?></td>
						<td><?php echo mostrar_fecha($value['tb_cuotadetalle_fec']); ?></td>
						<td><?php echo $simbolo . ' ' . formato_moneda($value['tb_cuotadetalle_cuo']); ?></td>
					</tr>
			<?php
				}
			}
			$result = null;
			?>
		</tbody>
	</table>
<?php } ?>