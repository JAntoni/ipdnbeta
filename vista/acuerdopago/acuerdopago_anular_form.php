<?php
//session_start();
require_once('../../core/usuario_sesion.php');
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();

$acu_id = $_POST['acu_id'];
$cre_tip = $_POST['cre_tip'];

$result = $oAcuerdopago->mostrarUno($acu_id);
if ($result['estado'] == 1) {
  $cre_id = $result['data']['tb_credito_id'];
  $cuode_ids = $result['data']['tb_cuotas_ids'];
  $cre_nuevo = $result['data']['tb_credito_nuevo']; //id del credito nuevo en base al orginal
}
$result = null;

//verificamos primero si tiene ingresos en el AP
$mon_id = 1; // en soles
$result = $oIngreso->ingreso_total_por_credito_cuotadetalle($cre_nuevo, $cre_tip, $mon_id);
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $ingreso_soles = floatval($value['importe_total']);
  }
}
$result = null;

$mon_id = 2; // en dólares
$dts = $oIngreso->ingreso_total_por_credito_cuotadetalle($cre_nuevo, $cre_tip, $mon_id);
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $ingreso_dolares = floatval($value['importe_total']);
  }
}
$result = null;

if (intval($ingreso_soles) > 0 || intval($ingreso_dolares) > 0) {
  echo '<h2 style="color: red;">NO SE PUEDE ANULAR ESTE ACUERDO DE PAGO YA QUE TIENE INGRESOS EN SUS CUOTAS, VERIFICAR POR FAVOR CON ADMINISTRACIÓN AP: ' . $cre_nuevo . ', TIENE ' . $ingreso_soles . ' INGRESOS EN SOLES Y ' . $ingreso_dolares . ' INGRESOS EN DÓLARES</h2>';
  exit();
}


?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_anular_acuerdopago" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">ANULAR ACUERDO DE PAGO</h4>
      </div>
      <form id="for_anular_acu">
        <input type="hidden" name="acu_id" id="acu_id" value="<?php echo $acu_id ?>">
        <input type="hidden" name="hdd_cre_tip" id="hdd_cre_tip" value="<?php echo $cre_tip ?>">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 col-lg-12">
                      <label for="txt_motivo" class="control-label">Motivo de la anulación :</label>
                      <div class="form-group">
                        <textarea name="txt_motivo" id="txt_motivo" rows="3" cols="50" class="form-control input-sm"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardar_anulacion">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/acuerdopago/acuerdopago_anular_form.js?ver=437433'; ?>"></script>