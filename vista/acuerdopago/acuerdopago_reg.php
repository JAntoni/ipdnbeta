<?php
//session_start();
require_once('../../core/usuario_sesion.php');
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['hdd_action'];
$det_cuo_ids = $_POST['hdd_cuo_ids']; // ids de los detalles de cuotas
$mon_acu = $_POST['txt_acu_mon']; //suma total del monto para el acuerdo
$cre_id = $_POST['hdd_cre_id']; //id del crédito
$cre_tip = $_POST['hdd_cre_tip']; //tipo del credito 2 asiveh, 3 garveh, 4 hipo
$cli_id = $_POST['hdd_cli_id']; // id del cliente
$mon_id = $_POST['hdd_mon_id']; //id de la moneda en que se paga la cuota
$periodo = $_POST['cmb_acu_peri']; //periodo 1 semana, 2 quincena, 4 mensual
$int = $_POST['txt_acu_int']; //interes aplicado al acuerdo de pago
$num_cuo = $_POST['txt_acu_numcuo']; //numero de cuotas para el acuerdo
$fecha_acu = $_POST['txt_acu_fec'];

$cuota_ids = $_POST['hdd_cuota_id'];
$cuota_fechas = $_POST['hdd_cuota_fec'];
$cuotadetalle_fecs = $_POST['hdd_cuotadetalle_fec']; //fechas de las cuotasdetalle


/*$distinct_cuota_ids = array_unique($cuota_ids);
sort($distinct_cuota_ids);

echo json_encode($distinct_cuota_ids);
exit();*/

//echo $det_cuo_ids.' moacu/'.$mon_acu.' creid/'.$cre_id.' clid/'.$cli_id.' per/'.$periodo.' in/'.$int.' num/'.$num_cuo;
if ($action == 'insertar' && !empty($cre_tip)) {
	$result = $oAcuerdopago->siguiente_numero_instancia($cre_id, $cre_tip);
	if ($result['estado'] == 1) {
		$num_ins = $result['data']['num_ins']; //numero de instancia
	}
	$result = null;

	if ($num_ins >= 3) {
		$data['estado'] = 2; //estado 2 de error
		$data['msj'] = 'Este crédito ya tiene 3 instancias';
		echo json_encode($data);
		exit();
		//$num_ins = 2;
	}
	if (!empty($cuota_ids) && !empty($cuota_fechas)) {
		$usureg = $_SESSION['usuario_id'];
		$num_ins = $num_ins + 1; //el siguiente número de la instancia
		$oAcuerdopago->tb_acuerdopago_fec = fecha_mysql($fecha_acu);
		$oAcuerdopago->tb_credito_id = $cre_id;
		$oAcuerdopago->tb_credito_tip = $cre_tip;
		$oAcuerdopago->tb_cliente_id = $cli_id;
		$oAcuerdopago->tb_acuerdopago_mon =  moneda_mysql($mon_acu);
		$oAcuerdopago->tb_acuerdopago_per = $periodo;
		$oAcuerdopago->tb_acuerdopago_int = $int;
		$oAcuerdopago->tb_acuerdopago_numcuo = $num_cuo;
		$oAcuerdopago->tb_acuerdopago_ins = $num_ins;
		$oAcuerdopago->tb_cuotas_ids = $det_cuo_ids;
		$oAcuerdopago->tb_moneda_id = $mon_id;
		$oAcuerdopago->tb_acuerdopago_usureg = $usureg;
		$result = $oAcuerdopago->insertar();
		if (intval($result['estado']) == 1) {
			$acuerdo_id = $result['tb_acuerdopago_id'];
		}
		$result = null;

		//		$rsp = $oAcuerdopago->insertar(fecha_mysql($fecha_acu), $cre_id, $cre_tip, $cli_id, moneda_mysql($mon_acu), 
		//                                                $periodo, $int, $num_cuo, $num_ins, $det_cuo_ids, 
		//                                                $mon_id, $usureg);
		//
		//		$dts = $oAcuerdopago->ultimoInsert();
		//		$dt = mysql_fetch_array($dts);
		//		$acuerdo_id = $dt['last_insert_id()'];
		//		mysql_free_result($dts);

		if ($acuerdo_id > 0) {
			$cuoids = explode(',', $det_cuo_ids);
			$cant = count($cuoids);

			/* AGREGAR PAGOS POR AP, SE HARÁ DESPUÉS DE APROBAR EL ACUERDO DE PAGO
			el estap (estado acuerdopago) será = 1, es decir, se pagara por acuerdo de pago
			for ($i=0; $i < $cant; $i++) {
				$oCuotadetalle->agregar_acuerdopago($cuoids[$i]);
			}*/

			$subper_id = 1; //mensual
			if ($periodo == 1)
				$subper_id = 3; //semanal
			if ($periodo == 2)
				$subper_id = 2; //quincenal

			$cre_cod = '';
			$credito_nom = '';
			if ($cre_tip == 2) {
				require_once("../creditoasiveh/Creditoasiveh.class.php");
				$oCredito = new Creditoasiveh();
				$cre_cod = 'CAV';
				$credito_nom = 'Crédito ASIVEH';
			}
			if ($cre_tip == 3) {
				require_once("../creditogarveh/Creditogarveh.class.php");
				$oCredito = new Creditogarveh();
				$cre_cod = 'CGV';
				$credito_nom = 'Crédito GARVEH';
			}
			if ($cre_tip == 4) {
				require_once("../creditohipo/Creditohipo.class.php");
				$oCredito = new Creditohipo();
				$cre_cod = 'CH';
				$credito_nom = 'Crédito HIPOTECARIO';
			}
			//generamos un nuevo credito, con los datos del credito original. Parametro 3 porque es de tipo: acuerdo pago
			if ($cre_tip == 2) {
				$result = $oCredito->registrar_acuerdopago(intval($cre_id), 3, intval($subper_id), moneda_mysql($mon_acu), moneda_mysql($int), intval($num_cuo), intval($usureg));
			}

			if ($cre_tip != 2) {
				$result = $oCredito->registrar_acuerdopago(intval($cre_id), 3, intval($subper_id), moneda_mysql($mon_acu), moneda_mysql($int), intval($num_cuo), intval($usureg));
			}

			if (intval($result['estado']) == 1) {
				$cre_id_new = $result['credito_id'];
			}
			$result = null;

			//antes de que aparezca el AP, primero ADMINISTRACIÓN DEBE APROBARLO
			$oCredito->modificar_campo($cre_id_new, 'tb_credito_xac', 0, 'INT'); //pendiente para aprobación por ADMIN                

			//registramos el historial para el crédito original
			$his = '<span style="color: #10DCF7;">Se ha creado un <b>ACUERDO DE PAGO</b> de este crédito con ID: ' . $cre_cod . '-' . $cre_id_new . '. AP creado por: <b>' . $_SESSION['usuario_nombre'] . '</b> | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
			$oCredito->registro_historial($cre_id, $his);

			//registramos el historial para el crédito nuevo o AP
			$his = '<span>Acuerdo de Pago creado por: <b>' . $_SESSION['usuario_nombre'] . '</b>. Detalle: este AP fue creado en base al crédito matriz ' . $cre_cod . '-' . $cre_id . ' | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
			$oCredito->registro_historial($cre_id_new, $his);

			$line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha creado un Acuerdo de Pago para el ' . $credito_nom . ' de número: ' . $cre_cod . '-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT) . '. El Acuerdo de Pago tiene como número AP-' . $cre_cod . '-' . str_pad($cre_id_new, 4, "0", STR_PAD_LEFT) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
			$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
			$oLineaTiempo->tb_lineatiempo_det = $line_det;
			$oLineaTiempo->insertar();

			//guardamos el id del nuevo credito generado para el credito original
			$oAcuerdopago->agregar_nuevo_credito_al_original($acuerdo_id, $cre_id_new);
			//cuotas para el nuevo credito, tomando como fecha de la siguiente cuota sin vencer
			$int = 0; // INTERES AHORA ES 0 SIEMPRE
			//CUOTAS
			$C = moneda_mysql($mon_acu);
			$i = moneda_mysql($int);
			$n = $num_cuo;

			if ($i > 0) {
				$uno = $i / 100;
				$dos = $uno + 1;
				$tres = pow($dos, $n);
				$cuatroA = ($C * $uno) * $tres;
				$cuatroB = $tres - 1;
				$R = $cuatroA / $cuatroB;
				$r_sum = $R * $n;
			} else
				$R = $C / $num_cuo;
			/*
			$mon_acu = moneda_mysql($mon_acu);
			$int = moneda_mysql($int);
			$interes = $mon_acu*$int;
			$monto_total = $mon_acu + $interes;
			$mon_subcuo = $monto_total / $num_cuo;*/

			//se toma como referencia los ids y fechas de las cuotas siguientes a las vencidas, entonces hay repetidas, a las cuales limpiamos repetidas y contamos cuantos son
			//nos quedamos solo con los ids de las cuotas
			$distinct_cuota_ids = array_unique($cuota_ids);
			sort($distinct_cuota_ids);
			$distinct_cuota_fec = array_unique($cuota_fechas);
			sort($distinct_cuota_fec);

			$total_cuota_ids = count($distinct_cuota_ids);

			//cantidad en que se repite el id de la cuota a tomar como referencia
			$cant_por_cuota_id = array_count_values($cuota_ids);

			$cont_fec = 0;

			for ($j = 0; $j < $total_cuota_ids; $j++) {

				$xac = 1;
				$cretip_id = $cre_tip; //credito tipo 1=menor 2=asive 3=garve 4=hipo
				$est = 1;
				$persubcuo = 1; // los acuerdo de pago se pagan como si fueran mensuales, una cuota con una cuotadetalle

				if ($i != 0) {
					if ($j > 1) {
						$C = $C - $amo;
						$int = $C * ($i / 100);
						$amo = $R - $int;
					} else {
						$int = $C * ($i / 100);
						$amo = $R - $int;
					}
				}
				//parametro 10 para hacer una validacion antes de guardar, indicar que esta cuota es de acuerdo de pago
				//				$oCuota->insertar(
				//					$xac,
				//					$cre_id_new,
				//					$cretip_id,
				//					$mon_id,
				//					$j+1,
				//					fecha_mysql($distinct_cuota_fec[$j]), 
				//					$C, 
				//					$amo,
				//					$int,
				//					$R, 
				//					$pro,
				//					$persubcuo,
				//					10
				//				);
				//
				//				$dts=$oCuota->ultimoInsert();
				//					$dt = mysql_fetch_array($dts);
				//					$cuo_id=$dt['last_insert_id()'];
				//				mysql_free_result($dts);

				$oCuota->credito_id = $cre_id_new;
				$oCuota->creditotipo_id = $cretip_id;
				$oCuota->moneda_id = $mon_id;
				$oCuota->cuota_num = $j+1;
				$oCuota->cuota_fec = fecha_mysql($distinct_cuota_fec[$j]);
				$oCuota->cuota_cap = $C;
				$oCuota->cuota_amo = intval($amo);
				$oCuota->cuota_int = $int;
				$oCuota->cuota_cuo = $R;
				$oCuota->cuota_pro = intval($pro);
				$oCuota->cuota_persubcuo = $persubcuo;
				$oCuota->cuota_est = 1;
				$oCuota->cuota_acupag = 1;
				$oCuota->cuota_interes = $i;

				$result = $oCuota->insertar();
				if (intval($result['estado']) == 1) {
					$cuo_id = $result['cuota_id'];
				}
				$result = null;


				//ejemplo: si el id de la cuota de referencia "123", se repite dos veces entonces tiene dos subcuotas para insertar
				$num = $cant_por_cuota_id[$distinct_cuota_ids[$j]];

				//parametro 10, para hacer una validacion antes de insertar, para indicar que este es un acuerdo de pago
				for ($k = 0; $k < $num; $k++) {

					//					$oCuotadetalle->insertar(
					//						$xac,
					//						$cuo_id,
					//						$mon_id,
					//						$k+1,
					//						fecha_mysql($cuotadetalle_fecs[$cont_fec]), 
					//						$R,
					//						10
					//					);
					//					
					//					$dts = $oCuotadetalle->ultimoInsert();
					//						$dt = mysql_fetch_array($dts);
					//						$cuode_id = $dt['last_insert_id()'];
					//					mysql_free_result($dts);
					$oCuotadetalle->tb_cuota_id = $cuo_id;
					$oCuotadetalle->tb_moneda_id = $mon_id;
					$oCuotadetalle->tb_cuotadetalle_num = $k + 1;
					$oCuotadetalle->tb_cuotadetalle_fec = fecha_mysql($cuotadetalle_fecs[$cont_fec]);
					$oCuotadetalle->tb_cuotadetalle_cuo = $R;
					$oCuotadetalle->tb_cuotadetalle_est = 10;
					$oCuotadetalle->tb_cuotadetalle_acupag = 0;

					$result = $oCuotadetalle->insertar();
					if (intval($result['estado']) == 1) {
						$cuotadetalle_id = $result['cuotadetalle_id'];
					}
					$result = null;

					$oAcuerdopago->agregar_cuotasdetalle_acuerdopago($acuerdo_id, $cuotadetalle_id, $cont_fec + 1);

					$cont_fec++;
				}
			}

			//$data['cre_id']=$cre_id;
			//$data['mon_id']=$_POST['cmb_mon_id'];
			$data['estado'] = 1; //estado 1 todo correcto
			$data['msj'] = 'Se registró correctamente.';
		}
	} else {
		$data['estado'] = 2; //estado 2 de error
		$data['msj'] = 'Primero calcule las nuevas fechas de pago.';
	}
	echo json_encode($data);
}
if ($action == 'editar') {
}

if ($action == 'aprobar') {
	$acu_id =  $_POST['acu_id'];
	$cre_tip =  $_POST['cre_tip'];

	if ($cre_tip == 2) {
		require_once("../creditoasiveh/Creditoasiveh.class.php");
		$oCredito = new Creditoasiveh();
	}
	if ($cre_tip == 3) {
		require_once("../creditogarveh/Creditogarveh.class.php");
		$oCredito = new Creditogarveh();
	}
	if ($cre_tip == 4) {
		require_once("../creditohipo/Creditohipo.class.php");
		$oCredito = new Creditohipo();
	}

	$result = $oAcuerdopago->mostrarUno($acu_id);
	if ($result['estado'] == 1) {
		$cre_id = $result['data']['tb_credito_id'];
		$cuode_ids = $result['data']['tb_cuotas_ids'];
		$cre_nuevo = $result['data']['tb_credito_nuevo']; //id del credito nuevo en base al orginal
	}
	$result = null;

	$cuoids = explode(',', $cuode_ids);
	$cant = count($cuoids);

	//el estap (estado acuerdopago) será = 1, es decir, se pagara por acuerdo de pago
	for ($i = 0; $i < $cant; $i++) {
		$oCuotadetalle->agregar_acuerdopago($cuoids[$i]);
	}

	//VAMOS A APROBAR LOS ACUERDOS DE PAGO DEL CLIENTE
	$oAcuerdopago->modificar_campo($acu_id, 'apr', 0, 'INT'); //aprobado
	$oCredito->modificar_campo($cre_nuevo, 'tb_credito_xac', 1, 'INT'); //AP AHORA APARECE

	$data['msj'] = 'ACUERDO DE PAGO APROBADO';
	$data['estado'] = 1;

	echo json_encode($data);
}
