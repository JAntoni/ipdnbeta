<?php
try {
	require_once('../../core/usuario_sesion.php');
	require_once("../cuota/Cuota.class.php");
	require_once("../funciones/funciones.php");
	require_once("../funciones/fechas.php");

	$oCuota = new Cuota();

	$cre_id = $_POST['hdd_cre_id'];
	$cre_tip = $_POST['hdd_cre_tip'];
	$num_cuo = intval($_POST['txt_acu_numcuo']);
	$monto = $_POST['txt_acu_mon'];
	//$interes = $_POST['txt_acu_int'];
	$periodo = $_POST['cmb_acu_peri'];
	$fecha = $_POST['txt_acu_fec'];
	$cuotip_id = intval($_POST['hdd_cuotip_id']);
	$cre_numcuomax = intval($_POST['hdd_cre_numcuomax']);
	$moneda_id = intval($_POST['hdd_mon_id']);

	//echo $cre_id.' / '.$num_cuo.' / '.$monto;
	$result = $oCuota->listar_cuotas_credito_sin_acuerdo($cre_id, $cre_tip, $num_cuo, $periodo, fecha_mysql($fecha));
	if ($result['estado'] == 1) {
		$num_rows = count($result['data']);
	}
	$monto_cal = $monto / $num_cuo;

	//VAMOS A OBTENER LOS DATOS PARA UN CRONOGRAMA DE CREDITO LIBRE
	if ($cuotip_id == 3) { // SOLO PARA CREDITOS LIBRES
		$num_rows = $num_cuo;
		$result1 = $oCuota->ultima_cuota_por_credito($cre_id, $cre_tip);
		if ($result1['estado'] == 1) {
			$cuota_num = $result1['data']['tb_cuota_num'];
		}
		$result1 = null;

		list($day, $month, $year) = explode('-', $fecha); //

		$cuotas_restantes = $cre_numcuomax - $cuota_num; // meses restantes por pagar el crédito LIBRE
		if ($num_cuo > $cuotas_restantes) {
			echo '<h3 style="color: red;">No se puede generar ' . $num_cuo . ' cuotas, solo le quedan ' . $cuotas_restantes . ' cuotas restantes. Cuotas total del Drédito: ' . $cre_numcuomax . ', cuota actual: ' . $cuota_num . '</h3>';
			exit();
		}
	}
} catch (Exception $e) {
	throw $e;
}
?>
<?php if ($cuotip_id == 4): ?>
	<input type="hidden" name="hdd_num_rows_acu" id="num_rows" value="<?php echo $num_rows; ?>">
	<input type="hidden" name="hdd_num_rows_acu" id="cre_id" value="<?php echo $cre_id; ?>">
	<input type="hidden" name="hdd_num_rows_acu" id="cre_tip" value="<?php echo $cre_tip; ?>">
	<input type="hidden" name="hdd_num_rows_acu" id="num_cuo" value="<?php echo $num_cuo; ?>">
	<input type="hidden" name="hdd_num_rows_acu" id="monto" value="<?php echo $monto; ?>">
	<!--input type="hidden" name="hdd_num_rows_acu" id="interes" value="<?php echo $interes; ?>"-->
	<input type="hidden" name="hdd_num_rows_acu" id="periodo" value="<?php echo $periodo; ?>">
	<input type="hidden" name="hdd_num_rows_acu" id="fecha" value="<?php echo $fecha; ?>">
	<input type="hidden" name="hdd_num_rows_acu" id="cuotip_id" value="<?php echo $cuotip_id; ?>">
	<input type="hidden" name="$moneda_id" id="moneda_id" value="<?php echo $moneda_id; ?>">
	<table class="table table-bordered table-hover" border="0">
		<thead>
			<tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
				<th id="filacabecera">ID CUO. DET.</th>
				<th id="filacabecera">FECHA</th>
				<th id="filacabecera">MONTO CUOTA</th>
				<th id="filacabecera">MONTO ACUERDO</th>
				<th id="filacabecera">MONTO TOTAL</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if ($result['estado'] == 1) {
				foreach ($result['data'] as $key => $value) {
					$monto_pagar = $value['tb_cuotadetalle_cuo'] + $monto_cal;
					$simbolo = 'S./';
					if ($value['tb_moneda_id'] == 2)
						$simbolo = 'US$';
			?>
					<tr class="filaTabla">
						<td>
							<?php echo $value['tb_cuotadetalle_id']; ?>
							<input type="hidden" name="hdd_cuota_id[]" value="<?php echo $value['tb_cuota_id'] ?>">
							<input type="hidden" name="hdd_cuota_fec[]" value="<?php echo $value['tb_cuota_fec'] ?>">
						</td>
						<td>
							<?php echo mostrar_fecha($value['tb_cuotadetalle_fec']); ?>
							<input type="hidden" name="hdd_cuotadetalle_fec[]" value="<?php echo $value['tb_cuotadetalle_fec'] ?>">
						</td>
						<td><?php echo $simbolo . ' ' . formato_moneda($value['tb_cuotadetalle_cuo']); ?></td>
						<td><?php echo $simbolo . ' ' . formato_moneda($monto_cal); ?></td>
						<td><?php echo $simbolo . ' ' . formato_moneda($monto_pagar); ?></td>
					</tr>
			<?php
				}
			}
			$result = null;
			?>
		</tbody>
	</table>
<?php endif; ?>

<?php if ($cuotip_id == 3): ?>
	<input type="hidden" name="hdd_num_rows_acu" id="hdd_num_rows_acu" value="<?php echo $num_rows; ?>">
	<table class="table table-bordered table-hover" border="0">
		<thead>
			<tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
				<th id="filacabecera">NÚMERO</th>
				<th id="filacabecera">FECHA</th>
				<th id="filacabecera">MONTO ACUERDO</th>
			</tr>
		</thead>
		<tbody>
			<?php
			for ($i = 1; $i <= $num_cuo; $i++) {
				$monto_pagar = $monto_cal;
				$simbolo = 'S./';
				if ($moneda_id == 2)
					$simbolo = 'US$';
				//fecha facturacion
				$month = $month + 1;
				if ($month == '13') {
					$month = 1;
					$year = $year + 1;
				}
				$fecha_val = validar_fecha_facturacion($day, $month, $year);
			?>
				<tr class="filaTabla">
					<td>
						<?php echo $i; ?>
						<input type="hidden" name="hdd_cuota_id[]" value="<?php echo $i; ?>">
						<input type="hidden" name="hdd_cuota_fec[]" value="<?php echo $fecha_val; ?>">
					</td>
					<td>
						<?php echo $fecha_val; ?>
						<input type="hidden" name="hdd_cuotadetalle_fec[]" value="<?php echo $fecha_val; ?>">
					</td>
					<td><?php echo $simbolo . ' ' . formato_moneda($monto_cal); ?></td>
				</tr>
			<?php
			}

			?>
		</tbody>
	</table>
<?php endif; ?>