<?php
  session_name('ipdnsac');
  session_start();

  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();

  $direc = 'creditotipo';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $creditotipo_id = $_POST['creditotipo_id'];

  $titulo = '';
  if($action == 'I')
    $titulo = 'Registrar Tipo de Crédito';
  elseif($action == 'M')
    $titulo = 'Editar Tipo de Crédito';
  else
    $titulo = 'Tipo de Crédito Registrado';

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en creditotipo
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }
    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($action, $array_permisos))
      $bandera = 1;
    elseif(!in_array($action, $array_permisos) && $action == 'M'){
      $mensaje = 'QUIERES EDITAR PERO NO TIENES EL PERMISO, VERIFICAMOS SI TE DIERON UN PERMISO';
      $bandera = 2;
    }
    elseif(!in_array($action, $array_permisos) && $action == 'I'){
      $mensaje = 'QUIERES INSERTAR PERO NO TIENES EL PERMISO, SOLICITA AL ADMIN';
      $bandera = 3;
    }
    elseif(!in_array($action, $array_permisos) && $action == 'L'){
      $mensaje = 'QUIERES LEER PERO NO TIENES EL PERMISO, SOLICITA AL ADMIN';
      $bandera = 3;
    }
    else{
      $mensaje = 'EL TIPO DE ACCION ES DESCONOCIDO: '.$action;
      $bandera = 4;
    }

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera != 1){
      $usuario_id = $_SESSION['usuario_id']; $tabla = 'tb_creditotipo'; $tabla_id = $creditotipo_id; $tipo_permiso = $action;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $tabla, $tabla_id, $tipo_permiso);
        if($result['estado'] == 1)
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
      $result = NULL;
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditotipo" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_creditotipo" method="post">
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_creditotipo_nom" class="control-label">Tipo de Crédito</label>
              <input type="text" name="txt_creditotipo_nom" id="txt_creditotipo_nom" class="form-control input-sm">
            </div>
            <div class="form-group">
              <label for="txt_creditotipo_des" class="control-label">Descripción</label>
              <input type="text" name="txt_creditotipo_des" id="txt_creditotipo_des" class="form-control input-sm">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?php if($action == 'I' || $action == 'M'): ?>
              <button type="submit" class="btn btn-info pull-right">Guardar</button>
            <?php endif; ?>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 2): ?>
  <div class="modal modal-warning" tabindex="-1" role="dialog" id="modal_registro_creditotipo">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Sin Permiso de Editar</h4>
        </div>
        <form id="form_creditotipo" method="post">
          <div class="modal-body">
            <div class="box-body">
              <p><?php echo $mensaje;?></p>
              <div class="form-group">
                <label for="txt_creditotipo_des" class="control-label">Solicitar:</label>
                <input type="text" name="txt_creditotipo_des" id="txt_creditotipo_des" class="form-control input-sm">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 3): ?>
  <div class="modal modal-warning" tabindex="-1" role="dialog" id="modal_registro_creditotipo" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Sin Permiso de Registro o Lectura</h4>
        </div>
        <form id="form_creditotipo" method="post">
          <div class="modal-body">
            <div class="box-body">
              <p><?php echo $mensaje;?></p>
              <div class="form-group">
                <label for="txt_creditotipo_des" class="control-label">Solicitar:</label>
                <input type="text" name="txt_creditotipo_des" id="txt_creditotipo_des" class="form-control input-sm">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_creditotipo">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Acceso Denegado</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
