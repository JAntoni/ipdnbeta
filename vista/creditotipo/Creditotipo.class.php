<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Creditotipo extends Conexion{

    function insertar($creditotipo_nom, $creditotipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_creditotipo(tb_creditotipo_xac, tb_creditotipo_nom, tb_creditotipo_des)
          VALUES (1, :creditotipo_nom, :creditotipo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_nom", $creditotipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_des", $creditotipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($creditotipo_id, $creditotipo_nom, $creditotipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_creditotipo SET tb_creditotipo_nom =:creditotipo_nom, tb_creditotipo_des =:creditotipo_des WHERE tb_creditotipo_id =:creditotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_nom", $creditotipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_des", $creditotipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($creditotipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_creditotipo WHERE tb_creditotipo_id =:creditotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($creditotipo_id){
      try {
        $sql = "SELECT * FROM tb_creditotipo WHERE tb_creditotipo_id =:creditotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_creditotipos(){
      try {
        $sql = "SELECT * FROM tb_creditotipo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
