<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../creditotipo/Creditotipo.class.php');
  $oCreditotipo = new Creditotipo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$creditotipo_nom = mb_strtoupper($_POST['txt_creditotipo_nom'], 'UTF-8');
 		$creditotipo_des = $_POST['txt_creditotipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Tipo de Crédito.';
 		if($oCreditotipo->insertar($creditotipo_nom, $creditotipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Crédito registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$creditotipo_id = intval($_POST['hdd_creditotipo_id']);
 		$creditotipo_nom = mb_strtoupper($_POST['txt_creditotipo_nom'], 'UTF-8');
 		$creditotipo_des = $_POST['txt_creditotipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Tipo de Crédito.';

 		if($oCreditotipo->modificar($creditotipo_id, $creditotipo_nom, $creditotipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Crédito modificado correctamente. '.$creditotipo_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$creditotipo_id = intval($_POST['hdd_creditotipo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Tipo de Crédito.';

 		if($oCreditotipo->eliminar($creditotipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Crédito eliminado correctamente. '.$creditotipo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>