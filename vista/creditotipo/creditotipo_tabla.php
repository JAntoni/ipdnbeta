<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Creditotipo.class.php');
  $oCreditotipo = new Creditotipo();

  $result = $oCreditotipo->listar_creditotipos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_creditotipo_id'].'</td>
          <td>'.$value['tb_creditotipo_nom'].'</td>
          <td>'.$value['tb_creditotipo_des'].'</td>
          <td align="center">
            
              <a class="btn btn-info btn-xs" title="Ver" onclick="creditotipo_form(\'L\','.$value['tb_creditotipo_id'].')"><i class="fa fa-eye"></i></a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="creditotipo_form(\'M\','.$value['tb_creditotipo_id'].')"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="creditotipo_form(\'E\','.$value['tb_creditotipo_id'].')"><i class="fa fa-trash"></i></a>
              <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
            
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_creditotipos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
