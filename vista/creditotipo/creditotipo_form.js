function listar_usuarios(){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"usuario/usuario_tabla.php",
		dataType: "html",
		data: ({as: 1}),
		beforeSend: function() {

		},
		success: function(data){
			console.log(data);
		},
		complete: function(data){
			//console.log(data);
			//if(data.statusText != "success")
				//console.log(data.responseText);
		},
		error: function(data){
			console.log(data.responseText);
		}
	});
}
$(document).ready(function(){

	$(".moneda").autoNumeric({
		aSep: ",",
		aDec: ".",
		vMin: "0.00",
		vMax: "999999999.00",
	  });
	  
  $('#form_creditotipo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"creditotipo/creditotipo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_creditotipo").serialize(),
				beforeSend: function() {
					$('#creditotipo_mensaje').show(400);
					$('#btn_guardar_creditotipo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#creditotipo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#creditotipo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		creditotipo_tabla();
		      		$('#modal_registro_creditotipo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#creditotipo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#creditotipo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_creditotipo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#creditotipo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#creditotipo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_creditotipo_nom: {
				required: true,
				minlength: 2
			},
			txt_creditotipo_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_creditotipo_nom: {
				required: "Ingrese un nombre para el Tipo de Crédito",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_creditotipo_des: {
				required: "Ingrese una descripción para el Tipo de Crédito",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
