<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Creditohipo extends Conexion{
    public $credito_id;

    public $credito_usureg;
    public $credito_usumod; 
    public $cliente_id; 
    public $credito_tip = 1; //tipo de credito es decir un subtipo, solo es cretido menor, no hay adendas ni otros
    public $cuotatipo_id; 
    public $moneda_id; 
    public $credito_tipcam; 
    public $credito_preaco; 
    public $credito_int; 
    public $credito_numcuo; 
    public $credito_numcuomax; 
    public $credito_linapr; 
    public $representante_id; 
    public $cuentadeposito_id; 
    public $credito_pla = 1; //plazo retroventa,no se usa actualmente
    public $credito_obs; 
    public $credito_feccre; 
    public $credito_fecdes; 
    public $credito_comdes; 
    public $credito_his;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "SELECT fn_insertar_creditohipo(
              :credito_usureg, :cliente_id, :credito_tip, 
              :cuotatipo_id, :moneda_id, :credito_tipcam, 
              :credito_preaco, :credito_int, :credito_numcuo, 
              :credito_numcuomax, :credito_linapr, :representante_id, 
              :cuentadeposito_id, :credito_pla, :credito_comdes, 
              :credito_obs, :credito_feccre, :credito_fecdes, 
              :credito_his) AS credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_usureg", $this->credito_usureg, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_tip", $this->credito_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_tipcam", $this->credito_tipcam, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_int", $this->credito_int, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_id", $this->representante_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_pla", $this->credito_pla, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_obs", $this->credito_obs, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_his", $this->credito_his, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $this->dblink->commit();

        $resultado = $sentencia->fetch();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['credito_id'] = $resultado['credito_id'];
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_creditohipo(
              :credito_id, :credito_usumod,
              :credito_usureg, :cliente_id, :credito_tip, 
              :cuotatipo_id, :moneda_id, :credito_tipcam, 
              :credito_preaco, :credito_int, :credito_numcuo, 
              :credito_numcuomax, :credito_linapr, :representante_id, 
              :cuentadeposito_id, :credito_pla, :credito_comdes, 
              :credito_obs, :credito_feccre, :credito_fecdes)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_usumod", $this->credito_usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_usureg", $this->credito_usureg, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_tip", $this->credito_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_tipcam", $this->credito_tipcam, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_int", $this->credito_int, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_id", $this->representante_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_pla", $this->credito_pla, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_obs", $this->credito_obs, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($credito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_creditohipo WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($credito_id){
      try {
        $sql = "SELECT hipo.*,cl.*, usu.tb_usuario_id, CONCAT(tb_usuario_nom,' ',tb_usuario_ape) as usuario_nombre, m.*, rep.tb_representante_nom, ct.*
          FROM tb_creditohipo hipo 
          INNER JOIN tb_cliente cl on cl.tb_cliente_id = hipo.tb_cliente_id 
          INNER JOIN tb_usuario usu on usu.tb_usuario_id = hipo.tb_credito_usureg
          INNER JOIN tb_cuotatipo ct ON hipo.tb_cuotatipo_id = ct.tb_cuotatipo_id
          INNER JOIN tb_representante rep on hipo.tb_representante_id = rep.tb_representante_id
          INNER JOIN tb_moneda m on hipo.tb_moneda_id = m.tb_moneda_id
          WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function filtrar($credito_id) {
      try {
          $sql = "SELECT * FROM tb_creditohipofile WHERE tb_credito_id = :tb_credito_id AND tb_creditohipofile_xac=1";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_credito_id", $credito_id, PDO::PARAM_INT);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
          } else {
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
          }

          return $retorno;
      } catch (Exception $e) {
          throw $e;
      }
  }

    function listar_creditos($filtro_fec1,$filtro_fec2,$estado,$cliente,$sede,$tipo1,$tipo2,$tipo3,$tipo4){
      try {
        $sql = "SELECT * FROM tb_creditohipo hipo 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = hipo.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = hipo.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = hipo.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = hipo.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = hipo.tb_representante_id 
          WHERE 
            tb_credito_xac = 1 
          AND
            tb_credito_feccre BETWEEN :filtro_fec1 AND :filtro_fec2
          AND
            (case :p_tb_cliente_id
              when 0 then
                1=1
              else
              hipo.tb_cliente_id = :p_tb_cliente_id
            end)
          -- AND
          --   (case :p_tb_empresa_id
          --     when 0 then
          --       1=1
          --     else
          --     hipo.tb_empresa_id = :p_tb_empresa_id
          --   end)
          AND
            (case :p_credito_tip1
              when 0 then
                (case :p_credito_tip2
                  when 0 then
                    (case :p_credito_tip3
                      when 0 then
                        (case :p_credito_tip4
                          when 0 then
                            1=1 -- NO SE EJECUTA ESTA LINEA XQ REQUIERE QUE UNO DE LOS 4 ESTÉ SELECCIONADO
                          else
                            tb_credito_tip = :p_credito_tip4
                        end)
                      else
                        tb_credito_tip = :p_credito_tip3 OR tb_credito_tip = :p_credito_tip4
                    end)
                  else
                    tb_credito_tip = :p_credito_tip2 OR tb_credito_tip = :p_credito_tip3
                end)
              else
                tb_credito_tip = :p_credito_tip1 OR tb_credito_tip = :p_credito_tip2 OR tb_credito_tip = :p_credito_tip3 OR tb_credito_tip = :p_credito_tip4
            end)
          AND
            (case :p_estado
              when 0 then
                1=1
              else
                tb_credito_est = :p_estado
            end)
          ORDER BY tb_credito_feccre desc";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro_fec1", $filtro_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":filtro_fec2", $filtro_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":p_tb_cliente_id", $cliente, PDO::PARAM_INT);
        $sentencia->bindParam(":p_estado", $estado, PDO::PARAM_INT);
        // $sentencia->bindParam(":p_tb_empresa_id", $sede, PDO::PARAM_INT);
        $sentencia->bindParam(":p_credito_tip1", $tipo1, PDO::PARAM_INT);
        $sentencia->bindParam(":p_credito_tip2", $tipo2, PDO::PARAM_INT);
        $sentencia->bindParam(":p_credito_tip3", $tipo3, PDO::PARAM_INT);
        $sentencia->bindParam(":p_credito_tip4", $tipo4, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    //lista de creditos por estado
    function listar_creditos_estado($credito_est){
      try {
        $sql = "SELECT * FROM tb_creditohipo hipo 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = hipo.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = hipo.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = hipo.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = hipo.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = hipo.tb_representante_id 
          WHERE tb_credito_xac = 1 and tb_credito_est in($credito_est) ORDER BY tb_credito_id desc";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function filtro_creditos($filtro_fec1, $filtro_fec2){
      try {
        $sql = "SELECT * FROM tb_creditohipo hipo 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = hipo.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = hipo.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = hipo.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = hipo.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = hipo.tb_representante_id 
          WHERE tb_credito_xac = 1 and tb_credito_feccre BETWEEN :filtro_fec1 and :filtro_fec2 ORDER BY tb_credito_id desc";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro_fec1", $filtro_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":filtro_fec2", $filtro_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function modificar_campo($credito_id, $credito_columna, $credito_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($credito_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_creditohipo SET ".$credito_columna." =:credito_valor WHERE tb_credito_id =:credito_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":credito_valor", $credito_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":credito_valor", $credito_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    //listado de cuotas facturadas
    function listar_creditos_cuotas_facturadas_subcredito_total($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est){
      try {
        $sql = "SELECT 1 AS moneda, SUM(CASE WHEN tb_cuotatipo_id =3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) AS facturado
          FROM tb_creditohipo cre 
          inner join tb_cuota cuo on cre.tb_credito_id = cuo.tb_credito_id 
          inner join tb_cuotadetalle cudet on cudet.tb_cuota_id = cuo.tb_cuota_id
          where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id = 4 and tb_credito_tip in($subcredito_id) and tb_cuotadetalle_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est) and tb_cuotadetalle_estap = 0 and cre.tb_moneda_id = 1
          UNION
          SELECT 2 AS moneda, SUM(CASE WHEN tb_cuotatipo_id =3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) AS facturado
          FROM tb_creditohipo cre 
          inner join tb_cuota cuo on cre.tb_credito_id = cuo.tb_credito_id 
          inner join tb_cuotadetalle cudet on cudet.tb_cuota_id = cuo.tb_cuota_id
          where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id = 4 and tb_credito_tip in($subcredito_id) and tb_cuotadetalle_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est) and tb_cuotadetalle_estap = 0 and cre.tb_moneda_id = 2";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_fec1", $cuota_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_fec2", $cuota_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    //funcion para listar cuotas facturadas en detalle
    function listar_creditos_cuotas_facturadas_subcredito_detalle($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est){
      try {
        $sql = "SELECT 
          cre.tb_credito_id, tb_credito_est, tb_credito_preaco, tb_credito_numcuo, tb_cuotadetalle_id,
          tb_cuotadetalle_fec, (CASE WHEN tb_cuotatipo_id =3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) AS tb_cuotadetalle_cuo, tb_cuotadetalle_est, cre.tb_moneda_id
          FROM tb_creditohipo cre 
          inner join tb_cuota cuo on cre.tb_credito_id = cuo.tb_credito_id 
          inner join tb_cuotadetalle cudet on cudet.tb_cuota_id = cuo.tb_cuota_id
          where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id = 4 and tb_cuotadetalle_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est) and tb_credito_tip in ($subcredito_id) and tb_cuotadetalle_estap = 0";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_fec1", $cuota_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_fec2", $cuota_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function registro_historial($cre_id, $his){
      $this->dblink->beginTransaction();
      try {
       $sql = "UPDATE tb_creditohipo set tb_credito_his = CONCAT(tb_credito_his, :credito_his) where tb_credito_id = :credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_his", $his, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_id", $cre_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  }

?>
