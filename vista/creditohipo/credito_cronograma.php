<?php
  require_once("../../core/usuario_sesion.php");

  require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();

  require_once("../funciones/funciones.php");
  require_once("../funciones/fechas.php");

  $inicial = moneda_mysql($_POST['cre_ini']);
  $vehiculo = moneda_mysql($_POST['veh_pre']);
  $tip_ade = $_POST['tip_ade'];
  $cre_id = $_POST['cre_id'];
  $cuotip_id = intval($_POST['cuotip_id']);
  
  //parametro 4 ya que debe buscar las cuotas de un credito ASIVEH
  if (!empty($cre_id)) {
    $result = $oCuota->ultima_cuota_por_credito($cre_id, 4);
    if ($result['estado'] == 1) {
      $cuo_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
    }
    $result = null;
  }

  $C = moneda_mysql($_POST['cre_preaco']);
  //echo "<br>";
  $i = moneda_mysql($_POST['cre_int']);
  //echo "<br>";
  $n = $_POST['cre_numcuo'];

  if ($tip_ade == 4)
    $n = $_POST['cre_numcuo_res']; // si es CUOTA BALON, se suma todas las cuotas por pagar para calcular el interes y generar una sola cuota

  if ($cuotip_id == 3) // si es 4 es fija, 3 es libre
    $n = 1; // si es libre solo será una cuota

  //echo "<br>";
  $fec_desem = $_POST['fec_desem']; //fecha de desembolso
  $fec_prorra = $_POST['fec_prorra']; //fecha de prorrateo
  $cre_per = $_POST['cre_per'];
  if ($cre_per == 1) {
    $cols = 1;
    $name_per = "MENSUAL";
  }
  if ($cre_per == 2) {
    $cols = 2;
    $name_per = "QUINCENAL";
  }
  if ($cre_per == 3) {
    $cols = 4;
    $name_per = "SEMANAL";
  }
  //para la formula del prorrateo, se toma la fecha del prorrateo y se cuenta cuántos días tiene el mes de la fecha del prorrateo, ejem: prorrateo 10/12/2016, el mes 12 tiene 31 días, para saber eso usamos la funcion de mktime
  //formula prorrateo = (interes de la primera cuota / total días del mes prorrateo) * dias prorrateo
  list($dia, $mes, $anio) = explode('-', $fec_prorra);
  $dias_mes = date('t', mktime(0, 0, 0, $mes, 1, $anio));
  $fec_desem1 = new DateTime($fec_desem);
  $fec_prorra1 = new DateTime($fec_prorra);
  $dias_prorra = $fec_desem1->diff($fec_prorra1)->format("%a");
  $R = $C / $n;
  if ($i != 0) {
    $uno = $i / 100;
    $dos = $uno + 1;
    $tres = pow($dos, $n);
    $cuatroA = ($C * $uno) * $tres;
    $cuatroB = $tres - 1;
    $R = $cuatroA / $cuatroB;
    $r_sum = $R * $n;
  }
  $fecha_factu = $_POST['cre_fecfac'];
  
  list($day, $month, $year) = explode('-', $fecha_factu);
  $moneda = $_POST['mon_id'];

  if (is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R) && is_numeric($inicial) && $tip_ade != 4) {
  ?>
    <!-- <fieldset> -->
      <!-- <legend>Cronograma de Crédito < ?php // echo $R;  ?></legend> -->
      <table id="tbl_cronograma" class="table table-bordered table-hover mt-4">
        <thead>
          <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
            <th class="text-center">N°</th>
            <th class="text-center" colspan="<?php echo $cols ?>">FECHAS</th>
            <th class="text-center">CAPITAL</th>
            <th class="text-center">AMORTIZACIÓN</th>
            <th class="text-center">INTERÉS</th>
            <th class="text-center">PRORRATEO</th>
            <th class="text-center">CUOTA</th>
            <th class="text-center"><?php echo $name_per; ?></th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($moneda == '1') {
            $moneda = 'S/. ';
          } else {
            $moneda = 'US$. ';
          }
          for ($j = 1; $j <= $n; $j++) {
            if ($j > 1) {
              $C = $C - $amo;
              $int = $C * ($i / 100);
              $amo = $R - $int;
            } else {
              $int = $C * ($i / 100);
              $amo = $R - $int;
            }
            //fecha facturacion
            $month = $month + 1;
            if ($month == '13') {
              $month = 1;
              $year = $year + 1;
            }
            $fecha = validar_fecha_facturacion($day, $month, $year);
            if ($j == 1) {
              $int_pro = ($int / $dias_mes) * $dias_prorra;
              $pro_div_cuo = $int_pro / $n;
              //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
              $pro_div_subcuo = $int_pro / ($n * $cols);
            }
            //else
            //$int_pro = 0;
          ?>
            <tr class="filaTabla">
              <td align="center">
                <?php echo $j; ?>
              </td>
              <?php
              list($day, $mon, $year) = explode('-', $fecha);
              if ($cre_per == 3) {
                $fecha_1 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 3), $year));
                $fecha_2 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 2), $year));
                $fecha_3 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 1), $year));
                $fecha_4 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 0), $year));
                echo '<td align="center">' . $fecha_1 . '</td>
                    <td align="center">' . $fecha_2 . '</td>
                    <td align="center">' . $fecha_3 . '</td>
                    <td align="center"><strong>' . $fecha_4 . '</strong></td>';
              }
              if ($cre_per == 2) {
                $fecha_1 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 2), $year));
                $fecha_2 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 0), $year));
                echo '<td align="center">' . $fecha_1 . '</td>
                    <td align="center"><strong>' . $fecha_2 . '</strong></td>';
              }
              if ($cre_per == 1) {
                $fecha_1 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 0), $year));
                echo '<td align="center"><strong>' . $fecha_1 . '</strong></td>';
              }
              ?>
              <td align="right">
                <?php echo $moneda . formato_moneda($C); ?>
              </td>
              <td align="right">
                <?php echo $moneda . formato_moneda($amo); ?>
              </td>
              <td align="right">
                <?php echo $moneda . formato_moneda($int); ?>
              </td>
              <td align="right">
                <?php echo '<input type="hidden" id="hdd_mon_pro_' . $j . '" value="' . formato_moneda($int_pro) . '">' ?>
                <?php echo $moneda . formato_moneda($int_pro); ?>
              </td>
              <td align="right">
                <?php echo $moneda . formato_moneda($R + $pro_div_cuo); ?>
              </td>
              <td align="right">
                <?php echo $moneda . formato_moneda($R / $cols + $pro_div_subcuo); ?>
              </td>
            </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
    <!-- </fieldset> -->
  <?php
  }
  if (is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R) && is_numeric($inicial) && $tip_ade == 4) {
  ?>
    <!-- <fieldset> -->
      <!-- <legend>Cronograma de Crédito < ?php echo $R; ?></legend> -->
      <table style="width:100%" class="tablesorter" mt-4>
        <thead>
          <tr>
            <th align="center">N°</th>
            <th align="center" colspan="<?php echo $cols ?>">FECHAS</th>
            <th align="center">CAPITAL</th>
            <th align="center">AMORTIZACIÓN</th>
            <th align="center">INTERÉS</th>
            <th align="center">PRORRATEO</th>
            <th align="center">CUOTA</th>
            <th align="center"><?php echo $name_per; ?></th>
          </tr>
        </thead>
        <tbody>
          <?php
            if ($moneda == '1') {
              $moneda = 'S/. ';
            } else {
              $moneda = 'US$. ';
            }
            $sum_int = 0;
            $sum_amo = 0;
            for ($j = 1; $j <= $n; $j++) {
              if ($j > 1) {
                $C = $C - $amo;
                $int = $C * ($i / 100);
                $amo = $R - $int;
              } else {
                $int = $C * ($i / 100);
                $amo = $R - $int;
              }
              //fecha facturacion
              $month = $month + 1;
              if ($month == '13') {
                $month = 1;
                $year = $year + 1;
              }
              $fecha = validar_fecha_facturacion($day, $month, $year);
              if ($j == 1) {
                $int_pro = ($int / $dias_mes) * $dias_prorra;
                $pro_div_cuo = $int_pro / $n;
                //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                $pro_div_subcuo = $int_pro / ($n * $cols);
              } else
                $int_pro = 0;

              $sum_int += $int;
              $sum_amo += $amo;
            }
            $cre_preaco = moneda_mysql($_POST['cre_preaco']);
          ?>
          <tr class="even">
            <td align="center">1</td>
            <td align="center"><?php echo $cuo_fec; ?></td>
            <td align="right">
              <?php echo $moneda . formato_moneda($cre_preaco); ?>
            </td>
            <td align="right">
              <?php echo $moneda . formato_moneda($sum_amo); ?>
            </td>
            <td align="right">
              <?php echo $moneda . formato_moneda($sum_int); ?>
            </td>
            <td align="right">
              <?php echo '<input type="hidden" id="hdd_mon_pro_1" value="0">' ?>
              <?php echo $moneda . '0.00'; ?>
            </td>
            <td align="right">
              <?php echo $moneda . formato_moneda($cre_preaco + $sum_int); ?>
            </td>
            <td align="right">
              <?php echo $moneda . formato_moneda($cre_preaco + $sum_int); ?>
            </td>
          </tr>
        </tbody>
      </table>
    <!-- </fieldset> -->
  <?php
  }
?>