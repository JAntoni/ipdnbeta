<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  require_once('Creditohipo.class.php');
  $oCreditohipo = new Creditohipo();

  $fecha1            = $_POST['fecha1'];
  $fecha2            = $_POST['fecha2'];
  $cliente_id        = $_POST['cliente_id'];
  $estado            = intval($_POST['p_estado']);
  $sede_id           = $_POST['sede_id'];
  $cmb_usuario_id    = $_POST['cmb_usuario_id'];
  $cmb_fil_opc_cre   = $_POST['cmb_fil_opc_cre'];
  $chckcredasiveh01  = $_POST['chckcredasiveh01'];
  $chckcredasiveh02  = $_POST['chckcredasiveh02'];
  $chckcredasiveh03  = $_POST['chckcredasiveh03'];
  $chckcredasiveh04  = $_POST['chckcredasiveh04'];

  // echo 'data: <br>'.$fecha1.'<br>'.$fecha2.'<br>'.$cliente_id.'<br>'.$estado.'<br>'.$cmb_usuario_id.'<br>'.$cmb_fil_opc_cre.'<br>'.$chckcredasiveh01.'<br>'.$chckcredasiveh02.'<br>'.$chckcredasiveh03.'<br>'.$chckcredasiveh04.'<br>';
  
  $fecha1 = date("Y-m-d", strtotime($fecha1));
  $fecha2 = date("Y-m-d", strtotime($fecha2));
  $fecha_hoy = date('d-m-Y');

  if( $chckcredasiveh01 == 0 && $chckcredasiveh02 == 0 && $chckcredasiveh03 == 0 && $chckcredasiveh04 == 0 )
  {
    echo '<span class="badge bg-blue"> Debe seleccionar al menos uno </span>';
    exit();
  }

  $tr = '';
  $result = $oCreditohipo->listar_creditos($fecha1,$fecha2,$estado,$cliente_id,$sede_id,$chckcredasiveh01,$chckcredasiveh02,$chckcredasiveh03,$chckcredasiveh04);
    if ($result['estado'] == 1)
    {
      $tip  = "";
      $tip2 = "";
      $multiple = "";
      $estado = "";

      foreach ($result['data'] as $key => $value)
      {
        switch ($value['tb_credito_est']) {
          case 1:
            $estado = 'PENDIENTE';
            break;
          case 2:
            $estado = 'APROBADO';
            break;
          case 3:
            $estado = 'VIGENTE';
            break;
          case 4:
            $estado = 'PARALIZADO';
            break;
          case 7:
            $estado = 'LIQUIDADO';
            break;
          case 8:
            $estado = 'RESUELTO';
            break;
          default:
            $estado = '<span class="badge bg-red"> NO ENCONTRADO: '.$value['tb_credito_est'].', REPÓRTELO </span>';
            break;
        }

        //  |  0  |  3  |  4  |
        if( $value['tb_cuotatipo_id'] == 3 ){ $cuotas = $value['tb_credito_numcuomax']; } else { $cuotas = $value['tb_credito_numcuo']; }

        //  |  1  |  2  | 3  |  4  |
        switch ($value['tb_credito_tip']) {
          case 1:
            $tip = 'COMPRA VENTA';
            break;
          case 2:
            $tip = 'ADENDAS';
            break;
          case 3:
            $tip = 'ACUERDOS DE PAGO';
            break;
          case 4:
            $tip = 'CON HIPOTECA';
            break;
          default:
            $tip = '<span class="badge bg-red"> NO ENCONTRADO: '.$value['tb_credito_tip'].', REPÓRTELO </span>';
            break;
        }

        //  |  0  |  1  |  2  | 3  |  4  |  5  |  6  |  7  |
        switch ($value['tb_credito_tip2']) {
          case 0:
            $tip2 = ' SIN TIPO';
            break;
          case 1:
            $tip2 = ' CREDITO REGULAR';
            break;
          case 2:
            $tip2 = ' CREDITO ESPECÍFICO';
            break;
          case 3:
            $tip2 = ' REPROGRAMADO';
            break;
          case 4:
            $tip2 = ' CUOTA BALON';
            break;
          case 5:
            $tip2 = ' REFINANCIADO AMORTIZADO';
            break;
          case 6:
            $tip2 = ' REFINANCIADO';
            break;
          case 7:
            $tip2 = ' TRANSFERIDO';
            break;
          default:
            $tip2 = ' <span class="badge bg-red"> NO ENCONTRADO, REPÓRTELO </span>';
            break;
        }

        if ($value['tb_cliente_tip'] == 1) {
          $cliente = $value['tb_cliente_nom'] . ' &nbsp;|&nbsp; ' . $value['tb_cliente_doc'];
        }

        if ($value['tb_cliente_tip'] == 2) {
          $cliente = $value['tb_cliente_emprs'] . ' &nbsp;|&nbsp; ' . $value['tb_cliente_empruc'];
        }

        if ($value['tb_moneda_id'] == 1) $moneda = "S/.";
        
        if ($value['tb_moneda_id'] == 2) $moneda = "US$";

        // if ($value['tb_credito_tip'] == 2 && $value['tb_credito_est'] == 3) {
        //   $multiple = '<input type="checkbox" name="che_multiple" class="che_multiple" data-credito="' . $value['tb_credito_id'] . '" data-cliente="' . $value['tb_cliente_id'] . '">';
        // }

        $tr .= '<tr class="tabla_filaTabla">';
          $tr .= '
            <td style="vertical-align: middle;">' . mostrar_fecha($value['tb_credito_fecdes']) . '</td>
            <td style="vertical-align: middle;">' . $tip . ' ' . $tip2 . '</td>
            <td style="vertical-align: middle;">' . $value['tb_credito_partel']. '</td>
            <td style="vertical-align: middle;">' . $cliente . '</td>
            <td style="vertical-align: middle;" class="text-center">' . $moneda . '</td>
            <td style="vertical-align: middle;" class="text-right">' . mostrar_moneda($value['tb_credito_preaco']) . '</td>
            <td style="vertical-align: middle;" class="text-center">' . $value['tb_credito_int'] . '</td>
            <td style="vertical-align: middle;" class="text-center">' . $cuotas . '</td>
            <td style="vertical-align: middle;">' . $value['tb_usuario_nom'] . ' ' . $value['tb_usuario_ape'] . '</td>
            <td style="vertical-align: middle;">' . $value['tb_representante_nom'] . '</td>
            <td style="vertical-align: middle;">'. $estado . '</td>
            <td style="vertical-align: middle;">' . $value['tb_credito_id'] . '</td>
            <td style="vertical-align: middle;" class="text-center">
              <a class="btn btn-success btn-xs" onClick="creditohipo_form(\'M\','.$value['tb_credito_id'].')" title="VER CREDITO"><i class="fa fa-search fa-fw"></i> Ver</a>
            </td>
          ';
          // <td style="vertical-align: middle;">
          //   <input type="radio" name="ra_estado" id="ra_estado" data-est="' . $value['tb_credito_est'] . '" data-cuo_tip="' . $value['tb_cuotatipo_id'] . '" data-tip="' . $value['tb_credito_tip'] . '" value="' . $value['tb_credito_id'] . '"> &nbsp;
          //   '.$multiple.'
          // </td>
        $tr .= '</tr>';
      }
    }
  $result = null;
?>
<!--  -->
<table id="tbl_creditoasiveh" class="table table-stripped table-hover">
  <thead>
    <tr>
      <th id="tabla_cabecera" class="text-center">FECHA</th>
      <th id="tabla_cabecera" class="text-center">TIPO</th>
      <!-- <th id="tabla_cabecera" class="text-center">PLACA</th> -->
      <th id="tabla_cabecera" class="text-center">N° de Partida</th>
      <th id="tabla_cabecera" class="text-center">CLIENTE</th>
      <th id="tabla_cabecera" class="text-center">MON</th>
      <th id="tabla_cabecera" class="text-center">MONTO</th>
      <th id="tabla_cabecera" class="text-center">INT(%)</th>
      <th id="tabla_cabecera" class="text-center">CUOTAS</th>
      <th id="tabla_cabecera" class="text-center">ASESOR</th>
      <th id="tabla_cabecera" class="text-center">REPRESENTANTE</th>
      <th id="tabla_cabecera" class="text-center">ESTADO</th>
      <th id="tabla_cabecera" class="text-center">ID</th>
      <th id="tabla_cabecera" class="text-center">ACCIONES</th>
    </tr> 
  </thead>
  <tbody>
    <?php echo $tr; ?>
  </tbody>
</table>