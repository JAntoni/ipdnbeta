<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	require_once("../../core/usuario_sesion.php");
	require_once("../funciones/funciones.php");
	require_once("../funciones/fechas.php");
	require_once('../creditohipo/Creditohipo.class.php');
	require_once('../usuario/Usuario.class.php');
	require_once('../cuota/Cuota.class.php');
	require_once('../zona/Zona.class.php');
	require_once ("../ubigeo/Ubigeo.class.php");

	$oCreditohipo = new Creditohipo();
	$oUsuario = new Usuario();
	$oCuota = new Cuota();
	$oZona = new Zona();
	$oUbigeo = new Ubigeo();

	$action = $_POST['action'];
	$creditohipo_id = $_POST['creditoasiveh_id'];

	$dts = $oCreditohipo->mostrarUno($creditohipo_id);


	if ($dts['estado'] == 1) {
		$reg		  = mostrar_fecha_hora($dts['data']['tb_credito_reg']);
		$mod		  = mostrar_fecha_hora($dts['data']['tb_credito_mod']);
		$apr		  = mostrar_fecha_hora($dts['data']['tb_credito_apr']);
		$usureg		  = $dts['data']['tb_credito_usureg'];
		$usumod		  = $dts['data']['tb_credito_usumod'];
		$usuapr		  = $dts['data']['tb_credito_usuapr'];
		$cre_tip1 	  = $dts['data']['tb_credito_tip'];
		$cre_tip2 	  = $dts['data']['tb_credito_tip2']; //1 credito regular, 2 credito especifico, 3 reporgramado, 4 cuota balon, 7 transferido
		$mon_id		  = $dts['data']['tb_moneda_id'];
		$cuedep_id 	  = $dts['data']['tb_cuentadeposito_id'];
		$cre_tipcam	  = $dts['data']['tb_credito_tipcam'];
		$cre_preaco   = $dts['data']['tb_credito_preaco'];
		$cre_int 	  = $dts['data']['tb_credito_int'];
		$cre_numcuo   = $dts['data']['tb_credito_numcuo'];
		$cre_linapr   = $dts['data']['tb_credito_linapr'];
		$cre_ini 	  = $dts['data']['tb_credito_ini'];
		$cre_feccre	  = mostrar_fecha($dts['data']['tb_credito_feccre']);
		$cre_fecdes	  = mostrar_fecha($dts['data']['tb_credito_fecdes']);
		$cre_fecfac	  = mostrar_fecha($dts['data']['tb_credito_fecfac']);
		$cre_fecpro	  = mostrar_fecha($dts['data']['tb_credito_fecpro']);

		$tb_credito_subgar = $dts['data']['tb_credito_subgar'];
		$tb_credito_tipgar = $dts['data']['tb_credito_tipgar'];
		$tb_credito_ubica = $dts['data']['tb_credito_ubica'];
		$tb_credito_ubige = $dts['data']['tb_credito_ubige'];
		$tb_credito_partel = $dts['data']['tb_credito_partel'];
		$tb_credito_areato = $dts['data']['tb_credito_areato'];
		$tb_credito_areacon = $dts['data']['tb_credito_areacon'];
		$tb_credito_tipmat = $dts['data']['tb_credito_tipmat'];
		$tb_zonaregistral_id = $dts['data']['tb_zonaregistral_id'];
		$cre_inmu = $dts['data']['tb_credito_inmu']; //tipo de inmueble: vivienda, terro, semi..
		$cargas = $dts['data']['tb_credito_carg']; //cargas que tanga el inmueble

		$consultarDatosZonaRegitral = $oZona->mostrarUno($tb_zonaregistral_id);
		if( $consultarDatosZonaRegitral['estado'] == 1)
		{
			$datosZonaReg_descripcion = $consultarDatosZonaRegitral['data']['tb_zona_nom'];
		}
		else
		{
			$datosZonaReg_descripcion = 'No encontrado en la B.D.';
		}

		if($tb_credito_ubige>0)
		{
			$dtsubig = $oUbigeo->mostrarUbigeo($tb_credito_ubige);
				$ubi_dep = $dtsubig['data']['Departamento'];
				$ubi_pro = $dtsubig['data']['Provincia'];
				$ubi_dis = $dtsubig['data']['Distrito'];
			$dtsubig = null;
		} else{ $ubi_dep = '-'; $ubi_pro = '-'; $ubi_dis = '-';}

		$cre_comdes   = $dts['data']['tb_credito_comdes'];
		$cre_obs 	  = $dts['data']['tb_credito_obs'];
		$cre_carg 	  = $dts['data']['tb_credito_carg'];
		$cre_est	  = $dts['data']['tb_credito_est'];
		$cre_est_carg = $dts['data']['tb_credito_estcar'];
		$cre_med_frente = $dts['data']['tb_lindero_fremed'];
		$cre_colind_frente = $dts['data']['tb_lindero_frecol'];
		$cre_med_fondo  = $dts['data']['tb_lindero_fonmed'];
		$cre_colind_fondo  = $dts['data']['tb_lindero_foncol'];
		$cre_med_ladizq = $dts['data']['tb_lindero_izqmed'];
		$cre_colind_ladizq = $dts['data']['tb_lindero_izqcol'];
		$cre_med_ladder = $dts['data']['tb_lindero_dermed'];
		$cre_colind_ladder = $dts['data']['tb_lindero_dercol'];

		$representante_nombre = $dts['data']['tb_representante_nom'];

		$tb_credito_numpis = $dts['data']['tb_credito_numpis'];
		$tb_credito_numhab = $dts['data']['tb_credito_numhab'];
		$tb_credito_numban = $dts['data']['tb_credito_numban'];
		$tb_credito_numban = $dts['data']['tb_credito_numban'];
		$tb_credito_numcoci = $dts['data']['tb_credito_numcoci'];
		$tb_credito_numsal = $dts['data']['tb_credito_numsal'];
		$tb_credito_desviv = $dts['data']['tb_credito_desviv'];

		$cli_id 	   = $dts['data']['tb_cliente_id'];
		$cli_doc 	   = $dts['data']['tb_cliente_doc'];
		$cli_nom	   = $dts['data']['tb_cliente_nom'];
		// $cli_ape	   = $dts['data']['tb_cliente_ape'];
		$cli_dir	   = $dts['data']['tb_cliente_dir'];
		$cli_ema	   = $dts['data']['tb_cliente_ema'];
		$cli_fecnac    = $dts['data']['tb_cliente_fecnac'];
		$cli_tel	   = $dts['data']['tb_cliente_tel'];
		$cli_protel    = $dts['data']['tb_cliente_protel']; //propietario telefono
		if (empty($cli_protel)) {
			$cli_protel = '-';
		}
		$cli_cel	   = $dts['data']['tb_cliente_cel'];
		$cli_procel    = $dts['data']['tb_cliente_procel']; //propietario celular
		if (empty($cli_procel)) {
			$cli_procel = '-';
		}
		$cli_telref    = $dts['data']['tb_cliente_telref'];
		$cli_protelref = $dts['data']['tb_cliente_protelref']; //propietario telefono referencial
		if (empty($cli_protelref)) {
			$cli_protelref = '-';
		}

		$subper			= $dts['data']['tb_cuotasubperiodo_id'];

		if ($usureg > 0) {
			$consultarDatosUsuario = $oUsuario->mostrarUno($usureg);
			$usureg	= $consultarDatosUsuario['data']['tb_usuario_nom'] . " " . $consultarDatosUsuario['data']['tb_usuario_ape'];
		}

		if ($usumod > 0) {
			$consultarDatosUsuario = $oUsuario->mostrarUno($usumod);
			$usumod	= $consultarDatosUsuario['data']['tb_usuario_nom'] . " " . $consultarDatosUsuario['data']['tb_usuario_ape'];
		}

		if ($usuapr > 0) {
			$mos_usuapr = 1;
			$consultarDatosUsuario = $oUsuario->mostrarUno($usuapr);
			$usuapr	= $consultarDatosUsuario['data']['tb_usuario_nom'] . " " . $consultarDatosUsuario['data']['tb_usuario_ape'];
		}

		//verificamos si este crédito ya tiene guardado su cronograma, para no volverlo a guardar
		$dtsCuo = $oCuota->filtrar(4,$creditohipo_id);
			$num_rows_crono = count($dtsCuo);
		$dtsCuo = null;

		$cuotip_id  = $dts['data']['tb_cuotatipo_id'];
		
	} else {
		echo '<span> Resultados no encontrados. </span> -->' . $dts['estado'] - ' -- ' . $creditohipo_id;
		exit();
	}
	// --------- DATOS DEL CLIENTE --------- //
?>
<script type="text/javascript">
	$(function() {
		cmb_cuedep_id('<?php echo $cuedep_id ?>');
	});

	function cmb_cuedep_id(ids) {
		$.ajax({
			type: "POST",
			url: "./vista/cuentadeposito/cuentadeposito_select.php",
			async: false,
			dataType: "html",
			data: ({
				cuentadeposito_id: ids
			}),
			beforeSend: function() {
				$('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
			},
			success: function(html) {
				$('#cmb_cuedep_id').html(html);
			}
		});
	}
</script>

<style>
	.form-control:disabled {
		background-color: white;
	}

	.row>.col-md-5elements {
		width: 20%;
		padding: 10px;
		float: left;
	}
</style>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditoasiveh" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" name="hdd_cre_id" id="hdd_cre_id" value="<?php echo $creditohipo_id; ?>">
			<input type="hidden" name="hdd_cre_tip" id="hdd_cre_tip" value="<?php echo $cre_tip1 ?>">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				<h4 class="modal-title text-center"> INFORMACIÓN DE CRÉDITO HIPOTECARIO: <?php echo $creditohipo_id; ?> </h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-12">
							<h4 class="text-teal"> DATOS DEL CLIENTE </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row">
							<div class="col-md-12">
								<div class="row mt-4 mb-4">
									<div class="col-md-2">
										<div class="form-group">
											<label>RUC/DNI:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-address-card"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_doc" id="txt_ven_cli_doc" class="form-control text-center" value="<?php echo $cli_doc; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="txt_ven_cli_nom">Cliente:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-user"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_nom" id="txt_ven_cli_nom" class="form-control text-center" value="<?php echo $cli_nom; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="txt_ven_cli_dir">Dirección:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_dir" id="txt_ven_cli_dir" class="form-control text-center" value="<?php echo $cli_dir; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											="txt_ven_cli_fecnac">Fecha de Nacimiento:</label>
											<div class="input-group">
												<input type="text" name="txt_ven_cli_fecnac" id="txt_ven_cli_fecnac" class="form-control text-center" value="<?php echo mostrar_fecha($cli_fecnac); ?>" disabled />
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-md-3">
										<div class="form-group">
											<label for="txt_ven_cli_ema">Email:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-envelope"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_ema" id="txt_ven_cli_ema" class="form-control text-center" value="<?php echo $cli_ema; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_tel" class="control-label">Teléfono:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_tel" id="txt_ven_cli_tel" class="form-control text-center" value="<?php echo $cli_tel; ?>" disabled /><br>
												<!-- <span class="input-group-btn">
													<button type="button" class="btn btn-default bt-sm"> < ?php echo $cli_protel; ?> </button>
												</span> -->
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_cel" class="control-label">Celular:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_cel" id="txt_ven_cli_cel" class="form-control text-center" value="<?php echo $cli_cel; ?>" disabled /><br>
												<!-- <span class="input-group-btn">
													<button type="button" class="btn btn-default bt-sm"> < ?php echo $cli_procel; ?> </button>
												</span> -->
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_telref" class="control-label">Teléfono de Ref:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_telref" id="txt_ven_cli_telref" class="form-control text-center" value="<?php echo $cli_telref; ?>" disabled /><br>
												<span class="input-group-btn">
													<button type="button" class="btn btn-default bt-sm"> <?php echo $cli_protelref; ?> </button>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row mt-4 mb-4">
						<div class="col-md-6">
							<h4 class="text-primary"> DATOS DE GARANTÍA INMOBILIARIA </h4>
							<div class="container-fluid" style="border: 1px solid #71baf5; border-radius: 8px; background-color: rgba(113, 186, 245, 0.3);">
								<div class="row mt-4 mb-4">
									<div class="col-md-6">
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label for="cmb_cre_captan">Tipo de Crédito:</label>
													<select name="cmb_cre_tip" id="cmb_cre_tip" class="form-control" disabled>
														<option <?php if($cre_tip2 == 0) echo 'selected';?> value="">Seleccione...</option>
														<option <?php if($cre_tip2 == 1) echo 'selected';?> value="1">Garantía Inmueble</option>
														<option <?php if($cre_tip2 == 4) echo 'selected';?> value="4">Garantía Hipotecaria</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Forma Garantía:</label>
													<select name="cmb_credito_subgar" id="cmb_credito_subgar" class="form-control" disabled>
														<option <?php if($tb_credito_subgar == 'REGULAR') echo 'selected';?>>REGULAR</option>
														<option <?php if($tb_credito_subgar == 'GARHIPO TRANSFERENCIA') echo 'selected';?>>GARHIPO TRANSFERENCIA</option>
														<option <?php if($tb_credito_subgar == 'GARHIPO COMPRA/VENTA') echo 'selected';?>>GARHIPO COMPRA/VENTA</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-12">
												<div class="form-group">
													<label>Ubicación:</label>
													<input type="text" class="form-control" value="<?php echo $tb_credito_ubica; ?>" disabled>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Tipo de Grantía Inmoviliaria:</label>
													<select name="cmb_cre_tipgar" id="cmb_cre_tipgar" class="form-control" disabled>
														<option <?php if($tb_credito_tipgar==1)echo 'selected';?>value="1" <?php echo $tb_credito_tipgar;?>>Urbano</option>
														<option <?php if($tb_credito_tipgar==2)echo 'selected';?>value="2">Rustico</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Tipo de Inmueble:</label>
													<select name="cmb_cre_inmu" id="cmb_cre_inmu" class="form-control" disabled>
														<option value="1" selected="">Semicostruido</option>
														<option value="2">Costruido Vivienda</option>
														<option value="3">Construido Departamento</option>
														<option value="4">Construido Local</option>
														<option value="5">Terreno</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Partida Electrónica:</label>
													<input type="text" class="form-control" value="<?php echo $tb_credito_partel;?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Zona Registral:</label>
													<input type="text" class="form-control" value="<?php echo $datosZonaReg_descripcion;?>" disabled>
												</div>
											</div>
										</div>
									</div>

								</div>
								<div class="row mt-4 mb-4">
									<div class="col-md-4">
										<div class="form-group">
											<label>Departamento:</label>
											<input type="text" value="<?php echo $ubi_dep;?>" class="form-control" disabled>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Provincia:</label>
											<input type="text" value="<?php echo $ubi_pro;?>" class="form-control" disabled>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Distrito:</label>
											<input type="text" value="<?php echo $ubi_dis;?>" class="form-control" disabled>
										</div>
									</div>
								</div>
								<div class="row mt-4 mb-4">
									<div class="col-md-4">
										<div class="form-group">
											<label>Área total:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_areato;?>" disabled>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Área construida:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_areacon;?>" disabled>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Tipo de material construido:</label>
											<select name="cmb_cre_tipmat" id="cmb_cre_tipmat" class="form-control" disabled>
												<option value="0">--</option>
												<option value="1" <?php if($tb_credito_tipmat == 1) echo 'selected';?>>Concreto Armado</option>
												<option value="2" <?php if($tb_credito_tipmat == 2) echo 'selected';?>>Costrucción Tradicional</option>
												<option value="3" <?php if($tb_credito_tipmat == 3) echo 'selected';?>>Costrucción Temporal</option>
												<option value="4" <?php if($tb_credito_tipmat == 4) echo 'selected';?>>Otros</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<br>

							<h4 class="text-blue"> INTERIORES </h4>
							<div class="container-fluid mb-4" style="border: 1px solid #71baf5; border-radius: 8px; background-color: rgba(113, 186, 245, 0.3);">
								<div class="row mt-4 mb-4">
									<div class="col-md-2">
										<div class="form-group">
											<label>N° de Pisos:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_numpis; ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>N° de Habitaciones:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_numhab; ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>N° de Baños:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_numban; ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>N° de Cocinas:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_numcoci; ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>N° de Salas:</label>
											<input type="text" class="form-control" value="<?php echo $tb_credito_numsal; ?>" disabled>
										</div>
									</div>
									<div class="col-md-10">
										<div class="form-group">
											<label>Otras Especificaciones:</label>
											<textarea class="form-control" style="resize: vertical;" rows="3" disabled><?php echo $tb_credito_desviv; ?></textarea>
										</div>
									</div>
								</div>
							</div>

							<br>

							<h4 class="text-blue"> LINDEROS </h4>
							<div class="container-fluid mb-4" style="border: 1px solid #71baf5; border-radius: 8px; background-color: rgba(113, 186, 245, 0.3);">
								<div class="row mt-4 mb-4">
									<div class="col-md-12">
										<table class="table table hover table-stripped">
											<thead>
												<tr>
													<th class="text-center">FRONTERAS</th>
													<th class="text-center">MEDIDAS</th>
													<th class="text-center">COLINDANTES</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td style="vertical-align: middle;">POR EL FRENTE</td>
													<td style="vertical-align: middle;" align="center"><input type="text" class="form-control text-center" value="<?php echo $cre_med_frente; ?>" style="width: 45%;" disabled></td>
													<td style="vertical-align: middle;"><input type="text" class="form-control" value="<?php echo $cre_colind_frente; ?>" disabled></td>
												</tr>
												<tr>
													<td style="vertical-align: middle;">POR EL FONDO</td>
													<td style="vertical-align: middle;" align="center"><input type="text" class="form-control text-center" value="<?php echo $cre_med_fondo; ?>" style="width: 45%;" disabled></td>
													<td style="vertical-align: middle;"><input type="text" class="form-control" value="<?php echo $cre_colind_fondo; ?>" disabled></td>
												</tr>
												<tr>
													<td style="vertical-align: middle;">POR LATERAL IZQUIERDO</td>
													<td style="vertical-align: middle;" align="center"><input type="text" class="form-control text-center" value="<?php echo $cre_med_ladizq; ?>" style="width: 45%;" disabled></td>
													<td style="vertical-align: middle;"><input type="text" class="form-control" value="<?php echo $cre_colind_ladizq; ?>" disabled></td>
												</tr>
												<tr>
													<td style="vertical-align: middle;">POR LATERAL DERECHO</td>
													<td style="vertical-align: middle;" align="center"><input type="text" class="form-control text-center" value="<?php echo $cre_med_ladder; ?>" style="width: 45%;" disabled></td>
													<td style="vertical-align: middle;"><input type="text" class="form-control" value="<?php echo $cre_colind_ladder; ?>" disabled></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<h4 class="text-blue"> DATOS DEL CRÉDITO </h4>
							<div class="container-fluid" style="border: 1px solid #71baf5; border-radius: 8px; background-color: rgba(113, 186, 245, 0.3);">
								<div class="row mt-4 mb-4">
									<!-- <div class="col-md-3">
										<div class="form-group">
											<label>Tipo de Crédito:</label>
											<select id="cmb_cre_tip" name="cmb_cre_tip" class="form-control" disabled>
												<option value="">-</option>
												<option value="1" < ?php if ($cre_tip1 == 1) echo 'selected' ?>>VENTA NUEVA</option>
												<option value="2" < ?php if ($cre_tip1 == 2) echo 'selected' ?>>ADENDA</option>
												<option value="3" < ?php if ($cre_tip1 == 3) echo 'selected' ?>>ACUERDO PAGO</option>
												<option value="4" < ?php if ($cre_tip1 == 4) echo 'selected' ?>>RE-VENTA</option>
											</select>
										</div>
									</div> -->
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Tipo Cuota:</label>
											<select id="cmb_cuotip_id" name="cmb_cuotip_id" class="form-control" disabled>
												<option value="4">CUOTA FIJA</option>
												<option value="3">CUOTA LIBRE</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-md-3">
										<div id="tip_ade" style="display: block;">
											<div class="form-group">
												<label>Tipo Adenda:</label>
												<select id="cmb_cre_tipade" name="cmb_cre_tipade" class="form-control">
													<option value="1" < ?php if ($cre_tip2 == 1) echo 'selected' ?>>REGULAR</option>
													<option value="4" < ?php if ($cre_tip2 == 4) echo 'selected' ?>>CUOTA BALON</option>
												</select>
											</div>
										</div>
										<div id="div_cuo_res" style="display: none;">
											<div class="form-group">
												<label>Cuotas Restantes:</label>
												<input type="number" id="txt_cre_cuo_res" name="txt_cre_cuo_res" class="form-control" value="<?php echo $cuo_res; ?>">
											</div>
										</div>
									</div> -->
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Periodo:</label>
											<select id="cmb_cre_per" name="cmb_cre_per" class="form-control" disabled>
												<option value="4">MENSUAL</option>
											</select>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Sub Periodo:</label>
											<select name="cmb_cre_subper" id="cmb_cre_subper" class="form-control" disabled>
												<option value="1" <?php if ($subper == 1) echo 'selected' ?>>MENSUAL</option>
												<option value="2" <?php if ($subper == 2) echo 'selected' ?>>QUINCENAL</option>
												<option value="3" <?php if ($subper == 3) echo 'selected' ?>>SEMANAL</option>
											</select>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Moneda:</label>
											<select name="cmb_mon_id" id="cmb_mon_id" class="form-control" disabled>
												<option value="0">Seleccione:</option>
												<option value="1" <?php if ($mon_id == 1) echo 'selected' ?>>Soles (S/)</option>
												<option value="2" <?php if ($mon_id == 2) echo 'selected' ?>>Dolares ($)</option>
											</select>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Cambio:</label>
											<input type="text" id="txt_cre_tipcam" name="txt_cre_tipcam" class="form-control text-right" value="<?php echo $cre_tipcam ?>" disabled>
										</div>
									</div>

									<!-- <div class="col-md-3">
										<div class="form-group">
											<label>Tipo de Préstamo:</label>
											<input type="text" class="form-control" value="CREDITO ASCVEH" disabled>
										</div>
									</div> -->
									<!-- <div class="col-md-2">
										<div class="form-group">
											<label>Inicial:</label>
											<input type="text" class="form-control text-right moneda" name="txt_cre_ini" id="txt_cre_ini" disabled value="<?php echo $cre_ini ?>">
										</div>
									</div> -->

								</div>
								<div class="row mb-4">
									<div class="col-md-3">
										<div class="form-group">
											<label>Precio Acordado:</label>
											<?php if ($action != 'ad_regular') { ?>
												<input type="text" class="form-control text-right moneda" id="txt_cre_preaco" name="txt_cre_preaco" value="<?php echo $cre_preaco ?>" disabled>
											<?php } else { ?>
												<input type="text" class="form-control text-right moneda" id="txt_cre_preaco_ad" name="txt_cre_preaco_ad" value="<?php echo $cre_preaco ?>" disabled>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Interés (%):</label>
											<input type="text" class="form-control text-right porcentaje" id="txt_cre_int" name="txt_cre_int" value="<?php echo $cre_int ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>N° Cuotas:</label>
											<input type="text" id="txt_cre_numcuo" name="txt_cre_numcuo" class="form-control text-center" value="<?php echo $cre_numcuo ?>" maxlength="2" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Línea Aprobada:</label>
											<input type="text" id="txt_cre_linapr" name="txt_cre_linapr" class="form-control text-right" value="<?php echo $cre_linapr ?>" maxlength="10" disabled>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Comprobante de Desembolso</label>
											<input type="text" class="form-control text-center" id="txt_cre_comdes" name="txt_cre_comdes" value="<?php echo $cre_comdes ?>" maxlength="250" disabled>
										</div>
									</div>
									<!-- <div class="col-md-3">
										<div class="form-group">
											<label>Retener en Caja Seguros: </label>
											<select class="form-control" id="cmb_cre_retener" name="cmb_cre_retener">
												<option value="">-</option>
												<option value="1">NO</option>
												<option value="2">SI</option>
											</select>
										</div>
									</div> -->
								</div>
								<div class="row mb-4">
									<div class="col-md-5">
										<div class="form-group">
											<label>Cuenta a Depositar:</label>
											<select class="form-control" id="cmb_cuedep_id" name="cmb_cuedep_id" disabled></select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Representante:</label>
											<input type="text" class="form-control" value="<?php echo $representante_nombre ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Estado de Carga:</label>
											<select class="form-control" disabled>
												<option value="<?php if ($cre_est_carg == 0) echo 'selected'; ?>"="">Sin cargas</option>
												<option value="<?php if ($cre_est_carg == 1) echo 'selected'; ?>">Cancelado</option>
												<option value="<?php if ($cre_est_carg == 2) echo 'selected'; ?>">Vigente</option>
												<option value="<?php if ($cre_est_carg == 3) echo 'selected'; ?>">Vencido</option>
											</select>
										</div>
									</div>
									<div class="col-md-9">
										<div class="form-group">
											<label>Observación:</label>
											<textarea class="form-control" id="txt_cre_obs" name="txt_cre_obs" style="resize: vertical;" rows="3"><?php echo $cre_obs; ?></textarea>
										</div>
									</div>
									<div class="col-md-9">
										<div class="form-group">
											<label>Detalles si tiene cargos:</label>
											<textarea class="form-control" id="txt_cre_obs" name="txt_cre_obs" style="resize: vertical;" rows="5"><?php echo $cre_carg; ?></textarea>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-md-3">
										<div class="form-group">
											<label>Fecha de Credito:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_feccre" name="txt_cre_feccre" class="form-control fecha" value="<?php echo $cre_feccre ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Fecha de Desembolso:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecdes" name="txt_cre_fecdes" class="form-control fecha" value="<?php echo $cre_fecdes ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Fecha de Facturación:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecfac" name="txt_cre_fecfac" class="form-control fecha" value="<?php echo $cre_fecfac ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Fecha de Prorrateo:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecpro" name="txt_cre_fecpro" class="form-control fecha" value="<?php echo $cre_fecpro ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<br>

							<div class="row mt-4">
								<div class="col-md-12">
									<h4 class="text-teal"> HISTORIAL </h4>
								</div>
							</div>

							<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
								<div class="row">
									<div class="col-md-6">
										<div class="row mt-4 mb-4">
											<div class="col-md-12">
												<ul>
													<li>
														<h5>Registrado por: <?php echo $usureg . ',<br> el ' . $reg; ?></h5>
													</li>
													<li>
														<h5> <?php if ($mos_usuapr == 1) { echo 'Aprobado por: ' . $usuapr . ',<br> el ' . $apr; } else { echo "Sin aprobar"; } ?> </h5>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row mt-4 mb-4">
											<div class="col-md-12">
												<ul>
													<li>
														<h5>Modificado por: <?php echo $usumod . ',<br> el ' . $mod; ?></h5>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<br>

							<div class="row">
								<div class="col-md-12">
									<h4 class="text-orange"> IMPRIMIR FORMATOS </h4>
								</div>
							</div>

							<div class="container-fluid" style="border: 1px solid #fada89; border-radius: 8px; background-color: rgba(250, 218, 137, 0.3);">
								<div class="row mt-4 mb-4">
									<div class="col-md-12">
									<?php if($cre_inmu !=5 && $cre_tip1 != 4):?>
									
									<?php if(!empty(trim($cargas))){//Inmueble semiconstruido o CONSTRUIDO con cargas?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_con_cargas_semi_y_construido.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Contrato Inmueble</a> &nbsp;

									<?php } else{?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_sin_cargas_semi_y_construido.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Contrato Inmueble</a> &nbsp;
									<?php }?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_simulador.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Simulador</a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_contrato_semi_y_construido.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Anexo01 y 02</a> &nbsp;
									<?php endif?>
									
									<!-- //Inmueble terreno -->
									<?php if($cre_inmu ==5 && $cre_tip1 != 4):?>
									<?php if(!empty(trim($cargas))){//Inmueble semiconstruido o CONSTRUIDO con cargas?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_con_cargas_terreno.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Contrato Inmueble</a> &nbsp;

									<?php } else{?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_sin_cargas_terreno.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Contrato Inmueble</a> &nbsp;
									<?php }?>

									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_simulador.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Simulador</a> &nbsp;

									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_contrato_terreno.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Actas</a> &nbsp;
									<?php endif?>
									<!-- //CREDITO DE HIPOTECA-->
									<?php if($cre_tip1 == 4):?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_contrato_hipoteca.php?d1='.$creditohipo_id.'&d2=pdf';?>">Contrato Hipoteca PDF</a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_contrato_hipoteca.php?d1='.$creditohipo_id.'&d2=word';?>">Contrato Hipoteca Word</a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_simulador.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Simulador</a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_contrato_terreno.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Actas</a> &nbsp;
									<?php endif?>
									<!--CUOTA FIJA Y LIBRE-->
									<?php if($num_rows_crono > 0 && $cre_tip1 != 4):
									$nom_cuo = 'Cronograma Cuotas Fijas';
									if($cuotip_id == 3)
										$nom_cuo = 'Cronograma Cuotas Libres';
									echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditohipo/doc_cronograma_fija_y_libre.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id.'">'.$nom_cuo.'</a> &nbsp;';
									endif?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditohipo/doc_anexo03.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id;?>">Declaración</a> &nbsp;
									<!-- FORMATOS IGUALES PARA TODOS-->
									<?php 
									if($cre_est == 8 && !empty($dt['tb_credito_file']))
										echo '<a class="btn btn-warning" style="color: blues;" target="_blank" title="Imprimir" href="'.$dt['tb_credito_file'].'">Resolución</a>';
									if($cre_est == 8 && !empty($dt['tb_credito_img']))
										echo '<a class="btn btn-warning" style="color: blues;" target="_blank" title="Imagen" href="'.$dt['tb_credito_img'].'">Img Resolución</a>';
									if($cuotip_id == 3 && $dt['tb_credito_numcuomax'] > 12)
										echo '<a class="btn btn-warning" style="color: blues;" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditohipo/doc_cronograma_fija_y_libre.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id.'&renov=1">Cuotas Renov.</a> &nbsp;';
									echo '<a class="btn btn-warning" style="color: blues;" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditohipo/doc_letras.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id.'">Letras de Cambio</a> &nbsp;';
									echo '<a class="btn btn-warning" style="color: blues;" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditohipo/doc_cronograma_resolver.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id.'">Resolver</a> &nbsp;';
									echo '<a class="btn btn-warning mt-4" style="color: blues;" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditohipo/doc_cronograma_resolver_abogado.php?d1='.$creditohipo_id.'&d2='.$creditohipo_id.'">Resolver para Abogado</a> &nbsp;';
									?>
									</div>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row mt-4">
						<div class="col-md-12">
							<h4 class="text-primary"> ADJUNTOS </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row mt-4 mb-4">
							<div class="col-md-12">
								<!-- <a class="btn btn-primary" onClick="credito_file_form();" style="border-radius: 8px;">Subir Imagen</a> -->
								<div id="div_credito_file_section"></div>
								<div id="div_credito_file_tabla"></div>
							</div>
						</div>
					</div>

					<div class="row mt-4">
						<div class="col-md-12">
							<h4 class="text-teal"> CRONOGRAMA DE CRÉDITO </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row mt-0 mb-0">
							<div class="col-md-12">
								<div id="div_credito_cronograma"></div>
								<hr>
								<?php if ($cre_tip1 != 3) {
									echo '<div id="div_cuota_tabla"></div>';
								} else {
									echo '<div id="div_acuerdo_tabla"></div>';
								} ?>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					<span aria-hidden="true">Cerrar</span>
				</button>
			</div>
		</div>
	</div>
</div>