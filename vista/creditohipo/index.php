<?php
	//include'archivo_prueba.php';exit();
    require_once('core/usuario_sesion.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php require_once(VISTA_URL.'templates/head.php'); ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
		<link href="./public/css/font-awesome-all-5.css" rel="stylesheet" type="text/css" />
	</head>

	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>

		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php require_once(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php require_once(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php require_once('creditohipo_vista.php'); ?>
			<!-- INCLUIR FOOTER-->
				<?php require_once(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php require_once(VISTA_URL.'templates/script.php'); ?>

		<script type="text/javascript" src="<?php echo VISTA_URL.'creditohipo/creditohipo.js?ver=24092024';?>"></script>
    <!-- <script type="text/javascript" src="< ?php echo VISTA_URL.'cliente/clientenota_form.js?ver=335464';?>"></script> -->
	</body>
</html>
