<?php 
  $fecha = new DateTime();
  
  $cajacambio_fec1 = date('01-01-Y');
  $cajacambio_fec2 = date('d-m-Y');
  $usuario_columna = 'tb_empresa_id';
  $empresa_id = $_SESSION['empresa_id'];
  $usuario_valor = $empresa_id;
  $param_tip = 'INT';
?>
<form id="form_creditogarveh_filtro" role="form">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha entre:</label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo $cajacambio_fec1; ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo $cajacambio_fec2; ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Cliente : </label>
                <input type="text" id="txt_creditoasiveh_fil_cli_nom"  class="form-control input-sm mayus" placeholder="ingrese cliente a buscar" size=45 onblur="clearClient()">
                <input type="hidden" id="hdd_creditoasiveh_fil_cli_id">
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="">Estado :</label>
                <br>
                <select class="form-control input-sm" name="cmb_estado" id="cmb_estado">
                    <option value="">-</option>
                    <option value="2">APROBADO</option>
                    <option value="7">LIQUIDADO</option>
                    <option value="4">PARALIZADO</option>
                    <option value="1">PENDIENTE</option>
                    <option value="8">RESUELTO</option>
                    <option value="3">VIGENTE</option>
                </select>
            </div>
        </div>
        <!-- <div class="col-md-2">
            <div class="form-group">
                <label for="">Sede :</label>
                <br>
                <select class="form-control input-sm" name="cmb_sede_id" id="cmb_sede_id">
                    < ?php require_once('vista/empresa/empresa_select.php');?>
                </select>
            </div>
        </div> -->
        <div class="col-md-3 hide">
            <div class="form-group">
                <label for="">Cambiar asesor :</label>
                <br>
                <input type="hidden" id="hdd_empresa_id" name="hdd_empresa_id" class="form-control" value="<?php echo $_SESSION['empresa_id'];?>" readonly>
                <select class="form-control input-sm" name="cmb_usuario_id" id="cmb_usuario_id">
                    <?php //require_once('vista/usuario/usuario_select.php');?>
                </select>
            </div>
        </div>
        <div class="col-md-2n hide">
            <?php if($_SESSION['usuariogrupo_id'] == 2): ?>
                <div class="form-group">
                    <label for="">Opciones credito :</label>
                    <br>
                    <select class="form-control input-sm" name="cmb_fil_opc_cre" id="cmb_fil_opc_cre">
                        <option value="">-</option>
                        <option value="11">FORMATO DE LIQUIDACION</option>
                        <option value="4">Paralizar Crédito</option>
                        <option value="5">Refinanciar / Liquidar</option>
                        <option value="10">Refinanciar Múltiple</option>
                        <!--option value="6">Refinanciado sin Amortizar</option>
                        <option value="7">Liquidado</option-->
                        <option value="8">Resolver Crédito</option>
                        <option value="3">Vigente</option>
                    </select>
                </div>
            <?php endif;?>
            <?php if($_SESSION['usuariogrupo_id'] != 2): ?>
                <div class="form-group">
                    <label for="">Opciones credito :</label>
                    <br>
                    <select class="form-control input-sm" name="cmb_fil_opc_cre" id="cmb_fil_opc_cre">
                        <option value="0">Selecciona aquí...</option>
                        <option value="11">FORMATO DE LIQUIDACION</option>
                    </select>
                </div>
            <?php endif;?> 
        </div>
        <div class="col-md-1"> <br>
            <div class="container-fluid"><div class="checkbox icheck"> <label> <input type="checkbox" checked id="chkcredasiveh01" name="chkcredasiveh01" value="1"> Compra Venta</label> </div></div>
        </div>
        <div class="col-md-1"> <br> 
            <div class="container-fluid"><div class="checkbox icheck"> <label> <input type="checkbox" checked id="chkcredasiveh02" name="chkcredasiveh02" value="4"> Con Hipoteca</label> </div></div>
        </div>
        <div class="col-md-1"> <br>
            <div class="container-fluid"><div class="checkbox icheck"> <label> <input type="checkbox" checked id="chkcredasiveh03" name="chkcredasiveh03" value="2"> Adendas</label> </div></div>
        </div>
        <div class="col-md-1"> <br>
            <div class="container-fluid"><div class="checkbox icheck"> <label> <input type="checkbox" checked id="chkcredasiveh04" name="chkcredasiveh04" value="3"> Acuerdos de pago</label> </div></div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="">&nbsp;</label> <br>
                <button type="button" class="btn btn-primary btn-block" id="btnfiltrar"><i class="fa fa-search fa-fw"></i> Filtrar</button>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="">&nbsp;</label> <br>
                <button type="button" class="btn btn-info" id="btnreset"><i class="fa fa-filter fa-fw"></i> Limpiar</button>
            </div>
        </div>
    </div>
</form>