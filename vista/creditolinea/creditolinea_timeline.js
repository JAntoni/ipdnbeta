var regmod_id = ''; //contendrá el id del registro donde se ingresará el comentario. ya sea de la tabla historial, o credito linea o otra 

function action_click(b){
  regmod_id = parseInt(b.id.split('-')[1]);
}

$('.input-sm').keyup(function(event) {
    var tecla = event.which || event.keyCode;
    var cant_coment = parseInt($('#cantidad_comentarios-'+regmod_id).val());
    //alert("cant comentarios anteriores "+ parseInt($('#cantidad_comentarios-'+regmod_id).val()));return;
    if (tecla == 13) {
        event.preventDefault();
        //alert("el comentario del #inp_comentario-" + regmod_id + " es: " + $('#inp_comentario-'+regmod_id).val());; return;
        $.ajax({
            type: "POST",
            url: VISTA_URL + "comentario/comentario_controller.php",
            async: true,
            dataType: "json",
            data: ({
                accion: "insertar",
                coment_det: $('#inp_comentario-'+regmod_id).val(),
                regmodid: regmod_id,
                cant_coment: cant_coment,
                nom_tabla: 'tb_creditolinea',
                columnas: ', cl.tb_credito_id',
                tabla_alias: 'tb_creditolinea cl',
                alias_columnaPK: 'cl.tb_creditolinea_id'
            }),
            beforeSend: function () {
            },
            success: function (data) {
                //console.log(data.estado);
                if(data.estado == '0'){
                    notificacion_info('Advertencia: El campo de texto está vacío', 10000);
                }
                else{
                  var div_comentar = '';
                  if(cant_coment == 0){
                    div_comentar = document.getElementById("div_comentar-"+regmod_id);
                    div_comentar.insertAdjacentHTML("beforeBegin", data.html_new_coment);
                  }
                  else{
                    div_comentar = document.getElementById("div_comentarios_anteriores-"+regmod_id);
                    div_comentar.insertAdjacentHTML("beforeEnd", data.html_new_coment);
                  }
                  $('#inp_comentario-'+regmod_id).val('');
                  notificacion_success('Confirmación: '+ data.mensaje, 5000);
                  console.log(data.mensaje);
                  $('#cantidad_comentarios-'+regmod_id).val(cant_coment+1);
                }
            },
            complete: function (data) {
            },
            error: function (data) {
                alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            }
        });
    }
});

function comentario_eliminar(coment_id, h_id){
    $.confirm({
        icon: 'fa fa-remove',
        title: 'Eliminar',
        content: '¿Desea eliminar este comentario?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
          si: function(){
            //alert("el id del comentario es: "+coment_id);
            $.ajax({
              type: "POST",
              url: VISTA_URL+"comentario/comentario_controller.php",
              async:true,
              dataType: "json",
              data: ({
                accion: "eliminar",
                coment_id: coment_id
              }),
              beforeSend: function() {
              },
              success: function(data){
                var cant_coment = parseInt($('#cantidad_comentarios-'+h_id).val());
                if(parseInt(data.estado) == 1){
                  $('#com-'+coment_id).remove();
                  if(cant_coment-1 == 0){
                    $("#div_comentarios_anteriores-"+h_id).remove();
                  }
                  $('#cantidad_comentarios-'+h_id).val(cant_coment-1);
                  notificacion_success(data.mensaje, 5000);
                }
                else{
                  alerta_warning('Alerta', data.mensaje); //en generales.js
                }
              },
              complete: function(){
              },
              error: function(data){
                alerta_error('Error', 'ERRROR!:'+ data.mensaje); //en generales.js
                console.log(data.responseText);
              }
            });
          },
          no: function () {}
        }
    });
}

function creditolinea_eliminar(creditolinea_id, credito_id){
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Eliminar',
    content: '¿Desea eliminar este mensaje?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"creditolinea/creditolinea_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "eliminar",
            creditolinea_id: creditolinea_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Eliminando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              $('#modal_creditolinea_timeline').modal('hide');
              $('.modal-backdrop').remove();
              creditolinea_timeline(credito_id);
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}
$(document).ready(function() {
  console.log('linea 888');

  $("#creditolinea_form").validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL +"creditolinea/creditolinea_controller.php",
        async:true,
        dataType: "json",
        data: $("#creditolinea_form").serialize(),
        beforeSend: function() {
          
        },
        success: function(data){
          if(parseInt(data.estado) > 0){
            notificacion_success(data.mensaje, 3000)
            var modulo = $('#hdd_modulo').val();
            var credito_id = $('#credito_id').val();

            $('#modal_creditolinea_form').modal('hide');

            if(modulo == 'creditogarveh'){
              $('#modal_creditogarveh_timeline').modal('hide');
              console.log('se abre el hostorial con: ' + credito_id)
              credito_his(credito_id);
            }
          }
        },
        complete: function(data){
          console.log(data);
        }
      });
    },
    rules: {
      txt_creditolinea_det: {
        required: true
      }
    },
    messages: {
      txt_creditolinea_det: {
        required: 'Ingresa un comentario para el crédito'
      }
    }
  });
});