<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../creditolinea/Creditolinea.class.php');
  $oCreditolinea = new Creditolinea();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
		$creditolinea_id = intval($_POST['creditolinea_id']);
		$creditotipo_id = intval($_POST['creditotipo_id']);
		$credito_id = intval($_POST['credito_id']);
		$usuario_id = intval($_SESSION['usuario_id']);
		$creditolinea_det = $_POST['txt_creditolinea_det'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el creditolinea.';
 		if($oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Creditolinea registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$creditolinea_id = intval($_POST['creditolinea_id']);

 		$creditolinea_det = $_POST['txt_creditolinea_det'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el creditolinea. '.$creditolinea_det;

 		if($oCreditolinea->modificar($creditolinea_id, $creditolinea_det)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Creditolinea modificado correctamente. '.$creditolinea_id;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$creditolinea_id = intval($_POST['creditolinea_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el mensaje de la Línea de Tiempo: '.$creditolinea_id;

 		if($oCreditolinea->eliminar($creditolinea_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Mensaje de Línea de Tiempo eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>