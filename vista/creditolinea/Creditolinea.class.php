<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Creditolinea extends Conexion{

    public $tb_creditolinea_id;
    public $tb_creditolinea_xac;
    public $tb_creditolinea_reg;
    public $tb_creditotipo_id;
    public $tb_credito_id;
    public $tb_usuario_id;
    public $tb_creditolinea_det;
    public $tb_creditolinea_subnom;

    function insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_creditolinea(tb_creditotipo_id, tb_credito_id, tb_usuario_id, tb_creditolinea_det)
          VALUES (:creditotipo_id, :credito_id, :usuario_id, :creditolinea_det)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditolinea_det", $creditolinea_det, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($creditolinea_id, $creditolinea_det){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_creditolinea SET tb_creditolinea_det =:creditolinea_det WHERE tb_creditolinea_id =:creditolinea_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditolinea_det", $creditolinea_det, PDO::PARAM_STR);
        $sentencia->bindParam(":creditolinea_id", $creditolinea_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($creditolinea_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_creditolinea SET tb_creditolinea_xac = 0 WHERE tb_creditolinea_id =:creditolinea_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditolinea_id", $creditolinea_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUno($creditolinea_id){
      try {
        $sql = "SELECT * FROM tb_creditolinea WHERE tb_creditolinea_id =:creditolinea_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditolinea_id", $creditolinea_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }


    // -------------------------------------------------------------------------------------- //

    function consultar_proximo(){
      try {
        $sql = "select coalesce((select max(tb_creditolinea_id) from tb_creditolinea),1) as newcod";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

        return $resultado;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_creditolineas($creditotipo_id, $credito_id){
      try {
        $sql = "SELECT crelin.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM tb_creditolinea crelin  
          INNER JOIN tb_usuario us on us.tb_usuario_id = crelin.tb_usuario_id
          where tb_creditotipo_id =:creditotipo_id and tb_credito_id =:credito_id and tb_creditolinea_xac = 1 ORDER BY tb_creditolinea_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function refrescar_historial_antiguo_por_nuevo($creditolinea_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_creditolinea WHERE tb_creditolinea_id = :creditolinea_id_prox";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditolinea_id_prox", $creditolinea_id, PDO::PARAM_INT);
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function insertar_nuevo(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_creditolinea(tb_creditotipo_id, tb_credito_id, tb_usuario_id, tb_creditolinea_det, tb_creditolinea_xac, tb_creditolinea_subnom)
          VALUES (:creditotipo_id, :credito_id, :usuario_id, :creditolinea_det, :creditolinea_xac, :creditolinea_subnom)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $this->tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditolinea_det", $this->tb_creditolinea_det, PDO::PARAM_INT);
        $sentencia->bindParam(":creditolinea_xac", $this->tb_creditolinea_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":creditolinea_subnom", $this->tb_creditolinea_subnom, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function listar_historial_titulo($creditotipo_id, $credito_id, $creditolinea_subnom){
      try {
        $sql = "SELECT crelin.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM tb_creditolinea crelin  
          INNER JOIN tb_usuario us on us.tb_usuario_id = crelin.tb_usuario_id
          WHERE 
            tb_creditotipo_id =:creditotipo_id 
          AND
            tb_creditolinea_subnom =:creditolinea_subnom
          AND
            tb_credito_id = :credito_id
          AND 
            tb_creditolinea_xac = 1 
          ORDER BY tb_creditolinea_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditolinea_subnom", $creditolinea_subnom, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

  }

?>
