<?php
	session_name("ipdnsac");
  session_start();
	require_once('../creditolinea/Creditolinea.class.php');
  $oCreditolinea = new Creditolinea();
  require_once('../funciones/fechas.php');
  $usuariogrupo_id = $_SESSION['usuariogrupo_id'];
  require_once('../comentario/Comentario.class.php');
  $oComentario = new Comentario();

  $creditotipo_id = intval($_POST['creditotipo_id']);
  if($creditotipo_id == 1){
    require_once('../creditomenor/Creditomenor.class.php');
    $oCredito = new Creditomenor();
  }
/*    */
  $credito_id = intval($_POST['credito_id']);
  $lista = ''; $cont = 0;
  $result = $oCreditolinea->listar_creditolineas($creditotipo_id, $credito_id);
  	if($result['estado'] == 1){
  		foreach ($result['data'] as $key => $value) {
        $cont++;
  			$lista .='
	  			<li>
				    <i class="fa fa-envelope bg-blue"></i>
				    <div class="timeline-item">';
            if($usuariogrupo_id == 2){
              $lista .='<button type="button" class="close" data-dismiss="alert" aria-hidden="true" title="Eliminar historial" onclick="creditolinea_eliminar('.$value['tb_creditolinea_id'].','.$value['tb_credito_id'].')">x</button>';
            }
            $lista .='
				      <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($value['tb_creditolinea_reg']).'</span>
				      <div class="user-block">
                      <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="user image">
                      <span class="username">
                        <a href="#">'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</a>
                      </span>
                      <span class="description">'.$value['tb_creditolinea_det'].'</span>
                    </div>
				      <div class="timeline-body">';
        $result1 = $oComentario->filtrar_comentarios_por($value['tb_creditolinea_id'], 'tb_creditolinea');
        $lista .= '<input type="hidden" id="cantidad_comentarios-'.$value['tb_creditolinea_id'].'" value="'.intval($result1['cantidad']).'">';
        if($result1['estado'] == 1){
          $lista .= '<div class="box-footer box-comments" id="div_comentarios_anteriores-'.$value['tb_creditolinea_id'].'">';
          foreach($result1['data'] as $key1 => $value1){
            $lista .= '<div class="box-comment" id="com-'.$value1['tb_coment_id'].'">
                                <img class="img-circle img-sm" src="'.$value1['tb_usuario_fot'].'" alt="User Image">
                                <div class="comment-text">
                                  <span class="username">
                                  '.$value1['tb_usuario_nom'].' '.$value1['tb_usuario_ape'];
                      if(intval($_SESSION['usuarioperfil_id']) == 1){
                        $lista .='<button type="button" class="close" data-dismiss="alert" aria-hidden="true" title="Eliminar comentario" onclick="comentario_eliminar('.$value1['tb_coment_id'].','.$value['tb_creditolinea_id'].')">x</button>';
                      }
                        $lista .='</span>'.$value1['tb_coment_det'].'.
                                  <span class="text-muted pull-right">'. date("h:i a, j-m-y", strtotime($value1['tb_coment_fecreg'])) .'</span>
                                </div>
                              </div>
            ';
          }
          $lista .= '</div>';
        }
                $lista .= '
                          <div class="box-footer" id="div_comentar-'.$value['tb_creditolinea_id'].'">
                              <img class="img-responsive img-circle img-sm" src="'.$_SESSION['usuario_fot'].'" alt="Alt Text">
                              <div class="img-push">
                                <input type="text" class="form-control input-sm" id="inp_comentario-'.$value['tb_creditolinea_id'].'" placeholder="Escribe y presiona Enter para postear un comentario" onclick=action_click(this)>
                              </div>
                          </div>
				    	</div>
				    </div>
				  </li>
	  		';
  		}
  	}
  $result = NULL;

  if($lista == '' || $cont <=1){
    $result = $oCredito->mostrarUno($credito_id);
      if($result['estado'] == 1){
        $credito_his = $result['data']['tb_credito_his'];

        $array_his = explode('<br>', $credito_his);
        $array_his = array_filter($array_his);

        for ($i=0; $i < count($array_his); $i++) { 
          $lista .='
            <li>
              <i class="fa fa-envelope bg-blue"></i>
              <div class="timeline-item">
                <h3 class="timeline-header"><a>SISTEMA</a></h3>
                <div class="timeline-body">';
                  $lista .= $array_his[$i].'
                </div>
              </div>
            </li>
          ';
        }
      }
    $result = NULL;
  }
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditolinea_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Historial del Crédito</h4>
      </div>
      <div class="modal-body">
        <ul class="timeline">
				  <?php echo $lista;?>
				</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditolinea/creditolinea_timeline.js';?>"></script>