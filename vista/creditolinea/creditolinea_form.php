<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../creditolinea/Creditolinea.class.php');
  $oCreditolinea = new Creditolinea();

  $action = $_POST['action'];
  $creditolinea_id = intval($_POST['creditolinea_id']);
  $creditotipo_id = intval($_POST['creditotipo_id']);
  $credito_id = intval($_POST['credito_id']);
  $usuario_id = intval($_SESSION['usuario_id']);
  $modulo = $_POST['modulo'];

  $detalle = '';

  $result = $oCreditolinea->mostrarUno($creditolinea_id);
    if($result['estado'] == 1){
      $detalle = $result['data']['tb_creditolinea_det'];
    }
  $result = NULL;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditolinea_form" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Historial de Procesos Realizados</h4>
      </div>
      <form id="creditolinea_form">
        <input name="creditolinea_id" id="creditolinea_id" type="hidden" value="<?php echo $creditolinea_id;?>">
        <input name="creditotipo_id" id="creditotipo_id" type="hidden" value="<?php echo $creditotipo_id;?>">
        <input name="credito_id" id="credito_id" type="hidden" value="<?php echo $credito_id;?>">
        <input name="usuario_id" id="usuario_id" type="hidden" value="<?php echo $usuario_id;?>">
        <input name="action" id="action" type="hidden" value="<?php echo $action;?>">
        <input name="hdd_modulo" id="hdd_modulo" type="hidden" value="<?php echo $modulo;?>">

        <div class="modal-body">
          <?php if($action == 'eliminar'):?>
            <div class="alert alert-danger alert-dismissible">
              <h4><i class="icon fa fa-ban"></i> Eliminar Historial</h4>
              Estás a punto de eliminar este comentario de Historial, preciona Eliminar para poder guardar los cambios.
            </div>
          <?php endif;?>

          <textarea class="form-control" id="txt_creditolinea_det" name="txt_creditolinea_det" rows="10"><?php echo $detalle;?></textarea>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <?php if($action == 'eliminar'):?>
              <button type="submit" class="btn btn-danger">Eliminar</button>
            <?php endif;?>
            <?php if($action != 'eliminar'):?>
              <button type="submit" class="btn btn-info">Guardar</button>
            <?php endif;?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditolinea/creditolinea_timeline.js?ver=1';?>"></script>