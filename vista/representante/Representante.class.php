<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Representante extends Conexion{

    function insertar($representante_nom, $representante_dni, $representante_dir, $representante_den){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_representante(tb_representante_xac, tb_representante_nom, tb_representante_dni, tb_representante_dir, tb_representante_den)
          VALUES (1, :representante_nom, :representante_dni, :representante_dir, :representante_den)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":representante_nom", $representante_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_dni", $representante_dni, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_dir", $representante_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_den", $representante_den, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($representante_id, $representante_nom, $representante_dni, $representante_dir, $representante_den){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_representante SET tb_representante_nom =:representante_nom, tb_representante_dni =:representante_dni, tb_representante_dir =:representante_dir, tb_representante_den =:representante_den WHERE tb_representante_id =:representante_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);
        $sentencia->bindParam(":representante_nom", $representante_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_dni", $representante_dni, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_dir", $representante_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_den", $representante_den, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($representante_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_representante SET tb_representante_xac=0 WHERE tb_representante_id =:representante_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($representante_id){
      try {
        $sql = "SELECT * FROM tb_representante WHERE tb_representante_id =:representante_id AND tb_representante_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_representantes(){
      try {
        $sql = "SELECT * FROM tb_representante WHERE tb_representante_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
