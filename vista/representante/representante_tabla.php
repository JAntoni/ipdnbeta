<?php
  require_once('../../core/usuario_sesion.php');
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'representante/Representante.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../representante/Representante.class.php');
    require_once('../funciones/fechas.php');
  }
  $oRepresentante = new Representante();

?>
<table id="tbl_horario" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Representante</th> 
      <th>DNI</th>
      <th>Dirección</th>
      <th>Definición</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oRepresentante->listar_representantes();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_representante_id'];?></td>
            <td><?php echo $value['tb_representante_nom'];?></td>
            <td><?php echo $value['tb_representante_dni'];?></td>
            <td><?php echo $value['tb_representante_dir']; ?></td>
            <td><?php echo $value['tb_representante_den']; ?></td>
            <td align="center">
              <a class="btn btn-warning btn-xs" title="Editar" onclick="representante_form(<?php echo "'M', ".$value['tb_representante_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="representante_form(<?php echo "'E', ".$value['tb_representante_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
