
$(document).ready(function(){
	console.log('cambios guardados');
  $('#form_representante').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"representante/representante_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_representante").serialize(),
				beforeSend: function() {
					$('#representante_mensaje').show(400);
					$('#btn_guardar_representante').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#representante_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#representante_mensaje').html(data.mensaje);
                        var vista = $("#hdd_vista2").val();
                        console.log(vista);
                        alerta_success("EXITO","REPRESENTANTE AGREGADO");
                        if(vista =='representante'){
                            representante_tabla();
                        }
                        if(vista =='cuentadeposito'){
                            cargar_representante();
                        }
                        if(vista =='creditogarveh'){
                            cargar_representante();
                        }
                        $('#modal_registro_representante').modal('hide');
					}
					else{
		      	$('#representante_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#representante_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_representante').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#representante_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#representante_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_representante_nom: {
				required: true,
				minlength: 2
			},
			txt_representante_dni: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 8
			},
			txt_representante_dir: {
				required: true,
				minlength: 5
			},
			txt_representante_den: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			txt_representante_nom: {
				required: "Ingrese un nombre para el Representante",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_representante_dni: {
				required: "Ingrese el DNI del Representante",
				digits: "Solo números por favor",
				minlength: "El DNI como mínimo debe tener 8 números",
				maxlength: "El DNI como máximo debe tener 8 números"
			},
			txt_representante_dir: {
				required: "Ingrese una dirección para el Representante",
				minlength: "La dirección debe tener como mínimo 5 caracteres"
			},
			txt_representante_den: {
				required: "Ingrese una definición para el Representante",
				minlength: "La definición debe tener como mínimo 2 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
