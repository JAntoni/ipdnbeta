<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../representante/Representante.class.php');
  $oRepresentante = new Representante();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$representante_nom = mb_strtoupper($_POST['txt_representante_nom'], 'UTF-8');
 		$representante_dni = $_POST['txt_representante_dni'];
 		$representante_dir = $_POST['txt_representante_dir'];
 		$representante_den = $_POST['txt_representante_den'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar al Representante.';
 		if($oRepresentante->insertar($representante_nom, $representante_dni, $representante_dir, $representante_den)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Representante registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$representante_id = intval($_POST['hdd_representante_id']);
 		$representante_nom = mb_strtoupper($_POST['txt_representante_nom'], 'UTF-8');
 		$representante_dni = $_POST['txt_representante_dni'];
 		$representante_dir = $_POST['txt_representante_dir'];
 		$representante_den = $_POST['txt_representante_den'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar al Representante.';

 		if($oRepresentante->modificar($representante_id, $representante_nom, $representante_dni, $representante_dir, $representante_den)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Representante modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$representante_id = intval($_POST['hdd_representante_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al Representante.';

 		if($oRepresentante->eliminar($representante_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Representante eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>