<?php
require_once('Representante.class.php');
$oRepresentante = new Representante();

$representante_id = (empty($representante_id))? 0 : $representante_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value=""></option>';

//PRIMER NIVEL
$result = $oRepresentante->listar_representantes();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($representante_id == $value['tb_representante_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_representante_id'].'" '.$selected.'>'.$value['tb_representante_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>