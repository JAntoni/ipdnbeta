<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Calculoliquidacion extends Conexion{

    function listar_cuotas_impagas_entre_rango($credito_id, $fecha_inicio, $fecha_fin){
      try {
        $sql = "SELECT * 
          FROM tb_cuota c
          WHERE c.tb_credito_id = :credito_id 
          AND c.tb_creditotipo_id = 1
          AND c.tb_cuota_fec > :fecha_inicio AND tb_cuota_fec <= :fecha_fin AND tb_cuota_est != 2 AND tb_cuota_xac = 1"; //cuotas impagas entre dos fechas

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle vencidas impagas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_cuotas_menores_condicion_fecha($credito_id, $fecha, $condicion){
      try {
        $sql = "SELECT * 
          FROM tb_cuota c
          WHERE c.tb_cuota_xac = 1  
          AND c.tb_credito_id = :credito_id 
          AND c.tb_creditotipo_id = 1
          AND c.tb_cuota_fec ".$condicion." :fecha AND tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle vencidas impagas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function ultima_cuota_facturada($cre_id){
      try {
        $sql = "SELECT * 
                from tb_cuota c
                where tb_cuota_xac = 1 
                AND c.tb_credito_id =:cre_id 
                AND c.tb_creditotipo_id = 1
                AND tb_cuota_fec <= DATE(NOW()) 
                AND tb_cuota_xac = 1
                order by tb_cuota_fec desc limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function ultima_cuota_facturada_pagada($credito_id){
      try {
        $sql = "SELECT * 
            FROM tb_cuota c
            WHERE c.tb_credito_id =:credito_id 
            AND c.tb_creditotipo_id = 1 
            AND tb_cuota_est = 2
            AND tb_cuota_xac = 1
            ORDER BY tb_cuota_fec DESC LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function obtener_primera_cuota($credito_id){
      try {
        $sql = "SELECT * 
                FROM tb_cuota c 
                WHERE c.tb_credito_id =:credito_id 
                AND c.tb_creditotipo_id = 1 
                AND tb_cuota_xac = 1 
                ORDER BY tb_cuota_fec DESC LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuota facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_cronograma_credito($credito_id, $creditotipo_id, $tabla_credito){
      try {
        $sql = "SELECT
          cuo.tb_cuota_id,
          cuo.tb_cuota_num,
          cuo.tb_cuota_fec,
          cuo.tb_cuota_cap,
          cuo.tb_cuota_amo,
          cuo.tb_cuota_int,
          cuo.tb_cuota_pro,
          cuo.tb_cuota_cuo,
          cuo.tb_cuota_est,
          cre.tb_cuotatipo_id,
          cre.tb_credito_numcuo,
          cre.tb_credito_numcuomax
        FROM ".$tabla_credito." cre
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id =:creditotipo_id)
        WHERE tb_cuota_xac = 1 AND cre.tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    

    function siguiente_cuota_facturada($credito_id, $numero_cuota){
      try {
        $sql = "SELECT * 
                from tb_cuota c
                where tb_cuota_xac = 1 
                AND c.tb_credito_id =:cre_id 
                AND c.tb_creditotipo_id = 1
                AND tb_cuota_num = :cuota_num
                order by tb_cuota_fec desc limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_num", $numero_cuota, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
