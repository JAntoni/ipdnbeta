<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../../core/usuario_sesion.php');

require_once("../vencimiento/Vencimiento.class.php");
$oVencimiento = new Vencimiento();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota;
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once("../morasugerida/Morasugerida.class.php");
$oMorasugerida = new Morasugerida();
require_once("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once("Calculoliquidacion.class.php");
$oCalculoliquidacion = new Calculoliquidacion();


require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once("../refinanciar/refinanciar_funciones.php");

$pago_moneda_id = $_POST['moneda_id'];
$cliente_id = $_POST['cliente_id'];
$credito_id = $_POST['credito_id'];
$fecha_hoy = fecha_mysql(date('d-m-Y'));

$TIPO_DE_CAMBIO = 1;
$result = $oMonedacambio->consultar(fecha_mysql($fecha_ref));
if ($result['estado'] == 1) {
  $TIPO_DE_CAMBIO = $result['data']['tb_monedacambio_val'];
}

$cuota = '';

$monto_total = 0.00;

$result = $oCredito->mostrarUno($credito_id);
if ($result['estado'] == 1) {
  $credito_fecha = $result['data']['tb_credito_feccre'];
  $credito_moneda_id = $result['data']['tb_moneda_id'];
  $cli_id = $result['data']['tb_cliente_id'];
  $credito_preaco = floatval($result['data']['tb_credito_preaco']);
  $credito_fecha_facturacion = $result['data']['tb_credito_fecfac'];
  $cre_cuotip = intval($result['data']['tb_cuotatipo_id']);
  $cre_subper_id = $result['data']['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
  $credito_int = floatval($result['data']['tb_credito_int']);
  $cre_diaspa = intval($result['data']['tb_credito_diapar']);
  $credito_numcuo = intval($result['data']['tb_credito_numcuo']); //captamos el número de cuotas del crédito
  $credito_numcuomax = $result['data']['tb_credito_numcuomax']; // optenemos el valor del gps guardado en el crédito
}
$result = null;

$mon_nom = simbolo_moneda($credito_moneda_id); //1 de vuelve s/, 2 $usd

$ultima_cuota_pagada_fecha = '';
$cuota_facturada_pagada = 'NO';

$TOTAL_INTERESES_PREVIOS = 0;
$TOTAL_PAGOS_PREVIOS = 0;

$TOTAl_INTERESES_POSTERIORES = 0;
$TOTAl_PAGOS_POSTERIORES = 0;
$TOTAl_MORAS_SUGERIDAS = 0;
$PRORRATEO_TOTAL = 0;

if($cre_cuotip == 2){
  /** VER LA ULTIMA CUOTA GENERADA EN EL SISTEMA **/
  //ULTIMA CUOTA HASTA EL DIA DE HOY
  $result = $oCalculoliquidacion->ultima_cuota_facturada($credito_id);
    if ($result['estado'] == 1) {   
      $Capital_cuota_facturada = $result['data']['tb_cuota_cap'];
      $numero_cuota = $result['data']['tb_cuota_num'];
      $cuota_estado = $result['data']['tb_cuota_est'];
      $ultima_cuota_facturada_fecha = $result['data']['tb_cuota_fec'];
      $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
      $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
      //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
      $ultima_cuota_facturada_interes_valido = formato_numero($operacion);

      if($credito_numcuo > $numero_cuota && strtotime($fecha_hoy) >= strtotime($value['tb_cuota_fec'])){
        $siguiente_busqueda = $numero_cuota+1;
        $result1 = $oCalculoliquidacion->siguiente_cuota_facturada($credito_id, $siguiente_busqueda);
        if ($result1['estado'] == 1) {
          $Capital_cuota_facturada = $result1['data']['tb_cuota_cap'];
        }else{
          $Capital_cuota_facturada = 0.00;
        }
        $result1 = NULL;
      }

      if ($cuota_estado != 2) {
        //ULTIMA CUOTA PAGADA DEL CREDITO
        $result1 = $oCalculoliquidacion->ultima_cuota_facturada_pagada($credito_id);
        if ($result1['estado'] == 1) {
          $cuota_facturada_pagada = 'SI';
          $ultima_cuota_pagada_fecha = $result1['data']['tb_cuota_fec'];
        }
        $result1 = NULL;

        if ($cuota_facturada_pagada == 'SI'){
          $result1 = $oCalculoliquidacion->listar_cuotas_impagas_entre_rango($credito_id, $ultima_cuota_pagada_fecha, $ultima_cuota_facturada_fecha);
          if ($result1['estado'] == 1) {
            //$bandera = "SII";
            foreach ($result1['data'] as $key => $value) {
              $CUOTAS_IMPAGAS += $value['tb_cuota_cuo'];
              //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
              $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
              if ($result2['estado'] == 1) {
                $PAGOS_PARCIALES += $result2['data']['importe_total'];
              }
              $result2 = NULL;

              $cuotas_vencidas_impagas .= '<b>' . mostrar_fecha($value['tb_cuota_fec']) . '</b>, ';

              /** CALCULAR MORA **/
                $pagos_cuota = 0;
                $mod_id = 1;
                $resultingreso = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
                if ($resultingreso['estado'] == 1) {
                  foreach ($resultingreso['data'] as $key => $value1) {
                    $pagos_cuota += floatval($value1['tb_ingreso_imp']);
                  }
                }
                $resultingreso = NULL;
                
                $pagos = $pagos_cuota;

                $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);

                $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
                $diasdelmes = date('t', strtotime($diasdelmes1));
            
                $diastranscurridos = $dias;
                $monto_pagar = 0;
                $monto_pagar = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos);

                //$saldo = $monto_pagar - $pagos_cuota;
                $saldo_srt .= '<b>' . $monto_pagar . '</b>, ';

                $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);

                $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));

                $dias_srt .= '<b>' . $diferencia_en_dias . '</b>, ';

                $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
                if($resultmora['estado']==1){
                  $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
                }else{
                  $porcentajemora = 0.00;
                }
                $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
                //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
                $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
                $resultmora = null;
              /** CALCULAR MORA **/
            }
            $result1 = NULL;

            $SALDO_CUOTAS_IMPAGAS = $CUOTAS_IMPAGAS - $PAGOS_PARCIALES; //SE SUMA AL CAPITAL RESTANTE DE LA ULTIMA CUOTA
            $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
          }
          $result = NULL;
        }

        if ($cuota_facturada_pagada == 'NO'){
          if ($numero_cuota >= 1) {
            $result1 = $oCalculoliquidacion->listar_cuotas_impagas_entre_rango($credito_id, $credito_fecha, $ultima_cuota_facturada_fecha);
            if ($result1['estado'] == 1) {
              //$bandera = "SII";
              foreach ($result1['data'] as $key => $value) {
                $CUOTAS_IMPAGAS += $value['tb_cuota_cuo'];
                //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
                $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
                if ($result2['estado'] == 1) {
                  $PAGOS_PARCIALES += $result2['data']['importe_total'];
                }
                $result2 = NULL;
  
                $cuotas_vencidas_impagas .= '<b>' . mostrar_fecha($value['tb_cuota_fec']) . '</b>, ';
  
                /** CALCULAR MORA **/
                  $pagos_cuota = 0;
                  $mod_id = 1;
                  $resultingreso = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
                  if ($resultingreso['estado'] == 1) {
                    foreach ($resultingreso['data'] as $key => $value1) {
                      $pagos_cuota += floatval($value1['tb_ingreso_imp']);
                    }
                  }
                  $resultingreso = NULL;
                  
                  $pagos = $pagos_cuota;
  
                  $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);
  
                  $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
                  $diasdelmes = date('t', strtotime($diasdelmes1));
              
                  $diastranscurridos = $dias;
                  $monto_pagar = 0;
                  $monto_pagar = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos);
  
                  //$saldo = $monto_pagar - $pagos_cuota;
                  $saldo_srt .= '<b>' . $monto_pagar . '</b>, ';
  
                  $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);
  
                  $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));
  
                  $dias_srt .= '<b>' . $diferencia_en_dias . '</b>, ';
  
                  $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
                  if($resultmora['estado']==1){
                    $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
                  }else{
                    $porcentajemora = 0.00;
                  }
                  $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
                  //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
                  $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
                  $resultmora = null;
                /** CALCULAR MORA **/
  
              }
              $SALDO_CUOTAS_IMPAGAS = $CUOTAS_IMPAGAS - $PAGOS_PARCIALES; //SE SUMA AL CAPITAL RESTANTE DE LA ULTIMA CUOTA
              $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
            }
            $result1 = NULL;
          }
        }
      }

      if ($ultima_cuota_facturada_fecha != '') {
        list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
        $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
        $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario
  
        $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
        $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
        $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
        $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);
      }
    }
  $result = NULL;
}

if($cre_cuotip == 1){
  /** VER LA ULTIMA CUOTA GENERADA EN EL SISTEMA **/
  //ULTIMA CUOTA HASTA EL DIA DE HOY
  $result = $oCalculoliquidacion->ultima_cuota_facturada($credito_id);
    if ($result['estado'] == 1) {   
      $Capital_cuota_facturada = $result['data']['tb_cuota_cap'];
      $numero_cuota = $result['data']['tb_cuota_num'];
      $cuota_estado = $result['data']['tb_cuota_est'];
      $ultima_cuota_facturada_fecha = $result['data']['tb_cuota_fec'];
      $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
      $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
      //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
      $ultima_cuota_facturada_interes_valido = formato_numero($operacion);

      /*if(strtotime($fecha_hoy) >= strtotime($value['tb_cuota_fec'])){
        $siguiente_busqueda = $numero_cuota+1;
        $result1 = $oCalculoliquidacion->siguiente_cuota_facturada($credito_id, $siguiente_busqueda);
        if ($result1['estado'] == 1) {
          $Capital_cuota_facturada = $result1['data']['tb_cuota_cap'];
        }else{
          $Capital_cuota_facturada = 0.00;
        }
        $result1 = NULL;
      }*/

      if ($cuota_estado != 2) {
        //ULTIMA CUOTA PAGADA DEL CREDITO
        $result1 = $oCalculoliquidacion->ultima_cuota_facturada_pagada($credito_id);
        if ($result1['estado'] == 1) {
          $cuota_facturada_pagada = 'SI';
          $ultima_cuota_pagada_fecha = $result1['data']['tb_cuota_fec'];
        }
        $result1 = NULL;

        if ($cuota_facturada_pagada == 'SI'){
          $result1 = $oCalculoliquidacion->listar_cuotas_impagas_entre_rango($credito_id, $ultima_cuota_pagada_fecha, $ultima_cuota_facturada_fecha);
          if ($result1['estado'] == 1) {
            //$bandera = "SII";
            foreach ($result1['data'] as $key => $value) {
              $CUOTAS_IMPAGAS += $value['tb_cuota_int'];
              //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
              $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
              if ($result2['estado'] == 1) {
                $PAGOS_PARCIALES += $result2['data']['importe_total'];
              }
              $result2 = NULL;

              $cuotas_vencidas_impagas .= '<b>' . mostrar_fecha($value['tb_cuota_fec']) . '</b>, ';

              /** CALCULAR MORA **/
                $pagos_cuota = 0;
                $mod_id = 1;
                $resultingreso = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
                if ($resultingreso['estado'] == 1) {
                  foreach ($resultingreso['data'] as $key => $value1) {
                    $pagos_cuota += floatval($value1['tb_ingreso_imp']);
                  }
                }
                $resultingreso = NULL;
                
                $pagos = $pagos_cuota;

                $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);
                $cuo_num = $value['tb_cuota_num'];

                $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
                $diasdelmes = date('t', strtotime($diasdelmes1));
            
                $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
                $fechahoy = date('d-m-Y'); // la fecha actual del dia
                $fechaPago = $cuo_fec; // cuando me toca pagar la cuota

                if ($cuo_num == 1) {
                  $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
                }
                if ($cuo_num > 1) {
                  $fechaFacturacion = strtotime($fechaFacturacion);
                  $fechahoy = strtotime($fechahoy);
                  $fechaPago = strtotime($fechaPago);
            
                  if ($fechahoy < $fechaFacturacion) {
                    $dias = 0;
                    $variableopcional = 1;
                  }
                  if ($fechahoy > $fechaFacturacion) {
            
                    $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
                    $variableopcional = 0;
                  }
                }
            
                $diastranscurridos = $dias;
                $monto_pagar = 0;
                $monto_pagar = formato_moneda($value['tb_cuota_int']) - formato_moneda($pagos);

                if ($cuo_num == $credito_numcuomax) {
                  $monto_pagar = $value['tb_cuota_int'] - $pagos;
                      if ($monto_pagar <= 0)
                        $monto_pagar = 0;
                      else
                        $monto_pagar = mostrar_moneda($monto_pagar);
                }else {
                  $monto_pagar = $value['tb_cuota_int'] - $pagos;
                  if ($monto_pagar <= 0)
                    $monto_pagar = 0;
                  else
                    $monto_pagar = mostrar_moneda($monto_pagar);
          
                  if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
                  } else {
                    if ($cuo_num > 1) {
                      $monto_pagar = mostrar_moneda((($value['tb_cuota_int'] / $diasdelmes) * $diastranscurridos) - $pagos);
                    }
                  }
                }

                //$saldo = $monto_pagar - $pagos_cuota;
                $saldo_srt .= '<b>' . $monto_pagar . '</b>, ';

                $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);

                $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));

                $dias_srt .= '<b>' . $diferencia_en_dias . '</b>, ';

                $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
                if($resultmora['estado']==1){
                  $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
                }else{
                  $porcentajemora = 0.00;
                }
                $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
                //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
                $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
                $resultmora = null;
              /** CALCULAR MORA **/
            }
            $result1 = NULL;
            
            $SALDO_CUOTAS_IMPAGAS = $CUOTAS_IMPAGAS - $PAGOS_PARCIALES; //SE SUMA AL CAPITAL RESTANTE DE LA ULTIMA CUOTA
            $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
          }
          $result = NULL;
        }

        if ($cuota_facturada_pagada == 'NO'){
          if ($numero_cuota >= 1) {
            $result1 = $oCalculoliquidacion->listar_cuotas_impagas_entre_rango($credito_id, $credito_fecha, $ultima_cuota_facturada_fecha);
            if ($result1['estado'] == 1) {
              //$bandera = "SII";
              foreach ($result1['data'] as $key => $value) {
                $CUOTAS_IMPAGAS += $value['tb_cuota_int'];
                //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
                $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
                if ($result2['estado'] == 1) {
                  $PAGOS_PARCIALES += $result2['data']['importe_total'];
                }
                $result2 = NULL;
  
                $cuotas_vencidas_impagas .= '<b>' . mostrar_fecha($value['tb_cuota_fec']) . '</b>, ';
  
                /** CALCULAR MORA **/
                  $pagos_cuota = 0;
                  $mod_id = 1;
                  $resultingreso = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
                  if ($resultingreso['estado'] == 1) {
                    foreach ($resultingreso['data'] as $key => $value1) {
                      $pagos_cuota += floatval($value1['tb_ingreso_imp']);
                    }
                  }
                  $resultingreso = NULL;
                  
                  $pagos = $pagos_cuota;
  
                  $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);
                  $cuo_num = $value['tb_cuota_num'];
  
                  $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
                  $diasdelmes = date('t', strtotime($diasdelmes1));

                  $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
                  $fechahoy = date('d-m-Y'); // la fecha actual del dia
                  $fechaPago = $cuo_fec; // cuando me toca pagar la cuota

                  if ($cuo_num == 1) {
                    $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
                  }
                  if ($cuo_num > 1) {
                    $fechaFacturacion = strtotime($fechaFacturacion);
                    $fechahoy = strtotime($fechahoy);
                    $fechaPago = strtotime($fechaPago);
              
                    if ($fechahoy < $fechaFacturacion) {
                      $dias = 0;
                      $variableopcional = 1;
                    }
                    if ($fechahoy > $fechaFacturacion) {
              
                      $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
                      $variableopcional = 0;
                    }
                  }
              
                  $diastranscurridos = $dias;
                  $monto_pagar = 0;
                  $monto_pagar = formato_moneda($value['tb_cuota_int']) - formato_moneda($pagos);

                  if ($cuo_num == $credito_numcuomax) {
                    $monto_pagar = $value['tb_cuota_int'] - $pagos;
                        if ($monto_pagar <= 0)
                          $monto_pagar = 0;
                        else
                          $monto_pagar = mostrar_moneda($monto_pagar);
                  }else {
                    $monto_pagar = $value['tb_cuota_int'] - $pagos;
                    if ($monto_pagar <= 0)
                      $monto_pagar = 0;
                    else
                      $monto_pagar = mostrar_moneda($monto_pagar);
            
                    if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
                    } else {
                      if ($cuo_num > 1) {
                        $monto_pagar = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
                      }
                    }
                  }
  
                  //$saldo = $monto_pagar - $pagos_cuota;
                  $saldo_srt .= '<b>' . $monto_pagar . '</b>, ';
  
                  $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);
  
                  $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));
  
                  $dias_srt .= '<b>' . $diferencia_en_dias . '</b>, ';
  
                  $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
                  if($resultmora['estado']==1){
                    $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
                  }else{
                    $porcentajemora = 0.00;
                  }
                  $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
                  //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
                  $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
                  $resultmora = null;
                /** CALCULAR MORA **/
  
              }
              $SALDO_CUOTAS_IMPAGAS = $CUOTAS_IMPAGAS - $PAGOS_PARCIALES; //SE SUMA AL CAPITAL RESTANTE DE LA ULTIMA CUOTA
              $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
            }
            $result1 = NULL;
          }
        }
      }

      if ($ultima_cuota_facturada_fecha != '') {
        list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
        $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
        $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario
  
        $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
        $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
        $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
        $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);
      }
    }
  $result = NULL;
}

$CAPITAL_RESTANTE_CAMBIO = $Capital_cuota_facturada;
$SALDO_CUOTAS_IMPAGAS_CAMBIO = $SALDO_CUOTAS_IMPAGAS;
$PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;
$TOTAl_MORAS_SUGERIDAS_CAMBIO = $TOTAl_MORAS_SUGERIDAS;

if ($credito_matriz_moneda_id == 2 && $credito_moneda_id == 1) {
  $CAPITAL_RESTANTE_CAMBIO = $Capital_cuota_facturada / $TIPO_DE_CAMBIO;
  $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
  $TOTAl_MORAS_SUGERIDAS_CAMBIO = $TOTAl_MORAS_SUGERIDAS / $TIPO_DE_CAMBIO;
  $SALDO_CUOTAS_IMPAGAS_CAMBIO = $SALDO_CUOTAS_IMPAGAS / $TIPO_DE_CAMBIO;
}

$SUMA_GLOBAL_A_PAGAR = $CAPITAL_RESTANTE_CAMBIO + $SALDO_CUOTAS_IMPAGAS_CAMBIO + $TOTAl_MORAS_SUGERIDAS_CAMBIO + $PRORRATEO_TOTAL_CAMBIO;

/*
//? PASO 1: IDENTIFICAR LA ULTIMA CUOTA FACTURADA Y PAGADA: obtenemos capital actual y fecha de cuota, esta función lista las cuotas en estado 2 (pagadas) menores a la fecha de HOY
$result = $oCalculoliquidacion->ultima_cuota_facturada_pagada($credito_id);
if ($result['estado'] == 1) {
  $cuota_facturada_pagada = 'SI';
  $ultima_cuota_pagada_fecha = $result['data']['tb_cuota_fec'];
}
$result = NULL;

if ($cuota_facturada_pagada == 'SI') {
  //$bandera = "FACTURADA SI";
  //! PASO 2: LISTAMOS LAS CUOTAS MENORES EN FECHA A LA OBTENIDA: esto para poder sumar todas las cuotas pagadas y determinar el CAPITAL actual adeudado
    //listamos todas las cuotas <= a la cuota para sumar los pagos realizados
    $numero_cuotas_pagadas = 0;

    $result = $oCalculoliquidacion->listar_cuotas_menores_condicion_fecha($credito_id, $ultima_cuota_pagada_fecha, '<=');
    if ($result['estado'] == 1) {
      foreach ($result['data'] as $key => $value) {
        $numero_cuotas_pagadas ++;

        $TOTAL_INTERESES_PREVIOS += ($value['tb_cuota_int'] + $value['tb_cuota_pro']); //? el prorrateo en la cuota se debe considerar como interés adicional
        //OBTENEMOS LOS PAGOS DE CADA CUOTA PAGADA
        $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
        if ($result2['estado'] == 1) {
          $TOTAL_PAGOS_PREVIOS += $result2['data']['importe_total'];
        }
        $result2 = NULL;

        /** CALCULAR MORA //
          $pagos_cuota = 0;
          $mod_id = 1;
          $result1 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
          if ($result1['estado'] == 1) {
            foreach ($result1['data'] as $key => $value1) {
              $pagos_cuota += floatval($value1['tb_ingreso_imp']);
            }
          }
          $result1 = NULL;
          
          $pagos = $pagos_cuota;

          $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);

          $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
          $diasdelmes = date('t', strtotime($diasdelmes1));

          $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
          $fechahoy = date('d-m-Y'); // la fecha actual del dia
          $fechaPago = $cuo_fec; // cuando me toca pagar la cuota
          $cuo_num = $value['tb_cuota_num'];

          if ($cuo_num == 1) {
            $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
          }
          if ($cuo_num > 1) {
            $fechaFacturacion = strtotime($fechaFacturacion);
            $fechahoy = strtotime($fechahoy);
            $fechaPago = strtotime($fechaPago);
      
            if ($fechahoy < $fechaFacturacion) {
              $dias = 0;
              $variableopcional = 1;
            }
            if ($fechahoy > $fechaFacturacion) {
      
              $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
              $variableopcional = 0;
            }
          }
          //$cuotip_id = $value['tb_cuotatipo_id'];
      
          $diastranscurridos = $dias;

          $monto_pagar = 0;
          $monto_pagar = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos);

          if ($cre_cuotip == 1):
            if ($cuo_num == $cre_numcuomax) {
              $monto_pagar = $value['tb_cuota_int'] - $pagos;
              if ($monto_pagar <= 0)
                $monto_pagar = 0;
              else
                $monto_pagar = mostrar_moneda($monto_pagar);
            } 
            else {
              $monto_pagar = $value['tb_cuota_int'] - $pagos;
              if ($monto_pagar <= 0)
                $monto_pagar = 0;
              else
                $monto_pagar = mostrar_moneda($monto_pagar);
      
              if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
              } else {
                if ($cuo_num > 1) {
                  $monto_pagar = mostrar_moneda((($value['tb_cuota_int'] / $diasdelmes) * $diastranscurridos) - $pagos);
                }
              }
            }
          endif;

          //$saldo = $monto_pagar - $pagos_cuota;
          //$saldo_srt .= '<b>' . $monto_pagar . '</b>, ';

          $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);

          $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));

          //$dias .= '<b>' . $diferencia_en_dias . '</b>, ';

          $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
          if($resultmora['estado']==1){
            $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
          }else{
            $porcentajemora = 0.00;
          }
          $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
          //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
          $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
          $resultmora = null;
        /** CALCULAR MORA //
      }
    }
    $result = NULL;
    $FALTANTE_PAGO_MENORES = $credito_preaco + $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS;


    //* PASO 3: FECHAS VENCIDAS IMPAGAS MAYORES A ULTIMA CUOTA FACTURADA: identificamos las cuotas mayores a la facturada para contarlas y sumar sus intereses
    $cuotas_vencidas_impagas = '';
      //* la ultima cuota pagada puede ser mayor que la fecha hoy, es decir el cliente pagó una cuota adelantada
    if(strtotime($fecha_hoy) >= strtotime($ultima_cuota_pagada_fecha)){
      
      $result = $oCalculoliquidacion->listar_cuotas_impagas_entre_rango($credito_id, $ultima_cuota_pagada_fecha, $fecha_hoy);
        if ($result['estado'] == 1) {
          
          //$bandera = "SII";
          foreach ($result['data'] as $key => $value) {
            $TOTAl_INTERESES_POSTERIORES += $value['tb_cuota_int'];
            //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
            $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
            if ($result2['estado'] == 1) {
              $TOTAl_PAGOS_POSTERIORES += $result2['data']['importe_total'];
            }
            $result2 = NULL;

            $cuotas_vencidas_impagas .= '<b>' . mostrar_fecha($value['tb_cuota_fec']) . '</b>, ';

            /** CALCULAR MORA //
              $pagos_cuota = 0;
              $mod_id = 1;
              $result1 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
              if ($result1['estado'] == 1) {
                foreach ($result1['data'] as $key => $value1) {
                  $pagos_cuota += floatval($value1['tb_ingreso_imp']);
                }
              }
              $result1 = NULL;
              
              $pagos = $pagos_cuota;

              $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);

              $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
              $diasdelmes = date('t', strtotime($diasdelmes1));

              $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
              $fechahoy = date('d-m-Y'); // la fecha actual del dia
              $fechaPago = $cuo_fec; // cuando me toca pagar la cuota
              $cuo_num = $value['tb_cuota_num'];

              if ($cuo_num == 1) {
                $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
              }
              if ($cuo_num > 1) {
                $fechaFacturacion = strtotime($fechaFacturacion);
                $fechahoy = strtotime($fechahoy);
                $fechaPago = strtotime($fechaPago);
          
                if ($fechahoy < $fechaFacturacion) {
                  $dias = 0;
                  $variableopcional = 1;
                }
                if ($fechahoy > $fechaFacturacion) {
          
                  $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
                  $variableopcional = 0;
                }
              }
              //$cuotip_id = $value['tb_cuotatipo_id'];
          
              $diastranscurridos = $dias;
              $monto_pagar = 0;
              $monto_pagar = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos);

              if ($cre_cuotip == 1):
                if ($cuo_num == $cre_numcuomax) {
                  $monto_pagar = $value['tb_cuota_int'] - $pagos;
                  if ($monto_pagar <= 0)
                    $monto_pagar = 0;
                  else
                    $monto_pagar = mostrar_moneda($monto_pagar);
                } 
                else {
                  $monto_pagar = $value['tb_cuota_int'] - $pagos;
                  if ($monto_pagar <= 0)
                    $monto_pagar = 0;
                  else
                    $monto_pagar = mostrar_moneda($monto_pagar);
          
                  if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
                  } else {
                    if ($cuo_num > 1) {
                      $monto_pagar = mostrar_moneda((($value['tb_cuota_int'] / $diasdelmes) * $diastranscurridos) - $pagos);
                    }
                  }
                }
              endif;

              //$saldo = $monto_pagar - $pagos_cuota;
              $saldo_srt .= '<b>' . $monto_pagar . '</b>, ';

              $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);

              $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));

              $dias_srt .= '<b>' . $diferencia_en_dias . '</b>, ';

              $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
              if($resultmora['estado']==1){
                $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
              }else{
                $porcentajemora = 0.00;
              }
              $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
              //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
              $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
              $resultmora = null;
            /** CALCULAR MORA //

          }

          $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
        }
        $FALTANTE_PAGO_POSTERIORES = $credito_preaco + $TOTAl_INTERESES_POSTERIORES - $TOTAl_PAGOS_POSTERIORES;
      $result = NULL;
    }

    //TODO PASO 4: CALCULAR EL PRORRATEO DE DÍAS PASADOS HASTA LA FECHA ACTUAL
    $ultima_cuota_facturada_fecha = '';
    $ultima_cuota_facturada_interes_valido = 0;
    $dias_prorrateo = 0;
    $interes_por_dia = 0;

    //PUEDE O NO ESTAR PAGADA ESTA CUOTA FACTURADA, SOLO NOS SIRVE PARA CALCULAR UN PRORRATEO, LISTA TODAS LAS CUOTAS MENORES A HOY, SI EL CLIENTE PAGÓ POR ADELANTADO
    //UNA CUOTA, NO ES NECESARIO CALCULAR UN PRORRATEO
    if(strtotime($fecha_hoy) >= strtotime($ultima_cuota_pagada_fecha)){
      $result = $oCalculoliquidacion->ultima_cuota_facturada($credito_id);
        if ($result['estado'] == 1) {
          $ultima_cuota_facturada_fecha = $result['data']['tb_cuota_fec'];
          $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
          $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
          //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
          $ultima_cuota_facturada_interes_valido = formato_numero($operacion);
        }
      $result = NULL;
    }

    if ($ultima_cuota_facturada_fecha != '') {
      list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
      $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
      $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

      $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
      $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
      $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
      $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);
    }
    

    
    $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $credito_preaco + $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS + $TOTAl_INTERESES_POSTERIORES - $TOTAl_PAGOS_POSTERIORES;
    $INTERESES_CUOTAS_IMPAGAS = $TOTAl_INTERESES_POSTERIORES - $TOTAl_PAGOS_POSTERIORES;
    $INTERESES_CUOTAS_PREV = $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS;
    $TOTAL_INTERESES_VENCIDAS = $TOTAL_INTERESES_PREVIOS + $TOTAl_INTERESES_POSTERIORES;
    //$CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA += $INTERESES_CUOTAS_IMPAGAS;

    $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA;
    $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;
    $TOTAl_MORAS_SUGERIDAS_CAMBIO = $TOTAl_MORAS_SUGERIDAS;

    if ($pago_moneda_id == 2 && $credito_moneda_id == 1) {
      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA / $TIPO_DE_CAMBIO;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
      $TOTAl_MORAS_SUGERIDAS_CAMBIO = $TOTAl_MORAS_SUGERIDAS / $TIPO_DE_CAMBIO;
    }

    $SUMA_GLOBAL_A_PAGAR += $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA + $TOTAl_MORAS_SUGERIDAS_CAMBIO + $PRORRATEO_TOTAL;
}

//NO TIENE CUOTAS PAGADAS
if ($cuota_facturada_pagada == 'NO') {
  //SI NO TIENE UNA CUOTA PAGADA DE FECHA MENOR A HOY, VEAMOS SI TIENE CUOTAS IMPAGAS MENOR A HOY. TAMBIEN PUEDE SER QUE NO TENGA NINGUNA CUOTA FACTURADA, ES DECIR ESTÁ PAGANDO ANTES DE TIEMPO
  $ultima_cuota_facturada_fecha = '';
  $ultima_cuota_facturada_interes_valido = 0;
  $dias_prorrateo = 0;
  $interes_por_dia = 0;

  $result = $oCalculoliquidacion->ultima_cuota_facturada($credito_id); //SIN PAGAR
  if ($result['estado'] == 1) {
    $ultima_cuota_facturada_fecha = $result['data']['tb_cuota_fec'];
    $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
    //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
    if ($ultima_cuota_facturada_capital_restante > 0) {
      $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
      $ultima_cuota_facturada_interes_valido = formato_numero($operacion);
    } else
      $ultima_cuota_facturada_interes_valido = formato_numero($result['data']['tb_cuota_int']);
  }
  $result = NULL;

  if ($ultima_cuota_facturada_fecha != '') {
    
    //listamos todas las cuotas <= a la cuota para sumar los pagos realizados
    $todo_cuotas_impagas = '';
    $result = $oCalculoliquidacion->listar_cuotas_menores_condicion_fecha($credito_id, $ultima_cuota_facturada_fecha, '<=');
    if ($result['estado'] == 1) {
      $bandera = "";
      foreach ($result['data'] as $key => $value) {
        if($cre_cuotip == 2){
          $TOTAL_INTERESES_PREVIOS += $value['tb_cuota_cuo']; //cuota
        }
        $todo_cuotas_impagas .= '<b>' . mostrar_fecha($value['tb_cuota_fec']) . '</b>, ';
        //OBTENEMOS LOS PAGOS DE CADA CUOTA PAGADA
        $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuota_id'], 1, $value['tb_moneda_id']); //1 pago cuota
        if ($result2['estado'] == 1) {
          $TOTAL_PAGOS_PREVIOS += $result2['data']['importe_total'];
        }
        $result2 = NULL;

        //$FALTANTE_PAGO_MENORES = $credito_preaco + $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS;

        /** CALCULAR MORA //
          $pagos_cuota = 0;
          $mod_id = 1;
          $result1 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
          if ($result1['estado'] == 1) {
            foreach ($result1['data'] as $key => $value1) {
              $pagos_cuota += floatval($value1['tb_ingreso_imp']);
            }
          }
          $result1 = NULL;
          
          $pagos = $pagos_cuota;
          $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);

          $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
          $diasdelmes = date('t', strtotime($diasdelmes1));

          $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
          $fechahoy = date('d-m-Y'); // la fecha actual del dia
          $fechaPago = $cuo_fec; // cuando me toca pagar la cuota
          $cuo_num = $value['tb_cuota_num'];

          if ($cuo_num == 1) {
            $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
          }
          if ($cuo_num > 1) {
            $fechaFacturacion = strtotime($fechaFacturacion);
            $fechahoy = strtotime($fechahoy);
            $fechaPago = strtotime($fechaPago);
      
            if ($fechahoy < $fechaFacturacion) {
              $dias = 0;
              $variableopcional = 1;
            }
            if ($fechahoy > $fechaFacturacion) {
      
              $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
              $variableopcional = 0;
            }
          }
          //$cuotip_id = $value['tb_cuotatipo_id'];
      
          $diastranscurridos = $dias;
          $monto_pagar = 0;
          $monto_pagar = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos);

          //$saldo = $monto_pagar - $pagos_cuota;
          

          if ($cre_cuotip == 1):
            if ($cuo_num == $cre_numcuomax) {
              $monto_pagar = $value['tb_cuota_int'] - $pagos;
              if ($monto_pagar <= 0)
                $monto_pagar = 0;
              else
                $monto_pagar = mostrar_moneda($monto_pagar);
            } 
            else {
              $monto_pagar = $value['tb_cuota_int'] - $pagos;
              if ($monto_pagar <= 0)
                $monto_pagar = 0;
              else
                $monto_pagar = mostrar_moneda($monto_pagar);
      
              if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
              } else {
                if ($cuo_num > 1) {
                  $monto_pagar = mostrar_moneda((($value['tb_cuota_int'] / $diasdelmes) * $diastranscurridos) - $pagos);
                }
              }
            }
          endif;

          //$saldo_srt .= '<b>' . $monto_pagar . '</b>, ';

          $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($value['tb_cuota_fec']);

          $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));

          //$dias_srt .= '<b>' . $diferencia_en_dias . '</b>, ';

          $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
          if($resultmora['estado']==1){
            $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
          }else{
            $porcentajemora = 0.00;
          }
          $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
          //$moras_impagas .= '<b>' . round($morasugerida,2) . '</b>, ';
          $TOTAl_MORAS_SUGERIDAS += round($morasugerida,2);
          $resultmora = null;
        /** CALCULAR MORA //
      }

      $todo_cuotas_impagas = substr($todo_cuotas_impagas, 0, -2);
    }
    $result = NULL;

    list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
    $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
    $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

    $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
    $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
    $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
    $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);

    $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $credito_preaco + $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS;
    $INTERESES_CUOTAS_IMPAGAS = $TOTAl_INTERESES_POSTERIORES - $TOTAl_PAGOS_POSTERIORES;
    $TOTAL_INTERESES_VENCIDAS = $TOTAL_INTERESES_PREVIOS;

    $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA;
    $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;
    $TOTAl_MORAS_SUGERIDAS_CAMBIO = $TOTAl_MORAS_SUGERIDAS;

    if ($pago_moneda_id == 2 && $credito_moneda_id == 1) {
      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA / $TIPO_DE_CAMBIO;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
      $TOTAl_MORAS_SUGERIDAS_CAMBIO = $TOTAl_MORAS_SUGERIDAS / $TIPO_DE_CAMBIO;
    }

    $SUMA_GLOBAL_A_PAGAR += $CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO + $TOTAl_MORAS_SUGERIDAS_CAMBIO;

    $texto_detalle = $titulo1 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</b>. Se suma el Capital prestado de <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($credito_preaco) . '</b> con las fechas vencidas impagas del: <b>' . $todo_cuotas_impagas . '</b>, a esto restamos los pagos previos existentes que fueron: <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($TOTAL_PAGOS_PREVIOS) . '</b>';

  }

  //SI NO TIENE FECHAS IMPAGAS VENCIDAS, ENTONCES ES UNA LIQUIDACION ANTES DE TIEMPO
  if ($ultima_cuota_facturada_fecha == '') {
    

    list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $credito_fecha_facturacion);
    $interes_valido = formato_numero($credito_preaco * $credito_int / 100);
    $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
    $interes_por_dia = formato_numero($interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

    $fecha_facturada_time = new DateTime($credito_fecha_facturacion); //cuantos días han pasado desde la ultima facturada hasta hoy
    $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
    $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
    $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);

    //CALCULAMOS TODOS LOS PAGOS QUE HAYA TENIDO ESTE CRÉDITO SIN CUOTAS FACTURADAS, SUELE PASAR QUE TIENE PAGOS
    $ingreso_total_credito = 0;
    $result2 = $oIngreso->ingreso_total_por_credito_cuota($credito_id, $credito_moneda_id);
    if ($result2['estado'] == 1) {
      foreach ($result2['data'] as $key => $value) {
        $ingreso_total_credito = floatval($value['importe_total']);
      }
    }
    $result2 = null;

    $CAPITAL_REAL_RESTANTE = $credito_preaco - $ingreso_total_credito;

    $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE;
    $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;

    if ($credito_matriz_moneda_id == 2 && $credito_moneda_id == 1) {
      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE / $TIPO_DE_CAMBIO;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
    }

    $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $CAPITAL_RESTANTE_CAMBIO;

    $SUMA_GLOBAL_A_PAGAR += $CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO;
  }
}*/

$AMORTIZACION_TOTAL_PREVIO = 0; //amortización previo
$mod_id = 1; //el modulo en INGRESO para amortizacion es 1 menor, 2 asve, 3 garve, 4 hipo
$modide = $credito_id; //el modide en ingreso para amortización es el ID  del crédito


$result2 = $oIngreso->mostrar_por_modulo(1, $credito_id, $credito_moneda_id, '1'); // parametro 1 ya que es el estado del ingreso 1 activo
if ($result2['estado'] == 1) {
  foreach ($result2['data'] as $key => $value) {
    $AMORTIZACION_TOTAL_PREVIO += floatval($value['tb_ingreso_imp']);
  }
}
$result2 = NULL;

$AMORTIZACION_TOTAL_PREVIO = formato_moneda($AMORTIZACION_TOTAL_PREVIO); //amortización previo a este crédito
$TOTAL_A_LIQUIDAR = formato_moneda($SUMA_GLOBAL_A_PAGAR);
$SUMA_GLOBAL_A_PAGAR = formato_moneda($SUMA_GLOBAL_A_PAGAR - $AMORTIZACION_TOTAL_PREVIO);

/**** CRONOGRAMA DE CREDITO ****/
$cronograma = '';
$result2 = $oCalculoliquidacion->mostrar_cronograma_credito($credito_id, 1, 'tb_creditomenor');
  if($result2['estado'] == 1){
    foreach ($result2['data'] as $key => $value) {

      $estado = '';
      if($value['tb_cuota_est'] == 2)
        $estado = '<span class="pull-right badge bg-green">Pagada</span>';
      if($value['tb_cuota_est'] == 3)
        $estado = '<span class="pull-right badge bg-aqua">Pago Parcial</span>';
      if($value['tb_cuota_est'] == 1 && strtotime($fecha_hoy) >= strtotime($value['tb_cuota_fec'])){
        $estado = '<span class="pull-right badge bg-red">Vencida</span>';
      }

      $cuo_cuo = $value['tb_cuota_cuo'];
      $cuo_cap = $value['tb_cuota_cap'];
      $cuo_int = $value['tb_cuota_int'];
      $cuo_num = $value['tb_cuota_num'];
      $cre_numcuomax = $value['tb_credito_numcuomax'];
      $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);


      if ($value['tb_cuotatipo_id'] == 2) {
        $cuota = $value['tb_cuota_num'] . '/' . $value['tb_credito_numcuo'];
      } else {
        $cuota = $value['tb_cuota_num'] . '/' . $value['tb_credito_numcuomax'];
      }

      $pagos_cuota = 0;
      $mod_id = 1;
      $result1 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
      if ($result1['estado'] == 1) {
        foreach ($result1['data'] as $key => $value1) {
          $pagos_cuota += floatval($value1['tb_ingreso_imp']);
        }
      }
      $result1 = NULL;
      
      $pagos = $pagos_cuota;
      $pagos_parciales = mostrar_moneda($pagos);

      $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
      $diasdelmes = date('t', strtotime($diasdelmes1));

      $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
      $fechahoy = date('d-m-Y'); // la fecha actual del dia
      $fechaPago = $cuo_fec; // cuando me toca pagar la cuota

      if ($cuo_num == 1) {
        $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
      }
      if ($cuo_num > 1) {
        $fechaFacturacion = strtotime($fechaFacturacion);
        $fechahoy = strtotime($fechahoy);
        $fechaPago = strtotime($fechaPago);
  
        if ($fechahoy < $fechaFacturacion) {
          $dias = 0;
          $variableopcional = 1;
        }
        if ($fechahoy > $fechaFacturacion) {
  
          $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
          $variableopcional = 0;
        }
      }
      //$cuotip_id = $value['tb_cuotatipo_id'];
  
      $diastranscurridos = $dias;
      $monto_pagar = 0;
      $monto_pagar = formato_moneda($cuo_cuo) - formato_moneda($pagos);
  

      if ($cre_cuotip == 1):
        if ($cuo_num == $cre_numcuomax) {
          $monto_pagar = $cuo_int - $pagos;
              if ($monto_pagar <= 0)
                $monto_pagar = 0;
              else
                $monto_pagar = mostrar_moneda($monto_pagar);
        } 
        else {
          $monto_pagar = $cuo_int - $pagos;
          if ($monto_pagar <= 0)
            $monto_pagar = 0;
          else
            $monto_pagar = mostrar_moneda($monto_pagar);
  
          if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
          } else {
            if ($cuo_num > 1) {
              $monto_pagar = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
            }
          }
        }
      endif;
  
      //$saldo = $monto_pagar - $pagos_cuota;

      $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($cuo_fec);

      $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));


      $morasugerida = 0.00;
      $resultmora = $oMorasugerida->obtener_morasugerida(moneda_mysql($monto_pagar), $diferencia_en_dias, 1);
      if($resultmora['estado']==1){
        $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
      }else{
        $porcentajemora = 0.00;
      }
      $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_pagar;
      $morasugerida = round($morasugerida,2);
      $resultmora = null;
      
      $cronograma .= '
        <tr>
          <td align="center" id="tabla_fila">'.$cuota.'</td>
          <td align="center" id="tabla_fila">'.$value['tb_cuota_fec'].'</td>
          <td align="center" id="tabla_fila" style="padding-right:20px;">'.$estado.'</td>
          <td align="center" id="tabla_fila">'.mostrar_moneda($value['tb_cuota_cap']).'</td>
          <td align="center" id="tabla_fila">'.mostrar_moneda($value['tb_cuota_int']).'</td>
          <td align="center" id="tabla_fila">'.mostrar_moneda($pagos_cuota).'</td>
          <td align="center" id="tabla_fila">'.mostrar_moneda($monto_pagar).'</td>
          <td align="center" id="tabla_fila">'.mostrar_moneda($morasugerida).'</td>
          <td align="center" id="tabla_fila">'.mostrar_moneda($monto_pagar + $morasugerida).'</td>
        </tr>
      ';
    }
  }
$result2 = NULL;

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_liquidacion_vencimientomenor" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document" style="max-width: 70%; width: 70%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">INFORMACIÓN DE CUOTA GAR VEH A PAGAR <?php echo $_POST['cuodet_id'] . '  - ' . $bandera ?></h4>

      </div>

        <div class="modal-body">

          <div class="box box-default">
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <label style="font-family: cambria;color: #006633;">CUOTAS VENCIDAS</label>
                  <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;font-size:15px">
                    <thead>
                    <tr id="tabla_cabecera">
                      <th id="tabla_cabecera_fila">N° CUOTA</th>
                      <th id="tabla_cabecera_fila">VENCIMIENTO</th>
                      <th id="tabla_cabecera_fila">ESTADO</th>
                      <th id="tabla_cabecera_fila">CAPITAL</th>
                      <th id="tabla_cabecera_fila">INTERES</th>
                      <th id="tabla_cabecera_fila">PAGOS</th>
                      <th id="tabla_cabecera_fila">SALDO</th>
                      <th id="tabla_cabecera_fila">MORA</th>
                      <th id="tabla_cabecera_fila">CUOTA</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo $cronograma
                    ?>
                    </tbody>
                  </table>

                  <input type="hidden" id="mon_nom" value="<?php echo $mon_nom; ?>">
                  <input type="hidden" id="hoy" value="<?php echo $fecha_hoy; ?>">
                  <input type="hidden" id="credito" value="<?php echo $credito_id; ?>">
                  <input type="hidden" id="ultima_cuota" value="<?php echo $cuota_facturada_pagada; ?>">
                  <input type="hidden" id="num_pagadas" value="<?php echo $numero_cuotas_pagadas; ?>">
                  <input type="hidden" id="int_previos" value="<?php echo $TOTAL_INTERESES_PREVIOS; ?>">
                  <input type="hidden" id="pagos_previos" value="<?php echo $TOTAL_PAGOS_PREVIOS; ?>">
                  <input type="hidden" id="int_previos" value="<?php echo $TOTAL_INTERESES_PREVIOS; ?>">
                  <input type="hidden" id="pagos_post" value="<?php echo $TOTAl_PAGOS_POSTERIORES; ?>">
                  <input type="hidden" id="impagas" value="<?php echo $cuotas_vencidas_impagas ; ?>">
                  <input type="hidden" id="prorrateo" value="<?php echo $PRORRATEO_TOTAL ; ?>">
                  <input type="hidden" id="cap" value="<?php echo $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA ; ?>">
                  <input type="hidden" id="int_impagas" value="<?php echo $INTERESES_CUOTAS_IMPAGAS ; ?>">
                  <input type="hidden" id="total" value="<?php echo $SUMA_GLOBAL_A_PAGAR ; ?>">
                  <input type="hidden" id="moras" value="<?php echo $TOTAl_MORAS_SUGERIDAS ; ?>">
                  <input type="hidden" id="faltante_menores" value="<?php echo $TOTAL_INTERESES_PREVIOS ; ?>">
                  <input type="hidden" id="faltante_posteriores" value="<?php echo $FALTANTE_PAGO_POSTERIORES ; ?>">
                  <input type="hidden" id="saldo_str" value="<?php echo $saldo_srt ; ?>">
                  <input type="hidden" id="dias_str" value="<?php echo $dias_srt ; ?>">
                  <input type="hidden" id="morasugerida_str" value="<?php echo $morasugerida_str ; ?>">
                  <input type="hidden" id="bandera" value="<?php echo $bandera ; ?>">
                  <input type="hidden" id="int_prev" value="<?php echo $INTERESES_CUOTAS_PREV ; ?>">
                  <input type="hidden" id="int_post" value="<?php echo $INTERESES_CUOTAS_IMPAGAS ; ?>">
                  
                  
                  <input type="hidden" id="ultima_pagada" value="<?php echo $ultima_cuota_pagada_fecha; ?>">
                  <input type="hidden" id="ultima" value="<?php echo $ultima_cuota_facturada_fecha ; ?>">
                  <input type="hidden" id="Capital_cuota_facturada" value="<?php echo $Capital_cuota_facturada ; ?>">
                  <input type="hidden" id="SALDO_CUOTAS_IMPAGAS" value="<?php echo $SALDO_CUOTAS_IMPAGAS ; ?>">
                  <input type="hidden" id="TOTAl_MORAS_SUGERIDAS" value="<?php echo $TOTAl_MORAS_SUGERIDAS ; ?>">
                  <input type="hidden" id="PRORRATEO_TOTAL" value="<?php echo $PRORRATEO_TOTAL ; ?>">
                  <input type="hidden" id="SUMA_GLOBAL_A_PAGAR" value="<?php echo $SUMA_GLOBAL_A_PAGAR ; ?>">
                  <input type="hidden" id="moras_impagas" value="<?php echo $moras_impagas ; ?>">
                </div>
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="inputEmail3" class="control-label" style="text-align:center">CAPITAL RESTANTE :</label>
                        <div class="">
                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_capital" type="text" id="hdd_capital" value="<?php echo mostrar_moneda($CAPITAL_RESTANTE_CAMBIO); ?>" readonly> 
                        </div>
                      </div>
                    </div>
                    <!--<div class="col-md-4">
                      <div class="form-group">
                        <label for="inputEmail3" class="control-label" style="text-align:center">INTERESES CUOTAS VENCIDAS:</label>
                        <div class="">
                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_intereses" type="text" id="hdd_intereses" value="<?php echo mostrar_moneda($TOTAL_INTERESES_VENCIDAS); ?>" readonly> 
                        </div>
                      </div>
                    </div>-->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="inputEmail3" class="control-label" style="text-align:center">DEUDA RESTANTE :</label>
                        <div class="">
                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_capital" type="text" id="hdd_capital" value="<?php echo mostrar_moneda($SALDO_CUOTAS_IMPAGAS_CAMBIO); ?>" readonly> 
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="inputEmail3" class="control-label" style="text-align:center">MORAS SUGERIDAS:</label>
                        <div class="">
                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_mora_sugerida" type="text" id="hdd_mora_sugerida" value="<?php echo mostrar_moneda($TOTAl_MORAS_SUGERIDAS_CAMBIO); ?>" readonly> 
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="inputEmail3" class="control-label" style="text-align:center">PRORRATEO:</label>
                        <div class="">
                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_prorrateo" type="text" id="hdd_prorrateo" value="<?php echo mostrar_moneda($PRORRATEO_TOTAL_CAMBIO); ?>" readonly> 
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="inputEmail3" class="control-label" style="text-align:center">SUMA FINAL:</label>
                        <div class="">
                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_suma_final" type="text" id="hdd_suma_final" value="<?php echo mostrar_moneda($SUMA_GLOBAL_A_PAGAR); ?>" readonly> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
    </div>
  </div>
</div>