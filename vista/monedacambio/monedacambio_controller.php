<?php
require_once('../../core/usuario_sesion.php');
require_once('../monedacambio/Monedacambio.class.php');
$oMonedacambio = new Monedacambio();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];
$monedacambio_id = intval($_POST['hdd_monedacambio_id']);
$monedacambio_fec = fecha_mysql($_POST['txt_monedacambio_fec']);
$monedacambio_val = floatval($_POST['txt_monedacambio_val']);
$monedacambio_com = floatval($_POST['txt_monedacambio_com']);
$usuario_id = intval($_SESSION['usuario_id']);

$oMonedacambio->tb_monedacambio_id=$monedacambio_id;
$oMonedacambio->tb_monedacambio_fec=$monedacambio_fec;
$oMonedacambio->tb_monedacambio_val=$monedacambio_val;
$oMonedacambio->tb_monedacambio_com=$monedacambio_com;

if ($action == 'insertar') {
    $oMonedacambio->tb_usuario_id=$usuario_id;
    $oMonedacambio->tb_monedacambio_his='El Usuario <b>'.$_SESSION['usuario_nom'].'</b> ha registrado el tipo de cambio a dia <b>'.date("d-m-Y H:i:s").'</b><br>';
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Tipo de Cambio.';

    $result = $oMonedacambio->verificar_monedacambio($monedacambio_fec);
    $existe = $result['estado'];
    $result = NULL;

    if ($existe == 0) {
        if ($oMonedacambio->insertar()) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Tipo de Cambio registrado correctamente.';
        }
    } else
        $data['mensaje'] = 'El Tipo de Cambio para la fecha ya está registrado';

    echo json_encode($data);
} 
elseif ($action == 'modificar') {
    $oMonedacambio->tb_usuarioreg_id=$usuario_id;
    $oMonedacambio->tb_monedacambio_his='El Usuario <b>'.$_SESSION['usuario_nom'].'</b> ha Modificado el tipo de cambio a dia <b>'.date("d-m-Y H:i:s").'</b><br>';
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar el Tipo de Cambio.';

    if ($oMonedacambio->modificar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Tipo de Cambio modificado correctamente. ';
    }

    echo json_encode($data);
} 

elseif ($action == 'eliminar') {
    $monedacambio_id = intval($_POST['hdd_monedacambio_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el Tipo de Cambio.';

    if ($oMonedacambio->eliminar($monedacambio_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Tipo de Cambio eliminado correctamente.';
    }

    echo json_encode($data);
} 

elseif ($action == 'tipocambio') {
    $monedacambio_fec = fecha_mysql($_POST['monedacambio_fec']);
    $moneda_id = intval($_POST['moneda_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha registrado el tipo de cambio del día: ' . $_POST['monedacambio_fec'] . ', y moneda: ' . $moneda_id;

    if ($moneda_id == 1) {
        $data['estado'] = 1;
        $data['valor'] = '1.00';
        $data['mensaje'] = 'Exito';
    }
    if ($moneda_id == 2) {
        $result = $oMonedacambio->verificar_monedacambio($monedacambio_fec);
        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value) {
                $data['valor'] = floatval($value['tb_monedacambio_val']);
            }
            $data['estado'] = 1;
            $data['mensaje'] = 'Exito';
        }
        $result = NULL;
    }
    echo json_encode($data);
}

elseif ($action == 'tipocambio_egreso') {
    $monedacambio_fec = fecha_mysql($_POST['monedacambio_fec']);
    $moneda_id = intval($_POST['moneda_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha registrado el tipo de cambio del día: ' . $_POST['monedacambio_fec'] . ', y moneda: ' . $moneda_id;

    if ($moneda_id == 1) {
        $data['estado'] = 1;
        $data['valor'] = '1.00';
        $data['mensaje'] = 'Exito';
    }
    if ($moneda_id == 2) {
        $result = $oMonedacambio->verificar_monedacambio($monedacambio_fec);
        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value) {
                $data['valor'] = floatval($value['tb_monedacambio_com']);
            }
            $data['estado'] = 1;
            $data['mensaje'] = 'Exito';
        }
        $result = NULL;
    }
    echo json_encode($data);
}

/* NUEVO CODIGO AGREGADO */

elseif ($action == "obtener_dato") {
    //soles

    if ($_POST['mon_id'] == '1') {
        $resultado = mostrar_moneda(1.000);
    }

    //dolares
    if ($_POST['mon_id'] == '2') {
        $fecha_buscar = fecha_mysql($_POST['fecha']);
        $fecha_buscar_val = fecha_mysql($_POST['fecha_validacion']);

        if(empty(trim($fecha_buscar)))
        $fecha_buscar =  $fecha_buscar_val;

        $rs = $oMonedacambio->consultar($fecha_buscar);
        if ($rs['estado'] == 1) {
            $resultado = number_format($rs['data']['tb_monedacambio_val'], 3);

            if ($resultado == '0.000')
                $resultado = "";
        }
    }
    $data['moncam_val'] = $resultado;

    echo json_encode($data);
} 
elseif($_POST['action'] == 'obtener_cambio'){

  $fecha = $_POST['fecha'];
  $tipo = $_POST['tipo'];
  
  $dts = $oMonedacambio->consultar(fecha_mysql($fecha));
    
    if($dts['estado']==1){
    $mon_venta = $dts['data']['tb_monedacambio_val']; //tipo de cmabio para venta
    $mon_comp =  $dts['data']['tb_monedacambio_com']; //tipo de cambio para compra
    }


  if($dts['estado']==1){
    if($tipo == 'venta'){
      $data['moncam_val'] = $mon_venta;
    }
    elseif ($tipo == 'compra') {
      $data['moncam_val'] = $mon_comp;
    }
    else{
      $data['moncam_val'] = 0;
    }
    $data['estado'] = 1;
  }
  else{
      $data['moncam_val'] = 0;
      $data['estado'] = 0;
  }
  
  echo json_encode($data);
}
else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>