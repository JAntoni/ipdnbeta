<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../monedacambio/Monedacambio.class.php');
  $oMonedacambio = new Monedacambio();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'monedacambio';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $monedacambio_id = $_POST['monedacambio_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Tipo de Cambio Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Tipo de Cambio';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Tipo de Cambio';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Tipo de Cambio';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en monedacambio
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'monedacambio'; $modulo_id = $monedacambio_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del monedacambio por su ID
    $monedacambio_fec = date('d-m-Y');

    if(intval($monedacambio_id) > 0){
      $result = $oMonedacambio->mostrarUno($monedacambio_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el monedacambio seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $monedacambio_fec = mostrar_fecha($result['data']['tb_monedacambio_fec']);
          $monedacambio_val = $result['data']['tb_monedacambio_val'];
          $monedacambio_com = $result['data']['tb_monedacambio_com'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_monedacambio" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_monedacambio" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_monedacambio_id" value="<?php echo $monedacambio_id;?>">
          <input type="hidden" id="monedacambio_vista" value="<?php echo $vista;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_monedacambio_fec" class="control-label">Fecha TC</label>
              <div class='input-group date' id='monedacambio_picker'>
                <input type='text' class="form-control" name="txt_monedacambio_fec" id="txt_monedacambio_fec" value="<?php echo $monedacambio_fec;?>"/>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="txt_monedacambio_val" class="control-label">TC Venta</label>
              <input type="text" name="txt_monedacambio_val" id="txt_monedacambio_val" class="form-control input-sm" value="<?php echo $monedacambio_val;?>">
            </div>
            <div class="form-group">
              <label for="txt_monedacambio_com" class="control-label">TC Compra</label>
              <input type="text" name="txt_monedacambio_com" id="txt_monedacambio_com" class="form-control input-sm" value="<?php echo $monedacambio_com;?>">
            </div>

            <?php if($action != 'eliminar'):?>
              <iframe id="frame_sunat" name="frame_sunat" src="http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias" width="100%" height="290" ></iframe>
            <?php endif;?>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Tipo de Cambio?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="monedacambio_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_monedacambio">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_monedacambio">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_monedacambio">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/monedacambio/monedacambio_form.js';?>"></script>
