<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
if (defined('VISTA_URL')) {
    require_once(VISTA_URL . 'monedacambio/Monedacambio.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../monedacambio/Monedacambio.class.php');
    require_once('../funciones/fechas.php');
}

$oMonedacambio = new Monedacambio();

$result = $oMonedacambio->listar_monedacambios();

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $tr .= '<tr id="tabla_cabecera_fila">';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_monedacambio_id'] . '</td>
          <td id="tabla_fila">' . mostrar_fecha($value['tb_monedacambio_fec']) . '</td>
          <td id="tabla_fila" id="tabla_fila">' . $value['tb_monedacambio_val'] . '</td>
          <td id="tabla_fila">' . $value['tb_monedacambio_com'] . '</td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="monedacambio_form(\'M\',' . $value['tb_monedacambio_id'] . ')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="monedacambio_form(\'E\',' . $value['tb_monedacambio_id'] . ')"><i class="fa fa-trash"></i> Eliminar</a>
            <a class="btn btn-success btn-xs" title="Historial" onclick="monedacambio_historial_form(' . $value['tb_monedacambio_id'] . ')"><i class="fa fa-eye"></i> Historial</a>
          </td>
        ';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
?>
<table id="tbl_monedacambios" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">Fecha</th>
            <th id="tabla_cabecera_fila">TC Venta</th>
            <th id="tabla_cabecera_fila">TC Compra</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
<?php echo $tr; ?>
    </tbody>
</table>
