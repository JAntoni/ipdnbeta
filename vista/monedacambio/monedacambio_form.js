
$(document).ready(function () {

    $('#monedacambio_picker').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
        endDate: new Date()
    });

    $('#txt_monedacambio_val, #txt_monedacambio_com').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.000',
        vMax: '99.000'
    });
    console.log('cambio 63');
    $('#form_monedacambio').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "monedacambio/monedacambio_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_monedacambio").serialize(),
                beforeSend: function () {
                    $('#monedacambio_mensaje').show(400);
                    $('#btn_guardar_monedacambio').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#monedacambio_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#monedacambio_mensaje').html(data.mensaje);
                        var vista = $('#monedacambio_vista').val();
                        
                        setTimeout(function () {
                            if (vista == 'credito')
                            monedacambio_tipo_cambio(); //función implementada en cada módulo de los créditos
                        
                            if (vista == 'monedacambio')
                                monedacambio_tabla();
                            $('#modal_registro_monedacambio').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#monedacambio_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#monedacambio_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_monedacambio').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#monedacambio_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#monedacambio_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_monedacambio_fec: {
                required: true
            },
            txt_monedacambio_val: {
                required: true
            },
            txt_monedacambio_com: {
                required: true
            }
        },
        messages: {
            txt_monedacambio_fec: {
                required: "Ingrese la fecha del Tipo de Cambio"
            },
            txt_monedacambio_val: {
                required: "Ingrese Tipo de Cambio Venta"
            },
            txt_monedacambio_com: {
                required: "Ingrese Tipo de Cambio Compra"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});
