<?php
require_once('../../core/usuario_sesion.php');

require_once ('./Monedacambio.class.php');
$oMonedacambio= new Monedacambio();

$monedacambio_id=$_POST['monedacambio_id'];

  $dts = $oMonedacambio->mostrarUno($monedacambio_id);
    if($dts['estado']==1){
          $not = $dts['data']['tb_monedacambio_his'];
    }
    $dts=NULL;

?>


<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_monedacambio_historial_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">INFORMACION DE MODIFICACIONES EN EL SISTEMA</h4>
            </div>
            <form id="for_cob" method="post">
                <input name="action_cobranza" id="action_cobranza" type="hidden" value="<?php echo $_POST['cuo_tip']?>">
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <label style="font-weight: bold;font-family: cambria">HISTORIAL</label>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12"  style="font-family: cambria">
                                        <?php echo $not;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                  <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <!--<button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
            
        </div>
    </div>
</div>