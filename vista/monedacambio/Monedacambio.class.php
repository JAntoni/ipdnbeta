<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Monedacambio extends Conexion {

    public $tb_monedacambio_id;
    public $tb_monedacambio_reg;
    public $tb_monedacambio_mod;
    public $tb_monedacambio_fec;
    public $tb_monedacambio_val;
    public $tb_monedacambio_com;
    public $tb_usuario_id;
    public $tb_usuarioreg_id;
    public $tb_monedacambio_his;

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_monedacambio(
                                            tb_monedacambio_fec, 
                                            tb_monedacambio_val, 
                                            tb_monedacambio_com,
                                            tb_usuario_id,
                                            tb_usuarioreg_id,
                                            tb_monedacambio_his)
                                    VALUES (
                                            :monedacambio_fec, 
                                            :monedacambio_val, 
                                            :monedacambio_com,
                                            :tb_usuario_id,
                                            :tb_usuarioreg_id,
                                            :tb_monedacambio_his)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":monedacambio_fec", $this->tb_monedacambio_fec, PDO::PARAM_STR);
            $sentencia->bindParam(":monedacambio_val", $this->tb_monedacambio_val, PDO::PARAM_STR);
            $sentencia->bindParam(":monedacambio_com", $this->tb_monedacambio_com, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuarioreg_id", $this->tb_usuarioreg_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_monedacambio_his", $this->tb_monedacambio_his, PDO::PARAM_STR);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_monedacambio SET 
                                            tb_monedacambio_fec =:monedacambio_fec, 
                                            tb_monedacambio_val =:monedacambio_val, 
                                            tb_monedacambio_com =:monedacambio_com,
                                            tb_usuarioreg_id=:tb_usuarioreg_id,
                                            tb_monedacambio_his= CONCAT(tb_monedacambio_his,:tb_monedacambio_his)
                                    WHERE 
                                            tb_monedacambio_id =:monedacambio_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":monedacambio_fec", $this->tb_monedacambio_fec, PDO::PARAM_STR);
            $sentencia->bindParam(":monedacambio_val", $this->tb_monedacambio_val, PDO::PARAM_STR);
            $sentencia->bindParam(":monedacambio_com", $this->tb_monedacambio_com, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_usuarioreg_id", $this->tb_usuarioreg_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_monedacambio_his", $this->tb_monedacambio_his, PDO::PARAM_STR);
            $sentencia->bindParam(":monedacambio_id", $this->tb_monedacambio_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($monedacambio_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_monedacambio WHERE tb_monedacambio_id =:monedacambio_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":monedacambio_id", $monedacambio_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($monedacambio_id) {
        try {
            $sql = "SELECT * FROM tb_monedacambio WHERE tb_monedacambio_id =:monedacambio_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":monedacambio_id", $monedacambio_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_monedacambios() {
        try {
            $sql = "SELECT * FROM tb_monedacambio ORDER BY tb_monedacambio_id DESC limit 50";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function verificar_monedacambio($monedacambio_fec) {
        try {
            $sql = "SELECT * FROM tb_monedacambio WHERE tb_monedacambio_fec =:monedacambio_fec";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":monedacambio_fec", $monedacambio_fec, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monedacambio_ultimo_registrado(){
        try {
            $sql = "SELECT * FROM tb_monedacambio ORDER BY tb_monedacambio_id DESC limit 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function consultar($fec) {
        try {
            $sql = "SELECT *
		FROM tb_monedacambio 
		WHERE tb_monedacambio_fec=:fec";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fec", $fec, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>
