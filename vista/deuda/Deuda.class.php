<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Deuda extends Conexion{
    public $tb_deuda_id;
    public $tb_deuda_reg;
    public $tb_deuda_inisol;
    public $tb_deuda_inidol;
    public $tb_deuda_finsol;
    public $tb_deuda_findol;
    public $tb_deuda_fecfin;
    public $tb_deuda_vensol;
    public $tb_deuda_vendol;
    public $tb_deuda_per;
    
    function guardar_inicio(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_deuda(
                                    tb_deuda_inisol, 
                                    tb_deuda_inidol,
                                    tb_deuda_per) 
                            VALUES (
                                    :tb_deuda_inisol,
                                    :tb_deuda_inidol,
                                    :tb_deuda_per)"; 
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_inisol", $this->tb_deuda_inisol, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_inidol", $this->tb_deuda_inidol, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_per", $this->tb_deuda_per, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_afps_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_afps_id'] = $tb_afps_id;
        return $data; //si es correcto el deuda retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function guardar_fin(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_deuda SET  
                                    tb_deuda_finsol = :tb_deuda_finsol,
                                    tb_deuda_findol = :tb_deuda_findol,
                                    tb_deuda_fecfin = :tb_deuda_fecfin
                                WHERE 
                                    tb_deuda_id =:tb_deuda_id;";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_finsol", $this->tb_afps_per, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_findol", $this->tb_seguro_id, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_fecfin", $this->tb_afps_apor, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_id", $this->tb_afps_apor2, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function ultimo_registro_periodo($deuda_per){ 
      try {
         $sql = "SELECT * FROM tb_deuda WHERE tb_deuda_per =:tb_deuda_per ORDER BY tb_deuda_id DESC LIMIT 1;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_per", $deuda_per, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function ultimo_mes($fecha){ 
      try {
         $sql = "SELECT * FROM tb_deuda WHERE tb_deuda_per = 1 AND DATE(tb_deuda_reg) = :tb_deuda_reg ORDER BY tb_deuda_id DESC LIMIT 1;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_reg", $fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function guardar_por_vencer(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_deuda SET  
                                    tb_deuda_vensol = :tb_deuda_vensol, 
                                    tb_deuda_vendol = :tb_deuda_vendol
                                WHERE 
                                    tb_deuda_id =:tb_deuda_id;";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_vensol", $this->tb_deuda_vensol, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_vendol", $this->tb_deuda_vendol, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deuda_id", $this->tb_afps_apor2, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($deuda_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_deuda WHERE tb_deuda_id = :tb_deuda_id"; 

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_id", $deuda_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarTodos($fecha1, $fecha2){
      try {
         $sql = "SELECT * FROM tb_deuda WHERE DATE(tb_deuda_reg) BETWEEN :fecha1 AND :fecha2;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarUno($id){
      try {
         $sql="SELECT 
                        * 
                FROM 
                        tb_deuda
                WHERE 
                        tb_deuda_id=:tb_deuda_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function modificar_campo($deuda_id, $deuda_columna, $deuda_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($deuda_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_deuda SET ".$deuda_columna." =:deuda_valor WHERE tb_deuda_id =:deuda_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":deuda_id", $deuda_id, PDO::PARAM_INT);
          if ($param_tip == 'INT') {
                    $sentencia->bindParam(":deuda_valor", $deuda_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":deuda_valor", $deuda_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el deuda retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
}