<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Deuda.class.php");
$oDeuda = new Deuda();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

$fecha1 = $_POST['fec1'];
$fecha2 = $_POST['fec2'];

$por_vencer_sol = 0;
$por_vencer_dol = 0;

$dts = $oDeuda->mostrarTodos(fecha_mysql($fecha1), fecha_mysql($fecha2));
?>

    <!--<legend>COMO INICIA LA DEUDA</legend>-->    
<table cellspacing="1" class="table table-striped table-bordered dataTable display" style="width: 100%">
  <thead>
    <tr>
      <th>ID</th>
      <th>PERIODO</th>
      <th>APERTURA SOLES</th>
      <th>CIERRE SOLES</th>
      <th>DIFERENCIA SOLES</th>
      <th>APERTURA DOLARES</th>
      <th>CIERRE DOLARES</th>
      <th>DIFERENCIA DOLARES</th>
    </tr>
  </thead>
  <?php
  if ($dts['estado'] == 1) {
    $semana_num = 0;
    ?>  
    <tbody> <?php
      foreach ($dts['data']as $key => $dt) {
        $por_vencer_sol += ($dt['tb_deuda_vensol']);
        $por_vencer_dol += ($dt['tb_deuda_vendol']);
        $periodo = '<span style="color: green;"><b>MENSUAL</b></span>';

        if ($dt['tb_deuda_per'] == 2) {
            $semana_num++;
            $periodo = '<span style="color: blue;"><b>Semana ' . $semana_num . ' (' . mostrar_fecha($dt['tb_deuda_reg']) . '/' . mostrar_fecha($dt['tb_deuda_fecfin']) . ')</b></span>';
        }
        $diferencia_sol = ($dt['tb_deuda_finsol']) - ($dt['tb_deuda_inisol']);
        $diferencia_dol = ($dt['tb_deuda_findol']) - ($dt['tb_deuda_inidol']);
        ?>
        <tr style="height:25px;">
          <td><?php echo $dt['tb_deuda_id'] ?></td>
          <td><?php echo $periodo; ?></td>
          <td><?php echo 'S/. ' . mostrar_moneda($dt['tb_deuda_inisol']) ?></td>
          <td><?php echo 'S/. ' . mostrar_moneda($dt['tb_deuda_finsol']) ?></td>
          <td><?php echo 'S/. ' . mostrar_moneda($diferencia_sol) ?></td>
          <td><?php echo 'US$ ' . mostrar_moneda($dt['tb_deuda_inidol']) ?></td>
          <td><?php echo 'US$ ' . mostrar_moneda($dt['tb_deuda_findol']) ?></td>
          <td><?php echo 'US$ ' . mostrar_moneda($diferencia_dol) ?></td>
        </tr><?php
      }?>
    </tbody>
    <?php 
  }?>
</table>

<br>

<table cellspacing="1" class="table table-striped table-bordered dataTable display">
  <thead>
    <tr>
      <th>POR VENCER SOLES</th>
      <th>POR VENCER DOLARES</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="font-weight: bold;font-size: 20px"><?php echo 'S/. ' . mostrar_moneda($por_vencer_sol) ?></td>
      <td style="font-weight: bold;font-size: 20px"><?php echo 'US$ ' . mostrar_moneda($por_vencer_dol) ?></td>
    </tr>
  </tbody>
</table>