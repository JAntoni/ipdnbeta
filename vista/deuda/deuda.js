/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    deuda_tabla();
});

function deuda_tabla()
{
    $.ajax({
        type: "POST",
        url: VISTA_URL+"deuda/deuda_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_resumen_fec1').val(),
            fec2: $('#txt_resumen_fec2').val()
        }),
        beforeSend: function () {
//            $('#div_deuda_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_deuda_tabla').html(html);
        },
        complete: function () {
            $('#div_deuda_tabla').removeClass("ui-state-disabled");
        }
    });
}