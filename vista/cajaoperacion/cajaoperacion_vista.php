<?php
$empresa_id = $_SESSION['empresa_id'];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">

                <div class="row">
                    <div class="col-md-2">
                        <?php if ($_SESSION['usuariogrupo_id'] == 2): ?>
                            <button class="btn btn-primary btn-sm" type="button" onclick="cajaoperacion_form('insertar', 0)"><i class="fa fa-plus"></i> Apertura de Caja</button>
                        <?php endif; ?>
                        <button class="btn btn-danger btn-sm" type="button" onclick="cerrarcaja_form()"><i class="fa fa-plus"></i> Cierre de Caja</button>
                    </div> 
                    <div class="col-md-2">
                        <?php if ($_SESSION['usuariogrupo_id'] == 2||$_SESSION['usuariogrupo_id'] == 6) { ?>
                            <select class="form-control" id="cmb_fil_empresa_id" name="cmb_fil_empresa_id">
                                <?php require_once 'vista/empresa/empresa_select.php'; ?>
                            </select>
                        <?php
                        } else {
                            ?>
                            <select class="form-control" id="cmb_fil_empresa_id" name="cmb_fil_empresa_id">
                                <option value="<?php echo $_SESSION['empresa_id'] ?>" style="font-weight: bold;" ><?php echo $_SESSION['empresa_nombre']; ?></option>
                            </select>
                            <?php
                        }
                        ?>
                    </div>   
                </div>    

            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="cajaoperacion_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_cajaoperacion_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('cajaoperacion_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_cajaoperacion_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_cajaoperacion_notificacion_form"></div>
            <div id="div_modal_upload_form"></div>
            <div id="div_modal_imagen_form"></div>
            <div id="div_cajaoperacion_cheques_form"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
