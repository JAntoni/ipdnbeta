<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Cajaoperacion extends Conexion{
    public $cajaoperacion_id;
    public $cajaoperacion_xac;
    public $cajaoperacion_apertura_fec;
    public $cajaoperacion_cierre_fec;
    public $cajaoperacion_apertura_est;
    public $cajaoperacion_cierre_est;
    public $cajaoperacion_cierre_monto_sol;
    public $cajaoperacion_cierre_monto_dol;
    public $usuario_apertura_id;
    public $usuario_cierra_id;
    public $cajaoperacion_obs;
    public $cajaoperacion_img;
    public $empresa_id;
    
    function insertar(){
        $this->dblink->beginTransaction();
            try {
              $sql = "INSERT tb_cajaoperacion (
				tb_cajaoperacion_apertura_fec, 
				tb_cajaoperacion_apertura_est, 
                                tb_cajaoperacion_cierre_est, 
				tb_usuario_apertura_id, 
                                tb_cajaoperacion_obs,
                                tb_empresa_id
			)
			VALUES (
                                NOW(),
                                :tb_cajaoperacion_apertura_est, 
                                :tb_cajaoperacion_cierre_est, 
				:tb_usuario_apertura_id, 
                                :tb_cajaoperacion_obs,
                                :tb_empresa_id);"; 

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_cajaoperacion_apertura_est",$this->cajaoperacion_apertura_est, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cajaoperacion_cierre_est",$this->cajaoperacion_cierre_est, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_usuario_apertura_id",$this->usuario_apertura_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_empresa_id",$this->empresa_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_cajaoperacion_obs",$this->cajaoperacion_obs, PDO::PARAM_STR);

              $result = $sentencia->execute();
              $cajaoperacion_id = $this->dblink->lastInsertId();
              $this->dblink->commit();

              $data['estado'] = $result;
              $data['cajaoperacion_id'] = $cajaoperacion_id;
              return $data; //si es correcto el ingreso retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
    }
    
    function mostrarTodos($empresa_id){
      try {
        $sql ="SELECT 
                    tb_cajaoperacion_id, tb_cajaoperacion_apertura_fec, 
                    tb_cajaoperacion_cierre_fec, tb_cajaoperacion_obs,
                    tb_cajaoperacion_cierre_monto_sol, tb_cajaoperacion_cierre_monto_dol,
                    u.tb_usuario_nom AS usuario_apertura, usu.tb_usuario_nom AS usuario_cierre
                FROM 
                    tb_cajaoperacion c 
                    INNER JOIN tb_usuario u ON u.tb_usuario_id = c.tb_usuario_apertura_id 
                    LEFT JOIN tb_usuario usu ON usu.tb_usuario_id = c.tb_usuario_cierra_id
                WHERE 
                    tb_cajaoperacion_xac = 1 AND c.tb_empresa_id=:tb_empresa_id ORDER BY tb_cajaoperacion_id desc";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_empresa_id",$empresa_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Registros de Apertura o Cierre de Caja registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function guardar_monto_cierre($cajaoperacion_id, $cajaoperacion_cierre_monto_sol, $cajaoperacion_cierre_monto_dol){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cajaoperacion SET  
                                tb_cajaoperacion_cierre_monto_sol = :tb_cajaoperacion_cierre_monto_sol,
                                tb_cajaoperacion_cierre_monto_dol = :tb_cajaoperacion_cierre_monto_dol
			WHERE 
                                tb_cajaoperacion_id = :tb_cajaoperacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cajaoperacion_cierre_monto_sol", $cajaoperacion_cierre_monto_sol, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajaoperacion_cierre_monto_dol", $cajaoperacion_cierre_monto_dol, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajaoperacion_id", $cajaoperacion_id, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function cerrar_caja($fecha_hoy, $usuario_cierre, $observacion,$empresa_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cajaoperacion SET  
			tb_cajaoperacion_cierre_fec =  NOW(),
			tb_cajaoperacion_cierre_est =  1,
			tb_usuario_cierra_id =:tb_usuario_cierra_id,
			tb_cajaoperacion_obs = CONCAT(tb_cajaoperacion_obs, '. ',:tb_cajaoperacion_obs)
			WHERE date(tb_cajaoperacion_apertura_fec) =:tb_cajaoperacion_apertura_fec AND tb_empresa_id=:tb_empresa_id"; 

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_cierra_id", $usuario_cierre, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cajaoperacion_obs", $observacion, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cajaoperacion_apertura_fec", $fecha_hoy, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($id){
      try {
        $sql = "SELECT * FROM tb_cajaoperacion WHERE tb_cajaoperacion_id=:tb_cajaoperacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cajaoperacion_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_fecha_hoy($fecha_hoy,$empresa_id){
      try {
        $sql = "SELECT * FROM tb_cajaoperacion WHERE tb_cajaoperacion_xac = 1 AND date(tb_cajaoperacion_apertura_fec) = :tb_cajaoperacion_apertura_fec AND tb_empresa_id=:tb_empresa_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cajaoperacion_apertura_fec", $fecha_hoy, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function verifica_cajaoperacion_tabla($id,$tabla){
      try {
        $sql = "SELECT * 
		FROM  $tabla 
		WHERE tb_cajaoperacion_id =:tb_cajaoperacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cajaoperacion_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($cajaoperacion_id, $cajaoperacion_columna, $cajaoperacion_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        $param_tip = strtoupper($param_tip);

        if(!empty($cajaoperacion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_cajaoperacion SET ".$cajaoperacion_columna." =:cajaoperacion_valor WHERE tb_cajaoperacion_id =:cajaoperacion_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cajaoperacion_id", $cajaoperacion_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":cajaoperacion_valor", $cajaoperacion_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":cajaoperacion_valor", $cajaoperacion_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($id){
      $this->dblink->beginTransaction();
      try {
        $sql="DELETE FROM tb_cajaoperacion WHERE tb_cajaoperacion_id=:tb_cajaoperacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cajaoperacion_id", $id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
}
