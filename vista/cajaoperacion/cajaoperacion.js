/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('.moneda2').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.99'
    });

    cajaoperacion_tabla();
});


function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'cajaoperacion', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
            //modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function upload_galeria(cajaoperacion_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'cajaoperacion', //nombre de la tabla a relacionar
            modulo_id: cajaoperacion_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

/* daniel odar 21-02-23*/
function insertar_comentario(cajaoperacion_id) {
    var cierre_sol = $('#txt_cajaoperacion_cierre_monto_sol_' + cajaoperacion_id).val();
    var cierre_dol = $('#txt_cajaoperacion_cierre_monto_dol_' + cajaoperacion_id).val();
    var importe_cheque_sol = $('#hdd_importe_sol').val();
    var importe_cheque_dol = $('#hdd_importe_dol').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_comentario_form.php",
        async: true,
        dataType: "html",
        data: ({
            cajaoperacion_id: cajaoperacion_id,
            cierre_sol: cierre_sol,
            cierre_dol: cierre_dol,
            importe_cheque_sol: importe_cheque_sol,
            importe_cheque_dol: importe_cheque_dol
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_cajaoperacion_notificacion_form').html(html);
            $('#div_modal_cajaoperacion_notificacion_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_cajaoperacion_notificacion_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_cajaoperacion_notificacion_form'); //funcion encontrada en public/js/generales.js
        }
    });
}

function cierre_monto(cajaoperacion_id) {
    var cierre_sol = $('#txt_cajaoperacion_cierre_monto_sol_' + cajaoperacion_id).val();
    var cierre_dol = $('#txt_cajaoperacion_cierre_monto_dol_' + cajaoperacion_id).val();
    var cheque_sol = $('#hdd_cheque_sol').val();
    var cheque_dol = $('#hdd_cheque_dol').val();
    var comentario = $('#txt_comentario').val();

    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action_cajaoperacion: 'cierre_monto',
            cajaoperacion_id: cajaoperacion_id,
            cajaoperacion_cierre_monto_sol: cierre_sol,
            cajaoperacion_cierre_monto_dol: cierre_dol,
            cheque_sol: cheque_sol,
            cheque_dol: cheque_dol,
            comentario: comentario,
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {
            //$('#msj_cajaoperacion').html("Guardando montos de cierre...");
            //$('#msj_cajaoperacion').show(300);
        },
        success: function (data) {
            if (parseInt(data.estado) == 1) {
                //$('#msj_cajaoperacion').html(data.cajope_msj);
                //cerrar formulario de comentario
                $('#div_modal_cajaoperacion_notificacion_form').modal('hide');
                swal_success("SISTEMA", data.cajope_msj, 3000);
                cajaoperacion_tabla();
            } else {
                swal_warning("AVISO", data.cajope_msj, 6000);
                //$('#msj_cajaoperacion').html("<h4><b>" + data.cajope_msj + "</b></h4>");
            }
        },
        complete: function (data) {
            if (data.statusText != 'success') {
                $('#msj_cajaoperacion').html(data.responseText);
                console.log(data);
            }
        }
    });
}
$("#cmb_fil_empresa_id").change(function () {
    cajaoperacion_tabla();
});


function cajaoperacion_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {
            //			$('#div_cajaoperacion_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_cajaoperacion_tabla').html(html);
            estilos_datatable();
        },
        complete: function () {
            //			$('#div_cajaoperacion_tabla').removeClass("ui-state-disabled");
        }
    });
}


function estilos_datatable() {
    datatable_global = $('#tabla_cajaoperacion').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            { targets: [1, 2, 5, 6], orderable: false }
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function cerrarcaja_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_cajaoperacion_form').html(html);
            $('#modal_cajaoperacion_form').modal('show');
            modal_hidden_bs_modal('modal_cajaoperacion_form', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
            //console.log(html);
        }
    });
}

function Abrir_imagen(cajaoperacion_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_cajaoperacion_img').modal('show');
            modal_hidden_bs_modal('modal_cajaoperacion_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(cajaoperacion_id);
        },
        complete: function (html) {
            //console.log(html);
        }
    });
}


function cajaoperacion_form(act, idf) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            cajope_id: idf,
            vista: 'cajaoperacion_tabla',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_cajaoperacion_form').html(html);
            $('#modal_cajaoperacion_form').modal('show');
            modal_hidden_bs_modal('modal_cajaoperacion_form', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
            //console.log(html);
        }
    });
}


function eliminar_cajaoperacion(id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿Está seguro de Eliminar?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "cajaoperacion_reg.php",
                    async: true,
                    dataType: "html",
                    data: ({
                        action: "eliminar",
                        cajope_id: id
                    }),
                    beforeSend: function () {

                    },
                    success: function (html) {
                        swal_success("MENSAJE", html, 5000);
                    },
                    complete: function () {
                        cajaoperacion_tabla();
                    }
                });
            },
            no: function () { }
        }
    });
}

function seleccionar_cheques(cajaoperacion_id) {
    var cheques_sol = $('#hdd_cheque_sol').val();
    var cheques_dol = $('#hdd_cheque_dol').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cajaoperacion/cajaoperacion_cheques.php",
        async: true,
        dataType: "html",
        data: ({
            cajaoperacion_id: cajaoperacion_id,
            cheques_sol: cheques_sol,
            cheques_dol: cheques_dol
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_cajaoperacion_cheques_form').html(html);
            $('#div_modal_cajaoperacion_cheques_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_cajaoperacion_cheques_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_cajaoperacion_cheques_form')
            modal_width_auto('div_modal_cajaoperacion_cheques_form', 80); //funcion encontrada en public/js/generales.js
        }
    });
}