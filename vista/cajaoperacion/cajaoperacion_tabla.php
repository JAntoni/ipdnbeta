<?php
if (defined('APP_URL')) {
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}



require_once ("Cajaoperacion.class.php");
$oCajaoperacion = new Cajaoperacion();

$empresa_id=$_POST['empresa_id'];

$dts = $oCajaoperacion->mostrarTodos($empresa_id);
//$num_rows = mysql_num_rows($dts);
?>
<input type="hidden" name="hdd_cheque_sol" id="hdd_cheque_sol">
<input type="hidden" name="hdd_cheque_dol" id="hdd_cheque_dol">
<input type="hidden" name="hdd_importe_sol" id="hdd_importe_sol">
<input type="hidden" name="hdd_importe_dol" id="hdd_importe_dol">

<table cellspacing="1" id="tabla_cajaoperacion" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila" align="center">FECHA</th>
            <th id="tabla_cabecera_fila" align="center">USUARIO APERTURA</th>
            <th id="tabla_cabecera_fila" align="center">APERTURA</th>
            <th id="tabla_cabecera_fila" align="center">USUARIO CIERRA</th>
            <th id="tabla_cabecera_fila" align="center">CIERRE</th>
            <th id="tabla_cabecera_fila" align="center">MONTO CIERRE</th>
            <th id="tabla_cabecera_fila" align="center" width="300">OBSERVACION</th>
            <th id="tabla_cabecera_fila" align="center">IMAGEN</th>
        </tr>
    </thead>
<?php if ($dts['estado']== 1) { ?>  
        <tbody>
        <?php
        foreach ($dts['data']as $key => $dt) {
            $fecha_hoy = date('d-m-Y');
            $fecha_apertura = mostrar_fecha($dt['tb_cajaoperacion_apertura_fec']);
            $btn_guardar = '';
            $btn_subir = '<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="upload_form(\'I\', '.$dt['tb_cajaoperacion_id'].')"> Subir Img</a>';
            if ($fecha_hoy == $fecha_apertura) {
                $btn_guardar = ' <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="seleccionar_cheques('. $dt['tb_cajaoperacion_id'] .')"> Guardar</a>';
                //$btn_subir = '<a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="cajaoperacion_img_form(' . $dt['tb_cajaoperacion_id'] . ');">Subir Img</a>';
            }
            ?>
                <tr>
                    <td id="tabla_fila" align="center"><?php echo $dt['tb_cajaoperacion_id'] ?></td>
                    <td id="tabla_fila" align="center"><?php echo mostrar_fecha($dt['tb_cajaoperacion_apertura_fec']) ?></td>
                    <td id="tabla_fila" align="center"><?php echo $dt['usuario_apertura'] ?></td>
                    <td id="tabla_fila" align="center"><?php echo mostrar_fecha_hora($dt['tb_cajaoperacion_apertura_fec']) ?></td>
                    <td id="tabla_fila" align="center"><?php echo $dt['usuario_cierre'] ?></td>
                    <td id="tabla_fila" align="center"><?php echo mostrar_fecha_hora($dt['tb_cajaoperacion_cierre_fec']) ?></td>
                    <td id="tabla_fila" align="center">
        <?php
        echo 'S/. <input type="text" id="txt_cajaoperacion_cierre_monto_sol_' . $dt['tb_cajaoperacion_id'] . '" class="form-control input-sm moneda2" value="'.mostrar_moneda($dt['tb_cajaoperacion_cierre_monto_sol']) . '"> ';
        echo 'USD$ <input type="text" id="txt_cajaoperacion_cierre_monto_dol_' . $dt['tb_cajaoperacion_id'] . '" class="form-control input-sm moneda2" value="'.mostrar_moneda($dt['tb_cajaoperacion_cierre_monto_dol']) . '">';

        echo $btn_guardar;
        ?>

                    </td>
                    <td id="tabla_fila"  align="center"><?php echo $dt['tb_cajaoperacion_obs'] ?></td>
                    <td id="tabla_fila" >
                        <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="Abrir_imagen(<?php echo $dt['tb_cajaoperacion_id'] ?>);"> Ver</a>
                            <?php echo $btn_subir; ?>
                    </td>
                </tr>
                        <?php
                    }
//                    mysql_free_result($dts);
                    ?>
        </tbody>
            <?php
        }
        ?>
<!--    <tr class="even">
        <td colspan="9"><?php echo $num_rows . ' registros' ?></td>
    </tr>-->
</table>