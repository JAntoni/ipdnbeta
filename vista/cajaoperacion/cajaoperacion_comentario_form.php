<?php
require_once('../../core/usuario_sesion.php');
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$array_usu = $_SESSION['usuario_nom'];
$cajaoperacion_id = intval($_POST["cajaoperacion_id"]);
$cierre_sol = $_POST["cierre_sol"];
$cierre_dol = $_POST["cierre_dol"];
$cheque_sol = moneda_mysql($_POST["importe_cheque_sol"]);
$cheque_dol = moneda_mysql($_POST["importe_cheque_dol"]);

?>


<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_cajaoperacion_notificacion_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">COMENTARIO DE APERTURA/CIERRE DE CAJA</h4>
            </div>
            <form id="form_cajaoperacion_comentario" method="post">
                <input name="hdd_cajaoperacion_id" id="hdd_cajaoperacion_id" type="hidden" value="<?php echo $cajaoperacion_id;?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Comentario:</label>
                                <textarea id="txt_comentario" name="txt_comentario" class="form-control input-sm" placeholder="comentario de apertura/cierre de caja"><?php echo "Se cierra caja con S/. $cierre_sol y US$ $cierre_dol. Se cierra cheques con S/. $cheque_sol y US$ $cheque_dol"; ?></textarea>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-info" id="btn_guardar_cajaoperacion_comentario" onclick="cierre_monto(<?php echo $cajaoperacion_id;?>)">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>