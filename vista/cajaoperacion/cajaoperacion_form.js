/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
    var nombre = $("#cmb_empresa_id" + " option:selected" ).text();
            
            $('#hdd_nombreempresa').val(nombre);
    
    
    $('.moneda2').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.99'
    });
    
    
    	$("#for_cajope").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"cajaoperacion/cajaoperacion_reg.php",
				async:true,
				dataType: "json",
				data: $("#for_cajope").serialize(),
				beforeSend: function() {
					
				},
				success: function(data){
                                    
                                    if(data.cajope_id>0){
                                        swal_success("SISTEMA",data.cajope_msj,2000);
					cajaoperacion_tabla();
                                        $('#modal_cajaoperacion_form').modal('hide');
                                    }
                                    else{
                                        swal_warning("AVISO",data.cajope_msj,5000);
                                    }
				},
				complete: function(data){
					console.log(data);
				}
			});
		},
		rules: {

		},
		messages: {

		}
	});


});


function cmb_caj_id(ids) {
    $.ajax({
        type: "POST",
        url: VISTA_URL+"caja/cmb_caj_id.php",
        async: true,
        dataType: "html",
        data: ({
            caj_id: ids
        }),
        beforeSend: function () {
            $('#cmb_caj_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_caj_id').html(html);
        }
    });
}