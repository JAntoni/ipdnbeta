<?php

if (defined('APP_URL')) {
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}
require_once ('Cajaoperacion.class.php');
$oCajaoperacion = new Cajaoperacion();

$empresa_id=$_POST['empresa_id'];

$fecha_hoy = date('d-m-Y');
$dts = $oCajaoperacion->mostrar_fecha_hoy(date('Y-m-d'),$empresa_id);
//if($_POST['action'] == "insertar"){
//
//	$dts = $oCajaoperacion->mostrar_fecha_hoy(date('Y-m-d'));
////		$registros = mysql_num_rows($dts);
////	mysql_free_result($dts);
//
//	if(intval($dts['estado'])== 1){
//		echo '<h2>Ya aperturaste esta caja del '.$fecha_hoy.'</h2>';
//		die();
//	}
//}

if (intval($dts['estado']) == 1 && $_POST['action'] == "insertar") {
//if($_POST['action'] == "insertar"){    
    ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_cajaoperacion_form" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Apertura / Cierre de Caja</h4>
                </div>
                <form id="for_cajope" method="post">
                    <input name="action_cajaoperacion" id="action_cajaoperacion" type="hidden" value="<?php echo $_POST['action'] ?>">
                    <input name="hdd_cajope_id" id="hdd_cajope_id" type="hidden" value="<?php echo $_POST['cajope_id'] ?>">

                    <div class="modal-body">
                        <div class="box box-primary">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo '<h2 style="font-family:cambria"> Ya aperturaste esta caja del ' . $fecha_hoy . ' para la Sede de '.$_SESSION['empresa_nombre'].'</h2>'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
//}
} else {
    ?>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal_cajaoperacion_form" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Apertura / Cierre de Caja</h4>
                </div>
                <form id="for_cajope" method="post">
                    <input name="action_cajaoperacion" id="action_cajaoperacion" type="hidden" value="<?php echo $_POST['action'] ?>">
                    <input name="hdd_cajope_id" id="hdd_cajope_id" type="hidden" value="<?php echo $_POST['cajope_id'] ?>">

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php if ($_POST['action'] == "insertar"): ?>
                                    <h4 style="color: green;">Estás aperturando la caja de este día <?php echo $fecha_hoy; ?>, por favor ingresa una descripción</h4>
                                    <br>
                                    <label>Observación:</label>
                                    <textarea name="txt_cajope_obs" type="text" id="txt_cajope_obs" cols="45" rows="3" class="form-control input-sm"><?php echo $obs ?></textarea>
                                <?php endif; ?>

                                <?php if ($_POST['action'] == "cerrar"): ?>
                                    <h2 style="color: green;">Estás cerrando la caja del <?php echo $fecha_hoy; ?></h2>
                                <?php endif; ?>
                            </div>
                        </div>
                        <p>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Elije la Sede</label>
                                <select class="form-control input-sm mayus" id="cmb_empresa_id" name="cmb_empresa_id">
                                    <?php require_once '../empresa/empresa_select.php'; ?>
                                </select>
                                <input type="hidden" id="hdd_nombreempresa" name="hdd_nombreempresa">
                            </div>
                        </div>
                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="area_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="submit" class="btn btn-info" id="btn_guardar_area">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>


    <?php
}
?>


<script type="text/javascript" src="<?php echo 'vista/cajaoperacion/cajaoperacion_form.js'; ?>"></script>
