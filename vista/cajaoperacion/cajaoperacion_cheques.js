$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
});

function cierre_cheques(cajaoperacion_id){
    var temp_ids_sol = '';
    var temp_ids_dol = '';
    // Selecciona los checkboxes estilizados con iCheck por su atributo "name"
    var checkboxes_sol = $('input[name="txt_cheque_sol"].flat-green');
    var checkboxes_dol = $('input[name="txt_cheque_dol"].flat-green');

    if (checkboxes_sol.length > 0){
        checkboxes_sol.each(function() {
            if (this.checked) {
                temp_ids_sol += $(this).val() + ',';
            }
        });
    }
    if (checkboxes_dol.length > 0){
        checkboxes_dol.each(function() {
            if (this.checked) {
                temp_ids_dol += $(this).val() + ',';
            }
        });
    }

    $("#hdd_cheque_sol").val(temp_ids_sol);
    $("#hdd_cheque_dol").val(temp_ids_dol);

    var cheques_soles = 0.00;
    var cheques_dolares = 0.00;

    // Selecciona los checkboxes estilizados con iCheck por su atributo "name"
    if (checkboxes_sol.length > 0){
        checkboxes_sol.each(function() {
        if (this.checked) {
            cheques_soles += parseFloat($(this).data("importe"));
        }
        });
    }
    if (checkboxes_dol.length > 0){
        checkboxes_dol.each(function() {
        if (this.checked) {
            cheques_dolares += parseFloat($(this).data("importe"));
        }
        });
    }

    $("#hdd_importe_sol").val(cheques_soles);
    $("#hdd_importe_dol").val(cheques_dolares);

    $('#div_modal_cajaoperacion_cheques_form').modal('hide');
    insertar_comentario(cajaoperacion_id)
}