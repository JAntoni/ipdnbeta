<?php
require_once('../../core/usuario_sesion.php');
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();

$array_usu = $_SESSION['usuario_nom'];
$cajaoperacion_id = intval($_POST["cajaoperacion_id"]);
$cheques_sol = $_POST["cheques_sol"];
$arr_cheques_sol = explode(',', $cheques_sol);
$cheques_dol = $_POST["cheques_dol"];
$arr_cheques_dol = explode(',', $cheques_dol);
$hoy = date('d-m-Y');
$hoy_mysql = fecha_mysql($hoy);

$cheques_soles = $oIngreso->ingreso_por_cuenta_subcuenta(2, 176, $hoy_mysql, $_SESSION['empresa_id'], 1);
$cheques_dolares = $oIngreso->ingreso_por_cuenta_subcuenta(2, 176, $hoy_mysql, $_SESSION['empresa_id'], 2);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_cajaoperacion_cheques_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h2 class="modal-title" style="font-weight: bold;font-family: cambria">INGRESOS CHEQUES DEL DÍA:  <?php fecha_mysql($hoy)?></h2>
            </div>
            <form id="form_cajaoperacion_comentario" method="post">
                <input name="hdd_cajaoperacion_id" id="hdd_cajaoperacion_id" type="hidden" value="<?php echo $cajaoperacion_id;?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>CHEQUES SOLES :</h4>
                            <br>
                            <table id="tabla_ingreso_soles" class="table table-hover">
                                <thead>
                                    <tr id="tabla_cabecera">
                                        <th id="tabla_cabecera_fila"></th>
                                        <th id="tabla_cabecera_fila">ID#</th>
                                        <th  id="tabla_cabecera_fila"align="center">NUM DOC</th>
                                        <th  id="tabla_cabecera_fila"align="center">DESCRIPCION</th>   
                                        <th  id="tabla_cabecera_fila"align="center">IMPORTE</th>   
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($cheques_soles['estado']==1){
                                        $checked = '';
                                        foreach ($cheques_soles['data'] as $key => $value) {
                                            if(in_array($value['tb_ingreso_id'], $arr_cheques_sol)){
                                                $checked = 'checked';
                                            }else{
                                                $checked = '';
                                            }
                                            ?>
                                            <tr id="tabla_cabecera_fila">
                                                <td id="tabla_fila"><input type="checkbox" name="txt_cheque_sol" data-importe="<?php echo $value['tb_ingreso_imp']?>" class="flat-green" value="<?php echo $value['tb_ingreso_id']?>" <?php echo $checked ?>></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_id']?></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_numdoc']?></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_det']?></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_imp']?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <h4>CHEQUES DOLARES :</h4>
                            <br>
                            <table id="tabla_ingreso_dolares" class="table table-hover">
                                <thead>
                                    <tr id="tabla_cabecera">
                                        <th id="tabla_cabecera_fila"></th>
                                        <th id="tabla_cabecera_fila">ID#</th>
                                        <th  id="tabla_cabecera_fila"align="center">NUM DOC</th>
                                        <th  id="tabla_cabecera_fila"align="center">DESCRIPCION</th>
                                        <th  id="tabla_cabecera_fila"align="center">IMPORTE</th>   
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($cheques_dolares['estado']==1){
                                        $checked = '';
                                        foreach ($cheques_dolares['data'] as $key => $value) {
                                            if(in_array($value['tb_ingreso_id'], $arr_cheques_dol)){
                                                $checked = 'checked';
                                            }else{
                                                $checked = '';
                                            }
                                            ?>
                                            <tr id="tabla_cabecera_fila">
                                                <td id="tabla_fila"><input type="checkbox" name="txt_cheque_dol" data-importe="<?php echo $value['tb_ingreso_imp']?>" class="flat-green" value="<?php echo $value['tb_ingreso_id']?>" <?php echo $checked ?>></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_id']?></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_numdoc']?></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_det']?></td>
                                                <td id="tabla_fila"><?php echo $value['tb_ingreso_imp']?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-info" id="btn_guardar_cajaoperacion_comentario" onclick="cierre_cheques(<?php echo $cajaoperacion_id;?>)">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/cajaoperacion/cajaoperacion_cheques.js?ver=011'; ?>"></script>