<?php
require_once ('../../core/usuario_sesion.php');
require_once("Cajaoperacion.class.php");
$oCajaoperacion = new Cajaoperacion();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

/* GERSON (20-04-23) */
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
/*  */

if($_POST['action_cajaoperacion']=="insertar"){
            
            $dts = $oCajaoperacion->mostrar_fecha_hoy(date('Y-m-d'),intval($_POST['cmb_empresa_id']));
            
            if($dts['estado']==1){
                $data['cajope_id'] = 0;
		$data['cajope_msj'] = 'La Caja de Esta Sede '.$_POST['hdd_nombreempresa'].' ya se Encuentra Aperturada';
                echo json_encode($data);
                
                exit();
            }

		//$oCajaoperacion->cajaoperacion_apertura_fec; automático con NOW()
		//$oCajaoperacion->cajaoperacion_cierre_fec; 
		$oCajaoperacion->cajaoperacion_apertura_est = 1; //1 apertura
		$oCajaoperacion->cajaoperacion_cierre_est = 0; // 0 sin cierre
		$oCajaoperacion->usuario_apertura_id = intval($_SESSION['usuario_id']);
		$oCajaoperacion->empresa_id = intval($_POST['cmb_empresa_id']);
		$oCajaoperacion->cajaoperacion_obs = $_SESSION['usuario_nom'].': '.$_POST['txt_cajope_obs'].' | '.date('h:i a');

                $result=$oCajaoperacion->insertar();
            if(intval($result['estado']) == 1){
                $cajaoperacion_id = $result['cajaoperacion_id'];
            }
		
		$data['cajope_id'] = $cajaoperacion_id;
		$data['cajope_msj'] = 'Se registró la Apertura de la Caja correctamente.';

	echo json_encode($data);
}

if($_POST['action_cajaoperacion']=="cerrar"){
	$fecha_hoy = date('Y-m-d');
	$usuario_cierre = intval($_SESSION['usuario_id']);
	$observacion = trim($_SESSION['usuario_nom']).': ha cerrado la caja del dia. | '.date('h:i a');
        $empresa_id=intval($_POST['cmb_empresa_id']);

	$oCajaoperacion->cerrar_caja($fecha_hoy, $usuario_cierre, $observacion,$empresa_id);
	
        $data['cajope_id'] = 1;
	$data['cajope_msj'] = 'Se ha cerrado la caja de manera adecuada. Caja de: '.$fecha_hoy;

	echo json_encode($data);
}

if($_POST['action_cajaoperacion']=="cierre_monto"){
	$cajaoperacion_id = $_POST['cajaoperacion_id'];
	$cajaoperacion_cierre_monto_sol = $_POST['cajaoperacion_cierre_monto_sol'];
	$cajaoperacion_cierre_monto_dol = $_POST['cajaoperacion_cierre_monto_dol'];
	$cheque_sol = $_POST['cheque_sol'];
	$cheque_dol = $_POST['cheque_dol'];
	$observacion = trim($_SESSION['usuario_nom']).': está cerrando la caja con S/. '.$cajaoperacion_cierre_monto_sol.' y US$ '.$cajaoperacion_cierre_monto_dol.' | '.date('h:i a');
  $empresa_id=intval($_POST['empresa_id']);
        
	$oCajaoperacion->guardar_monto_cierre($cajaoperacion_id, moneda_mysql($cajaoperacion_cierre_monto_sol), moneda_mysql($cajaoperacion_cierre_monto_dol));

  /* GERSON (20-04-23) */ /* ANTHONY 2024-12-02*/
  if(moneda_mysql($cajaoperacion_cierre_monto_sol)>0 || moneda_mysql($cajaoperacion_cierre_monto_dol)>0){

    $hoy = fecha_mysql($_POST['txt_ing_fec']);
    $mod_date = strtotime($hoy."+ 1 days");
    $fecha = date("Y-m-d",$mod_date);

    $sede_id = intval($_SESSION['empresa_id']);

    if(moneda_mysql($cajaoperacion_cierre_monto_sol)>0){
      //detectar si existe apertura de caja
      $aperturasoles = $oIngreso->existeApertura_moneda($fecha, 17, 55, $sede_id, 1);
      
      if($aperturasoles['estado']==1){ // si existe se elimina y se agrega el nuevo registro de ingreso

        //$oIngreso->modificar_campo($apertura['data']['tb_ingreso_id'],'tb_ingreso_xac',0,'INT');

        // Si ya existe, al igual que la apertura en cajaoperacion, solo se modificará el ingreso
        $oIngreso->modificar_campo($aperturasoles['data']['tb_ingreso_id'],'tb_ingreso_fecmod', date("Y-m-d H:i:s"),'STR');
        $oIngreso->modificar_campo($aperturasoles['data']['tb_ingreso_id'],'tb_ingreso_det', 'INGRESO DE APERTURA DE CAJA DEL DÍA '.mostrar_fecha($fecha).' TOTAL S/'.moneda_mysql($cajaoperacion_cierre_monto_sol),'STR');
        $oIngreso->modificar_campo($aperturasoles['data']['tb_ingreso_id'],'tb_ingreso_imp', moneda_mysql($cajaoperacion_cierre_monto_sol),'INT');

        
      }else{

        // detectar domingo o feriado 
        $dia=intval(date("w", strtotime($fecha))); // domingos
        $feriado = $oAsistencia->detectar_feriado_fecha($fecha); // feriados

        // detectar responsable
        $user_id = 0;
        if($sede_id==1){
          $user_id = 1144; // carlos
        }elseif($sede_id==2){
          //$user_id = 1619; // juan
          $user_id = 1144; // carlos --> Actualizacion 08-09-32: Sede Boulevard solicitó cambio de responsable para cierres de caja
        }
        if($feriado['estado']==0){ // Identifica si no se trata de un FERIADO
          if($dia>0){ // Identifica si el día no es un DOMINGO

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha; // fecha del día siguiente laborable
            $oIngreso->documento_id = 8; // otros ingresos
            $oIngreso->ingreso_numdoc = '';
            $oIngreso->ingreso_det = 'INGRESO DE APERTURA DE CAJA DEL DÍA '.mostrar_fecha($fecha).' TOTAL S/'.moneda_mysql($cajaoperacion_cierre_monto_sol); 
            $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_sol);  
            $oIngreso->cuenta_id = 17; // cuenta
            $oIngreso->subcuenta_id = 55; // subcuenta
            $oIngreso->cliente_id = $user_id; 
            $oIngreso->caja_id = 1; 
            $oIngreso->moneda_id = 1;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = 0; 
            $oIngreso->ingreso_modide = 0; 
            $oIngreso->empresa_id = $sede_id;
            $oIngreso->ingreso_fecdep = ''; 
            $oIngreso->ingreso_numope = ''; 
            $oIngreso->ingreso_mondep = 0; 
            $oIngreso->ingreso_comi = 0; 
            $oIngreso->cuentadeposito_id = 0; 
            $oIngreso->banco_id = 0; 
            $oIngreso->ingreso_ap = 0; 
            $oIngreso->ingreso_detex = '';

            $result=$oIngreso->insertar();
            if($result>0){
              $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
            }
          }else{
            // si es domingo es decir la apertura pasará para el día lunes de la siguiente semana
            $hoy = fecha_mysql($_POST['txt_ing_fec']);
            $mod_date_sab = strtotime($fecha."+ 1 days");
            $fecha_sab = date("Y-m-d",$mod_date_sab);

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha_sab; // fecha del día siguiente laborable, en este caso se trata de sábado entonces pasa para el lunes
            $oIngreso->documento_id = 8; // otros ingresos
            $oIngreso->ingreso_numdoc = '';
            $oIngreso->ingreso_det = 'Ingreso de apertura de caja del día '.mostrar_fecha($fecha_sab).' total S/'.moneda_mysql($cajaoperacion_cierre_monto_sol); 
            $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_sol);  
            $oIngreso->cuenta_id = 17; // cuenta
            $oIngreso->subcuenta_id = 55; // subcuenta
            $oIngreso->cliente_id = $user_id; 
            $oIngreso->caja_id = 1; 
            $oIngreso->moneda_id = 1;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = 0; 
            $oIngreso->ingreso_modide = 0; 
            $oIngreso->empresa_id = $sede_id;
            $oIngreso->ingreso_fecdep = ''; 
            $oIngreso->ingreso_numope = ''; 
            $oIngreso->ingreso_mondep = 0; 
            $oIngreso->ingreso_comi = 0; 
            $oIngreso->cuentadeposito_id = 0; 
            $oIngreso->banco_id = 0; 
            $oIngreso->ingreso_ap = 0; 
            $oIngreso->ingreso_detex = '';

            $result=$oIngreso->insertar();
            if($result>0){
              $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
            }
          }
        }else{ // es feriado
          
          $mod_date2 = strtotime($fecha."+ 1 days"); // identificamos si el siguiente dái es feriado
          $fecha2 = date("Y-m-d",$mod_date2);

          // detectar domingo o feriado 
          $dia2=intval(date("w", strtotime($fecha2))); // domingo
          $feriado2 = $oAsistencia->detectar_feriado_fecha($fecha2); // feriado

          //detectar si existe apertura de caja
          $apertura2 = $oIngreso->existeApertura_moneda($fecha2, 17, 55, $sede_id, 1);

          if($apertura2['estado']==1){ // si existe se elimina y se agrega el nuevo registro de ingreso
            $oIngreso->modificar_campo($apertura2['data']['tb_ingreso_id'],'tb_ingreso_xac',0,'INT');
          }

          if($feriado2['estado']==0){ // Identifica si no se trata de un FERIADO
            if($dia2>0){ // Identifica si el día no es un DOMINGO
      
              $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_fec = $fecha2; // fecha del día siguiente laborable
              $oIngreso->documento_id = 8; // otros ingresos
              $oIngreso->ingreso_numdoc = '';
              $oIngreso->ingreso_det = 'Ingreso de apertura de caja del día '.mostrar_fecha($fecha2).' total S/'.moneda_mysql($cajaoperacion_cierre_monto_sol); 
              $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_sol);  
              $oIngreso->cuenta_id = 17; // cuenta
              $oIngreso->subcuenta_id = 55; // subcuenta
              $oIngreso->cliente_id = $user_id; 
              $oIngreso->caja_id = 1; 
              $oIngreso->moneda_id = 1;
              //valores que pueden ser cambiantes según requerimiento de ingreso
              $oIngreso->modulo_id = 0; 
              $oIngreso->ingreso_modide = 0; 
              $oIngreso->empresa_id = $sede_id;
              $oIngreso->ingreso_fecdep = ''; 
              $oIngreso->ingreso_numope = ''; 
              $oIngreso->ingreso_mondep = 0; 
              $oIngreso->ingreso_comi = 0; 
              $oIngreso->cuentadeposito_id = 0; 
              $oIngreso->banco_id = 0; 
              $oIngreso->ingreso_ap = 0; 
              $oIngreso->ingreso_detex = '';
      
              $result=$oIngreso->insertar();
              if($result>0){
                $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
              }
            }
          }else{
            $mod_date3 = strtotime($fecha2."+ 1 days"); // identificamos si el siguiente dái es feriado
            $fecha3 = date("Y-m-d",$mod_date3);
      
            // detectar domingo o feriado 
            $dia3=intval(date("w", strtotime($fecha3))); // domingo
            $feriado3 = $oAsistencia->detectar_feriado_fecha($fecha3); // feriado

            //detectar si existe apertura de caja
            $apertura3 = $oIngreso->existeApertura_moneda($fecha3, 17, 55, $sede_id, 1);

            if($apertura3['estado']==1){ // si existe se elimina y se agrega el nuevo registro de ingreso
              $oIngreso->modificar_campo($apertura3['data']['tb_ingreso_id'],'tb_ingreso_xac',0,'INT');
            }

            if($feriado3['estado']==0){ // Identifica si no se trata de un FERIADO
              if($dia3>0){ // Identifica si el día no es un DOMINGO
   
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha3; // fecha del día siguiente laborable
                $oIngreso->documento_id = 8; // otros ingresos
                $oIngreso->ingreso_numdoc = '';
                $oIngreso->ingreso_det = 'Ingreso de apertura de caja del día '.mostrar_fecha($fecha3).' total S/'.moneda_mysql($cajaoperacion_cierre_monto_sol); 
                $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_sol);  
                $oIngreso->cuenta_id = 17; // cuenta
                $oIngreso->subcuenta_id = 55; // subcuenta
                $oIngreso->cliente_id = $user_id; 
                $oIngreso->caja_id = 1; 
                $oIngreso->moneda_id = 1;
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = 0; 
                $oIngreso->ingreso_modide = 0; 
                $oIngreso->empresa_id = $sede_id;
                $oIngreso->ingreso_fecdep = ''; 
                $oIngreso->ingreso_numope = ''; 
                $oIngreso->ingreso_mondep = 0; 
                $oIngreso->ingreso_comi = 0; 
                $oIngreso->cuentadeposito_id = 0; 
                $oIngreso->banco_id = 0; 
                $oIngreso->ingreso_ap = 0; 
                $oIngreso->ingreso_detex = '';
        
                $result=$oIngreso->insertar();
                if($result>0){
                  $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
                }
              }
            }
          }

        }

      }
    }
    if(moneda_mysql($cajaoperacion_cierre_monto_dol)>0){
      //detectar si existe apertura de caja
      $aperturadolares = $oIngreso->existeApertura_moneda($fecha, 17, 55, $sede_id, 2);
      if($aperturadolares['estado']==1){ // si existe se elimina y se agrega el nuevo registro de ingreso
        //$oIngreso->modificar_campo($apertura['data']['tb_ingreso_id'],'tb_ingreso_xac',0,'INT');
        // Si ya existe, al igual que la apertura en cajaoperacion, solo se modificará el ingreso
        $oIngreso->modificar_campo($aperturadolares['data']['tb_ingreso_id'],'tb_ingreso_fecmod', date("Y-m-d H:i:s"),'STR');
        $oIngreso->modificar_campo($aperturadolares['data']['tb_ingreso_id'],'tb_ingreso_det', 'INGRESO DE APERTURA DE CAJA DEL DÍA '.mostrar_fecha($fecha).' TOTAL S/'.moneda_mysql($cajaoperacion_cierre_monto_dol),'STR');
        $oIngreso->modificar_campo($aperturadolares['data']['tb_ingreso_id'],'tb_ingreso_imp', moneda_mysql($cajaoperacion_cierre_monto_dol),'INT');
      }else{
        // detectar domingo o feriado 
        $dia=intval(date("w", strtotime($fecha))); // domingos
        $feriado = $oAsistencia->detectar_feriado_fecha($fecha); // feriados

        // detectar responsable
        $user_id = 0;
        if($sede_id==1){
          $user_id = 1144; // carlos
        }elseif($sede_id==2){
          //$user_id = 1619; // juan
          $user_id = 1144; // carlos --> Actualizacion 08-09-32: Sede Boulevard solicitó cambio de responsable para cierres de caja
        }
        if($feriado['estado']==0){ // Identifica si no se trata de un FERIADO
          if($dia>0){ // Identifica si el día no es un DOMINGO

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha; // fecha del día siguiente laborable
            $oIngreso->documento_id = 8; // otros ingresos
            $oIngreso->ingreso_numdoc = '';
            $oIngreso->ingreso_det = 'INGRESO DE APERTURA DE CAJA DEL DÍA '.mostrar_fecha($fecha).' TOTAL US$ '.moneda_mysql($cajaoperacion_cierre_monto_dol); 
            $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_dol);  
            $oIngreso->cuenta_id = 17; // cuenta
            $oIngreso->subcuenta_id = 55; // subcuenta
            $oIngreso->cliente_id = $user_id; 
            $oIngreso->caja_id = 1; 
            $oIngreso->moneda_id = 2;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = 0; 
            $oIngreso->ingreso_modide = 0; 
            $oIngreso->empresa_id = $sede_id;
            $oIngreso->ingreso_fecdep = ''; 
            $oIngreso->ingreso_numope = ''; 
            $oIngreso->ingreso_mondep = 0; 
            $oIngreso->ingreso_comi = 0; 
            $oIngreso->cuentadeposito_id = 0; 
            $oIngreso->banco_id = 0; 
            $oIngreso->ingreso_ap = 0; 
            $oIngreso->ingreso_detex = '';

            $result=$oIngreso->insertar();
            if($result>0){
              $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
            }
          }else{
            // si es domingo es decir la apertura pasará para el día lunes de la siguiente semana
            $hoy = fecha_mysql($_POST['txt_ing_fec']);
            $mod_date_sab = strtotime($fecha."+ 1 days");
            $fecha_sab = date("Y-m-d",$mod_date_sab);

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha_sab; // fecha del día siguiente laborable, en este caso se trata de sábado entonces pasa para el lunes
            $oIngreso->documento_id = 8; // otros ingresos
            $oIngreso->ingreso_numdoc = '';
            $oIngreso->ingreso_det = 'Ingreso de apertura de caja del día '.mostrar_fecha($fecha_sab).' total US$'.moneda_mysql($cajaoperacion_cierre_monto_dol); 
            $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_dol);  
            $oIngreso->cuenta_id = 17; // cuenta
            $oIngreso->subcuenta_id = 55; // subcuenta
            $oIngreso->cliente_id = $user_id; 
            $oIngreso->caja_id = 1; 
            $oIngreso->moneda_id = 2;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = 0; 
            $oIngreso->ingreso_modide = 0; 
            $oIngreso->empresa_id = $sede_id;
            $oIngreso->ingreso_fecdep = ''; 
            $oIngreso->ingreso_numope = ''; 
            $oIngreso->ingreso_mondep = 0; 
            $oIngreso->ingreso_comi = 0; 
            $oIngreso->cuentadeposito_id = 0; 
            $oIngreso->banco_id = 0; 
            $oIngreso->ingreso_ap = 0; 
            $oIngreso->ingreso_detex = '';

            $result=$oIngreso->insertar();
            if($result>0){
              $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
            }
          }
        }else{
          $mod_date2 = strtotime($fecha."+ 1 days"); // identificamos si el siguiente dái es feriado
          $fecha2 = date("Y-m-d",$mod_date2);

          // detectar domingo o feriado 
          $dia2=intval(date("w", strtotime($fecha2))); // domingo
          $feriado2 = $oAsistencia->detectar_feriado_fecha($fecha2); // feriado

          //detectar si existe apertura de caja
          $apertura2 = $oIngreso->existeApertura_moneda($fecha2, 17, 55, $sede_id, 2);

          if($apertura2['estado']==1){ // si existe se elimina y se agrega el nuevo registro de ingreso
            $oIngreso->modificar_campo($apertura2['data']['tb_ingreso_id'],'tb_ingreso_xac',0,'INT');
          }

          if($feriado2['estado']==0){ // Identifica si no se trata de un FERIADO
            if($dia2>0){ // Identifica si el día no es un DOMINGO
      
              $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_fec = $fecha2; // fecha del día siguiente laborable
              $oIngreso->documento_id = 8; // otros ingresos
              $oIngreso->ingreso_numdoc = '';
              $oIngreso->ingreso_det = 'Ingreso de apertura de caja del día '.mostrar_fecha($fecha2).' total US$ '.moneda_mysql($cajaoperacion_cierre_monto_dol); 
              $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_dol);  
              $oIngreso->cuenta_id = 17; // cuenta
              $oIngreso->subcuenta_id = 55; // subcuenta
              $oIngreso->cliente_id = $user_id; 
              $oIngreso->caja_id = 1; 
              $oIngreso->moneda_id = 2;
              //valores que pueden ser cambiantes según requerimiento de ingreso
              $oIngreso->modulo_id = 0; 
              $oIngreso->ingreso_modide = 0; 
              $oIngreso->empresa_id = $sede_id;
              $oIngreso->ingreso_fecdep = ''; 
              $oIngreso->ingreso_numope = ''; 
              $oIngreso->ingreso_mondep = 0; 
              $oIngreso->ingreso_comi = 0; 
              $oIngreso->cuentadeposito_id = 0; 
              $oIngreso->banco_id = 0; 
              $oIngreso->ingreso_ap = 0; 
              $oIngreso->ingreso_detex = '';
      
              $result=$oIngreso->insertar();
              if($result>0){
                $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
              }
            }else{
              $mod_date3 = strtotime($fecha2."+ 1 days"); // identificamos si el siguiente dái es feriado
              $fecha3 = date("Y-m-d",$mod_date3);
        
              // detectar domingo o feriado 
              $dia3=intval(date("w", strtotime($fecha3))); // domingo
              $feriado3 = $oAsistencia->detectar_feriado_fecha($fecha3); // feriado

              //detectar si existe apertura de caja
              $apertura3 = $oIngreso->existeApertura_moneda($fecha3, 17, 55, $sede_id, 2);

              if($apertura3['estado']==1){ // si existe se elimina y se agrega el nuevo registro de ingreso
                $oIngreso->modificar_campo($apertura3['data']['tb_ingreso_id'],'tb_ingreso_xac',0,'INT');
              }

              if($feriado3['estado']==0){ // Identifica si no se trata de un FERIADO
                if($dia3>0){ // Identifica si el día no es un DOMINGO
          
                  $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                  $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                  $oIngreso->ingreso_fec = $fecha3; // fecha del día siguiente laborable
                  $oIngreso->documento_id = 8; // otros ingresos
                  $oIngreso->ingreso_numdoc = '';
                  $oIngreso->ingreso_det = 'Ingreso de apertura de caja del día '.mostrar_fecha($fecha3).' total US$ '.moneda_mysql($cajaoperacion_cierre_monto_dol); 
                  $oIngreso->ingreso_imp = moneda_mysql($cajaoperacion_cierre_monto_dol);  
                  $oIngreso->cuenta_id = 17; // cuenta
                  $oIngreso->subcuenta_id = 55; // subcuenta
                  $oIngreso->cliente_id = $user_id; 
                  $oIngreso->caja_id = 1; 
                  $oIngreso->moneda_id = 2;
                  //valores que pueden ser cambiantes según requerimiento de ingreso
                  $oIngreso->modulo_id = 0; 
                  $oIngreso->ingreso_modide = 0; 
                  $oIngreso->empresa_id = $sede_id;
                  $oIngreso->ingreso_fecdep = ''; 
                  $oIngreso->ingreso_numope = ''; 
                  $oIngreso->ingreso_mondep = 0; 
                  $oIngreso->ingreso_comi = 0; 
                  $oIngreso->cuentadeposito_id = 0; 
                  $oIngreso->banco_id = 0; 
                  $oIngreso->ingreso_ap = 0; 
                  $oIngreso->ingreso_detex = '';
          
                  $result=$oIngreso->insertar();
                  if($result>0){
                    $data['mensaje_apertura'] = 'Apertura de caja registrado correctamente.';
                  }
                }
              }
            }
          }
        }

      }
    }

  }
  /*  */
  /* ANTHONY 2024-12-05 */
  if($cheque_sol != "" || $cheque_dol != ""){

    $hoy = fecha_mysql($_POST['txt_ing_fec']);
    $mod_date = strtotime($hoy."+ 1 days");
    $fecha = date("Y-m-d",$mod_date);

    $sede_id = intval($_SESSION['empresa_id']);

    if($cheque_sol != ""){
      // detectar domingo o feriado 
      $dia=intval(date("w", strtotime($fecha))); // domingos
      $feriado = $oAsistencia->detectar_feriado_fecha($fecha); // feriados

      // detectar responsable
      $user_id = 0;
      if($sede_id==1){
        $user_id = 1144; // carlos
      }elseif($sede_id==2){
        //$user_id = 1619; // juan
        $user_id = 1144; // carlos --> Actualizacion 08-09-32: Sede Boulevard solicitó cambio de responsable para cierres de caja
      }

      if($feriado['estado']==0){ // Identifica si no se trata de un FERIADO
        if($dia>0){ // Identifica si el día no es un DOMINGO
          $arr_cheque = explode(',', $cheque_sol);
          $indice = count($arr_cheque)-1;
          unset($arr_cheque[$indice]);
          foreach ($arr_cheque as $key => $cheque_id) {
            $ing_cheque = $oIngreso->mostrarUno($cheque_id);
            if($ing_cheque['estado'] == 1){
              $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_fec = $fecha;
              $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
              $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
              $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
              $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
              $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
              $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
              $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
              $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
              $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
              //valores que pueden ser cambiantes según requerimiento de ingreso
              $oIngreso->modulo_id = 0;
              $oIngreso->ingreso_modide = 0;
              $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
              $oIngreso->ingreso_fecdep = '';
              $oIngreso->ingreso_numope = '';
              $oIngreso->ingreso_mondep = 0;
              $oIngreso->ingreso_comi = 0;
              $oIngreso->cuentadeposito_id = 0;
              $oIngreso->banco_id = 0;
              $oIngreso->ingreso_ap = 0;
              $oIngreso->ingreso_detex = '';
              $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
            
              $registro_cheque=$oIngreso->insertarChequeProceso();
            }
          }
        }else{
            // si es domingo es decir la apertura pasará para el día lunes de la siguiente semana
            $hoy = fecha_mysql($_POST['txt_ing_fec']);
            $mod_date_sab = strtotime($fecha."+ 1 days");
            $fecha_sab = date("Y-m-d",$mod_date_sab);

            $arr_cheque = explode(',', $cheque_sol);
            $indice = count($arr_cheque)-1;
            unset($arr_cheque[$indice]);
            foreach ($arr_cheque as $key => $cheque_id) {
              $ing_cheque = $oIngreso->mostrarUno($cheque_id);
              if($ing_cheque['estado'] == 1){
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha_sab;
                $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
                $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
                $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
                $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
                $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
                $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
                $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
                $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
                $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = 0;
                $oIngreso->ingreso_modide = 0;
                $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
                $oIngreso->ingreso_fecdep = '';
                $oIngreso->ingreso_numope = '';
                $oIngreso->ingreso_mondep = 0;
                $oIngreso->ingreso_comi = 0;
                $oIngreso->cuentadeposito_id = 0;
                $oIngreso->banco_id = 0;
                $oIngreso->ingreso_ap = 0;
                $oIngreso->ingreso_detex = '';
                $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
              
                $registro_cheque=$oIngreso->insertarChequeProceso();
              }
            }
        }
      }else{
        $mod_date2 = strtotime($fecha."+ 1 days"); // identificamos si el siguiente dái es feriado
        $fecha2 = date("Y-m-d",$mod_date2);

        // detectar domingo o feriado 
        $dia2=intval(date("w", strtotime($fecha2))); // domingo
        $feriado2 = $oAsistencia->detectar_feriado_fecha($fecha2); // feriado

        if($feriado2['estado']==0){ // Identifica si no se trata de un FERIADO
          if($dia2>0){ // Identifica si el día no es un DOMINGO
            $arr_cheque = explode(',', $cheque_sol);
            $indice = count($arr_cheque)-1;
            unset($arr_cheque[$indice]);
            foreach ($arr_cheque as $key => $cheque_id) {
              $ing_cheque = $oIngreso->mostrarUno($cheque_id);
              if($ing_cheque['estado'] == 1){
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha2;
                $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
                $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
                $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
                $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
                $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
                $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
                $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
                $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
                $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = 0;
                $oIngreso->ingreso_modide = 0;
                $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
                $oIngreso->ingreso_fecdep = '';
                $oIngreso->ingreso_numope = '';
                $oIngreso->ingreso_mondep = 0;
                $oIngreso->ingreso_comi = 0;
                $oIngreso->cuentadeposito_id = 0;
                $oIngreso->banco_id = 0;
                $oIngreso->ingreso_ap = 0;
                $oIngreso->ingreso_detex = '';
                $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
              
                $registro_cheque=$oIngreso->insertarChequeProceso();
              }
            }
          }else{
            $mod_date3 = strtotime($fecha2."+ 1 days"); // identificamos si el siguiente dái es feriado
            $fecha3 = date("Y-m-d",$mod_date3);
      
            // detectar domingo o feriado 
            $dia3=intval(date("w", strtotime($fecha3))); // domingo
            $feriado3 = $oAsistencia->detectar_feriado_fecha($fecha3); // feriado
  
            if($feriado3['estado']==0){ // Identifica si no se trata de un FERIADO
              if($dia3>0){ // Identifica si el día no es un DOMINGO
                $arr_cheque = explode(',', $cheque_sol);
                $indice = count($arr_cheque)-1;
                unset($arr_cheque[$indice]);
                foreach ($arr_cheque as $key => $cheque_id) {
                  $ing_cheque = $oIngreso->mostrarUno($cheque_id);
                  if($ing_cheque['estado'] == 1){
                    $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_fec = $fecha3;
                    $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
                    $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
                    $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
                    $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
                    $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
                    $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
                    $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
                    $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
                    $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
                    //valores que pueden ser cambiantes según requerimiento de ingreso
                    $oIngreso->modulo_id = 0;
                    $oIngreso->ingreso_modide = 0;
                    $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
                    $oIngreso->ingreso_fecdep = '';
                    $oIngreso->ingreso_numope = '';
                    $oIngreso->ingreso_mondep = 0;
                    $oIngreso->ingreso_comi = 0;
                    $oIngreso->cuentadeposito_id = 0;
                    $oIngreso->banco_id = 0;
                    $oIngreso->ingreso_ap = 0;
                    $oIngreso->ingreso_detex = '';
                    $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
                  
                    $registro_cheque=$oIngreso->insertarChequeProceso();
                  }
                }
              }
            }
          }
        }
      }
    }

    if($cheque_dol != ""){
      // detectar domingo o feriado 
      $dia=intval(date("w", strtotime($fecha))); // domingos
      $feriado = $oAsistencia->detectar_feriado_fecha($fecha); // feriados

      // detectar responsable
      $user_id = 0;
      if($sede_id==1){
        $user_id = 1144; // carlos
      }elseif($sede_id==2){
        //$user_id = 1619; // juan
        $user_id = 1144; // carlos --> Actualizacion 08-09-32: Sede Boulevard solicitó cambio de responsable para cierres de caja
      }

      if($feriado['estado']==0){ // Identifica si no se trata de un FERIADO
        if($dia>0){ // Identifica si el día no es un DOMINGO
          $arr_cheque = explode(',', $cheque_dol);
          $indice = count($arr_cheque)-1;
          unset($arr_cheque[$indice]);
          foreach ($arr_cheque as $key => $cheque_id) {
            $ing_cheque = $oIngreso->mostrarUno($cheque_id);
            if($ing_cheque['estado'] == 1){
              $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
              $oIngreso->ingreso_fec = $fecha;
              $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
              $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
              $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
              $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
              $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
              $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
              $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
              $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
              $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
              //valores que pueden ser cambiantes según requerimiento de ingreso
              $oIngreso->modulo_id = 0;
              $oIngreso->ingreso_modide = 0;
              $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
              $oIngreso->ingreso_fecdep = '';
              $oIngreso->ingreso_numope = '';
              $oIngreso->ingreso_mondep = 0;
              $oIngreso->ingreso_comi = 0;
              $oIngreso->cuentadeposito_id = 0;
              $oIngreso->banco_id = 0;
              $oIngreso->ingreso_ap = 0;
              $oIngreso->ingreso_detex = '';
              $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
            
              $registro_cheque=$oIngreso->insertarChequeProceso();
            }
          }
        }else{
            // si es domingo es decir la apertura pasará para el día lunes de la siguiente semana
            $hoy = fecha_mysql($_POST['txt_ing_fec']);
            $mod_date_sab = strtotime($fecha."+ 1 days");
            $fecha_sab = date("Y-m-d",$mod_date_sab);

            $arr_cheque = explode(',', $cheque_dol);
            $indice = count($arr_cheque)-1;
            unset($arr_cheque[$indice]);
            foreach ($arr_cheque as $key => $cheque_id) {
              $ing_cheque = $oIngreso->mostrarUno($cheque_id);
              if($ing_cheque['estado'] == 1){
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha_sab;
                $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
                $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
                $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
                $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
                $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
                $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
                $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
                $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
                $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = 0;
                $oIngreso->ingreso_modide = 0;
                $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
                $oIngreso->ingreso_fecdep = '';
                $oIngreso->ingreso_numope = '';
                $oIngreso->ingreso_mondep = 0;
                $oIngreso->ingreso_comi = 0;
                $oIngreso->cuentadeposito_id = 0;
                $oIngreso->banco_id = 0;
                $oIngreso->ingreso_ap = 0;
                $oIngreso->ingreso_detex = '';
                $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
              
                $registro_cheque=$oIngreso->insertarChequeProceso();
              }
            }
        }
      }else{
        $mod_date2 = strtotime($fecha."+ 1 days"); // identificamos si el siguiente dái es feriado
        $fecha2 = date("Y-m-d",$mod_date2);

        // detectar domingo o feriado 
        $dia2=intval(date("w", strtotime($fecha2))); // domingo
        $feriado2 = $oAsistencia->detectar_feriado_fecha($fecha2); // feriado

        if($feriado2['estado']==0){ // Identifica si no se trata de un FERIADO
          if($dia2>0){ // Identifica si el día no es un DOMINGO
            $arr_cheque = explode(',', $cheque_dol);
            $indice = count($arr_cheque)-1;
            unset($arr_cheque[$indice]);
            foreach ($arr_cheque as $key => $cheque_id) {
              $ing_cheque = $oIngreso->mostrarUno($cheque_id);
              if($ing_cheque['estado'] == 1){
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha2;
                $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
                $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
                $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
                $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
                $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
                $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
                $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
                $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
                $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = 0;
                $oIngreso->ingreso_modide = 0;
                $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
                $oIngreso->ingreso_fecdep = '';
                $oIngreso->ingreso_numope = '';
                $oIngreso->ingreso_mondep = 0;
                $oIngreso->ingreso_comi = 0;
                $oIngreso->cuentadeposito_id = 0;
                $oIngreso->banco_id = 0;
                $oIngreso->ingreso_ap = 0;
                $oIngreso->ingreso_detex = '';
                $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
              
                $registro_cheque=$oIngreso->insertarChequeProceso();
              }
            }
          }else{
            $mod_date3 = strtotime($fecha2."+ 1 days"); // identificamos si el siguiente dái es feriado
            $fecha3 = date("Y-m-d",$mod_date3);
      
            // detectar domingo o feriado 
            $dia3=intval(date("w", strtotime($fecha3))); // domingo
            $feriado3 = $oAsistencia->detectar_feriado_fecha($fecha3); // feriado
  
            if($feriado3['estado']==0){ // Identifica si no se trata de un FERIADO
              if($dia3>0){ // Identifica si el día no es un DOMINGO
                $arr_cheque = explode(',', $cheque_dol);
                $indice = count($arr_cheque)-1;
                unset($arr_cheque[$indice]);
                foreach ($arr_cheque as $key => $cheque_id) {
                  $ing_cheque = $oIngreso->mostrarUno($cheque_id);
                  if($ing_cheque['estado'] == 1){
                    $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_fec = $fecha3;
                    $oIngreso->documento_id = $ing_cheque['data']['tb_documento_id']; // otros ingresos
                    $oIngreso->ingreso_numdoc = $ing_cheque['data']['tb_ingreso_numdoc'];
                    $oIngreso->ingreso_det = $ing_cheque['data']['tb_ingreso_det'];
                    $oIngreso->ingreso_imp = moneda_mysql($ing_cheque['data']['tb_ingreso_imp']);
                    $oIngreso->cuenta_id = $ing_cheque['data']['tb_cuenta_id']; // INGRESO DE CAJA
                    $oIngreso->subcuenta_id = $ing_cheque['data']['tb_subcuenta_id']; // CHEQUE ALMACENADO
                    $oIngreso->cliente_id = $ing_cheque['data']['tb_cliente_id'];
                    $oIngreso->caja_id = $ing_cheque['data']['tb_caja_id'];
                    $oIngreso->moneda_id = $ing_cheque['data']['tb_moneda_id'];
                    //valores que pueden ser cambiantes según requerimiento de ingreso
                    $oIngreso->modulo_id = 0;
                    $oIngreso->ingreso_modide = 0;
                    $oIngreso->empresa_id = $ing_cheque['data']['tb_empresa_id'];
                    $oIngreso->ingreso_fecdep = '';
                    $oIngreso->ingreso_numope = '';
                    $oIngreso->ingreso_mondep = 0;
                    $oIngreso->ingreso_comi = 0;
                    $oIngreso->cuentadeposito_id = 0;
                    $oIngreso->banco_id = 0;
                    $oIngreso->ingreso_ap = 0;
                    $oIngreso->ingreso_detex = '';
                    $oIngreso->chequedetalle_id = $ing_cheque['data']['tb_chequedetalle_id'];
                  
                    $registro_cheque=$oIngreso->insertarChequeProceso();
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  /* ANTHONY 2024-12-05 */

	$fecha_hoy = date('Y-m-d');
	$usuario_cierre = intval($_SESSION['usuario_id']);
  $observacion = (empty($_POST["comentario"])) ? $observacion : trim($_SESSION['usuario_nom']) .": ". $_POST["comentario"] . ' | ' .date('h:i a');

	$oCajaoperacion->cerrar_caja($fecha_hoy, $usuario_cierre, $observacion,$empresa_id);
	
	$data['estado'] = 1;
	$data['cajope_msj'] = 'Se ha cerrado la caja de manera adecuada. Caja de: '.date('d-m-Y');

	echo json_encode($data);
}

if($_POST['action']=="imagen"){
  $uploadDir = '../../files/caja_cierres/';

  // Set the allowed file extensions
  //$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions
  $fileTypes = array('jpg', 'png', 'jpeg');

  $verifyToken = md5('unique_salt' . $_POST['timestamp']);

  if (!empty($_FILES) && $_POST['token'] == $verifyToken)
  {
    $tempFile   = $_FILES['Filedata']['tmp_name'];
    //$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;

    $nombre=$_FILES['Filedata']['name'];

    $fileParts = pathinfo($_FILES['Filedata']['name']);

    // inserta
    $nuevo_nombre = 'cajacierre_'.$_POST['cajaoperacion_id'].'.'.$fileParts['extension'];
    $nuevo_nombre = strtolower($nuevo_nombre);
    //$targetFile = $uploadDir . $_FILES['Filedata']['name'];
    $targetFile = $uploadDir.$nuevo_nombre;

    // Validate the filetype
    if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

      //crear carpeta si no existe
      //mkdir(str_replace('//','/',$uploadDir), 0755, true);
      
      // Save the file
      $oCajaoperacion->modificar_campo($_POST['cajaoperacion_id'], 'tb_cajaoperacion_img', $targetFile,'STR');
      
      move_uploaded_file($tempFile, $targetFile);

      echo '1';

    } else {

      // The file type wasn't allowed
      echo 'Tipo de archivo no permitido.';

    }
  }
}

if($_POST['action'] == 'img_subida'){
  $cajaoperacion_id = intval($_POST['cajaoperacion_id']);

  if($cajaoperacion_id){
    $dts = $oCajaoperacion->mostrarUno($cajaoperacion_id);
      if($dts['estado']==1){
      $img = $dts['data']['tb_cajaoperacion_img'];
    }

    echo '
      <center>
        <a href="'.$img.'" target="_blank"><img src="'.$img.'" width="70%" height="70%"/></a>
      </center>
    ';
  }
  else{
    echo 'Faltan datos, '.$cajaoperacion_id;
  }
}

if($_POST['action']=="eliminar")
{
	if(!empty($_POST['cajope_id']))
	{
		/*$cst1 = $oCajaoperacion->verifica_cajaoperacion_tabla($_POST['cajope_id'],'tb_misa');
		$rst1= mysql_num_rows($cst1);
		if($rst1>0)$msj1=' - Misa';
		
		if($rst1>0)
		{
			echo "No se puede eliminar, afecta información de: ".$msj1.".";
		}
		else
		{*/
			$oCajaoperacion->modificar_campo($_POST['cajope_id'],'tb_cajaoperacion_xac','0','INT');
			echo 'Se envió a la papelera correctamente.';
		//}
	}
	else
	{
		echo 'Intentelo nuevamente.';
	}
}
?>