$(document).ready(function() {
	jQuery('#wpf-loader-two').delay(200).fadeOut('slow'); 

	console.log('corriendo princi 44');
	$('#modal_asistencia').modal('show');

  contratos_vencidos();

  vendedor_progreso();

  $('#principal_mes, #principal_anio, #principal_usuario_id, #principal_empresa_id').change(function() {
    vendedor_progreso();
  })

  $('#datetimepickerECD1, #datetimepickerECD2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //startDate: "-0d"
    //endDate : new Date()
  });
  
  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });

  $("#datetimepickerECD1").on("change", function (e) {
    var startVal = $('#txt_fil_ecd_1').val();
    $('#datetimepickerECD2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepickerECD2").on("change", function (e) {
    var endVal = $('#txt_fil_ecd_2').val();
    $('#datetimepickerECD1').data('datepicker').setEndDate(endVal);
  });

  reporte_estado_cuota_cliente_tabla();

  $('#txt_fil_ecd_1, #txt_fil_ecd_2, #cbo_estado, #cbo_credtipo, #cmb_usuario_id').change(function() {
    reporte_estado_cuota_cliente_tabla();
  });

  usuario_select(0);

  $("#cmb_usuario_id").selectpicker('refresh');
  
  $('#cmb_sede_id_recc').change(function() {
    usuario_select(-1);
  });

  // $('#form_opcionesgc_asignar').validate({
  //   submitHandler: function () {
    
  //     $.confirm({
  //       icon: 'fa fa-question',
  //       title: 'Advertencia',
  //       content: '¿Está seguro guardar los datos?',
  //       type: 'blue',
  //       theme: 'material', // 'material', 'bootstrap'
  //       typeAnimated: true,
  //       buttons: {
  //         si: function () {

  //           alert('listo para registrar');
  //           return 0;
  //           $.ajax({
  //             type: "POST",
  //             url: VISTA_URL + "reportecreditomejorado/opcionesgc_asignar_controller.php",
  //             async: true,
  //             dataType: "json",
  //             data: $("#form_opcionesgc_asignar").serialize(),
  //             beforeSend: function () {
                
  //             },
  //             success: function (data) {
  //               if (parseInt(data.estado) > 0) {
  //                 swal_success('Genial!', data.mensaje, 3000)
  //                 $('#modal_registro_asignaropciongc').modal('hide');
      
  //                 //retorna los valores de gestión ingresados, para asiganarlo a los td
  //                 $.each(data.cuotadetalle_id, function(index, value) {
  //                   $('#td_opciones_' + value).text(data.td_opciones);
  //                   $('#td_comentario_' + value).text(data.td_comentario);
  //                 });
      
  //                 console.log(data)
  //               } 
  //               else {
  //                 alerta_warning('AVISO', 'Por favor tener en cuenta: ' + data.mensaje)
  //               }
  //             },
  //             complete: function (data) {
  //                 //console.log(data);
  //             },
  //             error: function (data) {
  //               alerta_error('Error', data.responseText);
  //               console.log(data)
  //             }
  //           });
  //         },
  //         no: function () {}
  //       }
  //     });

      

  //   },
  //   rules: {
  //     cmb_opcionesgc_id: {
  //       required: true,
  //       min: 1
  //     }
  //   },
  //   messages: {
  //     cmb_opcionesgc_id: {
  //       required: "Selecciona una opción de gestión para guardar",
  //       min: "Selecciona una opción de gestión para guardar"
  //     }
  //   },
  //   errorElement: "em",
  //   errorPlacement: function (error, element) {
  //       // Add the `help-block` class to the error element
  //       error.addClass("help-block");
  //       if (element.prop("type") === "checkbox") {
  //           error.insertAfter(element.parent("label"));
  //       } else {
  //           error.insertAfter(element);
  //       }
  //   },
  //   highlight: function (element, errorClass, validClass) {
  //       $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
  //   },
  //   unhighlight: function (element, errorClass, validClass) {
  //       $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
  //   }
  // });

});

//juan 18-08-2023
function vendedor_progreso() {
  empresa_id = $('#principal_empresa_id').val();
  usuario_id = $('#principal_usuario_id').val();
  mes = $('#principal_mes').val();
  anio = $('#principal_anio').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "principal/vendedor_progreso.php",
    async: true,
    dataType: "html",
    data: {
      empresa_id: empresa_id,
      usuario_id: usuario_id,
      mes: mes,
      anio: anio,
    },
    beforeSend: function () {},
    success: function (data) {
      $("#vendedor_progreso").html(data);
    },
    complete: function (data) {},
  });
}
function creditos_no_vigentes() {
  empresa_id = $('#principal_empresa_id').val();
  usuario_id = $('#principal_usuario_id').val();
  mes = $('#principal_mes').val();
  anio = $('#principal_anio').val();

  console.log('aquiiiiiiii  111')
  $.ajax({
    type: "POST",
    url: VISTA_URL + "principal/principal_creditos_no_vigentes.php",
    async: true,
    dataType: "html",
    data: {
      empresa_id: empresa_id,
      usuario_id: usuario_id,
      mes: mes,
      anio: anio,
    },
    beforeSend: function () {},
    success: function (data) {
      $("#div_modal_creditos_no_vigentes").html(data);
      $('#modal_creditos_no_vigentes').modal('show');
    },
    complete: function (data) {},
  });
}

function marcar_asistencia(act, tip){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"asistencia/asistencia_reg.php",
      async:true,
      dataType: "json",                      
      data: ({
        action: act,
        tipo: tip,
        asistencia_obs: $('#txt_asistencia_obs').val()
      }),
      beforeSend: function() {

      },
      success: function(data){
        if(parseInt(data.estado) == 1){
          swal_success("ASISTENCIA",data.msj, 3000);
          $('#modal_asistencia').modal('hide');
          setInterval(location.reload(),5000);
        }
        else
          swal_error("SISTEMA",data.msj,5000);
      },
      complete: function(data){
        if(data.status != 200){
          console.log(data);
          swal_error("SISTEMA",data,5000);
        }
      }
    });
  }

/* GERSON (21-04-23) */
function contratos_vencidos(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"personalcontrato/personalcontrato_controller.php",
    async:true,
    dataType: "json",                      
    data: ({
      action: 'contratos_vencidos',
    }),
    beforeSend: function() {

    },
    success: function(data){

      // CONTRATOS POR VENCER
      
      const createToastX = (idx) => {
        const {id, colaborador, fin_contrato, vencimiento} = data.por_vencer[idx];
        const toastx = document.createElement("li");
        toastx.className = `toast toast_${id} bg-yellow`;
        toastx.innerHTML = `<div class="col-2">
                              <i class="fa fa-lg fa-exclamation-triangle" style="color: #000000"></i>
                            </div>
                            <div align="left" class="col-10">
                              <div align="left" class="col-12">
                                <span class="h5">${colaborador}</span>
                              </div>
                              <div align="left" class="col-12">
                                <p>Contrato por vencer en <strong>${vencimiento}</strong> día(s).</p>
                              </div>
                            </div>
                            <i class="fa fa-close" onclick="removeToast(this.parentElement)"></i>`;
        notifications.appendChild(toastx);
        //toast.timeoutId = setTimeout(() => removeToast(), toastDetails.timer);
      };

      let i = 0;
      for (x of data.por_vencer) {
        createToastX(i);
        i++;
      }

      // CONTRATOS VENCIDOS
    
      const createToastY = (idy) => {
        const {id, colaborador, fin_contrato, vencimiento} = data.vencidos[idy];
        const toasty = document.createElement("li");
        toasty.className = `toast toast_${id} bg-red`;
        toasty.innerHTML = `<div class="col-2">
                              <i class="fa fa-lg fa-exclamation-circle" style="color: #000000"></i>
                            </div>
                            <div align="left" class="col-10">
                              <div align="left" class="col-12">
                                <span class="h5">${colaborador}</span>
                              </div>
                              <div align="left" class="col-12">
                                <p>Contrato vencido hace <strong>${vencimiento}</strong> día(s).</p>
                              </div>
                            </div>
                            <i class="fa fa-close" onclick="removeToast(this.parentElement)"></i>`;
        notifications.appendChild(toasty);
        //toast.timeoutId = setTimeout(() => removeToast(), toastDetails.timer);
      };

      let j = 0;
      for (y of data.vencidos) {
        createToastY(j);
        j++;
      }
      
    },
    complete: function(data){
      /* if(data.status != 200){
        console.log(data);
        swal_error("SISTEMA",data,5000);
      } */
    }
  });
}

const notifications = document.querySelector(".notifications");
buttons = document.querySelectorAll(".buttons .btn");

const toastDetails = {
  success: {
    icon: "fa fa-check-circle",
    text: "Success"
  },
  info: {
    icon: "fa fa-exclamation-circle",
    text: "Info"
  },
  warning: {
    icon: "fa fa-exclamation-triangle",
    text: "Warning"
  },
  error: {
    icon: "fa fa-times-circle",
    text: "Error"
  },
}
const removeToast = (toast) => {
  toast.classList.add('hide');
  if(toast.timeoutId) clearTimeout(toast.timeoutId);
};
/* const createToast = (id) => {
  const {icon, text} = toastDetails[id];
  const toast = document.createElement("li");
  toast.className = `toast ${id}`;
  toast.innerHTML = `<div class="column">
                        <i class="${icon}"></i>
                        <span>${text}</span>
                      </div>
                      <i class="fa fa-close" onclick="removeToast(this.parentElement)"></i>`;
  notifications.appendChild(toast);
  toast.timeoutId = setTimeout(() => removeToast(), toastDetails.timer);
}; */
buttons.forEach(btn => {
  btn.addEventListener("click", () => createToast(btn.id));
});
/*  */

// ------------------------------------------------------------------------- //

function reporte_estado_cuota_cliente_tabla(){

  $('#tblcuotacliente').empty();

  var fecha1 = $('#txt_fil_ecd_1').val();
  var fecha2 = $('#txt_fil_ecd_2').val();
  var estado = $('#cbo_estado').val();
  var credtipo = $('#cbo_credtipo').val();
  var usuario = $('#cmb_usuario_id').val();

  if( fecha1 == null )
  {
    fecha1 = '0';
  }

  if( fecha2 == null )
  {
    fecha2 = '0';
  }

  if( estado == null )
  {
    estado = '0';
  }

  if( credtipo == null )
  {
    credtipo = '0';
  }

  if( usuario == null || $.trim(usuario) == "" )
  {
    usuario = '0';
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"principal/estado_cuota_cliente_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      p_fecha1: fecha1,
      p_fecha2: fecha2,
      p_estado: estado,
      p_credtipo: credtipo,
      p_usuario: usuario
    }),
    beforeSend: function() {
      $('#reporte_ecd_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#tblcuotacliente').html(data);
      $('#reporte_ecd_mensaje_tbl').hide(300);
      estilos_datatable('tbl_reporte_estado_cuota_cliente');
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#reporte_ecd_mensaje_tbl').html('ERROR: ' + data.responseText);
    }
  });
}


function cliente_form2(usuario_act, cliente_id) {
  // var credito_cliente_id = Number($("#hdd_cre_cli_id").val());
  // console.log(credito_cliente_id);

  if (parseInt(cliente_id) <= 0 && usuario_act == "M") {
    alerta_warning("Información", "Búsqueda inválida");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: cliente_id,
      vista: "principal",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "principal";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {
      console.log("busqueda culminado correctamente");
    },
    error: function (data) {
      alerta_error("ERROR", data.responseText);
      console.log(data);
    },
  });
}

function usuario_select(id) {
  var sede_id = $("#cmb_sede_id_recc").val();
  $("#cmb_usuario_id").empty();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "/usuario/usuario_select.php",
    async: true,
    dataType: "html",
    data: {
      usuario_columna: 'tb_empresa_id',
      param_tip:'INT',
      usuario_valor: sede_id
    },
    beforeSend: function () {
      $("#cmb_usuario_id").val("<option>' cargando '</option>");
    },
    success: function (data) {
      $("#cmb_usuario_id").empty();
      $("#cmb_usuario_id").html(data);      
      $("#cmb_usuario_id").selectpicker('refresh');
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      $("#creditogarveh_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CANCHA: " + data.responseText
      );
    },
  });
}

function opcionesgc_asignar_form(usuario_act, cliente_id, credito_id, temp_cobranza_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"principal/fragment_consultar_datos_cliente.php",
    async: true,
		dataType: "html",
		data: ({
      p_action: usuario_act, // PUEDE SER: L, I, M , E
      p_cliente_id: cliente_id,
      p_credito_id: credito_id,
      p_temp_cobranza_id: temp_cobranza_id,
      p_modulo_nombre: 'principal'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_asignar_form').html(data);
      	$('#modal_registro_asignaropciongc').modal('show');
        modal_height_auto('modal_registro_asignaropciongc');
      	modal_hidden_bs_modal('modal_registro_asignaropciongc', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}