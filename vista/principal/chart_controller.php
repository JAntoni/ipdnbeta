<?php
	session_name("ipdnsac");
        session_start();

 	require_once('Chart.class.php');
        $oChart = new Chart();
        require_once ("../funciones/funciones.php");
        require_once ("../funciones/fechas.php");

        $fecha1 = fecha_mysql($_POST['fecha1']);
        $fecha2 = fecha_mysql($_POST['fecha2']);
        $año= intval($_POST['año']);
        $sede= intval($_POST['sede']);
        $grafico = $_POST['grafico'];
        
        if($grafico=="piechart"){
            $array = array();
            $result = $oChart->listarcreditosporsede($fecha1,$fecha2);
            if($result['estado'] == 1){
                 foreach ($result['data'] as $key => $value) {
                     $array_temporal = array();
                     array_push($array_temporal,$value['sede']);
                     array_push($array_temporal,$value['creditos']);
                     array_push($array,$array_temporal);
                 }

                echo json_encode($array);
            }
            $result="";
        }
        elseif($grafico=="chart_div"){
            $array = array();
            $result = $oChart->listarreportegeneral($sede,$fecha1,$fecha2);
            if($result['estado'] == 1){
                 foreach ($result['data'] as $key => $value) {
                     $array_temporal = array();
                     array_push($array_temporal,$value['Vendedor']);
                     array_push($array_temporal,$value['creditos']);
                     array_push($array_temporal,$value['monto']);
                     array_push($array,$array_temporal);
                 }

                echo json_encode($array);
            }
            $result="";
        }
        elseif($grafico=="chart_divaño"){
            $array = array();
            $result = $oChart->listarreporteporaño($sede, $año);
            if($result['estado'] == 1){
                 foreach ($result['data'] as $key => $value) {
                     if($value['tb_empresa_id']==1){
                         $sede = 'B';
                     }
                     else{
                         $sede = 'M';
                     }
                     $array_temporal = array();
                     array_push($array_temporal,$value['nombremes']);
                     array_push($array_temporal,$value['creditos']);
                     array_push($array_temporal,$value['monto']);
                     array_push($array,$array_temporal);
                 }

                echo json_encode($array);
            }
            $result="";
        }
        
      
?>