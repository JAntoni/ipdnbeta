<?php 
?>
<form id="form_charaño_filtro" class="form-inline" role="form">
    <div class="form-group" style="margin-left:9px">
        <label for="cmb_año" class="control-label">AÑO : </label>
        <select class="form-control form-control-sm" id="cmb_año" name="cmb_año">
            <?php echo devuelve_option_anios(date('Y'), 4);?>
        </select>
    </div>
    <div class="form-group" style="margin-left:9px">
        <label for="cmb_sede_idaño" class="control-label">SEDE : </label>
        <select class="form-control form-control-sm" id="cmb_sede_idaño" name="cmb_sede_idaño">
            <option value="0">TODAS</option>
            <option value="1" selected>IPDN - BOULEVARD</option>
            <option value="2">IPDN - MALL AVENTURA</option>
        </select>
    </div>
    <div class="form-group" style="margin-left:9px">
        <a href="javascript:void(0)" onClick="cargar_datachartaño()" class="btn btn-success btn-sm">Buscar</a>
  </div>

</form>