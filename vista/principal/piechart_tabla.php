<?php  
  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");
  require_once('Chart.class.php');
  $oChart = new Chart();
  $fecha1 = fecha_mysql($_POST['fecha1']);
  $fecha2 = fecha_mysql($_POST['fecha2']);
  
  $result = $oChart->listarvaloresporsede($fecha1,$fecha2);

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {

      $tr .='<tr>';
        $tr.='
          <td>'.$value['sede'].'</td>
          <td>'.$value['creditos'].'</td>
          <td>'.mostrar_moneda($value['monto']).'</td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_piecharts" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>SEDE</th>
      <th>CREDITOS</th>
      <th>MONTO</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
