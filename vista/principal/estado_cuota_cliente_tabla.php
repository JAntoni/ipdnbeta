
<?php  
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  require_once('Chart.class.php');  
  $oChart = new Chart();

  $fecha1 = fecha_mysql($_POST['p_fecha1']);
  $fecha2 = fecha_mysql($_POST['p_fecha2']);
  $estado = $_POST['p_estado'];
  $credtipo = $_POST['p_credtipo'];
  
  if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuarioperfil_id'] == 5 )
  {
    $usuario = $_POST['p_usuario'];
  }
  else
  {
    $usuario = $_SESSION['usuario_id'];
  }
  
  // echo 'Usuario detectado: '.$_SESSION['usuarioperfil_id'].' - '.$usuario.'<br>';
  $result = $oChart->listar_estado_cuota_cliente($fecha1,$fecha2,$estado,$usuario,$credtipo);

  $addDias = ' día';
  $colorDifDias = '';
  $descrDifDias = '';
  $tipoCredito = '';
  $dsc_cliente = '';
  $dsc_estado = '';
  $tr = '';
  $addColumn = '';
  $data_usuario = '';

  $idTempCobra = 0;
  $addBtnWhatsApps = '';

  if($result['estado'] == 1){

    foreach ($result['data'] as $key => $value) {
      $numeroCuota          = $value['tb_cuota_num'];
      $montoCuota           = $value['monto_cuota'];
      $data_diferencia_dias = $value['diferencia_dias'];
      $data_creditotipo_id  = $value['tipo_credito_id'];
      $data_usuario         = $value['tb_usuario_nom'].' '.$value['tb_usuario_ape'];
      $data_estado          = $value['estado_cuota'];
      $data_userfoto        = $value['tb_usuario_fot'];
      
      if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuarioperfil_id'] == 5 ) { $addColumn = '<td style="vertical-align: middle;" class="text-left"> <img src="'.$data_userfoto.'" class="img-circle" alt="img-user-tbl-estado-cuota" width="30"> &nbsp; '.$data_usuario.'</td>'; }
      if($value['tb_moneda_id'] == 1){ $dsc_moneda = "S/";}else{ $dsc_moneda = "US$"; }
      
      if( $data_diferencia_dias != 1 ){ $addDias = ' días';}

      if( $data_diferencia_dias >= 16 )
      {
        $colorDifDias = 'bg-info text-blue';
        $descrDifDias = '<i class="fa fa-info-circle fa-fw"></i> Faltan '. $data_diferencia_dias;

        if( $data_estado == 1 )
        {
          $dsc_estado = '<span class="text-blue">Pendiente</span>';
        }
        elseif( $data_estado == 2 )
        {
          $colorDifDias = 'bg-success text-green';
          $descrDifDias = '<i class="fa fa-check-circle fa-fw"></i> '. 0;
          $addDias = ' días';
          $dsc_estado = '<span class="text-green">Pagado</span>';
        }
        elseif( $data_estado == 3 )
        {
          $dsc_estado = '<span class="text-blue">Pago Parcial</span>';
        }
        else
        {
          $dsc_estado = 'REPORTAR :: '.$data_estado;
        }
      }
      elseif( $data_diferencia_dias >= -15 && $data_diferencia_dias <= 15 )
      {
        $colorDifDias = 'bg-warning text-orange';
        $descrDifDias = '<i class="fa fa-warning fa-fw"></i> Hace '. abs($data_diferencia_dias);

        if( $data_estado == 1 )
        {
          $dsc_estado = '<span class="text-orange">Pago Pendiente</span>';
        }
        elseif( $data_estado == 2 )
        {
          $colorDifDias = 'bg-success text-green';
          $descrDifDias = '<i class="fa fa-check-circle fa-fw"></i> '. 0;
          $addDias = ' días';
          $dsc_estado = '<span class="text-green">Pagado</span>';
        }
        elseif( $data_estado == 3 )
        {
          $dsc_estado = '<span class="text-blue">Pago Parcial</span>';
        }
        else
        {
          $dsc_estado = 'REPORTAR :: '.$data_estado;
        }
      }
      else
      {
        $colorDifDias = 'bg-danger text-red';
        $descrDifDias = '<i class="fa fa-warning fa-fw"></i> Hace '. abs($data_diferencia_dias);

        if( $data_estado == 1 )
        {
          $dsc_estado = '<span class="text-red">Pago Pendiente</span>';
        }
        elseif( $data_estado == 2 )
        {
          $colorDifDias = 'bg-success text-green';
          $descrDifDias = '<i class="fa fa-check-circle fa-fw"></i> '. 0;
          $addDias = ' días';
          $dsc_estado = '<span class="text-green">Pagado</span>';
        }
        elseif( $data_estado == 3 )
        {
          $dsc_estado = '<span class="text-blue">Pago Parcial</span>';
        }
        else
        {
          $dsc_estado = 'REPORTAR :: '.$data_estado;
        }
      }

      switch ($data_creditotipo_id) {
        case 1:
          $tipoCredito = 'Cred. Menor';
          break;
        case 2:
          $tipoCredito = 'Asistencia Vehicular';
          break;
        case 3:
          $tipoCredito = 'Garantía Vehicular';
          break;
        case 3:
          $tipoCredito = 'Hipotecario';
          break;
        default:
          $tipoCredito = $data_creditotipo_id . ' -- Reportar al A. Sistemas';
          break;
      }

      if( strlen($value['tb_cliente_tip']) == 1 ){ $dsc_cliente = $value['tb_cliente_doc'].' - '.$value['tb_cliente_nom']; } else { $dsc_cliente = $value['tb_cliente_doc'].' - '.$value['tb_cliente_emprs']; }

      $consultarDatosTempCobranza = $oChart->retornarIdTempCobranzaPorCredito($value['tb_credito_id']);
      if( $consultarDatosTempCobranza['estado'] == 1 ) { $idTempCobra = $consultarDatosTempCobranza['data']['temp_cobranza_id']; } else { $idTempCobra = 0; }

      if( $idTempCobra != 0 ) { $addBtnWhatsApps = '<button type="button" class="btn btn-info btn-xs" onclick=\'opcionesgc_asignar_form("L",'.$value['tb_cliente_id'].','.$value['tb_credito_id'].','.$idTempCobra.')\'> <i class="fa fa-phone"></i></button>'; }

      $tr .='<tr>';
        $tr.='
          <td style="vertical-align: middle;" class="text-center">'.$value['tb_credito_id'].'</td> 
          <td style="vertical-align: middle;" class="text-center">'.$tipoCredito.'</td>
          <td style="vertical-align: middle;"> '.$addBtnWhatsApps.' &nbsp; <a class="text-black" style="cursor: pointer;" onclick=\'opcionesgc_asignar_form("L",'.$value['tb_cliente_id'].')\'>'.$dsc_cliente.'</a> </td>
          <td style="vertical-align: middle;" class="text-center">'.$numeroCuota.'</td>
          <td style="vertical-align: middle;" class="text-center">'.mostrar_fecha($value['fecha_pago']).'</td>
          <td style="vertical-align: middle;" class="text-center '.$colorDifDias.'">'.$descrDifDias.$addDias.'</td>
          <td style="vertical-align: middle;" class="text-center">'.$dsc_estado.'</td>
          <td style="vertical-align: middle;" class="text-center">'.$dsc_moneda.'</td>
          <td style="vertical-align: middle;" class="text-right">'.mostrar_moneda($montoCuota).'</td>          
        '.$addColumn;
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }
?>
<div class="table-responsive">
  <table id="tbl_reporte_estado_cuota_cliente" class="table table-bordeless table-hover">
    <thead>
      <tr class="bg-primary">
        <th class="text-center">CREDITO ID</th>
        <th class="text-center">TIPO DE CREDITO</th>
        <th class="text-center">CLIENTE</th>
        <th class="text-center">N° CUOTA</th>
        <th class="text-center">FECHA DE CUOTA</th>
        <th class="text-center">DÍAS VENCIDOS</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">MONEDA</th>
        <th class="text-center">MONTO</th>
        <?php if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuarioperfil_id'] == 5 ) { echo '<th class="text-center">USUARIO</th>'; } ?>
      </tr>
    </thead>
    <tbody>
      <?php echo $tr;?>
    </tbody>
  </table>
</div>