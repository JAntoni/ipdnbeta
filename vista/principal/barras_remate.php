<?php
  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");
  require_once('Chart.class.php');
  $oChart = new Chart();

  $fecha1 = fecha_mysql($_POST['fecha1']);
  $fecha2 = fecha_mysql($_POST['fecha2']);
  $sede= intval($_POST['sede']);
  
  $result = $oChart->listar_reporte_venta_garantias($sede,$fecha1,$fecha2, 0); //0 para indicar que debe filtrar de todos los usuarios

  $total_vendido = 0;
  $tr = '';
  if($result['estado'] == 1){
      $n = 0;
    foreach ($result['data'] as $key => $value) {
        $n ++;
        if($n == 1){
            $estilos = "background:#3BF453;";
        }
        elseif ($n == 2) {
            $estilos = "background:#F5F33C;";
        }else{
            $estilos = "";
        }
      $tr .='<tr style="'.$estilos.'">';
        $tr.='
          <!--td class="top'.$n.'">'.$value['sede'].'</td -->
          <td>'.$value['tb_usuario_nom'].'</td>
          <td>'.$value['cantidad_ventas'].'</td>
          <td>'.mostrar_moneda($value['total_vendido']).'</td> 
        ';
      $tr.='</tr>';

      $total_vendido += formato_numero($value['total_vendido']);
    }
    $tr .='<tr style="'.$estilos.'"><td colspan="2"><b>TOTAL VENDIDO DEL '.$_POST['fecha1'].' AL '.$_POST['fecha2'].'</b></td> <td><b>'.mostrar_moneda($total_vendido).'</b></td></tr>';
    $result = null;
  }
  else {
    $tr = $result['mensaje'];
    $result = null;
  }

?>
<label style="font-family: cambria;font-weight: bolder;color: #0000FF;font-size: 18px">REMATES</label>
<table id="tbl_piecharts" class="table table-bordered table-hover">
  <thead>
    <tr>
      <!--th>SEDE</th-->
      <th>VENDEDOR</th>
      <th>REMATES</th>
      <th>MONTO</th>
      <!--th>COMISION</th-->
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
