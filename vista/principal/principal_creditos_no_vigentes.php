<?php
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../usuarioperfil/UsuarioPerfil.class.php');
$oUsuarioPerfil = new UsuarioPerfil();
require_once('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();
require_once('../principal/Chart.class.php');
$oChart = new Chart();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$empresa_id = intval($_POST['empresa_id']);
$usuario_id = intval($_POST['usuario_id']);
$mes = $_POST['mes'];
$anio = $_POST['anio'];

$fecha_fin = fecha_mysql('01-' . $mes . '-' . $anio);
$dateTime = new DateTime($fecha_fin);

$fecha_inicio = fecha_mysql('01-' . $mes . '-' . $anio);
$fecha_fin = $dateTime->format('Y-m-t');

$tabla = '';
$result = $oChart->listar_creditos_asiveh_garveh_hipo_no_vigentes($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $sub_tipo = $value['credito_subtipo'];
      if($value['credito_general'] == 3){
        if($value['custodia'] == 1) $sub_tipo .= ' CON CUSTODIA';
        if($value['custodia'] == 2) $sub_tipo .= ' SIN CUSTODIA';
      }

      $tipo_credito = '';
      if($value['tipo_credito'] == 2)
        $tipo_credito = '<b>ADENDA</b> ';

      $credito = '<span class="badge bg-yellow">sin credito</span>';
      if($value['credito_general'] == 2) $credito = '<span class="badge bg-yellow">ASISTENCIA VEHICULAR</span>';
      if($value['credito_general'] == 3) $credito = '<span class="badge bg-aqua">GARANTIA VEHICULAR</span>';
      if($value['credito_general'] == 4) $credito = '<span class="badge bg-purple">HIPOTECARIO</span>';

      $tabla .= '
        <tr>
          <td>'.$value['tb_credito_id'].'</td>
          <td>'.mostrar_fecha($value['fecha_aprobado']).'</td>
          <td>'.$value['tb_cliente_nom'].'</td>
          <td>'.$value['tb_usuario_nom'].'</td>
          <td>'.mostrar_moneda($value['prestamo_soles']).'</td>
          <td>'.$tipo_credito.$sub_tipo.'</td>
          <td>'.$credito.'</td>
        </tr>
      ';
    }
  }
$result = NULL;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditos_no_vigentes" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">LISTA DE CREDITOS QUE AÚN NO ESTÁN VIGENTES</h4>
      </div>
      <div class="modal-body">
          <table class="table table-striped table-bordered">
            <tr>
              <th>Credito ID</th>
              <th>Fecha Aprobado</th>
              <th>Cliente</th>
              <th>Asesor</th>
              <th>Monto Prestado S/.</th>
              <th>Subtipo de Crédito</th>
              <th>Tipo de Crédito</th>
            </tr>
            <?php  echo $tabla;?>
          </table>
      </div>
      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>

