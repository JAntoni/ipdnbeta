<?php 
  $fecha = new DateTime();
  $fecha->modify('last day of this month');

  $chart_fec1 = date('01-m-Y');
  $chart_fec2 = $fecha->format('d-m-Y');
?>
<form id="form_chart_filtro" class="form-inline" role="form">
  <div class="form-group">
    <div class='input-group date' id='datetimepicker1'>
      <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $chart_fec1;?>"/>
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
  </div>
  <div class="form-group">
    <div class='input-group date' id='datetimepicker2'>
      <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $chart_fec2;?>"/>
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
  </div>
    <div class="form-group" style="margin-left:9px">
    <label for="cmb_sede_id" class="control-label">SEDE : </label>
    <select class="form-control form-control-sm" id="cmb_sede_id" name="cmb_sede_id">
        <option value="0">SEDES</option>
        <option value="1">IPDN - BOULEVARD</option>
        <option value="2">IPDN - MALL AVENTURA</option>
    </select>
  </div>
    <div class="form-group" style="margin-left:9px">
        <a href="javascript:void(0)" onClick="cargargraficos()" class="btn btn-success btn-sm">Buscar</a>
  </div>

</form>