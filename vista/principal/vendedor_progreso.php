<?php
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../usuarioperfil/UsuarioPerfil.class.php');
$oUsuarioPerfil = new UsuarioPerfil();
require_once ('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();
require_once('../principal/Chart.class.php');
$oChart = new Chart();
require_once('../planilla/Planilla.class.php');
$oPlanilla = new Planilla();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$empresa_id = intval($_POST['empresa_id']); 
$usuario_id = intval($_POST['usuario_id']);
$mes = $_POST['mes'];
$anio = $_POST['anio'];

$fecha_fin = fecha_mysql('01-'.$mes.'-'.$anio);
$dateTime = new DateTime($fecha_fin);

$fecha_inicio = fecha_mysql('01-'.$mes.'-'.$anio);
$fecha_fin = $dateTime->format('Y-m-t');

$dateTime = new DateTime();
$ultimoDiaMes = intval($dateTime->format('t'));
$dias = '('.$ultimoDiaMes.' días)';
$numero_dia_hoy = intval(date('d'));

$tiene_metas = 'NO';
$metacredito_creditos = 0;
$metacredito_cmenor = 0;
$metacredito_ventacm = 0;
$metacredito_ventaveh = 0;

$equivalente_dia_creditos = 0;
$equivalente_dia_cmenor = 0;
$equivalente_dia_ventacm = 0;

$total_creditos_faltantes = 0;
$faltantes_vigentes = 0;
$total_logrado_creditos = 0;
$total_logrado_cmenor = 0;
$total_logrado_ventacm = 0;

$porcentaje_creditos = 0;
$porcentaje_cmenor = 0;
$porcentaje_ventacm = 0;

$metas = '';//$empresa_id.' / '.$usuario_id .' / '.$mes.' / '.$anio;
$barras = '';
$resumen = '';

//? AHORA DEBEMOS BUSCAR LA META DEL ASESOR O HACER LA SUMA DE LAS METAS TOTALES POR TODAS LAS SEDES POR SEDE INDEPENDIENTE
$result6 = $oMetacredito->sumar_metacredito_usuario_sede($mes, $anio, $usuario_id, $empresa_id);
  if($result6['estado'] == 1){
    $metacredito_creditos = $result6['data']['metacredito_creditos'];
    $metacredito_cmenor = $result6['data']['metacredito_cmenor'];
    $metacredito_ventacm = $result6['data']['metacredito_ventacm'];
    $metacredito_ventaveh = $result6['data']['metacredito_ventaveh'];

    $tiene_metas = 'SI';
  }
$result6 = NULL;

if($metacredito_creditos > 0){
  //? CONSULTAMOS LOS CRÉDITOS QUE ESTÁN EN PROGRESO A VIGENTES, PERO PUEDEN ESTAR EN APROBADO, DOCUMENTOS O POR TITULO, PARA PODER MEDIR LOS TIEMPOS
  $result = $oChart->listar_creditos_asiveh_garveh_hipo_no_vigentes($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $total_creditos_faltantes += $value['prestamo_soles'];
      }
    }
  $result = NULL;
  $detalle_mos = '';
  //? SI TIENE METAS DE CREDITOS GENERALES: ASIVEH, GARVEH, HIPO, CONSULTAMOS
  $result = $oChart->listar_creditos_asiveh_garveh_hipo($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $total_logrado_creditos += $value['prestamo_soles'];
        //$detalle_mos .= ' / ID: '.$value['tb_credito_id'].'('.$value['prestamo_soles'].') ';
      }
    }
  $result = NULL;
  
  $faltantes_vigentes = $total_creditos_faltantes + $total_logrado_creditos;

  $equivalente_dia_creditos = formato_numero($metacredito_creditos / $ultimoDiaMes);
  $porc_alcanzado_diario = 0;

  if($total_logrado_creditos > 0){
    $porcentaje_creditos = $total_logrado_creditos * 100 / $metacredito_creditos;
    $porc_alcanzado_diario = $total_logrado_creditos * 100 / ($numero_dia_hoy * $equivalente_dia_creditos);
  }

  $porc_faltantes = 0;
  if($total_creditos_faltantes > 0){
    $porc_faltantes = $total_creditos_faltantes * 100 / ($numero_dia_hoy * $equivalente_dia_creditos);
    $metas .= '
      <tr class="danger">
        <td><span class="badge bg-yellow" onclick="creditos_no_vigentes()" style="cursor: pointer;">Ver Créditos Faltantes</span></td>
        <td colspan="5">
          <span class="badge bg-green">Monto Vigente: '.mostrar_moneda($total_logrado_creditos).'</span>
          <span class="badge bg-red">Monto No Vigente: '.mostrar_moneda($total_creditos_faltantes).'</span>
          <span class="badge bg-purple">Suma Ambos: '.mostrar_moneda($faltantes_vigentes).'</span>
        </td>
        <td><span class="badge bg-purple">'.mostrar_moneda($porc_faltantes).' % logrado'.$detalle_mos.'</span></td>
      </tr>
    ';
  }

  $tag_logrado = '<span class="badge bg-red">'.mostrar_moneda($porcentaje_creditos).' % logrado</span>';
  $tag_barra = 'progress-bar-danger';
  $tag_diario = '<span class="badge bg-red">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';

  if($porcentaje_creditos > 50 && $porcentaje_creditos <= 85){
    $tag_logrado = '<span class="badge bg-yellow">'.mostrar_moneda($porcentaje_creditos).' % logrado</span>';
    $tag_barra = 'progress-bar-warning';
  }
  if($porcentaje_creditos > 85){
    $tag_logrado = '<span class="badge bg-green">'.mostrar_moneda($porcentaje_creditos).' % logrado</span>';
    $tag_barra = 'progress-bar-success';
  }

  if($porc_alcanzado_diario > 50 && $porc_alcanzado_diario <= 85){
    $tag_diario = '<span class="badge bg-yellow">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';
  }
  if($porc_alcanzado_diario > 85){
    $tag_diario = '<span class="badge bg-green">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';
  }

  $metas .= '
    <tr>
      <td>GARVEH + ASIVEH + HIPO</td>
      <td>'.mostrar_moneda($metacredito_creditos).'</td>
      <td>'.mostrar_moneda($equivalente_dia_creditos).'</td>
      <td>'.$numero_dia_hoy.'</td>
      <td>'.mostrar_moneda($numero_dia_hoy * $equivalente_dia_creditos).'</td>
      <td>'.mostrar_moneda($total_logrado_creditos).'</td>
      <td>'.$tag_diario.'</td>
    </tr>
  ';

  $barras .= '
    <div class="row">
      <div class="col-md-8">
        <div class="progress progress-xs progress-striped active height-bar"><div class="progress-bar '.$tag_barra.'" style="width: '.$porcentaje_creditos.'%;"></div></div>
      </div>
      <div class="col-md-2">'.$tag_logrado.'</div>
      <div class="col-md-2"><span class="badge bg-aqua">Gar-Mob</span></div>
    </div>
  ';

  $saldo = $metacredito_creditos - $faltantes_vigentes;
  if($saldo <= 0)
    $resumen .= '<span class="badge bg-purple">GarVeh e Hipo: ¡Logrado!</span>';
  else
    $resumen .= '<span class="badge bg-purple">GarVeh e Hipo: '.mostrar_moneda($saldo).'</span>';
}
if($metacredito_cmenor > 0){
  //? SI TIENE METAS EN COLOCACION DE C-MENOR, CONSULTAR LO COLOCADO EN C-MENOR
  $result = $oChart->listar_cmenor_colocacion($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $total_logrado_cmenor += $value['tb_garantia_val'];
      }
    }
  $result = NULL;

  $equivalente_dia_cmenor = formato_numero($metacredito_cmenor / $ultimoDiaMes);
  $porc_alcanzado_diario = 0;

  if($total_logrado_cmenor > 0){
    $porcentaje_cmenor = $total_logrado_cmenor * 100 / $metacredito_cmenor ;
    $porc_alcanzado_diario = $total_logrado_cmenor * 100 / ($numero_dia_hoy * $equivalente_dia_cmenor);
  }

  $tag_logrado = '<span class="badge bg-red">'.mostrar_moneda($porcentaje_cmenor).' % logrado</span>';
  $tag_barra = 'progress-bar-danger';
  $tag_diario = '<span class="badge bg-red">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';

  if($porcentaje_cmenor > 50 && $porcentaje_cmenor <= 85){
    $tag_logrado = '<span class="badge bg-yellow">'.mostrar_moneda($porcentaje_cmenor).' % logrado</span>';
    $tag_barra = 'progress-bar-warning';
  }
  if($porcentaje_cmenor > 85){
    $tag_logrado = '<span class="badge bg-green">'.mostrar_moneda($porcentaje_cmenor).' % logrado</span>';
    $tag_barra = 'progress-bar-success';
  }

  if($porc_alcanzado_diario > 50 && $porc_alcanzado_diario <= 85){
    $tag_diario = '<span class="badge bg-yellow">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';
  }
  if($porc_alcanzado_diario > 85){
    $tag_diario = '<span class="badge bg-green">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';
  }

  $metas .= '
    <tr>
      <td>COLOCACIÓN C-MENOR</td>
      <td>'.mostrar_moneda($metacredito_cmenor).'</td>
      <td>'.mostrar_moneda($equivalente_dia_cmenor).'</td>
      <td>'.$numero_dia_hoy.'</td>
      <td>'.mostrar_moneda($numero_dia_hoy * $equivalente_dia_cmenor).'</td>
      <td>'.mostrar_moneda($total_logrado_cmenor).'</td>
      <td>'.$tag_diario.'</td>
    </tr>
  ';

  $barras .= '
    <div class="row">
      <div class="col-md-8">
        <div class="progress progress-xs progress-striped active height-bar"><div class="progress-bar '.$tag_barra.'" style="width: '.$porcentaje_cmenor.'%;"></div></div>
      </div>
      <div class="col-md-2">'.$tag_logrado.'</div>
      <div class="col-md-2"><span class="badge bg-aqua">C-Menor</span></div>
    </div>
  ';

  $saldo = $metacredito_cmenor - $total_logrado_cmenor;
  if($saldo <= 0)
    $resumen .= '<span class="badge bg-purple">C - Menor: ¡Logrado!</span>';
  else
    $resumen .= '<span class="badge bg-purple">C - Menor: '.mostrar_moneda($saldo).'</span>';
}
if($metacredito_ventacm > 0){
  //? SI TIENE METAS EN VENTAS DE C-MENOR, REALIZAMOS LA CONSULTA
  $result = $oChart->listar_reporte_venta_garantias($empresa_id, $fecha_inicio, $fecha_fin, $usuario_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $total_logrado_ventacm += $value['total_vendido'];
      }
    }
  $result = NULL;

  $equivalente_dia_ventacm = formato_numero($metacredito_ventacm / $ultimoDiaMes);
  $porc_alcanzado_diario = 0;

  if($total_logrado_ventacm > 0){
    $porcentaje_ventacm = $total_logrado_ventacm * 100 / $metacredito_ventacm;
    $porc_alcanzado_diario = $total_logrado_ventacm * 100 / ($numero_dia_hoy * $equivalente_dia_ventacm);
  }

  $tag_logrado = '<span class="badge bg-red">'.mostrar_moneda($porcentaje_ventacm).' % logrado</span>';
  $tag_barra = 'progress-bar-danger';
  $tag_diario = '<span class="badge bg-red">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';

  if($porcentaje_ventacm > 50 && $porcentaje_ventacm <= 85){
    $tag_logrado = '<span class="badge bg-yellow">'.mostrar_moneda($porcentaje_ventacm).' % logrado</span>';
    $tag_barra = 'progress-bar-warning';
  }
  if($porcentaje_ventacm > 85){
    $tag_logrado = '<span class="badge bg-green">'.mostrar_moneda($porcentaje_ventacm).' % logrado</span>';
    $tag_barra = 'progress-bar-success';
  }

  if($porc_alcanzado_diario > 50 && $porc_alcanzado_diario <= 85){
    $tag_diario = '<span class="badge bg-yellow">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';
  }
  if($porc_alcanzado_diario > 85){
    $tag_diario = '<span class="badge bg-green">'.mostrar_moneda($porc_alcanzado_diario).' % logrado</span>';
  }

  $metas .= '
    <tr>
      <td>VENTA DE REMATES</td>
      <td>'.mostrar_moneda($metacredito_ventacm).'</td>
      <td>'.mostrar_moneda($equivalente_dia_ventacm).'</td>
      <td>'.$numero_dia_hoy.'</td>
      <td>'.mostrar_moneda($numero_dia_hoy * $equivalente_dia_ventacm).'</td>
      <td>'.mostrar_moneda($total_logrado_ventacm).'</td>
      <td>'.$tag_diario.'</td>
    </tr>
  ';

  $barras .= '
    <div class="row">
      <div class="col-md-8">
        <div class="progress progress-xs progress-striped active height-bar"><div class="progress-bar '.$tag_barra.'" style="width: '.$porcentaje_ventacm.'%;"></div></div>
      </div>
      <div class="col-md-2">'.$tag_logrado.'</div>
      <div class="col-md-2"><span class="badge bg-aqua">Ventas Remates</span></div>
    </div>
  ';

  $saldo = $metacredito_ventacm - $total_logrado_ventacm;
  if($saldo <= 0)
    $resumen .= '<span class="badge bg-purple">Remates: ¡Logrado!</span>';
  else
    $resumen .= '<span class="badge bg-purple">Remates: '.mostrar_moneda($saldo).'</span>';
}
if($metacredito_ventaveh > 0){
  //? LISTA DE VENTAS DE VEHICULOS DE CRÉDITOS DE (ASISTENCIA VEHICULAR)
  $dtsVentaVeh = $oPlanilla->comision_ventavehiculo($mes, $anio, $usuario_id, 2,$empresa_id); //venta contado creditos asiveh
    $rowsVentaVeh1 = count($dtsVentaVeh['data']);
  $dtsVentaVeh = NULL;

  //? LISTA DE VENTAS DE VEHICULOS DE CRÉDITOS DE (GARANTÍA VEHICULAR)
  $dtsVentaVeh = $oPlanilla->comision_ventavehiculo($mes, $anio, $usuario_id, 3,$empresa_id); //venta contado creditos GARVEH
    $rowsVentaVeh2 = count($dtsVentaVeh['data']);
  $dtsVentaVeh = null;

  $total_veh_vendidos = intval($rowsVentaVeh1) + intval($rowsVentaVeh2);

  $tag_diario = '<span class="badge bg-red">0% logrado</span>';
  if($total_veh_vendidos >= $metacredito_ventaveh)
    $tag_diario = '<span class="badge bg-green">¡Genial Logrado!</span>';

  $metas .= '
    <tr>
      <td>VENTA VEHICULOS</td>
      <td>'.$metacredito_ventaveh.'</td>
      <td></td>
      <td></td>
      <td></td>
      <td>'.$total_veh_vendidos.'</td>
      <td>'.$tag_diario.'</td>
    </tr>
  ';
}
?>

<style>
  .height-bar{
    height: 24px;
  }
</style>
<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7" style="margin-top: 20px;">
  <div class="panel shadow">
    <div class="panel-body">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Descripción de Meta</th>
          <th>Meta</th>
          <th>Meta / <?php echo $dias;?></th>
          <th>Día Hoy</th>
          <th>Meta hasta Hoy</th>
          <th>Logrado hasta Hoy</th>
          <th>% Meta Diaria</th>
        </tr>
        <?php echo $metas;?>
      </table>
    </div>
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" style="margin-top: 20px;">
  <div class="box shadow">
    <div class="box-header with-border">
      <h3 class="box-title">Barras de Progreso Acumulado Mensual</h3>
    </div>
    <div class="box-body">
      <?php echo $barras;?>
      <div class="row">
        <div class="col-md-12">
          <h4><b>Monto faltante para las metas en:</b></h4>
        </div>
        <br><br>
        <div class="col-md-12">
          <?php echo $resumen;?>
        </div>
      </div>
    </div>
  </div>
</div>