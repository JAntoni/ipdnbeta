<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Chart extends Conexion {

  function listarreporteporaño($sede, $año){
        
        if($sede > 0){
            $concatenar= ' AND E.tb_empresa_id =:sede_id';
          }
        else{ 
            $concatenar ='';
        }
        
      try {
        $sql = "Select YEAR(tb_credito_feccre) as año, MONTH (tb_credito_feccre) as nromes,
                CASE MONTH (tb_credito_feccre) 
                WHEN 1 THEN 'ENE' 
                WHEN 2 THEN 'FEB'
                WHEN 3 THEN 'MAR' 
                WHEN 4 THEN 'ABR' 
                WHEN 5 THEN 'MAY' 
                WHEN 6 THEN 'JUN' 
                WHEN 7 THEN 'JUL'
                WHEN 8 THEN 'AGOS' 
                WHEN 9 THEN 'SEPT' 
                WHEN 10 THEN 'OCT' 
                WHEN 11 THEN 'NOV' 
                WHEN 12 THEN 'DIC' 
                END as nombremes, E.tb_empresa_id,E.tb_empresa_nomcom as sede, COUNT(*) as creditos, SUM(tb_garantia_val) as monto 
                FROM tb_creditomenor CM 
                INNER JOIN tb_empresa E ON E.tb_empresa_id=CM.tb_empresa_id 
                INNER JOIN tb_garantia gar on gar.tb_credito_id = CM.tb_credito_id
                WHERE CM.tb_credito_xac = 1 AND tb_garantia_xac = 1 AND tb_garantiatipo_id IN(1,2,3) AND YEAR(tb_credito_feccre) = :fec1 ".$concatenar."
                GROUP BY YEAR(tb_credito_feccre), MONTH (tb_credito_feccre) ORDER BY MONTH (tb_credito_feccre) ASC";

        $sentencia = $this->dblink->prepare($sql);
        if($sede > 0){
            $sentencia->bindParam(":sede_id", $sede, PDO::PARAM_INT);
          }
            $sentencia->bindParam(":fec1", $año, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

  }
    
  function listarreportegeneral($sede,$fecha1,$fecha2){
      
      if($sede > 0){
          $concatenar= ' HAVING tb_empresa_id =:sede_id';
        }
      else{ 
          $concatenar ='';
      }
      
    try {
      $sql = "SELECT E.tb_empresa_nomcom as sede,tb_usuario_nom as Vendedor, COUNT(*) as creditos, SUM(tb_credito_preaco) as monto, CAST(SUM(tb_credito_preaco*(tb_credito_int/100)*(tb_credito_int/100)) AS DECIMAL(10,3)) as comision
              FROM tb_creditomenor CM
              INNER JOIN tb_empresa E ON E.tb_empresa_id=CM.tb_empresa_id
              INNER JOIN tb_usuario U ON U.tb_usuario_id=CM.tb_credito_usureg 
              WHERE CM.tb_credito_int>7 AND DATE(CM.tb_credito_feccre) BETWEEN :fec1 AND :fec2 
              AND CM.tb_credito_xac = 1
                  GROUP BY E.tb_empresa_id, U.tb_usuario_id ".$concatenar." ORDER BY monto DESC";

      $sentencia = $this->dblink->prepare($sql);
      if($sede > 0){
          $sentencia->bindParam(":sede_id", $sede, PDO::PARAM_INT);
        }
          $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
          $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  }
    
  function listar_reporte_venta_garantias($empresa_id, $fecha1, $fecha2, $usuario_id){        
    try {
      $filtro_usuario = '';
      $filtro_empresa = '';
      if(intval($usuario_id) > 0)
        $filtro_usuario = 'AND foo.usuario = '.intval($usuario_id);
      if(intval($empresa_id) > 0)
        $filtro_empresa = 'AND ven.tb_empresa_id = '.intval($empresa_id);

      $sql = "SELECT 
      usu.tb_usuario_nom, 
      COUNT(usu.tb_usuario_nom) AS cantidad_ventas, 
      SUM(foo.tb_ventagarantia_prec2) as total_vendido, 
      gar.tb_credito_id FROM (SELECT ven.tb_garantia_id, tb_ventagarantia_prec2, tb_usuario_id, tb_ventagarantia_col, (CASE WHEN tb_ventagarantia_col = 0 THEN tb_usuario_id ELSE tb_ventagarantia_col END) as usuario 
      FROM `tb_ventagarantia`  ven
      WHERE tb_ventagarantia_fec BETWEEN :fecha1 AND :fecha2 AND tb_ventagarantia_xac = 1 ".$filtro_empresa.") AS foo
      INNER JOIN tb_usuario usu ON usu.tb_usuario_id = foo.usuario
      INNER JOIN tb_garantia gar ON gar.tb_garantia_id = foo.tb_garantia_id WHERE gar.tb_garantia_est = 1 ".$filtro_usuario." 
      GROUP BY 1 ORDER BY 3 DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  }
    
  function listarcreditosporsede($fecha1,$fecha2){
    try {
      $sql = "SELECT tb_empresa_nomcom as sede, COUNT(*) as creditos
              FROM tb_creditomenor CM
              INNER JOIN tb_empresa E ON E.tb_empresa_id=CM.tb_empresa_id
              WHERE CM.tb_credito_int>7 AND DATE(CM.tb_credito_feccre) BETWEEN :fec1 AND :fec2 
              AND CM.tb_credito_xac = 1
              GROUP BY E.tb_empresa_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  }
    
  function listarvaloresporsede($fecha1,$fecha2){
    try {
      $sql = "SELECT tb_empresa_nomcom as sede, COUNT(*) as creditos, SUM(tb_credito_preaco) as monto
              FROM tb_creditomenor CM
              INNER JOIN tb_empresa E ON E.tb_empresa_id=CM.tb_empresa_id
              WHERE CM.tb_credito_int>7 AND DATE(CM.tb_credito_feccre) BETWEEN :fec1 AND :fec2 
              AND CM.tb_credito_xac = 1
              GROUP BY E.tb_empresa_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  } 

  //juan antonio 19-08-2023
  function listar_cmenor_colocacion($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id){        
    try {
      $filtro_usuario = '';
      $filtro_empresa = '';
      if(intval($usuario_id) > 0)
        $filtro_usuario = 'AND tb_credito_usureg = '.intval($usuario_id);
      if(intval($empresa_id) > 0)
        $filtro_empresa = ' AND cre.tb_empresa_id = '.intval($empresa_id);

      $sql = "SELECT 
          cre.tb_credito_id,
          cre.tb_credito_usureg,
          cre.tb_credito_preaco,
          tb_garantia_val,
          cre.tb_empresa_id
        FROM tb_creditomenor cre 
        INNER JOIN tb_garantia gar ON gar.tb_credito_id = cre.tb_credito_id
        WHERE tb_credito_xac = 1 AND tb_garantia_xac = 1 AND gar.tb_garantiatipo_id in(1,2,3) AND tb_credito_est NOT IN(1,2)
          AND tb_credito_feccre BETWEEN :fecha_inicio AND :fecha_fin ".$filtro_usuario.$filtro_empresa;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  }

  function listar_creditos_asiveh_garveh_hipo($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id){        
    try {
      $filtro_usuario = '';
      $filtro_empresa = '';
      if(intval($usuario_id) > 0)
        $filtro_usuario = 'AND tb_credito_usureg = '.intval($usuario_id);
      if(intval($empresa_id) > 0)
        $filtro_empresa = ' AND cre.tb_empresa_id = '.intval($empresa_id);

      // tb_credito_tip = 1 GARVEH COMPRA VENTA, 2 ADENDA, 3 ACUERDO DE PAGO, 4 GAR MOB
      // tb_credito_tip2 = 1 CRE REGULAR, 2 ESPECIFICO, 3 REPROGRAMADO, 4 CUOTA BALON, 5 REFINANCIADO, 6 AMORTIZADO
      
      $sql = "
          SELECT
            tb_credito_id,
            tb_credito_preaco,
            tb_credito_tipcam,
            IF(tb_moneda_id = 2, ROUND(tb_credito_preaco * tb_credito_tipcam, 2), tb_credito_preaco) AS prestamo_soles
          FROM tb_creditoasiveh cre
          WHERE tb_credito_xac = 1 AND tb_credito_tip1 != 3 AND tb_credito_tip2 NOT IN(2,5,6,7) 
            AND tb_credito_int > 0 AND tb_credito_fecvig BETWEEN :fecha_inicio AND :fecha_fin ".$filtro_usuario.$filtro_empresa."
        UNION
          SELECT
            tb_credito_id,
            tb_credito_preaco,
            tb_credito_tipcam,
            IF(tb_moneda_id = 2, ROUND(tb_credito_preaco * tb_credito_tipcam, 2), tb_credito_preaco) AS prestamo_soles
          FROM tb_creditogarveh cre
          WHERE tb_credito_xac = 1 AND tb_credito_tip != 3 AND tb_credito_tip2 NOT IN(5,6)
          AND tb_credito_int > 0 AND tb_credito_est NOT IN(1,2,8) AND tb_credito_fecvig BETWEEN :fecha_inicio AND :fecha_fin ".$filtro_usuario.$filtro_empresa."
        UNION
          SELECT
            tb_credito_id,
            tb_credito_preaco,
            tb_credito_tipcam,
            IF(tb_moneda_id = 2, ROUND(tb_credito_preaco * tb_credito_tipcam, 2), tb_credito_preaco) AS prestamo_soles
          FROM tb_creditohipo cre
          WHERE tb_credito_xac = 1 AND tb_credito_tip != 3 AND tb_credito_tip2 NOT IN(5,6) 
            AND tb_credito_int > 0 AND tb_credito_fecvig BETWEEN :fecha_inicio AND :fecha_fin ".$filtro_usuario.$filtro_empresa;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  }

  function listar_creditos_asiveh_garveh_hipo_no_vigentes($fecha_inicio, $fecha_fin, $usuario_id, $empresa_id){        
    try {
      $filtro_usuario = '';
      $filtro_empresa = '';
      if(intval($usuario_id) > 0)
        $filtro_usuario = ' AND tb_credito_usureg = '.intval($usuario_id);
      if(intval($empresa_id) > 0)
        $filtro_empresa = ' AND cre.tb_empresa_id = '.intval($empresa_id);

      $sql = "
          SELECT
            2 AS credito_general,
            tb_credito_id,
            tb_credito_preaco,
            tb_credito_tipcam,
            (CASE WHEN tb_moneda_id = 1 THEN tb_credito_preaco ELSE ROUND(tb_credito_preaco * tb_credito_tipcam, 2) END) AS prestamo_soles,
            tb_usuario_nom,
            tb_cliente_nom,
            'Crédito Asiveh' as credito_subtipo,
            1 as custodia,
            DATE(tb_credito_apr) as fecha_aprobado,
            tb_credito_tip1 as tipo_credito
          FROM tb_creditoasiveh cre 
          INNER JOIN tb_usuario usu ON usu.tb_usuario_id = cre.tb_credito_usureg
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          WHERE tb_credito_xac = 1 AND tb_credito_tip1 != 3 AND tb_credito_tip2 NOT IN(2,5,6,7) 
          AND tb_credito_int > 0 AND tb_credito_est IN(2, 9, 10)".$filtro_usuario.$filtro_empresa."
        UNION
          SELECT
            3 AS credito_general,
            tb_credito_id,
            tb_credito_preaco,
            tb_credito_tipcam,
            (CASE WHEN tb_moneda_id = 1 THEN tb_credito_preaco ELSE ROUND(tb_credito_preaco * tb_credito_tipcam, 2) END) AS prestamo_soles,
            tb_usuario_nom,
            tb_cliente_nom,
            cre.tb_credito_subgar AS credito_subtipo,
            tb_credito_cus as custodia,
            DATE(tb_credito_apr) as fecha_aprobado,
            tb_credito_tip as tipo_credito
          FROM tb_creditogarveh cre 
          INNER JOIN tb_usuario usu ON usu.tb_usuario_id = cre.tb_credito_usureg
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          WHERE tb_credito_xac = 1 AND tb_credito_tip != 3 AND tb_credito_tip2 NOT IN(5,6)
          AND tb_credito_int > 0 AND tb_credito_est IN(2, 9, 10)".$filtro_usuario.$filtro_empresa."
        UNION
          SELECT
            4 AS credito_general,
            tb_credito_id,
            tb_credito_preaco,
            tb_credito_tipcam,
            (CASE WHEN tb_moneda_id = 1 THEN tb_credito_preaco ELSE ROUND(tb_credito_preaco * tb_credito_tipcam, 2) END) AS prestamo_soles,
            tb_usuario_nom,
            tb_cliente_nom,
            'Crédito Hipotecario' as credito_subtipo,
            1 as custodia,
            DATE(tb_credito_apr) as fecha_aprobado,
            tb_credito_tip as tipo_credito
          FROM tb_creditohipo cre 
          INNER JOIN tb_usuario usu ON usu.tb_usuario_id = cre.tb_credito_usureg
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          WHERE tb_credito_xac = 1 AND tb_credito_tip != 3 AND tb_credito_tip2 NOT IN(5,6) 
          AND tb_credito_int > 0 AND tb_credito_est IN(2, 9, 10)".$filtro_usuario.$filtro_empresa;

      $sentencia = $this->dblink->prepare($sql);
      // $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
      // $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }

  }

  // -------------------------------------------------------- //

  function listar_estado_cuota_cliente($fecha1,$fecha2,$estado,$usuario,$credtipo){

    $sql = "";
    $sqlestado = '';
    $sqlusuario = '';

    try
    {
      if( $credtipo == 1 )
      {
        if($estado != 0){ $sqlestado = " AND cu.tb_cuota_est = $estado"; }
        if($usuario != 0){ $sqlusuario = " AND cm.tb_credito_usureg = $usuario"; }

        $sql = "SELECT cu.tb_cuota_id AS codigoid, cu.tb_cuota_reg, cu.tb_creditotipo_id AS tipo_credito_id, cu.tb_credito_id, cu.tb_moneda_id, cu.tb_cuota_num, cu.tb_cuota_fec AS fecha_pago, 
          cu.tb_cuota_cap, cu.tb_cuota_amo, cu.tb_cuota_int, cu.tb_cuota_cuo AS monto_cuota, cu.tb_cuota_est AS estado_cuota,
          cl.tb_cliente_id, cl.tb_cliente_doc, cl.tb_cliente_tip, cl.tb_cliente_nom, cl.tb_cliente_empruc, cl.tb_cliente_emprs, DATEDIFF (cu.tb_cuota_fec, CURRENT_DATE) AS diferencia_dias,
          u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot
        FROM tb_cuota cu
        INNER JOIN tb_creditomenor cm ON (cu.tb_credito_id = cm.tb_credito_id)
        INNER JOIN tb_cliente cl ON (cm.tb_cliente_id = cl.tb_cliente_id)
        INNER JOIN tb_usuario u ON (cm.tb_credito_usureg = u.tb_usuario_id)
        WHERE 
          cu.tb_cuota_xac = 1
        AND
          cm.tb_credito_xac = 1
        AND 
          cu.tb_creditotipo_id = 1 
        AND 
          cu.tb_cuota_num < 4 
        AND 
          DATE(cu.tb_cuota_fec) BETWEEN :p_fec1 AND :p_fec2".$sqlestado.$sqlusuario." ORDER BY 1 DESC
        ";
      }
      else // GARVEH
      {
        if($estado != 0){ $sqlestado = " AND cu.tb_cuota_est = $estado"; }
        if($usuario != 0){ $sqlusuario = " AND cg.tb_credito_usureg = $usuario"; }
    
        $sql = "SELECT cg.tb_credito_id, cg.tb_credito_reg, cg.tb_credito_usureg, cg.tb_credito_usuapr, cg.tb_cuotatipo_id, ct.tb_cuotatipo_nom, cu.tb_cuota_num, cu.tb_cuota_fec AS fecha_pago, cu.tb_cuota_cap, cu.tb_cuota_amo, cu.tb_cuota_int, cu.tb_cuota_cuo AS monto_cuota, cu.tb_cuota_est,
          cg.tb_cliente_id, cl.tb_cliente_doc, cl.tb_cliente_tip, cl.tb_cliente_nom, cl.tb_cliente_empruc, cl.tb_cliente_emprs,
          u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot,
          DATEDIFF (cu.tb_cuota_fec, CURRENT_DATE) AS diferencia_dias, cu.tb_creditotipo_id AS tipo_credito_id, cu.tb_cuota_est AS estado_cuota, cu.tb_moneda_id
          FROM tb_creditogarveh cg
          INNER JOIN tb_cuota cu ON (cg.tb_credito_id = cu.tb_credito_id)
          INNER JOIN tb_cuotatipo ct ON (cg.tb_cuotatipo_id = ct.tb_cuotatipo_id)
          INNER JOIN tb_cliente cl ON (cg.tb_cliente_id = cl.tb_cliente_id)
          INNER JOIN tb_usuario u ON (cg.tb_credito_usureg = u.tb_usuario_id)
          WHERE 
            cg.tb_credito_xac = 1
          AND
            cu.tb_cuota_xac = 1
          AND
            cu.tb_creditotipo_id = 3
          AND 
            cu.tb_cuota_num < 4
          AND
            cg.tb_credito_fecvig != '0000-00-00'
          AND 
            DATE(cu.tb_cuota_fec) BETWEEN :p_fec1 AND :p_fec2 ".$sqlestado.$sqlusuario." ORDER BY 1 DESC";
      }
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":p_fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":p_fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY RESULTADOS";
        $retorno["data"] = "";
      }

      return $retorno;
    }
    catch (Exception $e){
      throw $e;
    }
  }

  function retornarIdTempCobranzaPorCredito($credito_id){
    try {
      $sql = "SELECT temp_cobranza_id FROM temp_cobranza WHERE credito_id = :p_credito_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":p_credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

}