var arrayfinal = [];
var arraybarras = [];
var arraybarrasaño = [];
google.charts.load('current', {'packages':['corechart']});
google.charts.load('current', {'packages':['corechart', 'bar']});

$(document).ready(function() {     
     
    $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //startDate: "-0d"
    //endDate : new Date()
  });
  
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('#cmb_sede_id').change(function () {
    cargar_datachart1();    
    cargar_datachart2();
    console.log("este es el codigo del select " + $(this).val());
});

$('#cmb_año').change(function () {
    cargar_datachartaño();
    console.log("este es el codigo del select " + $(this).val());
});

$('#cmb_sede_idaño').change(function () {
    cargar_datachartaño();
    console.log("este es el codigo del select " + $(this).val());
});

//  CARGAR PRIMER GRUPO DE GRAFICOS (POR AÑO)
    cargar_datachartaño();

//  CARGAR SEGUNDO GRUPO DE GRÁFICOS
    cargar_datachart1();    
    cargar_datachart2();
});

function cargar_datachart1(){
    var fecha1 = $('#txt_filtro_fec1').val();
    var fecha2 = $('#txt_filtro_fec2').val();
    //alert(fecha1+fecha2);
    $.ajax({
      type: "POST",
      url: VISTA_URL+"principal/chart_controller.php",
      async:true,
      dataType: "json",                      
      data: ({
          fecha1: fecha1,
          fecha2: fecha2,
          grafico:"piechart"
      }),
      success: function(data){
          console.log(data);
          var arraydatos = [];
        for(let i=0; i<data.length;i++) {
        // Acceder a cada elemento de resultados y operaciones, según el primer ciclo
            let creditos = parseInt(data[i][1]);
            arraydatos.push([data[i][0],creditos]);
        }
        arraydatos.unshift(['Sede','Creditos']);
        arrayfinal = arraydatos;
        console.log(arrayfinal);
        google.charts.setOnLoadCallback(drawChart);
      }
    });
    piechart_tabla();
  }
  
  function piechart_tabla(){
    var fecha1 = $('#txt_filtro_fec1').val();
    var fecha2 = $('#txt_filtro_fec2').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL+"principal/piechart_tabla.php",
    async: true,
    dataType: "html",
    data: ({
        fecha1: fecha1,
        fecha2: fecha2
    }),
    beforeSend: function() {
      $('#piechart_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_piechart_tabla').html(data);
      $('#piechart_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#piechart_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
  
  function cargar_datachart2(){
    var fecha1 = $('#txt_filtro_fec1').val();
    var fecha2 = $('#txt_filtro_fec2').val();
    var sede = $('#cmb_sede_id').val();
    $.ajax({
      type: "POST",
      url: VISTA_URL+"principal/chart_controller.php",
      async:true,
      dataType: "json",                      
      data: ({
          fecha1: fecha1,
          fecha2: fecha2,
          sede: sede,
          grafico:"chart_div"
      }),
      success: function(data){
          //console.log(data);
          var arraydatos = [];
        for(let i=0; i<data.length;i++) {
        // Acceder a cada elemento de resultados y operaciones, según el primer ciclo
            let creditos = parseInt(data[i][1]);
            let monto = parseInt(data[i][2]);
            arraydatos.push([data[i][0],creditos,monto]);
        }
        arraydatos.unshift(['Vendedor','Creditos','Monto']);
        arraybarras = arraydatos;
        console.log(arraybarras);
        google.charts.setOnLoadCallback(drawStuff);
      }
    });
    barras_tabla();
    barras_remate();
  }
  
    function barras_tabla(){
    var fecha1 = $('#txt_filtro_fec1').val();
    var fecha2 = $('#txt_filtro_fec2').val();
    var sede = $('#cmb_sede_id').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL+"principal/barras_tabla.php",
    async: true,
    dataType: "html",
    data: ({
          fecha1: fecha1,
          fecha2: fecha2,
          sede: sede
    }),
    beforeSend: function() {
      $('#piechart_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_barras_tabla').html(data);
      $('#barras_mensaje_tbl').hide(300);
      
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#barras_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function barras_remate(){
    var fecha1 = $('#txt_filtro_fec1').val();
    var fecha2 = $('#txt_filtro_fec2').val();
    var sede = $('#cmb_sede_id').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL+"principal/barras_remate.php",
    async: true,
    dataType: "html",
    data: ({
          fecha1: fecha1,
          fecha2: fecha2,
          sede: sede
    }),
    beforeSend: function() {
      $('#piechart_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_barras_remate').html(data);
      $('#barras_mensaje_tbl').hide(300);
      
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#barras_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
  
    function drawChart() {

        var data = google.visualization.arrayToDataTable(arrayfinal);

        var options = {
          title: 'CREDITOS POR SEDE',
          pieSliceText: 'value', 
           is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
     
    function drawStuff() {

        var chartDiv = document.getElementById('chart_div');

        var data = google.visualization.arrayToDataTable(arraybarras);


        var materialOptions = {
          chart: {
            title: 'CREDITOS GENERAL'
          },
          series: {
            0: { axis: 'CREDITOS' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'MONTO' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              distance: {label: 'CREDITOS'}, // Left y-axis.
              brightness: {side: 'right', label: 'MONTO'} // Right y-axis.
            }
          }
        };

        function drawMaterialChart() {
          var materialChart = new google.charts.Bar(chartDiv);
          materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
        }

        drawMaterialChart();
    };

function cargargraficos(){
    cargar_datachart1();    
    cargar_datachart2();
}

  function cargar_datachartaño(){
    var año = $('#cmb_año').val();
    var sede = $('#cmb_sede_idaño').val();
    //console.log(año+sede);
    $.ajax({
      type: "POST",
      url: VISTA_URL+"principal/chart_controller.php",
      async:true,
      dataType: "json",                      
      data: ({
          año: año,
          sede: sede,
          grafico:"chart_divaño"
      }),
      success: function(data){
          console.log(data);
          var arraydatos = [];
        for(let i=0; i<data.length;i++) {
        // Acceder a cada elemento de resultados y operaciones, según el primer ciclo
            let creditos = parseInt(data[i][1]);
            let monto = parseInt(data[i][2]);
            arraydatos.push([data[i][0],creditos,monto]);
        }
        arraydatos.unshift(['Mes','Creditos','Monto']);
        arraybarrasaño = arraydatos;
        console.log(arraybarrasaño);
        google.charts.setOnLoadCallback(drawStuffaño);
      }
    });
    cargar_tablasaño();
  }
  
  function drawStuffaño() {

        var chartDiv = document.getElementById('chart_divaño');

        var data = google.visualization.arrayToDataTable(arraybarrasaño);
        
        var view = new google.visualization.DataView(data);
        view.setColumns([0,
                               { calc: "stringify",
                                 sourceColumn: 1,
                                 type: "string",
                                 role: "annotation" },
                               1,
                               { calc: "stringify",
                                 sourceColumn: 2,
                                 type: "string",
                                 role: "annotation" },
                           2]);

        var materialOptions = {
          chart: {
            title: 'CREDITOS GENERAL POR AÑO'
          },
          series: {
            0: { axis: 'CREDITOS' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'MONTO' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              distance: {label: 'CREDITOS'}, // Left y-axis.
              brightness: {side: 'right', label: 'MONTO'} // Right y-axis.
            }
          }
        };

        function drawMaterialChart() {
          var materialChart = new google.charts.Bar(chartDiv);
          materialChart.draw(view, google.charts.Bar.convertOptions(materialOptions));
        }

        drawMaterialChart();
    };
  
  function cargar_tablasaño(){
    var año = $('#cmb_año').val();
    var sede = $('#cmb_sede_idaño').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL+"principal/listaanual_tabla.php",
    async: true,
    dataType: "html",
    data: ({
        año: año,
        sede: sede
    }),
    beforeSend: function() {
      $('#barrasaño_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_barrasaño_tabla').html(data);
      $('#barrasaño_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#barrasaño_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}