<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  
  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");
  require_once('Chart.class.php');
  $oChart = new Chart();
  
  $año= intval($_POST['año']);
  $sede= intval($_POST['sede']);
  
  $result = $oChart->listarreporteporaño($sede, $año);

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
        
        if($sede == 0){
            $general = "TODAS";
        }
        else{
            $general = $value['sede'];
        }
        switch ($value['nromes']){
            case 1:
                $mes = 'ENERO';
                break;
            case 2:
                $mes = 'FEBRERO';
                break;
            case 3:
                $mes = 'MARZO';
                break;
            case 4:
                $mes = 'ABRIL';
                break;
            case 5:
                $mes = 'MAYO';
                break;
            case 6:
                $mes = 'JUNIO';
                break;
            case 7:
                $mes = 'JULIO';
                break;
            case 8:
                $mes = 'AGOSTO';
                break;
            case 9:
                $mes = 'SEPTIEMBRE';
                break;
            case 10:
                $mes = 'OCTUBRE';
                break;
            case 11:
                $mes = 'NOVIEMBRE';
                break;
            case 12:
                $mes = 'DICIEMBRE';
                break;
        }
        
      $tr .='<tr>';
        $tr.='
          <td>'.$value['año'].'</td>
          <td>'.$mes.'</td>
          <td>'.$general.'</td>
          <td>'.$value['creditos'].'</td> 
          <td>'.mostrar_moneda($value['monto']).'</td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_piecharts" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>AÑO</th>
      <th>MES</th>
      <th>SEDE</th>
      <th>CREDITOS</th>
      <th>MONTO</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
