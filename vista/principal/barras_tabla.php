<?php  
  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");
  require_once('Chart.class.php');
  $oChart = new Chart();

  $fecha1 = fecha_mysql($_POST['fecha1']);
  $fecha2 = fecha_mysql($_POST['fecha2']);
  $sede= intval($_POST['sede']);
  
  $result = $oChart->listarreportegeneral($sede,$fecha1,$fecha2);

  $tr = '';
  if($result['estado'] == 1){
      $n = 0;
    foreach ($result['data'] as $key => $value) {
        $n ++;
        if($n == 1){
            $estilos = "background:#3BF453;";
        }
        elseif ($n == 2) {
            $estilos = "background:#F5F33C;";
        }else{
            $estilos = "";
        }
      $tr .='<tr style="'.$estilos.'">';
        $tr.='
          <td class="top'.$n.'">'.$value['sede'].'</td>
          <td>'.$value['Vendedor'].'</td>
          <td>'.$value['creditos'].'</td>
          <td>'.mostrar_moneda($value['monto']).'</td> 
          <td>'.mostrar_moneda($value['comision']).'</td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<label style="font-family: cambria;font-weight: bolder;color: #0000FF;font-size: 18px">COLOCACIONES </label>
<table id="tbl_piecharts" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>SEDE</th>
      <th>VENDEDOR</th>
      <th>CREDITOS</th>
      <th>MONTO</th>
      <th>COMISION</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
