<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
//

  $fechap = new DateTime();
  $fechap->modify('last day of this month');
 
  $chart_fec1p = date('Y-m-01');
  $chart_fec2p = $fechap->format('Y-m-d');
  $año = date('Y');
  //$año = '2018';
  //$chart_fec1p = '2022-01-01';
  //$chart_fec2p = '2022-01-31';
  
require_once('vista/asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('vista/usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('vista/usuarioperfil/UsuarioPerfil.class.php');
$oUsuarioPerfil = new UsuarioPerfil();
require_once('Personal.class.php');
$oPersonal = new Personal();

require_once('vista/funciones/funciones.php');
require_once('vista/funciones/fechas.php');

$result = $oUsuario->mostrarUno($_SESSION['usuario_id']);

 if($result['estado'] == 1){
     $user = $result['data']['tb_usuario_nom'].' '.$result['data']['tb_usuario_ape'];
     $foto = $result['data']['tb_usuario_fot'];
     $result1 = $oUsuarioPerfil->mostrarUno($result['data']['tb_usuarioperfil_id']);
     $perfil = $result1['data']['tb_usuarioperfil_nom'];
 }
$result = "";

$result2 = $oPersonal->listarcreditosusuario($chart_fec1p,$chart_fec2p,$_SESSION['usuario_id']);

 if($result2['estado'] == 1){
     $creditos = $result2['data']['creditos'];
     $monto = $result2['data']['monto'];
     $comision = $result2['data']['comision'];
 }
 else{
     $creditos = 0;
     $monto = 0;
     $comision = '0.00';
 }
 $result2 = "";
 
 $newfec1 = strtotime ( '-1 month' , strtotime ( $chart_fec1p ) ) ;
 $newfec1 = date ( 'Y-m-d' , $newfec1 );
 $newfec2 = strtotime ( '-1 month' , strtotime ( $chart_fec2p ) ) ;
 $newfec2 = date ( 'Y-m-d' , $newfec2 );
 
 $result3 = $oPersonal->listarcreditosusuario($newfec1,$newfec2,$_SESSION['usuario_id']);
  if($result3['estado'] == 1){
      $monto = $result3['data']['monto'];
      $meta = floatval($monto) + floatval($monto)*0.20;
  }
 $cumplido = mostrar_moneda(($monto/$meta)*100);
?>
<!-- Content Wrapper. Contains page content -->

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="box box-widget widget-user">
                        <input type="hidden" name="hdd_año_id" id="hdd_año_id" value="<?php echo $año; ?>">
                        <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $_SESSION['usuario_id']; ?>">

                        <div class="widget-user-header bg-aqua-active">
                            <h3 class="widget-user-username"><?php echo $user ?></h3>
                            <h5 class="widget-user-desc"><?php echo $perfil ?></h5>
                        </div>
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?php echo $foto ?>" alt="User Avatar">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header"><?php echo $creditos ?></h5>
                                        <span class="description-text">CREDITOS</span>
                                    </div>

                                </div>
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header"><?php echo $monto ?></h5>
                                        <span class="description-text">MONTO</span>
                                    </div>

                                </div>

                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header"><?php echo $comision ?></h5>
                                        <span class="description-text">COMISION</span>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><?php echo $monto ?></h3>
                                    <p>MONTO</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-usd"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3><?php echo $meta ?></h3>
                                    <p>META</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-usd"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><?php echo $cumplido ?><sup style="font-size: 20px">%</sup></h3>
                                    <p>CUMPLIDO</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-chevron-up"></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div id="barrasgeneral" ></div>
                    <div id="png" ></div>
                </div>
            </div>
