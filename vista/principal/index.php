<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
  echo 'Terminó la sesión';
  exit();
}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- TODOS LOS ESTILOS-->
  <?php include(VISTA_URL . 'templates/head.php'); ?>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <title>IPDN | Inicio</title>

</head>

<body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>

  <div id="wpf-loader-two">
    <div class="wpf-loader-two-inner" style="color: #f4f4f4; font-family: verdana;font-size: 7pt;margin: 10px 0 0;    text-align: center; border: 2px;border-radius: 50%;height: 100px;left: 46%;position: absolute;top: 40%;width: 100px;text-align: center;">
      <img class="img-responsive fa-spin" src="public/images/logopres1.png" alt="Photo" style="width: 60px; height: 60px">
    </div>
  </div>
  <div class="wrapper">
    <!-- INCLUIR EL HEADER-->
    <?php include(VISTA_URL . 'templates/header.php'); ?>
    <!-- INCLUIR ASIDE, MENU LATERAL -->
    <?php include(VISTA_URL . 'templates/aside.php'); ?>
    <!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO -->
    <?php
    if ($_SESSION['usuarioperfil_id'] == 1) {
      include('principal_admin.php');
    } 
    else{
      include('principal_vendedor.php');
    }
    //else{
    include('principal_vista.php');
    //}

    ?>
    <!-- GERSON (21-04-23) -->
    <?php if ($_SESSION['usuario_id'] == 2 || $_SESSION['usuario_id'] == 11 || $_SESSION['usuario_id'] == 42 || $_SESSION['usuario_id'] == 52 || $_SESSION['usuario_id'] == 61) { ?>
      <ul class="notifications">

      </ul>
      <div class="buttons">
        <!-- <button class="btn" id="success">Success</button>
					<button class="btn" id="info">Info</button>
					<button class="btn" id="warning">Warning</button>
					<button class="btn" id="error">Error</button> -->
      </div>
    <?php } ?>
    <!--  -->
    <!-- INCLUIR FOOTER -->
    <?php include(VISTA_URL . 'templates/footer.php'); ?>
    <div class="control-sidebar-bg"></div>
  </div>
  <style>
    :root {
      --dark: #34495E;
      --light: #ffffff;
      --success: #0ABF30;
      --error: #E24D4C;
      --warning: #E9BD0C;
      --info: #3498DB;
    }

    .notifications {
      position: fixed;
      z-index: 10;
      top: 60px;
      right: 20px;
      padding-left: 0px;
    }

    .notifications :where(.toast, .column) {
      display: flex;
      align-items: center;
    }

    .notifications .toast {
      width: 300px;
      position: relative;
      overflow: hidden;
      list-style: none;
      border-radius: 4px;
      padding: 16px 17px;
      margin-bottom: 10px;
      background: var(--light);
      justify-content: space-between;
      animation: show_toast 0.3s ease forwards;
    }

    .text-grey{
      color: #6a6a6a !important;
    }

    @keyframes show_toast {
      0% {
        transform: translateX(100%);
      }

      40% {
        transform: translateX(-5%);
      }

      80% {
        transform: translateX(0%);
      }

      100% {
        transform: translateX(-10%);
      }
    }

    .notifications .toast.hide {
      animation: hide_toast 0.3s ease forwards;
    }

    @keyframes hide_toast {
      0% {
        transform: translateX(-10%);
      }

      40% {
        transform: translateX(0%);
      }

      80% {
        transform: translateX(-5%);
      }

      100% {
        transform: translateX(calc(100%+20px));
      }
    }

    /* .toast::before {
        position: absolute;
        content: "";
        height: 3px;
        width: 50%;
        bottom: 0px;
        left: 0px;
        animation: progress 10s linear forwards;
    } */
    @keyframes progress {
      100% {
        width: 0%;
      }
    }

    .toast.success::before,
    .btn#success {
      background: var(--success);
    }

    .toast.info::before,
    .btn#info {
      background: var(--info);
    }

    .toast.warning::before,
    .btn#warning {
      background: var(--warning);
    }

    .toast.error::before,
    .btn#error {
      background: var(--error);
    }

    .toast.success .column i {
      color: var(--success);
    }

    .toast.info .column i {
      color: var(--info);
    }

    .toast.warning .column i {
      color: var(--warning);
    }

    .toast.error .column i {
      color: var(--error);
    }

    .toast .column i {
      font-size: 1.75rem;
    }

    .toast .column span {
      font-size: 1.07rem;
      margin-left: 12px;
    }

    .toast i:last-child {
      color: #aeb0d7;
      cursor: pointer;
    }

    .toast i:last-child:hover {
      color: var(--dark);
      cursor: pointer;
    }

    .buttons .btn {
      border: none;
      outline: none;
      color: #ffffff;
      cursor: pointer;
      margin: 0 5px;
      font-size: 1.2rem;
      padding: 10px 20px;
      border-radius: 4px;
    }

    @media screen and (max-width: 530px) {
      .notifications {
        width: 95%;
        z-index: 10;
        padding-top: 50px;
      }

      .notifications .toast {
        width: 100%;
        font-size: 1rem;
        margin-left: 20px;
      }

      .buttons .btn {
        margin: 0 1px;
        font-size: 1.1rem;
        padding: 8px 15px;
      }
    }

    @media screen and (max-width: 850px) {
      .notifications {
        padding-top: 50px;
      }
    }
  </style>

  <!-- TODOS LOS SCRIPTS-->
  <?php include(VISTA_URL . 'templates/script.php'); ?>
  
  <?php if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuarioperfil_id'] == 5 || $_SESSION['usuarioperfil_id'] == 6):?>
    <script type="text/javascript" src="<?php echo VISTA_URL . 'principal/chart.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo VISTA_URL . 'principal/principal.js?ver=261124'; ?>"></script>
  <?php endif; ?>
  
  <?php if ($_SESSION['usuarioperfil_id'] != 1):?>
    <script type="text/javascript" src="<?php echo VISTA_URL . 'principal/vendedor.js?ver=261124'; ?>"></script>
  <?php endif; ?>


</body>

</html>