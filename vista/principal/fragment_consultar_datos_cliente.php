<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../../core/usuario_sesion.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  $oAsignar = new AsignarOpcionGc();
  require_once('../reportecredito/ReporteCredito.class.php');
  $oReporteCredito = new ReporteCredito();
  require_once('../cobranzatodos/CobranzaTodos.class.php');
  $oCobranzatodos = new CobranzaTodos();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $usuarioperfil_id   = $_SESSION['usuarioperfil_id'];
  $usuario_action     = $_POST['p_action'];  
  $modulo_nombre      = $_POST['p_modulo_nombre'];
  $cliente_id         = $_POST['p_cliente_id'];
  $credito_id         = $_POST['p_credito_id'];
  $temp_cobranza_id   = $_POST['p_temp_cobranza_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Opción de GC Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Asignar Opción o Registrar Comentarios';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Opción de GC ';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Opción de GC ';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  $cuotadetalle_id = 0;
  // $credito_id = 0;
  $opcionesgc_id = 0;
  $opciongc_fechapdp = date('d-m-Y'); // por defento fecha actual
  $comentario_des = '';
  $encargado_id = 32; // por defecto el responsable de gestionar este caso será el GESTOR DE COBRANZA: ahora 32 -> carlos augusto
  $style_pdp_fecha = 'style="display: none;"';
  // $cliente_id = 0;

  $result = $oReporteCredito->mostrarUnoTempCobranza($temp_cobranza_id);
    if($result['estado'] == 1){
      $cuotadetalle_id   = $result['data']['cuotadetalle_id'];
      // $credito_id        = $result['data']['credito_id'];
      $credito_tipo      = $result['data']['credito_tipo'];
      $credito_subtipo   = $result['data']['credito_subtipo'];
      $cuota_actual      = $result['data']['cuota_actual'];
      $cliente_nom       = $result['data']['cliente_nom'];
      // $cliente_id        = $result['data']['cliente_id'];
      $representante_nom = $result['data']['representante_nom'];
      $cliente_dir       = $result['data']['cliente_dir'];
      $cuota_monto       = $result['data']['cuota_monto'];
      $cuota_soles       = $result['data']['cuota_soles'];
      $cuota_fecha       = mostrar_fecha($result['data']['cuota_fecha']);
      $ultimopago_fecha  = mostrar_fecha($result['data']['ultimopago_fecha']);
      $inactividad       = $result['data']['inactividad'];
      $deuda_acumulada   = $result['data']['deuda_acumulada'];
    }
  $result = NULL;

  $result = $oAsignar->listar_asignar_opcionesgc_credito_cuotadetalle($credito_id, $cuotadetalle_id);
    if($result['estado'] == 1){
      $opcionesgc_id     = $result['data']['tb_opcionesgc_id'];
      $opcionesgc_nom    = $result['data']['tb_opcionesgc_nom'];
      $comentario_des    = $result['data']['comentario_des'];
      $encargado_id      = $result['data']['encargado_id'];
      $usu_apr_ejecucion = $result['data']['tb_asignaropciongc_usuapr'];
      if($opcionesgc_id == 1 || $opcionesgc_id == 4){
        $style_pdp_fecha = '';
        $opciongc_fechapdp = mostrar_fecha($result['data']['opciongc_fechapdp']);
      }
      if($encargado_id == 0)
        $encargado_id = 32; // por defecto el responsable de gestionar este caso será el GESTOR DE COBRANZA: ahora 32 -> carlos augusto
    }
  $result = NULL;

  $result = $oCliente->mostrarUno($cliente_id);
    if($result['estado'] == 1){
      $cliente_cel = str_replace(' ', '', $result['data']['tb_cliente_cel']);
      $cliente_tel = str_replace(' ', '', $result['data']['tb_cliente_tel']);
      $cliente_telref = str_replace(' ', '', $result['data']['tb_cliente_telref']);
      $cliente_procel = $result['data']['tb_cliente_procel'];
      $cliente_protel = $result['data']['tb_cliente_protel'];
      $cliente_protelref = $result['data']['tb_cliente_protelref'];

      $link_celular = $cliente_cel.' &nbsp;
        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51'.$cliente_cel.'" target="_blank"><i class="fa fa-whatsapp"></i></a> &nbsp;
        <a class="btn btn-social-icon btn-primary btn-sm" href="tel:+51'.$cliente_cel.'"><i class="fa fa-phone"></i></a>';
      $link_telefono = $cliente_tel.' &nbsp;
        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51'.$cliente_tel.'" target="_blank"><i class="fa fa-whatsapp"></i></a> &nbsp;
        <a class="btn btn-social-icon btn-primary btn-sm" href="tel:+51'.$cliente_tel.'"><i class="fa fa-phone"></i></a>';
      $link_referencial = $cliente_telref.' &nbsp;
        <a class="btn btn-social-icon btn-success btn-sm" href="https://wa.me/51'.$cliente_telref.'" target="_blank"><i class="fa fa-whatsapp"></i></a> &nbsp;
        <a class="btn btn-social-icon btn-primary btn-sm" href="tel:+51'.$cliente_telref.'"><i class="fa fa-phone"></i></a>';
    }
  $result = NULL;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_asignaropciongc" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      <form id="form_opcionesgc_asignar" role="form" method="POST">
        <div class="modal-body">
          <input type="hidden" name="action" value="insertar" class="form-control"/>
          <input type="hidden" id="directorio" value="opcionesgc_asignar">
          <input type="hidden" name="hdd_credito_subtipo" value="<?php echo $credito_subtipo;?>">
          <input type="hidden" name="hdd_credito_tipo" value="<?php echo $credito_tipo;?>">
          <input type="hidden" name="hdd_cuota_actual" value="<?php echo $cuota_actual;?>">
          <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
          <input type="hidden" name="hdd_cuotadetalle_id" value="<?php echo $cuotadetalle_id;?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" name="hdd_cliente_nom" value="<?php echo $cliente_nom;?>">
          <input type="hidden" name="hdd_cuota_fecha" value="<?php echo $cuota_fecha;?>">
          <input type="hidden" name="hdd_opcionesgc_nom" id="hdd_opcionesgc_nom" value="<?php echo $opcionesgc_nom;?>">
          <input type="hidden" name="hdd_temp_cobranza_id" value="<?php echo $temp_cobranza_id;?>">
          <div class="panel panel-primary shadow-simple">
            <div class="panel-header bg-info text-bold text-blue"><h3 class="panel-title p-4">DATOS DEL CLIENTE A GESTIONAR</h3></div>
            <div class="panel-body">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th><?php echo $cliente_procel;?></th>
                    <th><?php echo $cliente_protel;?></th>
                    <th><?php echo $cliente_protelref;?></th>
                  </tr>
                  <tr>
                    <th><?php echo $link_celular;?></th>
                    <th><?php echo $link_telefono;?></th>
                    <th><?php echo $link_referencial;?></th>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>Cliente:</th>
                    <td colspan="3"><?php echo $cliente_nom;?></td>
                  </tr>
                  <tr>
                    <th>Representante:</th>
                    <td colspan="3"><?php echo $representante_nom;?></td>
                  </tr>
                  <tr>
                    <th>Dirección:</th>
                    <td colspan="3"><?php echo $cliente_dir;?></td>
                  </tr>
                  <tr>
                    <th>Cuota Original:</th>
                    <td><?php echo mostrar_moneda($cuota_monto);?></td>
                    <th>Cuota en S/.:</th>
                    <td><?php echo mostrar_moneda($cuota_soles);?></td>
                  </tr>
                  <tr>
                    <th>Fecha Cuota:</th>
                    <td><?php echo $cuota_fecha;?></td>
                    <th>Fecha Ultimo Pago:</th>
                    <td><?php echo $ultimopago_fecha;?></td>
                  </tr>
                  <tr>
                    <th>Inactividad:</th>
                    <td><?php echo $inactividad;?></td>
                    <th>Deuda Acumulada:</th>
                    <td><?php echo mostrar_moneda($deuda_acumulada);?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <br>
          <div class="panel panel-primary shadow-simple">
            <div class="panel-header bg-info text-bold text-blue"><h3 class="panel-title p-4">ASIGNAR OPCIÓN PARA ÉSTE CRÉDITO</h3></div>
            <div class="panel-body">

              <div class="form-group col-md-4">
                <label for="cmb_opcionesgc_id" class="control-label">Opción de Gestión</label>
                <select class="form-control input-sm" id="cmb_opcionesgc_id" name="cmb_opcionesgc_id">
                  <option value="">Seleccione:</option>
                  <option value="1" <?php if( $opcionesgc_id == 1){ echo 'selected=""'; }?> >PDP</option>
                  <option value="2" <?php if( $opcionesgc_id == 2){ echo 'selected=""'; }?> >SIN RESPUESTA</option>
                </select>
              </div>
              <div class="form-group col-md-4 fecha_pdp" <?php echo $style_pdp_fecha;?> >
                <label for="txt_opciongc_fechapdp" class="control-label">Fecha para Gestión</label>
                <div class='input-group date' id='datetimepicker3'>
                  <input type='text' class="form-control input-sm" name="txt_opciongc_fechapdp" id="txt_opciongc_fechapdp" value="<?php echo $opciongc_fechapdp;?>"/>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
              <div class="form-group col-md-4">
                <label for="cmb_aplicar_todo" class="control-label">¿Aplica a Todo?</label>
                <select class="form-control input-sm" id="cmb_aplicar_todo" name="cmb_aplicar_todo">
                  <!-- <option value="0" selected="">Seleccione:</option> -->
                  <option value="1">Solo a esta Cuota</option>
                  <option value="2">Todas las cuotas del CLiente</option>
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="txt_comentario_des" class="control-label">Opcional Puede Dejar un Comentario</label>
                <input class="form-control input-sm" type="text" id="txt_comentario_des" name="txt_comentario_des" value="<?php echo $comentario_des;?>">
              </div>

              <input type="hidden" id="cmb_encargado_id" name="cmb_encargado_id" class="form-control" value="32" readonly/>

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_opcionesgc_asignar_principal">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/reportecreditomejorado/opcionesgc_todo.js?ver=251124';?>"></script>