<?php
require_once('Personal.class.php');
$oPersonal = new Personal();
require_once("../funciones/funciones.php");
$usuario_id = $_POST['usuario_id'];
$año = $_POST['año'];
$grafico = $_POST['grafico'];

if ($grafico == "barras_vendedor") {
  $array = array();
  $result = $oPersonal->listarendimientovendedor($año, $usuario_id);

  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $array_temporal = array();
      array_push($array_temporal, $value['nombremes']);
      array_push($array_temporal, $value['monto']);
      array_push($array, $array_temporal);
    }

    echo json_encode($array);
  }
  $result = "";
}
