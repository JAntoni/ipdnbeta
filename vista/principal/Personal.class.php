<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Personal extends Conexion
{

  function listarcreditosusuario($fecha1, $fecha2, $user)
  {

    try {
      $sql = "SELECT tb_usuario_nom as Vendedor, COUNT(DISTINCT CM.tb_credito_id) as creditos, SUM(tb_garantia_val) as monto, CAST(SUM(tb_garantia_val*(tb_credito_int/100)*(tb_credito_int/100)) AS DECIMAL(10,3)) as comision
                FROM tb_creditomenor CM
                INNER JOIN tb_usuario U ON U.tb_usuario_id=CM.tb_credito_usureg 
                INNER JOIN tb_garantia gar ON gar.tb_credito_id = CM.tb_credito_id
                WHERE CM.tb_credito_xac = 1 AND DATE(CM.tb_credito_feccre) BETWEEN :fec1 AND :fec2 
                AND tb_credito_est NOT IN(1,2) AND tb_garantia_xac = 1 AND gar.tb_garantiatipo_id IN(1,2,3) AND U.tb_usuario_id= :user 
                GROUP BY U.tb_usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":user", $user, PDO::PARAM_INT);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listarendimientovendedor($año, $usuario_id)
  {
    try {
      $sql = "Select MONTH (tb_credito_fecdes) as nromes,
                CASE MONTH (tb_credito_fecdes) 
                WHEN 1 THEN 'ENE' 
                WHEN 2 THEN 'FEB'
                WHEN 3 THEN 'MAR' 
                WHEN 4 THEN 'ABR' 
                WHEN 5 THEN 'MAY' 
                WHEN 6 THEN 'JUN' 
                WHEN 7 THEN 'JUL'
                WHEN 8 THEN 'AGOS' 
                WHEN 9 THEN 'SEPT' 
                WHEN 10 THEN 'OCT' 
                WHEN 11 THEN 'NOV' 
                WHEN 12 THEN 'DIC' 
                END as nombremes, COUNT(*) as creditos, SUM(tb_credito_preaco) as monto 
                FROM tb_creditomenor
                WHERE tb_credito_int>7 AND tb_credito_xac=1 AND YEAR(tb_credito_fecdes) = :fec1 AND tb_credito_usureg= :usuario
                GROUP BY MONTH (tb_credito_fecdes) ORDER BY MONTH (tb_credito_fecdes) ASC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $año, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario", $usuario_id, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTE AÑO PARA ESTE VENDEDOR";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }


  function listarCreditoMenorFecha($fecha1, $fecha2)
  {

    try {
      $sql = "SELECT *
                FROM tb_creditomenor WHERE 
                tb_credito_int>7 
                AND DATE(tb_credito_feccre) BETWEEN :fec1 AND :fec2
                AND tb_credito_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "NO HAY CREDITOS EN ESTAS FECHAS O SEDE";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
