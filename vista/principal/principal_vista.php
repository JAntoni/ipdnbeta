<?php
require_once('vista/asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();

$empresa_id = intval($_SESSION['empresa_id']);
$client_ip = $_SERVER['REMOTE_ADDR'];
$hostname = gethostbyaddr($client_ip);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_asistencia" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Marca tu Asistencia, Buen día</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">	
            <!-- /.info-box -->
            <div class="box  box-material">
              <div class="box-header with-border">
                <h3 class="box-title">Marcar Asistencia</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="padding: 0px">
                <div class="row">
                  <div class="col-md-12">
                    <div class="info-box bg-gray" style="margin-bottom: 0px; border-radius: 0px ">
                      <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">CHICLAYO</span>
                        <span class="info-box-number"><?php
                          $fecha = date("j/n/Y");
                          echo $fecha;
                          ?>
                        </span>
                        <div class="progress">
                          <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                          Actividades
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>		
                  </div>
                </div>

                <div class="col-md-12">
                  <table class="table">
                    <tr>
                      <td>Turno Mañana</td>
                      <td>9:00 am - 1:00 pm</td>
                    </tr>
                    <tr>
                      <td>Receso</td>
                      <td>1:00 pm - 3:00 pm</td>
                    </tr>
                    <tr>
                      <td>Turno Tarde</td>
                      <td>3:00 pm - 7:00 pm</td>
                    </tr>
                  </table>
                </div>
                
                <?php
                  $codigo = date('d-m-y'); //codigo unico por día, viene a ser la fecha dia-mes-año
                  $columna = ''; //guardará la columna que está vacía para insertar ya sea ingreso o salida
                  $disable1="";
                  $disable2="";
                  $btn = 'btn btn-block btn-success btn-lg';
                  $dts = $oAsistencia->listar_asistencia_codigo($codigo, $_SESSION['usuario_id']);
                    if($dts['estado']==1){
                      $ing1 = $dts['data']['tb_asistencia_ing1']; //ingreso turno mañana
                      $sal1 = $dts['data']['tb_asistencia_sal1']; //salida turno mañana

                      $ing2 = $dts['data']['tb_asistencia_ing2']; //ingreso turno tarde
                      $sal2 = $dts['data']['tb_asistencia_sal2']; //salida turno tarde

                      $array = array("ing1" => $ing1, "sal1" => $sal1, "ing2" => $ing2, "sal2" => $sal2);
                      $disable2="disabled=''";
                      $disable1="";
                    }

                    if ($dts['estado'] == 0) {
                      $disable1="disabled";
                      $disable2="";
                    } 
                    else {
                      foreach ($array as $key => $value) {
                        if (empty($value) || $value == '00:00:00') {
                          $columna = $key;
                          break;
                        }
                      }
                      $tipo = 'MARCAR INGRESO';
                      $turno = 'Marque su Ingreso Por Favor';
                      if ($columna == 'sal1' || $columna == 'sal2') {
                        $tipo = 'MARCAR SALIDA';
                        $turno = 'Marque su Salida Por Favor';
                        $btn = 'btn btn-block btn-danger btn-lg';
                      }
                    }
                  $dts = NULL;
                ?>
                <p></p>
                <div class="box-body col-md-12">
                  <div class="form-group">
                    <label for="txt_asistencia_obs" class="control-label">Escribe aquí alguna Observación</label>
                    <textarea name="txt_asistencia_obs" id="txt_asistencia_obs" class="form-control input-sm mayus"></textarea>
                  </div>
                  <div class="">
                    <?php if($empresa_id == 2):
                        $gtd = strpos($hostname, 'gtd'); ?>
                      <?php if($gtd !== false): ?>
                        <button type="button" class="<?php echo $btn;?>" id="btn_asi_ing_registrar" href="javascript:void(0)" onclick="marcar_asistencia('actualizar','<?php echo $columna;?>')"><i class="fa fa-fw fa-sign-in"></i><?php echo $tipo;?></button>
                      <?php endif;?>
                      <?php if($gtd == false):?>
                        <button type="button" id="btn" class="btn btn-block btn-lg">Debes Marcar Asistencia desde Oficina</button>
                      <?php endif;?>
                    <?php endif;?>

                    <?php if($empresa_id == 1):?>	
                      <button type="button" class="<?php echo $btn;?>" id="btn_asi_ing_registrar" href="javascript:void(0)" onclick="marcar_asistencia('actualizar','<?php echo $columna?>')"><i class="fa fa-fw fa-sign-in"></i><?php echo $tipo;?></button>
                    <?php endif;?>	
                  </div>
                  </div>
              </div>

            </div>
          </div>
        </div>
        <div class="row">
          <br>
          <div class="col-md-12 col-xs-12">
            <div class="callout callout-info" id="prestamo_mensaje">
              <h4>Fecha y hora de hoy: <?php echo date('d-m-Y h:i a') . ' / ' . date('P').' || '.$client_ip.' || '.$hostname.' || emp: '.$empresa_id; ?></h4>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>
    
