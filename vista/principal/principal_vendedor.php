<?php
$fechap = new DateTime();
$fechap->modify('last day of this month');

$chart_fec1p = date('Y-m-01');
$chart_fec2p = $fechap->format('Y-m-d');
$año = date('Y');
//$año = '2018';
//$chart_fec1p = '2022-01-01';
//$chart_fec2p = '2022-01-31';


//require_once('vista/asistencia/Asistencia.class.php');
//$oAsistencia = new Asistencia();
require_once('vista/usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('vista/usuarioperfil/UsuarioPerfil.class.php');
$oUsuarioPerfil = new UsuarioPerfil();
require_once('Personal.class.php');
$oPersonal = new Personal();
require_once('vista/asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();

require_once('vista/funciones/funciones.php');
require_once('vista/funciones/fechas.php');

// Gerson (30-05-23)
$oUsuario->modificar_campo($_SESSION['usuario_id'], 'tb_empresa_id', $_SESSION['empresa_id'], 'INT');
//

$result = $oUsuario->mostrarUno($_SESSION['usuario_id']);
  if ($result['estado'] == 1) {
    $user = $result['data']['tb_usuario_nom'] . ' ' . $result['data']['tb_usuario_ape'];
    $foto = $result['data']['tb_usuario_fot'];
    $result1 = $oUsuarioPerfil->mostrarUno($result['data']['tb_usuarioperfil_id']);
    $perfil = $result1['data']['tb_usuarioperfil_nom'];
  }
$result = "";

$result2 = $oPersonal->listarcreditosusuario($chart_fec1p, $chart_fec2p, $_SESSION['usuario_id']);
  if ($result2['estado'] == 1) {
    $creditos = $result2['data']['creditos'];
    $monto = $result2['data']['monto'];
    $comision = $result2['data']['comision'];
  } else {
    $creditos = 0;
    $monto = 0;
    $comision = '0.00';
  }
$result2 = "";

/* GERSON (23-03-23) */
$usu_id = $_SESSION['usuario_id'];
$hoy = date('Y-m-d');
$mes_actual = date('m');
$anio_actual = date('Y');

$fecha_hoy = explode("-", $hoy);
$anio = $fecha_hoy[0];
$mes = $fecha_hoy[1];
$fecha_ini = $anio . '-' . $mes . '-01';
$fecha_fin = $hoy;

$dias_faltados = $oAsistencia->lista_dias_faltados($usu_id, $fecha_ini, $fecha_fin);
/*  */

$newfec1 = strtotime('-1 month', strtotime($chart_fec1p));
$newfec1 = date('Y-m-d', $newfec1);
$newfec2 = strtotime('-1 month', strtotime($chart_fec2p));
$newfec2 = date('Y-m-d', $newfec2);

$result3 = $oPersonal->listarcreditosusuario($newfec1, $newfec2, $_SESSION['usuario_id']);
  if ($result3['estado'] == 1) {
    $monto1 = $result3['data']['monto'];
    $meta = floatval($monto1) + floatval($monto1) * 0.20;
  }
$result3 = NULL;

$cumplido = 0;
if ($monto > 0 && $meta > 0)
  $cumplido = mostrar_moneda(($monto / $meta) * 100);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Principal
      <small>Bienvenido al sistema</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="./principal"><i class="fa fa-dashboard"></i> Principal</a></li>
      <!--li class="active">Dashboard</li-->
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <div id="div_modal_opcionesgc_asignar_form"> </div>
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <!-- DE HABILITARSE LA ASISTENCIA PASAR A 8 -->
        
        <div class="box box-widget widget-user shadow">
          <input type="hidden" name="hdd_año_id" id="hdd_año_id" value="<?php echo $año; ?>">
          <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $_SESSION['usuario_id']; ?>">

          <div class="widget-user-header bg-aqua-active">
            <h3 class="widget-user-username"><?php echo $user ?></h3>
            <h5 class="widget-user-desc"><?php echo $perfil ?></h5>
          </div>
          <div class="widget-user-image">
            <img class="img-circle" src="<?php echo $foto ?>" alt="User Avatar">
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-4 border-right">
                <div class="description-block">
                  <h5 class="description-header"><?php echo $creditos ?></h5>
                  <span class="description-text">CREDITOS</span>
                </div>

              </div>
              <div class="col-sm-4 border-right">
                <div class="description-block">
                  <h5 class="description-header"><?php echo mostrar_moneda($monto) ?></h5>
                  <span class="description-text">MONTO</span>
                </div>

              </div>

              <div class="col-sm-4">
                <div class="description-block">
                  <h5 class="description-header"><?php echo mostrar_moneda($comision) ?></h5>
                  <span class="description-text">COMISION</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- GERSON (23-03-23) -->
      <?php if ($dias_faltados['estado'] == 1) { ?>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="callout callout-danger">
              <h4>Alerta Colaborador</h4>
              <p class="h5">Presenta faltas sin justificar en el presente mes, comunicarse con el área encargada y regularizar sus marcaciones.</p>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
      
    <!-- AQUI SE MOSTRARÁ EL PROGRESO DEL VENDEDOR-->
    <div class="box">
      
      <div class="box-body no-padding">
        <input type="hidden" id="vendedor_usuario_id" value="<?php echo $usu_id;?>"/>
        <input type="hidden" id="vendedor_empresa_id" value="0"/>
        <div class="panel">
          <div class="panel-body">
            <form class="form-inline" role="form">
              <div class="form-group">
                <label class="control-label">Mes:</label>
                <select class="form-control form-control-sm" id="vendedor_mes">
                  <?php echo devuelve_option_nombre_meses($mes_actual);?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">Año:</label>
                <select class="form-control form-control-sm" id="vendedor_anio">
                  <?php echo devuelve_option_anios_garveh(2020, $anio_actual, 4);?>
                </select>
              </div>
            </form>
          </div>
        </div>

        <div class="" id="vendedor_progreso">
          
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="box" style="border-top: none;">
          <div class="box-header bg-teal p-4"><b>ESTADO DE CUOTA POR CLIENTE</b></div>
          <div class="box-body">
            <div class="container-fluid">
              <div class="row mt-4 mb-4">
                <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12">
                  <div class="form-group">
                    <label for="txt_fil_ecd_1" class="text-grey">Fecha:</label>
                    <div class="input-group">
                      <div class='input-group date' id='datetimepickerECD1'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ecd_1" id="txt_fil_ecd_1" value="<?php echo date('d-01-Y'); ?>" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar text-blue"></span>
                        </span>
                      </div>
                      <span class="input-group-addon">-</span>
                      <div class='input-group date' id='datetimepickerECD2'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ecd_2" id="txt_fil_ecd_2" value="<?php echo date('d-m-Y'); ?>" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar text-blue"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                  <div class="form-group">
                    <label for="cbo_estado" class="text-grey">Estado:</label> <br>
                    <select class="form-control input-sm" name="cbo_estado" id="cbo_estado">
                      <option value="0">TODOS LOS ESTADOS</option>
                      <option value="1">PENDIENTE</option>
                      <option value="3">PAGO PARCIAL</option>
                      <option value="2">PAGADO</option>
                    </select>
                  </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                  <div class="form-group">
                    <label for="cbo_credtipo" class="text-grey">Tipo de Crédito:</label> <br>
                    <select class="form-control input-sm" name="cbo_credtipo" id="cbo_credtipo">
                      <!-- <option value="1">CRED. MENOR</option> -->
                      <option value="3" selected>GARANTÍA VEHICULAR</option>
                    </select>
                  </div>
                </div>
                <?php if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuarioperfil_id'] == 5 ): ?>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                  <div class="form-group">
                    <label for="cmb_sede_id_recc" class="text-grey">SEDE:</label> <br>
                    <select class="form-control input-sm" id="cmb_sede_id_recc" name="cmb_sede_id_recc">
                        <option value="0">TODAS LAS SEDES</option>
                        <option value="1">IPDN - BOULEVARD</option>
                        <option value="2">IPDN - MALL AVENTURA</option>
                    </select>
                  </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                  <div class="form-group">
                    <label for="cmb_usuario_id" class="text-grey">Usuario:</label> <br>
                    <select class="form-control input-sm selectpicker" name="cmb_usuario_id" id="cmb_usuario_id">
                      <?php // require_once('vista/usuario/usuario_select.php');?>
                    </select>
                  </div>
                </div>
                <?php endif;?> 
                <div class="col-lg-1 col-md-6 col-sm-12 col-xs-12 text-right">
                  <div class="form-group">
                    <label for="cbo_credtipo" class="text-grey">&nbsp;</label> <br>
                    <button type="button" class="btn btn-primary btn-sm" onclick="reporte_estado_cuota_cliente_tabla()"><i class="fa fa-search fa-fw"></i> Buscar</button>
                  </div>
                </div>
              </div>
              <div class="callout callout-info" id="reporte_ecd_mensaje_tbl" style="display: none;">
                <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
              </div>
              <div id="tblcuotacliente"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </section>
</div>

<div id="div_modal_cliente_form"> </div>