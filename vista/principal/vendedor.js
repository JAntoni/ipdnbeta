var arraygraficovendedor = [];
google.charts.load("current", { packages: ["corechart"] });

$(document).ready(function () {
  //CARGAR GRAFICOS
  jQuery('#wpf-loader-two').delay(200).fadeOut('slow');
  //cargar_grafico();
  console.log('yaaaas 55');

  $('#modal_asistencia').modal('show');

  //generar_calendario();
  notificar_faltas_mensual();

  //cargar el progreso del vendedor
  vendedor_progreso();

  reporte_estado_cuota_cliente_tabla();
  
  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });

  usuario_select(0);

  $("#cmb_usuario_id").selectpicker('refresh');

  $('#cmb_sede_id_recc').change(function() {
    usuario_select(-1);
  });
});

function marcar_asistencia(act, tip){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"asistencia/asistencia_reg.php",
    async:true,
    dataType: "json",                      
    data: ({
      action: act,
      tipo: tip,
      asistencia_obs: $('#txt_asistencia_obs').val()
    }),
    beforeSend: function() {

    },
    success: function(data){
      if(parseInt(data.estado) == 1){
        swal_success("ASISTENCIA",data.msj, 3000);
        $('#modal_asistencia').modal('hide');
        setInterval(location.reload(),5000);
      }
      else
        swal_error("SISTEMA",data.msj,5000);
    },
    complete: function(data){
      if(data.status != 200){
        console.log(data);
        swal_error("SISTEMA",data,5000);
      }
    }
  });
}

function cargar_grafico() {
  var año = $("#hdd_año_id").val();
  var usuario_id = $("#hdd_usuario_id").val();
  console.log(año + usuario_id);

  $.ajax({
    type: "POST",
    url: VISTA_URL + "principal/vendedor_controller.php",
    async: true,
    dataType: "json",
    data: {
      año: año,
      usuario_id: usuario_id,
      grafico: "barras_vendedor",
    },
    success: function (data) {
      console.log(data);
      var arraydatos = [];
      for (let i = 0; i < data.length; i++) {
        // Acceder a cada elemento de resultados y operaciones, según el primer ciclo
        let monto = parseInt(data[i][1]);
        arraydatos.push([data[i][0], monto]);
      }
      arraydatos.unshift(["Mes", "Monto"]);
      arraygraficovendedor = arraydatos;
      console.log(arraygraficovendedor);
      google.charts.setOnLoadCallback(barrasanual);
    },
  });
}

function barrasanual() {
  console.log("aquiiiiiiiiiii ");
  var data = google.visualization.arrayToDataTable(arraygraficovendedor);
  console.log(data);
  var view = new google.visualization.DataView(data);
  view.setColumns([
    0,
    { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" },
    1,
  ]);
  console.log(view);
  var options = {
    chart: {
      title: "VENTAS POR MES",
    },
  };
  var chart = new google.visualization.ColumnChart(
    document.getElementById("barrasgeneral")
  );
  chart.draw(view, options);
}

/* GERSON (16-03-23) */
function notificar_faltas_mensual() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "asistencia/asistencia_reg.php",
    async: true,
    dataType: "json",
    data: {
      action: "detectar_faltas",
      falta_obs: "FALTA DETECTADA POR SISTEMA, AUSENCIA DE MARCAJE",
    },
    beforeSend: function () {},
    success: function (data) {
      /* if(parseInt(data.estado) == 1){
          swal_success("ASISTENCIA",data.msj, 3000);
          $('#modal_asistencia').modal('hide');
          setInterval(location.reload(),5000);
        }
        else
          swal_error("SISTEMA",data.msj,5000); */
    },
    complete: function (data) {
      /* if(data.status != 200){
          console.log(data);
          swal_error("SISTEMA",data,5000);
        } */
    },
  });
}

//juan 18-08-2023
function vendedor_progreso() {
  empresa_id = $('#vendedor_empresa_id').val();
  usuario_id = $('#vendedor_usuario_id').val();
  mes = $('#vendedor_mes').val();
  anio = $('#vendedor_anio').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "principal/vendedor_progreso.php",
    async: true,
    dataType: "html",
    data: {
      empresa_id: empresa_id,
      usuario_id: usuario_id,
      mes: mes,
      anio: anio,
    },
    beforeSend: function () {},
    success: function (data) {
      $("#vendedor_progreso").html(data);
    },
    complete: function (data) {},
  });
}
/*  */

// prueba para generar horario mensual DE FORMA MANUAL
/* function generar_calendario(){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"asistencia/generar_calendario_usuarios.php",
      async:true,
      dataType: "json",                      
      data: ({
      }),
      beforeSend: function() {

      },
      success: function(data){
        console.log('HECHO');
      },
      complete: function(data){
      }
    });
  } */


// ------------------------------------------------------------------------- //

function reporte_estado_cuota_cliente_tabla(){

  $('#tblcuotacliente').empty();

  var fecha1 = $('#txt_fil_ecd_1').val();
  var fecha2 = $('#txt_fil_ecd_2').val();
  var estado = $('#cbo_estado').val();
  var credtipo = $('#cbo_credtipo').val();
  var usuario = $('#cmb_usuario_id').val();

  if( fecha1 == null )
  {
    fecha1 = '0';
  }

  if( fecha2 == null )
  {
    fecha2 = '0';
  }

  if( estado == null )
  {
    estado = '0';
  }

  if( credtipo == null )
  {
    credtipo = '0';
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"principal/estado_cuota_cliente_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      p_fecha1: fecha1,
      p_fecha2: fecha2,
      p_estado: estado,
      p_credtipo: credtipo,
      p_usuario: usuario
    }),
    beforeSend: function() {
      $('#reporte_ecd_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#tblcuotacliente').html(data);
      $('#reporte_ecd_mensaje_tbl').hide(300);
      estilos_datatable('tbl_reporte_estado_cuota_cliente');
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#reporte_ecd_mensaje_tbl').html('ERROR: ' + data.responseText);
    }
  });
}

function cliente_form2(usuario_act, cliente_id) {
  // var credito_cliente_id = Number($("#hdd_cre_cli_id").val());
  // console.log(credito_cliente_id);

  if (parseInt(cliente_id) <= 0 && usuario_act == "M") {
    alerta_warning("Información", "Búsqueda inválida");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: cliente_id,
      vista: "principal",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "principal";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {
      console.log("busqueda culminado correctamente");
    },
    error: function (data) {
      alerta_error("ERROR", data.responseText);
      console.log(data);
    },
  });
}

function usuario_select(id) {
  var sede_id = $("#cmb_sede_id_recc").val();
  $("#cmb_usuario_id").empty();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "/usuario/usuario_select.php",
    async: true,
    dataType: "html",
    data: {
      usuario_columna: 'tb_empresa_id',
      param_tip:'INT',
      usuario_valor: sede_id
    },
    beforeSend: function () {
      $("#cmb_usuario_id").val("<option>' cargando '</option>");
    },
    success: function (data) {
      $("#cmb_usuario_id").empty();
      $("#cmb_usuario_id").html(data);      
      $("#cmb_usuario_id").selectpicker('refresh');
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      $("#creditogarveh_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CANCHA: " + data.responseText
      );
    },
  });
}

function opcionesgc_asignar_form(usuario_act, cliente_id, credito_id, temp_cobranza_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"principal/fragment_consultar_datos_cliente.php",
    async: true,
		dataType: "html",
		data: ({
      p_action: usuario_act, // PUEDE SER: L, I, M , E
      p_cliente_id: cliente_id,
      p_credito_id: credito_id,
      p_temp_cobranza_id: temp_cobranza_id,
      p_modulo_nombre: 'principal'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_opcionesgc_asignar_form').html(data);
      	$('#modal_registro_asignaropciongc').modal('show');
        modal_height_auto('modal_registro_asignaropciongc');
      	modal_hidden_bs_modal('modal_registro_asignaropciongc', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}