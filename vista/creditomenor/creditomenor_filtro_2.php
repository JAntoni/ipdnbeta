<?php
$filtro_fec1 = date('01-01-Y');
$filtro_fec2 = date('d-m-Y');
$empresa_id=$_SESSION['empresa_id'];
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_creditomenor_filtro" class="form-inline" role="form">
                    <button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</button>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $filtro_fec1; ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $filtro_fec2; ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control input-sm mayus" id="cbm_empresa_id" name="cbm_empresa_id">
                            <?php
                            
                            require_once 'vista/empresa/empresa_select.php';
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="cmb_fil_usuario_id" id="cmb_fil_usuario_id" class="form-control input-sm">
                           <!--<option value="<?php echo $_SESSION['usuario_id'] ?>" style="font-weight: bold;" ><?php echo $_SESSION['usuario_ape'].' '.$_SESSION['usuario_nom'];?></option>--> 
                        </select>
                    </div>
                    <button type="button" class="btn btn-info btn-sm" onclick="creditomenor_tabla()"><i class="fa fa-search"></i></button>
                    <!--div class="form-group">
                      <input type='text' class="form-control input-sm" name="txt_filtro_cliente_nom" id="txt_filtro_cliente_nom" placeholder="Busca un cliente aquí" size="30" />
                      <input type='hidden' name="hdd_filtro_cliente_id" id="hdd_filtro_cliente_id"/>
                    </div-->
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12" id="creditomenor_mensaje_opc" style="padding-top: 5px;"> 
    </div>
</div>