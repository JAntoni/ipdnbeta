
function cuotapago_imppos_datos3(cuotapago_id){
    var codigo=cuotapago_id;
    window.open("http://ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);
//  window.open("http://localhost/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);
}

function cuotapago_imppos_datos(cuotapago_id){
  //$.alert('Contenido aquí', 'Titulo aqui');
  $.confirm({
    icon: 'fa fa-print',
    title: 'Imprimir',
    content: '¿Desea imprimir voucher?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        //ejecutamos AJAX
        $.ajax({
          type: "POST",
          url: VISTA_URL+"cuotapago/cuotapago_imppos_datos.php",
          async: true,
          dataType: "JSON",
          data: ({
            cuotapago_id: cuotapago_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Obteniendo datos...');
            $('#modal_mensaje').modal('show');
          },
          success: function(data){
            cuotapago_imppos_ticket(data);
          },
          complete: function(data){
            console.log(data);
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
//            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}
function cuotapago_imppos_ticket(datos){  
  $.ajax({
    type: "POST",
    url:  VISTA_URL+"cuotapago/cuotapago_imppos_ticket.php",
//    url: "http://127.0.0.1/prestamosdelnorte/app/modulos/cuotapago/cuotapago_imppos_ticket.php",
    async:true,
    dataType: "html",                      
    data: datos,
    beforeSend: function() {
      $('#h3_modal_title').text('Imprimiendo...');
    },
    success: function(html){
      $('#modal_mensaje').modal('hide');
    },
    complete: function(data){
      //console.log(data);
    }
  });
}
function cuotapago_anular(cuotapago_id){
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Anular',
    content: '¿Desea anular este pago?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"cuotapago/cuotapago_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "anular",
            cuotapago_id: cuotapago_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Anulando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              cuotapago_pagos_tabla();
              
              var hdd_form=$('#hdd_form').val();
              if(hdd_form=='vencimiento'){
                vencimiento_tabla();
              }
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}
function cuotapago_detalle(cuotapago_id){  
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cuotapago/cuotapago_detalle.php",
    async: true,
    dataType: "html",
    data: ({
      cuotapago_id: cuotapago_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Obteniendo datos...');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_cuotapago_detalle').html(data);
      $('#modal_cuotapago_detalle').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_cuotapago_detalle'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_cuotapago_detalle', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}
function cuotapago_pagos_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cuotapago/cuotapago_pagos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: $('#hdd_credito_id').val(),
      creditotipo_id: $('#hdd_creditotipo_id').val()
    }),
    beforeSend: function() {
      $('#creditomenor_pagos_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#creditomenor_pagos_mensaje_tbl').hide(300);
      $('#creditomenor_pagos_tabla').html(data);
    },
    complete: function(data){
      
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}
function morapago_pagos_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"morapago/morapago_pagos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: $('#hdd_credito_id').val(),
      creditotipo_id: $('#hdd_creditotipo_id').val()
    }),
    beforeSend: function() {
      $('#creditomenor_pagos_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#creditomenor_pagos_mensaje_tbl').hide(300);
      $('#creditomenor_pagos_tabla').html(data);
    },
    complete: function(data){
      
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}
/* GERSON (12-01-25) */
function solicitar_anular(cuotapago_id, credito_id, creditotipo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"permisoanulacion/solicitar_anulacion_form.php",
    async: true,
    dataType: "html",
    data: ({
      modid: cuotapago_id,
      origen: 1, // 1=cuota(cmenor), 2=cuotadetalle(cgarveh), 3=ingreso, 4=egreso
      credito_id: credito_id,
      creditotipo_id: creditotipo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Obteniendo datos...');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_solicitar_anulacion').html(data);
      $('#modal_solicitar_anulacion').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_solicitar_anulacion'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_solicitar_anulacion', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}
/*  */
$(document).ready(function() {
  cuotapago_pagos_tabla();
});