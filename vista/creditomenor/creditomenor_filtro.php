<?php
$filtro_fec1 = date('01-m-Y');
$filtro_fec2 = date('d-m-Y');
$empresa_id=$_SESSION['empresa_id'];
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_creditomenor_filtro" class="form-inline" role="form">
                    <input type="hidden" id="hdd_usuariogrupo_id" value="<?php echo $_SESSION['usuariogrupo_id'];?>"/>
                    <?php if(intval($_SESSION['usuariogrupo_id']) == 2):?>
                        <button type="button" class="btn btn-default btn-sm" onclick="creditomenor_form('I', 0, '')"><i class="fa fa-plus"></i> Nuevo</button>
                        <button type="button" class="btn btn-success btn-sm" onclick="creditomenor_reporte()"><i class="fa fa-plus"></i> Reporte Créditos</button>
                    <?php endif;?>
                    <button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0, 'simulador')"><i class="fa fa-plus"></i> Simulador</button>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $filtro_fec1; ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $filtro_fec2; ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control input-sm mayus" id="cbm_empresa_id" name="cbm_empresa_id">
                            <?php
                                $perfiles = [1, 3, 4, 5]; //1 admin, 2 ventas, 3 legal, 4 cobranza, 5 contabilidad
                                if (in_array($_SESSION['usuarioperfil_id'], $perfiles)) { 
                                    require_once 'vista/empresa/empresa_select.php';
                                }
                                else {?>
                                    <option value="<?php echo $_SESSION['empresa_id'] ?>" style="font-weight: bold;" ><?php echo $_SESSION['empresa_nombre']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="cmb_fil_usuario_id" id="cmb_fil_usuario_id" class="form-control form-control-sm selectpicker" data-live-search="true" data-max-options="1" data-size="12">
                           <option value="<?php echo $_SESSION['usuario_id'] ?>" style="font-weight: bold;" ><?php echo $_SESSION['usuario_ape'].' '.$_SESSION['usuario_nom'];?></option> 
                        </select>
                    </div>
                    <button type="button" class="btn btn-info btn-sm" onclick="creditomenor_tabla()"><i class="fa fa-search"></i></button>
                    <!-- <div class="form-group">
                        <button class="btn btn-github btn-sm" type="button" onclick="tasacionsug_form()">Tasación garantía</button>
                    </div> -->
                    <!--div class="form-group">
                      <input type='text' class="form-control input-sm" name="txt_filtro_cliente_nom" id="txt_filtro_cliente_nom" placeholder="Busca un cliente aquí" size="30" />
                      <input type='hidden' name="hdd_filtro_cliente_id" id="hdd_filtro_cliente_id"/>
                    </div-->
                </form>
                <!-- <div class="row "> -->
                    
                <!-- </div> -->
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12" id="creditomenor_mensaje_opc" style="padding-top: 5px;"> 
    </div>
</div>
