<?php
  require_once('../../core/usuario_sesion.php');
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 admin, 3 ventas

  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'creditomenor/Creditomenor.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../creditomenor/Creditomenor.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  $oCredito = new Creditomenor();
  $filtro_fec1 = (isset($_POST['txt_reporte_fec1']))? fecha_mysql($_POST['txt_reporte_fec1']) : date('Y-01-01');
  $filtro_fec2 = (isset($_POST['txt_reporte_fec2']))? fecha_mysql($_POST['txt_reporte_fec2']) : date('Y-m-d');
  $empresa_id = 0; 
  $usuario_id = 0;
  $cliente_id = 0;
  $fecha_hoy = date('Y-m-d');

?>
<div class="table-responsive">
  <table id="tb_creditomenor_reporte" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>CLIENTE</th>
        <th class="no-sort">TELEFONO 1</th>
        <th class="no-sort">TELEFONO 2</th>
        <th class="no-sort">TELEFONO 3</th>
        <th>MONTO</th>
        <th>INT(%)</th>
        <th>ASESOR</th>
        <th>UBICACION</th>
        <th>ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        $result = $oCredito->filtro_creditos($filtro_fec1, $filtro_fec2, $empresa_id, $usuario_id, $cliente_id);
          if($result['estado'] == 1){
            foreach ($result['data'] as $key => $value) {
              $empresa = 'Mall Aventura';
              if($value['empresa_cm'] == 2)
                $empresa = 'Boulevard';

                $estado = '';

              if (intval($value['tb_credito_est']) == 1) {
                $estado = 'Pendiente';
              }
              if(intval($value['tb_credito_est']) == 2) {
                $estado = 'Desembolsar';
              }
              if(intval($value['tb_credito_est']) == 3) {
                $estado = 'Vigente';
              }
              if(intval($value['tb_credito_est']) == 4) {
                $estado = 'Liquidado';
              }
              if(intval($value['tb_credito_est']) == 5) {
                $estado = 'Remate';
              }
              if(intval($value['tb_credito_est']) == 6) {
                $estado = 'Vendido';
              }
              if(intval($value['tb_credito_est']) == 7) {
                $estado = 'Por Entregar';
              }

              echo '
                <tr>
                  <td>'.$value['tb_credito_id'].'</td>
                  <td>'.$value['tb_cliente_nom'].'</td>
                  <td>'.$value['tb_cliente_cel'].'</td>
                  <td>'.$value['tb_cliente_tel'].'</td>
                  <td>'.$value['tb_cliente_telref'].'</td>
                  <td>'.$value['tb_credito_preaco'].'</td>
                  <td>'.$value['tb_credito_int'].'</td>
                  <td>'.$value['tb_usuario_nom'].'</td>
                  <td>'.$empresa.'</td>
                  <td>'.$estado.'</td>
                </tr>
              ';
            }
            
          }
        $result = NULL;
      ?>
    </tbody>
  </table>
</div>