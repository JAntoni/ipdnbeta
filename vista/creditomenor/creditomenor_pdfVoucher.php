
<?php

error_reporting(0);

define('FPDF_FONTPATH', 'font/');

require_once('../../static/tcpdf/tcpdf.php');


require_once ("../empresa/Empresa.class.php");
$oEmpresa = new Empresa();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();

require_once ("../impresora/Impresora.class.php");
$oImpresora = new Impresora();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

//$cuotapago_id = intval(37801);
//$cuotapago_id = intval(37908);
//$cuotapago_id = intval(37986);
$cuotapago_id = $_GET['id'];
//$empresa = $_GET['empresa'];
//	$cuotapago_id = intval($_POST['cuotapago']);

$result = $oCuotapago->mostrarUno($cuotapago_id);
if ($result['estado'] == 1) {
    $cuotapago_reg = mostrar_fecha_hora($result['data']['tb_cuotapago_reg']);
    $usuario_usureg = $result['data']['tb_cuotapago_usureg'];
    $usuario_nom = $result['data']['tb_usuario_nom'];
    $modulo_id = $result['data']['tb_modulo_id']; // 1 cuota, 2 cuotadetalle
    $cuotapago_modid = $result['data']['tb_cuotapago_modid']; // id de la cuota o cuotadetalle
    $cuotapago_fec = mostrar_fecha($result['data']['tb_cuotapago_fec']);
    $cuotapago_mon = $result['data']['tb_cuotapago_mon'];
    $cuotapago_tipcam = $result['data']['tb_cuotapago_tipcam'];
    $cuotapago_moncam = $result['data']['tb_cuotapago_moncam'];
    $moneda_id = $result['data']['tb_moneda_id'];
    $moneda_nom = $result['data']['tb_moneda_nom'];
    $cuotapago_est = $result['data']['tb_cuotapago_est'];
    
}
$result = NULL;

//si el modulo_id =1 -> cuota
if (intval($modulo_id) == 1) {
    $result = $oCuota->mostrarUno($cuotapago_modid);
    if ($result['estado'] == 1) {
        $cuota_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
        $cuota_id = $result['data']['tb_cuota_id'];
        $creditotipo_id = $result['data']['tb_creditotipo_id']; //1 creditomenor, 2 ASIVEH, 3 GARVEH, 4 HIPOTECARIO
        $credito_id = $result['data']['tb_credito_id'];
        $cuota_num = $result['data']['tb_cuota_num'];
        $cuota_cuo = $result['data']['tb_cuota_cuo'];
        $cuota_persubcuo = $result['data']['tb_cuota_persubcuo'];
        $cuota_amortizacion = mostrar_moneda($result['data']['tb_cuota_amo']);
        $cuota_interes = mostrar_moneda($result['data']['tb_cuota_int']);
    }
    $result = NULL;
}
if (intval($modulo_id) == 2) {
    $result = $oCuotadetalle->mostrarUno($cuotapago_modid);
    if ($result['estado'] == 1) {
        $cuota_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
        $cuota_id = $result['data']['tb_cuota_id'];
        $creditotipo_id = $result['data']['tb_creditotipo_id']; //1 creditomenor, 2 ASIVEH, 3 GARVEH, 4 HIPOTECARIO
        $credito_id = $result['data']['tb_credito_id'];
        $cuota_num = $result['data']['tb_cuota_num'];
        $cuota_cuo = $result['data']['tb_cuota_cuo'];
        $cuota_persubcuo = $result['data']['tb_cuota_persubcuo'];
        $cuota_amortizacion = mostrar_moneda($result['data']['tb_cuota_amo']);
        $cuota_interes = mostrar_moneda($result['data']['tb_cuota_int']);
    }
    $result = NULL;
    
    $result = $oCuota->mostrarUno($cuota_id);
    if ($result['estado'] == 1) {
        $cuota_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
        $cuota_id = $result['data']['tb_cuota_id'];
        $creditotipo_id = $result['data']['tb_creditotipo_id']; //1 creditomenor, 2 ASIVEH, 3 GARVEH, 4 HIPOTECARIO
        $credito_id = $result['data']['tb_credito_id'];
        $cuota_num = $result['data']['tb_cuota_num'];
        $cuota_cuo = $result['data']['tb_cuota_cuo'];
        $cuota_persubcuo = $result['data']['tb_cuota_persubcuo'];
        $cuota_amortizacion = mostrar_moneda($result['data']['tb_cuota_amo']);
        $cuota_interes = mostrar_moneda($result['data']['tb_cuota_int']);
    }
    $result = NULL;
}

$ingreso_imp = 0;
//parametro 30 para modulo id, 30 = cuotapago
$result = $oIngreso->mostrar_ingresos_modulo(30, $cuotapago_id);
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $ingreso_detex = $value['tb_ingreso_detex'];
        $ingreso_id = $value['tb_ingreso_id'];
        $ingreso_det = $value['tb_ingreso_det'];
        $empresa = $value['tb_empresa_id'];
        if ($cuotapago_mon <= 0)
            $ingreso_imp = $value['tb_ingreso_imp'];
        break;
    }
}
$result = NULL;

$result = $oEmpresa->mostrarUno($empresa);
if ($result['estado'] == 1) {
    $empresa_ruc = $result['data']['tb_empresa_ruc'];
    $empresa_nomcom = $result['data']['tb_empresa_nomcom'];
    $empresa_razsoc = $result['data']['tb_empresa_razsoc'];
    $empresa_dir = $result['data']['tb_empresa_dir'];
    $empresa_dir2 = $result['data']['tb_empresa_dir2'];
    $empresa_tel = $result['data']['tb_empresa_tel'];
    $empresa_ema = $result['data']['tb_empresa_ema'];
    $empresa_fir = $result['data']['tb_empresa_fir'];
}
$result = NULL;
$clientecaja = 0;

if ($creditotipo_id == 1) {
    require_once("../creditomenor/Creditomenor.class.php");
    $oCredito = new Creditomenor();

    $result = $oCredito->mostrarUno($credito_id);
    if ($result['estado'] == 1) {
        $credito_numcuomax = $result['data']['tb_credito_numcuomax'];

        $cliente_tip = $result['data']['tb_cliente_tip'];
        $cliente_id = $result['data']['tb_cliente_id'];
        if ($cliente_tip == 1) {
            $cliente_doc = $result['data']['tb_cliente_doc'];
            $cliente_nom = $result['data']['tb_cliente_nom'];
            $cliente_dir = $result['data']['tb_cliente_dir'];
        }
        if ($cliente_tip == 2) {
            $cliente_doc = $result['data']['tb_cliente_empruc'];
            $cliente_nom = $result['data']['tb_cliente_emprs'];
            $cliente_dir = $result['data']['tb_cliente_empdir'];
        }

        $cliente = $cliente_nom . ' | ' . $cliente_doc;
    }
    $result = NULL;

    $empresa_razsoc = "INVERSIONES Y PRESTAMOS DEL NORTE SAC";
    $empresa_ruc = "20600575679";
    //para credito menor el detalle será el mismo detalle de ingreso
    $detalle = $ingreso_det;
}
if ($creditotipo_id == 2) {
    require_once("../creditoasiveh/Creditoasiveh.class.php");
    $oCredito = new Creditoasiveh();

    $result = $oCredito->mostrarUno($credito_id);
    if ($result['estado'] == 1) {
        $credito_numcuomax = $result['data']['tb_credito_numcuomax'];

        $cliente_tip = $result['data']['tb_cliente_tip'];
        $cliente_id = $result['data']['tb_cliente_id'];
        if ($cliente_tip == 1) {
            $cliente_doc = $result['data']['tb_cliente_doc'];
            $cliente_nom = $result['data']['tb_cliente_nom'];
            $cliente_dir = $result['data']['tb_cliente_dir'];
        }
        if ($cliente_tip == 2) {
            $cliente_doc = $result['data']['tb_cliente_empruc'];
            $cliente_nom = $result['data']['tb_cliente_emprs'];
            $cliente_dir = $result['data']['tb_cliente_empdir'];
        }

        $cliente = $cliente_nom . ' | ' . $cliente_doc;
    }
    $result = NULL;

    $empresa_razsoc = "INVERSIONES Y PRESTAMOS DEL NORTE SAC";
    $empresa_ruc = "20600575679";
    //para credito menor el detalle será el mismo detalle de ingreso
    $detalle = $ingreso_det;
}

if ($creditotipo_id == 3) {
    require_once("../creditogarveh/Creditogarveh.class.php");
    $oCredito = new Creditogarveh();

    $result = $oCredito->mostrarUno($credito_id);
    if ($result['estado'] == 1) {
        $credito_numcuomax = $result['data']['tb_credito_numcuomax'];

        $cliente_tip = $result['data']['tb_cliente_tip'];
        $cliente_id = $result['data']['tb_cliente_id'];
        if ($cliente_tip == 1) {
            $cliente_doc = $result['data']['tb_cliente_doc'];
            $cliente_nom = $result['data']['tb_cliente_nom'];
            $cliente_dir = $result['data']['tb_cliente_dir'];
        }
        if ($cliente_tip == 2) {
            $cliente_doc = $result['data']['tb_cliente_empruc'];
            $cliente_nom = $result['data']['tb_cliente_emprs'];
            $cliente_dir = $result['data']['tb_cliente_empdir'];
        }

        $cliente = $cliente_nom . ' | ' . $cliente_doc;
    }
    $result = NULL;

    $empresa_razsoc = "INVERSIONES Y PRESTAMOS DEL NORTE SAC";
    $empresa_ruc = "20600575679";
    //para credito menor el detalle será el mismo detalle de ingreso
    $detalle = $ingreso_det;
}

if ($creditotipo_id == 4) {
    require_once("../creditohipo/Creditohipo.class.php");
    $oCredito = new Creditohipo();

    $result = $oCredito->mostrarUno($credito_id);
    if ($result['estado'] == 1) {
        $credito_numcuomax = $result['data']['tb_credito_numcuomax'];

        $cliente_tip = $result['data']['tb_cliente_tip'];
        $cliente_id = $result['data']['tb_cliente_id'];
        if ($cliente_tip == 1) {
            $cliente_doc = $result['data']['tb_cliente_doc'];
            $cliente_nom = $result['data']['tb_cliente_nom'];
            $cliente_dir = $result['data']['tb_cliente_dir'];
        }
        if ($cliente_tip == 2) {
            $cliente_doc = $result['data']['tb_cliente_empruc'];
            $cliente_nom = $result['data']['tb_cliente_emprs'];
            $cliente_dir = $result['data']['tb_cliente_empdir'];
        }

        $cliente = $cliente_nom . ' | ' . $cliente_doc;
    }
    $result = NULL;

    $empresa_razsoc = "INVERSIONES Y PRESTAMOS DEL NORTE SAC";
    $empresa_ruc = "20600575679";
    //para credito menor el detalle será el mismo detalle de ingreso
    $detalle = $ingreso_det;
}

//impresora, muestra los datos de la impresora por defecto 1
$result = $oImpresora->mostrarUno(1);
if ($result['estado'] == 1) {
    $impresora_nom = $result['data']['tb_impresora_nom'];
    $impresora_nomloc = $result['data']['tb_impresora_nomloc'];
    $impresora_ser = $result['data']['tb_impresora_ser'];
    $impresora_url = $result['data']['tb_impresora_url'];
    $impresora_ip = $result['data']['tb_impresora_ip'];
}
$result = NULL;

class MYPDF extends TCPDF {

    public function Header() {
        
    }

}

//$medidas = array(72, 200);
// create new PDF document
//MAXIMO CABE 420 LETRAS
//$detalle = 'PAGO DE CUOTA CM-1783 N° CUOTA: 1/1 (2). PAGO ABONADO EN EL BANCO, EN LA SIGUENTE CUENTA: CUENTA IPDN SAC '
//        . 'BCP SOLES, LA FECHA DE DEPÓSITO FUE: 27-01-2022, LA FECHA DE VALIDACIÓN FUE: 27-01-2022, EL N° DE OPERACIÓN '
//        . 'FUE: 07462152, EL MONTO DEPOSITADO FUE: S/. 400.00, LA COMISIÓN DEL BANCO FUE: S/. 0.00, '
//        . 'EL MONTO VALIDADO FUE: S/. 400.00 Y CON CAMBIO DE MONEDA EL MONTO PAGADO A LA '
//        . 'CUOTA FUE: S/. 400.00 HAHHA AH H HH TTHH HHH HH HAHAHSH SHSHSH SHSHS JASNJ ASJN AJKSN AJKS AJKSNSS NSJS NJSN JS NJSNJS'
//        . 'PAGO DE CUOTA CM-1783 N° CUOTA: 1/1 (2). PAGO ABONADO EN EL BANCO, EN LA SIGUENTE CUENTA: CUENTA IPDN SAC '
//        . 'BCP SOLES, LA FECHA DE DEPÓSITO FUE: 27-01-2022, LA FECHA DE VALIDACIÓN FUE: 27-01-2022, EL N° DE OPERACIÓN '
//        . 'FUE: 07462152, EL MONTO DEPOSITADO FUE: S/. 400.00, LA COMISIÓN DEL BANCO FUE: S/. 0.00, '
//        . 'EL MONTO VALIDADO FUE: S/. 400.00 Y CON CAMBIO DE MONEDA EL MONTO PAGADO A LA '
//        . 'CUOTA FUE: S/. 400.00 HAHHA AH H HH TTHH HHH HH HAHAHSH SHSHSH SHSHS JASNJ ASJN AJKSN AJKS AJKSNSS NSJS NJSN JS NJSNJS';

$contDetalle = strlen($detalle);
$resta = $contDetalle - 140;
$adicionar = 0;
if($resta > 1){
    $demas = $resta / 35;
    if($demas <= 1)
        $adicionar = 5;
    else {
        $adicionar = $demas * 5;
        $adicionar = round($adicionar);
    }
        
    
}

$heightPDF = 180 + $adicionar; 

$medidas = array(72, $heightPDF);
//$medidas = array(80,220);

$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);

$pdf->SetAuthor('cristhian.cyga@gmail.com');

$pdf->SetTitle($title);

$pdf->SetSubject('cristhian.cyga@gmail.com');

$pdf->SetKeywords('cristhian.cyga@gmail.com');

// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins

//$pdf->SetMargins(0, 0, 0); // left top right
$pdf->SetMargins(5,2, 5); // left top right

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//set auto page breaks

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
//set some language-dependent strings

$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// add a page

$pdf->AddPage();
//$detalle = 'PAGO DE CUOTA CM-1783 N° CUOTA: 1/1 (2). PAGO ABONADO EN EL BANCO, EN LA SIGUENTE CUENTA: CUENTA IPDN SAC BCP SOLES, LA FECHA DE DEPÓSITO FUE: 27-01-2022, LA FECHA DE VALIDACIÓN FUE: 27-01-2022, EL N° DE OPERACIÓN FUE: 07462152, EL MONTO DEPOSITADO FUE: S/. 400.00, LA COMISIÓN DEL BANCO FUE: S/. 0.00, EL MONTO VALIDADO FUE: S/. 400.00 Y CON CAMBIO DE MONEDA EL MONTO PAGADO A LA CUOTA FUE: S/. 400.00 HAHHA AH H HH TT';
//$contDetalle = strlen($detalle);
$html .= '
                
                <dl>
                <br>
                    <table width="100%">	

                        <tr style="text-align:justify;">

                                <td style="text-align:left; width: 25%;"></td>

                                <td style="text-align:center; width: 50%;"><img src="../../public/images/logopres.png"></td>

                                <td style="text-align:left; width: 25%;"></td>

                        </tr>

                    </table>
                    <br>
                    <dt style="color: black; text-align:center; font-weight: bold; font-size: 10pt;"> ' . $empresa_razsoc .'</dt>

                    <dt style="text-align:center; color: black;"></dt>

                    <dt style="text-align:center; color: black;"><b>R.U.C.:</b> ' . $empresa_ruc . '</dt>

                    <dt style="text-align:center; color: black;"><b>' . $empresa_dir . '</b></dt>

                    <dt style="text-align:center; color: black;">' . $empresa_tel . '</dt>

                    <dt style="text-align:center; color: black;"><b>Email:</b> ' . $empresa_ema . '</dt>

                    <dt style="text-align:center; color: black;"><b>Website:</b> www.prestamosdelnorte.com</dt>
                    <dt style="text-align:center; color: black;"><b>FECHA:</b> ' . $cuotapago_reg . '</dt>
                    <br>
                    <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
                    <dt style="text-align:center;font-family: cambria color: black;"><b>N° DE OPERACION:</b> ' . $cuotapago_id . '</dt>
                    <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
                </dl>
                <dl>
                    <dt style="text-align:justify; color: black;"><b>CLIENTE:</b> ' . $cliente_nom . '</dt>
                    <dt style="text-align:justify; color: black;"><b>RUC/DNI:</b> ' . $cliente_doc . '</dt>
                    <dt style="text-align:justify; color: black;"><b>DIRECCION:</b> ' . $cliente_dir . '</dt>
                    <dt style="text-align:justify; color: black;"><b>VENCIMIENTO:</b> ' . $cuota_fec . '</dt>
                    <dt style="text-align:justify; color: black;"><b>CUOTA:</b> ' . $moneda_nom . ' ' . $cuota_cuo . '</dt>    
                    <dt style="text-align:justify; color: black;"><b>AMORTIZACION:</b> ' . $moneda_nom . ' ' . $cuota_amortizacion . '</dt>
                    <dt style="text-align:justify; color: black;"><b>INTERES CUOTA:</b> ' . $moneda_nom . ' ' . $cuota_interes . '</dt>    

                    <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
		</dl>
                <dl>
                    <dt style="text-align:justify; color: black;"><b>CONCEPTO:</b> ' . $detalle . '</dt>';

                        if (trim($ingreso_detex) != '') {
                            $array = explode('||', trim($ingreso_detex));

                            for ($i = 0; $i < count($array); $i++) {
                                $html .= '<dt style="text-align:justify; color: black;">' . $array[$i] . '</dt><br>';
//                                $printer->text($array[$i] . "\n");
                            }
                        }


if ($cuotapago_mon > 0) {
    $html .= '<br>
                        <dt style="text-align:justify; color: black;"><b>MONTO:</b> ' . $moneda_nom . ' ' . $cuotapago_mon . '</dt>';
}

if ($cuotapago_moncam > 0) {
    $html .= '
                        <dt style="text-align:justify; color: black;"><b>TIP CAMB.:</b> ' . $cuotapago_tipcam . '</dt>
                        <dt style="text-align:justify; color: black;"><b>CAMBIO.:</b> ' . $cuotapago_moncam . '</dt>';
}

if ($ingreso_imp > 0) {
    $html .= '
                        <dt style="text-align:justify; color: black;"><b>PAGO C/CAJA CLIENTE :</b> ' . $moneda_nom . ' ' . $ingreso_imp . '</dt>';
}

if ($clientecaja > 0) {

    $html .= '
                        <dt style="text-align:justify; color: black;"><b>PAGO C/CAJA CLIENTE :</b> ' . $moneda_nom . ' ' . $clientecaja . '</dt>';
}

$html .= '
                <dt style="text-align:center; color: black;"><b>----------------------------------------------------------------</b></dt>
                <dt style="text-align:justify; color: black;"><b>REGISTRADO :</b> ' . $usuario_nom . '</dt>
		</dl>';

$pdf->SetFont('arial', '', 7);

// output the HTML content

$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

$pdf->lastPage();

$nombre_archivo = "CM-" . $cuotapago_id.".pdf";
ob_start();

$pdf->IncludeJS("print();");

$pdf->Output($nombre_archivo, 'I');
?>