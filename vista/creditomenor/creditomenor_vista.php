<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Créditos</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit)); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php
        //vamos a validar que la caja est茅 aperturada para poder hacer ingresos o egresos de todo tipo
        require_once ("vista/funciones/funciones.php");
        validar_apertura_caja();
        ?>
        <div class="box box-primary">
            <div class="box-header">
                <?php require_once('creditomenor_filtro.php'); ?>
            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="creditomenor_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Créditos Menores...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <!-- Input para guardar el valor ingresado en el search de la tabla-->
                        <input type="hidden" id="hdd_datatable_fil">

                        <div id="div_creditomenor_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php //require_once('creditomenor_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div id="div_tasacionsug_form"> -->
                <!-- INCLUIMOS MODAL PARA TASACION SUGERIDA -->
            <!-- </div> -->
            <div id="div_modal_creditomenor_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE CREDITO-->
            </div>
            <div id="div_modal_creditomenor_desembolso">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE desembolso-->
            </div>
            <div id="div_modal_creditomenor_pagos">
                <!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DE PAGOS-->
            </div>
            <div id="div_modal_cambio_asesor">
            </div>
            <div id="div_modal_creditolinea_timeline">
            </div>
            <div id="div_modal_garantia_resumen"></div>
            <div id="div_modal_carousel">
                <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
            </div>
            <div id="div_creditomenor_reporte">

            </div>
            
            <div id="div_filestorage_form">
				<!-- MODAL PARA SUBIR DOCUMENTOS-->
			</div>
            <!--                        <div id="div_modal_creditomenor_pagos"></div>-->

            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
            <?php require_once(VISTA_URL . 'templates/modal_general.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
