<?php
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$C = moneda_mysql($_POST['cre_preaco']);
//echo "<br>";
$i = moneda_mysql($_POST['cre_int']);
//echo "<br>";
$n = $_POST['cre_numcuo'];
//echo "<br>";

$uno = $i / 100;
$dos = $uno + 1;
$tres = pow($dos,$n);
$cuatroA = ($C * $uno) * $tres;
$cuatroB = $tres - 1;
$R = $cuatroA / $cuatroB;
$r_sum = $R*$n;

$fecha_desembolso = $_POST['cre_fecdes'];
list($day, $month, $year) = explode('-', $fecha_desembolso);

$moneda = $_POST['mon_id'];
$cuotip_id = $_POST['cuotip_id']; //2 cuota fija, 1 cuota libre

if(is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R))
{
?>
<br>
<div class="panel panel-primary">
    <div class="panel-heading" style="font-weight: bold;font-family: cambria;font-size: 15px">Cronograma de Crédito</div>
    <div class="panel-body">
       <div class="row">
       <div class="col-md-12">
                <table class="table table-bordered table-hover">
                     <thead>
                     <tr id="tabla_cabecera">
                         <th id="tabla_cabecera_fila">N°</th>
                         <th id="tabla_cabecera_fila">FECHA</th>
                         <th id="tabla_cabecera_fila">CAPITAL</th>
                         <th id="tabla_cabecera_fila">AMORTIZACIÓN</th>
                         <th id="tabla_cabecera_fila">INTERÉS</th>
                         <th id="tabla_cabecera_fila">CUOTA</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php
                     if($moneda == '1'){
                             $moneda = 'S/. ';
                     }else{
                             $moneda = 'US$. ';
                     }

                     for ($j=1; $j <= $n; $j++)
                     { 
                             if($j>1)
                             {
                                     $C = $C - $amo;
                                     $int = $C*($i/100);
                                     $amo = $R - $int;
                                     if($cuotip_id == 1)
                                             $amo = 0;
                             }
                             else
                             {
                                     $int = $C*($i/100);
                                     $amo = $R - $int;
                                     if($cuotip_id == 1)
                                             $amo = 0;
                             }

                             //fecha facturacion
                             $month = $month + 1;
                             if($month == '13'){
                                     $month = 1;
                                     $year = $year + 1;
                             }
                             $fecha=validar_fecha_facturacion($day,$month,$year);

                     ?>
                             <tr class="tabla_cabecera_fila">
                                 <td id="tabla_fila" style="text-align: center">
                                             <?php echo $j;?>
                                     </td>
                                     <td id="tabla_fila" style="text-align: center">
                                             <?php 
                                                     echo $fecha;				
                                             ?>
                                     </td>
                                     <td id="tabla_fila" style="text-align: center">
                                             <?php echo $moneda . mostrar_moneda($C);?>
                                     </td>
                                     <td id="tabla_fila" style="text-align: center">
                                             <?php echo $moneda . mostrar_moneda($amo);?>
                                     </td>
                                     <td id="tabla_fila" style="text-align: center">
                                             <?php echo $moneda . mostrar_moneda($int);?>
                                     </td>
                                     <td id="tabla_fila" style="text-align: center">
                                             <?php echo $moneda . mostrar_moneda($R);?>
                                     </td>
                             </tr>
                     <?php
                     }
                     ?>
                     </tbody>
                </table>
            </div>    
       </div>    
    </div>    
</div>    
<?php
	}
?>