<?php

if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Creditomenor extends Conexion
{

  public $credito_id;
  public $credito_usureg;
  public $credito_usumod;
  public $cliente_id;
  public $credito_tip = 1; //tipo de credito es decir un subtipo, solo es cretido menor, no hay adendas ni otros
  public $cuotatipo_id;
  public $moneda_id;
  public $credito_tipcam;
  public $credito_preaco;
  public $credito_int;
  public $credito_numcuo;
  public $credito_numcuomax;
  public $credito_linapr;
  public $representante_id;
  public $cuentadeposito_id;
  public $credito_pla = 1; //plazo retroventa,no se usa actualmente
  public $credito_obs;
  public $credito_feccre;
  public $credito_fecdes;
  public $credito_comdes;
  public $credito_his;
  public $empresa_id;
  public $credito_score = 0;
  public $credito_semaforo = '';

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $columns = [
        'tb_credito_usureg', 
        'tb_cliente_id', 
        'tb_credito_tip', 
        'tb_cuotatipo_id', 
        'tb_moneda_id', 
        'tb_credito_tipcam', 
        'tb_credito_preaco', 
        'tb_credito_int', 
        'tb_credito_numcuo', 
        'tb_credito_numcuomax', 
        'tb_credito_linapr', 
        'tb_representante_id', 
        'tb_cuentadeposito_id', 
        'tb_credito_comdes', 
        'tb_credito_obs', 
        'tb_credito_feccre', 
        'tb_credito_fecdes', 
        'tb_credito_his',
        'tb_credito_score',
        'tb_credito_semaforo'
      ];

      // Lista de placeholders
      $placeholders = implode(',', array_map(function ($column) {
        return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_creditomenor (" . implode(',', $columns) . ") VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_credito_usureg", $this->credito_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_tip", $this->credito_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_tipcam", $this->credito_tipcam, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_int", $this->credito_int, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_representante_id", $this->representante_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_obs", $this->credito_obs, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_his", $this->credito_his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_score", $this->credito_score, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_semaforo", $this->credito_semaforo, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $credito_id = $this->dblink->lastInsertId();

      $this->dblink->commit();

      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['credito_id'] = $credito_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "CALL proc_modificar_credito(
              :credito_id, :credito_usumod,
              :credito_usureg, :cliente_id, :credito_tip, 
              :cuotatipo_id, :moneda_id, :credito_tipcam, 
              :credito_preaco, :credito_int, :credito_numcuo, 
              :credito_numcuomax, :credito_linapr, :representante_id, 
              :cuentadeposito_id, :credito_pla, :credito_comdes, 
              :credito_obs, :credito_feccre, :credito_fecdes)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_usumod", $this->credito_usumod, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_usureg", $this->credito_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_tip", $this->credito_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_tipcam", $this->credito_tipcam, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_int", $this->credito_int, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
      $sentencia->bindParam(":representante_id", $this->representante_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_pla", $this->credito_pla, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_obs", $this->credito_obs, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function eliminar($credito_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "DELETE FROM tb_creditomenor WHERE tb_credito_id =:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrarUno($credito_id)
  {
    try {


      $sql = "SELECT cm.*, c.*, m.*, ct.*, r.*, cd.*, us.tb_usuario_nom, us.tb_usuario_ape, us.tb_usuario_ema, us.tb_usuario_tel, us.tb_usuario_dir, us.tb_usuario_dni,
            us.tb_usuariogrupo_id, us.tb_usuarioperfil_id  
		FROM tb_creditomenor cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_representante r ON cm.tb_representante_id=r.tb_representante_id
		INNER JOIN tb_cuentadeposito cd ON cm.tb_cuentadeposito_id=cd.tb_cuentadeposito_id
        INNER JOIN tb_usuario us ON cm.tb_credito_usureg = us.tb_usuario_id
		WHERE tb_credito_id=:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_Imagen($credito_id)
  {
    try {
      $sql = "SELECT
                        cm.tb_credito_id,
                        g.tb_garantia_pro AS producto,
                        IF(LENGTH(gf.tb_garantiafile_url)>0 ,'',' - No Tiene Imagen') AS Imagen
                    FROM
                        tb_creditomenor cm
                        INNER JOIN tb_garantia g ON g.tb_credito_id = cm.tb_credito_id
                        LEFT JOIN tb_garantiafile gf ON gf.tb_garantia_id = g.tb_garantia_id 
                    WHERE
                        tb_credito_xac = 1 
                        AND cm.tb_credito_id=:credito_id GROUP BY
                        2 ORDER BY 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No se Encontraron Registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_creditos()
  {
    try {
      $sql = "SELECT * FROM tb_creditomenor cm 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = cm.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = cm.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = cm.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = cm.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = cm.tb_representante_id 
          WHERE tb_credito_xac = 1 ORDER BY tb_credito_id desc limit 25";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function filtro_creditos($filtro_fec1, $filtro_fec2, $empresa_id, $usuario_id, $cliente_id)
  {
    $empresa = "";
    $usuario = "";
    $cliente = "";

    if ($empresa_id > 0) {
      $empresa = " AND cm.tb_empresa_id =:tb_empresa_id";
    }
    if ($usuario_id > 0) {
      $usuario = " AND cm.tb_credito_usureg =:credito_usureg";
    }
    if ($cliente_id > 0) {
      $cliente = " AND cli.tb_cliente_id =:cliente_id";
    }

    try {
      $sql = "SELECT *, cm.tb_empresa_id as empresa_cm FROM tb_creditomenor cm 
            LEFT JOIN tb_cliente cli on cli.tb_cliente_id = cm.tb_cliente_id 
            LEFT JOIN tb_moneda mo on mo.tb_moneda_id = cm.tb_moneda_id 
            LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = cm.tb_cuotatipo_id 
            LEFT JOIN tb_usuario usu on usu.tb_usuario_id = cm.tb_credito_usureg
            LEFT JOIN tb_representante rep on rep.tb_representante_id = cm.tb_representante_id 
            WHERE tb_credito_xac = 1 and tb_credito_feccre BETWEEN :filtro_fec1 and :filtro_fec2 " . $usuario . " " . $empresa . " " . $cliente . " ORDER BY tb_credito_id desc";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro_fec1", $filtro_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":filtro_fec2", $filtro_fec2, PDO::PARAM_STR);

      if ($empresa_id > 0) {
        $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
      }
      if ($usuario_id > 0) {
        $sentencia->bindParam(":credito_usureg", $usuario_id, PDO::PARAM_INT);
      }
      if ($cliente_id > 0) {
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_campo($credito_id, $credito_columna, $credito_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($credito_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_creditomenor SET " . $credito_columna . " =:credito_valor WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":credito_valor", $credito_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":credito_valor", $credito_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function creditomenor_garantias($credito_id)
  {
    try {
      $sql = "SELECT * FROM tb_creditomenor cm INNER JOIN tb_garantia ga on ga.tb_credito_id = cm.tb_credito_id where cm.tb_credito_id =:credito_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //listado de todos los creditos en remate con sus garantías, filtradas por fecha o sin fecha
  function creditomenor_garantias_remate($credito_fecrem1, $credito_fecrem2, $credito_est)
  {
    try {

      $sql = "SELECT cm.tb_credito_id, tb_cliente_id, tb_credito_preaco, tb_garantia_val FROM tb_creditomenor cm INNER JOIN tb_garantia ga on ga.tb_credito_id = cm.tb_credito_id where  tb_credito_xac = 1 and tb_garantia_xac = 1 and tb_credito_fecrem BETWEEN :credito_fecrem1 and :credito_fecrem2 and tb_credito_est in ($credito_est)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_fecrem1", $credito_fecrem1, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecrem2", $credito_fecrem2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function registro_historial($cre_id, $his)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_creditomenor set tb_credito_his = CONCAT(tb_credito_his, :his) where tb_credito_id = :cre_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->bindParam(":his", $his, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function ActualizarNcuotas($tb_credito_id, $num)
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_creditomenor SET 
                                        tb_credito_numcuo=:tb_credito_numcuo,
                                        tb_credito_numcuomax=:tb_credito_numcuomax 
                                WHERE 
                                        tb_credito_id=:tb_credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_credito_numcuo", $num, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_numcuomax", $num, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }


  //daniel odar: listar creditos por cliente, excluyendo algunos id
  function listar_por_clienteid($cliente_id, $extension)
  {
    try {
      $sql = "SELECT
                        cm.tb_credito_id, cm.tb_credito_reg,
                        em.tb_empresa_nomcom,
                        cli.tb_cliente_nom,
                        CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor
                    FROM tb_creditomenor cm
                    LEFT JOIN tb_usuario us ON cm.tb_credito_usureg = us.tb_usuario_id
                    LEFT JOIN tb_empresa em ON cm.tb_empresa_id = em.tb_empresa_id
                    LEFT JOIN tb_cliente cli ON cm.tb_cliente_id = cli.tb_cliente_id
                    WHERE tb_credito_xac = 1
                    AND cm.tb_cliente_id = :cliente$extension
                    ORDER BY tb_credito_reg DESC;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente", $cliente_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //? J. ANTONIO 05-02-2024
  function pagos_intereses_generados($fecha_ini, $fecha_fin){
    try {
      $sql = "SELECT 
                    ing.tb_cliente_id,
                    cu.tb_credito_id,
                    tb_cuotapago_id,
                    tb_cliente_nom,
                    tb_cuotapago_fec,
                    tb_cuota_int,
                    tb_cuota_cap,
                    tb_cuota_cuo,
                    tb_cuota_est,
                    tb_cuota_fec,
                    tb_ingreso_fec,
                    tb_ingreso_imp,
                    (CASE WHEN tb_ingreso_numope != '' THEN 'BANCO' ELSE 'CAJA' END) as lugar
                FROM tb_cuotapago cp 
                INNER JOIN tb_cuota cu ON (cp.tb_modulo_id = 1 AND cp.tb_cuotapago_modid = cu.tb_cuota_id) 
                INNER JOIN tb_ingreso ing ON (ing.tb_modulo_id = 30 AND ing.tb_ingreso_modide = cp.tb_cuotapago_id)
                INNER JOIN tb_creditomenor cre ON (cu.tb_creditotipo_id = 1 AND cre.tb_credito_id = cu.tb_credito_id)
                INNER JOIN tb_garantia gar ON gar.tb_credito_id = cre.tb_credito_id 
                INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
                WHERE tb_cuotapago_xac = 1 AND tb_ingreso_xac = 1 AND gar.tb_garantiatipo_id in(1,2,3)
                AND tb_ingreso_fec BETWEEN :fecha_ini AND :fecha_fin GROUP BY tb_ingreso_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function ultimo_score_cliente($cliente_id, $credito_id){
    try {
      $condicion_extra = 'AND tb_credito_id < :credito_id';
      if(intval($credito_id) <= 0)
        $condicion_extra = '';

      $sql = "SELECT * FROM tb_creditomenor
        WHERE tb_cliente_id =:cliente_id $condicion_extra AND tb_credito_id !=:credito_id AND (tb_credito_score > 0 OR tb_credito_semaforo IS NOT NULL)
        ORDER BY tb_credito_id DESC LIMIT 1;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
