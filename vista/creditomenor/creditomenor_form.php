<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../creditomenor/Creditomenor.class.php');
$oCreditomenor = new Creditomenor();
require_once('../funciones/funciones.php');
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../encuesta/Encuesta.class.php');
$oEncuesta = new Encuesta();

$direc = 'creditomenor';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$creditomenor_id = $_POST['creditomenor_id'];
$vista = $_POST['vista'];

$titulo = '';
if ($usuario_action == 'L')
  $titulo = 'Crédito Menor Registrado';
elseif ($usuario_action == 'I')
  $titulo = 'Registrar Crédito Menor';
elseif ($usuario_action == 'M')
  $titulo = 'Editar Crédito Menor';
elseif ($usuario_action == 'E')
  $titulo = 'Eliminar Crédito Menor';
else
  $titulo = 'Acción de Usuario Desconocido';

$caja_aperturada = validar_apertura_caja_2();

if ($usuario_action != 'L' && $caja_aperturada != 1) {
  $bandera = 5;
  $mensaje = 'No has aperturado o ya está cerrada la caja de este día: ' . date('d-m-Y') . ' ' . ' para la sede de ' . substr($_SESSION['empresa_nombre'], 7);
}

if ($bandera != 5) {
  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en creditomenor
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0;
  $mensaje = '';

  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
      $usuario_id = $_SESSION['usuario_id'];
      $modulo = 'creditomenor';
      $modulo_id = $creditomenor_id;
      $tipo_permiso = $usuario_action;
      $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
      if ($result['estado'] == 1) {
        $bandera = 1; // si tiene el permiso para la accion que desea hacer
      } else {
        $result = NULL;
        echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
        exit();
      }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del creditomenor por su ID
    $credito_est = 0;
    $creditotipo_id = 1;
    $representante_id = 1;
    $cuentadeposito_id = 16; //la cuenta 16 solo es para recibir pagos de c-menor
    $moneda_id = 1; //por defecto soloes seleccionado
    $credito_feccre = date('d-m-Y'); // por defecto fecha actual
    $credito_fecdes = date('d-m-Y');
    $credito_tipcam = '1.00';
    $style_oculto = '';

    if (intval($creditomenor_id) > 0) {
      $result = $oCreditomenor->mostrarUno($creditomenor_id);
        if ($result['estado'] != 1) {
          $mensaje = 'No se ha encontrado ningún registro para el creditomenor seleccionado, inténtelo nuevamente. Detecta el ID del Crédito: ' . $creditomenor_id . ', realiza la consulta en la BD pero no obtiene resultados.';
          $bandera = 4;
        } else {
          $cliente_id = $result['data']['tb_cliente_id'];
          $cliente_doc = $result['data']['tb_cliente_doc'];
          $cliente_nom = $result['data']['tb_cliente_nom'];
          $cliente_tel = $result['data']['tb_cliente_tel'];
          $cliente_cel = $result['data']['tb_cliente_cel'];
          $cliente_telref = $result['data']['tb_cliente_telref'];
          $tipodedicacion_id = $result['data']['tb_tipodedicacion_id']; // nueva columna en el cliente para guardar su dedicación

          $cliente_protel = $result['data']['tb_cliente_protel'];
          $cliente_procel = $result['data']['tb_cliente_procel'];
          $cliente_protelref = $result['data']['tb_cliente_protelref'];

          $cuotatipo_id = $result['data']['tb_cuotatipo_id'];
          $moneda_id = $result['data']['tb_moneda_id'];
          $credito_est = $result['data']['tb_credito_est']; // 1 pendiente, 2 aprobado
          $credito_tipcam = $result['data']['tb_credito_tipcam'];
          $credito_preaco = $result['data']['tb_credito_preaco'];
          $credito_int = $result['data']['tb_credito_int'];
          $credito_numcuo = $result['data']['tb_credito_numcuo'];
          $credito_numcuomax = $result['data']['tb_credito_numcuomax'];
          $credito_linapr = $result['data']['tb_credito_linapr'];
          $representante_id = $result['data']['tb_representante_id'];
          $cuentadeposito_id = $result['data']['tb_cuentadeposito_id'];
          $credito_obs = $result['data']['tb_credito_obs'];
          $credito_feccre = $result['data']['tb_credito_feccre'];
          $credito_fecdes = $result['data']['tb_credito_fecdes'];
          $credito_comdes = $result['data']['tb_credito_comdes'];
          $credito_reporte = $result['data']['tb_credito_reporte'];
          $credito_usuario = $result['data']['tb_credito_usureg'];
          $credito_porcentaje = $result['data']['tb_credito_porcentaje'];
          $credito_score = $result['data']['tb_credito_score'];
          $credito_semaforo = $result['data']['tb_credito_semaforo'];

          $result1 = $oUsuario->mostrarUno($credito_usuario);
          if ($result1['estado'] == 1) {
            $vendedor = $result1['data']['tb_usuario_nom'] . ' ' . $result1['data']['tb_usuario_ape'];
          }
          $result1 = NULL;

          $result1 = $oEncuesta->mostrarUnoCreditoMenor($creditomenor_id);
          if ($result1['estado'] == 1) {
            $motivoprestamo_id = $result1['data']['tb_motivoprestamo_id']; // id que se guarda en la tabla encuesta, FK de la tabla motivoprestamo
            $encuesta_motivo = $result1['data']['tb_encuesta_motivo'];
            $encuesta_ocu = $result1['data']['tb_encuesta_ocu'];
            $encuesta_des = $result1['data']['tb_encuesta_des'];
          }
          $result1 = NULL;
        }
      $result = NULL;

      //? VAMOS A OBTENER EL ULTIMO SCORE DEL CLIENTE QUE SE HAYA REGISTRADO
      $ultimo_score = '<b>Sin Score</b>';
      $ultimo_semaforo = '<b>Sin Color</b>';
      $result = $oCreditomenor->ultimo_score_cliente($cliente_id, $creditomenor_id);
        if($result['estado'] == 1){
          $ultimo_score = '<b>'.$result['data']['tb_credito_score'].'</b>';
          $semaforo = intval($result['data']['tb_credito_semaforo']);

          if($semaforo == 1)
            $ultimo_semaforo = '<span class="badge bg-green">Normal</span>';
          elseif($semaforo == 2)
            $ultimo_semaforo = '<span class="badge bg-yellow">CPP</span>';
          elseif($semaforo == 3)
            $ultimo_semaforo = '<span class="badge bg-orange">Deficiente</span>';
          elseif($semaforo == 4)
            $ultimo_semaforo = '<span class="badge bg-red">Dudoso</span>';
          elseif($semaforo == 5)
            $ultimo_semaforo = '<span class="badge bg-black">Pérdida</span>';
          else
            $ultimo_semaforo = '<b>Sin Color</b>';
        }
      $result = NULL;

    } elseif ($vista == 'precredito') {
      require_once('../precredito/Precredito.class.php');
      $oPcr = new Precredito();

      $pcr_id = $_POST['pcr_id'];

      $where[0]['column_name'] = 'pcr.tb_precred_id';
      $where[0]['param0'] = $pcr_id;
      $where[0]['datatype'] = 'INT';

      $inner[0]['alias_columnasparaver'] = 'pcl.tb_cliente_id';
      $inner[0]['tipo_union'] = 'LEFT';
      $inner[0]['tabla_alias'] = 'tb_precliente pcl';
      $inner[0]['columna_enlace'] = 'pcr.tb_precliente_id';
      $inner[0]['alias_columnaPK'] = 'pcl.tb_precliente_id';

      $res_pcr = $oPcr->listar_todos($where, $inner, array());
      if ($res_pcr['estado'] == 1) {
        $pcr_reg = $res_pcr['data'][0];
        $pcr_reg['tb_precred_producto'] = str_replace("''", "´´", $pcr_reg['tb_precred_producto']);
        $moneda_id = $pcr_reg['tb_moneda_id'];
        $pcl_id = $pcr_reg['tb_precliente_id'];
        $credito_preaco = $pcr_reg['tb_precred_monto'];
        $credito_int = $pcr_reg['tb_precred_int'];

        if (!empty($pcr_reg['tb_cliente_id'])) {
          $cliente_id = $pcr_reg['tb_cliente_id'];

          require_once('../cliente/Cliente.class.php');
          $oCli = new Cliente();
          $res_cli = $oCli->mostrarUno($cliente_id)['data'];

          $cliente_doc        = $res_cli['tb_cliente_doc'];
          $cliente_nom        = $res_cli['tb_cliente_nom'];
          $cliente_tel        = $res_cli['tb_cliente_tel'];
          $cliente_cel        = $res_cli['tb_cliente_cel'];
          $cliente_telref     = $res_cli['tb_cliente_telref'];
        }
      }
      unset($where, $res_pcr);
    } elseif ($vista == 'simulador') {
      $style_oculto = 'style="display: none;"';
      $med1 = 1;
      $med2 = -1;
      $med3 = 3;
    }
  } else {
    $mensaje = $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditomenor" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <?php if ($usuario_action == 'L') { ?>
            <h4 class="modal-title"><?php echo 'CREDITO REGISTRADO POR : ' . $vendedor;} else { ?></h4>
            <h4 class="modal-title"><?php echo $titulo;} ?></h4>
        </div>
        <form id="form_creditomenor" method="post">
          <input type="hidden" id="creditomenor_vista" value="<?php echo $vista; ?>">
          <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
          <input type="hidden" name="cuota_action" value="insertar_creditomenor">
          <input type="hidden" name="hdd_creditomenor_id" id="hdd_creditomenor_id" value="<?php echo $creditomenor_id; ?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">
          <input type="hidden" name="hdd_moneda_id" id="hdd_moneda_id" value="<?php echo $moneda_id; ?>">
          <input type="hidden" id="hdd_usuariogrupo_id" value="<?php echo intval($_SESSION['usuariogrupo_id']); ?>">

          <div class="modal-body">
            <div class="row">
              <!-- SECCION 1-->
              <div class="col-md-<?php echo 5 + $med1; ?>">
                <div class="box box-primary shadow" <?php echo $style_oculto; ?>>
                  <div class="box-header with-border">
                    <h3 class="box-title">Datos del Cliente</h3>
                    <div class="box-tools pull-right">
                      <a class="btn btn-primary btn-xs" title="Editar" onclick="cliente_form('I', 0)"><i class="fa fa-user-plus"></i></a>
                      <a class="btn btn-warning btn-xs" title="Editar" onclick="cliente_form('M', 0)"><i class="fa fa-edit"></i></a>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body no-padding">
                    <input type="hidden" name="hdd_pcl_id" id="hdd_pcl_id" value='<?php echo $pcl_id; ?>'>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="txt_cliente_fil_cre_doc" class="control-label">DNI</label>
                          <input type="text" name="txt_cliente_fil_cre_doc" id="txt_cliente_fil_cre_doc" class="form-control input-sm numero" value="<?php echo $cliente_doc; ?>">
                        </div>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                          <label for="txt_cliente_fil_cre_nom" class="control-label">Cliente</label>
                          <input type="text" name="txt_cliente_fil_cre_nom" id="txt_cliente_fil_cre_nom" class="form-control input-sm mayus" value="<?php echo $cliente_nom; ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_cliente_fil_cre_tel" class="control-label">Celular Titular</label>
                          <input type="text" name="txt_cliente_fil_cre_tel" id="txt_cliente_fil_cre_tel" class="form-control input-sm" value="<?php echo $cliente_tel; ?>">
                          <small class="form-text text-muted small_tel" style="margin-top: 0; color: blue;"><b><?php echo $cliente_protel;?></b></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_cliente_fil_cre_cel" class="control-label">Cel. Referencia 1</label>
                          <input type="text" name="txt_cliente_fil_cre_cel" id="txt_cliente_fil_cre_cel" class="form-control input-sm" value="<?php echo $cliente_cel; ?>">
                          <small class="form-text text-muted small_cel" style="margin-top: 0; color: blue;"><b><?php echo $cliente_procel;?></b></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_cliente_fil_cre_telref" class="control-label">Cel. Referencia 2</label>
                          <input type="text" name="txt_cliente_fil_cre_telref" id="txt_cliente_fil_cre_telref" class="form-control input-sm" value="<?php echo $cliente_telref; ?>">
                          <small class="form-text text-muted small_telref" style="margin-top: 0; color: blue;"><b><?php echo $cliente_protelref;?></b></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_cliente_fil_cre_nom" class="control-label">Reportar RCC</label>
                          <p></p>
                          <label class="checkbox-inline">
                            <input type="checkbox" name="cbx_credito_repor" id="cbx_credito_repor" value="1" class="flat-green" <?php if ($credito_reporte == 1) echo "checked"; ?> title="Reportar a la central de Riezgo">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="txt_credito_score" class="control-label">Score del Cliente</label>
                          <input type="text" name="txt_credito_score" id="txt_credito_score" class="form-control input-sm numero" value="<?php echo $credito_score;?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cmb_credito_semaforo" class="control-label">Color del Score</label>
                          <select class="form-control input-sm" name="cmb_credito_semaforo" id="cmb_credito_semaforo">
                            <option value="">Selecciona...</option>
                            <option value="1" <?php if($credito_semaforo == 1) echo 'selected';?>>Normal</option>
                            <option value="2" <?php if($credito_semaforo == 2) echo 'selected';?>>CPP</option>
                            <option value="3" <?php if($credito_semaforo == 3) echo 'selected';?>>Deficiente</option>
                            <option value="4" <?php if($credito_semaforo == 4) echo 'selected';?>>Dudoso</option>
                            <option value="5" <?php if($credito_semaforo == 5) echo 'selected';?>>Pérdida</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <table class="table no-margin">
                          <tr>
                            <td>Score Anterior:</td>
                            <td id="td_ultimo_score"><?php echo $ultimo_score;?></td>
                          </tr>
                          <tr>
                            <td>Color Anterior:</td>
                            <td id="td_ultimo_semaforo"><?php echo $ultimo_semaforo;?></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- -->
                <div class="box box-primary shadow">
                  <div class="box-header with-border">
                    <h3 class="box-title">Datos del Crédito</h3>
                    <div class="box-tools pull-right">
                      <span class="label label-info">Click en siguiente</span>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body no-padding">
                    <input type="hidden" name="hdd_pcr_reg" id="hdd_pcr_reg" value='<?php echo json_encode($pcr_reg) ?>'>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cmb_cuotatipo_id" class="control-label">Tipo de Cuota</label>
                          <select class="form-control input-sm" name="cmb_cuotatipo_id" id="cmb_cuotatipo_id">
                            <?php require_once('../cuotatipo/cuotatipo_select.php'); ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_per" class="control-label">Periodo</label>
                          <input type="text" name="txt_credito_per" id="txt_credito_per" class="form-control input-sm" value="Mensual" readonly>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="cmb_moneda_id" class="control-label">Moneda</label>
                          <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id" style="width: 65px;">
                            <?php require_once('../moneda/moneda_select.php'); ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_tipcam" class="control-label">Cambio</label>
                          <input type="text" name="txt_credito_tipcam" id="txt_credito_tipcam" class="form-control input-sm" value="<?php echo $credito_tipcam; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <span class="badge bg-red" id="span_error" style="display: none;"></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_preaco" class="control-label">Precio Acordado</label>
                          <input type="text" name="txt_credito_preaco" id="txt_credito_preaco" class="form-control input-sm moneda" value="<?php echo $credito_preaco;; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_int" class="control-label">Interés (%)</label>
                          <input type="text" name="txt_credito_int" id="txt_credito_int" class="form-control input-sm moneda" value="<?php echo $credito_int; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_numcuo" class="control-label">N° Cuotas Max</label>
                          <input type="text" name="txt_credito_numcuo" id="txt_credito_numcuo" class="form-control input-sm numero" value="<?php echo $credito_numcuo; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_linapr" class="control-label">Línea Probada</label>
                          <input type="text" name="txt_credito_linapr" id="txt_credito_linapr" class="form-control input-sm" value="<?php echo $credito_linapr; ?>" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="cmb_representante_id" class="control-label">Representante</label>
                          <select class="form-control input-sm" name="cmb_representante_id" id="cmb_representante_id">
                            <?php
                            $representante_id = 1; // para seleccionar a Manuel COMO REPRESENTANTE
                            require_once('../representante/representante_select.php'); ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="cmb_cuentadeposito_id" class="control-label">Cuenta a Depositar</label>
                          <select class="form-control input-sm" name="cmb_cuentadeposito_id" id="cmb_cuentadeposito_id">
                            <?php
                            $representante_id = 9; // para seleccionar la cuenta a pagar de CARLOS
                            require_once('../cuentadeposito/cuentadeposito_select.php');
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="txt_credito_obs" class="control-label">Observación</label>
                          <textarea class="form-control" name="txt_credito_obs" id="txt_credito_obs"><?php echo $credito_obs; ?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="cbo_credito_porcentaje" class="control-label">Porcentaje</label>
                          <select name="cbo_credito_porcentaje" id="cbo_credito_porcentaje" class="form-control">
                            <option value="0" <?php if ($credito_porcentaje == 0) echo 'selected'; ?>>... Seleccione ...</option>
                            <option value="1" <?php if ($credito_porcentaje == 1) echo 'selected'; ?>>30 %</option>
                            <option value="2" <?php if ($credito_porcentaje == 2) echo 'selected'; ?>>35 %</option>
                            <option value="3" <?php if ($credito_porcentaje == 3) echo 'selected'; ?>>40 %</option>
                            <option value="4" <?php if ($credito_porcentaje == 4) echo 'selected'; ?>>45 %</option>
                            <option value="5" <?php if ($credito_porcentaje == 5) echo 'selected'; ?>>50 %</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_feccre" class="control-label">Fecha de Crédito</label>
                          <div class='input-group date' id='creditomenor_picker1'>
                            <input type='text' class="form-control" name="txt_credito_feccre" id="txt_credito_feccre" value="<?php echo $credito_feccre; ?>" />
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_fecdes" class="control-label">Fecha de Desembolso</label>
                          <div class='input-group date' id='creditomenor_picker2'>
                            <input type='text' class="form-control" name="txt_credito_fecdes" id="txt_credito_fecdes" value="<?php echo $credito_fecdes; ?>" />
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="txt_credito_comdes" class="control-label">Comprobante</label>
                          <input type="text" name="txt_credito_comdes" id="txt_credito_comdes" class="form-control" value="<?php echo $credito_comdes; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php if ($action == 'insertar') : ?>
                  <button type="submit" class="btn btn-block btn-social btn-info" id="btn_creditomenor_sig" <?php echo $style_oculto; ?>><i class="fa fa-arrow-right"></i>Siguiente paso</button>
                <?php endif; ?>
                <!--- MESAJES DE GUARDADO PARA C-MENOR-->
                <div class="row">
                  <br>
                  <div class="col-md-12">
                    <div class="callout callout-info" id="creditomenor_mensaje" style="display: none;">
                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                  </div>
                </div>
              </div>
              <!-- SECCION 2-->
              <div class="col-md-<?php echo 7 + $med2; ?>">
                <div class="box box-primary shadow" <?php echo $style_oculto; ?>>
                  <div class="box-header with-border">
                    <h3 class="box-title">Datos de Garantías</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-primary btn-xs" onclick="garantia_form('I', 0)" id="btn_garantia_nuevo" disabled="true"><i class="fa fa-plus"></i> Nueva Garantía</button>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body no-padding" style="overflow-x: auto;">
                    <div class="row">
                      <!--- MESAJES DE CONSULTANDO TABLA GARANTIAS-->
                      <div class="col-md-12">
                        <div class="callout callout-info" id="garantia_mensaje_tbl" style="display: none;">
                          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Listando garantías...</h4>
                        </div>
                      </div>
                      <div class="col-md-12" id="div_garantia_tabla">
                        <?php if ($action == 'insertar') : ?>
                          <div class="alert alert-info alert-dismissible" id="garantia_info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <h4><i class="icon fa fa-info"></i>Paso 2</h4>
                            Registra las garantías luego de guardar el crédito
                          </div>
                        <?php endif; ?>
                        <?php
                        if ($action == 'modificar' || $action == 'leer')
                          require_once('../garantia/garantia_tabla.php');
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- -->
                <div class="row">
                  <div class="col-md-<?php echo 9 + $med3; ?>">
                    <div class="box box-primary shadow">
                      <div class="box-header with-border">
                        <h3 class="box-title">Cronograma de Crédito</h3>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                      </div>
                      <div class="box-body no-padding" style="overflow-x: auto;">
                        <div class="row">
                          <!--- MESAJES DE CONSULTANDO TABLA GARANTIAS-->
                          <div class="col-md-12">
                            <div class="callout callout-info" id="cronograma_mensaje_tbl" style="display: none;">
                              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Listando cronograma...</h4>
                            </div>
                          </div>
                          <div class="col-md-12" id="div_creditomenor_cronograma">
                            <?php if ($action == 'insertar') : ?>
                              <div class="alert alert-info alert-dismissible" id="garantia_info" <?php echo $style_oculto; ?>>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-info"></i>Paso 3</h4>
                                El cronograma se guarda después de registrar las garantías
                              </div>
                            <?php endif; ?>
                            <?php
                            if ($action == 'modificar' || $action == 'leer') {
                              $credito_id = $creditomenor_id;
                              require_once('../cuota/cuota_tabla.php');
                            }
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" <?php echo $style_oculto; ?>>
                      <div class="col-md-4">
                        <?php if ($action == 'insertar') : ?>
                          <button type="button" class="btn btn-block btn-social btn-info" id="btn_creditomenor_sig2" onclick="guardar_cuotas()" disabled><i class="fa fa-arrow-right"></i>Siguiente paso</button>
                        <?php endif; ?>
                      </div>
                      <!--- MESAJES DE GUARDADO PARA C-MENOR-->
                      <div class="col-md-12">
                        <br>
                        <div class="callout callout-info" id="cuota_mensaje" style="display: none;">
                          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3" <?php echo $style_oculto; ?>>
                    <div class="box box-primary shadow">
                      <div class="box-header with-border">
                        <h3 class="box-title">Imprimir Formatos</h3>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                      </div>
                      <div class="box-body no-padding">
                        <div class="row">
                          <!--- MESAJES DE CONSULTANDO TABLA GARANTIAS-->
                          <div class="col-md-12">
                            <div class="callout callout-info" id="formatos_mensaje" style="display: none;">
                              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Listando formatos...</h4>
                            </div>
                          </div>
                          <div class="col-md-12" id="div_creditomenor_formatos">
                            <?php if ($action == 'insertar') : ?>
                              <div class="alert alert-info alert-dismissible" id="garantia_info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-info"></i>Paso 4</h4>
                                Puedes imprimir los formatos después de guardar el cronograma
                              </div>
                            <?php endif; ?>
                            <?php
                            if ($action == 'modificar' || $action == 'leer') {
                              $credito_id = $creditomenor_id;
                              require_once('creditomenor_formatos.php');
                            }
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <?php if ($action == 'insertar') : ?>
                          <button type="button" class="btn btn-block btn-social btn-info" id="btn_creditomenor_fin" onclick="finalizar_credito()" disabled><i class="fa fa-check"></i>Finalizar</button>
                        <?php endif; ?>
                        <?php if ($credito_est == 1 || $credito_est == 2) : ?>
                          <button type="button" class="btn btn-block btn-social btn-info" id="btn_creditomenor_fin" onclick="finalizar_credito()"><i class="fa fa-check"></i>Finalizar</button>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- CUADRO PARA PEQUEÑA ENCUESTA SOBRE MOTIVO DE PRÉSTAMO-->
                <div class="row" style="margin-top: 10px;">
                  <div class="col-md-6" <?php echo $style_oculto; ?>>
                    <div class="box box-primary shadow">
                      <div class="box-header with-border">
                        <h3 class="box-title">Encuesta Rápida</h3>
                      </div>
                      <div class="box-body no-padding">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cmb_motivoprestamo_id" class="control-label">¿Cuál es el motivo del Préstamo?</label>
                            <select name="cmb_motivoprestamo_id" id="cmb_motivoprestamo_id" class="selectpicker form-control input-sm" data-live-search="true" data-max-options="1" data-size="12">
                              <?php require_once('../motivoprestamo/motivoprestamo_select.php'); ?>
                            </select>
                            <input type="hidden" name="txt_encuesta_motivo" id="txt_encuesta_motivo" class="form-control" value="<?php echo $encuesta_motivo; ?>">
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cmb_tipodedicacion_id" class="control-label">¿A qué se dedica o dónde trabaja?</label>
                            <select name="cmb_tipodedicacion_id" id="cmb_tipodedicacion_id" class="form-control input-sm">
                              <?php require_once('../tipodedicacion/tipodedicacion_select.php'); ?>
                            </select>
                            <input type="hidden" name="txt_encuesta_ocu" id="txt_encuesta_ocu" class="form-control input-sm" value="<?php echo $encuesta_ocu; ?>">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="txt_encuesta_des" class="control-label">Breve descripción de su trabajo o de su negocio</label>
                            <input type="text" name="txt_encuesta_des" id="txt_encuesta_des" class="form-control input-sm" value="<?php echo $encuesta_des; ?>">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="box box-primary shadow">
                      <div class="box-header with-border">
                        <h3 class="box-title">Check List Crédito Menor</h3>
                      </div>
                      <div class="box-body no-padding" id="creditomenor_checklist">
                        <?php 
                          $modulo_nom = 'creditomenor'; $modulo_id = $creditomenor_id;
                          require_once('../creditomenor/creditomenor_checklist.php');
                        ?>
                      </div>
                    </div>
                  </div>
                </div>

                <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                <?php if ($action == 'eliminar') : ?>
                  <div class="callout callout-warning">
                    <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Crédito?</h4>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'M') : ?>
                <!--button type="submit" class="btn btn-info" id="btn_guardar_creditomenor">Guardar</button-->
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_creditomenor">Aceptar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'I') : ?>
                <button type="button" class="btn btn-danger" onclick="creditomenor_cancelar()" id="btn_cancelar_creditomenor">Cancelar</button>
              <?php endif; ?>
              <?php if ($usuario_action != 'I') : ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <?php endif; ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if (in_array($bandera, array(4, 5))) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_creditomenor">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<div id="div_modal_monedacambio_form"></div>
<div id="div_modal_garantia_form"></div>
<div id="div_modal_cliente_form"></div>
<script type="text/javascript" src="<?php echo 'vista/creditomenor/creditomenor_form.js?ver=2799'; ?>"></script>