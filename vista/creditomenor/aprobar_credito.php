<?php
require_once('../precredito/Precredito.class.php');
$oPcr = new Precredito();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$credito_id = intval($_GET['credito_id']);

$lista['precred'] = 'Todo normal por aquí';

//buscar si existe un precred con xac: 1, que tenga a el id de ese precli, y que no tenga estado 4,5,9,10
    $wh[0]['column_name'] = 'pcr.tb_precred_xac';
    $wh[0]['param0'] = 1;
    $wh[0]['datatype'] = 'INT';

    $wh[1]['conector'] = 'AND';
    $wh[1]['column_name'] = 'pcr.tb_precliente_id';
    $wh[1]['param1'] = "= (SELECT pcr1.tb_precliente_id FROM tb_precred pcr1 where pcr1.tb_credito_id = $credito_id AND pcr1.tb_creditotipo_id = 1)";

    $join[0]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
    $join[0]['tipo_union'] = 'LEFT';
    $join[0]['tabla_alias'] = 'tb_usuario us';
    $join[0]['columna_enlace'] = 'pcr.tb_precred_usureg';
    $join[0]['alias_columnaPK'] = 'us.tb_usuario_id';

    $join[1]['alias_columnasparaver'] = 'epcr.tb_estadoprecred_nom';
    $join[1]['tipo_union'] = 'LEFT';
    $join[1]['tabla_alias'] = 'tb_estadoprecred epcr';
    $join[1]['columna_enlace'] = 'pcr.tb_estadoprecred_id';
    $join[1]['alias_columnaPK'] = 'epcr.tb_estadoprecred_id';

    $join[2]['alias_columnasparaver'] = 'tb_empresa_nomcom';
    $join[2]['tipo_union'] = 'LEFT';
    $join[2]['tabla_alias'] = 'tb_empresa em';
    $join[2]['columna_enlace'] = 'pcr.tb_empresa_id';
    $join[2]['alias_columnaPK'] = 'em.tb_empresa_id';

    $join[3]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
    $join[3]['tipo_union'] = 'LEFT';
    $join[3]['tabla_alias'] = 'tb_moneda mon';
    $join[3]['columna_enlace'] = 'pcr.tb_moneda_id';
    $join[3]['alias_columnaPK'] = 'mon.tb_moneda_id';

    $join[4]['alias_columnasparaver'] = 'pcl.tb_cliente_id, pcl.tb_precliente_nombres';
    $join[4]['tipo_union'] = 'LEFT';
    $join[4]['tabla_alias'] = 'tb_precliente pcl';
    $join[4]['columna_enlace'] = 'pcr.tb_precliente_id';
    $join[4]['alias_columnaPK'] = 'pcl.tb_precliente_id';

    $join[5]['alias_columnasparaver'] = 'im.tb_inconclusomotivo_nom';
    $join[5]['tipo_union'] = 'LEFT';
    $join[5]['tabla_alias'] = 'tb_inconclusomotivo im';
    $join[5]['columna_enlace'] = 'pcr.tb_inconclusomotivo_id';
    $join[5]['alias_columnaPK'] = 'im.tb_inconclusomotivo_id';

    $otros['orden']['column_name'] = 'pcr.tb_precred_fecreg';
    $otros['orden']['value'] = 'DESC';
//

$res = $oPcr->listar_todos($wh, $join, $otros);

if ($res['estado'] == 1) {
    $lista['precred'] = '';
    foreach ($res['data'] as $key => $pcr) {
        $colorear = $credito_id == intval($pcr['tb_credito_id']) ? 'background-color: #f7e2c3;' : '';
        $bold = $credito_id == intval($pcr['tb_credito_id']) ? 'font-weight: bold;' : '';
        $estado_motivo = $pcr['tb_estadoprecred_nom'];
        if (in_array(intval($pcr['tb_estadoprecred_id']), array(4, 9))) {
            if (!empty($pcr['tb_credito_id'])) {
                $cretip_nom = intval($pcr['tb_creditotipo_id']) == 1 ? 'CM' : 'CGV';
                $estado_motivo .= ' en '.$cretip_nom.'-'.$pcr['tb_credito_id'];
            } else {
                $estado_motivo .= ' <font color="red">(pendiente registrar crédito)</font>';
            }
        } elseif (in_array(intval($pcr['tb_estadoprecred_id']), array(5, 10))) {
            $estado_motivo .= ' - '.$pcr['tb_inconclusomotivo_nom'];
        }

        $lista['precred'] .= 
        '<div class="box box-primary shadow" style="padding: 5px 10px;'. $colorear .'">'.
            '<input type="hidden" name="hdd_precred" value=\''. json_encode($pcr) .'\'>'.
            '<div class="box-header with-border" style="padding: 5px 3px;">'.
                '<div class="col-md-3" style="padding: 0px">'. mostrar_fecha_hora($pcr['tb_precred_fecreg']) .'</div>'.
                '<div class="col-md-5" style="padding: 0px; text-align: center;">'. $pcr['asesor'] .'</div>'.
                '<div class="box-tools pull-right">'.
                    'SEDE: ' . substr($pcr['tb_empresa_nomcom'], 7).
                '</div>'.
            '</div>'.

            '<div class="box-body" style="padding: 5px 5px;">'.
                '<ul class="products-list product-list-in-box">'.
                    '<div style="padding: 0px 2px;" class="col-md-3">'. $pcr['tb_precliente_nombres'] .'</div>'.
                    '<div style="padding: 0px 2px;" class="col-md-5">'. $pcr['tb_precred_producto'] .'</div>'.
                    '<div style="padding: 0px 3px; text-align: center; '. $bold .'" class="col-md-2">'. "{$pcr['tb_moneda_nom']} ". mostrar_moneda($pcr['tb_precred_monto']) ." al {$pcr['tb_precred_int']}%".'</div>'.
                    '<div style="padding: 0px 5px; '. $bold .'" class="col-md-2">'. $estado_motivo .'</div>'.
                '</ul>'.
            '</div>'.
        '</div>';
    }
}
unset($res, $wh, $join, $otros);

?>

<div class="modal-body">
    <?php echo $lista['precred'];?>
</div>
