<?php
	$credito_id = (isset($_POST['credito_id']))? $_POST['credito_id'] : intval($credito_id);

  $result = NULL;
  if(intval($_POST['credito_id']) == 0){
    $result = $oCreditomenor->mostrarUno($credito_id);
  }
  else{
    require_once('../creditomenor/Creditomenor.class.php');
    $oCreditomenor = new Creditomenor();
    $result = $oCreditomenor->mostrarUno($credito_id);
  }
  if($result['estado'] == 1){
    $cuotatipo_id = intval($result['data']['tb_cuotatipo_id']); //1 libre, 2 fija
    $credito_est = intval($result['data']['tb_credito_est']); //4 liquidado, 7 por entregar
    $cuotatipo_nom = '';
    if($cuotatipo_id == 1)
      $cuotatipo_nom = 'libre';
    if($cuotatipo_id == 2)
      $cuotatipo_nom = 'fijo';

    //falta poder desarollar el modulo de tarifario, en donde esán los nuevos y antiguos documentos
    $doc_contrato = 'doc_cmenor_contrato.php?d1='.$credito_id.'&tipo_cuota='.$cuotatipo_nom;
    $doc_anexo02 = 'doc_cmenor_anexo.php?d1='.$credito_id.'&tipo_cuota='.$cuotatipo_nom;
    $doc_entrega = 'doc_cmenor_entrega.php?d1='.$credito_id.'&tipo_cuota='.$cuotatipo_nom;

    $result = NULL;
  }
?>                                              
<div class="row">
  <div class="col-md-12 form-group">
    <a href="<?php echo 'vista/creditomenor/'.$doc_contrato;?>" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i> Contrato</a>
  </div>
    <p>
  <div class="col-md-12 form-group">
    <a href="<?php echo 'vista/creditomenor/'.$doc_anexo02;?>" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i> Anexo 02</a>
  </div>
  <?php if($credito_est == 7):?>
    <div class="col-md-12 form-group">
      <a onclick="acta_entrega(<?php echo $credito_id.",'".$doc_entrega."'";?>)" class="btn btn-primary"><i class="fa fa-print"></i> Generar Acta de Entrega</a>
    </div>
  <?php endif;?>
  <?php if($credito_est == 4):?>
    <div class="col-md-12 form-group">
      <a href="<?php echo 'vista/creditomenor/'.$doc_entrega;?>" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i> Generar Acta de Entrega</a>
    </div>
  <?php endif;?>
</div>