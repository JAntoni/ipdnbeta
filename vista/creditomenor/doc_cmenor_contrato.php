<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');

/* require_once ("../../config/Cado.php"); */
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$creditomenor_id = intval($_GET['d1']);
$tipo_cuota = $_GET['tipo_cuota'];

$title = 'CONTRATO CREDITO MENOR';
$codigo = 'C-MENOR-'.str_pad($creditomenor_id, 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {
  public function Header() {
    $image_file = K_PATH_IMAGES.'logo.jpg';
    $this->Image($image_file, 20, 10, 30, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
  }
  public function Footer() {
    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(5, 20, 5);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$result = $oCredito->mostrarUno($creditomenor_id);
  if($result['estado'] == 1){
    $reg = mostrar_fecha_hora($result['data']['tb_credito_reg']);
    $mod = mostrar_fecha_hora($result['data']['tb_credito_mod']);
    $apr = mostrar_fecha_hora($result['data']['tb_credito_apr']);
    $usureg = $result['data']['tb_credito_usureg'];
    $usumod = $result['data']['tb_credito_usumod'];
    $usuapr = $result['data']['tb_credito_usuapr'];
    $cuotip_id = $result['data']['tb_cuotatipo_id'];
    $mon_id = $result['data']['tb_moneda_id'];
    $credito_tipcap = $result['data']['tb_credito_tipcap'];
    $empresa_id = $result['data']['tb_empresa_id'];
    //fechas
    $credito_feccre = mostrar_fecha($result['data']['tb_credito_feccre']);
    $credito_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
    $credito_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
    $credito_preaco = $result['data']['tb_credito_preaco'];
    $credito_int = $result['data']['tb_credito_int'];
    $credito_numcuo = $result['data']['tb_credito_numcuo'];
    $credito_linapr = $result['data']['tb_credito_linapr'];
    $credito_numcuomax = $result['data']['tb_credito_numcuomax'];
    //$transferencia_bien=($credito_linapr-$credito_preaco)*$credito_numcuomax+$credito_preaco;
    $transferencia_bien = $credito_linapr;
    //representante
    $rep_id = $result['data']['tb_representante_id'];
    $rep_nom = $result['data']['tb_representante_nom'];
    $rep_dni = $result['data']['tb_representante_dni'];
    //cuenta deposito
    $cuedep_id = $result['data']['tb_cuentadeposito_id'];
    $cuedep_num = $result['data']['tb_cuentadeposito_num'];
    $cuedep_ban = $result['data']['tb_cuentadeposito_ban'];

    $credito_pla = $result['data']['tb_credito_pla'];
    $credito_comdes = $result['data']['tb_credito_comdes'];
    $credito_obs = $result['data']['tb_credito_obs'];
    $credito_webref = $result['data']['tb_credito_webref'];
    $credito_est = $result['data']['tb_credito_est'];
    //cliente
    $cliente_id = $result['data']['tb_cliente_id'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_dir = $result['data']['tb_cliente_dir'];
    $cliente_ema = $result['data']['tb_cliente_ema'];
    $cliente_fecnac = $result['data']['tb_cliente_fecnac'];
    $cliente_tel = $result['data']['tb_cliente_tel'];
    $cliente_cel = $result['data']['tb_cliente_cel'];
    $cliente_telref = $result['data']['tb_cliente_telref'];
    $cliente_ubigeo = $result['data']['tb_ubigeo_cod'];
  }
$result = NULL;

//ubigeo del cliente
if($cliente_ubigeo  >0){
  $result = $oUbigeo->mostrarUbigeo($cliente_ubigeo);
    if($result['estado'] == 1){
      $cliente_dep = $result['data']['Departamento'];
      $cliente_pro = $result['data']['Provincia'];
      $cliente_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

$nombre_cuotas = 'CUOTAS MENSUALES';
if($tipo_cuota == 'fijo')
  $nombre_cuotas = 'CUOTAS FIJAS';

$html= '
  <p align="center"><strong><u>CONTRATO DE PRESTAMO CON GARANTÍA MOBILIARIA – '.$nombre_cuotas.'</u></strong></p>
    
  <p align="justify">Conste por el presente documento un Contrato de Préstamos “Consumo” con Garantía Mobiliaria con entrega de posesión, en adelante, <b>EL CONTRATO</b>, que  celebran de una parte <b>'.$cliente_nom.'</b>, identificado con D.N.I Nº <b>'.$cliente_doc.'</b>, con domicilio en <b>'.$cliente_dir.'</b>, del Distrito de <b>'.$cliente_dis.'</b>, Provincia de <b>'.$cliente_pro.'</b>, del Departamento de <b>'.$cliente_dep.'</b>, a quien en adelante se le denominará <b>EL CLIENTE</b>; y de la otra parte, <b>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</b>, identificada con R.U.C <b>20600752872</b>, con domicilio en <b> Av. Mariscal Nieto Nº 480 Ext. A7 1er Piso
(Centro Comercial Boulevard Plaza)</b> y sede en <b>Av. Panamericana Nº 639 Int. LF-01 Primer Piso (Centro Comercial Mall Aventura  Chiclayo)</b>, del Distrito de <b>Chiclayo</b>, Provincia de <b>Chiclayo</b>, del Departamento de <b>Lambayeque</b>, en adelante, <b>LAS OFICINAS</b>, debidamente representada por su Gerente General el Sr. <b>VARGAS TORRES  VÍCTOR MANUEL</b>, identificado con D.N.I. <b>Nº 44760801</b> según poderes inscritos en la Partida Registral N° 11218951 del Registro de Personas Jurídicas de la Oficina Registral de Chiclayo, a quien en adelante se les denominará <b>EL ACREEDOR</b>, en los términos y condiciones siguientes:</p>

  <p align="justify"><strong>CLÁUSULA 1°: INTENCIÓN DE LAS PARTES:</strong><br>

  1.1. Por EL CONTRATO, EL ACREEDOR concede a EL CLIENTE un préstamo de dinero en efectivo por la suma señalada en el Anexo N° 01: Hoja resumen, en adelante, EL ANEXO; por su parte, EL CLIENTE se compromete a pagar el préstamo en los términos y condiciones pactados y para el caso de un incumplimiento, constituye primera y preferente garantía mobiliaria en respaldo al préstamo y todas sus obligaciones presentes y futuras que asumen frente a EL ACREEDOR, tal como se detalla en las cláusulas siguientes.</p>

  <p align="justify"><strong>CLÁUSULA 2°: EL CONTRATO:</strong><br>

  2.1. Al firmar EL CONTRATO, EL CLIENTE declara estar conforme con todas las cláusulas, términos y condiciones, y haber recibido a entera satisfacción la información consignada en EL ANEXO, el cual forma parte integrante del presente. Asimismo, EL CLIENTE declara haber verificado que la descripción de los bienes grabados mediante la entrega en garantía mobiliaria es correcta y que la fecha de préstamo y sus vencimientos son los pactados, renunciando a todo reclamo futuro por estos conceptos.</p>
    
  <p align="justify"><strong>CLÁUSULA 3°: LAS PARTES:</strong><br>

  3.1. EL CLIENTE y EL ACREEDOR, en adelante, LAS PARTES, no reconocen adulteraciones, borrones ni enmendaduras en la información consignada en EL ANEXO. Este documento se extiende por duplicado, quedando un ejemplar en poder de EL ACREEDOR y otro en poder de EL CLIENTE. En caso de duda, se presentará el ejemplar de EL CONTRATO que obra en poder de EL ACREEDOR. Se deja expresa constancia que EL CLIENTE no podrá ceder el presente  CONTRATO a favor de terceros.</p>
    
  <p align="justify"><strong>CLÁUSULA 4°: PRESTAMO Y GARANTÍA:</strong><br>
  
  4.1. Por EL CONTRATO, EL ACREEDOR otorga a EL CLIENTE en calidad de Préstamo la suma de dinero en efectivo en moneda nacional y/o extranjera que se indica en EL ANEXO de este documento. Las obligaciones que EL CLIENTE asume o asumirá en el futuro frente a EL ACREEDOR, referidos en el párrafo anterior, generan los intereses compensatorios y los gastos pactados en EL ANEXO de este documento (gastos, administrativos de contratación, transporte y custodia de los bienes entregados en Garantía Mobiliaria), que EL CLIENTE conoce y acepta voluntariamente al ser pactado.<br>
  4.2. Por su parte, EL CLIENTE constituye una primera y preferente Garantía Mobiliaria con entrega de posesión sobre los bienes muebles, que se declaran en EL ANEXO de este instrumento, de acuerdo a lo establecido en los artículos 3.4, 17 y otros de la Ley de Garantía Mobiliaria, Ley N° 28677, a favor de EL ACREEDOR, hasta por el valor de dichos bienes según se detalla en EL ANEXO de este documento y en respaldo del cumplimiento del pago del préstamo.<br>
    
  4.3. Cualquier otra obligación de EL CLIENTE frente a EL ACREEDOR se detalla en los términos y condiciones establecidos en estos documentos.</p>
    
  <p align="justify"><strong>CLÁUSULA 5°: PLAZO Y PAGO DEL PRÉSTAMO:</strong><br>
  
  5.1. El préstamo será cancelado por EL CLIENTE con el pago total de la obligación adeudada, en los plazos y montos pactados, habiendo cumplido la primera y única cuota exigible; y tratándose de créditos bajo el sistema de cuotas, con la frecuencia que se señala en el Cronograma de pagos contenido en EL ANEXO. Los pagos del Préstamo se harán en efectivo en LAS OFICINAS o en los canales habilitados para tal efecto por EL ACREEDOR, que se detallan en la CLÁUSULA 14.<br>
    
  5.2. EL CLIENTE podrá efectuar pagos anticipados totales o parciales de sus obligaciones, una vez cancelada la primera cuota exigible. En estos casos, los intereses que se cobren serán únicamente los correspondientes a los días calendarios transcurridos y no generarán cobros, comisiones, gasto administrativo o costos adicionales a los correspondientes a capital e interés a la fecha.<br>
    
  5.2.1 Una vez cancelada la primera cuota exigible, el pago total del capital del Préstamo, así como el interés compensatorio, comisiones, gastos e impuestos pactados, correspondientes a la primera cuota exigible, que se devenguen hasta la fecha en que se haga efectivo el pago, así como, en caso de incumplimiento de pago en la fecha pactada, el interés moratorio respectivo.<br>
    
  5.2.2. El pago parcial del capital del Préstamo, con los correspondientes intereses compensatorios, comisiones, gastos e impuestos pactados que se devenguen hasta la fecha en que se haga efectivo el pago, así como, en caso de incumplimiento de pago en la fecha pactada, el interés moratorio respectivo. Efectuado el pago parcial, las partes determinarán un nuevo plazo de vencimiento, procediendo EL ACREEDOR al recalculo del saldo correspondiente del capital adeudado por EL CLIENTE, el mismo que comprenderá los intereses compensatorios, así como las comisiones, impuestos y gastos en caso correspondan, así como los intereses moratorios en caso de incumplimiento de pago en la fecha pactada, hasta el siguiente pago que realice EL CLIENTE<br>
    
  5.2.3. Tratándose de pagos parciales, se deberá tener en cuenta que EL CLIENTE podrá efectuar pagos después de la primera cuota exigible del periodo, ya sea a través de pagos anticipados o adelanto de cuotas, en forma total o parcial, sin condiciones ni limitaciones, y sin que aplique el cobro de gastos, comisiones o penalidades de ningún tipo, considerando para ello lo siguiente:<br>
    
  El Pago anticipado conlleva a la aplicación del monto al capital del crédito, con la consiguiente reducción de los intereses, comisiones y gastos al día del pago. Los pagos mayores a dos (2) cuotas (incluyendo la exigible en el periodo) se consideran pagos anticipados.<br>
    
  El Adelanto de cuotas supone la aplicación del monto pagado a las cuotas inmediatamente posteriores a la primera cuota exigible en el periodo, sin que se produzca una reducción de los intereses, comisiones y gastos al día del pago. Los pagos mayores a dos (2) cuotas (que incluyen aquella exigible en el periodo) se consideran adelanto de cuotas.<br>
    
  EL CLIENTE, sin embargo, podrá requerir a EL ACREEDOR que los pagos mayores de dos (2) cuotas sean considerados un “Adelanto de cuotas” o que los pagos menores o iguales a dos (2) cuotas sean considerados como “Pago Anticipado”. En el caso de pagos anticipados parciales, EL ACREEDOR requerirá a EL CLIENTE, al momento de realizar el pago, que señale si debe procederse a la reducción del monto de las cuotas restantes o la reducción del número de cuotas, para lo cual le otorgará un plazo de quince (15) días, contados a partir de la realización del pago. De no manifestar su decisión dentro del plazo otorgado a su favor, EL ACREEDOR procederá a la reducción del número de cuotas. EL CLIENTE podrá solicitar al momento de realizar el pago anticipado un nuevo Cronograma de Pagos considerando el pago realizado y el plazo del Préstamo, el plazo para la entrega del nuevo cronograma es no mayor a siete (7) días de efectuada la solicitud. En caso el pago anticipado se realice por un tercero, el Cronograma de Pagos estará a disposición de EL CLIENTE en cualquiera de LAS OFICINAS que solicitó el crédito.<br>
    
  5.2.4. En todo pago anticipado total o parcial que efectúe EL CLIENTE los gastos administrativos de contratación, transporte y custodia serán pagados íntegramente conforme se consigna en EL ANEXO de este documento.</p>
    
  <p align="justify"><strong>CLÁUSULA 6°: RETIRO DE LOS BIENES GRABADOS:</strong><br>

  6.1. Sólo EL CLIENTE o un representante debidamente autorizado mediante Carta Poder Legalizada Notarialmente o por Escritura Pública según el monto de la Garantía Mobiliaria, podrá retirar los bienes grabados una vez cumplidas todas las obligaciones garantizadas por EL CLIENTE frente a EL ACREEDOR.<br>
    
  6.2. En caso de fallecimiento de EL CLIENTE, serán sus herederos legalmente instituidos los autorizados para el retiro de los bienes grabados.<br>
    
  6.3. El retiro de los bienes grabados será programado después de efectuada la cancelación total de las obligaciones contraídas por EL CLIENTE ante EL ACREEDOR y serán solicitados por EL CLIENTE en el área de CAJA inmediatamente realizado el pago.<br>
    
  6.4. Una vez presentado el Boucher de pago en el área de CAJA, el recojo será programado para las siguientes 24 a 48 horas, según la disponibilidad de EL ACREEDOR para el envío de los bienes grabados mediante Garantía Mobiliaria y comunicado a EL CLIENTE.<br>
    
  6.5. Los costos del primer traslado pactado entre EL CLIENTE y EL ACREEDOR posterior a la cancelación total de las obligaciones de EL CLIENTE, serán asumidos íntegramente por EL ACREEDOR. Si EL CLIENTE incumpliese con la fecha pactada de recojo y solicitase un segundo traslado del bien desde los almacenes hasta LAS OFICINAS de entrega u otro destino previamente acordado entre ambas partes, será EL CLIENTE quién deberá asumir los gastos que se generen de tal procedimiento, dichos gastos serán solicitados por EL ACREEDOR de manera previa al traslado y ascienden a S/. 10.00 (DIEZ CON 00/100 SOLES) por vez. EL ACREEDOR suscribirá unilateralmente un ACTA DE INCUMPLIMIENTO DE RECOJO DE BIEN MUEBLE con la cual realizará el cobro pactado entre LAS PARTES.</p>
    
  <p align="justify"><strong>CLÁUSULA 7°: ALMACENAMIENTO DE BIENES OTORGADOS EN GARANTÍA:</strong><br>
  
  7.1. Los bienes otorgados en garantía mobiliaria por EL CLIENTE serán conservados en almacenes o bóvedas que serán elegidas por EL ACREEDOR.<br>
    
  7.2. Los bienes otorgados en garantía mobiliaria por EL CLIENTE no permanecerán en uso, siendo recibidos en LAS OFICINAS, luego embalados y codificados para su recojo y traslado a los almacenes o bóvedas determinadas por EL ACREEDOR para permanecer ahí hasta su respectiva liquidación y consecuente recojo.<br>
    
  7.3. Los costos que genere el almacenamiento de los bienes dentro de las fechas previstas y mientras se encuentre VIGENTE la operación, serán asumidos íntegramente por EL ACREEDOR. Si fuera el caso en que los bienes deban permanecer más tiempo dentro de los almacenes o bóvedas designadas a consecuencia de IMPAGOS por parte de EL CLIENTE, esto constituiría un gasto por concepto de CUSTODIA DE BIEN MUEBLE la cual generará un costo de S/. 5.00 (CINCO CON 00/100 SOLES) por día, los cuales serán asumidos íntegramente por EL CLIENTE. Dichos costos están establecidos  claramente en EL ANEXO. EL CLIENTE a la firma del presente  acepta haber sido debidamente informado de los costos y condiciones previamente descritas.</p>
    
  <p align="justify"><strong>CLÁUSULA 8°: PÉRDIDA DE LOS BIENES GRABADOS:</strong><br>
  
  8.1. Si EL ACREEDOR no pudiera efectuar la devolución de los bienes otorgados en  garantía mobiliaria a EL CLIENTE por causas no imputables, pagará a EL CLIENTE un importe igual al acordado como valor de tasación o valoración.</p>
    
  <p align="justify"><strong>CLÁUSULA 9°: VALORIZACIÓN:</strong><br>
  
  9.1. LAS PARTES valorizan de común acuerdo, los bienes grabados sobre la base de la buena  fe y los parámetros estandarizados de tasación en los montos indicados en EL ANEXO de este documento, para todos los efectos de la constitución de la Garantía Mobiliaria.<br>
    
  9.2. LAS PARTES aceptan como valor del préstamo y valor de tasación del bien los señalados en EL ANEXO.</p>
    
  <p align="justify"><strong>CLÁUSULA 10°: DECLARACIÓN JURADA:</strong><br>
  
  10.1. Con relación a los bienes entregados en Garantía Mobiliaria con carácter de declaración jurada EL CLIENTE manifestó lo siguiente:<br>
    
  10.1.1. Que cuenta con la capacidad suficiente para constituir la presente Garantía Mobiliaria.<br>
    
  10.1.2. Que es el único y legítimo propietario de dicho(s) bien(es).<br>
  
  10.1.3. Que la descripción, características, calidad y demás especificaciones de los mismos, que se detallan en EL ANEXO del presente documento, realmente le corresponde(n) y no contiene adulteraciones.<br>
    
  10.1.4. Que la celebración de EL CONTRATO no contraviene ninguna orden judicial  o extrajudicial, contrato o acuerdo alguno.<br>
    
  10.2. De determinarse que EL CLIENTE no es propietario de los bienes y/o no fueran genuinos y/o las especificaciones declaradas en EL ANEXO no le corresponden, asumirá toda la responsabilidad civil y penal, inclusive frente a terceros, sin perjuicio de ser denunciado penalmente por el delito de estafa y otros delitos aplicables.<br>
    
  10.3. En cualquiera de estos casos, EL ACREEDOR dará por vencido todos los plazos de todas las obligaciones debidas por EL CLIENTE a EL ACREEDOR con un monto igual al saldo del préstamo y todas las obligaciones impagas, más los intereses pactados y gastos administrativos, legales, operativos, devengados, etcétera, que se puedan generar como consecuencia de tal acto.</p>
    
  <p align="justify"><strong>CLÁUSULA 11°: CARGAS O GRAVÁMENES:</strong><br>
  
  11.1. EL CLIENTE declara que sobre EL BIEN que otorga en Garantía Mobiliaria a favor de EL ACREEDOR, no pesa carga ni gravamen alguno, ni está afecto a medida judicial o extrajudicial que restrinja o limite en forma alguna su dominio y libre disposición o reduzca su valor.</p>
    
  <p align="justify"><strong>CLÁUSULA 12°: DE LA REALIZACIÓN DE LA GARANTÍA MOBILIARIA:</strong><br>
  
  12.1. LAS PARTES acuerdan expresa e irrevocablemente que, en caso de incumplimiento de cualquiera de las Obligaciones Garantizadas, EL ACREEDOR procederá a la inmediata  ejecución de la Garantía Mobiliaria, pudiendo optar, para dicha ejecución, por cualquiera de los procedimientos señalados en la presente cláusula.<br>
    
  12.2 Si el procedimiento inicialmente elegido por EL ACREEDOR no se llegara a concretar, EL ACREEDOR se encontrará facultado a elegir alternativamente por cualquiera de los siguientes procedimientos:<br>
    
  12.2.1. Adjudicación Directa del bien mueble.<br>
      
  12.2.2. Venta Extrajudicial. EL CLIENTE a la firma de EL CONTRATO, autoriza a EL ACREEDOR, a proceder pasados 15 (quince) días IMPAGOS desde la fecha acordada en el presente contrato y anexo, con la Venta Extrajudicial de los bienes grabados en Garantía Mobiliaria, sin requerir notificación alguna, amparándose en las condiciones contractuales suscritas por LAS PARTES.<br>
      
  12.2.2.1. Remate: De la misma forma y como mecanismo alterno, LAS PARTES acuerdan que la ejecución de la Garantía Mobiliaria podrá también ser efectuada mediante el remate de los bienes gravados en el marco de  la Ley de Garantía Mobiliaria, Ley N° 28677, bajo el siguiente procedimiento:<br>

  12.2.2.1.1. Si hubieran transcurrido quince (15) días calendarios desde el vencimiento del plazo del préstamo o de los préstamos futuros o de cualquier otra obligación asumida por EL CLIENTE frente a EL ACREEDOR, y EL CLIENTE no hubiera cumplido con efectuar el pago debido en los términos y condiciones pactados.<br>

  12.2.2.1.2. EL ACREEDOR automáticamente y sin previo aviso a EL CLIENTE, dará por vencidos todos los plazos, todas las obligaciones debidas por EL CLIENTE a EL ACREEDOR y se cobrará la totalidad de las obligaciones debidas mediante la ejecución de los bienes entregados en Garantía Mobiliaria renunciando EL CLIENTE a todo reclamo futuro.<br>

  12.2.2.1.3. En cumplimiento de lo establecido en el numeral 2 del artículo 47 de la Ley de Garantía Mobiliaria, Ley N° 28677, el precio que servirá de base para el remate será igual al valor del préstamo otorgado y de la suma de todas las obligaciones debidas por EL CLIENTE a EL          ACREEDOR, más los montos devengados por concepto de intereses pactados, gastos administrativos, de contratación de transporte, custodia de los bienes entregados en Garantía Mobiliaria. De no presentarse postores al remate, EL ACREEDOR podrá adjudicarse los bienes gravados por el precio base.<br>

  12.2.2.1.4. LAS PARTES acuerdan, asimismo que el monto resultante de la ejecución de la presente Garantía Mobiliaria, será aplicado a la  cancelación de los intereses compensatorios y moratorios, gastos administrativos, impuestos y capital principal que se encuentren pendientes de pago en ese orden de prelación.<br>
        
  <b>12.3. Designación de Apoderado Irrevocable:</b><br>
    
  12.3.1. En el caso de la realización de una posible adjudicación, venta y/o modalidad dispuesta en las cláusulas precedentes, LAS PARTES, de común acuerdo deciden consignar al Sr. Erik Francesc Obiol Anaya, identificado con DNI N° 42417854, domiciliado en Mz. H2 Lote 28 Urbanización Derrama Magisteral en el Distrito y Provincia de Chiclayo, Departamento de Lambayeque, en calidad de apoderado irrevocable.</p>
      
  <p align="justify"><strong>CLÁUSULA 13°: INTERÉS Y GASTOS:</strong><br>
  
  13.1. Queda pactado que las tasas de interés aplicables al préstamo concedidos se regirá por la tasa más alta fijada para las operaciones activas moneda nacional, por el Banco Central de la Reserva del Perú, y está fijado a 360 días, en su defecto lo pactado por ambas partes en EL ANEXO; adicionalmente EL ACREEDOR cobrará a EL CLIENTE los gastos administrativos de contratación, transporte y custodia de los bienes entregados en Garantía Mobiliaria en los porcentajes indicados en EL ANEXO de este documento.<br>
    
  13.2. EL CLIENTE, a la firma de este CONTRATO, acepta tener conocimiento que los costos por concepto de CUSTODIA DEL BIEN MUEBLE pasados 1 (un) día de la fecha acordada de pago ascienden a S/. 5.00 (CINCO CON 00/100 SOLES) diarios.</p>
    
  <p align="justify"><strong>CLÁUSULA 14°: MEDIO Y LUGAR DE PAGO:</strong><br>
    
  14.1. Por el presente acto jurídico contractual LAS PARTES declaran que el Medio de Pago utilizado del precio pactado, es mediante la entrega de dinero en efectivo en cualquiera de LAS OFICINAS de la empresa, cuyos horarios de atención se precisan a continuación:<br>
    
  14.1.1. SEDE BOULEVARD: De Lunes a Viernes de 9:00 am a 1:00 pm y de 3:00 pm a 7:00 pm y los días Sábados de 9:00 am a 1:00 pm.<br>
    
  14.1.2. SEDE MALL AVENTURA: De Lunes a Sábado de 9:00 am a 7:00 pm.<br>
    
  14.2. De manera excepcional y ante la imposibilidad de utilizar el medio de pago mencionado en el párrafo anterior, EL CLIENTE podrá cancelar a través de depósito en Cuenta Bancaria en S/ (Soles) N° 305-2410005-0-92 ó en $ (Dólares) Nº 305-2285533-1-08, ambas en Banco de Crédito BCP, así como Cuenta Bancaria en S/ (Soles) N° 0011-0288-0100040139 en Banco BBVA Continental.<br>
    
  14.3. Los depósitos mediante BCP y BBVA Continental, deberán hacerse vía Agente siempre que sean depósitos locales (Dpto. Lambayeque) y mediante cajero de depósito cuando se realicen desde otra ciudad. Esto para evitar costos o tarifas propias de las entidades bancarias; de generarse alguna comisión por parte del Banco los cargos correrán por cuenta de EL CLIENTE, y cuyo depósito podría estar sujeto al cobro de intereses moratorios y/o compensatorios, por el pago de cuota incompleta.<br>
    
  14.4. Si EL CLIENTE realiza el pago bancario de su cuota, deberá presentar el Boucher original en LAS OFICINAS de EL ACREEDOR en el horario ya establecido, o vía correo electrónico al correo contacto@prestamosdelnortechiclayo.com en el plazo máximo de 24 horas contados a partir del depósito conteniendo el NOMBRE y DNI del titular identificando así el depósito, caso contrario, dicho ingreso de dinero a la cuenta no podrá ser utilizado para el pago de su cuota establecida. EL CLIENTE acepta que EL ACREEDOR solo considerará como válidos los pagos debidamente presentados y verificados.</p>
  
  <p align="justify"><strong>CLÁUSULA 15°: MODALIDAD DE PAGO:</strong><br>
  
  15.1. LAS PARTES acuerdan que el pago del crédito será en '.ucwords(strtolower($nombre_cuotas)).', siendo la primera de ellas, la cuota exigible en el periodo, las cuales serán determinadas por EL ACREEDOR y que se encuentran detalladas en EL ANEXO.</p>

  <p align="justify"><strong>CLÁUSULA 16°: VARIACIÓN DE TÉRMINOS CONTRACTUALES:</strong><br>
  
  16.1. LAS PARTES acuerdan que EL ACREEDOR podrá variar unilateralmente las condiciones contractuales del Préstamo referidas a comisiones y gastos, y como consecuencia de ello la TCEA (tasa de Costo Efectiva Anual), entre otros aspectos contractuales legalmente permitidos. En tal sentido la variación de la TCEA no se dará cuando se trate de un crédito a plazo fijo, salvo cuando exista efectiva negociación entre EL ACREEDOR y EL CLIENTE, en cada oportunidad en la que se pretenda efectuar una modificación, conforme a la legislación vigente.<br>
    
  16.2. Entre otros supuestos y sin que se pueda considerar esta lista como limitativa, las modificaciones a que se refieren el párrafo anterior podrán ser efectuadas por EL ACREEDOR –a su solo criterio– como consecuencia de: (i) cambios en las condiciones de la economía nacional o internacional; (ii) cambios en el funcionamiento o tendencias de los mercados o la competencia; (iii) cambios en las políticas de gobierno o de Estado que afecten las condiciones del mercado; (iv) impacto de alguna disposición legal sobre costos, características, definición o condiciones de los productos y servicios financieros; (v) modificación de las características, definición, rentabilidad o condiciones de los productos por EL ACREEDOR; (vi) inflación o deflación, devaluación o revaluación de la moneda; (vii) evaluación crediticia de EL CLIENTE; (viii) encarecimiento de los servicios prestados por terceros cuyos costos son trasladados a EL CLIENTE o de los costos de prestación de los productos y servicios ofrecidos por EL ACREEDOR; (ix) crisis financiera; o (x) hechos ajenos a la voluntad de las partes, conmoción social, desastres naturales, terrorismo, guerra, caso fortuito o fuerza mayor.<br>
    
  16.3. Para comunicar las modificaciones antes referidas, EL ACREEDOR enviará una carta notarial; y, de no estar conforme EL CLIENTE con las referidas modificaciones, éste podrá resolver EL CONTRATO, debiendo para ello manifestar su disconformidad mediante carta notarial y procediendo a realizar el pago de todo el saldo deudor u obligación que mantuviera pendiente, teniendo un plazo de cuarenta y cinco (45) días como máximo para cancelar. De no ejercer EL CLIENTE su derecho de resolución, se entiende que acepta tales modificaciones.</p>
    
  <p align="justify"><strong>CLÁUSULA 17°: SOLUCIÓN DE CONTROVERSIAS:</strong><br>
  
  17.1. Todo litigio o controversia, derivados o relacionados con este acto jurídico, será resuelto mediante arbitraje, de conformidad con los Reglamentos Arbitrales del Centro de Arbitraje de la Cámara de Comercio de Lambayeque, a cuyas normas, administración y decisión se someten LAS PARTES en forma incondicional, declarando conocerlas y aceptarlas en su integridad.</p>
    
  <p align="justify"><strong>CLÁUSULA 18°: RESPECTO DE LA COMUNICACIÓN ENTRE LAS PARTES:</strong><br>
  
  18.1. LAS PARTES consignan sus domicilios en el presente contrato, para que toda comunicación sea efectuada en aquellos domicilios; de existir alguna modificación en los domicilios señalados, sólo serán efectivos entre LAS PARTES si se realizó la comunicación de tal hecho.</p>
';

list($day, $month, $year) = explode('-', $credito_feccre);

$html .='
  <p align="justify">Firmado en la ciudad de Chiclayo, a los '.$day.' días, del mes de '.strtolower(nombre_mes($month)).' del '.$year.'.</p>
  <p><br/></p>
  <table>
    <tr>
      <td style="width:50%;" align="center" valign="bottom">
        <div style="position: relative;">
          <br/><br/><br/><br/>
          <strong>
            ________________________________<br/>
            EL CLIENTE
          </strong>
        </div>
      </td>
      <td style="width:30%;" align="center">
        <img src="'.K_PATH_IMAGES.'firma.png" height="50"/>
        <strong>
          ________________________________<br/>
          EL ACREEDOR
        </strong>
      </td>
    </tr>
  </table>
';

// set core font
$pdf->SetFont('dejavusans', '', 7.2);

$html = str_replace(array("\r\n", "\n", "\r", "  "), "", $html);
// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo = $codigo.$title.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>