<?php

require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../creditomenor/Creditomenor.class.php');
$oCredito = new Creditomenor();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../garantiafile/Garantiafile.class.php');
$oGarantiafile = new Garantiafile();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once('../encuesta/Encuesta.class.php');
$oEncuesta = new Encuesta();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../precredito/Precredito.class.php');
$oPcr = new Precredito;
require_once('../historial_nuevo/Historial_nuevo.class.php');
$oHist_n = new Historial_nuevo();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];
$creditotipo_id = 1; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
$usuario_id = intval($_SESSION['usuario_id']);
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

$oCredito->credito_usureg = intval($_SESSION['usuario_id']);
$oCredito->credito_usumod = intval($_POST['usuario_id']);
$oCredito->cliente_id = intval($_POST['hdd_cliente_id']);
$oCredito->cuotatipo_id = intval($_POST['cmb_cuotatipo_id']);
$oCredito->moneda_id = intval($_POST['cmb_moneda_id']);
$oCredito->credito_tipcam = moneda_mysql($_POST['txt_credito_tipcam']);
$oCredito->credito_preaco = moneda_mysql($_POST['txt_credito_preaco']);
$oCredito->credito_int = moneda_mysql($_POST['txt_credito_int']);
$oCredito->credito_numcuo = intval($_POST['txt_credito_numcuo']);
$oCredito->credito_numcuomax = intval($_POST['txt_credito_numcuo']);
$oCredito->credito_linapr = moneda_mysql($_POST['txt_credito_linapr']);
$oCredito->representante_id = intval($_POST['cmb_representante_id']);
$oCredito->cuentadeposito_id = intval($_POST['cmb_cuentadeposito_id']); //la recaudación de pagos de c-menor se hará en una sola cuenta N° 15
$oCredito->credito_obs = $_POST['txt_credito_obs'];
$oCredito->credito_feccre = fecha_mysql($_POST['txt_credito_feccre']);
$oCredito->credito_fecdes = fecha_mysql($_POST['txt_credito_fecdes']);
$oCredito->credito_comdes = $_POST['txt_credito_comdes'];
$oCredito->credito_his = 'Crédito registrado por: <b>' . $_SESSION['usuario_nom'] . '</b> | <b>' . date('d-m-Y h:i a') . '</b><br>';
$oCredito->credito_score = intval($_POST['txt_credito_score']);
$oCredito->credito_semaforo = intval($_POST['cmb_credito_semaforo']);
//  $oCredito->empresa_id = $_SESSION['empresa_id'];
$credito_repor = intval($_POST['cbx_credito_repor']);

$pcr_reg = (array) json_decode($_POST['hdd_pcr_reg']);
//ESTADOS DEL PRECREDITO
$estadoprecred_nombre = array(
  1 => 'INTERESADO', 2 => 'SEGUIMIENTO', 3 => 'VISITA', 4 => 'CONCLUIDO', 5 => 'RECHAZADO',
  6 => 'INTERESADO', 7 => 'SEGUIMIENTO', 8 => 'VISITA', 9 => 'CONCLUIDO', 10 => 'RECHAZADO'
);
//

if ($action == 'insertar') {

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al guardar el Crédito.';
  $cliente_id = intval($_POST['hdd_cliente_id']);

  if ($cliente_id <= 0) {
    $data['mensaje'] = 'Falta seleccionar el cliente por favor.';
    echo json_encode($data);
    exit();
  }

  $result = $oCredito->insertar();
    if ($result['estado'] == 1) {
      $data['estado'] = 1;
      $data['credito_id'] = $result['credito_id'];
      $data['mensaje'] = ' Crédito registrado, ID:' . $data['credito_id'] . '. Garantías pendientes.';

      //Guardamos la encuesta realizada al cliente al momento de solicitar el crédito
      $moneda_nom = 'S/.';
      if (intval($_POST['cmb_moneda_id']) == 2)
        $moneda_nom = 'US$';

      $credito_id = intval($data['credito_id']);
      $oCredito->modificar_campo($credito_id, 'tb_empresa_id', $_SESSION['empresa_id'], 'INT');
      $oCredito->modificar_campo($credito_id, 'tb_credito_reporte', $credito_repor, 'INT');

      $creditolinea_det = 'He registrado el Crédito Menor número: ' . $data['credito_id'] . '. Con un préstamo de ' . $moneda_nom . ' ' . $_POST['txt_credito_preaco'];
      $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

      if (!empty($pcr_reg)) {
        $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_credito_id', $data['credito_id'], 'INT');

        //guardar historial nuevo
        $oHist_n->tablanom = 'tb_precred';
        $oHist_n->regmodid = $pcr_reg['tb_precred_id'];
        $oHist_n->usureg = $_SESSION['usuario_id'];
        $oHist_n->columnamod = 'tb_credito_id';
        $oHist_n->valorant = null;
        $oHist_n->valornuevo = $data['credito_id'];
        $oHist_n->det = "Creó el crédito menor id <b>{$data['credito_id']}</b>";
        $oHist_n->insertar();
      }
    }
  $result = NULL;

  echo json_encode($data);
} 
elseif ($action == 'modificar') {
  $credito_id = intval($_POST['hdd_creditomenor_id']);
  $oCredito->credito_id = $credito_id;

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al modificar el Crédito.';

  if ($oCredito->modificar()) {
    $data['estado'] = 1;
    $data['mensaje'] = 'credito modificado correctamente.';

    $creditolinea_det = 'He modificado el Crédito Menor número: ' . $credito_id;
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);
  }

  echo json_encode($data);
} 
elseif ($action == 'cancelar' || $action == 'eliminar') {
  if ($action == 'eliminar' && $usuariogrupo_id != 2) {
    $data['estado'] = 0;
    $data['mensaje'] = 'Solicite a administración para poder eliminar un crédito por favor.';
    echo json_encode($data);
    exit();
  }
  //al cancelar el credito, el credito queda con xac = 0, se borra: garantias, fotos, cuotas
  $credito_id = (intval($_POST['credito_id']) > 0) ? intval($_POST['credito_id']) : intval($_POST['hdd_creditomenor_id']);
  $moneda_id = (intval($_POST['moneda_id']) > 0) ? intval($_POST['moneda_id']) : intval($_POST['hdd_moneda_id']);

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al eliminar al credito.';
  //primero verificamos que no tenga desembolsos el crédito
  $egreso_imp = 0;
  $modulo_id = 51;
  $egreso_modide = $credito_id;
  $egreso_xac = 1;
  $result = $oEgreso->mostrar_por_modulo($modulo_id, $egreso_modide, $moneda_id, $egreso_xac);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $egreso_imp += floatval($value['tb_egreso_imp']);
    }
  }
  $result = NULL;

  if ($egreso_imp > 0) {
    $data['estado'] = 0;
    $data['mensaje'] = 'Este Crédito tiene desembolsado un total de: ' . $egreso_imp . ', no se puede elimiar.';
    echo json_encode($data);
    exit();
  }
  //elimando las garantías y fotos
  $result = $oCredito->creditomenor_garantias($credito_id);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $garantia_id = $value['tb_garantia_id'];

      $result2 = $oGarantiafile->listar_garantiafiles($garantia_id);
      if ($result2['estado'] == 1) {
        foreach ($result2['data'] as $key => $value) {
          $garantiafile_id = $value['tb_garantiafile_id'];
          $garantiafile_url = $value['tb_garantiafile_url'];
          if (file_exists('../../' . $garantiafile_url)) {
            unlink('../../' . $garantiafile_url);
          }
          $oGarantiafile->eliminar($garantiafile_id);
        }
      }
      $result2 = NULL;
      $oGarantia->eliminar($garantia_id);
    }
  }
  $result = NULL;

  //eliminando las cuotas
  $result = $oCuota->cuotas_credito($credito_id, 1, 'tb_creditomenor'); // 1 creditotipo C -MENOR
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $cuota_id = $value['tb_cuota_id'];
      $oCuota->eliminar($cuota_id);
    }
  }
  $result = NULL;

  //cambiando XAC = 0 al crédito
  $oCredito->modificar_campo($credito_id, "tb_credito_xac", 0, "INT");

  //ELIMINAR LA ENCUESTA CREADA PARA EL CREDITO
  $oEncuesta->eliminar_por_credito($credito_id); //cambia xac = 0

  $creditolinea_det = 'He cancelado la culminación del Crédito Menor número: ' . $credito_id;
  $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

  $data['estado'] = 1;
  $data['mensaje'] = 'Datos del crédito eliminado correctamente';

  //si hay un precredito con id de ese credito, cambiar estado a rechazado(5) con motivo(5). y guardar historial
  $wh[0]['column_name'] = 'pcr.tb_credito_id';
  $wh[0]['param0'] = $credito_id;
  $wh[0]['datatype'] = 'INT';

  $wh[1]['conector'] = 'AND';
  $wh[1]['column_name'] = 'pcr.tb_creditotipo_id';
  $wh[1]['param1'] = 1;
  $wh[1]['datatype'] = 'INT';

  $wh[2]['conector'] = 'AND';
  $wh[2]['column_name'] = 'pcr.tb_precred_xac';
  $wh[2]['param2'] = 1;
  $wh[2]['datatype'] = 'INT';

  $res = $oPcr->listar_todos($wh, array(), array());
  if ($res['estado'] == 1) {
    $pcr_reg = $res['data'][0];
    $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_estadoprecred_id', 5, 'INT'); //rechazado
    $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_inconclusomotivo_id', 5, 'INT'); //motivo: eliminacion desde modulo creditos
    $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_credito_id', null, 'INT');

    $oHist_n->tablanom = 'tb_precred';
    $oHist_n->regmodid = $pcr_reg['tb_precred_id'];
    $oHist_n->usureg = $_SESSION['usuario_id'];
    $oHist_n->columnamod = 'tb_estadoprecred_id';
    $oHist_n->valorant = $pcr_reg['tb_estadoprecred_id'];
    $oHist_n->valornuevo = 5;
    $oHist_n->det = "Canceló o eliminó el registro del crédito menor";
    $oHist_n->insertar();
    unset($oHist_n->det);

    $oHist_n->columnamod = 'tb_credito_id';
    $oHist_n->valorant = $pcr_reg['tb_credito_id'];
    $oHist_n->valornuevo = null;
    $oHist_n->insertar();
  }
  unset($wh, $res);

  echo json_encode($data);
} 
elseif ($action == 'estado') {
  $credito_id = $_POST['credito_id'];
  $credito_est = $_POST['credito_est'];
  $usuario_id = $_SESSION['usuario_id'];

  $data['estado'] = 1;
  $data['mensaje'] = 'Crédito aprobado correctamente';

  //cambios de estado al crédito, mediante modificar campo
  $oCredito->modificar_campo($credito_id, 'tb_credito_est', $credito_est, 'INT');
  //guardamos el usuario que aprubea el crédito
  $oCredito->modificar_campo($credito_id, 'tb_credito_usuapr', $usuario_id, 'INT');
  //guardamos la fecha de aprobación
  $oCredito->modificar_campo($credito_id, 'tb_credito_apr', date('Y-m-d H:i:s'), 'STR');

  $creditolinea_det = 'He aprobado el Crédito Menor número: ' . $credito_id;
  $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

  //en caso haya precredito, y hayan aumentado el monto que prestaron del precredito al credito
  //cuando supervisor aprueba el credito, se actualiza el monto de precredito con el monto del credito
  $wh[0]['column_name'] = 'pcr.tb_precred_xac';
  $wh[0]['param0'] = 1;
  $wh[0]['datatype'] = 'INT';

  $wh[1]['conector'] = 'AND';
  $wh[1]['column_name'] = 'pcr.tb_creditotipo_id';
  $wh[1]['param1'] = 1;
  $wh[1]['datatype'] = 'INT';

  $wh[2]['conector'] = 'AND';
  $wh[2]['column_name'] = 'pcr.tb_credito_id';
  $wh[2]['param2'] = $credito_id;
  $wh[2]['datatype'] = 'INT';

  $res = $oPcr->listar_todos($wh, array(), array());
  if ($res['estado'] == 1) {
    $pcr_reg = $res['data'][0];

    //buscar monto del credito
    $res1 = $oCredito->mostrarUno($credito_id)['data'];
    $oCredito->credito_preaco = $res1['tb_credito_preaco'];
    $oCredito->moneda_id = $res1['tb_moneda_id'];
    $oCredito->credito_int = $res1['tb_credito_int'];

    $oHist_n->tablanom = 'tb_precred';
    $oHist_n->regmodid = $pcr_reg['tb_precred_id'];
    $oHist_n->usureg = $_SESSION['usuario_id'];

    if ($oCredito->credito_preaco != $pcr_reg['tb_precred_monto']) {
      $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_precred_monto', $oCredito->credito_preaco, 'STR');
      $oHist_n->columnamod = 'tb_precred_monto';
      $oHist_n->valorant = $pcr_reg['tb_precred_monto'];
      $oHist_n->valornuevo = $oCredito->credito_preaco;
      $oHist_n->det = "<b>{$res1['tb_usuario_nom']} {$res1['tb_usuario_ape']}</b> modificó el monto de precredito (" . mostrar_moneda($pcr_reg['tb_precred_monto']) . "), al monto credito (" . mostrar_moneda($oCredito->credito_preaco) . ") | <b>" . mostrar_fecha_hora($res1['tb_credito_reg']) . "</b><br>";
      $oHist_n->det .= "<b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> aprobó el cambio en el monto | <b>" . mostrar_fecha_hora(date('Y-m-d H:i:s')) . "</b><br>";
      $oHist_n->insertar();
    }
    if ($oCredito->moneda_id != $pcr_reg['tb_moneda_id']) {
      $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_moneda_id', $oCredito->moneda_id, 'INT');
      $oHist_n->columnamod = 'tb_moneda_id';
      $oHist_n->valorant = $pcr_reg['tb_moneda_id'];
      $oHist_n->valornuevo = $oCredito->moneda_id;
      $oHist_n->insertar();
    }
    if ($oCredito->credito_int != $pcr_reg['tb_precred_int']) {
      $oPcr->modificar_campo($pcr_reg['tb_precred_id'], 'tb_precred_int', $oCredito->credito_int, 'STR');
      $oHist_n->columnamod = 'tb_precred_int';
      $oHist_n->valorant = $pcr_reg['tb_precred_int'];
      $oHist_n->valornuevo = $oCredito->credito_int;
      $oHist_n->det = "<b>{$res1['tb_usuario_nom']} {$res1['tb_usuario_ape']}</b> modificó el % de interés del precredito (" . $pcr_reg['tb_precred_int'] . "%), al credito (" . $oCredito->credito_int . "%) | <b>" . mostrar_fecha_hora($res1['tb_credito_reg']) . "</b><br>";
      $oHist_n->det .= "<b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> aprobó el cambio en el % de interés | <b>" . mostrar_fecha_hora(date('Y-m-d H:i:s')) . "</b><br>";
      $oHist_n->insertar();
    }
  }

  echo json_encode($data);
} 
elseif ($action == 'acta_entrega') {
  $credito_id = $_POST['credito_id'];
  $usuario_id = $_SESSION['usuario_id'];
  $credito_est = 4; // 4 liquidado

  $data['estado'] = 1;
  $data['mensaje'] = 'Acta de entrega generado correctamente';

  //1. Verificamos que el crédito no tenga moras para poder entregar
  $creditotipo_id = 1;
  $mora_total = 0;
  $result = $oCuota->listar_cuotas_moras_impagas($credito_id, $creditotipo_id);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $mora_total += floatval($value['tb_cuota_mor']);
    }
  }
  $result = NULL;

  if ($mora_total > 0) {
    $data['estado'] = 0;
    $data['mensaje'] = 'El cliente aún debe una mora total de: S/. ' . mostrar_moneda($mora_total);
    echo json_encode($data);
    exit();
  }


  //cambios de estado al crédito, mediante modificar campo
  $oCredito->modificar_campo($credito_id, 'tb_credito_est', $credito_est, 'INT');
  //cambios la fecha del acta de entrega
  $oCredito->modificar_campo($credito_id, 'tb_credito_fecact', date('Y-m-d'), 'STR');

  $creditolinea_det = 'He generado el acta de entrega del Crédito Menor número: ' . $credito_id;
  $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

  echo json_encode($data);
} 
elseif ($_POST['action'] == "cuotas") {
  $cre_id = intval($_POST['cre_id']);
  $num_cuo = intval($_POST['num_cuo']);

  $dts = $oCredito->mostrarUno($cre_id);

  if ($dts['estado'] == 1) {
    $cre_numcuo = intval($dts['data']['tb_credito_numcuo']);
  }
  $cre_numcuo += $num_cuo;

  $oCredito->modificar_campo($cre_id, 'tb_credito_numcuo', $cre_numcuo, 'INT');
  $oCredito->modificar_campo($cre_id, 'tb_credito_numcuomax', $cre_numcuo, 'INT');

  $data['estado'] = 1;

  echo json_encode($data);
} 
elseif ($action == 'encuesta') {
  $motivoprestamo_id = intval($_POST['motivoprestamo_id']); //* este motivo viene de la tabla tb_motivoprestamo y se guarda el id en la tabla tb_encuesta
  $tipodedicacion_id = intval($_POST['tipodedicacion_id']); //? este tipo viene de la tabla tb_tipodedicacion y se guarda el id en la tabla tb_cliente
  $cliente_id = intval($_POST['cliente_id']);
  $credito_id = intval($_POST['credito_id']);

  $oEncuesta->usuario_id = intval($_SESSION['usuario_id']);
  $oEncuesta->credito_id = intval($_POST['credito_id']);
  $oEncuesta->cliente_id = $cliente_id;
  $oEncuesta->credito_preaco = moneda_mysql($_POST['credito_preaco']);
  $oEncuesta->motivoprestamo_id = $motivoprestamo_id;
  $oEncuesta->encuesta_motivo = $_POST['motivo']; // viene desde el form del credito en tipo HIDDEN
  $oEncuesta->encuesta_ocu = $_POST['ocupacion']; // viene desde el form del credito en tipo HIDDEN
  $oEncuesta->encuesta_des = $_POST['descripcion'];

  $result = $oEncuesta->mostrarUnoCreditoMenor($credito_id);
  if ($result['estado'] == 1) {
    //! EXISTE UNA ENCUESTA GUARDADA PARA ESTE CREDITO
    $oEncuesta->modificar();
  } else {
    //? SE PUEDE REGISTRAR UNA ENCUESTA NUEVA
    $oEncuesta->insertar();
  }
  $result = NULL;


  //después de insertar la encuesta, modificamos el tipo de dedicación del cliente
  $oCliente->modificar_campo($cliente_id, 'tb_tipodedicacion_id', $tipodedicacion_id, 'INT');
  
  //una vez que la encuesta está lista activamos el crédito, sino no lo haremos válido
  $oCredito->modificar_campo($credito_id, "tb_credito_xac", 1, "INT");

  $data['estado'] = 1;
  $data['mensaje'] = 'Encuesta guardada con Éxito';

  echo json_encode($data);
}
// GERSON (06-10-23)
elseif ($action == 'quitar_remate') {
  $credito_id = intval($_POST['hdd_creditomenor_id']);
  $oCredito->credito_id = $credito_id;

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al modificar el Crédito.';

  if ($oCredito->modificar()) {
    $data['estado'] = 1;
    $data['mensaje'] = 'credito modificado correctamente.';

    $creditolinea_det = 'He modificado el Crédito Menor número: ' . $credito_id;
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);
  }

  echo json_encode($data);
}
elseif($action == 'ultimo_score'){
  $ultimo_score = '<b>Sin Score</b>';
  $ultimo_semaforo = '<b>Sin Color</b>';
  $cliente_id = intval($_POST['cliente_id']);

  $result = $oCredito->ultimo_score_cliente($cliente_id, 0); //buscamos en todos los créditos del cliente
    if($result['estado'] == 1){
      $ultimo_score = '<b>'.$result['data']['tb_credito_score'].'</b>';
      $semaforo = intval($result['data']['tb_credito_semaforo']);

      if($semaforo == 1)
        $ultimo_semaforo = '<span class="badge bg-green">Normal</span>';
      elseif($semaforo == 2)
        $ultimo_semaforo = '<span class="badge bg-yellow">CPP</span>';
      elseif($semaforo == 3)
        $ultimo_semaforo = '<span class="badge bg-orange">Deficiente</span>';
      elseif($semaforo == 4)
        $ultimo_semaforo = '<span class="badge bg-red">Dudoso</span>';
      elseif($semaforo == 5)
        $ultimo_semaforo = '<span class="badge bg-black">Pérdida</span>';
      else
        $ultimo_semaforo = '<b>Sin Color</b>';
    }
  $result = NULL;
  
  $data['ultimo_score'] = $ultimo_score;
  $data['ultimo_semaforo'] = $ultimo_semaforo;

  echo json_encode($data);
}
//
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
