<?php 
  require_once('../filestorage/Filestorage.class.php');
  $oFilestorage = new Filestorage();

  $modulo_id = (isset($_POST['modulo_id']))? $_POST['modulo_id'] : intval($modulo_id);
  $modulo_nom = (isset($_POST['modulo_nom']))? $_POST['modulo_nom'] : $modulo_nom;
  $usuariogrupo_id = isset($_SESSION['usuariogrupo_id'])? intval($_SESSION['usuariogrupo_id']) : intval($_POST['usuariogrupo_id']);

  $doc_cm_contrato = '';
  $doc_cm_anexo = '';
  $doc_cm_actaentrega = '';
  $subir_contrato = '<button type="button" class="btn btn-success btn-xs" onclick="filestorage_form(\'cm_contrato\')"><i class="fa fa-upload"></i></button>';
  $subir_anexo = '<button type="button" class="btn btn-success btn-xs" onclick="filestorage_form(\'cm_anexo\')"><i class="fa fa-upload"></i></button>';
  $subir_actaentrega = '<button type="button" class="btn btn-success btn-xs" onclick="filestorage_form(\'cm_actaentrega\')"><i class="fa fa-upload"></i></button>';

  $result = $oFilestorage->listar_filestorages($modulo_nom, $modulo_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        if($value['modulo_subnom'] == 'cm_contrato'){
          $doc_cm_contrato = '<a class="btn btn-info btn-xs" href="'.$value['filestorage_url'].'" target="_blank" id="btn_cm_contrato"><i class="fa fa-file-pdf-o"></i></a>';
          $subir_contrato = '';
        }
        if($value['modulo_subnom'] == 'cm_anexo'){
          $doc_cm_anexo = '<a class="btn btn-info btn-xs" href="'.$value['filestorage_url'].'" target="_blank" id="btn_cm_anexo"><i class="fa fa-file-pdf-o"></i></a>';
          $subir_anexo = '';
        }
        if($value['modulo_subnom'] == 'cm_actaentrega'){
          $doc_cm_actaentrega = '<a class="btn btn-info btn-xs" href="'.$value['filestorage_url'].'" target="_blank" id="btn_cm_actaentrega"><i class="fa fa-file-pdf-o"></i></a>';
          $subir_actaentrega = '';
        }
      }
    }
  $result = NULL;
?>
<table class="table table-hover">
  <tr>
    <th>Nombre Documento</th>
    <th>Documento</th>
    <th>Opciones</th>
  </tr>
  <tbody>
    <tr>
      <td>Contrato de C-menor</td>
      <td><?php echo $doc_cm_contrato;?></td>
      <td>
        <?php echo $subir_contrato;?>
        <?php if(intval($usuariogrupo_id) == 2):?>
          <button type="button" class="btn btn-danger btn-xs" onclick="filestorage_eliminar_submodulo('cm_contrato', <?php echo $modulo_id;?>)"><i class="fa fa-trash"></i></button>
        <?php endif;?>
      </td>
    </tr>
    <tr>
      <td>Anexo de C-menor</td>
      <td><?php echo $doc_cm_anexo;?></td>
      <td>
        <?php echo $subir_anexo;?>
        <?php if(intval($usuariogrupo_id) == 2):?>
          <button type="button" class="btn btn-danger btn-xs" onclick="filestorage_eliminar_submodulo('cm_anexo', <?php echo $modulo_id;?>)"><i class="fa fa-trash"></i></button>
        <?php endif;?>
      </td>
    </tr>
    <tr>
      <td>Acta de Entrega</td>
      <td><?php echo $doc_cm_actaentrega;?></td>
      <td>
        <?php echo $subir_actaentrega;?>
        <?php if(intval($usuariogrupo_id) == 2):?>
          <button type="button" class="btn btn-danger btn-xs" onclick="filestorage_eliminar_submodulo('cm_actaentrega', <?php echo $modulo_id;?>)"><i class="fa fa-trash"></i></button>
        <?php endif;?>
      </td>
    </tr>
  </tbody>
</table>