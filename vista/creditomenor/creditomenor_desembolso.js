
$(document).ready(function() {
  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    vMin: '0.00',
    vMax: $('#hdd_desembolsar_max').val()
  });
  $('#creditomenor_picker').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
  });
  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: `icheckbox_flat-green`,
    radioClass: `iradio_flat-green`
  });

  $('#cmb_formadesembolso_id').change(function() {
    var forma_id = parseInt($(this).val());
    if(forma_id == 2)
      $('.desembolso_banco').show(300)
    else 
      $('.desembolso_banco').hide(300)
  });

  $('#txt_desembolso_numope').change(function() {
    var desembolso_numope = $(this).val().trim();
    if(desembolso_numope){
      $.ajax({
        type: "POST",
        url: VISTA_URL+"deposito/deposito_controller.php",
        async: true,
        dataType: "json",
        data:({
          action: 'validar_numope',
          numero_ope: desembolso_numope
        }),
        beforeSend: function() {
          
        },
        success: function(data){
          console.log(data)
          if(parseInt(data.estado) > 0){
            notificacion_success(data.mensaje, 5000)
          }
          else{
            alerta_warning('IMPORTANTE', data.mensaje)
            $('#txt_desembolso_numope').val('')
          }
        },
        complete: function(data){
        },
        error: function(data){
          alerta_error('ERROR', data.responseText)
          console.log(data)
        }
      });
    }
  });

  $('#form_creditomenor_desembolso').validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL+"egreso/egreso_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_creditomenor_desembolso").serialize(),
        beforeSend: function() {
          $('#creditomenor_mensaje').show(400);
          $('#btn_guardar_creditomenor').prop('disabled', true);
        },
        success: function(data){
          if(parseInt(data.estado) > 0){
            $('#creditomenor_mensaje').removeClass('callout-info').addClass('callout-success')
            $('#creditomenor_mensaje').html(data.mensaje);
            creditomenor_tabla();
            $('#modal_registro_creditomenor_desembolso').modal('hide');
            
            if ($('#imprimir_egreso').prop('checked')) {
              egreso_imppos_datos3(data.egreso_id, $('#hdd_empresa_id').val());
            }
          }
          else{
            $('#creditomenor_mensaje').removeClass('callout-info').addClass('callout-warning')
            $('#creditomenor_mensaje').html('Alerta: ' + data.mensaje);
            $('#btn_guardar_creditomenor').prop('disabled', false);
          }
        },
        complete: function(data){
            
        },
        error: function(data){
          alerta_error('ERROR', data.responseText)
          console.log(data)
        }
      });
    },
    rules: {
      txt_credito_fecdes: {
        required: true
      },
      txt_credito_mon: {
        required: true
      },
      txt_desembolso_numope:{
        required: function(){
          var forma_id = $('#cmb_formadesembolso_id').val();
          if(parseInt(forma_id) == 2)
            return true;
          else
            return false;
        }
      },
      txt_desembolso_numcuenta:{
        required: function(){
          var forma_id = $('#cmb_formadesembolso_id').val();
          if(parseInt(forma_id) == 2)
            return true;
          else
            return false;
        }
      },
      txt_desembolso_banco:{
        required: function(){
          var forma_id = $('#cmb_formadesembolso_id').val();
          if(parseInt(forma_id) == 2)
            return true;
          else
            return false;
        }
      },
      cmb_subcuenta_id:{
        min: function(){
          var forma_id = $('#cmb_formadesembolso_id').val();
          if(parseInt(forma_id) == 2)
            return 1;
          else
            return 0;
        }
      }
    },
    messages: {
      txt_credito_fecdes: {
        required: "Fecha de sembolso"
      },
      txt_credito_mon: {
        required: "Monto a desembolsar"
      },
      txt_desembolso_numope:{
        required: "Número de Operación"
      },
      txt_desembolso_numcuenta:{
        required: "Número de Cuenta"
      },
      txt_desembolso_banco:{
        required: "Banco o Billetera"
      },
      cmb_subcuenta_id:{
        min: "Orginen de cuenta IPDN"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      // Add the `help-block` class to the error element
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
    }
  });
});

function egreso_imppos_datos3(id_egr, empresa_id) {
  window.open(VISTA_URL + "egreso/egreso_ticket.php?egreso_id=" + id_egr + "&empresa_id=" + empresa_id);
}
