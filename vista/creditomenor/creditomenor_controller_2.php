<?php

require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../creditomenor/Creditomenor.class.php');
$oCredito = new Creditomenor();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../garantiafile/Garantiafile.class.php');
$oGarantiafile = new Garantiafile();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];
$creditotipo_id = 1; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
$usuario_id = intval($_SESSION['usuario_id']);
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

$oCredito->credito_usureg = intval($_SESSION['usuario_id']);
$oCredito->credito_usumod = intval($_POST['usuario_id']);
$oCredito->cliente_id = intval($_POST['hdd_cliente_id']);
$oCredito->cuotatipo_id = intval($_POST['cmb_cuotatipo_id']);
$oCredito->moneda_id = intval($_POST['cmb_moneda_id']);
$oCredito->credito_tipcam = moneda_mysql($_POST['txt_credito_tipcam']);
$oCredito->credito_preaco = moneda_mysql($_POST['txt_credito_preaco']);
$oCredito->credito_int = moneda_mysql($_POST['txt_credito_int']);
$oCredito->credito_numcuo = intval($_POST['txt_credito_numcuo']);
$oCredito->credito_numcuomax = intval($_POST['txt_credito_numcuo']);
$oCredito->credito_linapr = moneda_mysql($_POST['txt_credito_linapr']);
$oCredito->representante_id = intval($_POST['cmb_representante_id']);
$oCredito->cuentadeposito_id = intval($_POST['cmb_cuentadeposito_id']);
$oCredito->credito_obs = $_POST['txt_credito_obs'];
$oCredito->credito_feccre = fecha_mysql($_POST['txt_credito_feccre']);
$oCredito->credito_fecdes = fecha_mysql($_POST['txt_credito_fecdes']);
$oCredito->credito_comdes = $_POST['txt_credito_comdes'];
$oCredito->credito_his = 'Crédito registrado por: <b>' . $_SESSION['usuario_nom'] . '</b> | <b>' . date('d-m-Y h:i a') . '</b><br>';
//  $oCredito->empresa_id = $_SESSION['empresa_id'];
$credito_repor=intval($_POST['cbx_credito_repor']);
$credito_porcentaje=intval($_POST['cbo_credito_porcentaje']);
if ($action == 'insertar') {

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Crédito.';
    $cliente_id = intval($_POST['hdd_cliente_id']);

    if ($cliente_id <= 0) {
        $data['mensaje'] = 'Falta seleccionar el cliente por favor.';
        echo json_encode($data);
        exit();
    }

    $result = $oCredito->insertar();
    if ($result['estado'] == 1) {
        $data['estado'] = 1;
        $data['credito_id'] = $result['credito_id'];
        $data['mensaje'] = 'Crédito registrado, garantías pendientes. - ' . $data['credito_id'];

        $moneda_nom = 'S/.';
        if (intval($_POST['cmb_moneda_id']) == 2)
            $moneda_nom = 'US$';

        $credito_id = intval($data['credito_id']);
  $oCredito->modificar_campo($credito_id, 'tb_empresa_id', $_SESSION['empresa_id'],'INT');
  $oCredito->modificar_campo($credito_id, 'tb_credito_reporte', $credito_repor,'INT');
  $oCredito->modificar_campo($credito_id, 'tb_credito_porcentaje', $credito_porcentaje,'INT');

        $creditolinea_det = 'He registrado el Crédito Menor número: ' . $data['credito_id'] . '. Con un préstamo de ' . $moneda_nom . ' ' . $_POST['txt_credito_preaco'];
        $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);
    }

    echo json_encode($data);
} elseif ($action == 'modificar') {
    $credito_id = intval($_POST['hdd_creditomenor_id']);
    $oCredito->credito_id = $credito_id;

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar el Crédito.';

    if ($oCredito->modificar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'credito modificado correctamente.';

        $creditolinea_det = 'He modificado el Crédito Menor número: ' . $credito_id;
        $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);
    }

    echo json_encode($data);
} elseif ($action == 'cancelar' || $action == 'eliminar') {
    if ($action == 'eliminar' && $usuariogrupo_id != 2) {
        $data['estado'] = 0;
        $data['mensaje'] = 'Solicite a administración para poder eliminar un crédito por favor.';
        echo json_encode($data);
        exit();
    }
    //al cancelar el credito, el credito queda con xac = 0, se borra: garantias, fotos, cuotas
    $credito_id = (intval($_POST['credito_id']) > 0) ? intval($_POST['credito_id']) : intval($_POST['hdd_creditomenor_id']);
    $moneda_id = (intval($_POST['moneda_id']) > 0) ? intval($_POST['moneda_id']) : intval($_POST['hdd_moneda_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar al credito.';
    //primero verificamos que no tenga desembolsos el crédito
    $egreso_imp = 0;
    $modulo_id = 51;
    $egreso_modide = $credito_id;
    $egreso_xac = 1;
    $result = $oEgreso->mostrar_por_modulo($modulo_id, $egreso_modide, $moneda_id, $egreso_xac);
    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            $egreso_imp += floatval($value['tb_egreso_imp']);
        }
    }
    $result = NULL;

    if ($egreso_imp > 0) {
        $data['estado'] = 0;
        $data['mensaje'] = 'Este Crédito tiene desembolsado un total de: ' . $egreso_imp . ', no se puede elimiar.';
        echo json_encode($data);
        exit();
    }
    //elimando las garantías y fotos
    $result = $oCredito->creditomenor_garantias($credito_id);
    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            $garantia_id = $value['tb_garantia_id'];

            $result2 = $oGarantiafile->listar_garantiafiles($garantia_id);
            if ($result2['estado'] == 1) {
                foreach ($result2['data'] as $key => $value) {
                    $garantiafile_id = $value['tb_garantiafile_id'];
                    $garantiafile_url = $value['tb_garantiafile_url'];
                    if (file_exists('../../' . $garantiafile_url)) {
                        unlink('../../' . $garantiafile_url);
                    }
                    $oGarantiafile->eliminar($garantiafile_id);
                }
            }
            $result2 = NULL;
            $oGarantia->eliminar($garantia_id);
        }
    }
    $result = NULL;

    //eliminando las cuotas
    $result = $oCuota->cuotas_credito($credito_id, 1, 'tb_creditomenor'); // 1 creditotipo C -MENOR
    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            $cuota_id = $value['tb_cuota_id'];
            $oCuota->eliminar($cuota_id);
        }
    }
    $result = NULL;

    //cambiando XAC = 0 al crédito
    $oCredito->modificar_campo($credito_id, "tb_credito_xac", 0, "INT");

    $creditolinea_det = 'He cancelado la culminación del Crédito Menor número: ' . $credito_id;
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

    $data['estado'] = 1;
    $data['mensaje'] = 'Datos del crédito eliminado correctamente';

    echo json_encode($data);
} elseif ($action == 'estado') {
    $credito_id = $_POST['credito_id'];
    $credito_est = $_POST['credito_est'];
    $usuario_id = $_SESSION['usuario_id'];

    $data['estado'] = 1;
    $data['mensaje'] = 'Crédito aprobado correctamente';

    //cambios de estado al crédito, mediante modificar campo
    $oCredito->modificar_campo($credito_id, 'tb_credito_est', $credito_est, 'INT');
    //guardamos el usuario que aprubea el crédito
    $oCredito->modificar_campo($credito_id, 'tb_credito_usuapr', $usuario_id, 'INT');
    //guardamos la fecha de aprobación
    $oCredito->modificar_campo($credito_id, 'tb_credito_apr', date('Y-m-d H:i:s'), 'STR');

    $creditolinea_det = 'He aprobado el Crédito Menor número: ' . $credito_id;
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

    echo json_encode($data);
} elseif ($action == 'acta_entrega') {
    $credito_id = $_POST['credito_id'];
    $usuario_id = $_SESSION['usuario_id'];
    $credito_est = 4; // 4 liquidado

    $data['estado'] = 1;
    $data['mensaje'] = 'Acta de entrega generado correctamente';

    //1. Verificamos que el crédito no tenga moras para poder entregar
    $creditotipo_id = 1;
    $mora_total = 0;
    $result = $oCuota->listar_cuotas_moras_impagas($credito_id, $creditotipo_id);
    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            $mora_total += floatval($value['tb_cuota_mor']);
        }
    }
    $result = NULL;

    if ($mora_total > 0) {
        $data['estado'] = 0;
        $data['mensaje'] = 'El cliente aún debe una mora total de: S/. ' . mostrar_moneda($mora_total);
        echo json_encode($data);
        exit();
    }


    //cambios de estado al crédito, mediante modificar campo
    $oCredito->modificar_campo($credito_id, 'tb_credito_est', $credito_est, 'INT');
    //cambios la fecha del acta de entrega
    $oCredito->modificar_campo($credito_id, 'tb_credito_fecact', date('Y-m-d'), 'STR');

    $creditolinea_det = 'He generado el acta de entrega del Crédito Menor número: ' . $credito_id;
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $creditolinea_det);

    echo json_encode($data);
} elseif ($_POST['action'] == "cuotas") {
    $cre_id = intval($_POST['cre_id']);
    $num_cuo = intval($_POST['num_cuo']);

    $dts = $oCredito->mostrarUno($cre_id);

    if ($dts['estado'] == 1) {
        $cre_numcuo = intval($dts['data']['tb_credito_numcuo']);
    }
    $cre_numcuo += $num_cuo;

    $oCredito->modificar_campo($cre_id, 'tb_credito_numcuo', $cre_numcuo, 'INT');
    $oCredito->modificar_campo($cre_id, 'tb_credito_numcuomax', $cre_numcuo, 'INT');

    $data['estado'] = 1;

    echo json_encode($data);
} else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>