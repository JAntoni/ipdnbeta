<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../creditomenor/Creditomenor.class.php');
  $oCreditomenor = new Creditomenor();
  require_once('../egreso/Egreso.class.php');
  $oEgreso = new Egreso();
  require_once('../funciones/funciones.php');

  $direc = 'creditomenor';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $creditomenor_id = $_POST['creditomenor_id'];

  $titulo = '';
  if($usuario_action == 'L'){
    $titulo = 'Creditomenor Registrado';
  }
  if($usuario_action == 'I'){
    $titulo = 'Registrar Desembolso';
  }
  elseif($usuario_action == 'M'){
    $titulo = 'Editar Creditomenor';
  }
  elseif($usuario_action == 'E'){
    $titulo = 'Eliminar Creditomenor';
  }
  else{
    $titulo = 'Acción de Usuario Desconocido';
  }

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en creditomenor
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'creditomenor'; $modulo_id = $creditomenor_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }

  $credito_preaco = 0;
  $egreso_imp = 0;
  $credito_des = 0;
  $credito_fecdes = date('d-m-Y');
  if($creditomenor_id > 0){
    $result = $oCreditomenor->mostrarUno($creditomenor_id);
      $credito_preaco = floatval($result['data']['tb_credito_preaco']);
      $moneda_id = intval($result['data']['tb_moneda_id']);
    $result = NULL;

    $modulo_id = 51; $egreso_modide = $creditomenor_id; $egreso_xac = 1;
    $result = $oEgreso->mostrar_por_modulo($modulo_id, $egreso_modide, $moneda_id, $egreso_xac);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $egreso_imp += floatval($value['tb_egreso_imp']);
        }
      }
    $result = NULL;
  }

  $credito_des = $credito_preaco - $egreso_imp;
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditomenor_desembolso" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_creditomenor_desembolso" method="post" class="form-horizontal">
          <input type="hidden" name="action" value="<?php echo $action.'_desembolso';?>">
          <input type="hidden" name="vista" value="<?php echo 'creditomenor';?>">
          <input type="hidden" name="hdd_credito_id" value="<?php echo $creditomenor_id;?>">
          
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-widget widget-user-2">  
                  <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                      <li>
                        <a href="javascript:void(0)">Monto total <span class="pull-right badge bg-blue"><?php echo 'S/. '.mostrar_moneda($credito_preaco);?></span></a>
                      </li>
                      <li>
                        <a href="javascript:void(0)">Monto Desembolsado <span class="pull-right badge bg-blue"><?php echo 'S/. '.mostrar_moneda($egreso_imp)?></span></a>
                      </li>
                      <li>
                        <a href="javascript:void(0)">Máximo a desembolsar <span class="pull-right badge bg-blue"><?php echo 'S/. '.mostrar_moneda($credito_des)?></span></a>
                        <input type="hidden" id="hdd_desembolsar_max" name="hdd_desembolsar_max" value="<?php echo $credito_des;?>">
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_credito_fecdes" class="col-sm-2 control-label">Fecha</label>
                  <div class="col-sm-10">
                    <div class='input-group date' id='creditomenor_picker'>
                      <input type='text' class="form-control" name="txt_credito_fecdes" id="txt_credito_fecdes" value="<?php echo $credito_fecdes;?>"/>
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="cmb_moneda_id" class="col-sm-2 control-label">Moneda</label>
                  <div class="col-sm-10">
                    <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id" style="width: 65px;">
                      <?php require_once('../moneda/moneda_select.php');?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_credito_mon" class="col-sm-2 control-label">Monto</label>
                  <div class="col-sm-10">
                    <input type="text" name="txt_credito_mon" id="txt_credito_mon" class="form-control input-sm moneda" value="<?php echo mostrar_moneda($credito_des)?>">
                  </div>
                </div>
              </div>
            </div>
            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="creditomenor_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_creditomenor">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_creditomenor">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_creditomenor">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/creditomenor/creditomenor_desembolso.js?ver=324646464676746767';?>"></script>
