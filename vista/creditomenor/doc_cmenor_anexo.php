<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');

/* require_once ("../../config/Cado.php"); */
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../tarifario/Tarifario.class.php');
$oTarifario = new Tarifario();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$creditomenor_id = intval($_GET['d1']);
$tipo_cuota = $_GET['tipo_cuota'];

$title = 'AMEXO C-MENOR';
$codigo = 'ANEXO-'.str_pad($creditomenor_id, 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {
  public function Header() {
    //$image_file = K_PATH_IMAGES.'logo.jpg';
    //$this->Image($image_file, 20, 10, 30, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
  }
  public function Footer() {
    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(5, 10, 5);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$result = $oCredito->mostrarUno($creditomenor_id);
  if($result['estado'] == 1){
    $reg = mostrar_fecha_hora($result['data']['tb_credito_reg']);
    $mod = mostrar_fecha_hora($result['data']['tb_credito_mod']);
    $apr = mostrar_fecha_hora($result['data']['tb_credito_apr']);

    $usuario_nom = $result['data']['tb_usuario_nom'];

    $cuotip_id = $result['data']['tb_cuotatipo_id'];
    $mon_id = $result['data']['tb_moneda_id'];
    $credito_tipcap = $result['data']['tb_credito_tipcap'];
    $empresa_id = $result['data']['tb_empresa_id'];
    //fechas
    $credito_feccre = mostrar_fecha($result['data']['tb_credito_feccre']);
    $credito_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
    $credito_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
    $credito_preaco = $result['data']['tb_credito_preaco'];
    $credito_int = $result['data']['tb_credito_int'];
    $credito_numcuo = $result['data']['tb_credito_numcuo'];
    $credito_linapr = $result['data']['tb_credito_linapr'];
    $credito_numcuomax = $result['data']['tb_credito_numcuomax'];
    //$transferencia_bien=($credito_linapr-$credito_preaco)*$credito_numcuomax+$credito_preaco;
    $transferencia_bien = $credito_linapr;
    //representante
    $rep_id = $result['data']['tb_representante_id'];
    $rep_nom = $result['data']['tb_representante_nom'];
    $rep_dni = $result['data']['tb_representante_dni'];
    //cuenta deposito
    $cuedep_id = $result['data']['tb_cuentadeposito_id'];
    $cuedep_num = $result['data']['tb_cuentadeposito_num'];
    $cuedep_ban = $result['data']['tb_cuentadeposito_ban'];

    $credito_pla = $result['data']['tb_credito_pla'];
    $credito_comdes = $result['data']['tb_credito_comdes'];
    $credito_obs = $result['data']['tb_credito_obs'];
    $credito_webref = $result['data']['tb_credito_webref'];
    $credito_est = $result['data']['tb_credito_est'];
    //cliente
    $cliente_id = $result['data']['tb_cliente_id'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_dir = $result['data']['tb_cliente_dir'];
    $cliente_ema = $result['data']['tb_cliente_ema'];
    $cliente_fecnac = $result['data']['tb_cliente_fecnac'];
    $cliente_tel = $result['data']['tb_cliente_tel'];
    $cliente_cel = $result['data']['tb_cliente_cel'];
    $cliente_telref = $result['data']['tb_cliente_telref'];
    $cliente_ubigeo = $result['data']['tb_ubigeo_cod'];
  }
$result = NULL;

$interes_mensual_decimal = $credito_int / 100;
$interes_anual = pow((1 + $interes_mensual_decimal), 12) - 1;
$tea = formato_numero($interes_anual * 100);
$tcea = $tea;

//ubigeo del cliente
if($cliente_ubigeo  >0){
  $result = $oUbigeo->mostrarUbigeo($cliente_ubigeo);
    if($result['estado'] == 1){
      $cliente_dep = $result['data']['Departamento'];
      $cliente_pro = $result['data']['Provincia'];
      $cliente_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

$nombre_cuotas = 'CUOTAS MENSUALES';
if($tipo_cuota == 'fijo')
  $nombre_cuotas = 'CUOTAS FIJAS';

//? 1. LISTAMOS LAS GARANTÍAS DEL CRÉDITO MENOR
$garantia_pro_text = '';
$garantia_ser_text = '';
$garantia_det_text = '';
$garantia_suma_valtas = 0;

$result = $oGarantia->listar_garantias($creditomenor_id);
  $cont = 0;
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $garantia_can = $value['tb_garantia_can'];
      $garantia_pro = $value['tb_garantia_pro'];
      $garantia_ser = $value['tb_garantia_ser'];
      $garantia_kil = $value['tb_garantia_kil'];
      $garantia_pes = $value['tb_garantia_pes'];
      $garantia_tas = $value['tb_garantia_tas'];
      $garantia_det = $value['tb_garantia_det'];

      $garantia_suma_valtas += floatval($value['tb_garantia_valtas']);

      $cont++;

      $garantia_pro_text .= '<tr>
        <td colspan="4" border="1.5" align="center">Prod. N° '.$cont.'</td>
        <td colspan="16" border="1.5" align="center">'.$value['tb_garantia_pro'].'</td></tr>';
      $garantia_ser_text .= '<tr>
        <td colspan="4" border="1.5" align="center">Prod. N° '.$cont.'</td>
        <td colspan="16" border="1.5" align="center">'.$value['tb_garantia_ser'].'</td></tr>';
      $garantia_det_text .= '<tr>
        <td colspan="4" border="1.5" align="center">Prod. N° '.$cont.'</td>
        <td colspan="16" border="1.5" align="center">'.$value['tb_garantia_det'].'</td></tr>';
    }
  }
$result = NULL;

//? 2. LISTAMOS LAS CUOTAS DEL CRÉDITO YA SEA CUOTAS LIBRES O FIJAS
$result = $oCuota->primera_cuota_por_credito($creditomenor_id, 1);
  if($result['estado'] == 1){
    $cuota_fecha = mostrar_fecha($result['data']['tb_cuota_fec']);
    $cuota_int = $result['data']['tb_cuota_int'];
    $cuota_cuo = $result['data']['tb_cuota_cuo'];
  }
$result = NULL;

list($cuota_dia, $cuota_mes, $cuota_anio) = explode('-', $cuota_fecha);

$cronograma = ''; $i = 0; $interes_total = 0;

$result = $oCuota->obtener_cuotas_por_credito(1, $creditomenor_id);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $i++;
      $cm_cuota_cap = $value['tb_cuota_cap'];
      $cm_cuota_amo = $value['tb_cuota_amo'];
      $cm_cuota_int = $value['tb_cuota_int'];
      $cm_cuota_cuo = $value['tb_cuota_cuo'];
      $cm_cuota_fec = mostrar_fecha($value['tb_cuota_fec']);
      $interes_total += floatval($cm_cuota_int);

      $cronograma .='
        <tr>
          <td colspan="3" border="1.5">'.$cm_cuota_fec.'</td>
          <td colspan="3" border="1.5" align="center">'.$i.'</td>
          <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($cm_cuota_cap).'</td>
          <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($cm_cuota_amo).'</td>
          <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($cm_cuota_int).'</td>
          <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($cm_cuota_cuo).'</td>
        </tr>';
    }
  }
$result = NULL;

//? 3. MOSTRAMOS LOS DATOS GLOBALES LLAMADO TARIFARIO
$result = $oTarifario->mostrar_por_fecha_periodo(fecha_mysql($credito_feccre));
  if($result['estado'] == 1){
    $tarifario_fecini = mostrar_fecha($result['data']['tb_tarifario_fecini']);
    $tarifario_fecfin = mostrar_fecha($result['data']['tb_tarifario_fecfin']);
    $creditotipo_id = $result['data']['tb_creditotipo_id'];
    $tarifario_dupcon = $result['data']['tb_tarifario_dupcon']; //duplicado de contrato
    $tarifario_comant = $result['data']['tb_tarifario_comant']; //comision por pago anticipado
    $tarifario_gast = $result['data']['tb_tarifario_gast']; //gasto por segundo transporte
    $tarifario_cus = $result['data']['tb_tarifario_cus']; //monto de pago de custodia
    $tarifario_intmor = $result['data']['tb_tarifario_intmor']; //interés moratorio
    $tarifario_emi = $result['data']['tb_tarifario_emi']; //emisora de pago
  }
$result = NULL;

$codigo_cmenor ='CM-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);

$html = '
  <style>
      table {
        padding-left:4px;
      }
    </style>

    <table>
    <tr>
    <td style="width:40%;"><img src="'.K_PATH_IMAGES.'logo.jpg"/></td>
    <td style="width:60%;">
    <br/>
    <br/>
      <table style="padding-top:5px;padding-bottom:5px;">
        <tr>
          <td border="1.5" style="width:70%;color:#FFFFFF;background-color:#029799;">
            <strong>MODALIDAD: CREDMENOR</strong>
          </td>
          <td border="1.5" style="width:30%;">
            <strong>'.$nombre_cuotas.'</strong>
          </td>
        </tr>
        <tr>
          <td colspan="2">© Préstamos del Norte.<br/>
          <span>  </span>Prestamos de Efectivo con Garantía en Bienes y Valores.<br/>
          <span>  </span>Llámanos y obtén una cotización: CEL: 923 347 541 / TELÉFONO: 074 - 640041
          </td>
        </tr>
      </table>

    </td>
    </tr>
    </table>

    <table style="width:100%;">
      <tr border="2">
        <td style="width:20%;"></td>
        <td colspan="2" align="center" border="2" style="width:60%;">
          <strong>ANEXO: 01 HOJA RESUMEN</strong>
        </td>
        <td style="width:20%;"></td>
      </tr>
    </table>
    <br/>
    <table border="2" style="padding-left: 10px;padding: 5px;">
      <tr>
        <td>Por el presente documento DECLARO BAJO JURAMENTO lo siguiente:<br/>
 1.- La veracidad de la información consignada en el presente documento y documentos presentados exigidos, son copia fiel del original, caso contrario estaré incurriendo en delito contra el Código Penal vigente.<br/>
 2.- Que la procedencia de los bienes materia del presente contrato es lícita y que cuento con la validez legal para decidir sobre ellos.<br/>
 3.- Que no pesa sobre ellos cargo ni gravamen alguno.<br/>
 4.- Que de incumplir cualesquiera de las condiciones antes dichas me someteré a las condiciones que INVERSIONES Y PRESTAMOS DEL NORTE S.A.C determine.
        </td>
      </tr>
    </table>
    <br/>
    <br/>

    <table>
      <tr>
        <td colspan="5" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Nombres y Apellidos:</strong></td>
        <td colspan="7" style="color:#FFFFFF;background-color:#029799;" border="1.5">Impresión: <b>'.date('Y-m-d h:i: A').'</b></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td colspan="20" border="1.5" align="center">'.$cliente_nom.'</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>DNI:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Fecha de Nac.</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Cod. Cliente</strong></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td colspan="6" border="1.5" align="left">'.$cliente_doc.'</td>
        <td></td>
        <td colspan="6" border="1.5" align="center">'.mostrar_fecha($cliente_fecnac).'</td>
        <td></td>
        <td colspan="6" border="1.5" align="center">'.$codigo_cmenor.'</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Dirección:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>E-mail:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td colspan="10" border="1.5" align="center">'.$cliente_dir.' - '.$cliente_dis.'</td>
        <td colspan="10" border="1.5" align="center">'.$cliente_ema.'</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Telf. Celular:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Cel. Referencia 1:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Cel. Referencia 2:</strong></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td colspan="6" border="1.5" align="center">'.$cliente_tel.'</td>
        <td></td>
        <td colspan="6" border="1.5" align="center">'.$cliente_cel.'</td>
        <td></td>
        <td colspan="6" border="1.5" align="center">'.$cliente_telref.'</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Prod. Garantía:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      '.$garantia_pro_text.'
    </table>

    <table>
      <tr>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Serie:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      '.$garantia_ser_text.'
    </table>

    <table>
      <tr>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Estado:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      '.$garantia_det_text.'
    </table>
    
    <table>
      <tr>
        <td colspan="3"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Valor Préstamo:</strong></td>
        <td></td>
        <td colspan="4" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Interés del Periodo:</strong></td>
        <td colspan="2" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>TEA:</strong></td>
        <td colspan="2" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>TCEA:</strong></td>
        <td colspan="2" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Moneda:</strong></td>
        <td colspan="2" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Interés:</strong></td>
        <td></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>P. Mínimo:</strong></td>
      </tr>
      <tr>
        <td colspan="3" border="1.5">S/. '.formato_moneda($credito_preaco).'</td>
        <td></td>
        <td border="1.5" align="center">'.intval($credito_int).'%</td>
        <td colspan="3" border="1.5" align="center">MENSUAL</td>
        <td colspan="2" border="1.5" align="center">'.$tea.'%</td>
        <td colspan="2" border="1.5" align="center">'.$tcea.'%</td>
        <td colspan="2" border="1.5">SOLES</td>
        <td colspan="2" border="1.5">S/. '.formato_moneda($cuota_int).'</td>
        <td></td>
        <td colspan="3" border="1.5">S/. '.formato_moneda($cuota_int).'</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Fecha Crédito:</strong></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Vencimiento:</strong></td>
        <td colspan="4" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Fecha Pago:</strong></td>
        <td colspan="2" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Plazo:</strong></td>
        <td colspan="2" style="color:#FFFFFF;background-color:#029799" border="1.5"></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>V. Tasación:</strong></td>
        <td colspan="2" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Tasado:</strong></td>
      </tr>
      <tr>
        <td colspan="4" border="1.5">'.$credito_feccre.'</td>
        <td colspan="3" border="1.5" align="center">'.$cuota_fecha.'</td>
        <td colspan="1" border="1.5" align="center">'.$cuota_dia.'</td>
        <td colspan="3" border="1.5" align="center">CADA MES</td>
        <td colspan="2" border="1.5" align="center">1 MES</td>
        <td colspan="2" border="1.5" align="center">'.$nombre_cuotas.'</td>
        <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($garantia_suma_valtas).'</td>
        <td colspan="2" border="1.5" align="center">SI</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="20" border="1.5" align="center"><b>TARIFARIO POR SERVICIOS ADICIONALES</b></td>
      </tr>
      <tr>
        <td colspan="4"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Duplicado Contrato:</strong></td>
        <td colspan="6" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>TELEFONO DE COBRANZA:</strong></td>
        <td colspan="5" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Gastos 2do transporte:</strong></td>
        <td colspan="2" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Custodia:</strong></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Interés Moratorio:</strong></td>
      </tr>
      <tr>
        <td colspan="4" border="1.5">S/. '.formato_moneda($tarifario_dupcon).'</td>
        <td colspan="6" border="1.5" align="center">945 936 439</td>
        <td colspan="2" border="1.5" align="center">S/. '.formato_moneda($tarifario_gast).'</td>
        <td colspan="3" border="1.5" align="center">1er gratuito</td>
        <td colspan="2" border="1.5" align="center">S/. '.formato_moneda($tarifario_cus).'</td>
        <td colspan="4" border="1.5" align="center">'.$tarifario_intmor.'%</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Nro. Cuenta Depósito:</strong></td>
        <td colspan="5" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Banco de Depósito:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Asesor Designado:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Emisora de Pago:</strong></td>
        <td colspan="2" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Código:</strong></td>
      </tr>
      <tr>
        <td colspan="4" border="1.5">305-2410005-0-92</td>
        <td colspan="5" border="1.5" align="center">BANCO BCP</td>
        <td colspan="3" border="1.5" align="center">'.$usuario_nom.'</td>
        <td colspan="3" border="1.5" align="center">'.$tarifario_emi.'</td>
        <td colspan="2" border="1.5" align="center">'.$codigo_cmenor.'</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Nro. Cuenta Depósito:</strong></td>
        <td colspan="5" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Banco de Depósito:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Nro Celular Pagos:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Aplicación:</strong></td>
        <td colspan="2" style="color:#FFFFFF;background-color:#029799" border="1.5"></td>
      </tr>
      <tr>
        <td colspan="4" border="1.5">0011-0288-0100040139</td>
        <td colspan="5" border="1.5" align="center">BANCO BBVA</td>
        <td colspan="3" border="1.5" align="center">945936439</td>
        <td colspan="3" border="1.5" align="center">YAPE</td>
        <td colspan="2" border="1.5" align="center"></td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="4"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Cronograma:</strong></td>
        <td colspan="16" border="1.5"></td>
      </tr>
      <tr>
        <td colspan="3"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Fecha de Pago:</strong></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>N° de cuota:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Capital del Perdiodo:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Amortización:</strong></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Interés del Perdiodo:</strong></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Cuota:</strong></td>
      </tr>
      '.$cronograma.'
    </table>

    <table>
      <tr>
        <td colspan="3"  style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>N° de cuotas:</strong></td>
        <td colspan="3" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Capital Total:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Interés Total:</strong></td>
        <td colspan="3" align="center" style="color:#FFFFFF;background-color:#029799" border="1.5"><strong>Total a Pagar:</strong></td>
        <td></td>
        <td colspan="6" border="1.5" rowspan="2">Declaro haber tenido a la vista y haber entendido con claridad toda la información contenida en el presente.</td>
        <td></td>
      </tr>
      <tr>
        <td colspan="3" border="1.5" align="center">'.$credito_numcuo.'</td>
        <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($credito_preaco).'</td>
        <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($interes_total).'</td>
        <td colspan="3" border="1.5" align="center">S/. '.formato_moneda($credito_linapr).'</td>
        <td></td>
        <td colspan="6"></td>
        <td></td>
      </tr>
    </table>
';

$html2 .='
  <table style="width:100%;">
      <tr>
        <td colspan="5" style="color:#FFFFFF;background-color:#029799;" border="1.5"><strong>Términos y Condiciones:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td colspan="22" border="1.5" align="justify">
<b><u>1. OPCIONES DE PAGO:</u></b><br/>
1.1. El préstamo será cancelado por <b>EL CLIENTE</b> con el pago total de la obligación adeudada, en los plazos y montos pactados, habiendo cumplido con cancelar la primera y única cuota exigible; y tratándose de créditos bajo el sistema de cuotas, con la frecuencia que se señala en el Cronograma de pagos contenido en <b>EL ANEXO</b>. Los pagos del Préstamo se harán en efectivo en <b>LAS OFICINAS</b> o en los canales habilitados para tal efecto por <b>EL ACREEDOR</b>.<br/>
1.2.  Una vez cancelada la primera cuota exigible, el pago total del capital del Préstamo, así como el interés compensatorio, comisiones, gastos e impuestos pactados, correspondientes al capital del Préstamo, que se devenguen hasta la fecha en que se haga efectivo el pago, así como, en caso de incumplimiento de pago en la fecha pactada, el interés moratorio respectivo.<br/>
1.3.  El pago parcial del capital del Préstamo, con los correspondientes intereses devengados, compensatorios, comisiones, gastos e impuestos pactados que se devenguen hasta la fecha en que se haga efectivo el pago, así como, en caso de incumplimiento de pago en la fecha pactada, el interés moratorio respectivo. Efectuado el pago parcial, las partes determinarán un nuevo plazo de vencimiento, procediendo <b>EL ACREEDOR</b> al recalculo del saldo correspondiente del capital adeudado por <b>EL CLIENTE</b>, el mismo que comprenderá los intereses compensatorios, así como las comisiones, impuestos y gastos en caso correspondan, así como los intereses moratorios en caso de incumplimiento de pago en la fecha pactada, hasta el siguiente pago que realice <b>EL CLIENTE</b>.<br/>
1.4.  <b>EL CLIENTE</b> podrá efectuar pagos anticipados totales o parciales de sus obligaciones, una vez cancelada la primera cuota exigible. En estos casos, los intereses que se cobren serán únicamente los correspondientes a los días calendarios transcurridos y no generarán cobros, comisiones, gasto administrativo o costos adicionales a los correspondientes a capital e interés a la fecha.<br/>
  El Pago anticipado conlleva a la aplicación del monto al capital del crédito, con la consiguiente reducción de los intereses, comisiones y gastos al día del pago. Los pagos mayores a dos (2) cuotas (incluyendo la exigible en el periodo) se consideran pagos anticipados.<br/>
  El Adelanto de cuotas supone la aplicación del monto pagado a las cuotas inmediatamente posteriores a la primera cuota exigible en el periodo, sin que se produzca una reducción de los intereses, comisiones y gastos al día del pago. Los pagos mayores a dos (2) cuotas (que incluyen aquella exigible en el periodo) se consideran adelanto de cuotas.<br/>
  <b>EL CLIENTE</b>, sin embargo, podrá requerir a <b>EL ACREEDOR</b> que los pagos mayores de dos (2) cuotas sean considerados un “Adelanto de cuotas” o que los pagos menores o iguales a dos (2) cuotas sean considerados como “Pago Anticipado”. En el caso de pagos anticipados parciales, <b>EL ACREEDOR</b> requerirá a <b>EL CLIENTE</b>, al momento de realizar el pago, que señale si debe procederse a la reducción del monto de las cuotas restantes o la reducción del número de cuotas, para lo cual le otorgará un plazo de quince (15) días, contados a partir de la realización del pago. De no manifestar su decisión dentro del plazo otorgado a su favor, <b>EL ACREEDOR</b> procederá a la reducción del número de cuotas. <b>EL CLIENTE</b> podrá solicitar al momento de realizar el pago anticipado un nuevo Cronograma de Pagos considerando el pago realizado y el plazo del Préstamo, el plazo para la entrega del nuevo cronograma es no mayor a siete (7) días de efectuada la solicitud. En caso el pago anticipado se realice por un tercero, el Cronograma de Pagos estará a disposición de <b>EL CLIENTE</b> en cualquiera de <b>LAS OFICINAS</b> que solicitó el crédito.<br/>

  <b><u>2. ALMACENAMIENTO DE BIENES OTORGADOS EN GARANTÍA:</u></b><br/>
  2.1.  Los bienes serán conservados en almacenes o bóvedas que serán elegidas por <b>EL ACREEDOR</b>.<br/>
  2.2.  Los bienes no permanecerán en uso.<br/>
  2.3.  Los costos que genere el almacenamiento durante la vigencia del contrato, serán asumidos íntegramente por <b>EL ACREEDOR</b>. Si los bienes permanecen más tiempo dentro de los almacenes o bóvedas designadas a consecuencia de IMPAGOS por parte de EL CLIENTE, esto constituiría un gasto por concepto de CUSTODIA DE BIEN MUEBLE la cual generará un costo de S/. 5.00 (CINCO CON 00/100 SOLES) diarios contados a partir del día siguiente del vencimiento de la cuota hasta el día de la liquidación de la deuda.<br/>

  <b><u>3. INCUMPLIMIENTO DE PAGOS Y REMATE DEL BIEN MUEBLE:</u></b><br/>
3.1.  <b>EL CLIENTE</b>, a la firma de este documento, acepta tener conocimiento que pasados 15 días impagos, contando desde la fecha de pago acordada, <b>EL ACREEDOR</b> puede ejecutar la Garantía Mobiliaria.<br/>
3.2. Queda también entendido que el monto mínimo depositable es el que corresponda y cubra en su totalidad el concepto de “Cuota Mínima”. De darse el caso en que dicho deposito sea insuficiente para cubrir el mencionado costo, se entenderá como Incumplimiento de Pago, por lo cual comenzarán a aplicarse las condiciones de ejecución de Garantía Mobiliaria.

        </td>
      </tr>
    </table>
    <br/>
    <br/>
  <table style="width:100%;padding-top:50px;padding-bottom:3px;">
        <tr>
          <td colspan="15" border="1.5" align="bottom">Firma:</td>

          <td colspan="5" border="1.5" align="bottom";>Huella:</td>
          <td colspan="2"></td>
          <td colspan="20" border="1.5" align="bottom">
            Firma:
            <img src="'.K_PATH_IMAGES.'firma.png" height="50"/>
          </td>
        </tr>
    </table>
    <table style="width:100%;padding-top:5px;padding-bottom:3px;">
        <tr>
          <td colspan="20" border="1.5" align="bottom">Nombre:</td>
          <td colspan="2"></td>
          <td colspan="20" border="1.5" align="bottom">Nombre: VICTOR MANUEL VARGAS TORRES</td>
        </tr>
    </table>
    <table style="width:100%;padding-top:5px;padding-bottom:3px;">
        <tr>
          <td colspan="20" border="1.5" align="bottom">Dni:</td>
          <td colspan="2"></td>
          <td colspan="20" border="1.5" align="bottom">Dni: 44760801</td>
        </tr>
    </table>

    <br/>
    <br/>

    <table style="width:100%;padding-top:5px;padding-bottom:3px;padding-left:-5px;">
      <tr>
        <td border="1.5">
          <table>
            <tr>
              <td align="left">www.prestamosdelnorte.com</td>
              <td align="right">contacto@prestamosdelnortechiclayo.com<span> </span></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
';

// set core font
$pdf->SetFont('dejavusans', '', 9);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);
$pdf->Ln();

$pdf->AddPage();
$pdf->SetFont('dejavusans', '', 7);
$pdf->writeHTML($html2, true, 0, true, true);

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo = $codigo.$title.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>