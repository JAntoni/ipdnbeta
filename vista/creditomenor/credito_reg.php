<?php
session_start();
if($_SESSION["autentificado"]!= "SI"){ header("location: ../../index.php"); exit();}

require_once ("../../config/Cado.php");
require_once ("../creditomenor/cCredito.php");
$oCredito = new cCredito();
require_once ("../cuota/cCuota.php");
$oCuota = new cCuota();
require_once ("../garantia/cGarantia.php");
$oGarantia = new cGarantia();
require_once ("../egreso/cEgreso.php");
$oEgreso = new cEgreso();
require_once("../cuotapago/cCuotapago.php");
$oCuotapago = new cCuotapago();
require_once("../lineatiempo/cLineaTiempo.php");
$oLineaTiempo = new cLineaTiempo();

require_once ("../formatos/formato.php");
require_once ("../formatos/fechas.php");

if($_POST['action_credito']=="ver")
{
	$data['cre_msj']='Sólo visualización de datos.';
	echo json_encode($data);
}

if($_POST['action_credito']=="insertar")
{
	if(!empty($_POST['txt_cre_linapr']))
	{
		$xac=1;
		$est=1;
		$mon_id=$_POST['cmb_mon_id'];

		$fecfac=fecha_mysql($_POST['txt_cre_fecdes']);
		$preaco = moneda_mysql($_POST['txt_cre_preaco']);
		$pre_gar = 0;

		//GARANTIAS
		if(isset($_SESSION['garantia_pro']))
			$num_rows=count($_SESSION['garantia_pro']);
		else{
			if($pre_gar != $preaco){
				$data['cre_id'] = 0;
				$data['cre_msj'] = 'No puede guardar un crédito sin garantías';
				echo json_encode($data);
				exit();
			}
		}

		if($num_rows > 0){
			foreach($_SESSION['garantia_pro'] as $indice=>$pro){
				$pre_gar += moneda_mysql($_SESSION['garantia_val'][$indice]);
			}

			////echo 'pre acorado: '.$preaco.' // suma total: '.$pre_gar; exit();
				//la suma de los valores de las garantías no puede ser menor ni mayor que el precio acordado
			if($pre_gar != $preaco){
				$data['cre_id'] = 0;
				$data['cre_msj'] = 'La suma de los valores de las garantías no es igual al precio acordado. Monto garantías: '.$pre_gar.', precio acordado: '.$preaco;
				echo json_encode($data);
				exit();
			}
		}

		$oCredito->insertar(
			$xac, 
			$_SESSION['usuario_id'], $_SESSION['usuario_id'], 
			$_POST['cmb_cuotip_id'], $_POST['hdd_cre_cli_id'], $mon_id, 
			$_POST['txt_cre_tipcam'],
			$_POST['cmb_rep_id'], $_POST['cmb_cuedep_id'],
			moneda_mysql($_POST['txt_cre_preaco']), moneda_mysql($_POST['txt_cre_int']), $_POST['txt_cre_numcuo'], moneda_mysql($_POST['txt_cre_linapr']), 
			$_POST['txt_cre_numcuomax'], fecha_mysql($_POST['txt_cre_feccre']), fecha_mysql($_POST['txt_cre_fecdes']), $fecfac, 
			$_POST['txt_cre_pla'], $_POST['txt_cre_comdes'], $_POST['txt_cre_obs'], $_POST['txt_cre_webref'], 
			$est
		);
		
		$dts = $oCredito->ultimoInsert();
			$dt = mysql_fetch_array($dts);
			$cre_id = $dt['last_insert_id()'];
		mysql_free_result($dts);

		$his = 'Crédito guardado con '.$num_rows.' garantías, registrado por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b><br>';
		$oCredito->registro_historial($cre_id, $his);

		$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha registrado un nuevo Crédito Menor de número: CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'. El Crédito se guardó con '.$num_rows.' garantías. && <b>'.date('d-m-Y h:i a').'</b>';
		$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);

		//CUOTAS
		$C = moneda_mysql($_POST['txt_cre_preaco']);
		$i = moneda_mysql($_POST['txt_cre_int']);
		$n = $_POST['txt_cre_numcuo'];

		$uno = $i / 100;
		$dos = $uno + 1;
		$tres = pow($dos,$n);
		$cuatroA = ($C * $uno) * $tres;
		$cuatroB = $tres - 1;
		$R = $cuatroA / $cuatroB;
		$r_sum = $R*$n;

		$fecha_desembolso = $_POST['txt_cre_fecdes'];
		list($day, $month, $year) = split('-', $fecha_desembolso);

		for ($j=1; $j <= $n; $j++)
		{ 
			if($j>1)
			{
				$C = $C - $amo;
				$int = $C*($i/100);
				$amo = $R - $int;
				if($_POST['cmb_cuotip_id'] == 1)
					$amo = 0;
			}
			else
			{
				$int = $C*($i/100);
				$amo = $R - $int;
				if($_POST['cmb_cuotip_id'] == 1)
					$amo = 0;
			}	

			//fecha facturacion
			$month = $month + 1;
			if($month == '13'){
				$month = 1;
				$year = $year + 1;
			}
			$fecha=validar_fecha_facturacion($day,$month,$year);

			$xac=1;
			$cretip_id=1;//credito tipo
			$est=1;

			$oCuota->insertar(
				$xac,
				$cre_id,
				$cretip_id,
				$mon_id,
				$j,
				fecha_mysql($fecha), 
				$C, 
				$amo, 
				$int, 
				$R, 
				$pro,
				$persubcuo, 
				$est
			);

		}

		//GARANTIAS
		if($num_rows>0){

			foreach($_SESSION['garantia_pro'] as $indice=>$pro)
			{
				$producto = $_SESSION['garantia_pro'][$indice];
				$producto = str_replace('"', '', $producto);
				$producto = str_replace("'", '', $producto);
				
				$xac=1;
				$oGarantia->insertar(
					$xac,
					$cre_id,
					$_SESSION['garantia_gartip_id'][$indice],
					$_SESSION['garantia_can'][$indice],
					$producto,
					moneda_mysql($_SESSION['garantia_val'][$indice]),
					moneda_mysql($_SESSION['garantia_valtas'][$indice]),
					$_SESSION['garantia_ser'][$indice],
					$_SESSION['garantia_kil'][$indice],
					$_SESSION['garantia_pes'][$indice],
					$_SESSION['garantia_tas'][$indice],
					$_SESSION['garantia_det'][$indice],
					$_SESSION['garantia_web'][$indice]
				);

				$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha registrado la garantía: <b>'.$_SESSION['garantia_pro'][$indice].'</b> para el Crédito Menor de número: CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'. && <b>'.date('d-m-Y h:i a').'</b>';
				$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);

			}
		}
		if(isset($_SESSION['garantia_pro']))
		{
			unset($_SESSION['garantia_gartip_id']);
			unset($_SESSION['garantia_can']);
			unset($_SESSION['garantia_pro']);
			unset($_SESSION['garantia_val']);
			unset($_SESSION['garantia_ser']);
			unset($_SESSION['garantia_kil']);
			unset($_SESSION['garantia_pes']);
			unset($_SESSION['garantia_tas']);
			unset($_SESSION['garantia_det']);
			unset($_SESSION['garantia_web']);
		}
		
		
		$data['cre_id']=$cre_id;
		//$data['mon_id']=$_POST['cmb_mon_id'];
		$data['cre_msj']='Se registró correctamente.';
	}
	else
	{
		$data['cre_id'] = 0;
		$data['cre_msj'] = 'Intentelo nuevamente.';
	}

	echo json_encode($data);
}

if($_POST['action_credito']=="editar")
{
	if(!empty($_POST['hdd_cre_id']))
	{
		$oCredito->modificar(
			$_POST['hdd_cre_id'],
			$_SESSION['usuario_id'],
			$_POST['cmb_rep_id'],
			$_POST['cmb_cuedep_id'],
			$_POST['txt_cre_obs']
		);

		$his = '<span style="color: brown;">Datos del crédito editados por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
		$oCredito->registro_historial($_POST['hdd_cre_id'], $his);

		$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha editado los datos del Crédito Menor de número: CM-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'. && <b>'.date('d-m-Y h:i a').'</b>';
		$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);
		
		$data['cre_msj']='Se registró correctamente.';
	}
	else
	{
		$data['cre_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}

if($_POST['action']=="remate")
{
	if(!empty($_POST['cre_id']))
	{
		$oCredito->modificar_campo($_POST['cre_id'],'est','5');

		$his = '<span style="color: blue;">Las garantías del crédito pasados a remate por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
		$oCredito->registro_historial($_POST['cre_id'], $his);

		echo 'Se cambió a remate correctamente.';
	}
	else
	{
		echo 'Intentelo nuevamente.';
	}
}

if($_POST['action']=="eliminar")
{
	if(!empty($_POST['cre_id']))
	{
		//verificamos si el credito tiene desembolsos, ya que primero se debe eliminar el desembolso
    $mod_id = 51; //51 es el mod id para menor en egreso
		$est =1;
		$dts1 = $oEgreso->mostrar_por_modulo($mod_id,$_POST['cre_id'], 1, $est);
			$num_rows = mysql_num_rows($dts1);
			while($dt1 = mysql_fetch_array($dts1))
			{
				$total_desembolsado += floatval($dt1['tb_egreso_imp']);
			}
		mysql_free_result($dts1);

		if($total_desembolsado > 0){
      echo 'Este crédito tiene un desembolso generado, primero debe anular dicho egreso.';
      exit();
    }

		//verificamos si estete credito tiene pagos en sus cuotas detalle, parametro 1 ya que es MENOR
    $dts = $oCuotapago->mostrar_creditotipo_ingreso_cuota(1,$_POST['cre_id']);
      $num_rows = mysql_num_rows($dts);
    mysql_free_result($dts);
    
    if($num_rows > 0){
      echo 'Este crédito tiene pagos, primero elimine los pagos y luego el crédito.';
    }
    else{
			$oCredito->modificar_campo($_POST['cre_id'],'xac','0');

			$his = '<span style="color: red;">Crédito eliminado por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
			$oCredito->registro_historial($_POST['cre_id'], $his);

			$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha eliminado el Crédito Menor de número: CM-'.str_pad($_POST['cre_id'], 4, "0", STR_PAD_LEFT).'. && <b>'.date('d-m-Y h:i a').'</b>';
			$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);

			echo 'Se envió a la papelera correctamente.';
		}
	}
	else
	{
		echo 'Intentelo nuevamente.';
	}
}

if($_POST['action']=="aprobar")
{
	if(!empty($_POST['cre_id']))
	{
		/*$cst1 = $oCredito->verifica_credito_tabla($_POST['cre_id'],'tb_misa');
		$rst1= mysql_num_rows($cst1);
		if($rst1>0)$msj1=' - Misa';
		
		if($rst1>0)
		{
			echo "No se puede eliminar, afecta información de: ".$msj1.".";
		}
		else
		{*/
			$est=2;
			$oCredito->aprobar($_POST['cre_id'],$_SESSION['usuario_id'],$est);

			$his = '<span style="color: green;">Crédito aprobado por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
			$oCredito->registro_historial($_POST['cre_id'], $his);

			$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> aprobó el Crédito Menor de número: CM-'.str_pad($_POST['cre_id'], 4, "0", STR_PAD_LEFT).'. && <b>'.date('d-m-Y h:i a').'</b>';
			$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);

			echo 'Se aprobó correctamente.';
		//}
	}
	else
	{
		echo 'Intentelo nuevamente.';
	}
}

if($_POST['action_desembolso']=="insertar")
{
	if(!empty($_POST['hdd_cre_id']))
	{

		$codigo='CM-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT);

		$dts=$oCredito->mostrarUno($_POST['hdd_cre_id']);
		$dt = mysql_fetch_array($dts);
			$cli_id=$dt['tb_cliente_id'];
			$cli_doc = $dt['tb_cliente_doc'];
			$cli_nom=$dt['tb_cliente_nom'];
			$cli_ape=$dt['tb_cliente_ape'];
			$cli_dir=$dt['tb_cliente_dir'];
			$cli_ema=$dt['tb_cliente_ema'];
			$cli_fecnac=$dt['tb_cliente_fecnac'];
			$cli_tel=$dt['tb_cliente_tel'];
			$cli_cel=$dt['tb_cliente_cel'];
			$cli_telref=$dt['tb_cliente_telref'];

			$credito_preaco = moneda_mysql($dt['tb_credito_preaco']);
		mysql_free_result($dts);

		//evaluamos si es que ya hay desembolsos previos del crédito
		$total_desembolsado = 0;
		$mod_id = 51; //51 es el mod id para menor en egreso
		$est =1;
		$dts1 = $oEgreso->mostrar_por_modulo($mod_id, $_POST['hdd_cre_id'], $_POST['hdd_mon_id'],$est);
			$num_rows = mysql_num_rows($dts1);
			while($dt1 = mysql_fetch_array($dts1)){
				$total_desembolsado += moneda_mysql($dt1['tb_egreso_imp']);
			}
		mysql_free_result($dts1);

		$saldo_disponible = $credito_preaco - $total_desembolsado;
		$monto_desembolso = moneda_mysql($_POST['txt_credes_mon']);

		if($monto_desembolso > $saldo_disponible){
			$data['egr_id'] = 0;
			$data['cre_msj'] = 'No se puede desembolsar dicho monto ya que supera a lo disponible por desembolsar';

			echo json_encode($data); 
			exit();
		}

		//EGRESO
		$xac=1;
		$doc_id=9;
		$det="DESEMBOLSO: $codigo CLIENTE: $cli_nom $cli_ape ";
		$mod_id=51;//credito menor
		$numdoc=$mod_id.'-'.$_POST['hdd_cre_id'];
		$est=1;
		$cue_id=19; //creditos
		$subcue_id=60; // menor
		$pro_id=1;//proveedor
		$caj_id=1;//caja 

		$oEgreso->insertar(
			$_SESSION['usuario_id'],
			$_SESSION['usuario_id'],
			$xac,
			fecha_mysql($_POST['txt_credes_fec']),
			$doc_id,
			$numdoc,
			$det,
			moneda_mysql($_POST['txt_credes_mon']),
			$est,
			$cue_id,
			$subcue_id,
			$pro_id,
			$caj_id,
			$_POST['hdd_mon_id'],
			$mod_id,
			$_POST['hdd_cre_id'],
			$_SESSION['empresa_id']
		);

		$dts=$oEgreso->ultimoInsert();
		$dt = mysql_fetch_array($dts);
			$egr_id=$dt['last_insert_id()'];
		mysql_free_result($dts);

		$numdoc=$numdoc.'-'.$egr_id;

		$oEgreso->modificar_campo($egr_id,$_SESSION['usuario_id'],'numdoc',$numdoc);		

		$oCredito->modificar_campo($_POST['hdd_cre_id'],'comdes',$numdoc);

		//cambio de estado
		
		if(moneda_mysql($_POST['txt_credes_mon'])==moneda_mysql($_POST['hdd_credes_mon'])){
			$oCredito->modificar_campo($_POST['hdd_cre_id'],'est','3');
			$oCredito->modificar_campo($_POST['hdd_cre_id'],'fecdes',date('Y-m-d'));
		}
		
		$his = '<span>Se desembolsó un total de S/. '.formato_money($_POST['txt_credes_mon']).', desembolsado por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
		$oCredito->registro_historial($_POST['hdd_cre_id'], $his);

		$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> hizo un desembolso para el Crédito Menor de número: CM-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).', un total de S/. '.formato_money($_POST['txt_credes_mon']).' && <b>'.date('d-m-Y h:i a').'</b>';
		$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);

		$data['egr_id']=$egr_id;
		$data['cre_msj']='Se registró desembolso correctamente.';
	}
	else
	{
		$data['cre_msj']='Intentelo nuevamente.';
	}

	echo json_encode($data);
}

if($_POST['action']=="asesor"){
  $asesor_id = $_POST['asesor_id'];
  $cre_id = $_POST['cre_id'];

  $oCredito->modificar_campo($cre_id, 'usureg', $asesor_id);
  echo 1;
}

if($_POST['action']=="historial"){
  $dts = $oCredito->mostrarUno($_POST['cre_id']);
    $dt = mysql_fetch_array($dts);
    $his = $dt['tb_credito_his'];
  mysql_free_result($dts);

  echo $his;
}

if($_POST['action'] == "cuotas"){
  $cre_id = intval($_POST['cre_id']);
  $num_cuo = intval($_POST['num_cuo']);

  $dts = $oCredito->mostrarUno($cre_id);
    $dt = mysql_fetch_array($dts);
    $cre_numcuo = intval($dt['tb_credito_numcuo']);
  mysql_free_result($dts);

  $cre_numcuo += $num_cuo;
  
  $oCredito->modificar_campo($cre_id, 'numcuo', $cre_numcuo);
  $oCredito->modificar_campo($cre_id, 'numcuomax', $cre_numcuo);

  $data['estado'] = 1;
  echo json_encode($data);
}
?>