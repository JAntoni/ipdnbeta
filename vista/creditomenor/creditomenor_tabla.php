<?php
  require_once('../../core/usuario_sesion.php');
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 admin, 3 ventas

  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'creditomenor/Creditomenor.class.php');
    require_once(VISTA_URL.'filestorage/Filestorage.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../creditomenor/Creditomenor.class.php');
    require_once('../filestorage/Filestorage.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  $oCredito = new Creditomenor();
  $oFilestorage = new Filestorage();

  $filtro_fec1 = (isset($_POST['txt_filtro_fec1']))? fecha_mysql($_POST['txt_filtro_fec1']) : date('Y-01-01');
  $filtro_fec2 = (isset($_POST['txt_filtro_fec2']))? fecha_mysql($_POST['txt_filtro_fec2']) : date('Y-m-d');
  $empresa_id = intval($_POST['cbm_empresa_id']); 
  $usuario_id = intval($_POST['cmb_fil_usuario_id']);
  $cliente_id = 0;
  $fecha_hoy = date('Y-m-d');

?>
<table id="tbl_creditomenor" class="table table-hover">
  <thead>
      <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">FECHA</th>
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">TIPO</th>
      <th id="tabla_cabecera_fila">MONTO</th>
      <th id="tabla_cabecera_fila">INT(%)</th>
      <th id="tabla_cabecera_fila">CUOTAS</th>
      <th id="tabla_cabecera_fila">ASESOR</th>
      <th id="tabla_cabecera_fila">UBICACION</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila" width="11%"></th>
    </tr>
  </thead>
  <tbody>
    <?php
    list($hoy_anio, $hoy_mes, $hoy_dia) = explode('-', $fecha_hoy);
    list($fil_anio, $fil_mes, $fil_dia) = explode('-', $filtro_fec1);
    $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
    $valida_hoy = $hoy_anio.'-'.$hoy_mes;
    $valida_fil = $fil_anio.'-'.$fil_mes;

    if(($usuariogrupo_id != 2 && $usuariogrupo_id != 6) && $valida_hoy != $valida_fil){
      $result['estado'] = 0;
      $result['mensaje'] = 'No hay Créditos para Listar, solo puedes ver los créditos registrados en el mes actual USUARIO GRUPO ID: '.$usuariogrupo_id;
    }
    else
      $result = $oCredito->filtro_creditos($filtro_fec1, $filtro_fec2, $empresa_id, $usuario_id, $cliente_id);

    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value): 
        $total_egresos += floatval($value['tb_credito_preaco']);
        ?>
    
        <tr id="tabla_cabecera_fila">
          <td id="tabla_fila"><?php echo mostrar_fecha($value['tb_credito_feccre']);?></td>
          <td id="tabla_fila"><?php echo $value['tb_cliente_nom'];?></td>
          <td id="tabla_fila"><?php echo $value['tb_cuotatipo_nom'];?></td>
          <td id="tabla_fila"><?php echo $value['tb_moneda_nom'].' '.mostrar_moneda($value['tb_credito_preaco']); ?></td>
          <td id="tabla_fila"><?php echo mostrar_moneda($value['tb_credito_int']); ?></td>
          <td id="tabla_fila"><?php echo ($value['tb_cuotatipo_id'] == 2)? $value['tb_credito_numcuo']: $value['tb_credito_numcuo'].' Max'; ?></td>
          <td id="tabla_fila"><?php echo $value['tb_usuario_nom']; ?></td>
          <td id="tabla_fila">
            <?php 
              $result2 = $oCredito->mostrar_Imagen($value['tb_credito_id']);
                if($result2['estado'] == 1){
                  $mensaje_imagen = $result2['data']['Imagen'];
                  if($mensaje_imagen != ''){
                    $color = 'btn-danger';
                  }
                  else{
                    $color = 'bg-navy';
                  }
                }
              $result2 = NULL;
              
              $semaforo_contratos = '';
              $semaforo_acta = '';

              if($value['tb_credito_id'] > 5240 && intval($value['tb_credito_est']) == 3) {
                $result2 = $oFilestorage->mostrarUnoSubmodulo('creditomenor', 'cm_contrato', $value['tb_credito_id']);
                  if(intval($result2['estado']) == 0)
                    $semaforo_contratos = '<span class="badge bg-red">Llenar Checklist</span>';
                $result2 = NULL;
                $result2 = $oFilestorage->mostrarUnoSubmodulo('creditomenor', 'cm_anexo', $value['tb_credito_id']);
                  if(intval($result2['estado']) == 0)
                    $semaforo_contratos = '<span class="badge bg-red">Llenar Checklist</span>';
                $result2 = NULL;
              }

              if($value['tb_credito_id'] > 5240 && intval($value['tb_credito_est']) == 4) {
                $result2 = $oFilestorage->mostrarUnoSubmodulo('creditomenor', 'cm_actaentrega', $value['tb_credito_id']);
                  if(intval($result2['estado']) == 0)
                    $semaforo_acta = '<span class="badge bg-red">Subir A. Entrega</span>';
                $result2 = NULL;
              }
            ?>
            <button type="button" class="btn btn-xs <?php echo $color;?>" title="Editar" onclick="garantia_estado(<?php echo $value['tb_credito_id'];?>)">Ubicación <?php echo $mensaje_imagen;?></button>

            <?php echo $semaforo_contratos . $semaforo_acta;?>
              
            <br>
          </td>
          <td id="tabla_fila">
            <?php 
              if (intval($value['tb_credito_est']) == 1 && ( $usuariogrupo_id == 2 || $usuariogrupo_id == 6)) {
                echo '<button type="button" class="btn btn-warning btn-xs btn_creditomenor_opc " title="Editar" onclick="creditomenor_estado(' . $value['tb_credito_id'] . ', 2)">Aprobar</button>';
              }
              if (intval($value['tb_credito_est']) == 1 && $usuariogrupo_id != 2) {
                echo '<span class="badge bg-yellow">Pendiente</span>';
              }
              if(intval($value['tb_credito_est']) == 2) {
                echo '<button type="button" class="btn btn-info btn-xs" title="Editar" onclick="creditomenor_desembolso(\'I\', '.$value['tb_credito_id'].')">Desembolsar</button>';
              }
              
              if(intval($value['tb_credito_est']) == 3) {
                echo '<span class="badge bg-green">Vigente</span>';
              }
              if(intval($value['tb_credito_est']) == 4) {
                echo '<span class="badge bg-blue">Liquidado</span>';
              }
              if(intval($value['tb_credito_est']) == 5) {
                echo '<span class="badge bg-red">Remate</span>';
              }
              if(intval($value['tb_credito_est']) == 6) {
                echo '<span class="badge bg-purple">Vendido</span>';
              }
              if(intval($value['tb_credito_est']) == 7) {
                echo '<span class="badge bg-teal">Por Entregar</span>';
              }
            ?>              
          </td>
          <td id="tabla_fila"><?php echo $value['tb_credito_id']; ?></td>
          <td  id="tabla_fila" align="center">
            <?php if(intval($value['tb_credito_est']) > 2): ?>
              <a class="btn btn-success btn-xs" title="Pagos" onclick="creditomenor_pagos(<?php echo $value['tb_credito_id'];?>)"><i class="fa fa-money"></i></a>
            <?php endif;?>
            <?php if(intval($value['tb_credito_est']) == 1): ?>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="creditomenor_form(<?php echo "'M', ".$value['tb_credito_id'];?>)"><i class="fa fa-pencil"></i></a>
            <?php endif;?>
            <?php if(intval($value['tb_credito_est']) > 1): ?>
              <a class="btn btn-info btn-xs" title="Ver" onclick="creditomenor_form(<?php echo "'L', ".$value['tb_credito_id'];?>)"><i class="fa fa-eye"></i></a>
            <?php endif;?>

            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="creditomenor_form(<?php echo "'E', ".$value['tb_credito_id'];?>)"><i class="fa fa-trash"></i></a>
            <div class="btn-group">
              <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a class="btn" onclick="cambioasesor(<?php echo "'M', ".$value['tb_credito_id'];?>)"><b>Cambiar asesor</b></a></li>
                <li><a class="btn" onclick="creditolinea_timeline(<?php echo $value['tb_credito_id'];?>)"><b>Historial</b></a></li>
              </ul>
            </div>
          </td>
        </tr>
        <?php
      endforeach;
    }
    else
      echo '<h4 class="badge bg-red">'.$result['mensaje'].'</h4>';
    $result = NULL;
    ?>
  </tbody>
  <tfoot>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <td colspan="3" id="tabla_cabecera_fila">TOTAL DE CREDITOS </td>
      <td id="tabla_cabecera_fila"><?php echo "S/ " . mostrar_moneda($total_egresos); ?></td>
      <td colspan="7" id="tabla_cabecera_fila"></td>
    </tr>
  </tfoot>
</table>
