<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
require_once('../../static/tcpdf/tcpdf.php');

require_once('../../core/usuario_sesion.php');
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$creditomenor_id = intval($_GET['d1']);
$tipo_cuota = $_GET['tipo_cuota'];

$title = 'ACTA DE ENTREGA';
$codigo = 'C-MENOR-'.str_pad($creditomenor_id, 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {
  public function Header() {
    $image_file = K_PATH_IMAGES.'logo.jpg';
    $this->Image($image_file, 20, 10, 70, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
  }
  public function Footer() {
    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 40, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$result = $oCredito->mostrarUno($creditomenor_id);
  if($result['estado'] == 1){
    $reg = mostrar_fecha_hora($result['data']['tb_credito_reg']);
    $mod = mostrar_fecha_hora($result['data']['tb_credito_mod']);
    $apr = mostrar_fecha_hora($result['data']['tb_credito_apr']);
    $usureg = $result['data']['tb_credito_usureg'];
    $usuario_nom = $result['data']['tb_usuario_nom'];
    $cuotip_id = $result['data']['tb_cuotatipo_id'];
    $mon_id = $result['data']['tb_moneda_id'];
    $credito_tipcap = $result['data']['tb_credito_tipcap'];
    $empresa_id = $result['data']['tb_empresa_id'];
    $moneda = $result['data']['tb_moneda_nom'];
    //fechas
    $credito_feccre = mostrar_fecha($result['data']['tb_credito_feccre']);
    $credito_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
    $credito_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
    $credito_preaco = $result['data']['tb_credito_preaco'];
    $credito_int = $result['data']['tb_credito_int'];
    $credito_numcuo = $result['data']['tb_credito_numcuo'];
    $credito_linapr = $result['data']['tb_credito_linapr'];
    $credito_numcuomax = $result['data']['tb_credito_numcuomax'];
    //$transferencia_bien=($credito_linapr-$credito_preaco)*$credito_numcuomax+$credito_preaco;
    $transferencia_bien = $credito_linapr;
    //representante
    $rep_id = $result['data']['tb_representante_id'];
    $rep_nom = $result['data']['tb_representante_nom'];
    $rep_dni = $result['data']['tb_representante_dni'];
    //cuenta deposito
    $cuedep_id = $result['data']['tb_cuentadeposito_id'];
    $cuedep_num = $result['data']['tb_cuentadeposito_num'];
    $cuedep_ban = $result['data']['tb_cuentadeposito_ban'];

    $credito_pla = $result['data']['tb_credito_pla'];
    $credito_comdes = $result['data']['tb_credito_comdes'];
    $credito_obs = $result['data']['tb_credito_obs'];
    $credito_webref = $result['data']['tb_credito_webref'];
    $credito_est = $result['data']['tb_credito_est'];
    //cliente
    $cliente_id = $result['data']['tb_cliente_id'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_dir = $result['data']['tb_cliente_dir'];
    $cliente_ema = $result['data']['tb_cliente_ema'];
    $cliente_fecnac = $result['data']['tb_cliente_fecnac'];
    $cliente_tel = $result['data']['tb_cliente_tel'];
    $cliente_cel = $result['data']['tb_cliente_cel'];
    $cliente_telref = $result['data']['tb_cliente_telref'];
    $cliente_ubigeo = $result['data']['tb_ubigeo_cod'];

    $credito_monliq = $dt['tb_credito_monliq']; //monto de liquidacion guarda desde el 30/07/2019, si es 0 será _linapr
    $monto_liquidacion = $credito_monliq;
    if($credito_monliq <= 0)
      $monto_liquidacion = $credito_linapr;
  }
$result = NULL;

//ubigeo del cliente
if($cliente_ubigeo  >0){
  $result = $oUbigeo->mostrarUbigeo($cliente_ubigeo);
    if($result['estado'] == 1){
      $cliente_dep = $result['data']['Departamento'];
      $cliente_pro = $result['data']['Provincia'];
      $cliente_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

$nombre_cuotas = 'CUOTAS MENSUALES';
if($tipo_cuota == 'fijo')
  $nombre_cuotas = 'CUOTAS FIJAS';

$garantia_pro_text = '';
$garantia_ser_text = '';
$garantia_det_text = '';
$cont = 0;

$result = $oGarantia->listar_garantias($creditomenor_id);
  $cont = 0;
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $garantia_can = $value['tb_garantia_can'];
      $garantia_pro = $value['tb_garantia_pro'];
      $garantia_ser = $value['tb_garantia_ser'];
      $garantia_det = $value['tb_garantia_det'];
      //$garantia_val =$dt['tb_garantia_val'];
      if($cont == 0){
        $garantia_text = strtoupper($garantia_can . ' ' . $garantia_pro . ' ' . $garantia_det . ' / Serie N°: ' . $garantia_ser);
      }else{
        $garantia_text = $garantia_text . ', ' . strtoupper($garantia_can . ' ' . $garantia_pro . ' ' . $garantia_det . ' / Serie N°: ' . $garantia_ser);
      }

      $cont ++;
    }
  }
$result = NULL;

$codigo_cmenor ='CM-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
$credmen_hor = date('h:i a');
$fecha_hoy = date('d-m-Y');
list($day, $month, $year) = explode('-', $fecha_hoy);

$html= '
  <p align="center"><strong><u>ACTA DE ENTREGA</u></strong></p>
    <p>Acta de entrega: <b>'.$codigo_cmenor.'</b></p>
    <p>Generado por: '.$_SESSION['usuario_nombre'].'</p>
    <p>Asesor: <b>'.$usuario_nom.'</b></p>
    <p></p>
    <p></p>
    <p align="justify">Siendo las '.$credmen_hor.' del día '.$day.' de '.nombre_mes($month).' del '.$year.', se hace entrega a el(la) señor(a) <strong>'.$cliente_nom.'</strong>, identificado con <strong>DNI N°'.$cliente_doc.'</strong> de <strong>'.$garantia_text.'</strong> por haber cancelado la totalidad de su deuda, es decir, '.$moneda.' '.$monto_liquidacion.' ('.numtoletras($monto_liquidacion).').</p>

    <p align="justify">Se deja constancia que la(el) señor(a) <strong>'.$cliente_nom.'</strong>, recibe
        <strong>'.$garantia_text.'</strong> en óptimas condiciones y a su entera satisfacción.</p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>

    <table style="width:100%;" border="0">
      <tr>
        <td style="width:20%;"></td>
        <td style="width:60%;">
          <table>
            <tr>
              <td align="center"><strong>----------------------------------------------------------</strong>
              </td>
            </tr>
            <tr>
              <td align="center"><strong>'.$cliente_nom.'</strong></td>
            </tr>
            <tr>
              <td align="center"><strong>DNI N° '.$cliente_doc.'</strong></td>
            </tr>
          </table>
        </td>
        <td style="width:20%;"></td>
      </tr>
    </table>
';

// set core font
$pdf->SetFont('dejavusans', '', 10);

$html = str_replace(array("\r\n", "\n", "\r", "  "), "", $html);
// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo = $codigo.$title.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>