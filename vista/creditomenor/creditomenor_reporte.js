$(document).ready(function() {
  console.log('Dtewfe wyveyuw ew  333333')

  $('#reportepicker1, #reportepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

  $("#reportepicker1").on("change", function (e) {
    var startVal = $('#txt_reporte_fec1').val();
    $('#reportepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#reportepicker2").on("change", function (e) {
    var endVal = $('#txt_reporte_fec2').val();
    $('#reportepicker1').data('datepicker').setEndDate(endVal);
  });

});

function creditomenor_reporte_tabla(){
  var fec1 = $('#txt_reporte_fec1').val();
  var fec2 = $('#txt_reporte_fec2').val();

  if(!fec1 || !fec2){
    alerta_warning('AVISO', 'Debes ingresar fechas para el filtro de créditos');
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_reporte_tabla.php",
    async: true,
    dataType: "html",
    data: $('#form_reporte_cmenor').serialize(),
    beforeSend: function() {
      
    },
    success: function(data){
      $('.tabla_reporte').html(data);

      estilos_datatable_reporte()
    },
    complete: function(data){
      console.log('Tabla cargada')
    },
    error: function(data){
      console.log(data)
    }
  });
}
function estilos_datatable_reporte(){
  datatable_global = $('#tb_creditomenor_reporte').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    "bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    scrollY: 700,
    fixedColumns: {
      left: 2
    },
    //"scrollY": "80vh",
    //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
    dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
    buttons: [
      { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
      { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
      { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
      { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
    ],
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
    /*drawCallback: function () {
      var sum_desembolsado = $('#tbl_comportamiento').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
      var sum_capital_rest_sol = $('#tbl_comportamiento').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles

      $('#total').html(sum);
    }*/
  });
}
