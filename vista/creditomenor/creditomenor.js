var datatable_global;

function creditomenor_form(usuario_act, creditomenor_id, vista){
  var ancho = usuario_act == 'I' ? 95 : 95;
  $.ajax({
		type: "POST",
		url: VISTA_URL+"creditomenor/creditomenor_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      creditomenor_id: creditomenor_id,
      vista: vista
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_creditomenor_form').html(data);
      	$('#modal_registro_creditomenor').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_registro_creditomenor', ancho);
        modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'creditomenor';
      	var div = 'div_modal_creditomenor_form';
      	permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){

		},
		error: function(data){
      alerta_error('Error', data.responseText)
			console.log(data);
		}
	});
}
function creditomenor_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_tabla.php",
    async: true,
    dataType: "html",
    data: $('#form_creditomenor_filtro').serialize(),
    beforeSend: function() {
      $('#creditomenor_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_creditomenor_tabla').html(data);
      $('#creditomenor_mensaje_tbl').hide(300);

      $('.btn_creditomenor_opc').click(function(event) {
        $(this).prop('disabled', true);
      });

      estilos_datatable();
    },
    complete: function(data){
      console.log('Tabla cargada')
    },
    error: function(data){
      $('#creditomenor_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function creditomenor_estado(credito_id, credito_est){
  $.confirm({
    title: 'Revisión',
    content: 'url:' + VISTA_URL + `creditomenor/aprobar_credito.php?credito_id=${credito_id}`,
    type: 'blue',
    escapeKey: 'close',
    backgroundDismiss: true,
    columnClass: 'large',
    buttons: {
      Confirmar: {
        btnClass: 'btn-blue',
        action: function () {
          $.ajax({
            type: "POST",
            url: VISTA_URL+"creditomenor/creditomenor_controller.php",
            async: true,
            dataType: "json",
            data: ({
              action: 'estado',
              credito_id: credito_id,
              credito_est: credito_est
            }),
            beforeSend: function() {
              $('#creditomenor_mensaje_opc').html('<span class="badge bg-aqua">Ejecutando proceso...</span>');
              $('#creditomenor_mensaje_opc').show(300);
            },
            success: function(data){
              if(parseInt(data.estado) == 1){
                $('#creditomenor_mensaje_opc').html('<span class="badge bg-green">'+data.mensaje+'</span>');
                creditomenor_tabla();
              }
              else{
                $('#creditomenor_mensaje_opc').html('<span class="badge bg-red">ERRRO: '+data+'</span>');
              }
            },
            complete: function(data){
        
            },
            error: function(data){
              $('#creditomenor_mensaje_opc').html('<span class="badge bg-red">ERRRO: '+data.responseText+'</span>' );
            }
          });
        }
      },
      cancelar: function () {
      }
    }
  });
  /* $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'estado',
      credito_id: credito_id,
      credito_est: credito_est
    }),
    beforeSend: function() {
      $('#creditomenor_mensaje_opc').html('<span class="badge bg-aqua">Ejecutando proceso...</span>');
      $('#creditomenor_mensaje_opc').show(300);
    },
    success: function(data){
      if(parseInt(data.estado) == 1){
        $('#creditomenor_mensaje_opc').html('<span class="badge bg-green">'+data.mensaje+'</span>');
        creditomenor_tabla();
      }
      else{
        $('#creditomenor_mensaje_opc').html('<span class="badge bg-red">ERRRO: '+data+'</span>');
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#creditomenor_mensaje_opc').html('<span class="badge bg-red">ERRRO: '+data.responseText+'</span>' );
    }
  }); */
}

function creditomenor_desembolso(usuario_act, creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_desembolso.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      creditomenor_id: creditomenor_id,
      vista: 'creditomenor'
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditomenor_desembolso').html(data);
        $('#modal_registro_creditomenor_desembolso').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_height_auto('modal_registro_creditomenor_desembolso'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_creditomenor_desembolso', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'creditomenor';
        var div = 'div_modal_creditomenor_desembolso';
        permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function creditomenor_pagos(creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditomenor_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditomenor_pagos').html(data);
        $('#modal_creditomenor_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditomenor_pagos', 95);
        modal_height_auto('modal_creditomenor_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditomenor_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function cambioasesor(usuario_act, creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cambioasesor/cambioasesor_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act,
      credito_id: creditomenor_id,
      creditotipo_id: 1
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_cambio_asesor').html(data);
      $('#modal_cambio_asesor').modal('show');
      modal_hidden_bs_modal('modal_cambio_asesor', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}

function creditolinea_timeline(creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditolinea/creditolinea_timeline.php",
    async: true,
    dataType: "html",
    data: ({
      creditotipo_id: 1,
      credito_id: creditomenor_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_creditolinea_timeline').html(data);
      $('#modal_creditolinea_timeline').modal('show');
      modal_height_auto('modal_creditolinea_timeline'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_creditolinea_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}

function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
function garantia_estado(credito_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"garantia/garantia_estado.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: credito_id
    }),
    beforeSend: function() {
      $('#body_modal_general').html('');
      $('#general_title').text('Estado de los Productos');
      $('#modal_general').modal('show');
    },
    success: function(data){
      $('#body_modal_general').html(data);
      //funcion js para agregar un largo automatico al modal, al abrirlo
    },
    complete: function(data){

    },
    error: function(data){
      alerta_error('ERRROR', data.responseText)
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_creditomenor').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [1,2,5,6,7,8,10], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}



function usuario_select(empresa_id)
{
  var usuariogrupo_id = parseInt($('#hdd_usuariogrupo_id').val()) || 0;

  if(usuariogrupo_id == 2 || usuariogrupo_id == 6){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "usuario/usuario_select.php",
      async: false,
      dataType: "html",
      data: {
        usuario_columna: 'tb_usuario_ofi',
	      usuario_valor: 1, //para que solo muestre usuarios de oficina o que trabajaron en oficina
	      param_tip: 'INT',
        empresa_id: empresa_id
      },
      beforeSend: function () {
        $("#cmb_fil_usuario_id").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#cmb_fil_usuario_id").html(html);
        $("#cmb_fil_usuario_id").selectpicker('refresh');
      },
      complete: function (html) {},
    });
  }
}

function creditomenor_reporte(){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"creditomenor/creditomenor_reporte.php",
    async: true,
		dataType: "html",
		data: ({}),
		beforeSend: function() {
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');

      $('#div_creditomenor_reporte').html(data);
      $('#modal_creditomenor_reporte').modal('show');

      //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      modal_width_auto('modal_creditomenor_reporte', 95);
      modal_height_auto('modal_creditomenor_reporte'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_creditomenor_reporte', 'limpiar'); //funcion encontrada en public/js/generales.js

		},
		complete: function(data){

		},
		error: function(data){
      alerta_error('Error', data.responseText)
			console.log(data);
		}
	});
}

$(document).ready(function() {
    console.log('cambiosaaa c-menor 3333333')
    var empresa_id = $('#cbm_empresa_id').val();

//    var usuariogrupo = $('#usuariogrupo_id').val();
//
//    if (usuariogrupo == 2 || usuariogrupo == 6) {
        usuario_select(empresa_id);
//    }
    
    
  $('input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('.btn_creditomenor_opc').click(function(event) {
    $(this).prop('disabled', true);
  });

  $('#txt_filtro_cliente_nom').autocomplete({
    minLength: 3,
    source: function(request, response){
      $.getJSON(
        VISTA_URL+"cliente/cliente_autocomplete.php",
        {term: request.term, cliente_emp: 1}, //1 solo clientes de la empresa
        response
      );
    },
    select: function(event, ui){
      $('#hdd_filtro_cliente_id').val(ui.item.cliente_id);
    }
  });

  $('#txt_filtro_cliente_nom').keypress(function(e) {
    var text = $(this).val();
    if(text == '')
      $('#hdd_filtro_cliente_id').val('');
  });

  creditomenor_tabla();
});

//$("#cbm_empresa_id").change(function (){
//    creditomenor_tabla();
//});
$("#cmb_fil_usuario_id").change(function (){
    creditomenor_tabla();
});

$('#cbm_empresa_id').change(function () {
    var empresa_id = $(this).val();
    usuario_select(empresa_id);
    creditomenor_tabla();
});
