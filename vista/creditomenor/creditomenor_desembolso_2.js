
$(document).ready(function() {
  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    vMin: '0.00',
    vMax: $('#hdd_desembolsar_max').val()
  });
  $('#creditomenor_picker').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
  });
  console.log('897897 ')

  $('#form_creditomenor_desembolso').validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL+"egreso/egreso_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_creditomenor_desembolso").serialize(),
        beforeSend: function() {
          $('#creditomenor_mensaje').show(400);
          $('#btn_guardar_creditomenor').prop('disabled', true);
        },
        success: function(data){
          if(parseInt(data.estado) > 0){
            $('#creditomenor_mensaje').removeClass('callout-info').addClass('callout-success')
            $('#creditomenor_mensaje').html(data.mensaje);
            creditomenor_tabla();
            $('#modal_registro_creditomenor_desembolso').modal('hide');
          }
          else{
            $('#creditomenor_mensaje').removeClass('callout-info').addClass('callout-warning')
            $('#creditomenor_mensaje').html('Alerta: ' + data.mensaje);
            $('#btn_guardar_creditomenor').prop('disabled', false);
          }
        },
        complete: function(data){
          //console.log(data);
        },
        error: function(data){
          $('#creditomenor_mensaje').removeClass('callout-info').addClass('callout-danger')
          $('#creditomenor_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
        }
      });
    },
    rules: {
      txt_credito_fecdes: {
        required: true
      },
      txt_credito_mon: {
        required: true
      }
    },
    messages: {
      txt_credito_fecdes: {
        required: "Fecha de sembolso"
      },
      txt_credito_mon: {
        required: "Monto a desembolsar"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      // Add the `help-block` class to the error element
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
    }
  });
});
