function cliente_form(usuario_act, cliente_id) {
  var cliente_fil_cre_id = parseInt($("#hdd_cliente_id").val());
  if (usuario_act == "M") {
    if (!Number.isInteger(cliente_fil_cre_id) || cliente_fil_cre_id <= 0)
      return false;
    else cliente_id = cliente_fil_cre_id;
  }
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form_cmenor.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: cliente_id,
      vista: "credito",
      pcl_id: $("#hdd_pcl_id").val(),
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "cliente";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    },
  });
}
function monedacambio_form(usuario_act, monedacambio_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "monedacambio/monedacambio_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      monedacambio_id: monedacambio_id,
      vista: "credito",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_monedacambio_form").html(data);
        $("#modal_registro_monedacambio").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_monedacambio"); //funcion encontrada en public/js/generales.js

        modal_hidden_bs_modal("modal_registro_monedacambio", "limpiar"); //funcion encontrada en public/js/generales.js
        modal_height_auto("modal_registro_monedacambio"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "monedacambio";
        var div = "div_modal_monedacambio_form";
        permiso_solicitud(usuario_act, monedacambio_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}
function monedacambio_tipo_cambio() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "monedacambio/monedacambio_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "tipocambio",
      monedacambio_fec: $("#txt_credito_feccre").val(),
      moneda_id: $("#cmb_moneda_id").val(),
    },
    beforeSend: function () {
      $("#txt_credito_tipcam").val("Consultando...");
    },
    success: function (data) {
      if (parseInt(data.estado) == 1) {
        $("#txt_credito_tipcam").val(data.valor);
        $("#span_error").hide(300);
      } else {
        $("#txt_credito_tipcam").val("");
        $("#span_error").show(300);
        var link =
          ' <a href="javascript:void(0)" onclick="monedacambio_form(\'I\', 0)" style="color: black;">Registrar TC</a>';
        $("#span_error").html(data.mensaje + link);
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#span_error").show(300);
      $("#span_error").html("ERROR AL CONSULTAR: " + data.responseText);
    },
  });
}
function credito_calculo_linea(credito_numcuo) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "funciones/credito_calculo_linea.php",
    async: true,
    dataType: "json",
    data: {
      credito_preaco: $("#txt_credito_preaco").val(),
      credito_int: $("#txt_credito_int").val(),
      credito_numcuo: credito_numcuo,
    },
    beforeSend: function () {
      $("#txt_credito_linapr").val("Consultando...");
    },
    success: function (data) {
      if (parseInt(data.estado) == 1) {
        $("#txt_credito_linapr").val(data.credito_linapr);
        $("#span_error").hide(300);
      } else {
        $("#txt_credito_linapr").val("");
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#span_error").show(300);
      $("#span_error").html("ERROR AL CONSULTAR: " + data.responseText);
    },
  });
}

/* desde aqui modifica daniel odar 06-03-23, falta subir */
function garantia_form(usuario_act, garantia_id, credito_est) {
  if (credito_est == 5) {
    $.confirm({
      title: "Alerta del sistema",
      content: `Está modificando una garantía en remate. Solo podrá modificar el tipo de artículo.`,
      type: "orange",
      escapeKey: "close",
      backgroundDismiss: true,
      columnClass: "medium",
      buttons: {
        confirmar: function () {
          enviar_form(usuario_act + "_uno", garantia_id, credito_est);
        },
        cancelar: function () {
          return;
        },
      },
    });
  } else {
    enviar_form(usuario_act, garantia_id, credito_est);
  }
}

function enviar_form(usuario_act, garantia_id, credito_est) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "garantia/garantia_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      garantia_id: garantia_id,
      credito_id: $("#hdd_creditomenor_id").val(),
      vista: "creditomenor",
      credito_est: credito_est,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_garantia_form").html(data);
        $("#modal_registro_garantia").modal("show");

        //desabilitar elementos del form si fes L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_garantia"); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_garantia"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_garantia", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "garantia";
        var div = "div_modal_garantia_form";
        permiso_solicitud(usuario_act, garantia_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}
/* hasta aqui modificado*/

function garantia_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "garantia/garantia_tabla.php",
    async: true,
    dataType: "html",
    data: {
      creditomenor_id: $("#hdd_creditomenor_id").val(),
    },
    beforeSend: function () {
      $("#garantia_mensaje_tbl").show(300);
      $("#garantia_info").hide();
      $("#btn_creditomenor_sig2").prop("disabled", false);
    },
    success: function (data) {
      $("#div_garantia_tabla").html(data);
      $("#garantia_mensaje_tbl").hide(300);
    },
    complete: function (data) {},
    error: function (data) {
      $("#garantia_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE GARANTIA: " + data.responseText
      );
    },
  });
}
function creditomenor_cronograma(credito_numcuo) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "funciones/creditomenor_cronograma.php",
    async: true,
    dataType: "html",
    data: {
      credito_fecdes: $("#txt_credito_fecdes").val(),
      moneda_id: $("#cmb_moneda_id").val(),
      cuotatipo_id: $("#cmb_cuotatipo_id").val(), //2 cuota fija, 1 cuota libre
      credito_preaco: $("#txt_credito_preaco").val(),
      credito_int: $("#txt_credito_int").val(),
      credito_numcuo: credito_numcuo,
    },
    beforeSend: function () {
      $("#cronograma_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_creditomenor_cronograma").html(data);
      $("#cronograma_mensaje_tbl").hide(300);
    },
    complete: function (data) {},
    error: function (data) {
      $("#cronograma_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CRONOGRAMA: " + data.responseText
      );
    },
  });
}
function guardar_cuotas() {
  var garantia_tot = Number($("#hdd_garantia_tot").val()); //input impreso en garantia_tabla.php
  var credito_preaco = Number($("#txt_credito_preaco").autoNumeric("get"));

  if (garantia_tot != credito_preaco) {
    alert(
      "El monto de las garantías suman: " +
        garantia_tot +
        ", y no es igual al precio acordado de: " +
        credito_preaco
    );
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "cuota/cuota_controller.php",
    async: true,
    dataType: "json",
    data: $("#form_creditomenor").serialize(),
    beforeSend: function () {
      $("#cuota_mensaje").show(300);
      $("#btn_creditomenor_sig2").prop("disabled", true);
    },
    success: function (data) {
      if (parseInt(data.estado) > 0) {
        $("#cuota_mensaje")
          .removeClass("callout-info")
          .addClass("callout-success");
        $("#cuota_mensaje").html(data.mensaje);
        $("#btn_creditomenor_fin").prop("disabled", false);
        $(".btn_garantia_tabla").hide(300);
        creditomenor_formatos();
      } else {
        $("#cuota_mensaje")
          .removeClass("callout-info")
          .addClass("callout-warning");
        $("#cuota_mensaje").html("Alerta: " + data.mensaje);
        $("#btn_creditomenor_sig2").prop("disabled", false);
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#cuota_mensaje")
        .removeClass("callout-info")
        .addClass("callout-danger");
      $("#cuota_mensaje").html(
        "ERROR AL REGISTRAR LAS CUOTAS: " + data.responseText
      );
    },
  });
}
function finalizar_credito() {
  var motivo = $("#txt_encuesta_motivo").val().trim();
  var ocupacion = $("#txt_encuesta_ocu").val().trim();
  var descripcion = $("#txt_encuesta_des").val().trim();
  var motivoprestamo_id = $("#cmb_motivoprestamo_id").val();
  var tipodedicacion_id = $("#cmb_tipodedicacion_id").val();

  var cliente_id = $("#hdd_cliente_id").val();
  var credito_id = $("#hdd_creditomenor_id").val();
  var credito_preaco = $("#txt_credito_preaco").val();

  if (motivo == "" || ocupacion == "" || descripcion == "") {
    alerta_warning(
      "Importante",
      "Debes llenar la encuesta a los clientes para poder culminar el crédito"
    );

    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditomenor/creditomenor_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "encuesta",
      cliente_id: cliente_id,
      credito_id: credito_id,
      credito_preaco: credito_preaco,
      motivo: motivo,
      ocupacion: ocupacion,
      descripcion: descripcion,
      motivoprestamo_id: motivoprestamo_id,
      tipodedicacion_id: tipodedicacion_id,
    },
    beforeSend: function () {
      notificacion_info("Guardando encuesta...", 5000);
    },
    success: function (data) {
      if (parseInt(data.estado) > 0) {
        if ($("#creditomenor_vista").val() == "precredito") {
          precredito_contenido();
        } else {
          creditomenor_tabla();
          notificacion_success("Encuesta Guardada con éxito", 5000);
        }
        $("#modal_registro_creditomenor").modal("hide");
      } else {
        alerta_error(
          "Error",
          "Existe un error al guardar la encuesta, consulta con sistemas"
        );
        console.log(data);
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error(
        "Error",
        "Existe un error al guardar la encuesta, consulta con sistemas"
      );
      console.log(data);
    },
  });
}
function creditomenor_formatos() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditomenor/creditomenor_formatos.php",
    async: true,
    dataType: "html",
    data: {
      credito_id: $("#hdd_creditomenor_id").val(),
    },
    beforeSend: function () {
      $("#formatos_mensaje").show(300);
    },
    success: function (data) {
      $("#div_creditomenor_formatos").html(data);
      $("#formatos_mensaje").hide(300);
    },
    complete: function (data) {},
    error: function (data) {
      $("#formatos_mensaje").html(
        "ERROR AL CARGAR FORMATOS: " + data.responseText
      );
    },
  });
}
function creditomenor_cancelar() {
  var credito_id = parseInt($("#hdd_creditomenor_id").val());
  var action = $("#action").val();

  if (credito_id > 0 && action == "insertar") {
    if (
      confirm(
        "Al cancelar el registro, toda la información del crédito será eliminado. ¿Desea seguir de todos modos?"
      )
    ) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "creditomenor/creditomenor_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: "cancelar",
          credito_id: $("#hdd_creditomenor_id").val(),
          moneda_id: $("#hdd_moneda_id").val(),
        },
        beforeSend: function () {
          $("#btn_cancelar_creditomenor").prop("disabled", true);
          $("#cuota_mensaje").show(300);
          $("#cuota_mensaje")
            .removeClass("callout-success")
            .addClass("callout-info");
          $("#cuota_mensaje").html(
            '<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>'
          );
        },
        success: function (data) {
          console.log(data);
          if (parseInt(data.estado) > 0) {
            $("#cuota_mensaje")
              .removeClass("callout-info")
              .addClass("callout-success");
            $("#cuota_mensaje").html(data.mensaje);
            $("#modal_registro_creditomenor").modal("hide");
            precredito_contenido();
          } else {
            $("#btn_cancelar_creditomenor").prop("disabled", false);
            $("#cuota_mensaje")
              .removeClass("callout-info")
              .addClass("callout-warning");
            $("#cuota_mensaje").html(data.mensaje);
          }
        },
        complete: function (data) {},
        error: function (data) {
          $("#btn_cancelar_creditomenor").prop("disabled", false);
          $("#cuota_mensaje")
            .removeClass("callout-info")
            .addClass("callout-warning");
          $("#cuota_mensaje").html("ERROR AL CANCELAR: " + data.responseText);
        },
      });
    }
  } else {
    $("#modal_registro_creditomenor").modal("hide");
  }
}
function acta_entrega(credito_id, doc_entrega) {
  var fecha = new Date();
  $.confirm({
    icon: "fa fa-info",
    title: "Mensaje!",
    type: "blue",
    theme: "material", // 'material', 'bootstrap'
    typeAnimated: true,
    content:
      "Desea generar el acta de entrega para la fecha de hoy: " +
      fecha.getDate() +
      "/" +
      (fecha.getMonth() + 1) +
      "/" +
      fecha.getFullYear(),
    buttons: {
      aceptar: function () {
        $.ajax({
          type: "POST",
          url: VISTA_URL + "creditomenor/creditomenor_controller.php",
          async: true,
          dataType: "json",
          data: {
            action: "acta_entrega",
            credito_id: credito_id,
          },
          beforeSend: function () {},
          success: function (data) {
            var vista = $("#creditomenor_vista").val();
            if (parseInt(data.estado) == 1) {
              if (vista == "creditomenor") creditomenor_tabla();
              if (vista == "vencimiento") vencimiento_completa_tabla();
              window.open(
                VISTA_URL+"creditomenor/" +
                  doc_entrega,
                "_blank"
              );
            } else {
              alerta_warning("Importante", data.mensaje); //en generales.js
            }
          },
          complete: function (data) {},
          error: function (data) {
            alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
          },
        });
      },
      cancelar: function () {},
    },
  });
}
function llenar_datos_cliente(data) {
  console.log(data);
  $("#hdd_cliente_id").val(data.cliente_id);
  $("#txt_cliente_fil_cre_doc").val(data.cliente_doc);
  $("#txt_cliente_fil_cre_nom").val(data.cliente_nom);
  $("#txt_cliente_fil_cre_tel").val(data.cliente_tel);
  $("#txt_cliente_fil_cre_cel").val(data.cliente_cel);
  $("#txt_cliente_fil_cre_telref").val(data.cliente_telref);

  $(".small_tel").html("<b>" + data.cliente_protel + "</b>");
  $(".small_cel").html("<b>" + data.cliente_procel + "</b>");
  $(".small_telref").html("<b>" + data.cliente_protelref + "</b>");
}
function ultimo_score_cliente(cliente_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditomenor/creditomenor_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "ultimo_score",
      cliente_id: cliente_id,
    },
    beforeSend: function () {},
    success: function (data) {
      $('#td_ultimo_score').html(data.ultimo_score);
      $('#td_ultimo_semaforo').html(data.ultimo_semaforo);
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
    },
  });
}
function filestorage_form(modulo_subnom){
  var filestorage_des = '';
  if(modulo_subnom == 'cm_contrato')
    filestorage_des = 'Contrato de Crédito Menor';
  if(modulo_subnom == 'cm_anexo')
    filestorage_des = 'Anexo de Crédito Menor';
  if(modulo_subnom == 'cm_actaentrega')
    filestorage_des = 'Acta de entrega al cliente';

  $.ajax({
      type: "POST",
      url: VISTA_URL + "filestorage/filestorage_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: "I", //insertar
        public_carpeta: 'pdf',
        modulo_nom: 'creditomenor',
        modulo_id: $("#hdd_creditomenor_id").val(),
        modulo_subnom: modulo_subnom,
        filestorage_uniq: '',
        filestorage_des: filestorage_des
      }),
      beforeSend: function () {
        
      },
      success: function (data) {
        if (data != 'sin_datos') {
          $('#div_filestorage_form').html(data);
          $('#modal_registro_filestorage').modal('show');
          modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function (data) {
      },
      error: function (data) {
        
      }
  });
}
function filestorage_eliminar_submodulo(modulo_subnom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "filestorage/filestorage_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: "eliminar_submodulo", //insertar
      modulo_nom: 'creditomenor',
      modulo_id: modulo_id,
      modulo_subnom: modulo_subnom
    }),
    beforeSend: function () {
    },
    success: function (data) {
      if (parseInt(data.estado) == 1) {
        swal_success('Eliminación', data.mensaje, 3000)
        $('#btn_'+modulo_subnom).hide();
      }
      else
        alerta_error('Importante', data.mensaje);
    },
    complete: function (data) {
    },
    error: function (data) {
      
    }
  });
}

function listar_creditomenor_checklist(){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditomenor/creditomenor_checklist.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: 'creditomenor',
      modulo_id: $('#hdd_creditomenor_id').val(),
      usuariogrupo_id: $('#hdd_usuariogrupo_id').val()
    }),
    beforeSend: function () {
    },
    success: function (data) {
      console.log('listado de archivos');
      $('#creditomenor_checklist').html(data)
    },
    complete: function (data) {
    },
    error: function (data) {
      
    }
  });
}

$(document).ready(function () {
  console.log("cammmm 111212121");
  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck(
    {
      checkboxClass: "icheckbox_flat-green",
      radioClass: "iradio_flat-green",
    }
  );

  $("#cmb_motivoprestamo_id").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });

  //-----------formatos para campos ---------------//
  $(".numero").autoNumeric({
    aSep: "",
    aDec: ".",
    vMin: "0",
    vMax: "999999999",
  });

  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999999.00",
  });
  $("#creditomenor_picker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
  });
  $("#creditomenor_picker1").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    startDate: "-0d",
  });

  $("#creditomenor_picker1").on("change", function (e) {
    var startVal = $("#txt_credito_feccre").val();
    $("#creditomenor_picker2").data("datepicker").setStartDate(startVal);
  });
  $("#creditomenor_picker2").on("change", function (e) {
    var endVal = $("#txt_credito_fecdes").val();
    $("#creditomenor_picker1").data("datepicker").setEndDate(endVal);
  });

  $("#cmb_motivoprestamo_id").on("change", function () {
    var selectText = $("#cmb_motivoprestamo_id option:selected").text();
    $("#txt_encuesta_motivo").val(selectText);
  });

  $("#cmb_tipodedicacion_id").on("change", function () {
    var selectText = $("#cmb_tipodedicacion_id option:selected").text();
    $("#txt_encuesta_ocu").val(selectText);
  });
  //---------- FIN FORMATOS -------------------------------//

  //-----------FUNCIONES------------------------------//
  $("#txt_cliente_fil_cre_nom").autocomplete({
    minLength: 3,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        { term: request.term, cliente_emp: 1 }, //1 solo clientes de la empresa
        response
      );
    },
    select: function (event, ui) {
      $("#hdd_cliente_id").val(ui.item.cliente_id);
      $("#txt_cliente_fil_cre_doc").val(ui.item.cliente_doc);
      $("#txt_cliente_fil_cre_tel").val(ui.item.cliente_tel);
      $("#txt_cliente_fil_cre_cel").val(ui.item.cliente_cel);
      $("#txt_cliente_fil_cre_telref").val(ui.item.cliente_telref);
      ultimo_score_cliente(ui.item.cliente_id);
    },
  });
  $("#txt_cliente_fil_cre_doc").autocomplete({
    minLength: 3,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        { term: request.term, cliente_emp: 1 }, //1 solo clientes de la empresa
        response
      );
    },
    select: function (event, ui) {
      event.preventDefault();
      $("#hdd_cliente_id").val(ui.item.cliente_id);
      $("#txt_cliente_fil_cre_doc").val(ui.item.cliente_doc);
      $("#txt_cliente_fil_cre_nom").val(ui.item.value);
      $("#txt_cliente_fil_cre_tel").val(ui.item.cliente_tel);
      $("#txt_cliente_fil_cre_cel").val(ui.item.cliente_cel);
      $("#txt_cliente_fil_cre_telref").val(ui.item.cliente_telref);
      ultimo_score_cliente(ui.item.cliente_id);
    },
  });

  $("#cmb_moneda_id").change(function (event) {
    var moneda_id = parseInt($(this).val());
    if (moneda_id == 1 || moneda_id == 2) monedacambio_tipo_cambio();
    else $("#txt_credito_tipcam").val("");
  });

  $(
    "#txt_credito_preaco, #txt_credito_int, #txt_credito_numcuo, #cmb_cuotatipo_id"
  ).change(function (event) {
    var credito_preaco = Number($("#txt_credito_preaco").autoNumeric("get"));
    var credito_int = Number($("#txt_credito_int").autoNumeric("get"));
    var credito_numcuo = Number($("#txt_credito_numcuo").autoNumeric("get"));
    var cuotatipo_id = parseInt($("#cmb_cuotatipo_id").val());
    if (cuotatipo_id == 1)
      //cuota libre
      credito_numcuo = 1; //solo calcular el interés de un solo mes

    if (credito_preaco > 0 && credito_int > 0 && credito_numcuo > 0) {
      credito_calculo_linea(credito_numcuo);
      creditomenor_cronograma(credito_numcuo);
    } else $("#txt_credito_linapr").val("");
  });

  //-------------------- FIN FUNCIONES -----------------//
  var verificado = false,
    mostrar_mensaje = false;
  $("#form_creditomenor").validate({
    submitHandler: function () {
      var moneda_nom = ["", "S/.", "US$"];
      var precredito = JSON.parse($("#hdd_pcr_reg").val());
      if (!$.isEmptyObject(precredito) && !verificado) {
        var mensaje = "";
        var moneda_enviado = $("#cmb_moneda_id option:selected").val();
        var monto_enviado = $("#txt_credito_preaco").val();
        var interes = $("#txt_credito_int").val();
        if (moneda_enviado != precredito.tb_moneda_id) {
          mostrar_mensaje = true;
          mensaje = `- La moneda de solicitud precredito (${
            moneda_nom[precredito.tb_moneda_id]
          }) es diferente a la moneda del credito (${
            moneda_nom[moneda_enviado]
          })`;
        }
        if (quitar_comas_miles(monto_enviado) != precredito.tb_precred_monto) {
          if (!$.isEmptyObject(mensaje)) mensaje += "<br>";
          //
          mostrar_mensaje = true;
          mensaje += `- Los montos de solicitud precredito (${
            moneda_nom[precredito.tb_moneda_id]
          } ${precredito.tb_precred_monto}) y credito (${
            moneda_nom[moneda_enviado]
          } ${monto_enviado}) son diferentes`;
        }
        if (interes != precredito.tb_precred_int) {
          if (!$.isEmptyObject(mensaje)) mensaje += "<br>";
          //
          mostrar_mensaje = true;
          mensaje += `- El % de interes en la solicitud precredito (${precredito.tb_precred_int}) y credito (${interes}) son diferentes`;
        }
        if (mostrar_mensaje) {
          $.confirm({
            title: "¡Cuidado!",
            content: `<b>¿Desea registrar de todos modos este crédito?<br>${mensaje}</b>`,
            type: "orange",
            escapeKey: "close",
            backgroundDismiss: true,
            columnClass: "small",
            buttons: {
              Confirmar: {
                btnClass: "btn-orange",
                action: function () {
                  verificado = true;
                },
              },
              cancelar: function () {},
            },
          });
        }
      }
      if (!verificado && mostrar_mensaje) {
        return;
      }
      $.ajax({
        type: "POST",
        url: VISTA_URL + "creditomenor/creditomenor_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_creditomenor").serialize(),
        beforeSend: function () {
          $("#btn_creditomenor_minus").click();
          $("#creditomenor_mensaje").show(400);
          $("#btn_creditomenor_sig").prop("disabled", true);
        },
        success: function (data) {
          console.log(data);
          if (parseInt(data.estado) > 0) {
            $("#creditomenor_mensaje")
              .removeClass("callout-info")
              .addClass("callout-success");
            $("#creditomenor_mensaje").html(data.mensaje);
            $("#hdd_creditomenor_id").val(data.credito_id);
            $("#btn_garantia_nuevo").prop("disabled", false);
            //form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js
            if ($("#action").val() == "eliminar") {
              creditomenor_tabla();
              $("#modal_registro_creditomenor").modal("hide");
            } else if (
              $("#action").val() == "insertar" &&
              $("#creditomenor_vista").val() == "precredito"
            ) {
              precredito_contenido();
            }
          } else {
            $("#creditomenor_mensaje")
              .removeClass("callout-info")
              .addClass("callout-warning");
            $("#creditomenor_mensaje").html("Alerta: " + data.mensaje);
            $("#btn_creditomenor_sig").prop("disabled", false);
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          $("#creditomenor_mensaje")
            .removeClass("callout-info")
            .addClass("callout-danger");
          $("#creditomenor_mensaje").html("ALERTA DE ERROR: " + data.mensaje);
          console.log(data);
        },
      });
    },
    rules: {
      hdd_cliente_id: {
        required: true,
      },
      txt_cliente_fil_cre_doc: {
        required: true,
      },
      txt_cliente_fil_cre_nom: {
        required: true,
      },
      cmb_cuotatipo_id: {
        min: 1,
      },
      cmb_moneda_id: {
        min: 1,
      },
      txt_credito_tipcam: {
        required: true,
      },
      txt_credito_preaco: {
        required: true,
      },
      txt_credito_int: {
        required: true,
      },
      txt_credito_numcuo: {
        required: true,
        min: 1,
      },
      txt_credito_linapr: {
        required: true,
      },
      cmb_representante_id: {
        min: 1,
      },
      cmb_cuentadeposito_id: {
        min: 1,
      },
      txt_credito_obs: {
        required: true,
      },
      txt_credito_feccre: {
        required: true,
      },
      txt_credito_fecdes: {
        required: true,
      },
    },
    messages: {
      hdd_cliente_id: {
        required: "Busque un cliente o registre uno, falta CLIENTE ID",
      },
      txt_cliente_fil_cre_doc: {
        required: "Busque un cliente o registre uno",
      },
      txt_cliente_fil_cre_nom: {
        required: "Busque un cliente o registre uno",
      },
      cmb_cuotatipo_id: {
        min: "Seleccione un tipo de cuota",
      },
      cmb_moneda_id: {
        min: "Seleccione un tipo de moneda",
      },
      txt_credito_tipcam: {
        required: "El tipo de cambio no puede estar vacío",
      },
      txt_credito_preaco: {
        required: "Ingrese un precio acordado",
      },
      txt_credito_int: {
        required: "Ingrese un interés",
      },
      txt_credito_numcuo: {
        required: "Ingrese el número de cuotas",
        min: "Como mínimo debe ser 1",
      },
      txt_credito_linapr: {
        required: "La línea aprobada no puede estar vacío",
      },
      cmb_representante_id: {
        min: "Elija un representante",
      },
      cmb_cuentadeposito_id: {
        min: "Seleccione una cuota a depositar",
      },
      txt_credito_obs: {
        required: "Ingrese una observación",
      },
      txt_credito_feccre: {
        required: "Seleccione la fecha crédito",
      },
      txt_credito_fecdes: {
        required: "Seleccione la fecha de desembolso",
      },
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });

  if ($("#creditomenor_vista").val() == "precredito") {
    if (parseInt($("#cmb_moneda_id").val()) == 2) {
      $("#cmb_moneda_id").change();
    }
  }
});
