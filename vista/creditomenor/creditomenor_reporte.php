<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../creditomenor/Creditomenor.class.php');
$oCreditomenor = new Creditomenor();
require_once('../funciones/funciones.php');
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../encuesta/Encuesta.class.php');
$oEncuesta = new Encuesta();

$usuarioperfil_id = $_SESSION['usuarioperfil_id'];

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditomenor_reporte" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reporte Créditos Menores</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-info">
              <div class="panel-body">
                <form id="form_reporte_cmenor" class="form-inline" role="form">
                  <div class="form-group">
                    <div class='input-group date' id='reportepicker1'>
                      <input type='text' class="form-control input-sm" name="txt_reporte_fec1" id="txt_reporte_fec1" value="<?php echo $reporte_fec1; ?>"/>
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='reportepicker2'>
                      <input type='text' class="form-control input-sm" name="txt_reporte_fec2" id="txt_reporte_fec2" value="<?php echo $reporte_fec2; ?>"/>
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>

                  <button type="button" class="btn btn-info btn-sm" onclick="creditomenor_reporte_tabla()"><i class="fa fa-search"></i> Buscar</button>
                </form>
              </div>
            </div>
          </div>
          <br>
          <!-- SECCION 1-->
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Lista de Créditos</h3>
              </div>
              <div class="box-body no-padding tabla_reporte">
                
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/creditomenor/creditomenor_reporte.js?ver=12'; ?>"></script>