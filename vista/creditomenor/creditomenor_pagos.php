<?php
  $credito_id = $_POST['credito_id'];
  $hdd_form = $_POST['hdd_form'];
  $creditotipo_id = 1;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditomenor_pagos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Historial de Pagos</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="panel panel-info">
              <div class="panel-body">
                <button type="button" class="btn btn-info" onclick="cuotapago_pagos_tabla()">Historial</button>
                <button type="button" class="btn btn-info" onclick="morapago_pagos_tabla()">Moras</button>

                <input type="hidden" id="hdd_credito_id" value="<?php echo $credito_id;?>">
                <input type="hidden" id="hdd_creditotipo_id" value="<?php echo $creditotipo_id;?>">
                <input type="hidden" id="hdd_form" value="<?php echo $hdd_form;?>">
              </div>
            </div>
            <div class="callout callout-info" id="creditomenor_pagos_mensaje_tbl" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando el historial de Pagos</h4>
            </div>
            <div id="creditomenor_pagos_tabla">
              <!-- INCLUIMOS LA TABLA DE PAGOS, PUEDE SER CUOTAS O MORAS-->
              <?php //require_once('../cuotapago/cuotapago_pagos_tabla.php');?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div id="div_modal_cuotapago_detalle"></div>
<div id="div_modal_solicitar_anulacion"></div>
<script type="text/javascript" src="<?php echo 'vista/creditomenor/creditomenor_pagos.js?ver=437433';?>"></script>
