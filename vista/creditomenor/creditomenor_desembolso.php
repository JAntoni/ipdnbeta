<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../creditomenor/Creditomenor.class.php');
  $oCreditomenor = new Creditomenor();
  require_once('../egreso/Egreso.class.php');
  $oEgreso = new Egreso();
  require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();
  require_once('../funciones/funciones.php');

  $direc = 'creditomenor';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $creditomenor_id = $_POST['creditomenor_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Creditomenor Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Desembolso';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Creditomenor';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Creditomenor';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en creditomenor
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'creditomenor'; $modulo_id = $creditomenor_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }

  $credito_preaco = 0;
  $egreso_imp = 0;
  $credito_des = 0;
  $credito_fecdes = date('d-m-Y');
  $cliente_id = 0;
  if($creditomenor_id > 0){
    $result = $oCreditomenor->mostrarUno($creditomenor_id);
      $credito_preaco = formato_moneda($result['data']['tb_credito_preaco']);
      $moneda_id = intval($result['data']['tb_moneda_id']);
      $cliente_id = $result['data']['tb_cliente_id'];
    $result = NULL;

    $modulo_id = 51; // el id del modulo credito menor es 101
    $egreso_modide = $creditomenor_id; $egreso_xac = 1;
    $result = $oEgreso->mostrar_por_modulo($modulo_id, $egreso_modide, $moneda_id, $egreso_xac);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $egreso_imp += formato_moneda($value['tb_egreso_imp']);
        }
      }
    $result = NULL;
  }

  $credito_des = $credito_preaco - $egreso_imp;

  //CONSULTAMOS SI EL CLIENTE TIENE CREDITOS CON CUOTAS VENCIDAS, PARA CONDICIONAR EL PAGO PRIMERO DE CUOTA

  $result = $oCuota->cuotas_impagas_vencidas_cliente($cliente_id);
    if($result['estado'] == 1){
      $bandera = 4;
      $mensaje = '<ul class="nav nav-stacked">';
      foreach ($result['data'] as $key => $value) {
        $mensaje .= '<li>El cliente debe la cuota <b>'.$value['tb_cuota_fec'].'</b> del crédito <b>CM-'.$value['tb_credito_id'].'</b></li>';
      }
      $mensaje .= '</ul><h4>PARA PODER DESEMBOLSAR DEBES PAGAR LAS CUOTAS PENDIENTES O DESCONTARLE DEL PRÉSTAMO OTORGADO.</h4>';
    }
  $result = NULL;
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditomenor_desembolso" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_creditomenor_desembolso" method="post" class="">
          <input type="hidden" name="action" value="<?php echo $action.'_desembolso';?>">
          <input type="hidden" name="vista" value="<?php echo 'creditomenor';?>">
          <input type="hidden" id="hdd_credito_id" name="hdd_credito_id" value="<?php echo $creditomenor_id;?>">
          <input type="hidden" name="hdd_cuenta_id" value="43">
          
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-widget widget-user-2">  
                  <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                      <li>
                        <a href="javascript:void(0)">Monto total <span class="pull-right badge bg-blue"><?php echo 'S/. '.mostrar_moneda($credito_preaco);?></span></a>
                      </li>
                      <li>
                        <a href="javascript:void(0)">Monto Desembolsado <span class="pull-right badge bg-blue"><?php echo 'S/. '.mostrar_moneda($egreso_imp)?></span></a>
                      </li>
                      <li>
                        <a href="javascript:void(0)">Máximo a desembolsar <span class="pull-right badge bg-blue"><?php echo 'S/. '.mostrar_moneda($credito_des)?></span></a>
                        <input type="hidden" id="hdd_desembolsar_max" name="hdd_desembolsar_max" value="<?php echo $credito_des;?>">
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_credito_fecdes" class="control-label">Fecha</label>
                  <div class='input-group date' id='creditomenor_picker'>
                    <input type='text' class="form-control" name="txt_credito_fecdes" id="txt_credito_fecdes" value="<?php echo $credito_fecdes;?>"/>
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_moneda_id" class="control-label">Moneda</label>
                  <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id">
                    <?php require_once('../moneda/moneda_select.php');?>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_credito_mon" class="control-label">Monto</label>
                  <input type="text" name="txt_credito_mon" id="txt_credito_mon" class="form-control input-sm moneda" value="<?php echo mostrar_moneda($credito_des)?>">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="cmb_formadesembolso_id" class="control-label">Forma Desembolso</label>
                  <select class="form-control input-sm" name="cmb_formadesembolso_id" id="cmb_formadesembolso_id">
                    <option value="1">En Oficina</option>
                    <option value="2">Por Transferencia</option>
                  </select>
                </div>
              </div>
            </div>
            
            <div class="row desembolso_banco" style="display: none;">
              <hr class="hr">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_desembolso_numope" class="control-label">N* Operación</label>
                  <input type="text" name="txt_desembolso_numope" id="txt_desembolso_numope" class="form-control input-sm" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_desembolso_numcuenta" class="control-label">N° Cuenta</label>
                  <input type="text" name="txt_desembolso_numcuenta" id="txt_desembolso_numcuenta" class="form-control input-sm" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_desembolso_banco" class="control-label">Banco del Cliente</label>
                  <select class="form-control input-sm" name="txt_desembolso_banco" id="txt_desembolso_banco">
                    <?php echo genera_options_bancos_comunes() // genera options con bancos comunes funciones/funciones.php;?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_subcuenta_id" class="control-label">Origen del Capital</label>
                  <select class="form-control input-sm" name="cmb_subcuenta_id" id="cmb_subcuenta_id">
                    <?php
                      $cuenta_id = 43;
                      require_once('../subcuenta/subcuenta_select.php');
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_desembolso_obs" class="control-label">Observaciones Adicionales</label>
                  <input type="text" name="txt_desembolso_obs" id="txt_desembolso_obs" class="form-control input-sm" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <input type="hidden" name="hdd_empresa_id" id="hdd_empresa_id" value="<?php echo $_SESSION['empresa_id']; ?>">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="imprimir_egreso" name="imprimir_egreso" class="flat-green" value="1"><b>&nbsp; Imprimir egreso</b>
                    </label>
                </div>
              </div>
            </div>
            

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="creditomenor_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_creditomenor">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_creditomenor">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_creditomenor_desembolso">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/creditomenor/creditomenor_desembolso.js?ver=1234';?>"></script>
