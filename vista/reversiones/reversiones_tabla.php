<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('Reversiones.class.php');
  $oReversiones = new Reversiones();

  // require_once('../cuotapago/Cuotapago.class.php');
  // $oCuotapago = new Cuotapago();

  $result = $oReversiones->listar();
    $tr = '';
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $tr .='<tr>';
          $tr.='
            <td style="vertical-align: middle;" class="text-center">'.$value['tb_reversiones_id'].'</td>
            <td style="vertical-align: middle;">'.$value['tb_usuariogrupo_nom'].'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['fecha_periodo_inicial'].'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['fecha_periodo_final'].'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_1_1'].' %</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_2_2'].' %</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_3_3'].' %</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_1_3'].' %</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_3_9'].' %</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_9_18'].' %</td>
            <td style="vertical-align: middle;" class="text-center">'.$value['porcentaje_periodo_18_0'].' %</td>
            <td class="text-center">
              <a class="btn btn-default btn-xs" title="Editar" onclick="reversiones_form(\'M\','.$value['tb_reversiones_id'].')"><i class="fa fa-edit fa-fw"></i> Editar</a>
              <a class="btn btn-default btn-xs" title="Eliminar" onclick="reversiones_form(\'E\','.$value['tb_reversiones_id'].')"><i class="fa fa-trash fa-fw"></i> Eliminar</a>
              <a class="btn btn-default btn-xs" title="Informe" onclick="reversiones_form(\'L\','.$value['tb_reversiones_id'].')"><i class="fa fa-info-circle fa-fw"></i> Info</a>
            </td>
          ';
        $tr.='</tr>';
      }
    }
    else {
      $tr .='<tr><td colspan="12">'.$result['mensaje'].'</td></tr>';
    }
  $result = null;
?>
<table id="tbl_reversiones" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Cargo</th>
      <th class="text-center">Fecha de Periodo Inicial</th>
      <th class="text-center">Fecha de Periodo Final</th>
      <th class="text-center">1 - 1 <br>% Periodo</th>
      <th class="text-center">2 - 2 <br>% Periodo</th>
      <th class="text-center">3 - 3 <br>% Periodo</th>
      <th class="text-center">1 - 3 <br>% Periodo</th>
      <th class="text-center">3 - 9 <br>% Periodo</th>
      <th class="text-center">9 - 18 <br>% Periodo</th>
      <th class="text-center">18 a más <br>% Periodo</th>
      <th class="text-center">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>