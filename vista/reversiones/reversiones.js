function reversiones_form(usuario_act, reversiones_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reversiones/reversiones_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      reversiones_id: reversiones_id,
      modulo: 'reversiones'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      console.log(data);
      $('#modal_mensaje').modal('hide');
      // if(data != 'sin_datos'){
      	$('#div_modal_reversiones_form').html(data);
      	$('#modal_registro_reversiones').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_reversiones'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_reversiones', 'limpiar'); //funcion encontrada en public/js/generales.js
      // }
      // else{
      // 	//llamar al formulario de solicitar permiso
      // 	var modulo = 'reversiones';
      // 	var div = 'div_modal_reversiones_form';
      // 	permiso_solicitud(usuario_act, reversiones_id, modulo, div); //funcion ubicada en public/js/permiso.js
      // }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function listar(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"reversiones/reversiones_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#reversiones_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_reversiones_tabla').html(data);
      $('#reversiones_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#reversiones_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

$(document).ready(function() {
  console.log('reversiones');
});