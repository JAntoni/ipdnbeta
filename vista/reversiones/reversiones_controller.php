<?php
session_name("ipdnsac");
session_start();

require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../reversiones/Reversiones.class.php');
$oReversiones = new Reversiones();

$action = $_POST['action'];

$porcentaje_periodo_1_1  = 0;
$porcentaje_periodo_2_2  = 0;
$porcentaje_periodo_3_3  = 0;
$porcentaje_periodo_1_3  = 0;
$porcentaje_periodo_3_9  = 0;
$porcentaje_periodo_9_18 = 0;
$porcentaje_periodo_18_0 = 0;

if ($action == 'insertar') {
	$cargo_id 				 = $_POST['cmb_cargo_id'];
	$fecha_periodo_inicial	 = $_POST['txt_fecha_periodo_inicial'];
	$fecha_periodo_final 	 = $_POST['txt_fecha_periodo_final'];
	if ($cargo_id == 3) {
		$porcentaje_periodo_1_1  = $_POST['txt_porcentaje_periodo_1_1'];
		$porcentaje_periodo_2_2  = $_POST['txt_porcentaje_periodo_2_2'];
		$porcentaje_periodo_3_3  = $_POST['txt_porcentaje_periodo_3_3'];
	} else {
		$porcentaje_periodo_1_3  = $_POST['txt_porcentaje_periodo_1_3'];
		$porcentaje_periodo_3_9  = $_POST['txt_porcentaje_periodo_3_9'];
		$porcentaje_periodo_9_18 = $_POST['txt_porcentaje_periodo_9_18'];
		$porcentaje_periodo_18_0 = $_POST['txt_porcentaje_periodo_18_0'];
	}

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al guardar la Clase Reversiones.';

	if ($oReversiones->validarExisteRegistro($cargo_id) == true) {
		$data['estado'] = 0;
		$data['mensaje'] = 'Ya existen datos registrados con éste cargo, verifique.';
		$data['clase_id'] = 0;
	} else {
		$result = $oReversiones->insertar($cargo_id, $fecha_periodo_inicial, $fecha_periodo_final, $porcentaje_periodo_1_1, $porcentaje_periodo_2_2, $porcentaje_periodo_3_3, $porcentaje_periodo_1_3, $porcentaje_periodo_3_9, $porcentaje_periodo_9_18, $porcentaje_periodo_18_0);

		if ($result['estado']) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Reversión registrado correctamente.';
			$data['clase_id'] = $result['tb_reversiones_id'];
		}
	}
	echo json_encode($data);
} elseif ($action == 'modificar') {

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al modificar la Clase Reversiones.';
	
	$cargo_id 			   = $_POST['cmb_cargo_id'];
	$tb_reversiones_id     = intval($_POST['hdd_reversiones_id']);
	$fecha_periodo_inicial = $_POST['txt_fecha_periodo_inicial'];
	$fecha_periodo_final   = $_POST['txt_fecha_periodo_final'];

	if ($cargo_id == 3) {
		$porcentaje_periodo_1_1  = ($_POST['txt_porcentaje_periodo_1_1']*100);
		$porcentaje_periodo_2_2  = ($_POST['txt_porcentaje_periodo_2_2']*100);
		$porcentaje_periodo_3_3  = ($_POST['txt_porcentaje_periodo_3_3']*100);

		if( $porcentaje_periodo_1_1 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 1 - 1.';
		}
		elseif( $porcentaje_periodo_2_2 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 2 - 2.';
		}
		elseif( $porcentaje_periodo_3_3 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 3 - 3.';
		}
		else
		{
			if ($oReversiones->modificar_ventas($fecha_periodo_inicial, $fecha_periodo_final, $porcentaje_periodo_1_1,$porcentaje_periodo_2_2,$porcentaje_periodo_3_3,$tb_reversiones_id) == true ) {
				$data['estado'] = 1;
				$data['mensaje'] = 'Datos modificado correctamente.';
			}
		}
	} else {
		$porcentaje_periodo_1_3  = ($_POST['txt_porcentaje_periodo_1_3']*100);
		$porcentaje_periodo_3_9  = ($_POST['txt_porcentaje_periodo_3_9']*100);
		$porcentaje_periodo_9_18 = ($_POST['txt_porcentaje_periodo_9_18']*100);
		$porcentaje_periodo_18_0 = ($_POST['txt_porcentaje_periodo_18_0']*100);

		if( $porcentaje_periodo_1_3 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 1 - 3.';
		}
		elseif( $porcentaje_periodo_3_9 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 4 - 9.';
		}
		elseif( $porcentaje_periodo_9_18 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 10 - 18.';
		}
		elseif( $porcentaje_periodo_18_0 > 100 )
		{
			$data['estado'] = 0;
			$data['mensaje'] = 'El porcentaje ingresado es inválido, no puede superar el 100%. Verifique el valor ingresado en el periodo 19 a más.';
		}
		else
		{
			if ($oReversiones->modificar_otros($fecha_periodo_inicial, $fecha_periodo_final, $porcentaje_periodo_1_3,$porcentaje_periodo_3_9,$porcentaje_periodo_9_18,$porcentaje_periodo_18_0,$tb_reversiones_id) == true) {
				$data['estado'] = 1;
				$data['mensaje'] = 'Datos modificado correctamente.';
			}
		}
	}
	echo json_encode($data);
} elseif ($action == 'eliminar') {
	$tb_reversiones_id = intval($_POST['hdd_reversiones_id']);

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al eliminar la Clase Reversiones.';

	if ($oReversiones->eliminar($tb_reversiones_id)) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Reversión eliminado correctamente. ID: ' . $tb_reversiones_id;
	}

	echo json_encode($data);
} else {
	$data['estado'] = 0;
	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

	echo json_encode($data);
}
