<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Reversiones extends Conexion{

    function validarExisteRegistro($cargo_id){
      try {
        $sql = "SELECT tb_reversiones_id FROM tb_reversiones WHERE cargo = ? AND tb_reversiones_xac = 1";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(1, $cargo_id, PDO::PARAM_INT);
        $sentencia->execute();
        if ($sentencia->rowCount() > 0) {
          return true;
        }
        else{
          return false;
        }
      } catch (Exception $e) {
        throw $e;
      }
    }

    function insertar($cargo,$fechaInicial,$fechaFinal,$pp11,$pp22,$pp33,$pp13,$pp39,$pp918,$pp180){
      $this->dblink->beginTransaction();
      try {
        $sql = "select coalesce((select max(tb_reversiones_id)+1 from tb_reversiones),1) as newcod";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $nuevoCodigo = $resultado["newcod"];
        
        $sql = "INSERT INTO tb_reversiones(tb_reversiones_id, cargo, fecha_periodo_inicial, fecha_periodo_final, porcentaje_periodo_1_1, porcentaje_periodo_2_2, porcentaje_periodo_3_3, porcentaje_periodo_1_3, porcentaje_periodo_3_9, porcentaje_periodo_9_18, porcentaje_periodo_18_0, tb_reversiones_reg, tb_reversiones_xac) 
          VALUES (?,?,?,?,?,?,?,?,?,?,?,now(),1)";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(1, $nuevoCodigo, PDO::PARAM_INT);
        $sentencia->bindParam(2, $cargo, PDO::PARAM_INT);
        $sentencia->bindParam(3, $fechaInicial, PDO::PARAM_STR);
        $sentencia->bindParam(4, $fechaFinal, PDO::PARAM_STR);
        $sentencia->bindParam(5, $pp11, PDO::PARAM_INT);
        $sentencia->bindParam(6, $pp22, PDO::PARAM_INT);
        $sentencia->bindParam(7, $pp33, PDO::PARAM_INT);
        $sentencia->bindParam(8, $pp13, PDO::PARAM_INT);
        $sentencia->bindParam(9, $pp39, PDO::PARAM_INT);
        $sentencia->bindParam(10, $pp918, PDO::PARAM_INT);
        $sentencia->bindParam(11, $pp180, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tb_reversiones_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result;
        $data['tb_reversiones_id'] = $tb_reversiones_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function modificar_ventas($fechaInicial,$fechaFinal,$pp11,$pp22,$pp33,$codigoId){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_reversiones SET 
            fecha_periodo_inicial  = ?, 
            fecha_periodo_final    = ?, 
            porcentaje_periodo_1_1 = ?, 
            porcentaje_periodo_2_2 = ?, 
            porcentaje_periodo_3_3 = ?
          WHERE tb_reversiones_id = ?";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(1, $fechaInicial, PDO::PARAM_STR);
        $sentencia->bindParam(2, $fechaFinal, PDO::PARAM_STR);
        $sentencia->bindParam(3, $pp11, PDO::PARAM_INT);
        $sentencia->bindParam(4, $pp22, PDO::PARAM_INT);
        $sentencia->bindParam(5, $pp33, PDO::PARAM_INT);
        $sentencia->bindParam(6, $codigoId, PDO::PARAM_INT);
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function modificar_otros($fechaInicial,$fechaFinal,$pp13,$pp39,$pp918,$pp180,$codigoId){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_reversiones SET 
            fecha_periodo_inicial  = ?, 
            fecha_periodo_final    = ?, 
            porcentaje_periodo_1_3 = ?, 
            porcentaje_periodo_3_9 = ?, 
            porcentaje_periodo_9_18 = ?, 
            porcentaje_periodo_18_0 = ?
          WHERE tb_reversiones_id = ?";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(1, $fechaInicial, PDO::PARAM_STR);
        $sentencia->bindParam(2, $fechaFinal, PDO::PARAM_STR);
        $sentencia->bindParam(3, $pp13, PDO::PARAM_INT);
        $sentencia->bindParam(4, $pp39, PDO::PARAM_INT);
        $sentencia->bindParam(5, $pp918, PDO::PARAM_INT);
        $sentencia->bindParam(6, $pp180, PDO::PARAM_INT);
        $sentencia->bindParam(7, $codigoId, PDO::PARAM_INT);
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function eliminar($reversiones_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_reversiones SET tb_reversiones_xac = 0 WHERE tb_reversiones_id = ?";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(1, $reversiones_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUno($reversiones_id){
      try {
        $sql = "SELECT * FROM tb_reversiones WHERE tb_reversiones_id = :reversiones_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":reversiones_id", $reversiones_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar(){
      try {
        $sql = "SELECT r.*, u.tb_usuariogrupo_nom FROM tb_reversiones r INNER JOIN tb_usuariogrupo u ON (r.cargo = u.tb_usuariogrupo_id) WHERE tb_reversiones_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay datos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }

    function lista_reversiones_segmentado($fecha1,$fecha2,$isfilter,$incial,$final){ // $filterCuotas 
      try {
        // $filterCuotas = ''; if($isfilter>0) { $filterCuotas = " AND cuo.tb_cuota_num BETWEEN $incial AND $final "; } else { $filterCuotas = " AND cuo.tb_cuota_num > 0 "; }
        $sql = "SELECT 
          cli.tb_cliente_id,
          cli.tb_cliente_doc, 
          cli.tb_cliente_nom,
          cli.tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          cuo.tb_cuota_est,
          cuo.tb_cuota_fec,
          cre.tb_credito_preaco,
          cuo.tb_cuota_cap,
          cuo.tb_cuota_amo,
          cuo.tb_cuota_int, 
          cuo.tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real,
          cre.tb_credito_numcuo,
          cre.tb_credito_feccre,
          cuo.tb_creditotipo_id,
          cuo.tb_cuota_num,
          cuo.tb_cuota_id
        FROM tb_creditomenor cre 
        INNER JOIN tb_cuota cuo ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 1)
        INNER JOIN tb_cliente cli ON (cli.tb_cliente_id = cre.tb_cliente_id)
        WHERE cre.tb_credito_xac = 1 AND cuo.tb_cuota_xac = 1
          AND (tb_credito_est IN (3,5) OR (tb_credito_est IN (4,7) AND tb_cuota_est = 2)) 
          AND cuo.tb_cuota_fec BETWEEN :fec1 AND :fec2 
        UNION 
        SELECT 
          cli.tb_cliente_id,
          cli.tb_cliente_doc, 
          cli.tb_cliente_nom,
          cli.tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          cuo.tb_cuota_est,
          cuo.tb_cuota_fec,
          cre.tb_credito_preaco,
          cuo.tb_cuota_cap,
          cuo.tb_cuota_amo,
          cuo.tb_cuota_int, 
          cuo.tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real,
          cre.tb_credito_numcuo,
          cre.tb_credito_feccre,
          cuo.tb_creditotipo_id,
          cuo.tb_cuota_num,
          cuo.tb_cuota_id
        FROM tb_creditoasiveh cre 
        INNER JOIN tb_cuota cuo ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 2)
        INNER JOIN tb_cliente cli ON (cli.tb_cliente_id = cre.tb_cliente_id)
        WHERE cre.tb_credito_xac = 1 AND cuo.tb_cuota_xac = 1
          AND (tb_credito_est IN (3) OR (tb_credito_est IN (7,8) AND tb_cuota_est IN (2,3)))
          AND cuo.tb_cuota_fec BETWEEN :fec1 AND :fec2 
        UNION 
        SELECT 
          cli.tb_cliente_id,
          cli.tb_cliente_doc, 
          cli.tb_cliente_nom,
          cli.tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          cuo.tb_cuota_est,
          cuo.tb_cuota_fec,
          cre.tb_credito_preaco,
          cuo.tb_cuota_cap,
          cuo.tb_cuota_amo,
          cuo.tb_cuota_int, 
          cuo.tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real,
          cre.tb_credito_numcuo,
          cre.tb_credito_feccre,
          cuo.tb_creditotipo_id,
          cuo.tb_cuota_num,
          cuo.tb_cuota_id
        FROM tb_creditogarveh cre 
        INNER JOIN tb_cuota cuo ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 3)
        INNER JOIN tb_cliente cli ON (cli.tb_cliente_id = cre.tb_cliente_id)
        WHERE cre.tb_credito_xac = 1 AND cuo.tb_cuota_xac = 1
          AND (tb_credito_est IN (3) OR (tb_credito_est IN (7,8) AND tb_cuota_est IN (2,3)))
          AND cuo.tb_cuota_fec BETWEEN :fec1 AND :fec2 
        UNION 
        SELECT 
          cli.tb_cliente_id,
          cli.tb_cliente_doc, 
          cli.tb_cliente_nom,
          cli.tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          cuo.tb_cuota_est,
          cuo.tb_cuota_fec,
          cre.tb_credito_preaco,
          cuo.tb_cuota_cap,
          cuo.tb_cuota_amo,
          cuo.tb_cuota_int, 
          cuo.tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real,
          cre.tb_credito_numcuo,
          cre.tb_credito_feccre,
          cuo.tb_creditotipo_id,
          cuo.tb_cuota_num,
          cuo.tb_cuota_id
        FROM tb_creditohipo cre 
        INNER JOIN tb_cuota cuo ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 4)
        INNER JOIN tb_cliente cli ON (cli.tb_cliente_id = cre.tb_cliente_id)
        WHERE cre.tb_credito_xac = 1 AND cuo.tb_cuota_xac = 1
          AND (tb_credito_est IN (3) OR (tb_credito_est IN (7,8) AND tb_cuota_est IN (2,3)))
          AND cuo.tb_cuota_fec BETWEEN :fec1 AND :fec2 
          ";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay datos registrados";
          $retorno["data"] = "";
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
  }

?>
