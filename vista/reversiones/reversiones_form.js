
$(document).ready(function(){
	$(".fragment01").hide();
	$(".fragment02").hide();

	$("#cmb_cargo_id").change(function (event) {
		event.preventDefault();
		var cargo = $("#cmb_cargo_id").val();
		if($.trim(cargo) == '')
		{
			$(".fragment01").hide();
			$(".fragment02").hide();
		}
		else if(parseInt(cargo) != 3){
			$(".fragment01").hide();
			$(".fragment02").show();
		}
		else if(parseInt(cargo) == 3){
			$(".fragment01").show();
			$(".fragment02").hide();
		}
		else{
			$(".fragment01").hide();
			$(".fragment02").hide();
		}
	});

  	$('#form_reversiones').validate({
		submitHandler: function() {
		$('#cmb_cargo_id').prop('disabled', false);
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"reversiones/reversiones_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_reversiones").serialize(),
				beforeSend: function() {
					$('#reversiones_mensaje').show(400);
					$('#btn_guardar').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#reversiones_mensaje').removeClass('callout-info').addClass('callout-success')
		      			$('#reversiones_mensaje').html(data.mensaje);
                        $('#modal_registro_vehiculoclase').modal('hide');

                        var modulo = $("#hdd_modulo").val();
                        if(modulo = 'reversiones'){
                            listar();
							setTimeout(() => {
								$("#btn_modal_cerrar").click();
							}, 1500);
                        }
						else
						{
							// if(modulo != 'creditogarveh'){
							// 	setTimeout(function(){ 
							// 		vehiculoclase_tabla();
							// 		}, 1000
							// 	);
							// }
						}
					}
					else{
						$('#reversiones_mensaje').removeClass('callout-info').addClass('callout-warning')
						$('#reversiones_mensaje').html('Alerta: ' + data.mensaje);
						$('#btn_guardar_vehiculoclase').prop('disabled', false);
					}
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#reversiones_mensaje').removeClass('callout-info').addClass('callout-danger')
	      			$('#reversiones_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			cmb_cargo_id: {
				required: true,
			},
			txt_fecha_periodo_inicial: {
				required: true
			},
			txt_fecha_periodo_final: {
				required: true
			},
			txt_porcentaje_periodo_1_1: {
				required: true
			},
			txt_porcentaje_periodo_2_2: {
				required: true
			},
			txt_porcentaje_periodo_3_3: {
				required: true
			},
			txt_porcentaje_periodo_1_3: {
				required: true
			},
			txt_porcentaje_periodo_3_9: {
				required: true
			},
			txt_porcentaje_periodo_9_18: {
				required: true
			},
			txt_porcentaje_periodo_18_0: {
				required: true
			}
		},
		messages: {
			cmb_cargo_id: {
				required: "Debe seleccionar un cargo"
			},
			txt_fecha_periodo_inicial: {
				required: "Ingrese una fecha de periodo inicial",
				minlength: "La descripción debe tener el formato fecha"
			},
			txt_fecha_periodo_final: {
				required: "Ingrese una fecha de periodo final",
				minlength: "La descripción debe tener el formato fecha"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
