<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../reversiones/Reversiones.class.php');
  $oReversiones = new Reversiones();
  require_once('../funciones/funciones.php');

  $direc = 'reversiones';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = trim($_POST['action']); //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $reversiones_id = trim($_POST['reversiones_id']);
  $modulo         = trim($_POST['modulo']);

  $titulo = '';
  if($usuario_action === 'L')
    $titulo = 'Reversión Registrado';
  elseif($usuario_action === 'I')
    $titulo = 'Registrar Reversión';
  elseif($usuario_action === 'M')
    $titulo = 'Editar Reversión';
  elseif($usuario_action === 'E')
    $titulo = 'Eliminar Reversión';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  $bloqCargo               = 'disabled';
  $cargo_id                = 0;
  $fecha_periodo_inicial   = '';
  $fecha_periodo_final     = '';
  $porcentaje_periodo_1_1  = 0;
  $porcentaje_periodo_2_2  = 0;
  $porcentaje_periodo_3_3  = 0;
  $porcentaje_periodo_1_3  = 0;
  $porcentaje_periodo_3_9  = 0;
  $porcentaje_periodo_9_18 = 0;
  $porcentaje_periodo_18_0 = 0;

  $result = $oReversiones->mostrarUno($reversiones_id);
    if($result['estado'] != 1){
      $mensaje =  'No se ha encontrado ningún registro para la reversión seleccionada, inténtelo nuevamente.';
      // $bandera = 4;
    }
    else{
      $cargo_id                = $result['data']['cargo'];
      $fecha_periodo_inicial   = $result['data']['fecha_periodo_inicial'];
      $fecha_periodo_final     = $result['data']['fecha_periodo_final'];
      $porcentaje_periodo_1_1  = ($result['data']['porcentaje_periodo_1_1']/100);
      $porcentaje_periodo_2_2  = ($result['data']['porcentaje_periodo_2_2']/100);
      $porcentaje_periodo_3_3  = ($result['data']['porcentaje_periodo_3_3']/100);
      $porcentaje_periodo_1_3  = ($result['data']['porcentaje_periodo_1_3']/100);
      $porcentaje_periodo_3_9  = ($result['data']['porcentaje_periodo_3_9']/100);
      $porcentaje_periodo_9_18 = ($result['data']['porcentaje_periodo_9_18']/100);
      $porcentaje_periodo_18_0 = ($result['data']['porcentaje_periodo_18_0']/100);
    }
  $result = NULL;


  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en vehiculoclase
  // $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $reversiones_id);
  // $bandera = 0; $mensaje = '';

  // if($result['estado'] == 1){
  //   foreach ($result['data'] as $key => $value) {
  //     $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  //   }

  //   $result = NULL;
    
  //   $array_permisos = explode('-', $permisos);

  //   if(in_array($usuario_action, $array_permisos))
  //     $bandera = 1;

  //   //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  //   if($bandera == 0){
  //     $usuario_id = $_SESSION['usuario_id']; $modulo = 'reversiones'; $modulo_id = $reversiones_id; $tipo_permiso = $usuario_action; $estado = 1;

  //     $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
  //       if($result['estado'] == 1){
  //         $bandera = 1; // si tiene el permiso para la accion que desea hacer
  //       }
  //       else{
  //         $result = NULL;
  //         echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
  //         exit();
  //       }
  //     $result = NULL;
  //   }

  //   //si la accion es modificar, mostramos los datos del vehiculoclase por su ID
  //   if(intval($reversiones_id) > 0){
      
  //   }

  // }
  // else{
  //   $mensaje =  $result['mensaje'];
  //   $bandera = 4;
  //   $result = NULL;
  // }
?>
<!-- < ?php if($bandera == 1): ?> -->
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_reversiones" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_reversiones" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_reversiones_id" value="<?php echo $reversiones_id;?>">
          <input type="hidden" name="hdd_modulo" id="hdd_modulo" value="<?php echo $modulo;?>">
          <input type="hidden" name="hdd_cargo_id" id="hdd_cargo_id" value="<?php echo $cargo_id;?>">
          
          <div class="modal-body">
            
            <div class="row">
              <div class="col-lg-3">
                <div class="form-group">
                  <label for="cmb_cargo_id" class="control-label">Cargo:</label>
                  <select name="cmb_cargo_id" id="cmb_cargo_id" class="form-control"<?php if($action=='modificar') echo $bloqCargo;?>>
                    <option value="">Seleccione:</option>
                    <option value="3">Ventas</option>
                    <option value="5">Cobranzas</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="txt_fecha_periodo_inicial" class="control-label">Fecha inicial de periodo:</label>
                  <input type="date" name="txt_fecha_periodo_inicial" id="txt_fecha_periodo_inicial" class="form-control input-sm text-center" value="<?php echo $fecha_periodo_inicial;?>">
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="txt_fecha_periodo_final" class="control-label">Fecha final de periodo:</label>
                  <input type="date" name="txt_fecha_periodo_final" id="txt_fecha_periodo_final" class="form-control input-sm text-center" value="<?php echo $fecha_periodo_final;?>">
                </div>
              </div>
            </div>            
            <div class="row fragment01">
              <!-- < ?php if($cargo_id == 3):?> -->
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_1_1" class="control-label">1 - 1<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_1_1" id="txt_porcentaje_periodo_1_1" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_1_1;?>">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_2_2" class="control-label">2 - 2<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_2_2" id="txt_porcentaje_periodo_2_2" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_2_2;?>">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_3_3" class="control-label">3 - 3<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_3_3" id="txt_porcentaje_periodo_3_3" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_3_3;?>">
                  </div>
                </div>
              <!-- < ?php endif; ?> -->
            </div>
            <div class="row fragment02">
              <!-- < ?php if($cargo_id == 5 || $cargo_id != 3):?> -->
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_1_3" class="control-label">1 - 3<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_1_3" id="txt_porcentaje_periodo_1_3" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_1_3;?>">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_3_9" class="control-label">4 - 9<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_3_9" id="txt_porcentaje_periodo_3_9" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_3_9;?>">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_9_18" class="control-label">10 - 18<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_9_18" id="txt_porcentaje_periodo_9_18" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_9_18;?>">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label for="txt_porcentaje_periodo_18_0" class="control-label">19 a más<br>% Periodo:</label>
                    <input type="text" name="txt_porcentaje_periodo_18_0" id="txt_porcentaje_periodo_18_0" class="form-control input-sm text-center" value="<?php echo $porcentaje_periodo_18_0;?>">
                  </div>
                </div>
              <!-- < ?php endif; ?> -->
            </div>
            
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning fa-fw"></i> ¿Está seguro de que desea eliminar esta la reversión?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="reversiones_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" id="btn_modal_cerrar" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<!-- < ?php endif; ?>
< ?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_reversiones">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p>< ?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
< ?php endif; ?> -->

<script>
  var cargo = $("#hdd_cargo_id").val();
  if(parseInt(cargo) != 0)
    $("#cmb_cargo_id").val(parseInt(cargo));
  else
    $("#cmb_cargo_id").val('');
  setTimeout(() => {
    $("#cmb_cargo_id").change();
  }, 500);
</script>

<script type="text/javascript" src="<?php echo 'vista/reversiones/reversiones_form.js?ver=021224';?>"></script>