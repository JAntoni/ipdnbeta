<?php
    require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../ingreso/Ingreso.class.php');
  $oingreso = new ingreso();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'ingreso';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $ingreso_id = $_POST['ingreso_id'];
  $vista = $_POST['vista'];
  $ingreso_imp="0.00";
  $ingreso_fec=date('d-m-Y');
  $tipocuenta=1;//tipo de documento de ingresos

  if($vista == 'rendicioncuentas'){
    $cuenta_id = $_POST['cuenta_id'];
    $subcuenta_id = $_POST['subcuenta_id'];
    $ingreso_imp = $_POST['ingreso_imp'];
    $moneda_id = $_POST['moneda_egreso_id'];
    $ingreso_det = $_POST['egreso_numdoc'];
    $rendicioncuentas_id = $_POST['rendicioncuentas_id'];
  }
  

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Ingreso Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Ingreso';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Ingreso';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Ingreso';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en ingreso
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'ingreso'; $modulo_id = $ingreso_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    $caja_id = 1;

    //si la accion es modificar, mostramos los datos del ingreso por su ID
    if(intval($ingreso_id) > 0){
      $result = $oingreso->mostrarUno($ingreso_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el ingreso seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
            
            $ingreso_tip = $result['data']['tb_ingreso_tip'];
            $ingreso_fec   = mostrar_fecha($result['data']['tb_ingreso_fec']);
            $documento_id =$result['data']['tb_documento_id'];
            $ingreso_numdoc=$result['data']['tb_ingreso_numdoc'];
            $ingreso_det   =$result['data']['tb_ingreso_det'];
            $ingreso_imp   =$result['data']['tb_ingreso_imp'];
            $ingreso_est   =$result['data']['tb_ingreso_est'];
            $cuenta_id    =$result['data']['tb_cuenta_id'];
            $tipocuenta    =$result['data']['tb_cuenta_tip'];
            $subcuenta_id =$result['data']['tb_subcuenta_id'];
            $cliente_id =$result['data']['tb_cliente_id'];
            $cliente_nombre =$result['data']['tb_cliente_nom'];
            $cliente_doc =$result['data']['tb_cliente_doc'];
            $caja_id      =$result['data']['tb_caja_id'];
            $moneda_id    =$result['data']['tb_moneda_id'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_ingreso" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_ingreso" method="post">
          <input type="hidden" name="ingreso_vista" id="ingreso_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="hdd_rendicioncuentasid" id="hdd_rendicioncuentasid" value="<?php echo $rendicioncuentas_id;?>">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_ingreso_id" value="<?php echo $ingreso_id;?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          
          <div class="modal-body">
            
              <?php include 'ingreso_form_vista.php';?>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Ingreso?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="ingreso_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_ingreso">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_ingreso">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_ingreso">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/ingreso/ingreso_form.js?ver=145';?>"></script>
