<?php
if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}

$fec1 = fecha_mysql($_POST['txt_fil_ing_fec1']);
$fec2 = fecha_mysql($_POST['txt_fil_ing_fec2']);

$emp_id = intval($_POST['cmb_fil_empresa_id']);
//  $emp_id=$_SESSION['empresa_id'];
$caj_id = intval($_POST['cmb_fil_caj_id']);
$mon_id = intval($_POST['cmb_moneda_id']);
$cli_id = intval($_POST['hdd_fil_cli_id']);
$est = intval($_POST['cmb_fil_ing_est']);
$doc_id = intval($_POST['cmb_fil_doc_id']);
$numdoc = $_POST['txt_fil_ing_numdoc'];
$cue_id = intval($_POST['cmb_fil_cue_id']);
$subcue_id = intval($_POST['cmb_fil_subcue_id']);
$usuario_id = intval($_POST['cmb_fil_usuario_id']);
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
$fecha_hoy = date('Y-m-d');
$usuario_sesion_id = intval($_SESSION['usuario_id']);

if (empty($emp_id)) {
    $emp_id = intval($_SESSION['empresa_id']);
}

require_once('Ingreso.class.php'); 
$oIngreso = new Ingreso();
$tipo = "";
$estado = "";
$total_ingresos = 0;

if(($usuariogrupo_id != 2 && $usuariogrupo_id != 6) && $fec1 != $fecha_hoy){
    $result['estado'] = 0;
    $result['mensaje'] = 'No hay Ingresos | no puedes consultar ingresos de fechas anteriores, anterior: '.$fec1.', fecha hoy: '.$fecha_hoy.', USUARIO GRUPO ID: '.$usuariogrupo_id;
  }
else
    $result = $oIngreso->listar_ingresos($fec1, $fec2, $emp_id, $caj_id, $mon_id, $cli_id, $est, $doc_id, $numdoc, $cue_id, $subcue_id, $usuario_id);

$tr = '';
if ($result['estado'] == 1) {

    foreach ($result['data'] as $key => $value) {
        $total_ingresos += floatval($value['tb_ingreso_imp']);
        if ($value['tb_ingreso_est'] == 1)
            $estado = "CANCELADO";
        if ($value['tb_ingreso_est'] == 2)
            $estado = "EMITIDO";
        if ($value['tb_ingreso_est'] == 0 || $value['tb_ingreso_est'] > 2)
            $estado = "";

        $cliente_nom = $value['tb_cliente_nom'];
        $cliente_doc = $value['tb_cliente_doc'];

        if(!empty($value['tb_cliente_empruc'])){
            $cliente_nom 	= $value['tb_cliente_emprs'];
            $cliente_doc 	= $value['tb_cliente_empruc'];
        }

        $tr .= '<tr  style="font-family: cambria;font-size: 12px;border-color:#135896;">';
        $tr .= '
            <td id="tabla_fila">' . mostrar_fecha($value['tb_ingreso_fec']) . '</td>
            <td id="tabla_fila">' . $value['tb_ingreso_id'] . '</td>
            <td id="tabla_fila"><a style="color: blue;" title="Imprimir" href="javascript:void(0)" onClick="ingreso_imppos_datos3(' . $value['tb_ingreso_id'] . ','.$_SESSION['empresa_id'].')">' . $value['tb_documento_abr'] . ' ' . $value['tb_ingreso_numdoc'] . '</a></td>
            <td id="tabla_fila">' . $cliente_doc.' ' . $cliente_nom . '</td>
            <td id="tabla_fila">' . $value['tb_ingreso_det'] . '</td>
            <td id="tabla_fila">' . $value['tb_cuenta_des'] . '</td>
            <td id="tabla_fila">' . $value['tb_subcuenta_des'] . '</td>
            <td id="tabla_fila">' . $value['tb_ingreso_imp'] . '</td>
            <td id="tabla_fila">' . $value['tb_caja_nom'] . '</td>
            <td align="center" style="border: 1px solid #135896;font-size: 10px">' . $value['tb_usuario_nom'] . '<br>' . mostrar_fecha_hora($value['tb_ingreso_fecreg']) . '</td>
            <td id="tabla_fila">';
        if ($value['tb_modulo_id'] == 0 && $value['tb_ingreso_fec'] == $fecha_hoy) {
            $tr .= ' <!--<a class="btn btn-info btn-xs" title="Ver" onclick="ingreso_form(\'L\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-eye"></i></a>-->
            <a class="btn btn-warning btn-xs" title="Editar" onclick="ingreso_form(\'M\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-edit"></i></a>';
            //<a class="btn btn-danger btn-xs" title="Eliminar" onclick="ingreso_form(\'E\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-trash"></i></a>';
        }
        if (($usuario_sesion_id == 2 || $usuario_sesion_id == 11 || $usuario_sesion_id == 61) && $value['tb_ingreso_fec'] == $fecha_hoy) {
            $tr .= '
            <span class="badge bg-red">Solo Gerencia</span>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="ingreso_form(\'E\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-trash"></i></a>';
        }elseif(($usuario_sesion_id != 2 && $usuario_sesion_id != 11 && $usuario_sesion_id != 61) && $value['tb_ingreso_fec'] == $fecha_hoy){
            /* GERSON (14-01-25) */
            $tr .= '
            <span class="badge bg-red">Solic. Anular</span>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="solicitar_anular(' . $value['tb_ingreso_id'] . ', 0, 0)"><i class="fa fa-trash"></i></a>';
            /*  */
        }
            $tr.='<a class="btn btn-success btn-xs" title="Subir Imagen" onclick="upload_form(\'I\', '.$value['tb_ingreso_id'].')"><i class="fa fa-cloud-upload"></i></a>
            <a class="btn btn-facebook btn-xs" title="Ver Imagen" onclick="Abrir_imagen('.$value['tb_ingreso_id'].')"><i class="fa fa-picture-o"></i></a>';
       
        $tr .= '</td>';

        $tr .= '</tr>';
    }
//    <td id="tabla_fila"><a style="color: blue;" title="Imprimir" href="javascript:void(0)" onClick="ingreso_imppos_datos3(' . $value['tb_ingreso_id'] . ','.$_SESSION['empresa_id'].')">' . $value['tb_documento_abr'] . ' ' . $value['tb_ingreso_numdoc'] . '</a></td>
//    <td id="tabla_fila"><a style="color: blue;" title="Imprimir" href="javascript:void(0)" onClick="ingreso_imppos_datos(' . $value['tb_ingreso_id'] . ')">' . $value['tb_documento_abr'] . ' ' . $value['tb_ingreso_numdoc'] . '</a></td>
    $result = null;
} else {
    $tr = '<td colspan="4">' . $result['mensaje'] . '</td>';
    $result = null;
}
?>
<table id="tbl_ingresos" class="table table-hover">
    <thead>
        <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
            <th id="tabla_cabecera_fila">FECHA</th>
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">DOCUMENTO</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">DETALLE</th>
            <th id="tabla_cabecera_fila">CUENTA</th>
            <th id="tabla_cabecera_fila">SUBCUENTA</th>
            <th id="tabla_cabecera_fila">IMPORTE</th>
            <th id="tabla_cabecera_fila">CAJA</th>
            <th id="tabla_cabecera_fila">USUARIO</th>
            <th id="tabla_cabecera_fila"></th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
    <tfoot>
        <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
            <td colspan="6" id="tabla_cabecera_fila">TOTAL DE INGRESOS </td>
            <td id="tabla_cabecera_fila"><?php echo "S/ " . mostrar_moneda($total_ingresos); ?></td>
            <td colspan="4" id="tabla_cabecera_fila"></td>
        </tr>
    </tfoot>
</table>
