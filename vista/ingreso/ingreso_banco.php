<?php
require_once('../../core/usuario_sesion.php');

//require_once ("./Vencimiento.class.php");
//$oVencimiento = new Vencimiento();
//require_once ("../cuota/Cuota.class.php");
//$oCuota = new Cuota();
//require_once ("../cuota/Cuotadetalle.class.php");
//$oCuotadetalle = new Cuotadetalle();
//require_once ("../cuotapago/Cuotapago.class.php");
//$oCuotapago = new Cuotapago();
//require_once ("../acuerdopago/Acuerdopago.class.php");
//$oAcuerdopago = new Acuerdopago();

require_once("../funciones/fechas.php");
require_once("../funciones/funciones.php");

$credito = $_POST['credito'];
$cuota_id = $_POST['cuota_id'];
$url_mora = '';


?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenorbanco" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">INFORMACIÓN DE PAGO EN BANCO </h4>
        </div>
        <form id="for_cuopag">
            <input type="hidden" id="hdd_cuo_per" value="<?php echo $cuo_permiso;?>">
            <input type="hidden" id="hdd_vencida" value="<?php echo $vencida?>">
            <input type="hidden" id="hdd_cuotip_id" value="<?php echo $cuotip_id?>">
            <input type="hidden" id="hdd_usuariogrupo" value="<?php echo $_SESSION['usuariogrupo_id'];?>">
            <input type="hidden" name="hdd_cuenta_dep" id="hdd_cuenta_dep">

            <input type="hidden" name="hdd_pc_nombre" id="hdd_pc_nombre">
            <input type="hidden" name="hdd_pc_mac" id="hdd_pc_mac">   
            
            <div class="modal-body" style="font-family: cambria">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="text-align: right">Cliente:</label>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" id="txt_cli_nom" name="txt_cli_nom" class="form-control input-sm mayus" value="<?php echo $cliente_nombre;?>">
                                            <input type="hidden" id="hdd_cli_id" name="hdd_cli_id" class="form-control input-sm mayus" value="<?php echo $cliente_id;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-primary btn-sm" onclick="cliente_form('I',0)" title="Registrar Nuevo Cliente"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-sm-2 control-label">Moneda</label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm">
                                            <?php include '../moneda/moneda_select.php';?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label">Cuenta:</label>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <select name="cmb_cue_id" id="cmb_cue_id" class="form-control input-sm">
                                                <?php include '../cuenta/cuenta_select.php';?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label">Sub Cuenta:</label>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <select name="cmb_subcue_id" id="cmb_subcue_id" class="form-control input-sm">
                                                <?php include '../subcuenta/subcuenta_select.php';?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                
                <!--Segunda Parte-->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha Depósito:</label>
                                    <div class="col-md-8">
                                        <div class='input-group date' id='ingreso_fil_picker1'>
                                            <input type="text" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" class="form-control input-sm" readonly value="<?php echo date('d-m-Y');?>">
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Fecha Validación:</label>
                                    <div class="col-md-8">
                                        <div class='input-group date' id='ingreso_fil_picker2'>
                                            <input type="text" name="txt_cuopag_fec" id="txt_cuopag_fec" class="form-control input-sm" readonly value="<?php echo date('d-m-Y');?>">
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° Operación:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control input-sm" id="txt_cuopag_numope" name="txt_cuopag_numope">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Depósito:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Comisión Banco:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" maxlength="10" value="0.00">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Validar:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" maxlength="10" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">N° Cuenta:</label>
                                    <div class="col-md-8">
                                        <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                                            <?php require_once '../cuentadeposito/cuentadeposito_select.php';?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">En Banco:</label>
                                    <div class="col-md-8">
                                        <select name="cmb_banco_id" class="form-control input-sm">
                                            <option value="">--</option>
                                            <option value="1" selected="true">Banco BCP</option>
                                            <option value="2">Banco Interbak</option>
                                            <option value="3">Banco BBVA</option>
                                            <option value="4">Otros bancos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Monto Pagado:</label>
                                    <div class="col-md-8">
                                        <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda4 form-control input-sm" name="txt_cuopag_montot" type="text" id="txt_cuopag_montot" value="0" maxlength="10" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="text-align: right">Observación:</label>
                                    <div class="col-md-10">
                                        <textarea type="text" name="txt_cuopag_obs" rows="2" cols="50" class="form-control input-sm mayus"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                        
                        <div class="row">
                            <div id="msj_pagobanco" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <div class="f1-buttons">
                <button type="submit" class="btn btn-info" id="btn_guar_cuopag">Guardar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>   
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/ingreso/ingreso_banco.js';?>"></script>