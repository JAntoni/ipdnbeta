var datatable_global;

function ingreso_form(usuario_act, ingreso_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"ingreso/ingreso_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      ingreso_id: ingreso_id,
      vista: 'ingreso'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_ingreso_form').html(data);
      	$('#modal_registro_ingreso').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_ingreso'); //funcion encontrada en public/js/generales.js
        
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_registro_ingreso'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_ingreso', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'ingreso';
      	var div = 'div_modal_ingreso_form';
      	permiso_solicitud(usuario_act, ingreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function ingreso_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"ingreso/ingreso_tabla.php",
    async: true,
    dataType: "html",
    data: 
//            ({})
    $('#ingreso_filtro').serialize(),

    beforeSend: function() {
      $('#ingreso_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_ingreso_tabla').html(data);
      $('#ingreso_mensaje_tbl').hide(300);

      let registros = data.includes("No hay");
      console.log(registros);
      if (!registros) estilos_datatable();
      
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#ingreso_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_ingresos').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [1,2,3,4,5,6,7], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}



function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'ingreso', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
//                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function upload_galeria(ingreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'ingreso', //nombre de la tabla a relacionar
            modulo_id: ingreso_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

//funcion para abrir el modal visualizador de imagenes, de inmediato se ejecuta upload_galeria
//enviamos el id del modulo y el nombre: ingreso_id, modulo: ingreso
function Abrir_imagen(ingreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ingreso/ingreso_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_egreso_img').modal('show');
            modal_hidden_bs_modal('modal_egreso_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(ingreso_id);
        },
        complete: function (html) {
//            console.log(html);
        }
    });
}



$(document).ready(function() {
  console.log('Modulo ingressooo 1111')
    ingreso_tabla();
    Documento_select();
    Cuenta_select();
    var empresa_id=$('#cmb_fil_empresa_id').val();
    
    var usuariogrupo=$('#usuariogrupo_id').val();
//    console.log("usuario grupo = "+usuariogrupo);
    
    if(usuariogrupo==2||usuariogrupo==6){
        usuario_select(empresa_id);
    }
    
    $( "#txt_fil_cli" ).autocomplete({
          minLength: 1,
          source: function(request, response){
              $.getJSON(
                      VISTA_URL+"cliente/cliente_autocomplete.php",
                      {term: request.term}, //
                      response
              );
          },
          select: function(event, ui){
            console.log(ui);
              $('#hdd_fil_cli_id').val(ui.item.cliente_id);
              $('#txt_fil_cli').val(ui.item.cliente_nom);
              ingreso_tabla();
            event.preventDefault();
              $('#txt_fil_cli').focus();
          }
      });
  
  
  
   
    $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_fil_ing_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_fil_ing_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  
  
  
    
  
  
});



function Documento_select(){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"documento/documento_select.php",
            async: true,
            dataType: "html",
            data:({
                    documento_tipo: 6,
                    vista_documento: 'documento'
            }),
            beforeSend: function() {
                    $('#cmb_fil_doc_id').html('<option>Cargando...</option>');
            },
            success: function(data){
                    $('#cmb_fil_doc_id').html(data);
            },
            complete: function(data){
                    //console.log(data);
            },
            error: function(data){
                alert(data.responseText);
            }
    });
}


function Cuenta_select(){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"cuenta/cuenta_select.php",
            async: true,
            dataType: "html",
            data:({
                    tipocuenta: 1
            }),
            beforeSend: function() {
                    $('#cmb_fil_cue_id').html('<option>Cargando...</option>');
            },
            success: function(data){
                    $('#cmb_fil_cue_id').html(data);
            },
            complete: function(data){
                    //console.log(data);
            },
            error: function(data){
                alert(data.responseText);
            }
    });
}

function SubCuenta_select(cuenta_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"subcuenta/subcuenta_select.php",
		async: true,
		dataType: "html",
		data:({
			cuenta_id: cuenta_id
//			vista_subcategoria: 'producto'
		}),
		beforeSend: function() {
			$('#cmb_fil_subcue_id').html('<option>Cargando...</option>');
		},
		success: function(data){
			$('#cmb_fil_subcue_id').html(data);
		},
		complete: function(data){
			//console.log(data);
		},
		error: function(data){
    	alert(data.responseText);
		}
	});
}

$('#cmb_fil_cue_id').change(function(){
    SubCuenta_select($('#cmb_fil_cue_id').val());
    ingreso_tabla();
});

$('#cmb_fil_caj_id').change(function(){
    ingreso_tabla();
});
$('#cmb_fil_ing_est').change(function(){
    ingreso_tabla();
});
$('#cmb_fil_doc_id').change(function(){
    ingreso_tabla();
});
$('#cmb_fil_subcue_id').change(function(){
    ingreso_tabla();
});
$('#cmb_moneda_id').change(function(){
    ingreso_tabla();
});
$('#cmb_fil_empresa_id').change(function(){
    var empresa_id=$(this).val();
    usuario_select(empresa_id);
    ingreso_tabla();
});

function reestablecerValores(){
    
    let date = new Date()

    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()

    if(month < 10){
      $("#txt_fil_ing_fec1").val(day+"-0"+month+"-"+year);
    $("#txt_fil_ing_fec2").val(day+"-0"+month+"-"+year);
    }else{
      $("#txt_fil_ing_fec1").val(day+"-"+month+"-"+year);
    $("#txt_fil_ing_fec2").val(day+"-"+month+"-"+year);
    }
    
    $("#cmb_fil_cue_id").val(0);
    $("#cmb_fil_caj_id").val(0);
    $("#cmb_fil_ing_est").val("");
    $("#cmb_fil_doc_id").val(0);
    $("#cmb_fil_subcue_id").val(0);
    $("#cmb_moneda_id").val(0);
    $("#txt_fil_ing_numdoc").val("");
    $("#txt_fil_cli").val("");
    $("#hdd_fil_cli_id").val("");
    ingreso_tabla();
}

$("#txt_fil_ing_fec1,#txt_fil_ing_fec2,#cmb_fil_usuario_id").change(function(){
    ingreso_tabla();
});

function ingreso_imppos_datos3(id_ing,empresa_id)
{
    var codigo=id_ing;
    window.open("http://ipdnsac.com/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
//  window.open("http://localhost/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
}

function ingreso_imppos_datos(id_ing)
{
	if(confirm("Desea imprimir?"))
	{
		$.ajax({
			type: "POST",
			url:  VISTA_URL+"ingreso/ingreso_imppos_datos.php",
			async:true,
			dataType: "json",                      
			data: ({ ing_id: id_ing}),
			beforeSend: function() {
				//$('#msj_ingreso_imp').html("Imprimiendo.");
				//$('#msj_ingreso_imp').show(100);
			},
			success: function(data){
				// $.each(data, function(i, item) {
				//     console.log(item);
				// });
//				console.log(data);
				ingreso_imppos_ticket(data);
			},
                        complete: function (data) {
                            console.log(data);
            }
		});
	}
}

function ingreso_imppos_ticket(datos){	
    $.ajax({
            type: "POST",
            url: "http://127.0.0.1/prestamosdelnorte/app/modulos/ingreso/ingreso_imppos_ticket.php",
            async:true,
            dataType: "html",                      
            data: datos,
            beforeSend: function() {
                    //$('#msj_ingreso_imp').html("Imprimiendo...");
                    //$('#msj_ingreso_imp').show(100);
            },
            success: function(html){						
                    $('#msj_ingreso_imp').html(html);		
            },
            complete: function (data) {
                console.log(data);
            }
    });
}

function usuario_select(empresa_id){ 
    $.ajax({
      type: "POST",
      url: VISTA_URL + "usuario/usuario_select.php",
      async: false,
      dataType: "html",
      data: {
        usuario_columna: 'tb_usuario_ofi',
	      usuario_valor: 1, //para que solo muestre usuarios de oficina o que trabajaron en oficina
	      param_tip: 'INT',
        empresa_id: empresa_id
      },
      beforeSend: function () {
        $("#cmb_fil_usuario_id").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#cmb_fil_usuario_id").html(html);
        $("#cmb_fil_usuario_id").selectpicker('refresh');
      },
      complete: function (html) {},
    });
}



/*SI SE SELECCIONA PAGO EN ENTIDAD BANCARIA SE EJECUTA LA SIGUIENTE FUNCIÓN*/
//cuotapago_menor_form('pagar_libre','39187')
function cuotapago_banco(){
  $.ajax({
        type: "POST",
        url: VISTA_URL+"ingreso/ingreso_banco.php",
        async:true,
        dataType: "html",                    
        data: ({
            credito: 'cred',
            cuota_id: 0,
            vista: 'vencimiento_tabla'
        }),
        beforeSend: function() {

        },
        success: function(html){
//            $('#modal_mensaje').modal('hide');
            $('#div_modal_vencimiento_menor_banco_form').html(html);
            $('#modal_registro_vencimientomenorbanco').modal('show');
            modal_height_auto('modal_registro_vencimientomenorbanco');
            modal_hidden_bs_modal('modal_registro_vencimientomenorbanco', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function(data){
//                console.log(data);
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
        }
  });
}

/* GERSON (14-01-25) */
function solicitar_anular(ingreso_id, credito_id, creditotipo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"permisoanulacion/solicitar_anulacion_form.php",
    async: true,
    dataType: "html",
    data: ({
      modid: ingreso_id,
      origen: 3, // 1=cuota(cmenor), 2=cuotadetalle(cgarveh), 3=ingreso, 4=egreso
      credito_id: credito_id,
      creditotipo_id: creditotipo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Obteniendo datos...');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_solicitar_anulacion').html(data);
      $('#modal_solicitar_anulacion').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_solicitar_anulacion'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_solicitar_anulacion', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}
/*  */