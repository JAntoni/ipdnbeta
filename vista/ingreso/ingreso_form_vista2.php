<div class="row">
              <!-- COLUMNA IZQUIERDA-->
              <div class="col-md-12">
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="txt_ingreso_fec" class="control-label">Fecha</label>
                    <div class='input-group date' id='datetimepicker1'>
                      <input type='text' class="form-control" name="txt_ingreso_fec" id="txt_ingreso_fec" value="<?php echo $ingreso_fec;?>"/>
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="cmb_caja_id" class="control-label">Caja</label>
                    <select id="cmb_caja_id" name="cmb_caja_id" class="form-control input-sm">
                      <?php require_once('../caja/caja_select.php');?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="cmb_moneda_id" class="control-label">Moneda</label>
                    <select id="cmb_moneda_id" name="cmb_moneda_id" class="form-control input-sm">
                      <?php require_once('../moneda/moneda_select.php');?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="cmb_cuenta_id" class="control-label">Cuenta</label>
                    <select id="cmb_cuenta_id" name="cmb_cuenta_id" class="form-control input-sm">
                      <?php require_once('../cuenta/cuenta_select.php');?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="cmb_subcuenta_id" class="control-label">Sub Cuenta</label>
                    <select id="cmb_subcuenta_id" name="cmb_subcuenta_id" class="form-control input-sm">
                      <?php require_once('../cuenta/subcuenta_select.php');?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="txt_cliente_fil_doc" class="control-label">RUC/DNI</label>
                    <input type="text" name="txt_cliente_fil_doc" id="txt_cliente_fil_doc" class="form-control input-sm mayus" value="<?php echo $ingreso_pla;?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="txt_cliente_fil_nom" class="control-label">Cliente</label>
                    <input type="text" name="txt_cliente_fil_nom" id="txt_cliente_fil_nom" class="form-control input-sm mayus" value="<?php echo $ingreso_pla;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_ingreso_det" class="control-label">Detalle</label>
                  <textarea class="form-control" rows="3" id="txt_ingreso_det" name="txt_ingreso_det"></textarea>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="txt_ingreso_imp" class="control-label">Importe</label>
                    <input type="text" name="txt_ingreso_imp" id="txt_ingreso_imp" class="form-control input-sm mayus" value="<?php echo $ingreso_imp;?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="txt_ingreso_numdoc" class="control-label">Número Documento</label>
                    <input type="text" name="txt_ingreso_numdoc" id="txt_ingreso_numdoc" class="form-control input-sm mayus" value="<?php echo $ingreso_imp;?>">
                  </div>
                </div>
              </div>
            </div>