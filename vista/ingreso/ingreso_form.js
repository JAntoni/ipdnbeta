function Cuenta_select_form(){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"cuenta/cuenta_select.php",
            async: true,
            dataType: "html",
            data:({
                    tipocuenta: 1
            }),
            beforeSend: function() {
                    $('#cmb_cue_id').html('<option>Cargando...</option>');
            },
            success: function(data){
                    $('#cmb_cue_id').html(data);
            },
            complete: function(data){
                    //console.log(data);
            },
            error: function(data){
                alert(data.responseText);
            }
    });
}

function SubCuenta_select_form(cuenta_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"subcuenta/subcuenta_select.php",
		async: true,
		dataType: "html",
		data:({
			cuenta_id: cuenta_id
//			vista_subcategoria: 'producto'
		}),
		beforeSend: function() {
			$('#cmb_subcue_id').html('<option>Cargando...</option>');
		},
		success: function(data){
			$('#cmb_subcue_id').html(data);
		},
		complete: function(data){
			//console.log(data);
		},
		error: function(data){
    	alert(data.responseText);
		}
	});
}

function Documento_select_form(){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"documento/documento_select.php",
            async: true,
            dataType: "html",
            data:({
                    documento_tipo: 6,
                    vista_documento: 'documento',
                    documento_id:8
            }),
            beforeSend: function() {
                    $('#cmb_doc_id').html('<option>Cargando...</option>');
            },
            success: function(data){
                    $('#cmb_doc_id').html(data);
            },
            complete: function(data){
                    //console.log(data);
            },
            error: function(data){
                alert(data.responseText);
            }
    });
}

$('#txt_ing_det').autocomplete({
		minLength: 1,
		source: VISTA_URL+"ingreso/ingreso_complete_det.php"
	});

$('#txt_ing_det').change(function(){
        $(this).val($(this).val().toUpperCase());
});

$(document).ready(function(){
//    Cuenta_select_form();
    $("#cmb_ing_est").val('1');
    Documento_select_form();
    
//    autocompletar para cliente
    $("#txt_cli_doc,#txt_cli_nom").autocomplete({
        minLength: 1,
        source: function(request, response){
            $.getJSON(
                    VISTA_URL+"cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
            );
        },
        select: function(event, ui){
            $('#hdd_cli_id').val(ui.item.cliente_id);
            $('#txt_cli_doc').val(ui.item.cliente_doc);
            $('#txt_cli_nom').val(ui.item.cliente_nom);
          event.preventDefault();
            $('#txt_cli_nom').focus();
        }
    });
    
//    autocompletar para detalle
//    $("#txt_cli_doc,#txt_cli_nom").autocomplete({
//        minLength: 1,
//        source: function(request, response){
//            $.getJSON(
//                    VISTA_URL+"cliente/cliente_autocomplete.php",
//                    {term: request.term}, //
//                    response
//            );
//        },
//        select: function(event, ui){
//            $('#hdd_cli_id').val(ui.item.cliente_id);
//            $('#txt_cli_doc').val(ui.item.cliente_doc);
//            $('#txt_cli_nom').val(ui.item.cliente_nom);
//          event.preventDefault();
//            $('#txt_cli_nom').focus();
//        }
//  });
  
    
    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
     });

      $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
      });
      
	$('.moneda').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999999.00'
	});

	$('#txt_ingreso_imp').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999.00'
	});


  $('#form_ingreso').validate({
    submitHandler: function() {
        
        if ($("#hdd_cli_id").val()=="") {
                swal_warning("AVISO","Debe Ingresar Un Proveedor para poder continuar",5000);
                return false;
            }
            if ($("#txt_ing_imp").val()=="0.00") {
                swal_warning("AVISO","El importe no debe Ser Igual a S/ 0.00",5000);
                $("#txt_ing_imp").focus();
                return false;
            }
        $.ajax({
                type: "POST",
                url: VISTA_URL+"ingreso/ingreso_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_ingreso").serialize(),
                beforeSend: function() {
                        $('#ingreso_mensaje').show(400);
                        $('#btn_guardar_ingreso').prop('disabled', true);
                },
                success: function(data){
                        if(parseInt(data.estado) > 0){
                            $('#ingreso_mensaje').removeClass('callout-info').addClass('callout-success')
                            $('#ingreso_mensaje').html(data.mensaje);

                            var vista = $('#ingreso_vista').val();

                            if(vista == "rendicioncuentas"){
                                alerta_success("EXITO", "Devolucion Registrada")
                                rendicioncuentadetalle_tabla()
                                rendicioncuentas_tabla()
                                $('#modal_registro_ingreso').modal('hide');
                                $('#modal_registro_rendicioncuentas').modal('hide');
                            /* GERSON (14-01-25) */
                            }else if(vista=='permisoanulacion'){
                                setTimeout(function(){
                                    permisoanulacion_tabla();
                                    $('#modal_registro_ingreso').modal('hide');
                                    }, 1000
                                );
                            /*  */
                            }else{
                                setTimeout(function(){
                                                ingreso_tabla();
                                        $('#modal_registro_ingreso').modal('hide');
                                        }, 1000
                                );
                            }
                            }
                            else{
		      	$('#ingreso_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#ingreso_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_ingreso').prop('disabled', false);
		      }
				},
                complete: function(data){
                        //console.log(data);
                },
                error: function(data){
                        alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
                }
                    });
    },
    rules: {
            cmb_caj_id: {
                required: true,
                min: 1
            },
            cmb_mon_id: {
                required: true,
                min: 1
            },
            cmb_ing_est: {
                required: true,
                min: 1
            },
            cmb_cue_id: {
                required: true,
                min: 1
            },
            txt_cli_doc: {
                required: true,
            },
            hdd_cli_id: {
                required: true,
            },
            cmb_doc_id: {
                required: true,
                min: 1
            },
            txt_ing_imp: {
                required: true,
            },
            txt_ing_det: {
                required: true,
            }
          },
    messages: {
            cmb_caj_id: {
                required: "Debe seleccionar una Caja",
                min: "Debe seleccionar al menos una caja"
            },
            cmb_mon_id: {
                required: "Debe seleccionar una Moneda",
                min: "Debe seleccionar al menos una Moneda"
            },
            cmb_ing_est: {
                required: "Debe seleccionar un estado",
                min: "Debe seleccionar al menos un estado"
            },
            cmb_cue_id: {
                required: "Debe seleccionar una cuenta",
                min: "Debe seleccionar al menos una cuenta"
            },
            txt_cli_doc: {
                required: "Debe ingresar un Documento de Cliente",
            },
            hdd_cli_id: {
                required: "Debe ingresar un Cliente",
            },
            cmb_doc_id: {
                required: "Debe seleccionar un Documento",
                min: "Debe seleccionar al menos un Documento"
            },
            txt_ing_imp: {
                required: "Debe ingresar un Importe de Ingreso",
            },
            txt_ing_det: {
                required: "Debe ingresar un detalle",
            }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
                // Add the `help-block` class to the error element
                error.addClass( "help-block" );
                if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.parent( "label" ) );
                } else {
                        error.insertAfter( element );
                }
        },
        highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
        }
	});
});


$('#cmb_cue_id').change(function(){
    SubCuenta_select_form($('#cmb_cue_id').val());
});

/* GERSON (14-01-25) */
// CUANDO LA ANULACION SE HACE DESDE PERMISOANULACION
function permisoanulacion_tabla(){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"permisoanulacion/permisoanulacion_tabla.php",
      async: true,
      dataType: "html",
      data: $('#permisoanulacion_filtro').serialize(),
      beforeSend: function() {
        $('#permisoanulacion_mensaje_tbl').show(300);
      },
      success: function(data){
        $('#div_permisoanulacion_tabla').html(data);
        $('#permisoanulacion_mensaje_tbl').hide(300);
  
        let registros = data.includes("No hay");
        console.log(registros);
        if (!registros) estilos_datatable();
        
      },
      complete: function(data){
        
      },
      error: function(data){
        $('#permisoanulacion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
      }
    });
}
/*  */