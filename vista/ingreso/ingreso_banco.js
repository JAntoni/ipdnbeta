/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function llenar_datos_cliente_banco(data){
	console.log(data);
	$('#hdd_cli_id').val(data.cliente_id);
	$('#txt_cli_nom').val(data.cliente_nom);
}

function Cuenta_select_form(){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"cuenta/cuenta_select.php",
            async: true,
            dataType: "html",
            data:({
                    tipocuenta: 1
            }),
            beforeSend: function() {
                    $('#cmb_cue_id').html('<option>Cargando...</option>');
            },
            success: function(data){
                    $('#cmb_cue_id').html(data);
            },
            complete: function(data){
                    //console.log(data);
            },
            error: function(data){
                alert(data.responseText);
            }
    });
}

function SubCuenta_select_form(cuenta_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"subcuenta/subcuenta_select.php",
		async: true,
		dataType: "html",
		data:({
			cuenta_id: cuenta_id
//			vista_subcategoria: 'producto'
		}),
		beforeSend: function() {
			$('#cmb_subcue_id').html('<option>Cargando...</option>');
		},
		success: function(data){
			$('#cmb_subcue_id').html(data);
		},
		complete: function(data){
			//console.log(data);
		},
		error: function(data){
    	alert(data.responseText);
		}
	});
}



$('#cmb_cue_id').change(function(){
    SubCuenta_select_form($('#cmb_cue_id').val());
    var cuenta = $(this).find("option:selected").text();
        $('#hdd_cuenta_dep').val(cuenta);
});
//$('#cmb_cue_id').change(function(){
//        
////        $('#hdd_mon_iddep').val(moneda_sel);
//});

$(document).ready(function() {
    Cuenta_select_form();
    $("#txt_cli_doc,#txt_cli_nom").autocomplete({
        minLength: 1,
        source: function(request, response){
            $.getJSON(
                    VISTA_URL+"cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
            );
        },
        select: function(event, ui){
            $('#hdd_cli_id').val(ui.item.cliente_id);
            $('#txt_cli_doc').val(ui.item.cliente_doc);
            $('#txt_cli_nom').val(ui.item.cliente_nom);
          event.preventDefault();
            $('#txt_cli_nom').focus();
        }
  });

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
     });

      $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
      });
      
      $('#ingreso_fil_picker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate : new Date()
      });

    $('.moneda3').autoNumeric({
            aSep: ',',
            aDec: '.',
            //aSign: 'S/. ',
            //pSign: 's',
            vMin: '0.00',
            vMax: '999999.99'
    });
    $('.moneda4').autoNumeric({
            aSep: ',',
            aDec: '.',
            //aSign: 'S/. ',
            //pSign: 's',
            vMin: '0.00',
            vMax: '99999.00'
    });
    
    var pc_nom = $('#hdd_vista_pc_nombre').val();
    var pc_mac = $('#hdd_vista_pc_mac').val();
    $('#hdd_pc_nombre').val(pc_nom);
    $('#hdd_pc_mac').val(pc_mac);
    

//    cambio_moneda(2);
//    calcular_mora();
    
    $("#chk_mor_aut").change(function(){
        if($('#chk_mor_aut').is(':checked'))
        {  
                $("#txt_cuopag_mor").prop('readonly', false);
                //$("#txt_cuopag_mor").addClass("moneda2");
        } else {  
                $("#txt_cuopag_mor").prop('readonly', true);
                //$("#txt_cuopag_mor").removeClass("moneda2");
        }
    });
//    $('#txt_cuopag_fecdep').change(function(event) {
//        cambio_moneda(2);
//        calcular_mora();
//    });
    
    $('#txt_cuopag_mon, #txt_cuopag_comi').change(function(event) {
            var monto_deposito = Number($('#txt_cuopag_mon').autoNumeric('get'));
            var monto_comision = Number($('#txt_cuopag_comi').autoNumeric('get'));
            var cuenta_deposito = $('#cmb_cuedep_id').val();
            var saldo = 0;

            saldo = monto_deposito - monto_comision;

            if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
                    $('#txt_cuopag_montot').autoNumeric('set',saldo.toFixed(2));
                    $('#txt_cuopag_monval').autoNumeric('set',saldo.toFixed(2));
            }
            else{
                    $('#txt_cuopag_montot').autoNumeric('set',0);
                    $('#txt_cuopag_monval').autoNumeric('set',0);
            }

            if(cuenta_deposito)
                    $('#cmb_cuedep_id').change();
    });

    $('#cmb_cuedep_id').change(function(event) {
        var moneda_sel = $(this).find(':selected').data('moneda'); //este tipo de moneda corresponde al tipo de cuenta seleccionada
        var moneda_id = $('#hdd_mon_id').val(); //esta moneda pertenece al credito
        var monto_validar = Number($('#txt_cuopag_monval').autoNumeric('get')); //es el restanto de lo depositado menos la comision que cobra el banco
        var monto_cambio = Number($('#txt_cuopag_tipcam').val()); //monto del tipo de cambio del día del deposito
        var total_pagado = 0;
        if(parseInt(moneda_sel) == 2 && parseInt(moneda_id) == 1){
                total_pagado = parseFloat(monto_validar * monto_cambio).toFixed(2);
                $('#txt_cuopag_montot').autoNumeric('set',total_pagado);
                //console.log('dolar a soles monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
        }
        else if(parseInt(moneda_sel) == 1 && parseInt(moneda_id) == 2){
                total_pagado = parseFloat(monto_validar / monto_cambio).toFixed(2);
                $('#txt_cuopag_montot').autoNumeric('set',total_pagado);
                //console.log('soles a dolares monto validar: ' + monto_validar+' cambio: ' + monto_cambio);
        }
        else
                $('#txt_cuopag_montot').autoNumeric('set',monto_validar);
            
            
        var cuenta = $(this).find("option:selected").text();
        $('#hdd_cuenta_dep').val(cuenta);

    });

    
});

function cliente_form(usuario_act, cliente_id){ 
  $.ajax({
        type: "POST",
        url: VISTA_URL+"cliente/cliente_form.php",
        async: true,
        dataType: "html",
        data: ({
        action: usuario_act, // PUEDE SER: L, I, M , E
        cliente_id: cliente_id,
        vista: 'pago_banco'
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            if(data != 'sin_datos'){
                $('#div_modal_cliente_form').html(data);
                $('#modal_registro_cliente').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if(usuario_act == 'L' || usuario_act == 'E')
                  form_desabilitar_elementos('form_cliente'); //funcion encontrada en public/js/generales.js

                //funcion js para limbiar el modal al cerrarlo
                modal_hidden_bs_modal('modal_registro_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_cliente'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un ancho automatico al modal, al abrirlo
                modal_width_auto('modal_registro_cliente', 75); //funcion encontrada en public/js/generales.js
            }
            else{
              //llamar al formulario de solicitar permiso
              var modulo = 'cliente';
              var div = 'div_modal_cliente_form';
              permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){
                $('#modal_mensaje').modal('hide');
        },
        error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
        }
	});
}

$("#for_cuopag").validate({
submitHandler: function() {
    $.ajax({
            type: "POST",
            url: VISTA_URL+"ingreso/ingreso_banco_reg.php",
            async:true,
            dataType: "json",
            data: $("#for_cuopag").serialize(),
            beforeSend: function() {
            },
            success: function(data){
                if(parseInt(data.cuopag_id) > 0){
                    $('#modal_registro_vencimientomenorbanco').modal('hide');
                   swal_success('Sistema',data.cuopag_msj,2000);     
//                                if(confirm('¿Desea imprimir?'))
//                                         cuotapago_imppos_datos(data.cuopag_id);
                        ingreso_tabla();

                }
                else{
                        swal_warning('AVISO',data.cuopag_msj,10000);
                }	
            },
            complete: function(data){
//                console.log(data);
                if(data.statusText != "success" || data.statusText != "OK"){
//                                        $('#btn_guar_cuopag').show();
//                                        $('#msj_pagobanco').show(100);
//                                        $('#msj_pagobanco').text('ERROR: ' + data.responseText);
//                                        swal_error('ERROR',data.responseText,10000);
//                                        console.log(data);
                }
            }
        });
        },
        rules: {
                cmb_cue_id: {
                    min: 1
                },
                txt_cuopag_mon:{
                        required:true
                },
                txt_cuopag_tipcam:{
                        required:true
                },
                txt_cuopag_numope: {
                        required:true
                },
                txt_cuopag_comi:{
                        required: true
                },
                txt_cuopag_monval: {
                        required: true
                },
                cmb_cuedep_id: {
                        required: true
                },
                txt_cuopag_montot: {
                        required: true
                }
        },
        messages: {
                cmb_cue_id: {
                    min: 'debe seleccionar al menos una cuenta'
                },
                txt_cuopag_mon:{
                        required:'* registre el monto pagado'
                },
                txt_cuopag_tipcam:{
                        required:'* registre el tipo de cambio'
                },
                txt_cuopag_numope: {
                        required: '* registre el numero de operacion'
                },
                txt_cuopag_comi:{
                        required: '* registre la comisión del banco'
                },
                txt_cuopag_monval: {
                        required: '* registre el monto a validar'
                },
                cmb_cuedep_id: {
                        required: '* registre la cuenta de deposito'
                },
                txt_cuopag_montot: {
                        required: '* registre el monto total'
                }
        }
});