<form id="ingreso_filtro" name="ingreso_filtro">
    <input type="hidden" class="form-control input-sm" id="usuariogrupo_id" name="usuariogrupo_id" value="<?php echo $_SESSION['usuariogrupo_id'];?>">
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Fecha </label>
            <div class="input-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo date('d-m-Y');?>"/>
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <span class="input-group-addon">-</span>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo date('d-m-Y');?>"/>
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <label for="">Caja</label>
                <select class="form-control input-sm" id="cmb_fil_caj_id" name="cmb_fil_caj_id">
                    <?php require_once VISTA_URL.'caja/caja_select.php';?>
                </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Cliente</label>
            <input type="text" class="form-control input-sm" id="txt_fil_cli" name="txt_fil_cli">
            <input type="hidden" id="hdd_fil_cli_id" name="hdd_fil_cli_id" class="form-control input-sm" >
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <label for="">Estado</label>
            <select name="cmb_fil_ing_est" id="cmb_fil_ing_est" class="form-control input-sm">
                <option value="">-</option>
                    <option value="1">CANCELADO</option>
                <option value="2">EMITIDO</option>
            </select>
        </div>
    </div>
    
    <?php if($_SESSION['usuariogrupo_id']==2 || $_SESSION['usuariogrupo_id']==6){//2 es gerencia y 6 es contabilidad en el caso de perfil de usuario 5 si es de contabilidad
            $empresa_id=$_SESSION['empresa_id'];
        ?>
     <div class="col-md-2">
            <label for="">Sede :</label>
            <select class="form-control input-sm" id="cmb_fil_empresa_id" name="cmb_fil_empresa_id">
                <?php require_once VISTA_URL.'empresa/empresa_select.php';?>
            </select>
    </div>
    <div class="col-md-2">
        <label for="">Usuario</label>
        <select name="cmb_fil_usuario_id" id="cmb_fil_usuario_id" class="form-control orm-control-sm selectpicker" data-live-search="true" data-max-options="1" data-size="12">
            
        </select>
    </div>
    <?php 
    
    }
    else{
        ?>
        <div class="col-md-2">
                <label for="">Sede :</label>
                <select class="form-control input-sm" id="cmb_fil_empresa_id" name="cmb_fil_empresa_id">
                    <?php // require_once VISTA_URL.'empresa/empresa_select.php';?>
                    <option value="<?php echo $_SESSION['empresa_id'] ?>" style="font-weight: bold;" ><?php echo $_SESSION['empresa_nombre'];?></option>
                </select>
        </div>
    
        <div class="col-md-2">
            <label for="">Usuarios</label>
            <select name="cmb_fil_usuario_id" id="cmb_fil_usuario_id" class="form-control input-sm">
                <option value="<?php echo $_SESSION['usuario_id'] ?>" style="font-weight: bold;" ><?php echo $_SESSION['usuario_ape'].' '.$_SESSION['usuario_nom'];?></option>
            </select>
        </div>
     <?php
    }
    ?>
</div>
<div class="row">
    <div class="col-md-2">
        <label for="">Documento</label>
        <select name="cmb_fil_doc_id" id="cmb_fil_doc_id" class="form-control input-sm">
            
        </select>
    </div>
    <div class="col-md-2">
	<label for="txt_fil_egr_numdoc">N°:</label>
    <input name="txt_fil_ing_numdoc" type="text" id="txt_fil_ing_numdoc" size="15" maxlength="15" class="form-control input-sm">
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Cuenta</label>
            <select name="cmb_fil_cue_id" id="cmb_fil_cue_id" class="form-control input-sm">
                <?php include VISTA_URL.'cuenta/cuenta_select.php';?>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Sub Cuenta</label>
            <select name="cmb_fil_subcue_id" id="cmb_fil_subcue_id" class="form-control input-sm">
                <?php //include VISTA_URL.'cuenta/cuenta_select.php';?>
            </select>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <label for="">Moneda</label>
            <select name="cmb_moneda_id" id="cmb_moneda_id" class="form-control input-sm">
                <?php include VISTA_URL.'moneda/moneda_select.php';?>
            </select>
        </div>
    </div>
    <div class="col-md-1">
      <div class="form-group">
        <label for=""> </label>
        <div class="input-group">
            <button type="button" class="btn btn-primary btn-xs" onclick="ingreso_tabla()" title="Filtrar"><i class="fa fa-search"></i></button>
            <!--<span class="input-group-addon"></span>-->
            <button type="button" class="btn btn-success btn-xs" onclick="reestablecerValores()" title="Reestablecer"><i class="fa fa-refresh"></i></button>
        </div>
      </div>
    </div>
</div>
</form>