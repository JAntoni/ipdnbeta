<?php
    require_once('../../core/usuario_sesion.php');
    require_once('../ingreso/Ingreso.class.php');
    $oIngreso = new Ingreso();
    require_once('../creditolinea/Creditolinea.class.php');
    $oCreditolinea = new Creditolinea();
    require_once('../permisoanulacion/Permisoanulacion.class.php');
    $oPermisoanulacion = new Permisoanulacion();
    require_once('../rendicioncuentasdetalle/RendicionCuentasDetalle.class.php');
    $oRendicionCuentasDetalle = new RendicionCuentasDetalle();
    require_once('../rendicioncuentas/RendicionCuentas.class.php');
    $oRendicionCuentas = new RendicionCuentas();
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');

  $action = $_POST['action'];
  $oIngreso->ingreso_id=intval($_POST['hdd_ingreso_id']);
  $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
  $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
  $oIngreso->ingreso_fec = fecha_mysql($_POST['txt_ing_fec']); 
  $oIngreso->documento_id = intval($_POST['cmb_doc_id']); 
  $oIngreso->ingreso_numdoc = $_POST['txt_ing_numdoc'];
  $oIngreso->ingreso_det = $_POST['txt_ing_det']; 
  $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_ing_imp']);  
  $oIngreso->cuenta_id = intval($_POST['cmb_cue_id']);
  $oIngreso->subcuenta_id = intval($_POST['cmb_subcue_id']); 
  $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
  $oIngreso->caja_id = intval($_POST['cmb_caj_id']); 
  $oIngreso->moneda_id = intval($_POST['cmb_mon_id']);
  //valores que pueden ser cambiantes según requerimiento de ingreso
  $oIngreso->modulo_id = 0; 
  $oIngreso->ingreso_modide = 0; 
  $oIngreso->empresa_id = $_SESSION['empresa_id'];
  $oIngreso->ingreso_fecdep = ''; 
  $oIngreso->ingreso_numope = ''; 
  $oIngreso->ingreso_mondep = 0; 
  $oIngreso->ingreso_comi = 0; 
  $oIngreso->cuentadeposito_id = 0; 
  $oIngreso->banco_id = 0; 
  $oIngreso->ingreso_ap = 0; 
  $oIngreso->ingreso_detex = '';
  $vista = $_POST['ingreso_vista'];

 	if($action == 'insertar'){
 
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Ingreso.';
 
    $result=$oIngreso->insertar();
    if($result>0){
        $data['estado'] = 1;
        $data['mensaje'] = 'Ingreso registrado correctamente.';
        $data['datos'] =$result;

        if($vista == "rendicioncuentas"){
          $rendicioncuentasid = $_POST['hdd_rendicioncuentasid'];
          $data['ingresoid'] = $result['ingreso_id'];
          $data['rendicioncuentasid'] = $rendicioncuentasid;
          $resulti = $oRendicionCuentasDetalle->agregar_ingresodetalle($rendicioncuentasid, $result['ingreso_id'], intval($_SESSION['usuario_id']));
          //$data['resulti'] = $resulti['estado'];
          if($resulti['estado']){
            $oRendicionCuentas->modificar_campo_nuevo($rendicioncuentasid, intval($_SESSION['usuario_id']), 'estado', 2);
          }
        }
    }


 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Ingreso.';

 		if($oIngreso->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'ingreso modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar al ingreso.';
    
    /* daniel odar 15-04-2023 */
    //verificar si el ingreso está anexado en un gasto
    require_once('../gastogar/Gastogar.class.php');
    $oGastogar = new Gastogar();

    $array_where[0]['column_name'] = 'gg.tb_ingreso_idfk';
    $array_where[0]['param0'] = intval($_POST['hdd_ingreso_id']);
    $array_where[0]['datatype'] = 'INT';

    $array_where[1]['conector'] = 'AND';
    $array_where[1]['column_name'] = 'gg.tb_gastogar_xac';
    $array_where[1]['param1'] = 1;
    $array_where[1]['datatype'] = 'INT';

    $array_join = array();
    $res = $oGastogar->mostrarUno($array_where, $array_join);

    if ($res['estado'] == 1) {
      $data['mensaje'] = 'No se puede eliminar el ingreso, debido a que está anexado en un gasto | Gasto ID:'.$res['data']['tb_gastogar_id'].', en el CM-'. $res['data']['tb_credito_idfk'];
      echo json_encode($data); exit();
    }
    
    //? VAMOS A OBTENER LOS DATOS DEL INGRESO A ELIMINAR
    $ingreso_id = intval($_POST['hdd_ingreso_id']);

    $modulo_id = 0;
    $ingreso_modide = 0;
    $cadena_array = '';

    $result = $oIngreso->mostrarUno($ingreso_id);
      if($result['estado'] == 1){
        $modulo_id = $result['data']['tb_modulo_id'];
        $ingreso_modide = $result['data']['tb_ingreso_modide'];
        $cadena_array = $result['data']['tb_ingreso_array'];
      }
    $result = NULL;

 		if($oIngreso->eliminar()){
      //? 71 REPRESENTA QUE FUE UN INGRESO POR DEVOLUCION DE GASTOS VEHICULARES, AL ELIMINAR EL INGRESO SE DEBE VOLVER A CALULAR LAS DEVOLUCIONES Y ACTUALIZAR LA TABLA GASTO
      if($modulo_id == 71){
        require_once('../gasto/Gasto.class.php');
        $oGasto = new Gasto();

        $array_gasto_id = explode(',', $cadena_array);

        if(count($array_gasto_id) > 0){
          foreach($array_gasto_id as $gasto_id){
            $oGasto->modificar_campo($gasto_id, 'tb_gasto_devo', 0, 'STR');
            $oGasto->agregar_historial($gasto_id, 'El usuario <b>'.$_SESSION['usuario_nom'].'</b> ha eliminado el INGRESO POR DEVOLUCION de este gasto, el monto de: '.$_POST['txt_ing_imp'].'. | <b>'.date("d-m-Y h:i a").'</b><br>');
          }
        }

        $oCreditolinea->insertar(3, $ingreso_modide, $_SESSION['usuario_id'], 'El usuario <b>'.$_SESSION['usuario_nom'].'</b> ha eliminado el INGRESO POR DEVOLUCION de GASTOS VEHICULARES, el monto de: '.$_POST['txt_ing_imp'].'. | <b>'.date("d-m-Y h:i a").'</b>');

        //? VAMOS A VALIDAR INGRESOS QUE SE TENGA POR GASTO ID
        $result = $oIngreso->mostrar_ingresos_modulo(71, $ingreso_modide);
          if($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value) {
              $cadena_array = $value['tb_ingreso_array'];
              $ingreso_imp = $value['tb_ingreso_imp'];

              $array_gasto_id = explode(',', $cadena_array);

              if(count($array_gasto_id) == 1){
                $result2 = $oGasto->mostrarUno($array_gasto_id[0]);
                  $gasto_ptl = formato_numero($result2['data']['tb_gasto_ptl']);
                  $gasto_devo = formato_numero($result2['data']['tb_gasto_devo']);
                $result2 = NULL;

                $gasto_devo += $ingreso_imp;

                $oGasto->modificar_campo($array_gasto_id[0], 'tb_gasto_devo', $gasto_devo, 'STR');
              }
            }
          }
        $result = NULL;
      }

      /* GERSON (14-01-25) */
      $permisoanulacion = $oPermisoanulacion->mostrarPermisoanulacionIE($ingreso_id, 3);
      if($permisoanulacion['estado'] == 1){
        $oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_est', 2, 'INT');
        $oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_fec_aten', date('Y-m-d h:i:s'), 'STR');
        $oPermisoanulacion->modificar_campo_permisoanulacion($permisoanulacion['data']['tb_permisoanulacion_id'], 'tb_permisoanulacion_user_aten', $_SESSION['usuario_id'], 'INT');
      }
      /*  */
      
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Ingreso eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>