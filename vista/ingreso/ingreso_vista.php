<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Caja</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Ingresos</a></li>
            <li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
        </ol>
    </section>

	<!-- Main content -->
	<section class="content">
             <?php
                    //vamos a validar que la caja est茅 aperturada para poder hacer ingresos o egresos de todo tipo
                    require_once ("vista/funciones/funciones.php");
                    validar_apertura_caja();
                  ?>
            <div class="box box-primary">
                <div class="box-header">
                        <button class="btn btn-primary btn-sm" onclick="ingreso_form('I',0)"><i class="fa fa-plus"></i> Nuevo Ingreso</button>
                        <button class="btn btn-primary btn-sm" onclick=""><i class="fa fa-refresh"></i> Actualizar</button>
                        <button class="btn btn-primary btn-sm" onclick="cuotapago_banco()"><i class="fa fa-plus"></i> Caja Banco</button>
                </div>
                
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="txt_filtro" class="control-label">Filtro de Ingresos</label>
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                  <?php include 'ingreso_filtro.php';?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>
                        <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="ingreso_mensaje_tbl" style="display: none;">
                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Ingresos...</h4>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Input para guardar el valor ingresado en el search de la tabla-->
                            <input type="hidden" id="hdd_datatable_fil">

                            <div id="div_ingreso_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                    <?php require_once('ingreso_tabla.php');?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="div_modal_ingreso_form">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
                </div>
                <div id="div_modal_vencimiento_menor_banco_form">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
                </div>
                <div id="div_modal_cliente_form">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
                </div>
                <div id="div_modal_upload_form"></div>
                <div id="div_modal_imagen_form"></div>
                <div id="div_modal_solicitar_anulacion"></div>
                <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
                <?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
            </div>
	</section>
	<!-- /.content -->
</div>
