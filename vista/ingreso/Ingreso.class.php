<?php

if (defined('APP_URL')) {
  require_once(APP_URL . 'datos/conexion.php');
} else {
  require_once('../../datos/conexion.php');
}

class Ingreso extends Conexion
{

  public $ingreso_id;
  public $ingreso_usureg;
  public $ingreso_usumod;
  public $ingreso_fec;
  public $documento_id;
  public $ingreso_numdoc;
  public $ingreso_det;
  public $ingreso_imp;
  public $cuenta_id;
  public $subcuenta_id;
  public $cliente_id;
  public $caja_id;
  public $moneda_id;
  public $modulo_id;
  public $ingreso_modide;
  public $empresa_id;
  public $ingreso_fecdep = '';
  public $ingreso_numope = '';
  public $ingreso_mondep = 0;
  public $ingreso_comi = 0;
  public $cuentadeposito_id = 0;
  public $banco_id = 0;
  public $ingreso_ap = 0;
  public $ingreso_detex = '';
  public $chequedetalle_id = 0;

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_ingreso(
                                    tb_ingreso_usureg,tb_ingreso_fec,tb_documento_id, 
                                    tb_ingreso_numdoc,tb_ingreso_det,tb_ingreso_imp,
                                    tb_cuenta_id,tb_subcuenta_id,tb_cliente_id, 
                                    tb_caja_id,tb_moneda_id,tb_modulo_id, 
                                    tb_ingreso_modide,tb_empresa_id,tb_ingreso_fecdep,tb_ingreso_numope, 
                                    tb_ingreso_mondep,tb_ingreso_comi,tb_cuentadeposito_id, 
                                    tb_banco_id,tb_ingreso_ap,tb_ingreso_detex)
                            VALUES(
                                    :ingreso_usureg, :ingreso_fec, :documento_id, 
                                    :ingreso_numdoc, UPPER(:ingreso_det), :ingreso_imp,
                                    :cuenta_id, :subcuenta_id, :cliente_id, 
                                    :caja_id, :moneda_id, :modulo_id, 
                                    :ingreso_modide,:empresa_id, :ingreso_fecdep, :ingreso_numope, 
                                    :ingreso_mondep, :ingreso_comi, :cuentadeposito_id, 
                                    :banco_id, :ingreso_ap, :ingreso_detex)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_usureg", $this->ingreso_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec", $this->ingreso_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":documento_id", $this->documento_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_numdoc", $this->ingreso_numdoc, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_det", $this->ingreso_det, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_imp", $this->ingreso_imp, PDO::PARAM_STR);
      $sentencia->bindParam(":cuenta_id", $this->cuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta_id", $this->subcuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":caja_id", $this->caja_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_modide", $this->ingreso_modide, PDO::PARAM_INT);
      $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fecdep", $this->ingreso_fecdep, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_numope", $this->ingreso_numope, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_mondep", $this->ingreso_mondep, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_comi", $this->ingreso_comi, PDO::PARAM_STR);
      $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":banco_id", $this->banco_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_ap", $this->ingreso_ap, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_detex", $this->ingreso_detex, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $ingreso_id = $this->dblink->lastInsertId();

      //este update se realiza de manera automática para evitar que el n° documento quede sin numero asignado
      $numdoc = trim($this->ingreso_numdoc);

      if (empty($numdoc) || intval($numdoc) <= 0) {
        $sql = "UPDATE tb_ingreso SET tb_ingreso_numdoc=:ingreso_id WHERE tb_ingreso_id=:ingreso_id";

        $sentencia = $this->dblink->prepare($sql);

        $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_STR);
        $sentencia->execute();
      }

      $this->dblink->commit();
      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['ingreso_id'] = $ingreso_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  /* GERSON (29-01-24) */
  function insertarChequeProceso()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_ingreso(
                                    tb_ingreso_usureg,tb_ingreso_fec,tb_documento_id,
                                    tb_ingreso_numdoc,tb_ingreso_det,tb_ingreso_imp,
                                    tb_cuenta_id,tb_subcuenta_id,tb_cliente_id,
                                    tb_caja_id,tb_moneda_id,tb_modulo_id,
                                    tb_ingreso_modide,tb_empresa_id,tb_ingreso_fecdep,tb_ingreso_numope,
                                    tb_ingreso_mondep,tb_ingreso_comi,tb_cuentadeposito_id,
                                    tb_banco_id,tb_ingreso_ap,tb_ingreso_detex,tb_chequedetalle_id)
                            VALUES(
                                    :ingreso_usureg, :ingreso_fec, :documento_id,
                                    :ingreso_numdoc, UPPER(:ingreso_det), :ingreso_imp,
                                    :cuenta_id, :subcuenta_id, :cliente_id,
                                    :caja_id, :moneda_id, :modulo_id,
                                    :ingreso_modide,:empresa_id, :ingreso_fecdep, :ingreso_numope,
                                    :ingreso_mondep, :ingreso_comi, :cuentadeposito_id,
                                    :banco_id, :ingreso_ap, :ingreso_detex, :chequedetalle_id)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_usureg", $this->ingreso_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec", $this->ingreso_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":documento_id", $this->documento_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_numdoc", $this->ingreso_numdoc, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_det", $this->ingreso_det, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_imp", $this->ingreso_imp, PDO::PARAM_STR);
      $sentencia->bindParam(":cuenta_id", $this->cuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta_id", $this->subcuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":caja_id", $this->caja_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_modide", $this->ingreso_modide, PDO::PARAM_INT);
      $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fecdep", $this->ingreso_fecdep, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_numope", $this->ingreso_numope, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_mondep", $this->ingreso_mondep, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_comi", $this->ingreso_comi, PDO::PARAM_STR);
      $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":banco_id", $this->banco_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_ap", $this->ingreso_ap, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_detex", $this->ingreso_detex, PDO::PARAM_STR);
      $sentencia->bindParam(":chequedetalle_id", $this->chequedetalle_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $ingreso_id = $this->dblink->lastInsertId();

      //este update se realiza de manera automática para evitar que el n° documento quede sin numero asignado
      $numdoc = trim($this->ingreso_numdoc);

      if (empty($numdoc) || intval($numdoc) <= 0) {
        $sql = "UPDATE tb_ingreso SET tb_ingreso_numdoc=:ingreso_id WHERE tb_ingreso_id=:ingreso_id";

        $sentencia = $this->dblink->prepare($sql);

        $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_STR);
        $sentencia->execute();
      }

      $this->dblink->commit();
      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['ingreso_id'] = $ingreso_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  /*  */

  /* GERSON (07-04-24) */
  function modificar_campo_registro($ingreso_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET
                                        tb_ingreso_fecreg = DATE_ADD(now(), INTERVAL 1 DAY),
                                        tb_ingreso_fecmod = DATE_ADD(now(), INTERVAL 1 DAY),
                                        tb_ingreso_fec = DATE_ADD(now(), INTERVAL 1 DAY)
                                WHERE 
                                        tb_ingreso_id =:ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  /*  */

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET 
                                    tb_ingreso_fecmod = NOW( ),
                                    tb_ingreso_usumod=:ingreso_usumod,
                                    tb_ingreso_fec=:ingreso_fec,
                                    tb_documento_id=:documento_id, 
                                    tb_ingreso_numdoc=:ingreso_numdoc,
                                    tb_ingreso_det=UPPER(:ingreso_det),
                                    tb_ingreso_imp=:ingreso_imp,
                                    tb_cuenta_id=:cuenta_id,
                                    tb_subcuenta_id=:subcuenta_id,
                                    tb_cliente_id=:cliente_id, 
                                    tb_caja_id=:caja_id,
                                    tb_moneda_id=:moneda_id,
                                    tb_modulo_id=:modulo_id, 
                                    tb_ingreso_modide=:ingreso_modide,
                                    tb_empresa_id=:empresa_id,
                                    tb_ingreso_fecdep=:ingreso_fecdep,
                                    tb_ingreso_numope=:ingreso_numope, 
                                    tb_ingreso_mondep=:ingreso_mondep,
                                    tb_ingreso_comi=:ingreso_comi,
                                    tb_cuentadeposito_id=:cuentadeposito_id, 
                                    tb_banco_id=:banco_id,
                                    tb_ingreso_ap=:ingreso_ap,
                                    tb_ingreso_detex =:ingreso_detex
                                WHERE 
                                    tb_ingreso_id=:ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_id", $this->ingreso_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_usumod", $this->ingreso_usumod, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec", $this->ingreso_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":documento_id", $this->documento_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_numdoc", $this->ingreso_numdoc, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_det", $this->ingreso_det, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_imp", $this->ingreso_imp, PDO::PARAM_STR);
      $sentencia->bindParam(":cuenta_id", $this->cuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta_id", $this->subcuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":caja_id", $this->caja_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_modide", $this->ingreso_modide, PDO::PARAM_INT);
      $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fecdep", $this->ingreso_fecdep, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_numope", $this->ingreso_numope, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_mondep", $this->ingreso_mondep, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_comi", $this->ingreso_comi, PDO::PARAM_STR);
      $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":banco_id", $this->banco_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_ap", $this->ingreso_ap, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_detex", $this->ingreso_detex, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function xac_ingreso_id($ing_id, $valor)
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_ingreso set tb_ingreso_xac =:valor where tb_ingreso_id =:ing_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":valor", $valor, PDO::PARAM_INT);
      $sentencia->bindParam(":ing_id", $ing_id, PDO::PARAM_INT);


      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function eliminar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET tb_ingreso_xac=0 WHERE tb_ingreso_id=:tb_ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_ingreso_id", $this->ingreso_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function anular_ingreso_por_modulo($modulo_id, $modide)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso set tb_ingreso_xac = 0 
				where tb_modulo_id =:tb_modulo_id and tb_ingreso_modide =:tb_ingreso_modide";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_modulo_id", $modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_ingreso_modide", $modide, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrarUno($ingreso_id)
  {
    try {
      $sql = "SELECT 
                        * 
                FROM 
                        tb_ingreso i
                        INNER JOIN tb_cuenta c ON i.tb_cuenta_id = c.tb_cuenta_id
                        INNER JOIN tb_caja cj ON i.tb_caja_id = cj.tb_caja_id
                        INNER JOIN tb_cliente cl ON i.tb_cliente_id = cl.tb_cliente_id
                        INNER JOIN tb_documento d ON i.tb_documento_id=d.tb_documento_id
                        LEFT JOIN tb_subcuenta sc ON i.tb_subcuenta_id = sc.tb_subcuenta_id
                        INNER JOIN tb_usuario u ON i.tb_ingreso_usureg=u.tb_usuario_id
                WHERE 
                        i.tb_ingreso_xac=1 AND tb_ingreso_id =:tb_ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_ingreso_id", $ingreso_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function verificar_numero_voucher($numero)
  {
    try {
      $sql = "SELECT * FROM tb_ingreso where LOWER(tb_ingreso_numope) = LOWER(:numero) and tb_ingreso_xac =1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":numero", $numero, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_ingresos($fec1, $fec2, $emp_id, $caj_id, $mon_id, $cli_id, $est, $doc_id, $numdoc, $cue_id, $subcue_id, $usuario_id)
  {

    $extension = "";
    if ($emp_id > 0)
      $extension = $extension . " AND i.tb_empresa_id = $emp_id";
    if ($caj_id > 0)
      $extension = $extension . " AND i.tb_caja_id = $caj_id";
    if ($mon_id > 0)
      $extension = $extension . " AND i.tb_moneda_id = $mon_id";
    if ($cli_id > 0)
      $extension = $extension . " AND i.tb_cliente_id = $cli_id ";
    if ($est > 0)
      $extension = $extension . " AND i.tb_ingreso_est LIKE  $est";
    if ($doc_id > 0)
      $extension = $extension . " AND i.tb_documento_id = $doc_id ";
    if ($numdoc > 0)
      $extension = $extension . " AND i.tb_ingreso_numdoc LIKE '%$numdoc%' ";
    if ($cue_id > 0)
      $extension = $extension . " AND i.tb_cuenta_id = $cue_id ";
    if ($subcue_id > 0)
      $extension = $extension . " AND i.tb_subcuenta_id = $subcue_id ";
    if ($usuario_id > 0)
      $extension = $extension . " AND i.tb_ingreso_usureg = $usuario_id ";

    try {
      $sql = "SELECT i.*, u.tb_usuario_nom, u.tb_usuario_ape, c.*, cj.*, cl.*, d.*, sc.*
                FROM 
                        tb_ingreso i
                        INNER JOIN tb_cuenta c ON i.tb_cuenta_id = c.tb_cuenta_id
                        INNER JOIN tb_caja cj ON i.tb_caja_id = cj.tb_caja_id
                        INNER JOIN tb_cliente cl ON i.tb_cliente_id = cl.tb_cliente_id
                        INNER JOIN tb_documento d ON i.tb_documento_id=d.tb_documento_id
                        LEFT JOIN tb_subcuenta sc ON i.tb_subcuenta_id = sc.tb_subcuenta_id
                        INNER JOIN tb_usuario u ON i.tb_ingreso_usureg=u.tb_usuario_id
                WHERE 
                        i.tb_ingreso_xac=1
                        AND i.tb_ingreso_fec BETWEEN :fec1 AND :fec2   
                        " . $extension . " ORDER BY i.tb_ingreso_fec ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
      //        $sentencia->bindParam(":emp_id", $emp_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Ingresos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function total_ingresos_fecha($ingreso_fec1, $ingreso_fec2, $usuario_id, $empresa_id, $moneda_id)
  {
    try {
      $fecha = '';
      $usuario = '';
      if (!empty($ingreso_fec1) && !empty($ingreso_fec2)) {
        $fecha = ' AND tb_ingreso_fec BETWEEN :ingreso_fec1 AND :ingreso_fec2';
      }

      if (intval($usuario_id) > 0)
        $usuario = ' AND tb_ingreso_usureg =' . intval($usuario_id);

      $sql = "SELECT SUM(tb_ingreso_imp) AS total_ingresos FROM tb_ingreso WHERE tb_ingreso_xac = 1 AND tb_empresa_id =:empresa_id AND tb_moneda_id=:tb_moneda_id " . $fecha . ' ' . $usuario;

      $sentencia = $this->dblink->prepare($sql);
      if (!empty($ingreso_fec1) && !empty($ingreso_fec2)) {
        $sentencia->bindParam(":ingreso_fec1", $ingreso_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":ingreso_fec2", $ingreso_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_moneda_id", $moneda_id, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_ingresos_modulo($modulo_id, $ingreso_modide)
  {
    try {
      $sql = "SELECT * FROM tb_ingreso i INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id WHERE tb_ingreso_xac=1 AND i.tb_modulo_id =:modulo_id AND i.tb_ingreso_modide =:ingreso_modide";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_modide", $ingreso_modide, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function anular_ingresos_por_modulo($ingreso_valor, $modulo_id, $ingreso_modide)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET tb_ingreso_xac =:ingreso_valor WHERE tb_modulo_id =:modulo_id AND tb_ingreso_modide =:ingreso_modide";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_valor", $ingreso_valor, PDO::PARAM_INT);
      $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_modide", $ingreso_modide, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrar_ingreso_usuario_modulo($modulo_id, $ingreso_modide)
  {
    try {
      $sql = "SELECT tb_ingreso_id, tb_ingreso_numope, tb_ingreso_fecreg, tb_ingreso_det, tb_usuario_id, tb_usuario_nom, tb_usuario_fot FROM tb_ingreso ing INNER JOIN tb_usuario usu on usu.tb_usuario_id = ing.tb_ingreso_usureg where tb_modulo_id =:modulo_id and tb_ingreso_modide =:ingreso_modide";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_modide", $ingreso_modide, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //mostrar el importe total de una cuota o subcuota, por tipo de moneda
  function mostrar_importe_total_cuota($cuota_id, $tipo_cuota, $moneda_id)
  {
    try {
      $sql = "SELECT SUM(tb_ingreso_imp) AS importe_total FROM tb_ingreso ing 
            INNER JOIN tb_cuotapago cuo on cuo.tb_cuotapago_id = ing.tb_ingreso_modide 
            where tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and cuo.tb_modulo_id =:tipo_cuota 
                and tb_cuotapago_modid =:cuota_id and tb_cuotapago_xac = 1 and ing.tb_modulo_id = 30";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tipo_cuota", $tipo_cuota, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //mostrar el importe total de una cuota o ingreso por tipo de moneda y una determinada fecha de ingreso
  function mostrar_importe_total_cuota_por_fecha($cuota_id, $tipo_cuota, $moneda_id, $ingreso_fec1, $ingreso_fec2)
  {
    try {
      $sql = "SELECT SUM(tb_ingreso_imp) AS importe_total FROM tb_ingreso ing 
      INNER JOIN tb_cuotapago cuo on cuo.tb_cuotapago_id = ing.tb_ingreso_modide 
      where tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and cuo.tb_modulo_id =:tipo_cuota and tb_cuotapago_modid =:cuota_id and tb_cuotapago_xac = 1 and ing.tb_modulo_id = 30 and tb_ingreso_fec between :ingreso_fec1 and :ingreso_fec2";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tipo_cuota", $tipo_cuota, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec1", $ingreso_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_fec2", $ingreso_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //mostrar el importe total por credito, y sub credito, adenda, AP, depende de la columna credito_tip
  function mostrar_importe_total_credito_subcredito_fecha($tabla, $creditotipo_id, $credito_est, $subcredito_id, $ingreso_fec1, $ingreso_fec2)
  {
    try {
      $columna_tip = 'tb_credito_tip'; // para GARVEH, HIPO
      if ($tabla == 'tb_creditoasiveh')
        $columna_tip = 'tb_credito_tip1';

      $sql = "SELECT IFNULL(SUM(CASE WHEN ing.tb_moneda_id =2 THEN tb_ingreso_imp ELSE 0 END), 0) AS importe_total_dolares, IFNULL(SUM(CASE WHEN ing.tb_moneda_id =1 THEN tb_ingreso_imp ELSE 0 END), 0) AS importe_total_soles FROM 
          tb_ingreso ing INNER JOIN tb_cuotapago cupag on cupag.tb_cuotapago_id = ing.tb_ingreso_modide 
          inner join tb_ingreso cd on cd.tb_ingreso_id = cupag.tb_cuotapago_modid
          inner join tb_cuota cuo on cuo.tb_cuota_id = cd.tb_cuota_id
          inner join $tabla cre on cre.tb_credito_id = cuo.tb_credito_id
          where tb_creditotipo_id =:creditotipo_id and $columna_tip in ($subcredito_id) and tb_credito_est in ($credito_est) and tb_ingreso_xac = 1 and cupag.tb_modulo_id =2 and tb_cuotapago_xac = 1 and ing.tb_modulo_id = 30 and ing.tb_caja_id IN(1,14) and tb_ingreso_fec between :ingreso_fec1 and :ingreso_fec2";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec1", $ingreso_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_fec2", $ingreso_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //mostrar el importe total de una mora de una cuota o subcuota
  function mostrar_importe_total_mora($cuota_id, $tipo_cuota, $moneda_id)
  {
    try {
      $sql = "SELECT SUM(tb_ingreso_imp) AS importe_total FROM tb_ingreso ing INNER JOIN tb_morapago mor on mor.tb_morapago_id = ing.tb_ingreso_modide where tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and mor.tb_modulo_id =:tipo_cuota and tb_morapago_modid =:cuota_id and ing.tb_modulo_id = 50 and tb_morapago_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tipo_cuota", $tipo_cuota, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_importe_total_modulo_fecha($modulo_id, $moneda_id, $ingreso_fec1, $ingreso_fec2)
  {
    try {
      $sql = "SELECT SUM(tb_ingreso_imp) AS importe_total FROM tb_ingreso where tb_ingreso_xac = 1 and tb_moneda_id =:moneda_id and tb_ingreso_fec between :ingreso_fec1 and :ingreso_fec2 and tb_modulo_id =:modulo_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec1", $ingreso_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_fec2", $ingreso_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $ingreso_fec1, $ingreso_fec2)
  {
    try {
      $sql = "SELECT SUM(tb_ingreso_imp) AS importe_total, COUNT(DISTINCT(tb_ingreso_modide)) AS total_ingresos FROM tb_ingreso where tb_ingreso_xac = 1 and tb_moneda_id =:moneda_id and tb_ingreso_fec between :ingreso_fec1 and :ingreso_fec2 and tb_cuenta_id =:cuenta_id and tb_subcuenta_id =:subcuenta_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta_id", $subcuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":ingreso_fec1", $ingreso_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":ingreso_fec2", $ingreso_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function lista_ingresos_num_operacion($numope)
  {
    try {
      $sql = "SELECT * FROM tb_ingreso WHERE TRIM(tb_ingreso_numope) =:numope AND tb_ingreso_xac =1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":numope", $numope, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function complete_det($emp, $det)
  {
    $filtro = "%" . $det . "%";
    try {
      $sql = "  SELECT 
                        DISTINCT(tb_ingreso_det) 
                    FROM 
                        tb_ingreso
                    WHERE 
                        tb_empresa_id=:tb_empresa_id AND tb_ingreso_det like :filtro ORDER BY tb_ingreso_det LIMIT 0 , 10";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_empresa_id", $emp, PDO::PARAM_INT);
      $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function registrar_datos_deposito_banco()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso set   tb_ingreso_fecdep =:tb_ingreso_fecdep, 
                                        tb_ingreso_numope =:tb_ingreso_numope, 
                                        tb_ingreso_mondep =:tb_ingreso_mondep,
                                        tb_ingreso_comi =:tb_ingreso_comi,
                                        tb_cuentadeposito_id =:tb_cuentadeposito_id,
                                        tb_banco_id =:tb_banco_id 
                                where 
                                        tb_ingreso_id =:tb_ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_ingreso_fecdep", $this->ingreso_fecdep, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_ingreso_numope", $this->ingreso_numope, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_ingreso_mondep", $this->ingreso_mondep, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_ingreso_comi", $this->ingreso_comi, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_banco_id", $this->banco_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_ingreso_id", $this->ingreso_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar_campo($ingreso_id, $ingreso_columna, $ingreso_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($ingreso_columna) && ($param_tip == 'INT' || $param_tip == 'STR'))
      {
        $sql = "UPDATE tb_ingreso SET " . $ingreso_columna . " =:ingreso_valor WHERE tb_ingreso_id =:ingreso_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
        {
          $sentencia->bindParam(":ingreso_valor", $ingreso_valor, PDO::PARAM_INT);
        }
        else
        {
          $sentencia->bindParam(":ingreso_valor", $ingreso_valor, PDO::PARAM_STR);
        }
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto el ingreso retorna 1
      }
      else
      {
        return 0;
      }
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar_campo_nuevo($ingreso_id, $usumod, $campo, $ingreso_valor)
  {
    $this->dblink->beginTransaction();
    try {


      $sql = "UPDATE tb_ingreso SET
                                        tb_ingreso_fecmod = NOW( ),
                                        tb_ingreso_usumod =:tb_ingreso_usumod,
                                        tb_ingreso_$campo = :campo 
                                WHERE 
                                        tb_ingreso_id =:ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_id", $ingreso_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_ingreso_usumod", $usumod, PDO::PARAM_INT);
      $sentencia->bindParam(":campo", $ingreso_valor, PDO::PARAM_STR);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function ingresos_totales_num_operacion($numope)
  {
    try {
      $sql = "SELECT IFNULL(SUM(tb_ingreso_imp),0) as importe, tb_moneda_id FROM tb_ingreso WHERE TRIM(tb_ingreso_numope) =:tb_ingreso_numope AND tb_ingreso_xac =1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_ingreso_numope", $numope, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modulo_cuotapago($mod_id, $cuota_id)
  {
    try {
      $sql = "SELECT * FROM tb_ingreso ing INNER JOIN tb_usuario usu on usu.tb_usuario_id = ing.tb_ingreso_usureg where tb_modulo_id =:tb_modulo_id and tb_ingreso_modide =:tb_ingreso_modide";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_modulo_id", $mod_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_ingreso_modide", $cuota_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function ingreso_credito_rango_fecha_vigente($fec1, $fec2, $tabla, $cre_tip)
  {
    try {
      if (intval($cre_tip) != 1) {
        $sql = "
                    SELECT 
                    tb_ingreso_id, 
                    tb_ingreso_fec, 
                    tb_ingreso_imp, 
                    ing.tb_moneda_id, 
                    pag.tb_cuotapago_tipcam, 
                    tb_ingreso_det,
                    ing.tb_cliente_id,
                    cre.tb_credito_id,
                    tb_cuotadetalle_fec,
                    tb_cuotadetalle_cuo,
                    cuo.tb_cuota_id,
                    tb_cuota_est,
                    tb_cuota_fec,
                    tb_cuota_int,
                    tb_cuota_cuo,
                    (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) as cuota_real
                FROM tb_ingreso ing 
                INNER JOIN tb_cuotapago pag ON pag.tb_cuotapago_id = ing.tb_ingreso_modide 
                INNER JOIN tb_cuotadetalle det ON det.tb_cuotadetalle_id = pag.tb_cuotapago_modid
                INNER JOIN tb_cuota cuo ON cuo.tb_cuota_id = det.tb_cuota_id 
                INNER JOIN " . $tabla . " cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = " . $cre_tip . ")
                WHERE 
                    tb_ingreso_xac = 1 
                    AND pag.tb_modulo_id = 2 
                    AND ing.tb_modulo_id = 30 
                    AND tb_cuotapago_xac = 1 
                    AND tb_ingreso_ap = 0 
                    AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_cuotadetalle_fec >= :fec1";
      } else {
        $sql = "
                    SELECT 
                        tb_ingreso_id, 
                        tb_ingreso_fec, 
                        tb_ingreso_imp, 
                        ing.tb_moneda_id, 
                        pag.tb_cuotapago_tipcam, 
                        tb_ingreso_det,
                        ing.tb_cliente_id,
                        cre.tb_credito_id,
                        tb_cuota_id,
                        tb_cuota_est,
                        tb_cuota_fec,
                        tb_cuota_int,
                        tb_cuota_cuo,
                        (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
                    FROM tb_ingreso ing 
                    INNER JOIN tb_cuotapago pag ON pag.tb_cuotapago_id = ing.tb_ingreso_modide
                    INNER JOIN tb_cuota cuo ON cuo.tb_cuota_id = pag.tb_cuotapago_modid 
                    INNER JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id =:creditotipo_id)
                    WHERE 
                        tb_ingreso_xac = 1 
                        AND pag.tb_modulo_id = 1 
                        AND ing.tb_modulo_id = 30 
                        AND tb_cuotapago_xac = 1 
                        AND tb_ingreso_ap = 0 
                        AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_cuota_fec >= :fec1;
                ";
      }

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
      $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function ingreso_credito_rango_fecha_vencida($fec1, $fec2, $tabla, $cre_tip)
  {
    try {
      if (intval($cre_tip) != 1) {
        $sql = "
                SELECT 
                tb_ingreso_id, 
                tb_ingreso_fec, 
                tb_ingreso_imp, 
                ing.tb_moneda_id, 
                pag.tb_cuotapago_tipcam, 
                tb_ingreso_det,
                ing.tb_cliente_id,
                cre.tb_credito_id,
                tb_cuotadetalle_fec,
                tb_cuotadetalle_cuo,
                cuo.tb_cuota_id,
                tb_cuota_est,
                tb_cuota_fec,
                tb_cuota_int,
                tb_cuota_cuo,
                (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) as cuota_real
            FROM tb_ingreso ing 
            INNER JOIN tb_cuotapago pag ON pag.tb_cuotapago_id = ing.tb_ingreso_modide 
            INNER JOIN tb_cuotadetalle det ON det.tb_cuotadetalle_id = pag.tb_cuotapago_modid
            INNER JOIN tb_cuota cuo ON cuo.tb_cuota_id = det.tb_cuota_id 
            INNER JOIN " . $tabla . " cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = " . $cre_tip . ")
            WHERE 
                tb_ingreso_xac = 1 
                AND pag.tb_modulo_id = 2 
                AND ing.tb_modulo_id = 30 
                AND tb_cuotapago_xac = 1 
                AND tb_ingreso_ap = 0 
                AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_cuotadetalle_fec < :fec1";
      } else {
        $sql = "
                SELECT 
                    tb_ingreso_id, 
                    tb_ingreso_fec, 
                    tb_ingreso_imp, 
                    ing.tb_moneda_id, 
                    pag.tb_cuotapago_tipcam, 
                    tb_ingreso_det,
                    ing.tb_cliente_id,
                    cre.tb_credito_id,
                    tb_cuota_id,
                    tb_cuota_est,
                    tb_cuota_fec,
                    tb_cuota_int,
                    tb_cuota_cuo,
                    (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
                FROM tb_ingreso ing 
                INNER JOIN tb_cuotapago pag ON pag.tb_cuotapago_id = ing.tb_ingreso_modide
                INNER JOIN tb_cuota cuo ON cuo.tb_cuota_id = pag.tb_cuotapago_modid 
                INNER JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 1)
                WHERE 
                    tb_ingreso_xac = 1 
                    AND pag.tb_modulo_id = 1 
                    AND ing.tb_modulo_id = 30 
                    AND tb_cuotapago_xac = 1 
                    AND tb_ingreso_ap = 0 
                    AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_cuota_fec < :fec1;
            ";
      }

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
      $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function liquidacion_total_tipo_credito($subcue_id, $fecha1, $fecha2)
  {
    try {
      $sql = "SELECT tb_ingreso_id, tb_ingreso_imp, tb_moneda_id, tb_cuenta_id, tb_subcuenta_id, tb_monedacambio_fec, tb_monedacambio_val,tb_cliente_id,tb_ingreso_fec  FROM tb_ingreso ing
			LEFT JOIN tb_monedacambio cam ON cam.tb_monedacambio_fec = ing.tb_ingreso_fec
			WHERE tb_ingreso_xac = 1 and tb_subcuenta_id =:subcue_id AND tb_ingreso_fec BETWEEN :fec1 AND :fec2";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->bindParam(":subcue_id", $subcue_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function amortizacion_total_tipo_credito($subcue_id, $fecha1, $fecha2)
  {
    try {
      $sql = "SELECT tb_ingreso_id, tb_ingreso_imp, tb_moneda_id, tb_cuenta_id, tb_subcuenta_id, tb_monedacambio_fec, tb_monedacambio_val,tb_cliente_id,tb_ingreso_fec FROM tb_ingreso ing
			LEFT JOIN tb_monedacambio cam ON cam.tb_monedacambio_fec = ing.tb_ingreso_fec
			WHERE tb_ingreso_xac = 1 and tb_subcuenta_id = :subcue_id AND tb_ingreso_fec BETWEEN :fec1 AND :fec2";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->bindParam(":subcue_id", $subcue_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function suma_ingresos_cuenta($cuenta_id, $fecha1, $fecha2, $moneda_id)
  {
    try {
      if ($moneda_id == 1)
        $sql = "SELECT SUM(tb_ingreso_imp) AS ingreso_total 
                            FROM 
                                tb_ingreso
                            WHERE 
                            tb_cuenta_id = :tb_cuenta_id AND tb_ingreso_xac = 1 AND tb_ingreso_fec BETWEEN :fec1 and :fec2 
                            AND tb_moneda_id = :tb_moneda_id AND tb_caja_id IN(1,14) and tb_modulo_id = 30 and tb_ingreso_ap = 0";
      else
        $sql = "SELECT SUM(tb_ingreso_imp * tb_cuotapago_tipcam) AS ingreso_total 
                                FROM 
                                    tb_ingreso ing INNER JOIN tb_cuotapago cu ON cu.tb_cuotapago_id = ing.tb_ingreso_modide 
                                WHERE 
                                    tb_cuenta_id = :tb_cuenta_id AND tb_ingreso_xac = 1 AND tb_ingreso_fec BETWEEN :fec1 and :fec2 
                                    AND ing.tb_moneda_id = 2 AND tb_caja_id IN(1,14) and ing.tb_modulo_id = 30 and tb_ingreso_ap = 0";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_cuenta_id", $cuenta_id, PDO::PARAM_INT);
      if ($moneda_id == 1)
        $sentencia->bindParam(":tb_moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_caja_retencion_motivo($mod_id, $modide, $mon_id, $caj_id, $ingreso_mot)
  {
    try {
      $sql = "SELECT * FROM tb_ingreso 
                    WHERE 
                    tb_ingreso_xac=1 AND 
                    tb_modulo_id=:modulo_id AND 
                    tb_ingreso_modide=:modide and 
                    tb_moneda_id =:moneda_id and 
                    tb_caja_id =:caja_id and 
                    tb_ingreso_mot =:mot";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":modulo_id", $mod_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modide", $modide, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
      $sentencia->bindParam(":caja_id", $caj_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mot", $ingreso_mot, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function ingresos_caja_cliente($caja_id, $mon_id, $cli_id, $modulo_id)
  {
    try {
      $sql = "SELECT * FROM tb_ingreso where tb_modulo_id =:modulo_id and tb_caja_id =:caja_id and tb_moneda_id =:mon_id and tb_cliente_id =:cli_id and tb_ingreso_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_por_modulo($mod_id, $modide, $mon_id, $est)
  {
    $concatenar = "";
    $concatenar1 = "";
    if (intval($mon_id) > 0) {
      $concatenar = " AND i.tb_moneda_id =:mon_id";
    }
    if (intval($est) > 0) {
      $concatenar1 = " AND tb_ingreso_est IN (:est)";
    }

    try {
      $sql = "SELECT * 
                    FROM tb_ingreso i
                    INNER JOIN tb_cuenta c ON i.tb_cuenta_id = c.tb_cuenta_id
                    INNER JOIN tb_caja cj ON i.tb_caja_id = cj.tb_caja_id
                    INNER JOIN tb_cliente cl ON i.tb_cliente_id = cl.tb_cliente_id
                    INNER JOIN tb_documento d ON i.tb_documento_id=d.tb_documento_id
                    LEFT JOIN tb_subcuenta sc ON i.tb_subcuenta_id = sc.tb_subcuenta_id
                    WHERE tb_ingreso_xac=1
                    AND tb_modulo_id=:mod_id
                    AND tb_ingreso_modide=:modide " . $concatenar . $concatenar1;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modide", $modide, PDO::PARAM_INT);
      if (intval($mon_id) > 0) {
        $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
      }
      if (intval($est) > 0) {
        $sentencia->bindParam(":est", $est, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_por_modulo_cajaid($mod_id, $modide, $mon_id, $est, $caj_id)
  {
    $concatenar = "";
    $concatenar1 = "";
    $concatenar2 = "";
    try {
      if (intval($mon_id) > 0) {
        $concatenar = " AND i.tb_moneda_id=:moneda_id";
      }
      if ($est != '') {
        $concatenar1 = " AND tb_ingreso_est IN (:est)";
      }
      if (intval($caj_id) > 0) {
        $concatenar2 = " AND cj.tb_caja_id =:caja_id";
      }

      $sql = "SELECT * 
                FROM tb_ingreso i
                INNER JOIN tb_cuenta c ON i.tb_cuenta_id = c.tb_cuenta_id
                INNER JOIN tb_caja cj ON i.tb_caja_id = cj.tb_caja_id
                INNER JOIN tb_cliente cl ON i.tb_cliente_id = cl.tb_cliente_id
                INNER JOIN tb_documento d ON i.tb_documento_id=d.tb_documento_id
                LEFT JOIN tb_subcuenta sc ON i.tb_subcuenta_id = sc.tb_subcuenta_id
                WHERE tb_ingreso_xac = 1
                AND tb_modulo_id = :mod_id
                AND tb_ingreso_modide = :modide" . $concatenar . $concatenar1 . $concatenar2;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":mod_id", $mod_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modide", $modide, PDO::PARAM_INT);

      if (intval($mon_id) > 0) {
        $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
      }
      if ($est != '') {
        $sentencia->bindParam(":est", $est, PDO::PARAM_INT);
      }
      if (intval($caj_id) > 0) {
        $sentencia->bindParam(":caja_id", $caj_id, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos por modulo cajaid";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //mostrar el importe total de una mora de una cuota o subcuota
  function ingreso_total_por_credito_cuotadetalle($cre_id, $cre_tip, $mon_id)
  {
    try {
      $sql = "SELECT SUM(ing.tb_ingreso_imp) as importe_total FROM tb_ingreso ing 
                        INNER JOIN tb_cuotapago cp on ing.tb_ingreso_modide = cp.tb_cuotapago_id
                        INNER JOIN tb_cuotadetalle cud on cud.tb_cuotadetalle_id = cp.tb_cuotapago_modid 
                        INNER JOIN tb_cuota cu on cu.tb_cuota_id = cud.tb_cuota_id 
                        where cu.tb_credito_id =:cre_id 
                        and cu.tb_creditotipo_id =:cre_tip 
                        and cp.tb_modulo_id=2 
                        and ing.tb_modulo_id = 30 
                        and tb_ingreso_xac = 1 
                        and tb_cuotapago_xac = 1 
                        and ing.tb_moneda_id =:mon_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function cambio_de_cliente($cli_id, $cli_id_nuevo)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET tb_cliente_id =:cli_id_nuevo WHERE tb_cliente_id =:cli_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cli_id_nuevo", $cli_id_nuevo, PDO::PARAM_INT);
      $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }


  function ingresos_amor_liq_cliente_credito($cli_id, $cre_id, $cre_tip, $anio, $mes)
  {
    try {
      $sql = "SELECT 
                      IFNULL(SUM(tb_ingreso_imp),0) as importe, tb_moneda_id, tb_cuenta_id, IFNULL(SUM(tb_ingreso_pro),0) as prorrateo, tb_ingreso_fec,tb_ingreso_numope,tb_cuentadeposito_id
                  FROM 
                      tb_ingreso 
                  WHERE 
                      tb_ingreso_xac = 1 and tb_cliente_id =:cliente_id and tb_ingreso_modide =:modide and tb_modulo_id =:modulo_id and tb_cuenta_id in(35,36) 
                      and YEAR(tb_ingreso_fec) =:anio and MONTH(tb_ingreso_fec) =:mes 
                      GROUP BY tb_ingreso_modide";


      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modide", $cre_id, PDO::PARAM_INT);
      $sentencia->bindParam(":modulo_id", $cre_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function total_ingresos_cuenta_subcuenta_usuario($ingreso_fec1, $ingreso_fec2, $usuario_id, $empresa_id, $moneda_id, $cuenta_id, $subcuenta_id)
  {
    try {
      $fecha = '';
      $usuario = '';
      if (!empty($ingreso_fec1) && !empty($ingreso_fec2)) {
        $fecha = ' AND tb_ingreso_fec BETWEEN :ingreso_fec1 AND :ingreso_fec2';
      }
      if (intval($usuario_id) > 0)
        $usuario = ' AND tb_ingreso_usureg =' . intval($usuario_id);

      if (intval($cuenta_id) > 0)
        $cuenta = ' AND tb_cuenta_id =' . intval($cuenta_id);

      if (!empty($subcuenta_id))
        $subcuenta = ' AND tb_subcuenta_id IN (' . $subcuenta_id . ')';

      $sql = "SELECT IFNULL(SUM(tb_ingreso_imp),0) AS total_ingresos FROM tb_ingreso 
        WHERE tb_ingreso_xac = 1 AND tb_empresa_id =:empresa_id AND tb_moneda_id =:tb_moneda_id " . $fecha . '' . $usuario . ' ' . $cuenta . ' ' . $subcuenta;

      $sentencia = $this->dblink->prepare($sql);
      if (!empty($ingreso_fec1) && !empty($ingreso_fec2)) {
        $sentencia->bindParam(":ingreso_fec1", $ingreso_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":ingreso_fec2", $ingreso_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_moneda_id", $moneda_id, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Ingresos registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function ultima_fecha_que_pago_cliente($credito_id, $creditotipo_id)
  {
    try {
      /*$sql = "SELECT tb_ingreso_fec AS ultima_fecha, tb_ingreso_imp FROM tb_ingreso 
        WHERE tb_ingreso_xac = 1  AND tb_modulo_id = 30 AND tb_cliente_id =:cliente_id
        ORDER BY tb_ingreso_fec DESC LIMIT 1";*/

      // $sql = "SELECT tb_cuotapago_fec AS ultima_fecha, tb_cuotapago_mon FROM tb_cuotapago cp
      // INNER JOIN tb_cuotadetalle cud on cud.tb_cuotadetalle_id = cp.tb_cuotapago_modid 
      // INNER JOIN tb_cuota cu on cu.tb_cuota_id = cud.tb_cuota_id 
      // where cu.tb_credito_id =:credito_id 
      // and cu.tb_creditotipo_id =:creditotipo_id
      // and cp.tb_modulo_id = 2 
      // and tb_cuotapago_xac = 1 
      // ORDER BY tb_cuotapago_fec DESC
      // limit 1";

      $sql = "SELECT tb_cuotapago_fec AS ultima_fecha, tb_cuotapago_mon FROM tb_cuotapago cp
        INNER JOIN tb_cuotadetalle cud on (cud.tb_cuotadetalle_id = cp.tb_cuotapago_modid and cp.tb_modulo_id = 2)
        INNER JOIN tb_cuota cu on cu.tb_cuota_id = cud.tb_cuota_id 
        where cu.tb_credito_id =:credito_id 
        and cu.tb_creditotipo_id =:creditotipo_id 
        and tb_cuotapago_xac = 1 
        and tb_cuotadetalle_est in(2,3)
        and tb_cuotapago_mon != 0
        ORDER BY tb_cuotapago_id DESC
        limit 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function ultima_fecha_cliente_realizo_pago_version2($cliente_id, $credito_id, $creditotipo_id)
  {
    try {
      $filtro = "%SIN FILTRO%";
      $credito_numero = str_pad($credito_id, 4, "0", STR_PAD_LEFT);

      if (intval($creditotipo_id) == 2)
        $filtro = "%CAV-" . $credito_numero . "%";
      if (intval($creditotipo_id) == 3)
        $filtro = "%CGV-" . $credito_numero . "%";
      if (intval($creditotipo_id) == 4)
        $filtro = "%CH-" . $credito_numero . "%";

      $sql = "SELECT tb_ingreso_id, tb_ingreso_fec AS ultima_fecha FROM tb_ingreso 
            WHERE tb_ingreso_xac = 1 AND tb_modulo_id = 30 AND tb_cliente_id =:cliente_id 
            AND tb_ingreso_det LIKE :filtro 
            ORDER BY tb_ingreso_fec DESC LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  /* GERSON (28-04-23) */
  function existeApertura($fecha, $cuenta, $subcuenta, $sede_id)
  {
    try {

      $sql = "SELECT * 
                FROM tb_ingreso
                WHERE tb_ingreso_xac = 1
                AND tb_ingreso_fec = :fecha
                AND tb_cuenta_id = :cuenta
                AND tb_subcuenta_id = :subcuenta
                AND tb_empresa_id = :sede_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
      $sentencia->bindParam(":cuenta", $cuenta, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta", $subcuenta, PDO::PARAM_INT);
      $sentencia->bindParam(":sede_id", $sede_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  function eliminar_2()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET tb_ingreso_xac=0 WHERE tb_ingreso_id=:tb_ingreso_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_ingreso_id", $this->ingreso_id, PDO::PARAM_INT);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $this->dblink->commit();
        return 1; //si es correcto retorna 1
      } else {
        return 0;
      }
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  // ANTONIO 01-09-2023
  function consulta_ingresos_ventainterna_pago_caja($ventainterna_id)
  {
    try {

      $sql = "SELECT * FROM tb_ingreso WHERE tb_ingreso_xac = 1 and tb_subcuenta_id = 181 and tb_ventainterna_id =:ventainterna_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ventainterna_id", $ventainterna_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  // GERSON 31-10-23
  function consulta_ingresos_ventainterna_pago_banco($ventainterna_id)
  {
    try {

      $sql = "SELECT * FROM tb_ingreso WHERE tb_ingreso_xac = 1 and tb_cuenta_id = 43 and tb_ventainterna_id =:ventainterna_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ventainterna_id", $ventainterna_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  //

  function CancelarPago($tb_ingreso_modide)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_ingreso SET tb_ingreso_xac = 0 WHERE tb_ingreso_modide =:ingreso_modide";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":ingreso_modide", $tb_ingreso_modide, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function ingresos_credito_id_rango($fec1, $fec2, $tabla, $credito_id, $cre_tip)
  {
    try {
      if (intval($cre_tip) != 1) {
        $sql = "
                    SELECT 
                    tb_ingreso_id, 
                    tb_ingreso_fec, 
                    SUM(tb_ingreso_imp) as tb_ingreso_imp, 
                    ing.tb_moneda_id, 
                    pag.tb_cuotapago_tipcam, 
                    tb_ingreso_det,
                    ing.tb_cliente_id,
                    cre.tb_credito_id,
                    tb_cuotadetalle_fec,
                    tb_cuotadetalle_cuo,
                    cuo.tb_cuota_id,
                    tb_cuota_est,
                    tb_cuota_fec,
                    tb_cuota_int,
                    tb_cuota_cuo,
                    (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) as cuota_real
                FROM tb_ingreso ing 
                INNER JOIN tb_cuotapago pag ON pag.tb_cuotapago_id = ing.tb_ingreso_modide 
                INNER JOIN tb_cuotadetalle det ON det.tb_cuotadetalle_id = pag.tb_cuotapago_modid
                INNER JOIN tb_cuota cuo ON cuo.tb_cuota_id = det.tb_cuota_id 
                INNER JOIN " . $tabla . " cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = " . $cre_tip . ")
                WHERE 
                    tb_ingreso_xac = 1 
                    AND pag.tb_modulo_id = 2 
                    AND ing.tb_modulo_id = 30 
                    AND tb_cuotapago_xac = 1 
                    AND tb_ingreso_ap = 0 
                    AND cre.tb_credito_id =:credito_id
                    AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_cuotadetalle_fec >= :fec1 
                    GROUP BY tb_ingreso_fec;";
      } else {
        $sql = "
                    SELECT 
                        tb_ingreso_id, 
                        tb_ingreso_fec, 
                        SUM(tb_ingreso_imp), 
                        ing.tb_moneda_id, 
                        pag.tb_cuotapago_tipcam, 
                        tb_ingreso_det,
                        ing.tb_cliente_id,
                        cre.tb_credito_id,
                        tb_cuota_id,
                        tb_cuota_est,
                        tb_cuota_fec,
                        tb_cuota_int,
                        tb_cuota_cuo,
                        (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
                    FROM tb_ingreso ing 
                    INNER JOIN tb_cuotapago pag ON pag.tb_cuotapago_id = ing.tb_ingreso_modide
                    INNER JOIN tb_cuota cuo ON cuo.tb_cuota_id = pag.tb_cuotapago_modid 
                    INNER JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id =:creditotipo_id)
                    WHERE 
                        tb_ingreso_xac = 1 
                        AND pag.tb_modulo_id = 1 
                        AND ing.tb_modulo_id = 30 
                        AND tb_cuotapago_xac = 1 
                        AND tb_ingreso_ap = 0 
                        AND cre.tb_credito_id =:credito_id
                        AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_cuota_fec >= :fec1 GROUP BY tb_ingreso_fec;
                ";
      }

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
      $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }


  function ingreso_total_por_credito_cuota($cre_id, $mon_id)
  {
    try {
      $sql = "SELECT SUM(ing.tb_ingreso_imp) as importe_total FROM tb_ingreso ing 
                        INNER JOIN tb_cuotapago cp on ing.tb_ingreso_modide = cp.tb_cuotapago_id
                        INNER JOIN tb_cuota cu on cu.tb_cuota_id = cp.tb_cuotapago_modid
                        where cu.tb_credito_id =:cre_id 
                        and cu.tb_creditotipo_id = 1 
                        and cp.tb_modulo_id=2 
                        and ing.tb_modulo_id = 30 
                        and tb_ingreso_xac = 1 
                        and tb_cuotapago_xac = 1 
                        and ing.tb_moneda_id =:mon_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function existeApertura_moneda($fecha, $cuenta, $subcuenta, $sede_id, $moneda_id)
  {
    try {

      $sql = "SELECT * 
                FROM tb_ingreso
                WHERE tb_ingreso_xac = 1
                AND tb_ingreso_fec = :fecha
                AND tb_cuenta_id = :cuenta
                AND tb_subcuenta_id = :subcuenta
                AND tb_empresa_id = :sede_id
                AND tb_moneda_id = :moneda_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
      $sentencia->bindParam(":cuenta", $cuenta, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta", $subcuenta, PDO::PARAM_INT);
      $sentencia->bindParam(":sede_id", $sede_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function ingreso_por_cuenta_subcuenta($cuenta_id, $subcuenta_id, $fecha, $empresa_id, $moneda_id){
    try {
      $sql = "SELECT * FROM tb_ingreso 
                    WHERE 
                    tb_ingreso_xac = 1 AND 
                    tb_cuenta_id = :cuenta_id AND 
                    tb_subcuenta_id = :subcuenta_id and 
                    tb_ingreso_fec = :fecha AND
                    tb_empresa_id = :empresa_id AND
								    tb_moneda_id = :moneda_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subcuenta_id", $subcuenta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
      $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //mostrar el importe total de una mora de una cuota o subcuota
  function ingreso_total_por_credito_cuotadetalle_fecha($cre_id, $cre_tip, $mon_id, $fecha_inicio, $fecha_fin)
  {
    try {
      $sql = "SELECT SUM(
                CASE 
                    WHEN ing.tb_moneda_id = 1 THEN ing.tb_ingreso_imp
                    WHEN ing.tb_moneda_id = 2 THEN ing.tb_ingreso_imp * IF(ing.tb_ingreso_tipcam = 0.000, mc.tb_monedacambio_val, ing.tb_ingreso_tipcam)
                    ELSE 0
                END) AS importe_total
              FROM tb_ingreso ing 
              INNER JOIN tb_cuotapago cp ON ing.tb_ingreso_modide = cp.tb_cuotapago_id
              INNER JOIN tb_cuotadetalle cud ON cud.tb_cuotadetalle_id = cp.tb_cuotapago_modid
              INNER JOIN tb_cuota cu ON cu.tb_cuota_id = cud.tb_cuota_id
              INNER JOIN tb_monedacambio mc ON ing.tb_ingreso_fec = mc.tb_monedacambio_fec
              where cu.tb_credito_id =:cre_id 
              and cu.tb_creditotipo_id =:cre_tip 
              and cp.tb_modulo_id=2 
              and ing.tb_modulo_id = 30 
              and tb_ingreso_xac = 1 
              and tb_cuotapago_xac = 1 
              and ing.tb_moneda_id =:mon_id
              AND ing.tb_ingreso_fec > :fecha_inicio
              AND ing.tb_ingreso_fec < :fecha_fin";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
