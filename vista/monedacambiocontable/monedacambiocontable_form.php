<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../monedacambiocontable/Monedacambiocontable.class.php');
  $oMonedacambiocontable = new Monedacambiocontable();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'monedacambiocontable';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $monedacambiocontable_id = $_POST['monedacambiocontable_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Tipo de Cambio Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Tipo de Cambio';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Tipo de Cambio';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Tipo de Cambio';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en monedacambiocontable
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'monedacambiocontable'; $modulo_id = $monedacambiocontable_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //fecha actual
    $monedacambiocontable_fec = $vista == 'compracontadoc' ? mostrar_fecha($_POST['fecha']) : date('d-m-Y');

    //si la accion es modificar, mostramos los datos del monedacambiocontable por su ID
    if(intval($monedacambiocontable_id) > 0){
      $result = $oMonedacambiocontable->mostrarUno('tb_monedacambiocontable_id', $monedacambiocontable_id, 'INT');
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el monedacambiocontable seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $monedacambiocontable_fec = mostrar_fecha($result['data']['tb_monedacambiocontable_fecreg']);
          $monedacambiocontable_ventasunat = $result['data']['tb_monedacambiocontable_ventasunat'];
          $monedacambiocontable_comprasunat = $result['data']['tb_monedacambiocontable_comprasunat'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_monedacambiocontable" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_monedacambiocontable" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_monedacambiocontable_id" value="<?php echo $monedacambiocontable_id;?>">
          <input type="hidden" id="monedacambiocontable_vista" value="<?php echo $vista;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_monedacambiocontable_fec" class="control-label">Fecha TC</label>
              <div class='input-group date'>
                <input type='text' class="form-control input-sm" name="monedacambiocontable_picker" id="monedacambiocontable_picker" value="<?php echo $monedacambiocontable_fec;?>"/>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="txt_monedacambiocontable_comprasunat" class="control-label">TC Compra Sunat</label>
              <input type="text" name="txt_monedacambiocontable_comprasunat" id="txt_monedacambiocontable_comprasunat" class="form-control input-sm" value="<?php echo $monedacambiocontable_comprasunat;?>">
            </div>
            <div class="form-group">
              <label for="txt_monedacambiocontable_ventasunat" class="control-label">TC Venta Sunat</label>
              <input type="text" name="txt_monedacambiocontable_ventasunat" id="txt_monedacambiocontable_ventasunat" class="form-control input-sm" value="<?php echo $monedacambiocontable_ventasunat;?>">
            </div>
            
            <?php if($action != 'eliminar'):?>
              <label for="frame_sunat" class="control-label">Tipo de Cambio SUNAT</label>
              <iframe id="frame_sunat" name="frame_sunat" src="http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias" width="100%" height="290" ></iframe>
            <?php endif;?>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Tipo de Cambio?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="monedacambiocontable_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_monedacambiocontable">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_monedacambiocontable">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_monedacambiocontable">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/monedacambiocontable/monedacambiocontable_form.js';?>"></script>
