var datatable_global;
function monedacambiocontable_form(usuario_act, monedacambiocontable_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambiocontable/monedacambiocontable_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            monedacambiocontable_id: monedacambiocontable_id,
            vista: 'monedacambiocontable'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_monedacambiocontable_form').html(data);
                $('#modal_registro_monedacambiocontable').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_monedacambiocontable'); //funcion encontrada en public/js/generales.js

                modal_hidden_bs_modal('modal_registro_monedacambiocontable', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_height_auto('modal_registro_monedacambiocontable'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_monedacambiocontable', 40);
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'monedacambiocontable';
                var div = 'div_modal_monedacambiocontable_form';
                permiso_solicitud(usuario_act, monedacambiocontable_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function monedacambiocontable_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambiocontable/monedacambiocontable_tabla.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#monedacambiocontable_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_monedacambiocontable_tabla').html(data);
            $('#monedacambiocontable_mensaje_tbl').hide(300);
            estilos_datatable();
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#monedacambiocontable_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}
function estilos_datatable() {
    datatable_global = $('#tbl_monedacambiocontables').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [2, 3, 4], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}
$(document).ready(function () {
    monedacambiocontable_tabla()
    estilos_datatable();
});

function monedacambiocontable_historial_form(monedacambiocontable_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambiocontable/monedacambiocontable_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
                monedacambiocontable_id:monedacambiocontable_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_monedacambiocontable_historial_form').html(html);
            $('#div_modal_monedacambiocontable_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_monedacambiocontable_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_monedacambiocontable_historial_form'); //funcion encontrada en public/js/generales.js

        },
        complete: function (html) {
//                    console.log(html);
        }
    });
}