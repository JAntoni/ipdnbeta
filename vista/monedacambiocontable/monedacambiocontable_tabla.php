<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
if (defined('VISTA_URL')) {
    require_once(VISTA_URL . 'monedacambiocontable/Monedacambiocontable.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../monedacambiocontable/Monedacambiocontable.class.php');
    require_once('../funciones/fechas.php');
}

$oMonedacambiocontable = new Monedacambiocontable();

$result = $oMonedacambiocontable->listar_todos(1); //LOS ACTIVOS

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $tr .= '<tr id="tabla_cabecera_fila">';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_monedacambiocontable_id'] . '</td>
          <td id="tabla_fila">' . mostrar_fecha($value['tb_monedacambiocontable_fecha']) . '</td>
          <td id="tabla_fila">' . $value['tb_monedacambiocontable_comprasunat'] . '</td>
          <td id="tabla_fila">' . $value['tb_monedacambiocontable_ventasunat'] . '</td>
          <td id="tabla_fila">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="monedacambiocontable_form(\'M\',' . $value['tb_monedacambiocontable_id'] . ')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="monedacambiocontable_form(\'E\',' . $value['tb_monedacambiocontable_id'] . ')"><i class="fa fa-trash"></i> Eliminar</a>
            <a class="btn btn-success btn-xs" title="Historial" onclick="monedacambiocontable_historial_form(' . $value['tb_monedacambiocontable_id'] . ')"><i class="fa fa-eye"></i> Historial</a>
          </td>
        ';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
?>
<table id="tbl_monedacambiocontables" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">Fecha</th>
            <th id="tabla_cabecera_fila">TC Compra Sunat</th>
            <th id="tabla_cabecera_fila">TC Venta Sunat</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
<?php echo $tr; ?>
    </tbody>
</table>
