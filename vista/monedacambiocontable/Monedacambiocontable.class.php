<?php

if (defined('APP_URL')) {
    require_once (APP_URL . 'datos/conexion.php');
} else {
    require_once ('../../datos/conexion.php');
}

class Monedacambiocontable extends Conexion {
    private $tb_monedacambiocontable_id;
    private $tb_monedacambiocontable_fecreg;
    private $tb_monedacambiocontable_fecmod;
    private $tb_monedacambiocontable_usureg;
    private $tb_monedacambiocontable_usumod;
    private $tb_monedacambiocontable_fecha;
    private $tb_monedacambiocontable_comprasunat;
    private $tb_monedacambiocontable_ventasunat;
    private $tb_monedacambiocontable_xac;

    /**
     * Get the value of tb_monedacambiocontable_id
     */
    public function getTbMonedacambiocontableId()
    {
        return $this->tb_monedacambiocontable_id;
    }

    /**
     * Set the value of tb_monedacambiocontable_id
     */
    public function setTbMonedacambiocontableId($tb_monedacambiocontable_id)
    {
        $this->tb_monedacambiocontable_id = $tb_monedacambiocontable_id;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_fecreg
     */
    public function getTbMonedacambiocontableFecreg()
    {
        return $this->tb_monedacambiocontable_fecreg;
    }

    /**
     * Set the value of tb_monedacambiocontable_fecreg
     */
    public function setTbMonedacambiocontableFecreg($tb_monedacambiocontable_fecreg)
    {
        $this->tb_monedacambiocontable_fecreg = $tb_monedacambiocontable_fecreg;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_fecmod
     */
    public function getTbMonedacambiocontableFecmod()
    {
        return $this->tb_monedacambiocontable_fecmod;
    }

    /**
     * Set the value of tb_monedacambiocontable_fecmod
     */
    public function setTbMonedacambiocontableFecmod($tb_monedacambiocontable_fecmod)
    {
        $this->tb_monedacambiocontable_fecmod = $tb_monedacambiocontable_fecmod;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_usureg
     */
    public function getTbMonedacambiocontableUsureg()
    {
        return $this->tb_monedacambiocontable_usureg;
    }

    /**
     * Set the value of tb_monedacambiocontable_usureg
     */
    public function setTbMonedacambiocontableUsureg($tb_monedacambiocontable_usureg)
    {
        $this->tb_monedacambiocontable_usureg = $tb_monedacambiocontable_usureg;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_usumod
     */
    public function getTbMonedacambiocontableUsumod()
    {
        return $this->tb_monedacambiocontable_usumod;
    }

    /**
     * Set the value of tb_monedacambiocontable_usumod
     */
    public function setTbMonedacambiocontableUsumod($tb_monedacambiocontable_usumod)
    {
        $this->tb_monedacambiocontable_usumod = $tb_monedacambiocontable_usumod;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_comprasunat
     */
    public function getTbMonedacambiocontableComprasunat()
    {
        return $this->tb_monedacambiocontable_comprasunat;
    }

    /**
     * Set the value of tb_monedacambiocontable_comprasunat
     */
    public function setTbMonedacambiocontableComprasunat($tb_monedacambiocontable_comprasunat)
    {
        $this->tb_monedacambiocontable_comprasunat = $tb_monedacambiocontable_comprasunat;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_ventasunat
     */
    public function getTbMonedacambiocontableVentasunat()
    {
        return $this->tb_monedacambiocontable_ventasunat;
    }

    /**
     * Set the value of tb_monedacambiocontable_ventasunat
     */
    public function setTbMonedacambiocontableVentasunat($tb_monedacambiocontable_ventasunat)
    {
        $this->tb_monedacambiocontable_ventasunat = $tb_monedacambiocontable_ventasunat;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_xac
     */
    public function getTbMonedacambiocontableXac()
    {
        return $this->tb_monedacambiocontable_xac;
    }

    /**
     * Set the value of tb_monedacambiocontable_xac
     */
    public function setTbMonedacambiocontableXac($tb_monedacambiocontable_xac)
    {
        $this->tb_monedacambiocontable_xac = $tb_monedacambiocontable_xac;

        return $this;
    }

    /**
     * Get the value of tb_monedacambiocontable_fecha
     */
    public function getTbMonedacambiocontableFecha()
    {
        return $this->tb_monedacambiocontable_fecha;
    }

    /**
     * Set the value of tb_monedacambiocontable_fecha
     */
    public function setTbMonedacambiocontableFecha($tb_monedacambiocontable_fecha)
    {
        $this->tb_monedacambiocontable_fecha = $tb_monedacambiocontable_fecha;

        return $this;
    }

    public function listar_todos($xac){ //si no se envia parametro xac devuelve todos, de lo contrario añade clause WHERE
        try {
            $val = [0, 1]; // valores de xac permitidos
            $sql = "SELECT
                        mcc.*, u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot
                    FROM
                        tb_monedacambiocontable mcc
                    INNER JOIN tb_usuario u ON mcc.tb_monedacambiocontable_usureg = u.tb_usuario_id";
            if(in_array($xac, $val)){
                $sql .= " WHERE tb_monedacambiocontable_xac = :tb_monedacambiocontable_xac ";
            }
                     
            $sql .= " ORDER BY tb_monedacambiocontable_fecha DESC;";
            
            $sentencia = $this->dblink->prepare($sql);
            if(in_array($xac, $val)){
                $sentencia->bindParam(':tb_monedacambiocontable_xac', $xac, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipo de cambio contable registrado";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    function mostrarUno($column_name, $param, $datatype) {
        try {
            if(stripos($column_name, 'fec') !== FALSE){ //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato.
                $column_name = "DATE_FORMAT(".$column_name.", '%Y-%m-%d')";
            }
            
            $sql = "SELECT
                        mcc.*, u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot
                    FROM
                        tb_monedacambiocontable mcc
                    INNER JOIN tb_usuario u ON mcc.tb_monedacambiocontable_usureg = u.tb_usuario_id
                    WHERE " . $column_name . " = :param AND tb_monedacambiocontable_xac = 1";
            
            $_PARAM = strtoupper($datatype)=='INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param", $param, $_PARAM);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No existe este registro";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function eliminar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_monedacambiocontable SET tb_monedacambiocontable_xac = 0 WHERE tb_monedacambiocontable_id = :tb_monedacambiocontable_id ";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_monedacambiocontable_id", $this->tb_monedacambiocontable_id, PDO::PARAM_INT);
            $resultado = $sentencia->execute(); //retorna 1 si es correcto

            if($resultado == 1){
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_monedacambiocontable
                    SET
                        tb_monedacambiocontable_usumod = :param_1 ,
                        tb_monedacambiocontable_fecha = :param_2,
                        tb_monedacambiocontable_comprasunat = :param_3,
                        tb_monedacambiocontable_ventasunat = :param_4
                    WHERE tb_monedacambiocontable_id = :param_5 ;";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param_1', $this->tb_monedacambiocontable_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':param_2', $this->tb_monedacambiocontable_fecha, PDO::PARAM_STR);
            $sentencia->bindParam(':param_3', $this->tb_monedacambiocontable_comprasunat, PDO::PARAM_STR);
            $sentencia->bindParam(':param_4', $this->tb_monedacambiocontable_ventasunat, PDO::PARAM_STR);
            $sentencia->bindParam(':param_5', $this->tb_monedacambiocontable_id, PDO::PARAM_INT);
            $resultado = $sentencia->execute();

            if($resultado == 1){
                $this->dblink->commit();
            }

            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
    
    function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_monedacambiocontable (
                        tb_monedacambiocontable_usureg,
                        tb_monedacambiocontable_usumod,
                        tb_monedacambiocontable_fecha,
                        tb_monedacambiocontable_ventasunat,
                        tb_monedacambiocontable_comprasunat
                    )
                    VALUES(
                        :param_1,
                        :param_2,
                        :param_3,
                        :param_4,
                        :param_5
                    )";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param_1', $this->tb_monedacambiocontable_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param_2', $this->tb_monedacambiocontable_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':param_3', $this->tb_monedacambiocontable_fecha, PDO::PARAM_STR);
            $sentencia->bindParam(':param_4', $this->tb_monedacambiocontable_ventasunat, PDO::PARAM_STR);
            $sentencia->bindParam(':param_5', $this->tb_monedacambiocontable_comprasunat, PDO::PARAM_STR);
            
            $resultado = $sentencia->execute();
            $new_id = $this->dblink->lastInsertId();

            if($resultado == 1){
                $this->dblink->commit();
                $return['new_id'] = $new_id;
                $return['state'] = 1;
                $return['message'] = 'Registrado correctamente';
            }
            else{
                $return['new_id'] = false;
                $return['state'] = 0;
                $return['message'] = 'No se registró';
            }
            return $return;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
    
}
//ALT + FLECHA ARRIBA / ABAJO :  para mover la linea de codigo || SHIFT + SUPR : para eliminar la linea actual
//CTRL + L : para seleccionar la linea
// div#MyID.myClass: abreviatura emmet, para escribir un div con # para id="MyID" - con . para class="myClass"
?>