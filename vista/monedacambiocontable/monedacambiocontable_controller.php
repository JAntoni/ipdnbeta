<?php
require_once('../../core/usuario_sesion.php');
require_once('../monedacambiocontable/Monedacambiocontable.class.php');
$oMonedacambiocontable = new Monedacambiocontable();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../compracontadoc/Compracontadoc.class.php');
$oCompracontadoc = new Compracontadoc();

$action = $_POST['action'];
$monedacambiocontable_id = intval($_POST['hdd_monedacambiocontable_id']);
$monedacambiocontable_fec = fecha_mysql($_POST['monedacambiocontable_picker']);
$monedacambiocontable_ventasunat = floatval($_POST['txt_monedacambiocontable_ventasunat']);
$monedacambiocontable_comprasunat = floatval($_POST['txt_monedacambiocontable_comprasunat']);
$usuario_id = intval($_SESSION['usuario_id']);

$oMonedacambiocontable->setTbMonedacambiocontableId($monedacambiocontable_id);
$oMonedacambiocontable->setTbMonedacambiocontableFecha($monedacambiocontable_fec);
$oMonedacambiocontable->setTbMonedacambiocontableVentasunat($monedacambiocontable_ventasunat);
$oMonedacambiocontable->setTbMonedacambiocontableComprasunat($monedacambiocontable_comprasunat);
$oMonedacambiocontable->setTbMonedacambiocontableUsumod($usuario_id);

if ($action == 'insertar') {
    $oMonedacambiocontable->setTbMonedacambiocontableUsureg($usuario_id);
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Tipo de Cambio.';

    $result = $oMonedacambiocontable->mostrarUno('tb_monedacambiocontable_fecha', $monedacambiocontable_fec, 'STR');
    $existe = $result['estado'];

    if ($existe == 0) {
        $dat = $oMonedacambiocontable->insertar();
        if ($dat['state'] == 1) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Tipo de Cambio registrado correctamente.';
            $data['registro'] = $oMonedacambiocontable->mostrarUno('tb_monedacambiocontable_id',$dat['new_id'],'INT')['data'];

            $oHist->setTbHistUsureg($usuario_id);
            $oHist->setTbHistNomTabla('tb_monedacambiocontable');
            $oHist->setTbHistRegmodid($dat['new_id']);
            $oHist->setTbHistDet('Registró el tipo de cambio contable a fecha <b>'.$monedacambiocontable_fec.'</b>: <br>'.
                                    '- TC Venta: '.$monedacambiocontable_ventasunat.' || '.'- TC Compra: '.$monedacambiocontable_comprasunat);
            $oHist->insertar();
        }
    } else
        $data['mensaje'] = 'El Tipo de Cambio para la fecha ya está registrado, con id ' . $result["data"]["tb_monedacambiocontable_id"];

    $result = NULL;
    echo json_encode($data);
} 
elseif ($action == 'modificar') {
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar el Tipo de Cambio.';

    $result = $oMonedacambiocontable->mostrarUno('tb_monedacambiocontable_fecreg', $monedacambiocontable_fec, 'STR');
    $existe = $result['estado'];
    $registro = $result['data'];

    if($existe == 0 || ($existe == 1 && intval($registro['tb_monedacambiocontable_id']) == $monedacambiocontable_id)){
        if ($oMonedacambiocontable->modificar()) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Tipo de Cambio modificado correctamente.';
    
            $oHist->setTbHistUsureg($usuario_id);
            $oHist->setTbHistNomTabla('tb_monedacambiocontable');
            $oHist->setTbHistRegmodid($monedacambiocontable_id);
            $oHist->setTbHistDet('Editó el tipo de cambio contable a fecha <b>'.$monedacambiocontable_fec.'</b>: <br>'.
                                    '- TC Venta: '.$monedacambiocontable_ventasunat.' || '.'- TC Compra: '.$monedacambiocontable_comprasunat);
            $oHist->insertar();
        }
    }
    else{
        $data['mensaje'] = 'El Tipo de Cambio para la fecha ya está registrado, con id ' . $result["data"]["tb_monedacambiocontable_id"];
    }
    
    echo json_encode($data);
}

elseif ($action == 'eliminar') {
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el Tipo de Cambio.';

    //codigo para verificar si hay registro en la tabla compracontadoc

    if ($oMonedacambiocontable->eliminar($monedacambiocontable_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Tipo de Cambio eliminado correctamente.';

        $oHist->setTbHistUsureg($usuario_id);
        $oHist->setTbHistNomTabla('tb_monedacambiocontable');
        $oHist->setTbHistRegmodid($monedacambiocontable_id);
        $oHist->setTbHistDet('Eliminó el tipo de cambio contable registrado para fecha <b>'.$monedacambiocontable_fec.'</b>');
        $oHist->insertar();
    }

    echo json_encode($data);
}
elseif($action == 'tipocambio'){
    $monedacambio_fec = fecha_mysql($_POST['monedacambio_fec']);
    $moneda_id = intval($_POST['moneda_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'No hay registro de T.C. del día: ' . $_POST['monedacambio_fec'] . ', y moneda: US$.';

    if ($moneda_id == 1) {
        $data['estado'] = 1;
        $data['valor_compra'] = '1.000';
        $data['valor_venta'] = '1.000';
        $data['mensaje'] = 'Exito';
    }
    if ($moneda_id == 2) {
        $result = $oMonedacambiocontable->mostrarUno('tb_monedacambiocontable_fecha',$monedacambio_fec,'STR');
        if ($result['estado'] == 1) {
            $data['id'] = intval($result['data']['tb_monedacambiocontable_id']);
            $data['valor_compra'] = floatval($result['data']['tb_monedacambiocontable_comprasunat']);
            $data['valor_venta'] = floatval($result['data']['tb_monedacambiocontable_ventasunat']);
            $data['estado'] = 1;
            $data['mensaje'] = 'Exito';
        }
        $result = NULL;
    }
    $data['fecha'] = $monedacambio_fec;
    echo json_encode($data);
}
else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>