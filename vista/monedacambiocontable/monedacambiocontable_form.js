
$(document).ready(function () {

    $('#monedacambiocontable_picker').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
        endDate: new Date()
    });

    $('#txt_monedacambiocontable_ventasunat, #txt_monedacambiocontable_comprasunat').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.000',
        vMax: '99.000'
    });
    $('#form_monedacambiocontable').validate({
        submitHandler: function () {
            //console.log($("#form_monedacambiocontable").serialize());return;
            $.ajax({
                type: "POST",
                url: VISTA_URL + "monedacambiocontable/monedacambiocontable_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_monedacambiocontable").serialize(),
                beforeSend: function () {
                    $('#monedacambiocontable_mensaje').show(400);
                    $('#btn_guardar_monedacambiocontable').prop('disabled', true);
                },
                success: function (data) {
                    //console.log(data);return;
                    if (parseInt(data.estado) > 0) {
                        $('#monedacambiocontable_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#monedacambiocontable_mensaje').html(data.mensaje);
                        var vista = $('#monedacambiocontable_vista').val();
                        
                        setTimeout(function () {                        
                            if (vista == 'monedacambiocontable')
                                monedacambiocontable_tabla();
                            else if(vista == 'compracontadoc'){
                                llenar_datos_monedacambiocontable(data);
                            }
                            $('#modal_registro_monedacambiocontable').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#monedacambiocontable_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#monedacambiocontable_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_monedacambiocontable').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#monedacambiocontable_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#monedacambiocontable_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            monedacambiocontable_picker: {
                required: true
            },
            txt_monedacambiocontable_ventasunat: {
                required: true
            },
            txt_monedacambiocontable_comprasunat: {
                required: true
            }
        },
        messages: {
            monedacambiocontable_picker: {
                required: "Ingrese la fecha del Tipo de Cambio"
            },
            txt_monedacambiocontable_ventasunat: {
                required: "Ingrese Tipo de Cambio Venta"
            },
            txt_monedacambiocontable_comprasunat: {
                required: "Ingrese Tipo de Cambio Compra"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});
