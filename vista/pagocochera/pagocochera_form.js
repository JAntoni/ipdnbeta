function ListarHistorialPago() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pagocochera/pagocochera_tabla.php",
    async: true,
    dataType: "html",
    data: {
      fecha1:$('#txt_pagocochera_fecha1').val(),
      fecha2:$('#txt_pagocochera_fecha2').val(),
      placa:$('#txt_pagocochera_placa').val(),
      cochera:$('#cmb_pagocochera_cochera').val(),
    },
    beforeSend: function () {
      $("#gestioncochera_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_pagocochera_tabla").html(data);
      $("#gestioncochera_mensaje_tbl").hide(300);
      //Ejecutar los estilos de la data al llamar la funcion
      //estilos_datatable("tbl_pagoscochera");
    },
    complete: function (data) {},
    error: function (data) {
      console.log(data)
      $("#gestioncochera_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DEL MOVIMIENTO " + data.responseText
      );
    },
  });
}



$(document).ready(function () {
  //alert("ver");
  //Js para darle funcion a los input de fechas
  $("#datetimepicker3, #datetimepicker4").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });
  $("#datetimepicker3").on("change", function (e) {
    var startVal = $("#txt_pagocochera_fecha1").val();
    $("#datetimepicker4").data("datepicker").setStartDate(startVal);
  });
  $("#datetimepicker4").on("change", function (e) {
    var endVal = $("#txt_pagocochera_fecha2").val();
    $("#datetimepicker3").data("datepicker").setEndDate(endVal);
  });
})

function eliminarpagocochera(pagocochera_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pagocochera/pagocochera_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: 'eliminar', // PUEDE SER: L, I, M , E
      pagocochera_id: pagocochera_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Eliminando Pago");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      ListarHistorialPago();
      
      if(parseInt(data.estado) == 1){
        notificacion_success(data.mensaje)    
      }
else if(parseInt(data.estado) == 0){
  console.log(data)
  alerta_error('ERROR', data.mensaje)
}
    },
    complete: function (data) {},
    error: function(data){
      alerta_error('ERROR', data.responseText)
      console.log(data);
  },
  });
}


function egreso_form(usuario_act, egreso_id) {


$.ajax({
    type: "POST",
    url: VISTA_URL + "egreso/egreso_form.php",
    async: true,
    dataType: "html",
    data: ({
        action: usuario_act, // PUEDE SER: L, I, M , E
        egreso_id: egreso_id,
        vista: 'egreso'
    }),
    beforeSend: function () {
        $('#h3_modal_title').text('Cargando Formulario');
        $('#modal_mensaje').modal('show');
    },
    success: function (data) {
        $('#modal_mensaje').modal('hide');
        if (data != 'sin_datos') {
            $('#div_modal_pagocochera_egreso').html(data);
            $('#modal_registro_egreso').modal('show');
            /* alerta_exit("Exito",'Pagos eliminados correctamente'); */

            //desabilitar elementos del form si es L (LEER)
            if (usuario_act == 'L' || usuario_act == 'E')
                form_desabilitar_elementos('form_egreso'); //funcion encontrada en public/js/generales.js

            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_registro_egreso'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_registro_egreso', 'limpiar'); //funcion encontrada en public/js/generales.js
        } else {
            //llamar al formulario de solicitar permiso
            var modulo = 'egreso';
            var div = 'div_modal_pagocochera_egreso';
            permiso_solicitud(usuario_act, egreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
        }
    },
    complete: function (data) {

    },
    error: function (data) {
        $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
        $('#overlay_modal_mensaje').removeClass('overlay').empty();
        $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
        console.log(data.responseText);
    }
});
}

function gestioncochera_form(usuario_act, gestioncochera_id) {
$.ajax({
  type: "POST",
  url: VISTA_URL + "gestioncochera/gestioncochera_form.php",
  async: true,
  dataType: "html",
  data: {
    action: usuario_act, // PUEDE SER: L, I, M , E
    gestioncochera_id: gestioncochera_id,
  },
  beforeSend: function () {
    $("#h3_modal_title").text("Cargando Formulario");
    $("#modal_mensaje").modal("show");
  },
  success: function (data) {
    $("#modal_mensaje").modal("hide");
    if (data != "sin_datos") {
      $("#div_modal_gestioncochera_form").html(data);
      $("#modal_registro_gestioncochera").modal("show");

      //desabilitar elementos del form si es L (LEER)
      if (usuario_act == "L" || usuario_act == "E")
        form_desabilitar_elementos("form_gestioncochera"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_registro_gestioncochera", "limpiar"); //funcion encontrada en public/js/generales.js
    } else {
      //llamar al formulario de solicitar permiso
      var modulo = "gestioncochera";
      var div = "div_modal_gestioncochera_form";
      permiso_solicitud(usuario_act, gestioncochera_id, modulo, div); //funcion ubicada en public/js/permiso.js
    }
  },
  complete: function (data) {},
  error: function (data) {
    $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
    $("#overlay_modal_mensaje").removeClass("overlay").empty();
    $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
    console.log(data.responseText);
  },
});
}


