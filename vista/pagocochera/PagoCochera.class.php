<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class PagoCochera extends Conexion
{

    public $pagocochera_id;
    public $pagocochera_xac;
    public $pagocochera_reg;
    public $pagocochera_fechainicio_facturacion;
    public $pagocochera_fechafin_facturacion;
    public $pagocochera_placa;
    public $pagocochera_numeromovimientos;
    public $cochera_id;
    public $pagocochera_monto;
    public $pagocochera_idmovimientos;
    public $pagocochera_fechapago;
    

    function insertar()
    {
        $this->dblink->beginTransaction();

        try {


            $sql = "INSERT INTO tb_pagocochera (
                tb_pagocochera_fechainiciofacturacion,
                tb_pagocochera_fechafinfacturacion,
                tb_pagocochera_placa,
                tb_pagocochera_numeromovimientos,
                tb_cochera_id,
                tb_pagocochera_monto,
                tb_pagocochera_idmovimientos,
                tb_pagocochera_fechapago


                )
         VALUES (
                :pagocochera_fechainiciofacturacion,
                :pagocochera_fechafinfacturacion,
                :pagocochera_placa,
                :pagocochera_numeromovimientos,
                :cochera_id,
                :pagocochera_monto,
                :pagocochera_idmovimientos,
                :pagocochera_fechapago
                );";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":pagocochera_fechainiciofacturacion", $this->pagocochera_fechainicio_facturacion, PDO::PARAM_STR);
            $sentencia->bindParam(":pagocochera_fechafinfacturacion", $this->pagocochera_fechafin_facturacion, PDO::PARAM_STR);
            $sentencia->bindParam(":pagocochera_placa", $this->pagocochera_placa, PDO::PARAM_STR);
            $sentencia->bindParam(":pagocochera_numeromovimientos", $this->pagocochera_numeromovimientos, PDO::PARAM_INT);
            $sentencia->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_INT);
            $sentencia->bindParam(":pagocochera_monto", $this->pagocochera_monto, PDO::PARAM_STR);
            $sentencia->bindParam(":pagocochera_idmovimientos", $this->pagocochera_idmovimientos, PDO::PARAM_STR);
            $sentencia->bindParam(":pagocochera_fechapago", $this->pagocochera_fechapago, PDO::PARAM_STR);

            /*  $sentencia->bindParam(":estadollave_tipo", $this->estadollave_tipo, PDO::PARAM_STR); */

            $result = $sentencia->execute();
            $pagocochera_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result;
            $data['pagocochera_id'] = $pagocochera_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }


    function modificar()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_pagocochera
            SET
             tb_pagocochera_fechainiciofacturacion = :pagocochera_fechainiciofacturacion,
             tb_pagocochera_fechafinfacturacion = :pagocochera_fechafinfacturacion,
             tb_pagocochera_placa = :pagocochera_placa,
             tb_pagocochera_numeromovimientos = :pagocochera_numeromovimientos,
             tb_cochera_id = :cochera_id,
             tb_pagocochera_monto = :pagocochera_monto,
             tb_pagocochera_idmovimientos = :pagocochera_idmovimientos,
             tb_pagocochera_fechapago = :pagocochera_fechapago
            WHERE
            tb_pagocochera_id = :pagocochera_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':pagocochera_fechainiciofacturacion', $this->pagocochera_fechainicio_facturacion, PDO::PARAM_STR);
            $sentencia->bindParam(':pagocochera_fechafinfacturacion', $this->pagocochera_fechafin_facturacion, PDO::PARAM_STR);
            $sentencia->bindParam(':pagocochera_placa', $this->pagocochera_placa, PDO::PARAM_STR);
            $sentencia->bindParam(':pagocochera_numeromovimientos', $this->pagocochera_numeromovimientos, PDO::PARAM_INT);
            $sentencia->bindParam(':cochera_id', $this->cochera_id, PDO::PARAM_STR);
            $sentencia->bindParam(':pagocochera_monto', $this->pagocochera_monto, PDO::PARAM_STR);
            $sentencia->bindParam(':pagocochera_idmovimientos', $this->pagocochera_idmovimientos, PDO::PARAM_INT);
            $sentencia->bindParam(':pagocochera_fechapago', $this->pagocochera_fechapago, PDO::PARAM_STR);

            $resultado = $sentencia->execute();

            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado; //si es correcto retorna 1
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function eliminar($pagocochera_id)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_pagocochera SET tb_pagocochera_xac=0 WHERE tb_pagocochera_id =:pagocochera_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":pagocochera_id", $pagocochera_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificarEstado($movimientos_id_agrupados)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_cocheramovimiento SET tb_estado_autocochera = 1 WHERE tb_cocheramovimiento_id IN ($movimientos_id_agrupados)";

            $sentencia = $this->dblink->prepare($sql);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; // si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function CancelarPago($movimientos_id_agrupados)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_cocheramovimiento SET tb_estado_autocochera = 0 WHERE tb_cocheramovimiento_id IN ($movimientos_id_agrupados)";

            $sentencia = $this->dblink->prepare($sql);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; // si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function filtroPagoCochera($fecha1, $fecha2, $placa, $cochera)
    {
        try {
            $sql_add = "";
            $sql_add2 = "";

            /*if (!empty($placa)) {
                $sql_add2 .= " AND pc.tb_pagocochera_placa = :placa ";
            }*/
            if($cochera != 0){
                $sql_add .= " AND pc.tb_cochera_id = :cochera ";
            }
            $sql = "SELECT  pc.*,co.tb_cochera_nom
            from tb_pagocochera pc
            inner join tb_cochera co on co.tb_cochera_id=pc.tb_cochera_id
            WHERE
             pc.tb_pagocochera_xac = 1 
            AND DATE(pc.tb_pagocochera_fechainiciofacturacion) BETWEEN :fecha1 AND :fecha2
            $sql_add
            $sql_add2";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            if ($cochera != 0) {
                $sentencia->bindParam(":cochera", $cochera, PDO::PARAM_INT);
            }
            /*if (!empty($placa)) {
                $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
            }*/

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay movimientos que listar";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo 'Error!: ' . $e->getMessage();
        }
    }

    function mostrarUno($pagocochera_id)
  {
    try {
      $sql = "SELECT * from tb_pagocochera
        WHERE tb_pagocochera_id =:tb_pagocochera_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_pagocochera_id", $pagocochera_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $result["estado"] = 1;
        $result["mensaje"] = "exito";
        $result["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $result["estado"] = 0;
        $result["mensaje"] = "No hay pagos registrados";
        $result["data"] = "";
      }

      return $result;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }
}
