<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
} else {
  require_once('../../core/usuario_sesion.php');
}
require_once('PagoCochera.class.php');
$oPagoCochera = new PagoCochera();

require_once '../funciones/fechas.php';

$fecha1 = fecha_mysql($_POST['txt_filtro_fec1']);
$fecha2 = fecha_mysql($_POST['txt_filtro_fec2']);
$cochera = $_POST['cmb_cochera_id'];
$filtro_fec1 = date('01-m-Y');
$filtro_fec2 = date('d-m-Y');
/*
$result = $oPagoCochera->filtroPagoCochera($fecha1, $fecha2,$cochera,$placa);

$tr = '';
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $tr .= '<tr>';
    $tr .= '
          <td>' . $value['placa'] . '</td>
          <td>' . $value['movimientos'] . '</td>
          <td>' . $value['cochera'] . '</td>
          <td>' . $value['monto'] . '</td>
          <td>' . $value['tb_egreso_detalleplanilla'] . '</td>
        ';
    $tr .= '</tr>';
  }
  $result = null;
} else {
  echo $result['mensaje'];
  $result = null;
  exit();
}*/
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modalresumen_pago_cochera" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="panel shadow-simple">
        <div class="panel-body">

          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="panel panel-info">
                <div class="panel-body">
                  <form id="form_cocheramovimiento_filtro" class="form-inline" role="form">
                    <div class="form-group">
                      <div class='input-group date' id='datetimepicker3'>
                        <input type='text' class="form-control input-sm input-shadow" name="txt_pagocochera_fecha1" id="txt_pagocochera_fecha1" value="<?php echo $filtro_fec1; ?>" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar input-shadow"></span>
                        </span>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class='input-group date' id='datetimepicker4'>
                        <input type='text' class="form-control input-sm input-shadow" name="txt_pagocochera_fecha2" id="txt_pagocochera_fecha2" value="<?php echo $filtro_fec2; ?>" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar input-shadow"></span>
                        </span>
                      </div>
                    </div>

                    

                    <div class="form-group">
                      <input type="text" name="txt_pagocochera_placa" id="txt_pagocochera_placa" class="form-control input-sm mayus input-shadow" autocomplete="off" size="30" placeholder="Busca una placa aquí" value="<?php echo $placa; ?>">
                    </div>

                    <div class="form-group">
                      <!--Aqui ira el select -->
                      <select name="cmb_pagocochera_cochera" id="cmb_pagocochera_cochera" class="form-control input-sm mayus input-shadow" value="<?php echo $cochera; ?>">
                            <?php require_once('../cochera/cochera_select.php'); ?>
                        </select>
                    </div>
                    <button type="button" class="btn btn-info btn-sm" onclick="ListarHistorialPago()"><i class="fa fa-search"></i></button>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div id="div_pagocochera_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                
              </div>
            </div>
				</div>
      </div>
    </div>

          <div class="modal-footer">
            <div class="f1-buttons">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/pagocochera/pagocochera_form.js';?>"></script>
