<?php
require_once('../../core/usuario_sesion.php');
require_once('../funciones/funciones.php');
require_once('../gestioncochera/CocheraMovimiento.class.php');
$oCocheraMovimiento = new CocheraMovimiento();
$oCocheraMovimientoInsert = new CocheraMovimiento();
require_once('../gestioncochera/GestionCochera.class.php');
$oGestionCochera = new GestionCochera();
require_once('../pagocochera/PagoCochera.class.php');
$oPagoCochera = new PagoCochera();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once '../funciones/fechas.php';
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();

$action = $_POST['action'];
$metodopago = $_POST['cbo_metodo_id'];
$numope = $_POST['txt_numoperacion'];

if ($action == 'pagar') {
    $fecha1 = fecha_mysql(trim($_POST['txt_filtro_fec1Pago']));
    $fecha2 = fecha_mysql(trim($_POST['txt_filtro_fec2Pago']));
    $cochera_id = $_POST['hdd_cochera_id'];
    $fechapagar = trim($_POST['hdd_fechapagar']);
    $fechapago = $_POST['txt_filtro_fechapago'];

    $total_pagado = 0;
    $nombre_cochera = '';
    try {
        $result = $oCocheraMovimiento->listarPagoAgrupado($fecha1, $fecha2, $cochera_id);

        

        if ($metodopago == 2) {

            //verificar numero de operacion
            $consulta_deposito = $oDeposito->listar_depositos_paragasto($numope);
            $deposito_registrado = $consulta_deposito['estado'];
            $deposito = $consulta_deposito['data'];
            unset($consulta_deposito);

            if ($deposito_registrado == 1) {
                $moneda_deposito_nom = $deposito['tb_moneda_nom'];
                //la moneda del gasto y del deposito deben coincidir
                if (1 != intval($deposito['tb_moneda_id'])) {
                    $deposito_registrado = 3;
                    $data['mensaje'] = "Corregir deposito, debe ser registrado en una cuenta banco con moneda en soles".$deposito_registrado;

                } else {
                    //verificar que el monto del gasto no sobrepase el monto de deposito
                    $importe_ingresos_anteriores = 0;
                    $dts = $oIngreso->lista_ingresos_num_operacion($numope);
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data'] as $key => $dt) {

                            $tb_ingreso_imp = moneda_mysql(floatval($dt['tb_ingreso_imp']));
                            $importe_ingresos_anteriores += $tb_ingreso_imp;
                        }
                    }
                    unset($dts);

                    $monto_gasto = moneda_mysql($_POST['txt_montodeposito_cochera']);
                    $monto_deposito = abs($deposito['tb_deposito_mon']);
                    $monto_puede_usar = moneda_mysql($monto_deposito - $importe_ingresos_anteriores);

                    if ($monto_gasto > $monto_puede_usar) {
                        $deposito_registrado = 2;
                        $data['mensaje'] = 'El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. Monto ingresado: S/.' . mostrar_moneda($monto_gasto) . ', monto disponible: ' . $moneda_deposito_nom . mostrar_moneda($monto_puede_usar) . ', TOTAL DEL DEPOSITO: ' . $moneda_deposito_nom . mostrar_moneda($monto_deposito).' - '.$deposito_registrado;
                    }
                }
            } else {
                $data['mensaje'] = "No está registrado el deposito con numero de operacion $numope. 0";
            }

            if ($deposito_registrado != 1) {
                $data['estado'] = 0;
                echo json_encode($data);
                exit();
            }
        //fin de verificacion de número de operación
        }

        if ($result['estado'] == 1) {
            foreach ($result['data'] as $value) {
                $oPagoCochera->pagocochera_fechainicio_facturacion = $fecha1;
                $oPagoCochera->pagocochera_fechafin_facturacion = $fecha2;
                $oPagoCochera->pagocochera_placa = $value['placa'];
                $oPagoCochera->pagocochera_numeromovimientos = $value['movimientos'];
                $oPagoCochera->cochera_id = $value['tb_cochera_id'];
                $oPagoCochera->pagocochera_monto = $value['monto'];
                $oPagoCochera->pagocochera_idmovimientos = $value['movimientos_id'];
                $oPagoCochera->pagocochera_fechapago = date('Y-m-d');
                $insetarpago = $oPagoCochera->insertar();

                $nombre_cochera = $value['cochera'];

                if ($insetarpago['estado'] == 1) {
                    $movimientos_id_agrupados = $value['movimientos_id'];
                    $oPagoCochera->modificarEstado($movimientos_id_agrupados);

                    $forma_pago = '';
                    if($metodopago == 2)
                        $forma_pago = '. PAGO MEDIANTE DEPOSITO BANCARIO DE N° OPERACION: '.$_POST['txt_numoperacion'];

                    $numdoc = 'PC -' . $insetarpago['pagocochera_id'];
                    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                    $oEgreso->egreso_fec = date('Y-m-d');
                    $oEgreso->documento_id = 9; // OE otros egresos
                    $oEgreso->egreso_numdoc = $numdoc;
                    $oEgreso->egreso_det = 'Se hizo un egreso en la fecha "' . mostrar_fecha($fechapagar) . '" con  la placa de vehículo "' . $value['placa'] . '" registrando un monto de : S/' . $value['monto'].' en la cochera : '.$value['cochera'].$forma_pago;
                    $oEgreso->egreso_imp = $value['monto'];
                    $oEgreso->egreso_tipcam = 1;
                    $oEgreso->egreso_est = 1;
                    $oEgreso->cuenta_id = 10; // id de la cuenta
                    $oEgreso->subcuenta_id = 82; // id de la subcuenta

                    //logica para insertar id dependiendo de la cochera
                    $oEgreso->proveedor_id = 1;
                    switch ($cochera_id) {
                        case 1:
                            $oEgreso->proveedor_id = 300;
                            break;
                        case 3:
                            $oEgreso->proveedor_id = 301;
                            break;
                        case 8:
                            $oEgreso->proveedor_id = 302;
                            break;
                    }

                    $oEgreso->cliente_id = 0;
                    $oEgreso->usuario_id = 0; // el egreso no es para ningún colaborador
                    $oEgreso->caja_id = 1; // la caja ID 1 es la caja general
                    $oEgreso->moneda_id = 1; // 1 soles, 2 dolares
                    $oEgreso->modulo_id = 2; // modulo 2 es para pago cochera
                    $oEgreso->egreso_modide = $insetarpago['pagocochera_id']; // id del modulo desde donde se hace el egreso
                    $oEgreso->empresa_id = $_SESSION['empresa_id'];
                    $oEgreso->egreso_ap = 0; // aquí va el cuotapago de un acuerdo de pago

                    $resultEgreso = $oEgreso->insertar();
                    $total_pagado += formato_numero($value['monto']);

                    $gestion_sinsalida_id = 0;
                    $resultado_funcion = $oGestionCochera->buscar_movimiento_sin_salida($value['tb_gestioncochera_id']);

                    if ($resultado_funcion['estado'] == 1) {
                        $gestion_sinsalida_id = $resultado_funcion['data']['tb_gestioncochera_id'];
                    }

                    // Registrar salida a los movimientos que no tienen una salida, para luego volver a registrar entrada
                    $oCocheraMovimiento->finalidad_id = 5;
                    $oCocheraMovimiento->cocheramovimiento_dest = 'Salida automatizada por facturación';
                    $oCocheraMovimiento->cocheramovimiento_fecha = date($fechapagar);
                    $oCocheraMovimiento->cocheramovimiento_hora = '23:59:59';
                    $oCocheraMovimiento->usuario_id = $_SESSION['usuario_id']; // el último usuario responsable
                    $oCocheraMovimiento->cocheramovimiento_obs = 'Salida automatizada por facturación (cierre de mes)';
                    $oCocheraMovimiento->gestioncochera_id = $value['tb_gestioncochera_id'];
                    $oCocheraMovimiento->movimiento_id = 2;

                    $oCocheraMovimiento->insertar_salida();

                    if ($gestion_sinsalida_id > 0) {
                        // Registrar la entrada
                        $date = new DateTime($fechapagar);
                        $date->modify('+1 day');
                        $fecha_entrada = $date->format('Y-m-d');

                        $oCocheraMovimientoInsert->cocheramovimiento_fecha = $fecha_entrada;
                        $oCocheraMovimientoInsert->cocheramovimiento_dest = $value['tb_cochera_id'];
                        $oCocheraMovimientoInsert->cocheramovimiento_hora = '00:00:00';
                        $oCocheraMovimientoInsert->usuario_id = $_SESSION['usuario_id'];
                        $oCocheraMovimientoInsert->cocheramovimiento_obs = 'Entrada automatizada por facturación (cierre de mes)';
                        $oCocheraMovimientoInsert->gestioncochera_id = $gestion_sinsalida_id;
                        $oCocheraMovimientoInsert->insertar_entrada();
                    }
                }
            }

            if ($metodopago == 2) {

                $idcredito = 0;
                $placa_veh = '';
                $modelotxt = '';
                $marcatxt = '';
                $numope = $_POST['txt_numoperacion'];
                $montdepo = $_POST['txt_montodeposito_cochera'];
                $combanco = $_POST['txt_comisionbanco_cochera'];

                $numcuenta = $_POST['cmb_cuedep_id'];

                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->cliente_id = 1;
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = date('Y-m-d');
                $oIngreso->documento_id = 8;
                $oIngreso->ingreso_numdoc = '1-' . 0 . '' . $idcredito; // verificar
                $oIngreso->ingreso_det = 'INGRESO POR: Pago de Cochera: '.$nombre_cochera.', con depósito en banco del periodo de ' . mostrar_fecha($fecha1).' al '.mostrar_fecha($fechapagar).', los datos del depósito son: N° OPE: '.$numope.', monto depósito: '.$montdepo.' y comsión de banco: '.$combanco;

                $oIngreso->ingreso_imp = $total_pagado;
                $oIngreso->cuenta_id = 10;
                $oIngreso->subcuenta_id = 82;
                $oIngreso->caja_id = 1;
                $oIngreso->moneda_id = 1;
                $oIngreso->modulo_id = 62; // modulo que se detalla en ingreso
                $oIngreso->ingreso_modide = 0; // verificar variable
                $oIngreso->empresa_id = $_SESSION['empresa_id'];

                $oIngreso->ingreso_fecdep = fecha_mysql($fechapago); // cambiar fecha con nuevo filtro

                $oIngreso->ingreso_numope = $numope;
                $oIngreso->ingreso_mondep = $montdepo; // efectivo 0 banco variable
                $oIngreso->banco_id = $combanco;
                $oIngreso->cuentadeposito_id = $numcuenta;

                $oIngreso->insertar();
            }

            $data['estado'] = 1;
            $data['mensaje'] = 'Proceso exitoso';
        } else {
            $data['estado'] = 2;
            $data['mensaje'] = 'Error en el listado de pagos agrupados';
        }
    } catch (Exception $e) {
        $data['estado'] = 0;
        $data['mensaje'] = 'Error ' . $e;
    }

    echo json_encode($data);
} else if ($action == 'eliminar') {
    try {
        $fecha_actual = date("Y-m-d");
        $pagocochera_id = $_POST['pagocochera_id'];
        $data['estado'] = 0;
        $data['mensaje'] = 'Existe un error al eliminar el pago.';

        $result = $oPagoCochera->mostrarUno($pagocochera_id);

        if ($result['estado'] == 1) {
            $movimientosid = $result['data']['tb_pagocochera_idmovimientos'];
            $fechapago = $result['data']['tb_pagocochera_fechapago'];
            if ($fecha_actual == $fechapago) {
                if ($oPagoCochera->eliminar($pagocochera_id)) {
                    if ($oEgreso->cambiar_xac_por_modulo_modide(0, 2, $pagocochera_id)) {
                        $oPagoCochera->CancelarPago($movimientosid);
                        $data['estado'] = 1;
                        $data['mensaje'] = 'Pago cancelado';
                    }
                } else {
                    $data['estado'] = 0;
                    $data['mensaje'] = 'PROBLEMAS AL DAR DE BAJA AL PAGOCOCHERA';
                }
            } else {
                $data['estado'] = 0;
                $data['mensaje'] = 'Solo se pueden anular pagos hechos en el mismo día.';
            }
        }
    } catch (Exception $e) {
        $data['estado'] = 0;
        $data['mensaje'] = $e->getMessage();
    }
    echo json_encode($data);

    //metodo para verificar operacion en el controller
}
