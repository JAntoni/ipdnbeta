<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL .'pagocochera/PagoCochera.class.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../pagocochera/PagoCochera.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../egreso/Egreso.class.php');
    $oEgreso = new Egreso();
  }
  //echo "xddddd"; exit();

  $oPagoCochera = new PagoCochera();

    $fecha1 = fecha_mysql($_POST['fecha1']);
    $fecha2 = fecha_mysql($_POST['fecha2']);
    $placa = mb_strtoupper($_POST['placa'], 'UTF-8');
    $cochera = $_POST['cochera'];



  $result = $oPagoCochera->filtroPagoCochera($fecha1,$fecha2,$placa,$cochera);
  $tr = '';
  if($result['estado'] == 1){
      //echo $result['data']; exit();
    foreach ($result['data'] as $key => $value) {
      $egreso_detalle='';
      $egreso_id=0;
      
      $result2=$oEgreso->mostrar_por_modulo(2, $value['tb_pagocochera_id'],1,1);
      if($result2['estado']==1){
        $egreso_detalle=$result2['data'][0]['tb_egreso_det'];
        $egreso_id=$result2['data'][0]['tb_egreso_id'];
      }
      $result2=null;


      $tr .='<tr id="tabla_cabecera_fila">';
        $tr.='
        <td id="tabla_fila">'.$value['tb_pagocochera_id'].'</td>
        <td id="tabla_fila">'.$value['tb_pagocochera_placa'].'</td>
        <td id="tabla_fila">'.$value['tb_pagocochera_idmovimientos'].'</td>
        <td id="tabla_fila">'.$value['tb_cochera_nom'].'</td>
        <td id="tabla_fila">'.$value['tb_pagocochera_monto'].'</td>
        <td id="tabla_fila">'.$egreso_detalle.'</td>
        <td id="tabla_fila" align="center">
        <a class="btn btn-info btn-xs" title="Ver" onclick="egreso_form(\'L\','.$egreso_id.')"><i class="fa fa-eye"></i> Ver</a>
        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminarpagocochera('.$value["tb_pagocochera_id"].')"><i class="fa fa-trash"></i> Eliminar</a>

    </td>
          
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_pagoscochera" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
    <th id="tabla_cabecera_fila">ID</th>  
    <th id="tabla_cabecera_fila">PLACA</th>
    <th id="tabla_cabecera_fila">MOVIMIENTOS</th>
    <th id="tabla_cabecera_fila">COCHERA</th>
    <th id="tabla_cabecera_fila">MONTO</th>
    <th id="tabla_cabecera_fila">DETALLE DE EGRESO</th>
    <th id="tabla_cabecera_fila">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
