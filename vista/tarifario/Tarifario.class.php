<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Tarifario extends Conexion{

    function insertar($tarifario_nom, $tarifario_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tarifario(tb_tarifario_xac, tb_tarifario_nom, tb_tarifario_des)
          VALUES (1, :tarifario_nom, :tarifario_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifario_nom", $tarifario_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifario_des", $tarifario_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($tarifario_id, $tarifario_nom, $tarifario_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tarifario SET tb_tarifario_nom =:tarifario_nom, tb_tarifario_des =:tarifario_des WHERE tb_tarifario_id =:tarifario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifario_id", $tarifario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tarifario_nom", $tarifario_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifario_des", $tarifario_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($tarifario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_tarifario WHERE tb_tarifario_id =:tarifario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifario_id", $tarifario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($tarifario_id){
      try {
        $sql = "SELECT * FROM tb_tarifario WHERE tb_tarifario_id =:tarifario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifario_id", $tarifario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_tarifarios(){
      try {
        $sql = "SELECT * FROM tb_tarifario";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de Tarifarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function mostrar_por_fecha_periodo($fecha){
      try {
        $sql = "SELECT * FROM tb_tarifario where :fecha between tb_tarifario_fecini and tb_tarifario_fecfin";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
//          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de Tarifarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }

    
  }

?>
