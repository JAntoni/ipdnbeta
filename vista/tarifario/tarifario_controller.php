<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../tarifario/Tarifario.class.php');
  $oTarifario = new Tarifario();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$tarifario_nom = mb_strtoupper($_POST['txt_tarifario_nom'], 'UTF-8');
 		$tarifario_des = $_POST['txt_tarifario_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el tarifario.';
 		if($oTarifario->insertar($tarifario_nom, $tarifario_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'tarifario registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$tarifario_id = intval($_POST['hdd_tarifario_id']);
 		$tarifario_nom = mb_strtoupper($_POST['txt_tarifario_nom'], 'UTF-8');
 		$tarifario_des = $_POST['txt_tarifario_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el tarifario.';

 		if($oTarifario->modificar($tarifario_id, $tarifario_nom, $tarifario_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'tarifario modificado correctamente. '.$tarifario_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$tarifario_id = intval($_POST['hdd_tarifario_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el tarifario.';

 		if($oTarifario->eliminar($tarifario_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'tarifario eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>