
$(document).ready(function(){
  $('#form_tarifario').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"tarifario/tarifario_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_tarifario").serialize(),
				beforeSend: function() {
					$('#tarifario_mensaje').show(400);
					$('#btn_guardar_tarifario').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#tarifario_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#tarifario_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		tarifario_tabla();
		      		$('#modal_registro_tarifario').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#tarifario_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#tarifario_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_tarifario').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#tarifario_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#tarifario_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_tarifario_nom: {
				required: true,
				minlength: 2
			},
			txt_tarifario_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_tarifario_nom: {
				required: "Ingrese un nombre para el tarifario",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_tarifario_des: {
				required: "Ingrese una descripción para el tarifario",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
