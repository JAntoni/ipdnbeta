<?php
require_once('Tarifario.class.php');
$oTarifario = new Tarifario();

$tarifario_id = (empty($tarifario_id))? 0 : $tarifario_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oTarifario->listar_tarifarios();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($tarifario_id == $value['tb_tarifario_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_tarifario_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_tarifario_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>