



$(document).ready(function() {
    $('#txt_cuo_not').focus();
    
    
    $("#for_cob").validate({
        submitHandler: function() {
                var hist = $('#div_historial_cuota').html();
                $.ajax({
                        type: "POST",
                        url: VISTA_URL+"cobranza/cobranza_controller.php",
                        async:true,
                        dataType: "json",
                        data: $("#for_cob").serialize(),
                        beforeSend: function() {
//                            $('#msj_cobranza').html("Guardando...");
//                            $('#msj_cobranza').show(100);
                        },
                        success: function(data){
                            var cuo_id = $('#hdd_cuo_id').val();		
                                swal_success("SISTEMA",data.ven_msj,2000);
                                $('#span_'+cuo_id).html(data.nota);
                                var vista=$("#hdd_vista").val();
                                if(vista=="cmb_ven_id")
                                {
                                    vista+'.'+(data.ven_id);
                                }
                                var vistas=$("#hdd_vistas").val();
                                if(vistas=='cobranza'){
                                    cobranza_tabla();
                                }

                        },
                        complete: function(data){
                            console.log(data);
                            var not = $('#txt_cuo_not').val();
                                if(data.statusText == 'OK'){
                              //$('#btn_'+cuo_id).hide();
//                              $("#div_cobranza_form" ).dialog( "close" );
                              $('#modal_registro_cobranza').modal('hide');
                            }
                            else
                              swal_error("Error al guardar nota: ", data.responseText,5000);
                        }
                        
			});
                        },
                        rules: {
                            txt_cuo_not:{
                                required: true
                            }
                        },
                        messages: {
                            txt_cuo_not:{
                                required: 'Debe ingresar una nota'
                            }
		}
	});
});