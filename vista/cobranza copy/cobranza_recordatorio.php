<?php
  require_once('../creditomenor/Creditomenor.class.php');
  $oCredito = new Creditomenor();
  require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();
  require_once ("../cuotapago/Cuotapago.class.php");
  $oCuotapago = new Cuotapago();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $action = $_POST['action'];
  $credito_id = intval($_POST['credito_id']);
  $cuota_id = intval($_POST['cuota_id']);
  $vista = $_POST['vista'];

  if($action == 'recordatorio_cmenor'){
    $result = $oCredito->mostrarUno($credito_id);
      if($result['estado'] == 1){
        $cliente_nom = ucwords(mb_strtolower($result['data']['tb_cliente_nom'], 'UTF-8'));
      }
    $result = NULL;

    $result = $oCuota->mostrarUno($cuota_id);
      if($result['estado'] == 1){
        $cuota_fec = $result['data']['tb_cuota_fec'];
        $cuota_int = floatval($result['data']['tb_cuota_int']);
      }
    $result = NULL;

    $pagos_cuota = 0;
    $modulo_id = 1; // 1 se refiere a pagos de tipo cuota, 2 de tipo cuotadetalle

    $result = $oCuotapago->mostrar_cuotatipo_ingreso($modulo_id, $cuota_id);
      if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
          $pagos_cuota += floatval($value['tb_ingreso_imp']);
        }
      }
    $result = NULL;

    $pago_minimo = formato_numero($cuota_int - $pagos_cuota);
    $fecha_hoy = date('Y-m-d');
    $dias_vencido = 0; $mensaje_vencido = '';
    $cuenta_bcp_pago = '305-73185268-0-99'; //cuenta a nombre de carlos
    
    if(strtotime($fecha_hoy) > strtotime($cuota_fec)){ //CUOTA VENCIDA
      $dias_vencido = (strtotime($fecha_hoy) - strtotime($cuota_fec)) / 86400;

      if($dias_vencido <= 5){
        $mensaje_vencido = ' le saludamos cordialmente y le comunicamos que su crédito venció el *'.mostrar_fecha($cuota_fec).' (Vencida hace '.$dias_vencido.' días)*, su pago mínimo es *S/. '.mostrar_moneda($cuota_int).'*, mismo que seguramente por olvido o por falta de tiempo no ha podido cumplir.<br> Por tal motivo, agradeceremos acercarse a la brevedad a nuestra oficina para regularizar su pago. Recuerde que también puede pagar mediante *Cuenta Bancaria BCP N° '.$cuenta_bcp_pago.'*';
      }
      if($dias_vencido > 5 && $dias_vencido <= 10){
        $mensaje_vencido = ' anteriormente le notificamos que su cuenta se encuentra atrasada, venció el *'.mostrar_fecha($cuota_fec).' (Vencida hace '.$dias_vencido.' días)*, su pago mínimo es *S/. '.mostrar_moneda($cuota_int).'*. Deseamos no causarle mayores molestias y esperamos a la brevedad posible la regularización, lo que evitará cobrarle moras y gastos de cobranza.<br> Recuerde que también puede pagar mediante *Cuenta Bancaria BCP N° '.$cuenta_bcp_pago.'*';
      }
      if($dias_vencido > 10 && $dias_vencido < 14){
        $mensaje_vencido = '  le hemos enviado varios recordatorios anteriormente y su cuenta sigue atrasada, venció el *'.mostrar_fecha($cuota_fec).' (Vencida hace '.$dias_vencido.' días)*, su pago mínimo es *S/. '.mostrar_moneda($cuota_int).'*, *PRONTO SU PRODUCTO ESTARA PASANDO A REMATE SIN OPCIÓN A RECUPERARLO*. Agradeceremos acercarse a la brevedad a nuestra oficina.<br> Recuerde que también puede pagar mediante *Cuenta Bancaria BCP N° '.$cuenta_bcp_pago.'*';
      }
      if($dias_vencido >= 14){
        $mensaje_vencido = '  le hemos enviado varios recordatorios anteriormente y su cuenta sigue atrasada, venció el *'.mostrar_fecha($cuota_fec).' (Vencida hace '.$dias_vencido.' días)*, le avisamos que: *SU PRODUCTO YA PASÓ A REMATE Y TIENE MÁXIMO 2 DÍAS PARA PODER REGULARIZAR, PASANDO ESTOS DÍAS DÍAS EL PRODUCTO YA NO SE PODRÁ RECUPERAR*. Agradeceremos acercarse a la brevedad a nuestra oficina.<br>';
      }
    }
    else{
      $dias_vencido = (strtotime($cuota_fec) - strtotime($fecha_hoy)) / 86400;
      if($dias_vencido == 0)
        $mensaje_vencido = 'le saludamos cordialmente y le comunicamos que su crédito vence hoy *'.mostrar_fecha($cuota_fec).'*, su pago mínimo es *S/. '.mostrar_moneda($cuota_int).'*.<br>';
      else
        $mensaje_vencido = "le saludamos cordialmente y le comunicamos que su crédito vencerá el *".mostrar_fecha($cuota_fec)." (vence en ".$dias_vencido." días)*, su pago mínimo es *S/. ".mostrar_moneda($cuota_int)."*.<br>";
      
      $mensaje_vencido .= "Recuerde que no realizar el pago en las fechas indicadas según cronograma genera cobro de intereses moratorios, para ello tenemos los canales de pago a través de nuestras ventanillas o *Cuenta Bancaria BCP N° '.$cuenta_bcp_pago.'*";
    }

    $data['estado'] = 1;
    $mensaje = "
      <b>*RECORDATORIO DE PAGO*</b>
      <p>Estimado Cliente *".$cliente_nom.".*, ".$mensaje_vencido."
      <p>Atentamente: *Préstamos del Norte*<br>
      *Carlos Concepción León*.</p>";

    echo $mensaje;
  }
?>