<?php
require_once('../../core/usuario_sesion.php');

require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cobranza/TareaCobranza.class.php");
$oTarea = new TareaCobranza();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ('../cuotapago/Cuotapago.class.php');
$oCuotapago = new Cuotapago();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if($_POST['action_cobranza']=="1")
{
	$dts = $oCuota->mostrarUno($_POST['hdd_cuo_id']);
            if($dts['estado']==1){
		$cuota_fec = mostrar_fecha($dts['data']['tb_cuota_fec']);
		$credito_tipo = $dts['data']['tb_creditotipo_id'];
		$credito_id = $dts['data']['tb_credito_id'];
            }
            $dts=NULL;

	$dts = $oCuota->informacion_credito_cuota($credito_tipo, $credito_id);
            if($dts['estado']==1){
		$cliente_id = $dts['data']['tb_cliente_id'];
		$credito_subtip = $dts['data']['tb_credito_tip'];
		if($credito_tipo == 2)
                    $credito_subtip = $dts['data']['tb_credito_tip1'];
            }
        $dts=NULL;

  $array_usu = explode(' ', $_SESSION['usuario_nom']);
  $nota = '';
  $hora = date('d-m-Y h:i a');

  if($_SESSION['usuariogrupo_id'] == 2)
    $nota = '<b>'.$array_usu[0].': '.$_POST['txt_cuo_not'].' | '.$hora.'</b><br>';
  else
    $nota = '<b style="color: green;">'.$array_usu[0].': '.$_POST['txt_cuo_not'].' | '.$hora.'</b><br>';

  $data['nota'] = $nota;
  
	$oCuota->agregar_nota_cuota($_POST['hdd_cuo_id'],$nota);
  //ver el tema de que si se agrega una nota con fecha, obligatorio esa cuota debe estar registrado en tarea
  $oTarea->registrar_nota_fecha($nota,fecha_mysql($_POST['txt_cuo_fecref']),$_POST['hdd_cuo_id']);

  $dts = $oCuota->cliente_por_cuotaid($_POST['hdd_cuo_id']);
  	if($dts['estado']==1){
            $credito = $dts['data']['credito'];
            $cli_nom = $dts['data']['tb_cliente_nom'];
        }
    $dts=NULL;

  //el número 1 al final para que se muestre en mi GC, HISTORIAL DEL CLIENTE
  $nota_det = 'Nota GC para: <b>'.$cli_nom.':</b> '.$nota.' | Nota de la cuota de fecha: <b>'.$cuota_fec.'</b>, del Crédito: <b>'.$credito.'</b>';
  $oCliente->insertar_nota_cliente($cliente_id, $_SESSION['usuario_id'], $credito_tipo, $credito_subtip, $credito_id, $nota_det, 1);
  //-------------------------------------

  $line_det = '<b>'.$_SESSION['usuario_nom'].'</b> Ha registrado una nota en Gestión de Cobranza: '.$_POST['txt_cuo_not'].'. Del cliente: '.$cli_nom.', del crédito: '.$credito.' && <b>'.date('d-m-Y h:i a').'</b>';
  $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
  $oLineaTiempo->tb_lineatiempo_det=$line_det;
  $oLineaTiempo->insertar();

  if(!empty($_POST['txt_cuo_fecref'])){
  	$nota = 'Cliente: <b>'.$_POST['hdd_cliente_nom'].'</b> '.$_POST['txt_cuo_not'];
  	$oTarea->insertar_tarea_notificacion_cobranza(2, $nota, $_POST['hdd_cuo_id'], fecha_mysql($_POST['txt_cuo_fecref']));
  	$oTarea->insertar_tarea_notificacion_cobranza(11, $nota, $_POST['hdd_cuo_id'], fecha_mysql($_POST['txt_cuo_fecref']));
  	$oTarea->insertar_tarea_notificacion_cobranza(18, $nota, $_POST['hdd_cuo_id'], fecha_mysql($_POST['txt_cuo_fecref']));
  	$oTarea->insertar_tarea_notificacion_cobranza(32, $nota, $_POST['hdd_cuo_id'], fecha_mysql($_POST['txt_cuo_fecref']));

  	$line_det = '<b>'.$_SESSION['usuario_nom'].'</b> Ha creado una Notificación en Gestión de Cobranza, la nota fue: '.$_POST['txt_cuo_not'].' && <b>'.date('d-m-Y h:i a').'</b>';
        $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
        $oLineaTiempo->tb_lineatiempo_det=$line_det;
        $oLineaTiempo->insertar();
  }

	$data['ven_msj']='Se registró la nota correctamente.';
	
	echo json_encode($data);
}
if($_POST['action_cobranza']=="2")
{

	$oCuotadetalle->modificar_campo($_POST['hdd_cuo_id'],'tb_cuotadetalle_not',$_POST['txt_cuo_not'],'STR');
	$data['ven_msj']='Se registró la nota correctamente.';
	
	echo json_encode($data);
}

if($_POST['action']=="eliminar")
{
	if(!empty($_POST['cuopag_id']))
	{
		$oCuotapago->modificar_campo($_POST['cuopag_id'],'tb_cuotapago_xac','0','INT');
		echo 'Se envió a la papelera correctamente.';
	}
	else
	{
		echo 'Intentelo nuevamente.';
	}
}
if($_POST['action']=="notificacion"){
	$noti = '';
	$dts = $oTarea->listar_notificaciones_cobranza($_SESSION['usuario_id']);
		if($dts['estado']==1){
			$noti .='
				<fieldset class="fiel_notificacion">
				  <legend>Mensajes de Vencimiento de Cuotas Libres</legend>
				  <table width="100%">';
			foreach ($dts['estado']as $key=>$dt) {
				$noti .= '
                            <tr class="tr_tareas">
                                <td align="justify" width="90%">'.$dt['tb_cobranzanoti_not'].' | Nota programada para el: <b>'.mostrarFecha($dt['tb_cobranzanoti_fec']).'</b></td>
                                <td width="10%" align="right">
                                  <a href="#vis" class="btn_listo" id="btn_listo'.$dt['tb_cobranzanoti_id'].'" onclick="visto_cobranza_noti('.$dt['tb_cobranzanoti_id'].')">OK</a>
                                </td>
                            </tr>';
			}
			$noti .='
					</table>
				</fieldset>';
			echo $noti;
		}
		else
			echo '';
}
if($_POST['action'] == "notificacion_vista"){
	$noti_id = $_POST['noti_id'];
	//$oTarea->modificar_estado_notificacion_cobranza($noti_id, 1); // parametro 1 es visto
	$dts = $oTarea->mostrar_una_notificacion($noti_id);
            if($dts['estado']==1){
		$cuo_id = $dts['data']['tb_cuota_id'];
            }

	$oTarea->modificar_estado_notificacion_cobranza_cuota_id($cuo_id, 1);
	echo 1;
}
?>