<?php

  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class TareaCobranza extends Conexion{
    
    public $tb_tarea_id;
    public $tb_tarea_est;
    public $tb_tarea_fecreg;
    public $tb_usuario_id;
    public $tb_cuota_id;
    public $tb_tarea_fec1;//fecha inicial para el filtro de cuotas vencidas
    public $tb_tarea_fec2;//fecha final para el filtro de cuotas vencidas
    public $tb_tarea_not;//nota, descripcion del acuerdo con el que se llega con el cliente
    public $tb_tarea_fecac;//fecha referencial en el que el asesor y el cliente llegan a un acuerdo, se genera una notificación si esta fecha está por vencer
    public $tb_tarea_his;//historial de quien asigana esta tarea y de quien aprueba o rechaza
  
  function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tareacobranza (
                                            tb_tarea_fecreg, 
                                            tb_usuario_id, 
                                            tb_cuota_id, 
                                            tb_tarea_fec1, 
                                            tb_tarea_fec2, 
                                            tb_tarea_his) 
                                    VALUES (
                                            NOW(),
                                            :tb_usuario_id, 
                                            :tb_cuota_id, 
                                            :tb_tarea_fec1, 
                                            :tb_tarea_fec2, 
                                            :tb_tarea_his)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_id", $this->tb_cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tarea_fec1", $this->tb_tarea_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_tarea_fec2", $this->tb_tarea_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_tarea_his", $this->clietb_tarea_hisnte_des, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $tb_tarea_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['tb_tarea_id'] = $tb_tarea_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function filtrar_tarea_estado($cuo_id,$est){
      try {
        $sql = "SELECT * FROM tb_tareacobranza where tb_cuota_id =:tb_cuota_id AND tb_tarea_est =:tb_tarea_est";

        $sentencia = $this->dblink->prepare($sql);
         $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);
         $sentencia->bindParam(":tb_tarea_est", $est, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function filtrar_tarea_cuota($cuo_id){
      try {
             $sql = "SELECT * FROM tb_tareacobranza where tb_cuota_id =:tb_cuota_id";

            $sentencia = $this->dblink->prepare($sql);
             $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function filtrar_tarea($tar_id){
      try {
             $sql = "SELECT * FROM tb_tareacobranza where tb_tarea_id =:tb_tarea_id";

            $sentencia = $this->dblink->prepare($sql);
             $sentencia->bindParam(":tb_tarea_id", $tar_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_credito_cuotas($credito,$tip1,$usu_id,$not){
      try {
            $aden_acupag = '';
            if($tip1 == 1)
              $aden_acupag = ' AND (tb_credito_tip1 = 1 OR tb_credito_tip1 = 4)';
            if($tip1 == 2 || $tip1 == 3)
              $aden_acupag = ' AND tb_credito_tip1 ='.$tip1;

            $cre_tip = 2;
            if($credito == 'tb_creditogarveh')
              $cre_tip = 3;
            if($credito == 'tb_creditohipo')
              $cre_tip = 4;

            $sql = "SELECT * from tb_tareacobranza tc 
              INNER JOIN tb_cuota c on c.tb_cuota_id = tc.tb_cuota_id
              INNER JOIN $credito cre on cre.tb_credito_id = c.tb_credito_id
              LEFT JOIN tb_cuotatipo cu on cu.tb_cuotatipo_id = cre.tb_cuotatipo_id
              INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id
              WHERE cre.tb_credito_xac =1 AND c.tb_creditotipo_id =:tb_creditotipo_id AND tc.tb_usuario_id =:tb_usuario_id".$aden_acupag;

            if($not != "")
              $sql.= " AND tc.tb_tarea_est =".$not;
            if($credito == 'tb_creditoasiveh')
              $sql.= " AND cre.tb_credito_est NOT IN(4,8)";

        $sentencia = $this->dblink->prepare($sql);
         $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
         $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_cuotasdetalle_cuota($credito,$cuo_id){
      try {
            $acupag = '';
    if($credito == 'tb_creditoasiveh')
            $acupag = ' AND cd.tb_cuotadetalle_estap = 0';


          $sql = "SELECT * from tb_tareacobranza tc 
                    INNER JOIN tb_cuota c on c.tb_cuota_id = tc.tb_cuota_id
                    INNER JOIN tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id
                    INNER JOIN $credito cre on cre.tb_credito_id = c.tb_credito_id
                    LEFT JOIN tb_cuotatipo cu on cu.tb_cuotatipo_id = cre.tb_cuotatipo_id
                    WHERE cd.tb_cuotadetalle_fec <= NOW() AND cd.tb_cuotadetalle_fec between tc.tb_tarea_fec1 and 
                    tc.tb_tarea_fec2 and c.tb_cuota_id =:tb_cuota_id AND cd.tb_cuotadetalle_est !=2".$acupag.' GROUP BY cd.tb_cuotadetalle_id';

        $sentencia = $this->dblink->prepare($sql);
         $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
  function modificar_estado_nota($cuo_id, $est, $his){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tareacobranza set 
                                        tb_tarea_est =:tb_tarea_est, 
                                        tb_tarea_his = CONCAT(tb_tarea_his,:tb_tarea_his) 
                                where 
                                        tb_cuota_id =$cuo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_tarea_est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tarea_his", $his, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

  function registrar_nota_fecha($not,$fec,$cuo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tareacobranza set 
                                            tb_tarea_not =CONCAT(tb_tarea_not,:tb_tarea_not), 
                                            tb_tarea_fecac=:tb_tarea_fecac 
                                        where 
                                            tb_cuota_id =:tb_cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_tarea_not", $not, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_tarea_fecac", $fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function filtrar_tareas_por_hacer($usu_id){
      try {
            $sql = "SELECT * FROM tb_tareacobranza where tb_tarea_est =0 and tb_usuario_id =:tb_usuario_id";

        $sentencia = $this->dblink->prepare($sql);
         $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function insertar_tarea_notificacion_cobranza($usu_id, $nota, $cuo_id, $fec){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cobranzanoti(
                                            tb_cobranzanoti_reg, 
                                            tb_usuario_id, 
                                            tb_cobranzanoti_not, 
                                            tb_cuota_id,
                                            tb_cobranzanoti_fec) 
                                    VALUES (
                                            NOW(),
                                            tb_usuario_id, 
                                            tb_cobranzanoti_not, 
                                            tb_cuota_id,
                                            tb_cobranzanoti_fec)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzanoti_not", $nota, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzanoti_fec", $fec, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar_notificaciones_cobranza($usu_id){
      try {
            $sql = "SELECT * FROM tb_cobranzanoti where tb_cobranzanoti_fec <= NOW() and tb_cobranzanoti_est =0 and tb_usuario_id =:tb_usuario_id";

        $sentencia = $this->dblink->prepare($sql);
         $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_estado_notificacion_cobranza_notificacion_id($noti_id, $est){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cobranzanoti SET tb_cobranzanoti_est =:tb_cobranzanoti_est where tb_cobranzanoti_id =:tb_cobranzanoti_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cobranzanoti_est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzanoti_id", $noti_id, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar_estado_notificacion_cobranza_cuota_id($cuo_id, $est){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cobranzanoti SET tb_cobranzanoti_est =:tb_cobranzanoti_est where tb_cuota_id =:tb_cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cobranzanoti_est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrar_una_notificacion($noti_id){
      try {
            $sql = "SELECT * FROM tb_cobranzanoti where tb_cobranzanoti_id =:tb_cobranzanoti_id";

            $sentencia = $this->dblink->prepare($sql);
             $sentencia->bindParam(":tb_cobranzanoti_id", $noti_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
          } catch (Exception $e) {
            throw $e;
          }
    }

  // -------------------------------------- REPORTE DE NOTAS --------------------------------
    
    
    function listar_notificaciones_cobranza_fecha($fecha, $usuario_id){
      try {
            $sql = "SELECT * FROM tb_cobranzanoti where tb_cobranzanoti_fec >=:tb_cobranzanoti_fec and tb_usuario_id =:tb_usuario_id";

            $sentencia = $this->dblink->prepare($sql);
             $sentencia->bindParam(":tb_cobranzanoti_fec", $fecha, PDO::PARAM_STR);
             $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
          } catch (Exception $e) {
            throw $e;
          }
    }

}

?>