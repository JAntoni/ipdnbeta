$(document).ready(function() {
  console.log('camm 99')
  $( "#txt_fil_cli" ).autocomplete({
    minLength: 1,
    source: function(request, response){
      $.getJSON(
        VISTA_URL+"cliente/cliente_autocomplete.php",
        {term: request.term}, //
        response
      );
    },
    select: function(event, ui){
      $('#hdd_fil_cli_id').val(ui.item.cliente_id);
      $('#txt_fil_cli').val(ui.item.cliente_nom);
      cobranza_tabla();
      event.preventDefault();
      $('#txt_fil_cli').focus();
    }
  });
  
  $('#datetimepicker1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $('#datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("changeDate", function (e) {
    var startVal = $('#txt_fil_cre_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("changeDate", function (e) {
    var endVal = $('#txt_fil_cre_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  

});

function recordatorio_cobranza(credito_id, cuota_id){
  $.confirm({
    columnClass: 'medium',
    title: 'Recordatorio de Pago',
    type: 'purple',
    typeAnimated: true,
    buttons: {
      close: {
        text: 'Cerrar',
        action: function(){}
      }
    },
    content: function () {
      var self = this;
      return $.ajax({
        data: ({
          action: 'recordatorio_cmenor', // PUEDE SER: L, I, M , E
          credito_id: credito_id,
          cuota_id: cuota_id,
          vista: 'creditomenor'
        }),
        url: VISTA_URL + "cobranza/cobranza_recordatorio.php",
        dataType: 'html',
        method: 'POST'
      }).done(function (response) {
        self.setContent(response);
        // self.setContentAppend('<br>Version: ' + response.version);
        // self.setTitle(response.name);
        //console.log(response);
      }).fail(function(data){
        self.setContent('Something went wrong.');
        console.log(data);
      });
    }
  });
}

function creditomenor_form(usuario_act, creditomenor_id){
  $.ajax({
        type: "POST",
        url: VISTA_URL+"creditomenor/creditomenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditomenor_id: creditomenor_id,
            vista: 'creditomenor'
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
                $('#div_modal_creditomenor_form').html(data);
                $('#modal_registro_creditomenor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if(usuario_act == 'L' || usuario_act == 'E')
                  form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_registro_creditomenor', 95);
                modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
              //llamar al formulario de solicitar permiso
              var modulo = 'creditomenor';
              var div = 'div_modal_creditomenor_form';
              permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){

        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
            console.log(data.responseText);
        }
	});
}

function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function cobranza_form(idf, id2){
	var cliente = $('#span_cli'+idf).text();
  //console.log('cliente: ' + cliente);
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cobranza/cobranza_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cuo_id:	idf,
			cuo_tip:id2,
			cli_nom: cliente,
			vista:	'remate',
			vistas:	'cobranza'
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_cobranza_form').html(html);		
      $('#modal_registro_cobranza').modal('show');
      $('#modal_mensaje').modal('hide');
          
      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal('modal_registro_cobranza', 'limpiar'); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_registro_cobranza'); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un ancho automatico al modal, al abrirlo
      //modal_width_auto('modal_registro_cobranza', 75); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}


function cobranza_tabla(){
  console.log('Se ejecuta tabla cobranza');
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_tabla.php",
    async: true,
    dataType: "html",
    data: $("#for_fil_cre").serialize(),
    beforeSend: function() {
      $('#cobranza_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_cobranza_tabla').html(data);
      $('#cobranza_mensaje_tbl').hide(300);
      //estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cliente_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}