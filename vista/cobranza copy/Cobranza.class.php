<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
  class Cobranza extends Conexion{
      
    function mostrarUnoMenor($id){
        try {
            $sql="SELECT *
                    FROM tb_creditomenor cm
                    INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
                    INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
                    INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                    WHERE cm.tb_credito_xac = 1 AND cu.tb_cuota_id=:tb_cuota_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
      
    function  mostrarUnoAsiveh($id, $fec_1, $fec_2, $cond){
        try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
              }
            }
		$sql = "SELECT 
                                tb_cuotadetalle_id, tb_cuotadetalle_fec, tb_cuotadetalle_cuo, tb_cuotadetalle_num, cu.tb_moneda_id 
                        FROM 
                                tb_cuota cu 
                                INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id 
                        WHERE 
                                cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est <> 2 
                                AND cd.tb_cuotadetalle_estap = 0 
                                AND cd.tb_cuotadetalle_fec 
                                BETWEEN :fecha1 AND :fecha2 ".$cond_sql;

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
      
    function  mostrarUnoGarveh($id, $fec_1, $fec_2, $cond){
        try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
              }
            }
		$sql = "SELECT 
                            tb_cuotadetalle_id, tb_cuotadetalle_fec, tb_cuotadetalle_cuo, tb_cuotadetalle_num, cu.tb_moneda_id 
                        FROM 
                            tb_cuota cu 
                            INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id 
                        WHERE 
                            cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est <> 2 
                            AND cd.tb_cuotadetalle_estap = 0 
                            AND cd.tb_cuotadetalle_fec 
                            BETWEEN :fecha1 AND :fecha2".$cond_sql;


            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
      
    function  mostrarUnoHipotecario($id, $fec_1, $fec_2, $cond){
        try {
            $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
                  }else{
                    $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
                  }
                }
		$sql = "SELECT 
                                tb_cuotadetalle_id, tb_cuotadetalle_fec, tb_cuotadetalle_cuo, tb_cuotadetalle_num, cu.tb_moneda_id, tb_cuota_int 
                        FROM    tb_cuota cu 
                                INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id 
                        WHERE 
                                cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est <> 2 
                                AND cd.tb_cuotadetalle_estap = 0 
                                AND cd.tb_cuotadetalle_fec 
                                BETWEEN :fecha1 AND :fecha2".$cond_sql;
		
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
    
    function filtrar_credito_cliente($tabla,$tipo,$cli_id,$fec_1,$fec_2,$cond){
      try {
                $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
                  }else{
                    $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
                  }
                }
            $sql.=" SELECT cd.tb_cuotadetalle_id
            FROM $tabla cm
            INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
            INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
            INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id

            WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN(1,2,4,7,8) AND cu.tb_creditotipo_id =:tb_creditotipo_id
            AND cu.tb_cuota_est != 2 AND cu.tb_cuota_xac = 1 AND cd.tb_cuotadetalle_est != 2 AND cd.tb_cuotadetalle_estap = 0
            AND cm.tb_cliente_id = :tb_cliente_id
            AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2" .$cond_sql;

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_creditotipo_id", $tipo, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_menor($cli_id, $fec_1, $fec_2, $cond){
      try {
            $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cu.tb_cuota_fec >= date(now())";
                  }else{
                    $cond_sql = " AND cu.tb_cuota_fec < date(now())";
                  }
                }
            $sql = " SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_int, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ct.tb_cuotatipo_id, ct.tb_cuotatipo_nom, ur.tb_usuario_nom, ur.tb_usuario_ape
                    FROM tb_creditomenor cm
                    INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
                    INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                    WHERE cm.tb_credito_xac = 1 and cm.tb_credito_est = 3 and cu.tb_creditotipo_id = 1
                    AND cu.tb_cuota_fec BETWEEN :fecha1 AND :fecha2
                    AND cu.tb_cuota_est IN (1,3) AND tb_cuota_xac = 1".$cond_sql;
            if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
            $sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_asiveh($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
              }
            }
            $sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id

                WHERE cm.tb_credito_xac = 1 AND (cm.tb_credito_tip1 = 1 OR cm.tb_credito_tip1 = 4) AND cu.tb_creditotipo_id = 2
                AND cm.tb_credito_est NOT IN(1,2,4,7,8) AND cd.tb_cuotadetalle_estap = 0 AND cd.tb_cuotadetalle_acupag = 0
                AND cu.tb_cuota_acupag = 0 AND cu.tb_cuota_est != 2
                AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 " .$cond_sql;

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc, tb_cuota_cuo desc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_asiveh_acupag($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())  ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
		$sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cu.tb_creditotipo_id = 2 AND cm.tb_credito_est NOT IN(4,8)
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 and cu.tb_cuota_acupag = 1 AND cu.tb_cuota_est in(1,3)" .$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_asiveh_adendas($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
             $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())  ";
                  }else{
                    $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
                  }
                }
		$sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cu.tb_creditotipo_id = 2 AND cm.tb_credito_est NOT IN(1,2,4,7,8)
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 and cm.tb_credito_tip1 = 2 AND cu.tb_cuota_est in(1,3)" .$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_garveh($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
		$sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_int, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN (1,2,4,7,8)  AND cu.tb_creditotipo_id = 3
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 AND cd.tb_cuotadetalle_estap = 0 AND cd.tb_cuotadetalle_acupag = 0
		AND cu.tb_cuota_acupag = 0 AND cu.tb_cuota_est IN(1,3) ".$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_garveh_acupag($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
		$sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN (1,2,4,7,8)  AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_acupag = 1
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2
		AND cu.tb_cuota_est IN(1,3) ".$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_hipotecario($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
            $sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_int,cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
              FROM tb_creditohipo cm
              INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
              INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
              INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
              INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
              INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
              INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
              WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN(1,2,4,7,8) AND cu.tb_creditotipo_id = 4
              AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 AND cd.tb_cuotadetalle_estap = 0 AND cd.tb_cuotadetalle_acupag = 0 AND cu.tb_cuota_acupag = 0 AND cu.tb_cuota_est != 2 ".$cond_sql;

            if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
            if($clientes != '')
                $sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

            $sql.=" GROUP BY cu.tb_cuota_id";
            $sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_hipotecario_acupag($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
            $sql.=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
              FROM tb_creditohipo cm
              INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
              INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
              INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
              INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
              INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
              INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
              WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN(4,7,8) AND cu.tb_creditotipo_id = 4
              AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 AND cu.tb_cuota_acupag = 1 AND cu.tb_cuota_est != 2 ".$cond_sql;

            if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
            if($clientes != '')
                $sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

            $sql.=" GROUP BY cu.tb_cuota_id";
            $sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function verificarAlerta($tipCre, $dia){
      try {
            $sql="SELECT *
		FROM tb_creditoalerta
		WHERE tb_creditotipo_id = :tb_creditotipo_id AND tb_creditoalerta_val = :tb_creditoalerta_val";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_creditotipo_id", $tipCre, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_creditoalerta_val", $dia, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function verificarEnvioAlerta($tipCuo, $dia, $idcuo){
      try {
            $sql="SELECT *
                    FROM 
                        tb_cuotaalerta c1
                        INNER JOIN tb_creditoalerta c2 ON c1.tb_creditoalerta_id = c2.tb_creditoalerta_id
                    WHERE 
                        c1.tb_cuotaalerta_tip = :tb_cuotaalerta_tip AND c2.tb_creditoalerta_val = :tb_creditoalerta_val AND c1.tb_cuotaalerta_cuoid = :tb_cuotaalerta_cuoid";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuotaalerta_tip", $tipCuo, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_creditoalerta_val", $dia, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_cuotaalerta_cuoid", $idcuo, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
     function registrarEnvioAlerta($cuoale, $tipCuo, $idcuo){
         $tb_cuotaalerta_est=1;
      $this->dblink->beginTransaction();
      try {
        $sql="
		INSERT INTO tb_cuotaalerta
		(
		tb_creditoalerta_id,
		tb_cuotaalerta_tip,
		tb_cuotaalerta_cuoid,
		tb_cuotaalerta_est
		)
		VALUES
		(
                :tb_creditoalerta_id,
		:tb_cuotaalerta_tip,
		:tb_cuotaalerta_cuoid,
		:tb_cuotaalerta_est);";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_creditoalerta_id", $cuoale, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaalerta_tip", $tipCuo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaalerta_cuoid", $idcuo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaalerta_est", $tb_cuotaalerta_est, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarDetalleAsiveh($id, $id2){
      try {
          $sql="SELECT *
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_id=:tb_cuotadetalle_id and cd.tb_cuotadetalle_est <> 2";  
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_id", $id2, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function mostrarDetalleGarveh($id, $id2){
      try {
          $sql="SELECT *
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_id=:tb_cuotadetalle_id and cd.tb_cuotadetalle_est <> 2";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_id", $id2, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function mostrarPagadoAsiveh($id){
      try {
          $sql="SELECT *
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function mostrarPagadoGarveh($id){
      try {
          $sql="SELECT *
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function filtrar_cuotasvencidas_cliente_creditoasiveh($mon_id, $tip1){
      try {
          $sql="SELECT 
                    cl.tb_cliente_id,cl.tb_cliente_nom, 
                    COUNT(cud.tb_cuotadetalle_id) AS num_cuotasdetalle,
                    SUM(cud.tb_cuotadetalle_cuo) AS mon_total_cuota, 
                    SUM(IFNULL((SELECT SUM(ing.tb_ingreso_imp) 
                FROM 
                    tb_cuotapago cp inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id 
                    where 
                        cp.tb_modulo_id = 2 and ing.tb_ingreso_xac =1 
                        and tb_cuotapago_xac=1 and cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id 
                        GROUP by cp.tb_cuotapago_modid), 0)) AS pagos_parciales 
                    FROM 
                        tb_cliente cl
                        inner join tb_creditoasiveh cre on cre.tb_cliente_id = cl.tb_cliente_id
                        inner join tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id
                        inner join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id
                where 
                        tb_credito_est NOT IN(1,2,4,7,8) and tb_credito_xac = 1 
                        and cuo.tb_creditotipo_id = 2 
                        and cud.tb_cuotadetalle_fec <= NOW() 
                        and cud.tb_cuotadetalle_est != 2 
                        and cud.tb_cuotadetalle_estap = 0 
                        and cre.tb_moneda_id =:tb_moneda_id";
          
                if($tip1 != '')
                    $sql.=' and tb_credito_tip1 in ('.$tip1.')';
                    $sql .=' GROUP BY 1';
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
     function asignar_cliente_colaborador($cli_id, $usu_id, $cre_tip, $det){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cobranzacliente(
                                            tb_cliente_id, 
                                            tb_usuario_id, 
                                            tb_cobranzacliente_cretip,
                                            tb_cobranzacliente_det) 
                                    VALUES (
                                            tb_cliente_id, 
                                            tb_usuario_id, 
                                            tb_cobranzacliente_cretip,
                                            tb_cobranzacliente_det)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzacliente_cretip", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzacliente_det", $det, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
     function eliminar_cliente_colaborador($cli_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cobranzacliente where tb_cliente_id =:tb_cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function clientes_asignados_colaborador($usu_id){
      try {
          $sql = "SELECT GROUP_CONCAT(tb_cliente_id) as cliente_asignado, tb_usuario_id 
                  FROM tb_cobranzacliente where tb_usuario_id =:tb_usuario_id group by tb_usuario_id";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
  }

?>
