        //const placeinput = document.getElementById('place-input');
        var initCoords = {lat: -6.775237499999999, lng: -79.8318281};
        //const mapDiv = document.getElementById("map");
        //const input = document.getElementById("place_input");
        let map;
        let marker;
        //var autocomplete;
        var infowindow;
        let  lat = document.getElementById('txt_latitud_gps').value;
        let  lng = document.getElementById('txt_longitud_gps').value;

        let mapLoaded = false;

        function loadGoogleMapsScript() {
            if (!mapLoaded) {
                const script = document.createElement("script");
                script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyC1NyafdNBeGQnOIeP7W8dIJ13PS6C2Eb0&libraries=places";
                script.defer = true;
                script.async = true;
                document.head.appendChild(script);
                script.onload = () => {
                    mapLoaded = true;
                    initMap();
                };
            } else {
                initMap();
            }
        }

        async function initMap() {
            map = new google.maps.Map(document.getElementById("map"),{
                zoom: 18,
                center: initCoords,
            });
            infowindow = new google.maps.InfoWindow();

            // Agrega el input de búsqueda al mapa
            const input = document.getElementById("pac-input");
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Inicializa Autocomplete y asócialo al input
            const autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo("bounds", map);

            // Escucha el evento place_changed para centrar el mapa en la ubicación seleccionada
            autocomplete.addListener("place_changed", function () {
                infowindow.close();
                const place = autocomplete.getPlace();

                if (!place.geometry) {
                    console.error("El lugar seleccionado no contiene geometría válida.");
                    return;
                }

                // Centra el mapa en la ubicación seleccionada
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Ajusta el zoom si es necesario
                }

                // Agrega un marcador en la ubicación seleccionada
                new google.maps.Marker({
                    map: map,
                    position: place.geometry.location,
                });
            });

            codeAddress();
            //autocomplete = new google.maps.places.Autocomplete(input)
            //console.log(autocomplete);
            //initAutocomplete();
        }

        function codeAddress() {
            var address = document.getElementById('txt_cliente_dir').value;
            if(address != ""){
                console.log(address);
                var geocoder = new google.maps.Geocoder()
                geocoder.geocode( { 'address': address}, function(results, status) {
                    console.log(status);
                    console.log(results);
                    
                  if (status == 'OK') {
                    if(lat == "" || lng == ""){
                        document.getElementById('txt_latitud_gps').value = results[0].geometry.location.lat();
                        document.getElementById('txt_longitud_gps').value = results[0].geometry.location.lng();
                    }
                    map.setCenter(results[0].geometry.location);
                    var marcador = new google.maps.Marker({
                        map: map,
                        title: address,
                        position: results[0].geometry.location,
                        draggable: true,
                    });
                    infowindow.close();
                    infowindow.setContent(address);
    
                    marcador.addListener('dragend', function(event){
                        console.log(event);
                        var latitud = event.latLng.lat();
                        document.getElementById('txt_latitud_gps').value = latitud;
                        var longitud = event.latLng.lng();
                        document.getElementById('txt_longitud_gps').value = longitud;
                        codeCoord(latitud, longitud, marcador);
                    })
                } else if(status == 'ZERO_RESULTS') {
                    setTimeout(swal_warning('AVISO','DIRECCION NO ENCONTRADA, SELECCIONE MANUALMENTE',6000), 3000);
                    
                    var marcador = new google.maps.Marker({
                        title: "PRESTAMOS DEL NORTE",
                        map: map,
                        position: {lat:-6.775237499999999, lng:-79.8318281},
                        draggable: true,
                    });
                    infowindow.close();
                    infowindow.setContent("PRESTAMOS DEL NORTE");

                    marcador.addListener('dragend', function(event){
                        console.log(event);
                        var latitud = event.latLng.lat();
                        document.getElementById('txt_latitud_gps').value = latitud;
                        var longitud = event.latLng.lng();
                        document.getElementById('txt_longitud_gps').value = longitud;
                        codeCoord(latitud, longitud, marcador);
                    })
                }
    
                infowindow.open(map, marcador);
                });
            } else {
                var marcador = new google.maps.Marker({
                    title: "PRESTAMOS DEL NORTE",
                    map: map,
                    position: {lat:-6.775237499999999, lng:-79.8318281},
                    draggable: true,
                });
                infowindow.close();
                infowindow.setContent("PRESTAMOS DEL NORTE");

                marcador.addListener('dragend', function(event){
                    console.log(event);
                    var latitud = event.latLng.lat();
                    document.getElementById('txt_latitud_gps').value = latitud;
                    var longitud = event.latLng.lng();
                    document.getElementById('txt_longitud_gps').value = longitud;
                    codeCoord(latitud, longitud, marcador);
                })
            }

            infowindow.open(map, marcador);
        }

        function codeCoord(latitud, longitud, marcador){
            var latlng = {lat: latitud, lng: longitud};
            var geocoder = new google.maps.Geocoder()
            geocoder.geocode({'location': latlng}, function(results, status) {
                infowindow.close();
                if (status === 'OK') {
                    console.log(results[0]);
                    if (results[0]) {
                        // Obtiene la dirección legible y la establece como título del marcador
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marcador);
                        document.getElementById('txt_cliente_dir').value = results[0].formatted_address;
                    } else {
                        console.log('No se encontraron resultados de geocodificación.');
                    }
                } else {
                    console.log('Error de geocodificación: ' + status);
                }
            });
        }



        function ver_mapa(){
            initMap();
            $("#modal_cliente_mapa").modal("show");
        }


        