<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');

/* require_once ("../../config/Cado.php"); */
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$cliente_id = intval($_GET['cliente_id']);
$cambio_cel = isset($_GET['cel']) ? trim($_GET['cel']) : '';
$cambio_cel2 = isset($_GET['cel2']) ? trim($_GET['cel2']) : '';
$cambio_cel3 = isset($_GET['cel3']) ? trim($_GET['cel3']) : '';

$title = 'Cambio de Numeros DJ';
$codigo = 'CLI-'.str_pad($cliente_id, 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {

  public function Header() {
    $image_file = K_PATH_IMAGES.'logo.jpg';
    $this->Image($image_file, 10, 10, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  //$pdf->setLanguageArray();

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$result = $oCliente->mostrarUno($cliente_id);
  if($result['estado'] == 1){
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
    $cliente_dir = $result['data']['tb_cliente_dir'];
    $cliente_emprs = $result['data']['tb_cliente_emprs'];
    $cliente_tel = $result['data']['tb_cliente_tel']; //numero de celular principal
    $cliente_cel = $result['data']['tb_cliente_cel']; //referencia 1
    $cliente_telref = $result['data']['tb_cliente_telref']; // referencia 2
    $cliente_estciv = $result['data']['tb_cliente_estciv']; // 1 soltero, 2 casado
    $cliente_ubige = $result['data']['tb_ubigeo_cod']; //ubigeo d<b>EL CLIENTE</b>
  }
$result = NULL;

$estado_civil = 'SOLTERO(A)';
if($cliente_estciv == 2) $estado_civil = 'CASADO(A)';
if($cliente_estciv == 3) $estado_civil = 'DIVORCIADO(A)';
if($cliente_estciv == 4) $estado_civil = 'VIUDO(A)';

if($cliente_ubige  >0){
  $result = $oUbigeo->mostrarUbigeo($cliente_ubige);
    if($result['estado'] == 1){
      $cliente_dep = $result['data']['Departamento'];
      $cliente_pro = $result['data']['Provincia'];
      $cliente_dis = $result['data']['Distrito'];
    }
  $result = NULL;
}

$celular_cambiado_1 = '';
$celular_cambiado_2 = '';
$celular_cambiado_3 = '';

if($cambio_cel != '')
  $celular_cambiado_1 = '<p>EL CELULAR N° <b>'.$cliente_tel.'</b>, SIENDO ACTUALMENTE EL CELULAR N° <b>'.$cambio_cel.'</b></p>';
if($cambio_cel2 != '')
  $celular_cambiado_2 = '<p>EL CELULAR N° <b>'.$cliente_cel.'</b>, SIENDO ACTUALMENTE EL CELULAR N° <b>'.$cambio_cel2.'</b></p>';
if($cambio_cel3 != '')
  $celular_cambiado_3 = '<p>EL CELULAR N° <b>'.$cliente_telref.'</b>, SIENDO ACTUALMENTE EL CELULAR N° <b>'.$cambio_cel3.'</b></p>';

list($day, $month, $year) = explode('-', date('d-m-Y'));

$html = '
  <p align="center"><strong><u>DECLARACIÓN JURADA</u></strong></p>
  <p align="justify">YO, '.$cliente_nom.', QUIEN DECLARO SER DE NACIONALIDAD PERUANA, MAYOR DE EDAD, IDENTIFICADO(A) CON DNI N° '.$cliente_doc.', ESTADO CIVIL '.$estado_civil.', CON DOMICILIO EN '.$cliente_dir.' DEL DISTRITO DE '.$cliente_dis.', PROVINCIA DE '.$cliente_pro.', DEL DEPARTAMENTO DE '.$cliente_dep.'; DECLARO BAJO JURAMENTO QUE: HE CAMBIADO DE NÚMERO TELEFÓNICO, HABIENDO CONSIGNADO ANTES:</p>
  '.$celular_cambiado_1. $celular_cambiado_2. $celular_cambiado_3 .'
  <p align="justify">EN ESE SENTIDO, SOLICITO SE MODIFIQUE MI NÚMERO DE CONTACTO A FIN DE QUE ME PUEDAN LLEGAR LAS COMUNICACIONES DE LA EMPRESA INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</p>

  <p align="justify">FIRMANDO EL INTERVINIENTE, EN SEÑAL DE PLENA CONFORMIDAD EN LA CIUDAD DE CHICLAYO, A LOS '.$day.' DÍAS, DEL MES DE '.strtoupper(nombre_mes($month)).' DEL '.$year.'.</p>

  <p></p>
  <p></p>
  <p></p>
  <p></p>
  <table style="width:100%; text-align: center;">
    <tr>
      <td style="width:100%; text-align: center;"><b>
        ______________________________________<br/>
        '.$cliente_nom.' <br/>
        '.$cliente_doc.'</b>
      </td>
    </tr>
  </table>
';

// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$codigo."_46464".$title.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>