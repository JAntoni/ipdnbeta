<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'cliente';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cliente_id = $_POST['cliente_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Cliente Registrado';
}
if ($usuario_action == 'I') {
    $titulo = 'Registrar Cliente';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Cliente';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Cliente';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

// GERSON 23-11-23
$proceso_id = 0;
$cliente_doc = '';
$cliente_nom = '';
$cliente_cel = '';
$precliente_id = 0;
$readonly = '';
$readonly_mod = '';
$disabled = '';
if($vista == "proceso"){
  $proceso_id = intval($_POST['proceso_id']);
  $cliente_doc = $_POST['cliente_dni'];
  $cliente_nom = $_POST['cliente_nom'];
  $cliente_cel = $_POST['cliente_cel'];
  $precliente_id = $_POST['precliente_id'];

  // pasar funcion a cliente.class
  $cant_cred = $oProceso->mostrarCreditosActivosXCliente($cliente_id);
  if($cant_cred['estado'] == 1){
    if($cant_cred['data']['creditos'] > 0){
      $readonly = 'readonly';
      $readonly_mod = 'readonly';
      $disabled = 'disabled';
    }else{
      //$readonly = 'readonly';
      //$disabled = 'disabled';
    }
  }else{
    //$readonly = 'readonly';
    //$disabled = 'disabled';
  }
  
  //ARMAR ARREGLO QUE EN EL CONTROLLER SE VALIDARÁ
  if (!empty($precliente_id)) {
    require_once('../precliente/Precliente.class.php');
    $oPrecli = new Precliente();

    $wh[0]['column_name'] = 'tb_precliente_id';
    $wh[0]['param0'] = $precliente_id;
    $wh[0]['datatype'] = 'INT';

    $res = $oPrecli->listar_todos($wh, array(), array());

    if ($res['estado'] == 1) {
      $pcl_reg = $res['data'][0];
      
      if (empty($cliente_id)) {
        $cliente_doc = $pcl_reg['tb_precliente_numdoc'];
        $cliente_nom = $pcl_reg['tb_precliente_nombres'];
        $cliente_tel = $pcl_reg['tb_precliente_numcel'];
        $mediocom_id = $pcl_reg['tb_mediocom_id'];
        $creditotipo_id = 1;
      }
    }
    unset($wh, $res);
  }

}
//

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cliente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cliente'; $modulo_id = $cliente_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cliente por su ID
    $cliente_tip = 0;

    if(intval($cliente_id) > 0){
      $result = $oCliente->mostrarUno($cliente_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el cliente seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $cliente_tip = intval($result['data']['tb_cliente_tip']);
          $cliente_emp = intval($result['data']['tb_cliente_emp']);
          $cliente_ref = intval($result['data']['tb_cliente_ref']);
          $cliente_seg = intval($result['data']['tb_cliente_seg']);
          $cliente_vis = intval($result['data']['tb_cliente_vis']);
          $cliente_des = intval($result['data']['tb_cliente_des']);

          $cliente_empruc = $result['data']['tb_cliente_empruc'];
          $cliente_emprs = $result['data']['tb_cliente_emprs'];
          $cliente_empdir = $result['data']['tb_cliente_empdir'];
          $cliente_empger = $result['data']['tb_cliente_empger'];
          $cliente_empact = $result['data']['tb_cliente_empact'];

          $cliente_doc = $result['data']['tb_cliente_doc'];
          $cliente_nom = $result['data']['tb_cliente_nom'];
          //$cliente_con = $result['data']['tb_cliente_con'];
          $cliente_tel = $result['data']['tb_cliente_tel'];
          $cliente_cel = $result['data']['tb_cliente_cel'];
          $cliente_telref = $result['data']['tb_cliente_telref'];
          $cliente_protel = $result['data']['tb_cliente_protel'];
          $cliente_procel = $result['data']['tb_cliente_procel'];
          $cliente_protelref = $result['data']['tb_cliente_protelref'];

          $cliente_ema = $result['data']['tb_cliente_ema'];
          $cliente_dir = $result['data']['tb_cliente_dir'];
          $cliente_refdir = $result['data']['tb_cliente_refdir'];
          $cliente_dir2 = $result['data']['tb_cliente_dir2'];
          $cliente_refdir2 = $result['data']['tb_cliente_refdir2'];
          $cliente_fecnac = mostrar_fecha($result['data']['tb_cliente_fecnac']);
          $cliente_estciv = $result['data']['tb_cliente_estciv'];
          $cliente_cadest = $result['data']['tb_cliente_cadest'];

          $ubigeo_cod = $result['data']['tb_ubigeo_cod'];
          $ubigeo_cod2 = $result['data']['tb_ubigeo_cod2'];
          $ubigeo_cod3 = $result['data']['tb_ubigeo_cod3'];
          $mediocom_id = $result['data']['tb_mediocom_id'];
          $creditotipo_id = $result['data']['tb_creditotipo_id'];

          $zona_id = $result['data']['tb_zona_id'];
          $cliente_numpar = $result['data']['tb_cliente_numpar'];
          $cliente_bien = $result['data']['tb_cliente_bien'];
          $cliente_firm = $result['data']['tb_cliente_firm'];
          $cliente_sexo = intval($result['data']['tb_cliente_sexo']);
          
          $cliente_con=$result['data']['tb_cliente_con'];
          $cliente_feccad = mostrar_fecha($result['data']['tb_cliente_feccad']); //fecha caducidad
          $cliente_ocupacion = $result['data']['tb_cliente_ocu'];
          $cliente_nacimiento = $result['data']['tb_cliente_lugnac']; //lugar de nacimiento
          $cliente_fondos = $result['data']['tb_cliente_fon']; //de donde provienen los fondos
          $cliente_dj = $result['data']['dj_file']; // declaracion jurada subida

          $ubigeo_coddep1 = substr($ubigeo_cod, 0, 2);
          $ubigeo_codpro1 = substr($ubigeo_cod, 2, 2);
          $ubigeo_coddis1 = substr($ubigeo_cod, 4, 2);

          $ubigeo_coddep2 = substr($ubigeo_cod2, 0, 2);
          $ubigeo_codpro2 = substr($ubigeo_cod2, 2, 2);
          $ubigeo_coddis2 = substr($ubigeo_cod2, 4, 2);

          $ubigeo_coddep3 = substr($ubigeo_cod3, 0, 2);
          $ubigeo_codpro3 = substr($ubigeo_cod3, 2, 2);
          $ubigeo_coddis3 = substr($ubigeo_cod3, 4, 2);

          $cliente_latitud_gps = $result['data']['tb_cliente_latitud'];
          $cliente_longitud_gps = $result['data']['tb_cliente_longitud'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>

<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cliente" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cliente" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" id="cliente_vista" name="cliente_vista" value="<?php echo $vista;?>">
          <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id;?>">
          <input type="hidden" name="hdd_pcl_reg" id="hdd_pcl_reg" value='<?php echo json_encode($pcl_reg);?>'>

          <div class="modal-body">
            <div class="row">
              <!-- GRUPO 1 DATOS DEL CLIENTE-->
              <div class="col-md-6">
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Datos del Cliente
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label for="txt_cliente_tip" class="control-label">Persona</label>
                            <br>
                            <label class="radio-inline" style="padding-left: 0px;">
                              <input type="radio" name="txt_cliente_tip" class="flat-green" value="1" <?php if($cliente_tip == 1 || $cliente_tip == 0) echo 'checked';?> > Natural
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="txt_cliente_tip" class="flat-green" value="2" <?php if($cliente_tip == 2) echo 'checked';?> > Jurídica
                            </label>
                          </div>
                          <div class="col-md-6">
                            <label for="txt_cliente_sexo" class="control-label">Sexo</label>
                            <br>
                            <label class="radio-inline" style="padding-left: 0px;">
                              <input type="radio" name="txt_cliente_sexo" class="flat-green" value="1" <?php if($cliente_sexo == 1) echo 'checked';?> > Masculino
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="txt_cliente_sexo" class="flat-green" value="2" <?php if($cliente_sexo == 2) echo 'checked';?> > Femenino
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="che_cliente_emp" class="control-label">Estados del Cliente</label>
                        <br>
                        <label class="checkbox-inline" style="padding-left: 0px;">
                          <input type="checkbox" name="che_cliente_emp" id="che_cliente_emp" value="1" class="flat-green" <?php if($cliente_emp==1) echo "checked";?> > Empresa
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_ref" id="che_cliente_ref" value="1" class="flat-green" <?php if($cliente_ref==1) echo "checked";?> > Referido
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_seg" id="che_cliente_seg" value="1" class="flat-green" <?php if($cliente_seg==1) echo "checked";?> > Seguimiento
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_vis" id="che_cliente_vis" value="1" class="flat-green" <?php if($cliente_vis==1) echo "checked";?> > Visitante
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_des" id="che_cliente_des" value="1" class="flat-green" <?php if($cliente_des==1) echo "checked";?> > Descartado
                        </label>
                      </div>
                    </div>

                    <!-- DATOS PARA PERSONA JURIDICA-->
                    <div class="shadow datos_juridica">
                      <div class="form-group datos_juridica">
                        <label for="txt_cliente_doc" class="control-label">RUC de la Empresa</label>
                        <input type="text" name="txt_cliente_empruc" id="txt_cliente_empruc" class="form-control input-sm" value="<?php echo $cliente_empruc;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group datos_juridica">
                        <label for="txt_cliente_doc" class="control-label">Razón Social</label>
                        <input type="text" name="txt_cliente_emprs" id="txt_cliente_emprs" class="form-control input-sm mayus" value="<?php echo $cliente_emprs;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group datos_juridica">
                        <label for="txt_cli_empact" class="control-label">Actividad de la Empresa</label>
                      <input type="text" name="txt_cli_empact" id="txt_cli_empact" class="form-control input-sm" value="<?php echo $cliente_empact;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group datos_juridica">
                        <label for="cmb_cliente_empger" class="control-label">Tipo de Gerente</label>
                        <select name="cmb_cliente_empger" id="cmb_cliente_empger" class="form-control input-sm" <?php echo $readonly;?>>
                          <option value="0"></option>
                          <option value="Gerente General" <?php if($cliente_empger == 'GERENTE GENERAL') echo 'selected';?> >Gerente General</option>
                          <option value="Titular Gerente" <?php if($cliente_empger == 'TITULAR GERENTE') echo 'selected';?> >Titular Gerente</option>
                          <option value="Gerente Administrativo" <?php if($cliente_empger == 'GERENTE ADMINISTRATIVO') echo 'selected';?> >Gerente Administrativo</option>
                        </select>
                      </div>
                      <div class="form-group datos_juridica">
                        <label for="txt_cli_empdir" class="control-label">Dirección de la Empresa</label>
                        <input type="text" name="txt_cliente_empdir" id="txt_cliente_empdir" class="form-control input-sm" value="<?php echo $cliente_empdir;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group datos_juridica">
                        <div class="row">
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddep3" class="control-label">Departamento</label>
                            <select name="cmb_ubigeo_coddep3" id="cmb_ubigeo_coddep3" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_coddep = $ubigeo_coddep3;?>
                              <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_codpro3" class="control-label">Provincia</label>
                            <select name="cmb_ubigeo_codpro3" id="cmb_ubigeo_codpro3" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_codpro = $ubigeo_codpro3;?>
                              <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddis3" class="control-label">Distrito</label>
                            <select name="cmb_ubigeo_coddis3" id="cmb_ubigeo_coddis3" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_coddis = $ubigeo_coddis3;?>
                              <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- FIN DATOS DE PERSONA JURIDICA-->

                    
                    <div class="shadow">
                      <div class="form-group">
                        <label for="txt_cliente_doc" class="control-label">DNI del Cliente</label>
                        <input type="text" name="txt_cliente_doc" id="txt_cliente_doc" class="form-control input-sm" value="<?php echo $cliente_doc;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group">
                        <label for="txt_cliente_nom" class="control-label">Nombres del Cliente</label>
                        <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm mayus" value="<?php echo $cliente_nom;?>" <?php echo $readonly;?>>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_tel" class="control-label">Teléfono</label>
                            <input type="text" name="txt_cliente_tel" id="txt_cliente_tel" class="form-control input-sm" value="<?php echo $cliente_tel;?>" <?php echo $readonly_mod;?>>
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_cel" class="control-label">Celular</label>
                            <input type="text" name="txt_cliente_cel" id="txt_cliente_cel" class="form-control input-sm" value="<?php echo $cliente_cel;?>" <?php echo $readonly_mod;?>>
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_telref" class="control-label">Telefono Referencia</label>
                            <input type="text" name="txt_cliente_telref" id="txt_cliente_telref" class="form-control input-sm" value="<?php echo $cliente_telref;?>" <?php echo $readonly_mod;?>>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_protel" class="control-label">Propietario</label>
                            <input type="text" name="txt_cliente_protel" id="txt_cliente_protel" class="form-control input-sm" value="<?php echo $cliente_protel;?>" <?php echo $readonly;?>>
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_procel" class="control-label">Propietario</label>
                            <input type="text" name="txt_cliente_procel" id="txt_cliente_procel" class="form-control input-sm" value="<?php echo $cliente_procel;?>" <?php echo $readonly;?>>
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_protelref" class="control-label">Propietario</label>
                            <input type="text" name="txt_cliente_protelref" id="txt_cliente_protelref" class="form-control input-sm" value="<?php echo $cliente_protelref;?>" <?php echo $readonly;?>>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="txt_cliente_ema" class="control-label">Email del Cliente</label>
                        <input type="text" name="txt_cliente_ema" id="txt_cliente_ema" class="form-control input-sm" value="<?php echo $cliente_ema;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group">
                        <label for="txt_cli_con" class="control-label">Contacto</label>
                        <input type="text" name="txt_cli_con" id="txt_cli_con" class="form-control input-sm" value="<?php echo $cliente_con;?>" <?php echo $readonly;?>>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_fecnac" class="control-label">Fecha de Nacimiento</label>
                            <div class="input-group date" id="datetimepicker1_cliente">
                              <input type='text' class="form-control" name="txt_cliente_fecnac" id="txt_cliente_fecnac" value="<?php echo $cliente_fecnac;?>" <?php echo $readonly;?>/>
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="cmb_cliente_estciv" class="control-label">Estado Civil</label>
                            <select name="cmb_cliente_estciv" id="cmb_cliente_estciv" class="form-control input-sm" <?php echo $readonly;?>>
                              <option value="0"></option>
                              <option value="1" <?php if($cliente_estciv == 1) echo "selected";?> >Soltero</option>
                              <option value="2" <?php if($cliente_estciv == 2) echo "selected";?> >Casado</option>
                              <option value="3" <?php if($cliente_estciv == 3) echo "selected";?> >Divorciado</option>
                              <option value="4" <?php if($cliente_estciv == 4) echo "selected";?> >Viudo</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cli_feccad" class="control-label">Fecha de Caducidad</label>
                            <div class='input-group date' id='datetimepicker2_cliente'>
                              <input type='text' class="form-control" name="txt_cli_feccad" id="txt_cli_feccad" value="<?php echo $cliente_feccad;?>" <?php echo $readonly;?>/>
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cli_feccad" class="control-label">Marcar si DNI no Caduca</label>
                            <div class='input-group date' id='datetimepicker2_cliente'>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="che_cliente_cadest" id="che_cliente_cadest" value="1" class="flat-green" <?php if($cliente_cadest==1) echo "checked";?> > No Caduca
                            </label>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="txt_cli_ocu" class="control-label">Ocupación</label>
                        <input type="text" name="txt_cli_ocu" id="txt_cli_ocu" class="form-control input-sm" value="<?php echo $cliente_ocupacion;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group">
                        <label for="txt_cli_lugnac" class="control-label">Lugar Nacimiento</label>
                        <input type="text" name="txt_cli_lugnac" id="txt_cli_lugnac" class="form-control input-sm" value="<?php echo $cliente_nacimiento;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group">
                        <label for="txt_cli_fon" class="control-label">Origen Fondos</label>
                        <input type="text" name="txt_cli_fon" id="txt_cli_fon" class="form-control input-sm" value="<?php echo $cliente_fondos;?>" <?php echo $readonly;?>>
                      </div>
                      <div class="form-group">            
                        <div class="row">
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddep" class="control-label">Departamento</label>
                            <select name="cmb_ubigeo_coddep" id="cmb_ubigeo_coddep" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_coddep = $ubigeo_coddep1;?>
                              <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_codpro" class="control-label">Provincia</label>
                            <select name="cmb_ubigeo_codpro" id="cmb_ubigeo_codpro" class="form-control input-sm" <?php echo $readonly;?>>
                                <?php $ubigeo_codpro = $ubigeo_codpro1;?>
                                <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddis" class="control-label">Distrito</label>
                            <select name="cmb_ubigeo_coddis" id="cmb_ubigeo_coddis" class="form-control input-sm" <?php echo $readonly;?>>
                                <?php $ubigeo_coddis = $ubigeo_coddis1;?>
                                <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="txt_cliente_dir" class="control-label">Dirección del Cliente</label>
                        <input type="text" name="txt_cliente_dir" id="txt_cliente_dir" class="form-control input-sm" value="<?php echo $cliente_dir;?>" <?php echo $readonly_mod;?>>
                      </div>
                      <div class="form-group">
                        <label for="txt_cli_ref" class="control-label">Referencia</label>
                        <textarea class="form-control input-sm" name="txt_cli_ref" id="txt_cli_ref" rows="2" cols="70" <?php echo $readonly;?>><?php echo $cliente_refdir?></textarea>
                      </div>
                      <div class="row">
                        <div class="form-group">            
                          <div class="col-md-5">
                            <label for="txt_latitud_gps" class="control-label">Latitud</label>
                            <input type="text" name="txt_latitud_gps" id="txt_latitud_gps" class="form-control input-sm" value="<?php echo $cliente_latitud_gps;?>" readonly>
                          </div>
                          <div class="col-md-5">
                            <label for="txt_longitud_gps" class="control-label">Longitud</label>
                            <input type="text" name="txt_longitud_gps" id="txt_longitud_gps" class="form-control input-sm" value="<?php echo $cliente_longitud_gps;?>" readonly>
                          </div>
                          <div class="col-md-2">
                            <br>
                            <p></p>
                            <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="ver_mapa()" title="ABRIR MAPA"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- GRUPO 2 DEL FORMULARIO-->
              <div class="col-md-6">
                <!-- MEDIO COMUNICACION E INTERESES-->
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Medios e Intereses
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="form-group">
                        <label for="cmb_mediocom_id" class="control-label">Medio de Comununicación</label>
                        <select name="cmb_mediocom_id" id="cmb_mediocom_id" class="form-control input-sm" <?php echo $readonly;?>>
                          <?php require_once('../mediocom/mediocom_select.php');?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="cmb_creditotipo_id" class="control-label">Crédito de Interés</label>
                        <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm" <?php echo $readonly;?>>
                          <?php require_once('../creditotipo/creditotipo_select.php');?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <!-- SOCIEDAD DEL CLIENTE-->
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Sociedad del Cliente
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="row" id="group_casado" style="display: none;">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="cmb_cliente_bien" class="control-label">Tipo de Bienes</label>
                            <select name="cmb_cliente_bien" id="cmb_cliente_bien" class="form-control input-sm" <?php echo $readonly;?>>
                              <option value="0"></option>
                              <option value="1" <?php if($cliente_bien == 1) echo "selected";?> >Separacion de Bienes</option>
                              <option value="2" <?php if($cliente_bien == 2) echo "selected";?> >Sin Separación de Bienes</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="cmb_cliente_firm" class="control-label">Forma de Firma</label>
                            <select name="cmb_cliente_firm" id="cmb_cliente_firm" class="form-control input-sm" <?php echo $readonly;?>>
                              <option value="0"></option>
                              <option value="1" <?php if($cliente_firm == 1) echo "selected";?>>Individual</option>
                              <option value="2" <?php if($cliente_firm == 2) echo "selected";?>>Con Cónyuge</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="cmb_zona_id" class="control-label">Zona Registral</label>
                            <select name="cmb_zona_id" id="cmb_zona_id" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php require_once('../zona/zona_select.php');?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_numpar" class="control-label">N° de Partida</label>
                            <input type="text" name="txt_cliente_numpar" id="txt_cliente_numpar" class="form-control input-sm mayus" value="<?php echo $cliente_numpar;?>" <?php echo $readonly;?>>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_fil_nom" class="control-label">Busque un Cliente</label>
                            <input type="text" name="txt_cliente_fil_nom" id="txt_cliente_fil_nom" class="form-control input-sm mayus" <?php echo $readonly;?>>
                            <input type="hidden" id="txt_cliente_fil_id">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="cmb_cliente_tipsoc" class="control-label">Sociedad</label>
                            <select name="cmb_cliente_tipsoc" id="cmb_cliente_tipsoc" class="form-control input-sm" <?php echo $readonly;?>>
                              <option value="0"></option>
                              <option value="1">Sociedad Conyugal</option>
                              <option value="2">Co-Propietario</option>
                              <option value="3">Apoderado</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="btn_sociedad_add" class="control-label">Agregar</label>
                            <a href="" id="btn_sociedad_add" class="btn btn-info btn-sm">Add</a>
                          </div>
                        </div>
                      </div>
                      <div id="div_sociedad_list">
                        <?php
                          if($action == 'modificar'){
                            $result = $oCliente->listar_sociedad_cliente($cliente_id);
                              if($result['estado'] == 1){
                                foreach ($result['data'] as $key => $value) {
                                  echo '
                                    <section class="row" style="margin-bottom: 10px;">
                                      <div class="col-md-6">
                                        <input type="text"  class="form-control input-sm mayus" value="'.$value['tb_cliente_nom'].'" disabled>
                                        <input type="hidden" name="txt_cliente_id2[]" value="'.$value['tb_cliente_id2'].'">
                                      </div>
                                      <div class="col-md-4">
                                        <label class="control-label">'.$value['tipo'].'</label>
                                        <input type="hidden" name="txt_clientedetalle_tip[]" value="'.$value['tb_clientedetalle_tip'].'">
                                      </div>
                                      <div class="col-md-2">
                                        <a href="" class="btn btn-danger btn-sm btn_eliminar_soc">Eliminar</a>
                                      </div>
                                    </section>';
                                }
                              }
                            $result = NULL;
                          }
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
                <br>

                <div class="panel panel-info">
                  <div class="panel-heading">
                    Nuevas Direcciones
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="row">
                        <div class="form-group">
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddep2" class="control-label">Departamento</label>
                            <select name="cmb_ubigeo_coddep2" id="cmb_ubigeo_coddep2" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_coddep = $ubigeo_coddep2;?>
                              <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_codpro2" class="control-label">Provincia</label>
                            <select name="cmb_ubigeo_codpro2" id="cmb_ubigeo_codpro2" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_codpro = $ubigeo_codpro2;?>
                              <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddis2" class="control-label">Distrito</label>
                            <select name="cmb_ubigeo_coddis2" id="cmb_ubigeo_coddis2" class="form-control input-sm" <?php echo $readonly;?>>
                              <?php $ubigeo_coddis = $ubigeo_coddis2;?>
                              <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <label for="txt_cli_dir2" class="control-label">Direccion Cobranza</label>
                          <div class="form-group">
                            <input type="text" id="txt_cli_dir2" name="txt_cli_dir2" class="form-control input-sm mayus" value="<?php echo $cliente_dir2?>" <?php echo $readonly;?>/>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <label for="txt_cli_ref2" class="control-label">Referencia Cobranza</label>
                          <div class="form-group">
                            <textarea class="form-control input-sm" name="txt_cli_ref2" id="txt_cli_ref2" rows="2" cols="70" <?php echo $readonly;?>><?php echo $cliente_refdir2?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- GERSON (29-05-24) -->
                <?php if($vista == "proceso"){ ?>
                  <?php if($cliente_dj == "..." || $cliente_dj == ''){ ?>
                    <br/>
                    <div class="panel panel-info">
                      <div class="panel-heading">
                        Declaración Jurada
                      </div>
                      <div class="panel-body">
                        <div class="shadow">
                          <div class="row">
                            <div class="form-group">
                              <div class="col-md-12">
                                <label class="control-label">Suba Documento:</label>
                                <input type="file" class="form-control input-sm" name="dj_file" id="dj_file" accept="application/pdf">
                              </div>
                            </div>
                          </div>
                          <br>
                        </div>
                      </div>
                    </div>
                  <?php }else{ 
                      $url = explode("public/pdf/dj_cliente/", $cliente_dj);
                      $ruta = $url[0];
                      $archivo = $url[1];
                    ?>
                    <br/>
                    <div class="panel panel-info">
                      <div class="panel-heading">
                        Declaración Jurada
                      </div>
                      <div class="panel-body">
                        <div class="shadow">
                          <div class="row">
                            <div class="col-md-12">
                              <p class="message" style="font-family:cambria">
                                  <a href="<?php echo $cliente_dj ?>" class="name"  target="_blank"><?php echo $archivo ?><i class="fa fa-file-pdf-o" title="VER"></i> 
                                  </a>
                                  <small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminarDJ(<?php echo $cliente_id ?>)">Eliminar <i class="fa fa-trash"></i></a></small>
                              </p>
                            </div>
                          </div>
                          <br>
                        </div>
                      </div>
                    </div>

                  <?php } ?>
                <?php } ?>
                <!--  -->

              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cliente?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cliente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>

          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cliente">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="modal fade" id="modal_cliente_mapa" tabindex="-1">
    <div class="modal-dialog modal-lg modal-simple">
        <div class="modal-content p-3 p-md-5">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
              <h4 class="modal-title">UBICACION</h4>
            </div>
            <div class="modal-body">
              <!--<input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">-->
                <div id="map" style="width: 100%; height: 650px"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/cliente/cliente_form.js?ver=05062025';?>"></script>
<script type="text/javascript" src="vista/cliente/cliente_mapa.js?ver=<?= rand(); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1NyafdNBeGQnOIeP7W8dIJ13PS6C2Eb0&libraries=places&region=PE&language=es">
</script>
