$(document).ready(function() {
 	$('.btn_vista_not').button({
		icons: {primary: "ui-icon-plus"},
		text: true
	});

});

function vista_nota(nota_id, estado){
		$.ajax({
      type: "POST",
      url: VISTA_URL + "cliente/clientenota_reg.php",
      dataType: "html",
      data: ({
      	action: 'estado',
      	nota_id: nota_id,
      	estado: estado
      }),
      beforeSend: function() {
        $('#msj_clientenota').html("Guardando datos...");
        $('#msj_clientenota').show(100);
        $('#btn_nota_'+nota_id).hide(200);
      },
      success: function(data){
        if(data == 'exito'){
          $('#msj_clientenota').text('Lo Nota para el Crédito del Cliente se ha modificado');
          //$('#div_clientenota_form').dialog('close');
        }
        else
          alert(data);
      },
      complete: function(data){
        console.log(data);
        if(data.statusText == "parsererror"){
          console.log(data);
          $('#msj_clientenota').text('ERROR: ' + data.responseText);
        }
      }
    });
}

function guardarnota(){
$.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        dataType: "html",
        data: $('#frm_clientenota').serialize(),
        beforeSend: function() {
          //$('#msj_clientenota').html("Guardando datos...");
          //$('#msj_clientenota').show(100);
        },
        success: function(data){
            console.log(data);
          if(data == 'exito'){
          	alerta_success("EXITO","Lo Nota para el Crédito del Cliente se ha guardado");
            $('#modal_cliente_nota').modal('hide');
          }
          else
            alert(data);
        },
        complete: function(data){
          console.log(data);
          if(data.statusText == "parsererror"){
            console.log(data);
            $('#msj_clientenota').text('ERROR: ' + data.responseText);
          }
        }
      });
    };