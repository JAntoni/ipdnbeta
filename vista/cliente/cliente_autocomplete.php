<?php
  require_once ("Cliente.class.php");
  $oCliente = new Cliente();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $cliente_id;
    var $cliente_nom;
    var $cliente_dir;
    var $cliente_doc;
    var $cliente_fecnac;
    var $cliente_ema;
    var $cliente_tel;
    var $cliente_cel;
    var $cliente_telref;
    var $cliente_tip;

    function __construct($label, $value, $cliente_id, $cliente_nom,$cliente_dir, $cliente_doc, $cliente_fecnac, $cliente_ema, $cliente_tel, $cliente_cel, $cliente_telref, $cliente_tip){ 
      $this->label = $label;
      $this->value = $value;
      $this->cliente_id = $cliente_id;
      $this->cliente_nom = $cliente_nom;
      $this->cliente_dir = $cliente_dir;	
      $this->cliente_doc = $cliente_doc;
      $this->cliente_fecnac = $cliente_fecnac;
      $this->cliente_ema = $cliente_ema;
      $this->cliente_tel = $cliente_tel;
      $this->cliente_cel = $cliente_cel;
      $this->cliente_telref = $cliente_telref;
      $this->cliente_tip = $cliente_tip;  
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $cliente_emp = $_GET['cliente_emp']; // si es 1, solo mostrar los clientes de la empresa, si es 0: todos los clientes

  //busco un valor aproximado al dato escrito
  $result = $oCliente->cliente_autocomplete($datoBuscar, $cliente_emp);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        array_push(
          $arrayElementos, 
          new ElementoAutocompletar(
                  $value["tb_cliente_doc"].'-'.$value["tb_cliente_nom"].' - '.$value["tb_cliente_emprs"],
                  $value["tb_cliente_doc"].'-'.$value["tb_cliente_nom"], 
                  $value["tb_cliente_id"], 
                  $value["tb_cliente_nom"], 
                  $value['tb_cliente_dir'], 
                  $value['tb_cliente_doc'], 
                  $value['tb_cliente_fecnac'], 
                  $value['tb_cliente_ema'], 
                  $value['tb_cliente_tel'], 
                  $value['tb_cliente_cel'], 
                  $value['tb_cliente_telref'], 
                  $value['tb_cliente_tip'])
        );
      }
    }
    print_r(json_encode($arrayElementos));
  $result = NULL;
?>