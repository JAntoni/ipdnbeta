<?php
  require_once('../../core/usuario_sesion.php');
  require_once ("../cliente/Cliente.class.php");
  require_once ("../usuario/Usuario.class.php");
  
  $oCliente = new Cliente();
  $oUsuario = new Usuario();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $titulo = 'Historial de Notas del Crédito';
  $cliente_id = $_POST['cliente_id'];
  $credito_id = $_POST['credito_id'];
  $creditotiponotas = $_POST['creditotiponotas'];
  $subcreditonotasarr = $_POST['subcreditonotas'];
  $subcreditonotas = implode(",",$subcreditonotasarr);
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
  $timeline = '';

  $result = $oCliente->listar_nota_cliente_credito2($cliente_id, $credito_id, $creditotiponotas, $subcreditonotas);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $timeline .= '
          <li>
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($value['tb_notacliente_reg']).'</span>
              
              <div class="user-block separador">
                <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="Usuario imagen">
                <span class="username"><a href="#">'.$value['tb_usuario_nom'].'</a></span>
                <span class="description">'.$value['tb_notacliente_det'].'</span>';

        if($usuariogrupo_id == 2){
          $timeline .= '';
        }
        $timeline .= '
              </div>
            </div>
          </li>
        ';
      }
    }
  $result = NULL;
?>
<style>
  .separador {
    padding: 10px;
    display: block;
    box-shadow: rgb(14 30 37 / 12%) 3px 2px 0px -2px, rgb(14 30 37 / 32%) 2px 2px 17px 3px;
    margin-bottom: 0px;
  }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_notas_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      
      <div class="modal-body">
        <ul class="timeline">
          <?php echo $timeline;?>
        </ul>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>

    </div>
  </div>
</div>