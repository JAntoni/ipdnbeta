<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'cliente';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cliente_id = $_POST['cliente_id'];
  $vista = $_POST['vista'];
  $pcl_id = $_POST['pcl_id'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Cliente Registrado';
}
elseif ($usuario_action == 'I') {
    $titulo = 'Registrar Cliente';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Cliente';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Cliente';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cliente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cliente'; $modulo_id = $cliente_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cliente por su ID
    $cliente_tip = 0;

    if(intval($cliente_id) > 0){
      $result = $oCliente->mostrarUno($cliente_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el cliente seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $cliente_tip = intval($result['data']['tb_cliente_tip']);
          $cliente_emp = intval($result['data']['tb_cliente_emp']);
          $cliente_ref = intval($result['data']['tb_cliente_ref']);
          $cliente_seg = intval($result['data']['tb_cliente_seg']);
          $cliente_vis = intval($result['data']['tb_cliente_vis']);
          $cliente_des = intval($result['data']['tb_cliente_des']);

          $cliente_empruc = $result['data']['tb_cliente_empruc'];
          $cliente_emprs = $result['data']['tb_cliente_emprs'];
          $cliente_empdir = $result['data']['tb_cliente_empdir'];
          $cliente_empger = $result['data']['tb_cliente_empger'];
          $cliente_empact = $result['data']['tb_cliente_empact'];

          $cliente_doc = $result['data']['tb_cliente_doc'];
          $cliente_nom = $result['data']['tb_cliente_nom'];
          //$cliente_con = $result['data']['tb_cliente_con'];
          $cliente_tel = $result['data']['tb_cliente_tel'];
          $cliente_cel = $result['data']['tb_cliente_cel'];
          $cliente_telref = $result['data']['tb_cliente_telref'];
          $cliente_protel = $result['data']['tb_cliente_protel'];
          $cliente_procel = $result['data']['tb_cliente_procel'];
          $cliente_protelref = $result['data']['tb_cliente_protelref'];

          $cliente_ema = $result['data']['tb_cliente_ema'];
          $cliente_dir = $result['data']['tb_cliente_dir'];
          $cliente_refdir = $result['data']['tb_cliente_refdir'];
          $cliente_dir2 = $result['data']['tb_cliente_dir2'];
          $cliente_refdir2 = $result['data']['tb_cliente_refdir2'];
          $cliente_fecnac = mostrar_fecha($result['data']['tb_cliente_fecnac']);
          $cliente_estciv = $result['data']['tb_cliente_estciv'];

          $ubigeo_cod = $result['data']['tb_ubigeo_cod'];
          $ubigeo_cod2 = $result['data']['tb_ubigeo_cod2'];
          $ubigeo_cod3 = $result['data']['tb_ubigeo_cod3'];
          $mediocom_id = $result['data']['tb_mediocom_id'];
          $creditotipo_id = $result['data']['tb_creditotipo_id'];

          $zona_id = $result['data']['tb_zona_id'];
          $cliente_numpar = $result['data']['tb_cliente_numpar'];
          $cliente_bien = $result['data']['tb_cliente_bien'];
          $cliente_firm = $result['data']['tb_cliente_firm'];
          $cliente_sexo = intval($result['data']['tb_cliente_sexo']);
          
          $cliente_con=$result['data']['tb_cliente_con'];
          $cliente_feccad = mostrar_fecha($result['data']['tb_cliente_feccad']); //fecha caducidad
          $cliente_ocupacion = $result['data']['tb_cliente_ocu'];
          $cliente_nacimiento = $result['data']['tb_cliente_lugnac']; //lugar de nacimiento
          $cliente_fondos = $result['data']['tb_cliente_fon']; //de donde provienen los fondos

          $cliente_celadi1 = $result['data']['tb_cliente_celadi1'];
          $cliente_celadi2 = $result['data']['tb_cliente_celadi2'];
          $cliente_celadi3 = $result['data']['tb_cliente_celadi3'];
          $cliente_proceladi1 = $result['data']['tb_cliente_proceladi1'];
          $cliente_proceladi2 = $result['data']['tb_cliente_proceladi2'];
          $cliente_proceladi3 = $result['data']['tb_cliente_proceladi3'];

          $ubigeo_coddep1 = substr($ubigeo_cod, 0, 2);
          $ubigeo_codpro1 = substr($ubigeo_cod, 2, 2);
          $ubigeo_coddis1 = substr($ubigeo_cod, 4, 2);

          $ubigeo_coddep2 = substr($ubigeo_cod2, 0, 2);
          $ubigeo_codpro2 = substr($ubigeo_cod2, 2, 2);
          $ubigeo_coddis2 = substr($ubigeo_cod2, 4, 2);

          $ubigeo_coddep3 = substr($ubigeo_cod3, 0, 2);
          $ubigeo_codpro3 = substr($ubigeo_cod3, 2, 2);
          $ubigeo_coddis3 = substr($ubigeo_cod3, 4, 2);
        }
      $result = NULL;
    } 
    if (!empty($pcl_id)) {
      require_once('../precliente/Precliente.class.php');
      $oPrecli = new Precliente();

      $wh[0]['column_name'] = 'tb_precliente_id';
      $wh[0]['param0'] = $pcl_id;
      $wh[0]['datatype'] = 'INT';

      $res = $oPrecli->listar_todos($wh, array(), array());

      if ($res['estado'] == 1) {
        $pcl_reg = $res['data'][0];
        
        if (empty($cliente_id)) {
          $cliente_doc = $pcl_reg['tb_precliente_numdoc'];
          $cliente_nom = $pcl_reg['tb_precliente_nombres'];
          $cliente_tel = $pcl_reg['tb_precliente_numcel'];
          $mediocom_id = $pcl_reg['tb_mediocom_id'];
          $creditotipo_id = 1;
        }
      }
      unset($wh, $res);
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cliente" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cliente" method="post">
          <input type="hidden" name="action" id="hdd_action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" name="hdd_pcl_reg" id="hdd_pcl_reg" value='<?php echo json_encode($pcl_reg);?>'>
          <input type="hidden" id="cliente_vista" name="cliente_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <div class="row">
              <!-- GRUPO 1 DATOS DEL CLIENTE-->
              <div class="col-md-6">
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Datos del Cliente
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <!-- LE ASIGNAMOS POR DEFECTO EL VALOR DE PERSONA NATURAL-->
                            <input type="hidden" name="txt_cliente_tip" value="1">

                            <label for="txt_cliente_sexo" class="control-label">Sexo</label>
                            <br>
                            <label class="radio-inline" style="padding-left: 0px;">
                              <input type="radio" name="txt_cliente_sexo" class="flat-green" value="1" <?php if($cliente_sexo == 1) echo 'checked';?> > Masculino
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="txt_cliente_sexo" class="flat-green" value="2" <?php if($cliente_sexo == 2) echo 'checked';?> > Femenino
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="che_cliente_emp" class="control-label">Estados del Cliente</label>
                        <br>
                        <label class="checkbox-inline" style="padding-left: 0px;">
                          <input type="checkbox" name="che_cliente_emp" id="che_cliente_emp" value="1" class="flat-green" <?php if($cliente_emp==1) echo "checked";?> > Empresa
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_ref" id="che_cliente_ref" value="1" class="flat-green" <?php if($cliente_ref==1) echo "checked";?> > Referido
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_seg" id="che_cliente_seg" value="1" class="flat-green" <?php if($cliente_seg==1) echo "checked";?> > Seguimiento
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_vis" id="che_cliente_vis" value="1" class="flat-green" <?php if($cliente_vis==1) echo "checked";?> > Visitante
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="che_cliente_des" id="che_cliente_des" value="1" class="flat-green" <?php if($cliente_des==1) echo "checked";?> > Descartado
                        </label>
                      </div>
                    </div>
                    <div class="shadow">
                      <!-- DATOS DEL CLIENTE-->
                      <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12">
                          <div class="form-group">
                            <label for="txt_cliente_doc" class="control-label">DNI del Cliente</label>
                            <input type="text" name="txt_cliente_doc" id="txt_cliente_doc" class="form-control input-sm" value="<?php echo $cliente_doc;?>">
                          </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-12">
                          <div class="form-group">
                            <label for="txt_cliente_nom" class="control-label">Nombres del Cliente</label>
                            <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm mayus" value="<?php echo $cliente_nom;?>">
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_tel" class="control-label">Celular Titular</label>
                            <input type="text" name="txt_cliente_tel" id="txt_cliente_tel" class="form-control input-sm" value="<?php echo $cliente_tel;?>">
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_cel" class="control-label">Celular Referencia 1</label>
                            <input type="text" name="txt_cliente_cel" id="txt_cliente_cel" class="form-control input-sm" value="<?php echo $cliente_cel;?>">
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_telref" class="control-label">Celular Referencia 2</label>
                            <input type="text" name="txt_cliente_telref" id="txt_cliente_telref" class="form-control input-sm" value="<?php echo $cliente_telref;?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_cliente_protel" class="control-label">Propietario</label>
                            <input type="text" name="txt_cliente_protel" id="txt_cliente_protel" class="form-control input-sm" value="<?php echo $cliente_protel;?>">
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_procel" class="control-label">Propietario</label>
                            <input type="text" name="txt_cliente_procel" id="txt_cliente_procel" class="form-control input-sm" value="<?php echo $cliente_procel;?>">
                          </div>
                          <div class="form-group">
                            <label for="txt_cliente_protelref" class="control-label">Propietario</label>
                            <input type="text" name="txt_cliente_protelref" id="txt_cliente_protelref" class="form-control input-sm" value="<?php echo $cliente_protelref;?>">
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-warning">
                        <div class="panel-heading">
                          Más números adicionales (opcional)
                        </div>
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                              <input type="text" id="txt_cliente_celadi1" name="txt_cliente_celadi1" class="form-control input-sm" placeholder="Número adicional 1" value="<?php echo $cliente_celadi1;?>"/>
                            </div>
                            <div class="col-md-6">
                              <input type="text" id="txt_cliente_proceladi1" name="txt_cliente_proceladi1" class="form-control input-sm" placeholder="Propietario" value="<?php echo $cliente_proceladi1;?>"/>
                            </div>
                            <div class="col-md-6">
                              <input type="text" id="txt_cliente_celadi2" name="txt_cliente_celadi2" class="form-control input-sm" placeholder="Número adicional 2" value="<?php echo $cliente_celadi2;?>"/>
                            </div>
                            <div class="col-md-6">
                              <input type="text" id="txt_cliente_proceladi2" name="txt_cliente_proceladi2" class="form-control input-sm" placeholder="Propietario" value="<?php echo $cliente_proceladi2;?>"/>
                            </div>
                            <div class="col-md-6">
                              <input type="text" id="txt_cliente_celadi3" name="txt_cliente_celadi3" class="form-control input-sm" placeholder="Número adicional 3" value="<?php echo $cliente_celadi3;?>"/>
                            </div>
                            <div class="col-md-6">
                              <input type="text" id="txt_cliente_proceladi3" name="txt_cliente_proceladi3" class="form-control input-sm" placeholder="Propietario" value="<?php echo $cliente_proceladi3;?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="txt_cliente_ema" class="control-label">Email del Cliente</label>
                            <input type="text" name="txt_cliente_ema" id="txt_cliente_ema" class="form-control input-sm" value="<?php echo $cliente_ema;?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="txt_cliente_fecnac" class="control-label">Fecha de Nacimiento</label>
                            <div class="input-group date" id="datetimepicker1_cliente">
                              <input type='text' class="form-control" name="txt_cliente_fecnac" id="txt_cliente_fecnac" value="<?php echo $cliente_fecnac;?>"/>
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="cmb_cliente_estciv" class="control-label">Estado Civil</label>
                            <select name="cmb_cliente_estciv" id="cmb_cliente_estciv" class="form-control input-sm">
                              <option value="0"></option>
                              <option value="1" <?php if($cliente_estciv == 1) echo "selected";?> >Soltero</option>
                              <option value="2" <?php if($cliente_estciv == 2) echo "selected";?> >Casado</option>
                              <option value="3" <?php if($cliente_estciv == 3) echo "selected";?> >Divorciado</option>
                              <option value="4" <?php if($cliente_estciv == 4) echo "selected";?> >Viudo</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="txt_cliente_dir" class="control-label">Dirección del Cliente</label>
                        <input type="text" name="txt_cliente_dir" id="txt_cliente_dir" class="form-control input-sm" value="<?php echo $cliente_dir;?>">
                      </div>

                      <div class="row">
                        <div class="form-group">
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddep" class="control-label">Departamento</label>
                            <select name="cmb_ubigeo_coddep" id="cmb_ubigeo_coddep" class="form-control input-sm">
                                <?php $ubigeo_coddep = $ubigeo_coddep1;?>
                                <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_codpro" class="control-label">Provincia</label>
                            <select name="cmb_ubigeo_codpro" id="cmb_ubigeo_codpro" class="form-control input-sm">
                                <?php $ubigeo_codpro = $ubigeo_codpro1;?>
                                <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddis" class="control-label">Distrito</label>
                            <select name="cmb_ubigeo_coddis" id="cmb_ubigeo_coddis" class="form-control input-sm">
                                <?php $ubigeo_coddis = $ubigeo_coddis1;?>
                                <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="txt_cli_ref" class="control-label">Referencia</label>
                        <textarea class="form-control input-sm" name="txt_cli_ref" id="txt_cli_ref" rows="2" cols="70"><?php echo $cliente_refdir?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- GRUPO 2 DEL FORMULARIO-->
              <div class="col-md-6">
                <!-- MEDIO COMUNICACION E INTERESES-->
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Medios e Intereses
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="form-group">
                        <label for="cmb_mediocom_id" class="control-label">Medio de Comununicación</label>
                        <select name="cmb_mediocom_id" id="cmb_mediocom_id" class="form-control input-sm">
                          <?php require_once('../mediocom/mediocom_select.php');?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="cmb_creditotipo_id" class="control-label">Crédito de Interés</label>
                        <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm">
                          <?php require_once('../creditotipo/creditotipo_select.php');?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- PANEL PARA SOLICITAR MODIFICACION DE NÚMEROS DEL CLIENTE-->
                
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Solicitud de Moficación de Números Telefónicos
                  </div>
                  <div class="panel-body">
                    <div class="shadow">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Celular Titular</label>
                            <input type="text" id="txt_original_cel" class="form-control input-sm" readonly value="<?php echo $cliente_tel;?>"/>
                          </div>
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Celular Referencia 1</label>
                            <input type="text" id="txt_original_cel2" class="form-control input-sm" readonly value="<?php echo $cliente_cel;?>"/>
                          </div>
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Celular Referencia 2</label>
                            <input type="text" id="txt_original_cel3" class="form-control input-sm" readonly value="<?php echo $cliente_telref;?>"/>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Cambiar Por</label>
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-hand-o-right"></i></button>
                          </div>
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Cambiar Por</label>
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-hand-o-right"></i></button>
                          </div>
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Cambiar Por</label>
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-hand-o-right"></i></button>
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Celular Titular</label>
                            <div class="input-group input-group-sm">
                              <input type="text" id="txt_cambiarpor_cel" class="form-control input-sm" readonly/>
                              <span class="input-group-btn"><button type="button" class="btn btn-info btn-flat" onclick="boton_cambiarpor('cel')"><i class="fa fa-pencil"></i></button></span>
                            </div>
                            
                          </div>
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Celular Referencia 1</label>
                            <div class="input-group input-group-sm">
                              <input type="text" id="txt_cambiarpor_cel2" class="form-control input-sm" readonly/>
                              <span class="input-group-btn"><button type="button" class="btn btn-info btn-flat" onclick="boton_cambiarpor('cel2')"><i class="fa fa-pencil"></i></button></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cmb_mediocom_id" class="control-label">Celular Referencia 2</label>
                            <div class="input-group input-group-sm">
                              <input type="text" id="txt_cambiarpor_cel3" class="form-control input-sm" readonly/>
                              <span class="input-group-btn"><button type="button" class="btn btn-info btn-flat" onclick="boton_cambiarpor('cel3')"><i class="fa fa-pencil"></i></button></span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <button type="button" id="btn_descargar_dj" class="btn btn-info btn-flat" disabled onclick="descargar_dj_cambio_numeros()"><i class="fa fa-download"></i> Descargar DJ</button>
                        </div>
                        <div class="col-md-4">
                          <button type="button" id="btn_subir_dj" class="btn btn-warning btn-flat" disabled onclick="filestorage_form()"><i class="fa fa-upload"></i> Subir Sustento</button>
                        </div>
                        <div class="col-md-4">
                          <button type="button" id="btn_validar_guardar" class="btn btn-success btn-flat" disabled onclick="guardar_cambio_numeros()"><i class="fa fa-save"></i> Validar y Guardar</button>
                        </div>
                      </div>
                      
                      <div class="panel panel-warning" style="margin-top: 10px;">
                        <div class="panel-heading">
                          Sustentos de Cambio de números
                        </div>
                        <div class="panel-body" id="tbl_sustento_cambio">
                          <?php 
                            $modulo_id = $cliente_id;
                            $modulo_nom = 'cliente';
                            require_once('../filestorage/filestorage_tabla.php');
                          ?>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cliente?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cliente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>

          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cliente">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cliente/cliente_form_cmenor.js?ver=45';?>"></script>
