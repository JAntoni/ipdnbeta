<?php
//session_start();
require_once ("../../core/usuario_sesion.php");
require_once ("../cliente/Cliente.class.php");
require_once ("../usuario/Usuario.class.php");

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$oCliente = new Cliente();
$oUsuario = new Usuario();

$hora = date('d-m-Y h:i:s a');
$cli_id = $_POST['hdd_cli_id'];
$cre_tip = $_POST['hdd_cre_tip']; //3 credito garveh, 4 credito hipo
$cre_subtip = $_POST['hdd_cre_subtip'];
$cre_id = $_POST['hdd_cre_id'];
$nota_det = $_POST['txt_clientenota_det'];
$che_mos = 1;//(empty($_POST['che_nota_mos']))? 0 : $_POST['che_nota_mos'];
$gc = 0; // SI SE REGISTRA DESDE AQUÍ, NO SE MUESTRA EN EL HISTORIAL DE GC DEL CLIENTE

$action = $_POST['action'];

if($action == 'insertar'){
	if(!empty($nota_det)){
        /*$result = $oCliente->mostrarUno($cliente_id);
        if($result['estado']==1){
            $nombre_cliente = $result['data']['tb_cliente_nom'];
        }
        $result1 = $oUsuario->mostrarUno($_SESSION['usuario_id']);
        if($result1['estado']==1){
            $nombre_usuario = $result1['data']['tb_usuario_nom'];
        }
        
        $nota = 'Nota GC para: <b>'.$nombre_cliente.':</b><b style="color: green;">'.$nombre_usuario.': '.$nota_det.' | '.$hora.'</b><br> | Nota de la cuota de fecha: <b>20-03-2022</b>, del Crédito: <b>CGV-0'.$cre_id.'</b>';
        */
	$oCliente->insertar_nota_cliente($cli_id, $_SESSION['usuario_id'], $cre_tip, $cre_subtip, $cre_id, $nota_det, $gc);
	//$oCliente->mostrar_notas_cliente($cli_id, $cre_tip, $cre_subtip, $che_mos);
	//el mostrar o ocultar se hace de manera manual
        // Nota GC para: <b>DIAZ VELA CARLOS ALBERTO:</b> <b style="color: green;">CARLOS: timbra, se insiste | 29-03-2022 09:43 am</b><br> 
        // | Nota de la cuota de fecha: <b>20-03-2022</b>, del Crédito: <b>CGV-0768</b>
	echo 'exito';}
}

if ($action == 'estado') {
	$nota_id = $_POST['nota_id'];
	$estado = $_POST['estado'];

	$oCliente->estado_nota_cliente($nota_id, $estado);
	echo 'exito';
}

if ($action == 'update_state') {

    $response = [
        'estado' => 0,
        'clientenota_msj' => 'UN PARAMETRO ESTÁ FALTANTE'
    ];
    
	$nota_id = $_POST['nota_id'];
	$estado = $_POST['estado'];

    if(!empty($_POST['nota_id']) || !is_numeric($_POST['estado']))
	{
        try {
            $result = $oCliente->estado_nota_cliente($nota_id, $estado);
            if($result){
                $response['estado'] = 1;
                $response['clientenota_msj'] = 'LA NOTA SE HA OCULTADO';
            }else{
                $response['clientenota_msj'] = 'Sucedió un error';
            }
        }catch (Exception $e) {
            $response['clientenota_msj'] = 'Error: ' . $e->getMessage();
          }
    }
    echo json_encode($response);
}

if ($action == 'ver_notas') {

    $response = [
        'estado' => 0,
        'clientenota_msj' => 'UN PARAMETRO ESTÁ FALTANTE',
        'html' => ''
    ];
    
	$cliente_id = $_POST['cliente_id'];
	$credito_id = $_POST['credito_id'];
	$tipo_credito = $_POST['tipo_credito'];

    if(!empty($_POST['cliente_id']) || !empty($_POST['credito_id']) || !empty($_POST['tipo_credito']))
	{
        try {
            $result = $oCliente->listar_nota_cliente_credito2($cliente_id,$credito_id,$tipo_credito,'1,2,3,4');
            $vernotas = '';
            if($result['estado']==1){
                $vernotas .= '<ul>';
                foreach ($result['data'] as $key => $value) {
                    $vernotas.= '<div class="row" style="padding:2px">
                                    <div class="col-md-11">
                                    <strong>'.$value['tb_usuario_nom'].':</strong> '.$value['tb_notacliente_det'].'. <strong> '.mostrar_fecha_hora($value['tb_notacliente_reg']).'</strong>
                                    </div>
                                    <div class="col-md-1">
                                    <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="ocultar_nota('.$value['tb_notacliente_id'].')"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    </div>
                                </div>';
                }
                $vernotas .= '</ul>';

                $response['estado'] = 1;
                $response['html'] = $vernotas;
                $response['clientenota_msj'] = 'NOTAS LISTADAS';
            }else{
                $response['clientenota_msj'] = 'EL CLIENTE NO CUENTA CON NOTAS PARA ESTE CREDITO';
            }
        }catch (Exception $e) {
            $response['clientenota_msj'] = 'ERROR: ' . $e->getMessage();
        }
    }
    echo json_encode($response);
}
if ($action == 'ver_nota_gc') {

    $response = [
        'estado' => 0,
        'clientenota_msj' => 'UN PARAMETRO ESTÁ FALTANTE',
        'html' => ''
    ];
    
	$cliente_id = $_POST['cliente_id'];
	$credito_id = $_POST['credito_id'];
	$tipo_credito = $_POST['tipo_credito'];

    if(!empty($_POST['cliente_id']) || !empty($_POST['credito_id']) || !empty($_POST['tipo_credito']))
	{
        try {
            $result = $oCliente->listar_nota_cliente_credito2($cliente_id,$credito_id,$tipo_credito,'5');
            $vernota = '';
            if($result['estado']==1){
                $vernota = ' - OBSERVACION : '. $result['data'][0]['tb_notacliente_det'];

                $response['estado'] = 1;
                $response['html'] = $vernota;
                $response['clientenota_msj'] = 'NOTAS LISTADAS';
            }else{
                $response['clientenota_msj'] = 'EL CLIENTE NO CUENTA CON NOTAS GC PARA ESTE CREDITO';
            }
        }catch (Exception $e) {
            $response['clientenota_msj'] = 'ERROR: ' . $e->getMessage();
        }
    }
    echo json_encode($response);
}

?>