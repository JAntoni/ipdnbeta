<?php
error_reporting(1);
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cliente extends Conexion{
    public $cliente_id;
    public $cliente_tip; // tipo personal, 1 natural, 2 juridica
    public $cliente_doc; //dni del cliente
    public $cliente_nom; //nombres del cliente
    public $cliente_dir; //direccion del cliente
    public $cliente_con; //contacto del cliente
    public $cliente_tel; //telefono del cliente
    public $cliente_cel; //celular del cliente
    public $cliente_telref; //telefono referencia del cliente
    public $cliente_ema; //email del cliente
    public $cliente_fecnac; //fecha de naciemiento
    public $cliente_feccad; //fecha caducidad del dni
    public $cliente_ocu; //ocupación del cliente
    public $cliente_lugnac; //lugar de naciemiento del cliente
    public $cliente_fondos; //origen de fondo del cliente
    public $ubigeo_cod; //ubigeo de la direccion del cliente

    public $cliente_ref; //estado si el cliente es un referido 1 si es, 0 no es
    public $cliente_emp; //estado si el cliente  ya es de la empresa 1 si es, 0 no es
    public $cliente_seg; //estado si el cliente estuvo en seguimiento 1 si es, 0 no es
    public $cliente_vis; //estado si el cliente visió la oficina 1 si es, 0 no es
    public $cliente_des; //estado si el cliente está descartado 1 si es, 0 no es
    public $cliente_int = 0; //el interés del cliente: 1 cmenor, 2 asiveh, 3 garveh, 4 hipo

    public $mediocom_id; //medio de comunición cómo llegó el cliente: 1 web, 2 face, 3amigos, 4 tv
    public $creditotipo_id; // igual - medio de comunición cómo llegó el cliente: 1 web, 2 face, 3amigos, 4 tv
    public $zona_id; //zona registral de la empresa del cliente, 1 zona chiclayo, 2 piura, etc...

    public $cliente_protel; //propietario del telefono del cliente
    public $cliente_procel; //propietario del celular del cliente
    public $cliente_protelref; //propietario del telefono referencia del cliente
    public $cliente_estciv; //estado civil del cliente
    public $cliente_bien; //1 separacion de bienes, 2 sin separación de bienes
    public $cliente_firm; //como firma, 1 individual, 2 con cónyuge

    public $cliente_numpar; //número de partida electrónica de la empresa
    public $cliente_empruc; //ruc de la empresa
    public $cliente_emprs; //empresa razón social
    public $cliente_empdir; //dirección de la empresa
    public $cliente_empact; //actividad de la empresa, a qué se dedica
    public $cliente_cadest; //caducidad del DNI, 1 ya no caduca, 0 por defecto si caduca el dni
    public $cliente_empger; //tipo de gerente de la empresa: texto (gerente general, titular gerente)
    public $ubigeo_cod2; //ubigeo de la ciudad de la empresa
    public $cliente_dir2; //dirección para realizar la cobranza
    public $ubigeo_cod3; //ubigeo de la ciudad donde se hará la cobranza
    public $cliente_declaracion; //para declarar al cliente ante SUNAT: 0 no se declara, 1 si se declara
    public $cliente_fecha_declaracion; //fecha desde que el cliente empieza a ser declarable
    public $cliente_sexo; //tipo de sexo del cliente: 1 masculino, 2 femenino
    public $cliente_refdir; //referencia de la dirección del cliente
    public $cliente_refdir2; //referencia de la dirección para realizar la cobranza

    public $usuario_id;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cliente (
          tb_cliente_tip, tb_cliente_doc, 
          tb_cliente_nom, tb_cliente_dir, 
          tb_cliente_con, tb_cliente_tel, 
          tb_cliente_cel, tb_cliente_telref, 
          tb_cliente_ema, tb_cliente_fecnac, 
          tb_cliente_feccad, tb_cliente_ocu, 
          tb_cliente_lugnac, tb_cliente_fon, 
          tb_ubigeo_cod, tb_cliente_ref, 
          tb_cliente_emp, tb_cliente_seg, 
          tb_cliente_vis, tb_cliente_des, 
          tb_cliente_int, tb_mediocom_id, 
          tb_creditotipo_id, tb_zona_id, 
          tb_cliente_protel, tb_cliente_procel, 
          tb_cliente_protelref, tb_cliente_estciv, 
          tb_cliente_bien, tb_cliente_firm, 
          tb_cliente_numpar, tb_cliente_empruc, 
          tb_cliente_emprs, tb_cliente_empdir, 
          tb_cliente_empact, tb_cliente_cadest, 
          tb_cliente_empger, tb_ubigeo_cod2, 
          tb_cliente_dir2, tb_ubigeo_cod3, 
          tb_cliente_sexo, tb_cliente_refdir, 
          tb_cliente_refdir2, tb_cliente_usureg
        ) VALUES (
          :cliente_tip, :cliente_doc, 
          :cliente_nom, :cliente_dir, 
          :cliente_con, :cliente_tel, 
          :cliente_cel, :cliente_telref, 
          :cliente_ema, :cliente_fecnac, 
          :cliente_feccad, :cliente_ocu, 
          :cliente_lugnac, :cliente_fondos, 
          :ubigeo_cod, :cliente_ref, 
          :cliente_emp, :cliente_seg, 
          :cliente_vis, :cliente_des, 
          :cliente_int, :mediocom_id, 
          :creditotipo_id, :zona_id, 
          :cliente_protel, :cliente_procel, 
          :cliente_protelref, :cliente_estciv, 
          :cliente_bien, :cliente_firm, 
          :cliente_numpar, :cliente_empruc, 
          :cliente_emprs, :cliente_empdir, 
          :cliente_empact, :cliente_cadest, 
          :cliente_empger, :ubigeo_cod2, 
          :cliente_dir2, :ubigeo_cod3, 
          :cliente_sexo, :cliente_refdir, 
          :cliente_refdir2, :cliente_usureg
        );";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_tip", $this->cliente_tip, PDO::PARAM_INT); //hola amigos
        $sentencia->bindParam(":cliente_doc", $this->cliente_doc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_nom", $this->cliente_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_dir", $this->cliente_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_con", $this->cliente_con, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_tel", $this->cliente_tel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_cel", $this->cliente_cel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_telref", $this->cliente_telref, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_ema", $this->cliente_ema, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_fecnac", $this->cliente_fecnac, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_feccad", $this->cliente_feccad, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_ocu", $this->cliente_ocu, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_lugnac", $this->cliente_lugnac, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_fondos", $this->cliente_fondos, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_cod", $this->ubigeo_cod, PDO::PARAM_STR);
        
        $sentencia->bindParam(":cliente_ref", $this->cliente_ref, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_emp", $this->cliente_emp, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_seg", $this->cliente_seg, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_vis", $this->cliente_vis, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_des", $this->cliente_des, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_int", $this->cliente_int, PDO::PARAM_INT);
        $sentencia->bindParam(":mediocom_id", $this->mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->cliente_int, PDO::PARAM_INT); //cliente_int es lo mismo que creditotipo
        $sentencia->bindParam(":zona_id", $this->zona_id, PDO::PARAM_INT);

        $sentencia->bindParam(":cliente_protel", $this->cliente_protel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_procel", $this->cliente_procel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_protelref", $this->cliente_protelref, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_estciv", $this->cliente_estciv, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_bien", $this->cliente_bien, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_firm", $this->cliente_firm, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_numpar", $this->cliente_numpar, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empruc", $this->cliente_empruc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_emprs", $this->cliente_emprs, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empdir", $this->cliente_empdir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empact", $this->cliente_empact, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_cadest", $this->cliente_cadest, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_empger", $this->cliente_empger, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_cod2", $this->ubigeo_cod2, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_dir2", $this->cliente_dir2, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_cod3", $this->ubigeo_cod3, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_sexo", $this->cliente_sexo, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_refdir", $this->cliente_refdir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_refdir2", $this->cliente_refdir2, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_usureg", $this->usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $cliente_id = $this->dblink->lastInsertId();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['cliente_id'] = $cliente_id;

        $this->dblink->commit();

        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cliente SET 
          tb_cliente_tip =:cliente_tip, tb_cliente_doc =:cliente_doc, 
          tb_cliente_nom =:cliente_nom, tb_cliente_dir =:cliente_dir, 
          tb_cliente_con =:cliente_con, tb_cliente_tel =:cliente_tel, 
          tb_cliente_cel =:cliente_cel, tb_cliente_telref =:cliente_telref, 
          tb_cliente_ema =:cliente_ema, tb_cliente_fecnac =:cliente_fecnac, 
          tb_cliente_feccad =:cliente_feccad, tb_cliente_ocu =:cliente_ocu, 
          tb_cliente_lugnac =:cliente_lugnac, tb_cliente_fon =:cliente_fondos, 
          tb_ubigeo_cod =:ubigeo_cod, tb_cliente_ref =:cliente_ref, 
          tb_cliente_emp =:cliente_emp, tb_cliente_seg =:cliente_seg, 
          tb_cliente_vis =:cliente_vis, tb_cliente_des =:cliente_des, 
          tb_cliente_int =:cliente_int, tb_mediocom_id =:mediocom_id, 
          tb_creditotipo_id =:creditotipo_id, tb_zona_id =:zona_id, 
          tb_cliente_protel =:cliente_protel, tb_cliente_procel =:cliente_procel, 
          tb_cliente_protelref =:cliente_protelref, tb_cliente_estciv =:cliente_estciv, 
          tb_cliente_bien =:cliente_bien, tb_cliente_firm =:cliente_firm, 
          tb_cliente_numpar =:cliente_numpar, tb_cliente_empruc =:cliente_empruc, 
          tb_cliente_emprs =:cliente_emprs, tb_cliente_empdir =:cliente_empdir, 
          tb_cliente_empact =:cliente_empact, tb_cliente_cadest =:cliente_cadest, 
          tb_cliente_empger =:cliente_empger, tb_ubigeo_cod2 =:ubigeo_cod2, 
          tb_cliente_dir2 =:cliente_dir2, tb_ubigeo_cod3 =:ubigeo_cod3,  
          tb_cliente_sexo =:cliente_sexo, tb_cliente_refdir =:cliente_refdir, 
          tb_cliente_refdir2 =:cliente_refdir2, tb_cliente_usumod =:cliente_usumod 
          WHERE tb_cliente_id =:cliente_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_tip", $this->cliente_tip, PDO::PARAM_INT); //hola amigos
        $sentencia->bindParam(":cliente_doc", $this->cliente_doc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_nom", $this->cliente_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_dir", $this->cliente_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_con", $this->cliente_con, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_tel", $this->cliente_tel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_cel", $this->cliente_cel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_telref", $this->cliente_telref, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_ema", $this->cliente_ema, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_fecnac", $this->cliente_fecnac, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_feccad", $this->cliente_feccad, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_ocu", $this->cliente_ocu, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_lugnac", $this->cliente_lugnac, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_fondos", $this->cliente_fondos, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_cod", $this->ubigeo_cod, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_ref", $this->cliente_ref, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_emp", $this->cliente_emp, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_seg", $this->cliente_seg, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_vis", $this->cliente_vis, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_des", $this->cliente_des, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_int", $this->cliente_int, PDO::PARAM_INT);
        $sentencia->bindParam(":mediocom_id", $this->mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->cliente_int, PDO::PARAM_INT); //cliente_int es lo mismo que creditotipo
        $sentencia->bindParam(":zona_id", $this->zona_id, PDO::PARAM_INT);

        $sentencia->bindParam(":cliente_protel", $this->cliente_protel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_procel", $this->cliente_procel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_protelref", $this->cliente_protelref, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_estciv", $this->cliente_estciv, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_bien", $this->cliente_bien, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_firm", $this->cliente_firm, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_numpar", $this->cliente_numpar, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empruc", $this->cliente_empruc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_emprs", $this->cliente_emprs, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empdir", $this->cliente_empdir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empact", $this->cliente_empact, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_cadest", $this->cliente_cadest, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_empger", $this->cliente_empger, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_cod2", $this->ubigeo_cod2, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_dir2", $this->cliente_dir2, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_cod3", $this->ubigeo_cod3, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_sexo", $this->cliente_sexo, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_refdir", $this->cliente_refdir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_refdir2", $this->cliente_refdir2, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_usumod", $this->usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($cliente_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cliente set tb_cliente_xac = 0 WHERE tb_cliente_id =:cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function insertar_nota_cliente($cli_id, $usu_id, $cre_tip, $cre_subtip, $cre_id, $nota_det, $gc){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_notacliente(
                                            tb_cliente_id, 
                                            tb_usuario_id, 
                                            tb_creditotipo_id, 
                                            tb_subcredito_id, 
                                            tb_credito_id, 
                                            tb_notacliente_det, 
                                            tb_notacliente_gc) 
                                    VALUES (
                                            :tb_cliente_id, 
                                            :tb_usuario_id, 
                                            :tb_creditotipo_id, 
                                            :tb_subcredito_id, 
                                            :tb_credito_id, 
                                            :tb_notacliente_det, 
                                            :tb_notacliente_gc)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_subcredito_id", $cre_subtip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_notacliente_det", $nota_det, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_notacliente_gc", $gc, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUno($cliente_id){
      try {
        $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_id =:cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrarPorDni($cliente_doc){
      try {
        $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_doc =:cliente_doc and tb_cliente_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_doc", $cliente_doc, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_clientes(){
      try {
        $sql = "SELECT * FROM tb_cliente where tb_cliente_xac = 1 ORDER BY tb_cliente_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function verificar_documento_cliente($cliente_doc, $cliente_id){
      try {
        if(intval($cliente_id) > 0){
          //cliente está siendo editado
          $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_doc !='' AND tb_cliente_doc =:cliente_doc AND tb_cliente_id !=:cliente_id AND tb_cliente_xac = 1";
        }
        else{
          //cliente a registrar es nuevo
          $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_doc !='' AND tb_cliente_doc =:cliente_doc AND tb_cliente_xac = 1";
        }

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_doc", $cliente_doc, PDO::PARAM_STR);
        if(intval($cliente_id) > 0)
          $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function insertar_clientedetalle($cliente_id1, $cliente_id2, $clientedetalle_tip){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_clientedetalle(tb_cliente_id1, tb_cliente_id2, tb_clientedetalle_tip)
            VALUES(:cliente_id1, :cliente_id2, :clientedetalle_tip)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id2", $cliente_id2, PDO::PARAM_INT);
        $sentencia->bindParam(":clientedetalle_tip", $clientedetalle_tip, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar_sociedad_cliente($cliente_id1){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_clientedetalle WHERE tb_cliente_id1 =:cliente_id1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function verificar_sociedad_cliente($cliente_id1, $cliente_id2){
      try {
        $sql = "SELECT * FROM tb_clientedetalle where tb_cliente_id1 =:cliente_id1 and tb_cliente_id2 =:cliente_id2";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id2", $cliente_id2, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_sociedad_cliente($cliente_id1){
      try {
        $sql = "SELECT clidet.*,(case when tb_clientedetalle_tip = 1 then 'Sociedad Conyugal' when tb_clientedetalle_tip = 2 then 'Co-Propietario' else 'Apoderado' end) as tipo,tb_cliente_nom, tb_cliente_doc as cliente_asociado_doc FROM tb_clientedetalle clidet inner join tb_cliente cli on cli.tb_cliente_id = clidet.tb_cliente_id2 where tb_cliente_id1 =:cliente_id1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function cliente_autocomplete($dato, $cliente_emp){
      try {

        $filtro = "%".$dato."%";
        $arr_text = explode(' ', $dato);
        $arr_text = array_filter(array_unique($arr_text));
        //$filtro = '';
        // foreach ($arr_text as $key => $value)
        //   $filtro .= '(?=.*'.$arr_text[$key].')';

        $cliente_empresa = '';
        if(intval($cliente_emp) == 1)
          $cliente_empresa = 'AND tb_cliente_emp = 1';

        $sql = "SELECT c.*, mc.tb_mediocom_nom FROM tb_cliente c
        LEFT JOIN tb_mediocom mc ON (c.tb_mediocom_id = mc.tb_mediocom_id)
        WHERE tb_cliente_xac = 1 AND CONCAT(tb_cliente_nom, ' ', tb_cliente_doc, ' ', tb_cliente_emprs) LIKE :filtro ".$cliente_empresa." LIMIT 0,12";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function listar_nota_cliente_credito($cli_id, $cre_tip,$cre_subtip){
      try {
        $sql = "SELECT * FROM tb_notacliente 
                WHERE tb_cliente_id =:cliente_id 
                AND tb_creditotipo_id =:creditotipo_id 
                AND tb_subcredito_id IN(:subcredito_id) 
                AND tb_notacliente_mos = 1 
                AND tb_notacliente_gc = 0 
                ORDER BY tb_notacliente_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":subcredito_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No se ha Encontrado Ninguna Nota de Cliente";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function modificar_campo($cliente_id, $cliente_columna, $cliente_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($cliente_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_cliente SET " . $cliente_columna . " =:cliente_valor WHERE tb_cliente_id =:cliente_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":cliente_valor", $cliente_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":cliente_valor", $cliente_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    function listar_nota_cliente_credito1($cli_id, $cre_tip,$cre_subtip){
      try {
        $sql = "SELECT  *
                FROM tb_notacliente 
                WHERE tb_cliente_id =:cliente_id 
                AND tb_creditotipo_id =:creditotipo 
                AND tb_subcredito_id IN(:creditosubtipo_id) 
                ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":creditosubtipo_id", $cre_subtip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_nota_cliente_credito2($cli_id, $credito, $cre_tip,$cre_subtip){
      try {
        $sql = "SELECT  *
                FROM tb_notacliente nc
                INNER JOIN tb_usuario usu ON usu.tb_usuario_id = nc.tb_usuario_id
                WHERE nc.tb_cliente_id =:cliente_id 
                AND nc.tb_notacliente_mos = 1 
                AND nc.tb_credito_id = :credito_id
                AND nc.tb_creditotipo_id =:creditotipo 
                AND nc.tb_subcredito_id IN($cre_subtip)
                AND nc.tb_notacliente_gc = 0 
                ORDER BY tb_notacliente_reg desc";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo", $cre_tip, PDO::PARAM_INT);
        //$sentencia->bindParam(":creditosubtipo_id", $cre_subtip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $retorno["sql"] = $sql;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

function estado_nota_cliente($nota_id, $estado){
        $this->dblink->beginTransaction();
        
        try{
        
            $sql = "UPDATE tb_notacliente 
                    SET tb_notacliente_mos =:estado 
                    WHERE tb_notacliente_id = :notaid";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);
            $sentencia->bindParam(":notaid", $nota_id, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    /*AANTHONY*/
    function mostrar_cliente_sociedad($cli_id, $socied){
      try {
        $sql = "SELECT * FROM tb_clientedetalle cd INNER JOIN tb_cliente cl on cl.tb_cliente_id = cd.tb_cliente_id2 where tb_cliente_id1 =:cli_id and tb_clientedetalle_tip =:socied";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":socied", $socied, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function listar_cliente_sociedad($cliente_id1){
      try {
        $sql = "SELECT * FROM tb_cliente cl LEFT join tb_clientedetalle cd on cd.tb_cliente_id2 = cl.tb_cliente_id where tb_cliente_id1 =$cliente_id1 or cl.tb_cliente_id = $cliente_id1";

        $sentencia = $this->dblink->prepare($sql);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_nota_cliente_credito_3($cli_id, $cre_tip,$cre_subtip){
      try {
        $sql = "SELECT * 
                FROM tb_notacliente nc
                INNER JOIN tb_usuario usu ON usu.tb_usuario_id = nc.tb_usuario_id
                WHERE tb_cliente_id =:cliente_id 
                AND tb_creditotipo_id =:creditotipo_id 
                AND tb_subcredito_id IN($cre_subtip) 
                AND tb_notacliente_mos = 1 
                AND tb_notacliente_gc = 0 
                ORDER BY tb_notacliente_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
        //$sentencia->bindParam(":subcredito_id", $cre_subtip, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No se ha Encontrado Ninguna Nota de Cliente";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
