<?php
  
  
  if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'cliente/Cliente.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
    require_once('../cliente/Cliente.class.php');
    require_once('../funciones/fechas.php');
  }
  $oCliente = new Cliente();

?>
<table id="tbl_clientes" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Cliente</th> 
      <th>RUC/DNI</th>
      <th>Teléfono</th>
      <th>Celular</th>
      <th>Referencial</th>
      <th>Tipo Cliente</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oCliente->listar_clientes();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_cliente_id'];?></td>
            <td><?php echo $value['tb_cliente_nom'];?></td>
            <td><?php echo $value['tb_cliente_doc'];?></td>
            <td><?php echo $value['tb_cliente_tel']; ?></td>
            <td><?php echo $value['tb_cliente_cel']; ?></td>
            <td><?php echo $value['tb_cliente_telref']; ?></td>
            <td>
              <?php 
                if($value['tb_cliente_emp'] == 1)
                  echo '<span class="badge bg-green">Cliente</span>';
                else{
                  $tipo = 'SIN TIPO';
                  if($value['tb_cliente_ref'] == 1)
                    $tipo = '<span class="badge bg-yellow">Referido</span>';
                  if($value['tb_cliente_seg'] == 1)
                    $tipo = '<span class="badge bg-aqua">Seguimiento</span>';
                  if($value['tb_cliente_vis'] == 1)
                    $tipo = '<span class="badge bg-blue">Visitante</span>';
                  if($value['tb_cliente_des'] == 1)
                    $tipo = '<span class="badge bg-red">Descartado</span>';

                  echo $tipo;
                }
              ?> 
            </td>
            <td align="center">
              <a class="btn btn-info btn-xs" title="Ver" onclick="cliente_form(<?php echo "'L', ".$value['tb_cliente_id'];?>)"><i class="fa fa-eye"></i> Ver</a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="cliente_form(<?php echo "'M', ".$value['tb_cliente_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cliente_form(<?php echo "'E', ".$value['tb_cliente_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
