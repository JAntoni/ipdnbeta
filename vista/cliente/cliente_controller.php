<?php
require_once('../../core/usuario_sesion.php');

require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../funciones/fechas.php');
require_once('../referidos/Referidos.class.php');
$oReferido = new Referidos();

$action = (empty($_POST['action'])) ? $_GET['action'] : $_POST['action'];
// VALIDAR QUE SI ES JURIDICO Q OCULTE LOS COMBOS DE UBIGEO ACTUAL
$ubigeo_cod = '';
$ubigeo_cod2 = '';
$ubigeo_cod3 = '';

if (intval($_POST['cmb_ubigeo_coddis']) > 0)
  $ubigeo_cod = $_POST['cmb_ubigeo_coddep'] . $_POST['cmb_ubigeo_codpro'] . $_POST['cmb_ubigeo_coddis'];
if (intval($_POST['cmb_ubigeo_coddis2']) > 0)
  $ubigeo_cod2 = '' . $_POST['cmb_ubigeo_coddep2'] . $_POST['cmb_ubigeo_codpro2'] . $_POST['cmb_ubigeo_coddis2'] . '';
if (intval($_POST['cmb_ubigeo_coddis3']) > 0)
  $ubigeo_cod3 = '' . $_POST['cmb_ubigeo_coddep3'] . $_POST['cmb_ubigeo_codpro3'] . $_POST['cmb_ubigeo_coddis3'] . '';

$oCliente->cliente_tip = intval($_POST['txt_cliente_tip']);
$oCliente->cliente_doc = $_POST['txt_cliente_doc'];
$oCliente->cliente_nom = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
$oCliente->cliente_dir = $_POST['txt_cliente_dir'];
$oCliente->cliente_con = isset($_POST['txt_cli_con']) ? mb_strtoupper($_POST['txt_cli_con'], 'UTF-8') : '';
$oCliente->cliente_tel = $_POST['txt_cliente_tel'];
$oCliente->cliente_cel = $_POST['txt_cliente_cel'];
$oCliente->cliente_telref = $_POST['txt_cliente_telref'];
$oCliente->cliente_ema = $_POST['txt_cliente_ema'];
$oCliente->cliente_fecnac = fecha_mysql($_POST['txt_cliente_fecnac']);
$oCliente->cliente_feccad = isset($_POST['txt_cli_feccad']) ? fecha_mysql($_POST['txt_cli_feccad']) : '';
$oCliente->cliente_ocu = isset($_POST['txt_cli_ocu']) ? mb_strtoupper($_POST['txt_cli_ocu'], 'UTF-8') : '';
$oCliente->cliente_lugnac = isset($_POST['txt_cli_lugnac']) ? mb_strtoupper($_POST['txt_cli_lugnac'], 'UTF-8') : '';
$oCliente->cliente_fondos = isset($_POST['txt_cli_fon']) ? mb_strtoupper($_POST['txt_cli_fon'], 'UTF-8') : '';
$oCliente->ubigeo_cod = $ubigeo_cod;

$oCliente->cliente_ref = intval($_POST['che_cliente_ref']);
$oCliente->cliente_emp = intval($_POST['che_cliente_emp']);
$oCliente->cliente_seg = intval($_POST['che_cliente_seg']);
$oCliente->cliente_vis = intval($_POST['che_cliente_vis']);
$oCliente->cliente_des = intval($_POST['che_cliente_des']);
$oCliente->cliente_int = intval($_POST['cmb_creditotipo_id']);

$oCliente->mediocom_id = intval($_POST['cmb_mediocom_id']);
$oCliente->creditotipo_id = intval($_POST['cmb_creditotipo_id']);
$oCliente->zona_id = intval($_POST['cmb_zona_id']);

$oCliente->cliente_protel = $_POST['txt_cliente_protel'];
$oCliente->cliente_procel = $_POST['txt_cliente_procel'];
$oCliente->cliente_protelref = $_POST['txt_cliente_protelref'];
$oCliente->cliente_estciv = intval($_POST['cmb_cliente_estciv']);
$oCliente->cliente_bien = isset($_POST['cmb_cliente_bien']) ? intval($_POST['cmb_cliente_bien']) : 0;
$oCliente->cliente_firm = isset($_POST['cmb_cliente_firm']) ? intval($_POST['cmb_cliente_firm']) : 0;

$oCliente->cliente_numpar = isset($_POST['txt_cliente_numpar']) ? $_POST['txt_cliente_numpar'] : '';
$oCliente->cliente_empruc = isset($_POST['txt_cliente_empruc']) ? $_POST['txt_cliente_empruc'] : '';
$oCliente->cliente_emprs = isset($_POST['txt_cliente_emprs']) ? mb_strtoupper($_POST['txt_cliente_emprs'], 'UTF-8') : '';
$oCliente->cliente_empdir = isset($_POST['txt_cliente_empdir']) ? $_POST['txt_cliente_empdir'] : '';
$oCliente->cliente_empact = isset($_POST['txt_cli_empact']) ? mb_strtoupper($_POST['txt_cli_empact'], 'UTF-8') : ''; //
$oCliente->cliente_cadest = intval($_POST['che_cliente_cadest']);
$oCliente->cliente_empger = isset($_POST['cmb_cliente_empger']) ? mb_strtoupper($_POST['cmb_cliente_empger'], 'UTF-8') : '';
$oCliente->ubigeo_cod2 = mb_strtoupper($ubigeo_cod2, 'UTF-8');
$oCliente->cliente_dir2 = isset($_POST['txt_cli_dir2']) ? mb_strtoupper($_POST['txt_cli_dir2'], 'UTF-8') : '';
$oCliente->ubigeo_cod3 = mb_strtoupper($ubigeo_cod3, 'UTF-8'); //
$oCliente->cliente_sexo = intval($_POST['txt_cliente_sexo']);
$oCliente->cliente_refdir = mb_strtoupper($_POST['txt_cli_ref'], 'UTF-8'); //
$oCliente->cliente_refdir2 = isset($_POST['txt_cli_ref2']) ? mb_strtoupper($_POST['txt_cli_ref2'], 'UTF-8') : '';

//$oCliente->cliente_latitud = $_POST['txt_latitud_gps'];
//$oCliente->cliente_longitud = $_POST['txt_longitud_gps'];

$oCliente->usuario_id = intval($_SESSION['usuario_id']);

$pcl_reg = (array) json_decode($_POST['hdd_pcl_reg']);

// GERSON 23-11-23
$vista = '';
$proceso_id = 0;
if (isset($_POST['cliente_vista'])) {
  $vista = $_POST['cliente_vista'];
  $proceso_id = intval($_POST['hdd_proceso_id']);
}
//

if ($action == 'insertar') {
  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al guardar al Cliente.';

  $cliente_doc = $_POST['txt_cliente_doc'];
  $cliente_id = 0;
  if (trim($cliente_doc) != '') {
    $result = $oCliente->verificar_documento_cliente($cliente_doc, $cliente_id);
    $cliente_existe = $result['estado'];
    $result = NULL;
  }

  if ($cliente_existe == 1) {
    $data['mensaje'] = 'El documento del Cliente ya ha sido registrado / ' . $cliente_doc;
  } 
  else {

    $result = $oCliente->insertar(); //devuelve un array, estado e ID de Cliente
      $cliente_id = $result['cliente_id'];

      if ($result['estado'] == 1) {

        //? SI EN CASO EL USUARIO INGHESÓ NUMEROS ADICIONALES VAMOS AGREGARLO MEDIANTE UPDATE
        $cliente_celadi1 = isset($_POST['txt_cliente_celadi1'])? trim($_POST['txt_cliente_celadi1']) : '';
        $cliente_celadi2 = isset($_POST['txt_cliente_celadi2'])? trim($_POST['txt_cliente_celadi2']): '';
        $cliente_celadi3 = isset($_POST['txt_cliente_celadi3'])? trim($_POST['txt_cliente_celadi3']): '';
        $cliente_proceladi1 = isset($_POST['txt_cliente_proceladi1'])? trim($_POST['txt_cliente_proceladi1']): '';
        $cliente_proceladi2 = isset($_POST['txt_cliente_proceladi2'])? trim($_POST['txt_cliente_proceladi2']): '';
        $cliente_proceladi3 = isset($_POST['txt_cliente_proceladi3'])? trim($_POST['txt_cliente_proceladi3']): '';

        if($cliente_celadi1 != ''){
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_celadi1', $cliente_celadi1, 'STR');
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_proceladi1', $cliente_proceladi1, 'STR');
        }
        if($cliente_celadi2 != ''){
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_celadi2', $cliente_celadi2, 'STR');
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_proceladi2', $cliente_proceladi2, 'STR');
        }
        if($cliente_celadi3 != ''){
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_celadi3', $cliente_celadi3, 'STR');
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_proceladi3', $cliente_proceladi3, 'STR');
        }
        //? FIN DE NUMEROS AFICIONALES AGREGADOS PARA EL CLIENTE

        $array_cliente_id2 = $_POST['txt_cliente_id2'];
        $array_clientedetalle_tip = $_POST['txt_clientedetalle_tip'];

        if (count($array_cliente_id2) == count($array_clientedetalle_tip) && count($array_cliente_id2) > 0) {

          $oCliente->eliminar_sociedad_cliente($cliente_id);

          for ($i = 0; $i < count($array_cliente_id2); $i++) {

            $result2 = $oCliente->verificar_sociedad_cliente($cliente_id, $array_cliente_id2[$i]);
            $estado_detalle = intval($result2['estado']);
            $result2 = NULL;

            if ($estado_detalle == 0)
              $oCliente->insertar_clientedetalle($cliente_id, $array_cliente_id2[$i], $array_clientedetalle_tip[$i]);
          }
        }

        // GERSON 23-11-23
        if ($vista == 'proceso') {
          $oProceso->modificar_campo_proceso($proceso_id, 'tb_cliente_id', $cliente_id, 'INT');
        }
        //

        $data['estado'] = 1;
        $data['mensaje'] = 'Cliente registrado correctamente.';

        if (!empty($pcl_reg)) {
          require_once('../precliente/Precliente.class.php');
          $oPcl = new Precliente();

          if ($oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_cliente_id', $cliente_id, 'INT') == 1) {
            //actualizar al precliente con datos del formulario cliente
            $oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_precliente_numdoc', $oCliente->cliente_doc, 'STR');
            $oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_precliente_nombres', $oCliente->cliente_nom, 'STR');
            $oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_precliente_numcel', $oCliente->cliente_tel, 'STR');
          }
        }

        //REGISTRAR LATITUD Y LONGITUD
        if($_POST['txt_latitud_gps'] != "" && $_POST['txt_longitud_gps'] != ""){
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_latitud', $_POST['txt_latitud_gps'], 'STR');
          $oCliente->modificar_campo($cliente_id, 'tb_cliente_longitud', $_POST['txt_longitud_gps'], 'STR');
        }
      }
    $result = NULL;

    $data['cliente_id'] = $cliente_id;
    $data['cliente_doc'] = $_POST['txt_cliente_doc'];
    $data['cliente_nom'] = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
    $data['cliente_tel'] = $_POST['txt_cliente_tel'];
    $data['cliente_cel'] = $_POST['txt_cliente_cel'];
    $data['cliente_telref'] = $_POST['txt_cliente_telref'];
    $data['cliente_protel'] = $_POST['txt_cliente_protel'];
    $data['cliente_procel'] = $_POST['txt_cliente_procel'];
    $data['cliente_protelref'] = $_POST['txt_cliente_protelref'];

    if (intval($_POST['che_cliente_ref']) > 0) {
      if ($_SESSION['usuariogrupo_id'] == 3) {
        $oReferido->tb_cliente_id = $cliente_id;
        $oReferido->tb_usuario_id = $_SESSION['usuario_id'];
        $oReferido->registrar_tarea();
      }
    }
  }

  echo json_encode($data);
} 
elseif ($action == 'modificar') {
  $cliente_id = intval($_POST['hdd_cliente_id']);
  $oCliente->cliente_id = $cliente_id;

  $cliente_doc = $_POST['txt_cliente_doc'];
  if (trim($cliente_doc) != '') {
    $result = $oCliente->verificar_documento_cliente($cliente_doc, $cliente_id);
    $cliente_existe = $result['estado'];
    $result = NULL;
  }

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al modificar al Cliente.';

  if ($oCliente->modificar()) {
    //? SI EN CASO EL USUARIO INGHESÓ NUMEROS ADICIONALES VAMOS AGREGARLO MEDIANTE UPDATE
    $cliente_celadi1 = isset($_POST['txt_cliente_celadi1'])? trim($_POST['txt_cliente_celadi1']) : '';
    $cliente_celadi2 = isset($_POST['txt_cliente_celadi2'])? trim($_POST['txt_cliente_celadi2']): '';
    $cliente_celadi3 = isset($_POST['txt_cliente_celadi3'])? trim($_POST['txt_cliente_celadi3']): '';
    $cliente_proceladi1 = isset($_POST['txt_cliente_proceladi1'])? trim($_POST['txt_cliente_proceladi1']): '';
    $cliente_proceladi2 = isset($_POST['txt_cliente_proceladi2'])? trim($_POST['txt_cliente_proceladi2']): '';
    $cliente_proceladi3 = isset($_POST['txt_cliente_proceladi3'])? trim($_POST['txt_cliente_proceladi3']): '';

    if($cliente_celadi1 != ''){
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_celadi1', $cliente_celadi1, 'STR');
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_proceladi1', $cliente_proceladi1, 'STR');
    }
    if($cliente_celadi2 != ''){
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_celadi2', $cliente_celadi2, 'STR');
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_proceladi2', $cliente_proceladi2, 'STR');
    }
    if($cliente_celadi3 != ''){
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_celadi3', $cliente_celadi3, 'STR');
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_proceladi3', $cliente_proceladi3, 'STR');
    }
    //? FIN DE NUMEROS AFICIONALES AGREGADOS PARA EL CLIENTE
    
    $array_cliente_id2 = $_POST['txt_cliente_id2'];
    $array_clientedetalle_tip = $_POST['txt_clientedetalle_tip'];

    if (count($array_cliente_id2) == count($array_clientedetalle_tip) && count($array_cliente_id2) > 0) {

      $oCliente->eliminar_sociedad_cliente($cliente_id);

      for ($i = 0; $i < count($array_cliente_id2); $i++) {

        $result2 = $oCliente->verificar_sociedad_cliente($cliente_id, $array_cliente_id2[$i]);
        $estado_detalle = intval($result2['estado']);
        $result2 = NULL;

        if ($estado_detalle == 0)
          $oCliente->insertar_clientedetalle($cliente_id, $array_cliente_id2[$i], $array_clientedetalle_tip[$i]);
      }
    }

    //REGISTRAR LATITUD Y LONGITUD
    if($_POST['txt_latitud_gps'] != "" && $_POST['txt_longitud_gps'] != ""){
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_latitud', $_POST['txt_latitud_gps'], 'STR');
      $oCliente->modificar_campo($cliente_id, 'tb_cliente_longitud', $_POST['txt_longitud_gps'], 'STR');
    }

    $data['estado'] = 1;
    $data['mensaje'] = 'Cliente modificado correctamente.';
    $data['cliente_id'] = $cliente_id;
    $data['cliente_doc'] = $_POST['txt_cliente_doc'];
    $data['cliente_nom'] = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
    $data['cliente_tel'] = $_POST['txt_cliente_tel'];
    $data['cliente_cel'] = $_POST['txt_cliente_cel'];
    $data['cliente_telref'] = $_POST['txt_cliente_telref'];
    $data['cliente_protel'] = $_POST['txt_cliente_protel'];
    $data['cliente_procel'] = $_POST['txt_cliente_procel'];
    $data['cliente_protelref'] = $_POST['txt_cliente_protelref'];

    /* gerson (02-06-24) */
    if ($vista == 'proceso') {
      if (isset($_FILES['dj_file'])) {
        $fileTmpPath = $_FILES['dj_file']['tmp_name'];
        $fileName = $_FILES['dj_file']['name'];
        $fileSize = $_FILES['dj_file']['size'];
        $fileType = $_FILES['dj_file']['type'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));

        $directorio = '../../public/pdf/dj_cliente/'; //nombre con el que irá guardado en la carpeta
        $fileParts = pathinfo($_FILES['Filedata']['name']); //extensiones de las imágenes
        $fileRoute = $directorio . '' . $fileName; //ruta de doc

        // Check if the directory exists, if not, create it
        if (!is_dir($directorio)) {
          if (!mkdir($directorio, 0777, true)) {
            echo json_encode(['estado' => 0, 'mensaje' => 'Error al crear el directorio de subida.']);
            exit;
          }
        }

        if (move_uploaded_file($fileTmpPath, $fileRoute)) {
          //insertar DOC
          $oCliente->modificar_campo($cliente_id, 'dj_file', 'public/pdf/dj_cliente/' . $fileName, 'STR');
        }
      }
    }
    /*  */

    /* gerson (02-06-24) */
    if ($vista == 'proceso') {
      if (isset($_FILES['dj_file'])) {
        $fileTmpPath = $_FILES['dj_file']['tmp_name'];
        $fileName = $_FILES['dj_file']['name'];
        $fileSize = $_FILES['dj_file']['size'];
        $fileType = $_FILES['dj_file']['type'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));

        $directorio = '../../public/pdf/dj_cliente/'; //nombre con el que irá guardado en la carpeta
        $fileParts = pathinfo($_FILES['Filedata']['name']); //extensiones de las imágenes
        $fileRoute = $directorio . '' . $fileName; //ruta de doc

        // Check if the directory exists, if not, create it
        if (!is_dir($directorio)) {
          if (!mkdir($directorio, 0777, true)) {
            echo json_encode(['estado' => 0, 'mensaje' => 'Error al crear el directorio de subida.']);
            exit;
          }
        }

        if (move_uploaded_file($fileTmpPath, $fileRoute)) {
          //insertar DOC
          $oCliente->modificar_campo($cliente_id, 'dj_file', 'public/pdf/dj_cliente/' . $fileName, 'STR');
        }
      }
    }
    /*  */

    if (!empty($pcl_reg)) {
      require_once('../precliente/Precliente.class.php');
      $oPcl = new Precliente();

      //verificar si corrigió algun campo en cliente, para actualizar al precliente
      if ($oCliente->cliente_doc != $pcl_reg['tb_precliente_numdoc']) {
        $oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_precliente_numdoc', $oCliente->cliente_doc, 'STR');
      }
      if ($oCliente->cliente_nom != $pcl_reg['tb_precliente_nombres']) {
        $oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_precliente_nombres', $oCliente->cliente_nom, 'STR');
      }
      if ($oCliente->cliente_tel != $pcl_reg['tb_precliente_numcel']) {
        $oPcl->modificar_campo($pcl_reg['tb_precliente_id'], 'tb_precliente_numcel', $oCliente->cliente_tel, 'STR');
      }
    }
  }

  echo json_encode($data);
} 
elseif ($action == 'eliminar') {
  $cliente_id = intval($_POST['hdd_cliente_id']);

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al eliminar al cliente.';

  if ($oCliente->eliminar($cliente_id)) {
    $data['estado'] = 1;
    $data['mensaje'] = 'Cliente eliminado correctamente. ' . $cliente_des;
  }

  echo json_encode($data);
} 
elseif ($action == 'clientedetalle') {
} 
elseif ($action == 'buscar') {
  $cliente_doc = $_POST['doc'];
  $result = $oCliente->mostrarPorDni($cliente_doc);
  $data['estado'] = 0;
  $data['mensaje'] = 'No se encuentra un cliente con ese documento.';
  if ($result['estado']) {
    $id_cliente = $result['data']['tb_cliente_id'];
    $data['estado'] = 1;
    $data['id_cliente'] = $id_cliente;
    $data['mensaje'] = 'Cliente encontrado correctamente. ' . $id_cliente;
  }
  echo json_encode($data);
} 
elseif ($action == 'cambio_numeros') {
  $cliente_id = $_POST['cliente_id'];
  $cambiarpor_cel = trim($_POST['cambiarpor_cel']); //modificaría tb_cliente_tel
  $cambiarpor_cel2 = trim($_POST['cambiarpor_cel2']); //modificaría tb_cliente_cel
  $cambiarpor_cel3 = trim($_POST['cambiarpor_cel3']); //modificaría tb_cliente_telref
  
  $data['estado'] = 0; 
  $data['mensaje'] = 'No se ha actualizado ningún número';
  
  if($cambiarpor_cel != ''){
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $cambiarpor_cel, 'STR');
    $data['estado'] = 1; 
    $data['mensaje'] = 'Números actualizados correctamente';
  }
  if($cambiarpor_cel2 != ''){
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_cel', $cambiarpor_cel2, 'STR');
    $data['estado'] = 1; 
    $data['mensaje'] = 'Números actualizados correctamente';
  }
  if($cambiarpor_cel3 != ''){
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_telref', $cambiarpor_cel3, 'STR');
    $data['estado'] = 1; 
    $data['mensaje'] = 'Números actualizados correctamente';
  }

  echo json_encode($data);  
}
elseif ($action == 'obtener_ubicaciones') {
  $data['estado'] = 0;
  $data['mensaje'] = 'NO HA SELECCIONADO CREDITOS';
  $arr_clientes = [];
  $arr_documentos = [];

  $client_ids = $_POST['client_ids'];
  if($client_ids != ''){
    $arr_client_ids = explode(',', $client_ids);
    $arr_client_ids = array_unique($arr_client_ids);
    foreach ($arr_client_ids as $key => $client_id) {
      $result = $oCliente->mostrarUno($client_id);
      if($result['estado'] == 1){

        if($result['data']['tb_cliente_latitud'] != null && $result['data']['tb_cliente_longitud'] != null){
          $cliente = [
            'client_id' => intval($result['data']['tb_cliente_id']),
            'client_documento' => $result['data']['tb_cliente_doc'],
            'client_nombre' => $result['data']['tb_cliente_nom'],
            'client_direccion' => $result['data']['tb_cliente_dir'],
            'client_latitud' => $result['data']['tb_cliente_latitud'],
            'client_longitud' => $result['data']['tb_cliente_longitud']
          ];
        }else{
          array_push($arr_documentos, $result['data']['tb_cliente_doc']);
        }

        array_push($arr_clientes, $cliente);

      }
      $result = null;
    }
    if(count($arr_clientes) > 0 ){
      $data['estado'] = 1;
      $data['mensaje'] = 'SE ENCONTRÓ INFORMACION';
      $data['data'] = $arr_clientes;
      $data['nullos'] = $arr_documentos;
    }
  }

  echo json_encode($data);
}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
