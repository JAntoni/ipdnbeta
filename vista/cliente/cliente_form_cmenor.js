$(document).ready(function () {
  console.log('cambios al 02-10-2024 -- 2')
  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#datetimepicker1_cliente').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy"
      //endDate : new Date()
  });

  $('#datetimepicker2_cliente').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy"
      //endDate : new Date()
  });

  $(".numero").autoNumeric({
    aSep: "",
    aDec: ".",
    vMin: "0",
    vMax: "999999999",
  });

  $('#cmb_ubigeo_coddep').change(function (event) {
    var codigo_dep = $(this).val();
    if (parseInt(codigo_dep) == 0) {
      $('#cmb_ubigeo_codpro').html('');
      $('#cmb_ubigeo_coddis').html('');
    } else {
      ubigeo_provincia_select(codigo_dep);
      $('#cmb_ubigeo_coddis').html('');
    }
  });

  $('#cmb_ubigeo_codpro').change(function (event) {
    var codigo_dep = $('#cmb_ubigeo_coddep').val();
    var codigo_pro = $(this).val();

    if (parseInt(codigo_pro) == 0)
      $('#cmb_ubigeo_coddis').html('');
    else
      ubigeo_distrito_select(codigo_dep, codigo_pro);
  });

  bloquedo_editar_numeros(); //actualmente no se podrá modificar los 3 números registrados

  $('#form_cliente').validate({
    submitHandler: function () {

      if (!$('#che_cliente_emp').is(":checked") && !$('#che_cliente_ref').is(":checked")) {
        alerta_warning('Importante', 'Debes seleccionar si el cliente ya pertenece a la empresa o es un cliente Referido')
        return false;
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/cliente_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_cliente").serialize(),
        beforeSend: function () {
          $('#cliente_mensaje').show(400);
          $('#btn_guardar_cliente').prop('disabled', true);
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {

            notificacion_success(data.mensaje, 6000);

            var vista = $('#cliente_vista').val();
            if (vista == 'cliente')
                cliente_tabla();
            if (vista == 'credito') {
                llenar_datos_cliente(data);
            }
            if (vista == 'creditogarveh') {
                llenar_campos_cliente(data);
            }
            if (vista == 'pago_banco') {
                llenar_datos_cliente_banco(data);
            }
            if (vista == 'cliente_referido') {
              referidos_tabla();
            }

            $('#modal_registro_cliente').modal('hide');
          } else {
            alerta_error('Error', data.mensaje);
            console.log(data)
          }
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      txt_cliente_sexo: {
        required: true,
        min: 1
      },
      txt_cliente_doc: {
        required: true,
        digits: true,
        minlength: 8,
        maxlength: 11
      },
      txt_cliente_nom: {
        required: true,
        minlength: 2
      },
      txt_cliente_tel: {
        required: true,
        digits: true,
        minlength: 6,
        maxlength: 9
      },
      txt_cliente_cel: {
          required: true,
          digits: true,
          minlength: 6,
          maxlength: 9
      },
      txt_cliente_telref: {
        required: true,
        digits: true,
        minlength: 6,
        maxlength: 9
    },
      txt_cliente_ema: {
        required: true,
        email: true
      },
      txt_cliente_dir: {
        required: true,
        minlength: 5
      },
      txt_cliente_fecnac: {
        required: true
      },
      cmb_ubigeo_coddis: {
        required: true
      },
      cmb_mediocom_id: {
        min: 1
      },
      cmb_creditotipo_id: {
        min: 1
      },
      cmb_cliente_estciv: {
        min: 1
      }
    },
    messages: {
      txt_cliente_sexo: {
        required: "Selecciona el tipo de género del cliente",
        min: "Selecciona el tipo de género del cliente"
      },
      cmb_creditotipo_id: {
        min: "Selecciona el tipo de interés del cliente"
      },
      cmb_cliente_estciv: {
        min: "Selecciona el estado civil del cliente"
      },
      txt_cliente_doc: {
          required: "Ingrese el número de documento del Cliente",
          digits: "Solo números por favor",
          minlength: "El documento como mínimo debe tener 8 números",
          maxlength: "El documento como máximo debe tener 11 números"
      },
      txt_cliente_nom: {
          required: "Ingrese un nombre para el Cliente",
          minlength: "Como mínimo el nombre debe tener 2 caracteres"
      },
      txt_cliente_tel: {
          required: "Ingrese el número de teléfono del Cliente",
          digits: "Solo números por favor",
          minlength: "El teléfono como mínimo debe tener 6 números",
          maxlength: "El teléfono como máximo debe tener 9 números"
      },
      txt_cliente_cel: {
          required: "Ingrese el número de celular del Cliente",
          digits: "Solo números por favor",
          minlength: "El celular como mínimo debe tener 6 números",
          maxlength: "El celular como máximo debe tener 9 números"
      },
      txt_cliente_telref: {
        required: "Ingrese el número de celular del Cliente",
        digits: "Solo números por favor",
        minlength: "El celular como mínimo debe tener 6 números",
        maxlength: "El celular como máximo debe tener 9 números"
      },
      txt_cliente_ema: {
        required: "Ingresa el Email del Cliente",
        email: "Ingrese un Email válido por favor."
      },
      txt_cliente_dir: {
          required: "Ingrese una dirección para el Cliente",
          minlength: "La dirección debe tener como mínimo 5 caracteres"
      },
      txt_cliente_fecnac: {
          required: "Ingrese la fecha de nacimiento del Cliente"
      },
      cmb_ubigeo_coddis: {
          required: "Elija un distrito para el Cliente"
      },
      cmb_mediocom_id: {
          min: "Elija el medio de comunicación del Cliente"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });
});

function ubigeo_provincia_select(ubigeo_coddep) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
      async: true,
      dataType: "html",
      data: ({
          ubigeo_coddep: ubigeo_coddep
      }),
      beforeSend: function () {
          $('#cmb_ubigeo_codpro').html('<option>Cargando...</option>');
      },
      success: function (data) {
          $('#cmb_ubigeo_codpro').html(data);
      },
      complete: function (data) {
          //console.log(data);
      },
      error: function (data) {
          alert(data.responseText);
      }
  });
}

function ubigeo_distrito_select(ubigeo_coddep, ubigeo_codpro) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
      async: true,
      dataType: "html",
      data: ({
          ubigeo_coddep: ubigeo_coddep,
          ubigeo_codpro: ubigeo_codpro
      }),
      beforeSend: function () {
          $('#cmb_ubigeo_coddis').html('<option>Cargando...</option>');
      },
      success: function (data) {
          $('#cmb_ubigeo_coddis').html(data);
      },
      complete: function (data) {
          //console.log(data);
      },
      error: function (data) {
          alert(data.responseText);
      }
  });
}

function boton_cambiarpor(id_input){
  console.log(id_input)
  $('#txt_cambiarpor_'+id_input).prop('readonly', false);
  $('#btn_descargar_dj').prop('disabled', false);
}

function descargar_dj_cambio_numeros(){
  var cambio_cel = $('#txt_cambiarpor_cel').val().replace(/[^0-9]/g, '');
  var cambio_cel2 = $('#txt_cambiarpor_cel2').val().replace(/[^0-9]/g, '');
  var cambio_cel3 = $('#txt_cambiarpor_cel3').val().replace(/[^0-9]/g, '');
  var cliente_id = $('#hdd_cliente_id').val();
  
  var rutaArchivo = VISTA_URL+'cliente/doc_cliente_cambio_numeros.php?cliente_id='+cliente_id+'&cel='+cambio_cel+'&cel2='+cambio_cel2+'&cel3='+cambio_cel3;
  window.open(rutaArchivo, '_blank');

  $('#btn_subir_dj').prop('disabled', false);
}

function listar_sustento_cambio(){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "filestorage/filestorage_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: 'cliente',
      modulo_id: $('#hdd_cliente_id').val()
    }),
    beforeSend: function () {
    },
    success: function (data) {
      if(data != ''){
        $('#tbl_sustento_cambio').html(data);
        $('#btn_validar_guardar').prop('disabled', false);
      }
    },
    complete: function (data) {
    },
    error: function (data) {
      alert(data.responseText);
    }
  });
}
function guardar_cambio_numeros(){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'cambio_numeros',
      cliente_id: $('#hdd_cliente_id').val(),
      cambiarpor_cel: $('#txt_cambiarpor_cel').val(),
      cambiarpor_cel2: $('#txt_cambiarpor_cel2').val(),
      cambiarpor_cel3: $('#txt_cambiarpor_cel3').val()
    }),
    beforeSend: function () {
    },
    success: function (data) {
      if(data.estado == 1){
        swal_success('Cambio de Números', data.mensaje, 3000)
        var cambiarpor_cel = $('#txt_cambiarpor_cel').val().trim();
        var cambiarpor_cel2 = $('#txt_cambiarpor_cel2').val().trim();
        var cambiarpor_cel3 = $('#txt_cambiarpor_cel3').val().trim();

        if(cambiarpor_cel)
          $('#txt_cliente_tel').val(cambiarpor_cel)
        if(cambiarpor_cel2)
          $('#txt_cliente_cel').val(cambiarpor_cel2)
        if(cambiarpor_cel3)
          $('#txt_cliente_telref').val(cambiarpor_cel3)
      }
      else
        notificacion_warning('No se ha actualizado ningún número')
    },
    complete: function (data) {
    },
    error: function (data) {
      alerta_error('ERROR', data.responseText);
    }
  });
}
function filestorage_form(){
  $.ajax({
      type: "POST",
      url: VISTA_URL + "filestorage/filestorage_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: "I", //insertar
        public_carpeta: 'pdf',
        modulo_nom: 'cliente',
        modulo_id: $("#hdd_cliente_id").val(),
        filestorage_uniq: '',
        filestorage_des: 'Sustento de cambio de números telefónicos'
      }),
      beforeSend: function () {
        
      },
      success: function (data) {
        if (data != 'sin_datos') {
          $('#div_filestorage_form').html(data);
          $('#modal_registro_filestorage').modal('show');
          modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function (data) {
      },
      error: function (data) {
        
      }
  });

}
function filestorage_eliminar(filestorage_id){
  Swal.fire({
    title: '¿DESEA ELIMINAR EL SUSTENTO DE CAMBIO DE NÚMEROS?',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class ="fa fa-home"></i> Confirmo!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> No',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "filestorage/filestorage_controller.php",
        async: true,
        dataType: "json",
        data: ({
          action: 'eliminar',
          hdd_filestorage_id: filestorage_id,
        }),
        beforeSend: function () {
        },
        success: function (data) {
          if(parseInt(data.estado) == 1){
            notificacion_success(data.mensaje, 4000)
            $('#tr_'+filestorage_id).hide();
          }
          else
            alerta_error('IMPORTANTE', data.mensaje)
        },
        complete: function (data) {
        },
        error: function (data) {
        }
      });
    } else if (result.isDenied) {
      
    }
  });

}
function bloquedo_editar_numeros(){
  var tipo_accion = $('#hdd_action').val();

  var cliente_tel = $('#txt_cliente_tel').val().trim();
  var cliente_cel = $("#txt_cliente_cel").val().trim();
  var cliente_telref = $("#txt_cliente_telref").val().trim();

  if(tipo_accion == 'modificar'){
    if(cliente_tel != '')
      $("#txt_cliente_tel").prop('readonly', true);
    if(cliente_cel != '')
      $("#txt_cliente_cel").prop('readonly', true);
    if(cliente_telref != '')
      $("#txt_cliente_telref").prop('readonly', true);
  }
}