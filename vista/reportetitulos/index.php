<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php include(VISTA_URL.'templates/head.php'); ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
		<link href="./public/css/font-awesome-all-5.css" rel="stylesheet" type="text/css" />
	</head>

	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>

		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php include(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php include(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php include('reportetitulos_vista.php'); ?>
			<!-- INCLUIR FOOTER-->
				<?php include(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include(VISTA_URL.'templates/script.php'); ?>

		<script type="text/javascript" src="<?php echo VISTA_URL.'reportetitulos/reportetitulos.js?ver=17102024';?>"></script>
	</body>
</html>
