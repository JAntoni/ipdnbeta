<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  require_once('../creditogarveh/Creditogarveh.class.php');
  $oCreditogarveh = new Creditogarveh();

  $fecha1a = $_POST['fecha1'];
  $fecha2a = $_POST['fecha2'];
  $credito_id = 0;
  // $tipo = 0;
  $filtro_cmb_tipo = $_POST['filtro_cmb_tipo'];
  $filtro_cmb_estado = $_POST['filtro_cmb_estado'];
  $filtro_cmb_titulo = $_POST['filtro_cmb_titulo'];
  // $fecha1 = date("Y-m-d", strtotime($fecha1a));
  // $fecha2 = date("Y-m-d", strtotime($fecha2a));
  // $fecha_hoy = date('d-m-Y');

  // $borderInmatric = '';
  // if($tipo==3) { $borderInmatric = 'border: 1px solid #dbdbdb'; }

  $tr = '';
  $color = '';
  $colorDifDias = '';
  $descrDifDias = '';
  $addDias = ' día';

  $result = $oCreditogarveh->proceso_titulos_listar($credito_id, $filtro_cmb_titulo, $filtro_cmb_estado,$filtro_cmb_tipo);

  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {

      if ($value['tb_cliente_tip'] == 1) {
        $cliente = $value['tb_cliente_nom'] . ' | ' . $value['tb_cliente_doc'];
      }
      if ($value['tb_cliente_tip'] == 2) {
        $cliente = $value['tb_cliente_emprs'] . ' | ' . $value['tb_cliente_empruc'];
      }

      $data_tb_seguimiento_id          = $value['tb_seguimiento_id'];
      $data_tb_seguimiento_nrotitulo   = $value['tb_seguimiento_nrotitulo'];
      $data_tb_seguimiento_anhotitulo  = $value['tb_seguimiento_anhotitulo'];
      $data_tb_credito_id              = $value['tb_credito_id'];
      $data_tb_credito_vehpla          = $value['tb_credito_vehpla'];
      $data_tb_creditotipo_id          = $value['tb_creditotipo_id'];
      $data_tb_creditotipo_nom         = $value['tb_creditotipo_nom'];
      $data_tb_cgarvtipo_id            = $value['cgarvtipo_id'];
      $data_cgarvtipo_nombre           = $value['cgarvtipo_nombre'];
      $data_tb_credestado_titulo_id    = $value['tb_credestado_titulo_id'];
      $data_tb_credestado_titulo_descripcion = $value['tb_credestado_titulo_descripcion'];
      $data_tb_seguimiento_fecreg      = $value['tb_seguimiento_fecreg'];
      $data_fecreg_newfecha = date("d-m-Y", strtotime($data_tb_seguimiento_fecreg));
      $data_fecreg_newhora  = date('h:i a', strtotime($data_tb_seguimiento_fecreg));
      $data_tb_seguimiento_fecmod      = $value['tb_seguimiento_fecmod'];
      $data_fecmod_newfecha = date("d-m-Y", strtotime($data_tb_seguimiento_fecmod));
      $data_fecmod_newhora  = date('h:i a', strtotime($data_tb_seguimiento_fecmod));
      $data_tb_seguimiento_usureg      = $value['tb_seguimiento_usureg'];
      $data_usuario                    = $value['usuario'];
      $data_tb_seguimiento_observacion = $value['tb_seguimiento_observacion'];
      $data_tb_seguimiento_tipo = $value['tb_seguimiento_tipo'];
      $data_diferencia_dias = $value['diferencia_dias'];

      switch ($data_tb_credestado_titulo_id) {
        case 1:
          $color = '25dbcf';
          break;
        case 2:
          $color = '2057AC';
          break;
        case 3:
          $color = 'F38C05';
          break;
        case 4:
          $color = 'B4B4B4';
          break;
        case 5:
          $color = '662485';
          break;
        case 6:
          $color = '84BC18';
          break;
        case 7:
          $color = '575755';
          break;
        case 8:
          $color = 'EF1B52';
          break;
        case 9:
          $color = '006632';
          break;
        case 10:
          $color = '81D1FF';
          break;
        case 11:
          $color = 'FC0002';
          break;
        case 12:
          $color = '95141E';
          break;
        case 13:
          $color = '020202';
          break;
        case 14:
          $color = '6BA7CF';
          break;
        case 15:
          $color = 'CD3C40';
          break;
        default:
          $color = 'ffffff';
          break;
      }

      switch ($data_tb_seguimiento_tipo) {
        case 1:
          $descripTituloTipo = 'GARANTÍA';
          break;
        case 2:
          $descripTituloTipo = 'PODER';
          break;
        case 3:
          $descripTituloTipo = 'INMATRICULACIÓN';
          break;
        case 4:
          $descripTituloTipo = 'LEVANTAMIENTO';
          break;
        default:
          $descripTituloTipo = 'No encontrado';
        break;
      }

      if( $data_diferencia_dias != 1 ){ $addDias = ' días';}


      if( $data_tb_credestado_titulo_descripcion != 'INSCRITO' )
      {
        if( $data_diferencia_dias >= 0 && $data_diferencia_dias <= 3 )
        {
          $colorDifDias = 'bg-success text-green';
          $descrDifDias = '<i class="fa fa-check-circle fa-fw"></i> '. $data_diferencia_dias;        
        }
        elseif( $data_diferencia_dias >= 4 && $data_diferencia_dias <= 5 )
        {
          $colorDifDias = 'bg-warning text-orange';
          $descrDifDias = '<i class="fa fa-warning fa-fw"></i> '. $data_diferencia_dias;
        }
        else
        {
          $colorDifDias = 'bg-danger text-red';
          $descrDifDias = '<i class="fa fa-warning fa-fw"></i> '. $data_diferencia_dias;
        }
      }
      else
      {
          $colorDifDias = 'bg-white';
          $descrDifDias = ' ';
          $addDias = ' ';
      }

      $fragmentDocument = $oCreditogarveh->proceso_titulos_listar_documentos_pdf($data_tb_seguimiento_id);

      $tr .= '<tr class="tabla_filaTabla text-center">';
        $tr .= '
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_seguimiento_id . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_seguimiento_nrotitulo . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_seguimiento_anhotitulo . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_credito_id . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_creditotipo_nom . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $cliente . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_credito_vehpla . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_cgarvtipo_nombre . '</td>
          <td id="tabla_fila" style="vertical-align: middle; background-color: #'.$color.'"> &nbsp; </td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_tb_credestado_titulo_descripcion . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_fecreg_newfecha . ' '. $data_fecreg_newhora . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_fecmod_newfecha . ' '. $data_fecmod_newhora . '</td>
          <td id="tabla_fila" style="vertical-align: middle;" class="'.$colorDifDias.'">' . $descrDifDias . $addDias . ' </td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $data_usuario . '</td>
          <td id="tabla_fila" style="vertical-align: middle;" class="text-left">' . $data_tb_seguimiento_observacion . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $descripTituloTipo . '</td>
        ';

        if ($fragmentDocument['estado'] == 1) {
          $tr .= '<td id="tabla_fila" style="vertical-align: middle;">';
            foreach ($fragmentDocument['data'] as $key => $valueDocs) {
              $rutaArchivo = $valueDocs['filestorage_url'];
              $tr .= '<a href="'.$rutaArchivo.'" target="_blank" style="font-size: 25px;" class="text-red">
                <i class="fa fa-file-text"></i>
              </a> &nbsp;';
            }
          $tr .= '</td>';
        }
        else
        {
          $tr .= '<td id="tabla_fila" style="vertical-align: middle;"> sin archivos</td>';
        }

      $tr .= '</tr>';
      
    }      
  }
  $result = null;
?>
<table id="tbl_reportetitulos" class="table table-hover table-borderless">
  <thead>
    <tr>
      <th id="tabla_cabecera" class="text-center">ID</th>
      <th id="tabla_cabecera" class="text-center">Nro. de Título</th>
      <th id="tabla_cabecera" class="text-center">Año de Título</th>
      <th id="tabla_cabecera" class="text-center">Crédito ID</th>
      <th id="tabla_cabecera" class="text-center">Crédito Descripción</th>
      <th id="tabla_cabecera" class="text-center">Cliente</th>
      <th id="tabla_cabecera" class="text-center">Nro. de Placa</th>
      <th id="tabla_cabecera" class="text-center">Tipo</th>
      <th id="tabla_cabecera" class="text-center">&nbsp;</th>
      <th id="tabla_cabecera" class="text-center">Estado</th>
      <th id="tabla_cabecera" class="text-center">Fech. Reg.</th>
      <th id="tabla_cabecera" class="text-center">Fech. Modif.</th>
      <th id="tabla_cabecera" class="text-center">Días Transcurridos</th>
      <th id="tabla_cabecera" class="text-center">Usuario Responsable</th>
      <th id="tabla_cabecera" class="text-center">Observación</th>
      <th id="tabla_cabecera" class="text-center">Título</th>
      <th id="tabla_cabecera" class="text-center">PDF</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr; ?>
  </tbody>
</table>