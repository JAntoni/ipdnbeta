<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<!-- <small>< ?php echo $menu_des; ?></small> -->
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit)); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title text-primary"> <i class="fa fa-filter fa-fw"></i> Filtros </h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-original-title="Minimizar">
						<i class="fa fa-minus text-white"></i>
					</button>
					<!-- <button type="button" class="btn btn-box-tool" onclick="listar()">
                    <i class="fa fa-refresh fa-fw"></i> Actualizar
                  </button> -->
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Fecha :</label>
							<div class="input-group">
								<div class='input-group date' id='datetimepicker1'>
									<input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo date('d-m-Y'); ?>" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<span class="input-group-addon">-</span>
								<div class='input-group date' id='datetimepicker2'>
									<input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo date('d-m-Y'); ?>" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="cmb_tipo">Tipo :</label>
							<select class="form-control input-sm" name="cmb_tipo" id="cmb_tipo">
								<option value="0">Todos</option>
								<option value="1">PRECONSTITUCION</option>
								<option value="2">CONSTITUCION SIN CUSTODIA</option>
								<option value="3">CONSTITUCION CON CUSTODIA</option>
								<option value="4">LEVANTAMIENTO</option>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="cmb_estado">Estado :</label>
							<select class="form-control input-sm" name="cmb_estado" id="cmb_estado"></select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="cmb_titulo">Título :</label>
							<select class="form-control input-sm" name="cmb_titulo" id="cmb_titulo">
								<option value="0">Todos</option>
								<option value="1">GARANTÍA</option>
								<option value="2">PODER</option>
								<option value="3">INMATRICULACIÓN</option>
								<option value="4">LEVANTAMIENTO</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mt-0 mb-4">
			<div class="col-md-12">
				<div class="panel panel-default shadow-now">
					<div class="panel-header p-4">
						<h5 class="panel-title">Leyenda</h5>
						<!-- <div class="pull-right"><a href="https://sigueloplus.sunarp.gob.pe/siguelo/" target="_blank" class="btn btn-success btn-sm mb-4"><i class="fa fa-file-text fa-fw"></i> SIGUELO </a></div> -->
					</div>
					<div class="panel-body">
						<div class="col-md-2 col-sm-6">
							<table class="table datatable">
								<tbody>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #0b9c92; background-color: #25dbcf;"></div>
										</td>
										<td style="padding: 3px 10px;">Presentado</td>

										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #0e397d; background-color: #2057AC;"></div>
										</td>
										<td style="padding: 3px 10px;">Reingresado</td>

									</tr>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #d10d40; background-color: #EF1B52;"></div>
										</td>
										<td style="padding: 3px 10px;">Distribuido</td>

										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #003d1e; background-color: #006632;"></div>
										</td>
										<td style="padding: 3px 10px;">Liquidado</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3 col-sm-6">
							<table class="table datatable">
								<tbody>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #ff5e00; background-color: #F38C05;"></div>
										</td>
										<td style="padding: 3px 10px;">Apelado</td>

										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #757575; background-color: #B4B4B4;"></div>
										</td>
										<td style="padding: 3px 10px;">En Proceso</td>
									</tr>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #468bb3; background-color: #81D1FF;"></div>
										</td>
										<td style="padding: 3px 10px;">Prorrogado</td>

										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #990001; background-color: #FC0002;"></div>
										</td>
										<td style="padding: 3px 10px;">Observado</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3 col-sm-6">
							<table class="table datatable">
								<tbody>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #3d0d54; background-color: #662485;"></div>
										</td>
										<td style="padding: 3px 10px;">En Calificación</td>

										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #5e8a08; background-color: #84BC18;"></div>
										</td>
										<td style="padding: 3px 10px;">Inscrito</td>
									</tr>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #54070d; background-color: #95141E;"></div>
										</td>
										<td style="padding: 3px 10px;">Suspendido</td>

										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #000000; background-color: #020202;"></div>
										</td>
										<td style="padding: 3px 10px;">Tachado</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-2 col-sm-3">
							<table class="table datatable">
								<tbody>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #333332; background-color: #575755;"></div>
										</td>
										<td style="padding: 3px 10px;">Reservado</td>
									</tr>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #34607d; background-color: #6BA7CF;"></div>
										</td>
										<td style="padding: 3px 10px;">Anotado</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-2 col-sm-3">
							<table class="table datatable">
								<tbody>
									<tr>
										<td style="padding: 3px 10px;" align="center">
											<div class="square" style="width:15px; height: 13px; border:1px solid #801d20; background-color: #CD3C40;"></div>
										</td>
										<td style="padding: 3px 10px;">Cancelado</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="box">
			<div class="box-header">
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="reportetitulos_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div id="div_listar" class="table-responsive dataTables_wrapper form-inline dt-bootstrap"></div>
					</div>
				</div>
			</div>

			<!-- <div id="div_modal_ajustar_form"> -->
			<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			<!-- </div> -->

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>