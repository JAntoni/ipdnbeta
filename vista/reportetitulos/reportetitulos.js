$(document).ready(function () {

  $("#datetimepicker1, #datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  // $(".moneda").autoNumeric({
  //   aSep: ",",
  //   aDec: ".",
  //   vMin: "0.00",
  //   vMax: "999999999.00",
  // });
  
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $("#txt_filtro_fec1").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
    listar();
  });

  $("#datetimepicker2").on("change", function (e) {
    var endVal = $("#txt_filtro_fec2").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
    listar();
  });

  $("#cmb_tipo,#cmb_titulo,#cmb_estado").on("change", function (e) {
    listar();
  });

  cargarComboEstado("#cmb_estado");

  listar();

});

var datatable_global;

function cargarComboEstado(nombreCombo)
{
  $(nombreCombo).empty();

  var modal = "";

  if (nombreCombo == "#cmb_estado") { modal = "0"; } else { modal = "1"; }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "reportetitulos/cmbestado_select.php",
    async: false,
    dataType: "html",
    data: ({
      modal: modal
    }),
    beforeSend: function () {
      $(nombreCombo).html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      // console.log(html);
      $(nombreCombo).empty();
      $(nombreCombo).append(html);
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      alerta_warning("Advertencia",data.responseText);
    },
  });
}

function listar() {

  var filtro_fecha_inicio = $("#txt_fil_ing_fec1").val();
  var filtro_fecha_fin    = $("#txt_fil_ing_fec2").val();
  var filtro_cmb_tipo     = $("#cmb_tipo").val();
  var filtro_cmb_titulo   = $("#cmb_titulo").val();
  var filtro_cmb_estado   = $("#cmb_estado").val();

  if( filtro_cmb_tipo == null )
  {
    filtro_cmb_tipo = '0';
  }
  
  if( filtro_cmb_titulo == null )
  {
    filtro_cmb_titulo = '0';
  }

  if( filtro_cmb_estado == null )
  {
    filtro_cmb_estado = '0';
  }

  console.log(filtro_cmb_tipo);
  console.log(filtro_cmb_estado);
  console.log(filtro_cmb_titulo);

  $("#div_listar").empty();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "reportetitulos/reportetitulos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha1: filtro_fecha_inicio,
      fecha2: filtro_fecha_fin,
      filtro_cmb_tipo: filtro_cmb_tipo,
      filtro_cmb_estado: filtro_cmb_estado,
      filtro_cmb_titulo: filtro_cmb_titulo
    }), 
    beforeSend: function () {
      // console.log('cargando modal repote...');
      $("#reportetitulos_mensaje_tbl").show(300);
    },
    success: function (data) {
      //INSERTA MEDIANTE HTML LA DATA QUE REGRESA DE CLIENTE_TABLA.PHP
      $("#div_listar").html(data);
      $("#reportetitulos_mensaje_tbl").hide(600);
    },
    complete: function (data) {
      estilos_datatable('tbl_reportetitulos');
    },
    error: function (data) {
      // $("#modal_mensaje").modal("show");
      $("#reportetitulos_mensaje_tbl").html("ERROR: " + data.responseText);
    },
  });
}