<?php
  session_name("ipdnsac");
  session_start();
	require_once('Permiso.class.php');
  $oPermiso = new Permiso();
  
	$action = $_POST['action'];

	if($action == 'permiso'){
    $usuario_id = intval($_SESSION['usuario_id']); 
    $modulo = $_POST['hdd_modulo'];
    $modulo_id = intval($_POST['hdd_modulo_id']); 
    $permiso_tip = $_POST['hdd_permiso_tip']; 
    $permiso_des = $_POST['txt_permiso_des'];
    $estado = 0; //estado de la solicutd: 0 pendiente, 1 aprobado, 2 rechazado

    $data['estado'] = 0;
    $data['mensaje'] = '';

    //solo se guarda la solicitud si es nueva, sino será un mensaje de que ya se solictó lo mismo
    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $permiso_tip, $estado);

    if($result['estado'] == 0){
      if($oPermiso->insertar($usuario_id, $modulo, $modulo_id, $permiso_tip, $permiso_des, $estado)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Se ha registrado la solicitud correctamente.';
      }
      else
        $data['mensaje'] = 'Existe un error al guardar la solicitud.';
    }
    else{
      $data['estado'] = 0;
      $data['mensaje'] = 'Ya se solicitó el permiso de la transacción que desea hacer.';
    }
    
    $result = NULL;
    echo json_encode($data);
  }
  elseif($action == 'modificar'){
    $permiso_id = intval($_POST['hdd_permiso_id']);
    $permiso_est = intval($_POST['rad_permiso_est']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al registrar la operación';

    if($permiso_id > 0){
      $permiso_columna = 'tb_permiso_est';
      $permiso_valor = $permiso_est;
      $param_tip = 'INT'; // el PARAM DEL PDO, PUEDE SER PARAM_INT O PARAM_STR

      if($oPermiso->modificar_campo($permiso_id, $permiso_columna, $permiso_valor, $param_tip)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Opción de Solictud de Permiso guardado';
      }
    }
    else
      $data['mensaje'] = 'Falta el ID del módulo de Permiso';

    echo json_encode($data);
  }
?>