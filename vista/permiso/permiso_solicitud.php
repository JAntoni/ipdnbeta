<?php
$tipo_permiso = $_POST['action']; //este es el tipo de permiso que desea el usuario
$modulo_id = $_POST['modulo_id']; //id del registro de la tabla al que desea acceder
$modulo = $_POST['modulo']; //url del controllador a donde se enviaran los datos

$titulo = '';
  if($tipo_permiso == 'L')
    $titulo = 'Solicitar Lectura de Datos';
  elseif($tipo_permiso == 'I')
    $titulo = 'Solicitar Registro de Datos';
  elseif($tipo_permiso == 'M')
    $titulo = 'Solicitar Edición de Datos';
  elseif($tipo_permiso == 'E')
    $titulo = 'Solicitar Eliminación de Datos';
  else
    $titulo = 'Solicitud Desconocida: Consulte con su área de sistemas';

?>
<div class="modal modal-primary" tabindex="-1" role="dialog" id="modal_permiso_solicitud">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      <form id="form_permiso_solicitud" method="post">
        <input type="hidden" name="action" value="permiso">
        <input type="hidden" name="hdd_permiso_tip" value="<?php echo $tipo_permiso;?>">
        <input type="hidden" name="hdd_modulo_id" value="<?php echo $modulo_id;?>">
        <input type="hidden" name="hdd_modulo" id="hdd_modulo" value="<?php echo $modulo;?>">
        <div class="modal-body">
          <div class="box-body">
            <p>Usted no tiene permiso para la transacción que desea hacer, pero puede enviar una solicitud al área de administración indicando el tipo de operación que desea realizar.</p>
            <div class="form-group">
              <label for="txt_creditotipo_des" class="control-label">Solicitar:</label>
              <input type="text" name="txt_permiso_des" id="txt_permiso_des" class="form-control input-sm">
            </div>
          </div>
          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="permiso_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Guardando solicitud...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_solicitud">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/permiso/permiso_solicitud.js';?>"></script>
