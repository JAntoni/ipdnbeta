<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Permiso extends Conexion{

    function insertar($usuario_id, $modulo, $modulo_id, $permiso_tip, $permiso_des, $estado){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_permiso (tb_usuario_id, tb_permiso_mod, tb_permiso_modid, tb_permiso_tip, tb_permiso_des, tb_permiso_est) VALUES (:usuario_id, :modulo, :modulo_id, :permiso_tip, :permiso_des, :estado)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo", $modulo, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":permiso_tip", $permiso_tip, PDO::PARAM_STR);
        $sentencia->bindParam(":permiso_des", $permiso_des, PDO::PARAM_STR);
        $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_campo($permiso_id, $permiso_columna, $permiso_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($permiso_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_permiso SET ".$permiso_columna." =:permiso_valor WHERE tb_permiso_id =:permiso_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":permiso_id", $permiso_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":permiso_valor", $permiso_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":permiso_valor", $permiso_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($permiso_id){
      try {
        //estado de permiso = 1, es decir si el el permiso solicitado está aprobado, 0 pendiente, 2 rechazado
        $sql = "SELECT * FROM tb_permiso WHERE tb_permiso_id =:permiso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":permiso_id", $permiso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay ningún registro para la consulta";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_permisos(){
      try {
        //estado de permiso = 1, es decir si el el permiso solicitado está aprobado, 0 pendiente, 2 rechazado
        $sql = "SELECT * FROM tb_permiso per INNER JOIN tb_usuario us on us.tb_usuario_id = per.tb_usuario_id order by tb_permiso_id desc";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay ningún registro para la consulta";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $permiso_tip, $estado){
      try {
        //estado de permiso = 1, es decir si el el permiso solicitado está aprobado, 0 pendiente, 2 rechazado
        $sql = "SELECT * FROM tb_permiso WHERE tb_usuario_id =:usuario_id and tb_permiso_mod =:modulo and tb_permiso_modid =:modulo_id and tb_permiso_tip =:permiso_tip and tb_permiso_est =:estado";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo", $modulo, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":permiso_tip", $permiso_tip, PDO::PARAM_STR);
        $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay ningún registro para la consulta";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
