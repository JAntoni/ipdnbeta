$(document).ready(function(){
	$('#form_permiso_solicitud').submit(function(event) {
		event.preventDefault();

		var controller = $('#hdd_controller').val();
		var descripcion = $('#txt_permiso_des').val();
		if(!descripcion){
			$('#txt_permiso_des').focus();
			return false;
		}

		$.ajax({
			type: "POST",
			url: VISTA_URL+'permiso/permiso_controller.php',
			dataType: "json",
			data: $('#form_permiso_solicitud').serialize(),
			beforeSend: function() {
				$('#permiso_mensaje').show(600);
				$('#btn_guardar_solicitud').prop('disabled', true);
			},
			success: function(data){
	      if(parseInt(data.estado) > 0){
	      	$('#permiso_mensaje').removeClass('callout-info').addClass('callout-success')
	      	$('#permiso_mensaje').html(data.mensaje);
	      	setTimeout(function(){ 
	      		$('#modal_permiso_solicitud').modal('hide'); }, 1000
	      	);
	      }
	      else{
	      	$('#permiso_mensaje').removeClass('callout-info').addClass('callout-warning')
	      	$('#permiso_mensaje').html('Alerta: ' + data.mensaje);
	      	$('#btn_guardar_solicitud').prop('disabled', false);
	      }
			},
			complete: function(data){

			},
			error: function(data){
	      $('#permiso_mensaje').removeClass('callout-info').addClass('callout-danger')
	      $('#permiso_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
			}
		});

	});
});