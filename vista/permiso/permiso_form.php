<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();

  require_once('../funciones/funciones.php');

  $direc = 'permiso';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];

  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $permiso_id = $_POST['permiso_id'];
  $tb_usuarioperfil_id = $_POST['tb_usuarioperfil_id'];

  $titulo = 'Desconocido';
  if($usuario_action == 'M')
    $titulo = 'Editar Opción de Solicitud de Permiso';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en permiso
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'permiso'; $modulo_id = $permiso_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    $aprobar = ''; $rechazar = '';

    if(intval($permiso_id) > 0){
      $result = $oPermiso->mostrarUno($permiso_id);
      	$permiso_est = $result['data']['tb_permiso_est'];
      $result = NULL;
      if(intval($permiso_est) == 1)
      	$aprobar = 'checked';
      if(intval($permiso_est) == 2)
      	$rechazar = 'checked';
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_permiso" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_permiso" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_permiso_id" value="<?php echo $permiso_id;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="rad_permiso_est" class="control-label">Permiso</label>
              <div class="radio">
                <label><input type="radio" name="rad_permiso_est" class="flat-green" value="1" <?php echo $aprobar; ?>> Aprobar</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="rad_permiso_est" class="flat-green" value="2" <?php echo $rechazar; ?>> Rechazar</label>
              </div>
            </div>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="permiso_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Guardando Opción...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <button type="submit" class="btn btn-info" id="btn_guardar_permiso">Guardar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_permiso">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/permiso/permiso_form.js';?>"></script>
