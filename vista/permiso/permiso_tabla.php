<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL'))
    require_once(VISTA_URL.'permiso/Permiso.class.php');
  else
    require_once('../permiso/Permiso.class.php');
  
  $oPermiso = new Permiso();
?>
<table id="tbl_permisos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Usuario</th>
      <th>Descripción</th>
      <th>ID Registro</th>
      <th>Directorio</th>
      <th>Estado</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
    
    $result = $oPermiso->listar_permisos();

    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value): ?>
        <tr>
          <td><?php echo $value['tb_permiso_id']; ?></td>
          <td><?php echo $value['tb_usuario_nom'].' '.$value['tb_usuario_ape']; ?></td>
          <td><?php echo $value['tb_permiso_des']; ?></td>
          <td><?php echo $value['tb_permiso_modid']; ?></td>
          <td><?php echo '<a href="'.$value['tb_permiso_mod'].'?id=2" target="_blank"><span class="fa fa-fw fa-eye"></span> Ir al Directorio</a>'; ?></td>
          <td>
            <?php
              if($value['tb_permiso_est'] == 1)
                echo '<span class="badge bg-green">Aprobado</span>';
              elseif($value['tb_permiso_est'] == 2)
                echo '<span class="badge bg-red">Rechazado</span>';
              else
                echo '<span class="badge bg-yellow">Pendiente</span>';
            ?>
          </td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="permiso_form(<?php echo "'M',".$value['tb_permiso_id']; ?>)"><i class="fa fa-edit"></i> Modificar</a>
          </td>
        </tr>
        <?php
      endforeach;
    }
    $result = NULL;
    ?>
  </tbody>
</table>
