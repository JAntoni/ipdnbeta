$(document).ready(function(){
	
	$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

	$('#form_permiso').submit(function(event) {
		event.preventDefault();

		$.ajax({
			type: "POST",
			url: VISTA_URL+'permiso/permiso_controller.php',
			dataType: "json",
			data: $('#form_permiso').serialize(),
			beforeSend: function() {
				$('#permiso_mensaje').show(600);
				$('#btn_guardar_permiso').prop('disabled', true);
			},
			success: function(data){
	      if(parseInt(data.estado) > 0){
	      	$('#permiso_mensaje').removeClass('callout-info').addClass('callout-success')
	      	$('#permiso_mensaje').html(data.mensaje);
	      	setTimeout(function(){ 
	      		permiso_tabla();
	      		$('#modal_registro_permiso').modal('hide'); 
	      	}, 1000 );
	      }
	      else{
	      	$('#permiso_mensaje').removeClass('callout-info').addClass('callout-warning')
	      	$('#permiso_mensaje').html('Alerta: ' + data.mensaje);
	      	$('#btn_guardar_permiso').prop('disabled', false);
	      }
			},
			complete: function(data){

			},
			error: function(data){
	      $('#permiso_mensaje').removeClass('callout-info').addClass('callout-danger')
	      $('#permiso_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
			}
		});

	});
});