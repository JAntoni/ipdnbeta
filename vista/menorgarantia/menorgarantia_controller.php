<?php

require_once('../../core/usuario_sesion.php');
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("Ventagarantia.class.php");
$oVentagarantia = new Ventagarantia();
require_once('Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once('../formulacomision/Formulacomision.class.php');
$oComision = new Formulacomision();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once '../monedacambio/Monedacambio.class.php';
$oMonedacambio= new Monedacambio();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$gar_id = $_POST['hdd_gar_id'];
$mod_id = 90; //modulo de venta de una garantia de credito menor
$colabo_id = intval($_POST['cmb_usu_id']); //id del colaborador al que se le está vendiendo
$cliente_val = intval($_POST['hdd_cli_id']);

$action = $_POST['action'];
$empresa_id = intval($_SESSION['empresa_id']);

$tipocambio_dia = 0.0;
$monto_validar = moneda_mysql($_POST['txt_gar_mon']); // monto del depósito
$monto_pagar   = moneda_mysql($_POST["txt_gar_val"]);
$saldo_a_pagar = moneda_mysql($_POST["txt_saldo_a_pagar"]);

$resultmc =$oMonedacambio->consultar(date('Y-m-d'));
if($resultmc['estado'] == 1){
    $venta  = ($resultmc['data']['tb_monedacambio_val']);
    $compra = ($resultmc['data']['tb_monedacambio_com']);
}

if ($action == 'vender') {

    if (empty($monto_pagar) || $monto_pagar < 0) {
        $data['ing_id']  = 0;
        $data['gar_msj'] = 'Monto a pagar inválido: ' . $monto_pagar;
        echo json_encode($data);
        exit();
    }

    if ($colabo_id == 0 && $cliente_val == 0) {
        $data['ing_id'] = 0;
        $data['gar_msj'] = 'Debes seleccionar un colaborador o un cliente para poder realizar la venta: Colaborador id: '.$colabo_id.', cliente id: '.$cliente_val;
        echo json_encode($data);
        exit();
    }

    if ($colabo_id != 0 && $cliente_val != 0) {
        $data['ing_id'] = 0;
        $data['gar_msj'] = 'Debes seleccionar sólo uno, colaborador o cliente.';
        echo json_encode($data);
        exit();
    }

    $hoy = date('d-m-Y');
    $fec_pago = $_POST['txt_gar_fec'];

    if ($hoy != $fec_pago) {
        $data['ing_id'] = 0;
        $data['gar_msj'] = '<strong style="color: red;">No se puede hacer ventas con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $hoy . ', fecha venta: ' . $fec_pago . '</strong>';
        echo json_encode($data);
        exit();
    }

    if ($_POST['cmb_gar_lugpag'] == 2) {
        //SE ESTÁ VENDIENDO MEDIANTE DEPOSITO, POR ELLO EL DEPOSITO Y VENTA DEBEN SER IGUALES
        $validar_precio_venta = formato_numero($_POST['txt_gar_val']);
        $validar_precio_deposito = formato_numero($_POST['txt_gar_mon']);

        if($validar_precio_venta != $validar_precio_deposito){
            $data['ing_id'] = 0;
            $data['gar_msj'] = '<strong style="color: red;">No se puede vender el producto con dos precios diferentes, para ello debe el mismo monto de venta con el monto de depósito. Valor venta: '.$_POST['txt_gar_val'].', valor depósito: '.$_POST['txt_gar_mon'].'</strong>';
            echo json_encode($data);
            exit();
        }

        $deposito_mon = 0;
        $dts = $oDeposito->validar_num_operacion($_POST['txt_gar_numope']);
            if ($dts['estado'] == 1) {
                foreach ($dts['data'] as $key => $dt) {
                    $deposito_mon += floatval($dt['tb_deposito_mon']);
                }
            }
        $dts = NULL;

        $ingreso_importe = 0;
        $dts = $oIngreso->lista_ingresos_num_operacion($_POST['txt_gar_numope']);
        if ($dts['estado'] == 1) {
            foreach ($dts['data'] as $key => $dt) {
                $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
                $ingreso_mon_id = intval($dt['tb_moneda_id']);
                if ($ingreso_mon_id == 2) //si el ingreso se hizo en dólares
                    $tb_ingreso_imp = moneda_mysql($tb_ingreso_imp * $tipocambio_dia);
                $ingreso_importe += $tb_ingreso_imp;
            }
        }

        $monto_usar = abs($deposito_mon) - $ingreso_importe;
        $monto_usar = moneda_mysql($monto_usar);

        if ($monto_validar > $monto_usar) {
            $data['colabo_id'] = 0;
            $data['ing_id'] = 0;
            $data['gar_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . ' | validar: ' . $monto_validar . ' | usar: ' . $monto_usar . ' | ingre: ' . $ingreso_importe . ' | cambio: ' . $tipocambio_dia . ' / mond ing: ' . $ingreso_mon_id;
            echo json_encode($data);
            exit();
        }

    }

    $dts = $oGarantia->mostrarUno($gar_id);
    if ($dts['estado'] == 1) {
        $cre_id = $dts['data']['tb_credito_id'];
        $gar_alm = $dts['data']['tb_almacen_id']; //lugar en que está la garantia, 1 oficina 2 en almacen
        $gar_val = floatval($dts['data']['tb_garantia_val']);
        $gar_val = floatval($dts['data']['tb_garantia_val']);
        $gar_almest = floatval($dts['data']['tb_garantia_almest']);
        $garantia_det = $dts['data']['tb_garantia_det'];
    }
    $dts = NULL;

    $cli_id = $_POST['hdd_cli_id'];

    if( $monto_pagar < $saldo_a_pagar ) // SEPARACIÓN
    {
        $mensajeIngresoEgreso = "";
        $descripMetodoPago = "";
        $descripLugarPago = "";
        $fecha_deposito = $_POST["txt_fecha_deposito"];
        $numoperacion = $_POST['txt_gar_numope'];
        $numerocuenta = $_POST['hdd_cuenta_dep'];
        $combanco = $_POST['txt_gar_comi'];
        $banco_id = 0;
        $validacion_egreso_subcuenta_id = 71; // 72 DOLARES
        $tipo_cambio = $venta;

        if ($_POST['cmb_gar_lugpag'] == 2)
        {
            $descripLugarPago = "CON DEPÓSITO";
            $descripMetodoPago = "TRANSFERENCIA BANCARIA"; // <br> Producto: '.$_POST['txt_gar_pro'].',
            $mensajeIngresoEgreso = 'Registro por separación de venta C-MENOR  <br> Lugar de depósito: '.$descripLugarPago. ', Monto abonado: '.$monto_pagar.', Método de pago: '.$descripMetodoPago . ', Fecha de pago: '.$fec_pago. ', Producto: '.$_POST['txt_gar_pro']. ', <br> Fecha de deposito: ' . mostrar_fecha($fecha_deposito) . ', Cuenta: '.$numerocuenta.', N° Operación: '.$numoperacion.', Monto Depósito: '.$monto_validar.', Comisión del banco: '.$combanco;
            
        }
        else
        {
            $descripLugarPago = "EN OFICINA";
            $descripMetodoPago = "EFECTIVO";
            $mensajeIngresoEgreso = 'Registro por separación de venta C-MENOR  <br> Lugar de depósito: '.$descripLugarPago. ', Monto abonado: '.$monto_pagar.', Método de pago: '.$descripMetodoPago . ', Fecha de pago: '.$fec_pago. ', Producto: '.$_POST['txt_gar_pro'];
        }

        // ---------------------- DATOS INGRESOS ------------------------- //

            $oIngreso->modulo_id         = 6;   // NUEVO ID CREADO EN B.D.
            $oIngreso->cuenta_id         = 37;  // INGRESO POR SEPARACIÓN
            $oIngreso->subcuenta_id      = 192; // NEW ID ASIGNADO

            $oIngreso->ingreso_usureg    = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod    = intval($_SESSION['usuario_id']);
            // $oIngreso->ingreso_xac       = 1;
            $oIngreso->ingreso_fec       = date('Y-m-d');
            $oIngreso->documento_id      = 8;                // OTROS INGRESOS 
            $oIngreso->ingreso_numdoc    = 0;                // UPDATE AUTOMARICO
            $oIngreso->ingreso_det       = 'INGRESO POR: ' . $mensajeIngresoEgreso;
            $oIngreso->ingreso_imp       = moneda_mysql($monto_pagar);
            // $oIngreso->ingreso_est       = 1;
            $oIngreso->cliente_id        = $cli_id;
            $oIngreso->caja_id           = 1; // CAJA
            $oIngreso->moneda_id         = 1; // SOLES
            $oIngreso->ingreso_modide    = $gar_id; // $stockunidad_id;
            $oIngreso->empresa_id        = $empresa_id;
            $oIngreso->ingreso_fecdep    = $fecha_deposito;
            $oIngreso->ingreso_numope    = $numoperacion;
            $oIngreso->ingreso_mondep    = $monto_validar;
            $oIngreso->ingreso_comi      = $combanco;
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
            $oIngreso->banco_id          = $banco_id;
            $oIngreso->ingreso_ap        = 0;                   // 1 = ACUERDO DE PAGO  | 0 = INGRESO NORMAL

        // ---------------------- DATOS INGRESOS ------------------------- //

        // ---------------------- DATOS EGRESOS ------------------------- //

            $oEgreso->egreso_usureg     = intval($_SESSION['usuario_id']);
            $oEgreso->egreso_fec        = date('Y-m-d');
            $oEgreso->documento_id      = 9;                                // OTROS EGRESOS
            $oEgreso->egreso_numdoc     = 0;                                // UPDATE AUTOMATICO
            $oEgreso->egreso_det        = 'EGRESO POR: ' . $mensajeIngresoEgreso;
            $oEgreso->egreso_imp        = moneda_mysql($monto_pagar);
            $oEgreso->egreso_tipcam     = 0;                                // TIPO DE CAMBIO DE VENTA ES 0
            $oEgreso->egreso_est        = 1;
            $oEgreso->cuenta_id         = 28;                               // POR DEFECTO APGO EN CUENTA
            $oEgreso->subcuenta_id      = $validacion_egreso_subcuenta_id;  // VARIABLE 71 O 72
            $oEgreso->proveedor_id      = 1;                                // POR DEFECTO
            $oEgreso->cliente_id        = 0;                                // NO VA
            $oEgreso->usuario_id        = 0;                                // NO VA
            $oEgreso->caja_id           = 1;                                // CAJA = 1
            $oEgreso->moneda_id         = 1;                                // SOLES
            $oEgreso->modulo_id         = 6;                               // 6 NEW ID ASIGNADO
            $oEgreso->egreso_modide     = $gar_id; // $stockunidad_id;
            $oEgreso->empresa_id        = $empresa_id;
            $oEgreso->egreso_ap         = 0;                                // ACUERDO DE PAGO

        // ---------------------- DATOS EGRESOS ------------------------- //

        $resultadoRegistrarIngreso = $oIngreso->insertar();

        if ($_POST['cmb_gar_lugpag'] == 2)
        {
            if ($resultadoRegistrarIngreso['estado'] == true) {
                $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $tipo_cambio, 'STR');
                    if ($resultadoMofificacionIngreso == false) {
                        $data["ing_id"]  = 0;
                        $data["gar_msj"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                    } else {
                        $resultadoRegistrarEgreso = $oEgreso->insertar();
                            if ($resultadoRegistrarEgreso['estado'] == false) {
                                $data["estado"]  = 0;
                                $data["mensaje"] = "Se presentó un inconveniente al registrar el egreso. <br> Error SQL: Egreso.class :: insertar() :: linea: 30";
                            }
                        $resultadoRegistrarEgreso = null;
                        
                        $data["ing_id"]  = 1;
                        $data["gar_msj"] = "Separación registrada correctamente";
                    }
                $resultadoMofificacionIngreso = null;
            }
        }
        else
        {
            if ($resultadoRegistrarIngreso['estado'] == true) {
                $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $tipo_cambio, 'STR');
                    if ($resultadoMofificacionIngreso == false) {
                        $data["ing_id"]  = 0;
                        $data["gar_msj"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                    } else {
                        $data["ing_id"]  = 1;
                        $data["gar_msj"] = "Separación registrada correctamente";
                    }
                $resultadoMofificacionIngreso = null;
            }
        }

        echo json_encode($data);
        exit();
    }
    else
    {
        $_POST["txt_gar_val"] = formato_moneda(formato_moneda($_POST['txt_monto_previos']) + formato_moneda($_POST["txt_gar_val"]));
        // $data['ing_id'] = 0;
        // $data['gar_msj'] = 'LISTO PARA VENDER A: '.$_POST["txt_gar_val"];
        // echo json_encode($data);
        // exit();
        
        //if ($gar_alm !=3 && $gar_almest==1) {
        if ($gar_almest == 1 && $empresa_id == $gar_alm) { // si la empresa en donde estas ahora es igual al almacén en donde está el producto, si puedes vender, si está en otras sedes no puedes venderlo
            $det_col = '';
            $comprador = '';
            //AQUI COMIENZA LA COMPRA COLABORADOR
            if (!empty($colabo_id)) {

                $cli_id = 1;

                $dts = $oUsuario->mostrarUno($colabo_id);
                if ($dts['estado'] == 1) {
                    $usu_nom = $dts['data']['tb_usuario_nom'];
                    $usu_tipper = $dts['data']['tb_usuario_per']; //tipo de personal del usuario: 1 JEFATURA, 2 VENTAS
                    $empresa_id = $dts['data']['tb_empresa_id']; //
                }
                $dts = NULL;

                $det_col = '. Venta interna al colaborador: ' . $usu_nom;
                $fecha1 = $_POST['txt_gar_pag1'];
                $fecha2 = $_POST['txt_gar_pag2']; //si es en dos cuotas se toma también esta fecha
                $fecha3 = $_POST['txt_gar_pag3']; //si es en tres cuotas se toma también esta fecha
                $fecha4 = $_POST['txt_gar_pag4']; //si es en cuatro cuotas se toma también esta fecha
                $fecha5 = $_POST['txt_gar_pag5']; //si es en cinco cuotas se toma también esta fecha
                $fecha6 = $_POST['txt_gar_pag6']; //si es en seis cuotas se toma también esta fecha
                $monto = moneda_mysql($_POST['txt_gar_val']); //si el numero de cuotas es 2 este monto se divide en 2

                $cuotas = $_POST['cmb_gar_cuo'];

                $line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha autorizado la venta interna al colaborador: <b>' . $usu_nom . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
                $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
                $oLineaTiempo->tb_lineatiempo_det = $line_det;
                $oLineaTiempo->insertar();

                $line_det = '<b>' . $usu_nom . '</b> ha hecho una compra interna de: ' . $_POST['txt_gar_pro'] . '. && <b>' . date('d-m-Y h:i a') . '</b>';
                $oLineaTiempo->tb_usuario_id = $colabo_id;
                $oLineaTiempo->tb_lineatiempo_det = $line_det;
                $oLineaTiempo->insertar();

                //obtenemos el porcentaje de comision que se debe aplicar para la compra colaborador, para ello consultamos a las formulas de comision
                //$perso = $usu_tipper; //según tipo de personal del usuario
                $perso = 2; //según tipo de personal del usuario
                $est = 1; // el estado debe ser vigente 1, 0 no vigente
                $cretip = 1; //el creditotipo_id de MENOR es 1
                $cre_asveh = 0; //menor no tiene venta nueva ni reventa
                $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
                $mes = date('m');
                $dts = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, moneda_mysql($monto), $mes, $empresa_id);
                if ($dts['estado'] == 1) {
                    $porcen = floatval($dts['data']['tb_comisiondetalle_porcen']) / 100;
                }
                $dts = NULL;
                if (empty($porcen)) {
                    $data['gar_msj'] = $porcen . '----' . $usu_tipper . '==<strong style="color: red;">El porcentaje del colaborador no está registrado por favor registre las fórmulas de este mes.</strong>';
                    echo json_encode($data);
                    exit();
                }

                //para compra colaborador el precio de la garantia es menor, debe restarse el 15% del precio
                $utilidad = floatval($monto - $gar_val);
                $mon_cola = $monto - moneda_mysql($utilidad * $porcen);
                if($utilidad <= 0)
                    $mon_cola = $monto;

                $his = 'Garantía autorizada y vendida por: <b>' . $_SESSION['usuario_nom'] . '</b> al colaborador: <b>' . $usu_nom . '</b> al precio de: <b>S/. '.mostrar_moneda($monto).'</b>, con descuento de comisión a pagar es: <b>S/. '.mostrar_moneda($mon_cola).'</b>. Pago en ' . $cuotas . ' cuotas. | ' . date('d-m-Y h:i a') . '<br>';

                if($cuotas == 0){
                    $data['gar_msj'] = 'Si selecciona un colaborador debe ingresar el número de cuotas.';
                    echo json_encode($data);
                    exit();
                }else{

                    if ($cuotas == 2 || $cuotas == 3) {
                        if ($cuotas == 2 && (empty($fecha1) || empty($fecha2))) {
                            $data['gar_msj'] = 'Ingrese todas las fechas de las '.$cuotas.' cuotas seleccionadas.';
                            echo json_encode($data);
                            exit();
                        }
                        if ($cuotas == 3 && (empty($fecha1) || empty($fecha2) || empty($fecha3))) {
                            $data['gar_msj'] = 'Ingrese todas las fechas de las '.$cuotas.' cuotas seleccionadas.';
                            echo json_encode($data);
                            exit();
                        }
                        
                        $mon_div = moneda_mysql($mon_cola) / intval($cuotas);

                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha1);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '1 /' . $cuotas;
                        $oVentainterna->insertar(); //insertamos el pago en la fecha 1

                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha2);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '2 / ' . $cuotas;
                        $oVentainterna->insertar(); //insertamos el pago en la fecha 2

                        if ($cuotas == 3) {
                            $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha3);
                            $oVentainterna->tb_garantia_id = $gar_id;
                            $oVentainterna->tb_usuario_id = $colabo_id;
                            $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                            $oVentainterna->tb_ventainterna_his = $his;
                            $oVentainterna->tb_ventainterna_nrocuo = '3 /' . $cuotas;
                            $oVentainterna->insertar(); //insertamos el pago en la fecha 3
                        }
                    }
                    if ($cuotas == 1) {
                        if (empty($fecha1)) {
                            $data['gar_msj'] = 'Ingrese fecha de la cuota.';
                            echo json_encode($data);
                            exit();
                        }
                        $utilidad = floatval($monto - $gar_val);
                        $mon_cola = $monto - moneda_mysql($utilidad * $porcen);
                        if($utilidad <= 0)
                            $mon_cola = $monto;

                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha1);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_cola);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = (1) . '/' . $cuotas;
                        $oVentainterna->insertar(); //insertamos el pago en la fecha 1
                    }

                    if ($cuotas == 4 || $cuotas == 5) {
                        if ($cuotas == 4 && (empty($fecha1) || empty($fecha2) || empty($fecha3) || empty($fecha4))) {
                            $data['gar_msj'] = 'Ingrese todas las fechas de las '.$cuotas.' cuotas seleccionadas.';
                            echo json_encode($data);
                            exit();
                        }
                        if ($cuotas == 5 && (empty($fecha1) || empty($fecha2) || empty($fecha3) || empty($fecha4) || empty($fecha5))) {
                            $data['gar_msj'] = 'Ingrese todas las fechas de las '.$cuotas.' cuotas seleccionadas.';
                            echo json_encode($data);
                            exit();
                        }

                        //para compra colaborador el precio de la garantia es menor, debe restarse el 15% del precio
                        $utilidad = floatval($monto - $gar_val);
                        $mon_cola = $monto - moneda_mysql($utilidad * $porcen);
                        $mon_div = moneda_mysql($mon_cola) / intval($cuotas);

                        $fecha4 = $_POST['txt_gar_pag4'];
                        $fecha5 = $_POST['txt_gar_pag5'];

                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha1);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '1 /' . $cuotas;
                        $oVentainterna->insertar(); //insertamos el pago en la fecha 1
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha2);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '2 /' . $cuotas;
                        $oVentainterna->insertar();
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha3);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '3 /' . $cuotas;
                        $oVentainterna->insertar();
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha4);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '4 /' . $cuotas;
                        $oVentainterna->insertar();
                        if ($cuotas == 5) {
                            $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha5);
                            $oVentainterna->tb_garantia_id = $gar_id;
                            $oVentainterna->tb_usuario_id = $colabo_id;
                            $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                            $oVentainterna->tb_ventainterna_his = $his;
                            $oVentainterna->tb_ventainterna_nrocuo = '5 /' . $cuotas;
                            $oVentainterna->insertar();
                        }
                    }

                    /* GERSON (04-05-23) */
                    if ($cuotas == 6) {
                        if ($cuotas == 6 && (empty($fecha1) || empty($fecha2) || empty($fecha3) || empty($fecha4) || empty($fecha5) || empty($fecha6))) {
                            $data['gar_msj'] = 'Ingrese todas las fechas de las '.$cuotas.' cuotas seleccionadas.';
                            echo json_encode($data);
                            exit();
                        }
                        //para compra colaborador el precio de la garantia es menor, debe restarse el 15% del precio
                        $utilidad = floatval($monto - $gar_val);
                        $mon_cola = $monto - moneda_mysql($utilidad * $porcen);
                        $mon_div = moneda_mysql($mon_cola) / intval($cuotas);

                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha1);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '1 /' . $cuotas;
                        $oVentainterna->insertar(); //insertamos el pago en la fecha 1
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha2);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '2 /' . $cuotas;
                        $oVentainterna->insertar();
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha3);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '3 /' . $cuotas;
                        $oVentainterna->insertar();
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha4);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '4 /' . $cuotas;
                        $oVentainterna->insertar();
                        $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha5);
                        $oVentainterna->tb_garantia_id = $gar_id;
                        $oVentainterna->tb_usuario_id = $colabo_id;
                        $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                        $oVentainterna->tb_ventainterna_his = $his;
                        $oVentainterna->tb_ventainterna_nrocuo = '5 /' . $cuotas;
                        $oVentainterna->insertar();
                        if ($cuotas == 6) {
                            $oVentainterna->tb_ventainterna_fecpag = fecha_mysql($fecha6);
                            $oVentainterna->tb_garantia_id = $gar_id;
                            $oVentainterna->tb_usuario_id = $colabo_id;
                            $oVentainterna->tb_ventainterna_mon = moneda_mysql($mon_div);
                            $oVentainterna->tb_ventainterna_his = $his;
                            $oVentainterna->tb_ventainterna_nrocuo = '5 /' . $cuotas;
                            $oVentainterna->insertar();
                        }
                    }
                    /*  */

                }

                $comprador = 'al colaborador: ' . $usu_nom;
            } 
            else {
                //venta a un cliente particular
                $his = '<span>Se vendió la garantía: ' . $_POST['txt_gar_pro'] . ', al cliente: ' . $_POST['txt_cliente_fil_cre_nom'] . ', </b> al precio de: <b>S/. '.$_POST['txt_gar_val'].'</b>. Vendido por: <b>' . $_SESSION['usuario_nom'] . '</b> | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
                $comprador = 'al cliente: ' . $_POST['txt_cliente_fil_cre_nom'];
            }

            $oCredito->registro_historial($cre_id, $his);
            $oCreditolinea->insertar(1, $cre_id, intval($_SESSION['usuario_id']), $his);

            $monto_pagado = 0;

            if ($_POST['cmb_gar_lugpag'] == 2) {
                //lugar de pago fue con deposito en banco
                $ing_det = 'Venta: ' . $_POST['txt_gar_pro'] . ' ('.$garantia_det.'). Producto del crédito CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT) . $det_col . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_gar_fec'] . ', el N° de operación fue: ' . $_POST['txt_gar_numope'] . ', el monto depositado fue: S/. ' . $_POST['txt_gar_mon'] . ', la comisión del banco fue: S/. ' . $_POST['txt_gar_comi'] . ', el monto validado fue: S/. ' . $_POST['txt_gar_montot'].'. El cliente al firmar este voucher acepta que revisó el producto y está conforme. El cliente también acepta que este producto no tiene garantía.';
                $monto_pagado = $_POST['txt_gar_montot'];
                $his = '<span>La garantía fue vendida a un precio de S/. ' . $_POST['txt_gar_montot'] . ', el pago fue realizado con depósito en banco. Cuenta: ' . $_POST['hdd_cuenta_dep'] . ', N° de operación: ' . $_POST['txt_gar_numope'] . '</b> | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
            } else {
                //lugar de pago fue en oficinas
                $ing_det = 'Venta: ' . $_POST['txt_gar_pro'] . ' ('.$garantia_det.'). Producto del crédito CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT) . $det_col . '. Pago realizado en oficinas'.'. El cliente al firmar este voucher acepta que revisó el producto y está conforme. El cliente también acepta que este producto no tiene garantía.';
                $monto_pagado = $monto_pagar; //? ESTO ES EL INPUT EN DONDE SE INGRESA EL MONTO PARA VENTA
                $his = '<span>La garantía fue vendida a un precio de S/. ' . $_POST['txt_gar_val'] . ', el pago fue realizado en las oficinas.</b> | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
            }

            $line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha vendido la garantía: ' . $_POST['txt_gar_pro'] . ', ' . $comprador . ', a un precio de S/. ' . $_POST["txt_gar_val"] . ' && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $line_det;
            $oLineaTiempo->insertar();

            $oCredito->registro_historial($cre_id, $his);

            $cue_id = 5; //cuenta de REMATE
            $subcue_id = 17; // remate de credito menor

            $numdoc = '90-' . str_pad($mod_id, 3, "0", STR_PAD_LEFT) . '-' . str_pad($gar_id, 3, "0", STR_PAD_LEFT);

            if (empty($colabo_id)) {
                //generamos el ingreso de la venta siempre y cuando no sea colaborador
                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = fecha_mysql($_POST['txt_gar_fec']);
                $oIngreso->documento_id = 8;
                $oIngreso->ingreso_numdoc = $numdoc;
                $oIngreso->ingreso_det = $ing_det;
                $oIngreso->ingreso_imp = moneda_mysql($monto_pagado);
                $oIngreso->cuenta_id = $cue_id;
                $oIngreso->subcuenta_id = $subcue_id;
                $oIngreso->cliente_id = $cli_id;
                $oIngreso->caja_id = 1;
                $oIngreso->moneda_id = 1;
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = $mod_id;
                $oIngreso->ingreso_modide = $gar_id;
                $oIngreso->empresa_id = $_SESSION['empresa_id'];
                $oIngreso->ingreso_fecdep = '';
                $oIngreso->ingreso_numope = '';
                $oIngreso->ingreso_mondep = 0;
                $oIngreso->ingreso_comi = 0;
                $oIngreso->cuentadeposito_id = 0;
                $oIngreso->banco_id = 0;
                $oIngreso->ingreso_ap = 0;
                $oIngreso->ingreso_detex = '';
                $result = $oIngreso->insertar();
            }
            if (intval($result['estado']) == 1) {
                $ing_id = $result['ingreso_id'];
            }


            if ($_POST['cmb_gar_lugpag'] == 2) {
                //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
                $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_gar_fec']);
                $oIngreso->ingreso_numope = $_POST['txt_gar_numope'];
                $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_gar_mon']);
                $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_gar_comi']);
                $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
                $oIngreso->banco_id = 1;
                $oIngreso->ingreso_id = $ing_id;

                $oIngreso->registrar_datos_deposito_banco(); // parametro 1 de BANCO BCP

                $subcue_id = 71; // 71 es cuenta en soles

                $xac = 1;
                $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO	
                $pro_id = 1; //sistema
                $mod_id = 90; //90 es el modulo_id en ingreso, representa a la VENTA DE GARANTIA, será igual para EGRESO servira para cuando se anule el pago
                $modide = $gar_id; //el modide de egreso guardará el id de la GARANTIA para poder anularlo mediante este id
                $egr_det = 'EGRESO POR: ' . 'Venta: ' . $_POST['txt_gar_pro'] . $det_col . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_gar_fec'] . ', el N° de operación fue: ' . $_POST['txt_gar_numope'] . ', el monto depositado fue: S/. ' . $_POST['txt_gar_mon'] . ', la comisión del banco fue: S/. ' . $_POST['txt_gar_comi'] . ', el monto validado fue: S/. ' . $_POST['txt_gar_montot'];
                $caj_id = 1; //caja id 14 ya que es CAJA AP
                $numdoc = '90-' . str_pad($mod_id, 3, "0", STR_PAD_LEFT) . '-' . str_pad($gar_id, 3, "0", STR_PAD_LEFT);

                $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                $oEgreso->egreso_fec = fecha_mysql($_POST['txt_gar_fec']);
                $oEgreso->documento_id = 9; //otros egresos
                $oEgreso->egreso_numdoc = $numdoc;
                $oEgreso->egreso_det = $egr_det;
                $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
                $oEgreso->egreso_tipcam = 0;
                $oEgreso->egreso_est = 1;
                $oEgreso->cuenta_id = $cue_id;
                $oEgreso->subcuenta_id = $subcue_id;
                $oEgreso->proveedor_id = $pro_id;
                $oEgreso->cliente_id = 0;
                $oEgreso->usuario_id = 0;
                $oEgreso->caja_id = $caj_id;
                $oEgreso->moneda_id = 1;
                $oEgreso->modulo_id = $mod_id;
                $oEgreso->egreso_modide = $modide;
                $oEgreso->empresa_id = $_SESSION['empresa_id'];
                $oEgreso->egreso_ap = 0; //si
                $oEgreso->insertar();
            }

            $oGarantia->modificar_campo($gar_id, 'tb_garantia_est', '1', 'INT');
            $oGarantia->modificar_campo($gar_id, 'tb_garantia_fecven', fecha_mysql($_POST['txt_gar_fec']), 'STR');

            $dts2 = $oGarantia->filtrar($cre_id);
            $num_rows = count($dts2['data']);
            $sum = 0;
            if ($dts2['estado'] == 1) {
                foreach ($dts2['data'] as $key => $dt2) {
                    if ($dt2['tb_garantia_est'] == 1) {
                        $sum++;
                    }
                }
            }
            if ($sum == $num_rows) {
                //CAMBIAR ESTADO DEL CREDITO
                $oCredito->modificar_campo($cre_id, 'tb_credito_est', '6', 'INT');
            }

            //insertamos los datos de la venta en tb_ventagarantia
            $cli_id = $_POST['hdd_cli_id'];
            $gar_id = $_POST['hdd_gar_id'];
            $prec1 = $_POST['hdd_gar_prec1'];
            $prec2 = $_POST['txt_gar_val'];
            if ($_POST['cmb_gar_lugpag'] == 2)
                $prec2 = moneda_mysql(moneda_mysql($_POST['txt_gar_montot']) + formato_moneda($_POST['txt_monto_previos'])); //PAGO CON DEPOSITO
            $fecha = $_POST['txt_gar_fec'];

            $comi = moneda_mysql($prec2) - moneda_mysql($prec1);

            if (!empty($colabo_id) || empty($cli_id))
                $cli_id = 0;
            if (empty($colabo_id))
                $colabo_id = 0;

            $oVentagarantia->tb_usuario_id = $_SESSION['usuario_id'];
            $oVentagarantia->tb_cliente_id = $cli_id;
            $oVentagarantia->tb_ventagarantia_col = $colabo_id;
            $oVentagarantia->tb_garantia_id = $gar_id;
            $oVentagarantia->tb_ventagarantia_prec1 = moneda_mysql($prec1);
            $oVentagarantia->tb_ventagarantia_prec2 = moneda_mysql($prec2);
            $oVentagarantia->tb_ventagarantia_com = moneda_mysql($comi);
            $oVentagarantia->tb_ventagarantia_fec = fecha_mysql($fecha);
            $oVentagarantia->tb_empresa_id = $_SESSION['empresa_id'];
            $oVentagarantia->insertar();

            //$data['ing_id'] = 1;
            $data['colabo_id'] = $colabo_id;
            $data['ing_id'] = $ing_id;
            $data['gar_msj'] = 'Se registró la venta correctamente.';
            echo json_encode($data);
        } else {
            $data['colabo_id'] = 0;
            $data['ing_id'] = 0;
            $data['gar_msj'] = 'La garantía que intenta vender no está en oficina, o se encuentra en tránsito pídalo en Inventario por favor. // emp: '.$empresa_id.' // almacen id: '.$gar_alm;
            echo json_encode($data);
        }
    }
    
}

if ($action == 'ventainterna') {
    $venin_id = $_POST['venin_id'];
    $dts = $oVentainterna->mostrarUno($venin_id);

    if ($dts['estado'] == 1) {
        $his = $dts['data']['tb_ventainterna_his'];
    }

    echo'<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_ventainterna_historial">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                      </button>
                      <h4 class="modal-title" style="font-family: cambria;"><b>Detalle de Venta Interna</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            ' . $his . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
            </div>';

    //echo $his;
}

if ($action == 'aprobar_venta') {
    $credito_id = $_POST['credito_id'];
    $usuario_id = $_POST['usuario_id'];

    $creditolinea_det = 'He revisado el caso y he dado mi aprobación para que el producto en REMATE pase a ser vendido';
    $oCreditolinea->insertar(1, $credito_id, $usuario_id, $creditolinea_det);
    $oCredito->modificar_campo($credito_id, 'tb_credito_gcapr', 1, 'INT');

    $data['mensaje'] = 'Aprobación de venta correcto';
    $data['estado'] = 1;

    echo json_encode($data);
}

if ($action == 'revisar_producto') {
    $garantia_id = intval($_POST['garantia_id']);
    $usuario_nom = $_POST['usuario_nom'];
    $garantia_det = $_POST['usuario_nom'].': '.$_POST['comentario']; // Enviamos el comentario al backend

    $oGarantia->modificar_campo($garantia_id, 'tb_garantia_rev', 1, 'INT'); //estado de revisión a 1, garantía revisada
    $oGarantia->agregar_detalle_extra($garantia_id, $garantia_det); //concatene el detalle que ya existía con un nuevo comentario

    $data['mensaje'] = 'Producto revisado correctamente y comentario agregado';
    $data['estado'] = 1;

    echo json_encode($data);
}
?>