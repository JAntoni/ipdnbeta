<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}
class Ventagarantia extends Conexion{
    
    public $tb_ventagarantia_id;
    public $tb_ventagarantia_fecreg;
    public $tb_usuario_id;
    public $tb_cliente_id;
    public $tb_ventagarantia_col;
    public $tb_garantia_id;
    public $tb_ventagarantia_prec1;
    public $tb_ventagarantia_prec2;
    public $tb_ventagarantia_com;
    public $tb_ventagarantia_fec;
    public $tb_ventagarantia_xac;
    public $tb_empresa_id;
            
    
    function insertar(){
      $this->dblink->beginTransaction();
      try {
            $sql ="INSERT INTO tb_ventagarantia(
                                        tb_usuario_id,
                                        tb_cliente_id,
                                        tb_ventagarantia_col, 
                                        tb_ventagarantia_fecreg,
                                        tb_garantia_id, 
                                        tb_ventagarantia_prec1, 
                                        tb_ventagarantia_prec2, 
                                        tb_ventagarantia_com, 
                                        tb_ventagarantia_fec,
                                        tb_empresa_id) 
                                VALUES (
                                        :tb_usuario_id,
                                        :tb_cliente_id,
                                        :tb_ventagarantia_col,
                                        NOW(),
                                        :tb_garantia_id, 
                                        :tb_ventagarantia_prec1, 
                                        :tb_ventagarantia_prec2, 
                                        :tb_ventagarantia_com, 
                                        :tb_ventagarantia_fec,
                                        :tb_empresa_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cliente_id", $this->tb_cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_ventagarantia_col", $this->tb_ventagarantia_col, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_garantia_id", $this->tb_garantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_ventagarantia_prec1", $this->tb_ventagarantia_prec1, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_ventagarantia_prec2", $this->tb_ventagarantia_prec2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_ventagarantia_com", $this->tb_ventagarantia_com, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_ventagarantia_fec", $this->tb_ventagarantia_fec, PDO::PARAM_STR);      
        $sentencia->bindParam(":tb_empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);      

        $result = $sentencia->execute();
        $tb_ventagarantia_id = $this->dblink->lastInsertId();
        
        $this->dblink->commit();
        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['tb_ventagarantia_id'] = $tb_ventagarantia_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function anular_ventagarantia($gar_id, $valor){
      $this->dblink->beginTransaction();
      try {
       $sql ="UPDATE tb_ventagarantia set 
                                    tb_ventagarantia_xac =:tb_ventagarantia_xac
                                where 
                                    tb_garantia_id =:tb_garantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_ventagarantia_xac", $valor, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_garantia_id", $gar_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrar_garantia_vendida($garantia_id){
        try {
            $sql ="SELECT ven.*, CONCAT(cli.tb_cliente_doc,' | ',cli.tb_cliente_nom) AS cliente, usu.tb_usuario_nom as colaborador, usu2.tb_usuario_nom as autorizador FROM tb_ventagarantia ven 
                LEFT JOIN tb_cliente cli on cli.tb_cliente_id = ven.tb_cliente_id
                LEFT JOIN tb_usuario usu ON usu.tb_usuario_id = ven.tb_ventagarantia_col
                LEFT JOIN tb_usuario usu2 ON usu2.tb_usuario_id = ven.tb_usuario_id
                WHERE tb_ventagarantia_xac = 1 AND ven.tb_garantia_id =:garantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay registros para esta garantía vendida";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
}

?>