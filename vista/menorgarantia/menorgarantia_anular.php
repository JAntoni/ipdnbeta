<?php
require_once('../../core/usuario_sesion.php');

require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once('Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('Ventagarantia.class.php');
$oVentagarantia = new Ventagarantia();

require_once("../funciones/funciones.php");

$gar_id = $_POST['gar_id'];
$mod_id = 90;

//anulamos el ingreso mediante el modulo y el modide, es decir: modulo de garantia e garantia_id, solo se cambia el xac a 0
$oIngreso->anular_ingreso_por_modulo($mod_id,$gar_id);

//si el pago fue por deposito en banco, anulamos el egreso generado
$oEgreso->cambiar_xac_por_modulo_modide(0, $mod_id, $gar_id);

//est es el campo de la tabla:tb_garantia_est, 0 es el valor, 1 sería vendido y 0 por vender, en este caso se anula-> regresa a 0.
$oGarantia->modificar_campo($gar_id,'tb_garantia_est','0','INT');

//cambiar xac en Ventainterna, la garantia puede estar dividida en 2, para ello eviar como parametro la garantia_id
$his = '<span style="color: red;">Venta interna anulada por: <b>'.$_SESSION['usuario_nombre'].' | '.date('d-m-Y h:i a').'</span><br>';
$oVentainterna->anular_ventainterna($gar_id, 0, $his); //parametro 0, cambia xac = 0
//$oGarantia->modificar_campo($gar_id,'fecven',fecha_mysql($_POST['txt_gar_fec']));
//anulamos la venta garantia, cambiamos su xac a 0
$oVentagarantia->anular_ventagarantia($gar_id, 0);

$dts=$oGarantia->mostrarUno($gar_id);
    if($dts['estado']==1){
	$cre_id = $dts['data']['tb_credito_id'];
	$gar_pro = $dts['data']['tb_garantia_pro'];
    }
$dts=NULL;


$his = '<span style="color: red;">La venta de la garantía: '.$gar_pro.' fue anulada por: <b>'.$_SESSION['usuario_nombre'].' | '.date('d-m-Y h:i a').'</span><br>';
$oCredito->registro_historial($cre_id, $his);
//cuando está vendido el estado de credito es 6, en este caso le regreso a 5:
$oCredito->modificar_campo($cre_id,'tb_credito_est','5','INT');

$data['gar_id']=$gar_id;
$data['gar_msj']='Se anuló la venta de garantía.';

echo json_encode($data);

?>