<?php
require_once('../../core/usuario_sesion.php');
require_once("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once("../subasta/Subasta.class.php");
$oSubasta = new Subasta();
require_once("../funciones/funciones.php");

$num_cuotas = 0;
$garantia_subasta = 0; // si es 1 estuvo en subasta
$dts = $oGarantia->mostrarUno($_POST['gar_id']);
  if ($dts['estado'] == 1) {
    $gar_pro = $dts['data']['tb_garantia_pro'];
    $gar_val = $dts['data']['tb_garantia_val'];
    $gar_fec = date('d-m-Y');
    $garantia_prefij = $dts['data']['tb_garantia_prefij'];
    $num_cuotas = $dts['data']['tb_garantia_num_cuota_rem'];
    $garantia_subasta = intval($dts['data']['tb_garantia_subasta']);
  }
$dts = NULL;

//consulta a los valores de la formula vigente para calcular el precio de venta
if ($_POST['formenor_rec'] > 0 && $_POST['formenor_pro'] >= 0) {
  $formenor_rec = $_POST['formenor_rec'];
  $formenor_pro = $_POST['formenor_pro'];
} else {
  echo '<h2>NO SE HA SELECCIONADO UNA FÓRMULA PARA LA VENTA O EXISTE MÁS DE UNO EN ESTADO VIGENTE. CONSULTE CON ADMISTRADOR DE SISTEMA</h2>';
  exit();
}

$forma_precio = 'Precio:';
$pre_ven = mostrar_moneda(($gar_val * $formenor_rec) + ($gar_val * $formenor_pro * ($_POST['int'] / 100)));

if (floatval($garantia_prefij) > 0) {
  $pre_ven = mostrar_moneda($garantia_prefij);
  $forma_precio = '<span class="badge bg-green">Precio Fijado:</span>';
}
//$pre_ven = formato_money(($gar_val*2.2) + ($gar_val*2*($_POST['int']/100)));

//? VAMOS A EVALUAR SI EL PRODUCTO ESTUVO EN SUBASTA Y SI EXISTE UN USUARIO GANADOR DEL MISMO
$usuario_session_id = intval($_SESSION['usuario_id']);
$usuario_ganador = '';
$bandera_venta = 1; //? 1 se habilita para compra el producto
$bandera_mensaje = '';

if($garantia_subasta == 1){
  //? REPRESENTA QUE SI ESTUVO EN SUBASTA, VAMOS A VERIFICAR SI TIENE GANADOR
  $result = $oSubasta->mostrar_ganador_por_garantia($_POST['gar_id']);
    if($result['estado'] == 1){
      //? REPRESENTA QUE SI HAY UN GANADOR DE LA SUBASTA, ENTONCES SOLO EL PODRÁ COMPRARLO
      $usuario_ganador_id = intval($result['data']['tb_subasta_usugana']);
      $usuario_ganador = $result['data']['tb_usuario_nom'].' '.$result['data']['tb_usuario_ape'];

      if($usuario_session_id != $usuario_ganador_id){
        $bandera_venta = 0; //? no se habilita para compra, solo el usuario ganador puede comprarlo
        $bandera_mensaje = '
          <div class="callout callout-info">
            <h4>Importante!</h4>
            <p>Este producto estuvo en subasta y solo el ganador podrá comprarlo, el ganador fue: <b>'.$usuario_ganador.'</b></p>
          </div>';
      }
    }
  $result = NULL;
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_garantiaventa_registro" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" id="modalSizeVender" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title text-blue" style="font-weight: bold; font-family: cambria">Venta Item de Garantia</h4>
      </div>
      <form id="for_gar" method="post">
        <input type="hidden" name="action" value="vender">
        <input type="hidden" name="hdd_gar_id" id="hdd_gar_id" value="<?php echo $_POST['gar_id']; ?>">
        <input type="hidden" name="hdd_cli_id" id="hdd_cli_id">
        <input type="hidden" name="hdd_gar_prec1" id="hdd_gar_prec1" value="<?php echo moneda_mysql($pre_ven); ?>">
        <input type="hidden" name="hdd_cuenta_dep" id="hdd_cuenta_dep">

        <div class="modal-body">

          <?php 
            if($bandera_venta == 0)
              echo $bandera_mensaje;
            if($bandera_venta == 1)
              require_once 'menorgarantia_form_vista.php'; 
          ?>

          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="ingreso_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <?php if($bandera_venta == 1):?>
              <button type="submit" class="btn btn-primary" id="btn_guardar_menorgarantia">Guardar</button>
            <?php endif;?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/menorgarantia/menorgarantia_form.js?ver=2410241045'; ?>"></script>
<script>
    function adjustModalSizeVender() {
        var modalDialogVender = document.getElementById('modalSizeVender');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogVender.classList.remove('modal-lg');
            modalDialogVender.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogVender.classList.remove('modal-xl');
            modalDialogVender.classList.add('modal-lg');
        }
    }

    adjustModalSizeVender();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizeVender);
</script>