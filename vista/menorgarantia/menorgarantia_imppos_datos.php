<?php

require_once ("../empresa/Empresa.class.php");
$oEmpresa = new Empresa();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();

require_once ("../impresora/Impresora.class.php");
$oImpresora = new Impresora();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");


$dts=$oEmpresa->mostrarUno($_SESSION['empresa_id']);
    if($dts['estado']==1){
	$emp_ruc=$dts['data']['tb_empresa_ruc'];
	$emp_nomcom=$dts['data']['tb_empresa_nomcom'];
	$emp_razsoc=$dts['data']['tb_empresa_razsoc'];
	$emp_dir=$dts['data']['tb_empresa_dir'];
	$emp_dir2=$dts['data']['tb_empresa_dir2'];
	$emp_tel=$dts['data']['tb_empresa_tel'];
	$emp_ema=$dts['data']['tb_empresa_ema'];
	$emp_fir=$dts['data']['tb_empresa_fir'];		
    }
    $dts=NULL;


$dts=$oIngreso->mostrarUno($_POST['ing_id']);
    if($dts['estado']==1){
	$fecreg		= mostrar_fecha_hora($dts['data']['tb_ingreso_fecreg']);
	$fecmod		=mostrar_fecha_hora($dts['data']['tb_ingreso_fecmod']);
	$usureg		=$dts['data']['tb_ingreso_usureg'];
	$usumod		=$dts['data']['tb_ingreso_usumod'];
	
	$fec		= mostrar_fecha($dts['data']['tb_ingreso_fec']);
	$doc_id 	=$dts['data']['tb_documento_id'];
	$numdoc		=$dts['data']['tb_ingreso_numdoc'];
	
	$det		=$dts['data']['tb_ingreso_det'];
	
	$cue_id		=$dts['data']['tb_cuenta_id'];
	$subcue_id	=$dts['data']['tb_subcuenta_id'];

	$cli_id		=$dts['data']['tb_cliente_id'];
	$cli_nom 	=$dts['data']['tb_cliente_nom'];
	$cli_doc 	=$dts['data']['tb_cliente_doc'];
	$cli_dir 	=$dts['data']['tb_cliente_dir'];
	$cli_tip 	=$dts['data']['tb_cliente_tip'];
	
	$imp		=$dts['data']['tb_ingreso_imp'];
	
	$caj_id		=$dts['data']['tb_caja_id'];

	$ven_id		=$dts['data']['tb_venta_id'];

	$mon_id		=$dts['data']['tb_moneda_id'];
	
	$est		=$dts['data']['tb_ingreso_est'];
    }
    $dts=NULL;

/*
if($emp_ruc==0)$emp_ruc="";

//venta
$dts= $oVenta->mostrarUno($ven_id);
$dt = mysql_fetch_array($dts);
	$reg	=mostrarFechaHoraH($dt['tb_venta_reg']);
	
	$fec	=mostrarFecha($dt['tb_venta_fec']);
	
	$doc_id	=$dt['tb_documento_id'];
	$doc_nom=$dt['tb_documento_nom'];
	$numdoc	=$dt['tb_venta_numdoc'];
	
	$cli_id	=$dt['tb_cliente_id'];
	$cli_nom=$dt['tb_cliente_nom'];
	$cli_doc=$dt['tb_cliente_doc'];
	$cli_dir=$dt['tb_cliente_dir'];
	
	$valven	=$dt['tb_venta_valven'];
	$igv	=$dt['tb_venta_igv'];
	$tot	=$dt['tb_venta_tot'];
	
	$lab1	=$dt['tb_venta_lab1'];
	
	$punven_id	=$dt['tb_puntoventa_id'];
	$usu_id	=$dt['tb_usuario_id'];
mysql_free_result($dts);

//cliente
if($cli_id==1)
{
	$cli_doc="";
}

//punto de venta
$dts=$oPuntoventa->mostrarUno($punven_id);
$dt = mysql_fetch_array($dts);
	$punven_nom=$dt['tb_puntoventa_nom'];
	$alm_id=$dt['tb_almacen_id'];
	$caj_id=$dt['tb_caja_id'];
	$punven_imp_id=$dt['tb_impresora_id'];
mysql_free_result($dts);

//documento
$dts=$oDocumento->mostrarUno($doc_id);
$dt = mysql_fetch_array($dts);
	$doc_actimp	=$dt['tb_documento_actimp'];
	$doc_imp_id=$dt['tb_impresora_id'];
mysql_free_result($dts);

//selección de impresora
if($doc_actimp==1)
{
	$imp_id=$doc_imp_id;
}
else
{
	$imp_id=$punven_imp_id;
}
*/

//impresora
$imp_id=1;
if($imp_id>0)
{
	$dts=$oImpresora->mostrarUno($imp_id);
	if($dts['estado']==1){
		$imp_nom 	=$dts['data']['tb_impresora_nom'];
		$imp_nomloc     =$dts['data']['tb_impresora_nomloc'];
		$imp_ser 	=$dts['data']['tb_impresora_ser'];
		$imp_url 	=$dts['data']['tb_impresora_url'];
		$imp_ip 	=$dts['data']['tb_impresora_ip'];
        }
        $dts=NULL;
}

//cajero
$dts=$oUsuario->mostrarUno($usureg);
if($dts['estado']==1){
	$usu_nom	=$dts['data']['tb_usuario_nom'];
	$usu_apepat	=$dts['data']['tb_usuario_apepat'];
	$usu_apemat	=$dts['data']['tb_usuario_apemat'];
}
$dts=NULL;

//$texto_cajero=substr($usu_nom, 0, 3).substr($usu_apepat, 0, 1).substr($usu_apemat, 0, 1);
$cajero=$usu_nom.' '.$usu_apepat.' '.$usu_apemat;

//------------impresion
$data['imp_nom'] 	=$imp_nom;
$data['imp_nomloc'] =$imp_nomloc;
$data['imp_ser'] 	=$imp_ser;
$data['imp_url'] 	=$imp_url;
$data['imp_ip']	 	=$imp_ip;

$data['emp_razsoc'] =$emp_razsoc;
$data['emp_ruc']	=$emp_ruc;
$data['emp_dir1']	=$emp_dir;
$data['emp_dir2']	=$emp_dir2;

$data['fecha'] 			= $fec;
$data['fechareg'] 		= $fecreg;

$data['operacion'] 		= $numdoc;

$data['cliente'] 		= $cli_nom;
$data['cliente_doc']	= $cli_doc;
$data['cliente_dir']	= $cli_dir;

$data['detalle'] 	= $det;

$data['total'] 		= $imp;
$data['mon_id'] 	= $mon_id;
$data['total_letras'] =numtoletras($imp,$tb_mon_id);

$data['cajero']		=$cajero;



echo json_encode($data);

function sanear_string($string)
{
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
  
    return $string;
}

?>