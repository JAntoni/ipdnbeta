<?php
require_once('../../core/usuario_sesion.php');
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$venin_id = $_POST['venin_id'];
$tipo = intval($_POST['tipo']);
$pago_parcial = $_POST['pago_parcial'];

//      echo 'hasta aki estoy entrando normal =='.$venin_id;exit();
$forma_pago = array('selected', '', '');
$empresa_nom = $_SESSION['empresa_nombre'];

$dts = $oVentainterna->mostrarUno($venin_id);
  if ($dts['estado'] == 1) {
    $reg_origin = $dts['data'];
    $mon_tot = $dts['data']['tb_ventainterna_mon'];
    $mon_pag = $dts['data']['tb_ventainterna_monpag'];
    if (!empty($reg_origin['tb_ingreso_id'])) {
      $forma_pago[$reg_origin['tb_ventainterna_formapago']] = 'selected';
      $empresa_nom = $reg_origin['tb_empresa_nomcom'];
    }
  }
$dts = NULL;

$prev = mostrar_moneda($mon_pag);
$saldo = mostrar_moneda($mon_tot - $mon_pag);

$vista = 'credito';

if (isset($_POST['vista']))
  $vista = $_POST['vista'];

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_pagarventainterna_registro" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-weight: bold;font-family: cambria">Venta Item de Garantia</h4>
      </div>
      <form id="form_pago_ventainterna">
        <input type="hidden" name="action" id="action" value="abonar_interno">
        <input type="hidden" name="hdd_saldo_pag" id="hdd_saldo_pag" value="<?php echo $saldo; ?>">
        <input type="hidden" name="venin_id" id="venin_id" value="<?php echo $venin_id; ?>">
        <input type="hidden" id="hdd_vista" name="hdd_vista" value="<?php echo $vista; ?>">
        <input type="hidden" id="hdd_saldo" name="hdd_saldo" value="<?php echo $saldo; ?>">
        <input type="hidden" id="hdd_tipo" name="hdd_tipo" value="<?php echo $tipo; ?>">
        <input type="hidden" name="hdd_ventainternapago_reg" id="hdd_ventainternapago_reg" value='<?php echo json_encode($reg_origin); ?>'>

        <div class="modal-body">
          <div class="box box-primary">
            <div class="box-header">
              <div class="row">
                <div class="col-md-6">
                  <label for="txt_empresa_nom">Empresa:</label>
                  <input type="text" name="txt_empresa_nom" id="txt_empresa_nom" readonly class="form-control input-sm" value="<?php echo $empresa_nom; ?>">
                </div>
                <div class="col-md-6">
                  <label>Fecha:</label>
                  <input type="text" name="txt_fecha_pago" value="<?php echo date('d-m-Y'); ?>" readonly class="form-control input-sm">
                </div>
              </div>
              <p>
              <div class="row">
                <div class="col-md-6">
                  <label>Monto Total S/.</label>
                  <input type="text" name="txt_monto_total" value="<?php echo mostrar_moneda($mon_tot) ?>" readonly class="form-control input-sm">
                </div>
                <div class="col-md-6">
                  <label>Pagos Previos S/.</label>
                  <!-- <input type="text" name="txt_pagos_pre" value="<?php echo mostrar_moneda($mon_pag) ?>" readonly class="form-control input-sm"> -->
                  <input type="text" name="txt_pagos_pre" value="<?php echo $prev ?>" readonly class="form-control input-sm">
                </div>
              </div>
              <p>
              <div class="row">
                <div class="col-md-6">
                  <label>Saldo a Pagar S/.</label>
                  <input type="text" name="txt_saldo_pagar" value="<?php echo ($saldo) ?>" readonly class="form-control input-sm">
                </div>
                <div class="col-md-6">
                  <label>Monto Pagar S/.</label>
                  <?php if ($pago_parcial > 0) { // =1 
                  ?>
                    <input type="text" name="txt_monto_pagar" id="txt_monto_pagar" <?php if ($vista == 'planilla') echo 'readonly'; ?> value="<?php echo $saldo ?>" class="form-control input-sm">
                  <?php } else { ?>
                    <input type="text" name="txt_monto_pagar" id="txt_monto_pagar" readonly value="<?php echo ($mon_tot) ?>" class="form-control input-sm">
                  <?php } ?>
                </div>
              </div>
              <p>
              <div class="row">
                <?php if ($vista == 'credito') { ?>
                  <div class="col-md-6">
                    <label for="cbo_formapago">Forma pago:</label>
                    <select name="cbo_formapago" id="cbo_formapago" class="form-control input-sm">
                      <option value="0" <?php echo $forma_pago[0]; ?>>Seleccione</option>
                      <option value="1" <?php echo $forma_pago[1]; ?>>En Caja Oficina</option>
                      <option value="2" <?php echo $forma_pago[2]; ?>>Por Banco</option>
                    </select>
                  </div>
                  <div class="col-md-6 num-ope" style="display: none;">
                    <label for="txt_numope">N° Operación:</label>
                    <input type="text" name="txt_numope" id="txt_numope" class="form-control input-sm" placeholder="Num. operación">
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>



          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="ingreso_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/menorgarantia/ventainterna_pago_form.js?ver=27062023'; ?>"></script>