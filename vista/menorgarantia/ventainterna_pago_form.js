/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var error = 0;
$(document).ready(function () {
    $('#txt_monto_pagar').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
//        vMax: parseFloat($("#txt_saldo_pagar").val()) + 1
        vMax: '999999999.99'
    });

    $("#cbo_formapago").change(function () {
        var forma_id = $("#cbo_formapago option:selected").val();
        if (forma_id == "2") {
            $("div .num-ope").show(200);
        } else {
            $("div .num-ope").hide(200);
            $('#txt_numope').val('');
        }
    });

    if ($("#cbo_formapago option:selected").val() == 2) {
        $("div .num-ope").show();
    } else {
        $("div .num-ope").hide();
    }
    
    $('#txt_numope, #txt_monto_pagar').blur(function(){
        if($('#txt_numope').val().length > 0 && $('#txt_monto_pagar').val().length > 0){
            verificar_numope();
        }
    });

    $(function () {
        $('#form_pago_ventainterna').submit(function (event) {
            event.preventDefault();
            var pag = Number($('#txt_monto_pagar').autoNumeric('get'));
//            var pag = Number($('#txt_monto_pagar').autoNumericGet());
            if (!pag || pag == 0) {
//                alert('Ingrese un monto a pagar');
                swal_warning("AVISO",'Ingrese un monto a pagar',2500);
                return false;
            }
            var formapago = parseInt($('#cbo_formapago option:selected').val());
            var num_ope = $('#txt_numope').val();
            if (formapago < 1 || (formapago == 2 && num_ope.length < 2)) {
                var mensaje = formapago < 1 ? 'Seleccione forma de pago' : 'Numero de operacion invalido';
                swal_warning("AVISO", mensaje, 3500);
                return false;
            }
            if (formapago == 2 && error == 1) {
                return false;
            }
            $.ajax({
                type: "POST",
                url: VISTA_URL+"menorgarantia/ventainterna_reg.php",
                async: true,
                dataType: "json",
                data: $('#form_pago_ventainterna').serialize(),
                beforeSend: function () {
//                    $('#btn_guar_pag').hide();
//                    $('#msj_pagointerno').show(300);
//                    $('#msj_pagointerno').text('Guardando pago...');
                },
                success: function (data) {
                    if (parseInt(data.ing_id) > 0) {
//                        $('#msj_comprarcola').show();
//                        $('#msj_comprarcola').text(data.msj);
                        swal_success("SISTEMA",data.msj,2500);
                        var vista = $('#hdd_vista').val();

                        if (vista == 'credito') {
                            compracolaborador_tabla();
                        }
                        if (vista == 'planilla') {
                            planilla_colaborador();
//                            $('#td_planilla_pagar').html();
//                            $('#td_planilla_pagar').html('<span style="color: green;">Pagado</span>');
                        }

                        $('#modal_pagarventainterna_registro').modal('hide');
                    }
                },
                complete: function (data) {
                    if (data.statusText != "success") {
                        $('#msj_pagointerno').text('Error: ' + data.responseText);
                        console.log(data);
                    }
                }
            });

        });
    });

});

function verificar_numope() {
    const action_original = $('#action').val();
    $('#action').val('verificar_numope');
    var form_serializado = serializar_form();
    $('#action').val(action_original);
    $.ajax({
        type: "POST",
        url: VISTA_URL + "menorgarantia/ventainterna_reg.php",
        async: true,
        dataType: "json",
        data: form_serializado,
        beforeSend: function () {
            $("#ingreso_mensaje").removeClass("callout-warning").addClass("callout-info");
            $("#ingreso_mensaje").html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
            $("#btn_guardar_menorgarantia").prop("disabled", true);
        },
        success: function (data) {
            $("#btn_guardar_menorgarantia").prop("disabled", false);
            $("#ingreso_mensaje").hide(400);
            if (data.estado != 1) { //si hay error retorna uno
                alertas_error('Verifique', data.mensaje, 'small');
                error = 1;
            } else {
                error = 0;
            }
        }
    });
}

function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            close: function () {
            }
        }
    });
}

function serializar_form() {
    var extra = ``;
    var elementos_disabled = $('#form_pago_ventainterna').find('input:disabled, select:disabled').removeAttr('disabled');
    var form_serializado = $('#form_pago_ventainterna').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}