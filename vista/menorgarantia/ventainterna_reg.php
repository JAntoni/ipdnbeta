<?php

//date_default_timezone_set('America/Lima');
require_once('../../core/usuario_sesion.php');
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$venin_id = $_POST['venin_id'];
$tipo= $_POST['hdd_tipo'];
$num_operacion = $_POST['txt_numope'];
$vista = $_POST['hdd_vista'];

$fecha_hoy = date('d-m-Y');

if ($action == 'abonar_interno') {

    $dts = $oVentainterna->mostrarUno($venin_id);
    if ($dts['estado'] == 1) {
        $gar_id = $dts['data']['tb_garantia_id'];
        $gar_pro = $dts['data']['tb_garantia_pro'];
        $usu_nom = $dts['data']['tb_usuario_nom'];
        $ven_mon = $dts['data']['tb_ventainterna_mon'];
    }
    $dts = NULL;

    $dts = $oGarantia->mostrarUno($gar_id);
    if ($dts['estado'] == 1) {
        $cre_id = $dts['data']['tb_credito_id'];
    }
    $dts = NULL;

    $saldo = moneda_mysql($_POST['hdd_saldo_pag']); //saldo a pagar
    $monto_pag = moneda_mysql($_POST['txt_monto_pagar']); //monto a pagar

    $mod_id = 90; //90 ingreso por venta de garantías
    $num_doc = '90-' . str_pad($gar_id, 3, "0", STR_PAD_LEFT);
    // Gerson 14-12-23
    if($vista=='planilla'){
        $ing_det='Ingreso parcial por compra interna desde el módulo planilla de: ' . $gar_pro . ', por: ' . $usu_nom . ', del crédito CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    }elseif($vista=='credito'){
        $ing_det='Ingreso parcial por compra interna desde el módulo compra colaborador de: ' . $gar_pro . ', por: ' . $usu_nom . ', del crédito CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    }
    //
    
        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = fecha_mysql($fecha_hoy); 
        $oIngreso->documento_id = 8; 
        $oIngreso->ingreso_numdoc = $num_doc;
        $oIngreso->ingreso_det = $ing_det; 
        $oIngreso->ingreso_imp = moneda_mysql($monto_pag);
        //$oIngreso->cuenta_id = 1;
        //$oIngreso->subcuenta_id = 5; 
        $oIngreso->cuenta_id = 5; // 5 = REMATE
        $oIngreso->subcuenta_id = 181; // 181 = PAGO POR CAJA / 182 = PAGO POR PLANILLA 
        $oIngreso->cliente_id = 1; 
        $oIngreso->caja_id = 1; 
        $oIngreso->moneda_id = 1;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id; 
        $oIngreso->ingreso_modide = $gar_id; 
        $oIngreso->empresa_id = $_SESSION['empresa_id'];
        $oIngreso->ingreso_fecdep = ''; 
        $oIngreso->ingreso_numope = ''; 
        $oIngreso->ingreso_mondep = 0; 
        $oIngreso->ingreso_comi = 0; 
        $oIngreso->cuentadeposito_id = 0; 
        $oIngreso->banco_id = 0; 
        $oIngreso->ingreso_ap = 0; 
        $oIngreso->ingreso_detex = '';

        $result=$oIngreso->insertar();
            if(intval($result['estado']) == 1){
                $ing_id = $result['ingreso_id'];
            }

    $num_doc = $num_doc . '-' . $ing_id;
    $oIngreso->modificar_campo($ing_id,'tb_ingreso_numdoc',$num_doc,'STR');
    $oIngreso->modificar_campo($ing_id,'tb_ventainterna_id',$venin_id,'INT'); // ingresar id de la ventainterna

    // Gerson 14-12-23
    if($vista=='planilla'){
        $his = '<span style="color: blue;">Se ha hecho un abono desde el módulo <strong>planilla</strong> de S/. ' . mostrar_moneda($monto_pag) . ' a la garantía comprada por: <b>' . $usu_nom . '</b>. Pago registrado por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</span><br>';
    }elseif($vista=='credito'){
        $his = '<span style="color: blue;">Se ha hecho un abono desde el módulo <strong>compra colaborador</strong> de S/. ' . mostrar_moneda($monto_pag) . ' a la garantía comprada por: <b>' . $usu_nom . '</b>. Pago registrado por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</span><br>';
    }
    //

    //agregar el monto pagado a la columna monpag de la tabla tb_ventainterna, sumar el monto registrado más el abonado
    $oVentainterna->sumar_montopagado($venin_id, $monto_pag);

    if ($monto_pag >= $saldo){
        $oVentainterna->estado_ventainterna($venin_id, 1, $ing_id, $his); //parametro 1 es pagado
    }
    else{
        $oVentainterna->estado_ventainterna($venin_id, 0, $ing_id, $his); //parametro 0 es pendiente de pago
    }
    
    if ($tipo==0){
        $oVentainterna->modificar_campo($venin_id, 'tb_ventainterna_tipo', 0, 'INT');//venta por modulo de pago de compra colaboradores
        
        $forma_pago = intval($_POST['cbo_formapago']);
        if ($forma_pago == 2){
            $consulta_deposito = $oDeposito->listar_depositos_paragasto($num_operacion);
            $deposito = $consulta_deposito['data'];
            unset($consulta_deposito);

            //Nos guiamos de modulo (flujocaja -> boton caja banco)
			//BAnco 2: interbank pero no tiene una subcuenta creada.
			//BANCO_ID : 1 = bcp, 3=BBVA, 4=otros bancos
            if(is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BCP'))){
				$banco_id = 1;
				$subcuenta_ingreso_id = 147;//BANCO DE CREDITO
			} elseif (is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BBVA'))){
				$banco_id = 3;
				$subcuenta_ingreso_id = 148;//BBVA CONTINENTAL
			} else {
				$banco_id = 4;
				$subcuenta_ingreso_id = 183;//OTROS BANCOS
			}

            //en venta interna modificar la forma de pago
            $oVentainterna->modificar_campo($venin_id, 'tb_ventainterna_formapago', $forma_pago, 'INT');
            //en venta interna modificar el nro_operacion
            $oVentainterna->modificar_campo($venin_id, 'tb_ventainterna_numope', $num_operacion, 'STR');
            //modificar los campos de ingreso
            $oIngreso->modificar_campo($ing_id, 'tb_cuenta_id', 43, 'INT');//cuenta INGRESO/EGRESO POR BANCO
            $oIngreso->modificar_campo($ing_id, 'tb_subcuenta_id', $subcuenta_ingreso_id, 'INT');
            $oIngreso->modificar_campo($ing_id, 'tb_ingreso_fecdep', $deposito['tb_deposito_fec'], 'STR');
            $oIngreso->modificar_campo($ing_id, 'tb_ingreso_numope', $num_operacion, 'STR');
            $oIngreso->modificar_campo($ing_id, 'tb_ingreso_mondep', moneda_mysql($monto_pag), 'STR');
            $oIngreso->modificar_campo($ing_id, 'tb_ingreso_comi', 0, 'STR');
            $oIngreso->modificar_campo($ing_id, 'tb_cuentadeposito_id', $deposito['tb_cuentadeposito_id'], 'STR');
            $oIngreso->modificar_campo($ing_id, 'tb_banco_id', $banco_id, 'STR');

            // Gerson 14-12-23
            if($vista=='planilla'){
                $egr_det = 'EGRESO DE CAJA POR LA COMPRA INTERNA DESDE EL MÓDULO PLANILLA DE: ' . $gar_pro . ', POR: ' . $usu_nom . ', DEL CRÉDITO CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT).". INGRESO/EGRESO POR BANCO CON NUMERO DE OPERACION: $num_operacion, INGRESO ID: $ing_id";
            }elseif($vista=='credito'){
                $egr_det = 'EGRESO DE CAJA POR LA COMPRA INTERNA DESDE EL MÓDULO COMPRA COLABORADOR DE: ' . $gar_pro . ', POR: ' . $usu_nom . ', DEL CRÉDITO CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT).". INGRESO/EGRESO POR BANCO CON NUMERO DE OPERACION: $num_operacion, INGRESO ID: $ing_id";
            }
            //

            $pro_id = 18;
            //en caja realizar egreso hacia la cuenta banco de prestamos del norte
            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = fecha_mysql($fecha_hoy);
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = '';
            $oEgreso->egreso_det = $egr_det;
            $oEgreso->egreso_imp = moneda_mysql($monto_pag);
            $oEgreso->egreso_tipcam = 1;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = 11;//haberes
            $oEgreso->subcuenta_id = 39;//otros
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = 1;
            $oEgreso->moneda_id = 1;
            $oEgreso->modulo_id = 90;//90 para egreso por pago en banco de venta garantias
            $oEgreso->egreso_modide = intval($gar_id);
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0;
            $result = $oEgreso->insertar();
            if (intval($result['estado']) == 1) {
                $egr_id = $result['egreso_id'];
            }
        }
    }
    else{
        $oVentainterna->modificar_campo($venin_id, 'tb_ventainterna_tipo', 1, 'INT');// pago por planilla

        $oIngreso->modificar_campo($ing_id,'tb_subcuenta_id', 182,'INT'); // Si el pago es por planilla se edita el valor por defecto por la subcuenta 182

        // REGISTRO DE EGRESO
        
            $cuenta = 11;//haberes
            $subcuenta = 39;//otros
    
            $tipo_pag = 'LOS HABERES MENSUALES';
            $egr_det = ' EGRESO FICTICIO DE CAJA POR LA COMPRA INTERNA DE: ' . $gar_pro . ', POR: ' . $usu_nom . ', DEL CRÉDITO CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
            $pro_id=18;


            //$det = 'EGRESO FICTICIO POR [ INGRESO ] DE obs : [ ' . $_POST['txt_cuopag_obs'] . ' ]. Pago abonado en el banco, en la siguente cuenta: [ ' . $_POST['hdd_cuenta_dep'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fecdep'] . ' ], la fecha de validación fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['txt_cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ]';
            $caj_id = 1; //caja id 14 ya que es CAJA AP
            //$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-';

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = fecha_mysql($fecha_hoy);
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = '';
            $oEgreso->egreso_det = $egr_det;
            $oEgreso->egreso_imp = moneda_mysql($monto_pag);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cuenta;
            $oEgreso->subcuenta_id = $subcuenta;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = 1;
            $oEgreso->moneda_id = 1;
            $oEgreso->modulo_id = 0;
            $oEgreso->egreso_modide = intval($gar_id);
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $result = $oEgreso->insertar();
            if (intval($result['estado']) == 1) {
                $egr_id = $result['egreso_id'];
            }

            //echo ' hasta aki estoy entrando al sistemaa';exit();
            $oEgreso->modificar_campo($egr_id, 'tb_egreso_numdoc', $_SESSION['usuario_id'], 'STR');   
    }
    
    $data['ing_id'] = $ing_id;
    $data['msj'] = 'Se registró el pago de la garantía: ' . $gar_pro;

    echo json_encode($data);
}
if ($action == 'pagointerno') {
    $dts = $oVentainterna->mostrarUno($venin_id);
        if($dts['estado']==1){
            $gar_id  = $dts['data']['tb_garantia_id'];
            $gar_pro = $dts['data']['tb_garantia_pro'];
            $usu_nom = $dts['data']['tb_usuario_nom'];
            $ven_mon = $dts['data']['tb_ventainterna_mon'];
        }
        $dts = NULL;

    $dts = $oGarantia->mostrarUno($gar_id);
    if($dts['estado']==1){
        $cre_id = $dt['tb_credito_id'];
    }
    $dts = NULL;

    $mod_id = 90; //90 ingreso por venta de garantías
    
    $num_doc='90-' . str_pad($mod_id, 3, "0", STR_PAD_LEFT) . '-' . str_pad($gar_id, 3, "0", STR_PAD_LEFT);
    $ing_det='Ingreso compra interna de: ' . $gar_pro . ', por: ' . $usu_nom . ', del crédito CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    
    $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
    $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
    $oIngreso->ingreso_fec = $fecha_hoy; 
    $oIngreso->documento_id = 8; 
    $oIngreso->ingreso_numdoc = $num_doc;
    $oIngreso->ingreso_det = $ing_det; 
    $oIngreso->ingreso_imp = moneda_mysql($ven_mon);
    $oIngreso->cuenta_id = 1;
    $oIngreso->subcuenta_id = 5; 
    $oIngreso->cliente_id = 17; 
    $oIngreso->caja_id = 1; 
    $oIngreso->moneda_id = 1;
    //valores que pueden ser cambiantes según requerimiento de ingreso
    $oIngreso->modulo_id = $mod_id; 
    $oIngreso->ingreso_modide = $gar_id; 
    $oIngreso->empresa_id = $_SESSION['empresa_id'];
    $oIngreso->ingreso_fecdep = ''; 
    $oIngreso->ingreso_numope = ''; 
    $oIngreso->ingreso_mondep = 0; 
    $oIngreso->ingreso_comi = 0; 
    $oIngreso->cuentadeposito_id = 0; 
    $oIngreso->banco_id = 0; 
    $oIngreso->ingreso_ap = 0; 
    $oIngreso->ingreso_detex = '';

    $result=$oIngreso->insertar();
        if(intval($result['estado']) == 1){
            $ing_id = $result['ingreso_id'];
        }


    $his = '<span style="color: blue;">Se ha pagado la garantía comprada por: <b>' . $usu_nom . '</b>. Pago registrado por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</span><br>';
    $oVentainterna->estado_ventainterna($venin_id, 1, $ing_id, $his); //parametro 1 es pagado

    $data['ing_id'] = $ing_id;
    $data['msj'] = 'Se registró el pago de la garantía: ' . $gar_pro;

    echo json_encode($data);
}
if ($action == 'anularpago') {
    $dts = $oVentainterna->mostrarUno($venin_id);
    
        if($dts['estado']==1){
            $gar_id  = $dts['data']['tb_garantia_id'];
            $gar_pro = $dts['data']['tb_garantia_pro'];
            $usu_nom = $dts['data']['tb_usuario_nom'];
            $ven_mon = $dts['data']['tb_ventainterna_mon'];
            $ing_id  = $dts['data']['tb_ingreso_id'];
        }

    $his = '<span style="color: red;">Pago de la garantía anulado por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</span><br>';

    $oVentainterna->estado_ventainterna($venin_id, 0, $ing_id, $his); //parametro 0 por pagar, el id del ingreso se puede quedar, ya que si hay otro pago se actualizará

    $oIngreso->xac_ingreso_id($ing_id, 0); //anulamos el ingreso haciendolo xac = 0
    $data['ing_id'] = $ing_id;
    $data['msj'] = 'Se anuló el pago de la garantía: ' . $gar_pro;

    echo json_encode($data);
}
if ($action == 'verificar_numope') {
    $registro_origin = (array) json_decode($_POST['hdd_ventainternapago_reg']);
    $consulta_deposito = $oDeposito->listar_depositos_paragasto($num_operacion);
    $deposito_registrado = $consulta_deposito['estado'];
    $deposito = $consulta_deposito['data'];
    unset($consulta_deposito);

    if ($deposito_registrado == 1) {
        $moneda_deposito_nom = $deposito['tb_moneda_nom'];
        //la moneda del gasto y del deposito deben coincidir
        if (1 != intval($deposito['tb_moneda_id'])) {
            $deposito_registrado = 3;
            $data['mensaje'] = "Corregir deposito, debe ser registrado en una cuenta banco con moneda en soles";
        } else {
            //verificar que el monto del gasto no sobrepase el monto de deposito
            $importe_ingresos_anteriores = 0;
            $dts = $oIngreso->lista_ingresos_num_operacion($num_operacion);
            if ($dts['estado'] == 1) {
                foreach ($dts['data'] as $key => $dt) {
                    $tb_ingreso_imp = moneda_mysql(floatval($dt['tb_ingreso_imp']));
                    if ($dt['tb_ingreso_id'] != $registro_origin['tb_ingreso_id']) {
                        $importe_ingresos_anteriores += $tb_ingreso_imp;
                    }
                }
            }
            unset($dts);

            $monto_gasto = moneda_mysql($_POST['txt_monto_pagar']);
            $monto_deposito = abs($deposito['tb_deposito_mon']);
            $monto_puede_usar = moneda_mysql($monto_deposito - $importe_ingresos_anteriores);

            if ($monto_gasto > $monto_puede_usar) {
                $deposito_registrado = 2;
                $data['mensaje'] = 'El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. Monto ingresado: S/.'.mostrar_moneda($monto_gasto).', monto disponible: '.$moneda_deposito_nom.mostrar_moneda($monto_puede_usar).', TOTAL DEL DEPOSITO: '.$moneda_deposito_nom.mostrar_moneda($monto_deposito);
            }
        }
    } else {
        $data['mensaje'] = "No está registrado el deposito con numero de operacion $num_operacion";
    }
    
    $data['estado'] = $deposito_registrado;
    echo json_encode($data);
}

?>