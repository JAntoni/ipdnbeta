<?php
require_once("../funciones/fechas.php");
require_once('../usuario/Usuario.class.php');
require_once '../ingreso/Ingreso.class.php';
require_once '../cliente/Cliente.class.php';

$oUsuario = new Usuario();
$oIngreso = new Ingreso();
$oCliente = new Cliente();

$usuario_id = $_SESSION['usuario_id'];
$flag = 0;

if ($usuario_id == 2 || $usuario_id == 11 || $usuario_id == 61) { // editar solo Engel, Juan, Gerson
  $flag = 1;
}

$hoy = date('Y-m-d');

$year = date('Y');
$month = date('m');
$hoy     = $year . '-' . $month;

$aux_1 = date('Y-m-d', strtotime("{$hoy} + 1 month"));
$last_day_1 = date('Y-m-d', strtotime("{$aux_1} - 1 day"));

$fecha_1 = mostrar_fecha($last_day_1);

$aux_2 = date('Y-m-d', strtotime("{$hoy} + 2 month"));
$last_day_2 = date('Y-m-d', strtotime("{$aux_2} - 1 day"));

$fecha_2 = mostrar_fecha($last_day_2);

$aux_3 = date('Y-m-d', strtotime("{$hoy} + 3 month"));
$last_day_3 = date('Y-m-d', strtotime("{$aux_3} - 1 day"));

$fecha_3 = mostrar_fecha($last_day_3);

$aux_4 = date('Y-m-d', strtotime("{$hoy} + 4 month"));
$last_day_4 = date('Y-m-d', strtotime("{$aux_4} - 1 day"));

$fecha_4 = mostrar_fecha($last_day_4);

$aux_5 = date('Y-m-d', strtotime("{$hoy} + 5 month"));
$last_day_5 = date('Y-m-d', strtotime("{$aux_5} - 1 day"));

$fecha_5 = mostrar_fecha($last_day_5);

$aux_6 = date('Y-m-d', strtotime("{$hoy} + 6 month"));
$last_day_6 = date('Y-m-d', strtotime("{$aux_6} - 1 day"));

$fecha_6 = mostrar_fecha($last_day_6);

$validacion_monto_base  = moneda_mysql($pre_ven);
$validacion_saldo_pagar = 0;
$suma_total_ingresos    = 0;
$venta                  = 0.00;
$compra                 = 0.00;
$validacion_moneda      = 1;

$mod_id = 6; // ID NUEVO ASIGNADO PARA C-MENOR - DISPONIBLE PARA REMATE
$consultarDatosIngresosRestantes = $oIngreso->mostrar_por_modulo($mod_id, $_POST['gar_id'], 0, 0);
if ($consultarDatosIngresosRestantes['estado'] == 1) {
  foreach ($consultarDatosIngresosRestantes['data'] as $key => $value2) {

    $getClient = $value2['tb_cliente_id'];

    if ($validacion_moneda == 1) {
      if ($value2['tb_moneda_id'] == 1) {
        $suma_total_ingresos += formato_numero($value2['tb_ingreso_imp']);
      } elseif ($value2['tb_moneda_id'] == 2) {
        $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] * $value2['tb_ingreso_tipcam']));
      } else {
        $suma_total_ingresos += 0;
      }
    } elseif ($validacion_moneda == 2) {
      if ($value2['tb_moneda_id'] == 1) {
        $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] / $value2['tb_ingreso_tipcam']));
      } elseif ($value2['tb_moneda_id'] == 2) {
        $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp']));
      } else {
        $suma_total_ingresos += 0;
      }
    } else {
      $suma_total_ingresos = 0;
    }
  }
}
$consultarDatosIngresosRestantes = null;
$validacion_saldo_pagar = formato_numero($validacion_monto_base - $suma_total_ingresos);

$valid_client_id    = '';
$valid_client_dni   = '';
$valid_client_input = '';
$valid_readonly     = '';
if (moneda_mysql($suma_total_ingresos) > 0) {
  $result = $oCliente->mostrarUno($getClient);
  if ($result["estado"] == 1) {
    $valid_client_id    = $result['data']['tb_cliente_id'];
    $valid_client_dni   = $result['data']['tb_cliente_doc'];
    $valid_client_input = $result['data']['tb_cliente_nom'];
    $valid_readonly     = 'readonly';
  }
  $result = null;
}

?>
<style>
  .form-control {
    border-radius: 5px;
  }

  .text-gray {
    color: #969696 !important;
  }
</style>

<div class="container-fluid">
  <div class="row mt-4 ">
    <div class="col-md-6">

      <div class="panel panel-info mb-4">
        <div class="panel-heading">
          <b>DATOS DEL CLIENTE</b>
          <div class="box-tools pull-right">
            <a class="btn btn-primary btn-xs" title="Editar" onclick="cliente_form('I', 0)"><i class="fa fa-user-plus"></i></a> &nbsp;
            <a class="btn btn-warning btn-xs" title="Editar" onclick="cliente_form('M', 0)"><i class="fa fa-edit"></i></a>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="txt_cliente_fil_cre_doc" class="text-gray">DNI</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-address-card text-gray"></i></span>
                  </div>
                  <input type="text" name="txt_cliente_fil_cre_doc" id="txt_cliente_fil_cre_doc" class="form-control" value="<?php echo $valid_client_dni; ?>" <?php echo $valid_readonly; ?> />
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label for="txt_cliente_fil_cre_nom" class="text-gray">Cliente</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-user-circle text-gray"></i></span>
                  </div>
                  <input type="text" name="txt_cliente_fil_cre_nom" id="txt_cliente_fil_cre_nom" class="form-control mayus" value="<?php echo $valid_client_input; ?>" <?php echo $valid_readonly; ?> />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-info mb-4">
        <div class="panel-heading">
          <b>COMPRA COLABORADOR</b>
          <div class="box-tools pull-right">

          </div>
        </div>
        <div class="panel-body">
          <input type="hidden" class="form-control" name="permiso_edit_fecha" id="permiso_edit_fecha" value="<?php echo $flag; ?>">
          <input type="hidden" class="form-control" name="validationIdCliente" id="validationIdCliente" value="<?php echo $valid_client_id; ?>">

          <div class="row">
            <div class="col-md-8">
              <label class="text-gray">Venta a :</label>
              <select name="cmb_usu_id" id="cmb_usu_id" class="form-control input-sm mayus">
                <?php
                if ($usuario_id == 2 || $usuario_id == 11 || $usuario_id == 61) { // mostrar todo solo Engel, Juan, Gerson
                  //require_once '../usuario/usuario_select.php';
                  $result = $oUsuario->listar_usuarios();
                  $option = '<option value="0"></option>';
                  if ($result['estado'] == 1) {
                    foreach ($result['data'] as $key => $value) {
                      $option .= '<option value="' . $value['tb_usuario_id'] . '">' . $value['tb_usuario_nom'] . ' ' . $value['tb_usuario_ape'] . '</option>';
                    }
                  }
                  echo $option;
                } else {
                  $result = $oUsuario->mostrarUno($usuario_id);

                  $option = '<option value="0"></option>';
                  if ($result['estado'] == 1) {
                    $option .= '<option value="' . $result['data']['tb_usuario_id'] . '">' . $result['data']['tb_usuario_nom'] . ' ' . $result['data']['tb_usuario_ape'] . '</option>';
                  }
                  echo $option;
                }
                ?>
              </select>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="text-gray">Número de Cuotas :</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-list text-gray"></i></span>
                  </div>
                  <select name="cmb_gar_cuo" id="cmb_gar_cuo" class="form-control input-sm mayus">
                    <option value="0">Seleccione número</option>
                    <?php if ($usuario_id == 2 || $usuario_id == 11 || $usuario_id == 61) { // mostrar todo solo Engel, Juan, Gerson 
                    ?>
                      <option value="1">1 Cuota</option>
                      <option value="2">2 Cuotas</option>
                      <option value="3">3 Cuotas</option>
                      <option value="4">4 Cuotas</option>
                      <option value="5">5 Cuotas</option>
                      <option value="6">6 Cuotas</option>
                    <?php } else { ?>
                      <?php if ($num_cuotas > 0) {
                        $i = 0; ?>
                        <?php while ($i < $num_cuotas) { ?>
                          <option value="<?php echo $i + 1; ?>"><?php echo $i + 1; ?> Cuotas</option>
                        <?php $i++;
                        } ?>
                      <?php } ?>
                    <?php } ?>

                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4 pago1">
              <div class="form-group">
                <label for="txt_gar_pag1" class="text-gray pago1" style="display: none;">Fecha Pago 1:</label>
                <div class="input-group date" id="menorgarantia_picker1">
                  <input type="text" class="form-control pago1" name="txt_gar_pag1" id="txt_gar_pag1" value="<?php echo $fecha_1; ?>" style="display: none;">
                  <span class="input-group-addon pago1" style="display: none;">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-4 pago2">
              <div class="form-group">
                <label for="txt_gar_pag2" class="text-gray pago2" style="display: none;">Fecha Pago 2:</label>
                <div class="input-group date" id="menorgarantia_picker2">
                  <input type="text" class="form-control pago2" name="txt_gar_pag2" id="txt_gar_pag2" value="<?php echo $fecha_2; ?>" style="display: none;">
                  <span class="input-group-addon pago2" style="display: none;">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group pago3">
                <label for="txt_gar_pag3" class="text-gray pago3" style="display: none;">Fecha Pago 3:</label>
                <div class="input-group date" id="menorgarantia_picker3">
                  <input type="text" class="form-control pago3" name="txt_gar_pag3" id="txt_gar_pag3" value="<?php echo $fecha_3; ?>" style="display: none;">
                  <span class="input-group-addon pago3" style="display: none;">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 pago4">
              <div class="form-group">
                <label for="txt_gar_pag4" class="text-gray pago4" style="display: none;">Fecha Pago 4:</label>
                <div class="input-group date" id="menorgarantia_picker4">
                  <input type="text" class="form-control pago4" name="txt_gar_pag4" id="txt_gar_pag4" value="<?php echo $fecha_4; ?>" style="display: none;">
                  <span class="input-group-addon pago4" style="display: none;">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-4 pago5">
              <div class="form-group">
                <label for="txt_gar_pag5" class="text-gray pago5" style="display: none;">Fecha Pago 5:</label>
                <div class="input-group date" id="menorgarantia_picker5">
                  <input type="text" class="form-control pago5" name="txt_gar_pag5" id="txt_gar_pag5" value="<?php echo $fecha_5; ?>" style="display: none;">
                  <span class="input-group-addon pago5" style="display: none;">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-4 pago6">
              <div class="form-group">
                <label for="txt_gar_pag6" class="text-gray pago6" style="display: none;">Fecha Pago 6:</label>
                <div class="input-group date" id="menorgarantia_picker6">
                  <input type="text" class="form-control pago6" name="txt_gar_pag6" id="txt_gar_pag6" value="<?php echo $fecha_6; ?>" style="display: none;">
                  <span class="input-group-addon pago6" style="display: none;">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-footer text-danger" style="background-color: #f2dede !important;">
          <b><i class="fa fa-warning fa-fw"></i> Tener en Cuenta que de las fechas seleccionadas, se tomará el mes y año</b>
        </div>
      </div>

    </div>

    <div class="col-md-6">

      <div class="panel panel-info">
        <div class="panel-heading">
          <b>DATOS DEL PRODUCTO</b>
        </div>
        <div class="panel-body">
          <div class="row mb-4">
            <div class="col-md-3">
              <div class="form-group">
                <label for="txt_gar_fec" class="text-gray">Fecha:</label>
                <div class="input-group date" id="menorgarantia_picker6">
                  <input name="txt_gar_fec" type="text" id="txt_gar_fec" value="<?php echo $gar_fec; ?>" class="form-control">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-9">
              <div class="form-group">
                <label for="txt_gar_pro" class="text-gray">Nombre Producto:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-info-circle text-gray"></i></span>
                  </div>
                  <input name="txt_gar_pro" type="text" id="txt_gar_pro" class="form-control mayus" value="<?php echo $gar_pro; ?>" readonly="">
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col-md-3 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="text-gray" for="cmb_gar_lugpag">Lugar Pago:</label>
                <select name="cmb_gar_lugpag" id="cmb_gar_lugpag" class="form-control mayus">
                  <option value="1">En Oficinas</option>
                  <option value="2">Con depósito</option>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label><?php echo $forma_precio; ?></label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input type="text" class="form-control moneda text-right" id="txtmonto_base" name="txtmonto_base" value="<?php echo $pre_ven; ?>" readonly>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="text-gray">Ingresos Previos:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input type="text" class="form-control moneda text-right" name="txt_monto_previos" id="txt_monto_previos" value="<?php echo formato_moneda($suma_total_ingresos); ?>" readonly>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label for="txt_saldo_a_pagar" class="text-gray">Saldo a Pagar:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input type="text" class="form-control moneda text-right" name="txt_saldo_a_pagar" id="txt_saldo_a_pagar" value="<?php echo formato_moneda($validacion_saldo_pagar); ?>" readonly>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6" style="display: none;" id="precio_banco3">
              <div class="row mb-4">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="text-gray">N° Operación:</label>
                    <input type="text" name="txt_gar_numope" id="txt_gar_numope" class="form-control text-center">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_fecha_deposito" class="text-gray">Fecha de Depósito:</label>
                    <input type="date" name="txt_fecha_deposito" id="txt_fecha_deposito" class="form-control" value="<?php date('d-m-Y'); ?>" />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6" style="display: none;" id="precio_banco">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="text-gray">N° Cuenta:</label>
                    <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control">
                      <?php require_once '../cuentadeposito/cuentadeposito_select.php'; ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="display: none;" id="precio_banco2">
            <div class="col-md-3">
              <div class="form-group">
                <label class="text-gray">Monto Depósito:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input style="text-align:right;" class="form-control moneda3" name="txt_gar_mon" type="text" id="txt_gar_mon">
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="text-gray">Comisión Banco:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input style="text-align:right;" class="form-control moneda3" name="txt_gar_comi" type="text" id="txt_gar_comi" value="0.00">
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="text-gray">Monto Pagado:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input style="text-align:right;" class="form-control moneda3" name="txt_gar_montot" type="text" id="txt_gar_montot" value="0.00" readonly>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="form-group">
                <label for="txt_gar_val" class="text-gray">Monto a Pagar:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span>S/</span>
                  </div>
                  <input name="txt_gar_val" type="text" id="txt_gar_val" class="form-control" style="text-align:right;" class="moneda2">
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>

  <div class="row">
    <br>
    <div class="col-md-12">
      <div class="callout callout-info" id="creditomenor_mensaje" style="display: none;">
        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
      </div>
    </div>
  </div>

</div>

<script>
  if ($.trim($("#validationIdCliente").val()) != "") {
    var datoID = $("#validationIdCliente").val()
    $("#hdd_cli_id").val(datoID);
  }
</script>