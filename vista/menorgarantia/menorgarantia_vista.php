<?php
require_once('vista/formulaventa/Formulamenor.class.php');
$oFormula = new Formulamenor();

$dts1 = $oFormula->mostrar_vigente();

if($dts1['estado']==1){
        $formenor_id =  $dts1['data']['tb_formula_id'];
        $formenor_nom = $dts1['data']['tb_formula_nom']; //nombre de la formula
        $formenor_rec = $dts1['data']['tb_formula_rec']; //valor de recuperacion, generalmente es 1.5 a mas
        $formenor_pro = $dts1['data']['tb_formula_pro']; //numero de meses de proyeccion
    }

//if($num_rows1 > 1 || $num_rows1 == 0){
//  echo '<h2>NO SE HA SELECCIONADO UNA FÓRMULA PARA LA VENTA O EXISTE MÁS DE UNO EN ESTADO VIGENTE. CONSULTE CON ADMISTRADOR DE SISTEMA: '.$num_rows1.'</h2>';
//  exit();
//}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
            <h1>
                    <?php echo $menu_tit; ?>
                    <small><?php echo $menu_des; ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
                <li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
            </ol>
	</section>

	<!-- Main content -->
	<section class="content">

            <div class="box">
                <h4  style="font-weight: bold;text-align: center;font-family: cambria">FÓRMULA VIGENTE : [ <?php echo $formenor_nom;?> ]</h4>
                <div class="box-header">
                    <?php include_once 'menorgarantia_filtro.php';?>
                </div>
                <div class="box-body">
                        <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="menorgarantia_mensaje_tbl" style="display: none;">
                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Garantias de Créditos Menores...</h4>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Input para guardar el valor ingresado en el search de la tabla-->
                            <input type="hidden" id="hdd_datatable_fil">

                            <div id="div_menorgarantia_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                    <?php // require_once('menorgarantia_tabla.php');?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="div_modal_creditomenor_pagos"></div>
                <div id="div_modal_creditomenor_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE CREDITO-->
                </div>
                <div id="div_modal_garantiaventa_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE CREDITO-->
                </div>
                <div id="div_modal_garantia_resumen">
				<!-- INCLUIMOS EL MODAL PARA VER EL RESUMEN DE UNA GARANTIA-->
                </div>
                <div id="div_modal_cliente_form">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
                </div>
                <div id="div_cobranza_form">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
                </div>
                <div id="div_modal_carousel">
                    <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
                </div>
                <div id="div_pagar_ventainterna">
                    <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
                </div>
                <div id="div_modal_upload_form">
				<!-- INCLUIMOS EL MODAL PARA SUBIR IMAGENES -->
                </div>
                    <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
                <div id="div_gastogar_tabla"></div>
                <div id="div_gastogar_form"></div>
                <div id="div_gastogar_historial_form"></div>
                <div id="div_modal_subasta"></div>
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
