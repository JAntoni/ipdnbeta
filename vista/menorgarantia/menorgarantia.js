var datatable_global;

function menorgarantia_tabla() {
  estado_gar = parseInt($("#cmb_gar_est").val());

  $.ajax({
    type: "POST",
    url: VISTA_URL + "menorgarantia/menorgarantia_tabla.php",
    async: true,
    dataType: "html",
    data: {
      pro_est: $("#cmb_fil_pro_est").val(),
      formenor_rec: $("#hdd_formenor_rec").val(),
      formenor_pro: $("#hdd_formenor_pro").val(),
      cre_est: estado_gar,
      gar_oro: $("#che_gar_oro").val(),
    },
    beforeSend: function () {
      $("#menorgarantia_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_menorgarantia_tabla").html(data);
      $("#menorgarantia_mensaje_tbl").hide(300);
      estilos_datatable();
    },
    complete: function (data) {},
    error: function (data) {
      $("#menorgarantia_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CRÉDITO: " + data.responseText
      );
    },
  });
}
function estilos_datatable() {
  datatable_global = $("#tbl_menorgarantias").DataTable({
    pageLength: 25,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    order: [[0, "asc"]],
    columnDefs: [{ targets: "no-sort", orderable: false }],
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
  $(".dataTables_filter input").attr("id", "txt_datatable_fil");

  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

function creditomenor_pagos(creditomenor_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditomenor/creditomenor_pagos.php",
    async: true,
    dataType: "html",
    data: {
      credito_id: creditomenor_id,
      hdd_form: "vencimiento",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_creditomenor_pagos").html(data);
        $("#modal_creditomenor_pagos").modal("show");

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto("modal_creditomenor_pagos", 95);
        modal_height_auto("modal_creditomenor_pagos"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_creditomenor_pagos", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

function creditomenor_form(usuario_act, creditomenor_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditomenor/creditomenor_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      creditomenor_id: creditomenor_id,
      vista: "creditomenor",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_creditomenor_form").html(data);
        $("#modal_registro_creditomenor").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_creditomenor"); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto("modal_registro_creditomenor", 95);
        modal_height_auto("modal_registro_creditomenor"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_creditomenor", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "creditomenor";
        var div = "div_modal_creditomenor_form";
        permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

function garantia_resumen(garantia_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "garantia/garantia_resumen.php",
    async: true,
    dataType: "html",
    data: {
      garantia_id: garantia_id,
      vista: "garantia",
      action: "I",
      formenor_rec: $("#hdd_formenor_rec").val(),
      formenor_pro: $("#hdd_formenor_pro").val(),
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_garantia_resumen").html(data);
        $("#modal_garantia_resumen").modal("show");
        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        //modal_width_auto('modal_garantia_resumen',50);
        modal_height_auto("modal_garantia_resumen"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_garantia_resumen", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alerta_error("Error", data.responseText);
      console.log(data);
    },
  });
}

function garantia_venta(idf, int) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "menorgarantia/menorgarantia_form.php",
    async: true,
    dataType: "html",
    data: {
      gar_id: idf,
      int: int,
      vista: "credito_tabla",
      formenor_rec: $("#hdd_formenor_rec").val(),
      formenor_pro: $("#hdd_formenor_pro").val(),
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_garantiaventa_form").html(data);
        $("#modal_garantiaventa_registro").modal("show");

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto("modal_garantiaventa_registro", 75);
        modal_height_auto("modal_garantiaventa_registro"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_garantiaventa_registro", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

function carousel(modulo_nom, modulo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "templates/carousel.php",
    async: true,
    dataType: "html",
    data: {
      modulo_nom: modulo_nom,
      modulo_id: modulo_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Galería");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      $("#div_modal_carousel").html(data);
      $("#modal_carousel_galeria").modal("show");

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_carousel_galeria"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_carousel_galeria", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

function anular_garantia_venta(gar) {
  if (gar <= 0 || gar == "") {
    swal_warning("AVISO", "No es una garantía válida", 3000);
    return false;
  }
  $.confirm({
    icon: "fa fa-print",
    title: "Imprimir",
    content: "¿Realmente desea anular la venta?",
    type: "blue",
    theme: "material", // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function () {
        //ejecutamos AJAX
        $.ajax({
          type: "POST",
          url: VISTA_URL + "menorgarantia/menorgarantia_anular.php",
          async: true,
          dataType: "json",
          data: {
            gar_id: gar,
          },
          beforeSend: function () {
            //alert(datos);
          },
          success: function (data) {
            //console.log(data);
            if (parseInt(data.gar_id) > 0) {
              swal_success("SISTEMA", data.gar_msj, 2000);
              menorgarantia_tabla();
            } else {
              swal_error("ERROR", data, 6000);
            }
          },
          complete: function (data) {
            //			console.log(data);
          },
        });
      },
      no: function () {},
    },
  });
}

function cliente_form(usuario_act, cliente_id) {
  console.log("data");
  var cliente_fil_cre_id = parseInt($("#hdd_cli_id").val());
  if (usuario_act == "M") {
    if (!Number.isInteger(cliente_fil_cre_id) || cliente_fil_cre_id <= 0)
      return false;
    else cliente_id = cliente_fil_cre_id;
  }
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: cliente_id,
      vista: "menorgarantia",
      //            vista: 'credito'
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");
        $("#modal_mensaje").modal("hide");
        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "cliente";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {
      console.log(data);
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data.responseText);
    },
  });
}

function menorgarantia_imppos_datos(gar_id) {
  $.confirm({
    icon: "fa fa-print",
    title: "Imprimir",
    content: "¿Realmente desea Imprimir baucher?",
    type: "blue",
    theme: "material", // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function () {
        //ejecutamos AJAX
        $.ajax({
          type: "POST",
          url: VISTA_URL + "menorgarantia/menorgarantia_imppos_datos.php",
          async: true,
          dataType: "json",
          data: {
            ing_id: gar_id,
          },
          beforeSend: function () {
            //$('#msj_ingreso_imp').html("Imprimiendo.");
            //$('#msj_ingreso_imp').show(100);
          },
          success: function (data) {
            //alert(data);
            // $.each(data, function(i, item) {
            //     console.log(item);
            // });
            menorgarantia_imppos_ticket(data);
          },
          complete: function (data) {
            console.log(data);
          },
        });
      },
      no: function () {},
    },
  });
}

function menorgarantia_imppos_ticket(datos) {
  $.ajax({
    type: "POST",
    url: "http://127.0.0.1/prestamosdelnorte/app/modulos/creditomenorgarantia/menorgarantia_imppos_ticket.php",
    async: true,
    dataType: "html",
    data: datos,
    beforeSend: function () {
      //alert(datos);
      //$('#msj_ingreso_imp').html("Imprimiendo...");
      //$('#msj_ingreso_imp').show(100);
    },
    success: function (html) {
      //alert('ok');
      $("#msj_ingreso_imp").html(html);
    },
  });
}

function cobranza_form(idf, id2) {
  var cliente = $("#span_cli" + idf).text();
  //console.log('cliente: ' + cliente);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cobranza/cobranza_form.php",
    async: true,
    dataType: "html",
    data: {
      cuo_id: idf,
      cuo_tip: id2,
      cli_nom: cliente,
      vista: "remate",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_cobranza_form").html(html);
      $("#modal_registro_cobranza").modal("show");
      $("#modal_mensaje").modal("hide");

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal("modal_registro_cobranza", "limpiar"); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_registro_cobranza"); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un ancho automatico al modal, al abrirlo
      //                    modal_width_auto('modal_registro_cobranza', 75); //funcion encontrada en public/js/generales.js
    },
    complete: function (html) {
      //                    console.log(html);
    },
  });
}

function aprobar_venta(credito_id, usuario_id) {
  Swal.fire({
    title: "¿DESEAS APROBAR ESTE CRÉDITO PARA SER VENDIDO?",
    icon: "info",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-home"></i> Si Apruebo',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Salir',
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#dd4b39",
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "menorgarantia/menorgarantia_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: 'aprobar_venta',
          credito_id: credito_id,
          usuario_id: usuario_id
        },
        beforeSend: function () {
          
        },
        success: function (data) {
          if(parseInt(data.estado) == 1){
            notificacion_success(data.mensaje, 5000);
            menorgarantia_tabla();
          }
        },
        complete: function (data) {
          
        },
      });
    }
  });
}
function revisar_producto(garantia_id, usuario_nom) {
  Swal.fire({
    title: "¿HAS REVISADO EL PRODUCTO Y ESTÁ LISTO PARA VENTA?",
    icon: "info",
    input: "text", // Tipo de input: texto
    inputPlaceholder: "Ingresa detalles", // Placeholder del input
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-home"></i> Si Revisado',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Salir',
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#dd4b39",
    inputValidator: (value) => {
      if (!value) {
        return "Debe ingresar un detalle al menos"; // Mensaje si el input está vacío
      }
    },
  }).then((result) => {
    if (result.isConfirmed) {
      const comentario = result.value; // Capturamos el texto ingresado
      $.ajax({
        type: "POST",
        url: VISTA_URL + "menorgarantia/menorgarantia_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: 'revisar_producto',
          garantia_id: garantia_id,
          usuario_nom: usuario_nom,
          comentario: comentario // Enviamos el comentario al backend
        },
        beforeSend: function () {
          // Opcional: lógica antes de la solicitud
        },
        success: function (data) {
          if (parseInt(data.estado) == 1) {
            notificacion_success(data.mensaje, 5000);
            menorgarantia_tabla();
          }
        },
        complete: function (data) {
          console.log(data);
        },
      });
    }
  });  
}
function subasta_form(garantia_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "subasta/subasta_form.php",
    async: true,
    dataType: "html",
    data: {
      garantia_id: garantia_id,
      action: "I"
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_subasta").html(data);
        $("#modal_subasta").modal("show");
        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        //modal_width_auto('modal_garantia_resumen',50);
        modal_height_auto("modal_subasta"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_subasta", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alerta_error("Error", data.responseText);
      console.log(data);
    },
  });
}
$(document).ready(function () {
  console.log('Mejoras al 03-10-2024 - Antonio')
  menorgarantia_tabla();

  $("#cmb_gar_est").change(function (event) {
    menorgarantia_tabla();
  });
  $("#che_gar_oro").change(function (event) {
    if ($("#che_gar_oro").is(":checked")) $("#che_gar_oro").val(1);
    else $(this).val("");
    menorgarantia_tabla();
  });
});

/* daniel odar 02-03-23 */
function gasto_form(action, garantia_id, gasto_id, credito_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gastogar/gastogar_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      garantia_id: garantia_id,
      gasto_id: gasto_id,
      credito_tipo: 1, //credito menor
      credito_id: credito_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_gastogar_form").html(html);
      $("#div_modal_gastogar_form").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_hidden_bs_modal("div_modal_gastogar_form", "limpiar"); //funcion encontrada en public/js/generales.js
      modal_width_auto("div_modal_gastogar_form", 40);
      modal_height_auto("div_modal_gastogar_form"); //funcion encontrada en public/js/generales.js
    },
    complete: function (html) {},
  });
}

/* daniel odar 09-03-23 */
function tabla_gastos_form(garantia_id, garantia_nom) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gastogar/gastogar_tabla.php",
    async: true,
    dataType: "html",
    data: {
      garantia_id: garantia_id,
      garantia_nom: garantia_nom,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_gastogar_tabla").html(html);
      $("#div_modal_gastogar_tabla").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_hidden_bs_modal("div_modal_gastogar_tabla", "limpiar"); //funcion encontrada en public/js/generales.js
      modal_width_auto("div_modal_gastogar_tabla", 70);
      modal_height_auto("div_modal_gastogar_tabla"); //funcion encontrada en public/js/generales.js
    },
    complete: function (html) {},
  });
}
/* */

/* daniel odar 20-03-23*/
function consultar_tabla_gastos() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gastogar/gastogar_tabla_form_vista.php",
    async: true,
    dataType: "json",
    data: {
      garantia_id: $("#hdd_garantia_id").val(),
    },
    beforeSend: function () {},
    success: function (data) {
      if (data.estado == 1) {
        $("#tbody_gastogar").html(data.tr);
      } else {
        $("#tbl_gastogars").html(data.tr);
      }
    },
  });
}
/* */

/* GERSON (07-03-23) */
function creditomenor_remate(credito_id, cuota_id) {
  Swal.fire({
    title: "¿DESEA RETORNAR EL CRÉDITO A COBRANZA?",
    icon: "warning",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-check"></i> SI',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> NO',
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#DD3333",
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "cobranza/cobranza_controller.php",
        async: false,
        dataType: "json",
        data: {
          action: "retornar_remate",
          cuota_id: cuota_id,
          credito_id: credito_id,
        },
        beforeSend: function () {},
        success: function (data) {
          //$('#div_inventarioproducto_tabla').html(html);
        },
        complete: function (data) {
          if (data.responseJSON.estado == 1) {
            swal_success("¡Hecho!", data.responseJSON.mensaje, 1500);
            menorgarantia_tabla();
          } else {
            swal_error("¡Error!", data.responseJSON.mensaje, 3000);
          }
        },
      });
    }
  });
}
/*  */

//Gerson 20-03-23
function upload_cobranza_form(usuario_act, upload_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      upload_id: upload_id,
      modulo_nom: "cobranza", //nombre de la tabla a relacionar
      modulo_id: upload_id, //aun no se guarda este modulo
      //modulo_id: 0, //aun no se guarda este modulo
      upload_uniq: $("#upload_uniq").val(), //ID temporal
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_upload_form").html(data);
        $("#modal_registro_upload").modal("show");

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_upload"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_upload", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
      upload_cobranza_galeria(upload_id);
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      swal_error("ERRROR", data.responseText, 5000);
    },
  });
}

function upload_cobranza_galeria(cobranza_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_galeria.php",
    async: true,
    dataType: "html",
    data: {
      modulo_nom: "cobranza", //nombre de la tabla a relacionar
      modulo_id: cobranza_id,
    },
    beforeSend: function () {},
    success: function (data) {
      $(".galeria_cobranza").html(data);
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      swal_error("ERRROR", data.responseText, 5000);
    },
  });
}
//
