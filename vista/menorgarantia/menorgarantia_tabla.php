<?php
  if(defined('VISTA_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once (VISTA_URL.'creditomenor/Creditomenor.class.php');
    $oCredito = new Creditomenor();
    require_once (VISTA_URL.'garantia/Garantia.class.php');
    $oGarantia = new Garantia();
    require_once (VISTA_URL.'administrador/CuotasNuevasMenor.class.php');
    $oCuotasNuevas = new CuotasNuevasMenor();
    require_once(VISTA_URL.'creditolinea/Creditolinea.class.php');
    require_once(VISTA_URL.'usuario/Usuario.class.php');
    $oUsuario = new Usuario();
  }
  else{
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
    require_once ('../creditomenor/Creditomenor.class.php');
    $oCredito = new Creditomenor();
    require_once ("../garantia/Garantia.class.php");
    $oGarantia = new Garantia();
    require_once ("../administrador/CuotasNuevasMenor.class.php");
    $oCuotasNuevas = new CuotasNuevasMenor();
    require_once('../creditolinea/Creditolinea.class.php');
    require_once('../usuario/Usuario.class.php');
    $oUsuario = new Usuario();
  }
  
  $oCreditolinea = new Creditolinea();
  require_once ('Menorgarantia.class.php');
  $oMenorgarantia = new Menorgarantia();
  require_once ("../cobranza/Cobranza.class.php");
  $oCobranza = new Cobranza();

  $cre_est = intval($_POST['cre_est']);
  $gar_oro = $_POST['gar_oro'];
  
  //credito estado 3, creditos vigentes que pasaron 15 días de vencidos sus cuotas
  /* $result = $oMenorgarantia->mostrarTodos(3);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $oCredito->modificar_campo($value['tb_credito_id'],'tb_credito_est',5,'INT');
        $oCredito->modificar_campo($value['tb_credito_id'], 'tb_credito_fecrem', date('Y-m-d'),'STR'); //fecha en que pasa a remate

        $creditolinea_det = '<span style="color: red;">Este crédito ha pasado a remate ya que pasó los 15 días de tolerancia a su fecha vencida. | <b>'.date('d-m-Y h:i a').'</b></span><br>';

        $oCreditolinea->insertar(1, $value['tb_credito_id'], 2, $creditolinea_det); //tipo credito 1, usuario 2 Soporte

        $oCredito->registro_historial($value['tb_credito_id'], $his);
      }
    }
  $result = NULL; */

  $dts = $oMenorgarantia->mostrarTodos($cre_est);

  /* GERSON (05-10-23) */
  $perfil_usuario = 0;
  $grupo_usuario = 0;
  $usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
  if($usuario['data']!=null || $usuario['data']!=''){
    $grupo_usuario = intval($usuario['data']['tb_usuariogrupo_id']);
    $perfil_usuario = intval($usuario['data']['tb_usuarioperfil_id']);
  }  
  //echo $grupo_usuario;
  /*  */

  if($_POST['formenor_rec'] > 0 && $_POST['formenor_pro'] >= 0){
    $formenor_rec = $_POST['formenor_rec'];
    $formenor_pro = $_POST['formenor_pro'];
  }
  else{
    echo '<h2>NO SE HA SELECCIONADO UNA FÓRMULA PARA LA VENTA O EXISTE MÁS DE UNO EN ESTADO VIGENTE. CONSULTE CON ADMISTRADOR DE SISTEMA</h2>';
    exit();
  }

  $monto_prestado = 0;
  $monto_recuperar = 0;
?>
<table id="tbl_menorgarantias" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">CRÉDITO ID</th>
      <th id="tabla_cabecera_fila" class="no-sort">CLIENTE</th>
      <th id="tabla_cabecera_fila" class="no-sort">APROBADO GC</th>
      <th id="tabla_cabecera_fila" style="width:900px;" class="no-sort">GARANTIAS</th>
      <th id="tabla_cabecera_fila" class="no-sort">VENCIDA EL</th>
      <th id="tabla_cabecera_fila" width="9%" class="no-sort">&nbsp;</th>
    </tr>
  </thead>
  <?php if($dts['estado'] == 1): ?>    
    <tbody>
      <?php
      foreach($dts['data'] as $key => $dt){
        $dts3 = $oGarantia->filtrar_oro($dt['tb_credito_id'], $gar_oro);
        $porcentaje = $dt['tb_credito_porcentaje'];

        if($dts3['estado']==1): ?>
          <tr id="tabla_cabecera_fila">
            <td id="tabla_fila"><?php echo 'CM-'.$dt['tb_credito_id']?></td>
            <td id="tabla_fila"  align="right">
              <?php echo '<span id="span_cli'.$dt['tb_cuota_id'].'">'.$dt['tb_cliente_nom'].'</span>'?></span>
              <a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="cobranza_form('<?php echo $dt['tb_cuota_id']?>','1')">+</a>
            </td>
            <td id="tabla_fila"  align="right">
              <?php 
                $credito_gcapr = intval($dt['tb_credito_gcapr']);
                $aprobacion_gc = '<span class="badge bg-green">Aprobado por GC</span>';
                if($credito_gcapr == 0){
                  $aprobacion_gc = '<span class="badge bg-red">Falta Aprobación GC</span>';
                  if($_SESSION['usuariogrupo_id'] == 6 || $_SESSION['usuariogrupo_id'] == 2)
                    $aprobacion_gc = '<button type="button" class="btn btn-danger btn-xs" onclick="aprobar_venta('.$dt['tb_credito_id'].', '.$_SESSION['usuario_id'].')">Aprobar la Venta</button>';
                }

                echo $aprobacion_gc;
              ?>
            </td>
            <td id="tabla_fila"><?php
              $dts2 = $oGarantia->filtrar_oro($dt['tb_credito_id'], $gar_oro);
              echo '<table class=" table-hover" width="100%">';
              if($dts2['estado']==1){
                foreach ($dts2['data']as $key=>$dt2) {
                  if($porcentaje==1||$porcentaje==2){
                    $formenor_rec_2=$formenor_rec;
                    $por="30 al 35 %";
                      
                  }
                  if($porcentaje==3||$porcentaje==4){
                    $formenor_rec_2=$formenor_rec-0.25;
                    $por="40 al 45 %";
                  }
                  if($porcentaje==5){
                    $formenor_rec_2=$formenor_rec-0.5;
                    $por="50 %";
                  }
                  
                  $pre_ven = ($dt2['tb_garantia_val']*$formenor_rec_2) + ($dt2['tb_garantia_val']*$formenor_pro*($dt['tb_credito_int']/100));
                  $ver_preven = mostrar_moneda($pre_ven);
                  if(intval($dt2['tb_garantia_prefij']) > 0){ //precio fijado por GERENCIA
                      $pre_ven = $dt2['tb_garantia_prefij'];
                      $ver_preven = '<span class="badge bg-green">'.mostrar_moneda($pre_ven).'</span>';
                  }
                  //$pre_ven = ($dt2['tb_garantia_val']*2.2) + ($dt2['tb_garantia_val']*2*($dt['tb_credito_int']/100));

                  $det = '';
                  if($dt2['tb_garantiatipo_id'] == 2)
                    $det = ': '.$dt2['tb_garantia_det'];

                  $garantia_rev = intval($dt2['tb_garantia_rev']); // 0 no está revisado, 1 producto revisado y listo para venta
                  $aprobacion_revision = '<span class="badge bg-blue">Producto Revisado</span>';

                  if($garantia_rev == 0){
                    $aprobacion_revision = '<span class="badge bg-yellow">Falta Revisar</span>';
                    if($_SESSION['usuariogrupo_id'] == 6 || $_SESSION['usuariogrupo_id'] == 2)
                      $aprobacion_revision = '<button type="button" class="btn btn-warning btn-xs" onclick="revisar_producto('.$dt2['tb_garantia_id'].', \''.$_SESSION['usuario_nom'].'\')">Revisar Producto?</button>';
                  }

                  echo '<tr  id="tabla_cabecera_fila"  style="height: 30px;">
                  <td id="tabla_fila" style="width:50px;">'.$aprobacion_revision.'</td>
                  <td id="tabla_fila" style="width:50px;"><b>'.$dt2['tb_garantiatipo_nom'].' | </b></td>
                  <td id="tabla_fila" style="width:400px;">• '.$dt2['tb_garantia_pro'].$det.'</td>
                  <td id="tabla_fila" style="width:90px;">'.$dt2['tb_garantia_val'].'</td>
                  <td id="tabla_fila" style="width:110px;">'.$por.' al '.$formenor_rec_2.'</td>
                  <td id="tabla_fila" style="width:100px;">'.$ver_preven.'</td>';

                  if($dt['tb_credito_est']==5 AND $dt2['tb_garantia_est']==0){
                    $monto_prestado += $dt2['tb_garantia_val'];
                    $monto_recuperar += $pre_ven;
                    
                    echo '<td  id="tabla_fila" align="center" style="width:300px;">';
                    if($dt2['tb_garantia_subasta'] == 1){
                      echo ' <a class="btn btn-xs bg-black" href="javascript:void(0)" onClick="subasta_form('.$dt2['tb_garantia_id'].')" title="Subasta"><i class="fa fa-legal"></i> Subasta</a>';
                    }
                    if($credito_gcapr == 1){
                      echo '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="garantia_resumen('.$dt2['tb_garantia_id'].')">Info</a>';
                    }

                    if(!is_numeric(strpos($dt2['tb_garantia_pro'],'DE CAMBIO')) && !in_array($dt2['tb_garantiatipo_id'], array('5','6'))){
                      echo ' <div class="btn-group"> 
                                <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown" title="Opciones de Gastos" aria-expanded="false">Gastos <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a class="btn" href="javascript:void(0)" '."onclick='gasto_form(\"I\",". $dt2['tb_garantia_id'] .", 0, {$dt2['tb_credito_id']})'".'><b>Registrar gasto</b></a></li>
                                    <li><a class="btn" href="javascript:void(0)" onclick="tabla_gastos_form('.$dt2['tb_garantia_id'].', \''. $dt2['tb_garantia_pro'] .'\')"><b>Ver gastos</b></a></li>
                                </ul>
                            </div>';
                    }
                    if(intval($dt2['tb_garantia_prefij']) > 0 && $credito_gcapr == 1 && $garantia_rev == 1){ //? SOLO SE PODRÁ VENDER SI ES QUE TIENE UN PRECIO FIJADO POR GERENCIA Y ESTÁ REVISADO POR JEFE DE SEDE
                      echo ' <a class="btn btn-xs bg-purple" href="javascript:void(0)" onClick="garantia_venta('.$dt2['tb_garantia_id'].','.$dt['tb_credito_int'].')" title="Vender Garantía"><i class="fa fa-money"></i> Vender</a>
                          </td>';
                    }
                  }
                  elseif($dt2['tb_garantia_est']==1){
                    echo '<td id="tabla_fila" align="right" style="width:200px;">
                            <b><a href="javascript:void(0)" style="color: red;">Vendido el '. mostrar_fecha($dt2['tb_garantia_fecven']).'</a></b>';
                    if($dt2['tb_garantia_subasta'] == 1){
                      echo ' <a class="btn btn-xs bg-black" href="javascript:void(0)" onClick="subasta_form('.$dt2['tb_garantia_id'].')" title="Subasta"><i class="fa fa-legal"></i> Subasta</a>';
                    }
                    echo '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="garantia_resumen('.$dt2['tb_garantia_id'].')">Info</a>';
                    if($dt2['tb_garantia_fecven'] == date('Y-m-d'))
                      echo '<a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="anular_garantia_venta('.$dt2['tb_garantia_id'].')">Anular</a>';
                      echo '
                      </td>';
                  }
                  echo '</tr>';
                }
              }
              echo '</table>';
              $dts2=NULL;;
              ?>
            </td>
            <td id="tabla_fila">
              <?php
                $dias_vencido = restaFechas($dt['tb_cuota_fec'], date('Y-m-d'));
                if($dt['tb_credito_fecrem']!='0000-00-00'){
                  $dias_en_remate = (strtotime(date('Y-m-d')) - strtotime($dt['tb_credito_fecrem'])) / 86400;
                }else{
                  $dias_en_remate = ($dias_vencido - 15);
                }
                if(intval($dias_vencido) <= 45)
                  echo '<span class="badge bg-dark">'.mostrar_fecha($dt['tb_cuota_fec']).' | '.$dias_en_remate.' días</span>';
                if(intval($dias_vencido) > 45 && intval($dias_vencido) <= 60)
                  echo '<span class="badge bg-yellow">'.mostrar_fecha($dt['tb_cuota_fec']).' | '.$dias_en_remate.' días</span>';
                if(intval($dias_vencido) > 60)
                  echo '<span class="badge bg-red">'.mostrar_fecha($dt['tb_cuota_fec']).' | '.$dias_en_remate.' días</span>';
              ?>
            </td>
            <td  id="tabla_fila">
              <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="creditomenor_pagos(<?php echo $dt['tb_credito_id']?>)">Pagos</a>
              <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="creditomenor_form('L','<?php echo $dt['tb_credito_id']?>')">Ver CM</a>
              <!-- GERSON (20-03-23) -->  
              <?php
                $cobranza = $oCobranza->obtener_cobranza($dt['tb_cuota_id'], $dt['tb_credito_id']); // cobranza seleccionada
                if($cobranza['estado']==1){
                  $cobranza_id =$cobranza['data']['tb_cobranza_id'];
              ?>
                <a class="btn btn-xs btn-primary" href="javascript:void(0)" onClick="upload_cobranza_form('I','<?php echo $cobranza_id; ?>')"><i class="fa fa-picture-o"></i> Gestión</a>
              <?php } ?>
              <!--  -->
              <!-- GERSON (05-10-23) -->  
              <?php if($perfil_usuario==1 || $perfil_usuario==4 || $perfil_usuario==5){ // 1 = ADMINISTRADOR, 4 = GESTOR DE COBRANZA, 5 = CONTABILIDAD ?>
                <a class="btn btn-xs bg-purple" href="javascript:void(0)" onClick="creditomenor_remate('<?php echo $dt['tb_credito_id']?>','<?php echo $dt['tb_cuota_id']; ?>')">Quitar Remate</a>
              <?php } ?>
              <!--  -->
            </td>
          </tr>
          <?php 
        endif;
      }
     ?>
      
    </tbody>
    <tfoot>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila"></td>
        <td id="tabla_fila"></td>
        <td id="tabla_fila"></td>
        <td id="tabla_fila" style="font-size: 20pt;"><?php echo '<b>Monto prestado: S/. '.mostrar_moneda($monto_prestado).'</b> | <b>Monto Venta:</b> <span class="badge bg-green" style="font-size: 15pt;">S/. '.mostrar_moneda($monto_recuperar).'</span>'?></td>
        <td id="tabla_fila"></td>
        <td id="tabla_fila"></td>
      </tr>
    </tfoot>
    <?php 
  endif;?>
    
</table>