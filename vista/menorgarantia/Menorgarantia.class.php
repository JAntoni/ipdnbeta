<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Menorgarantia extends Conexion{
       
    function mostrarTodos($cre_est){
      try {
            /* $sql="SELECT * FROM tb_creditomenor cm
                    INNER JOIN tb_cliente cl ON cm.tb_cliente_id = cl.tb_cliente_id
                    INNER JOIN tb_cuota c ON cm.tb_credito_id = c.tb_credito_id
                    WHERE c.tb_creditotipo_id = 1 AND DATE_ADD(c.tb_cuota_fec, INTERVAL 15 DAY) < DATE(NOW()) AND c.tb_cuota_est <> 2 AND c.tb_cuota_xac = 1 AND cm.tb_credito_xac = 1 AND cm.tb_credito_apr <> '0000-00-00 00:00:00'"; */
            $sql="SELECT * FROM tb_creditomenor cm
                    INNER JOIN tb_cliente cl ON cm.tb_cliente_id = cl.tb_cliente_id
                    INNER JOIN tb_cuota c ON cm.tb_credito_id = c.tb_credito_id
                    WHERE c.tb_creditotipo_id = 1 AND c.tb_cuota_est <> 2 AND c.tb_cuota_xac = 1 AND cm.tb_credito_xac = 1";
            if (intval($cre_est) > 0) {
                $sql .= " and cm.tb_credito_est =:tb_credito_est";
            }

            $sql .=" GROUP BY cm.tb_credito_id";

        $sentencia = $this->dblink->prepare($sql);
        if(intval($cre_est) > 0)
          $sentencia->bindParam(":tb_credito_est", $cre_est, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    function mostrarUno($id){
      try {
        $sql="SELECT * FROM tb_menorgarantia WHERE tb_menorgarantia_id=:tb_menorgarantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_menorgarantia_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "no se encontro ningún resultado";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
}

?>