<label>FILTRO DE GARANTÍAS EN REMATE</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <div class="row">
            <input type="hidden" class="form-control input-sm" id="hdd_formenor_rec" name="hdd_formenor_rec" class="form-control input-sm" value="<?php echo $formenor_rec;?>">
            <input type="hidden" class="form-control input-sm" id="hdd_formenor_pro" name="hdd_formenor_pro" class="form-control input-sm" value="<?php echo $formenor_pro;?>">
            <div class="col-md-3">
                <label>ESTADO : </label>
                <select name="cmb_gar_est" id="cmb_gar_est" class="form-control input-sm mayus">
                    <option value="">TODOS...</option>
                    <option value="5" selected>DISPONIBLE</option>
                    <option value="6">VENDIDO</option>
                </select>
            </div>
            <div class="col-md-2">
                <p>
                <br>
                <label for="che_gar_oro">MOSTRAR ORO : </label>&nbsp;
                <input type="checkbox" class="flat-green" name="che_gar_oro" id="che_gar_oro" value="" style="width: 15px; height: 15px;">
            </div>
            <div class="col-md-7">
                <p><label>Leyenda de Colores</label></p>
                <span class="pull-right-container">
                    <span class="badge bg-green" style="padding: 5px 7px !Important;">Precio Fijo</span>
                    <span class="badge bg-dark" style="padding: 5px 7px !Important;">30 días para vender</span>
                    <span class="badge bg-yellow" style="padding: 5px 7px !Important;">15 días venta urgente</span>
                    <span class="badge bg-red" style="padding: 5px 7px !Important;">10 días asignar a colaborador</span>
                </span>
            </div>
        </div>
    </div>
</div>    