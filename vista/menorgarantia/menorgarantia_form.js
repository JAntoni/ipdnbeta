$(document).ready(function(){

  $('.pago1').hide();
  $('.pago2').hide();
  $('.pago3').hide();
  $('.pago4').hide();
  $('.pago5').hide();
  $('.pago6').hide();
  
  /* GERSON (03-05-23) */
  var permiso = $('#permiso_edit_fecha').val();
  if(permiso==0){
    //$('#cmb_usu_id').prop('disabled', 'disabled');

    $("#txt_gar_pag1").attr("readonly", true);
    $(".pago1").css("pointer-events", "none");
    $("#txt_gar_pag2").attr("readonly", true);
    $(".pago2").css("pointer-events", "none");
    $("#txt_gar_pag3").attr("readonly", true);
    $(".pago3").css("pointer-events", "none");
    $("#txt_gar_pag4").attr("readonly", true);
    $(".pago4").css("pointer-events", "none");
    $("#txt_gar_pag5").attr("readonly", true);
    $(".pago5").css("pointer-events", "none");
    $("#txt_gar_pag6").attr("readonly", true);
    $(".pago6").css("pointer-events", "none");
  }
  /*  */

  $('#menorgarantia_picker1,#menorgarantia_picker2,#menorgarantia_picker3,#menorgarantia_picker4,#menorgarantia_picker5,#menorgarantia_picker6').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		//endDate : new Date()
  });
    
  $('.moneda2').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '99999.00'
  });
    
  $('.moneda3').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '99999.99'
  });
  $('#txt_gar_val').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '1.00',
    vMax: '99999.99'
  });

  $( "#txt_cliente_fil_cre_nom,#txt_cliente_fil_cre_doc" ).autocomplete({
    minLength: 3,
    source: function(request, response){
    	$.getJSON(
    		VISTA_URL+"cliente/cliente_autocomplete.php",
    		{term: request.term, cliente_emp: 1},
    		response
    	);
    },
    select: function(event, ui){
    	$('#hdd_cli_id').val(ui.item.cliente_id);
    	$('#txt_cliente_fil_cre_nom').val(ui.item.cliente_nom);
    	$('#txt_cliente_fil_cre_doc').val(ui.item.cliente_doc);
    }
  });
  
  $('#cmb_gar_cuo').change(function(event) {
    var cuo = $(this).val();
    if(cuo == 0){
      $('.pago1').hide();
      $('.pago2').hide();
      $('.pago3').hide();
      $('.pago4').hide();
      $('.pago5').hide();
      $('.pago6').hide();
    }
    if(cuo == 1){
      $('.pago1').show();
      $('.pago2').hide();
      $('.pago3').hide();
      $('.pago4').hide();
      $('.pago5').hide();
      $('.pago6').hide();
    }
    if(cuo == 2){
      $('.pago1').show();
      $('.pago2').show();
      $('.pago3').hide();
      $('.pago4').hide();
      $('.pago5').hide();
      $('.pago6').hide();
    }
    if(cuo == 3){
      $('.pago1').show();
      $('.pago2').show();
      $('.pago3').show();
      $('.pago4').hide();
      $('.pago5').hide();
      $('.pago6').hide();
    }
    if(cuo == 4){
      $('.pago1').show();
      $('.pago2').show();
      $('.pago3').show();
      $('.pago4').show();
      $('.pago5').hide();
      $('.pago6').hide();
    }
    if(cuo == 5){
      $('.pago1').show();
      $('.pago2').show();
      $('.pago3').show();
      $('.pago4').show();
      $('.pago5').show();
      $('.pago6').hide();
    }
    if(cuo == 6){
      $('.pago1').show();
      $('.pago2').show();
      $('.pago3').show();
      $('.pago4').show();
      $('.pago5').show();
      $('.pago6').show();
    }
  });
  
  $('#txt_gar_mon, #txt_gar_comi').change(function(event) {
    var monto_deposito = Number($('#txt_gar_mon').autoNumeric('get'));
    var monto_comision = Number($('#txt_gar_comi').autoNumeric('get'));
    var saldo = 0;

    saldo = monto_deposito - monto_comision;

    if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
      $('#txt_gar_montot').autoNumeric('set',saldo.toFixed(2));
      $('#txt_gar_val').autoNumeric('set',saldo.toFixed(2));
    }
    else{
      $('#txt_gar_montot').autoNumeric('set',0);
      $('#txt_gar_val').autoNumeric('set',0);
    }
  });
  
  $('#txt_gar_pro').keyup(function(){
    $(this).val($(this).val().toUpperCase());
  });
 
  $('#cmb_gar_lugpag').change(function (event) {
    var lugar = parseInt($(this).val());
    if (lugar == 1) {
      $('#precio_ofi').show(200);
      $('#precio_banco').hide(200);
      $('#precio_banco2').hide(200);
      $('#precio_banco3').hide(200);
      if($("#cmb_usu_id").attr('disabled')){
        $("#cmb_usu_id, #cmb_gar_cuo").removeAttr('disabled');
      }
    }
    else { //con deposito solo para clientes externos
      $('#precio_ofi').hide(200);
      $('#precio_banco').show(200);
      $('#precio_banco2').show(200);
      $('#precio_banco3').show(200);
      if (parseInt($('#cmb_usu_id').val()) > 0) {
        $('#cmb_usu_id, #cmb_gar_cuo').val(0).change();
      }
      disabled($('#cmb_usu_id, #cmb_gar_cuo'));
    }
  });
 
  $('#cmb_cuedep_id').change(function(event) {
    var cuenta = $(this).find("option:selected").text();
    $('#hdd_cuenta_dep').val(cuenta);
  });  
 
  $("#for_gar").validate({
    submitHandler: function() {    
      var lugar_pago = $('#cmb_gar_lugpag').val();
      var cli = $("#hdd_cli_id").val();
      var cola = $('#cmb_usu_id').val();
      var valor_minimo = Number($('#hdd_gar_prec1').val());
      var valor_venta = Number($('#txt_gar_val').autoNumeric('get'));

      if(cli=="" && cola=='0'){
        swal_warning("AVISO",'Ingrese un cliente o un colaborador para vender',4000);
        return false;
      }
      // if(valor_venta < valor_minimo){
      //   swal_warning("AVISO",'No puedes vender la garantía a un precio menor que S/. ' + valor_minimo + ', ingresado S/. ' + valor_venta,4000);
      //   return false;
      // }

      if(lugar_pago == 2){
        var valor_deposito = $('#txt_gar_mon').autoNumeric('get');
        if(valor_deposito != valor_venta){
          swal_warning("AVISO",'El monto de venta y el monto de depósito deben ser iguales.',4000);
          return false;
        }
      }

      $.confirm({
        title: "¿Está seguro que desea guardar los datos?",
        content: "",
        buttons: {
          formSubmit: {
            text: "Aceptar",
            btnClass: "btn-blue",
            action: function () {

              $.ajax({
                type: "POST",
                url:  VISTA_URL+"menorgarantia/menorgarantia_controller.php",
                async:true,
                dataType: "json",
                data: $("#for_gar").serialize(),
                beforeSend: function() {
                },
                success: function(data){						
                  if(parseInt(data.ing_id ) > 0){
                    $('#modal_garantiaventa_registro').modal('hide');
                    menorgarantia_imppos_datos(data.ing_id);
                    swal_success("SISTEMA",data.gar_msj,4000);
                    menorgarantia_tabla();
                  }
                  else if (parseInt(data.colabo_id ) > 0) {
                    $('#modal_garantiaventa_registro').modal('hide');
                    swal_success("SISTEMA",data.gar_msj,2000);
                    menorgarantia_tabla();
                  }
                  else{
                    swal_warning("AVISO",data.gar_msj,5000);
                  }
                },
                complete: function(data){
                  console.log(data);
                }
              });

            },
          },
          Cancelar: function () {
            //close
          },
        },
        onContentReady: function () {
          // bind to events
          var jc = this;
          this.$content.find("form").on("submit", function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger("click"); // reference the button and click it
          });
        },
      });
		},
		rules: {
			txt_gar_pro: {
				required: true
			},
			txt_gar_val: {
				required: true
			},
      cmb_cuedep_id: {
        required: function(){
          var lugar = parseInt($('#cmb_gar_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_gar_mon: {
        required: function(){
          var lugar = parseInt($('#cmb_gar_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_gar_comi: {
        required: function(){
          var lugar = parseInt($('#cmb_gar_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_gar_numope: {
        required: function(){
          var lugar = parseInt($('#cmb_gar_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      }
		},
    messages: {
      txt_gar_pro: {
              required: '* requiere un garantia'
      },
      txt_gar_val: {
              required: '*'
      },
      cmb_cuedep_id: {
        required: 'Numero de cuenta'
      },
      txt_gar_mon: {
        required: 'Monto depositado'
      },
      txt_gar_comi: {
        required: 'Comisión cobrada'
      },
      txt_gar_numope: {
        required: 'Número de operación'
      }
		}
	});

  // ------------------------------------------------------- //

  $('#txt_fecha_deposito').change(function (event) {
    event.preventDefault();
  
    var fecha_deposito = $('#txt_fecha_deposito').val();
    var num_operacion  = $.trim($('#txt_gar_numope').val());
  
    if(num_operacion == ""){
      swal_warning('AVISO', "DEBE REGISTRAR NUMERO DE OPERACION", 6000);
      $('#txt_fecha_deposito').val('')
      return false;
    }
  
    $.ajax({
      type: "POST",
      url: VISTA_URL + "vencimiento/vencimiento_funciones.php",
      async: true,
      dataType: "JSON",
      data: {
        funcion: "validar_operacion_fecha",
        fecha_deposito: fecha_deposito,
        num_operacion: num_operacion,
        fecha_vencimiento: null,
        numero_cuota: 0
      },
      success: function (data) {
  
        if(data.estado === 0){
  
          swal_warning('AVISO', data.mensaje, 6000);
          $('#txt_fecha_deposito').val('');
          $('#txt_gar_mon').val('');
          $('#txt_gar_comi').val('');
          $('#txt_gar_montot').val('');
          $('#txt_gar_val').val('');
          // $('#cmb_banco_id').val('');
          $('#cmb_cuedep_id').val('');
          $('#txt_gar_numope, #txt_fecha_deposito, #cmb_cuedep_id, #txt_gar_mon').closest('.form-group').removeClass('has-success').addClass('has-error');  // #cmb_banco_id
          $('#txt_gar_comi, #txt_gar_montot').closest('.form-group').removeClass('has-success').addClass('has-warning');
        }
        else {
          notificacion_success('N° operación y fecha correctos', 6000);
          $('#cmb_cuedep_id').val(data.cuentaid).change();
          $('#txt_gar_numope, #cmb_cuedep_id, #txt_fecha_deposito, #txt_gar_mon').closest('.form-group').removeClass('has-error').addClass('has-success'); //  #cmb_banco_id
          $('#txt_gar_comi, #txt_gar_montot').closest('.form-group').removeClass('has-error').addClass('has-warning');
          
          validar_saldo_utilizar();
        }
  
        $("#txt_gar_mon").focus();
  
      },
      complete: function (data) {
        console.log(data)
      },
      error: function (data) {
        alerta_error("Error:", "" + data.responseText);
      },
    });
  
  });

});

function validar_saldo_utilizar(){

  var fecha_deposito = $('#txt_fecha_deposito').val();
  var num_operacion  = $.trim($('#txt_gar_numope').val());

  $.ajax({
    type: "POST",
    url: VISTA_URL +"stockunidad/stockunidad_validacion_salgo_utilizar.php",
    async:true,
    dataType: "JSON",
    data: ({
      action: "consultar",
      numoperacion: num_operacion,
      fecha_deposito: fecha_deposito,
      moneda_id_padre: 1
    }),
    beforeSend: function() {
      console.log('validando...')
    },
    success: function(data){
      console.log(data);      
      alerta_warning("Información",data.mensaje);
      $("#txt_gar_mon").val(data.monto_usar);
    },
    complete: function(data){
      // console.log(data);
    },
    error: function (data) {
      alerta_error("Error:", "" + data.responseText);
    },
  });
}