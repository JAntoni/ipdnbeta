<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Ventainterna extends Conexion{
    
  public $tb_ventainterna_id;
  public $tb_ventainterna_xac;
  public $tb_ventainterna_fecpag;
  public $tb_garantia_id;
  public $tb_usuario_id;
  public $tb_ventainterna_mon;
  public $tb_ventainterna_monpag;
  public $tb_ventainterna_est;
  public $tb_ventainterna_tip;
  public $tb_ingreso_id;
  public $tb_ventainterna_his;
  public $tb_ventainterna_nrocuo;
                
  function insertar(){
    $this->dblink->beginTransaction();
    try {
          $sql ="INSERT INTO tb_ventainterna(
              tb_ventainterna_fecpag, 
              tb_garantia_id, 
              tb_usuario_id, 
              tb_ventainterna_mon, 
              tb_ventainterna_his,
              tb_ventainterna_nrocuo) 
            VALUES (
              :tb_ventainterna_fecpag, 
              :tb_garantia_id, 
              :tb_usuario_id, 
              :tb_ventainterna_mon, 
              :tb_ventainterna_his,
              :tb_ventainterna_nrocuo)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_ventainterna_fecpag", $this->tb_ventainterna_fecpag, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_garantia_id", $this->tb_garantia_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_ventainterna_mon", $this->tb_ventainterna_mon, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_ventainterna_his", $this->tb_ventainterna_his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_ventainterna_nrocuo", $this->tb_ventainterna_nrocuo, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $tb_ventagarantia_id = $this->dblink->lastInsertId();
      
      $this->dblink->commit();
      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['tb_ventagarantia_id'] = $tb_ventagarantia_id;
      return $data;

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
    
  function anular_ventainterna($gar_id, $valor, $his){
      $this->dblink->beginTransaction();
      try {
       $sql ="UPDATE tb_ventainterna set 
              tb_ventainterna_xac =:tb_ventainterna_xac,
              tb_ventainterna_his = CONCAT(tb_ventainterna_his,:tb_ventainterna_his)
              where tb_garantia_id =:tb_garantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_ventainterna_xac", $valor, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_ventainterna_his", $his, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_garantia_id", $gar_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar_ventainterna($usu_id, $mes, $anio){
      try {
            $sql ="SELECT * FROM tb_ventainterna ve 
              inner join tb_garantia ga on ga.tb_garantia_id = ve.tb_garantia_id 
              inner join tb_usuario usu on usu.tb_usuario_id = ve.tb_usuario_id 
              where tb_ventainterna_xac =1";
                if(!empty($usu_id))
                    $sql .=" and ve.tb_usuario_id =".$usu_id;
                if(!empty($mes))
                    $sql.=" and left(tb_ventainterna_fecpag, 7) ='".$anio."-".$mes."'";
                if(empty($mes))
            $sql .=" and YEAR(tb_ventainterna_fecpag) =".$anio;
            $sql .=' order by tb_ventainterna_fecpag';

        $sentencia = $this->dblink->prepare($sql);
        //$sentencia->bindParam(":fec1", $usu_id, PDO::PARAM_STR);
        //$sentencia->bindParam(":fec2", $mes, PDO::PARAM_STR);
        //$sentencia->bindParam(":emp_id", $anio, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  
  // funcion emergencia para pago de planilals mayo 2023
  function lista_pagos_garantia($usu_id, $mes, $anio){
    try {
          $sql ="SELECT ing.tb_ingreso_id, ing.tb_ingreso_imp, ing.tb_subcuenta_id, ve.tb_ventainterna_id, ve.tb_ventainterna_mon, ve.tb_ventainterna_est, ve.tb_ventainterna_tipo, ve.tb_ventainterna_monpag, ga.*, usu.* FROM tb_ventainterna ve 
            inner join tb_garantia ga on ga.tb_garantia_id = ve.tb_garantia_id 
            inner join tb_ingreso ing on (ing.tb_ingreso_modide = ve.tb_garantia_id  AND ing.tb_modulo_id=90)
            inner join tb_usuario usu on usu.tb_usuario_id = ve.tb_usuario_id 
            where ve.tb_ventainterna_xac =1 and ing.tb_ingreso_xac=1";
              if(!empty($usu_id))
                  $sql .=" and ve.tb_usuario_id =".$usu_id;
              if(!empty($mes))
                  $sql.=" and left(tb_ventainterna_fecpag, 7) ='".$anio."-".$mes."'";
              if(empty($mes))
          //$sql .=" and YEAR(tb_ventainterna_fecpag) =".$anio;
          $sql .=' order by tb_ventainterna_fecpag';

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  // funcion que lista los pagos parciales de una cuota de venta interna
  function lista_pagos_ventainterna($ventainterna_id){
    try {
          $sql ="SELECT * FROM tb_ingreso
            WHERE tb_ingreso_xac = 1";
              if(!empty($ventainterna_id))
                  $sql .=" and tb_ventainterna_id =".$ventainterna_id;
          $sql .=' ORDER BY tb_ingreso_fecreg';

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ingresos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  // Saber si la garantia en venta interna su última cuota está antes de la actualización
  function lista_garantia_fuera_actua($usu_id, $fecha_corte){
    try {
          $sql ="SELECT * 
                  FROM tb_ventainterna 
                  WHERE tb_ventainterna_xac = 1 AND tb_ventainterna_fecpag > '".$fecha_corte."'";
              if(!empty($usu_id))
                  $sql .=" AND tb_usuario_id =".$usu_id;
          $sql .=' order by tb_ventainterna_fecpag';

      $sentencia = $this->dblink->prepare($sql);
      //$sentencia->bindParam(":fec1", $usu_id, PDO::PARAM_STR);
      //$sentencia->bindParam(":fec2", $mes, PDO::PARAM_STR);
      //$sentencia->bindParam(":emp_id", $anio, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  //

  function mostrarUno($venin_id){
      try {
        $sql ="SELECT ve.*, ga.*, usu.*, emp.tb_empresa_nomcom FROM tb_ventainterna ve 
            inner join tb_garantia ga on ga.tb_garantia_id = ve.tb_garantia_id 
            inner join tb_usuario usu on usu.tb_usuario_id = ve.tb_usuario_id
            left join tb_ingreso ing on ve.tb_ingreso_id = ing.tb_ingreso_id
            left join tb_empresa emp on ing.tb_empresa_id = emp.tb_empresa_id
            where ve.tb_ventainterna_id =:tb_ventainterna_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_ventainterna_id", $venin_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
  }

  function mostrar_venta_interna_garantia($garantia_id){
      try {
        $sql ="SELECT * FROM tb_ventainterna WHERE tb_garantia_id =:garantia_id AND tb_ventainterna_xac = 1 LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay venta interna para esta garantía";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
  }
  
  function estado_ventainterna($venin_id, $est, $ing_id, $his){
      $this->dblink->beginTransaction();
      try {
        $sql ="UPDATE tb_ventainterna set 
          tb_ventainterna_est =:tb_ventainterna_est, 
          tb_ingreso_id =:tb_ingreso_id, 
          tb_ventainterna_his = CONCAT(tb_ventainterna_his,:tb_ventainterna_his) 
          where tb_ventainterna_id =:tb_ventainterna_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_ventainterna_est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_ingreso_id", $ing_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_ventainterna_his", $his, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_ventainterna_id", $venin_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  
  function sumar_montopagado($venin_id, $mon_pag){
      
      $this->dblink->beginTransaction();
      try {
        $sql ="UPDATE tb_ventainterna set tb_ventainterna_monpag =tb_ventainterna_monpag + :tb_ventainterna_monpag  where tb_ventainterna_id =:tb_ventainterna_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_ventainterna_monpag", $mon_pag, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_ventainterna_id", $venin_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar_campo($ventainterna_id, $ventainterna_columna, $ventainterna_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($ventainterna_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_ventainterna SET " . $ventainterna_columna . " =:ventainterna_valor WHERE tb_ventainterna_id =:ventainterna_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":ventainterna_id", $ventainterna_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":ventainterna_valor", $ventainterna_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":ventainterna_valor", $ventainterna_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            }
            else
              return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

}

?>