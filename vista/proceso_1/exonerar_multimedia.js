$(document).ready(function() {
  var proceso_fase_id = $("#hdd_proceso_fase_id").val();
  $("#frm_exonerar_proceso_fase_item").validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_controller.php",
        async:true,
        dataType: "json",
        data: $("#frm_exonerar_proceso_fase_item").serialize(),
        beforeSend: function() {
          $('#btn_guardar_exonerar').prop('disabled', true);
        },
        success: function(data){
          //console.log(data);
          if(parseInt(data.estado) > 0){
              notificacion_success(data.mensaje, 3000);
              $("#modal_proceso_fase_item_exonerar").modal('hide');

              /* var i = 0;
              while(i<data.cant_item) {
                verProcesoFaseContenido(data.proceso_fase_ids[i]);
                i++;
              } */
              verProcesoFaseContenido(data.proceso_fase_id);
          }
          else
          console.log(data);
        },
        complete: function(data){
          $('#btn_guardar_exonerar').prop('disabled', false);
        }
      });
    },
    rules: {
        txt_motivo_exonerar: {
                required: true
        }
    },
    messages: {
        txt_motivo_exonerar: {
                required: 'Ingresar motivo'
        }
    }
	  });
});

function verProcesoFaseContenido(proceso_fase_id) {
	var id = 0;
	if(proceso_fase_id==null || proceso_fase_id=='' || proceso_fase_id==undefined){
	  id = $('#hdd_proceso_fase_id_'+proceso_fase_id).val();
	}else{
	  id = proceso_fase_id;
	}
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "proceso/proceso_vista_fase_contenido.php",
	  async: true,
	  dataType: "html",
	  data: ({
		usuario_id: $('#hdd_usuario_id').val(),
		proceso_id: $('#hdd_proceso_id').val(),
		proceso_fase_id: id,
	  }),
	  beforeSend: function () {
	  },
	  success: function (html) {
		$('#div_contenido'+id).html(html);
	  },
	  complete: function (html) {
	  }
	});
}

