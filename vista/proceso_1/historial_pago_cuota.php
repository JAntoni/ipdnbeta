<?php
session_name("ipdnsac");
session_start();

require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$cliente_id = intval($_POST['cliente_id']);

$proc = false;
$cli = false;
$c_menor = false;
$c_garveh = false;

$cliente_nom = '';
if($cliente_id>0){
    $cli = true;

    $cliente = $oCliente->mostrarUno($cliente_id);
    $cliente_nom = $cliente['data']['tb_cliente_nom'];

    $cred_menor = $oProceso->mostrarMenorXCliente($cliente_id); // lista de credito menor
    $cred_menor_cuotas = $oProceso->mostrarMenorCuotasXCliente($cliente_id); // lista de cuotas de los creditos encontrado
    if($cred_menor['estado'] == 1){
        $c_menor = true;
    }

    $cred_garveh = $oProceso->mostrarGarvehXCliente($cliente_id); // lista de credito menor
    $cred_garveh_cuotas = $oProceso->mostrarGarvehCuotasXCliente($cliente_id); // lista de cuotas de los creditos encontrado
    if($cred_garveh['estado'] == 1){
        $c_garveh = true;
    }

    $procesos = $oProceso->mostrarProcesosXCliente($cliente_id);
    if($procesos['estado'] == 1){
        $proc = true;
    }
}
?>

<style>
    
</style>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_pago_cuotas_cliente" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">HISTORIAL DE CLIENTE: <span style="color: blue;"><strong><?php echo $cliente_nom;?></strong></span></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                    <div class="box box-success class_menor">
                            <div class="box-header with-border">
                                <h3 class="box-title"><strong>CALIFICACIONES</strong></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">

                                <?php if($cli){ ?>
                                    <?php if($proc){ ?>
                                        <?php foreach ($procesos['data'] as $key => $p){ ?>
                                            <button type="button" class="btn btn-primary" onclick="exportarHojaPDFHistorial(<?php echo $p['tb_proceso_id'];?>, 'con_firma');"><?php echo mostrar_fecha($p['tb_proceso_fecreg']); ?></button>
                                        <?php } ?>
                                    <?php }else{ ?>
                                        <h5>No se encontraron calificaciones.</h5>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <h5>Cliente no encontrado.</h5>
                                <?php } ?>
                                
                            </div>
                        </div>

                        <div class="box box-success class_menor">
                            <div class="box-header with-border">
                                <h3 class="box-title"><strong>CREDITO MENOR</strong></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">

                                <?php if($cli){ ?>
                                    <?php if($c_menor){ ?>
                                        <div class="table-responsive" style="overflow-y: auto; height: 170px;">
                                            <table id="tbl_menor" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>FECHA</th>
                                                        <th>CRÉDITO</th>
                                                        <th>ESTADO CRÉDITO</th>
                                                        <th>CUOTA</th>
                                                        <th>VENCIMIENTO</th>
                                                        <th>FECHA PAGO</th>
                                                        <th>DIAS DE RETRASO</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="lista_credito_menor">
                                                    <?php foreach ($cred_menor['data'] as $key => $v){ ?>
                                                        <?php if($cred_menor_cuotas['estado'] == 1){ ?>
                                                            <?php foreach ($cred_menor_cuotas['data'] as $key => $value) { ?>
                                                                <?php if($v['tb_credito_id'] == $value['tb_credito_id']){ ?>

                                                                    <tr>
                                                                        <td><?php echo mostrar_fecha($value['tb_credito_reg']); ?></td>
                                                                        <td><?php echo 'CM-'.$value['tb_credito_id']; ?></td>
                                                                        <td><?php echo $value['estado']; ?></td>
                                                                        <td><?php echo $value['tb_cuota_num']; ?></td>
                                                                        <td><?php echo mostrar_fecha($value['tb_cuota_fec']); ?></td>
                                                                        <td><?php echo mostrar_fecha($value['fecha_pago']); ?></td>
                                                                        <td><?php echo $value['dias_diff']; ?></td>
                                                                    </tr>

                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }else{ ?>
                                        <h5>No se encontraron cuotas.</h5>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <h5>Cliente no encontrado.</h5>
                                <?php } ?>
                                
                            </div>
                        </div>

                        <div class="box box-success class_garveh">
                            <div class="box-header with-border">
                                <h3 class="box-title"><strong>CREDITO GARVEH</strong></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">

                                <?php if($cli){ ?>
                                    <?php if($c_garveh){ ?>
                                        <div class="table-responsive" style="overflow-y: auto; height: 170px;">
                                            <table id="tbl_garveh" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>FECHA</th>
                                                        <th>CRÉDITO</th>
                                                        <th>ESTADO CRÉDITO</th>
                                                        <th>CUOTA</th>
                                                        <th>VENCIMIENTO</th>
                                                        <th>FECHA PAGO</th>
                                                        <th>DIAS DE RETRASO</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="lista_credito_garveh">
                                                    <?php foreach ($cred_garveh['data'] as $key => $v){ ?>
                                                        <?php if($cred_garveh_cuotas['estado'] == 1){ ?>
                                                            <?php foreach ($cred_garveh_cuotas['data'] as $key => $value) { ?>
                                                                <?php if($v['tb_credito_id'] == $value['tb_credito_id']){ ?>

                                                                    <tr>
                                                                        <td><?php echo mostrar_fecha($value['tb_credito_reg']); ?></td>
                                                                        <td><?php echo 'CG-'.$value['tb_credito_id']; ?></td>
                                                                        <td><?php echo $value['estado']; ?></td>
                                                                        <td><?php echo $value['tb_cuota_num']; ?></td>
                                                                        <td><?php echo mostrar_fecha($value['tb_cuotadetalle_fec']); ?></td>
                                                                        <td><?php echo mostrar_fecha($value['fecha_pago']); ?></td>
                                                                        <td><?php echo $value['dias_diff']; ?></td>
                                                                    </tr>

                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }else{ ?>
                                        <h5>No se encontraron cuotas.</h5>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <h5>Cliente no encontrado.</h5>
                                <?php } ?>

                            </div>
                        </div>

                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/proceso/hoja_precalificacion_form.js?ver=03072024';?>"></script>