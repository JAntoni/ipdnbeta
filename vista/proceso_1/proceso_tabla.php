<?php
require_once '../../core/usuario_sesion.php';
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');
require_once('../templates/paginado.php');
require_once ('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once ('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

if($_POST['cantidad']==NULL || $_POST['cantidad']==''){
	$pagina=1;
	$inicio=0;
	$cantidad=50;
}else{
	$pagina=$_POST['pagina'];
	$cantidad=$_POST['cantidad'];
	$inicio=($pagina-1)*$cantidad;
}

$fecini = fecha_mysql($_POST['fecini']);
$fecfin = fecha_mysql($_POST['fecfin']);
$cre_id = intval($_POST['cre_id']);
$use_id = intval($_POST['use_id']);
$cli_id = intval($_POST['cli_id']);
$estado = intval($_POST['estado']);
$fecha_hoy = date('d-m-Y');
$usuario_id = $_SESSION['usuario_id'];

$total_pag = 0;
$dts = $oProceso->mostrar_proceso_paginado($fecini, $fecfin, $cre_id, $use_id, $cli_id, $estado, $inicio, $cantidad);
$dts_num = $oProceso->mostrar_proceso_paginado($fecini, $fecfin, $cre_id, $use_id, $cli_id, $estado, $inicio, $cantidad, true);


if ($dts['estado'] == 1) { 
	$num=0;
	$total_pag=ceil($dts_num['data']/$cantidad); ?>

	<table id="tabla_lista_procesos" class="table table-hover table-responsive dataTables_wrapper form-inline dt-bootstrap">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">#</th>
                <th id="tabla_cabecera_fila">N° PROCESO</th>
                <th id="tabla_cabecera_fila">N° CREDITO</th>
                <th id="tabla_cabecera_fila">FECHA REGISTRO</th>
                <th id="tabla_cabecera_fila">CLIENTE</th>
                <th id="tabla_cabecera_fila">ASESOR</th>
                <th id="tabla_cabecera_fila">CRÉDITO</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th  id="tabla_cabecera_fila" width="12%">ACCIONES</th>
            </tr>
        </thead>  
		
		<tbody>
		<?php 
			foreach ($dts['data'] as $key => $dt) {
				// gerson (23-03-24)
				$client = '';
				$precali = $oProceso->mostrarUnoPrecalificacion($dt['tb_proceso_id']);
				if($precali['estado'] == 1){
					if($precali['data']['tb_precalificacion_razon_social'] != '' || $precali['data']['tb_precalificacion_razon_social'] != null){
						$client = $precali['data']['tb_precalificacion_razon_social'];
					}else{
						$client = $precali['data']['tb_precalificacion_cliente'];
					}
				}else{
					if($dt['tb_cliente_nom'] != null || $dt['tb_cliente_nom'] != ''){
						$client = $dt['tb_cliente_nom'];
					}elseif($dt['tb_precliente_nombres'] != null || $dt['tb_precliente_nombres'] != ''){
						$client = $dt['tb_precliente_nombres'];
					}
				}
				//

				$estado = '';
				$estado_bg = '';
				$estado_color = '';
				/* if(intval($dt['tb_proceso_est']) == 1){
					$estado = "ACTIVO";
					$estado_bg = '#3061F7';
					$estado_color = '#FFFFFF';
				}elseif(intval($dt['tb_proceso_est']) == 2){
					$estado = "FINALIZADO";
					$estado_bg = '#00BA49';
					$estado_color = '#FFFFFF';
				}elseif(intval($dt['tb_proceso_est']) == 3){
					$estado = "ANULADO";
					$estado_bg = '#E5B416';
					$estado_color = '#000000';
				}elseif(intval($dt['tb_proceso_est']) == 4){
					$estado = "ELIMINADO";
					$estado_bg = '#F73030';
					$estado_color = '#FFFFFF';
				} */
				/* GERSON (17-05-24) */
				if(intval($dt['tb_credito_id']) > 0){
					$cred = $oCreditogarveh->mostrarUno(intval($dt['tb_credito_id']));
					if(intval($cred['estado']) == 1){
						if(intval($cred['data']['tb_credito_est']) == 1){
							$estado = "PENDIENTE";
							$estado_bg = '#D5D5D5';
							$estado_color = '#000000';
						}elseif(intval($cred['data']['tb_credito_est']) == 2){
							$estado = "APROBADO";
							$estado_bg = '#7D9BF9';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 3){
							$estado = "VIGENTE";
							$estado_bg = '#3061F7';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 4){
							$estado = "PARALIZADO";
							$estado_bg = '#F73030';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 5){
							$estado = "REFINANCIADO C/AMORT";
							$estado_bg = '#E5B416';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 6){
							$estado = "REFINANCIADO S/AMORT";
							$estado_bg = '#E5B416';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 7){
							$estado = "LIQUIDADO";
							$estado_bg = '#00BA49';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 8){
							$estado = "RESUELTO";
							$estado_bg = '#00BA49';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 9){
							$estado = "TITULO";
							$estado_bg = '#00BA49';
							$estado_color = '#FFFFFF';
						}elseif(intval($cred['data']['tb_credito_est']) == 10){
							$estado = "DOCUMENTOS";
							$estado_bg = '#00BA49';
							$estado_color = '#FFFFFF';
						}
						if(intval($dt['tb_proceso_est']) == 4){
							$estado = "ELIMINADO";
							$estado_bg = '#F73030';
							$estado_color = '#FFFFFF';
						}
					}
				}else{
					if(intval($dt['tb_proceso_est']) == 4){
						$estado = "ELIMINADO";
						$estado_bg = '#F73030';
						$estado_color = '#FFFFFF';
					}else{
						$estado = "PENDIENTE";
						$estado_bg = '#D5D5D5';
						$estado_color = '#000000';
					}
				}
				/*  */
		?>
				<tr id="tabla_cabecera_fila">
					<td id="tabla_fila"><?php echo $num+1; ?></td>
					<td id="tabla_fila"><?php echo intval($dt['tb_proceso_id']) ?></td>
					<?php if($dt['tb_credito_id']!=null){ ?>
						<td id="tabla_fila"><?php echo str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td>
					<?php }else{ ?>
						<td id="tabla_fila"></td>
					<?php } ?>
					<td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_proceso_fecreg']) ?></td>
					<td id="tabla_fila"><?php echo $client ?></td>
					<td id="tabla_fila"><?php echo $dt['asesor'] ?></td>
					<td id="tabla_fila"><?php echo $dt['tb_creditotipo_nom'].' | '.$dt['cgarvtipo_nombre'] ?></td>
					<td id="tabla_fila" style="background-color: <?php echo $estado_bg ?>; color: <?php echo $estado_color ?>;"><strong><?php echo $estado ?></strong></td>
					<td id="tabla_fila">
						<?php if(intval($dt['tb_proceso_est']) == 1){ // Proceso Activo ?>
							<a class="btn btn-info btn-xs" title="Ver" onclick="btnProcesoFase(<?php echo "'L', ".$dt['tb_proceso_id'].", ".$usuario_id;?>)"><i class="fa fa-eye"></i></a>
							<!-- <a class="btn btn-success btn-xs" title="Finalizar Proceso" onclick="btnFinalizarProceso(<?php echo $dt['tb_proceso_id'] ?>)"><i class="fa fa-check"></i></a> -->
							<a class="btn btn-warning btn-xs" <?php if($estado=='VIGENTE'){ echo 'style="pointer-events: none" disabled';}?> title="Anular Proceso" onclick="btnAnularProceso(<?php echo $dt['tb_proceso_id'] ?>, 'R')"><i class="fa fa-ban"></i></a>
							<?php if(intval($usuario_id) == 11 || intval($usuario_id) == 2){ //Solo podrán eliminar Engel y Juan ?>
								<a class="btn btn-danger btn-xs" <?php if($estado=='VIGENTE'){ echo 'style="pointer-events: none" disabled';}?> title="Eliminar Proceso" onclick="btnEliminarProceso(<?php echo $dt['tb_proceso_id'] ?>)"><i class="fa fa-trash"></i></a>
							<?php } ?>
						<?php } ?>
						<?php if(intval($dt['tb_proceso_est']) == 2){ // Proceso Finalizado ?>
							<a class="btn btn-info btn-xs" title="Ver" onclick="btnProcesoFase(<?php echo "'L', ".$dt['tb_proceso_id'].", ".$usuario_id;?>)"><i class="fa fa-eye"></i></a>
							<?php if(intval($usuario_id) == 11 || intval($usuario_id) == 2){ //Solo podrán eliminar Engel y Juan ?>
								<a class="btn btn-danger btn-xs" <?php if($estado=='VIGENTE'){ echo 'style="pointer-events: none" disabled';}?> title="Eliminar Proceso" onclick="btnEliminarProceso(<?php echo $dt['tb_proceso_id'] ?>)"><i class="fa fa-trash"></i></a>
							<?php } ?>
						<?php } ?>
						<?php if(intval($dt['tb_proceso_est']) == 3){ // Proceso Anulado ?>
							<a class="btn bg-purple btn-xs" title="Motivo de Anulación" onclick="btnAnularProceso(<?php echo $dt['tb_proceso_id'] ?>, 'L')"><i class="fa fa-commenting"></i></a>
						<?php } ?>
						<!-- <a class="btn btn-info btn-xs" title="Ver" onclick="btnProcesoFase(<?php echo "'L', ".$dt['tb_proceso_id'].", ".$usuario_id;?>)"><i class="fa fa-eye"> Ver</i></a> -->
					</td>
				</tr>
		<?php 
				$num++;
			}
		?>
		</tbody>
	</table>
	<!-- <div class="box-footer clearfix">
        <?php 
        $fin=$inicio+$cantidad;
        echo getFooterCase($pagina,$total_pag,$dts_num['data'],$cantidad,($inicio+1),$fin,'proceso');
        ?>
    </div> -->
<?php 
	}else
		echo '<center><h2>SIN RESULTADOS</h2></center>'; 
?>