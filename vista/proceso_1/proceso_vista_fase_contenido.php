<?php 
session_name("ipdnsac");
session_start();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if(isset($proceso_id)){
    $proceso_id = intval($proceso_id); // Tenemos el proceso_id
}else{
    $proceso_id = intval($_POST['proceso_id']); // al subir docs en los items, retornamos el proceso_id
}

if(isset($value['tb_proceso_fase_id'])){
    $proceso_fase_id = intval($value['tb_proceso_fase_id']); // Tenemos el proceso_fase_id
}else{
    $proceso_fase_id = intval($_POST['proceso_fase_id']); // al subir docs en los items, retornamos el proceso_id
}

if(isset($value['tb_proceso_fase_orden'])){
    $proceso_fase_orden = $value['tb_proceso_fase_orden'];
}else{
    $proceso_fase = $oProceso->mostrarUnoProcesoFase($proceso_fase_id);
    $proceso_fase_orden = $proceso_fase['data']['tb_proceso_fase_orden'];
}

$comite = $oProceso->listar_comite();
$comite_otros = $oProceso->listar_comite_otros();
$config11 = $oProceso->obtenerCofig(11);
$puntos_comite = 0.00;
if($config11['estado']==1){
    $puntos_comite = floatval($config11['data']['tb_config_valor']);
}

$credito_id = 0;
$cliente_id = 0;
$cliente_nom = '';
$cliente_cel = '';
$proc_fec_inspeccion = '';
$proc_fec_deposito = '';
$proc_inicial = '';
$proc_inicial_est = '';

$fec_inspeccion = '';
$prueba_veh = '';
$cli_acepta = '';
$cli_acepta_det = '';

$voto_comite = '';
$boton_precali = 'disabled';
$color_boton_precali = 'btn-default';
$suma_boton_precali = 0.00;
$voto_comite_si = 0;
$voto_comite_no = 0;
$voto_comite_sin_voto = 0;

if(!isset($proceso)){
    $proceso = $oProceso->mostrarUno($proceso_id);
}

if(isset($proceso['data']['tb_cliente_id'])){
    $cliente_id = $proceso['data']['tb_cliente_id'];
    
    $cliente = $oCliente->mostrarUno($cliente_id);
    
    $cliente_nom = $cliente['data']['tb_cliente_nom'];
    $cliente_cel = $cliente['data']['tb_cliente_cel'];
    $cliente_doc = $cliente['data']['tb_cliente_doc'];
    
}elseif(isset($proceso['data']['tb_precliente_id'])){
    $precliente = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);

    $cliente_id = ($precliente['data']['tb_cliente_id']) == null ? 0 : intval($precliente['data']['tb_cliente_id']);

    $cliente_nom = ($precliente['data']['tb_precliente_nombres'] == null || $precliente['data']['tb_precliente_nombres'] == '') ? '' : $precliente['data']['tb_precliente_nombres'];
    $cliente_cel = ($precliente['data']['tb_precliente_numcel'] == null || $precliente['data']['tb_precliente_numcel'] == '') ? '' : $precliente['data']['tb_precliente_numcel'];
    $cliente_numdoc = ($precliente['data']['tb_precliente_numdoc'] == null || $precliente['data']['tb_precliente_numdoc'] == '') ? '' : $precliente['data']['tb_precliente_numdoc'];
}

if(isset($proceso['data']['tb_credito_id'])){
    $credito_id = intval($proceso['data']['tb_credito_id']);
}

$proc_fec_inspeccion = $proceso['data']['tb_proceso_fec_inspeccion'];
$proc_fec_deposito = $proceso['data']['tb_proceso_fec_inicial'];
$proc_inicial = $proceso['data']['tb_proceso_inicial'];
$proc_inicial_est = $proceso['data']['tb_proceso_inicial_est'];
$proc_banca = intval($proceso['data']['tb_proceso_banca']);

$fec_inspeccion = $proceso['data']['tb_proceso_fec_inspeccion'];
$prueba_veh = $proceso['data']['tb_proceso_prueba_veh'];
$cli_acepta = $proceso['data']['tb_proceso_cli_acepta'];
$cli_acepta_det = $proceso['data']['tb_proceso_cli_acepta_det'];

// gerson (31-01-24)
$bloqueo_exo = 'disabled';
if($comite['estado'] == 1){
    foreach ($comite['data'] as $key => $value) {
        if(intval($_SESSION['usuario_id']) == intval($value['tb_usuario_id'])){
            $bloqueo_exo = '';
        }
    }
}
//
?>

<?php if($proceso['data']['tb_creditotipo_id'] == 1){ // CREDITO MENOR ?>

    <div class="row">
        <div class="col-md-12">
            CREDITO MENOR
        </div>
    </div>

<?php }elseif($proceso['data']['tb_creditotipo_id'] == 2){ // CREDITO ASCVEH ?>

    <div class="row">
        <div class="col-md-12">
            CREDITO ASCVEH
        </div>
    </div>

<?php }elseif($proceso['data']['tb_creditotipo_id'] == 3){ // CREDITO GARVEH ?>

    <?php if($proceso['data']['tb_cgarvtipo_id'] == 1){ // CREDITO GARVEH - Pre Constitución ?>

        <div class="row">
            <div class="col-md-12">

            <?php if($proceso_fase_orden == 1){ // Orden de Fase ?>

                <div class="col-md-6">

                    <div class="col-md-12" style="margin-bottom: 5px;">
                        <div class="form-group">
                            <label for="cliente_nom" class="col-sm-4 control-label">CLIENTE</label>
                            <div class="col-sm-6 input-group">
                                <input type="hidden" id="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                <input type="hidden" id="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                <input type="hidden" id="hdd_proc_fase_id_temp" name="hdd_proc_fase_id_temp" value="<?php echo $proceso_fase_id ?>">
                                <input type="hidden" id="cliente_dni_<?php echo $proceso_fase_id; ?>" name="cliente_dni_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_numdoc ?>">
                                <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id ?>">

                                <input type="text" class="form-control input-sm" name="cliente_nom_<?php echo $proceso_fase_id; ?>" id="cliente_nom_<?php echo $proceso_fase_id; ?>" placeholder="Ingrese Cliente" value="<?php echo $cliente_nom ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                                        <span class="fa fa-pencil icon"></span>
                                    </button>
                                </span>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                                        <span class="fa fa-plus icon"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-md-12" style="margin-bottom: 5px;">
                        <div class="form-group">
                            <label for="cliente_celular" class="col-sm-4 control-label">CELULAR</label>
                            <div class="col-sm-8"> 
                                <input type="text" class="form-control input-sm" name="cliente_cel_<?php echo $proceso_fase_id; ?>" id="cliente_cel_<?php echo $proceso_fase_id; ?>" onkeypress="return solo_numero(event)" placeholder="Ingrese N° Celular" value="<?php echo $cliente_cel ?>">
                            </div>
                        </div>
                    </div>

                    <!-- <div align="center" class="col-md-12" style="margin-bottom: 5px; padding-top: 10px;">
                        <button 
                            class="btn btn-primary btn-sm " 
                            type="button" 
                            name="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                            id="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                            title="Guardar Datos" 
                            onClick="guardar_preconsti_fase1(<?php echo $proceso_fase_id; ?>)">
                            <span class="fa fa-save"></span> Guardar Cambios
                        </button>
                    </div> -->

                    <div class="col-md-12 hidden">
                        <section id="participantes">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class='col-md-3 h5'><label>Añadir Participantes</label></div>
                                    <div class='col-md-9' id="rowPlusParticipante">
                                    <div class="input-group" id="buttonPlusParticipante">
                                        <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoParticipante').removeClass('hidden'); $('#rowPlusParticipante').addClass('hidden'); limpiarCamposParticipante();" data-toggle="tooltip" title="Añadir Ingreso"><li class="fa fa-plus"></li></button>
                                    </div>
                                    <br>
                                    </div>
                                </div>

                                <div class="col-md-12 hidden" id="divNuevoParticipante">
                                    <form name="frmNuevoParticipante" id="frmNuevoParticipante">
                                    <input type="hidden" name="action" value="registrar_participante">
                                    <input type="hidden" name="hdd_participante_proceso_id" id="hdd_participante_proceso_id" value="<?php echo $proceso_id; ?>">

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                        
                                        <div class='col-md-12'>	
                                            <div class="input-group" >
                                            <span class="input-group-addon"><strong>Nombres y Apellidos</strong><span class="text-red">*</span></span>
                                            <input autocomplete="off" class="form-control input-sm" name="nombre_p" id="nombre_p" type="text" placeholder="Nombres y apellidos de participante"/>
                                            </div>
                                            <br>
                                        </div>

                                        <div class='col-md-6'>	
                                            <div class="input-group" >
                                            <span class="input-group-addon"><strong>DNI</strong><span class="text-red">*</span></span>
                                            <input autocomplete="off" class="form-control input-sm" name="documento_p" id="documento_p" type="text" onkeypress="return solo_numero(event)" placeholder="DNI de participante"/>
                                            </div>
                                            <br>
                                        </div>

                                        <div class='col-md-6'>	
                                            <div class="input-group" >
                                            <span class="input-group-addon"><strong>Celular</strong></span>
                                            <input autocomplete="off" class="form-control input-sm" name="celular_p" id="celular_p" type="text" onkeypress="return solo_numero(event)" placeholder="Celular de participante"/>
                                            </div>
                                            <br>
                                        </div>

                                        </div>

                                        <div class="col-md-12" align="right" >
                                        <button class="btn btn-sm btn-primary" type="button" id="btnParticipante" onClick="guardarParticipanteDetalle('frmNuevoParticipante', 'Participante','I')"><li class="fa fa-save"></li> Guardar</button>
                                        <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoParticipante').addClass('hidden'); $('#frmNuevoParticipante').trigger('reset'); $('#rowPlusParticipante').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                                        </div>
                                    </div>
                                    </form>
                                    <br>
                                </div>

                                <div class="col-md-12" id="listaParticipante">

                                </div>

                            </div>
                        </section>
                    </div>


                </div>

                <div id="div_items_contenido<?php echo $proceso_fase_id; ?>" class="col-md-6">
                    <?php 
                    $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                    $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                    $estados_items = [];

                    if($items['estado']==1){
                        foreach ($items['data'] as $key => $item) {
                            $action='nuevo';

                            $estado_item = 'fa-circle-o text-red';
                            /*  */
                            $btn_exo = '';
                            $btn_exo_color = 'btn-success';
                            $btn_exo_icon = 'fa-check';
                            $btn_exo_title = 'Exonerar';
                            /*  */
                            $btn_doc = '';

                            $multimedia_id = 0; // para cuando ya existe archivo subido

                            if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                // $siguiente_fase seguirá como true si el item ha sido exonerado
                                
                                $estado_item = 'fa-circle text-green';
                                /*  */
                                $btn_exo = 'disabled';
                                $btn_exo_color = 'btn-default';
                                $btn_exo_icon = 'fa-ban';
                                $btn_exo_title = 'Exonerado';
                                /*  */
                                $btn_doc = 'disabled';

                            }else{ // items habilitados

                                if(intval($item['tb_multimedia_est']) == 2){  // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                    $siguiente_fase = false;
                                }

                            }

                            if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                array_push($estados_items, 3);
                            }else{
                                array_push($estados_items, intval($item['tb_multimedia_est']));
                            }

                    ?>
                            <div id="item<?php echo $item['tb_proceso_fase_item_id']; ?>" class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php echo $bloqueo_exo; ?>
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>
                                        <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                            <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                        <?php } ?>
                                        <?php if($item['tb_item_idx'] == 'hoja_pre_calificacion'){ // solo mostrar botón en nivel uno para formato pre calificacion ?>
                                            <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="hojaPrecalificacion('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="comentarioPreCalificacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Comentarios"><span class="fa fa-commenting"></span></span></button>
                                        <?php } ?>
                                        <?php if($item['tb_item_idx'] == 'form_ing_egr'){ // solo mostrar botón en nivel uno para formato de ingresos y gastos ?>
                                            <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="ingresosEgresos('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                /* if($siguiente_fase){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                                    $boton_precali = '';
                                    $color_boton_precali = 'btn-success';
                                }else{
                                    $boton_precali = 'disabled';
                                    $color_boton_precali = 'btn-default';
                                } */
                                ?>
                            </div>
                    <?php 
                        }
                    }
                    ?>
                </div>

            <?php } elseif($proceso_fase_orden == 2){ // Orden de Fase ?>

                <div class="col-md-12">
                    <?php 
                    $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                    $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                    $estados_items = [];

                    if($items['estado']==1){
                        foreach ($items['data'] as $key => $item) {
                            $action='nuevo';

                            $estado_item = 'fa-circle-o text-red';
                            /*  */
                            $btn_exo = '';
                            $btn_exo_color = 'btn-success';
                            $btn_exo_icon = 'fa-check';
                            $btn_exo_title = 'Exonerar';
                            /*  */
                            $btn_doc = '';

                            $multimedia_id = 0; // para cuando ya existe archivo subido

                            if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                // $siguiente_fase seguirá como true si el item ha sido exonerado
                                
                                $estado_item = 'fa-circle text-green';
                                /*  */
                                $btn_exo = 'disabled';
                                $btn_exo_color = 'btn-default';
                                $btn_exo_icon = 'fa-ban';
                                $btn_exo_title = 'Exonerado';
                                /*  */
                                $btn_doc = 'disabled';

                            }else{ // items habilitados

                                if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                    $action='editar';

                                    // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                    $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                    if($items_fase['estado']==1){
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        /*  */
                                        $btn_doc = '';
                                        /*  */
                                        $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                    }
                                }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                    $siguiente_fase = false;
                                }

                            }

                            if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                array_push($estados_items, 3);
                            }else{
                                array_push($estados_items, intval($item['tb_multimedia_est']));
                            }
                    
                    ?>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                            <div class="form-group">
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php if($item['tb_item_idx']=='libro_negro' || $item['tb_item_idx']=='boleta_factura'){echo '';}else{echo $bloqueo_exo;} // Manejar las exoneraciones por parte del colaborador ?> 
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>
                                        <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                            <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                    <?php 
                        }
                    }
                    ?>
                </div>

                <!-- Boton de Bancarización -->
                <div align="left" class="col-md-12" style="padding-top: 15px;">
                    <div class="form-group">
                        <button type="button" class="btn bg-maroon" onclick="elegirBancarizacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-money"></i> Bancarización</strong></button>
                        <button type="button" class="btn bg-purple" onclick="generarCheche(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-indent"></i> Generar Cheques</strong></button>
                        <button type="button" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id;?>, 'con_firma');"><strong><i class="fa fa-download"></i> Hoja Pre Calicación</strong></button>
                    </div>
                </div>
                <!--  -->

                <!-- Votación de comité -->
                <?php //if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 56){ // Solo mostrar al comité LOCAL ?>
                <div class="col-md-12" style="padding-top: 15px;">
                <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>
                    <h4><strong>COMITÉ: </strong></h4>
                <?php } ?>

                    <table>
                        <tbody>
                            <?php if($comite['estado'] == 1){ ?>
                                <?php foreach($comite['data'] as $key => $com){ ?>
                                <?php 
                                        $voto_miembro = $oProceso->obtenerVotoComite($proceso_id, $com['tb_usuario_id']); 
                                        $voto_comite = 0;
                                        if($voto_miembro['estado']==1){
                                            $voto_comite = $voto_miembro['data']['tb_comitevoto_voto'];
                                        }
                                ?>
                                    <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>

                                    <tr>
                                        <td style="padding: 10px;"><strong>- <?php echo $com['usuario'] ?></strong></td>
                                        <td style="padding: 10px;">
                                            <input 
                                                type="radio" 
                                                name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_si" 
                                                value="2" 
                                                onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                class="flat-red" <?php if($voto_comite==2) echo 'checked' ?>
                                            >
                                                <strong> Procede</strong>
                                        </td>
                                        <td style="padding: 10px;">
                                            <input 
                                                type="radio" 
                                                name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                value="1" 
                                                onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                class="flat-red" <?php if($voto_comite==1) echo 'checked' ?>
                                            >
                                                <strong> No procede</strong>
                                        </td>
                                        <td style="padding: 10px;">
                                            <input 
                                                type="radio" 
                                                name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                value="0" 
                                                onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                class="flat-red" <?php if($voto_comite==0) echo 'checked' ?>
                                            >
                                                <strong> Sin voto</strong>
                                        </td>
                                    </tr>
                                    <?php } ?>

                                    <?php 
                                        // CONTEO DE VOTOS PARA FASE 2
                                        if($voto_miembro['data']['tb_comitevoto_voto']==2){ // Votos a favor
                                            $suma_boton_precali = $suma_boton_precali + floatval($com['tb_comite_cred']);
                                            $voto_comite_si++;
                                        }elseif($voto_miembro['data']['tb_comitevoto_voto']==1){ // Votos en contra
                                            $voto_comite_no++;
                                        }elseif($voto_miembro['data']['tb_comitevoto_voto']==0){ // Aún no vota
                                            $voto_comite_sin_voto++;
                                        }
                                    ?>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                                        
                    <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité 
                        
                        $proceso_otro = $oProceso->mostrarUno($proceso_id);
                        $firma_otro = 0;
                        if($proceso_otro['estado'] == 1){
                            $firma_otro = intval($proceso_otro['data']['tb_proceso_usu_firma_otro']);
                        }
    
                    ?>
                        
                        <br>
                        <?php if($comite_otros['estado'] == 1){ ?>
                            <h5><strong>OTROS: </strong></h5>
                            <table>
                                <tbody>

                                    <?php foreach($comite_otros['data'] as $key => $otro){ ?>
                                        <tr>
                                            <td>
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_otro" 
                                                    id="voto_comite_otro_<?php echo $otro['tb_usuario_id']; ?>" 
                                                    value="<?php echo $otro['tb_usuario_id']; ?>" 
                                                    onChange="votoComiteOtro(this.value, <?php echo $otro['tb_usuario_id']; ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($firma_otro==$otro['tb_usuario_id']) echo 'checked' ?>
                                                >
                                                    <strong> <?php echo $otro['usuario'] ?></strong>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        <?php } ?>

                    <?php } ?>

                </div>
                <?php 
                
                /* if($suma_boton_precali >= $puntos_comite){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                    $boton_precali = '';
                    $boton_banca = '';
                    $color_boton_precali = 'btn-success';
                    $color_boton_banca = 'btn-success';
                } */
                ?>
                <?php //} ?>
                <!--  -->

            <?php } elseif($proceso_fase_orden == 3){ // Orden de Fase ?>

                <div class="col-md-12">
                    <?php 
                    $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                    $estados_items = [];

                    if($items['estado']==1){
                        foreach ($items['data'] as $key => $item) {
                        if(intval($item['tb_item_id']) != 64 && intval($item['tb_item_id']) != 65){ // Se excluye CHEQUE GERENCIA o OTRO TIPO DE DESEMBOLSO se maneja de forma manual luego
                            $action='nuevo';

                            $estado_item = 'fa-circle-o text-red';
                            /*  */
                            $btn_exo = '';
                            $btn_exo_color = 'btn-success';
                            $btn_exo_icon = 'fa-check';
                            $btn_exo_title = 'Exonerar';
                            /*  */
                            $btn_doc = '';

                            $multimedia_id = 0; // para cuando ya existe archivo subido

                            if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                
                                $estado_item = 'fa-circle text-green';
                                /*  */
                                $btn_exo = 'disabled';
                                $btn_exo_color = 'btn-default';
                                $btn_exo_icon = 'fa-ban';
                                $btn_exo_title = 'Exonerado';
                                /*  */
                                $btn_doc = 'disabled';

                            }else{ // items habilitados

                                if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                    $action='editar';

                                    //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                    $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                    if($items_fase['estado']==1){
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        /*  */
                                        $btn_doc = '';
                                        /*  */
                                        $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                    }
                                }

                            }

                            if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                array_push($estados_items, 3);
                            }else{
                                array_push($estados_items, intval($item['tb_multimedia_est']));
                            }
                    
                    ?>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php echo $bloqueo_exo; ?>
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>
                                        <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                            <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                    <?php 
                        }
                        }
                    }
                    ?>
                </div>

                <div class="col-md-12 page-header" style="padding-top: 15px;"></div>

                <?php if($proc_banca == 1){ //CHEQUES ?>
                <!-- Desembolso Cheques -->
                <div class="col-md-12" style="padding-top: 15px;">
                    <div class="col-md-6" style="margin-bottom: 5px;">
                        <div class="form-group">
                            <?php                                
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                $btn_doc = '';
                                $multimedia_id = 0; // para cuando ya existe archivo subido
                                $proceso_fase_item_id = 0;

                                $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 64);
                                $proceso_fase_item_64 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 64);

                                if($items_fase1['estado']==1){
                                    $action='editar';

                                    $estado_item = 'fa-circle text-green';
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    $btn_doc = '';
                                    $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                }

                                if($proceso_fase_item_64['estado']==1){
                                    $proceso_fase_item_id = intval($proceso_fase_item_64['data']['tb_proceso_fase_item_id']);
                                }

                            ?>
                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> CHEQUE DE GERENCIA</label>
                            <div class="col-sm-4" style="padding: 0px;">
                                <button 
                                    <?php echo $bloqueo_exo; ?>
                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                    type="button" 
                                    name="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                    id="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                    title="<?php echo $btn_exo_title; ?>" 
                                    onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                </button>
                                <?php if($action == 'editar'){ ?>
                                    <button 
                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                        type="button" 
                                        name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="Documento"
                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                        <span class="fa fa-file-pdf-o"></span>
                                    </button>
                                <?php }else{ ?>
                                    <button 
                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                        type="button" 
                                        name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="Documento"
                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                        <span class="fa fa-file-pdf-o"></span>
                                    </button>
                                <?php } ?>
                                
                                <button 
                                    class="btn btn-warning btn-sm" 
                                    type="button" 
                                    name="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                    id="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_id; ?>, 'tb_proc_proceso_fase_item')">
                                    <span class="fa fa-history"></span>
                                </button>
                                
                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" title="Subir Cheque" onclick="subidaCheques(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $proceso_fase_item_id;?>);"><span class="fa fa-upload"></span></button>
                                <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_id;?>, 'cheque')"><span class="fa fa-money"></span></button>
                            
                            </div>
                        </div>
                    </div>
                </div>
                <!--  -->
                <?php }else{ ?>
                    <?php if($proc_banca == 0){ ?>
                        <h5 class="" style="color: red;"> <strong>Seleccione una opción de bancarización en la Fase 2.</strong></h5>
                    <?php }else{ ?>
                        <!-- Desembolso otro medio de pago -->
                        <div class="col-md-12">
                            <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <?php
                                        $action='nuevo';

                                        $estado_item = 'fa-circle-o text-red';
                                        $btn_exo = '';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        $btn_doc = '';
                                        $multimedia_id = 0; // para cuando ya existe archivo subido

                                        $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 65);
                                        $proceso_fase_item_65 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 65);

                                        if($items_fase1['estado']==1){
                                            $action='editar';

                                            $estado_item = 'fa-circle text-green';
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            $btn_doc = '';
                                            $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                        }

                                    ?>
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> EFECTIVO, TRANSFERENCIA O DEPÓSITO</label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php echo $bloqueo_exo; ?>
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>
                                        
                                        <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>, 'otro')"><span class="fa fa-money"></span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                    <?php } ?>
                <?php } ?>

            <?php } else { ?>

                <div class="col-md-12">
                    <?php 
                    $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                    $estados_items = [];
                    $items_comprimidos = [];

                    $i = 0;
                    if($items['estado']==1){
                        foreach ($items['data'] as $key => $item) {
                            $action='nuevo';

                            $estado_item = 'fa-circle-o text-red';
                            /*  */
                            $btn_exo = '';
                            $btn_exo_color = 'btn-success';
                            $btn_exo_icon = 'fa-check';
                            $btn_exo_title = 'Exonerar';
                            /*  */
                            $btn_doc = '';

                            $multimedia_id = 0; // para cuando ya existe archivo subido
                            $multi_exo = 0;
                            $multi_url = '';

                            if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                
                                $estado_item = 'fa-circle text-green';
                                /*  */
                                $btn_exo = 'disabled';
                                $btn_exo_color = 'btn-default';
                                $btn_exo_icon = 'fa-ban';
                                $btn_exo_title = 'Exonerado';
                                /*  */
                                $btn_doc = 'disabled';
                                $multi_exo = 1;
                                $multi_url = '...';

                            }else{ // items habilitados

                                if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                    $action='editar';

                                    //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                    $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                    
                                    if($items_fase['estado']==1){
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        /*  */
                                        $btn_doc = '';
                                        /*  */
                                        $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                        $multi_url      = $items_fase['data']['tb_multimedia_url'];
                                    }
                                }

                            }

                            if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                array_push($estados_items, 3);
                            }else{
                                array_push($estados_items, intval($item['tb_multimedia_est']));
                            }
                    
                    ?>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php echo $bloqueo_exo; ?>
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>

                                        <!-- < ?php if($item['tb_item_idx'] == 'escritura_publica'){ // solo mostrar botón en nivel 4 para formulario de titulo ?> -->
                                            <!-- <button class="btn btn-info btn-sm" type="button" name="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="titulo_form(< ?php echo $credito_id;?>);"><span class="fa fa-pencil-square-o"></span></button> -->
                                        <!-- < ?php } ?> -->
                                    </div>
                                </div>
                            </div>
                    <?php 

                            //añadir valores dde item al array
                            $valor['item_id']=$item['tb_item_id'];
                            $valor['item_des']=$item['tb_item_des'];
                            $valor['multi_exo']=$multi_exo;
                            $valor['multi_url']=$multi_url;
                            $items_comprimidos[$i]=$valor;
                            $i++;
                        }
                    }
                    $array = json_encode($items_comprimidos);
                    ?>

                </div>

                <br>
                <input type="hidden" id="items_comprimidos" name="items_comprimidos[]" value='<?php echo json_encode($items_comprimidos); ?>'>
                <div align="center">
                    <button type="button" class="btn btn-primary" id="" name="" onclick="comprimirArchivos(<?php echo $item['tb_proceso_id']; ?>)" /><strong><i class="fa fa-file-zip-o"></i> COMPRIMIR</strong></button>
                    <a id="downloadZip" name="downloadZip" href="" target="blank_"></a> <!-- Enlace que disparra la descarga del zip -->
                </div>

            <?php } ?>
                
            </div>
        </div>
        

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 2){ // CREDITO GARVEH - Constitución Sin Custodia ?>

        <div class="row">
            <div class="col-md-12">

                <?php if($proceso_fase_orden == 1){ // Orden de Fase ?>

                    <div class="col-md-6">

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_nom" class="col-sm-4 control-label">CLIENTE</label>
                                <div class="col-sm-6 input-group">
                                    <input type="hidden" id="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                    <input type="hidden" id="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                    <input type="hidden" id="hdd_proc_fase_id_temp" name="hdd_proc_fase_id_temp" value="<?php echo $proceso_fase_id ?>">
                                    <input type="hidden" id="cliente_dni_<?php echo $proceso_fase_id; ?>" name="cliente_dni_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_numdoc ?>">
                                    <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id ?>">
                                
                                    <input type="text" class="form-control input-sm" name="cliente_nom_<?php echo $proceso_fase_id; ?>" id="cliente_nom_<?php echo $proceso_fase_id; ?>" placeholder="Ingrese Cliente" value="<?php echo $cliente_nom ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                                            <span class="fa fa-pencil icon"></span>
                                        </button>
                                    </span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                                            <span class="fa fa-plus icon"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
            
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_celular" class="col-sm-4 control-label">CELULAR</label>
                                <div class="col-sm-8"> 
                                    <input type="text" class="form-control input-sm" name="cliente_cel_<?php echo $proceso_fase_id; ?>" id="cliente_cel_<?php echo $proceso_fase_id; ?>" onkeypress="return solo_numero(event)" placeholder="Ingrese N° Celular" value="<?php echo $cliente_cel ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="inspeccion_vehiculo_const_sc" class="col-sm-4 control-label">INSPECCIÓN DE VEHÍCULO</label>
                                <div class="col-sm-8"> 
                                    <div class="input-group date"> 
                                        <input type='text' class="form-control input-sm" name="inspeccion_vehiculo_const_sc" id="inspeccion_vehiculo_const_sc" value="<?php echo mostrar_fecha($fec_inspeccion); ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="prueba_vehiculo_const_sc" class="col-sm-12 control-label">¿GERENTE PROBÓ EL VEHÍCULO?</label>
                                <div class="col-sm-12"> 
                                    <label class="radio-inline" style="padding-left: 0px;">
                                        <input type="radio" name="prueba_vehiculo_const_sc" value="s" class="flat-green" <?php if($prueba_veh=='s') echo 'checked' ?>> SI
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="prueba_vehiculo_const_sc" value="n" class="flat-green" <?php if($prueba_veh=='n') echo 'checked' ?>> NO
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="prueba_vehiculo_const_sc" value="e" class="flat-green" <?php if($prueba_veh=='e') echo 'checked' ?>> EXONERADO
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_acepta_const_sc" class="col-sm-12 control-label">¿CLIENTE DE ACUERDO CON LO PRESENTADO?</label>
                                <div class="col-sm-12"> 
                                    <label class="radio-inline" style="padding-left: 0px;">
                                        <input type="radio" name="cliente_acepta_const_sc" value="s" class="flat-green" <?php if($cli_acepta=='s') echo 'checked' ?>> SI
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cliente_acepta_const_sc" value="n" class="flat-green" <?php if($cli_acepta=='n') echo 'checked' ?>> NO
                                    </label>
                                </div>
                                <div class="col-sm-12" style="margin-top: 5px;"> 
                                    <textarea id="cliente_acepta_des_const_sc" name="cliente_acepta_des_const_sc" class="form-control input-sm" rows="3"; style="resize: none;" placeholder="Registre detalle de la decisión del cliente..."><?php echo $cli_acepta_det; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <!-- <div align="center" class="col-md-12" style="padding-bottom: 10px; padding-top: 5px;">
                            <button 
                                class="btn btn-primary btn-sm " 
                                type="button" 
                                name="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                id="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                title="Guardar Datos" 
                                onClick="guardar_consti_fase1(<?php echo $proceso_fase_id; ?>)">
                                <span class="fa fa-save"></span> Guardar Cambios
                            </button>
                        </div> -->

                        <div class="col-md-12">
                            <section id="participantes" style="padding-top: 10px;">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class='col-md-3 h5'><label>Añadir Participantes</label></div>
                                        <div class='col-md-9' id="rowPlusParticipante">
                                        <div class="input-group" id="buttonPlusParticipante">
                                            <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoParticipante').removeClass('hidden'); $('#rowPlusParticipante').addClass('hidden'); limpiarCamposParticipante();" data-toggle="tooltip" title="Añadir Ingreso"><li class="fa fa-plus"></li></button>
                                        </div>
                                        <br>
                                        </div>
                                    </div>

                                    <div class="col-md-12 hidden" id="divNuevoParticipante">
                                        <form name="frmNuevoParticipante" id="frmNuevoParticipante">
                                        <input type="hidden" name="action" value="registrar_participante">
                                        <input type="hidden" name="hdd_participante_proceso_id" id="hdd_participante_proceso_id" value="<?php echo $proceso_id; ?>">

                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-10">
                                            
                                            <div class='col-md-12'>	
                                                <div class="input-group" >
                                                <span class="input-group-addon"><strong>Nombres y Apellidos</strong><span class="text-red">*</span></span>
                                                <input autocomplete="off" class="form-control input-sm" name="nombre_p" id="nombre_p" type="text" placeholder="Nombres y apellidos de participante"/>
                                                </div>
                                                <br>
                                            </div>

                                            <div class='col-md-6'>	
                                                <div class="input-group" >
                                                <span class="input-group-addon"><strong>DNI</strong><span class="text-red">*</span></span>
                                                <input autocomplete="off" class="form-control input-sm" name="documento_p" id="documento_p" type="text" onkeypress="return solo_numero(event)" placeholder="DNI de participante"/>
                                                </div>
                                                <br>
                                            </div>

                                            <div class='col-md-6'>	
                                                <div class="input-group" >
                                                <span class="input-group-addon"><strong>Celular</strong></span>
                                                <input autocomplete="off" class="form-control input-sm" name="celular_p" id="celular_p" type="text" onkeypress="return solo_numero(event)" placeholder="Celular de participante"/>
                                                </div>
                                                <br>
                                            </div>

                                            </div>

                                            <div class="col-md-12" align="right" >
                                            <button class="btn btn-sm btn-primary" type="button" id="btnParticipante" onClick="guardarParticipanteDetalle('frmNuevoParticipante', 'Participante','I')"><li class="fa fa-save"></li> Guardar</button>
                                            <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoParticipante').addClass('hidden'); $('#frmNuevoParticipante').trigger('reset'); $('#rowPlusParticipante').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                                            </div>
                                        </div>
                                        </form>
                                        <br>
                                    </div>

                                    <div class="col-md-12" id="listaParticipante">

                                    </div>

                                </div>
                            </section>
                        </div>

                    </div>

                    <div id="div_items_contenido<?php echo $proceso_fase_id; ?>" class="col-md-6">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                        $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                    // $siguiente_fase seguirá como true si el item ha sido exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){  // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                    }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                        $siguiente_fase = false;
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }

                        ?>
                                <div id="item<?php echo $item['tb_proceso_fase_item_id']; ?>" class="col-md-12" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <?php } ?>
                                            <?php if($item['tb_item_idx'] == 'hoja_pre_calificacion'){ // solo mostrar botón en nivel uno para formato pre calificacion ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="hojaPrecalificacion('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="comentarioPreCalificacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Comentarios"><span class="fa fa-commenting"></span></span></button>
                                            <?php } ?>
                                            <?php if($item['tb_item_idx'] == 'form_ing_egr'){ // solo mostrar botón en nivel uno para formato de ingresos y gastos ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="ingresosEgresos('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                    /* if($siguiente_fase){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                                        $boton_precali = '';
                                        $color_boton_precali = 'btn-success';
                                    }else{
                                        $boton_precali = 'disabled';
                                        $color_boton_precali = 'btn-default';
                                    } */
                                    ?>
                                </div>
                        <?php 
                            }
                        }
                        ?>
                    </div>

                <?php } elseif($proceso_fase_orden == 2){ // Orden de Fase ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                        $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                    // $siguiente_fase seguirá como true si el item ha sido exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                    }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                        $siguiente_fase = false;
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                            <?php if($item['tb_item_idx']=='libro_negro' || $item['tb_item_idx']=='boleta_factura'){echo '';}else{echo $bloqueo_exo;} // Manejar las exoneraciones por parte del colaborador ?> 
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                            }
                        }
                        ?>
                    </div>

                    <!-- Boton de Bancarización -->
                    <div align="left" class="col-md-12" style="padding-top: 15px;">
                        <div class="form-group">
                            <button type="button" class="btn bg-maroon" onclick="elegirBancarizacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-money"></i> Bancarización</strong></button>
                            <button type="button" class="btn bg-purple" onclick="generarCheche(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-indent"></i> Generar Cheques</strong></button>
                            <button type="button" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id;?>, 'con_firma');"><strong><i class="fa fa-download"></i> Hoja Pre Calicación</strong></button>
                        </div>
                    </div>
                    <!--  -->

                    <!-- Votación de comité -->
                    <?php //if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 56){ // Solo mostrar al comité LOCAL ?>
                    <div class="col-md-12" style="padding-top: 15px;">
                    <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>
                        <h4><strong>COMITÉ: </strong></h4>
                    <?php } ?>

                        <table>
                            <tbody>
                                <?php if($comite['estado'] == 1){ ?>
                                    <?php foreach($comite['data'] as $key => $com){ ?>
                                    <?php 
                                            $voto_miembro = $oProceso->obtenerVotoComite($proceso_id, $com['tb_usuario_id']); 
                                            $voto_comite = 0;
                                            if($voto_miembro['estado']==1){
                                                $voto_comite = $voto_miembro['data']['tb_comitevoto_voto'];
                                            }
                                    ?>
                                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>

                                        <tr>
                                            <td style="padding: 10px;"><strong>- <?php echo $com['usuario'] ?></strong></td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_si" 
                                                    value="2" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==2) echo 'checked' ?>
                                                >
                                                    <strong> Procede</strong>
                                            </td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                    value="1" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==1) echo 'checked' ?>
                                                >
                                                    <strong> No procede</strong>
                                            </td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                    value="0" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==0) echo 'checked' ?>
                                                >
                                                    <strong> Sin voto</strong>
                                            </td>
                                        </tr>
                                        <?php } ?>

                                        <?php 
                                            // CONTEO DE VOTOS PARA FASE 2
                                            if($voto_miembro['data']['tb_comitevoto_voto']==2){ // Votos a favor
                                                $suma_boton_precali = $suma_boton_precali + floatval($com['tb_comite_cred']);
                                                $voto_comite_si++;
                                            }elseif($voto_miembro['data']['tb_comitevoto_voto']==1){ // Votos en contra
                                                $voto_comite_no++;
                                            }elseif($voto_miembro['data']['tb_comitevoto_voto']==0){ // Aún no vota
                                                $voto_comite_sin_voto++;
                                            }
                                        ?>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>

                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité 
                        
                            $proceso_otro = $oProceso->mostrarUno($proceso_id);
                            $firma_otro = 0;
                            if($proceso_otro['estado'] == 1){
                                $firma_otro = intval($proceso_otro['data']['tb_proceso_usu_firma_otro']);
                            }
        
                        ?>
                            
                            <br>
                            <?php if($comite_otros['estado'] == 1){ ?>
                                <h5><strong>OTROS: </strong></h5>
                                <table>
                                    <tbody>

                                        <?php foreach($comite_otros['data'] as $key => $otro){ ?>
                                            <tr>
                                                <td>
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_otro" 
                                                        id="voto_comite_otro_<?php echo $otro['tb_usuario_id']; ?>" 
                                                        value="<?php echo $otro['tb_usuario_id']; ?>" 
                                                        onChange="votoComiteOtro(this.value, <?php echo $otro['tb_usuario_id']; ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($firma_otro==$otro['tb_usuario_id']) echo 'checked' ?>
                                                    >
                                                        <strong> <?php echo $otro['usuario'] ?></strong>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            <?php } ?>

                        <?php } ?>

                    </div>

                <?php } elseif($proceso_fase_orden == 3){ // Orden de Fase ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                            if(intval($item['tb_item_id']) != 64 && intval($item['tb_item_id']) != 65){ // Se excluye CHEQUE GERENCIA o OTRO TIPO DE DESEMBOLSO se maneja de forma manual luego
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                            }
                            }
                        }
                        ?>
                    </div>

                    <div class="col-md-12 page-header" style="padding-top: 15px;"></div>

                    <?php if($proc_banca == 1){ //CHEQUES ?>
                    <!-- Desembolso Cheques -->
                    <div class="col-md-12" style="padding-top: 15px;">
                        <div class="col-md-6" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <?php
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    $btn_doc = '';
                                    $multimedia_id = 0; // para cuando ya existe archivo subido
                                    $proceso_fase_item_id =  0;

                                    $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 64);
                                    $proceso_fase_item_64 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 64);

                                    if($items_fase1['estado']==1){
                                        $action='editar';

                                        $estado_item = 'fa-circle text-green';
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        $btn_doc = '';
                                        $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                    }

                                    if($proceso_fase_item_64['estado']==1){
                                        $proceso_fase_item_id = intval($proceso_fase_item_64['data']['tb_proceso_fase_item_id']);
                                    }


                                ?>
                                <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> CHEQUE DE GERENCIA</label>
                                <div class="col-sm-4" style="padding: 0px;">
                                    <button 
                                        <?php echo $bloqueo_exo; ?>
                                        class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                        type="button" 
                                        name="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="<?php echo $btn_exo_title; ?>" 
                                        onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                        <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                    </button>
                                    <?php if($action == 'editar'){ ?>
                                        <button 
                                            class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                            type="button" 
                                            name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Documento"
                                            onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                            <span class="fa fa-file-pdf-o"></span>
                                        </button>
                                    <?php }else{ ?>
                                        <button 
                                            class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                            type="button" 
                                            name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Documento"
                                            onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                            <span class="fa fa-file-pdf-o"></span>
                                        </button>
                                    <?php } ?>
                                    
                                    <button 
                                        class="btn btn-warning btn-sm" 
                                        type="button" 
                                        name="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_id; ?>, 'tb_proc_proceso_fase_item')">
                                        <span class="fa fa-history"></span>
                                    </button>
                                    
                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" title="Subir Cheque" onclick="subidaCheques(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $proceso_fase_item_id;?>);"><span class="fa fa-upload"></span></button>
                                    <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_id;?>, 'cheque')"><span class="fa fa-money"></span></button>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <?php }else{ ?>
                        <?php if($proc_banca == 0){ ?>
                            <h5 class="" style="color: red;"> <strong>Seleccione una opción de bancarización en la Fase 2.</strong></h5>
                        <?php }else{ ?>
                            <!-- Desembolso otro medio de pago -->
                            <div class="col-md-12">
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <?php
                                            $action='nuevo';

                                            $estado_item = 'fa-circle-o text-red';
                                            $btn_exo = '';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            $btn_doc = '';
                                            $multimedia_id = 0; // para cuando ya existe archivo subido

                                            $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 65);
                                            $proceso_fase_item_65 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 65);

                                            if($items_fase1['estado']==1){
                                                $action='editar';

                                                $estado_item = 'fa-circle text-green';
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                $btn_doc = '';
                                                $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                            }

                                        ?>
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> EFECTIVO, TRANSFERENCIA O DEPÓSITO</label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>, 'otro')"><span class="fa fa-money"></span> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        <?php } ?>
                    <?php } ?>

                <?php } else { ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];
                        $items_comprimidos = [];

                        $i = 0;
                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido
                                $multi_exo = 0;
                                $multi_url = '';

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';
                                    $multi_exo = 1;
                                    $multi_url = '...';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                        
                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                            $multi_url      = $items_fase['data']['tb_multimedia_url'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <!-- < ?php if($item['tb_item_idx'] == 'escritura_publica'){ // solo mostrar botón en nivel 4 para formulario de titulo ?>
                                                <button class="btn btn-info btn-sm" type="button" name="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="titulo_form(< ?php echo $credito_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            < ?php } ?> -->
                                        </div>
                                    </div>
                                </div>
                        <?php 

                                //añadir valores dde item al array
                                $valor['item_id']=$item['tb_item_id'];
                                $valor['item_des']=$item['tb_item_des'];
                                $valor['multi_exo']=$multi_exo;
                                $valor['multi_url']=$multi_url;
                                $items_comprimidos[$i]=$valor;
                                $i++;
                            }
                        }
                        $array = json_encode($items_comprimidos);
                        ?>

                    </div>

                    <br>
                    <input type="hidden" id="items_comprimidos" name="items_comprimidos[]" value='<?php echo json_encode($items_comprimidos); ?>'>
                    <div align="center">
                        <button type="button" class="btn btn-primary" id="" name="" onclick="comprimirArchivos(<?php echo $item['tb_proceso_id']; ?>)" /><strong><i class="fa fa-file-zip-o"></i> COMPRIMIR</strong></button>
                        <a id="downloadZip" name="downloadZip" href="" target="blank_"></a> <!-- Enlace que disparra la descarga del zip -->
                    </div>

                <?php } ?>

            </div>
        </div>

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 3){ // CREDITO GARVEH - Constitución Con Custodia ?>

        <div class="row">
            <div class="col-md-12">
                
                <?php if($proceso_fase_orden == 1){ // Orden de Fase ?>
                    <div class="col-md-6">

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_nom" class="col-sm-4 control-label">CLIENTE</label>
                                <div class="col-sm-6 input-group">
                                    <input type="hidden" id="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                    <input type="hidden" id="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                    <input type="hidden" id="hdd_proc_fase_id_temp" name="hdd_proc_fase_id_temp" value="<?php echo $proceso_fase_id ?>">
                                    <input type="hidden" id="cliente_dni_<?php echo $proceso_fase_id; ?>" name="cliente_dni_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_numdoc ?>">
                                    <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id ?>">
                                
                                    <input type="text" class="form-control input-sm" name="cliente_nom_<?php echo $proceso_fase_id; ?>" id="cliente_nom_<?php echo $proceso_fase_id; ?>" placeholder="Ingrese Cliente" value="<?php echo $cliente_nom ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                                            <span class="fa fa-pencil icon"></span>
                                        </button>
                                    </span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                                            <span class="fa fa-plus icon"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
            
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_celular" class="col-sm-4 control-label">CELULAR</label>
                                <div class="col-sm-8"> 
                                    <input type="text" class="form-control input-sm" name="cliente_cel_<?php echo $proceso_fase_id; ?>" id="cliente_cel_<?php echo $proceso_fase_id; ?>" onkeypress="return solo_numero(event)" placeholder="Ingrese N° Celular" value="<?php echo $cliente_cel ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="inspeccion_vehiculo_const_sc" class="col-sm-4 control-label">INSPECCIÓN DE VEHÍCULO</label>
                                <div class="col-sm-8"> 
                                    <div class="input-group date"> 
                                        <input type='text' class="form-control input-sm" name="inspeccion_vehiculo_const_sc" id="inspeccion_vehiculo_const_sc" value="<?php echo mostrar_fecha($fec_inspeccion); ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="prueba_vehiculo_const_sc" class="col-sm-12 control-label">¿GERENTE PROBÓ EL VEHÍCULO?</label>
                                <div class="col-sm-12"> 
                                    <label class="radio-inline" style="padding-left: 0px;">
                                        <input type="radio" name="prueba_vehiculo_const_sc" value="s" class="flat-green" <?php if($prueba_veh=='s') echo 'checked' ?>> SI
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="prueba_vehiculo_const_sc" value="n" class="flat-green" <?php if($prueba_veh=='n') echo 'checked' ?>> NO
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="prueba_vehiculo_const_sc" value="e" class="flat-green" <?php if($prueba_veh=='e') echo 'checked' ?>> EXONERADO
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_acepta_const_sc" class="col-sm-12 control-label">¿CLIENTE DE ACUERDO CON LO PRESENTADO?</label>
                                <div class="col-sm-12"> 
                                    <label class="radio-inline" style="padding-left: 0px;">
                                        <input type="radio" name="cliente_acepta_const_sc" value="s" class="flat-green" <?php if($cli_acepta=='s') echo 'checked' ?>> SI
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cliente_acepta_const_sc" value="n" class="flat-green" <?php if($cli_acepta=='n') echo 'checked' ?>> NO
                                    </label>
                                </div>
                                <div class="col-sm-12" style="margin-top: 5px;"> 
                                    <textarea id="cliente_acepta_des_const_sc" name="cliente_acepta_des_const_sc" class="form-control input-sm" rows="3"; style="resize: none;" placeholder="Registre detalle de la decisión del cliente..."><?php echo $cli_acepta_det; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <!-- <div align="center" class="col-md-12" style="padding-bottom: 10px; padding-top: 5px;">
                            <button 
                                class="btn btn-primary btn-sm " 
                                type="button" 
                                name="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                id="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                title="Guardar Datos" 
                                onClick="guardar_consti_fase1(<?php echo $proceso_fase_id; ?>)">
                                <span class="fa fa-save"></span> Guardar Cambios
                            </button>
                        </div> -->

                        <div class="col-md-12">
                            <section id="participantes" style="padding-top: 10px;">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class='col-md-3 h5'><label>Añadir Participantes</label></div>
                                        <div class='col-md-9' id="rowPlusParticipante">
                                        <div class="input-group" id="buttonPlusParticipante">
                                            <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoParticipante').removeClass('hidden'); $('#rowPlusParticipante').addClass('hidden'); limpiarCamposParticipante();" data-toggle="tooltip" title="Añadir Ingreso"><li class="fa fa-plus"></li></button>
                                        </div>
                                        <br>
                                        </div>
                                    </div>

                                    <div class="col-md-12 hidden" id="divNuevoParticipante">
                                        <form name="frmNuevoParticipante" id="frmNuevoParticipante">
                                        <input type="hidden" name="action" value="registrar_participante">
                                        <input type="hidden" name="hdd_participante_proceso_id" id="hdd_participante_proceso_id" value="<?php echo $proceso_id; ?>">

                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-10">
                                            
                                            <div class='col-md-12'>	
                                                <div class="input-group" >
                                                <span class="input-group-addon"><strong>Nombres y Apellidos</strong><span class="text-red">*</span></span>
                                                <input autocomplete="off" class="form-control input-sm" name="nombre_p" id="nombre_p" type="text" placeholder="Nombres y apellidos de participante"/>
                                                </div>
                                                <br>
                                            </div>

                                            <div class='col-md-6'>	
                                                <div class="input-group" >
                                                <span class="input-group-addon"><strong>DNI</strong><span class="text-red">*</span></span>
                                                <input autocomplete="off" class="form-control input-sm" name="documento_p" id="documento_p" type="text" onkeypress="return solo_numero(event)" placeholder="DNI de participante"/>
                                                </div>
                                                <br>
                                            </div>

                                            <div class='col-md-6'>	
                                                <div class="input-group" >
                                                <span class="input-group-addon"><strong>Celular</strong></span>
                                                <input autocomplete="off" class="form-control input-sm" name="celular_p" id="celular_p" type="text" onkeypress="return solo_numero(event)" placeholder="Celular de participante"/>
                                                </div>
                                                <br>
                                            </div>

                                            </div>

                                            <div class="col-md-12" align="right" >
                                            <button class="btn btn-sm btn-primary" type="button" id="btnParticipante" onClick="guardarParticipanteDetalle('frmNuevoParticipante', 'Participante','I')"><li class="fa fa-save"></li> Guardar</button>
                                            <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoParticipante').addClass('hidden'); $('#frmNuevoParticipante').trigger('reset'); $('#rowPlusParticipante').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                                            </div>
                                        </div>
                                        </form>
                                        <br>
                                    </div>

                                    <div class="col-md-12" id="listaParticipante">

                                    </div>

                                </div>
                            </section>
                        </div>
                    </div>

                    <div id="div_items_contenido<?php echo $proceso_fase_id; ?>" class="col-md-6">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                        $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                    // $siguiente_fase seguirá como true si el item ha sido exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){  // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                    }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                        $siguiente_fase = false;
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }

                        ?>
                                <div id="item<?php echo $item['tb_proceso_fase_item_id']; ?>" class="col-md-12" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <?php } ?>
                                            <?php if($item['tb_item_idx'] == 'hoja_pre_calificacion'){ // solo mostrar botón en nivel uno para formato pre calificacion ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="hojaPrecalificacion('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="comentarioPreCalificacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Comentarios"><span class="fa fa-commenting"></span></span></button>
                                            <?php } ?>
                                            <?php if($item['tb_item_idx'] == 'form_ing_egr'){ // solo mostrar botón en nivel uno para formato de ingresos y gastos ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="ingresosEgresos('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                    /* if($siguiente_fase){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                                        $boton_precali = '';
                                        $color_boton_precali = 'btn-success';
                                    }else{
                                        $boton_precali = 'disabled';
                                        $color_boton_precali = 'btn-default';
                                    } */
                                    ?>
                                </div>
                        <?php 
                            }
                        }
                        ?>
                    </div>

                <?php } elseif($proceso_fase_orden == 2){ // Orden de Fase ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                        $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                    // $siguiente_fase seguirá como true si el item ha sido exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                    }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                        $siguiente_fase = false;
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                            <?php if($item['tb_item_idx']=='libro_negro' || $item['tb_item_idx']=='boleta_factura'){echo '';}else{echo $bloqueo_exo;} // Manejar las exoneraciones por parte del colaborador ?> 
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                            }
                        }
                        ?>
                    </div>

                    <!-- Boton de Bancarización -->
                    <div align="left" class="col-md-12" style="padding-top: 15px;">
                        <div class="form-group">
                            <button type="button" class="btn bg-maroon" onclick="elegirBancarizacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-money"></i> Bancarización</strong></button>
                            <button type="button" class="btn bg-purple" onclick="generarCheche(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-indent"></i> Generar Cheques</strong></button>
                            <button type="button" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id;?>, 'con_firma');"><strong><i class="fa fa-download"></i> Hoja Pre Calicación</strong></button>
                        </div>
                    </div>
                    <!--  -->

                    <!-- Votación de comité -->
                    <?php //if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 56){ // Solo mostrar al comité LOCAL ?>
                    <div class="col-md-12" style="padding-top: 15px;">
                    <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>
                        <h4><strong>COMITÉ: </strong></h4>
                    <?php } ?>

                        <table>
                            <tbody>
                                <?php if($comite['estado'] == 1){ ?>
                                    <?php foreach($comite['data'] as $key => $com){ ?>
                                    <?php 
                                            $voto_miembro = $oProceso->obtenerVotoComite($proceso_id, $com['tb_usuario_id']); 
                                            $voto_comite = 0;
                                            if($voto_miembro['estado']==1){
                                                $voto_comite = $voto_miembro['data']['tb_comitevoto_voto'];
                                            }
                                    ?>
                                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>

                                        <tr>
                                            <td style="padding: 10px;"><strong>- <?php echo $com['usuario'] ?></strong></td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_si" 
                                                    value="2" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==2) echo 'checked' ?>
                                                >
                                                    <strong> Procede</strong>
                                            </td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                    value="1" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==1) echo 'checked' ?>
                                                >
                                                    <strong> No procede</strong>
                                            </td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                    value="0" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==0) echo 'checked' ?>
                                                >
                                                    <strong> Sin voto</strong>
                                            </td>
                                        </tr>
                                        <?php } ?>

                                        <?php 
                                            // CONTEO DE VOTOS PARA FASE 2
                                            if($voto_miembro['data']['tb_comitevoto_voto']==2){ // Votos a favor
                                                $suma_boton_precali = $suma_boton_precali + floatval($com['tb_comite_cred']);
                                                $voto_comite_si++;
                                            }elseif($voto_miembro['data']['tb_comitevoto_voto']==1){ // Votos en contra
                                                $voto_comite_no++;
                                            }elseif($voto_miembro['data']['tb_comitevoto_voto']==0){ // Aún no vota
                                                $voto_comite_sin_voto++;
                                            }
                                        ?>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>

                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité 
                        
                            $proceso_otro = $oProceso->mostrarUno($proceso_id);
                            $firma_otro = 0;
                            if($proceso_otro['estado'] == 1){
                                $firma_otro = intval($proceso_otro['data']['tb_proceso_usu_firma_otro']);
                            }
        
                        ?>
                            
                            <br>
                            <?php if($comite_otros['estado'] == 1){ ?>
                                <h5><strong>OTROS: </strong></h5>
                                <table>
                                    <tbody>

                                        <?php foreach($comite_otros['data'] as $key => $otro){ ?>
                                            <tr>
                                                <td>
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_otro" 
                                                        id="voto_comite_otro_<?php echo $otro['tb_usuario_id']; ?>" 
                                                        value="<?php echo $otro['tb_usuario_id']; ?>" 
                                                        onChange="votoComiteOtro(this.value, <?php echo $otro['tb_usuario_id']; ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($firma_otro==$otro['tb_usuario_id']) echo 'checked' ?>
                                                    >
                                                        <strong> <?php echo $otro['usuario'] ?></strong>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            <?php } ?>

                        <?php } ?>

                    </div>
                    
                <?php } elseif($proceso_fase_orden == 3){ // Orden de Fase ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                            if(intval($item['tb_item_id']) != 64 && intval($item['tb_item_id']) != 65){ // Se excluye CHEQUE GERENCIA o OTRO TIPO DE DESEMBOLSO se maneja de forma manual luego
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                            }
                            }
                        }
                        ?>
                    </div>

                    <div class="col-md-12 page-header" style="padding-top: 15px;"></div>

                    <?php if($proc_banca == 1){ //CHEQUES ?>
                    <!-- Desembolso Cheques -->
                    <div class="col-md-12" style="padding-top: 15px;">
                        <div class="col-md-6" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <?php
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    $btn_doc = '';
                                    $multimedia_id = 0; // para cuando ya existe archivo subido
                                    $proceso_fase_item_id = 0;

                                    $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 64);
                                    $proceso_fase_item_64 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 64);

                                    if($items_fase1['estado']==1){
                                        $action='editar';

                                        $estado_item = 'fa-circle text-green';
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        $btn_doc = '';
                                        $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                    }

                                    if($proceso_fase_item_64['estado']==1){
                                        $proceso_fase_item_id = intval($proceso_fase_item_64['data']['tb_proceso_fase_item_id']);
                                    }

                                ?>
                                <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> CHEQUE DE GERENCIA</label>
                                <div class="col-sm-4" style="padding: 0px;">
                                    <button 
                                        <?php echo $bloqueo_exo; ?>
                                        class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                        type="button" 
                                        name="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="<?php echo $btn_exo_title; ?>" 
                                        onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                        <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                    </button>
                                    <?php if($action == 'editar'){ ?>
                                        <button 
                                            class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                            type="button" 
                                            name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Documento"
                                            onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                            <span class="fa fa-file-pdf-o"></span>
                                        </button>
                                    <?php }else{ ?>
                                        <button 
                                            class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                            type="button" 
                                            name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Documento"
                                            onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                            <span class="fa fa-file-pdf-o"></span>
                                        </button>
                                    <?php } ?>
                                    
                                    <button 
                                        class="btn btn-warning btn-sm" 
                                        type="button" 
                                        name="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_id; ?>, 'tb_proc_proceso_fase_item')">
                                        <span class="fa fa-history"></span>
                                    </button>
                                    
                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" title="Subir Cheque" onclick="subidaCheques(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $proceso_fase_item_id;?>);"><span class="fa fa-upload"></span></button>
                                    <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_id;?>, 'cheque')"><span class="fa fa-money"></span></button>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <?php }else{ ?>
                        <?php if($proc_banca == 0){ ?>
                            <h5 class="" style="color: red;"> <strong>Seleccione una opción de bancarización en la Fase 2.</strong></h5>
                        <?php }else{ ?>
                            <!-- Desembolso otro medio de pago -->
                            <div class="col-md-12">
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <?php
                                            $action='nuevo';

                                            $estado_item = 'fa-circle-o text-red';
                                            $btn_exo = '';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            $btn_doc = '';
                                            $multimedia_id = 0; // para cuando ya existe archivo subido

                                            $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 65);
                                            $proceso_fase_item_65 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 65);

                                            if($items_fase1['estado']==1){
                                                $action='editar';

                                                $estado_item = 'fa-circle text-green';
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                $btn_doc = '';
                                                $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                            }

                                        ?>
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> EFECTIVO, TRANSFERENCIA O DEPÓSITO</label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>, 'otro')"><span class="fa fa-money"></span> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        <?php } ?>
                    <?php } ?>

                <?php } else { ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];
                        $items_comprimidos = [];

                        $i = 0;
                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido
                                $multi_exo = 0;
                                $multi_url = '';

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';
                                    $multi_exo = 1;
                                    $multi_url = '...';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                        
                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                            $multi_url      = $items_fase['data']['tb_multimedia_url'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <!-- < ?php if($item['tb_item_idx'] == 'escritura_publica'){ // solo mostrar botón en nivel 4 para formulario de titulo ?>
                                                <button class="btn btn-info btn-sm" type="button" name="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="titulo_form(< ?php echo $credito_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            < ?php } ?> -->
                                        </div>
                                    </div>
                                </div>
                        <?php 

                                //añadir valores dde item al array
                                $valor['item_id']=$item['tb_item_id'];
                                $valor['item_des']=$item['tb_item_des'];
                                $valor['multi_exo']=$multi_exo;
                                $valor['multi_url']=$multi_url;
                                $items_comprimidos[$i]=$valor;
                                $i++;
                            }
                        }
                        $array = json_encode($items_comprimidos);
                        ?>

                    </div>

                    <br>
                    <input type="hidden" id="items_comprimidos" name="items_comprimidos[]" value='<?php echo json_encode($items_comprimidos); ?>'>
                    <div align="center">
                        <button type="button" class="btn btn-primary" id="" name="" onclick="comprimirArchivos(<?php echo $item['tb_proceso_id']; ?>)" /><strong><i class="fa fa-file-zip-o"></i> COMPRIMIR</strong></button>
                        <a id="downloadZip" name="downloadZip" href="" target="blank_"></a> <!-- Enlace que disparra la descarga del zip -->
                    </div>

                <?php } ?>

            </div>
        </div>

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 4){ // CREDITO GARVEH - Constitución Compraventa a tercero Sin Custodia ?>

        <div class="row">
            <div class="col-md-12">
                
                <?php if($proceso_fase_orden == 1){ // Orden de Fase ?>
                        <div class="col-md-6">

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="cliente_nom" class="col-sm-4 control-label">CLIENTE</label>
                                    <div class="col-sm-6 input-group">
                                        <input type="hidden" id="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                        <input type="hidden" id="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                        <input type="hidden" id="hdd_proc_fase_id_temp" name="hdd_proc_fase_id_temp" value="<?php echo $proceso_fase_id ?>">
                                        <input type="hidden" id="cliente_dni_<?php echo $proceso_fase_id; ?>" name="cliente_dni_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_numdoc ?>">
                                        <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id ?>">
                                    
                                        <input type="text" class="form-control input-sm" name="cliente_nom_<?php echo $proceso_fase_id; ?>" id="cliente_nom_<?php echo $proceso_fase_id; ?>" placeholder="Ingrese Cliente" value="<?php echo $cliente_nom ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                                                <span class="fa fa-pencil icon"></span>
                                            </button>
                                        </span>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                                                <span class="fa fa-plus icon"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                
                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="cliente_celular" class="col-sm-4 control-label">CELULAR</label>
                                    <div class="col-sm-8"> 
                                        <input type="text" class="form-control input-sm" name="cliente_cel_<?php echo $proceso_fase_id; ?>" id="cliente_cel_<?php echo $proceso_fase_id; ?>" onkeypress="return solo_numero(event)" placeholder="Ingrese N° Celular" value="<?php echo $cliente_cel ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="inspeccion_vehiculo_const_sc" class="col-sm-4 control-label">INSPECCIÓN DE VEHÍCULO</label>
                                    <div class="col-sm-8"> 
                                        <div class="input-group date"> 
                                            <input type='text' class="form-control input-sm" name="inspeccion_vehiculo_const_sc" id="inspeccion_vehiculo_const_sc" value="<?php echo mostrar_fecha($fec_inspeccion); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="prueba_vehiculo_const_sc" class="col-sm-12 control-label">¿GERENTE PROBÓ EL VEHÍCULO?</label>
                                    <div class="col-sm-12"> 
                                        <label class="radio-inline" style="padding-left: 0px;">
                                            <input type="radio" name="prueba_vehiculo_const_sc" value="s" class="flat-green" <?php if($prueba_veh=='s') echo 'checked' ?>> SI
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="prueba_vehiculo_const_sc" value="n" class="flat-green" <?php if($prueba_veh=='n') echo 'checked' ?>> NO
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="prueba_vehiculo_const_sc" value="e" class="flat-green" <?php if($prueba_veh=='e') echo 'checked' ?>> EXONERADO
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="cliente_acepta_const_sc" class="col-sm-12 control-label">¿CLIENTE DE ACUERDO CON LO PRESENTADO?</label>
                                    <div class="col-sm-12"> 
                                        <label class="radio-inline" style="padding-left: 0px;">
                                            <input type="radio" name="cliente_acepta_const_sc" value="s" class="flat-green" <?php if($cli_acepta=='s') echo 'checked' ?>> SI
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="cliente_acepta_const_sc" value="n" class="flat-green" <?php if($cli_acepta=='n') echo 'checked' ?>> NO
                                        </label>
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 5px;"> 
                                        <textarea id="cliente_acepta_des_const_sc" name="cliente_acepta_des_const_sc" class="form-control input-sm" rows="3"; style="resize: none;" placeholder="Registre detalle de la decisión del cliente..."><?php echo $cli_acepta_det; ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- <div align="center" class="col-md-12" style="padding-bottom: 10px; padding-top: 5px;">
                                <button 
                                    class="btn btn-primary btn-sm " 
                                    type="button" 
                                    name="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                    id="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                    title="Guardar Datos" 
                                    onClick="guardar_consti_fase1(<?php echo $proceso_fase_id; ?>)">
                                    <span class="fa fa-save"></span> Guardar Cambios
                                </button>
                            </div> -->

                            <div class="col-md-12">
                                <section id="participantes" style="padding-top: 10px;">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class='col-md-3 h5'><label>Añadir Beneficiarios</label></div>
                                            <div class='col-md-9' id="rowPlusParticipante">
                                            <div class="input-group" id="buttonPlusParticipante">
                                                <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoParticipante').removeClass('hidden'); $('#rowPlusParticipante').addClass('hidden'); limpiarCamposParticipante();" data-toggle="tooltip" title="Añadir Ingreso"><li class="fa fa-plus"></li></button>
                                            </div>
                                            <br>
                                            </div>
                                        </div>

                                        <div class="col-md-12 hidden" id="divNuevoParticipante">
                                            <form name="frmNuevoParticipante" id="frmNuevoParticipante">
                                            <input type="hidden" name="action" value="registrar_participante">
                                            <input type="hidden" name="hdd_participante_proceso_id" id="hdd_participante_proceso_id" value="<?php echo $proceso_id; ?>">

                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                
                                                <div class='col-md-12'>	
                                                    <div class="input-group" >
                                                    <span class="input-group-addon"><strong>Nombres y Apellidos</strong><span class="text-red">*</span></span>
                                                    <input autocomplete="off" class="form-control input-sm" name="nombre_p" id="nombre_p" type="text" placeholder="Nombres y apellidos de beneficiario"/>
                                                    </div>
                                                    <br>
                                                </div>

                                                <div class='col-md-6'>	
                                                    <div class="input-group" >
                                                    <span class="input-group-addon"><strong>DNI</strong><span class="text-red">*</span></span>
                                                    <input autocomplete="off" class="form-control input-sm" name="documento_p" id="documento_p" type="text" onkeypress="return solo_numero(event)" placeholder="DNI de beneficiario"/>
                                                    </div>
                                                    <br>
                                                </div>

                                                <div class='col-md-6'>	
                                                    <div class="input-group" >
                                                    <span class="input-group-addon"><strong>Celular</strong></span>
                                                    <input autocomplete="off" class="form-control input-sm" name="celular_p" id="celular_p" type="text" onkeypress="return solo_numero(event)" placeholder="Celular de beneficiario"/>
                                                    </div>
                                                    <br>
                                                </div>

                                                </div>

                                                <div class="col-md-12" align="right" >
                                                <button class="btn btn-sm btn-primary" type="button" id="btnParticipante" onClick="guardarParticipanteDetalle('frmNuevoParticipante', 'Participante','I')"><li class="fa fa-save"></li> Guardar</button>
                                                <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoParticipante').addClass('hidden'); $('#frmNuevoParticipante').trigger('reset'); $('#rowPlusParticipante').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                                                </div>
                                            </div>
                                            </form>
                                            <br>
                                        </div>

                                        <div class="col-md-12" id="listaParticipante">

                                        </div>

                                    </div>
                                </section>
                            </div>
                        </div>

                        <div id="div_items_contenido<?php echo $proceso_fase_id; ?>" class="col-md-6">
                            <?php 
                            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                            $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                            $estados_items = [];

                            if($items['estado']==1){
                                foreach ($items['data'] as $key => $item) {
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    /*  */
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    /*  */
                                    $btn_doc = '';

                                    $multimedia_id = 0; // para cuando ya existe archivo subido

                                    if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                        // $siguiente_fase seguirá como true si el item ha sido exonerado
                                        
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-default';
                                        $btn_exo_icon = 'fa-ban';
                                        $btn_exo_title = 'Exonerado';
                                        /*  */
                                        $btn_doc = 'disabled';

                                    }else{ // items habilitados

                                        if(intval($item['tb_multimedia_est']) == 2){  // 0=inactivo, 1=sin archivo, 2=con archivo
                                                $action='editar';

                                                // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                                $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                                if($items_fase['estado']==1){
                                                    $estado_item = 'fa-circle text-green';
                                                    /*  */
                                                    $btn_exo = 'disabled';
                                                    $btn_exo_color = 'btn-success';
                                                    $btn_exo_icon = 'fa-check';
                                                    $btn_exo_title = 'Exonerar';
                                                    /*  */
                                                    $btn_doc = '';
                                                    /*  */
                                                    $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                                }
                                        }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                            $siguiente_fase = false;
                                        }

                                    }

                                    if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                        array_push($estados_items, 3);
                                    }else{
                                        array_push($estados_items, intval($item['tb_multimedia_est']));
                                    }

                            ?>
                                    <div id="item<?php echo $item['tb_proceso_fase_item_id']; ?>" class="col-md-12" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php echo $bloqueo_exo; ?>
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <?php } ?>
                                                <?php if($item['tb_item_idx'] == 'hoja_pre_calificacion'){ // solo mostrar botón en nivel uno para formato pre calificacion ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="hojaPrecalificacion('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                    <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="comentarioPreCalificacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Comentarios"><span class="fa fa-commenting"></span></span></button>
                                                <?php } ?>
                                                <?php if($item['tb_item_idx'] == 'form_ing_egr'){ // solo mostrar botón en nivel uno para formato de ingresos y gastos ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="ingresosEgresos('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                        /* if($siguiente_fase){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                                            $boton_precali = '';
                                            $color_boton_precali = 'btn-success';
                                        }else{
                                            $boton_precali = 'disabled';
                                            $color_boton_precali = 'btn-default';
                                        } */
                                        ?>
                                    </div>
                            <?php 
                                }
                            }
                            ?>
                        </div>

                <?php } elseif($proceso_fase_orden == 2){ // Orden de Fase ?>

                        <div class="col-md-12">
                            <?php 
                            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                            $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                            $estados_items = [];

                            if($items['estado']==1){
                                foreach ($items['data'] as $key => $item) {
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    /*  */
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    /*  */
                                    $btn_doc = '';

                                    $multimedia_id = 0; // para cuando ya existe archivo subido

                                    if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                        // $siguiente_fase seguirá como true si el item ha sido exonerado
                                        
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-default';
                                        $btn_exo_icon = 'fa-ban';
                                        $btn_exo_title = 'Exonerado';
                                        /*  */
                                        $btn_doc = 'disabled';

                                    }else{ // items habilitados

                                        if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                        }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                            $siguiente_fase = false;
                                        }

                                    }

                                    if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                        array_push($estados_items, 3);
                                    }else{
                                        array_push($estados_items, intval($item['tb_multimedia_est']));
                                    }
                            
                            ?>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php if($item['tb_item_idx']=='libro_negro' || $item['tb_item_idx']=='boleta_factura'){echo '';}else{echo $bloqueo_exo;} // Manejar las exoneraciones por parte del colaborador ?> 
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                                }
                            }
                            ?>
                        </div>

                        <!-- Boton de Bancarización -->
                        <div align="left" class="col-md-12" style="padding-top: 15px;">
                            <div class="form-group">
                                <button type="button" class="btn bg-maroon" onclick="elegirBancarizacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-money"></i> Bancarización</strong></button>
                                <button type="button" class="btn bg-purple" onclick="generarCheche(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-indent"></i> Generar Cheques</strong></button>
                                <button type="button" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id;?>, 'con_firma');"><strong><i class="fa fa-download"></i> Hoja Pre Calicación</strong></button>
                            </div>
                        </div>
                        <!--  -->

                        <!-- Votación de comité -->
                        <?php //if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 56){ // Solo mostrar al comité LOCAL ?>
                        <div class="col-md-12" style="padding-top: 15px;">
                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>
                            <h4><strong>COMITÉ: </strong></h4>
                        <?php } ?>

                            <table>
                                <tbody>
                                    <?php if($comite['estado'] == 1){ ?>
                                        <?php foreach($comite['data'] as $key => $com){ ?>
                                        <?php 
                                                $voto_miembro = $oProceso->obtenerVotoComite($proceso_id, $com['tb_usuario_id']); 
                                                $voto_comite = 0;
                                                if($voto_miembro['estado']==1){
                                                    $voto_comite = $voto_miembro['data']['tb_comitevoto_voto'];
                                                }
                                        ?>
                                            <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>

                                            <tr>
                                                <td style="padding: 10px;"><strong>- <?php echo $com['usuario'] ?></strong></td>
                                                <td style="padding: 10px;">
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                        id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_si" 
                                                        value="2" 
                                                        onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($voto_comite==2) echo 'checked' ?>
                                                    >
                                                        <strong> Procede</strong>
                                                </td>
                                                <td style="padding: 10px;">
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                        id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                        value="1" 
                                                        onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($voto_comite==1) echo 'checked' ?>
                                                    >
                                                        <strong> No procede</strong>
                                                </td>
                                                <td style="padding: 10px;">
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                        id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                        value="0" 
                                                        onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($voto_comite==0) echo 'checked' ?>
                                                    >
                                                        <strong> Sin voto</strong>
                                                </td>
                                            </tr>
                                            <?php } ?>

                                            <?php 
                                                // CONTEO DE VOTOS PARA FASE 2
                                                if($voto_miembro['data']['tb_comitevoto_voto']==2){ // Votos a favor
                                                    $suma_boton_precali = $suma_boton_precali + floatval($com['tb_comite_cred']);
                                                    $voto_comite_si++;
                                                }elseif($voto_miembro['data']['tb_comitevoto_voto']==1){ // Votos en contra
                                                    $voto_comite_no++;
                                                }elseif($voto_miembro['data']['tb_comitevoto_voto']==0){ // Aún no vota
                                                    $voto_comite_sin_voto++;
                                                }
                                            ?>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité 
                        
                                $proceso_otro = $oProceso->mostrarUno($proceso_id);
                                $firma_otro = 0;
                                if($proceso_otro['estado'] == 1){
                                    $firma_otro = intval($proceso_otro['data']['tb_proceso_usu_firma_otro']);
                                }
            
                            ?>
                                
                                <br>
                                <?php if($comite_otros['estado'] == 1){ ?>
                                    <h5><strong>OTROS: </strong></h5>
                                    <table>
                                        <tbody>

                                            <?php foreach($comite_otros['data'] as $key => $otro){ ?>
                                                <tr>
                                                    <td>
                                                        <input 
                                                            type="radio" 
                                                            name="voto_comite_otro" 
                                                            id="voto_comite_otro_<?php echo $otro['tb_usuario_id']; ?>" 
                                                            value="<?php echo $otro['tb_usuario_id']; ?>" 
                                                            onChange="votoComiteOtro(this.value, <?php echo $otro['tb_usuario_id']; ?>, <?php echo $proceso_id; ?>)" 
                                                            class="flat-red" <?php if($firma_otro==$otro['tb_usuario_id']) echo 'checked' ?>
                                                        >
                                                            <strong> <?php echo $otro['usuario'] ?></strong>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                <?php } ?>

                            <?php } ?>

                        </div>
                        
                <?php } elseif($proceso_fase_orden == 3){ // Orden de Fase ?>

                        <div class="col-md-12">
                            <?php 
                            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                            $estados_items = [];

                            if($items['estado']==1){
                                foreach ($items['data'] as $key => $item) {
                                if(intval($item['tb_item_id']) != 64 && intval($item['tb_item_id']) != 65){ // Se excluye CHEQUE GERENCIA o OTRO TIPO DE DESEMBOLSO se maneja de forma manual luego
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    /*  */
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    /*  */
                                    $btn_doc = '';

                                    $multimedia_id = 0; // para cuando ya existe archivo subido

                                    if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                        
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-default';
                                        $btn_exo_icon = 'fa-ban';
                                        $btn_exo_title = 'Exonerado';
                                        /*  */
                                        $btn_doc = 'disabled';

                                    }else{ // items habilitados

                                        if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                        }

                                    }

                                    if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                        array_push($estados_items, 3);
                                    }else{
                                        array_push($estados_items, intval($item['tb_multimedia_est']));
                                    }
                            
                            ?>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php echo $bloqueo_exo; ?>
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                                }
                                }
                            }
                            ?>
                        </div>

                        <div class="col-md-12 page-header" style="padding-top: 15px;"></div>

                        <?php if($proc_banca == 1){ //CHEQUES ?>
                        <!-- Desembolso Cheques -->
                        <div class="col-md-12" style="padding-top: 15px;">
                            <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <?php
                                        $action='nuevo';

                                        $estado_item = 'fa-circle-o text-red';
                                        $btn_exo = '';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        $btn_doc = '';
                                        $multimedia_id = 0; // para cuando ya existe archivo subido
                                        $proceso_fase_item_id = 0;

                                        $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 64);
                                        $proceso_fase_item_64 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 64);

                                        if($items_fase1['estado']==1){
                                            $action='editar';

                                            $estado_item = 'fa-circle text-green';
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            $btn_doc = '';
                                            $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                        }

                                        if($proceso_fase_item_64['estado']==1){
                                            $proceso_fase_item_id = intval($proceso_fase_item_64['data']['tb_proceso_fase_item_id']);
                                        }

                                    ?>
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> CHEQUE DE GERENCIA</label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php echo $bloqueo_exo; ?>
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_id; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>
                                        
                                        <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" title="Subir Cheque" onclick="subidaCheques(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $proceso_fase_item_id;?>);"><span class="fa fa-upload"></span></button>
                                        <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_id;?>, 'cheque')"><span class="fa fa-money"></span></button>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <?php }else{ ?>
                            <?php if($proc_banca == 0){ ?>
                                <h5 class="" style="color: red;"> <strong>Seleccione una opción de bancarización en la Fase 2.</strong></h5>
                            <?php }else{ ?>
                                <!-- Desembolso otro medio de pago -->
                                <div class="col-md-12">
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <?php
                                                $action='nuevo';

                                                $estado_item = 'fa-circle-o text-red';
                                                $btn_exo = '';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                $btn_doc = '';
                                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                                $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 65);
                                                $proceso_fase_item_65 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 65);

                                                if($items_fase1['estado']==1){
                                                    $action='editar';

                                                    $estado_item = 'fa-circle text-green';
                                                    $btn_exo = 'disabled';
                                                    $btn_exo_color = 'btn-success';
                                                    $btn_exo_icon = 'fa-check';
                                                    $btn_exo_title = 'Exonerar';
                                                    $btn_doc = '';
                                                    $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                                }

                                            ?>
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> EFECTIVO, TRANSFERENCIA O DEPÓSITO</label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php echo $bloqueo_exo; ?>
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                
                                                <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>, 'otro')"><span class="fa fa-money"></span> </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  -->
                            <?php } ?>
                        <?php } ?>

                <?php } else { ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];
                        $items_comprimidos = [];

                        $i = 0;
                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido
                                $multi_exo = 0;
                                $multi_url = '';

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';
                                    $multi_exo = 1;
                                    $multi_url = '...';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                        
                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                            $multi_url      = $items_fase['data']['tb_multimedia_url'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <!-- < ?php if($item['tb_item_idx'] == 'escritura_publica'){ // solo mostrar botón en nivel 4 para formulario de titulo ?>
                                                <button class="btn btn-info btn-sm" type="button" name="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="titulo_form(< ?php echo $credito_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            < ?php } ?> -->
                                        </div>
                                    </div>
                                </div>
                        <?php 

                                //añadir valores dde item al array
                                $valor['item_id']=$item['tb_item_id'];
                                $valor['item_des']=$item['tb_item_des'];
                                $valor['multi_exo']=$multi_exo;
                                $valor['multi_url']=$multi_url;
                                $items_comprimidos[$i]=$valor;
                                $i++;
                            }
                        }
                        $array = json_encode($items_comprimidos);
                        ?>

                    </div>

                    <br>
                    <input type="hidden" id="items_comprimidos" name="items_comprimidos[]" value='<?php echo json_encode($items_comprimidos); ?>'>
                    <div align="center">
                        <button type="button" class="btn btn-primary" id="" name="" onclick="comprimirArchivos(<?php echo $item['tb_proceso_id']; ?>)" /><strong><i class="fa fa-file-zip-o"></i> COMPRIMIR</strong></button>
                        <a id="downloadZip" name="downloadZip" href="" target="blank_"></a> <!-- Enlace que disparra la descarga del zip -->
                    </div>

                <?php } ?>

            </div>
        </div>

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 5){ // CREDITO GARVEH - Constitución Compraventa a tercero Con Custodia ?>

        <div class="row">
            <div class="col-md-12">
                
                <?php if($proceso_fase_orden == 1){ // Orden de Fase ?>
                        <div class="col-md-6">

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="cliente_nom" class="col-sm-4 control-label">CLIENTE</label>
                                    <div class="col-sm-6 input-group">
                                        <input type="hidden" id="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                        <input type="hidden" id="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                        <input type="hidden" id="hdd_proc_fase_id_temp" name="hdd_proc_fase_id_temp" value="<?php echo $proceso_fase_id ?>">
                                        <input type="hidden" id="cliente_dni_<?php echo $proceso_fase_id; ?>" name="cliente_dni_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_numdoc ?>">
                                        <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id ?>">
                                    
                                        <input type="text" class="form-control input-sm" name="cliente_nom_<?php echo $proceso_fase_id; ?>" id="cliente_nom_<?php echo $proceso_fase_id; ?>" placeholder="Ingrese Cliente" value="<?php echo $cliente_nom ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                                                <span class="fa fa-pencil icon"></span>
                                            </button>
                                        </span>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                                                <span class="fa fa-plus icon"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                
                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="cliente_celular" class="col-sm-4 control-label">CELULAR</label>
                                    <div class="col-sm-8"> 
                                        <input type="text" class="form-control input-sm" name="cliente_cel_<?php echo $proceso_fase_id; ?>" id="cliente_cel_<?php echo $proceso_fase_id; ?>" onkeypress="return solo_numero(event)" placeholder="Ingrese N° Celular" value="<?php echo $cliente_cel ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="inspeccion_vehiculo_const_sc" class="col-sm-4 control-label">INSPECCIÓN DE VEHÍCULO</label>
                                    <div class="col-sm-8"> 
                                        <div class="input-group date"> 
                                            <input type='text' class="form-control input-sm" name="inspeccion_vehiculo_const_sc" id="inspeccion_vehiculo_const_sc" value="<?php echo mostrar_fecha($fec_inspeccion); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="prueba_vehiculo_const_sc" class="col-sm-12 control-label">¿GERENTE PROBÓ EL VEHÍCULO?</label>
                                    <div class="col-sm-12"> 
                                        <label class="radio-inline" style="padding-left: 0px;">
                                            <input type="radio" name="prueba_vehiculo_const_sc" value="s" class="flat-green" <?php if($prueba_veh=='s') echo 'checked' ?>> SI
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="prueba_vehiculo_const_sc" value="n" class="flat-green" <?php if($prueba_veh=='n') echo 'checked' ?>> NO
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="prueba_vehiculo_const_sc" value="e" class="flat-green" <?php if($prueba_veh=='e') echo 'checked' ?>> EXONERADO
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label for="cliente_acepta_const_sc" class="col-sm-12 control-label">¿CLIENTE DE ACUERDO CON LO PRESENTADO?</label>
                                    <div class="col-sm-12"> 
                                        <label class="radio-inline" style="padding-left: 0px;">
                                            <input type="radio" name="cliente_acepta_const_sc" value="s" class="flat-green" <?php if($cli_acepta=='s') echo 'checked' ?>> SI
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="cliente_acepta_const_sc" value="n" class="flat-green" <?php if($cli_acepta=='n') echo 'checked' ?>> NO
                                        </label>
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 5px;"> 
                                        <textarea id="cliente_acepta_des_const_sc" name="cliente_acepta_des_const_sc" class="form-control input-sm" rows="3"; style="resize: none;" placeholder="Registre detalle de la decisión del cliente..."><?php echo $cli_acepta_det; ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- <div align="center" class="col-md-12" style="padding-bottom: 10px; padding-top: 5px;">
                                <button 
                                    class="btn btn-primary btn-sm " 
                                    type="button" 
                                    name="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                    id="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                    title="Guardar Datos" 
                                    onClick="guardar_consti_fase1(<?php echo $proceso_fase_id; ?>)">
                                    <span class="fa fa-save"></span> Guardar Cambios
                                </button>
                            </div> -->

                            <div class="col-md-12">
                                <section id="participantes" style="padding-top: 10px;">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class='col-md-3 h5'><label>Añadir Beneficiarios</label></div>
                                            <div class='col-md-9' id="rowPlusParticipante">
                                            <div class="input-group" id="buttonPlusParticipante">
                                                <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoParticipante').removeClass('hidden'); $('#rowPlusParticipante').addClass('hidden'); limpiarCamposParticipante();" data-toggle="tooltip" title="Añadir Ingreso"><li class="fa fa-plus"></li></button>
                                            </div>
                                            <br>
                                            </div>
                                        </div>

                                        <div class="col-md-12 hidden" id="divNuevoParticipante">
                                            <form name="frmNuevoParticipante" id="frmNuevoParticipante">
                                            <input type="hidden" name="action" value="registrar_participante">
                                            <input type="hidden" name="hdd_participante_proceso_id" id="hdd_participante_proceso_id" value="<?php echo $proceso_id; ?>">

                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                
                                                <div class='col-md-12'>	
                                                    <div class="input-group" >
                                                    <span class="input-group-addon"><strong>Nombres y Apellidos</strong><span class="text-red">*</span></span>
                                                    <input autocomplete="off" class="form-control input-sm" name="nombre_p" id="nombre_p" type="text" placeholder="Nombres y apellidos de beneficiario"/>
                                                    </div>
                                                    <br>
                                                </div>

                                                <div class='col-md-6'>	
                                                    <div class="input-group" >
                                                    <span class="input-group-addon"><strong>DNI</strong><span class="text-red">*</span></span>
                                                    <input autocomplete="off" class="form-control input-sm" name="documento_p" id="documento_p" type="text" onkeypress="return solo_numero(event)" placeholder="DNI de beneficiario"/>
                                                    </div>
                                                    <br>
                                                </div>

                                                <div class='col-md-6'>	
                                                    <div class="input-group" >
                                                    <span class="input-group-addon"><strong>Celular</strong></span>
                                                    <input autocomplete="off" class="form-control input-sm" name="celular_p" id="celular_p" type="text" onkeypress="return solo_numero(event)" placeholder="Celular de beneficiario"/>
                                                    </div>
                                                    <br>
                                                </div>

                                                </div>

                                                <div class="col-md-12" align="right" >
                                                <button class="btn btn-sm btn-primary" type="button" id="btnParticipante" onClick="guardarParticipanteDetalle('frmNuevoParticipante', 'Participante','I')"><li class="fa fa-save"></li> Guardar</button>
                                                <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoParticipante').addClass('hidden'); $('#frmNuevoParticipante').trigger('reset'); $('#rowPlusParticipante').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                                                </div>
                                            </div>
                                            </form>
                                            <br>
                                        </div>

                                        <div class="col-md-12" id="listaParticipante">

                                        </div>

                                    </div>
                                </section>
                            </div>
                        </div>

                        <div id="div_items_contenido<?php echo $proceso_fase_id; ?>" class="col-md-6">
                            <?php 
                            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                            $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                            $estados_items = [];

                            if($items['estado']==1){
                                foreach ($items['data'] as $key => $item) {
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    /*  */
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    /*  */
                                    $btn_doc = '';

                                    $multimedia_id = 0; // para cuando ya existe archivo subido

                                    if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                        // $siguiente_fase seguirá como true si el item ha sido exonerado
                                        
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-default';
                                        $btn_exo_icon = 'fa-ban';
                                        $btn_exo_title = 'Exonerado';
                                        /*  */
                                        $btn_doc = 'disabled';

                                    }else{ // items habilitados

                                        if(intval($item['tb_multimedia_est']) == 2){  // 0=inactivo, 1=sin archivo, 2=con archivo
                                                $action='editar';

                                                // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                                $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                                if($items_fase['estado']==1){
                                                    $estado_item = 'fa-circle text-green';
                                                    /*  */
                                                    $btn_exo = 'disabled';
                                                    $btn_exo_color = 'btn-success';
                                                    $btn_exo_icon = 'fa-check';
                                                    $btn_exo_title = 'Exonerar';
                                                    /*  */
                                                    $btn_doc = '';
                                                    /*  */
                                                    $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                                }
                                        }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                            $siguiente_fase = false;
                                        }

                                    }

                                    if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                        array_push($estados_items, 3);
                                    }else{
                                        array_push($estados_items, intval($item['tb_multimedia_est']));
                                    }

                            ?>
                                    <div id="item<?php echo $item['tb_proceso_fase_item_id']; ?>" class="col-md-12" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php echo $bloqueo_exo; ?>
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <?php } ?>
                                                <?php if($item['tb_item_idx'] == 'hoja_pre_calificacion'){ // solo mostrar botón en nivel uno para formato pre calificacion ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="hojaPrecalificacion('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                    <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="comentarioPreCalificacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Comentarios"><span class="fa fa-commenting"></span></span></button>
                                                <?php } ?>
                                                <?php if($item['tb_item_idx'] == 'form_ing_egr'){ // solo mostrar botón en nivel uno para formato de ingresos y gastos ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="ingresosEgresos('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                        /* if($siguiente_fase){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                                            $boton_precali = '';
                                            $color_boton_precali = 'btn-success';
                                        }else{
                                            $boton_precali = 'disabled';
                                            $color_boton_precali = 'btn-default';
                                        } */
                                        ?>
                                    </div>
                            <?php 
                                }
                            }
                            ?>
                        </div>

                <?php } elseif($proceso_fase_orden == 2){ // Orden de Fase ?>

                        <div class="col-md-12">
                            <?php 
                            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                            $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                            $estados_items = [];

                            if($items['estado']==1){
                                foreach ($items['data'] as $key => $item) {
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    /*  */
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    /*  */
                                    $btn_doc = '';

                                    $multimedia_id = 0; // para cuando ya existe archivo subido

                                    if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                        // $siguiente_fase seguirá como true si el item ha sido exonerado
                                        
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-default';
                                        $btn_exo_icon = 'fa-ban';
                                        $btn_exo_title = 'Exonerado';
                                        /*  */
                                        $btn_doc = 'disabled';

                                    }else{ // items habilitados

                                        if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                        }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                            $siguiente_fase = false;
                                        }

                                    }

                                    if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                        array_push($estados_items, 3);
                                    }else{
                                        array_push($estados_items, intval($item['tb_multimedia_est']));
                                    }
                            
                            ?>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php if($item['tb_item_idx']=='libro_negro' || $item['tb_item_idx']=='boleta_factura'){echo '';}else{echo $bloqueo_exo;} // Manejar las exoneraciones por parte del colaborador ?> 
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                                }
                            }
                            ?>
                        </div>

                        <!-- Boton de Bancarización -->
                        <div align="left" class="col-md-12" style="padding-top: 15px;">
                            <div class="form-group">
                                <button type="button" class="btn bg-maroon" onclick="elegirBancarizacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-money"></i> Bancarización</strong></button>
                                <button type="button" class="btn bg-purple" onclick="generarCheche(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-indent"></i> Generar Cheques</strong></button>
                                <button type="button" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id;?>, 'con_firma');"><strong><i class="fa fa-download"></i> Hoja Pre Calicación</strong></button>
                            </div>
                        </div>
                        <!--  -->

                        <!-- Votación de comité -->
                        <?php //if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 56){ // Solo mostrar al comité LOCAL ?>
                        <div class="col-md-12" style="padding-top: 15px;">
                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>
                            <h4><strong>COMITÉ: </strong></h4>
                        <?php } ?>

                            <table>
                                <tbody>
                                    <?php if($comite['estado'] == 1){ ?>
                                        <?php foreach($comite['data'] as $key => $com){ ?>
                                        <?php 
                                                $voto_miembro = $oProceso->obtenerVotoComite($proceso_id, $com['tb_usuario_id']); 
                                                $voto_comite = 0;
                                                if($voto_miembro['estado']==1){
                                                    $voto_comite = $voto_miembro['data']['tb_comitevoto_voto'];
                                                }
                                        ?>
                                            <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>

                                            <tr>
                                                <td style="padding: 10px;"><strong>- <?php echo $com['usuario'] ?></strong></td>
                                                <td style="padding: 10px;">
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                        id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_si" 
                                                        value="2" 
                                                        onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($voto_comite==2) echo 'checked' ?>
                                                    >
                                                        <strong> Procede</strong>
                                                </td>
                                                <td style="padding: 10px;">
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                        id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                        value="1" 
                                                        onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($voto_comite==1) echo 'checked' ?>
                                                    >
                                                        <strong> No procede</strong>
                                                </td>
                                                <td style="padding: 10px;">
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                        id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                        value="0" 
                                                        onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($voto_comite==0) echo 'checked' ?>
                                                    >
                                                        <strong> Sin voto</strong>
                                                </td>
                                            </tr>
                                            <?php } ?>

                                            <?php 
                                                // CONTEO DE VOTOS PARA FASE 2
                                                if($voto_miembro['data']['tb_comitevoto_voto']==2){ // Votos a favor
                                                    $suma_boton_precali = $suma_boton_precali + floatval($com['tb_comite_cred']);
                                                    $voto_comite_si++;
                                                }elseif($voto_miembro['data']['tb_comitevoto_voto']==1){ // Votos en contra
                                                    $voto_comite_no++;
                                                }elseif($voto_miembro['data']['tb_comitevoto_voto']==0){ // Aún no vota
                                                    $voto_comite_sin_voto++;
                                                }
                                            ?>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité 
                        
                                $proceso_otro = $oProceso->mostrarUno($proceso_id);
                                $firma_otro = 0;
                                if($proceso_otro['estado'] == 1){
                                    $firma_otro = intval($proceso_otro['data']['tb_proceso_usu_firma_otro']);
                                }
            
                            ?>
                                
                                <br>
                                <?php if($comite_otros['estado'] == 1){ ?>
                                    <h5><strong>OTROS: </strong></h5>
                                    <table>
                                        <tbody>

                                            <?php foreach($comite_otros['data'] as $key => $otro){ ?>
                                                <tr>
                                                    <td>
                                                        <input 
                                                            type="radio" 
                                                            name="voto_comite_otro" 
                                                            id="voto_comite_otro_<?php echo $otro['tb_usuario_id']; ?>" 
                                                            value="<?php echo $otro['tb_usuario_id']; ?>" 
                                                            onChange="votoComiteOtro(this.value, <?php echo $otro['tb_usuario_id']; ?>, <?php echo $proceso_id; ?>)" 
                                                            class="flat-red" <?php if($firma_otro==$otro['tb_usuario_id']) echo 'checked' ?>
                                                        >
                                                            <strong> <?php echo $otro['usuario'] ?></strong>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                <?php } ?>

                            <?php } ?>

                        </div>
                        
                <?php } elseif($proceso_fase_orden == 3){ // Orden de Fase ?>

                        <div class="col-md-12">
                            <?php 
                            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                            $estados_items = [];

                            if($items['estado']==1){
                                foreach ($items['data'] as $key => $item) {
                                if(intval($item['tb_item_id']) != 64 && intval($item['tb_item_id']) != 65){ // Se excluye CHEQUE GERENCIA o OTRO TIPO DE DESEMBOLSO se maneja de forma manual luego
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    /*  */
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    /*  */
                                    $btn_doc = '';

                                    $multimedia_id = 0; // para cuando ya existe archivo subido

                                    if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                        
                                        $estado_item = 'fa-circle text-green';
                                        /*  */
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-default';
                                        $btn_exo_icon = 'fa-ban';
                                        $btn_exo_title = 'Exonerado';
                                        /*  */
                                        $btn_doc = 'disabled';

                                    }else{ // items habilitados

                                        if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                        }

                                    }

                                    if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                        array_push($estados_items, 3);
                                    }else{
                                        array_push($estados_items, intval($item['tb_multimedia_est']));
                                    }
                            
                            ?>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php echo $bloqueo_exo; ?>
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                                }
                                }
                            }
                            ?>
                        </div>

                        <div class="col-md-12 page-header" style="padding-top: 15px;"></div>

                        <?php if($proc_banca == 1){ //CHEQUES ?>
                        <!-- Desembolso Cheques -->
                        <div class="col-md-12" style="padding-top: 15px;">
                            <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <?php
                                        $action='nuevo';

                                        $estado_item = 'fa-circle-o text-red';
                                        $btn_exo = '';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        $btn_doc = '';
                                        $multimedia_id = 0; // para cuando ya existe archivo subido
                                        $proceso_fase_item_id = 0;

                                        $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 64);
                                        $proceso_fase_item_64 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 64);

                                        if($items_fase1['estado']==1){
                                            $action='editar';

                                            $estado_item = 'fa-circle text-green';
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            $btn_doc = '';
                                            $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                        }

                                        if($proceso_fase_item_64['estado']==1){
                                            $proceso_fase_item_id = intval($proceso_fase_item_64['data']['tb_proceso_fase_item_id']);
                                        }

                                    ?>
                                    <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> CHEQUE DE GERENCIA</label>
                                    <div class="col-sm-4" style="padding: 0px;">
                                        <button 
                                            <?php echo $bloqueo_exo; ?>
                                            class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                            type="button" 
                                            name="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="<?php echo $btn_exo_title; ?>" 
                                            onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                            <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                        </button>
                                        <?php if($action == 'editar'){ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                title="Documento"
                                                onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php }else{ ?>
                                            <button 
                                                class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                type="button" 
                                                name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                                title="Documento"
                                                onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                                <span class="fa fa-file-pdf-o"></span>
                                            </button>
                                        <?php } ?>
                                        
                                        <button 
                                            class="btn btn-warning btn-sm" 
                                            type="button" 
                                            name="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_id; ?>, 'tb_proc_proceso_fase_item')">
                                            <span class="fa fa-history"></span>
                                        </button>
                                        
                                        <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" title="Subir Cheque" onclick="subidaCheques(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $proceso_fase_item_id;?>);"><span class="fa fa-upload"></span></button>
                                        <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_id;?>, 'cheque')"><span class="fa fa-money"></span></button>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <?php }else{ ?>
                            <?php if($proc_banca == 0){ ?>
                                <h5 class="" style="color: red;"> <strong>Seleccione una opción de bancarización en la Fase 2.</strong></h5>
                            <?php }else{ ?>
                                <!-- Desembolso otro medio de pago -->
                                <div class="col-md-12">
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <?php
                                                $action='nuevo';

                                                $estado_item = 'fa-circle-o text-red';
                                                $btn_exo = '';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                $btn_doc = '';
                                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                                $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 65);
                                                $proceso_fase_item_65 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 65);

                                                if($items_fase1['estado']==1){
                                                    $action='editar';

                                                    $estado_item = 'fa-circle text-green';
                                                    $btn_exo = 'disabled';
                                                    $btn_exo_color = 'btn-success';
                                                    $btn_exo_icon = 'fa-check';
                                                    $btn_exo_title = 'Exonerar';
                                                    $btn_doc = '';
                                                    $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                                }

                                            ?>
                                            <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> EFECTIVO, TRANSFERENCIA O DEPÓSITO</label>
                                            <div class="col-sm-4" style="padding: 0px;">
                                                <button 
                                                    <?php echo $bloqueo_exo; ?>
                                                    class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                    type="button" 
                                                    name="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="<?php echo $btn_exo_title; ?>" 
                                                    onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                    <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                                </button>
                                                <?php if($action == 'editar'){ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php }else{ ?>
                                                    <button 
                                                        class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                        type="button" 
                                                        name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                        title="Documento"
                                                        onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </button>
                                                <?php } ?>
                                                
                                                <button 
                                                    class="btn btn-warning btn-sm" 
                                                    type="button" 
                                                    name="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                    <span class="fa fa-history"></span>
                                                </button>
                                                
                                                <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>, 'otro')"><span class="fa fa-money"></span> </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  -->
                            <?php } ?>
                        <?php } ?>

                <?php } else { ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];
                        $items_comprimidos = [];

                        $i = 0;
                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido
                                $multi_exo = 0;
                                $multi_url = '';

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';
                                    $multi_exo = 1;
                                    $multi_url = '...';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                        
                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                            $multi_url      = $items_fase['data']['tb_multimedia_url'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <!-- < ?php if($item['tb_item_idx'] == 'escritura_publica'){ // solo mostrar botón en nivel 4 para formulario de titulo ?>
                                                <button class="btn btn-info btn-sm" type="button" name="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="< ?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="titulo_form(< ?php echo $credito_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            < ?php } ?> -->
                                        </div>
                                    </div>
                                </div>
                        <?php 

                                //añadir valores dde item al array
                                $valor['item_id']=$item['tb_item_id'];
                                $valor['item_des']=$item['tb_item_des'];
                                $valor['multi_exo']=$multi_exo;
                                $valor['multi_url']=$multi_url;
                                $items_comprimidos[$i]=$valor;
                                $i++;
                            }
                        }
                        $array = json_encode($items_comprimidos);
                        ?>

                    </div>

                    <br>
                    <input type="hidden" id="items_comprimidos" name="items_comprimidos[]" value='<?php echo json_encode($items_comprimidos); ?>'>
                    <div align="center">
                        <button type="button" class="btn btn-primary" id="" name="" onclick="comprimirArchivos(<?php echo $item['tb_proceso_id']; ?>)" /><strong><i class="fa fa-file-zip-o"></i> COMPRIMIR</strong></button>
                        <a id="downloadZip" name="downloadZip" href="" target="blank_"></a> <!-- Enlace que disparra la descarga del zip -->
                    </div>

                <?php } ?>

            </div>
        </div>

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 6){ // CREDITO GARVEH - Adenda ?>

        <div class="row">
            <div class="col-md-12">
                
                <?php if($proceso_fase_orden == 1){ // Orden de Fase ?>

                    <div class="col-md-6">

                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_nom" class="col-sm-4 control-label">CLIENTE</label>
                                <div class="col-sm-6 input-group">
                                    <input type="hidden" id="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                    <input type="hidden" id="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" name="hdd_proc_cliente_id_ori_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_id ?>">
                                    <input type="hidden" id="hdd_proc_fase_id_temp" name="hdd_proc_fase_id_temp" value="<?php echo $proceso_fase_id ?>">
                                    <input type="hidden" id="cliente_dni_<?php echo $proceso_fase_id; ?>" name="cliente_dni_<?php echo $proceso_fase_id; ?>" value="<?php echo $cliente_numdoc ?>">
                                    <input type="hidden" id="hdd_proceso_id" name="hdd_proceso_id" value="<?php echo $proceso_id ?>">
                                
                                    <input type="text" class="form-control input-sm" name="cliente_nom_<?php echo $proceso_fase_id; ?>" id="cliente_nom_<?php echo $proceso_fase_id; ?>" placeholder="Ingrese Cliente" value="<?php echo $cliente_nom ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                                            <span class="fa fa-pencil icon"></span>
                                        </button>
                                    </span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                                            <span class="fa fa-plus icon"></span>
                                        </button>
                                    </span>
                                </div>
                                <!-- <div class="col-sm-1" style="padding: 0px;">
                                    <button class="btn btn-primary btn-sm" type="button" name="btn_ficha_<?php echo $proceso_fase_id; ?>" id="btn_ficha_<?php echo $proceso_fase_id; ?>" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE"><span class="fa fa-user-plus"></span></button>
                                </div> -->
                            </div>
                        </div>
            
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <label for="cliente_celular" class="col-sm-4 control-label">CELULAR</label>
                                <div class="col-sm-8"> 
                                    <input type="text" class="form-control input-sm" name="cliente_cel_<?php echo $proceso_fase_id; ?>" id="cliente_cel_<?php echo $proceso_fase_id; ?>" onkeypress="return solo_numero(event)" placeholder="Ingrese N° Celular" value="<?php echo $cliente_cel ?>">
                                </div>
                            </div>
                        </div>

                        <!-- <div align="center" class="col-md-12" style="margin-bottom: 5px; padding-top: 10px;">
                            <button 
                                class="btn btn-primary btn-sm " 
                                type="button" 
                                name="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                id="form_proc_fase_<?php echo $proceso_fase_id; ?>" 
                                title="Guardar Datos" 
                                onClick="guardar_preconsti_fase1(<?php echo $proceso_fase_id; ?>)">
                                <span class="fa fa-save"></span> Guardar Cambios
                            </button>
                        </div> -->

                    </div>

                    <div id="div_items_contenido<?php echo $proceso_fase_id; ?>" class="col-md-6">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                        $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                    // $siguiente_fase seguirá como true si el item ha sido exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){  // 0=inactivo, 1=sin archivo, 2=con archivo
                                            $action='editar';

                                            // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                            $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 
                                            if($items_fase['estado']==1){
                                                $estado_item = 'fa-circle text-green';
                                                /*  */
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                /*  */
                                                $btn_doc = '';
                                                /*  */
                                                $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                            }
                                    }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                        $siguiente_fase = false;
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }

                        ?>
                                <div id="item<?php echo $item['tb_proceso_fase_item_id']; ?>" class="col-md-12" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <?php } ?>
                                            <?php if($item['tb_item_idx'] == 'hoja_pre_calificacion'){ // solo mostrar botón en nivel uno para formato pre calificacion ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="hojaPrecalificacion('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                                <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="comentarioPreCalificacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Comentarios"><span class="fa fa-commenting"></span></span></button>
                                            <?php } ?>
                                            <?php if($item['tb_item_idx'] == 'form_ing_egr'){ // solo mostrar botón en nivel uno para formato de ingresos y gastos ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" onclick="ingresosEgresos('reg',<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $item['tb_proceso_fase_item_id'];?>);" title="Formulario"><span class="glyphicon glyphicon-edit"></span></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                    /* if($siguiente_fase){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                                        $boton_precali = '';
                                        $color_boton_precali = 'btn-success';
                                    }else{
                                        $boton_precali = 'disabled';
                                        $color_boton_precali = 'btn-default';
                                    } */
                                    ?>
                                </div>
                        <?php 
                            }
                        }
                        ?>
                    </div>

                <?php } elseif($proceso_fase_orden == 2){ // Orden de Fase ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 

                        $siguiente_fase = true; // inicializo en true, en caso algún item falte contenido pasará a false
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado

                                    // $siguiente_fase seguirá como true si el item ha sido exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        // $siguiente_fase pasará a false mientras el item aún no tenga archivo subido o esté exonerado

                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                    }elseif(intval($item['tb_multimedia_est']) == 1 || intval($item['tb_multimedia_est']) == 0 || $item['tb_multimedia_est'] == null){
                                        $siguiente_fase = false;
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                            }
                        }
                        ?>
                    </div>

                    <!-- Boton de Bancarización -->
                    <div align="left" class="col-md-12" style="padding-top: 15px;">
                        <div class="form-group">
                            <button type="button" class="btn bg-maroon" onclick="elegirBancarizacion(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-money"></i> Bancarización</strong></button>
                            <button type="button" class="btn bg-purple" onclick="generarCheche(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-indent"></i> Generar Cheques</strong></button>
                            <button type="button" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id;?>, 'con_firma');"><strong><i class="fa fa-download"></i> Hoja Pre Calicación</strong></button>
                        </div>
                    </div>
                    <!--  -->

                    <!-- Votación de comité -->
                    <?php //if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 56){ // Solo mostrar al comité LOCAL ?>
                    <div class="col-md-12" style="padding-top: 15px;">
                    <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>
                        <h4><strong>COMITÉ: </strong></h4>
                    <?php } ?>

                        <table>
                            <tbody>
                                <?php if($comite['estado'] == 1){ ?>
                                    <?php foreach($comite['data'] as $key => $com){ ?>
                                    <?php 
                                            $voto_miembro = $oProceso->obtenerVotoComite($proceso_id, $com['tb_usuario_id']); 
                                            $voto_comite = 0;
                                            if($voto_miembro['estado']==1){
                                                $voto_comite = $voto_miembro['data']['tb_comitevoto_voto'];
                                            }
                                    ?>
                                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité ?>

                                        <tr>
                                            <td style="padding: 10px;"><strong>- <?php echo $com['usuario'] ?></strong></td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_si" 
                                                    value="2" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==2) echo 'checked' ?>
                                                >
                                                    <strong> Procede</strong>
                                            </td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                    value="1" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==1) echo 'checked' ?>
                                                >
                                                    <strong> No procede</strong>
                                            </td>
                                            <td style="padding: 10px;">
                                                <input 
                                                    type="radio" 
                                                    name="voto_comite_<?php echo $com['tb_usuario_id']; ?>" 
                                                    id="voto_comite_<?php echo $com['tb_usuario_id']; ?>_no" 
                                                    value="0" 
                                                    onChange="votoCom(this.name, this.value, <?php echo $com['tb_usuario_id']; ?>, <?php echo intval($usuario_id); ?>, <?php echo $proceso_id; ?>)" 
                                                    class="flat-red" <?php if($voto_comite==0) echo 'checked' ?>
                                                >
                                                    <strong> Sin voto</strong>
                                            </td>
                                        </tr>
                                        <?php } ?>

                                        <?php 
                                            // CONTEO DE VOTOS PARA FASE 2
                                            if($voto_miembro['data']['tb_comitevoto_voto']==2){ // Votos a favor
                                                $suma_boton_precali = $suma_boton_precali + floatval($com['tb_comite_cred']);
                                                $voto_comite_si++;
                                            }elseif($voto_miembro['data']['tb_comitevoto_voto']==1){ // Votos en contra
                                                $voto_comite_no++;
                                            }elseif($voto_miembro['data']['tb_comitevoto_voto']==0){ // Aún no vota
                                                $voto_comite_sin_voto++;
                                            }
                                        ?>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                                            
                        <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61){ // Solo mostrar al comité 
                            
                            $proceso_otro = $oProceso->mostrarUno($proceso_id);
                            $firma_otro = 0;
                            if($proceso_otro['estado'] == 1){
                                $firma_otro = intval($proceso_otro['data']['tb_proceso_usu_firma_otro']);
                            }
        
                        ?>
                            
                            <br>
                            <?php if($comite_otros['estado'] == 1){ ?>
                                <h5><strong>OTROS: </strong></h5>
                                <table>
                                    <tbody>

                                        <?php foreach($comite_otros['data'] as $key => $otro){ ?>
                                            <tr>
                                                <td>
                                                    <input 
                                                        type="radio" 
                                                        name="voto_comite_otro" 
                                                        id="voto_comite_otro_<?php echo $otro['tb_usuario_id']; ?>" 
                                                        value="<?php echo $otro['tb_usuario_id']; ?>" 
                                                        onChange="votoComiteOtro(this.value, <?php echo $otro['tb_usuario_id']; ?>, <?php echo $proceso_id; ?>)" 
                                                        class="flat-red" <?php if($firma_otro==$otro['tb_usuario_id']) echo 'checked' ?>
                                                    >
                                                        <strong> <?php echo $otro['usuario'] ?></strong>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            <?php } ?>

                        <?php } ?>

                    </div>
                    <?php 
                    
                    /* if($suma_boton_precali >= $puntos_comite){ // si la suma de "procede" es igual o mayor a lo solicitado por el sistema
                        $boton_precali = '';
                        $boton_banca = '';
                        $color_boton_precali = 'btn-success';
                        $color_boton_banca = 'btn-success';
                    } */
                    ?>
                    <?php //} ?>
                    <!--  -->

                <?php } else { // Orden de Fase ?>

                    <div class="col-md-12">
                        <?php 
                        $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase_id); 
                        $estados_items = [];

                        if($items['estado']==1){
                            foreach ($items['data'] as $key => $item) {
                            if(intval($item['tb_item_id']) != 64 && intval($item['tb_item_id']) != 65){ // Se excluye CHEQUE GERENCIA o OTRO TIPO DE DESEMBOLSO se maneja de forma manual luego
                                $action='nuevo';

                                $estado_item = 'fa-circle-o text-red';
                                /*  */
                                $btn_exo = '';
                                $btn_exo_color = 'btn-success';
                                $btn_exo_icon = 'fa-check';
                                $btn_exo_title = 'Exonerar';
                                /*  */
                                $btn_doc = '';

                                $multimedia_id = 0; // para cuando ya existe archivo subido

                                if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                                    $estado_item = 'fa-circle text-green';
                                    /*  */
                                    $btn_exo = 'disabled';
                                    $btn_exo_color = 'btn-default';
                                    $btn_exo_icon = 'fa-ban';
                                    $btn_exo_title = 'Exonerado';
                                    /*  */
                                    $btn_doc = 'disabled';

                                }else{ // items habilitados

                                    if(intval($item['tb_multimedia_est']) == 2){ // 0=inactivo, 1=sin archivo, 2=con archivo
                                        $action='editar';

                                        //$items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_fase_item_id']); 
                                        $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                        if($items_fase['estado']==1){
                                            $estado_item = 'fa-circle text-green';
                                            /*  */
                                            $btn_exo = 'disabled';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            /*  */
                                            $btn_doc = '';
                                            /*  */
                                            $multimedia_id = $items_fase['data']['tb_multimedia_id'];
                                        }
                                    }

                                }

                                if($item['tb_multimedia_exo'] == 1){ // armar array de estados con exonerados se usará el estado 3 
                                    array_push($estados_items, 3);
                                }else{
                                    array_push($estados_items, intval($item['tb_multimedia_est']));
                                }
                        
                        ?>
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> <?php echo $item['tb_item_des']; ?></label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $item['tb_proceso_fase_item_id']; ?>, '<?php echo $item['tb_item_des']; ?>', '<?php echo $item['tb_item_idx']; ?>', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $item['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            <?php if($item['tb_item_idx'] == 'simulacion'){ // solo mostrar botón en nivel uno para formulario de simulación ?>
                                                <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_'.$item['tb_item_idx'].''.$item['tb_proceso_fase_item_id'];?>" title="Ver" onclick="generarSimulador(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><span class="fa fa-pencil-square-o"></span></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                            }
                            }
                        }
                        ?>
                    </div>

                    <div class="col-md-12 page-header" style="padding-top: 15px;"></div>

                    <?php if($proc_banca == 1){ //CHEQUES ?>
                    <!-- Desembolso Cheques -->
                    <div class="col-md-12" style="padding-top: 15px;">
                        <div class="col-md-6" style="margin-bottom: 5px;">
                            <div class="form-group">
                                <?php                                
                                    $action='nuevo';

                                    $estado_item = 'fa-circle-o text-red';
                                    $btn_exo = '';
                                    $btn_exo_color = 'btn-success';
                                    $btn_exo_icon = 'fa-check';
                                    $btn_exo_title = 'Exonerar';
                                    $btn_doc = '';
                                    $multimedia_id = 0; // para cuando ya existe archivo subido
                                    $proceso_fase_item_id = 0;

                                    $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 64);
                                    $proceso_fase_item_64 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 64);

                                    if($items_fase1['estado']==1){
                                        $action='editar';

                                        $estado_item = 'fa-circle text-green';
                                        $btn_exo = 'disabled';
                                        $btn_exo_color = 'btn-success';
                                        $btn_exo_icon = 'fa-check';
                                        $btn_exo_title = 'Exonerar';
                                        $btn_doc = '';
                                        $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                    }

                                    if($proceso_fase_item_64['estado']==1){
                                        $proceso_fase_item_id = intval($proceso_fase_item_64['data']['tb_proceso_fase_item_id']);
                                    }

                                ?>
                                <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> CHEQUE DE GERENCIA</label>
                                <div class="col-sm-4" style="padding: 0px;">
                                    <button 
                                        <?php echo $bloqueo_exo; ?>
                                        class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                        type="button" 
                                        name="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'exo_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="<?php echo $btn_exo_title; ?>" 
                                        onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                        <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                    </button>
                                    <?php if($action == 'editar'){ ?>
                                        <button 
                                            class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                            type="button" 
                                            name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Documento"
                                            onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                            <span class="fa fa-file-pdf-o"></span>
                                        </button>
                                    <?php }else{ ?>
                                        <button 
                                            class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                            type="button" 
                                            name="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            id="<?php echo 'doc_subir_cheque'.$proceso_fase_item_id;?>" 
                                            title="Documento"
                                            onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_id; ?>, 'CHEQUE DE GERENCIA', 'subir_cheque', '<?php echo $action; ?>')">
                                            <span class="fa fa-file-pdf-o"></span>
                                        </button>
                                    <?php } ?>
                                    
                                    <button 
                                        class="btn btn-warning btn-sm" 
                                        type="button" 
                                        name="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                        id="<?php echo 'his_subir_cheque'.$proceso_fase_item_id;?>" 
                                        title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_id; ?>, 'tb_proc_proceso_fase_item')">
                                        <span class="fa fa-history"></span>
                                    </button>
                                    
                                    <button class="btn btn-info btn-sm" type="button" name="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'form_subir_cheque'.$proceso_fase_item_id;?>" title="Subir Cheque" onclick="subidaCheques(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, <?php echo $proceso_fase_item_id;?>);"><span class="fa fa-upload"></span></button>
                                    <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" id="<?php echo 'des_subir_cheque'.$proceso_fase_item_id;?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_id;?>, 'cheque')"><span class="fa fa-money"></span></button>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <?php }else{ ?>
                        <?php if($proc_banca == 0){ ?>
                            <h5 class="" style="color: red;"> <strong>Seleccione una opción de bancarización en la Fase 2.</strong></h5>
                        <?php }else{ ?>
                            <!-- Desembolso otro medio de pago -->
                            <div class="col-md-12">
                                <div class="col-md-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <?php
                                            $action='nuevo';

                                            $estado_item = 'fa-circle-o text-red';
                                            $btn_exo = '';
                                            $btn_exo_color = 'btn-success';
                                            $btn_exo_icon = 'fa-check';
                                            $btn_exo_title = 'Exonerar';
                                            $btn_doc = '';
                                            $multimedia_id = 0; // para cuando ya existe archivo subido

                                            $items_fase1 = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 65);
                                            $proceso_fase_item_65 =  $oProceso->obtenerProcesoFaseItemXItem($proceso_fase_id, 65);

                                            if($items_fase1['estado']==1){
                                                $action='editar';

                                                $estado_item = 'fa-circle text-green';
                                                $btn_exo = 'disabled';
                                                $btn_exo_color = 'btn-success';
                                                $btn_exo_icon = 'fa-check';
                                                $btn_exo_title = 'Exonerar';
                                                $btn_doc = '';
                                                $multimedia_id = $items_fase1['data']['tb_multimedia_id'];
                                            }

                                        ?>
                                        <label for="" class="col-sm-8 control-label"><span class="fa <?php echo $estado_item; ?>" style="font-size: 15px;"></span> EFECTIVO, TRANSFERENCIA O DEPÓSITO</label>
                                        <div class="col-sm-4" style="padding: 0px;">
                                            <button 
                                                <?php echo $bloqueo_exo; ?>
                                                class="btn <?php echo $btn_exo_color; ?> btn-sm" <?php echo $btn_exo; ?> 
                                                type="button" 
                                                name="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'exo_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="<?php echo $btn_exo_title; ?>" 
                                                onClick="exonerar_multimedia_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                <span class="fa <?php echo $btn_exo_icon; ?>"></span>
                                            </button>
                                            <?php if($action == 'editar'){ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="ver_subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>', <?php echo $multimedia_id; ?>)">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php }else{ ?>
                                                <button 
                                                    class="btn btn-primary btn-sm" <?php echo $btn_doc; ?> 
                                                    type="button" 
                                                    name="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    id="<?php echo 'doc_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                    title="Documento"
                                                    onClick="subida_doc_form(<?php echo $proceso_fase_id; ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'EFECTIVO, TRANSFERENCIA O DEPÓSITO', 'otro_desembolso', '<?php echo $action; ?>')">
                                                    <span class="fa fa-file-pdf-o"></span>
                                                </button>
                                            <?php } ?>
                                            
                                            <button 
                                                class="btn btn-warning btn-sm" 
                                                type="button" 
                                                name="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                id="<?php echo 'his_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" 
                                                title="Historial y Comentarios"  onclick="hist_doc_form(<?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id']; ?>, 'tb_proc_proceso_fase_item')">
                                                <span class="fa fa-history"></span>
                                            </button>
                                            
                                            <button class="btn bg-purple btn-sm" type="button" name="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" id="<?php echo 'form_otro_desembolso'.$proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>" title="Desembolsar" onclick="credito_desembolso_form('insertar',<?php echo $credito_id ?>, <?php echo $proceso_fase_item_65['data']['tb_proceso_fase_item_id'];?>, 'otro')"><span class="fa fa-money"></span> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        <?php 
                            //añadir valores dde item al array
                            $valor['item_id']=$item['tb_item_id'];
                            $valor['item_des']=$item['tb_item_des'];
                            $valor['multi_exo']=$multi_exo;
                            $valor['multi_url']=$multi_url;
                            $items_comprimidos[$i]=$valor;
                            $i++;
                        } ?>
                    <?php } ?>

                    <br>
                    <input type="hidden" id="items_comprimidos" name="items_comprimidos[]" value='<?php echo json_encode($items_comprimidos); ?>'>
                    <div align="center">
                        <button type="button" class="btn btn-primary" id="" name="" onclick="comprimirArchivos(<?php echo $item['tb_proceso_id']; ?>)" /><strong><i class="fa fa-file-zip-o"></i> COMPRIMIR</strong></button>
                        <a id="downloadZip" name="downloadZip" href="" target="blank_"></a> <!-- Enlace que disparra la descarga del zip -->
                    </div>

                <?php } ?>
                
            </div>
        </div>

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 7){ // CREDITO GARVEH - IPDN Venta ?>

        <div class="row">
            <div class="col-md-12">
                CREDITO GARVEH - IPDN Venta 
            </div>
        </div>

    <?php }elseif($proceso['data']['tb_cgarvtipo_id'] == 8){ // CREDITO GARVEH - IPDN Venta con RD ?>

        <div class="row">
            <div class="col-md-12">
                CREDITO GARVEH - IPDN Venta con RD
            </div>
        </div>

    <?php } ?>


<?php }elseif($proceso['data']['tb_creditotipo_id'] == 4){ // CREDITO HIPOTECARIO ?>

    <div class="row">
        <div class="col-md-12">
            CREDITO HIPOTECARIO
        </div>
    </div>

<?php }elseif($proceso['data']['tb_creditotipo_id'] == 5){ // ADENDA ?>

    <div class="row">
        <div class="col-md-12">
        ADENDA
        </div>
    </div>

<?php } ?>

<script>
    $('#fecha_inspeccion, #fecha_deposito, #inspeccion_vehiculo_const_sc').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
    });

    $('#cliente_nom_'+$('#hdd_proc_fase_id_temp').val()).change(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $("#cliente_nom_"+$('#hdd_proc_fase_id_temp').val()).autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            var proceso_fase_id = $('#hdd_proc_fase_id_temp').val();
            $('#hdd_proc_cliente_id_'+proceso_fase_id).val(ui.item.cliente_id);
            $("#cliente_nom_"+proceso_fase_id).val(ui.item.cliente_nom);
            $('#cliente_cel_'+proceso_fase_id).val(ui.item.cliente_cel);
            event.preventDefault();
        }
    });

    $('input[type="radio"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

</script>