<?php
	session_name("ipdnsac");
  session_start();
	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();

  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");

  $proceso_id = intval($_POST['proceso_id']);
  $tipo = $_POST['tipo'];

  $desembolsos = $oProceso->mostrarBancarizacionXProceso($proceso_id, $tipo);

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_otro_retiro" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <!-- <input type="hidden" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>"> -->

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Detalle Movimientos de Desembolso</h4>
      </div>

      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <?php if($desembolsos['estado'] == 1){ ?>
              <table id="tabla_inventario" class="table table-hover">
                <thead>
                    <tr id="tabla_cabecera">
                        <th id="tabla_cabecera_fila">#</th>
                        <th id="tabla_cabecera_fila">FECHA DESEMBOLSO</th>
                        <th id="tabla_cabecera_fila">DOCUMENTO</th>
                        <th id="tabla_cabecera_fila">USUARIO</th>
                        <th id="tabla_cabecera_fila">MONTO</th>
                        <th id="tabla_cabecera_fila">SEDE</th>
                        <!-- <th  id="tabla_cabecera_fila" width="12%">ACCIONES</th> -->
                    </tr>
                </thead>  
                <tbody>
                <?php 
                  $num = 1;
                  foreach ($desembolsos['data'] as $key => $dt) {
                    $egreso = $oProceso->mostrarEgresoXCheque(intval($dt['tb_egreso_id']));

                    $fecha = '';
                    $usuario_reg = '';
                    $monto = 0.00;
                    $empresa = "";
                    $moneda = "";

                    if($egreso['estado'] == 1){
                      $usuario = $oUsuario->mostrarUno($egreso['data']['tb_egreso_usureg']);

                      $fecha = mostrar_fecha($egreso['data']['tb_egreso_fec']);
                      $usuario_reg = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
                      $monto = floatval($egreso['data']['tb_egreso_imp']);
                      if ($egreso['data']['tb_empresa_id'] == 1)
                          $empresa = "SEDE BOULEVARD";
                      if ($egreso['data']['tb_empresa_id'] == 2)
                          $empresa = "SEDE MALL";
                      if ($egreso['data']['tb_moneda_id'] == 1)
                          $moneda = "S/ ";
                      if ($egreso['data']['tb_moneda_id'] == 2)
                          $moneda = "US$ ";

                    }
                ?>
                    <tr id="tabla_cabecera_fila">
                      <td id="tabla_fila"><?php echo $num; ?></td>
                      <td id="tabla_fila"><?php echo $fecha ?></td>
                      <td id="tabla_fila"><a style="color: blue;" title="Imprimir" href="javascript:void(0)" onClick="documento_desembolso('<?php echo $egreso['data']['tb_egreso_id'] ?>','<?php echo $_SESSION['empresa_id'] ?>')"><?php echo $egreso['data']['tb_documento_abr'] . ' ' . $egreso['data']['tb_egreso_numdoc'] ?></a></td>
                      <td id="tabla_fila"><?php echo $usuario_reg ?></td>
                      <td id="tabla_fila"><?php echo $moneda.''.number_format($monto, 2) ?></td>
                      <td id="tabla_fila"><?php echo $empresa ?></td>
                    </tr>
                <?php 
                    $num++;
                  }
                ?>
                </tbody>
              </table>
            <?php 
            }else{ 
              echo "<div class='col-md-12'>";
              echo "<h4>".$desembolsos['mensaje']."</h4>";
              echo "</div>";
            } ?>
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <!-- <button type="button" id="btn_guardar_cheque_retiro" class="btn btn-info" onclick="registrarBanca()">Guardar</button> -->
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>