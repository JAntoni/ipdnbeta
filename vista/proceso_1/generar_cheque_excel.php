<?php
//echo 'hasta aki estoy entrando normal '; exit();
//require_once('../../core/usuario_sesion.php');
//error_reporting(0);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../transferente/Transferente.class.php');
$oTransferente = new Transferente();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$proceso_id = intval($_GET['proceso_id']);
$usuario_id = intval($_GET['usuario_id']);

$proceso = $oProceso->mostrarUno($proceso_id);
$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

$banca = 0;
$tipo_garantia = '';
$tipo_garantia_id = 0;
$cliente_doc = 0;
$cliente_nom = '';

if($proceso['estado']==1){
  $banca = intval($proceso['data']['tb_proceso_banca']);
  $tipo_garantia_id = intval($proceso['data']['tb_cgarvtipo_id']);
  $garvtipo = $oProceso->mostrarUnoGarvtipo($proceso['data']['tb_cgarvtipo_id']);
  if($garvtipo['estado']==1){
    $tipo_garantia = $garvtipo['data']['cgarvtipo_nombre'];
  }
  $cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
  if($cliente['estado']==1){
    $cliente_doc = $cliente['data']['tb_cliente_doc'];
    $cliente_nom = $cliente['data']['tb_cliente_nom'];
  }
}

$cheque_c = $oProceso->mostrarCabeceraChequeXProceso($proceso_id);
$cheques = $oProceso->mostrarChequeXProceso($proceso_id);
$fecha = '';
$detalle = '';
if( $cheque_c['estado'] == 1){
  $fecha = date("d/m/Y, h:i a", strtotime($cheque_c['data']['tb_cheque_fecreg']));
  $detalle = $cheque_c['data']['tb_cheque_det'];
}else{
  $fecha = date("d/m/Y, h:i a");
}


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("contacto@prestamosdelnortechiclayo.com")
	->setLastModifiedBy("contacto@prestamosdelnortechiclayo.com")
	->setTitle("CRONOGRAMA DE CRÉDITO ACVEH")
	->setSubject("Cronograma")
	->setDescription("Reporte generado por contacto@prestamosdelnortechiclayo.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
    'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  => 8,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '279944')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);
$estiloTituloFilas2 = array(
    'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '135825')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas4 = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);


  $nombre_archivo = "CHEQUES_".$cliente_doc;

  $i = 1;
  $c = 1;

  if($cheques['estado']==1){
    $cant_cheques = count($cheques['data']);

    if($tipo_garantia_id == 1){ // PRE CONSTITUCION

      $transferente_id = 0;
      $transferente_nom = '';
      $transferente_doc = '';
      if($precalificacion['estado']==1){
        $transferente_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
        $transferente = $oTransferente->mostrarUno($transferente_id);
        if($transferente['estado']==1){
          $transferente_nom = $transferente['data']['tb_transferente_nom'];
          $transferente_doc = $transferente['data']['tb_transferente_doc'];
        }
      } 

      $beneficiarios = $transferente_nom.' / '.$transferente_doc;
      
    }elseif($tipo_garantia_id == 2 || $tipo_garantia_id == 3 || $tipo_garantia_id == 6){ // CONSTITUCIÓN SIN Y CON CUSTODIA O ADENDA

      $beneficiarios = $cliente_nom.' / '.$cliente_doc;
      $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
      if($participantes['estado']==1){
        foreach ($participantes['data'] as $key => $p) {
          $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'].' / '.$p['tb_participante_doc'];
        }
      }

    }elseif($tipo_garantia_id == 4 || $tipo_garantia_id == 5) { // TERCERO SIN Y CON CUSTODIA

      $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
      $beneficiarios = '';
      if($participantes['estado']==1){
        foreach ($participantes['data'] as $key => $p) {
          if($key == 0){
            $beneficiarios .= $p['tb_participante_nom'].' / '.$p['tb_participante_doc'];
          }else{
            $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'].' / '.$p['tb_participante_doc'];
          }
        }
      }

    }

    foreach ($cheques['data'] as $key => $value) { 
      if($i==1){ 
        $moneda = "";
        $beneficiado = $value['tb_chequedetalle_bene'];
        $fecha = date("j-m-y, h:i a", strtotime($value['tb_cheque_fecreg']));
        if($value['tb_cheque_moneda']=='sol'){
          $moneda = "S/ ";
        }else{
          $moneda = "US$ ";
        }
      }
      $i++;
    }

    $titulo = "CHEQUES A GENERAR";
    $objPHPExcel->getActiveSheet()->mergeCells("A$c:G$c");
    $objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
    $objPHPExcel->getActiveSheet()->getStyle("A$c:G$c")->applyFromArray($estiloTituloFilas2);
    $objPHPExcel->getActiveSheet()->mergeCells("H$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("H$c", $fecha);
    $objPHPExcel->getActiveSheet()->getStyle("H$c:I$c")->applyFromArray($estiloTituloFilas2);

    $c = 2;

    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'BENEFICIARIOS');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $beneficiarios);
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

    $c = 3;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'ORDENANTE');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", '20600752872');
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

    $c = 4;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'N° CHEQUES');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $cant_cheques);
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

    $c = 5;

    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'MONEDA');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $moneda);
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

    $c = 6;
    foreach ($cheques['data'] as $key => $value2) { 
      $tipo = '';
      if($value2['tb_chequedetalle_tipo']=='nego'){
        $tipo = 'NEGOCIABLE';
      }else{
        $tipo = 'NO NEGOCIABLE';
      }
      
      $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'MONTO '.($key+1));
      $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

      $objPHPExcel->getActiveSheet()->mergeCells("B$c:G$c");
      $objPHPExcel->getActiveSheet()->setCellValue("B$c", $moneda.''.number_format($value2['tb_chequedetalle_monto'], 2));
      $objPHPExcel->getActiveSheet()->getStyle("B$c:G$c")->applyFromArray($estiloTituloFilas);

      $objPHPExcel->getActiveSheet()->mergeCells("H$c:I$c");
      $objPHPExcel->getActiveSheet()->setCellValue("H$c", $tipo);
      $objPHPExcel->getActiveSheet()->getStyle("H$c:I$c")->applyFromArray($estiloTituloFilas4);

      $c++;
    }


    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'OPERACIÓN');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $tipo_garantia);
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

    $c++;

    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'CLIENTE');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $cliente_nom);
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

    $c++;

    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'DETALLE');
    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);

    $objPHPExcel->getActiveSheet()->mergeCells("B$c:I$c");
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $detalle);
    $objPHPExcel->getActiveSheet()->getStyle("B$c:I$c")->applyFromArray($estiloTituloFilas);

  }else{
    $objPHPExcel->getActiveSheet()->mergeCells("A$c:G$c");
    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'NO SE PUDO EXPORTAR LOS CHEQUES');
    $objPHPExcel->getActiveSheet()->getStyle("A$c:G$c")->applyFromArray($estiloTituloFilas4);
  }
  

  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
  

  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('CHEQUES '.$cliente_doc);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

header("Content-type: text/xls");
header("Content-Disposition: attachment; filename=".$nombre_archivo.".xls");
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
