<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../proceso/Proceso.class.php');
  	$oProceso = new Proceso();
	require_once ('../historial/Historial.class.php');
	$oHistorial = new Historial();
	require_once('../comentario/Comentario.class.php');
	$oComentario = new Comentario();
	require_once('../cliente/Cliente.class.php');
	$oCliente = new Cliente();
	require_once('../transferente/Transferente.class.php');
	$oTransferente = new Transferente();
	require_once('../usuario/Usuario.class.php');
  	$oUsuario = new Usuario();
	require_once('../precredito/Precredito.class.php');
	$oPrecred = new Precredito();
	require_once('../precliente/Precliente.class.php');
	$oPrecliente = new Precliente();
	require_once('../ingreso/Ingreso.class.php');
	$oIngreso = new Ingreso();
	require_once('../empresa/Empresa.class.php');
	$oEmpresa = new Empresa();
	require_once('../cliente/Cliente.class.php');
	$oCliente = new Cliente();

	require_once ("../funciones/funciones.php");
	require_once ("../funciones/fechas.php");

	$fecha_hoy = date('d-m-Y h:i a');

 	$action = $_POST['action'];

 	if($action == 'insertar'){
        $usuario_id = $_SESSION['usuario_id']; 
 		$creditotipo_id = intval($_POST['creditotipo_id']);
 		$cgarvtipo_id = intval($_POST['cgarvtipo_id']);
 		$credito_matriz_id = intval($_POST['credito_matriz_id']);
 		$precliente_id = 0;
		if(isset($_POST['precliente_id'])){
			$precliente_id = intval($_POST['precliente_id']);
		}
		$precred_id = 0;
		if(isset($_POST['precred_id'])){
			$precred_id = intval($_POST['precred_id']);
		}

		$bancarizacion = 0;
		if($cgarvtipo_id==1) {
			$bancarizacion = 1;
		}

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Proceso.';

		$res= $oProceso->insertar($usuario_id, $creditotipo_id, $cgarvtipo_id, $precliente_id);

 		if($res['estado'] == 1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proceso registrado correctamente.';
			$data['proceso'] = $oProceso->mostrarUno($res['proceso_id'])['data']; //ultimo registro
			$data['proceso_id'] = $res['proceso_id']; //ultimo id de registro
			
			if($cgarvtipo_id==6){ // ADENDA
				$oProceso->modificar_campo_proceso($res['proceso_id'], 'tb_creditomatriz_id', $credito_matriz_id, 'INT');
			}

			/* gerson (15-06-24) */
			if($creditotipo_id==3){ // credito garveh
				if($precred_id > 0){ // si existe precred_id
					$oPrecred->modificar_campo($precred_id, 'tb_proceso_id', $res['proceso_id'], 'INT');
				}
			}
			if($precliente_id > 0){
				$precli = $oPrecliente->mostrarUno($precliente_id);
				if($precli['estado'] == 1){
					if(intval($precli['data']['tb_cliente_id']) > 0){
						$oProceso->modificar_campo_proceso($res['proceso_id'], 'tb_cliente_id', intval($precli['data']['tb_cliente_id']), 'INT');
					}
				}

			}
			/*  */

			// Obtener fases para el tipo de crédito del proceso
			$fases = $oProceso->obtenerFases(intval($data['proceso']['tb_creditotipo_id']), intval($data['proceso']['tb_cgarvtipo_id']));
			if($fases['estado'] == 1){
				foreach ($fases['data'] as $key => $fase) {
					if($key==0){
						$res2= $oProceso->insertarProcesoFase($res['proceso_id'], $fase['tb_fase_id'], $fase['tb_fase_orden'], 1); // la primera fase siempre será 1 y aparecerá activo en la vista de fases del proceso
					}elseif($key==1){// nivel 2 se agrega votos comite
						
						$res2= $oProceso->insertarProcesoFase($res['proceso_id'], $fase['tb_fase_id'], $fase['tb_fase_orden'], 0);

						$comite = $oProceso->listar_comite();
						if($comite['estado']==1){
							foreach ($comite['data'] as $key => $value) {
								$res3= $oProceso->insertarComiteVoto($res['proceso_id'], $value['tb_usuario_id']);
							}
						}
					}else{
						$res2= $oProceso->insertarProcesoFase($res['proceso_id'], $fase['tb_fase_id'], $fase['tb_fase_orden'], 0);
					}

					// Por cada fase se registrará los registros de subida de archivos
					
					/** GARVEH: PRE-CONSTITUCION */
					// fase 1 : foto_atencion,simulacion,proforma_vehiculo,coti_seg_vehicular,hoja_pre_calificacion,form_ing_egr
					// fase 2 : dni_cliente,ficha_ruc,c4_cliente,vigencia_poder,licencia_conducir,rec_servicio,boleta_factura,libro_negro,precio_referencia,formato_evaluacion,evaluacion_financiera,historial_crediticio
					// fase 3 : formato_evaluacion_firma,rec_servicio,inspeccion_domiciliaria,bol_fac_vehiculo,deposito_inicial,carta_aprobacion,carta_caracteristica,dni_cliente,ficha_ruc,c4_cliente,vigencia_poder,duplicado_dni,dni_gerente,dni_concesionaria,vigencia_poder,dj_ing_cliente,dj_ing_emp_cliente,dj_ing_gerente,dj_ing_emp_gerente,minuta_pre,poder_llave_w,poder_inscripcion_veh_w,minuta_cons,poder_impuesto_veh
					// fase 4 : des_ret_cheque,cargo_cheque_firm,hoja_precalif_firm,dni_cliente,ficha_ruc,c4_cliente,vigencia_poder,proforma_vehiculo,bol_fac_compra,deposito_inicial,,carta_aprobacion,carta_caracteristica,cheque_gerencia,licencia_conducir,voucher_gps,voucher_str,fact_pago_gps,fact_pago_str,poliza_emitada_firm,escritura_publica,hoja_verde,anexo2,anexo1,protocolo_robo,poder_llave,poder_inscripcion_veh,copia_dj_ingreso,letra_cambio,gasto_gen_cheque,foto_entrega,llave_rotulada,gps_verificado,str_registrado,inspeccion_domiciliaria,googlemaps_inspeccion
					
					/** GARVEH: CONSTITUCION SIN CUSTODIA */
					// fase 1 : foto_atencion,foto_vehiculo,coti_seg_vehicular,boleta_informativa,revision_tecnica,papeleta,inspeccion_mecanica,simulacion,hoja_pre_calificacion,form_ing_egr
					// fase 2 : 
					// fase 3 : 
					// fase 4 : 

					/** GARVEH: CONSTITUCION CON CUSTODIA */
					// fase 1 : 
					// fase 2 : 
					// fase 3 : 
					// fase 4 : 

					$proceso_fase_id = $res2['proceso_fase_id'];
					$items = [];
					$items = explode(',', $fase['tb_fase_cont']);
					for ($i=0; $i < count($items); $i++) { 
						$item = $oProceso->obtenerItemPorIdx($items[$i]);
						if($item['estado']==1){
							$oProceso->insertarProcesoFaseItem($proceso_fase_id, $item['data']['tb_item_id'], ($i+1));
						}
					}

				}
				$data['estado'] = 1;
 				$data['mensaje'] = 'Proceso y fases registrado correctamente.';
			}else{
				$data['estado'] = 0;
 				$data['mensaje'] = 'Existe un error al guardar el Proceso. No se encuentra tipo garantía';
			}
			//
 		}

 		echo json_encode($data);

    }

	if($action == 'registro_comentario'){
		$usuario_id = $_SESSION['usuario_id']; 
		$proceso_fase_id = intval($_POST['proceso_fase_id']);
		$txt_comentario = $_POST['txt_comentario'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'No se puede registrar el comentario.';

		$res= $oProceso->insertarComentario($proceso_fase_id, $usuario_id, $txt_comentario, $doc_comentario);
 		if(intval($res['estado']) == 1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Comentario registrado correctamente.';
 			$data['proceso_fase_id'] = $proceso_fase_id;
 		}
 		echo json_encode($data);
	}

	if($action == 'exonerar'){
		$motivo = mb_strtoupper($_POST['txt_motivo_exonerar'], 'UTF-8');
    	$proceso_fase_item_id = intval($_POST['hdd_proceso_fase_item_id']);
    	$cliente_id = intval($_POST['hdd_cliente_id']);
    	$usuario_id = intval($_POST['hdd_usuario_id']);
    	$usuario = $_POST['hdd_usuario'];
    	$item_idx = $_POST['hdd_item_idx'];
    	$url = '...';
    	$his = '<span style="color: green;">Documento exonerado por: <b>'.$usuario.'</b>, el día <b>'.date('d-m-Y h:i a').'</b> | el motivo ingresado es: <b>'. $motivo .'</b></span><br>';
		
		$data['estado'] = 0;
 		$data['mensaje'] = 'No se puede registrar el comentario.';

		$proceso_fase_item = $oProceso->mostrarUnoProcesoFaseItem(intval($_POST['hdd_proceso_fase_item_id']));

		$proceso_fase = $oProceso->mostrarUnoProcesoFase(intval($proceso_fase_item['data']['tb_proceso_fase_id'])); // Obtener el Proceso Fase

		$proceso_id = intval($proceso_fase['data']['tb_proceso_id']);
		$item_id = intval($proceso_fase_item['data']['tb_item_id']);
		$proceso_fase_id = intval($proceso_fase['data']['tb_proceso_fase_id']);
		$proceso_fase_item_id = intval($proceso_fase_item['data']['tb_proceso_fase_item_id']);

		//? JUAN 28-09-2024 -/ antes de insertar un multimedia, validaremos que no exista otro registrado, solo puede aceptar 1 multimedia por item
		$bandera_multimedia = 0;
		$result = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, $item_id);
			$bandera_multimedia = intval($result['estado']); //? 0 no existe el multimedia, 1 ya existe uno registrado
		$result = NULL;

		if($bandera_multimedia == 0){
			$res = $oProceso->insertarDocExoneracion($proceso_id, $item_id, $usuario_id, $url, $his);
				if(intval($res['estado']) == 1){

					$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exo', 1, 'INT');
					$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exomot', $motivo, 'STR');

					$data['estado'] = 1;
					$data['mensaje'] = 'Documento exonerado correctamente.';
					$data['proceso_fase_id'] = $proceso_fase_id;
					
					//registrar historial
					$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
					$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
					$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
					$oHistorial->setTbHistRegmodid2($proceso_id);
					$oHistorial->setTbHistRegmodid3($item_id);
					$oHistorial->setTbHistDet('Ha exonerado el documento "'.$_POST['hdd_item_des'].'" con fecha <b>'.$fecha_hoy.'</b>');
			
					$histo_id = $oHistorial->insertar()['historial_id'];
					//registrar comentario de la exoneracion
					$oComentario->setTbComentUsureg($_SESSION['usuario_id']);
					$oComentario->setTbComentDet(explode(' ',$_SESSION['usuario_nom'])[0].' ingresó el motivo: '.$motivo.'.');
					$oComentario->setTbComentNomTabla('tb_hist');
					$oComentario->setTbComentRegmodid($histo_id);
					$oComentario->insertar();

				}
			$res = NULL;

			if($data['estado'] == 0)
				$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exomot', $motivo.' / Se insertó exonerado pero no el historial, no ingresó al if de $res estado == 1 / '.$res['estado'], 'STR');
		}
		else{
			$data['mensaje'] = 'No se puede exonerar porque ya fue exonerado el documento.';
		}

		echo json_encode($data);

	}

	// Subida de documentos para cada item
	if ($action == 'subir_documento') {
		if (!empty($_FILES)) {

			$proceso_fase_item_id = $_POST['proceso_fase_item_id'];
			$item_idx = $_POST['item_idx'];

			$proceso_fase_item = $oProceso->mostrarUnoProcesoFaseItem($proceso_fase_item_id);

			$proceso_fase = $oProceso->mostrarUnoProcesoFase(intval($proceso_fase_item['data']['tb_proceso_fase_id'])); // Obtener el Proceso Fase

			$proceso_id = intval($proceso_fase['data']['tb_proceso_id']);
			$item_id = intval($proceso_fase_item['data']['tb_item_id']);
			$proceso_fase_id = intval($proceso_fase['data']['tb_proceso_fase_id']);

			$directorio = 'public/pdf/procesos/'; //nombre con el que irá guardado en la carpeta
			$tempFile = $_FILES['Filedata']['tmp_name'];//temp servicio de PHP
			$fileParts = pathinfo($_FILES['Filedata']['name']); //extensiones de las imágenes

			$fileTypes = array('pdf'); // Allowed file extensions
			$pdf_is = $_POST['pdf_is'];
			$his = 'Archivo subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');

			$data['estado'] = 0;
 			$data['mensaje'] = 'No se puede subir el documento.';

			if($pdf_is == 'nuevo'){ //si es exitosa la modificacion retorna 1
				$usuario_id = $_SESSION['usuario_id'];

				if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

					// identificar si existe archivo subido
					$existe_archivo = $oProceso->mostrarDocXItem($proceso_id, $item_id);

					if($existe_archivo['estado']==0){ // no exiiste registro

						$res = $oProceso->insertarDocProcesoFaseItem($proceso_id, $item_id, $proceso_fase_id, $usuario_id, $his, $directorio, $tempFile, $fileParts);
						
						if($res['estado'] == 1){

							$oProceso->modificar_campo_multimedia(intval($res['multimedia_id']), 'tb_multimedia_est', 2, 'INT'); // item con archivo
							
							//registrar historial
							$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
							$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
							$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
							$oHistorial->setTbHistRegmodid2($proceso_id);
							$oHistorial->setTbHistRegmodid3($item_id);
							$oHistorial->setTbHistDet('Ha subido el documento "'.$_POST['item_des'].'" con fecha <b>'.$fecha_hoy.'</b>');
							//$oHistorial->insertar();
	
							$histo_id = $oHistorial->insertar()['historial_id'];
							//registrar comentario de la subida de doc
							$oComentario->setTbComentUsureg($_SESSION['usuario_id']);
							$oComentario->setTbComentDet(explode(' ',$_SESSION['usuario_nom'])[0].' subió un archivo al ítem: '.$_POST['item_des'].'.');
							$oComentario->setTbComentNomTabla('tb_hist');
							$oComentario->setTbComentRegmodid($histo_id);
							$oComentario->insertar();

							$data['estado'] = 1;
							$data['mensaje'] = 'Documento subido correctamente.';
							$data['proceso_fase_id'] = $proceso_fase_id;
					
						}
						
					}else{

						$data['estado'] = 2;
						$data['mensaje'] = 'El documento ya fue subido o se encuentra exonerado.';

					}
					
					echo json_encode($data);

				} else {
					echo 'Tipo de archivo no válido.';
				}

			}else{ //mod1... si es exitosa la modificacion retorna 2

				// NO SE USA 

				if (in_array(strtolower($fileParts['extension']), $fileTypes)){
					$oCreditogarvehpdf->pdfgarv_id = intval($_POST['id_reg_mod']);
					$oCreditogarvehpdf->pdfgarv_his = 'Archivo modificado. Subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');
					$oCreditogarvehpdf->pdfgatv_estado = 1;

					$return = $oCreditogarvehpdf->modificar($pdf_is, 's');
					if($return['estado']==1){
						echo 2;
						$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
						$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
						$oHistorial->setTbHistRegmodid($oCreditogarvehpdf->pdfgarv_id);
						$oHistorial->setTbHistDet('Ha actualizado el archivo "'.$_POST['cgarvdoc_nom'].'" del crédito '.$oCreditogarvehpdf->credito_id.' | <b>'.$fecha_hoy.'</b>');

						$oHistorial->insertar();
					}
					else{
						echo trim($return['mensaje']);
					}
				}
				else {
					// The file type wasn't allowed
					echo 'Tipo de archivo no válido.';
				}
			}
			
			//echo json_encode($data);
		}
		else {
			echo 'No hay ningún archivo para subir';
		}
	}

	// eliminar documento para cada item
	if ($action == 'eliminar_documento') {
		$multimedia_id = intval($_POST['multimedia_id']);
		$proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);
	
		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al eliminar el documento.';

		$proceso_fase_item = $oProceso->mostrarUnoProcesoFaseItem($proceso_fase_item_id);

		if ($oProceso->eliminarDocProcesoFaseItem($multimedia_id)) {

			$data['estado'] = 1;
			$data['mensaje'] = 'Documento eliminado correctamente.';
			$data['proceso_fase_item_id'] = $proceso_fase_item_id;

			//añadir historial de eliminacion del pdf
			$ruta = $oProceso->mostrarUnoMultimediaEliminado($multimedia_id)['data']['tb_multimedia_url'];
			$url = explode("/procesos/",$ruta);
			$archivo = $url[1];

			if($ruta != '...'){
				$ruta = ' El archivo eliminado está <a href="'.$ruta.'" target="_blank">AQUI</a>';
			}
			else{
				$ruta='';
			}

			if($proceso_fase_item['estado'] == 1){
				$data['proceso_fase_id'] = intval($proceso_fase_item['data']['tb_proceso_fase_id']);

				$oProceso->modificar_campo_item($_POST['proceso_fase_item_id'], 'tb_proceso_fase_item_est', 1, 'INT'); // pasar el item al estado sin archivo
	
				$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
				$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
				$oHistorial->setTbHistRegmodid($_POST['proceso_fase_item_id']);
				$oHistorial->setTbHistDet('Ha eliminado el archivo "'.$archivo.'" del item <strong>'.$_POST['item_des'].'</strong> proceso fase '.$_POST['proceso_fase_item_id'].'. '.$ruta.' | <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();

			}

			
		}
	
		echo json_encode($data);
	}

	if($action == 'guardar_preconsti_fase1'){

        $usuario_id = $_SESSION['usuario_id']; 
 		$proceso_fase_id = intval($_POST['proceso_fase_id']);
 		$cliente_id = intval($_POST['cliente_id']);
 		$cliente_id_ori = intval($_POST['cliente_id_ori']);
 		$cliente_nom = $_POST['cliente_nom'];
 		$cliente_cel = $_POST['cliente_cel'];
 		//$fecha_inspeccion = fecha_mysql($_POST['fecha_inspeccion']);
 		//$fecha_deposito = fecha_mysql($_POST['fecha_deposito']);
 		//$inicial = floatval($_POST['inicial']);
 		//$inicial_est = $_POST['inicial_est'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar los datos del cliente.';

		$proceso_fase = $oProceso->mostrarUnoProcesoFase($proceso_fase_id);

		if (intval($proceso_fase['estado']) == 1) { // existe proceso

			$proceso = $oProceso->mostrarUno($proceso_fase['data']['tb_proceso_id']);

			// registrar cliente a partir de precliente
			if($proceso['data']['tb_cliente_id']==null || $proceso['data']['tb_precliente_id']==0){
				$precliente = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);
				if($precliente['estado'] == 1){
					if(intval($precliente['data']['tb_cliente_id']) > 0){
						$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_cliente_id', intval($precliente['data']['tb_cliente_id']), 'INT'); // añadir cliente modificado
					}	
				}
			}
			//

			if ($cliente_id_ori == 0) { // proceso con cliente nuevo
				$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_cliente_id', $cliente_id, 'INT'); // añadir cliente nuevo
			} else {
				if($cliente_id != $cliente_id_ori){ // Se cambió cliente
					$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_cliente_id', $cliente_id, 'INT'); // añadir cliente modificado
				}
			}

			

			//$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_fec_inspeccion', $fecha_inspeccion, 'STR');
			//$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_fec_inicial', $fecha_deposito, 'STR');
			//$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_inicial', $inicial, 'INT');
			//$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_inicial_est', $inicial_est, 'STR');

			$data['estado'] = 1;
 			$data['mensaje'] = 'Se ha guardado los datos correctamente.';

		} else {
			$data['mensaje'] = 'Existe un error, no se encuentra proceso.';
		}

 		echo json_encode($data);

    }

	if($action == 'guardar_consti_fase1'){

        $usuario_id = $_SESSION['usuario_id']; 
 		$proceso_fase_id = intval($_POST['proceso_fase_id']);
 		$tipo_garv = intval($_POST['tipo_garv']);
 		$cliente_id = intval($_POST['cliente_id']);
 		$cliente_id_ori = intval($_POST['cliente_id_ori']);
 		$cliente_nom = $_POST['cliente_nom'];
 		$cliente_cel = $_POST['cliente_cel'];
 		$inspeccion_vehiculo = fecha_mysql($_POST['inspeccion_vehiculo']);
 		$prueba_vehiculo = $_POST['prueba_vehiculo'];
 		$cliente_acepta = $_POST['cliente_acepta'];
 		$cliente_acepta_des = $_POST['cliente_acepta_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar los datos del cliente.';

		$proceso_fase = $oProceso->mostrarUnoProcesoFase($proceso_fase_id);

		if (intval($proceso_fase['estado']) == 1) { // existe proceso

			$proceso = $oProceso->mostrarUno($proceso_fase['data']['tb_proceso_id']);

			// registrar cliente a partir de precliente
			if($proceso['data']['tb_cliente_id']==null || $proceso['data']['tb_precliente_id']==0){
				$precliente = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);
				if($precliente['estado'] == 1){
					if(intval($precliente['data']['tb_cliente_id']) > 0){
						$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_cliente_id', intval($precliente['data']['tb_cliente_id']), 'INT'); // añadir cliente modificado
					}	
				}
			}
			//

			if ($cliente_id_ori == 0) { // proceso con cliente nuevo
				$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_cliente_id', $cliente_id, 'INT'); // añadir cliente nuevo
			} else {
				if($cliente_id != $cliente_id_ori){ // Se cambió cliente
					$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_cliente_id', $cliente_id, 'INT'); // añadir cliente modificado
				}
			}
			$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_fec_inspeccion', $inspeccion_vehiculo, 'STR');
			$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_prueba_veh', $prueba_vehiculo, 'STR');
			$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_cli_acepta', $cliente_acepta, 'STR');
			$oProceso->modificar_campo_proceso($proceso_fase['data']['tb_proceso_id'], 'tb_proceso_cli_acepta_det', $cliente_acepta_des, 'STR');

			$data['estado'] = 1;
 			$data['mensaje'] = 'Se ha guardado los datos correctamente.';

		} else {
			$data['mensaje'] = 'Existe un error, no se encuentra proceso.';
		}

 		echo json_encode($data);

    }

	if($action == 'pasar_siguiente_fase'){
		
    	$proceso_id = intval($_POST['proceso_id']);
    	$proceso_fase_id = intval($_POST['proceso_fase_id']);
    	$orden = intval($_POST['orden']);
		$inspeccion_vehiculo = fecha_mysql($_POST['inspeccion_vehiculo']);
 		$prueba_vehiculo = $_POST['prueba_vehiculo'];
 		$cliente_acepta = $_POST['cliente_acepta'];
 		$cliente_acepta_des = $_POST['cliente_acepta_des'];
    	$usuario_id = $_SESSION['usuario_id'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al pasar a la siguiente fase.';

		$proceso = $oProceso->mostrarUno($proceso_id); // Obtener el Proceso
		$tipo_garv = 0;
		if($proceso['estado']==1){
			$tipo_garv = intval($proceso['data']['tb_cgarvtipo_id']);
		}
		// obtener cantidad de fases
		$proceso_fase_item = $oProceso->obtenerFasesProceso($proceso_id);
		$cantidad_fases = 0;
		if($proceso_fase_item['estado']==1){
			$cantidad_fases = count($proceso_fase_item['data']);
		}

		/* GERSON (04-04-24) */
		$precali = $oProceso->mostrarUnoPrecalificacion($proceso_id);
		if($orden == 1){
			// validacion fase 1

			/* GERSON (28-05-24) */
			if($tipo_garv == 4 || $tipo_garv == 5){
				$participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
				if($participantes['estado'] == 0){ // No se encontraron participantes
					$data['mensaje'] = 'Ingrese <strong>PARTICIPANTES</strong> beneficiario de cheque/depósito.';
					$data['proceso_fase_id'] = $proceso_fase_id;
					$data['tipo_garv'] = $tipo_garv;
					echo json_encode($data);
					exit();
				}
			}
			/*  */

			if($precali['estado'] == 1){
				$tipo_cliente = ""; // natural o juridica
				$tipo_cliente = $precali['data']['tb_precalificacion_tipo_cliente'];
				if($tipo_cliente != null || $tipo_cliente != ""){

					$url = '...';
					$usuario = $_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'];
					$motivo = 'SISTEMA: Cliente es PERSONA NATURAL.';
					$his = '<span style="color: green;">Documento exonerado de forma automática por -----: <b>'.$usuario.'</b>, el día <b>'.date('d-m-Y h:i a').'</b> | el motivo ingresado es: <b>'. $motivo .'</b></span><br>';

					if($tipo_cliente == 'natural'){
						// exoneracion FICHA RUC
						$bandera_multimedia = 0;
						$result = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 8);
							$bandera_multimedia = intval($result['estado']); //? 0 no existe el multimedia, 1 ya existe uno registrado
						$result = NULL;

						if($bandera_multimedia == 0){
							$res = $oProceso->insertarDocExoneracion($proceso_id, 8, $usuario_id, $url, $his);

								if(intval($res['estado']) == 1){
									$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exo', 1, 'INT');
									$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exomot', $motivo, 'STR');

									$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
									$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
									$oHistorial->setTbHistRegmodid($proceso_fase_id);
									$oHistorial->setTbHistRegmodid2($proceso_id);
									$oHistorial->setTbHistRegmodid3(8);
									$oHistorial->setTbHistDet('FICHA RUC exonerado en automático: con fecha <b>'.$fecha_hoy.'</b>');
							
									$histo_id = $oHistorial->insertar()['historial_id'];
									//registrar comentario de la exoneracion
									$oComentario->setTbComentUsureg($_SESSION['usuario_id']);
									$oComentario->setTbComentDet('Motivo: '.$motivo);
									$oComentario->setTbComentNomTabla('tb_hist');
									$oComentario->setTbComentRegmodid($histo_id);
									$oComentario->insertar();
								}
							$res = NULL;
						}

						$bandera_multimedia = 0;
						$result = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 10);
							$bandera_multimedia = intval($result['estado']); //? 0 no existe el multimedia, 1 ya existe uno registrado
						$result = NULL;
						// exoneracion VIGENCIA PODER
						if($bandera_multimedia == 0){
							$res = $oProceso->insertarDocExoneracion($proceso_id, 10, $usuario_id, $url, $his);
								if(intval($res['estado']) == 1){
									$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exo', 1, 'INT');
									$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exomot', $motivo, 'STR');

									//registrar historial
									$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
									$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
									$oHistorial->setTbHistRegmodid($proceso_fase_id);
									$oHistorial->setTbHistRegmodid2($proceso_id);
									$oHistorial->setTbHistRegmodid3(10);
									$oHistorial->setTbHistDet('VIGENCIA DE PODER exonerado en automático:  con fecha <b>'.$fecha_hoy.'</b>');
							
									$histo_id = $oHistorial->insertar()['historial_id'];
									//registrar comentario de la exoneracion
									$oComentario->setTbComentUsureg($_SESSION['usuario_id']);
									$oComentario->setTbComentDet('Motivo: '.$motivo);
									$oComentario->setTbComentNomTabla('tb_hist');
									$oComentario->setTbComentRegmodid($histo_id);
									$oComentario->insertar();
								}
							$res = NULL;
						}
					}
					
				}else{
					$data['mensaje'] = 'Seleccione si el cliente es <strong>PERSONA NATURAL</strong> o <strong>PERSONA JURÍDICA</strong>.';
					$data['proceso_fase_id'] = $proceso_fase_id;
					$data['tipo_garv'] = $tipo_garv;
					echo json_encode($data);
					exit();
				}
			}else{
				
				$data['mensaje'] = 'El proceso aún no tiene Pre Calificación.';
				$data['proceso_fase_id'] = $proceso_fase_id;
				$data['tipo_garv'] = $tipo_garv;
				echo json_encode($data);
				exit();
			}

			/* GERSON (20-05-24) */
			// Otros campos de Fase 1
			if($tipo_garv!=1 && $tipo_garv!=6){
				if($inspeccion_vehiculo=='' || $inspeccion_vehiculo==null ||
				$prueba_vehiculo=='' || $prueba_vehiculo==null ||
				$cliente_acepta=='' || $cliente_acepta==null ||
				$cliente_acepta_des=='' || $cliente_acepta_des==null){
	
					$data['mensaje'] = 'Faltan llenar campos como Revisiones y Comentarios de Gerente.';
					$data['proceso_fase_id'] = $proceso_fase_id;
					$data['tipo_garv'] = $tipo_garv;
					echo json_encode($data);
					exit();
				}
			}
			/*  */

		}elseif($orden == 2){
			// validacion fase 3
			if($precali['estado'] == 1){
				$tipo_cliente = ""; // natural o juridica
				$tipo_cliente = $precali['data']['tb_precalificacion_tipo_cliente'];
				if($tipo_cliente != null || $tipo_cliente != ""){

					$url = '...';
					$usuario = $_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'];
					$motivo = 'SISTEMA: Cliente es PERSONA JURÍDICA.';
					$his = '<span style="color: green;">Documento exonerado de forma automática por: <b>'.$usuario.'</b>, el día <b>'.date('d-m-Y h:i a').'</b> | el motivo ingresado es: <b>'. $motivo .'</b></span><br>';

					if($tipo_cliente == 'juridica'){
						// exoneracion DJ REPRESENTANTE LEGAL
						$bandera_multimedia = 0;
						$result = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 85);
							$bandera_multimedia = intval($result['estado']); //? 0 no existe el multimedia, 1 ya existe uno registrado
						$result = NULL;

						if($bandera_multimedia == 0){
							$res = $oProceso->insertarDocExoneracion($proceso_id, 85, $usuario_id, $url, $his);
								if(intval($res['estado']) == 1){
									$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exo', 1, 'INT');
									$oProceso->modificar_campo_multimedia($res['tb_multimedia_id'], 'tb_multimedia_exomot', $motivo, 'STR');

									//registrar historial
									$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
									$oHistorial->setTbHistNomTabla("tb_proc_proceso_fase_item");
									$oHistorial->setTbHistRegmodid($proceso_fase_id);
									$oHistorial->setTbHistRegmodid2($proceso_id);
									$oHistorial->setTbHistRegmodid3(85);
									$oHistorial->setTbHistDet('Ha exonerado el documento DJ REPRESENTANTE LEGAL con fecha <b>'.$fecha_hoy.'</b>');
							
									$histo_id = $oHistorial->insertar()['historial_id'];
									//registrar comentario de la exoneracion
									$oComentario->setTbComentUsureg($_SESSION['usuario_id']);
									$oComentario->setTbComentDet('Motivo: '.$motivo);
									$oComentario->setTbComentNomTabla('tb_hist');
									$oComentario->setTbComentRegmodid($histo_id);
									$oComentario->insertar();
								}
							$res = NULL;
						}
					}

				}else{
					$data['mensaje'] = 'Seleccione si el cliente es <strong>PERSONA NATURAL</strong> o <strong>PERSONA JURÍDICA</strong>.';
					$data['proceso_fase_id'] = $proceso_fase_id;
					$data['tipo_garv'] = $tipo_garv;
					echo json_encode($data);
					exit();
				}
			}

		}
		
		//
		/*  */
		
		// validar que tiene una siguiente fase
		if($cantidad_fases > 0){
			$siguiente_fase = 0;
			if($cantidad_fases > $orden){ // está dentro de las fases

				$orden = $orden + 1; // siguiente fase 

				$proceso_fase_next = $oProceso->mostrarProcesoFase($proceso_id, $orden);
				if($proceso_fase_next['estado']==1){

					$oProceso->modificar_campo_proceso_fase($proceso_fase_next['data']['tb_proceso_fase_id'], 'tb_proceso_fase_proc', 1, 'INT');

					$data['estado'] = 1;
					$data['mensaje'] = 'Se pasó a la siguiente fase correctamente.';
					$data['usuario_id'] = $usuario_id;
					$data['proceso_fase_id'] = $proceso_fase_id;
					$data['tipo_garv'] = $tipo_garv;

				}else{
					$data['mensaje'] = 'No se encontró otra fase en este proceso.';
				}

			}else{
				//ultima fase
				$data['mensaje'] = 'Nos encontramos en la última fase de este proceso.';
			}
		}else{
			$data['mensaje'] = 'No existen fases en este proceso.';
		}

 		echo json_encode($data);
	}

	if($action == 'votacion_comite'){

    	$proceso_id = intval($_POST['proceso_id']);
    	$usuario_id = intval($_POST['usuario_id']);
    	$value = intval($_POST['value']);

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al votar.';

		if($oProceso->registrarVotacionComite($proceso_id, $usuario_id, $value)){
			$data['estado'] = 1;
 			$data['mensaje'] = 'Votación registrada correctamente.';
 			$data['proceso_id'] = $proceso_id;
 			$data['usuario_id'] = $usuario_id;
		}
 		echo json_encode($data);
		
	}

	if($action == 'votacion_comite_otro'){

    	$proceso_id = intval($_POST['proceso_id']);
    	$usuario_id = intval($_POST['usuario_id']);
    	$value = intval($_POST['value']);

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al escoger.';

		if($oProceso->modificar_campo_proceso($proceso_id, "tb_proceso_usu_firma_otro", $usuario_id, "INT")){
			$data['estado'] = 1;
 			$data['mensaje'] = 'Cambio registrado correctamente.';
 			$data['proceso_id'] = $proceso_id;
 			$data['usuario_id'] = $usuario_id;
		}
 		echo json_encode($data);
		
	}

	if($action == 'guardar_bancarizacion'){

    	$proceso_id = intval($_POST['proceso_id']);
    	$banca = intval($_POST['banca']);

		$proceso = $oProceso->mostrarUno($proceso_id);

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar bancarización.';

		if($oProceso->registrarBancarizacion($proceso_id, $banca, $proceso['data']['tb_proceso_fecreg'])){
			$data['estado'] = 1;
 			$data['mensaje'] = 'Bancarización registrada correctamente.';
 			$data['proceso_id'] = $proceso_id;
		}
 		echo json_encode($data);
		
	}

	
	if($action=="detalle_cheque"){
		$cant_cheque = intval($_POST['cant_cheque']);
		$proceso_id = intval($_POST['proceso_id']);
		$garvtipo_id = intval($_POST['garvtipo_id']);
		$usuario_id = intval($_POST['usuario_id']);

		$cliente_id = 0;
		//$garvtipo_id = 0;
		$proceso = $oProceso->mostrarUno($proceso_id);
		$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
		
		if($proceso['estado']==1){
			$cliente_id = $proceso['data']['tb_cliente_id'];
			//$garvtipo_id = intval($proceso['data']['tb_cgarvtipo_id']);
		} 

		$transferente_id = 0;
		$transferente = '';
		if($precalificacion['estado']==1){
			$transferente_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
			$transferente = $oTransferente->mostrarUno($transferente_id);
			if($transferente['estado']==1){
				$transferente = $transferente['data']['tb_transferente_nom'];
			}
		} 

		$nombre = '';
		$cliente = $oCliente->mostrarUno($cliente_id);
		if($cliente['estado']==1){
			$nombre = $cliente['data']['tb_cliente_nom'];
		} 
		
		$html = '';
		$html .= '<div class="col-md-12" style="">

					<input type="hidden" id="hdd_cant_cheque" value="'.$cant_cheque.'">

					<div class="col-md-6">
						<div class="form-group" style="padding-top: 15px;">
						<label>Banco: </label>
						<select id="cmb_banco_id" name="cmb_banco_id" class="form-control input-sm" data-live-search="true" data-max-options="1">
							<option value="1">Banco de Crédito del Perú</option>
							<option value="2">BBVA Continental</option>
						</select>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group" style="padding-top: 15px;">
						<label>Moneda: </label>
						<select id="cmb_moneda" name="cmb_moneda" class="form-control input-sm" data-live-search="true" data-max-options="1">
							<option value="sol">Soles</option>
							<option value="dolar">Dólares</option>
						</select>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group" style="padding-top: 15px;">
						<label>Enlace: </label>
						<select id="cmb_enlace" name="cmb_enlace" class="form-control input-sm" data-live-search="true" data-max-options="1">
							<option value="y">Y</option>
							<option value="o">O</option>
						</select>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group" style="padding-bottom: 15px;">
						<label>Detalle: </label>
						<textarea class="form-control" name="detalle" id="detalle" rows="3" placeholder="Ingrese Detalle..." style="resize: none;"></textarea>
						</div>
					</div>

					<div class="col-md-5"><strong>Beneficiario</strong></div>
					<div class="col-md-3"><strong>Monto</strong></div>
					<div class="col-md-4"><strong>Tipo</strong></div>';

					for ($i=0; $i < $cant_cheque; $i++) { 
					
						if($garvtipo_id==1){ // PRE CONSTITUCION: Maneja directamente con conscionaria (transferentes)
							/* $html .= '<div class="col-md-5">
									<div class="form-group" style="padding-top: 5px; padding-bottom: 5px;">
										<input type="text" class="form-control input-sm" name="cheque_per'.$i.'" id="cheque_per'.$i.'" onkeyup="detectarInput(event)" placeholder="Buscar concesionaria..." readonly />
									</div>
								</div>'; */
							$html .= '<div class="col-md-5">
								<div class="form-group" style="padding-top: 5px; padding-bottom: 5px;">
									<input type="text" class="form-control input-sm" name="cheque_per'.$i.'" id="cheque_per'.$i.'" value="'.$transferente.'" readonly />
									<input type="hidden" name="cheque_per_id'.$i.'" id="cheque_per_id'.$i.'" value="'.$transferente_id.'" />
								</div>
							</div>';
						}else{ // CONSTITUCION: Maneja los participantes del proceso como beneficiarios de los cheques, se hará uso de los enlaces
							/* $html .= '<div class="col-md-5">
									<div class="form-group" style="padding-top: 5px; padding-bottom: 5px;">
										<input type="text" class="form-control input-sm" name="cheque_per'.$i.'" id="cheque_per'.$i.'" value="'.$nombre.'" readonly />
									</div>
								</div>'; */
							$html .= '<div class="col-md-5">
									<div class="form-group" style="padding-top: 5px; padding-bottom: 5px;">
										<input type="text" class="form-control input-sm" name="cheque_per'.$i.'" id="cheque_per'.$i.'" value="" placeholder="Autogenerado" readonly />
									</div>
								</div>';
						}
						$html .= '<div class="col-md-3">
								<div class="form-group" style="padding-top: 5px; padding-bottom: 5px;">
									<input type="text" class="form-control input-sm" name="cheque_monto'.$i.'" id="cheque_monto'.$i.'" onkeypress="return solo_decimal(event)" style="text-align:right;" value="" placeholder="0.00">
								</div>
							</div>';
						$html .= '<div class="col-md-4">
							<div class="form-group" style="padding-top: 5px; padding-bottom: 5px;">
								<select id="cmb_negociable'.$i.'" name="cmb_negociable'.$i.'" class="form-control input-sm" data-live-search="true" data-max-options="1">
									<option value="nego">Negociable</option>
									<option value="no_nego">No Negociable</option>
								</select>
							</div>
						</div>';			

					}

		$html .= '</div>';

		$html .= '<script>
					var cont = parseInt($("#hdd_cant_cheque").val());
					var i = 0;

					function detectarInput(event){
						let x = "#"+event.target.id;
						$(document).on("keydown", x, function() {
							$(x).autocomplete({
								minLength: 1,
								source: function (request, response) {
									$.getJSON(
											VISTA_URL + "transferente/transferente_autocomplete.php",
											{term: request.term}, 
											response
											);
								},
								select: function (event, ui) {
									$(x).val(ui.item.transferente_nom);
						
									event.preventDefault();
									$(x).focus();
								}
							});
						});
					}

					
				</script>';

		echo $html;
  	}

	if($action == 'guardar_cheque'){

    	$proceso_id = intval($_POST['proceso_id']);
        $usuario_id = $_SESSION['usuario_id']; 
    	$cheques = $_POST['cheques'];
    	$cant_cheque = intval($_POST['cant_cheque']);
    	$banco_id = intval($_POST['banco_id']);
    	$moneda = $_POST['moneda'];
    	$enlace = $_POST['enlace'];
    	$detalle = $_POST['detalle'];

		$proceso = $oProceso->mostrarUno($proceso_id);

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar cheques.';

		$res = $oProceso->registrarCheque($proceso_id, $usuario_id, $banco_id, $moneda, $enlace, $detalle);
		if($res['estado']==1){
			foreach ($cheques as $key => $value) {
				$oProceso->registrarChequeDetalle($res['cheque_id'], $value['cheque_nombre'], $value['cheque_monto'], $value['cheque_tipo']);
			}
			$data['estado'] = 1;
			$data['mensaje'] = 'Cheques registrados correctamente.';
		}
 		echo json_encode($data);
		
	}

	if($action=="detalle_simulacion"){

		$proceso_id = intval($_POST['proceso_id']);
		$usuario_id = intval($_POST['usuario_id']);
		$fecha = $_POST['fecha'];
		$moneda = $_POST['moneda'];
		$cambio = floatval($_POST['cambio']);
		$cuota = intval($_POST['cuota']);
		$interes = floatval($_POST['interes']);
		$costo = floatval($_POST['costo']);
		$inicial = floatval($_POST['inicial']);
		$valor = floatval($_POST['valor']);

		$chk_gps = intval($_POST['chk_gps']);

		$seguro_porcentaje = floatval($_POST['seguro_porcentaje']);
		$gps_precio = floatval($_POST['gps_precio']);
		$span_seguro_gps = $_POST['span_seguro_gps'];

		$proceso = $oProceso->mostrarUno($proceso_id);

		// GERSON (14-11-23)
		if($seguro_porcentaje > 0){ // nuevo modo de interes al 14-11-23, se suma el 0.27 a la tasa de interes fijada
			$interes = $interes + $seguro_porcentaje;
		}
		//

		$mon = '';
		$sub_mon = '';
		if($moneda=='sol'){
			$mon = 'S/ ';
		}elseif($moneda=='dolar'){
			$mon = '$ ';
			$sub_mon = 'S/ ';
		}elseif($moneda=='sol_d'){
			$mon = '$ ';
			$sub_mon = 'S/ ';
		}elseif($moneda=='dolar_s'){
			$mon = 'S/ ';
		}

		$html = '';
		$html .= '<div class="col-md-12">

					<table class="table table-hover table-responsive">
						<thead>
							<tr id="tabla_cabecera">
								<th id="tabla_cabecera_fila">Fecha Pago</th>
								<th id="tabla_cabecera_fila">N° Cuota</th>
								<th id="tabla_cabecera_fila">Capital</th>
								<th id="tabla_cabecera_fila">Amortización</th>
								<th id="tabla_cabecera_fila">Sobrecosto Periodo</th>
								<th id="tabla_cabecera_fila">GPS</th>
								<th id="tabla_cabecera_fila">Cuota</th>';
								if($moneda=='dolar' || $moneda=='sol_d'){
									$html .= '<th id="tabla_cabecera_fila">Cuota Soles</th>';
								}
				$html .= '</tr>
						</thead>
						<tbody>';

					if($moneda=='sol' || $moneda=='dolar'){

						echo "<div class='col-md-12'>";
						echo "<h4>Costo de Vehículo: <strong>".$mon.''.number_format($costo, 2)."</strong></h4>";
						echo "<h4>Valor de Préstamo: <strong>".$mon.''.number_format($valor, 2)."</strong></h4>";
						echo "</div>";

						for ($i=0; $i < $cuota; $i++) { 

							$seguro_mensual = 0.00;
							$gps_mensual = 0.00;

							if($i==0){
								$new_date = $fecha;
								$capital = $valor;
							}
							$new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));
	
							$cuota_pagar = 0.00;
							$cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*$valor/((pow(1+($interes/100),$cuota))-1);
	
							$sobrecosto = 0.00;
							$sobrecosto = $capital*($interes/100);

							$amortizacion = 0.00;
							if($cuota_pagar>0){
								$amortizacion = $cuota_pagar - $sobrecosto;
							}

							// GERSON (10-11-23)
							/* if($seguro_porcentaje > 0){
								$seguro_mensual = formato_moneda($seguro_porcentaje * $cuota_pagar / 100);
								$sobrecosto = $sobrecosto + $seguro_mensual;
								$cuota_pagar = $cuota_pagar + $seguro_mensual;
							} */

							if($chk_gps > 0){
								if($gps_precio > 0 && $cuota > 0){
									if($moneda=='sol'){
										$gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
									}elseif($moneda=='dolar'){
										$gps_mensual = formato_moneda($gps_precio / $cuota);
									}
									$cuota_pagar = $cuota_pagar + $gps_mensual;
								}
							}
							//
							
							$html .= '<tr id="tabla_cabecera_fila">
										<td id="tabla_fila" style="">'.$new_date.'</td>
										<td id="tabla_fila" style="">'.($i+1).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($capital, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($amortizacion, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($sobrecosto, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($gps_mensual, 2).'</td>
										<td id="tabla_fila" style="text-align: right; background: #FFE6CA;">'.$mon.''.number_format($cuota_pagar, 2).'</td>';
										if($moneda=='dolar'){
											$html .= '<td id="tabla_fila" style="text-align: right; background: #D0F6E6;">'.$sub_mon.''.number_format($cuota_pagar*$cambio, 2).'</th>';
										}
							$html .= '</tr>';
	
							$capital = $capital - $amortizacion;
							$total_sobrecosto = $total_sobrecosto + $sobrecosto;
							$total_cuotas = $total_cuotas + $cuota_pagar - $gps_mensual;
	
						}

						echo "<div class='col-md-12' style='padding-top: 3px; padding-bottom: 7px;'>";
						echo "<span class='h4' style='padding: 5px;'>N° Cuotas: <strong>".$cuota."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'>Capital <strong>".$mon.''.number_format($valor, 2)."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'>Sobrecosto Total: <strong>".$mon.''.number_format($total_sobrecosto, 2)."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'><strong>TOTAL: ".$mon.''.number_format($total_cuotas, 2)."</strong></span><br><br>";
						echo "<span class='h5' style='padding: 5px; color:green'><strong>".$span_seguro_gps."</strong></span>";
						echo "</div>";


					}elseif($moneda=='dolar_s'){

						echo "<div class='col-md-12'>";
						echo "<h4>Costo de Vehículo: <strong>".$mon.''.number_format($costo*$cambio, 2)."</strong></h4>";
						echo "<h4>Valor de Préstamo: <strong>".$mon.''.number_format($valor*$cambio, 2)."</strong></h4>";
						echo "</div>";
						

						for ($i=0; $i < $cuota; $i++) { 

							$seguro_mensual = 0.00;
							$gps_mensual = 0.00;

							if($i==0){
								$new_date = $fecha;
								$capital = $valor*$cambio;
							}

							$new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));
	
							$cuota_pagar = 0.00;
							$cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor*$cambio)/((pow(1+($interes/100),$cuota))-1);

							$sobrecosto = 0.00;
							$sobrecosto = $capital*($interes/100);

							$amortizacion = 0.00;
							if($cuota_pagar>0){
								$amortizacion = $cuota_pagar - $sobrecosto;
							}

							// GERSON (10-11-23)
							/* if($seguro_porcentaje > 0){
								$seguro_mensual = formato_moneda($seguro_porcentaje * $cuota_pagar / 100);
								$sobrecosto = $sobrecosto + $seguro_mensual;
								$cuota_pagar = $cuota_pagar + $seguro_mensual;
							} */

							if($chk_gps > 0){
								if($gps_precio > 0 && $cuota > 0){
									$gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
									//$gps_mensual = formato_moneda($gps_precio / $cuota);
									$cuota_pagar = $cuota_pagar + $gps_mensual;
								}
							}
							//

							$html .= '<tr id="tabla_cabecera_fila">
										<td id="tabla_fila" style="">'.$new_date.'</td>
										<td id="tabla_fila" style="">'.($i+1).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($capital, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($amortizacion, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($sobrecosto, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($gps_mensual, 2).'</td>
										<td id="tabla_fila" style="text-align: right; background: #FFE6CA;">'.$mon.''.number_format($cuota_pagar, 2).'</td>';
							//$html .= '<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($seguro_mensual, 2).' + '.$mon.''.number_format($sobrecosto-$seguro_mensual, 2).'</td>';
							//$html .= '<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($gps_mensual, 2).'</td>';
							//$html .= '<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($seguro_mensual, 2).' + '.$mon.''.number_format($gps_mensual, 2).' + '.$mon.''.number_format($cuota_pagar-$seguro_mensual-$gps_mensual, 2).'</td>';
							$html .= '</tr>';
	
							$capital = $capital - $amortizacion;
							$total_sobrecosto = $total_sobrecosto + $sobrecosto;
							$total_cuotas = $total_cuotas + $cuota_pagar - $gps_mensual;
						}

						echo "<div class='col-md-12' style='padding-top: 3px; padding-bottom: 7px;'>";
						echo "<span class='h4' style='padding: 5px;'>N° Cuotas: <strong>".$cuota."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'>Capital <strong>".$mon.''.number_format($valor*$cambio, 2)."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'>Sobrecosto Total: <strong>".$mon.''.number_format($total_sobrecosto, 2)."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'><strong>TOTAL: ".$mon.''.number_format($total_cuotas, 2)."</strong></span><br><br>";
						echo "<span class='h5' style='padding: 5px; color:green'><strong>".$span_seguro_gps."</strong></span>";
						echo "</div>";

					}elseif($moneda=='sol_d'){

						echo "<div class='col-md-12'>";
						echo "<h4>Costo de Vehículo: <strong>".$mon.''.number_format($costo/$cambio, 2)."</strong></h4>";
						echo "<h4>Valor de Préstamo: <strong>".$mon.''.number_format($valor/$cambio, 2)."</strong></h4>";
						echo "</div>";
						

						for ($i=0; $i < $cuota; $i++) { 

							$seguro_mensual = 0.00;
							$gps_mensual = 0.00;

							if($i==0){
								$new_date = $fecha;
								$capital = $valor/$cambio;
							}

							$new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

							$cuota_pagar = 0.00;
							$cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor/$cambio)/((pow(1+($interes/100),$cuota))-1);
	
							$sobrecosto = 0.00;
							$sobrecosto = $capital*($interes/100);

							$amortizacion = 0.00;
							if($cuota_pagar>0){
								$amortizacion = $cuota_pagar - $sobrecosto;
							}

							// GERSON (10-11-23)
							/* if($seguro_porcentaje > 0){
								$seguro_mensual = formato_moneda($seguro_porcentaje * $cuota_pagar / 100);
								$sobrecosto = $sobrecosto + $seguro_mensual;
								$cuota_pagar = $cuota_pagar + $seguro_mensual;
							} */

							if($chk_gps > 0){
								if($gps_precio > 0 && $cuota > 0){
									//$gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
									$gps_mensual = formato_moneda($gps_precio / $cuota);
									$cuota_pagar = $cuota_pagar + $gps_mensual;
								}
							}
							//
	
							$html .= '<tr id="tabla_cabecera_fila">
										<td id="tabla_fila" style="">'.$new_date.'</td>
										<td id="tabla_fila" style="">'.($i+1).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($capital, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($amortizacion, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($sobrecosto, 2).'</td>
										<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($gps_mensual, 2).'</td>
										<td id="tabla_fila" style="text-align: right; background: #FFE6CA;">'.$mon.''.number_format($cuota_pagar, 2).'</td>
										<td id="tabla_fila" style="text-align: right; background: #D0F6E6;">'.$sub_mon.''.number_format($cuota_pagar*$cambio, 2).'</th>';
							$html .= '</tr>';
	
							$capital = $capital - $amortizacion;
							$total_sobrecosto = $total_sobrecosto + $sobrecosto;
							$total_cuotas = $total_cuotas + $cuota_pagar - $gps_mensual;
						}

						echo "<div class='col-md-12' style='padding-top: 3px; padding-bottom: 7px;'>";
						echo "<span class='h4' style='padding: 5px;'>N° Cuotas: <strong>".$cuota."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'>Capital <strong>".$mon.''.number_format($valor*$cambio, 2)."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'>Sobrecosto Total: <strong>".$mon.''.number_format($total_sobrecosto, 2)."</strong></span> | ";
						echo "<span class='h4' style='padding: 5px;'><strong>TOTAL: ".$mon.''.number_format($total_cuotas, 2)."</strong></span><br><br>";
						echo "<span class='h5' style='padding: 5px; color:green'><strong>".$span_seguro_gps."</strong></span>";
						echo "</div>";

					}
						

		$html .= '		</tbody>
					</table>
				</div>';

		echo $html;
  	}

	if($action=="registrar_precalificacion"){
		
		$proceso_id = intval($_POST['hdd_preca_proceso_id']);
		$usuario_id = intval($_POST['hdd_preca_usuario_id']);
		$proceso_fase_item_id = intval($_POST['hdd_preca_proceso_fase_item_id']);

		// Cliente
		$modalidad 		= $_POST['cli_modalidad']; // presencial, virtual, exonerada
		$tipo_cliente 	= $_POST['cli_tipo_cliente']; // juridica, natural
		$nacionalidad 	= $_POST['cli_nacionalidad'];
		$edad 			= $_POST['cli_edad'] ? intval($_POST['cli_edad']) : NULL;
		$razon 			= $_POST['cli_razon'];
		$ruc 			= $_POST['cli_ruc'] ? intval($_POST['cli_ruc']) : NULL;
		$cliente 		= $_POST['cli_cliente'];
		$dni 			= $_POST['cli_dni'] ? intval($_POST['cli_dni']) : NULL;
		// Vehiculo
		$marca 				= $_POST['cli_marca'] ? intval($_POST['cli_marca']) : NULL;
		$modelo 			= $_POST['cli_modelo'] ? intval($_POST['cli_modelo']) : NULL;
		$proveedor_id 		= $_POST['cli_proveedor_id'] ? intval($_POST['cli_proveedor_id']) : NULL;
		$proveedor 			= $_POST['cli_proveedor'];
		$anio 				= $_POST['cli_anio'] ? intval($_POST['cli_anio']) : NULL;
		$licencia 			= $_POST['cli_licencia']; // si o no
		$tipo_licencia 		= $_POST['cli_tipo_licencia'];
		$vigencia_licencia 	= $_POST['cli_vigencia_licencia'] ? fecha_mysql($_POST['cli_vigencia_licencia']) : NULL;
		$papeleta 			= $_POST['cli_papeleta']; // si o no
		$monto 				= $_POST['cli_monto'] ? floatval($_POST['cli_monto']) : NULL;
		$cuota 				= $_POST['cli_cuota'] ? floatval($_POST['cli_cuota']) : NULL;
		$tasa 				= $_POST['cli_tasa'] ? floatval($_POST['cli_tasa']) : NULL;
		$plazo 				= $_POST['cli_plazo'] ? intval($_POST['cli_plazo']) : NULL;
		$km 				= $_POST['cli_km'] ? floatval($_POST['cli_km']) : NULL;
		$placa 				= $_POST['cli_placa'];
		$precio_propuesto 	= $_POST['cli_precio'] ? floatval($_POST['cli_precio']) : NULL;
		$verificado 		= $_POST['cli_verificado']; // si o no
		$metodo_verificado 	= $_POST['cli_metodo_ver'];
		$tipo_operacion 	= $_POST['cli_tipo_operacion'] ? intval($_POST['cli_tipo_operacion']) : NULL; // 1=pre consti, 2=consti s/custodia, 3=consti c/custodia, 4=compra s/custodia, 5=compra c/custodia, 6=adenda, 7=ipdn venta
		$comentario 		= $_POST['cli_comentario'];
		$vigencia_str 		= $_POST['cli_vigencia_str'];
		$vigencia_gps 		= $_POST['cli_vigencia_gps'];
		$inicial_moneda 	= $_POST['cli_inicial_moneda']; // sol, dolar
		$inicial_porcentaje = $_POST['cli_inicial_porcentaje'] ? floatval($_POST['cli_inicial_porcentaje']) : NULL;
		$inicial_monto 		= $_POST['cli_inicial_monto'] ? floatval($_POST['cli_inicial_monto']) : NULL;
		$fec_inicial 		= $_POST['cli_inicial_fec'] ? fecha_mysql($_POST['cli_inicial_fec']) : NULL;
		$inicial_est 		= $_POST['cli_inicial_est'];
		$finalidad 			= $_POST['cli_finalidad'];
		$moneda 			= $_POST['cli_moneda']; // sol, dolar
		$uso 				= $_POST['cli_uso'];
		// Datos Cliente
		$ocupacion 			= $_POST['cli_ocupacion'];
		$grado_instruccion 	= $_POST['cli_grado_instruccion'];
		$carga_familiar 	= $_POST['cli_carga_familiar'];
		$carga_familiar_e 	= $_POST['cli_carga_familiar_e'];
		$descuento	 		= $_POST['cli_descuento'] ? floatval($_POST['cli_descuento']) : NULL;
		$estado_civil 		= $_POST['cli_estado_civil'];
		$gastos 			= $_POST['cli_gastos'];
		$direccion 			= $_POST['cli_direccion'];
		$vivienda 			= $_POST['cli_vivienda']; // propia, alquilada, familiar
		$tipo_resi			= $_POST['cli_tipo_resi'] ? intval($_POST['cli_tipo_resi']) : NULL; // 1=casa/dpto, 2=urbano/resid, 3=modulo/terreno, 4=rural. 5=habitacion, 6=otro
		$tipo_describir 	= $_POST['cli_tipo_describir'];
		$fec_inspeccion 	= $_POST['cli_inspeccion_domi'] ? fecha_mysql($_POST['cli_inspeccion_domi']) : NULL;
		$uso_vehiculo 		= $_POST['cli_uso_vehiculo'];
		$zona_vehiculo 		= $_POST['cli_zona_vehiculo'];
		$semaforizacion 	= $_POST['cli_semaforizacion'];
		$score				= $_POST['cli_score'] ? intval($_POST['cli_score']) : NULL;
		$cargo_antiguedad 	= $_POST['cli_cargo_antiguedad'];
		$ingreso_forma		= $_POST['cli_ingreso_forma'];
		$casa_antiguedad	= $_POST['cli_casa_antiguedad'];
		
		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar pre calificación.';
		
		$res= $oProceso->insertarPreCalificación($proceso_id, $modalidad , $tipo_cliente, $nacionalidad, $edad, $razon, $ruc, $cliente, $dni, $marca, $modelo, $proveedor_id, $anio, $licencia, $tipo_licencia, $vigencia_licencia, $papeleta, $monto, $cuota, $tasa, $plazo, $km, $placa, $precio_propuesto, $verificado, $metodo_verificado, $tipo_operacion, $comentario, $vigencia_str, $vigencia_gps, $inicial_moneda, $inicial_porcentaje, $inicial_monto, $fec_inicial, $inicial_est, $finalidad, $moneda, $uso, $ocupacion, $grado_instruccion, $carga_familiar, $carga_familiar_e, $descuento, $estado_civil, $gastos, $direccion, $vivienda, $tipo_resi, $tipo_describir, $fec_inspeccion, $uso_vehiculo, $zona_vehiculo, $semaforizacion, $score, $cargo_antiguedad, $ingreso_forma, $casa_antiguedad);
 		if(intval($res['estado']) == 1){

			$usuario_precali = $oUsuario->mostrarUno($_SESSION['usuario_id']);
			if($usuario_precali['estado'] == 1){
				$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
				$oHistorial->setTbHistNomTabla("tb_proc_precalificacion");
				$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
				$oHistorial->setTbHistDet('El usuario <b>'.$usuario_precali['data']['tb_usuario_nom'].' '.$usuario_precali['data']['tb_usuario_ape'].'</b> ha realizado la creación de la pre calificación con fecha <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();
			}

 			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['precalificacion_id'] = $res['precalificacion_id']; // precalificacion registrado
		}

		echo json_encode($data);

	}

	if($action=="actualizar_precalificacion"){

		$proceso_id = intval($_POST['hdd_preca_proceso_id']);
		$usuario_id = intval($_POST['hdd_preca_usuario_id']);
		$proceso_fase_item_id = intval($_POST['hdd_preca_proceso_fase_item_id']);
		$precalificacion_id = intval($_POST['hdd_preca_precalificacion_id']);

		// Cliente
		$modalidad 		= $_POST['cli_modalidad']; // presencial, virtual, exonerada
		$tipo_cliente 	= $_POST['cli_tipo_cliente']; // juridica, natural
		$nacionalidad 	= $_POST['cli_nacionalidad'];
		$edad 			= $_POST['cli_edad'] ? intval($_POST['cli_edad']) : NULL;
		$razon 			= $_POST['cli_razon'];
		$ruc 			= $_POST['cli_ruc'] ? $_POST['cli_ruc'] : NULL;
		$cliente 		= $_POST['cli_cliente'];
		$dni 			= $_POST['cli_dni'] ? intval($_POST['cli_dni']) : NULL;
		// Vehiculo
		$marca 				= $_POST['cli_marca'] ? intval($_POST['cli_marca']) : NULL;
		$modelo 			= $_POST['cli_modelo'] ? intval($_POST['cli_modelo']) : NULL;
		$proveedor_id 		= $_POST['cli_proveedor_id'] ? intval($_POST['cli_proveedor_id']) : NULL;
		$proveedor 			= $_POST['cli_proveedor'];
		$anio 				= $_POST['cli_anio'] ? intval($_POST['cli_anio']) : NULL;
		$licencia 			= $_POST['cli_licencia']; // si o no
		$tipo_licencia 		= $_POST['cli_tipo_licencia'];
		$vigencia_licencia 	= $_POST['cli_vigencia_licencia'] ? fecha_mysql($_POST['cli_vigencia_licencia']) : NULL;
		$papeleta 			= $_POST['cli_papeleta']; // si o no
		$monto 				= $_POST['cli_monto'] ? floatval($_POST['cli_monto']) : NULL;
		$cuota 				= $_POST['cli_cuota'] ? floatval($_POST['cli_cuota']) : NULL;
		$tasa 				= $_POST['cli_tasa'] ? floatval($_POST['cli_tasa']) : NULL;
		$plazo 				= $_POST['cli_plazo'] ? intval($_POST['cli_plazo']) : NULL;
		$km 				= $_POST['cli_km'] ? floatval($_POST['cli_km']) : NULL;
		$placa 				= $_POST['cli_placa'];
		$precio_propuesto 	= $_POST['cli_precio'] ? floatval($_POST['cli_precio']) : NULL;
		$verificado 		= $_POST['cli_verificado']; // si o no
		$metodo_verificado 	= $_POST['cli_metodo_ver'];
		$tipo_operacion 	= $_POST['cli_tipo_operacion'] ? intval($_POST['cli_tipo_operacion']) : NULL; // 1=pre consti, 2=consti s/custodia, 3=consti c/custodia, 4=compra s/custodia, 5=compra c/custodia, 6=adenda, 7=ipdn venta
		$comentario 		= $_POST['cli_comentario'];
		$vigencia_str 		= $_POST['cli_vigencia_str'];
		$vigencia_gps 		= $_POST['cli_vigencia_gps'];
		$inicial_moneda 	= $_POST['cli_inicial_moneda']; // sol, dolar
		$inicial_porcentaje = $_POST['cli_inicial_porcentaje'] ? floatval($_POST['cli_inicial_porcentaje']) : NULL;
		$inicial_monto 		= $_POST['cli_inicial_monto'] ? floatval($_POST['cli_inicial_monto']) : NULL;
		$fec_inicial 		= $_POST['cli_inicial_fec'] ? fecha_mysql($_POST['cli_inicial_fec']) : NULL;
		$inicial_est 		= $_POST['cli_inicial_est'];
		$finalidad 			= $_POST['cli_finalidad'];
		$moneda 			= $_POST['cli_moneda']; // sol, dolar
		$uso 				= $_POST['cli_uso'];
		// Datos Cliente
		$ocupacion 			= $_POST['cli_ocupacion'];
		$grado_instruccion 	= $_POST['cli_grado_instruccion'];
		$carga_familiar 	= $_POST['cli_carga_familiar'];
		$carga_familiar_e 	= $_POST['cli_carga_familiar_e'];
		$descuento	 		= $_POST['cli_descuento'] ? floatval($_POST['cli_descuento']) : NULL;
		$estado_civil 		= $_POST['cli_estado_civil'];
		$gastos 			= $_POST['cli_gastos'];
		$direccion 			= $_POST['cli_direccion'];
		$vivienda 			= $_POST['cli_vivienda']; // propia, alquilada, familiar
		$tipo_resi			= $_POST['cli_tipo_resi'] ? intval($_POST['cli_tipo_resi']) : NULL; // 1=casa/dpto, 2=urbano/resid, 3=modulo/terreno, 4=rural. 5=habitacion, 6=otro
		$tipo_describir 	= $_POST['cli_tipo_describir'];
		$fec_inspeccion 	= $_POST['cli_inspeccion_domi'] ? fecha_mysql($_POST['cli_inspeccion_domi']) : NULL;
		$uso_vehiculo 		= $_POST['cli_uso_vehiculo'];
		$zona_vehiculo 		= $_POST['cli_zona_vehiculo'];
		$semaforizacion 	= $_POST['cli_semaforizacion'];
		$score				= $_POST['cli_score'] ? intval($_POST['cli_score']) : NULL;
		$cargo_antiguedad 	= $_POST['cli_cargo_antiguedad'];
		$ingreso_forma		= $_POST['cli_ingreso_forma'];
		$casa_antiguedad	= $_POST['cli_casa_antiguedad'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar pre calificación.';
		
		$res= $oProceso->modificarPreCalificación($precalificacion_id, $proceso_id, $modalidad , $tipo_cliente, $nacionalidad, $edad, $razon, $ruc, $cliente, $dni, $marca, $modelo, $proveedor_id, $anio, $licencia, $tipo_licencia, $vigencia_licencia, $papeleta, $monto, $cuota, $tasa, $plazo, $km, $placa, $precio_propuesto, $verificado, $metodo_verificado, $tipo_operacion, $comentario, $vigencia_str, $vigencia_gps, $inicial_moneda, $inicial_porcentaje, $inicial_monto, $fec_inicial, $inicial_est, $finalidad, $moneda, $uso, $ocupacion, $grado_instruccion, $carga_familiar, $carga_familiar_e, $descuento, $estado_civil, $gastos, $direccion, $vivienda, $tipo_resi, $tipo_describir, $fec_inspeccion, $uso_vehiculo, $zona_vehiculo, $semaforizacion, $score, $cargo_antiguedad, $ingreso_forma, $casa_antiguedad);
 		if(intval($res['estado']) == 1){

			$usuario_precali = $oUsuario->mostrarUno($_SESSION['usuario_id']);
			if($usuario_precali['estado'] == 1){
				$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
				$oHistorial->setTbHistNomTabla("tb_proc_precalificacion");
				$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
				$oHistorial->setTbHistDet('El usuario <b>'.$usuario_precali['data']['tb_usuario_nom'].' '.$usuario_precali['data']['tb_usuario_ape'].'</b> ha realizado cambios en la pre calificación con fecha <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();
			}

 			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['precalificacion_id'] = $res['precalificacion_id']; // precalificacion registrado
		}

		echo json_encode($data);

	}

	if($action=="guardar_campo_simulador"){

		$proceso_id = intval($_POST['proceso_id']);
		$usuario_id	= $_SESSION['usuario_id']; 
		$id 		= $_POST['id'];
		$tipo_dato 	= $_POST['tipo_dato'];
		$monto_ini  = 0.00;
		$tipo 		= '';
		$campo 		= '';

		if($tipo_dato=='text'){
			$valor = $_POST['value'];
			$tipo = 'STR';
		}elseif($tipo_dato=='int'){
			$valor = intval($_POST['value']);
			$tipo = 'INT';
		}elseif($tipo_dato=='float'){
			$valor = floatval($_POST['value']);
			$tipo = 'STR';
		}

		if($id=='simu_moneda'){
			$campo = 'tb_proceso_moneda';
		}elseif($id=='simu_cambio'){
			$campo = 'tb_proceso_tip_cambio';
		}elseif($id=='simu_cuota'){
			$campo = 'tb_proceso_plazo';
		}elseif($id=='simu_interes'){
			$campo = 'tb_proceso_tasa';
		}elseif($id=='simu_costo'){
			$campo = 'tb_proceso_monto';
		}elseif($id=='simu_inicial'){
			$campo = 'tb_proceso_inicial_porcen';
		}elseif($id=='simu_inicial_monto'){
			$campo = 'tb_proceso_inicial';
		}elseif($id=='simu_valor'){
			$campo = 'tb_proceso_prestamo';
		}

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar datos.';

		$proceso = $oProceso->mostrarUno($proceso_id);

 		if($oProceso->modificar_campo_proceso($proceso_id, $campo, $valor, $tipo)){

			//$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_fecreg', $proceso['data']['tb_proceso_fecreg'], 'STR'); // existe un pequeño error que al modificar cualquier campo se modifica tmb el fecreg sin razon

			//if($id=='simu_valor'){
				if($proceso['estado'] == 1){
					//$monto_ini = floatval($proceso['data']['tb_proceso_monto']) - floatval($proceso['data']['tb_proceso_prestamo']);
					/* $monto_ini = floatval($proceso['data']['tb_proceso_monto']) - ( floatval($proceso['data']['tb_proceso_monto']) - (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100)) );
					if($monto_ini > 0){
						$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_inicial', $monto_ini, 'INT');
						//$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_fecreg', $proceso['data']['tb_proceso_fecreg'], 'STR'); // existe un pequeño error que al modificar cualquier campo se modifica tmb el fecreg sin razon
					} */
					if($id=='simu_inicial_monto'){ // si se modifica monto inicial, recalcular monto de prestamo
						$new_prestamo = floatval($proceso['data']['tb_proceso_monto']) - $valor;
						$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_prestamo', $new_prestamo, 'INT');
					}
				}

			//}

			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['proceso_id'] = $proceso_id;
		}

		echo json_encode($data);


	}

	if($action=="registrar_ingreso"){
		
		$proceso_id = intval($_POST['hdd_i_proceso_id']);
		$cliente_id = intval($_POST['hdd_i_cliente_id']);
		$usuario_id = intval($_POST['hdd_i_usuario_id']);
		$proceso_fase_item_id = intval($_POST['hdd_i_proceso_fase_item_id']);
		
		$monto 			= floatval($_POST['monto_i']);
		$moneda 		= $_POST['moneda_i'];
		$origen 		= $_POST['origen_i'];
		$antiguedad 	= $_POST['antiguedad_i'];
		$sustento 		= $_POST['sustento_i'];
		$verificacion 	= $_POST['verificacion_i'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar ingreso.';

		 if($oProceso->registrarIngresoCliente($proceso_id, $cliente_id, $moneda, $monto, $origen, $antiguedad, $sustento, $verificacion)){

			$usuario_ie = $oUsuario->mostrarUno($usuario_id);
			if($usuario_id>0){
				$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
				$oHistorial->setTbHistNomTabla("tb_proc_ingresoegreso");
				$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
				$oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado cambios en los ingresos del cliente con fecha <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();
			}


			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['proceso_id'] = $proceso_id;
		}

		echo json_encode($data);

	}

	if($action=="registrar_ingreso_detalle"){
		
		$proceso_id = intval($_POST['hdd_i_proceso_d_id']);
		$cliente_id = intval($_POST['hdd_i_cliente_d_id']);
		$usuario_id = intval($_POST['hdd_i_usuario_d_id']);
		$proceso_fase_item_id = intval($_POST['hdd_i_proceso_fase_item_d_id']);
		
		$detalle 	= $_POST['detalle_i'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar detalle de ingreso.';

		$proceso = $oProceso->mostrarUno($proceso_id);

		if($oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_ing_det', $detalle, 'STR')){

			//$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_fecreg', $proceso['data']['tb_proceso_fecreg'], 'STR'); // existe un pequeño error que al modificar cualquier campo se modifica tmb el fecreg sin razon

			$usuario_ie = $oUsuario->mostrarUno($usuario_id);
			if($usuario_id>0){
				$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
				$oHistorial->setTbHistNomTabla("tb_proc_ingresoegreso");
				$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
				$oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado cambios en los ingresos del cliente con fecha <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();
			}

			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['proceso_id'] = $proceso_id;

		}

		echo json_encode($data);

	}

	if($action=="registrar_egreso"){

		$proceso_id = intval($_POST['hdd_e_proceso_id']);
		$cliente_id = intval($_POST['hdd_e_cliente_id']);
		$usuario_id = intval($_POST['hdd_e_usuario_id']);
		$proceso_fase_item_id = intval($_POST['hdd_e_proceso_fase_item_id']);
		
		$subtipo 		= $_POST['tipo_e'];
		$monto 			= floatval($_POST['monto_e']);
		$montome		= floatval($_POST['montome_e']);
		$moneda 		= $_POST['moneda_e'];
		$entidad 		= $_POST['entidad_e'];
		if($subtipo=='familiar'){
			$detalle 	= $_POST['familiar_det_e'];
		}elseif($subtipo=='empresa'){
			$detalle 	= $_POST['empresa_det_e'];
		}
		$finalidad 		= $_POST['finalidad_e'];
		$frecuencia 	= $_POST['frecuencia_e'];
		$sustento 		= $_POST['sustento_e'];
		$verificacion 	= $_POST['verificacion_e'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar egreso.';

		 if($oProceso->registrarEgresoCliente($proceso_id, $cliente_id, $moneda, $monto, $montome, $subtipo, $entidad, $detalle, $finalidad, $frecuencia, $sustento, $verificacion)){

			$usuario_ie = $oUsuario->mostrarUno($usuario_id);
			if($usuario_id>0){
				$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
				$oHistorial->setTbHistNomTabla("tb_proc_ingresoegreso");
				$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
				$oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado cambios en los egresos del cliente con fecha <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();
			}


			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['proceso_id'] = $proceso_id;
		}

		echo json_encode($data);

	}

	if($action=="detalle_ingresos_egresos_cliente"){

		$proceso_id = intval($_POST['proceso_id']);
		$usuario_id = intval($_POST['usuario_id']);
		$tipo_cambio = floatval($_POST['tipo_cambio']);
		$type = $_POST['type'];

		$proceso = $oProceso->mostrarUno($proceso_id);
		$ingresos_egresos = $oProceso->mostrarIEXProceso($proceso_id, $type);

		$color = '';
		if($type=='I'){$color = '#D0F6E6';}else{$color = '#F6D0D0';}
		
		$html = '';
		$html .= '<div class="col-md-12">
					<table class="table table-hover table-responsive">
						<thead>
							<tr id="tabla_cabecera">';
							if($type=='I'){
								$html .= '<th id="tabla_cabecera_fila">#</th>
									<th id="tabla_cabecera_fila">Fecha Registro</th>
									<th id="tabla_cabecera_fila">Ingreso</th>
									<th id="tabla_cabecera_fila">Origen</th>
									<th id="tabla_cabecera_fila">Antigüedad</th>
									<th id="tabla_cabecera_fila">Sustento</th>
									<th id="tabla_cabecera_fila">Comentario</th>
									<th id="tabla_cabecera_fila"></th>';
							}else{
								$html .= '<th id="tabla_cabecera_fila">#</th>
									<th id="tabla_cabecera_fila">Fecha Registro</th>
									<th id="tabla_cabecera_fila">Gasto</th>
									<th id="tabla_cabecera_fila">Finalidad</th>
									<th id="tabla_cabecera_fila">Frecuencia</th>
									<th id="tabla_cabecera_fila">Sustento</th>
									<th id="tabla_cabecera_fila">Comentario</th>
									<th id="tabla_cabecera_fila"></th>';
							}
					
				$html .= '</tr>
						</thead>
						<tbody>';

						if($ingresos_egresos['estado']==1){
							$i = 0;
							$total_ingresoegreso = 0.00;
							foreach ($ingresos_egresos['data'] as $key => $value) {

								$mon = '';
								if($value['tb_ingresoegreso_moneda']=='sol'){
									$mon = 'S/ ';
								}else{
									$mon = '$ ';
								}

								if($type=='I'){
									$html .= '<tr id="tabla_cabecera_fila">
											<td id="tabla_fila" style="">'.($i+1).'</td>
											<td id="tabla_fila" style="text-align: left;">'.mostrar_fecha_hora($value['tb_ingresoegreso_fecreg']).'</td>
											<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($value['tb_ingresoegreso_monto']+($value['tb_ingresoegreso_montome']*$tipo_cambio), 2).'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_origen'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_antiguedad'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_sustento'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_verificacion'].'</td>
											<td id="tabla_fila">
												<button 
													class="btn btn-danger btn-xs" 
													type="button"
													name="eliminar_i"
													id="eliminar_i"
													title="Eliminar"
													onClick="eliminar_ingresoegreso('.$value['tb_ingresoegreso_id'].', \'I\')">
													<span class="fa fa-trash"></span>
												</button>
											</td>';
								}else{
									$html .= '<tr id="tabla_cabecera_fila">
											<td id="tabla_fila" style="">'.($i+1).'</td>
											<td id="tabla_fila" style="text-align: left;">'.mostrar_fecha_hora($value['tb_ingresoegreso_fecreg']).'</td>
											<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($value['tb_ingresoegreso_monto']+($value['tb_ingresoegreso_montome']*$tipo_cambio), 2).'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_finalidad'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_frecuencia'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_sustento'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_verificacion'].'</td>
											<td id="tabla_fila">
												<button 
													class="btn btn-danger btn-xs" 
													type="button" 
													name="eliminar_i" 
													id="eliminar_i" 
													title="Eliminar"
													onClick="eliminar_ingresoegreso('.$value['tb_ingresoegreso_id'].', \'E\')">
													<span class="fa fa-trash"></span>
												</button>
											</td>';
								}

								
								
								$html .= '</tr>';
								$total_ingresoegreso = $total_ingresoegreso + $value['tb_ingresoegreso_monto'] + ($value['tb_ingresoegreso_montome']*$tipo_cambio);
								$i++;			
							}
						}
						

						
		$html .= '		</tbody>';
		$html .= '		<tfoot>';
					$html .= '<tr id="tabla_cabecera_fila">
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila" style="font-size:12px; text-align: right; background:'.$color.' ;">'.$mon.''.number_format($total_ingresoegreso, 2).'</th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>';
					
				$html .= '</tr></tfoot>
					</table>
				</div>';

		$data['html'] = $html;
		$data['total_ingresoegreso'] = $total_ingresoegreso;
		echo json_encode($data);
  	}

	if($action=="detalle_ingresos_egresos_antiguo"){

		$proceso_id = intval($_POST['proceso_id']);
		$tipo_cambio = floatval($_POST['tipo_cambio']);
		$type = $_POST['type'];

		$proceso = $oProceso->mostrarUno($proceso_id);
		$ingresos_egresos = $oProceso->mostrarIEXProceso($proceso_id, $type);

		$color = '';
		if($type=='I'){$color = '#D0F6E6';}else{$color = '#F6D0D0';}
		
		$html = '';
		$html .= '<div class="col-md-12">
					<table class="table table-hover table-responsive">
						<thead>
							<tr id="tabla_cabecera">';
							if($type=='I'){
								$html .= '<th id="tabla_cabecera_fila">#</th>
									<th id="tabla_cabecera_fila">Fecha Registro</th>
									<th id="tabla_cabecera_fila">Ingreso</th>
									<th id="tabla_cabecera_fila">Origen</th>
									<th id="tabla_cabecera_fila">Antigüedad</th>
									<th id="tabla_cabecera_fila">Sustento</th>
									<th id="tabla_cabecera_fila">Comentario</th>';
							}else{
								$html .= '<th id="tabla_cabecera_fila">#</th>
									<th id="tabla_cabecera_fila">Fecha Registro</th>
									<th id="tabla_cabecera_fila">Gasto</th>
									<th id="tabla_cabecera_fila">Finalidad</th>
									<th id="tabla_cabecera_fila">Frecuencia</th>
									<th id="tabla_cabecera_fila">Sustento</th>
									<th id="tabla_cabecera_fila">Comentario</th>';
							}
					
				$html .= '</tr>
						</thead>
						<tbody>';

						if($ingresos_egresos['estado']==1){
							$i = 0;
							$total_ingresoegreso = 0.00;
							foreach ($ingresos_egresos['data'] as $key => $value) {

								$mon = '';
								if($value['tb_ingresoegreso_moneda']=='sol'){
									$mon = 'S/ ';
								}else{
									$mon = '$ ';
								}

								if($type=='I'){
									$html .= '<tr id="tabla_cabecera_fila">
											<td id="tabla_fila" style="">'.($i+1).'</td>
											<td id="tabla_fila" style="text-align: left;">'.mostrar_fecha_hora($value['tb_ingresoegreso_fecreg']).'</td>
											<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($value['tb_ingresoegreso_monto']+($value['tb_ingresoegreso_montome']*$tipo_cambio), 2).'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_origen'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_antiguedad'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_sustento'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_verificacion'].'</td>';
								}else{
									$html .= '<tr id="tabla_cabecera_fila">
											<td id="tabla_fila" style="">'.($i+1).'</td>
											<td id="tabla_fila" style="text-align: left;">'.mostrar_fecha_hora($value['tb_ingresoegreso_fecreg']).'</td>
											<td id="tabla_fila" style="text-align: right;">'.$mon.''.number_format($value['tb_ingresoegreso_monto']+($value['tb_ingresoegreso_montome']*$tipo_cambio), 2).'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_finalidad'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_frecuencia'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_sustento'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_ingresoegreso_verificacion'].'</td>';
								}

								
								
								$html .= '</tr>';
								$total_ingresoegreso = $total_ingresoegreso + $value['tb_ingresoegreso_monto'] + ($value['tb_ingresoegreso_montome']*$tipo_cambio);
								$i++;			
							}
						}
						

						
		$html .= '		</tbody>';
		$html .= '		<tfoot>';
					$html .= '<tr id="tabla_cabecera_fila">
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila" style="font-size:12px; text-align: right; background:'.$color.' ;">'.$mon.''.number_format($total_ingresoegreso, 2).'</th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>
								<th id="tabla_cabecera_fila"></th>';
					
				$html .= '</tr></tfoot>
					</table>
				</div>';

		$data['html'] = $html;
		$data['total_ingresoegreso'] = $total_ingresoegreso;
		echo json_encode($data);
  	}
	

	if ($action == 'eliminar_ingresoegreso') {

		$ingresoegreso_id = intval($_POST['ingresoegreso_id']);
	
		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al eliminar el registro.';


		if ($oProceso->eliminarIngresoEgreso($ingresoegreso_id)) {

			$data['estado'] = 1;
			$data['mensaje'] = 'Registro eliminado correctamente.';

		}
	
		echo json_encode($data);
	}

	if($action=="detalle_participante_cliente"){

		$proceso_id = intval($_POST['proceso_id']);

		$proceso = $oProceso->mostrarUno($proceso_id);
		$participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
		$usuario_id = 0;
		if($proceso['estado'] == 1){
			$usuario_id = $proceso['data']['tb_usuario_id'];
		}
        $usuario_id_session = $_SESSION['usuario_id']; 

		$html = '';
		$html .= '<div class="col-md-12">
					<table class="table table-hover table-responsive">
						<thead>
							<tr id="tabla_cabecera">
								<th id="tabla_cabecera_fila">#</th>
								<th id="tabla_cabecera_fila">Participante</th>
								<th id="tabla_cabecera_fila">DNI</th>
								<th id="tabla_cabecera_fila">Celular</th>
								<th id="tabla_cabecera_fila"></th>
							</tr>
						</thead>';
					
				
				$html .= '<tbody>';

						if($participantes['estado']==1){
							foreach ($participantes['data'] as $key => $value) {

								$html .= '<tr id="tabla_cabecera_fila">
											<td id="tabla_fila" style="">'.($i+1).'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_participante_nom'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_participante_doc'].'</td>
											<td id="tabla_fila" style="text-align: left;">'.$value['tb_participante_tel'].'</td>
											<td id="tabla_fila">
												<button 
													class="btn btn-danger btn-xs" 
													type="button" 
													name="eliminar_p" 
													id="eliminar_p" 
													title="Eliminar"
													onClick="eliminar_participante('.$value['tb_participante_id'].', '.$usuario_id.', '.$usuario_id_session.', \'E\')">
													<span class="fa fa-trash"></span>
												</button>
											</td>
										</tr>';
								$i++;			
							}
						}
						
				$html .= '</tbody>
					</table>
				</div>';

		echo $html;
  	}

	  if($action=="registrar_participante"){
		
		$proceso_id = intval($_POST['hdd_participante_proceso_id']);
		$nombre = $_POST['nombre_p'];
		$documento = $_POST['documento_p'];
		$celular = $_POST['celular_p'];
		
		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar participante.';

		 if($oProceso->registrarParticipanteCliente($proceso_id, $nombre, $documento, $celular)){

			$data['estado'] = 1;
 			$data['mensaje'] = 'Datos guardados correctamente.';
			$data['proceso_id'] = $proceso_id;
		}

		echo json_encode($data);

	}

	if ($action == 'eliminar_participante') {

		$participante_id = intval($_POST['participante_id']);
	
		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al eliminar el registro.';


		if ($oProceso->eliminarParticipante($participante_id)) {

			$data['estado'] = 1;
			$data['mensaje'] = 'Registro eliminado correctamente.';

		}
	
		echo json_encode($data);
	}

	
	if ($action == 'insertar_credito_proceso') {

		$proceso_id = intval($_POST['proceso_id']);
		$credito_id = intval($_POST['credito_id']);
	
		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al actualizazr el proceso.';
		
		$proceso = $oProceso->mostrarUno($proceso_id);
		
		if ($oProceso->modificar_campo_proceso($proceso_id, 'tb_credito_id', $credito_id, 'INT')) {
			$precredito = $oPrecred->mostrarPreCreXProceso($proceso_id);
			if($precredito['estado']==1){
				$oPrecred->modificar_campo($precredito['data']['tb_precred_id'], 'tb_credito_id', $credito_id, 'INT'); // existe un pequeño error que al modificar cualquier campo se modifica tmb el fecreg sin razon

			}


			$data['estado'] = 1;
			$data['mensaje'] = 'Proceso actualizado correctamente.';

		}
	
		echo json_encode($data);
	}

	if($action=="guardar_datos_chequedetalle"){
		
		$proceso_id 			= intval($_POST['proceso_id']);
		$usuario_id 			= intval($_POST['usuario_id']);
		$proceso_fase_item_id	= intval($_POST['proceso_fase_item_id']);
		$chequedetalle_id 		= intval($_POST['chequedetalle_id']);
		$empresa_id 			= intval($_POST['empresa_id']);
		$num_cheque 			= $_POST['num_cheque'];
		$uso 					= $_POST['uso'];
		$fecha					= fecha_mysql($_POST['fecha_subida']);
		
		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al subir cheque.';

		$chequedetalle = $oProceso->mostrarChequeDetalle($chequedetalle_id);
		$nuevo = false;
		if($chequedetalle['data']['tb_chequedetalle_nro_cheque'] == '' || $chequedetalle['data']['tb_chequedetalle_nro_cheque'] == NULL){
			$nuevo = true;
		}
		if($oProceso->registrarSubidaCheque($chequedetalle_id, $num_cheque, $uso, $empresa_id, $fecha)){

			$oHistorial->setTbHistUsureg($usuario_id);
			$oHistorial->setTbHistNomTabla("tb_proc_chequedetalle");
			$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
			if($nuevo){
				$oHistorial->setTbHistDet('Ha realizado la subida del <strong>Cheque N° '.$num_cheque.'</strong> del proceso N° '.$proceso_id.' | <b>'.$fecha_hoy.'</b>');
				$oHistorial->insertar();
			}else{
				if(($empresa_id != intval($chequedetalle['data']['tb_empresa_id'])) || ($num_cheque != $chequedetalle['data']['tb_chequedetalle_nro_cheque']) || ($uso != $chequedetalle['data']['tb_chequedetalle_uso'])){
					$oHistorial->setTbHistDet('Ha realizado la actualización del <strong>Cheque N° '.$num_cheque.'</strong> del proceso N° '.$proceso_id.' | <b>'.$fecha_hoy.'</b>');
					$oHistorial->insertar();
				}
			}

			$data['estado'] = 1;
 			$data['mensaje'] = 'Cheque subido o actualizado correctamente.';
			$data['proceso_id'] = $proceso_id;
			$data['usuario_id'] = $usuario_id;

		}

		echo json_encode($data);

	}

	if($action=="guardar_aprobacion_cheque"){
		
		$proceso_id 			= intval($_POST['proceso_id']);
		$usuario_id 			= intval($_SESSION['usuario_id']);
		$proceso_fase_item_id	= intval($_POST['proceso_fase_item_id']);
		$cheque_id 				= intval($_POST['cheque_id']);
		$aprobado 				= intval($_POST['aprobado']);
		$fecha 					= date('Y-m-d h:i:s');
		
		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al aprobar cheques.';


		// GERSON (06-04-24)
		if($aprobado == 2){

			$proceso = $oProceso->mostrarUno($proceso_id);
			$cliente_id = 0;
			if($proceso['estado'] == 1){
				$garvtipo_id = intval($proceso['data']['tb_cgarvtipo_id']);
				$cliente_id = intval($proceso['data']['tb_cliente_id']);
			}
			$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
			if($precalificacion['estado'] == 1){
				$cliente = $precalificacion['data']['tb_cgarvtipo_id'];
				$concesionaria_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
				$concesionaria = $oTransferente->mostrarUno($concesionaria_id);
				if($concesionaria['estado'] == 1){
					$concesionaria_nom = $concesionaria['data']['tb_transferente_nom'];
				}
			}
			$cheque_c = $oProceso->mostrarCabeceraChequeXProceso($proceso_id);
			if($cheque_c['estado'] == 1){
				$mon = $cheque_c['data']['tb_cheque_moneda'];
			}
			
			$cheques = $oProceso->mostrarDetalleCheque($cheque_id);
			if($cliente_id == 0){

			}else{

				if($cheques['estado'] == 1){
					foreach ($cheques['data'] as $key => $value) {
		
						$moneda = "";
						$moneda_id = 0;
						if($mon=='sol'){
							$moneda = "S/ ";
							$moneda_id = 1;
		
						}else{
							$moneda = "US$ ";
							$moneda_id = 2;
		
						}
		
						$detalle = "";
						$detalle = "INGRESO A CAJA DE CHEQUE DE GERENCIA Nº ".$value['tb_chequedetalle_nro_cheque']." ".$value['tb_chequedetalle_bene']." / CASO: ".$cliente.". ".$moneda." ".mostrar_moneda($value['tb_chequedetalle_monto']).' / FECHA REGISTRO DE CHEQUE: '.mostrar_fecha(date('Y-m-d'));
		
						/* $registro_cheque = $oIngreso->insertarChequeProceso(
							$usuario_id,
							date('Y-m-d'),
							8, // OTROS INGRESOS
							$value['tb_chequedetalle_nro_cheque'],
							$detalle,
							floatval($value['tb_chequedetalle_monto']),
							2, // INGRESO DE CAJA
							176, // CHEQUE ALMACENADO
							$cliente_id,
							1, // CAJA
							$moneda_id,
							0,
							0,
							$value['tb_empresa_id'],
							'',
							'',
							0,
							0,
							0,
							0,
							0,
							'',
							$value['tb_chequedetalle_id']); */

							if($value['tb_chequedetalle_nro_cheque'] == null || $value['tb_chequedetalle_nro_cheque'] == ''){
								$data['estado'] = 2;
 								$data['mensaje'] = 'Existen cheques sin <strong>"N° Cheque"</strong>.';
								echo json_encode($data);
								exit();
							}
							

							$oIngreso->ingreso_usureg = $usuario_id;
							$oIngreso->ingreso_usumod = $usuario_id;
							$oIngreso->ingreso_fec = date('Y-m-d');
							$oIngreso->documento_id = 8; // otros ingresos
							$oIngreso->ingreso_numdoc = $value['tb_chequedetalle_nro_cheque'];
							$oIngreso->ingreso_det = $detalle;
							$oIngreso->ingreso_imp = floatval($value['tb_chequedetalle_monto']);
							$oIngreso->cuenta_id = 2; // INGRESO DE CAJA
							$oIngreso->subcuenta_id = 176; // CHEQUE ALMACENADO
							$oIngreso->cliente_id = $cliente_id;
							$oIngreso->caja_id = 1;
							$oIngreso->moneda_id = $moneda_id;
							//valores que pueden ser cambiantes según requerimiento de ingreso
							$oIngreso->modulo_id = 0;
							$oIngreso->ingreso_modide = 0;
							$oIngreso->empresa_id = $value['tb_empresa_id'];
							$oIngreso->ingreso_fecdep = '';
							$oIngreso->ingreso_numope = '';
							$oIngreso->ingreso_mondep = 0;
							$oIngreso->ingreso_comi = 0;
							$oIngreso->cuentadeposito_id = 0;
							$oIngreso->banco_id = 0;
							$oIngreso->ingreso_ap = 0;
							$oIngreso->ingreso_detex = '';
							$oIngreso->chequedetalle_id = $value['tb_chequedetalle_id'];
						
							$registro_cheque=$oIngreso->insertarChequeProceso();

          					if($registro_cheque['estado'] || intval($registro_cheque['estado'])==1){ // si es true
								if($oProceso->registrarAprobacionCheque($cheque_id, $aprobado, $usuario_id, $fecha)){

									$oHistorial->setTbHistUsureg($usuario_id);
									$oHistorial->setTbHistNomTabla("tb_proc_chequedetalle");
									$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
									if($aprobado == 1){
										$oHistorial->setTbHistDet('Ha realizado la <strong>DESAPROBACIÓN</strong> de los cheques del proceso N° '.$proceso_id.' | <b>'.$fecha_hoy.'</b>');
										$data['mensaje'] = 'Los cheque han sido desaprobados correctamente.';
									}else{
										$oHistorial->setTbHistDet('Ha realizado la <strong>APROBACIÓN</strong> de los cheques del proceso N° '.$proceso_id.' | <b>'.$fecha_hoy.'</b>');
										$data['mensaje'] = 'Los cheque han sido aprobados correctamente.';
									}
									$oHistorial->insertar();

									$data['estado'] = 1;
									$data['proceso_id'] = $proceso_id;
									$data['usuario_id'] = $usuario_id;

								}
							}else{
								$data['estado'] = 2; // sin cliente
								$data['mensaje'] = 'No se encuentra cliente asociado al proceso GARVEH.';
								$data['proceso_id'] = $proceso_id;
								$data['usuario_id'] = $usuario_id;
							}
							
					}
				}

			}

		}else{

			$data['estado'] = 3; //retorno de aprobacion
			$data['mensaje'] = 'No se puede cancelar la aprobación de cheques.';

		}
		//

		echo json_encode($data);

	}

	if($action=="comprimir_archivos"){
		
		$proceso_id	= intval($_POST['proceso_id']);
		$items		= $_POST['items'];
		
		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al comprimir archivos.';
		
		$proceso = $oProceso->mostrarUno($proceso_id);
		$cliente_doc = '';
		if($proceso['estado']==1){
			$cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
			if($cliente['estado']==1){
				$cliente_doc = $cliente['data']['tb_cliente_doc'];
			}
		}
		$zip = new ZipArchive();
		$zip_name = "EXPEDIENTE_".$cliente_doc.".zip"; 
		$tmpFile = pathinfo($zip_name);
		if($zip->open($zip_name,ZipArchive::CREATE)) {
			foreach ($items as $key => $item) {
				if(intval($item['multi_exo'])==0){
					if($item['multi_url']=='...' || $item['multi_url']==''){
					}else{
						$zip->addFromString(quitar_caracteres_tildes($item['item_des']).".pdf","../../".$item['multi_url']);
					}
				}
			}
			$zip->close();

			$data['estado'] = 1;
			$data['archivo'] = $zip_name; 
			$data['mensaje'] = 'Archivos comprimidos correctamente.';
		}else{
			$data['mensaje'] = 'Error al crear archivo comprimido.';
		}

		ob_end_clean();
		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename="'.$zip_name.'"');
		header('Content-Length: ' . filesize($tmpFile));
		readfile($tmpFile);
		unlink($tmpFile);

		echo json_encode($data);

	}

	if($action=="limpiar_comprimido"){
		$archivo = $_POST['archivo'];
		unlink($archivo);
	}

	if($action=="comentario_precalificacion"){

		$proceso_id	= intval($_POST['hdd_proceso_id']);
		$usuario_id	= intval($_POST['hdd_usuario_id']);
		$tipo		= $_POST['tipo_coment'];
		$comentario	= $_POST['txt_comentario'];
		
		$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
		$usuario = $oUsuario->mostrarUno($usuario_id);

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al registrar comentario, no existe datos de pre calificación.';

		$comentario_formateado = '';

		if($precalificacion['estado'] == 1){
			if($comentario=='' || $comentario==null){
				$data['estado'] = 2;
				$data['mensaje'] = 'Comentario vacío.';
			}else{

				$usu_nom = '';
				if($usuario['estado'] == 1){
					$usu_nom = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
				}

				if($tipo == 'procede'){
					if($precalificacion['data']['tb_precalificacion_observacion'] == '' || $precalificacion['data']['tb_precalificacion_observacion'] == null){
						// si no existe observacion previa
						$comentario_formateado = '<strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
	
					}else{
						// si ya tiene observaciones previas
						$comentario_formateado = $precalificacion['data']['tb_precalificacion_observacion'].'<br><strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
					}
					if($oProceso->modificar_campo_precalificacion($precalificacion['data']['tb_precalificacion_id'], 'tb_precalificacion_observacion', $comentario_formateado, 'STR')){
						$data['estado'] = 1;
						$data['mensaje'] = 'Comentario registrado correctamente.';
					}
				}elseif($tipo == 'familiar'){
					if($precalificacion['data']['tb_precalificacion_coment_fam'] == '' || $precalificacion['data']['tb_precalificacion_coment_fam'] == null){
						// si no existe observacion previa
						$comentario_formateado = '<strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
	
					}else{
						// si ya tiene observaciones previas
						$comentario_formateado = $precalificacion['data']['tb_precalificacion_coment_fam'].'<br><strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
					}
					if($oProceso->modificar_campo_precalificacion($precalificacion['data']['tb_precalificacion_id'], 'tb_precalificacion_coment_fam', $comentario_formateado, 'STR')){
						$data['estado'] = 1;
						$data['mensaje'] = 'Comentario registrado correctamente.';
					}
				}elseif($tipo == 'observacion'){
					if($precalificacion['data']['tb_precalificacion_coment_obs'] == '' || $precalificacion['data']['tb_precalificacion_coment_obs'] == null){
						// si no existe observacion previa
						$comentario_formateado = '<strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
	
					}else{
						// si ya tiene observaciones previas
						$comentario_formateado = $precalificacion['data']['tb_precalificacion_coment_obs'].'<br><strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
					}
					if($oProceso->modificar_campo_precalificacion($precalificacion['data']['tb_precalificacion_id'], 'tb_precalificacion_coment_obs', $comentario_formateado, 'STR')){
						$data['estado'] = 1;
						$data['mensaje'] = 'Comentario registrado correctamente.';
					}
				}elseif($tipo == 'conclusion'){
					if($precalificacion['data']['tb_precalificacion_coment_con'] == '' || $precalificacion['data']['tb_precalificacion_coment_con'] == null){
						// si no existe observacion previa
						$comentario_formateado = '<strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
	
					}else{
						// si ya tiene observaciones previas
						$comentario_formateado = $precalificacion['data']['tb_precalificacion_coment_con'].'<br><strong>- '.$comentario.'</strong>  <i>('.$usu_nom.' | '.date('d-m-Y g:i a').')</i>';
					}
					if($oProceso->modificar_campo_precalificacion($precalificacion['data']['tb_precalificacion_id'], 'tb_precalificacion_coment_con', $comentario_formateado, 'STR')){
						$data['estado'] = 1;
						$data['mensaje'] = 'Comentario registrado correctamente.';
					}
				}
				
			}
		}

		echo json_encode($data);

	}

	if($action == 'finalizar_proceso'){
		
    	$proceso_id = intval($_POST['proceso_id']);
    	$usuario_id = $_SESSION['usuario_id'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al finalizar proceso.';

		if($oProceso->actualizarEstadoProceso($proceso_id, $usuario_id, 2)){ // estado 2 para finalizar Proceso

			$data['estado'] = 1;
			$data['mensaje'] = 'Se finalizó el proceso correctamente.';

		}

 		echo json_encode($data);
	}

	if($action=="anular_proceso"){

		$proceso_id	= intval($_POST['hdd_proceso_id']);
		$usuario_id	= intval($_POST['hdd_usuario_id']);
		$motivo	= $_POST['txt_motivo'];
		
		$proceso = $oProceso->mostrarUno($proceso_id);

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al registrar motivo, no existe datos de proceso.';

		if($proceso['estado'] == 1){
			if($motivo=='' || $motivo==null){
				$data['estado'] = 2;
				$data['mensaje'] = 'Motivo vacío.';
			}else{
				if($oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_mot_anu', $motivo, 'STR')){

					$oProceso->actualizarEstadoProceso($proceso_id, $usuario_id, 3);

					$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_fecmod', date('Y-m-d h:i:s'), 'STR');
					$oProceso->modificar_campo_proceso($proceso_id, 'tb_proceso_usumod', $usuario_id, 'STR');
					$data['estado'] = 1;
					$data['mensaje'] = 'Anulación registrada correctamente.';
				}
			}
		}

		echo json_encode($data);

	}

	if($action == 'eliminar_proceso'){
		
    	$proceso_id = intval($_POST['proceso_id']);
    	$usuario_id = $_SESSION['usuario_id'];

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar proceso.';

		$proceso = $oProceso->mostrarUno($proceso_id);
		
		if($proceso['estado'] == 1){

			if($oProceso->actualizarEstadoProceso($proceso_id, $usuario_id, 4)){ // estado 4 para eliminar Proceso

				$proceso_fases = $oProceso->obtenerFasesProceso($proceso_id); // Listado de fases
				
				if($proceso_fases['estado'] == 1){
					foreach ($proceso_fases['data'] as $key => $value) {
	
						$proceso_fase_items = $oProceso->obtenerItemsPorProcesoFase($value['tb_proceso_fase_id']); // Listado de items 
	
						if($proceso_fase_items['estado'] == 1){
							foreach ($proceso_fase_items['data'] as $key => $v) {
	
								if(!empty($v['tb_multimedia_id'])){ // si existe multimedia, eliminar
	
									if(!empty($v['tb_multimedia_url']) || $v['tb_multimedia_url'] != '...'){
										// eliminar archivo
										unlink("../../".$v['tb_multimedia_url']);
									}
									
									// eliminar registro de multimedia
									$oProceso->eliminarDocProcesoFaseItem(intval($v['tb_multimedia_id']));
	
								}
	
								// eliminar el proceso_fase_item de toda la BD
								$oProceso->eliminarProcesoFaseItem(intval($v['tb_proceso_fase_item_id']));
							}
						}
	
					}
	
					// eliminar pre credito
					if($proceso['estado'] == 1){
						$precredito_cli = $oPrecred->obtener_precredito_activo_x_cliente($proceso['data'][['tb_cliente_id']], 3);
						if($precredito_cli['estado'] == 1){ // existe precredito garveh con mismo cliente_id que no es lo mismo que 'precliente_id'
							$oPrecred->modificar_campo($precredito_cli['data']['tb_precred_id'], 'tb_precred_xac', 0, 'INT');
						}else{
							$precredito_precli = $oPrecred->obtener_precredito_activo_x_precliente($proceso['data'][['tb_precliente_id']], 3);
							if($precredito_precli['estado'] == 1){ // existe precredito garveh con mismo precliente_id al no tener 'cliente_id'
								$oPrecred->modificar_campo($precredito_precli['data']['tb_precred_id'], 'tb_precred_xac', 0, 'INT');
							}
						}
					}
	
				}
	
				$data['estado'] = 1;
				$data['mensaje'] = 'Se eliminó el proceso correctamente.';
	
			}

		}
		

 		echo json_encode($data);
	}

	if($action=="eliminar_cheque"){

		$cheque_id	= intval($_POST['hdd_cheque_id']);
		$proceso_id	= intval($_POST['hdd_proceso_id']);
		$usuario_id	= intval($_POST['hdd_usuario_id']);
		$motivo	= $_POST['txt_motivo'];
		
		$cheques = $oProceso->mostrarDetalleCheque($cheque_id);

		$fase = $oProceso->mostrarProcesoFase($proceso_id, 2); // en fase 2 se crea los cheques, obtendremos su proceso_fase_id
		$fase_id = 0;
		if($fase['estado'] == 1){
			$fase_id = intval($fase['data']['tb_proceso_fase_id']);
		}
		$data['estado'] = 0;
		$data['proceso_id'] = 0;
		$data['usuario_id'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar los cheques, no existe datos.';

		if($cheques['estado'] == 1){
			if($motivo=='' || $motivo==null){
				$data['estado'] = 2;
				$data['mensaje'] = 'Motivo vacío.';
			}else{

				foreach ($cheques['data'] as $key => $value) {
					$oProceso->eliminarChequeDetalle($value['tb_chequedetalle_id']);
				}

				$oProceso->eliminarCheque($cheque_id);

				// añadir comentario
				$comentario = "Se ha eliminado los cheques generado. ".$motivo;
				if($fase_id > 0){
					$oProceso->insertarComentario($fase_id, $usuario_id, $comentario,null);
				}
				//

				$data['estado'] = 1;
				$data['proceso_id'] = $proceso_id;
				$data['usuario_id'] = $usuario_id;
				$data['mensaje'] = 'Eliminación registrada correctamente.';

			}
		}

		echo json_encode($data);

	}

	if($action=="lista_cheques"){

		$usuario_id = intval($_POST['usuario_id']);
		$fecha_ini = fecha_mysql($_POST['fecha_ini']);
		$fecha_fin = fecha_mysql($_POST['fecha_fin']);
		$moneda = $_POST['moneda'];
		
		$cheques = $oProceso->mostrarCheques($fecha_ini, $fecha_fin, $moneda);

		$html = '';
		$total_saldo = 0.00;
		if($cheques['estado'] == 1){

			foreach ($cheques['data'] as $key => $value) {

				$proceso = $oProceso->mostrarUno($value['tb_proceso_id']);
				$flag_proceso = false;

				$proceso_id = 0;
				$banca_id = 0;
				$cliente_nom_cheque = '';

				if($proceso['estado'] == 1){

					$flag_proceso = true;

					$tipo_garantia_id = intval($proceso['data']['tb_cgarvtipo_id']);
					$proceso_id = $proceso['data']['tb_proceso_id'];
					$banca_id = intval($proceso['data']['tb_proceso_banca']);

					if($banca == ''){ // si el desembolso se hace desde listado GARVEH
						$proceso_banca = intval($proceso['data']['tb_proceso_banca']);
						if($proceso_banca == 1){
						$banca = 'cheque';
						}elseif($proceso_banca == 2 || $proceso_banca == 3 || $proceso_banca == 4){
						$banca = 'otro';
						}
						
					}

					$cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
					if($cliente['estado'] == 1){
						$cliente_nom_cheque = $cliente['data']['tb_cliente_nom'];
					}

				}
				
				if($tipo_garantia_id == 1){ // PRE CONSTITUCION

					$transferente_id = 0;
					$transferente = '';
					if($precalificacion['estado']==1){
					  $transferente_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
					  $transferente = $oTransferente->mostrarUno($transferente_id);
					  if($transferente['estado']==1){
						$transferente = $transferente['data']['tb_transferente_nom'];
					  }
					} 
	  
					$beneficiarios = $transferente;
					
				  }elseif($tipo_garantia_id == 2 || $tipo_garantia_id == 3){ // CONSTITUCIÓN SIN Y CON CUSTODIA
	  
					$beneficiarios = $cliente_nom_cheque;
					$participantes = $oProceso->mostrarParticipanteXProceso($value['tb_proceso_id']);
					if($participantes['estado']==1){
					  foreach ($participantes['data'] as $key => $p) {
						$beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'];
					  }
					}
	  
				  }elseif($tipo_garantia_id == 4 || $tipo_garantia_id == 5) { // TERCERO SIN Y CON CUSTODIA
	  
					$participantes = $oProceso->mostrarParticipanteXProceso($value['tb_proceso_id']);
					$beneficiarios = '';
					if($participantes['estado']==1){
					  foreach ($participantes['data'] as $key => $p) {
						if($key == 0){
						  $beneficiarios .= $p['tb_participante_nom'];
						}else{
						  $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'];
						}
					  }
					}
	  
				  }

				  $moneda = "";
				  if($value['tb_cheque_moneda']=='sol'){
					$moneda = "S/ ";
				  }else{
					$moneda = "US$ ";
				  }

				  $tipo = "";
				  if($value['tb_chequedetalle_tipo']=='nego'){
					$tipo = "Negociable";
				  }else{
					$tipo = "No Negociable";
				  }

				  $num_cheque = "";
				  $sede = "";
				  $uso = "";
				  $fecha_subida = "";

				  $num_cheque = $value['tb_chequedetalle_nro_cheque'];
				  $sede = intval($value['tb_empresa_id']);
				  $uso = intval($value['tb_chequedetalle_uso']);
				  $fecha_subida = mostrar_fecha($value['tb_chequedetalle_fecsub']);

				  $empresa = $oEmpresa->mostrarUno($sede);

				  // 1=bcp, bbva=2
				  $img_cheque = "";
				  $banco_nom = '';
				  $empresa_nom = '';

				  if($empresa['estado'] == 1){
					$empresa_nom = $empresa['data']['tb_empresa_nomcom'];
				  }

				  if(intval($value['tb_cheque_banco'])==1){
					$banco_nom = 'BANCO DE CRÉDITO DEL PERÚ';
					if($value['tb_cheque_moneda']=='sol'){
					  if($value['tb_chequedetalle_tipo']=='nego'){
						$img_cheque = "public/images/cheques/bcp_sol.png";
					  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
						$img_cheque = "public/images/cheques/bcp_sol_ng.png";
					  }
					}elseif($value['tb_cheque_moneda']=='dolar'){
					  if($value['tb_chequedetalle_tipo']=='nego'){
						$img_cheque = "public/images/cheques/bcp_dolar.png";
					  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
						$img_cheque = "public/images/cheques/bcp_dolar_ng.png";
					  }
					}
				  }elseif(intval($value['tb_cheque_banco'])==2){
					$banco_nom = 'BBVA CONTINENTAL';
					if($value['tb_cheque_moneda']=='sol'){
					  if($value['tb_chequedetalle_tipo']=='nego'){
						$img_cheque = "public/images/cheques/bbva_sol.png";
					  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
						$img_cheque = "public/images/cheques/bbva_sol_ng.png";
					  }
					}elseif($value['tb_cheque_moneda']=='dolar'){
					  if($value['tb_chequedetalle_tipo']=='nego'){
						$img_cheque = "public/images/cheques/bbva_dolar.png";
					  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
						$img_cheque = "public/images/cheques/bbva_dolar_ng.png";
					  }
					}
				  }

				  $chequesretiro = $oProceso->movimientosChequeXChequedetalle(intval($value['tb_chequedetalle_id']));
				  $suma_retiro = 0.00;
				  if($chequesretiro['estado'] == 1){
					foreach ($chequesretiro['data'] as $key => $v) {
					  $egreso = $oProceso->mostrarEgresoXCheque(intval($v['tb_egreso_id']));
					  if($egreso['estado'] == 1){
						$suma_retiro = $suma_retiro + floatval($egreso['data']['tb_egreso_imp']);
					  }
					}
				  }

				  $nuevo_saldo = 0.00;
				  $nuevo_saldo = floatval($value['tb_chequedetalle_monto']) - $suma_retiro;

				  $bloqueo_check = '';
				  $color_cheque = '';
				  if($nuevo_saldo==0){
					$bloqueo_check = 'disabled';
					$color_cheque = 'background-color: #FFE7E6;';
				  }

				$html .= '<div class="col-md-12">

						<table class="table table-bordered">
							<tbody>
								<tr>
									<td style="width: 300px;">
										<ul>
											<li><strong>Beneficiarios(as):</strong> '.$beneficiarios.'</li>
											<li><strong>N° Cheque:</strong> '.$value['tb_chequedetalle_nro_cheque'].'</li>
											<li><strong>Monto:</strong> '.$moneda.''.number_format($value['tb_chequedetalle_monto'], 2).'</li>
											<li><strong>Tipo:</strong> '.$tipo.'</li>';
				  							if($uso==1){
												$html .= '<li><strong>Uso:</strong> Desembolso Total</li>';
											}else{
												$html .= '<li><strong>Uso:</strong> Retención Total</li>';
											}
									$html .= '<li><strong>Registrada en Sede:</strong> '.$empresa_nom.'</li>
										</ul>
									</td>
									<td>
										<div class="row">	
										<div align="center" class="col-md-12">
											<img class="" style="width: 240px;" src="'.$img_cheque.'" alt="Cheque">
										</div>
										</div>
										<div class="row">	
											<div align="center" class="col-md-12">
												<span><i>Cliente: '.$cliente_nom_cheque.'</i></span>
											</div>
										</div>
									</td>
									<td style="text-align: center; vertical-align: middle; width: 100px;">
										<strong>SALDO</strong><br>'.$moneda.' '.number_format($nuevo_saldo, 2).'
									</td>
									<td style="text-align: center; vertical-align: middle;">
										<a class="btn btn-info btn-xs" title="Ver Detalle" onclick="verMovimientosCheque('.$value['tb_chequedetalle_id'].')"><i class="fa fa-eye"></i></a>
									</td>
								</tr>';

				$html .= '	</tbody>
						</table>
					</div>';
				$total_saldo = $total_saldo + $nuevo_saldo;							
			}
		}else{
			$html .= '<div class="col-md-12">
						<h2>No se encontraron registros</h2>
					</div>';
		}

		$data['html'] = $html;
		$data['saldo'] = $total_saldo;

		echo json_encode($data);
		//echo $html;
  	}

	/* GERSON (12-06-24) */
	if($action=="guardar_campo_comentario"){

		$proceso_id = intval($_POST['proceso_id']);
		$usuario_id	= $_SESSION['usuario_id'];
		$proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);
		$id 		= $_POST['id'];
		$tipo_dato 	= $_POST['tipo_dato'];
		$monto_ini  = 0.00;
		$tipo 		= '';
		$campo 		= '';
		$hist 		= '';

		if($tipo_dato=='text'){
			$valor = $_POST['value'];
			$tipo = 'STR';
		}elseif($tipo_dato=='int'){
			$valor = intval($_POST['value']);
			$tipo = 'INT';
		}elseif($tipo_dato=='float'){
			$valor = floatval($_POST['value']);
			$tipo = 'STR';
		}

		if($id=='familia'){
			$campo = 'tb_precalificacion_familiar';
			if($valor=='s'){
				$hist = 'FAMILIARES EN LA CIUDAD = SI';
			}else{
				$hist = 'FAMILIARES EN LA CIUDAD = NO';
			}
		}elseif($id=='calificacion'){
			$campo = 'tb_precalificacion_calif_finan';
			$hist = 'CALIFICACIÓN FINANCIERA = '.$valor;
		}elseif($id=='valorizacion'){
			$campo = 'tb_proceso_moneda'; // tabla 'tb_proc_proceso'
			if($valor=='sol'){
				$hist = 'MONEDA = S/';
			}else{
				$hist = 'MONEDA = $';
			}
		}elseif($id=='valorizacion_monto'){
			$campo = 'tb_precalificacion_valor_veh';
			$hist = 'VALORIZACIÓN DE UNIDAD = '.$valor;
		}elseif($id=='depreciacion'){
			$campo = 'tb_precalificacion_depreciacion';
			$hist = 'DEPRECIACIÓN = '.$valor;
		}elseif($id=='porcentaje_asignado'){
			$campo = 'tb_precalificacion_calif_veh_porc';
			$hist = 'PORCENTAJE ASIGNADO = '.($valor*100).'%';
		}elseif($id=='monto_calificado'){
			$campo = 'tb_precalificacion_calif_veh';
			$hist = 'VEHÍCULO CALIFICA PARA = '.$valor;
		}

		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar datos.';

		$proceso = $oProceso->mostrarUno($proceso_id);
		$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

		if($id=='valorizacion'){ // tabla 'tb_proc_proceso'

			if($oProceso->modificar_campo_proceso($proceso_id, $campo, $valor, $tipo)){

				$usuario_coment = $oUsuario->mostrarUno($_SESSION['usuario_id']);
				if($usuario_coment['estado'] == 1){
					$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
					$oHistorial->setTbHistNomTabla("tb_proc_precalificacion");
					$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
					$oHistorial->setTbHistDet('El usuario <b>'.$usuario_precali['data']['tb_usuario_nom'].' '.$usuario_precali['data']['tb_usuario_ape'].'</b> ha realizado cambios en datos de evaluación <b>'.$hist.'</b> con fecha <b>'.$fecha_hoy.'</b>');
					$oHistorial->insertar();
				}
	
				$data['estado'] = 1;
				$data['mensaje'] = 'Datos guardados correctamente.';
				$data['proceso_id'] = $proceso_id;
			}
			
		}else{ // tabla 'tb_proc_precalificacion'

			if($precalificacion['estado']==1){
				if($oProceso->modificar_campo_precalificacion($precalificacion['data']['tb_precalificacion_id'], $campo, $valor, $tipo)){

					$usuario_coment = $oUsuario->mostrarUno($_SESSION['usuario_id']);
					if($usuario_coment['estado'] == 1){
						$oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
						$oHistorial->setTbHistNomTabla("tb_proc_precalificacion");
						$oHistorial->setTbHistRegmodid($proceso_fase_item_id);
						$oHistorial->setTbHistDet('El usuario <b>'.$usuario_precali['data']['tb_usuario_nom'].' '.$usuario_precali['data']['tb_usuario_ape'].'</b> ha realizado cambios en datos de evaluación <b>'.$hist.'</b> con fecha <b>'.$fecha_hoy.'</b>');
						$oHistorial->insertar();
					}

					/* if($id=='valorizacion_monto'){

					} */
		
					$data['estado'] = 1;
					$data['mensaje'] = 'Datos guardados correctamente.';
					$data['proceso_id'] = $proceso_id;
				}
			}
			

		}

		echo json_encode($data);

	}
	/*  */

?>