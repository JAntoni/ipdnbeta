<?php
	session_name("ipdnsac");
  session_start();
	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();

  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");

  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);

  $proceso = $oProceso->mostrarUno($proceso_id);
  $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

  $cliente_id = 0;
  $banca = 0;
  $tipo_garantia = '';
  $tipo_garantia_id = 0;
  $cliente_doc = 0;
  $cliente_nom = '';
  $tipo_cambio = 0.000;

  if($proceso['estado']==1){
    $banca = intval($proceso['data']['tb_proceso_banca']);
    $tipo_garantia_id = intval($proceso['data']['tb_cgarvtipo_id']);
    $garvtipo = $oProceso->mostrarUnoGarvtipo($proceso['data']['tb_cgarvtipo_id']);
    $tipo_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);

    if($garvtipo['estado']==1){
      $tipo_garantia = $garvtipo['data']['cgarvtipo_nombre'];
    }
    $cliente_id = intval($proceso['data']['tb_cliente_id']);
    $cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
    if($cliente['estado']==1){
      $cliente_doc = $cliente['data']['tb_cliente_doc'];
      $cliente_nom = $cliente['data']['tb_cliente_nom'];
    }
  }

  

  $cheque_c = $oProceso->mostrarCabeceraChequeXProceso($proceso_id);
  $cheques = $oProceso->mostrarChequeXProceso($proceso_id);
  $fecha = '';
  $detalle = '';
  $cheque_id = 0;
  $conector = '';
  if( $cheque_c['estado'] == 1){
    $fecha = date("d/m/Y, h:i a", strtotime($cheque_c['data']['tb_cheque_fecreg']));
    $detalle = $cheque_c['data']['tb_cheque_det'];
    $cheque_id = intval($cheque_c['data']['tb_cheque_id']);
    $conector = $cheque_c['data']['tb_cheque_conector'];
  }else{
    $fecha = date("d/m/Y, h:i a");
  }


?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_generar_cheque" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <input type="hidden" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Generar Cheques</h4>
      </div>
      <div class="modal-body">

        <h5 style="color: red"><strong>Tipo de Cambio: <?php echo number_format($tipo_cambio, 3); ?></strong></h5>

        <?php if($cheques['estado']==1){ $i = 1; ?>

          <h3><strong>Listado de cheques</strong></h3>
          
          <?php 
            foreach ($cheques['data'] as $key => $value) { 
              if($i==1){ 
                $moneda = "";
                if($value['tb_cheque_moneda']=='sol'){
                  $moneda = "S/ ";
                }else{
                  $moneda = "$ ";
                }
          ?>
                <p>
                  <strong>Registrado por: </strong>
                  <span><i><?php echo $value['usuario'].' el '.date("j-m-y, h:i a", strtotime($value['tb_cheque_fecreg'])); ?></i></span>&nbsp; &nbsp; &nbsp; &nbsp; 
                  <?php if($cheque_id > 0){ ?>
                    <span><a class="btn btn-danger btn-xs" title="Eliminar Proceso" onclick="btnEliminarCheque(<?php echo $cheque_id ?>, <?php echo $proceso_id ?>)"><i class="fa fa-trash"></i> Eliminar Cheques</a></span>
                  <?php } ?>
                </p>
          <?php 
              }
              $banco = "";
              if($value['tb_cheque_banco']==1){
                $banco = "BANCO DE CRÉDITO DEL PERÚ";
              }elseif($value['tb_cheque_banco']==2){
                $banco = "BBVA CONTINENTAL";
              }
              $tipo = "";
              if($value['tb_chequedetalle_tipo']=='nego'){
                $tipo = "Negociable";
              }else{
                $tipo = "No Negociable";
              }
          ?>
            <h4><strong><?php echo 'Cheque N° '.$i.':' ?></strong></h4>
            <ul>
              <!-- <li><?php echo 'Beneficiarios(as): '.$value['tb_chequedetalle_bene'] ?></li> -->
              <li><?php echo 'Banco: '.$banco ?></li>
              <li><?php echo 'Monto: '.$moneda.''.number_format($value['tb_chequedetalle_monto'], 2) ?></li>
              <li><?php echo 'Tipo: '.$tipo.':' ?></li>
            </ul>
          <?php $i++; } ?>

          <div class="row">
            <div align="center" id="divFormatoCheque" class="col-md-12" style="padding-top: 5px;">
              <table class="table table-hover">
                <thead>
                  <tr id="tabla_cabecera" style="background-color: #135825; color: #FFFFFF; font-family: cambria; ">
                    <th id="tabla_cabecera_fila" colspan="4" scope="rowgroup" width="80%" style="font-size: 14px !important;">CHEQUES A GENERAR</th>
                    <th id="tabla_cabecera_fila" colspan="1" scope="rowgroup" width="20%"><?php echo $fecha; ?></th>
                  </tr>
                </thead>  
                  <?php
                    $i = 1;
                    if($cheques['estado']==1){

                      $cant_cheques = count($cheques['data']);

                      if($tipo_garantia_id == 1){ // PRE CONSTITUCION

                        $transferente_id = 0;
                        $transferente_nom = '';
                        $transferente_doc = '';
                        if($precalificacion['estado']==1){
                          $transferente_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
                          $transferente = $oTransferente->mostrarUno($transferente_id);
                          if($transferente['estado']==1){
                            $transferente_nom = $transferente['data']['tb_transferente_nom'];
                            $transferente_doc = $transferente['data']['tb_transferente_doc'];
                          }
                        } 
            
                        $beneficiarios = $transferente_nom.' / '.$transferente_doc;
                        
                      }elseif($tipo_garantia_id == 2 || $tipo_garantia_id == 3 || $tipo_garantia_id == 6){ // CONSTITUCIÓN SIN Y CON CUSTODIA O ADENDA

                        // gerson (23-03-24)
                        if($precalificacion['estado'] == 1){
                          if($precalificacion['data']['tb_precalificacion_razon_social'] != '' || $precalificacion['data']['tb_precalificacion_razon_social'] != null){
                            $beneficiarios = $precalificacion['data']['tb_precalificacion_razon_social'].' / '.$precalificacion['data']['tb_precalificacion_ruc'];
                          }else{
                            $beneficiarios = $cliente_nom.' / '.$cliente_doc;
                          }
                        }else{
                          $beneficiarios = $cliente_nom.' / '.$cliente_doc;
                        }
                        //

                        $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
                        if($participantes['estado']==1){
                          foreach ($participantes['data'] as $key => $p) {
                            $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'].' / '.$p['tb_participante_doc'];
                          }
                        }

                      }elseif($tipo_garantia_id == 4 || $tipo_garantia_id == 5) { // TERCERO SIN Y CON CUSTODIA

                        $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
                        $beneficiarios = '';
                        if($participantes['estado']==1){
                          foreach ($participantes['data'] as $key => $p) {
                            if($key == 0){
                              $beneficiarios .= $p['tb_participante_nom'].' / '.$p['tb_participante_doc'];
                            }else{
                              $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'].' / '.$p['tb_participante_doc'];
                            }
                          }
                        }

                      }

                      foreach ($cheques['data'] as $key => $value) { 
                        if($i==1){ 
                          $moneda = "";
                          $beneficiado = $value['tb_chequedetalle_bene'];
                          if($value['tb_cheque_moneda']=='sol'){
                            $moneda = "S/ ";
                          }else{
                            $moneda = "US$ ";
                          }
                        }
                        $i++;
                      }
                  ?>
                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>BENEFICIARIOS</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" style="border: #000000 1px solid"><strong><?php echo $beneficiarios; ?></strong></td>
                      </tr>
                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>ORDENANTE</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" width="80%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;">20600752872</td>
                      </tr>
                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>N° CHEQUES</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" width="80%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $cant_cheques; ?></td>
                      </tr>
                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>MONEDA</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" width="80%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $moneda; ?></td>
                      </tr>

                      <?php 
                      $cont = 1;
                      foreach ($cheques['data'] as $key => $value2) { 
                        $tipo = '';
                        if($value2['tb_chequedetalle_tipo']=='nego'){
                          $tipo = 'NEGOCIABLE';
                        }else{
                          $tipo = 'NO NEGOCIABLE';
                        }
                        
                      ?>
                        <tr id="tabla_cabecera_fila">
                          <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong><?php echo "MONTO ".$cont; ?></strong></td>
                          <td id="tabla_fila" colspan="3" scope="rowgroup" width="60%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $moneda.' '.formato_moneda($value2['tb_chequedetalle_monto']); ?></td>
                          <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $tipo; ?></td>
                        </tr>
                      <?php $cont++; }?>

                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>OPERACIÓN</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" width="80%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $tipo_garantia; ?></td>
                      </tr>
                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>CLIENTE</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" width="80%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $cliente_nom; ?></td>
                      </tr>
                      <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila" colspan="1" scope="rowgroup" width="20%" style="border: #000000 1px solid; padding: 4px !important; background-color: #279944; color: #FFFFFF; font-family: cambria; font-size: 11px;"><strong>DETALLE</strong></td>
                        <td id="tabla_fila" colspan="4" scope="rowgroup" width="80%" style="border: #000000 1px solid; font-size: 13px !important; padding: 4px !important;"><?php echo $detalle; ?></td>
                      </tr>

                  <?php }?>
              </table>
            </div> 
          </div> 

          <div class="row">
            <div align="center" class="col-md-12" style="padding-top: 5px;">
              <div class="form-group">
                <button type="button" class="btn btn-sm btn-success" onclick="generarChequeExcel(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-file-excel-o"></i> Exportar Excel</strong></button>
              </div>
            </div> 
          </div> 
        <?php }else{ ?>
          
          <?php if($banca==1){ ?>

            <?php if($cliente_id > 0){ ?>
            
              <div class="row">

                <div class="col-md-7">
                  <div class="form-group">
                    <label for="cant_cheque" class="col-sm-4 control-label">Cantidad de Cheques</label>
                    <div class="col-sm-8"> 
                      <input type="hidden" class="form-control input-sm" id="hdd_tipogarantia_id" id="hdd_tipogarantia_id" value="<?php echo $tipo_garantia_id; ?>">
                      <div class="input-group">
                        <input type="text" class="form-control input-sm" id="cant_cheque" onkeypress="return solo_numero(event)" style="text-align:right;" value="" placeholder="0">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-sm btn-success btn-flat" id="btn_crear_cheque" onClick="generarCheque()"><i class="fa fa-indent"></i> Generar</button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="div_cheques" class="col-md-12">

                </div>

              </div>

            <?php }else{ ?>
              <h3>Guarde los datos del cliente en la fase 1.</h3>
            <?php } ?>
            
          <?php }else{ ?>
            <h3>La Generación de Cheques solo está permitido para bancarización con Cheques.</h3>
          <?php } ?>

        <?php } ?>

      </div>
      <div class="modal-footer">
        <?php if($cheques['estado']==1){ ?>
        <?php }else{ ?>
          <?php if($banca==1){ ?>
            <?php if($cliente_id > 0){ ?>
              <button type="button" id="btn_guardar_bancarizacion" class="btn btn-info" onclick="registrarCheque()">Guardar</button>
            <?php } ?>
          <?php } ?>
        <?php } ?>
        <button type="button" id="btn_cerrar_bancarizacion" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>
function registrarCheque(){
  
  var proceso_id = parseInt($('#hdd_proceso_id').val());
  var garvtipo_id = parseInt($('#hdd_garvtipo_id').val());
  var cant_cheque = parseInt($('#hdd_cant_cheque').val());
  var banco_id = parseInt($('#cmb_banco_id').val());
  var moneda = $('#cmb_moneda').val();
  var enlace = $('#cmb_enlace').val();
  var detalle = $('#detalle').val();

  if(cant_cheque==0){
    Swal.fire(
      '¡Aviso!',
      'Seleccione un opción.',
      'warning'
    );
  }else{

    var cheques = [];
    for (let i = 0; i < cant_cheque; i++) {
      cheques.push({ 
        "cheque_nombre": $('#cheque_per'+i).val(),
        "cheque_monto": $('#cheque_monto'+i).val(),
        "cheque_tipo": $('#cmb_negociable'+i).val()
      });
    }

    if(cheques == '' || cheques == []){
      Swal.fire(
        '¡Aviso!',
        'Genere cheques.',
        'warning'
      );
    }else{

      $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_controller.php",
        async: false,
        dataType: "json",
        data: ({
          action: 'guardar_cheque',
          proceso_id: proceso_id,
          cheques: cheques,
          cant_cheque: cant_cheque,
          banco_id: banco_id,
          moneda: moneda,
          enlace: enlace,
          detalle: detalle,
        }),
          beforeSend: function () {
            $('#btn_crear_cheque').prop('disabled', true);
          },
          success: function (data) {
            if(data.estado == 1){
              notificacion_success(data.mensaje, 3000);
              $("#modal_generar_cheque").modal('hide');
            }else{
              notificacion_warning(data.mensaje, 3000);
            }
          },
          complete: function (data) {
            $('#btn_crear_cheque').prop('disabled', false);
          }
      });

    }
    
  }
}
</script>