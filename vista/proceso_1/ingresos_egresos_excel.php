<?php
//echo 'hasta aki estoy entrando normal '; exit();
//require_once('../../core/usuario_sesion.php');
//error_reporting(0);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$proceso_id = intval($_GET['proceso_id']);
$usuario_id = intval($_GET['usuario_id']);

$colaborador = '';
$celular = '';
$detalle_ingreso = '';
$proceso = $oProceso->mostrarUno($proceso_id);
$usuario = $oUsuario->mostrarUno($usuario_id);
$ingresos = $oProceso->mostrarIEXProceso($proceso_id, 'I');
$egresos = $oProceso->mostrarIEXProceso($proceso_id, 'E');

$cliente_nom = '';
$moneda = '';
$plazo = '';
$tipo_cambio = 0.000;
$monto = 0.00;
$monto_solicitado = 0.00;
$monto_solicitado_me = 0.00;
$porc_inicial = 0.00;
$monto_inicial = 0.00;
/* if($usuario['estado']==1){
  $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
  $celular = $usuario['data']['tb_usuario_tel'];
} */
if($proceso['estado']==1){
  $moneda = $proceso['data']['tb_proceso_moneda'];
  $tipo_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);
  $titulo_moneda = '';
  $mon = '';
	$sub_mon = '';
  if($moneda=='sol' || $moneda=='dolar_s'){
    $titulo_moneda = 'SOLES';
    $mon = 'S/ ';
    $monto_solicitado = floatval($proceso['data']['tb_proceso_prestamo']);
    $monto_solicitado_me = floatval($proceso['data']['tb_proceso_prestamo'])/$tipo_cambio;
  }elseif($moneda=='dolar' || $moneda=='sol_d'){
    $titulo_moneda = 'DOLARES';
    $mon = '$ ';
		$sub_mon = 'S/ ';
    $monto_solicitado = floatval($proceso['data']['tb_proceso_prestamo'])*$tipo_cambio;
    $monto_solicitado_me = floatval($proceso['data']['tb_proceso_prestamo']);
  }
  $monto = floatval($proceso['data']['tb_proceso_monto']);
  $porc_inicial = floatval($proceso['data']['tb_proceso_inicial_porcen']);
  $monto_inicial = floatval($proceso['data']['tb_proceso_inicial']);
  $plazo = floatval($proceso['data']['tb_proceso_plazo']);

  $detalle_ingreso = $proceso['data']['tb_proceso_ing_det'];

  $cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
  if($cliente['estado']==1){
    $cliente_nom = $cliente['data']['tb_cliente_nom'];
  }
}
$fecha_hoy = date("d/m/Y");

//calcular ingresos
$total_ingresos = 0.00;
if($ingresos['estado']==1){
	foreach ($ingresos['data'] as $key => $value) {
    $total_ingresos = $total_ingresos + $value['tb_ingresoegreso_monto'] + ($value['tb_ingresoegreso_montome']*$tipo_cambio);
  }
}
//

//calcular egresos
$total_egresos  = 0.00;
$alimentacion   = 0.00;
$alquiler       = 0.00;
$vestido        = 0.00;
$educacion      = 0.00;
$salud          = 0.00;
$transporte     = 0.00;
$servicio       = 0.00;
$juntas         = 0.00;
$gastos         = 0.00;
$otros          = 0.00;

//calcular egresos empresa
$personal     = 0.00;
$servicios    = 0.00;
$alquiler_l   = 0.00;
$transportes  = 0.00;
$activo       = 0.00;
$impuesto     = 0.00;
$otro         = 0.00;

if($egresos['estado']==1){
	foreach ($egresos['data'] as $key => $value) {
    $total_egresos = $total_egresos + $value['tb_ingresoegreso_monto'] + ($value['tb_ingresoegreso_montome']*$tipo_cambio);

    if($value['tb_ingresoegreso_subtipo']=='familiar'){

      if($value['tb_ingresoegreso_det']=='alimetacion'){
        $alimentacion = $alimentacion + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='alquiler'){
        $alquiler = $alquiler + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='vestido'){
        $vestido = $vestido + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='educacion'){
        $educacion = $educacion + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='salud'){
        $salud = $salud + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='transporte'){
        $transporte = $transporte + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='servicio'){
        $servicio = $servicio + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='junta'){
        $juntas = $juntas + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='gasto_personal'){
        $gastos = $gastos + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='otro'){
        $otros = $otros + $value['tb_ingresoegreso_monto'];
      }

    }elseif($value['tb_ingresoegreso_subtipo']=='empresa'){

      if($value['tb_ingresoegreso_det']=='personal'){
        $personal = $personal + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='servicio'){
        $servicios = $servicios + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='alquiler'){
        $alquiler_l = $alquiler_l + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='transporte'){
        $transportes = $transportes + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='v'){
        $activo = $activo + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='impuesto'){
        $impuesto = $impuesto + $value['tb_ingresoegreso_monto'];
      }elseif($value['tb_ingresoegreso_det']=='otro'){
        $otro = $otro + $value['tb_ingresoegreso_monto'];
      }

    }

  }
}
//

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("contacto@prestamosdelnortechiclayo.com")
	->setLastModifiedBy("contacto@prestamosdelnortechiclayo.com")
	->setTitle("EEGGPP")
	->setSubject("Ingresos y Gastos")
	->setDescription("Reporte generado por contacto@prestamosdelnortechiclayo.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloFilas2 = array(
    'font' => array(
    'name'  => 'arial',
    'bold'  => true,
    'size'  => 11,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '1F497D')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaColumna = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_DASHED,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaColumnaBold = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => true,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_DASHED,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaValor = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_DASHED,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaValorRight = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_DASHED,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaColumnaBorder = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaValorBorder = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$contenidoTablaValorBorderRight = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array(
          'rgb' => '000000'
        )
      )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$textoIzquierda = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_NONE,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$textoDerecha = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_NONE,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$textoCentro = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  /* 'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_NONE,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ), */
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$textBold = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => true,
    'size'  => 9,
    'color' => array(
      'rgb' => '000000'
    )
  )
);

$textRed = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => false,
    'size'  => 9,
    'color' => array(
      'rgb' => 'FF001C'
    )
  )
);

$textTituloRed = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => true,
    'size'  => 11,
    'color' => array(
      'rgb' => 'FF001C'
    )
  )
);

$textBoldBlue = array(
  'font' => array(
    'name'  => 'arial',
    'bold'  => true,
    'size'  => 9,
    'color' => array(
      'rgb' => '2A00FF'
    )
  )
);

$fondoColorCeleste = array(
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'B7DEE8')
  )
);

$bordeSeccion = array(
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
        'color' => array(
          //'rgb' => '143860'
          'rgb' => '000000'
        )
      )
  )
);

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

  
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);

  $objDrawing = new PHPExcel_Worksheet_Drawing();
  $objDrawing->setName('Logo IPDN');
  $objDrawing->setDescription('Logo IPDN');
  $img = '../../public/images/logopres.png'; // Provide path to your logo file
  $objDrawing->setPath($img);
  $objDrawing->setCoordinates("B2");
  $objDrawing->setWidth(100);
  $objDrawing->setHeight(50);
  $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

  $nombre_archivo = "EEGGPP - IPDN2022";

  $c = 2;

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'FECHA DE EVALUACIÓN: ');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($textoDerecha);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", $fecha_hoy);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($textoIzquierda);
  $objPHPExcel->getActiveSheet()->getStyle("G$c:H$c")->applyFromArray($textBold);


  $c = 5;

  /* INFORMACIION DEL CLIENTE */

  $titulo = "Información del Cliente";
  $objPHPExcel->getActiveSheet()->mergeCells("B$c:H$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("B$c:H$c")->applyFromArray($estiloTituloFilas2);

  $c = 7;

  $objPHPExcel->getActiveSheet()->mergeCells("B$c:C$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Actividad');
  $objPHPExcel->getActiveSheet()->getStyle("B$c:C$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("D$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("D$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("D$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Tipo de Cambio');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", $tipo_cambio);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 8;

  $objPHPExcel->getActiveSheet()->mergeCells("B$c:C$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Tipo de Crédito');
  $objPHPExcel->getActiveSheet()->getStyle("B$c:C$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("D$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("D$c", 'GARMOB');
  $objPHPExcel->getActiveSheet()->getStyle("D$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Monto Total');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", $monto);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 9;

  $objPHPExcel->getActiveSheet()->mergeCells("B$c:C$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Monto Solicitado (Financiar) Dólares');
  $objPHPExcel->getActiveSheet()->getStyle("B$c:C$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("D$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("D$c", $monto_solicitado_me);
  $objPHPExcel->getActiveSheet()->getStyle("D$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Monto Inicial');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", $monto_inicial);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 10;

  $objPHPExcel->getActiveSheet()->mergeCells("B$c:C$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Monto Solicitado (Financiar) Soles');
  $objPHPExcel->getActiveSheet()->getStyle("B$c:C$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("D$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("D$c", $monto_solicitado);
  $objPHPExcel->getActiveSheet()->getStyle("D$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", '% Inicial');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", $porc_inicial."%" );
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 11;

  $objPHPExcel->getActiveSheet()->mergeCells("B$c:C$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Moneda');
  $objPHPExcel->getActiveSheet()->getStyle("B$c:C$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("D$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("D$c", $titulo_moneda);
  $objPHPExcel->getActiveSheet()->getStyle("D$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Frecuencia');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", "MENSUAL");
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);


  $objPHPExcel->getActiveSheet()->getStyle("B5:H5")->applyFromArray($bordeSeccion);
  $objPHPExcel->getActiveSheet()->getStyle("G5:H11")->applyFromArray($bordeSeccion);

  $objPHPExcel->getActiveSheet()->mergeCells("B6:C6");
  $objPHPExcel->getActiveSheet()->setCellValue("B6", 'Cliente');
  $objPHPExcel->getActiveSheet()->getStyle("B6:C6")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("D6:H6");
  $objPHPExcel->getActiveSheet()->setCellValue("D6", $cliente_nom);
  $objPHPExcel->getActiveSheet()->getStyle("D6:H6")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->getStyle("B5:H11")->applyFromArray($bordeSeccion);

  /*  */

  /* PASIVOS - GASTOS FINANCIEROS */

  $c = 13;

  $titulo = "PASIVOS - GASTOS FINANCIEROS (X)";
  $objPHPExcel->getActiveSheet()->mergeCells("B$c:E$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("B$c:E$c")->applyFromArray($estiloTituloFilas2);
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($textTituloRed);

  $titulo = "Nombre de la Entidad";
  $objPHPExcel->getActiveSheet()->mergeCells("B14:B17");
  $objPHPExcel->getActiveSheet()->setCellValue("B14", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("B14:B17")->applyFromArray($estiloTituloFilas2);

  $titulo = "M.N. (A)";
  $objPHPExcel->getActiveSheet()->setCellValue("C14", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("C14")->applyFromArray($estiloTituloFilas2);

  $titulo = "Cuotas";
  $objPHPExcel->getActiveSheet()->mergeCells("C15:C16");
  $objPHPExcel->getActiveSheet()->setCellValue("C15", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("C15:C16")->applyFromArray($estiloTituloFilas2);

  $titulo = "S/.";
  $objPHPExcel->getActiveSheet()->setCellValue("C17", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("C17")->applyFromArray($estiloTituloFilas2);

  $titulo = "M.E. (B)";
  $objPHPExcel->getActiveSheet()->setCellValue("D14", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("D14")->applyFromArray($estiloTituloFilas2);

  $titulo = "Cuotas";
  $objPHPExcel->getActiveSheet()->mergeCells("D15:D16");
  $objPHPExcel->getActiveSheet()->setCellValue("D15", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("D15:D16")->applyFromArray($estiloTituloFilas2);

  $titulo = "US$";
  $objPHPExcel->getActiveSheet()->setCellValue("D17", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("D17")->applyFromArray($estiloTituloFilas2);

  $titulo = "Cuota Total M.N. (A+B)";
  $objPHPExcel->getActiveSheet()->mergeCells("E14:E16");
  $objPHPExcel->getActiveSheet()->setCellValue("E14", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("E14:E16")->applyFromArray($estiloTituloFilas2);

  $titulo = "S/.";
  $objPHPExcel->getActiveSheet()->setCellValue("E17", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("E17")->applyFromArray($estiloTituloFilas2);

  $c = 18;

  if($egresos['estado']==1){
    foreach ($egresos['data'] as $key => $value) {
  
      if($value['tb_ingresoegreso_subtipo']=='financiero'){

        $objPHPExcel->getActiveSheet()->setCellValue("B$c", $value['tb_ingresoegreso_entidad']);
        $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaValor);
        $objPHPExcel->getActiveSheet()->setCellValue("C$c", $value['tb_ingresoegreso_monto']);
        $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($contenidoTablaValorRight);
        $objPHPExcel->getActiveSheet()->setCellValue("D$c", $value['tb_ingresoegreso_montome']);
        $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($contenidoTablaValorRight);
        $objPHPExcel->getActiveSheet()->setCellValue("E$c", floatval($value['tb_ingresoegreso_monto'])+floatval($value['tb_ingresoegreso_montome'])*$tipo_cambio);
        $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($contenidoTablaValorRight);
        $c++;
      }

    }
  }

  $objPHPExcel->getActiveSheet()->getStyle("B25")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("B25", 'TOTALES');
  $objPHPExcel->getActiveSheet()->setCellValue("C25", "=SUM(C18:C24)");
  $objPHPExcel->getActiveSheet()->setCellValue("D25", "=SUM(D18:D24)");
  $objPHPExcel->getActiveSheet()->setCellValue("E25", "=SUM(E18:E24)");
  $objPHPExcel->getActiveSheet()->getStyle("C25:E25")->applyFromArray($contenidoTablaValorRight);
  $objPHPExcel->getActiveSheet()->getStyle("C25:E25")->applyFromArray($textBold);

  $objPHPExcel->getActiveSheet()->getStyle('C18:E25')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);


  $objPHPExcel->getActiveSheet()->getStyle("B13:E13")->applyFromArray($bordeSeccion); // TITULO
  $objPHPExcel->getActiveSheet()->getStyle("B14:B17")->applyFromArray($bordeSeccion); // Nombre banco/caja
  $objPHPExcel->getActiveSheet()->getStyle("C14")->applyFromArray($bordeSeccion); // M.N.
  $objPHPExcel->getActiveSheet()->getStyle("C15:C16")->applyFromArray($bordeSeccion); // M.N. Cuotas
  $objPHPExcel->getActiveSheet()->getStyle("C17")->applyFromArray($bordeSeccion); // S/
  $objPHPExcel->getActiveSheet()->getStyle("C14:C25")->applyFromArray($bordeSeccion); 
  $objPHPExcel->getActiveSheet()->getStyle("D14")->applyFromArray($bordeSeccion); // M.E.
  $objPHPExcel->getActiveSheet()->getStyle("D15:D16")->applyFromArray($bordeSeccion); // M.E. Cuotas
  $objPHPExcel->getActiveSheet()->getStyle("D17")->applyFromArray($bordeSeccion); // US$
  $objPHPExcel->getActiveSheet()->getStyle("D14:D25")->applyFromArray($bordeSeccion); 
  $objPHPExcel->getActiveSheet()->getStyle("E14:E16")->applyFromArray($bordeSeccion); // Total
  $objPHPExcel->getActiveSheet()->getStyle("E17")->applyFromArray($bordeSeccion); // Total S/
  $objPHPExcel->getActiveSheet()->getStyle("B25:E25")->applyFromArray($bordeSeccion); // Totales general
  $objPHPExcel->getActiveSheet()->getStyle("B13:E25")->applyFromArray($bordeSeccion); // Toda tabla

  /*  */

  /* PASIVOS - GASTOS FAMILIARES */

  $c = 27;

  $titulo = "PASIVOS - GASTOS FAMILIARES (CONSUMO) (Y)";
  $objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("B$c:D$c")->applyFromArray($estiloTituloFilas2);
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($textTituloRed);

  $titulo = "Detalle";
  $objPHPExcel->getActiveSheet()->mergeCells("B28:C28");
  $objPHPExcel->getActiveSheet()->setCellValue("B28", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("B28:C28")->applyFromArray($estiloTituloFilas2);

  $titulo = "S/.";
  $objPHPExcel->getActiveSheet()->setCellValue("D28", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("D28")->applyFromArray($estiloTituloFilas2);

  $objPHPExcel->getActiveSheet()->mergeCells("B29:C29");
  $objPHPExcel->getActiveSheet()->getStyle("B29:C29")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B29", 'Alimentación');
  $objPHPExcel->getActiveSheet()->setCellValue("D29", $alimentacion);
  $objPHPExcel->getActiveSheet()->getStyle("D29")->applyFromArray($contenidoTablaValorRight);
  
  $objPHPExcel->getActiveSheet()->mergeCells("B30:C30");
  $objPHPExcel->getActiveSheet()->getStyle("B30:C30")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B30", 'Alquiler de Casa');
  $objPHPExcel->getActiveSheet()->setCellValue("D30", $alquiler);
  $objPHPExcel->getActiveSheet()->getStyle("D30")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B31:C31");
  $objPHPExcel->getActiveSheet()->getStyle("B31:C31")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B31", 'Vestido y Calzado');
  $objPHPExcel->getActiveSheet()->setCellValue("D31", $vestido);
  $objPHPExcel->getActiveSheet()->getStyle("D31")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B32:C32");
  $objPHPExcel->getActiveSheet()->getStyle("B32:C32")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B32", 'Educación');
  $objPHPExcel->getActiveSheet()->setCellValue("D32", $educacion);
  $objPHPExcel->getActiveSheet()->getStyle("D32")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B33:C33");
  $objPHPExcel->getActiveSheet()->getStyle("B33:C33")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B33", 'Salud');
  $objPHPExcel->getActiveSheet()->setCellValue("D33", $salud);
  $objPHPExcel->getActiveSheet()->getStyle("D33")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B34:C34");
  $objPHPExcel->getActiveSheet()->getStyle("B34:C34")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B34", 'Transporte');
  $objPHPExcel->getActiveSheet()->setCellValue("D34", $transporte);
  $objPHPExcel->getActiveSheet()->getStyle("D34")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B35:C35");
  $objPHPExcel->getActiveSheet()->getStyle("B35:C35")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B35", 'Servicio (Luz, agua, teléfono)');
  $objPHPExcel->getActiveSheet()->setCellValue("D35", $servicio);
  $objPHPExcel->getActiveSheet()->getStyle("D35")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B36:C36");
  $objPHPExcel->getActiveSheet()->getStyle("B36:C36")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B36", 'Juntas');
  $objPHPExcel->getActiveSheet()->setCellValue("D36", $juntas);
  $objPHPExcel->getActiveSheet()->getStyle("D36")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B37:C37");
  $objPHPExcel->getActiveSheet()->getStyle("B37:C37")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B37", 'Gastos Personales');
  $objPHPExcel->getActiveSheet()->setCellValue("D37", $gastos);
  $objPHPExcel->getActiveSheet()->getStyle("D37")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B38:C38");
  $objPHPExcel->getActiveSheet()->getStyle("B38:C38")->applyFromArray($contenidoTablaColumna);
  $objPHPExcel->getActiveSheet()->setCellValue("B38", 'Gastos Personales');
  $objPHPExcel->getActiveSheet()->setCellValue("D38", $otros);
  $objPHPExcel->getActiveSheet()->getStyle("D38")->applyFromArray($contenidoTablaValorRight);

  $objPHPExcel->getActiveSheet()->mergeCells("B39:C39");
  $objPHPExcel->getActiveSheet()->getStyle("B39:C39")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("B39", 'TOTAL');
  $objPHPExcel->getActiveSheet()->setCellValue("D39", "=SUM(D29:D38)");
  $objPHPExcel->getActiveSheet()->getStyle("D39")->applyFromArray($contenidoTablaValorRight);
  $objPHPExcel->getActiveSheet()->getStyle("D39")->applyFromArray($textBold);

  $objPHPExcel->getActiveSheet()->getStyle('D29:D39')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

  $objPHPExcel->getActiveSheet()->getStyle("B27:D27")->applyFromArray($bordeSeccion); // TITULO
  $objPHPExcel->getActiveSheet()->getStyle("B28:D28")->applyFromArray($bordeSeccion); // SUBTITULO
  $objPHPExcel->getActiveSheet()->getStyle("D28:D39")->applyFromArray($bordeSeccion); // Total
  $objPHPExcel->getActiveSheet()->getStyle("B39:D39")->applyFromArray($bordeSeccion); // Totales general
  $objPHPExcel->getActiveSheet()->getStyle("B27:D39")->applyFromArray($bordeSeccion); // Toda tabla

  /*  */

  /* ESTADO DE PÉRDIDAS Y GANANCIAS */

  $c = 13;

  $titulo = "ESTADO DE PÉRDIDAS Y GANANCIAS";
  $objPHPExcel->getActiveSheet()->mergeCells("G$c:H$c");
  $objPHPExcel->getActiveSheet()->setCellValue("G$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("G$c:H$c")->applyFromArray($estiloTituloFilas2);

  $objPHPExcel->getActiveSheet()->getStyle("G14")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G14", 'Ventas Mensuales');
  $objPHPExcel->getActiveSheet()->setCellValue("H14", $total_ingresos);
  $objPHPExcel->getActiveSheet()->getStyle("H14")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G15")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->setCellValue("G15", 'Ventas Contado');
  $objPHPExcel->getActiveSheet()->setCellValue("H15", "");
  $objPHPExcel->getActiveSheet()->getStyle("H15")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G16")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->setCellValue("G16", 'Ventas Crédito');
  $objPHPExcel->getActiveSheet()->setCellValue("H16", "");
  $objPHPExcel->getActiveSheet()->getStyle("H16")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G17")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G17", 'Costo de Ventas');
  $objPHPExcel->getActiveSheet()->setCellValue("H17", "");
  $objPHPExcel->getActiveSheet()->getStyle("H17")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G18")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G18", 'Utilidad Bruta');
  $objPHPExcel->getActiveSheet()->setCellValue("H18", "=H14-H17");
  $objPHPExcel->getActiveSheet()->getStyle("H18")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->getStyle("G18:H18")->applyFromArray($fondoColorCeleste);
  $objPHPExcel->getActiveSheet()->getStyle("G18")->applyFromArray($textBold);
  $objPHPExcel->getActiveSheet()->getStyle("H18")->applyFromArray($textBoldBlue);

  $objPHPExcel->getActiveSheet()->getStyle("G19")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G19", 'Gasto de Personal');
  $objPHPExcel->getActiveSheet()->setCellValue("H19", $personal);
  $objPHPExcel->getActiveSheet()->getStyle("H19")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G20")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G20", 'Luz, agua, teléfono');
  $objPHPExcel->getActiveSheet()->setCellValue("H20", $servicios);
  $objPHPExcel->getActiveSheet()->getStyle("H20")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G21")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G21", 'Alquiler de Local');
  $objPHPExcel->getActiveSheet()->setCellValue("H21", $alquiler_l);
  $objPHPExcel->getActiveSheet()->getStyle("H21")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G22")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G22", 'Transporte');
  $objPHPExcel->getActiveSheet()->setCellValue("H22", $transportes);
  $objPHPExcel->getActiveSheet()->getStyle("H22")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G23")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G23", 'Gastos Financieros (X)');
  $objPHPExcel->getActiveSheet()->setCellValue("H23", "=E25");
  $objPHPExcel->getActiveSheet()->getStyle("H23")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->getStyle("G23")->applyFromArray($textRed);

  $objPHPExcel->getActiveSheet()->getStyle("G24")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G24", 'CUOTA ACTIVO FIJO');
  $objPHPExcel->getActiveSheet()->setCellValue("H24", $activo);
  $objPHPExcel->getActiveSheet()->getStyle("H24")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G25")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G25", 'Impuestos');
  $objPHPExcel->getActiveSheet()->setCellValue("H25", $impuesto);
  $objPHPExcel->getActiveSheet()->getStyle("H25")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G26")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G26", 'Otros');
  $objPHPExcel->getActiveSheet()->setCellValue("H26", $otro);
  $objPHPExcel->getActiveSheet()->getStyle("H26")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G27")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G27", 'Gastos oper. del Neg.');
  $objPHPExcel->getActiveSheet()->setCellValue("H27", "=SUM(H19:H26)");
  $objPHPExcel->getActiveSheet()->getStyle("H27")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->getStyle("H27")->applyFromArray($textBoldBlue);

  $objPHPExcel->getActiveSheet()->getStyle("G28")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G28", 'Utilidad Neta');
  $objPHPExcel->getActiveSheet()->setCellValue("H28", "=H18-H27");
  $objPHPExcel->getActiveSheet()->getStyle("H28")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->getStyle("G28:H28")->applyFromArray($fondoColorCeleste);
  $objPHPExcel->getActiveSheet()->getStyle("G28")->applyFromArray($textBold);
  $objPHPExcel->getActiveSheet()->getStyle("H28")->applyFromArray($textBoldBlue);

  $objPHPExcel->getActiveSheet()->getStyle("G29")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G29", 'Otros Ingresos');
  $objPHPExcel->getActiveSheet()->setCellValue("H29", "");
  $objPHPExcel->getActiveSheet()->getStyle("H29")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G30")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G30", 'Utilidad Neta de Otros Negocios');
  $objPHPExcel->getActiveSheet()->setCellValue("H30", "");
  $objPHPExcel->getActiveSheet()->getStyle("H30")->applyFromArray($contenidoTablaValorBorderRight);

  $objPHPExcel->getActiveSheet()->getStyle("G31")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G31", 'Gastos Familiares (Y)');
  $objPHPExcel->getActiveSheet()->setCellValue("H31", "=D39");
  $objPHPExcel->getActiveSheet()->getStyle("H31")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->getStyle("G31")->applyFromArray($textRed);

  $objPHPExcel->getActiveSheet()->getStyle("G32")->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("G32", 'Excedente Fam. Mensual');
  $objPHPExcel->getActiveSheet()->setCellValue("H32", "=H28+H29+H30-H31");
  

  $objPHPExcel->getActiveSheet()->getStyle("H32")->applyFromArray($contenidoTablaValorBorderRight);
  $objPHPExcel->getActiveSheet()->getStyle("G32:H32")->applyFromArray($fondoColorCeleste);
  $objPHPExcel->getActiveSheet()->getStyle("G32")->applyFromArray($textBold);
  $objPHPExcel->getActiveSheet()->getStyle("H32")->applyFromArray($textBoldBlue);

  $objPHPExcel->getActiveSheet()->getStyle('H14:H32')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

  $objPHPExcel->getActiveSheet()->getStyle("H$c:H32")->applyFromArray($bordeSeccion);
  $objPHPExcel->getActiveSheet()->getStyle("H14:H32")->applyFromArray($bordeSeccion);
  $objPHPExcel->getActiveSheet()->getStyle("G32:H32")->applyFromArray($bordeSeccion);
  $objPHPExcel->getActiveSheet()->getStyle("G$c:H32")->applyFromArray($bordeSeccion);


  // DETALEL DE INGRESOS
  $c = 14;

  $objPHPExcel->getActiveSheet()->setCellValue("I$c", '->');
  $objPHPExcel->getActiveSheet()->getStyle("I$c")->applyFromArray($textoCentro);

  $saltos_celda = 0;
  if($ingresos['estado']==1){
    foreach ($ingresos['data'] as $key => $value) {
      
      $objPHPExcel->getActiveSheet()->mergeCells("J$c:U$c");
      $objPHPExcel->getActiveSheet()->getStyle("J$c:U$c")->applyFromArray($contenidoTablaColumnaBorder);
      $objPHPExcel->getActiveSheet()->setCellValue("J$c", 'Ingreso: S/ '.formato_moneda($value['tb_ingresoegreso_monto']).'  |  Origen: '.$value['tb_ingresoegreso_origen'].'  |  Antigüedad: '.$value['tb_ingresoegreso_antiguedad']);
      $c++;

    }
    $c = $c + 1;
  }

  $objPHPExcel->getActiveSheet()->mergeCells("J$c:P".($c+6));
  $objPHPExcel->getActiveSheet()->getStyle("J$c:P".($c+6))->applyFromArray($contenidoTablaColumnaBorder);
  $objPHPExcel->getActiveSheet()->setCellValue("J$c", $detalle_ingreso);


  /*  */

  /* PROPUESTA DEL CRÉDITO */

  $c = 41;

  $titulo = "Propuesta de Crédito";
  $objPHPExcel->getActiveSheet()->mergeCells("B$c:H$c");
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("B$c:H$c")->applyFromArray($estiloTituloFilas2);

  $c = 42;

  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Servicio');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("C$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", "FINANCIAMIENTO");
  $objPHPExcel->getActiveSheet()->getStyle("C$c:D$c")->applyFromArray($contenidoTablaValor);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("E$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Producto');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", "GARMOB");
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 43;

  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Tasa Efectiva Anual');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("C$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", "4%");
  $objPHPExcel->getActiveSheet()->getStyle("C$c:D$c")->applyFromArray($contenidoTablaValor);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("E$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Forma de Pago');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", "Fecha Fija");
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 44;

  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Monto del Préstamo');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("C$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", $monto_solicitado);
  $objPHPExcel->getActiveSheet()->getStyle("C$c:D$c")->applyFromArray($contenidoTablaValor);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", $titulo_moneda);
  $objPHPExcel->getActiveSheet()->getStyle("E$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Frecuencia');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", "Mensual");
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 45;

  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Valor de Cuota');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("C$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("C$c:D$c")->applyFromArray($contenidoTablaValor);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", $titulo_moneda);
  $objPHPExcel->getActiveSheet()->getStyle("E$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Periodo de Gracia');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", "10 días");
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 46;

  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Tipo de Garantía');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("C$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", "PRECONSTITUCION");
  $objPHPExcel->getActiveSheet()->getStyle("C$c:D$c")->applyFromArray($contenidoTablaValor);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("E$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Fecha de 1era Cuota');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);

  $c = 47;

  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Valor de la Garantía');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->mergeCells("C$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", $monto);
  $objPHPExcel->getActiveSheet()->getStyle("C$c:D$c")->applyFromArray($contenidoTablaValor);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", "");
  $objPHPExcel->getActiveSheet()->getStyle("E$c:F$c")->applyFromArray($contenidoTablaValor);

  $objPHPExcel->getActiveSheet()->setCellValue("G$c", 'Plazo');
  $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($contenidoTablaColumnaBold);
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", $plazo);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($contenidoTablaValor);


  $objPHPExcel->getActiveSheet()->getStyle("B41:H41")->applyFromArray($bordeSeccion); // TITULO
  $objPHPExcel->getActiveSheet()->getStyle("G42:H47")->applyFromArray($bordeSeccion); 
  $objPHPExcel->getActiveSheet()->getStyle("B41:H47")->applyFromArray($bordeSeccion); // Toda tabla

  /*  */


  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('EEPP_GG');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

header("Content-type: text/xls");
header("Content-Disposition: attachment; filename=".$nombre_archivo.".xls");
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
