<?php
session_name("ipdnsac");
session_start();

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$cheque_id = $_POST['cheque_id'];
$proceso_id = $_POST['proceso_id'];
$usuario_id = $_SESSION['usuario_id'];

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_eliminar_cheque" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">MOTIVO DE ELIMINACIÓN</h4>
            </div>
            <form id="form_eliminar_cheque" method="post">
                <input name="action" id="action" type="hidden" value="eliminar_cheque">
                <input name="hdd_proceso_id" id="hdd_proceso_id" type="hidden" value="<?php echo $proceso_id; ?>">
                <input name="hdd_cheque_id" id="hdd_cheque_id" type="hidden" value="<?php echo $cheque_id; ?>">
                <input name="hdd_usuario_id" id="hdd_usuario_id" type="hidden" value="<?php echo $usuario_id; ?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 pb-3">
                            <div class="form-group">
                                <label>Motivo:</label>
                                <textarea id="txt_motivo" name="txt_motivo" class="form-control input-sm" rows="5"; style="resize: none;" placeholder="Registre el motivo de la eliminación de los cheques..."></textarea>
                            </div>
                        </div>

                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_eliminar_cheque">Eliminar</button>
                        <button type="button" class="btn btn-default" id="btn_cerrar_eliminar_cheque" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/proceso/proceso.js?ver=12012024';?>"></script>