<?php
/* error_reporting(0); */
session_name("ipdnsac");
session_start();
require_once('../../public/librerias/html2pdf/_tcpdf_5.9.206/tcpdf.php');

require_once ("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../ubigeo/Ubigeo.class.php");
$oUbigeo = new Ubigeo();
require_once ("../cuentadeposito/Cuentadeposito.class.php");
$oCuentadeposito = new Cuentadeposito();
require_once ("../vehiculoadicional/Vehiculoadicional.class.php");
$oVehiculoadicional = new Vehiculoadicional();
require_once("../tarifario/Tarifario.class.php");
$oTarifario = new Tarifario();

//require_once("../funciones/formato.php");
//require_once("../funciones/numletras.php");
require_once("../funciones/fechas.php");
require_once("../funciones/operaciones.php");

$dts = $oTarifario->mostrar_por_fecha_periodo(date('Y-m-d'));
$dt = mysql_fetch_array($dts);
$igv = moneda_mysql($dt['tb_tarifario_igv']);
mysql_free_result($dts);

$title = 'Anexo 01 Simulador';
$codigo = 'CGV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);

  $dts=$oCredito->mostrarUno($_GET['d1']);
  $dt = mysql_fetch_array($dts);

  $cre_id =$dt['tb_credito_id'];

  $reg=mostrar_fecha_hora($dt['tb_credito_reg']);
  $mod=mostrar_fecha_hora($dt['tb_credito_mod']);
  $apr=mostrar_fecha_hora($dt['tb_credito_apr']);

  $usureg=$dt['tb_credito_usureg'];
  $usumod=$dt['tb_credito_usumod'];
  $usuapr=$dt['tb_credito_usuapr'];

  $subper = $dt['tb_cuotasubperiodo_id']; //sub periodo del credito: 1 mes, 2 quincena, 3 monto_cuo
  $mon_pro = $dt['tb_credito_monpro']; //monto del prorrateo si es que tiene

  $cuotip_id=$dt['tb_cuotatipo_id'];
  $mon_id=$dt['tb_moneda_id'];
  if($mon_id == 1){
    $moneda = 'S/.';
  }else{
    $moneda = '$';
  }
  
  //datos para calculos de porcentaje inicial
  $cre_tipcam = floatval($dt['tb_credito_tipcam']);
  $cre_ini = floatval($dt['tb_credito_ini']);

  //fechas
  $cre_feccre=mostrarFecha($dt['tb_credito_feccre']);
  $cre_fecdes=mostrarFecha($dt['tb_credito_fecdes']);
  $cre_fecfac=mostrarFecha($dt['tb_credito_fecfac']);
  $cre_fecpro=mostrarFecha($dt['tb_credito_fecpro']);
  $cre_fecent=mostrarFecha($dt['tb_credito_fecent']);


  $cre_fecren=mostrarFecha($dt['tb_credito_fecren']); //vencimiento de SOAT
  $cre_fecseg=mostrarFecha($dt['tb_credito_fecseg']); //vencimiento STR
  $cre_fecgps=mostrarFecha($dt['tb_credito_fecgps']); //vencimiento GPS

  $cre_preaco = floatval($dt['tb_credito_preaco']);
  $cre_int = $dt['tb_credito_int'];
  $cre_numcuo = $dt['tb_credito_numcuo'];
  $cre_linapr = floatval($dt['tb_credito_linapr']);
  $cre_numcuomax = $dt['tb_credito_numcuomax'];
  $cre_tip2 = $dt['tb_credito_tip2']; //5 refinanciado amortizado, 6 refinanciado sin amortizar
  $cre_tipo = $dt['tb_credito_tip']; //sub tipo de credito 1 COMPRA VENTA, 2 adenda, 3 AP, 4 GARANTI MOBILIARIA

  //$nombre_tip_cre = 'CREDITO DE COMPRA VENTA';
  //if($cre_tipo == 4)
    $nombre_tip_cre = 'CREDITO GARANTÍA MOBILIARIA';
  $transferencia_bien=$cre_linapr;

  //cuenta deposito
  $cuedep_id = $dt['tb_cuentadeposito_id'];

  $rws=$oCuentadeposito->mostrarUno($cuedep_id);
  $rw = mysql_fetch_array($rws);
    $cuedep_num = $rw['tb_cuentadeposito_num'];
    $cuedep_ban = $rw['tb_cuentadeposito_ban'];
    $cuedep_mon_nom=$rw['tb_moneda_nom'];
    $cuedep_mon_des=$rw['tb_moneda_des'];
  mysql_free_result($rws);


  $cre_pla = $dt['tb_credito_pla'];
  $cre_comdes = $dt['tb_credito_comdes'];
  $cre_obs = $dt['tb_credito_obs'];
  $cre_webref = $dt['tb_credito_webref'];

  $cre_est= $dt['tb_credito_est'];

  //cliente
  $cli_id=$dt['tb_cliente_id'];

  $cli_estciv = $dt['tb_cliente_estciv']; // 1 soltero, 2 casado
  $cli_bien = $dt['tb_cliente_bien']; // 1 separacion de bienes, 2 sin separacion
  $cli_firm = $dt['tb_cliente_firm']; // 1 firma solo, 2 firma con conyuge
  $cli_zon_id = $dt['tb_zonaregistral_id']; // id de la zona registral donde está el N° de partida
  $cli_numpar = $dt['tb_cliente_numpar']; //numero de partida del cliente

  $cli_doc = $dt['tb_cliente_doc'];
  $cli_nom=$dt['tb_cliente_nom'];
  $cli_dir=$dt['tb_cliente_dir'];
  $cli_ema=$dt['tb_cliente_ema'];
  $cli_fecnac=$dt['tb_cliente_fecnac'];
  $cli_tel=$dt['tb_cliente_tel'];
  $cli_cel=$dt['tb_cliente_cel'];
  $cli_telref=$dt['tb_cliente_telref'];
  $cli_ubigeo=$dt['tb_ubigeo_cod'];
  //RAZON SOCIAL
  $cli_tip = $dt['tb_cliente_tip']; // 1 persona natural, 2 persona juridica
  $cliente_empruc = $dt['tb_cliente_empruc'];
  $cliente_emprs = $dt['tb_cliente_emprs'];
  $cliente_empdir = $dt['tb_cliente_empdir'];

mysql_free_result($dts);

if($cli_ubigeo>0)
{
  $dts=$oUbigeo->mostrarUbigeo($cli_ubigeo);
  while ($dt = mysql_fetch_array($dts)) {
    $ubi_dep = $dt['Departamento'];
    $ubi_pro = $dt['Provincia'];
    $ubi_dis = $dt['Distrito'];
  }
  mysql_free_result($dts);
}

if($usureg>0)
{
  $rws=$oUsuario->mostrarUno($usureg);
  $rw = mysql_fetch_array($rws);
    $usureg =$rw['tb_usuario_nom']." ".$rw['tb_usuario_ape'];
  mysql_free_result($rws);
}

if($usumod>0)
{
  $rws=$oUsuario->mostrarUno($usumod);
  $rw = mysql_fetch_array($rws);
    $usumod =$rw['tb_usuario_nom']." ".$rw['tb_usuario_ape'];
  mysql_free_result($rws);
}

if($usuapr>0)
{
  $mos_usuapr=1;
  $rws=$oUsuario->mostrarUno($usuapr);
  $rw = mysql_fetch_array($rws);
    $usuapr =$rw['tb_usuario_nom']." ".$rw['tb_usuario_ape'];
  mysql_free_result($rws);
}

//obtenemos información del cliente, sus nombres, su esposa(o), copropietario o apoderado
$esposa = ''; $apoderado = '';
$clientes = '<strong>'.$cli_nom.'</strong>, identificado con DNI N° <strong>'.$cli_doc.'</strong> con domicilio en <strong>'.trim($cli_dir).'</strong>, del Distrito de <strong>'.$ubi_dis.'</strong>, Provincia de <strong>'.$ubi_pro.'</strong>, del Departamento de <strong>'.$ubi_dep.'</strong>';

//si el cliente es casado
if($cli_estciv == 2){
  $esposa = devolver_datos_esposa_apoderado($cli_id, $cli_bien, $cli_firm, 1); //parametro 1 CONYUGAL
  $clientes = $clientes.' '.$esposa;
}

$apoderado = devolver_datos_esposa_apoderado($cli_id, $cli_bien, $cli_firm, 3); //parametro 3 apoderado

if($apoderado != '')
  $clientes = $clientes.', '.$apoderado;

//verificar si el cliente tiene co-propietarios
$copro = devolver_datos_copropietarios($cli_id);

if($copro != '')
  $clientes .= ' '.$copro;

$clientes = str_replace(' ;', ';', $clientes);

list($day, $month, $year) = split('-', $cre_feccre);
$feccre_dia = $day;
$feccre_mes = $month;
$feccre_ano = $year;
$fec_width = 'width="32%"';

if($subper == 1){
  $cols = 1;
  $name_per = "MENSUAL";
  $fec_width = 'width="8%"';
}
if($subper == 2){
  $cols = 2;
  $name_per = "QUINCENAL";
  $fec_width = 'width="16%"';
}
if($subper == 3){
  $cols = 4;
  $name_per = "SEMANAL";
}

$dts = $oCuota->primera_cuota_por_credito(3, $cre_id);
  $dt = mysql_fetch_array($dts);
  $cuo_cuo = floatval($dt['tb_cuota_cuo']);
mysql_free_result($dts);

//calculos de porcentajes de incial y más
$porcen_ini = $cre_ini*100 / ($cre_preaco + $cre_ini);
$cre_ini_sol = $cre_ini * $cre_tipcam;
$cre_preaco_sol = $cre_preaco * $cre_tipcam;
$valor_cre = $cre_preaco + $cre_ini;
$cuo_cuo_sol = $cuo_cuo * $cre_tipcam;
$cuo_sol_dia = $cuo_cuo_sol / 30;
$sobrecosto_tot = $cre_linapr - $cre_preaco;

$dts = $oCuota->filtrar(3, $cre_id); // 3 creditotipo 3 CRE GARVEH

$tipo_cliente = '
  <tr>
    <td>Cliente</td>
    <td colspan="3"><b>'.$cli_nom.' | '.$cli_doc.'</b></td>
  </tr>
';
if(intval($cli_tip) == 2){
  $tipo_cliente = '
  <tr>
    <td>Cliente</td>
    <td colspan="3"><b>'.$cliente_emprs.' | '.$cliente_empruc.'</b></td>
  </tr>
  <tr>
    <td>Gerente</td>
    <td colspan="3"><b>'.$cli_nom.' | '.$cli_doc.'</b></td>
  </tr>
';
}

$SUMA_PRORRATEO = 0;

class MYPDF extends TCPDF {

  public function Header() {
    $image_file = K_PATH_IMAGES.'logo.jpg';
    $this->Image($image_file, 20, 10, 71, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    //$this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(15, 40, 5);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  $pdf->setLanguageArray($l);

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');
  
  $html = '
    <style>
      .tb_crono{
        font-size: 6pt;
      }
      td{
        padding-top: 5px;
        padding-bottom: 5px;
      }
    </style>
  ';

  $html .= '<p align="center"><strong><u>ANEXO N° 1 CRONOGRAMA DE GARANTÍA VEHICULAR '.$codigo.'</u></strong></p>';

  $html .='
    <table width="70%" border="1" cellpadding="3">
      <tr>
        <td colspan="4" style="background-color: rgb(33,175,57); color: white;"><b>'.$nombre_tip_cre.'</b></td>
      </tr>
      '.$tipo_cliente.'
      <tr>
        <td>FECHA DE DESEMBOLSO</td>
        <td>'.$cre_fecdes.'</td>
        <td colspan="2" align="center" valign="middle" rowspan="5">
          <h2>'.$moneda.'</h2>
        </td>
      </tr>
      <tr>
        <td>Valor del Préstamo</td>
        <td align="right">'.$moneda.formato_money($cre_preaco).'</td>
      </tr>
      <tr>
        <td>Tasa de Interés</td>
        <td align="right">'.formato_money($cre_int).'%</td>
      </tr>
      <tr>
        <td>Total a Pagar</td>
        <td align="right">'.$moneda.formato_money($cre_linapr).'</td>
      </tr>
      <tr>
        <td>Cantidad de Cuotas</td>
        <td align="right">'.$cre_numcuo.'</td>
      </tr>
    </table>
  ';

  $html .='
    <p></p>
    <table width="100%" border="1" class="tb_crono" cellpadding="3">
      <thead>
        <tr>
          <th align="center" width="5%">N°</th>
          <th align="center" colspan="'.$cols.'" '.$fec_width.'>FECHAS</th>
          <th align="center">CAPITAL</th>
          <th align="center" width="10%">AMORTIZACIÓN</th>
          <th align="center">INTERÉS</th>
          <th align="center">PRORRATEO</th>
          <th align="center">IGV</th>
          <th align="center">GPS</th>
          <th align="center">CUOTA</th>
          <th align="center">'.$name_per.'</th>
        </tr>
      </thead>
      <tbody>';
        while($dt = mysql_fetch_array($dts)){
          if($dt['tb_moneda_id'] == '1'){
            $moneda = 'S/. ';
          }
          if($dt['tb_moneda_id'] == '2'){
            $moneda = 'US$. ';
          }

          $total_adicionales += $dt['tb_cuota_pregps'];

          $SUMA_PRORRATEO += $dt['tb_cuota_pro']; 
          $html .='
            <tr class="even">
              <td align="center" width="5%">'.$dt['tb_cuota_num'].'</td>';
                $rws = $oCuotadetalle->filtrar($dt['tb_cuota_id']);
                $num_cuo = mysql_num_rows($rws);
                  while($rw = mysql_fetch_array($rws)){
                    $monto_cuo = $rw['tb_cuotadetalle_cuo'];
                    $fecha = mostrarFecha($rw['tb_cuotadetalle_fec']);
                      
                    $html .= '<td align="center" width="8%">'.$fecha.'</td>';
                    
                  }
                mysql_free_result($rws);
                $interes_igv = moneda_mysql($dt['tb_cuota_int']) - (moneda_mysql($dt['tb_cuota_int']) / 1.18);
                $interes_sin_igv = moneda_mysql($dt['tb_cuota_int']) - $interes_igv;
              $html .='
              <td align="right">'.$moneda.' '.formato_money($dt['tb_cuota_cap']).'</td>
              <td align="right" width="10%">'.$moneda.' '.formato_money($dt['tb_cuota_amo']).'</td>
              <td align="right">'.$moneda.' '.formato_money($interes_sin_igv).'</td>
              <td align="right">'.$moneda.' '.formato_money($dt['tb_cuota_pro']).'</td>
              <td align="right">'.$moneda.' '.formato_money($interes_igv).'</td>
              <td align="right">'.$moneda.' '.formato_money($dt['tb_cuota_pregps']).'</td>
              <td align="right">'.$moneda.' '.formato_money($dt['tb_cuota_cuo']).'</td>
              <td align="right">'.$moneda.' '.formato_money($monto_cuo).'</td>
            </tr>';

            $credito_total += $dt['tb_cuota_cuo'];
        }

      $interes_total = $credito_total - $cre_preaco - $total_adicionales;
      $html .='
      </tbody>
    </table>
    <p></p>

    <table width="70%" border="1" cellpadding="3">
      <tr>
        <th align="center" style="background-color: rgb(33,175,57); color: white;">N° de cuota</th>
        <th align="center" style="background-color: rgb(33,175,57); color: white;">Capital</th>
        <th align="center" style="background-color: rgb(33,175,57); color: white;">Interés Total</th>
        <th align="center" style="background-color: rgb(33,175,57); color: white;">Total GPS</th>
        <th align="center" style="background-color: rgb(33,175,57); color: white;">Total a Pagar</th>
      </tr>
      <tr>
        <td align="center">'.$cre_numcuo.'</td>
        <td align="center">'.formato_money($cre_preaco).'</td>
        <td align="center">'.formato_money($interes_total + $SUMA_PRORRATEO).'</td>
        <td align="center">'.formato_money($total_adicionales).'</td>
        <td align="center">'.formato_money($credito_total).'</td>
      </tr>
      <tr>
        <td colspan="4"></td>
      </tr>
    </table>
    <p></p>

    <table style="width:100%;padding-top:50px;padding-bottom:3px;">
      <tr>
        <td colspan="15" border="1.5" align="bottom">Firma:</td>

        <td colspan="5" border="1.5" align="bottom";>Huella:</td>
        <td colspan="2"></td>
        <td colspan="15" border="1.5" align="bottom">Firma:</td>

        <td colspan="5" border="1.5" align="bottom";>Huella:</td>
      </tr>
    </table>
    <table style="width:100%;padding-top:5px;padding-bottom:3px;">
        <tr>
          <td colspan="20" border="1.5" align="bottom">Nombre:</td>
          <td colspan="2"></td>
          <td colspan="20" border="1.5" align="bottom">Nombre:</td>
        </tr>
    </table>
    <table style="width:100%;padding-top:5px;padding-bottom:3px;">
        <tr>
          <td colspan="20" border="1.5" align="bottom">Dni:</td>
          <td colspan="2"></td>
          <td colspan="20" border="1.5" align="bottom">Dni:</td>
        </tr>
    </table>
    <p></p>

    <table style="width:100%;padding-top:5px;padding-bottom:3px;padding-left:-5px;">
      <tr>
        <td border="1.5">
          <table>
            <tr>
              <td align="left">www.prestamosdelnortechiclayo.com</td>
              <td align="right">contacto@prestamosdelnortechiclayo.com<span> </span></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  ';

// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$codigo."_".$title.".pdf";
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>