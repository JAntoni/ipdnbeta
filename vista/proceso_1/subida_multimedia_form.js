
$(document).ready(function(){
	$('#file_upload').uploadifive({
	  'auto'             : false,
	  //'checkScript'      : 'programacion_file_check.php',
	  'formData'         : {
								'action'    : 'subir_documento',
								//'credito_id' : $('#hdd_credito_id').val(),
								//'cliente_id' : $('#hdd_cliente_id').val(),
								'proceso_fase_item_id' : $('#hdd_proceso_fase_item_id').val(),
								'item_des' : $('#hdd_item_des').val(),
								'item_idx' : $('#hdd_item_idx').val(),
								'id_reg_mod' : $('#hdd_id_reg_mod').val(),
								'pdf_is' : $('#hdd_pdf_is').val()
	                       },
	  'queueID'          : 'queue',
	  'uploadScript'     : VISTA_URL+'proceso/proceso_controller.php',
	  'queueSizeLimit'   : 10,
	  'uploadLimit'      : 10,
	  'multi'            : true,
	  'buttonText'       : 'Seleccionar Archivo',
	  'height'           : 20,
	  'width'            : 180,
	  'fileSizeLimit'    : '50MB',
	  'fileType': ['pdf'],
	  'onUploadComplete' : function(file, data) {
		let valor = JSON.parse(data)
		if(valor.estado == 1){
			alerta_success("EXITO", "SE HA REGISTRADO CORRECTAMENTE EL DOCUMENTO");
			$("#modal_proceso_fase_item_form").modal('hide');
			verProcesoFaseContenido(valor.proceso_fase_id);
		}else{
			$('#alert_img').show(300);
			$('#alert_img').append('<strong>El archivo: '+file.name+', no se pudo subir -> '+data+'</strong><br>');
		}
		
	  },
	  'onError': function (errorType) {
	      var message = "Error desconocido.";
	      if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
	          message = "Tamaño de archivo debe ser menor a 3MB.";
	      } else if (errorType == 'FORBIDDEN_FILE_TYPE') {
	          message = "Tipo de archivo no válido.";
	          alert(message);
	      }
	  },
	  'onAddQueueItem' : function(file) {
	  },
	});

	$('#form_proceso_fase_item_doc').submit(function(event) {
		event.preventDefault();
		$.ajax({
				type: "POST",
				url: VISTA_URL+"creditogarvehpdf/credito_pdf_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_proceso_fase_item_doc").serialize(),
				beforeSend: function() {
					$('#garantiafile_mensaje').show(400);
					$('#btn_guardar_creditogarvehpdf').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#garantiafile_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#garantiafile_mensaje').html(data.mensaje);
		      	//var garantiafile_id = $('#hdd_garantiafile_id').val();
		      	//$('#garantiafileid_'+garantiafile_id).hide(400);
		      	$('#modal_proceso_fase_item_form').modal('hide');
					}
					else{
		      	$('#garantiafile_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#garantiafile_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_creditogarvehpdf').prop('disabled', false);
		      }
				},
				complete: function(data){
				},
				error: function(data){
					$('#garantiafile_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#garantiafile_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	});
});

function eliminar(multimedia_id){
    $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/proceso_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'eliminar_documento',
	  proceso_fase_item_id: $('#hdd_proceso_fase_item_id').val(),
	  item_des: $('#hdd_item_des').val(),
      multimedia_id: multimedia_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
        if(parseInt(data.estado)>0){
          alerta_success("EXITO","Documento eliminado correctamente");
          $("#modal_proceso_fase_item_ver").modal('hide');
		  verProcesoFaseContenido(data.proceso_fase_id);
        }
      },
    complete: function(data){
      //$('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
};

function verProcesoFaseContenido(proceso_fase_id) {
	var id = 0;
	if(proceso_fase_id==null || proceso_fase_id=='' || proceso_fase_id==undefined){
	  id = $('#hdd_proceso_fase_id_'+proceso_fase_id).val();
	}else{
	  id = proceso_fase_id;
	}
	
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "proceso/proceso_vista_fase_contenido.php",
	  async: true,
	  dataType: "html",
	  data: ({
		usuario_id: $('#hdd_usuario_id').val(),
		proceso_id: $('#hdd_proceso_id').val(),
		proceso_fase_id: id,
	  }),
	  beforeSend: function () {
	  },
	  success: function (html) {
		$('#div_contenido'+id).html(html);
	  },
	  complete: function (html) {
	  }
	});
  }