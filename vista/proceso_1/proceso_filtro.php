<form id="proceso_filtro" name="proceso_filtro">
    <div class="row">

        <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha </label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fecha_ini" id="txt_fecha_ini" value="<?php echo date('d-m-Y'); ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" value="<?php echo date('t-m-Y'); ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-1">
            <div class="form-group">
                <label>N° Crédito:</label>
                <input type="text" name="txt_num_credito" id="txt_num_credito" class="form-control input-sm" onKeyUp="if(event.keyCode=='13'){verPaginaproceso('1');}">
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label>Cliente:</label>
                <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm" onKeyUp="if(event.keyCode=='13'){verPaginaproceso('1');}">
                <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" class="form-control input-sm">
				<input type="hidden" value="proceso" id="sufijo" name="sufijo"/>
				<input type="hidden" value="0" id="nroPagina" name="nroPagina"/>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <input type="hidden" id="hdd_usuario_id">
                <label>Colaborador:</label>
                <select id="cmb_usuario_id" name="cmb_usuario_id" class="selectpicker form-control" data-live-search="true" data-max-options="1">
                    <?php 
                        include VISTA_URL.'usuario/usuario_select.php';
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-1">
            <div class="form-group">
                <label>Estado:</label>
                <select id="cmb_estado" name="cmb_estado" class="form-control" onchange="verPaginaproceso('1');">
                    <option value="0">TODOS</option>
                    <option value="1">PENDIENTE</option>
                    <option value="2">APROBADO</option>
                    <option value="3">VIGENTE</option>
                    <option value="4">PARALIZADO</option>
                    <option value="5">REFINANCIADO C/AMORT</option>
                    <option value="6">REFINANCIADO S/AMORT</option>
                    <option value="7">LIQUIDADO</option>
                    <option value="8">RESUELTO</option>
                    <option value="9">TITULO</option>
                    <option value="10">DOCUMENTOS</option>
                    <option value="11">ELIMINADOS</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <div style="padding-top: 22px">
                    <button type="button" class="btn btn-sm btn-info" onclick="verPaginaproceso('1')"><i class="fa fa-search"></i> Buscar</button>
                    <!-- <button type="button" class="btn btn-primary btn-sm" id="btn-nuevo-proceso" name="btn-nuevo-proceso" onclick="btnNuevoProceso()"><i class="fa fa-plus"></i> Nuevo</button> -->
                </div>
            </div>
        </div>
    </div>
    
</form>