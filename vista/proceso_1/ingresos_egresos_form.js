
$(document).ready(function(){

	detalleIngresosEgresos('I');
	detalleIngresosEgresos('E');
	tipoGasto();
});

function guardarIngresoEgreso(form, type, prefijo) {
	
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/proceso_controller.php",
		async: true,
		dataType: "json",
		data: $("#frmNuevo"+type).serialize(),
		beforeSend: function() {
			//$('#proveedor_mensaje').show(400);
			//$('#divNuevo'+type).loadingOverlay();
			$('#btn'+type).prop('disabled', true);
		},
		success: function(data){
			if(data.estado==1){
				notificacion_success(data.mensaje, 3000);
				
				if(prefijo=='I'){
					detalleIngresosEgresos('I');

					$('#divNuevoIngreso').addClass('hidden'); 
					$('#frmNuevoIngreso').trigger('reset'); 
					$('#rowPlusIngreso').removeClass('hidden');
				}else{
					detalleIngresosEgresos('E');

					limpiarCampos();

					$('#divNuevoEgreso').addClass('hidden'); 
					$('#frmNuevoEgreso').trigger('reset'); 
					$('#rowPlusEgreso').removeClass('hidden');
				}
			}else{
				notificacion_warning(data.mensaje, 3000);
			}
		},
		complete: function(data){
			//$('#divNuevo'+type).loadingOverlay('remove');
			$('#btn'+type).prop('disabled', false);
		},
			
		});
	 
}

function guardarIngresoEgresoDetalle(form, type, prefijo) {
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/proceso_controller.php",
		async: true,
		dataType: "json",
		data: $("#"+form).serialize(),
		beforeSend: function() {
		},
		success: function(data){
			if(data.estado==1){
				notificacion_success(data.mensaje, 3000);
			}else{
				notificacion_warning(data.mensaje, 3000);
			}
		},
		complete: function(data){
		},
			
	});
}

// en uso para paginado / numero celular
function solo_numero(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}

function tipoGasto() {
	let tipo = $("#tipo_e").val();
	if(tipo === 'financiero'){
		$("#divSustento").removeClass("col-md-4");

		$("#divGastoMe").removeClass("hidden");
		$("#divEntidad").removeClass("hidden");
		$("#divDetalleFamiliar").addClass("hidden");
		$("#divDetalleEmpresa").addClass("hidden");
		$("#divFrecuencia").addClass("col-md-4");
		$("#divSustento").addClass("col-md-8");
		
	}else if(tipo === 'familiar'){
		$("#divSustento").removeClass("col-md-8");

		$("#divGastoMe").addClass("hidden");
		$("#divEntidad").addClass("hidden");
		$("#divDetalleFamiliar").removeClass("hidden");
		$("#divDetalleEmpresa").addClass("hidden");
		$("#divFrecuencia").addClass("col-md-4");
		$("#divSustento").addClass("col-md-4");

	}else if(tipo === 'empresa'){
		$("#divSustento").removeClass("col-md-8");

		$("#divGastoMe").addClass("hidden");
		$("#divEntidad").addClass("hidden");
		$("#divDetalleFamiliar").addClass("hidden");
		$("#divDetalleEmpresa").removeClass("hidden");
		$("#divFrecuencia").addClass("col-md-4");
		$("#divSustento").addClass("col-md-4");
	}
}

function detalleIngresosEgresos(type){
  
	$.ajax({
		type: "POST",
		url: VISTA_URL + "proceso/proceso_controller.php",
		async:true,
		dataType: "json",                      
		data: ({
		  action: 'detalle_ingresos_egresos_cliente',
		  type: type,
		  proceso_id: parseInt($('#hdd_ie_proceso_id').val()),
		  usuario_id: parseInt($('#hdd_ie_usuario_id').val()),
		  tipo_cambio: parseFloat($('#hdd_ie_tipo_cambio').val()),
		}),
		beforeSend: function() {
		},
		success: function(data){
			let ingresoegreso = 0.00;

			if(type=='I'){
				$('#listaIngresoCliente').html(data.html);  
				let ingreso = parseFloat(data.total_ingresoegreso);
				$("#monto_ingreso").val(ingreso);
			}else{
				$('#listaEgresoCliente').html(data.html);  
				let egreso = parseFloat(data.total_ingresoegreso);
				$("#monto_egreso").val(egreso);
			}
			ingresoegreso = parseFloat($("#monto_ingreso").val()) - parseFloat($("#monto_egreso").val());
			if(ingresoegreso<0){
				$("#total_IE").addClass("text-red");
			}else{
				$("#total_IE").removeClass("text-red");
			}
			let totalIE = document.getElementById("total_IE");
			totalIE.innerHTML = "<strong>Ingresos - Gastos = S/ "+ingresoegreso.toFixed(2)+"</strong>";
		}
	});
  
}

function limpiarCampos(type){
	/* $("#monto_i").val('');
	//$("#moneda_i").val('');
	$("#origen_i").val('');
	$("#antiguedad_i").val('');
	$("#sustento_i").val('');
	$("#verificacion_i").val('');
	$("#observacionT").val(''); */

	$("#divSustento").removeClass("col-md-4");

	$("#divGastoMe").removeClass("hidden");
	$("#divEntidad").removeClass("hidden");
	$("#divDetalleFamiliar").addClass("hidden");
	$("#divDetalleEmpresa").addClass("hidden");
	$("#divFrecuencia").addClass("col-md-4");
	$("#divSustento").addClass("col-md-8");
}

function eliminar_ingresoegreso(ingresoegreso_id, type){
	Swal.fire({
		title: '¿SEGURO QUE DESEA ELIMINAR EL REGISTRO?',
		icon: 'info',
		showCloseButton: true,
		showCancelButton: false,
		focusConfirm: false,
		confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
		showDenyButton: true,
		denyButtonText: '<i class="fa fa-band"></i> Cancelar',
		confirmButtonColor: '#3b5998', 
		denyButtonColor: '#DD3333',
		}).then((result) => {
	
		if (result.isConfirmed) {

			$.ajax({
				type: "POST",
				url: VISTA_URL + "proceso/proceso_controller.php",
				async: false,
				dataType: "json",
				data: ({
					action: 'eliminar_ingresoegreso',
					ingresoegreso_id: ingresoegreso_id
				}),
				beforeSend: function () {
				},
				success: function (html) {
					if(type=='I'){
						detalleIngresosEgresos('I');
					}else{
						detalleIngresosEgresos('E');
					}
					notificacion_success('Registro eliminado correctamente.', 2000);
				},
			});
		}
	});
}

function verProcesosAntiguos(proceso_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/ingresos_egresos_antiguos.php",
		async:true,
		dataType: "html",                      
		data: ({
			proceso_id: proceso_id,
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_ingresos_egresos_antiguos').html(html);		
      $('#modal_ingresos_egresos_antiguos').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_ingresos_egresos_antiguos', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_ingresos_egresos_antiguos'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}