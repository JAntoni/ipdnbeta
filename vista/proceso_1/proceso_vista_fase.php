<?php
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

$usuario_id = $_POST['usuario_id'];

$proceso_id = 0;
if (isset($_POST['proceso_id'])) {
  $proceso_id = $_POST['proceso_id'];
}
$proceso = $oProceso->mostrarUno($proceso_id);
$listaFases = $oProceso->obtenerFasesProceso($proceso_id);

if ($proceso['estado'] == 1) {
  $proceso_tipo = '';
  switch (intval($proceso['data']['tb_cgarvtipo_id'])) {
    case 1:
      $proceso_tipo = "PRECONSTITUCION";
      break;
    case 2:
      $proceso_tipo = "CONSTITUCION SIN CUSTODIA";
      break;
    case 3:
      $proceso_tipo = "CONSTITUCION CON CUSTODIA";
      break;
    case 4:
      $proceso_tipo = "CONSTITUCION - COMPRAVENTA A UN TERCERO SIN CUSTODIA";
      break;
    case 5:
      $proceso_tipo = "CONSTITUCION - COMPRAVENTA A UN TERCERO CON CUSTODIA";
      break;
    case 6:
      $proceso_tipo = "ADENDA";
      break;
    default:
      $proceso_tipo = " ";
  }

  if (isset($proceso['data']['tb_cliente_id'])) {
    //$precliente_id = $proceso['data']['tb_precliente_id'];
    $cliente_id = $proceso['data']['tb_cliente_id'];
    $cliente = $oCliente->mostrarUno($cliente_id);
    $cliente_nom = $cliente['data']['tb_cliente_nom'];
  } elseif (isset($proceso['data']['tb_precliente_id'])) {
    $precliente = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);
    $precliente_id = $proceso['data']['tb_precliente_id'];
    $cliente_id = ($precliente['data']['tb_cliente_id']) == null ? 0 : intval($precliente['data']['tb_cliente_id']);
    $cliente_nom = ($precliente['data']['tb_precliente_nombres'] == null || $precliente['data']['tb_precliente_nombres'] == '') ? '' : $precliente['data']['tb_precliente_nombres'];
  }
}

// obtener ultima fase activa
$faseActivas = [];
$ultimaFaseActiva = 0;
if ($listaFases['estado'] == 1) {
  $orden_fase = 0;
  $id_fase = 0;
  foreach ($listaFases['data'] as $key => $value) {
    if (intval($value['tb_proceso_fase_proc']) == 1) {
      array_push($faseActivas, intval($value['tb_proceso_fase_id']));
    }
  }
  $ultimaFaseActiva = end($faseActivas);
}

?>

<div id="vista_fase" class="content-wrapper hidden">

  <section class="content-header">
    <h1>
      <?php echo $menu_tit; ?>
      <small><?php echo $menu_des; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Créditos</a></li>
      <li class=""><i class="fa fa-dashboard"></i> Procesos</li>
      <li class="active"><a href="javascript:void(0)"></i>Fase</a></li>

    </ol>
  </section>

  <section class="content-header">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <div style="padding-top: 12px">
            <button type="button" class="btn btn-danger" onclick="btnVolverMenu('editar')"><i class="fa fa-arrow-left"></i> Lista Procesos</button>
          </div>
        </div>
      </div>
      <div class="my-fixed-item bg-warning h5">
        <strong><?php echo $cliente_nom . ' | ' . $proceso_tipo; ?></strong>
      </div>
    </div>
  </section>

  <section class="content">

    <div class="box">
      <input type="hidden" name="hdd_proceso_id" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" name="hdd_precliente_id" id="hdd_precliente_id" value="<?php echo $precliente_id; ?>">
      <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>">

      <div class="box-body">

        <?php
        if ($listaFases['estado'] == 1) {
          $orden_fase = 0;
          $id_fase = 0;
          foreach ($listaFases['data'] as $key => $value) {
            $orden_fase = intval($value['tb_proceso_fase_orden']);
            $id_fase = intval($value['tb_proceso_fase_id']);
            $new_cred = 'hidden';
            if ($value['tb_proceso_fase_orden'] == '2') {
              $new_cred = '';
            }
            $clase_box = "collapsed-box";
            $color_box = "box-default";
            $boton_box = "fa-plus";
            $bloqueo_boton = "disabled";
            $vision_comentario = "hidden";
            if ($value['tb_proceso_fase_proc'] > 0) { // estado "procede" | la fase 1 siempre iniciará activa
              if (intval($value['tb_proceso_fase_id']) == $ultimaFaseActiva) {
                $clase_box = "box-solid";
                $style_bg = '';
                $style_bg_body = '';
              } else {
                $clase_box = "collapsed-box";
                $style_bg = 'background: #00a65a; background-color: #00a65a;';
                $style_bg_body = 'background-color: #fff;';
              }
              $color_box = "box-success";
              $boton_box = "fa-minus";
              $bloqueo_boton = "";
              $vision_comentario = "";
            }
        ?>

            <div class="box <?php echo $color_box . " " . $clase_box; ?>" style="<?php echo $style_bg; ?>">
              <div class="box-header with-border">
                <h3 class="box-title" style="color: #000000;"><strong><?php echo "(".$id_fase.") FASE " . $value['tb_proceso_fase_orden'] . ": " . $value['tb_fase_des']; ?></strong></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" <?php echo $bloqueo_boton; ?>><i class="fa <?php echo $boton_box; ?>"></i></button>
                </div>
              </div>
              <div class="box-body" style="<?php echo $style_bg_body; ?>">

                <input
                  type="hidden"
                  name="hdd_proceso_fase_id_<?php echo $value['tb_proceso_fase_id']; ?>"
                  id="hdd_proceso_fase_id_<?php echo $value['tb_proceso_fase_id']; ?>"
                  value="<?php echo $value['tb_proceso_fase_id']; ?>">

                <!-- Seccion CONTENIDO de la fase -->
                <div id="div_contenido<?php echo $value['tb_proceso_fase_id']; ?>" class="content">
                  <?php require('proceso_vista_fase_contenido.php'); ?>
                </div>
                <!--  -->

                <!-- Seccion NOTAS -->
                <?php if ($value['tb_fase_nota'] != '' || $value['tb_fase_nota'] != null) { ?>
                  <div class="box-body chat" id="chat-box">
                    <div class="item">
                      <div class="attachment" style="margin-left: 15px !important;">
                        <h4><strong>NOTAS:</strong></h4>
                        <?php
                        $notas = explode('-', $value['tb_fase_nota']);
                        foreach ($notas as $key => $nota) {
                          if ($key > 0) {
                            echo '<p class="filename">- ' . $nota . '</p>';
                          }
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                <!--  -->

                <!-- Puntos de COMITE -->
                <?php if ($value['tb_proceso_fase_orden'] == '2') { ?>
                  <div align="center" class="col-md-12">
                    <h4 class="" style="color: red;"><strong><?php echo 'Votos a favor: ' . $voto_comite_si . '<br> Votos en contra: ' . $voto_comite_no . '<br> Sin voto: ' . $voto_comite_sin_voto ?></strong></45>
                      <h5 class="" style="color: red;"> Puntos obtenidos por parte del cómite: <strong><?php echo $suma_boton_precali; ?></strong></h5>
                  </div>
                <?php } ?>
                <!--  -->

                <div align="right">
                  <div>
                    <?php
                    // 0=inactivo, 1=sin archivo, 2=con archivo, 3=exonerado 
                    $boton_precali = '';
                    $color_boton_precali = 'btn-success';
                    $btn_new_credito = "";

                    if ($value['tb_proceso_fase_orden'] == '2') {
                      if ($voto_comite_si > $voto_comite_no) { // votos a favor mayor

                        if ($voto_comite_si > $voto_comite_sin_voto) { // votos a favor mayor
                          $pos1 = false; // inactivo
                          $pos2 = false; // sin archivo
                          foreach ($estados_items as $key => $v) {
                            if ($v == 0) {
                              $pos1 = true;
                            } else {
                              if ($v == 1) {
                                $pos2 = true;
                              }
                            }
                          }
                          if ($pos1) {
                            $boton_precali = 'disabled';
                            $color_boton_precali = 'btn-default';
                            $btn_new_credito = "disabled";
                          } else {
                            if ($pos2) {
                              $boton_precali = 'disabled';
                              $color_boton_precali = 'btn-default';
                              $btn_new_credito = "disabled";
                            } else {
                              $boton_precali = '';
                              $boton_banca = '';
                              $color_boton_precali = 'btn-success';
                              $color_boton_banca = 'btn-success';
                              $btn_new_credito = "";
                            }
                          }
                        } else {
                          $boton_precali = 'disabled';
                          $color_boton_precali = 'btn-default';
                        }
                      } else {
                        $boton_precali = 'disabled';
                        $color_boton_precali = 'btn-default';
                      }

                      if (intval($proceso['data']['tb_credito_id']) > 0 || $proceso['data']['tb_credito_id'] != null) {
                        $btn_new_credito = "disabled";
                      } else {
                        $btn_new_credito = "";
                        $boton_precali = 'disabled';
                        $color_boton_precali = 'btn-default';
                      }
                    } else {
                      // bloquear botón nuevo crédito
                      $pos1 = false; // inactivo
                      $pos2 = false; // sin archivo
                      foreach ($estados_items as $key => $v) {
                        if ($v == 0) {
                          $pos1 = true;
                        } else {
                          if ($v == 1) {
                            $pos2 = true;
                          }
                        }
                      }
                      if ($pos1) {
                        $boton_precali = 'disabled';
                        $color_boton_precali = 'btn-default';
                        //$btn_new_credito = "disabled";
                      } else {
                        if ($pos2) {
                          $boton_precali = 'disabled';
                          $color_boton_precali = 'btn-default';
                          //$btn_new_credito = "disabled";
                        } else {
                          $boton_precali = '';
                          $boton_banca = '';
                          $color_boton_precali = 'btn-success';
                          $color_boton_banca = 'btn-success';
                          //$btn_new_credito = "";
                        }
                      }
                      if (intval($proceso['data']['tb_credito_id']) > 0 || $proceso['data']['tb_credito_id'] != null) {
                        $btn_new_credito = "disabled";
                      }
                    }

                    ?>

                    <div class="d-flex">
                      <?php if (intval($proceso['data']['tb_cgarvtipo_id']) == 6) { ?>
                        <button type="button" <?php echo $btn_new_credito; ?> class="btn bg-purple <?php echo $new_cred; ?>" id="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" name="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" onClick="adenda_form('adenda', <?php echo intval($proceso['data']['tb_creditomatriz_id']); ?>, <?php echo $proceso_id; ?>)" /><strong><i class="fa fa-plus"></i> CREAR ADENDA</strong></button>
                      <?php } else { ?>
                        <button type="button" <?php echo $btn_new_credito; ?> class="btn bg-purple <?php echo $new_cred; ?>" id="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" name="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" onClick="creditogarveh_form('I', 0, <?php echo $proceso_id; ?>)" /><strong><i class="fa fa-plus"></i> CREAR CRÉDITO</strong></button>
                      <?php } ?>
                    </div>

                    <?php if ($orden_fase == 1) { ?>
                      <button type="button" class="btn btn-primary" id="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" name="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" onclick="verHistorialPagos(<?php echo $cliente_id; ?>)" /><strong><i class="fa fa-dolar"></i> HISTORIAL DE CLIENTE</strong></button>
                      <button type="button" <?php echo $boton_precali ?> class="btn <?php echo $color_boton_precali ?>" id="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" name="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" onclick="siguienteFase(<?php echo $proceso_id; ?>, <?php echo $id_fase; ?>, <?php echo $orden_fase; ?>)" /><strong><i class="fa fa-arrow-right"></i> GUARDAR Y SIGUIENTE</strong></button>
                    <?php } elseif ($orden_fase == 2) { ?>
                      <button type="button" <?php echo $boton_precali ?> class="btn <?php echo $color_boton_precali ?>" id="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" name="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" onclick="siguienteFase(<?php echo $proceso_id; ?>, <?php echo $id_fase; ?>, <?php echo $orden_fase; ?>)" /><strong><i class="fa fa-arrow-right"></i> SIGUIENTE</strong></button>
                    <?php } else { ?>
                      <button type="button" <?php echo $boton_precali ?> class="btn <?php echo $color_boton_precali ?>" id="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" name="fase_<?php echo $id_fase; ?>_orden_<?php echo $orden_fase; ?>" onclick="siguienteFase(<?php echo $proceso_id; ?>, <?php echo $id_fase; ?>, <?php echo $orden_fase; ?>)" /><strong><i class="fa fa-arrow-right"></i> SIGUIENTE</strong></button>
                    <?php } ?>

                  </div>
                </div>

              </div>
            </div>

            <!-- Seccion COMENTARIO -->
            <div class="row <?php echo $vision_comentario; ?>" style="margin-bottom: 15px; padding-left: 15px; padding-right: 15px;">
              <div class="col-md-12">
                <div class="form-group">
                  <!-- <label>Comentario</label> -->
                  <textarea class="form-control" rows="3" name="txt_comentario<?php echo $value['tb_proceso_fase_id']; ?>" id="txt_comentario<?php echo $value['tb_proceso_fase_id']; ?>" placeholder="Ingrese comentario ..." style="resize: none;"></textarea>
                </div>
              </div>

              <div align="right" class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-warning" name="btn_comentario<?php echo $value['tb_proceso_fase_id']; ?>" id="btn_comentario<?php echo $value['tb_proceso_fase_id']; ?>" onclick="registrarComentario('<?php echo $usuario_id; ?>','<?php echo $value['tb_proceso_fase_id']; ?>')"><strong>COMENTAR</strong></button>
                  <button type="button" class="btn btn-primary" onclick="upload_form('I', <?php echo $usuario_id; ?>, <?php echo $value['tb_proceso_fase_id']; ?>);"><strong><i class="fa fa-upload"></i> SUBIR</strong></button>
                  <button type="button" class="btn btn-info" title="Usar en caso de subir imagen" onclick="verComentariosFase(<?php echo $value['tb_proceso_fase_id']; ?>);"><strong><i class="fa fa-refresh"></i> ACTUALIZAR</strong></button>
                </div>
              </div>

              <div class="col-md-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title"><strong>Comentarios</strong></h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">

                    <div id="div_comentarios<?php echo $value['tb_proceso_fase_id']; ?>" class="col-md-12">
                      <?php require_once('proceso_vista_comentario.php'); ?>
                    </div>

                  </div>
                </div>
              </div>


            </div>
            <!--  -->

        <?php }
        } ?>

      </div>

      <div id="div_modal_upload_form">
        <!-- INCLUIMOS EL MODAL PARA SUBIDA DE IMÁGENES EN COMENTARIO -->
      </div>
      <div id="div_modal_carousel">
        <!-- INCLUIMOS EL MODAL PARA VISUALIZAR LAS IMÁGENES DE COMENTARIO -->
      </div>
      <div id="div_exonerar_proceso_fase_item">
        <!-- INCLUIMOS EL MODAL PARA EXONERARA ALGUNA SUBIDA DE ARCHIVO DENTRO DEL PROCESO -->
      </div>
      <div id="div_proceso_fase_item_file">
        <!-- INCLUIMOS EL MODAL PARA SUBIDA DE ARCHIVO DENTRO DEL PROCESO -->
      </div>
      <div id="div_proceso_fase_item_file_ver">
        <!-- INCLUIMOS EL MODAL PARA VER LA SUBIDA DE ARCHIVO DENTRO DEL PROCESO -->
      </div>
      <div id="div_historial_doc">
        <!-- INCLUIMOS EL MODAL PARA HISTORIAL SUBIDA DE ARCHIVO DENTRO DEL PROCESO -->
      </div>

      <div id="div_modal_bancarizacion">
        <!-- INCLUIMOS EL MODAL PARA ESCOGER LA BANCARIZACION DEL PROCESO -->
      </div>
      <div id="div_modal_generar_cheque">
        <!-- INCLUIMOS EL MODAL PARA GENERAR CHEQUES -->
      </div>
      <div id="div_modal_eliminar_cheque"></div>
      <div id="div_modal_generar_simulador">
        <!-- INCLUIMOS EL MODAL PARA GENERAR SIMULACION DE CUOTAS -->
      </div>
      <div id="div_modal_hoja_precalificacion">
        <!-- INCLUIMOS EL MODAL PARA GENERAR SIMULACION DE CUOTAS -->
      </div>
      <div id="div_modal_ingresos_egresos">
        <!-- INCLUIMOS EL MODAL PARA GENERAR SIMULACION DE CUOTAS -->
      </div>
      <div id="div_modal_creditogarveh_form">
        <!-- INCLUIMOS EL MODAL PARA GENERAR NUEVO CRÉDITO -->
      </div>
      <div id="div_modal_adenda_form">
        <!-- INCLUIMOS EL MODAL PARA GENERAR NUEVO CRÉDITO -->
      </div>
      <div id="div_modal_vehiculomarca_form">
        <!-- INCLUIMOS EL MODAL PARA GENERAR NUEVA MARCA DE AUTOS -->
      </div>
      <div id="div_modal_vehiculomodelo_form">
        <!-- INCLUIMOS EL MODAL PARA GENERAR NUEVO MODELO DE AUTOS -->
      </div>
      <div id="div_modal_vehiculoclase_form"></div>
      <div id="div_modal_vehiculotipo_form"></div>
      <div id="div_modal_cuentadeposito_form"></div>
      <div id="div_modal_representante_form"></div>
      <div id="div_modal_gps_simple"></div>

      <div id="div_modal_cliente_form">
        <!-- INCLUIMOS EL MODAL PARA REGISTRAR CLIENTE DENTRO DEL PROCESO -->
      </div>
      <div id="div_modal_transferente_form"></div>
      <div id="div_modal_creditogarveh_inicial">

      </div>
      <div id="div_modal_subida_cheque">
        <!-- INCLUIMOS EL MODAL PARA SUBIDA DE CHEQUES -->
      </div>
      <div id="div_credito_desembolso_form">
        <!-- INCLUIMOS EL MODAL PARA DESEMBOLSO -->
      </div>
      <div id="div_modal_comentario_precalificacion">
        <!-- INCLUIMOS EL MODAL PARA VISUALIZACION DE ESTADO DE CUENTA -->
      </div>
      <div id="div_modal_anular_proceso">
        <!-- INCLUIMOS EL MODAL PARA VISUALIZACION DE ESTADO DE CUENTA -->
      </div>
      <div id="div_titulo_form"></div>
      <div id="div_modal_pago_cuotas_cliente"></div>
      <div id="div_modal_ingresos_egresos_antiguos"></div>
      <?php //require_once(VISTA_URL . 'templates/modal_mensaje.php'); 
      ?>
    </div>
  </section>

</div>

<style>
  #new-process {
    position: relative;
    align-items: center;
  }

  #new-process-body {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  #new-process .box {
    width: 50% !important;
  }

  .my-fixed-item {
    right: 15px;
    position: fixed;
    text-align: center;
    word-wrap: break-word;
    background-color: rgb(223, 203, 54, .4);
    padding-left: 8px;
    padding-right: 8px;
    padding-top: 5px;
    padding-bottom: 5px;
    z-index: 10;
  }
</style>