<?php
	session_name("ipdnsac");
  session_start();

	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();
  require_once('../precliente/Precliente.class.php');
  $oPrecliente = new Precliente();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../segurogps/Seguro_Gps.class.php');
  $oSeguro_Gps = new Seguro_Gps();

  require_once ("../funciones/funciones.php");
	require_once ("../funciones/fechas.php");

  $accion = $_POST['accion'];
  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);
  $proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);
  $precalificacion_id = 0;

  $action = 'registrar_precalificacion';

  $proceso = $oProceso->mostrarUno($proceso_id);
  $colaborador='';
  $tipogarveh=0;

  // Cliente
  $modalidad 		= "";
  $tipo_cliente = "";
  $nacionalidad = "";
  $edad 			  = "";
  $razon 			  = "";
  $ruc 			    = "";
  $cliente 		  = "";
  $dni 			    = "";
  // Vehiculo
  $marca 				      = "";
  $vehiculomarca_id   = ""; // Para el uso del Select
  $modelo 			      = "";
  $vehiculomodelo_id   = ""; // Para el uso del Select
  $proveedor_id 		  = "";
  $proveedor 			    = "";
  $anio 				      = "";
  $licencia 			    = "";
  $tipo_licencia 		  = "";
  $vigencia_licencia  = "";
  $papeleta 			    = "";
  $monto 				      = "";
  $cuota 				      = "";
  $tasa 				      = "";
  $plazo 				      = "";
  $km 				        = "";
  $placa 				      = "";
  $precio_propuesto 	= "";
  $verificado 		    = "";
  $metodo_verificado 	= "";
  $tipo_operacion 	  = "";
  $comentario 		    = "";
  $vigencia_str 		  = "";
  $vigencia_gps 		  = "";
  $inicial_moneda 	  = "";
  $inicial_porcentaje = "";
  $inicial_monto 		  = "";
  $fec_inicial        = "";
  $inicial_est        = "";
  $finalidad 			    = "";
  $moneda 			      = "";
  $uso 				        = "";
  // Datos Cliente
  $ocupacion 			    = "";
  $grado_instruccion  = "";
  $carga_familiar 	  = "";
  $carga_familiar_e 	= "";
  $descuento	 		    = "";
  $estado_civil 		  = "";
  $gastos 			      = "";
  $direccion 			    = "";
  $vivienda 			    = "";
  $tipo_resi			    = "";
  $tipo_describir 	  = "";
  $fec_inspeccion     = "";
  $uso_vehiculo 	    = "";
  $zona_vehiculo 	    = "";
  $semaforizacion 	  = "";
  $score 	            = "";
  $cargo_antiguedad 	= "";
  $ingreso_forma      = "";
  $casa_antiguedad 	  = "";
  $bandera = '';

  if($proceso['estado']==1){
    $usuario = $oUsuario->mostrarUno($proceso['data']['tb_usuario_id']);
    $client = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
    $preclient = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);
    $tipogarveh = intval($proceso['data']['tb_cgarvtipo_id']);
    if($usuario['estado']==1){
      $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
    }
    if($client['estado']==1){
      $cliente = $client['data']['tb_cliente_nom'];
      $dni = $client['data']['tb_cliente_doc'];
    }else{
      if($preclient['estado']==1){
        $cliente = $preclient['data']['tb_precliente_nombres'];
        $dni = $preclient['data']['tb_precliente_numdoc'];
      }
    }

    // Datos traídos del fomulario de simulación de cuotas
    $monto 				      = $proceso['data']['tb_proceso_monto'] ? floatval($proceso['data']['tb_proceso_monto']) : NULL;
    $tasa 				      = $proceso['data']['tb_proceso_tasa'] ? floatval($proceso['data']['tb_proceso_tasa']) : NULL;
    $plazo 				      = $proceso['data']['tb_proceso_plazo'] ? intval($proceso['data']['tb_proceso_plazo']) : NULL;
    $precio_propuesto 	= $proceso['data']['tb_proceso_prestamo'] ? floatval($proceso['data']['tb_proceso_prestamo']) : floatval($proceso['data']['tb_proceso_monto']) - (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100));
    $tipo_operacion 	  = $proceso['data']['tb_cgarvtipo_id'] ? $proceso['data']['tb_cgarvtipo_id'] : NULL; // 1=pre consti, 2=consti s/custodia, 3=consti c/custodia, 4=compra s/custodia, 5=compra c/custodia, 6=adenda, 7=ipdn venta
    $inicial_porcentaje = $proceso['data']['tb_proceso_inicial_porcen'] ? floatval($proceso['data']['tb_proceso_inicial_porcen']) : NULL;
    $inicial_monto 		  = $proceso['data']['tb_proceso_inicial'] ? floatval($proceso['data']['tb_proceso_inicial']) : (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100));
    $moneda 			      = $proceso['data']['tb_proceso_moneda']; // sol, dolar
    $cambio 			      = $proceso['data']['tb_proceso_tip_cambio'];

    if($tasa == NULL || $plazo == NULL || $cambio == NULL || $moneda == NULL)
      $bandera = 'no_procede';

    // calcular valor de cuota
      $mon = '';
      if($moneda=='sol' || $moneda=='dolar_s'){
        $mon = 'S/ ';
      }elseif($moneda=='dolar' || $moneda=='sol_d'){
        $mon = '$ ';
      }
      $valor = floatval($proceso['data']['tb_proceso_monto']) - (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100));

      // precio de gps y seguro
      $seguro_gps_meses = $plazo;
      $valor_seguro = 0;
      $valor_gps = 0;

      $result = $oSeguro_Gps->obtener_valor_adicional(1, $seguro_gps_meses); // obtenemos el valor del seguro, el porcentaje para aplicar a las cuotas
        if($result['estado'] == 1){
          $valor_seguro = $result['data']['tb_seguro_gps_porcentaje'];
        }
      $result = NULL;

      $result = $oSeguro_Gps->obtener_valor_adicional(2, $seguro_gps_meses); // obtenemos el valor del GPS
        if($result['estado'] == 1){
          $valor_gps = $result['data']['tb_seguro_gps_precio'];
        }
      $result = NULL;
      //

      for ($i=0; $i < $plazo; $i++) { 

        $seguro_mensual = 0.00;
        $gps_mensual = 0.00;

        if($i==0){
          $capital = $valor;
        }

        $seguro_mensual = $valor_seguro;
        $tasa = $tasa + $seguro_mensual;

        if($moneda=='sol' || $moneda=='dolar_s'){
          $cuota_pagar = 0.00;
          if($moneda=='dolar_s'){
            $cuota_pagar = formato_moneda((($tasa/100)*pow(1+($tasa/100),$plazo))*$valor/((pow(1+($tasa/100),$plazo))-1)*$cambio);
          }else{
            $cuota_pagar = formato_moneda((($tasa/100)*pow(1+($tasa/100),$plazo))*$valor/((pow(1+($tasa/100),$plazo))-1));
          }
        }elseif($moneda=='dolar' || $moneda=='sol_d'){
          $cuota_pagar = 0.00;
          if($moneda=='sol_d'){
            $valor = $valor/$cambio;
          }
          $cuota_pagar = formato_moneda((($tasa/100)*pow(1+($tasa/100),$plazo))*$valor/((pow(1+($tasa/100),$plazo))-1));
        }

        if($valor_gps > 0){
          if($moneda=='sol' || $moneda=='dolar_s'){
            $gps_mensual = formato_moneda(($valor_gps / $plazo)*$cambio);
          }elseif($moneda=='dolar' || $moneda=='sol_d'){
            $gps_mensual = formato_moneda($valor_gps / $plazo);
          }
          $cuota_pagar_con_gps = formato_moneda($cuota_pagar + $gps_mensual);
        }

        break;

      }
    //
  }
  
  $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

  if($precalificacion['estado']==1){

    $precalificacion_id = $precalificacion['data']['tb_precalificacion_id'];
    $action = 'actualizar_precalificacion';
    $accion = 'edit';

    // Cliente
    $modalidad 		      = $precalificacion['data']['tb_precalificacion_modalidad']; // presencial, virtual, exonerada
    $tipo_cliente       = $precalificacion['data']['tb_precalificacion_tipo_cliente']; // juridica, natural
    $nacionalidad       = $precalificacion['data']['tb_precalificacion_nacionalidad'];
    $edad 			        = $precalificacion['data']['tb_precalificacion_edad'] ? intval($precalificacion['data']['tb_precalificacion_edad']) : NULL;
    $razon 			        = $precalificacion['data']['tb_precalificacion_razon_social'];
    $ruc 			          = $precalificacion['data']['tb_precalificacion_ruc'] ? $precalificacion['data']['tb_precalificacion_ruc'] : NULL;
    $cliente 		        = $precalificacion['data']['tb_precalificacion_cliente'];
    $dni 			          = $precalificacion['data']['tb_precalificacion_dni'] ? intval($precalificacion['data']['tb_precalificacion_dni']) : NULL;
    // Vehiculo
    $marca 				      = $precalificacion['data']['tb_precalificacion_marca'] ? intval($precalificacion['data']['tb_precalificacion_marca']) : NULL;
    $vehiculomarca_id 	= $precalificacion['data']['tb_precalificacion_marca'] ? intval($precalificacion['data']['tb_precalificacion_marca']) : NULL; // Para el uso del Select
    $modelo 			      = $precalificacion['data']['tb_precalificacion_modelo'] ? intval($precalificacion['data']['tb_precalificacion_modelo']) : NULL;
    $vehiculomodelo_id  = $precalificacion['data']['tb_precalificacion_modelo'] ? intval($precalificacion['data']['tb_precalificacion_modelo']) : NULL; // Para el uso del Select
    $proveedor_id 		  = $precalificacion['data']['tb_precalificacion_proveedor_id'] ? intval($precalificacion['data']['tb_precalificacion_proveedor_id']) : NULL;
    $provee = $oTransferente->mostrarUno($proveedor_id);
    if($provee['estado']==1){
      $proveedor        = $provee['data']['tb_transferente_nom'];
    }
    $anio 				      = $precalificacion['data']['tb_precalificacion_anio'];
    $licencia 			    = $precalificacion['data']['tb_precalificacion_licencia']; // si o no
    $tipo_licencia 		  = $precalificacion['data']['tb_precalificacion_tipo_licencia'];
    $vigencia_licencia 	= $precalificacion['data']['tb_precalificacion_licencia_vigencia'] ? mostrar_fecha($precalificacion['data']['tb_precalificacion_licencia_vigencia']) : NULL;
    $papeleta 			    = $precalificacion['data']['tb_precalificacion_papeleta']; // si o no
    //$monto 				      = $precalificacion['data']['tb_precalificacion_monto'] ? floatval($precalificacion['data']['tb_precalificacion_monto']) : ($proceso['data']['tb_proceso_monto'] ? floatval($proceso['data']['tb_proceso_monto']) : NULL);
    $cuota 				      = $precalificacion['data']['tb_precalificacion_cuota'] ? floatval($precalificacion['data']['tb_precalificacion_cuota']) : NULL;
    //$tasa 				      = $precalificacion['data']['tb_precalificacion_tasa'] ? floatval($precalificacion['data']['tb_precalificacion_tasa']) : ($proceso['data']['tb_proceso_tasa'] ? floatval($proceso['data']['tb_proceso_tasa']) : NULL);
    //$plazo 				      = $precalificacion['data']['tb_precalificacion_plazo'] ? intval($precalificacion['data']['tb_precalificacion_plazo']) : ($proceso['data']['tb_proceso_plazo'] ? intval($proceso['data']['tb_proceso_plazo']) : NULL);
    $km 				        = $precalificacion['data']['tb_precalificacion_km'] ? floatval($precalificacion['data']['tb_precalificacion_km']) : NULL;
    $placa 				      = $precalificacion['data']['tb_precalificacion_placa'];
    //$precio_propuesto 	= $precalificacion['data']['tb_precalificacion_precio_propuesto'] ? floatval($precalificacion['data']['tb_precalificacion_precio_propuesto']) : ($proceso['data']['tb_proceso_prestamo'] ? floatval($proceso['data']['tb_proceso_prestamo']) : NULL);
    if($precalificacion['data']['tb_precalificacion_precio_propuesto'] == null || $precalificacion['data']['tb_precalificacion_precio_propuesto'] == 0.00){
      if($proceso['data']['tb_proceso_prestamo'] == null){
        $precio_propuesto  = floatval($proceso['data']['tb_proceso_monto']) - (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100));
      }else{
        $precio_propuesto 	= floatval($proceso['data']['tb_proceso_prestamo']);
      }
    }else{
      $precio_propuesto 		= floatval($precalificacion['data']['tb_precalificacion_precio_propuesto']);
    }
    $verificado 		    = $precalificacion['data']['tb_precalificacion_verificado']; // si o no
    $metodo_verificado 	= $precalificacion['data']['tb_precalificacion_verificado_metodo'];
    $tipo_operacion 	  = $precalificacion['data']['tb_precalificacion_tipo_operacion'] ? $precalificacion['data']['tb_precalificacion_tipo_operacion'] : ($proceso['data']['tb_cgarvtipo_id'] ? intval($proceso['data']['tb_cgarvtipo_id']) : NULL); // 1=pre consti, 2=consti s/custodia, 3=consti c/custodia, 4=compra s/custodia, 5=compra c/custodia, 6=adenda, 7=ipdn venta
    $comentario 		    = $precalificacion['data']['tb_precalificacion_comentario'];
    $vigencia_str 		  = $precalificacion['data']['tb_precalificacion_vigencia_str'];
    $vigencia_gps 		  = $precalificacion['data']['tb_precalificacion_vigencia_gps'];
    $inicial_moneda 	  = $precalificacion['data']['tb_precalificacion_inicial_moneda']; // sol, dolar
    $inicial_porcentaje = $precalificacion['data']['tb_precalificacion_inicial_porcen'] ? floatval($precalificacion['data']['tb_precalificacion_inicial_porcen']) : ($proceso['data']['tb_proceso_inicial_porcen'] ? floatval($proceso['data']['tb_proceso_inicial_porcen']) : NULL);
    //$inicial_monto 		  = $precalificacion['data']['tb_precalificacion_inicial_monto'] ? floatval($precalificacion['data']['tb_precalificacion_inicial_monto']) : ($proceso['data']['tb_proceso_inicial'] ? floatval($proceso['data']['tb_proceso_inicial']) : NULL);
    if($precalificacion['data']['tb_precalificacion_inicial_monto'] == null || $precalificacion['data']['tb_precalificacion_inicial_monto'] == 0.00){
      if($proceso['data']['tb_proceso_inicial'] == null){
        $inicial_monto  = (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100));
      }else{
        $inicial_monto 	= floatval($proceso['data']['tb_proceso_inicial']);
      }
    }else{
      $inicial_monto 		= floatval($precalificacion['data']['tb_precalificacion_inicial_monto']);
    }
    $fec_inicial        = $precalificacion['data']['tb_precalificacion_fec_inicial'] ? mostrar_fecha($precalificacion['data']['tb_precalificacion_fec_inicial']) : NULL;;
    $inicial_est        = $precalificacion['data']['tb_precalificacion_inicial_est'];
    $finalidad 			    = $precalificacion['data']['tb_precalificacion_finalidad'];
    //$moneda 			      = $precalificacion['data']['tb_precalificacion_moneda_credito'] ? $precalificacion['data']['tb_precalificacion_moneda_credito'] : $proceso['data']['tb_proceso_moneda']; // sol, dolar
    $uso 				        = $precalificacion['data']['tb_precalificacion_uso'];
    // Datos Cliente
    $ocupacion 			    = $precalificacion['data']['tb_precalificacion_ocupacion'];
    $grado_instruccion  = $precalificacion['data']['tb_precalificacion_grado'];
    $carga_familiar 	  = $precalificacion['data']['tb_precalificacion_carga_familiar'];
    $carga_familiar_e 	= $precalificacion['data']['tb_precalificacion_edad_escolar'];
    $descuento	 		    = $precalificacion['data']['tb_precalificacion_descuento'] ? floatval($precalificacion['data']['tb_precalificacion_descuento']) : NULL;
    $estado_civil 		  = $precalificacion['data']['tb_precalificacion_estado_civil'];
    $gastos 			      = $precalificacion['data']['tb_precalificacion_gastos'];
    $direccion 			    = $precalificacion['data']['tb_precalificacion_direccion'];
    $vivienda 			    = $precalificacion['data']['tb_precalificacion_vivienda']; // propia, alquilada, familiar
    $tipo_resi			    = $precalificacion['data']['tb_precalificacion_tipo_resid'] ? intval($precalificacion['data']['tb_precalificacion_tipo_resid']) : NULL; // 1=casa/dpto, 2=urbano/resid, 3=modulo/terreno, 4=rural. 5=habitacion, 6=otro
    $tipo_describir 	  = $precalificacion['data']['tb_precalificacion_resid_otro'];
    $fec_inspeccion     = $precalificacion['data']['tb_precalificacion_fec_inspeccion'] ? mostrar_fecha($precalificacion['data']['tb_precalificacion_fec_inspeccion']) : NULL;
    $tipo_describir 	  = $precalificacion['data']['tb_precalificacion_resid_otro'];
    $uso_vehiculo 	    = $precalificacion['data']['tb_precalificacion_uso_vehiculo'];
    $zona_vehiculo 	    = $precalificacion['data']['tb_precalificacion_zona_vehiculo'];
    $semaforizacion 	  = $precalificacion['data']['tb_precalificacion_semaforizacion'];
    $score 	            = $precalificacion['data']['tb_precalificacion_score'] ? intval($precalificacion['data']['tb_precalificacion_score']) : NULL;
    $cargo_antiguedad 	= $precalificacion['data']['tb_precalificacion_cargo_antig'];
    $ingreso_forma 	    = $precalificacion['data']['tb_precalificacion_ingreso_forma'];
    $casa_antiguedad 	  = $precalificacion['data']['tb_precalificacion_casa_antig'];
    
  }
  
?>
<?php if($bandera == 'no_procede'):?>
  <div class="modal fade in" tabindex="-1" role="dialog" id="modal_hoja_precalificacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><strong>IMPORTANTE</strong></h4>
          <div class="modal-body">
            <h4 class="modal-title"><strong>Debes llenar todos los datos de la simulación antes de llenar los datos de esta HOJA PRE-CALIFICACION</strong></h4>
            
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Moneda: <?php echo $moneda;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Tasa: <?php echo $tasa;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Plazo: <?php echo $plazo;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Tipo de Cambio: <?php echo $cambio;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Monto Préstamo: <?php echo $monto;?></span>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif;?>

<?php if($bandera == ''):?>
  <div class="modal fade in" tabindex="-1" role="dialog" id="modal_hoja_precalificacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><strong>HOJA DE PRE CALIFICACIÓN <?php echo 'PROCESO ID: '.$proceso_id;?></strong></h4>
        </div>

        <form id="form_precalificacion" method="post">
          <input type="hidden" name="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_preca_precalificacion_id" id="hdd_preca_precalificacion_id" value="<?php echo $precalificacion_id; ?>">
          <input type="hidden" name="hdd_preca_proceso_id" id="hdd_preca_proceso_id" value="<?php echo $proceso_id; ?>">
          <input type="hidden" name="hdd_preca_usuario_id" id="hdd_preca_usuario_id" value="<?php echo $usuario_id; ?>">
          <input type="hidden" name="hdd_preca_proceso_fase_item_id" id="hdd_preca_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id; ?>">

          <div class="modal-body">

            <div class="row">

              <div class="col-md-7">
                <div class="form-group">
                  <p><strong>Colaborador: </strong><span><i><?php echo $colaborador; ?></i></span></p>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Modalidad: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;">
                      <input type="radio" name="cli_modalidad" value="presencial" class="flat-green" <?php if($modalidad=='presencial') echo 'checked' ?>> Presencial 
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="cli_modalidad" value="virtual" class="flat-green" <?php if($modalidad=='virtual') echo 'checked' ?>> Virtual 
                    </label>
                    <label class="radio-inline <?php if($tipogarveh==1 || $tipogarveh==2 || $tipogarveh==3){ echo 'hidden'; } // ?>">
                      <input type="radio" name="cli_modalidad" value="exonerada" class="flat-green" <?php if($modalidad=='exonerada') echo 'checked' ?>> Exonerada
                    </label>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <section id="payment">
                  <h5 class="page-header"><strong>Cliente</strong></h5>

                  <div class="row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Tipo de cliente: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_tipo_cliente" value="natural" class="flat-green" <?php if($tipo_cliente=='natural') echo 'checked' ?>> P. Natural
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_cliente" value="juridica" class="flat-green" <?php if($tipo_cliente=='juridica') echo 'checked' ?>> P. Jurídica
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Nacionalidad: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm color" name="cli_nacionalidad" id="cli_nacionalidad" placeholder="Ingrese Nacionalidad" value="<?php echo $nacionalidad ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Edad: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_edad" id="cli_edad" onkeypress="return solo_numero(event)" placeholder="Ingrese Edad" value="<?php echo $edad ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-8">
                      <div class="form-group">
                        <label>Razón Social: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_razon" id="cli_razon" placeholder="Ingrese Razón Social" value="<?php echo $razon ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>RUC: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_ruc" id="cli_ruc" onkeypress="return solo_numero(event)" placeholder="Ingrese RUC" value="<?php echo $ruc ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-8">
                      <div class="form-group">
                        <label>Cliente: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_cliente" id="cli_cliente" placeholder="Ingrese Cliente" value="<?php echo $cliente ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_dni" id="cli_dni" onkeypress="return solo_numero(event)" placeholder="Autocompletado" value="<?php echo $dni ?>" readonly>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </section>
              </div>

              
              <div class="col-md-12">
                <section id="payment">
                  <h5 class="page-header"><strong>Vehículo</strong></h5>

                  <div class="row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Marca: </label>
                        <div class="input-group">
                          <select name="cli_marca" id="cli_marca" class="form-control input-sm mayus">
                            <?php
                              require_once '../vehiculomarca/vehiculomarca_select.php';
                            ?>
                          </select>
                          <span class="input-group-btn">
                            <button class="btn btn-primary btn-sm" type="button" onclick="vehiculomarca_form('I',0)" title="AGREGAR MARCA">
                                <span class="fa fa-plus icon"></span>
                            </button>
                          </span>
                        </div>
                        <!-- <input type="text" class="form-control input-sm" name="cli_marca" id="cli_marca" value="<?php echo $marca ?>"> -->
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Modelo: </label>
                        <div class="input-group">
                          <select name="cli_modelo" id="cli_modelo" class="form-control input-sm mayus">
                            <?php
                              require_once '../vehiculomodelo/vehiculomodelo_select.php';
                            ?>
                          </select>
                          <span class="input-group-btn">
                            <button class="btn btn-primary btn-sm" type="button" onclick="vehiculomodelo_form('I', 0)" title="AGREGAR MODELO">
                                <span class="fa fa-plus icon"></span>
                            </button>
                          </span>
                        </div>
                        <!-- <input type="text" class="form-control input-sm" name="cli_modelo" id="cli_modelo"  value="<?php echo $modelo ?>"> -->
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Año: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_anio" id="cli_anio" onkeypress="return solo_numero(event)" value="<?php echo $anio ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Proveedor: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_proveedor" id="cli_proveedor" value="<?php echo $proveedor ?>">
                          <input type="hidden" class="form-control input-sm" name="cli_proveedor_id" id="cli_proveedor_id" value="<?php echo $proveedor_id ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Licencia MTC: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_licencia" value="s" class="flat-green" <?php if($licencia=='s') echo 'checked' ?>> SI
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_licencia" value="n" class="flat-green" <?php if($licencia=='n') echo 'checked' ?>> NO
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Tipo Licencia: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_tipo_licencia" id="cli_tipo_licencia" placeholder="" value="<?php echo $tipo_licencia ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Vigencia Licencia: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_vigencia_licencia" id="cli_vigencia_licencia" placeholder="" value="<?php echo $vigencia_licencia ?>">
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Papeletas: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_papeleta" value="s" class="flat-green" <?php if($papeleta=='s') echo 'checked' ?>> SI
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_papeleta" value="n" class="flat-green" <?php if($papeleta=='n') echo 'checked' ?>> NO
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Valor de Vehículo: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_monto" id="cli_monto" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="0.00" value="<?php echo formato_numero($monto) ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Valor Cuota: </label>
                        <div class="input-group" style="width:100%">
                          <select name="cli_cuota" id="cli_cuota" class="form-control input-sm mayus">
                            <option value="<?php echo formato_moneda($cuota_pagar_con_gps); ?>" <?php if($cuota != null || $cuota > 0){ if($cuota == $cuota_pagar_con_gps){ echo 'selected';} } ?>>Con GPS: <?php echo $mon.' '.formato_moneda($cuota_pagar_con_gps); ?></option>
                            <option value="<?php echo formato_moneda($cuota_pagar); ?>" <?php if($cuota != null || $cuota > 0){ if($cuota == $cuota_pagar){ echo 'selected';} } ?>>Sin GPS: <?php echo $mon.' '.formato_moneda($cuota_pagar); ?></option>
                          </select>
                        </div>
                        <!-- <div class="">
                          <input type="text" class="form-control input-sm" name="cli_cuota" id="cli_cuota" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="0.00" value="<?php echo formato_numero($cuota) ?>">
                        </div> -->
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Tasa Propuesta: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_tasa" id="cli_tasa" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="0.00" value="<?php echo formato_numero($tasa) ?>">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Plazo: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_plazo" id="cli_plazo" onkeypress="return solo_numero(event)" value="<?php echo $plazo ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Kilometros: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_km" id="cli_km" onkeypress="return solo_decimal(event)" value="<?php echo $km ?>">
                          <span class="input-group-addon">Km</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Placa: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_placa" id="cli_placa" placeholder="" value="<?php echo $placa ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Valor de Préstamo: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_precio" id="cli_precio" placeholder="" value="<?php echo formato_moneda($precio_propuesto) ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Verificado: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_verificado" value="s" class="flat-green" <?php if($verificado=='s') echo 'checked' ?>> SI
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_verificado" value="n" class="flat-green" <?php if($verificado=='n') echo 'checked' ?>> NO
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Método Verificación: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_metodo_ver" id="cli_metodo_ver" placeholder="" value="<?php echo $metodo_verificado ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-8">
                      <div class="form-group">
                        <label>Tipo Operación: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_tipo_operacion" value="1" class="flat-green" <?php if($tipo_operacion==1) echo 'checked' ?>> Pre Constitución &nbsp;&nbsp;
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_operacion" value="2" class="flat-green" <?php if($tipo_operacion==2) echo 'checked' ?>> Constitución sin custodia &nbsp;&nbsp;
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_operacion" value="3" class="flat-green" <?php if($tipo_operacion==3) echo 'checked' ?>> Constitución con custodia &nbsp;&nbsp;
                          </label> 
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_operacion" value="4" class="flat-green" <?php if($tipo_operacion==4) echo 'checked' ?>> Compra Tercero S/C &nbsp;&nbsp;
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_operacion" value="5" class="flat-green" <?php if($tipo_operacion==5) echo 'checked' ?>> Compra Tercero C/C &nbsp;&nbsp;
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_operacion" value="6" class="flat-green" <?php if($tipo_operacion==6) echo 'checked' ?>> Adenda &nbsp;&nbsp;
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_operacion" value="7" class="flat-green" <?php if($tipo_operacion==7) echo 'checked' ?>> IPDN Venta &nbsp;&nbsp;
                          </label>
                          
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Comentario: </label>
                        <div class="">
                          <textarea class="form-control input-sm" rows="2" name="cli_comentario" id="cli_comentario" placeholder="Ingrese comentario ..." style="resize: none;"><?php echo $comentario; ?></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Vigencia de STR: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_vigencia_str" id="cli_vigencia_str" placeholder="" value="<?php echo $vigencia_str ?>">
                          <!-- <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span> -->
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Vigencia de GPS: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_vigencia_gps" id="cli_vigencia_gps" placeholder="" value="<?php echo $vigencia_gps ?>">
                          <!-- <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span> -->
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Moneda Inicial: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_inicial_moneda" value="dolar" class="flat-green" <?php if($inicial_moneda=='dolar') echo 'checked' ?>> Dólares
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_inicial_moneda" value="sol" class="flat-green" <?php if($inicial_moneda=='sol') echo 'checked' ?>> Soles
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Inicial Porcentaje: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_inicial_porcentaje" id="cli_inicial_porcentaje" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="0.00" value="<?php echo formato_numero($inicial_porcentaje) ?>">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Inicial Monto: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_inicial_monto" id="cli_inicial_monto" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="0.00" value="<?php echo formato_moneda($inicial_monto) ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Estado Inicial </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_inicial_est" value="deposito" class="flat-green" <?php if($inicial_est=='deposito') echo 'checked' ?>> Depositada
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_inicial_est" value="deposito_parcial" class="flat-green" <?php if($inicial_est=='deposito_parcial') echo 'checked' ?>> Depositada parcialmente
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_inicial_est" value="sin_deposito" class="flat-green" <?php if($inicial_est=='sin_deposito') echo 'checked' ?>> No depositada
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Fecha Inicial: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_inicial_fec" id="cli_inicial_fec" placeholder="" value="<?php echo $fec_inicial ?>">
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Crédito a colocarse en: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_moneda" value="dolar" class="flat-green" <?php if($moneda=='dolar') echo 'checked' ?>> Dólares
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_moneda" value="sol" class="flat-green" <?php if($moneda=='sol') echo 'checked' ?>> Soles
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6 <?php if($tipogarveh==1){ echo 'hidden'; } ?>">
                      <div class="form-group">
                        <label>Finalidad de Crédito: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_finalidad" id="cli_finalidad" placeholder="" value="<?php echo $finalidad ?>">
                        </div>
                      </div>
                    </div>

                    <div class="<?php if($tipogarveh==1){ echo 'col-md-12'; }else{ echo 'col-md-6'; } ?>">
                      <div class="form-group">
                        <label>Uso del Vehículo: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_uso" id="cli_uso" placeholder="" value="<?php echo $uso ?>">
                        </div>
                      </div>
                    </div>
              
                  </div>
                </section>
              </div>

              <div class="col-md-12">
                <section id="payment">
                  <h5 class="page-header"><strong>Datos de Cliente</strong></h5>

                  <div class="row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Ocupación: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_ocupacion" id="cli_ocupacion" value="<?php echo $ocupacion ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Antiguedad: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_cargo_antiguedad" id="cli_cargo_antiguedad"  value="<?php echo $cargo_antiguedad ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Forma de Ingreso: </label>
                        <div class="input-group" style="width: 100%;">
                          <select name="cli_ingreso_forma" id="cli_ingreso_forma" class="form-control input-sm" required title="Campo requerido">
                            <option <?php if($ingreso_forma == 'formal'){ echo "selected='selected'";} ?> value="formal">Formal</option>
                            <option <?php if($ingreso_forma == 'informal'){ echo "selected='selected'";} ?> value="informal">Informal</option>
                            <option <?php if($ingreso_forma == 'mixto'){ echo "selected='selected'";} ?> value="mixto">Mixto</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Grado de Intrucción: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_grado_instruccion" id="cli_grado_instruccion"  value="<?php echo $grado_instruccion ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Carga Familiar: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_carga_familiar" id="cli_carga_familiar" value="<?php echo $carga_familiar ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>En edad de estudio: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_carga_familiar_e" id="cli_carga_familiar_e" value="<?php echo $carga_familiar_e ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Dsctos Judiciales: </label>
                        <!-- <label class="radio-inline" style="padding-left: 0px;">
                            <input type="checkbox" name="chk_descuento" id="chk_descuento" onchange="descJudicial(this.value)" class="flat-green">
                          </label> -->
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_descuento" id="cli_descuento" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="0.00" value="<?php echo $descuento ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Estado Civil: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_estado_civil" id="cli_estado_civil" value="<?php echo $estado_civil ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Describir Gastos: </label>
                        <div class="">
                          <textarea class="form-control input-sm" rows="2" name="cli_gastos" id="cli_gastos" placeholder="Ingrese Gastos ..." style="resize: none;" required title="Campo requerido"><?php echo $gastos; ?></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Uso de Vehículo: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_uso_vehiculo" id="cli_uso_vehiculo" value="<?php echo $uso_vehiculo ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Zona de uso de Vehículo: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_zona_vehiculo" id="cli_zona_vehiculo" value="<?php echo $zona_vehiculo ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Semaforización: </label>
                        <div class="input-group" style="width: 100%;">
                          <select name="cli_semaforizacion" id="cli_semaforizacion" class="form-control input-sm" required title="Campo requerido">
                            <option <?php if($semaforizacion == 'rojo'){ echo "selected='selected'";} ?> value="rojo">Rojo</option>
                            <option <?php if($semaforizacion == 'amarillo'){ echo "selected='selected'";} ?> value="amarillo">Amarillo</option>
                            <option <?php if($semaforizacion == 'verde'){ echo "selected='selected'";} ?> value="verde">Verde</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Score: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_score" id="cli_score" value="<?php echo $score ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Dirección: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_direccion" id="cli_direccion" value="<?php echo $direccion ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Antiguedad Vivienda: </label>
                        <div class="">
                          <input type="text" class="form-control input-sm" name="cli_casa_antiguedad" id="cli_casa_antiguedad"  value="<?php echo $casa_antiguedad ?>" required title="Campo requerido">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Vivienda: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_vivienda" value="propia" class="flat-green" <?php if($vivienda=='propia') echo 'checked' ?> required title="Requerido"> Propia
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_vivienda" value="alquilada" class="flat-green" <?php if($vivienda=='alquilada') echo 'checked' ?>> Alquilada
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_vivienda" value="familiar" class="flat-green" <?php if($vivienda=='familiar') echo 'checked' ?>> Familiar
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-9">
                      <div class="form-group">
                        <label>Tipo Residencia: </label>
                        <div class="">
                          <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="cli_tipo_resi" value="1" class="flat-green" <?php if($tipo_resi==1) echo 'checked' ?> required title="Requerido"> Casa/Dpto
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_resi" value="2" class="flat-green" <?php if($tipo_resi==2) echo 'checked' ?>> Urbano/Resid
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_resi" value="3" class="flat-green" <?php if($tipo_resi==3) echo 'checked' ?>> Módulo/Terreno
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_resi" value="4" class="flat-green" <?php if($tipo_resi==4) echo 'checked' ?>> Rural
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_resi" value="5" class="flat-green" <?php if($tipo_resi==5) echo 'checked' ?>> Habitación
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="cli_tipo_resi" value="6" class="flat-green" <?php if($tipo_resi==6) echo 'checked' ?>> Otro
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Inspección Domiciliaria: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="cli_inspeccion_domi" id="cli_inspeccion_domi" placeholder="" value="<?php echo $fec_inspeccion ?>" required title="Campo requerido">
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Describir (Otro Tipo Residencia): </label>
                        <div class="">
                          <textarea class="form-control input-sm" rows="2" name="cli_tipo_describir" id="cli_tipo_describir" placeholder="" style="resize: none;"><?php echo $tipo_describir; ?></textarea>
                        </div>
                      </div>
                    </div>

                  </div>
                </section>
              </div>

            </div>

          </div>

          <div class="modal-footer">
            <?php if($accion === 'reg') { ?>
              <button type="submit" id="btn_guardar_precalificacion" class="btn btn-success" onclick="">Guardar</button>
            <?php }else{ ?>
              <button type="button" id="btn_exportar_precalificacion" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id; ?>, 'sin_firma')"><span class="fa fa-file-pdf-o"></span> Exportar</button>
              <button type="submit" id="btn_guardar_precalificacion" class="btn btn-success" onclick="">Actualizar</button>
            <?php } ?>
            <button type="button" id="btn_cerrar" class="btn btn-danger" onclick="" data-dismiss="modal">Cerrar</button>
          </div>

        </form>

      </div>
    </div>
  </div>
  <style>
    .color {
      background-color: #FFE7E6;
    }
    input:required {
      border: 1px solid #ccc !important;
    }
  </style>
  <script type="text/javascript" src="<?php echo 'vista/proceso/hoja_precalificacion_form.js?ver=123';?>"></script>
<?php endif;?>