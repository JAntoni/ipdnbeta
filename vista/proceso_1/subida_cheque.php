<?php
	session_name("ipdnsac");
  session_start();
	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();
  require_once('../empresa/Empresa.class.php');
  $oEmpresa = new Empresa();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();

  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");

  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);
  $proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);

  $proceso = $oProceso->mostrarUno($proceso_id);
  $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

  $banca = 0;
  $tipo_garantia = '';
  $tipo_garantia_id = 0;
  $cliente_doc = 0;
  $cliente_nom = '';
  $tipo_cambio = 0.000;

  if($proceso['estado']==1){
    $banca = intval($proceso['data']['tb_proceso_banca']);
    $tipo_garantia_id = intval($proceso['data']['tb_cgarvtipo_id']);
    $garvtipo = $oProceso->mostrarUnoGarvtipo($proceso['data']['tb_cgarvtipo_id']);
    $tipo_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);

    if($garvtipo['estado']==1){
      $tipo_garantia = $garvtipo['data']['cgarvtipo_nombre'];
    }
    $cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
    if($cliente['estado']==1){
      $cliente_doc = $cliente['data']['tb_cliente_doc'];
      $cliente_nom = $cliente['data']['tb_cliente_nom'];
    }
  }

  $cheque_c = $oProceso->mostrarCabeceraChequeXProceso($proceso_id);
  $cheques = $oProceso->mostrarChequeXProceso($proceso_id);
  $fecha = '';
  $detalle = '';
  $banco = 0;
  $moneda_cheque = '';
  $cheque_aprobado = 0;
  $usuario_aprobado = 0;
  $fecha_aprobado = '';
  $cheque_id = 0;
  $conector = '';
  if( $cheque_c['estado'] == 1){
    $cheque_id = intval($cheque_c['data']['tb_cheque_id']);
    $fecha = date("d/m/Y, h:i a", strtotime($cheque_c['data']['tb_cheque_fecreg']));
    $detalle = $cheque_c['data']['tb_cheque_det'];
    $banco = intval($cheque_c['data']['tb_cheque_banco']);
    $moneda_cheque = $cheque_c['data']['tb_cheque_moneda'];
    $cheque_aprobado = intval($cheque_c['data']['tb_cheque_aprobado']);
    $usuario_aprobado = intval($cheque_c['data']['tb_cheque_usu_aprob']);
    $fecha_aprobado = date("d/m/Y, h:i a", strtotime($cheque_c['data']['tb_cheque_fec_aprob']));
    $conector = $cheque_c['data']['tb_cheque_conector'];
  }else{
    $fecha = date("d/m/Y, h:i a");
  }
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_subida_cheque" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <input type="hidden" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Subida de Cheques</h4>
      </div>
      <div class="modal-body">

        <div class="row">

          <?php 
            if($tipo_garantia_id == 1){ // PRE CONSTITUCION

              $transferente_id = 0;
              $transferente = '';
              if($precalificacion['estado']==1){
                $transferente_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
                $transferente = $oTransferente->mostrarUno($transferente_id);
                if($transferente['estado']==1){
                  $transferente = $transferente['data']['tb_transferente_nom'];
                }
              } 

              $beneficiarios = $transferente;
              
            }elseif($tipo_garantia_id == 2 || $tipo_garantia_id == 3 || $tipo_garantia_id == 6){ // CONSTITUCIÓN SIN Y CON CUSTODIA O ADENDA

              // gerson (23-03-24)
              if($precalificacion['estado'] == 1){
                if($precalificacion['data']['tb_precalificacion_razon_social'] != '' || $precalificacion['data']['tb_precalificacion_razon_social'] != null){
                  $beneficiarios = $precalificacion['data']['tb_precalificacion_razon_social'];
                }else{
                  $beneficiarios = $cliente_nom;
                }
              }else{
                $beneficiarios = $cliente_nom;
              }
              //
              $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
              if($participantes['estado']==1){
                foreach ($participantes['data'] as $key => $p) {
                  $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'];
                }
              }

            }elseif($tipo_garantia_id == 4 || $tipo_garantia_id == 5) { // TERCERO SIN Y CON CUSTODIA

              $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
              $beneficiarios = '';
              if($participantes['estado']==1){
                foreach ($participantes['data'] as $key => $p) {
                  if($key == 0){
                    $beneficiarios .= $p['tb_participante_nom'];
                  }else{
                    $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'];
                  }
                }
              }

            }

            $i = 0;
            foreach ($cheques['data'] as $key => $value) { 

              $moneda = "";
              if($value['tb_cheque_moneda']=='sol'){
                $moneda = "S/ ";
              }else{
                $moneda = "US$ ";
              }

              $num_cheque = "";
              $sede = "";
              $uso = "";
              $fecha_subida = "";

              $num_cheque = $value['tb_chequedetalle_nro_cheque'];
              $sede = intval($value['tb_empresa_id']);
              $uso = intval($value['tb_chequedetalle_uso']);
              $fecha_subida = mostrar_fecha($value['tb_chequedetalle_fecsub']);


              // 1=bcp, bbva=2
              $img_cheque = "";
              if($banco==1){
                if($moneda_cheque=='sol'){
                  if($value['tb_chequedetalle_tipo']=='nego'){
                    $img_cheque = "public/images/cheques/bcp_sol.png";
                  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                    $img_cheque = "public/images/cheques/bcp_sol_ng.png";
                  }
                }elseif($moneda_cheque=='dolar'){
                  if($value['tb_chequedetalle_tipo']=='nego'){
                    $img_cheque = "public/images/cheques/bcp_dolar.png";
                  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                    $img_cheque = "public/images/cheques/bcp_dolar_ng.png";
                  }
                }
              }elseif($banco==2){
                if($moneda_cheque=='sol'){
                  if($value['tb_chequedetalle_tipo']=='nego'){
                    $img_cheque = "public/images/cheques/bbva_sol.png";
                  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                    $img_cheque = "public/images/cheques/bbva_sol_ng.png";
                  }
                }elseif($moneda_cheque=='dolar'){
                  if($value['tb_chequedetalle_tipo']=='nego'){
                    $img_cheque = "public/images/cheques/bbva_dolar.png";
                  }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                    $img_cheque = "public/images/cheques/bbva_dolar_ng.png";
                  }
                }
              }

          ?>

            <div class="col-md-12">
              <div class="col-md-7">

                <div class='col-md-7'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>N° Cheque</strong></span>
                    <input autocomplete="off" class="form-control input-sm" name="num_cheque<?php echo $i; ?>" id="num_cheque<?php echo $i; ?>" type="text" placeholder="Ingrese N° de Cheque" value="<?php echo $num_cheque; ?>" />
                  </div>
                  <br>
                </div>

                <div class='col-md-5'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>Fecha</strong></span>
                    <input readonly class="form-control input-sm" name="fecha_subida<?php echo $i; ?>" id="fecha_subida<?php echo $i; ?>" type="text" onkeypress="return solo_numero(event)" placeholder="Fecha de subida" value="<?php echo date("d-m-Y"); ?>" />
                    <!-- <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
                  </div>
                  <br>
                </div>

                <div class='col-md-6'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>Sede</strong></span>
                    <select name="cmb_empresa_id<?php echo $i; ?>" id="cmb_empresa_id<?php echo $i; ?>" class="form-control input-sm">
                      <?php 
                      $result = $oEmpresa->listar_Sucursales();
                      if($result['estado'] == 1) {
                        foreach ($result['data'] as $key => $empresa) {
                      ?>  
                        <option <?php if($empresa['tb_empresa_id']==$sede){ echo "selected='selected'"; } ?> value="<?php echo $empresa['tb_empresa_id']; ?>" style="font-weight: bold;"><?php echo $empresa['tb_empresa_nomcom']; ?></option>
                      <?php } }?>
                    </select>
                  </div>
                  <br>
                </div>

                <div class='col-md-6'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>Uso</strong></span>
                    <select name="cmb_uso<?php echo $i; ?>" id="cmb_uso<?php echo $i; ?>" class="form-control input-sm">
                      <option <?php if($uso==1){ echo "selected='selected'"; } ?> value="1" style="font-weight: bold;">Desembolso Total</option>
                      <option <?php if($uso==2){ echo "selected='selected'"; } ?> value="2" style="font-weight: bold;">Retención Total</option>
                    </select>
                  </div>
                  <br>
                </div>

                <div class='col-md-12'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>Beneficiario</strong></span>
                    <input readonly class="form-control input-sm" name="beneficiario<?php echo $i; ?>" id="beneficiario<?php echo $i; ?>" type="text" placeholder="Nombres y apellidos de participante" value="<?php echo $beneficiarios; ?>" />
                  </div>
                  <br>
                </div>

                <div class='col-md-6'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>Moneda</strong></span>
                    <input readonly style="text-align: right;" class="form-control input-sm" name="moneda<?php echo $i; ?>" id="moneda<?php echo $i; ?>" type="text" placeholder="Monto" value="<?php echo $moneda; ?>"/>
                  </div>
                  <br>
                </div>

                <div class='col-md-6'>	
                  <div class="input-group" >
                    <span class="input-group-addon"><strong>Monto</strong></span>
                    <input readonly style="text-align: right;" class="form-control input-sm" name="monto<?php echo $i; ?>" id="monto<?php echo $i; ?>" type="text" onkeypress="return solo_decimal(event)" placeholder="Monto" value="<?php echo number_format($value['tb_chequedetalle_monto'], 2); ?>"/>
                  </div>
                  <br>
                </div>

              </div>

              <div class="col-md-5" id="divChequeSubido">
                
                <div class='col-md-12'>	
                  <div class="">
                    <img class="" style="width: 100%;" src="<?php echo $img_cheque ?>" alt="Cheque">
                  </div>
                  <br>
                </div>      
                
                <div align="center" class='col-md-12'>	
                  <?php if($num_cheque=='' || $num_cheque==null){ ?>
                    <button type="button" id="btn_guardar_subida_cheque" class="btn btn-success btn-sm" onclick="guardarNumeroCheque(<?php echo $proceso_id; ?>, <?php echo $usuario_id; ?>, <?php echo $value['tb_chequedetalle_id']; ?>, <?php echo $proceso_fase_item_id; ?>, <?php echo $i; ?>)"><li class="fa fa-upload"></li> Subir Cheque</button>
                  <?php }else{ ?>
                    <button type="button" id="btn_guardar_subida_cheque" class="btn btn-primary btn-sm" onclick="guardarNumeroCheque(<?php echo $proceso_id; ?>, <?php echo $usuario_id; ?>, <?php echo $value['tb_chequedetalle_id']; ?>, <?php echo $proceso_fase_item_id; ?>, <?php echo $i; ?>)"><li class="fa fa-upload"></li> Actualizar Datos Cheque</button>
                  <?php } ?>
                  <br>
                </div>  

              </div>

            </div>
            <div class="col-md-12 page-header" style="padding-top: 10px;"></div>

          <?php $i++; } ?>

        </div>

        <!-- aceptacion de cheques -->
        <?php
        $aprobado_text = '';
        $aprobado_text_style = '';
        //$usuario_aprob = '';
        //$fecha_aprob = '';

        $usuario = $oUsuario->mostrarUno($usuario_aprobado);

        if($cheque_aprobado==0){
          $aprobado_text = 'Los cheques subidos aún no han sido revisados.';
          $aprobado_text_style = 'color: red;';

        }elseif($cheque_aprobado==1){
          if($usuario['estado'] == 1){
            $aprobado_text = 'Los cheques subidos han sido <strong>RECHAZADOS</strong> por '.$usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'].' con fecha '.$fecha_aprobado;
            $aprobado_text_style = 'color: red;';
          }else{
            $aprobado_text = 'No se encontraron datos del <strong>RECHAZO</strong> de los cheques.';
            $aprobado_text_style = 'color: red;';
          }
        }elseif($cheque_aprobado==2){
          if($usuario['estado'] == 1){
            $aprobado_text = 'Los cheques subidos han sido <strong>APROBADOS</strong> por '.$usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'].' con fecha '.$fecha_aprobado;
            $aprobado_text_style = 'color: green;';
          }else{
            $aprobado_text = 'No se encontraron datos de la <strong>APROBACIÓN</strong> de los cheques.';
            $aprobado_text_style = 'color: green;';
          }
        }
        ?>
        <div class="row">
          <div class="col-md-12">

            <?php if(intval($usuario_id) == 2 || intval($usuario_id) == 11 || intval($usuario_id) == 20 || intval($usuario_id) == 32 || intval($usuario_id) == 61 || intval($usuario_id) == 56){ // Solo mostrar al comité ?>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="h4">Aceptación de los Cheques: </label>
                  <div class="input-group date" style="padding-left: 20px;">
                    <label class="radio-inline" style="padding-left: 0px;">
                      <input type="radio" name="r_aprobado" value="1" class="flat-green" onclick="aprobarCheques(<?php echo $proceso_id; ?>, <?php echo $usuario_id; ?>, <?php echo $cheque_id; ?>, <?php echo $proceso_fase_item_id; ?>)" <?php if($cheque_aprobado==1) echo 'checked' ?>> <strong>NO</strong> 
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="r_aprobado" value="2" class="flat-green" onclick="aprobarCheques(<?php echo $proceso_id; ?>, <?php echo $usuario_id; ?>, <?php echo $cheque_id; ?>, <?php echo $proceso_fase_item_id; ?>)" <?php if($cheque_aprobado==2) echo 'checked' ?>> <strong>SI</strong> 
                    </label>
                  </div>
                </div>
              </div>

              <div align="center" class="col-md-12">
                  <h5 class="" style="<?php echo $aprobado_text_style; ?>"> <strong><?php echo $aprobado_text; ?></strong></h5>                            
              </div>

            <?php }else{ ?>
              <div align="center" class="col-md-12">
                  <h5 class="" style="<?php echo $aprobado_text_style; ?>"> <strong><?php echo $aprobado_text; ?></strong></h5>                            
              </div>
            <?php } ?>

          </div>

              
        </div>
        <!--  -->

      </div>
      <div class="modal-footer">
        <!-- <button type="button" id="btn_guardar_subida_cheque" class="btn btn-info" onclick="registrarBanca()">Guardar</button> -->
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script>
  /* $('input[type="radio"].flat-green').iCheck({
		radioClass: 'iradio_flat-green'
	}); */
</script>