<?php
	session_name("ipdnsac");
  session_start();

	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once ('../creditogarveh/Creditogarveh.class.php');
  $oCreditogarveh = new Creditogarveh();


  require_once ("../funciones/funciones.php");
	require_once ("../funciones/fechas.php");

  $accion = $_POST['accion'];
  $proceso_id = intval($_POST['proceso_id']);

  $proceso = $oProceso->mostrarUno($proceso_id);
  $tipo_cambio = 0.000;
  if($proceso['estado'] == 1){
    $tipo_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);
    $cliente_id = $proceso['data']['tb_cliente_id'];
    $fecha_ie = fecha_mysql($proceso['data']['tb_proceso_fecreg']);

    //BUSCAR PROCESOS ANTIGUOS
    $procesoCliente = $oProceso->mostrarProcesXCliente($cliente_id, $proceso_id, $fecha_ie);
    $bandera = $procesoCliente['estado'];
    if($procesoCliente['estado'] == 1){
      $procesosantiguos = count($procesoCliente['data']);
      $procesos = $procesoCliente['data'];
    }
    $procesoCliente = null;
  }

  
?>
<div class="modal fade in" tabindex="-1" role="dialog" id="modal_ingresos_egresos_antiguos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><strong>REPORTE DE INGRESOS Y GASTOS</strong></h4>
      </div>

      <input type="hidden" name="hdd_ie_proceso_id" id="hdd_ie_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" name="hdd_ie_tipo_cambio" id="hdd_ie_tipo_cambio" value="<?php echo $tipo_cambio; ?>">
      <input type="hidden" name="hdd_procesosantiguos" id="hdd_procesosantiguos" value="<?php echo $procesosantiguos; ?>">
      <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">
      <input type="hidden" name="hdd_fecha_ie" id="hdd_fecha_ie" value="<?php echo $fecha_ie; ?>">

      <div class="modal-body">

      <?php if($bandera == 1){?>
        <div class="row">
        <div class="col-md-12">
          <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;font-size:15px">
            <thead>
              <tr id="tabla_cabecera">
                  <th id="tabla_cabecera_fila">N° PROCESO</th>
                  <th id="tabla_cabecera_fila">N° CREDITO</th>
                  <th id="tabla_cabecera_fila">FECHA REGISTRO</th>
                  <th id="tabla_cabecera_fila">ASESOR</th>
                  <th id="tabla_cabecera_fila">CRÉDITO</th>
                  <th id="tabla_cabecera_fila">ESTADO</th>
                  <th  id="tabla_cabecera_fila" width="12%">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
            <?php if($procesosantiguos>0){ 
              foreach ($procesos as $key => $proceso) {

              if(intval($dt['tb_credito_id']) > 0){
                $cred = $oCreditogarveh->mostrarUno(intval($dt['tb_credito_id']));
                if(intval($cred['estado']) == 1){
                  if(intval($cred['data']['tb_credito_est']) == 1){
                    $estado = "PENDIENTE";
                    $estado_bg = '#D5D5D5';
                    $estado_color = '#000000';
                  }elseif(intval($cred['data']['tb_credito_est']) == 2){
                    $estado = "APROBADO";
                    $estado_bg = '#7D9BF9';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 3){
                    $estado = "VIGENTE";
                    $estado_bg = '#3061F7';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 4){
                    $estado = "PARALIZADO";
                    $estado_bg = '#F73030';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 5){
                    $estado = "REFINANCIADO C/AMORT";
                    $estado_bg = '#E5B416';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 6){
                    $estado = "REFINANCIADO S/AMORT";
                    $estado_bg = '#E5B416';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 7){
                    $estado = "LIQUIDADO";
                    $estado_bg = '#00BA49';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 8){
                    $estado = "RESUELTO";
                    $estado_bg = '#00BA49';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 9){
                    $estado = "TITULO";
                    $estado_bg = '#00BA49';
                    $estado_color = '#FFFFFF';
                  }elseif(intval($cred['data']['tb_credito_est']) == 10){
                    $estado = "DOCUMENTOS";
                    $estado_bg = '#00BA49';
                    $estado_color = '#FFFFFF';
                  }
                  if(intval($dt['tb_proceso_est']) == 4){
                    $estado = "ELIMINADO";
                    $estado_bg = '#F73030';
                    $estado_color = '#FFFFFF';
                  }
                }
                }else{
                  if(intval($dt['tb_proceso_est']) == 4){
                    $estado = "ELIMINADO";
                    $estado_bg = '#F73030';
                    $estado_color = '#FFFFFF';
                  }else{
                    $estado = "PENDIENTE";
                    $estado_bg = '#D5D5D5';
                    $estado_color = '#000000';
                  }
                }
              ?>
              <tr id="tabla_cabecera_fila">
                <td id="tabla_fila"><?php echo intval($proceso['tb_proceso_id']) ?></td>
                <?php if($proceso['tb_credito_id']!=null){ ?>
                <td id="tabla_fila"><?php echo str_pad($proceso['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td>
                <?php }else{ ?>
                <td id="tabla_fila"></td>
                <?php } ?>
                <td id="tabla_fila"><?php echo mostrar_fecha($proceso['tb_proceso_fecreg']) ?></td>
                <td id="tabla_fila"><?php echo $proceso['asesor'] ?></td>
                <td id="tabla_fila"><?php echo $proceso['tb_creditotipo_nom'].' | '.$proceso['cgarvtipo_nombre'] ?></td>
                <td id="tabla_fila" style="background-color: <?php echo $estado_bg ?>; color: <?php echo $estado_color ?>;"><strong><?php echo $estado ?></strong></td>
                <td id="tabla_fila">
                  <a class="btn btn-info btn-xs" title="Ver" onclick="verIE(<?php echo $proceso['tb_proceso_id'];?>)"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
            <?php 
              }
            ?>

            <?php } ?>
            </tbody>
          </table>
        </div>

          <?php if($tipo_cambio>0 || $tipo_cambio!=null || $tipo_cambio!=''){ ?>

            <div class="col-md-12">
              <section id="payment">
                <h5 class="page-header"><strong>Ingresos</strong></h5>

                <div class="row">

                  <div class="col-md-12" id="listaIngresoClienteHistorico">

                  </div>

                </div>
              </section>
            </div>

            
            <div class="col-md-12">
              <section id="payment">
                <h5 class="page-header"><strong>Gastos</strong></h5>

                <div class="row">
            
                  <div class="col-md-12" id="listaEgresoClienteHistorico">

                  </div>

                </div>
              </section>
            </div>

          <?php }else{ ?>
            <h3>Aún no ha fijado un tipo de cambio.</h3>
          <?php } ?>

        </div>
        <?php }else{?>
          <div class="row">
            <h3>No cuenta con procesos antiguos.</h3>
          </div>
          <?php }?>
      </div>

      <div class="modal-footer">
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>


    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/proceso/ingresos_egresos_antiguos_form.js?ver=1';?>"></script>
<!-- <script type="text/javascript" src="<?php //echo 'vista/proceso/reloj.js?ver=31082023'; ?>"></script> -->
