function detalleIngresosEgresos(proceso_id,type){
  
	$.ajax({
		type: "POST",
		url: VISTA_URL + "proceso/proceso_controller.php",
		async:true,
		dataType: "json",                      
		data: ({
		  action: 'detalle_ingresos_egresos_antiguo',
		  type: type,
		  proceso_id: proceso_id,
		  tipo_cambio: parseFloat($('#f').val()),
		}),
		beforeSend: function() {
		},
		success: function(data){
			let ingresoegreso = 0.00;

			if(type=='I'){
				$('#listaIngresoClienteHistorico').html(data.html);  
				let ingreso = parseFloat(data.total_ingresoegreso);
				$("#monto_ingreso").val(ingreso);
			}else{
				$('#listaEgresoClienteHistorico').html(data.html);  
				let egreso = parseFloat(data.total_ingresoegreso);
				$("#monto_egreso").val(egreso);
			}
			ingresoegreso = parseFloat($("#monto_ingreso").val()) - parseFloat($("#monto_egreso").val());
			if(ingresoegreso<0){
				$("#total_IE").addClass("text-red");
			}else{
				$("#total_IE").removeClass("text-red");
			}
			let totalIE = document.getElementById("total_IE");
			totalIE.innerHTML = "<strong>Ingresos - Gastos = S/ "+ingresoegreso.toFixed(2)+"</strong>";
		}
	});
  
}

function verIE(proceso_id){
    $('#listaIngresoClienteHistorico').html('');  
    $('#listaEgresoClienteHistorico').html('');  
    detalleIngresosEgresos(proceso_id,'I');
	detalleIngresosEgresos(proceso_id,'E');
}