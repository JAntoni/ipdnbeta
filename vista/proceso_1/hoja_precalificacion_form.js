
$(document).ready(function(){
	console.log('Cambios antonio al 10-10-2024')
	// color de campos
  	var inputs = document.querySelectorAll('#form_precalificacion input[type="text"], #form_precalificacion textarea, #form_precalificacion select');
	inputs.forEach((elemento) => {
		(elemento.value === "" || elemento.value === "0") ? $(elemento).addClass('color') : $(elemento).removeClass('color')
	})

	$('#form_precalificacion input[type="text"], #form_precalificacion textarea').focusin(function(){
		$(this).removeClass('color');
	});
	
	$('#form_precalificacion input[type="text"], #form_precalificacion textarea').focusout(function(){      
		if($(this).val() == ""){
			$(this).addClass('color');
		}
	});
	
	$('#form_precalificacion input[name="cli_vigencia_licencia"], #form_precalificacion input[name="cli_inicial_fec"], #form_precalificacion input[name="cli_inspeccion_domi"]').change(function(){
		$(this).removeClass('color');
	});

	$('#form_precalificacion select[name="cli_marca"], #form_precalificacion select[name="cli_modelo"], #form_precalificacion select[name="cli_semaforizacion"]').change(function(){
		$(this).removeClass('color');
	});
	//

	$('input[type="radio"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});
	
	$('input[type="checkbox"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('#cli_vigencia_licencia, #cli_inicial_fec, #cli_inspeccion_domi').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
	});

	/* $("#cli_razon").autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
				VISTA_URL + "cliente/cliente_autocomplete.php",
				{ term: request.term }, //
				response
			);
		},
		select: function (event, ui) {
			//$('#cli_proveedor_id').val(ui.item.cliente_id);
			$('#cli_razon').val(ui.item.cliente_nom);
			$('#cli_ruc').val(ui.item.cliente_doc);
			
			verPaginaproceso('1');
			event.preventDefault();
		}
	}); */

	$("#cli_proveedor").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "transferente/transferente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            //$('#hdd_cre_cli_id').val(ui.item.cliente_id);
            //$('#hdd_cre_cli_id_ori').val(ui.item.cliente_id);
            //$("#txt_transferente_fil_doc").val(ui.item.transferente_doc);
            $("#cli_proveedor").val(ui.item.transferente_nom);
            $("#cli_proveedor_id").val(ui.item.transferente_id);

            //vencimiento_tabla();
            event.preventDefault();
            //$('#txt_transferente_fil_nom').focus();
        }
    });

  	$('#form_precalificacion').validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"proceso/proceso_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_precalificacion").serialize(),
				beforeSend: function() {
					//$('#proveedor_mensaje').show(400);
					$('#btn_guardar_precalificacion').prop('disabled', true);
				},
				success: function(data){
					console.log(data);
					if(parseInt(data.estado) > 0){
						$('#modal_hoja_precalificacion').modal('hide');
						Swal.fire(
							'Excelente!',
							data.mensaje,
							'success'
						);
					}
					
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					console.log(data);
				}
			});
	  	},
	  
	});

	$('#form_comentario_precalificacion').validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"proceso/proceso_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_comentario_precalificacion").serialize(),
				beforeSend: function() {
					$('#btn_guardar_comentario_precalificacion').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) == 0){
						Swal.fire(
							'Error!',
							data.mensaje,
							'error'
						);
						$('#btn_guardar_comentario_precalificacion').prop('disabled', false);

					}else
					if(parseInt(data.estado) == 1){
						$('#modal_comentario_precalificacion').modal('hide');
						Swal.fire(
							'Excelente!',
							data.mensaje,
							'success'
						);
						verComentarioPreCalificacion($('#hdd_proceso_id').val(),$('#hdd_usuario_id').val(),$('#hdd_proceso_fase_item_id').val());
					}else
					if(parseInt(data.estado) == 2){
						Swal.fire(
							'Alerta!',
							data.mensaje,
							'alert'
						);
						$('#btn_guardar_comentario_precalificacion').prop('disabled', false);

					}
					
				},
				complete: function(data){
					//console.log(data);
				},
					
			});
	  	},
	  
	});

	$('#cli_marca').change(function () {
        cargar_modelo($(this).val());
    });
});

// en uso para paginado / numero celular
function solo_numero(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}

function vehiculomarca_form(usuario_act, vehiculomarca_id){ 
	$.ajax({
		type: "POST",
		url: VISTA_URL+"vehiculomarca/vehiculomarca_form.php",
	  	async: true,
		dataType: "html",
		data: ({
			action: usuario_act, // PUEDE SER: L, I, M , E
			modulo: 'creditogarveh',
			vehiculomarca_id: vehiculomarca_id
	  	}),
		beforeSend: function() {
			$('#h3_modal_title').text('Cargando Formulario');
			$('#modal_mensaje').modal('show');
		},
		success: function(data){
			$('#modal_mensaje').modal('hide');
			if(data != 'sin_datos'){
				$('#div_modal_vehiculomarca_form').html(data);
				$('#modal_registro_vehiculomarca').modal('show');
	
				//desabilitar elementos del form si es L (LEER)
				if(usuario_act == 'L' || usuario_act == 'E')
					form_desabilitar_elementos('form_vehiculomarca'); //funcion encontrada en public/js/generales.js
					modal_hidden_bs_modal('modal_registro_vehiculomarca', 'limpiar'); //funcion encontrada en public/js/generales.js
			}else{
				//llamar al formulario de solicitar permiso
				var modulo = 'vehiculomarca';
				var div = 'div_modal_vehiculomarca_form';
				permiso_solicitud(usuario_act, vehiculomarca_id, modulo, div); //funcion ubicada en public/js/permiso.js
			}
		},
		complete: function(data){
			
		},
		error: function(data){
			$('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
			$('#overlay_modal_mensaje').removeClass('overlay').empty();
			$('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			//console.log(data.responseText);
		}
	});
}

function cargar_modelo(marca_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
        async: true,
        dataType: "html",
        data: ({
            vehiculomarca_id: marca_id
        }),
        beforeSend: function () {
            $('#cli_modelo').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cli_modelo').html(html);
        }
    });
}

function vehiculomodelo_form(usuario_act, vehiculomodelo_id){ 
	$.ajax({
		type: "POST",
		url: VISTA_URL+"vehiculomodelo/vehiculomodelo_form.php",
	  	async: true,
		dataType: "html",
		data: ({
			action: usuario_act, // PUEDE SER: L, I, M , E
			modulo: 'creditogarveh',
			vehiculomodelo_id: vehiculomodelo_id
		}),
		beforeSend: function() {
			$('#h3_modal_title').text('Cargando Formulario');
			$('#modal_mensaje').modal('show');
		},
		success: function(data){
			$('#modal_mensaje').modal('hide');
			if(data != 'sin_datos'){
				$('#div_modal_vehiculomodelo_form').html(data);
				$('#modal_registro_vehiculomodelo').modal('show');
  
				//desabilitar elementos del form si es L (LEER)
				if(usuario_act == 'L' || usuario_act == 'E')
					form_desabilitar_elementos('form_vehiculomodelo'); //funcion encontrada en public/js/generales.js
					modal_hidden_bs_modal('modal_registro_vehiculomodelo', 'limpiar'); //funcion encontrada en public/js/generales.js
			}else{
				//llamar al formulario de solicitar permiso
				var modulo = 'vehiculomodelo';
				var div = 'div_modal_vehiculomodelo_form';
				permiso_solicitud(usuario_act, vehiculomodelo_id, modulo, div); //funcion ubicada en public/js/permiso.js
			}
		},
		complete: function(data){
			
		},
		error: function(data){
			$('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
			$('#overlay_modal_mensaje').removeClass('overlay').empty();
			$('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			//console.log(data.responseText);
		}
	});
}

function verComentarioPreCalificacion(proceso_id, usuario_id, proceso_fase_item_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/comentario_precalificacion.php",
		async:true,
		dataType: "html",                      
		data: ({
			proceso_id: proceso_id,
			usuario_id: usuario_id,
      proceso_fase_item_id: proceso_fase_item_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_comentario_precalificacion').html(html);		
      $('#modal_comentario_precalificacion').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_comentario_precalificacion', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_comentario_precalificacion'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

function guardarDatosComite(id, value, tipo_dato) {
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "proceso/proceso_controller.php",
	  async: false,
	  dataType: "json",
	  data: ({
		action: 'guardar_campo_comentario',
		proceso_id: $("#hdd_proceso_id").val(),
		usuario_id: $("#hdd_usuario_id").val(),
		proceso_fase_item_id: $("#hdd_proceso_fase_item_id").val(),
		id: id, 
		value: value,
		tipo_dato: tipo_dato,
	  }),
		beforeSend: function () {
		},
		success: function (data) {
		  if(data.estado == 1){
			notificacion_success(data.mensaje, 3000);
		  }else{
			notificacion_warning(data.mensaje, 3000);
		  }
		},
		complete: function (data) {
		}
	});
  
  }

// EXPORT PFD HOJA DE PRECALIFICACION
function exportarHojaPDFHistorial(proceso_id, accion) {
	window.open("vista/proceso/hoja_precalificacion_pdf.php?proceso_id="+proceso_id+"&accion="+accion,"_blank");
}