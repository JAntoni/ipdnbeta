<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../uploadpdf/Upload.class.php');
  $oUpload = new Upload();
  require_once('../funciones/funciones.php');

  $direc = 'upload';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)

  $modulo_nom = $_POST['modulo_nom'];
  $modulo_id = $_POST['modulo_id'];
  $upload_uniq = $_POST['upload_uniq']; //id temporal
  $upload_id = $_POST['upload_id'];
  $vista = $_POST['vista'];

  $bandera = 1;

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'ACCION NO DISPONIBLE';
  elseif($usuario_action == 'I')
    $titulo = 'SUBIR DOCUMENTOS EN PDF';
  elseif($usuario_action == 'M')
    $titulo = 'ACCION NO DISPONIBLE';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar imagen';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_upload" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
            <h4 class="modal-title" style="font-weight: bold;font-family: cambria;color: #003399"><?php echo $titulo;?></h4>
        </div>
        <form id="form_upload" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_modulo_nom" id="hdd_modulo_nom" value="<?php echo $modulo_nom;?>">
          <input type="hidden" name="hdd_modulo_id" id="hdd_modulo_id" value="<?php echo $modulo_id;?>">
          <input type="hidden" name="hdd_upload_uniq" id="hdd_upload_uniq" value="<?php echo $upload_uniq;?>">
          <input type="hidden" name="hdd_upload_id" id="hdd_upload_id" value="<?php echo $upload_id;?>">
          <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <?php if($action != 'eliminar'):?>
              <div class="row">
                <div class="col-md-12">
                  <input id="file_upload" name="file_upload" type="file" multiple="true">
                </div>
                <div class="col-md-12">
                  <div id="queue"></div>
                </div>
                <div class="col-md-12">
                  <div class="alert alert-danger alert-dismissible" id="alert_img" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>
                      <i class="icon fa fa-ban"></i> Alerta
                    </h4>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Imagen?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="upload_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="button" class="btn btn-info" id="btn_guardar_upload" onclick="javascript:$('#file_upload').uploadifive('upload')">Subir Documento</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_upload">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_cerrar_upload">Listo</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_upload">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo 'static/css/uploadifive/uploadifive.css';?>">
<script type="text/javascript" src="<?php echo 'static/js/uploadifive/jquery.uploadifive.js';?>"></script>
<script type="text/javascript" src="<?php echo 'vista/uploadpdf/upload_form.js?ver=56464646464646464646';?>"></script>
