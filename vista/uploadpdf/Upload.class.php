<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Upload extends Conexion{
    public $upload_id;

    public $modulo_nom;
    public $modulo_id;
    public $upload_uniq;
    public $upload_url = '...';
    public $usuario_id;

    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO upload(modulo_nom, modulo_id, upload_url, upload_uniq, tb_usuario_id) 
                VALUES(:modulo_nom, :modulo_id, :upload_url, :upload_uniq, :usuario_id)";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $this->modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":upload_url", $this->upload_url, PDO::PARAM_STR);
        $sentencia->bindParam(":upload_uniq", $this->upload_uniq, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);

        $result1 = $sentencia->execute();
        $this->upload_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        
        
           

        $nombre_imagen = $this->modulo_nom.'_'.$this->modulo_id.'_upload_'. $this->upload_id .'.'. $this->fileParts['extension'];
        $upload_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $upload_url)){
          //insertamos las imágenes
          $sql = "UPDATE upload SET upload_url =:upload_url WHERE upload_id =:upload_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":upload_id", $this->upload_id, PDO::PARAM_INT);
          $sentencia->bindParam(":upload_url", $upload_url, PDO::PARAM_STR);
          $result2 = $sentencia->execute();
        }
        else{
          //$this->eliminar($this->upload_id);
        }
        

        if($result1 == 1){
          return 'exito';
//            $data['mensaje'] = 'exito';
//            $data['estado'] = $result1;
//            $data['upload_id'] = $this->upload_id;
//            return $data; //si es correcto el poliza retorna 1
        }
        else
          return 'Error al registrar la imagen subida';
//            $data['mensaje'] = 'Error al registrar el PDF subido';
//            $data['estado'] = 0;
//            $data['upload_id'] = $this->upload_id;
//            return $data; //si es correcto el poliza retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function modificar_modulo_id_imagen($modulo_id, $upload_uniq){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE upload SET modulo_id =:modulo_id WHERE upload_uniq =:upload_uniq";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":upload_uniq", $upload_uniq, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($upload_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE upload SET upload_xac = 0 WHERE upload_id =:upload_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":upload_id", $upload_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($upload_id){
      try {
        $sql = "SELECT * FROM upload WHERE upload_id =:upload_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":upload_id", $upload_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrarUpload_uniq($upload_id){
      try {
        $sql = "SELECT * FROM upload WHERE upload_uniq =:upload_uniq";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":upload_uniq", $upload_id, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_uploads($modulo_nom, $modulo_id){
      try {
        $sql = "SELECT * FROM upload u INNER JOIN tb_factura f on f.tb_uploap_id = u.upload_id"
                . " where modulo_nom =:modulo_nom and modulo_id =:modulo_id AND upload_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay imágenes subidas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    function modificar_campo($poliza_id, $poliza_columna, $poliza_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($poliza_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_poliza SET " . $poliza_columna . " =:poliza_valor WHERE tb_poliza_id =:poliza_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);
                if ($param_tip == 'INT') {
                    $sentencia->bindParam(":poliza_valor", $poliza_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":poliza_valor", $poliza_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el poliza retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    
    function mostrarUno_modulo($modulo_id,$modulo_nom){
      try {
        $sql = "SELECT * FROM upload WHERE modulo_id=:modulo_id AND modulo_nom =:modulo_nom AND upload_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
  }

?>
