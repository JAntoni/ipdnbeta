<?php

if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'uploadpdf/Upload.class.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../uploadpdf/Upload.class.php');
}
$oUpload = new Upload();

require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$modulo_id = (isset($_POST['modulo_id'])) ? $_POST['modulo_id'] : intval($modulo_id);
$modulo_nom = (isset($_POST['modulo_nom'])) ? $_POST['modulo_nom'] : intval($modulo_nom);
$usuario_id = (isset($_POST['usuario_id'])) ? $_POST['usuario_id'] : intval($usuario_id);



//echo '$modulo_id=='.$modulo_id.'  $modulo_nom=='.$modulo_nom.' $usuario_id=='.$usuario_id;exit();

$dts = $oUsuario->mostrarUno($usuario_id);
if ($dts['estado'] == 1) {
    $usuario_nombre = $dts['data']['tb_usuario_nom'];
    $usuario_apellido = $dts['data']['tb_usuario_ape'];
    $tb_usuario_fot = $dts['data']['tb_usuario_fot'];
}

$result = $oUpload->listar_uploads($modulo_nom, $modulo_id);
$contar=0;
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $contar++;
        echo '
            <div class="item">
                <img src="'.$tb_usuario_fot.'" alt="user image" class="online">
                <p class="message" style="font-family:cambria">
                    <a href="'.$value['upload_url'].'" class="name"  target="_blank">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> ' . mostrar_fecha_hora($value['upload_reg']) . '</small>
                        '.$usuario_nombre.' '.$usuario_apellido.'  <i class="fa fa-file-pdf-o" title="Ver Contratos"></i> 
                    </a>
                    CONTRATO N° '.$contar.'
                </p>
            </div>';
    }
}
$result = NULL;
?>