
$(document).ready(function () {
    $('#btn_cerrar_upload').click(function (event) {
//        upload_galeria();
    });

    $('#file_upload').uploadifive({
        'auto': false,
        //'checkScript'      : 'programacion_file_check.php',
        'formData': {
            'action': 'insertar',
            'modulo_nom': $('#hdd_modulo_nom').val(),
            'modulo_id': $('#hdd_modulo_id').val(),
            'upload_uniq': $('#hdd_upload_uniq').val()
        },
        'queueID': 'queue',
        'uploadScript': VISTA_URL + 'uploadpdf/upload_controller.php',
        'queueSizeLimit': 10,
        'uploadLimit': 10,
        'multi': true,
        'buttonText': 'Seleccionar Archivo',
        'height': 20,
        'width': 180,
        'fileSizeLimit': '50MB',
        'fileType': ['pdf'],
        'onUploadComplete': function (file, data) {
            if (data != 'exito') {
                $('#alert_img').show(300);
                $('#alert_img').append('<strong>El archivo: ' + file.name + ', no se pudo subir -> ' + data + '</strong><br>');
            }
        },
        'onError': function (errorType) {
            var message = "Error desconocido.";
            if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
                message = "Tamaño de archivo debe ser menor a 20MB.";
            } else if (errorType == 'FORBIDDEN_FILE_TYPE') {
                message = "Tipo de archivo no válido.";
                alert(message);
            }
            //$(".uploadifive-queue-item.error").hide();
            //
            //$.FormFeedbackError(message);
        },
        'onAddQueueItem': function (file) {
            //console.log('The file ' + file.name + ' was added to the queue!');
            //alert(file.type);
        },
        /*'onCheck'      : function(file, exists) {
         if (exists) {
         alert('El archivo ' + file.name + ' ya existe.');
         }
         }*/
    });

    $('#form_upload').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: VISTA_URL + "uploadpdf/upload_controller.php",
            async: true,
            dataType: "json",
            data: $("#form_upload").serialize(),
            beforeSend: function () {
                $('#upload_mensaje').show(400);
                $('#btn_guardar_upload').prop('disabled', true);
            },
            success: function (data) {
                if (parseInt(data.estado) > 0) {
                    $('#upload_mensaje').removeClass('callout-info').addClass('callout-success')
                    $('#upload_mensaje').html(data.mensaje);
                    var upload_id = $('#hdd_upload_id').val();
                    $('#uploadid_' + upload_id).hide(400);
                    $('#modal_registro_upload').modal('hide');
                    var vista=$("#hdd_vista").val();
                    if(vista=="seguros"){
                        $("#txt_uploap").val(data.upload_id);
                    }
//                                                $(codigo).val();
                } else {
                    $('#upload_mensaje').removeClass('callout-info').addClass('callout-warning')
                    $('#upload_mensaje').html('Alerta: ' + data.mensaje);
                    $('#btn_guardar_upload').prop('disabled', false);
                }
            },
            complete: function (data) {
                //console.log(data);
            },
            error: function (data) {
                $('#upload_mensaje').removeClass('callout-info').addClass('callout-danger')
                $('#upload_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
            }
        });
    });

});
