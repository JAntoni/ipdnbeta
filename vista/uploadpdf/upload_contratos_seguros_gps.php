<?php

if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'uploadpdf/Upload.class.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../uploadpdf/Upload.class.php');
}
$oUpload = new Upload();

require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$modulo_id = (isset($_POST['modulo_id'])) ? $_POST['modulo_id'] : intval($modulo_id);
$modulo_nom = (isset($_POST['modulo_nom'])) ? $_POST['modulo_nom'] : intval($modulo_nom);



$result = $oUpload->listar_uploads($modulo_nom, $modulo_id);
$contar=0;
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        if($value['tb_factura_tipo']==1){$tipo_factura = 'BOLETA';}
        elseif ($value['tb_factura_tipo']==2) {$tipo_factura = 'FACTURA';}
        else{$tipo_factura = 'NOTA CREDITO';}
            
        $contar++;
        echo '
            <div class="item">
                <img src="public/images/avatar.png" alt="user image" class="online">
                <p class="message" style="font-family:cambria">
                <i class="fa fa-trash text-danger  pull-right" title="Eliminar PDF" onclick="Eliminar_pdf('.$value['upload_id'].')"></i>;
                    <a href="'.$value['upload_url'].'" class="name"  target="_blank">                      
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> ' . mostrar_fecha_hora($value['upload_reg']) . '</small>
                          '.$tipo_factura.' - '.$value['tb_factura_numero'].' - '.$value['tb_factura_emitente'].' <i class="fa fa-file-pdf-o" title="Ver Contratos"></i> 
                    </a>
                </p>
            </div><br>';
    }
}
$result = NULL;
?>
