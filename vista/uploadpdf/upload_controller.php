<?php

require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../uploadpdf/Upload.class.php');
$oUpload = new Upload();

$action = $_POST['action'];

if ($action == 'insertar') {

    if (!empty($_FILES)) {
        $modulo_nom = $_POST['modulo_nom'];
        $modulo_id = intval($_POST['modulo_id']);
        $upload_uniq = $_POST['upload_uniq']; //id temporal

        //echo $modulo_nom.' / '.$modulo_id.' / '.$upload_uniq; exit();
        $directorio = 'public/pdf/' . $modulo_nom . '/';
        $fileTypes = array('pdf'); // Allowed file extensions

        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetFile = $directorio . $_FILES['Filedata']['name'];
        $fileParts = pathinfo($_FILES['Filedata']['name']);
//        echo 'hasta aqui todo normal ';exit();
        if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
            //move_uploaded_file($tempFile, $targetFile); insertamos las imágenes
            $oUpload->modulo_nom = $modulo_nom;
            $oUpload->modulo_id = $modulo_id;
            $oUpload->upload_uniq = $upload_uniq;
            $oUpload->usuario_id = intval($_SESSION['usuario_id']);

            $oUpload->tempFile = $tempFile; //temp servicio de PHP
            $oUpload->directorio = $directorio; //nombre con el que irá guardado en la carpeta
            $oUpload->fileParts = $fileParts; //extensiones de las imágenes

            $return = $oUpload->insertar();
            echo $return;

        } else {
            // The file type wasn't allowed
            echo 'Tipo de archivo no válido.';
        }
    } else {
        echo 'No hay ningún archivo para subir';
    }
} elseif ($action == 'modificar') {
    $upload_id = intval($_POST['hdd_upload_id']);
    $oUpload->upload_id = $upload_id;

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar.';

    if ($oUpload->modificar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'upload modificado correctamente.';
    }

    echo json_encode($data);
} elseif ($action == 'eliminar') {
    $upload_id = intval($_POST['upload_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar al upload.';

    if ($oUpload->eliminar($upload_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Documento eliminado correctamente';
    }

    echo json_encode($data);
} else {
    echo 'No se ha identificado ningún tipo de transacción para ' . $action;
}
?>
