function metodo_mostrar_ocultar() {
  $("#cbo_metodo_id").change(function (event) {
    var metodo = parseInt($(this).val());
    if (metodo == 2) {
      $("#txt_montodeposito").show(200);
      $("#precio_banco").show(200);
      $("#precio_banco2").show(200);
      $("#precio_banco3").show(200);
    } else {
      // Con depósito solo para clientes externos
      $("#txt_montodeposito").hide(200);
      $("#precio_banco").hide(200);
      $("#precio_banco2").hide(200);
      $("#precio_banco3").hide(200);
    }
  });
}

function seleccionarModal(
  placa,
  creditoid,
  creditotipo,
  marca,
  modelo,
  stockunidadid
) {
  Swal.fire({
    title: "Se está liquidando el total del monto ¿Deseas generar una venta?",
    icon: "info",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "SI",
    showDenyButton: true,
    denyButtonText: "NO",
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#21ba45",
    /*  montobase:montobase, */
  }).then((result) => {
    if (result.isConfirmed) {
      // Crear el botón dinámicamente
      $("#div_modal_stockunidad_separar").modal("hide");
      $("#hdd_precio_base").val(montobase);
      $("#txt_ajuste_tot").val();
      ventavehiculo_form(
        placa,
        "vender",
        creditoid,
        creditotipo,
        marca,
        modelo,
        stockunidadid
      );
    } else if (result.isDenied) {
      swal.close();
    }
  });
}

function calcularDiferencia(monto) {
  var placa = $("#txt_placa_mostrar").val();
  var creditoid = $("#hdd_credito_id").val();
  var creditotipo = $("#hdd_creditotipo_id").val();
  var marca = $("#marca").val();
  var modelo = $("#modelo").val();
  var stockunidadid = $("#hdd_stockunidad_id").val();

  var moneda = $("#cbo_moneda_id").val();
  var tipocambio = parseFloat($("#txt_tipocambio_id").val());

  var pre_tot = parseFloat($("#txt_tot").val()); // Obtener el valor del monto total
  var diferencia;

  if (moneda == "1") {
    diferencia = pre_tot - monto; // Calcular la diferencia normalmente
  } else if (moneda == "2") {
    var pre_tot_div = pre_tot / tipocambio; // Dividir el monto total por el tipo de cambio
    diferencia = pre_tot_div - monto; // Calcular la diferencia
  }

  if (monto > pre_tot) {
    alerta_error(
      "ERROR",
      "El monto de pago no puede ser mayor al monto total."
    );
    $("#txt_monto").val(""); // Limpiar el campo de monto
    $("#txt_ajuste_tot").val(pre_tot.toFixed(2)); // Restaurar el valor del monto total
  } else if (monto == pre_tot) {
    console.log(placa, creditoid, creditotipo, marca, modelo, stockunidadid);
    seleccionarModal(
      placa,
      creditoid,
      creditotipo,
      marca,
      modelo,
      stockunidadid
    );
    $("#txt_ajuste_tot").val("0.00"); // Ajustar el valor del monto total
  } else {
    $("#txt_ajuste_tot").val(diferencia.toFixed(2)); // Actualizar el valor del monto total en el campo de texto
  }

  console.log("Precio total: " + pre_tot);
  console.log("Monto: " + monto);
}

//funcion para llamar a formulario de registrar cliente
function cliente_form(usuario_act, cliente_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: cliente_id,
      vista: "stockunidad",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "cliente";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

function llenar_campos_cliente(data) {
  //obtener el valor de un array (primero referencia la data y luego referencia el campo)
  $("#hdd_stockunidad_clienteid").val(data.cliente_id);
  $("#txt_stockunidad_clienteid").val(
    data.cliente_doc + "-" + data.cliente_nom
  );
  disabled($("#btn_agr"));
  disabled($("#txt_stockunidad_clienteid"));
}
//fin de formulario para llamar a cliente

$(document).ready(function () {
  var tipocambio = $("#hdd_tipo_cambio_venta").val();

  $(".moneda2").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "99999.00",
  });

  $(".moneda3").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "99999.99",
  });

  $("#txt_stockunidad_clienteid")
    .autocomplete({
      minLength: 1,
      source: function (request, response) {
        $.getJSON(
          VISTA_URL + "cliente/cliente_autocomplete.php",
          { term: request.term }, //1 solo clientes de la empresa
          response
        );
      },
      select: function (event, ui) {
        $("#txt_stockunidad_clienteid").val(ui.item.cliente_nom);
        $("#hdd_stockunidad_clienteid").val(ui.item.cliente_id);
      },
    })
    .keyup(function () {
      if ($("#txt_stockunidad_clienteid").val() === "") {
        $("#hdd_stockunidad_clienteid").val("");
      }
    });

  $("#cbo_metodo_id").change();
  metodo_mostrar_ocultar();

  // Evento para calcular la diferencia cada vez que el usuario ingrese un nuevo valor en el monto
  $("#txt_monto").on("keyup", function () {
    calcularDiferencia();
    var monto = parseFloat($(this).val());
    if (!isNaN(monto)) {
      // Verificar si el valor ingresado es un número válido
      calcularDiferencia(monto);
    } else {
      alert("Por favor, ingrese un valor numérico válido.");
      $(this).val(""); // Limpiar el campo de monto
      $("#txt_ajuste_tot").val($("#txt_tot").val()); // Restaurar el valor del monto total
    }
  });

  $("#form_stockunidad").validate({
    submitHandler: function () {
      var nombrecuenta =
        "&cmb_cuedep_nombre=" + $("#cmb_cuedep_id option:selected").text();

      $.ajax({
        type: "POST",
        url: VISTA_URL + "stockunidad/stockunidad_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_stockunidad").serialize() + nombrecuenta,
        beforeSend: function () {
          $("#stockunidad_mensaje").show(400);
          $("#btn_guardar_stockunidad").prop("disabled", true);
        },
        success: function (data) {
          console.log(data);
          if (parseInt(data.estado) > 0) {
            $("#stockunidad_mensaje")
              .removeClass("callout-info")
              .addClass("callout-success");
            $("#stockunidad_mensaje").html(data.mensaje);

            setTimeout(function () {
              $("#div_modal_stockunidad_separar").modal("hide");
            }, 1000);
          } else {
            $("#stockunidad_mensaje")
              .removeClass("callout-info")
              .addClass("callout-warning");
            $("#stockunidad_mensaje").html("Alerta: " + data.mensaje);
            $("#btn_guardar_stockunidad").prop("disabled", false);
          }
        },
        complete: function (data) {
          console.log(data);
        },
        error: function (data) {
          $("#stockunidad_mensaje")
            .removeClass("callout-info")
            .addClass("callout-danger");
          $("#stockunidad_mensaje").html(
            "ALERTA DE ERROR: " + data.responseText
          );
        },
      });
    },
    rules: {
      txt_monto: {
        required: true,
        min: "1000",
      },
      txt_stockunidad_clienteid: {
        required: true,
      },
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else if (element.parent("div.input-group").length == 1) {
        error.insertAfter(element.parent("div.input-group")[0]);
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });

  $("#txt_montodeposito, #txt_combanco").change(function (event) {
    var montdepo = Number($("#txt_montodeposito").autoNumeric("get"));
    var combanco = Number($("#txt_combanco").autoNumeric("get"));
    var saldo = 0;

    saldo = montdepo - combanco;

    if (montdepo > 0 && combanco >= 0 && saldo > 0) {
      $("#txt_montopag").autoNumeric("set", saldo.toFixed(2));
    } else {
      $("#txt_montopag").autoNumeric("set", 0);
    }
  });
});
