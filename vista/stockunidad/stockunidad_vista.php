<?php
	
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<!-- <small>< ?php echo $menu_des; ?></small> -->
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- INCLUIMOS LOS FILTROS -->
		<?php //require_once(VISTA_URL.'./stockunidad/stockunidad_filtro.php'); ?>

		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<span><b>Filtros</b></span>
						<div class="panel panel-primary" id="div_filtros_cgv">
							<div class="panel-body">
								<?php include VISTA_URL . 'stockunidad/stockunidad_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="stockunidad_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div id="div_stockunidad_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap"></div>
					</div>
				</div>
			</div>

			<div id="div_modal_stockunidad_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>

			<div id="div_modal_stockunidad_separar_form">
				<!-- INCLUIMOS EL MODAL PARA SEPARAR VEHÍCULO-->
			</div>

			<div id="div_modal_stockunidad_venta_form">
				<!-- INCLUIMOS EL MODAL PARA VENDER VEHÍCULO-->
			</div>

			<div id="div_modal_stockunidad_historial_form">
				<!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DEL VEHÍCULO-->
			</div>

			<div id="div_modal_cliente_form">
				<!-- INCLUIMOS EL MODAL PARA REGISTRAR CLIENTE-->
			</div>

			<div id="div_modal_liquidacion">
				<!-- INCLUIMOS EL MODAL PARA ACTUALIZAR DATOS DL SEGUNDO MOMENTO-->
			</div>

			<div id="div_modal_ajustar_form">
				<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			</div>
			<div id="div_modal_balanceventa_form">
				<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>

	</section>
	<!-- /.content -->
</div>
