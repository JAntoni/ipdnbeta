$(document).ready(function () {

  disabled($("#btn_edit"));

  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999999.00",
  });

  // $("#cbo_metodo_id").change();
  metodo_mostrar_ocultar();

  $("#txt_cliente_id")
    .autocomplete({
      minLength: 1,
      source: function (request, response) {
        $.getJSON(
          VISTA_URL + "cliente/cliente_autocomplete.php",
          { term: request.term }, //1 solo clientes de la empresa
          response
        );
      },
      select: function (event, ui) {
        $("#btn_edit").removeAttr("disabled");
        $("#txt_cliente_id").val(ui.item.cliente_nom);
        $("#hdd_cliente_id").val(ui.item.cliente_id);
      },
    })
    .keyup(function () {
      if ($("#txt_cliente_id").val() === "") {
        disabled($("#btn_edit"));
        $("#hdd_cliente_id").val("");
      }
    });

  $("#txt_usuario_id2")
    .autocomplete({
      minLength: 1,
      source: function (request, response) {
        $.getJSON(
          VISTA_URL + "usuario/usuario_autocomplete.php",
          { term: request.term }, //1 solo clientes de la empresa
          response
        );
      },
      select: function (event, ui) {
        $("#txt_usuario_id2").val(
          ui.item.tb_usuario_nom + " " + ui.item.tb_usuario_ape
        );
        $("#hdd_usuario_id2").val(ui.item.tb_usuario_id);
        event.preventDefault();
      },
    })
    .keyup(function () {
      if ($("#txt_usuario_id2").val() === "") {
        $("#hdd_usuario_id2").val("");
      }
    });

  $("#txt_usuarioautoriza_id")
    .autocomplete({
      minLength: 1,
      source: function (request, response) {
        $.getJSON(
          VISTA_URL + "usuario/usuario_autocomplete.php",
          { term: request.term }, //1 solo clientes de la empresa
          response
        );
      },
      select: function (event, ui) {
        $("#txt_usuarioautoriza_id").val(
          ui.item.tb_usuario_nom + " " + ui.item.tb_usuario_ape
        );
        $("#hdd_usuarioautoriza_id").val(ui.item.tb_usuario_id);
        event.preventDefault();
      },
    })
    .keyup(function () {
      if ($("#txt_usuarioautoriza_id").val() === "") {
        $("#hdd_usuarioautoriza_id").val("");
      }
  });

  $('#txt_credito_id').keydown(function(event) {
    var tecla = event.which || event.keyCode;
    if (tecla == 13) { buscarCredito(); } else { return 0; }
  });

  $("#form_stockunidad_vender").validate({
    submitHandler: function () {

      var moneda_id = $('#hdd_moneda_id_padre').val(); // Moneda padre
      cambio_moneda(moneda_id); // Moneda Padre

      console.log('Tipo de cambio validado: | Compra | Venta | '+$('#txt_ventavehiculo_tipcam').val());
      
      $.confirm({
        title: "¡Cuidado!",
        content: "¿Esta seguro que desea registrar la venta?",
        type: "blue",
        escapeKey: "close",
        backgroundDismiss: true,
        columnClass: "small",
        buttons: {
          Confirmar: {
            btnClass: "btn-blue",
            action: function () {

              var floatMontoPagar = 0;
              var monto_a_pagar = $("#txt_ventavehiculo_mon").val();
              monto_a_pagar = monto_a_pagar.replaceAll(",", "");
              floatMontoPagar = floatMontoPagar + parseFloat(monto_a_pagar);
              $("#hdd_ventavehiculo_mon").val(floatMontoPagar.toFixed(2));

              var monto_base = $("#hdd_precio_base").val();
              monto_base = parseFloat(monto_base).toFixed(2);
              var ingresos_previos = $("#hdd_monto_previos").val();
              ingresos_previos = parseFloat(ingresos_previos).toFixed(2);
              var saldo_a_pagar = $("#hdd_saldo_a_pagar").val();
              saldo_a_pagar = parseFloat(saldo_a_pagar).toFixed(2);

              var validar_operacion = "";
              var validar_sobrecosto = 0;
              var validar_suma_ingprev_montpag = 0;

              validar_suma_ingprev_montpag = (parseFloat(ingresos_previos) + parseFloat(floatMontoPagar)).toFixed(2);
              
              $.ajax({
                type: "POST",
                url: VISTA_URL + "stockunidad/stockunidad_controller.php",
                async: true,
                dataType: "json",
                data: {
                  action: "vender_registrar",
                  p_array_datos: $("#form_stockunidad_vender").serialize(),
                  vista: "stockunidad",
                },
                beforeSend: function () {
                  $("#h3_modal_title").text("Cargando ...");
                  $("#modal_mensaje").modal("show");
                  $("#btn_vender_unidad_vehicular").prop("disabled", true);
                  // console.log('mapeando errores 1')
                },
                success: function (data) {
                  // console.log('mapeando errores 2')
                  console.log(data);
                  if (parseInt(data.estado) > 0) {
                    // $("#stockunidad_vender_mensaje").removeClass("callout-info").addClass("callout-success");
                    // $("#stockunidad_vender_mensaje").html(data.mensaje);
                    $("#modal_mensaje").modal("hide");
                    alerta_success("EXITO",""+data.mensaje);
                    $("#div_modal_stockunidad_vender").modal("hide");
                    stockunidad_tabla();
                  } else {
                    $("#modal_mensaje").modal("hide");
                    alerta_warning("Advertencia",""+data.mensaje);
                    $("#btn_vender_unidad_vehicular").prop("disabled", false);
                  }
                },
                complete: function (data) {
                  // console.log('mapeando errores 3')
                  // console.log(data);
                  $("#modal_mensaje").modal("hide");
                },
                error: function (data) {
                  console.log(data);
                  $("#stockunidad_vender_mensaje").removeClass("callout-info").addClass("callout-danger");
                  $("#stockunidad_vender_mensaje").empty();
                  $("#stockunidad_vender_mensaje").html(
                    "ALERTA DE ERROR: " + data.responseText
                  );
                },
              });
            },
          },
          Cancelar: function () {

          },
        },
      });      
    },
    rules: {
      txt_ventavehiculo_fec: {
        required: true,
      },
      txt_ventavehiculo_tipcam: {
        required: true,
      },
      txt_ventavehiculo_bas: {
        required: true,
      },
      cmb_moneda_id_padre: {
        required: true,
      },
      cmb_moneda_id_conversion: {
        required: true,
      },
      txt_cliente_id: {
        required: true,
      },
      txt_usuario_id2: {
        required: true,
      },
      txt_usuarioautoriza_id: {
        required: true,
      },
      cmb_metodo_pago: {
        required: true,
      },
      txt_monto_previos: {
        required: true,
      },
      txt_saldo_a_pagar: {
        required: true,
      },
      txt_ventavehiculo_mon: {
        required: true,
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else if (element.parent("div.input-group").length == 1) {
        error.insertAfter(element.parent("div.input-group")[0]);
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });
});

$('#txt_montodeposito, #txt_comisionbanco').change(function (event) {
  var monto_deposito  = Number($('#txt_montodeposito').autoNumeric('get'));
  var monto_comision  = Number($('#txt_comisionbanco').autoNumeric('get'));
  var cuenta_deposito = $('#cmb_cuedep_id').val();
  var saldo = 0;

  saldo = monto_deposito - monto_comision;

  if (monto_deposito > 0 && monto_comision >= 0 && saldo > 0) {
    $('#txt_ventavehiculo_mon').autoNumeric('set', saldo.toFixed(2));
    $('#txt_montoDepositoValido').autoNumeric('set', saldo.toFixed(2));
  } else {
    $('#txt_ventavehiculo_mon').autoNumeric('set', 0);
    $('#txt_montoDepositoValido').autoNumeric('set', 0);
  }

  if (cuenta_deposito){ $('#cmb_cuedep_id').change(); }
    
});

$('#cmb_cuedep_id').change(function (event) {
  var moneda_sel = $(this).find(':selected').data('moneda'); // este tipo de moneda corresponde al tipo de cuenta seleccionada  
  var moneda_id = $('#hdd_moneda_id_padre').val(); // Moneda padre

  cambio_moneda(moneda_id); // Moneda Padre

  var monto_validar = Number($('#txt_montoDepositoValido').autoNumeric('get')); // es el restanto de lo depositado menos la comision que cobra el banco
  var monto_cambio = Number($('#txt_ventavehiculo_tipcam').val()); // monto del tipo de cambio del día del deposito
  var total_pagado = 0;
  var concatDescripMon = "";
  if( moneda_sel == 1 ) { concatDescripMon = "Soles"; } else { concatDescripMon = "Dólares"; }
    
  $("#cmb_moneda_id_conversion").val(parseInt(moneda_sel));
  if (parseInt(moneda_sel) == 2 && parseInt(moneda_id) == 1) {
    total_pagado = parseFloat(monto_validar * monto_cambio);
    $('#txt_ventavehiculo_mon').autoNumeric('set', total_pagado);
    console.log('dolar a soles monto validar: ' + monto_validar + ' cambio: ' + monto_cambio);
  } else if (parseInt(moneda_sel) == 1 && parseInt(moneda_id) == 2) {
    total_pagado = parseFloat(monto_validar / monto_cambio);
    $('#txt_ventavehiculo_mon').autoNumeric('set', total_pagado);
    console.log('soles a dolares monto validar: ' + monto_validar + ' cambio: ' + monto_cambio);
  } else
    $('#txt_montoDepositoValido').autoNumeric('set', monto_validar);

  setTimeout(() => { 
    $('#txt_montoDepositoValido').autoNumeric('set', $('#txt_ventavehiculo_mon').val());
    if( parseFloat(total_pagado) > 0) { $('#txt_montoDepositoValido').closest('.form-group').removeClass('has-error').addClass('has-success'); }
  }, 1300);

  $('#txt_ventavehiculo_mon').val($("#txt_montoDepositoValido").val()); // SE IGUALA EL MONTO OBTENIDOA AL MONTO DEPOSITO VALIDO

  var cuenta = $(this).find("option:selected").text();
  $('#hdd_cuenta_dep').val(cuenta);
  $('#hdd_mon_iddep').val(moneda_sel);

  if( buscarSubcadena($('#hdd_cuenta_dep').val(),"BCP") == true )
  {
    $('#cmb_banco_id').val(1);
    $('#cmb_banco_id').closest('.form-group').removeClass('has-warning').addClass('has-success');
  }
  else if( buscarSubcadena($('#hdd_cuenta_dep').val(),"Interbank") == true  )
  {
    $('#cmb_banco_id').val(2);
    $('#cmb_banco_id').closest('.form-group').removeClass('has-warning').addClass('has-success');
  }
  else if( buscarSubcadena($('#hdd_cuenta_dep').val(),"BBVA") == true  )
  {
    $('#cmb_banco_id').val(3);
    $('#cmb_banco_id').closest('.form-group').removeClass('has-warning').addClass('has-success');
  }
  else
  {
    $('#cmb_banco_id').val(4);
    return 0;
  }
  
});

function buscarSubcadena(cadena, subcadena) {
  var posicion = cadena.indexOf(subcadena);
  if (posicion !== -1) {
      return true;
  } else {
      return false;
  }
}

function cambio_moneda(monid) {
  var monedaconvers = $.trim($("#cmb_moneda_id_conversion").val());
  var tipo = '';
  
  if(monedaconvers == 1 && monid == 2) {
    tipo = 'venta';
  }
  else if(monedaconvers == 2 && monid == 1) {
    tipo = 'compra';
  }
  else
  {
    tipo = 'venta';
  }

  console.log(monid);
  console.log($('#txt_ventavehiculo_fec').val());
  console.log(monedaconvers);
  console.log(tipo);
  console.log('TC valido: ' +tipo + ' | conversion: '+monedaconvers+' | moneda padre: '+monid);
  
  $.ajax({
    type: "POST",
    url: VISTA_URL + "monedacambio/monedacambio_controller.php",
    async: false,
    dataType: "json",
    data: ({
      action: "obtener_cambio",
      fecha: $('#txt_ventavehiculo_fec').val(),
      tipo:   tipo
    }),
    beforeSend: function () {
    },
    success: function (data) {
      console.log(data)
      $('#txt_ventavehiculo_tipcam').val(data.moncam_val);
    },
    complete: function () {

    }
  });
}

function metodo_mostrar_ocultar() {
  $("#cmb_metodo_pago").change(function (event) {
    var metodo = parseInt($(this).val());
    if (metodo == 2) {
      // $("#precio_ofi").hide(200);
      $("#fragment_transferencia").show(200);
      $("#fragment_alcredito").hide(200);
    }
    else if(metodo == 3){
      $("#fragment_alcredito").show(200);
      $("#fragment_transferencia").hide(200);
      $("#txt_credito_id").focus();
    }
    else {
      // Con depósito solo para clientes externos
      // $("#precio_ofi").show(200);
      $("#fragment_transferencia").hide(200);
      $("#fragment_alcredito").hide(200);
    }
  });
}

function buscarCredito(){
  var tipo_credito = 3; // $("#cmb_tipo_credito").val();
  var id_credito   = $("#txt_credito_id").val();
  
  if( $.trim(tipo_credito) == "" ){
    swal_warning('AVISO', "Debe seleccionar el tipo del crédito.", 5000);
    return false;
  }
  else if( $.trim(id_credito) == "" ){
    swal_warning('AVISO', "Debe ingresar el ID del crédito.", 5000);
    return false;
  }
  else
  {
    var stockunidad_placa = $("#hdd_numero_placa").val();
    var stockunidad_id_cliente = $("#hdd_cliente_id").val();
    var validacion_placa = '';
    var validacion_clienteid = '';

    // console.log('antiguo: '+stockunidad_placa);
    // console.log('antiguo: '+stockunidad_id_cliente);

    $.ajax({
      type: "POST",
      url: VISTA_URL + "stockunidad/stockunidad_controller.php",
      async: true,
      dataType: "json",
      data: {
        tipo_credito: tipo_credito,
        id_credito: id_credito,
        vista: "stockunidad",
        action: "consultar_datos"
      },
      beforeSend: function () {
        $("#h3_modal_title").text("Cargando Formulario");
        $("#modal_mensaje").modal("show");
      },
      success: function (data) {
        $("#modal_mensaje").modal("hide");
        if(parseInt(data.estado)==1)
        {
          validacion_placa = $.trim(data.cred_placa);
          validacion_clienteid = $.trim(data.cred_cliente_id);

          if(stockunidad_id_cliente != validacion_clienteid)
          {
            swal_warning('AVISO', 'El cliente del crédito id ingresado, no coincide con el cliente que ya solicitó su adelanto, por lo tanto no procede la venta al crédito', 8000);
          }
          else if(stockunidad_placa != validacion_placa)
          {
            swal_warning('AVISO', 'La placa del crédito id ingresado, no coincide con la placa del stock unidad, por lo tanto no procede la venta al crédito', 8000);
          }
          else
          {
            $("#txt_return_monto").val(data.monto_preaco);
            $("#txt_ventavehiculo_mon").val(data.monto_preaco);
            $("#txt_return_moneda").val(data.moneda);
            $("#cmb_moneda_id_conversion").val(data.moneda); // seteo de moneda del crédito
            $("#getMoneda").empty().append(data.moneda_simbolo);

            swal_success('CORRECTO', data.mensaje, 3000);
          }

          // console.log('nuevo: '+validacion_placa);
          // console.log('nuevo: '+validacion_clienteid);

        }
        else
        {
          $("#txt_credito_id").val('');
          $("#txt_return_monto").val(data.monto_preaco);
          $("#txt_return_moneda").val(0);
          $("#getMoneda").empty().append(data.moneda_simbolo);
          swal_warning('AVISO', data.mensaje, 5000);
        }
      },
      complete: function (data) {},
      error: function (data) {
        $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
        $("#overlay_modal_mensaje").removeClass("overlay").empty();
        $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
        console.log(data.responseText);
      },
    });
  }
}

function cliente_form(usuario_act, cliente_id) {
  var getClienteID = $("#hdd_cliente_id").val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: getClienteID,
      vista: "stockunidad",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "cliente";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

function llenar_campos_cliente(data) {
  //obtener el valor de un array (primero referencia la data y luego referencia el campo)
  $("#hdd_cliente_id").val(data.cliente_id);
  $("#txt_cliente_id").val(data.cliente_doc + "-" + data.cliente_nom);
  disabled($("#btn_agr"));
  disabled($("#txt_cliente_id"));
}

function preparar_momento2_form(credito_id) {
  var fecha_hoy = $("#txt_fecha_ref").val();

  $.confirm({
    title: "Fecha de Liquidación",
    content:
      "" +
      '<form action="" class="formName">' +
      '<div class="form-group">' +
      "<label>Ingresa tu Fecha Aquí:</label>" +
      '<input type="date" class="name form-control" required />' +
      "</div>" +
      "</form>",
    buttons: {
      formSubmit: {
        text: "Generar",
        btnClass: "btn-blue",
        action: function () {
          var fecha_liquidacion = this.$content.find(".name").val();
          if (!fecha_liquidacion) {
            $.alert("Debes ingresar una fecha valida");
            return false;
          }
          var tipo_cambio = $("#hdd_tipo_cambio_venta").val();

          $.ajax({
            type: "POST",
            url: VISTA_URL + "liquidacion/liquidacion_formato_form.php",
            async: true,
            dataType: "html",
            data: {
              credito_id: credito_id,
              creditotipo_id: 3, //estamos en la carpeta de garveh, por ende es el 3
              fecha_liquidacion: fecha_liquidacion,
              tipo_cambio: tipo_cambio,
              vista: "stockunidad",
            },
            beforeSend: function () {
              $("#modal_mensaje").modal("show");
            },
            success: function (html) {
              // console.log(html)
              $("#modal_mensaje").modal("hide");
              $("#div_modal_liquidacion").html(html);
              modal_width_auto("modal_liquidacion_formato", 90);
              modal_height_auto("modal_liquidacion_formato");
              $("#modal_liquidacion_formato").modal("show");
              modal_hidden_bs_modal("modal_liquidacion_formato", "limpiar"); //funcion encontrada en public/js/generales.js              
            },
            complete: function () {
              //credito_tabla();
            },
          });
        },
      },
      Cancelar: function () {
        //close
      },
    },
    onContentReady: function () {
      // bind to events
      var jc = this;
      this.$content.find("form").on("submit", function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger("click"); // reference the button and click it
      });
    },
  });
}

function registra_preparar_segundo_momento() {
  var credito_id          = $("#hdd_credito_id").val()
  var tipo_id             = parseInt($("#hdd_tipo_id").val());
  var numero_placa        = $("#hdd_vehiculo_placa").val();
  var txt_credito_cuoven2 = Number($("#txt_credito_cuoven1").autoNumeric("get"));
  var txt_credito_cap2    = Number($("#txt_credito_cap1").autoNumeric("get"));
  var txt_credito_gasto2  = Number($("#txt_credito_gasto1").autoNumeric("get"));
  var vista               = "stockunidad";
  var moneda_credito_id   = $("#hdd_credito_moneda_id").val()

  console.log(txt_credito_cuoven1)
  console.log(txt_credito_cap1)
  console.log(txt_credito_gasto1)
  console.log(moneda_credito_id)

  $.confirm({
    title: "¡Advertencia!",
    content: "<b>¿Está seguro que desea guardar los datos?</b>",
    type: "orange",
    escapeKey: "close",
    backgroundDismiss: true,
    columnClass: "small",
    buttons: {
      Confirmar: {
        btnClass: "btn-orange",
        action: function () {
          $.ajax({
            type: "POST",
            url: VISTA_URL + "stockunidad/stockunidad_controller.php",
            async: true,
            dataType: "json",
            data: {
              action: "preparar_registrar_momento02",
              vista: vista,
              hdd_credito_id: credito_id,
              hdd_tipo_id: 3, //estamos en la carpeta de garveh, por ende es el 3
              hdd_numero_placa: numero_placa,
              txt_credito_cuoven2: txt_credito_cuoven2,
              txt_credito_cap2: txt_credito_cap2,
              txt_credito_gasto2: txt_credito_gasto2,
              txt_moneda_credito_id: moneda_credito_id
            },
            beforeSend: function () {
              // $('#stockunidad_mensaje').hide(300);
              console.log("cagando...");
            },
            success: function (data) {
              console.log(data);

              if (parseInt(data.estado) > 0) {
                $("#modal_liquidacion_formato").modal("hide");
                notificacion_success(""+data.mensaje,5000);
              } else {
                alerta_warning("Advertencia", data.mensaje);
              }
            },
            complete: function (data) {
              console.log(data);
            },
            error: function (data) {
              $("#stockunidad_mensaje_tbl").removeClass("callout-info").addClass("callout-danger");
              $("#stockunidad_mensaje_tbl").html("ALERTA DE ERROR: " + data.responseText);
              alerta_error("Advertencia", "" + data.responseText);
            },
          });
        },
      },
      cancelar: function () {},
    },
  });
}

$("#btnGuardarMomento02").click(function() {
  registra_preparar_segundo_momento();
});

function validar_no_operacion_fecha() {

  var fecha_deposito = $('#txt_fecha_deposito').val();
  var num_operacion = $('#txt_numoperacion').val();
  var fecha_vencimiento = $('#hidden_fecha_deposito').val(); // no existe
  var numero_cuota = $('#hdd_cuo_num').val()
  if(num_operacion == ""){
      swal_warning('AVISO', "DEBE REGISTRAR NUMERO DE OPERACION", 6000);
      $('#txt_fecha_deposito').val('')
      return false;
  }
  $.ajax({
      type: "POST",
      url: VISTA_URL + "vencimiento/vencimiento_funciones.php",
      async: true,
      dataType: "json",
      data: ({
          funcion: "validar_operacion_fecha",
          fecha_deposito: fecha_deposito,
          num_operacion: num_operacion,
          fecha_vencimiento: null,
          numero_cuota: 0
      }),
      success: function (data) {
          // console.log(data);
          if(data.estado == 0){
              swal_warning('AVISO', data.mensaje, 6000);
              $('#txt_fecha_deposito').val('');
              
              $('#txt_montodeposito').val('');
              $('#txt_comisionbanco').val('');
              $('#txt_montoDepositoValido').val('');
              $('#cmb_banco_id').val('');
              $('#cmb_cuedep_id').val('');
              $('#txt_numoperacion, #txt_fecha_deposito, #cmb_cuedep_id, #txt_montodeposito, #cmb_banco_id').closest('.form-group').removeClass('has-success').addClass('has-error');
              $('#txt_comisionbanco, #txt_montoDepositoValido').closest('.form-group').removeClass('has-success').addClass('has-warning');
          }
          else {
              notificacion_success('N° operación y fecha correctos', 6000);
              $('#cmb_cuedep_id').val(data.cuentaid).change();
              $('#txt_numoperacion, #cmb_cuedep_id, #txt_fecha_deposito, #txt_montodeposito, #cmb_banco_id').closest('.form-group').removeClass('has-error').addClass('has-success');
              $('#txt_comisionbanco, #txt_montoDepositoValido').closest('.form-group').removeClass('has-error').addClass('has-warning');
              
              validar_saldo_utilizar();
          }
          
          $('#hdd_var_op').val(data.mismo_mes) //nos permite mantener la misma fecha para crear las cuotas

          $("#txt_montodeposito").focus();

      },
      complete: function (data) {
        // console.log(data)
      }
  });
}

$(function () {
  $(document).bind("contextmenu", function (e) {
      return false;
  });
});

$(document).keydown(function (event) {
  if (event.keyCode == 123) { // Prevent F12
      return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
      return false;
  }
});

function validar_saldo_utilizar(){
  var concatsimbolo = '';
  $.ajax({
    type: "POST",
    url: VISTA_URL +"stockunidad/stockunidad_validacion_salgo_utilizar.php",
    async:true,
    dataType: "JSON",
    data: ({
      action: "consultar",
      numoperacion: $("#txt_numoperacion").val(),
      fecha_deposito: $("#txt_fecha_deposito").val(),
      moneda_id_padre: $("#hdd_mon_iddep").val()
    }),
    beforeSend: function() {
      console.log('validando...')
    },
    success: function(data){
      console.log(data);
      // $('#txt_montodeposito').val(parseFloat(data.mensaje).toFixed(2));
      if( parseInt($("#hdd_mon_iddep").val()) == 1 ) { concatsimbolo = "S/ "; } else { concatsimbolo = "$ "; }
      alerta_warning("Información",data.mensaje);
      $("#txt_montodeposito").val(data.monto_usar);
    },
    complete: function(data){
      // console.log(data);
    }
  });
}