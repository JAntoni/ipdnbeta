<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once("../../core/usuario_sesion.php");
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../permiso/Permiso.class.php');
    require_once('../stockunidad/Stockunidad.class.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../egreso/Egreso.class.php');
    require_once('../deposito/Deposito.class.php');
    require_once('../ventavehiculo/Ventavehiculo.class.php');
    require_once('../historial/Historial.class.php');
    require_once('../cliente/Cliente.class.php');

    $oPermiso     = new Permiso();
    $oStockunidad = new Stockunidad();
    $oIngreso     = new Ingreso();
    $oEgreso      = new Egreso();
    $oDeposito    = new Deposito();
    $oVenta       = new Ventavehiculo();
    $oHist        = new Historial();
    $oCliente     = new Cliente();

    $action       = $_POST['action'];
    $arrayResultado = array();

    if ($action == 'anular') {
        $fecha_actual   = date("Y-m-d");
        $placa          = $_POST['placa'];
        $stockunidad_id = $_POST['stock_id'];
        $result = $oStockunidad->mostrarUno($stockunidad_id);
        $resultValidarExisteVenta = $oVenta->anularVenta_validar_exite_venta($placa, $stockunidad_id);
        if ($resultValidarExisteVenta["estado"] == true) {
            // -------------- CONSULTAR SI HAY SEPARACIÓN -------------- //
            $validacion_moneda_padre = $result['data']['tb_moneda_id'];
            $result              = null;
            $suma_total_ingresos = 0;
            $mod_id              = 62;   // INGRESO POR SEPARACIÓN DE VEHICULO
            $consultarDatosIngresosPrevios = $oIngreso->mostrar_por_modulo($mod_id, $stockunidad_id, 0, 0);
            if ($consultarDatosIngresosPrevios['estado'] == 1) {
                foreach ($consultarDatosIngresosPrevios['data'] as $key => $value2) {
                    if ($validacion_moneda_padre == 1) {
                        if ($value2['tb_moneda_id'] == 1) {
                            $suma_total_ingresos += formato_numero($value2['tb_ingreso_imp']);
                        } elseif ($value2['tb_moneda_id'] == 2) {
                            $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] * $value2['tb_ingreso_tipcam']));
                        } else {
                            $suma_total_ingresos += 0;
                        }
                    } elseif ($validacion_moneda_padre == 2) {
                        if ($value2['tb_moneda_id'] == 1) {
                            $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] / $value2['tb_ingreso_tipcam']));
                        } elseif ($value2['tb_moneda_id'] == 2) {
                            $suma_total_ingresos += formato_numero($value2['tb_ingreso_imp']);
                        } else {
                            $suma_total_ingresos += 0;
                        }
                    } else {
                        $suma_total_ingresos = 0;
                    }
                }
            }
            $consultarDatosIngresosPrevios = null;
            // -------------- CONSULTAR SI HAY SEPARACIÓN -------------- //
            $ventavehiculo_mon = formato_numero($resultValidarExisteVenta["data"]["tb_ventavehiculo_mon"]);
            $suma_validar = formato_numero(($ventavehiculo_mon + $suma_total_ingresos));
            if ($suma_validar > 0) {
                $arrayResultado['estado'] = 0;
                $arrayResultado["mensaje"] = 'Existen ingresos previos y una venta registrada que deben anularse primero. <br>N° de Placa: ' . $placa . ' <br>Fecha de Venta: ' . $resultValidarExisteVenta["data"]["tb_ventavehiculo_fec"] . ' <br>Monto: ' . $resultValidarExisteVenta["data"]["tb_ventavehiculo_mon"];
            } else {
                if ($oVenta->anularVenta_anular($stockunidad_id)) {
                    $arrayResultado['estado'] = 1;
                    $arrayResultado["mensaje"] = 'Venta anulada correctamente.';
                }
            }
        } else {
            $arrayResultado['estado']  = 0;
            $arrayResultado["mensaje"] = 'No se encotró ninguna venta asociada, consulte con soporte técnico.';
        }
        $resultValidarExisteVenta = null;

        echo json_encode($arrayResultado);
        exit();
    } elseif ($action == 'ajustar_validar') {
        $credito_id     = $_POST["hdd_credito_id"];
        $creditotipo_id = $_POST["hdd_tipo_id"];
        $resultado = $oStockunidad->ajustar_consultar_existe($credito_id, $creditotipo_id);
        if ($resultado['estado'] == 1) {
            $arrayResultado['estado']  = 1;
            $arrayResultado["mensaje"] = $resultado['mensaje'];
            $arrayResultado['datos']   = $resultado['data'];
        } else {
            $arrayResultado['estado']  = 0;
            $arrayResultado["mensaje"] = "Sin resultados";
        }
        $resultado = null;
        echo json_encode($arrayResultado);
        exit();
    } elseif ($action == 'ajustar_update') {
        $jsonDetalle = $_POST["p_detalle"];
        parse_str($_POST["p_array_datos"], $datosFrm);
        $credito_id                  = $datosFrm["hdd_credito_id"];
        $creditotipo_id              = $datosFrm["hdd_tipo_id"];
        $stockunidad_montoresolucion = $datosFrm["txt_stockunidad_montoresolucion"];
        $stockunidad_monbas          = formato_numero($datosFrm["txt_stockunidad_monbas"]);
        $moneda_id                   = $datosFrm["cmb_moneda_id"];
        $stockunidad_soatval         = $datosFrm["txt_stockunidad_soatval"];
        $stockunidad_revtecval       = $datosFrm["txt_stockunidad_revtecval"];
        $stockunidad_kil             = $datosFrm["txt_stockunidad_kil"];
        $stockunidad_tarjeta         = $datosFrm["cmb_stockunidad_tarjeta"];
        $json_detalle                = $_POST["p_detalle"];

        $ofrecerCredito             = $datosFrm["cmb_ofrecercred"];

        $appendDetalle = "";
        $arrayDataDetalle = json_decode($json_detalle);
        foreach ($arrayDataDetalle as $key => $value) {
            $appendDetalle .= $value->descripcion . '/&/&/';
        }
        // $appendDetalle = substr($appendDetalle,0,-5);
        $resultado = $oStockunidad->ajustar_consultar_existe($credito_id, $creditotipo_id);
        if ($oStockunidad->ajustar_actualizar($credito_id, $creditotipo_id, $stockunidad_soatval, $stockunidad_revtecval, $stockunidad_kil, $stockunidad_tarjeta, $appendDetalle, $moneda_id, $stockunidad_monbas, $ofrecerCredito) == true) {
            $arrayResultado['estado'] = 1;
            $arrayResultado["mensaje"] = "Proceso de ajuste realizado correctamente";
        } else {
            $arrayResultado['estado'] = 0;
            $arrayResultado["mensaje"] = "Se presentó un inconveniente al momento de realizar el proceso de ajuste: Actualizar información. <br> Error SQL: StockUnidad.class :: ajustar_actualizar() :: linea: 500";
        }
        $resultado = null;
        echo json_encode($arrayResultado);
        exit();
    } elseif ($action == 'preparar_registrar') {
        $credito_id       = intval($_POST["hdd_credito_id"]);
        $creditotipo_id   = intval($_POST["hdd_tipo_id"]);
        $creditomoneda_id = intval($_POST["txt_moneda_credito_id"]);
        $credito_vehpla   = trim($_POST["hdd_numero_placa"]);
        $credito_cuoven   = formato_moneda($_POST["txt_credito_cuoven1"]);
        $credito_cap      = formato_moneda($_POST["txt_credito_cap1"]);
        $credito_gasto    = formato_moneda($_POST["txt_credito_gasto1"]);
        $montoresolucion  = formato_numero($credito_cuoven + $credito_cap + $credito_gasto);
        $vista            = trim($_POST["vista"]);

        $oStockunidad->tb_credito_id       = $credito_id;
        $oStockunidad->tb_creditotipo_id   = $creditotipo_id;
        $oStockunidad->tb_creditomoneda_id = $creditomoneda_id;
        $oStockunidad->tb_credito_vehpla   = $credito_vehpla;
        $oStockunidad->tb_credito_cuoven1  = $credito_cuoven;
        $oStockunidad->tb_credito_cap1     = $credito_cap;
        $oStockunidad->tb_credito_gasto1   = $credito_gasto;

        if ($vista = 'gasto') {
            $consultarExisteData = $oStockunidad->ajustar_consultar_existe($credito_id, $creditotipo_id);  // VALIDAMOS QUE EXISTA 
            if ($consultarExisteData["estado"] == 0) // NO -> REGISTRAR
            {
                if ($oStockunidad->preparar_insertar() == true) {
                    $arrayResultado['estado'] = 1;
                    $arrayResultado["mensaje"] = "Proceso de preparacion registrados correctamente";
                } else {
                    $arrayResultado['estado'] = 0;
                    $arrayResultado["mensaje"] = "Se presentó un inconveniente al momento de realizar el proceso de prepación: Registrar. <br> Error SQL: StockUnidad.class :: preparar_insertar() :: linea: 20";
                }
            } else                                      // SI - ACTUALIZAR
            {
                if ($oStockunidad->preparar_actualizar01() == true) {
                    $arrayResultado['estado'] = 1;
                    $arrayResultado["mensaje"] = "Proceso de preparacion actualizados correctamente";
                } else {
                    $arrayResultado['estado'] = 0;
                    $arrayResultado["mensaje"] = "Se presentó un inconveniente al momento de realizar el proceso de prepación: Actualizar. <br> Error SQL: StockUnidad.class :: preparar_actualizar01() :: linea: 53";
                }
            }
            $consultarExisteData = null;
        } else {
            $arrayResultado["estado"]  = 0;
            $arrayResultado["mensaje"] = "Vista invalida";
            echo json_encode($arrayResultado);
            exit();
        }

        echo json_encode($arrayResultado);
        exit();
    } elseif ($action == 'preparar_registrar_momento02') {
        $credito_id       = intval($_POST["hdd_credito_id"]);
        $creditotipo_id   = intval($_POST["hdd_tipo_id"]);
        $creditomoneda_id = intval($_POST["txt_moneda_credito_id"]);
        $credito_vehpla   = trim($_POST["hdd_numero_placa"]);
        $credito_cuoven   = formato_moneda($_POST["txt_credito_cuoven2"]);
        $credito_cap      = formato_moneda($_POST["txt_credito_cap2"]);
        $credito_gasto    = formato_moneda($_POST["txt_credito_gasto2"]);
        $vista            = trim($_POST["vista"]);

        $oStockunidad->tb_credito_id       = $credito_id;
        $oStockunidad->tb_creditotipo_id   = $creditotipo_id;
        $oStockunidad->tb_creditomoneda_id = $creditomoneda_id;
        $oStockunidad->tb_credito_vehpla   = $credito_vehpla;
        $oStockunidad->tb_credito_cuoven2  = $credito_cuoven;
        $oStockunidad->tb_credito_cap2     = $credito_cap;
        $oStockunidad->tb_credito_gasto2   = $credito_gasto;

        if ($oStockunidad->preparar_actualizar02() == true) {
            $arrayResultado['estado'] = 1;
            $arrayResultado["mensaje"] = "Datos de ejecución guardados correctamente";
        } else {                              // SI - ACTUALIZAR
            $arrayResultado['estado'] = 0;
            $arrayResultado["mensaje"] = "Se presentó un inconveniente al momento de guadar los datos de ejecución en el segundo momento. <br> Error SQL: StockUnidad.class :: preparar_actualizar02() :: linea: 82";
        }
        echo json_encode($arrayResultado);
        exit();
    } elseif ($action == 'consultar_datos') {
        $vista            = $_POST['vista'];
        $tipo_credito     = $_POST['tipo_credito'];
        $id_credito_nuevo = $_POST['id_credito'];
        $data             = array();
        $array_creditos   = [3];

        if(in_array($tipo_credito, $array_creditos)) {
            require_once("../creditogarveh/Creditogarveh.class.php");
            $oCredito = new Creditogarveh();

            $result = $oCredito->mostrarUno($id_credito_nuevo);
                if ($result['estado'] == 1) {
                    $monto_preaco = mostrar_moneda($result['data']['tb_credito_preaco']);
                    $moneda       = $result['data']['tb_moneda_id'];
                    if($moneda==1) $simbolo = 'S/'; else $simbolo = 'US $';

                    $data['estado']          = 1;
                    $data['moneda']          = $moneda;
                    $data['moneda_simbolo']  = $simbolo;
                    $data['monto_preaco']    = $monto_preaco;
                    $data['cred_cliente_id'] = $result['data']['tb_cliente_id'];
                    $data['cred_placa']      = $result['data']['tb_credito_vehpla'];
                    $data['mensaje']         = 'Crédito ID y validaciones de placa, cliente y monto, son correctas';
                }
                else
                {
                    $data['estado']          = 0;
                    $data['moneda']          = 0;
                    $data['moneda_simbolo']  = '<i class="fa fa-coins"></i>';
                    $data['monto_preaco']    = mostrar_moneda(0);
                    $data['cred_cliente_id'] = 0;
                    $data['cred_placa']      = '-';
                    $data['mensaje']         = 'El ID Crédito consultado, no se ha encontrado';
                }
            $result = null;

            echo json_encode($data);
        }
        else
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'El tipo de crédito ingresado es inválido a consultar.';
            echo json_encode($data);
            exit();
        }
    } elseif ($action == 'vender_registrar') {
        $validarMetodosPagos            = array(1, 2, 3);
        $validarMonedas                 = array(1, 2);
        $validation                     = false;
        $empresa_id                     = $_SESSION['empresa_id'];
        parse_str($_POST["p_array_datos"], $datosFrm);
        $stockunidad_id                 = $datosFrm["hdd_stockunidad_id"];
        $vehiculo_pla                   = $datosFrm["hdd_numero_placa"];
        $credito_id                     = $datosFrm["hdd_credito_id"];
        $creditotipo_id                 = $datosFrm["hdd_creditotipo_id"];
        $ventavehiculo_fec              = fecha_mysql($datosFrm["txt_ventavehiculo_fec"]);
        $ventavehiculo_tipcam           = $datosFrm["txt_ventavehiculo_tipcam"];
        $ventavehiculo_bas              = moneda_mysql($datosFrm["txt_ventavehiculo_bas"]);
        $moneda_id_padre_descrip        = $datosFrm["cmb_moneda_id_padre"];
        $moneda_id_padre_int            = $datosFrm["hdd_moneda_id_padre"];
        $moneda_id_conversion           = $datosFrm["cmb_moneda_id_conversion"];
        $descripMonedaConver            = "";
        $cliente_id                     = $datosFrm["hdd_cliente_id"];                       // txt_cliente_id --> name
        $usuario_id2_name               = $datosFrm["txt_usuario_id2"];                      // ASESOR NAME
        $usuario_id2_id                 = $datosFrm["hdd_usuario_id2"];                      // ASESOR ID
        $usuario_id_name                = $datosFrm["txt_usuarioautoriza_id"];               // USUARIO NAME
        $usuario_id                     = $datosFrm["hdd_usuarioautoriza_id"];               // USUARIO ID
        $metodo_pago                    = $datosFrm["cmb_metodo_pago"];
        $descripMetodoPago              = "";
        $monto_previos                  = moneda_mysql($datosFrm["txt_monto_previos"]);
        $saldo_a_pagar                  = moneda_mysql($datosFrm["txt_saldo_a_pagar"]);
        $ventavehiculo_mon              = moneda_mysql($datosFrm["txt_ventavehiculo_mon"]);
        $suma_ing_prev_mont_pagar       = moneda_mysql(($monto_previos + $ventavehiculo_mon)); // $ventavehiculo_bas
        $set_tbcuentadepositoid         = 0;
        $validacion_egreso_subcuenta_id = 0;

        $fecha_deposito       = "";
        $banco_id             = 0;
        $cuedep_id            = 0;
        $numoperacion         = "";
        $montodeposito        = 0;
        $combanco             = 0;
        $montopag             = 0;
        $subaction            = "";
        $monto_sobre_costo    = 0;  // $monto_sobre_costo = formato_numero(($ventavehiculo_mon - $ventavehiculo_bas));
        $mensaje              = "";
        $mensajeIngresoEgreso = "";
        $ventavehiculo_xac    = 1;  // 1 = VENDIDO || 0 = POR COBRAR
        $add_monto_conversión = "";
        $monto_conversion_a_soles = 0.00;
        $nuevo_credito_id = 0;

        if (in_array($metodo_pago, $validarMetodosPagos)) {
            if (in_array($moneda_id_conversion, $validarMonedas)) {
                if (empty($ventavehiculo_mon) || $ventavehiculo_mon <= 0) {
                    $arrayResultado['estado']  = 0;
                    $arrayResultado["mensaje"] = 'Monto a pagar inválido: ' . $ventavehiculo_mon;
                    echo json_encode($arrayResultado);
                    exit();
                } elseif (empty($ventavehiculo_tipcam) || $ventavehiculo_tipcam <= 0) {
                    $arrayResultado['estado']  = 0;
                    $arrayResultado["mensaje"] = 'Tipo de cambio inválido: ' . formato_moneda($ventavehiculo_tipcam);
                    echo json_encode($arrayResultado);
                    exit();
                } else {

                    if ($moneda_id_conversion == 1) {
                        $descripMonedaConver            = "SOLES";
                        $set_tbcuentadepositoid         = 18; // CUENTA VENTA VEHICULAR SOLES
                        $validacion_egreso_subcuenta_id = 71;
                    } else {
                        $descripMonedaConver            = "DOLARES";
                        $set_tbcuentadepositoid         = 19; // CUENTA VENTA VEHICULAR DOLARES
                        $validacion_egreso_subcuenta_id = 72;
                    }

                    if ($moneda_id_padre_int == 1) {
                        if ($moneda_id_conversion == 1) {
                            $monto_conversion_a_soles = $ventavehiculo_mon;
                            $add_monto_conversión = "";
                        } else {
                            $monto_conversion_a_soles = formato_moneda($ventavehiculo_tipcam * $ventavehiculo_mon);
                            $add_monto_conversión = "<br>Monto convertido a Soles: ".mostrar_moneda($monto_conversion_a_soles);
                        }
                    }
                    else{
                        if ($moneda_id_conversion == 1) {
                            $monto_conversion_a_soles = formato_moneda($ventavehiculo_tipcam / $ventavehiculo_mon);
                            $add_monto_conversión = "<br>Monto convertido a Soles: ".mostrar_moneda($monto_conversion_a_soles);
                        } else {
                            $monto_conversion_a_soles = $ventavehiculo_mon;
                            $add_monto_conversión = "";
                        }
                    }

                    $monto_conversion_a_soles = formato_moneda($monto_conversion_a_soles);

                    $consultar_datos_cliente = $oCliente->mostrarUno($cliente_id);
                    $datosCliente_nombre_completo = $consultar_datos_cliente['data']['tb_cliente_nom'];                    

                    if ($metodo_pago == 1) {
                        $descripMetodoPago = "EFECTIVO";
                        $mensajeIngresoEgreso = 'Registro por separación: <br>Placa del vehículo: ' . $vehiculo_pla . ', Monto abonado: ' . $ventavehiculo_mon . ', Tipo de cambio: ' . $ventavehiculo_tipcam . ', Moneda: ' . $descripMonedaConver . ', Método de pago: ' . $descripMetodoPago;
                        $mensaje = 'Registro por separación: <br>Cliente: ' . $datosCliente_nombre_completo . ' <br>Placa del vehículo: ' . $vehiculo_pla . '<br>Tipo de cambio: ' . $ventavehiculo_tipcam . '<br>Monto abonado: ' . mostrar_moneda($ventavehiculo_mon) . '<br>Moneda: ' . $descripMonedaConver . $add_monto_conversión . '<br>Método de pago: ' . $descripMetodoPago;
                        $validation = true;
                    }
                    elseif ($metodo_pago == 2) {
                        $descripMetodoPago = "TRANSFERENCIA BANCARIA";
                        $descripBanco_id         = "";
                        $fecha_deposito          = $datosFrm["txt_fecha_deposito"];
                        $banco_id                = $datosFrm["cmb_banco_id"];
                        $descripValidatorBancoId = $datosFrm["hdd_cuenta_dep"];
                        $cuedep_id               = $datosFrm["cmb_cuedep_id"];
                        $numoperacion            = $datosFrm["txt_numoperacion"];
                        $montodeposito           = $datosFrm["txt_montodeposito"];
                        $combanco                = $datosFrm["txt_comisionbanco"];
                        $montopag                = $datosFrm["txt_montoDepositoValido"];

                        if (empty($numoperacion) || empty($fecha_deposito) || empty($cuedep_id) || empty($montodeposito) || empty($montopag)) {
                            $arrayResultado['estado']  = 0;
                            $arrayResultado["mensaje"] = 'Debe ingresar los datos requeridos de la opción seleccionada: transferencia bancaria';
                            echo json_encode($arrayResultado);
                            exit();
                        } elseif (empty($banco_id)) {
                            $arrayResultado['estado']  = 0;
                            $arrayResultado["mensaje"] = 'Debe seleccionar el <b>banco</b> para el número de cuenta:<br>' . $descripValidatorBancoId;
                            echo json_encode($arrayResultado);
                            exit();
                        } else {
                            if (empty($combanco)) { $combanco = formato_moneda(0); }
                            if ($banco_id == 1) { $descripBanco_id = "BCP"; } elseif ($banco_id == 2) { $descripBanco_id = "Interbank"; } else { $descripBanco_id = "BBVA"; }
                            $mensajeIngresoEgreso = 'Registro por separación: <br>Placa del vehículo: ' . $vehiculo_pla . ', Monto abonado: ' . $ventavehiculo_mon . ', Tipo de cambio: ' . $ventavehiculo_tipcam . ', Moneda: ' . $descripMonedaConver . ', Método de pago: ' . $descripMetodoPago . ', Fecha de deposito: ' . mostrar_fecha($fecha_deposito) . ', Banco: ' . $descripBanco_id . ', N° Operación: ' . $numoperacion . ', Monto Depósito: ' . mostrar_moneda($montodeposito) . ', Comisión del banco: ' . $combanco;
                            $mensaje = 'Registro por separación: <br>Cliente: ' . $datosCliente_nombre_completo . ' <br>Placa del vehículo: ' . $vehiculo_pla . '<br>Tipo de cambio: ' . $ventavehiculo_tipcam . '<br>Monto abonado: ' . mostrar_moneda($ventavehiculo_mon) . '<br>Moneda: ' . $descripMonedaConver . $add_monto_conversión . '<br>Método de pago: ' . $descripMetodoPago . '<ul><li>Fecha de deposito: ' . mostrar_fecha($fecha_deposito) . '</li><li>Banco: ' . $descripBanco_id . '</li><li>N° Operación: ' . $numoperacion . '</li><li>Monto Depósito: ' . mostrar_moneda($montodeposito) . '</li><li>Comisión del banco: ' . $combanco . '</li></ul>';

                            $deposito_mon = 0;
                            $result = $oDeposito->validar_no_operacion_fecha($numoperacion, $fecha_deposito);
                                if ($result['estado'] == 1) {
                                    foreach ($result['data'] as $key => $value) {
                                        $deposito_mon += floatval($value['tb_deposito_mon']);
                                    }
                                }
                            $result = NULL;

                            if ($deposito_mon == 0) {
                                $arrayResultado['estado']  = 0;
                                $arrayResultado["mensaje"] = 'El número de operación que ingresaste no se encuentra asociado a la cuenta seleccionada o la fecha de depósito no corresponde, consulta con administración por favor.';
                                echo json_encode($arrayResultado);
                                exit();
                            }

                            $deposito_mon = formato_numero($deposito_mon);

                            $ingreso_importe = 0;
                            $dts = $oIngreso->lista_ingresos_num_operacion($numoperacion); //OBTIENE TODOS LOS INGRESOS USANDO EL NUMERO DE OPERACION, ES DECIR EL SALDO USADO
                                if ($dts['estado'] == 1) {
                                    foreach ($dts['data'] as $key => $dt) {
                                        $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
                                        $ingreso_mon_id = intval($dt['tb_moneda_id']);
                                        if ($moneda_id_padre_int == 1)  // SOLES
                                        {
                                            if ($ingreso_mon_id == 1) {
                                                $ingreso_importe += $tb_ingreso_imp;
                                            } elseif ($ingreso_mon_id == 2) { // DÓLARES - SOLES                                    
                                                $tb_ingreso_imp = ($tb_ingreso_imp * $ventavehiculo_tipcam);
                                            } else {
                                                $ingreso_importe += 0;
                                            }
                                        }
                                        elseif ($moneda_id_padre_int == 2) // DÓLARES
                                        {
                                            if ($ingreso_mon_id == 1) // SOLES - DÓLARES
                                            {
                                                $tb_ingreso_imp = ($tb_ingreso_imp / $ventavehiculo_tipcam);
                                            } elseif ($ingreso_mon_id == 2) {
                                                $ingreso_importe += $tb_ingreso_imp;
                                            } else {
                                                $ingreso_importe += 0;
                                            }
                                        } else {
                                            $ingreso_importe = 0;
                                        }
                                    }
                                }
                            $dts = null;

                            $monto_usar = $deposito_mon - $ingreso_importe; // MONTO DISPONIBLE PARA UTILIZAR EN OTRO PAGO
                            $monto_usar = formato_moneda($monto_usar);      // FONDOS DISPONIBLES

                            if (formato_numero($montodeposito) > formato_numero($monto_usar)) //SI EL MONTO DEPOSITO ES MAYOR QUE LOS FONDOS DISPONIBLES LANZA UNA ALERTA Y DETIENE EL PROCESO
                            {
                                $arrayResultado['estado'] = 0;
                                $arrayResultado["mensaje"] = 'El MONTO ingresado es mayor que el MONTO disponible, <br>no puede pagar mas de lo depositado. <br>Monto ingresado: ' . mostrar_moneda($montodeposito) .
                                    ', monto disponible: ' . mostrar_moneda($monto_usar) . ', <br>TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) .
                                    ' | validar: ' . $montodeposito . ' | usar: ' . $monto_usar . ' | cambio: ' . $ventavehiculo_tipcam .
                                    ' | ingre: ' . $ingreso_importe  . ' | mond ing: ' . $moneda_id_conversion;
                                echo json_encode($arrayResultado);
                                exit();
                            }
                        }
                        $validation = true;
                    }
                    else {
                        $nuevo_credito_id = $datosFrm["txt_credito_id"];
                        $monto_credito = mostrar_moneda($datosFrm["txt_return_monto"]);
                        $moneda_credito = intval($datosFrm["txt_return_moneda"]);

                        if(empty($nuevo_credito_id))
                        {
                            $arrayResultado["estado"]  = 0;
                            $arrayResultado["mensaje"] = "En el método de pago: Al Crédito, es necesario ingresar el ID Crédito";
                            echo json_encode($arrayResultado);
                            exit();
                        }
                        elseif($monto_credito <= 0)
                        {
                            $arrayResultado["estado"]  = 0;
                            $arrayResultado["mensaje"] = "Monto del crédito consultado, es inválido.";
                            echo json_encode($arrayResultado);
                            exit();
                        }
                        elseif($moneda_id_conversion != $moneda_credito){
                            $validation = false;
                            $arrayResultado["estado"]  = 0;
                            $arrayResultado["mensaje"] = "Se presentó un inconveniente de validación, la moneda de conversión y la moneda del crédito son diferentes, por favor verifique que sean iguales.";
                            echo json_encode($arrayResultado);
                            exit();
                        }
                        else{
                            if($moneda_credito==1) $descripMonedaConver = "SOLES"; else $descripMonedaConver = "DOLARES";
                            $descripMetodoPago = "AL CRÉDITO";
                            $mensajeIngresoEgreso = '';
                            $mensaje = 'Registro de Venta realizado al crédito <br>ID Crédito: '.$credito_id. '<br>Cliente: ' . $datosCliente_nombre_completo . ' <br>Placa del vehículo: ' . $vehiculo_pla . ' <br>Monto Total: ' . mostrar_moneda($ventavehiculo_bas) . ' <br>Monto del crédito: ' . mostrar_moneda($monto_credito) . ' <br>Monto Ingresos previos: ' . mostrar_moneda($monto_previos) . ' <br>Monto Saldo a Pagar: ' . mostrar_moneda($saldo_a_pagar) . '<br>Tipo de cambio: ' . $ventavehiculo_tipcam . '<br>Moneda: ' . $descripMonedaConver . $add_monto_conversión . '<br>Método de pago: ' . $descripMetodoPago;
                            $validation = true;
                        }
                    }

                    // ---------------------- DATOS HISTORIAL ------------------------- //
                    $oHist->setTbHistUsureg($_SESSION['usuario_id']);
                    $oHist->setTbHistNomTabla('tb_stockunidad');
                    $oHist->setTbHistRegmodid($stockunidad_id);
                    // ---------------------- DATOS HISTORIAL ------------------------- //

                    // ---------------------- DATOS VENTA VEHICULO ------------------------- //

                    $oVenta->ventavehiculo_xac  = $ventavehiculo_xac;
                    $oVenta->ventavehiculo_fec  = $ventavehiculo_fec;
                    $oVenta->vehiculo_pla       = $vehiculo_pla;
                    $oVenta->moneda_id          = $moneda_id_conversion;
                    $oVenta->montobase          = $ventavehiculo_bas;
                    $oVenta->montototal         = $ventavehiculo_mon;
                    $oVenta->tipcam             = $ventavehiculo_tipcam;
                    $oVenta->montosobr          = $monto_sobre_costo;
                    $oVenta->metpag             = $metodo_pago;
                    $oVenta->usuario_id         = $usuario_id;
                    $oVenta->usuario_id2        = $usuario_id2_id;
                    $oVenta->cliente            = $cliente_id;
                    $oVenta->credito_id         = $credito_id;
                    $oVenta->creditotipo_id     = $creditotipo_id;
                    $oVenta->vehiculo_est       = 1;                 // 0 por cobrar - 1 vendido
                    $oVenta->empresa_id         = $empresa_id;
                    $oVenta->stockunidad        = $stockunidad_id;
                    $oVenta->stockunidad_estado = 4;
                    $oVenta->ventavehiculo_crenuevoid = $nuevo_credito_id;

                    // ---------------------- DATOS VENTA VEHICULO ------------------------- //

                    // ---------------------- DATOS INGRESOS ------------------------- //

                    $oIngreso->ingreso_usureg    = intval($_SESSION['usuario_id']);
                    $oIngreso->ingreso_usumod    = intval($_SESSION['usuario_id']);
                    // $oIngreso->ingreso_xac       = 1;
                    $oIngreso->ingreso_fec       = date('Y-m-d');
                    $oIngreso->documento_id      = 8;                // OTROS INGRESOS 
                    $oIngreso->ingreso_numdoc    = 0;                // UPDATE AUTOMARICO
                    $oIngreso->ingreso_det       = 'INGRESO POR: ' . $mensajeIngresoEgreso;
                    $oIngreso->ingreso_imp       = moneda_mysql($ventavehiculo_mon);
                    // $oIngreso->ingreso_est       = 1;
                    $oIngreso->cliente_id        = $cliente_id;
                    $oIngreso->caja_id           = 1; // CAJA
                    $oIngreso->moneda_id         = $moneda_id_conversion;
                    $oIngreso->ingreso_modide    = $stockunidad_id;
                    $oIngreso->empresa_id        = $empresa_id;
                    $oIngreso->ingreso_fecdep    = $fecha_deposito;
                    $oIngreso->ingreso_numope    = $numoperacion;
                    $oIngreso->ingreso_mondep    = $montodeposito;
                    $oIngreso->ingreso_comi      = $combanco;
                    $oIngreso->cuentadeposito_id = $cuedep_id;
                    $oIngreso->banco_id          = $banco_id;
                    $oIngreso->ingreso_ap        = 0;                   // 1 = ACUERDO DE PAGO  | 0 = INGRESO NORMAL

                    // ---------------------- DATOS INGRESOS ------------------------- //

                    // ---------------------- DATOS EGRESOS ------------------------- //

                    $oEgreso->egreso_usureg     = intval($_SESSION['usuario_id']);
                    $oEgreso->egreso_fec        = date('Y-m-d');
                    $oEgreso->documento_id      = 9;                                // OTROS EGRESOS
                    $oEgreso->egreso_numdoc     = 0;                                // UPDATE AUTOMARICO
                    $oEgreso->egreso_det        = 'EGRESO POR: ' . $mensajeIngresoEgreso;
                    $oEgreso->egreso_imp        = moneda_mysql($ventavehiculo_mon);
                    $oEgreso->egreso_tipcam     = 0;                                // TIPO DE CAMBIO DE VENTA ES 0
                    $oEgreso->egreso_est        = 1;
                    $oEgreso->cuenta_id         = 28;                               // POR DEFECTO
                    $oEgreso->subcuenta_id      = $validacion_egreso_subcuenta_id;  // VARIABLE 71 O 72
                    $oEgreso->proveedor_id      = 1;                                // POR DEFECTO
                    $oEgreso->cliente_id        = 0;                                // NO VA
                    $oEgreso->usuario_id        = 0;                                // NO VA
                    $oEgreso->caja_id           = 1;                                // CAJA = 1
                    $oEgreso->moneda_id         = $moneda_id_conversion;            // MONEDA EN QUE PAGA
                    $oEgreso->modulo_id         = 61;                               // 61 fijo defecto
                    $oEgreso->egreso_modide     = $stockunidad_id;                  // id del stockunidad
                    $oEgreso->empresa_id        = $empresa_id;
                    $oEgreso->egreso_ap         = 0;                                // ACUERDO DE PAGO

                    // ---------------------- DATOS EGRESOS ------------------------- //

                    if ($ventavehiculo_mon < $saldo_a_pagar) // C < D --> SEPARACION
                    {
                        $oIngreso->modulo_id       = 62;
                        $oIngreso->cuenta_id       = 37;
                        $oIngreso->subcuenta_id    = 188;

                        if ($oStockunidad->separar_actualizar_cliente_id_userasesor_userautoriza($stockunidad_id, $cliente_id, $usuario_id2_id, $usuario_id) == true) {
                            if ($metodo_pago == 1) // EFECTIVO  ---> SÓLO REGISTRAR INGRESOS
                            {
                                $resultadoRegistrarIngreso = $oIngreso->insertar();
                                    if ($resultadoRegistrarIngreso['estado'] == true) {
                                        $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                                        if ($resultadoMofificacionIngreso == true) {
                                            $arrayResultado["estado"]  = 1;
                                            $arrayResultado["mensaje"] = "Separación registrada correctamente";
                                            // ----------- REGISTRAMOS EL HISTORIAL ----------- //
                                            $oHist->setTbHistDet($mensaje);
                                            $oHist->insertar();
                                            // ----------- REGISTRAMOS EL HISTORIAL ----------- //
                                        } else {
                                            $arrayResultado["estado"]  = 0;
                                            $arrayResultado["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                                        }
                                        $resultadoMofificacionIngreso = null;
                                    } else {
                                        $arrayResultado["estado"]  = 0;
                                        $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar el ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                                    }
                                $resultadoRegistrarIngreso = null;
                            }
                            elseif ($metodo_pago == 2) // TRANSFERENCIA BANCARIA ---> REGISTRAR INGRESOS - EGRESOS
                            {
                                $resultadoRegistrarIngreso = $oIngreso->insertar();
                                    if ($resultadoRegistrarIngreso['estado'] == true) {
                                        $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                                            if ($resultadoMofificacionIngreso == true) {
                                                $resultadoRegistrarEgreso = $oEgreso->insertar();
                                                    if ($resultadoRegistrarEgreso['estado'] == true) {
                                                        $arrayResultado["estado"]  = 1;
                                                        $arrayResultado["mensaje"] = "Separación registrada correctamente";
                                                        // ----------- REGISTRAMOS EL HISTORIAL ----------- //
                                                        $oHist->setTbHistDet($mensaje);
                                                        $oHist->insertar();
                                                        // ----------- REGISTRAMOS EL HISTORIAL ----------- //
                                                    } else {
                                                        $arrayResultado["estado"]  = 0;
                                                        $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar el egreso. <br> Error SQL: Egreso.class :: insertar() :: linea: 30";
                                                    }
                                                $resultadoRegistrarEgreso = null;
                                            } else {
                                                $arrayResultado["estado"]  = 0;
                                                $arrayResultado["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                                            }
                                        $resultadoMofificacionIngreso = null;
                                    } else {
                                        $arrayResultado["estado"]  = 0;
                                        $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                                    }
                                $resultadoRegistrarIngreso = null;
                            }
                            else
                            {
                                $monto_credito = moneda_mysql($datosFrm["txt_return_monto"]);
                                $arrayResultado["estado"]  = 0;
                                $arrayResultado["mensaje"] = "La separación por venta al crédito no está disponible, verifique el monto del crédito sea igual al saldo a pagar. <br>Saldo a pagar: ".mostrar_moneda($saldo_a_pagar)." - Monto a pagar: ".mostrar_moneda($ventavehiculo_mon)." - Monto Crédito: ".mostrar_moneda($monto_credito);
                            }
                        } else {
                            $arrayResultado["estado"]  = 0;
                            $arrayResultado["mensaje"] = "Se presentó un inconveniente al actualizar el cliente por separación de la unidad vehicular. <br> Error SQL: Stockunidad.class :: separar_actualizar_cliente_id_userasesor_userautoriza() :: linea: 641";
                        }

                        echo json_encode($arrayResultado);
                        exit();
                    }
                    elseif ($ventavehiculo_mon >= $saldo_a_pagar) // INGRESOS - EGRESOS - VENTA - DEPÓSITO / SOBRECOSTO
                    {
                        $resultaDataValidate = $oStockunidad->validar_montos02_esten_ingresados($credito_id, $creditotipo_id);
                            if ( ! isset($resultaDataValidate['data']['tb_credito_cap2']) || $resultaDataValidate['data']['tb_credito_cap2'] == 0 ) // ! isset($resultaDataValidate['data']['tb_credito_cuoven2']) || $resultaDataValidate['data']['tb_credito_cuoven2'] == 0 ||  // ! isset($resultaDataValidate['data']['tb_credito_gasto2'])  || $resultaDataValidate['data']['tb_credito_gasto2']  == 0
                            {
                                $arrayResultado["estado"]  = 0;
                                $arrayResultado["mensaje"] = "Proceso de venta detenido, está PENDIENTE guardar los 3 valores relevantes para finalizar la venta, recuerda que este proceso debe registrarlo primero el administrador.";
                                echo json_encode($arrayResultado);
                                exit();
                            }
                            elseif( $oStockunidad->validar_venta_no_registrada_por_credito($credito_id) == true )
                            {
                                $arrayResultado['estado']  = 0;
                                $arrayResultado["mensaje"] = 'COMUNICATE DE INMEDIATO CON GERENCIA Y EL ARÉA DE SISTEMAS, la venta no procede porque ya se ha registrado previamente con el id credito: '.$credito_id;
                                echo json_encode($arrayResultado);
                                exit();
                            }
                            else {
                                $oIngreso->modulo_id       = 10;
                                $oIngreso->cuenta_id       = 40;
                                $oIngreso->subcuenta_id    = 133;
                                
                                if ($metodo_pago == 1) // EFECTIVO ---> SÓLO REGISTRAR INGRESOS
                                {
                                    $resultadoRegistrarIngreso = $oIngreso->insertar();
                                        if ($resultadoRegistrarIngreso['estado'] == true) {
                                            $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                                                if ($resultadoMofificacionIngreso == false) {
                                                    $arrayResultado["estado"]  = 0;
                                                    $arrayResultado["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                                                }
                                            $resultadoMofificacionIngreso = null;
                                        } else {
                                            $arrayResultado["estado"]  = 0;
                                            $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                                        }
                                    $resultadoRegistrarIngreso = null;
                                }
                                elseif ($metodo_pago == 2) // TRANSFERENCIA BANCARIA ---> REGISTRAR INGRESOS - EGRESOS
                                {
                                    $resultadoRegistrarIngreso = $oIngreso->insertar();
                                        if ($resultadoRegistrarIngreso['estado'] == true) {
                                            $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                                                if ($resultadoMofificacionIngreso == true) {
                                                    $resultadoRegistrarEgreso = $oEgreso->insertar();
                                                    if ($resultadoRegistrarEgreso['estado'] == false) {
                                                        $arrayResultado["estado"]  = 0;
                                                        $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar el egreso. <br> Error SQL: Egreso.class :: insertar() :: linea: 30";
                                                    }
                                                    $resultadoRegistrarEgreso = null;
                                                } else {
                                                    $arrayResultado["estado"]  = 0;
                                                    $arrayResultado["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                                                }
                                            $resultadoMofificacionIngreso = null;
                                        } else {
                                            $arrayResultado["estado"]  = 0;
                                            $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                                        }
                                    $resultadoRegistrarIngreso = null;
                                }
                                else
                                {
                                    $monto_credito = moneda_mysql($datosFrm["txt_return_monto"]);
                                    if($monto_credito != $saldo_a_pagar || $ventavehiculo_mon != $saldo_a_pagar)
                                    {
                                        $arrayResultado["estado"]  = 0;
                                        $arrayResultado["mensaje"] = "Proceso de Venta al crédito detenido, el monto del crédito y el monto a pagar deben ser iguales al saldo a pagar.";
                                        echo json_encode($arrayResultado);
                                        exit();
                                    }
                                    else
                                    {
                                        $oVenta->vehiculo_est = 1;
                                        $oVenta->stockunidad_estado = 5;
                                    }
                                }

                                // ----------- REGISTRAR VENTA ----------- //
                                $oVenta->montototal = $suma_ing_prev_mont_pagar;
                                $resultadoRegistrarVenta = $oVenta->insertar();
                                    if ($resultadoRegistrarVenta["estado"] == true) {
                                        $newIdVenta = $resultadoRegistrarVenta["ventavehiculo_id"];

                                        if($metodo_pago==3){
                                            $monto_credito = moneda_mysql($datosFrm["txt_return_monto"]);
                                            $oVenta->modificar_campo($newIdVenta, 'tb_ventavehiculo_monini', $monto_previos, 'STR');
                                            $oVenta->modificar_campo($newIdVenta, 'tb_ventavehiculo_moncre', $monto_credito, 'STR');
                                        }

                                        $arrayResultado["estado"]  = 1;
                                        $arrayResultado["mensaje"] = "Nueva venta registrada correctamente. <br>Nro. de Venta: ".$newIdVenta;
                                    } else {
                                        $arrayResultado["estado"]  = 0;
                                        $arrayResultado["mensaje"] = "Se presentó un inconveniente al registrar la venta. <br> Error SQL: Venta.class :: insertar() :: linea: 30";
                                    }
                                $resultadoRegistrarVenta = null;
                                // ----------- REGISTRAR VENTA ----------- //

                                // ----------- REGISTRAR DEPOSITO ----------- //
                                $consultarDataDeposito = $oDeposito->calcularProximoDpositoID();
                                    if ($consultarDataDeposito["estado"] == 1) {
                                        $oDeposito->tb_deposito_fec = fecha_mysql($ventavehiculo_fec);
                                        $oDeposito->tb_deposito_des = '';
                                        $oDeposito->tb_deposito_mon = moneda_mysql($ventavehiculo_bas);
                                        $oDeposito->tb_deposito_num = 'VENTA-CGV-'.$credito_id.'-'.$consultarDataDeposito["data"]["newid"];
                                        $oDeposito->tb_cuentadeposito_id = $set_tbcuentadepositoid;
                                        $oDeposito->tb_deposito_venta = 1;
                                        $oDeposito->insertar();
                                    }
                                $consultarDataDeposito = null;
                                // ----------- REGISTRAR DEPOSITO ----------- //

                                // ----------- REGISTRAMOS EL HISTORIAL SEPARACIÓN ----------- //

                                $oHist->setTbHistDet($mensaje);
                                $oHist->insertar();

                                // ----------- REGISTRAMOS EL HISTORIAL SEPARACIÓN ----------- //

                                $monto_sobre_costo = formato_numero(($ventavehiculo_mon - $saldo_a_pagar));

                                // ----------- VALIDAR SI HAY SOBRE COSTO ----------- //
                                if( formato_numero($monto_sobre_costo) > 0) {
                                    $oVenta->modificar_campo($newIdVenta, 'tb_ventavehiculo_sob', $monto_sobre_costo, 'STR');
                                }
                                // ----------- VALIDAR SI HAY SOBRE COSTO ----------- //

                                // ----------- REGISTRAMOS EL HISTORIAL VENTA POR MONTO COMPLETO ----------- //
                                $mensaje = 'Venta Finalizada <br>Monto Total: ' . mostrar_moneda($ventavehiculo_bas) . '<br>Moneda: ' . $moneda_id_padre_descrip . '<br>Cliente: ' . $datosCliente_nombre_completo . ' <br>Placa del vehículo: ' . $vehiculo_pla;
                                $oHist->setTbHistDet($mensaje);
                                $oHist->insertar();
                                // ----------- REGISTRAMOS EL HISTORIAL VENTA POR MONTO COMPLETO ----------- //

                            }
                        $resultaDataValidate = null;

                        echo json_encode($arrayResultado);
                        exit();
                    } else {
                        $arrayResultado['estado']  = 0;
                        $arrayResultado["mensaje"] = 'Error de validación final :: operación inválida.';
                        echo json_encode($arrayResultado);
                        exit();
                    }
                }
            } else {
                $arrayResultado['estado']  = 0;
                $arrayResultado["mensaje"] = 'Moneda inválida, verifique.';
                echo json_encode($arrayResultado);
            }
        } else {
            $arrayResultado['estado']  = 0;
            $arrayResultado["mensaje"] = 'Método de pago inválido, verifique.';
            echo json_encode($arrayResultado);
        }
    } else {
        $arrayResultado["estado"] = 0;
        $arrayResultado["mensaje"] = "Operacion inválida";
        echo json_encode($arrayResultado);
    }
?>