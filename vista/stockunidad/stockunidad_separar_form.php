<?php
//revision de sesion activa
if (defined('VISTA_URL'))
  require_once(APP_URL . 'core/usuario_sesion.php');
else
  require_once('../../core/usuario_sesion.php');
//
//requires
require_once('../funciones/fechas.php');
require_once('../stockunidad/Stockunidad.class.php');
$oStockunidad = new Stockunidad();
require_once("../gasto/Sugerir.class.php");
$oSugerir = new Sugerir();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();
require_once('../funciones/funciones.php');



//datos post
$usuario_action = $_POST['action'];
$credito_id = $_POST['credito_id'];
$creditotipo_id = $_POST['creditotipo_id'];

$marca = $_POST['marca'];
$modelo = $_POST['modelo'];
$pre_ajus_bas = moneda_mysql($_POST['precio_bas']);
$pre_ajus_tot = moneda_mysql($_POST['precio_tot']);
$vista = $_POST['vista'];
$stockunidad_id = intval($_POST['stockunidad_id']);
$tipocambio = $_POST['ventaseparar'];


$credito_vehpla = $_POST['credito_vehpla'];




//datos estaticos
$listo = 1;
$action = devuelve_nombre_usuario_action($usuario_action);

$pre_ajus_tot = moneda_mysql($_POST['precio_tot']);

$titulo = 'Separar unidad';
//titulo del form
if ($usuario_action == 'I') {
  $titulo = 'Registrar stockunidad';
} elseif ($usuario_action == 'M') {
  $titulo = 'Editar stockunidad';
} elseif ($usuario_action == 'E') {
  $titulo = 'Eliminar stockunidad';
} elseif ($usuario_action == 'L') {
  $titulo = 'Persona registrada';
}
if ($usuario_action != 'I') {
  $titulo .= ": ID $stockunidad_id";
}
//

if ($listo == 1) {
  if (!empty($stockunidad_id)) {
    /* $res = $oPersona->mostrar_uno_id($stockunidad_id);

    if ($res['estado'] == 1) {
      $stockunidad = $res['data'];

      $nombres        = $stockunidad['tb_stockunidad_nom'];
      $apellidos      = $stockunidad['tb_stockunidad_ape'];
      $documento      = $stockunidad['tb_stockunidad_doc'];
      $numero_cel     = $stockunidad['tb_stockunidad_numcel'];
      $style_numcel   = empty($numero_cel) ? 'display: none;' : '';
    }
    unset($res); */
  }
} else {
  $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}

$result = $oSugerir->mostrar_datos_sugerir_credito($credito_id, $creditotipo_id, 2); //de tipo 2 para que devuelva datos de ajustes
if ($result['estado'] == 1) {
  $sug_id = $result['data']['tb_sugerir_id'];
  $pre_base = (!empty($result['data']['tb_sugerir_prebas'])) ? floatval($result['data']['tb_sugerir_prebas']) : 0;
  $pre_soat = (!empty($result['data']['tb_sugerir_presoat'])) ? floatval($result['data']['tb_sugerir_presoat']) : 0;
  $pre_str = (!empty($result['data']['tb_sugerir_prestr'])) ? floatval($result['data']['tb_sugerir_prestr']) : 0;
  $pre_gps = (!empty($result['data']['tb_sugerir_pregps'])) ? floatval($result['data']['tb_sugerir_pregps']) : 0;
  $pre_gas = (!empty($result['data']['tb_sugerir_pregas'])) ? floatval($result['data']['tb_sugerir_pregas']) : 0;

  //INGRESOS
  $mod_id = 62;
  $est = "";
  $total_desembolsado = 0;
  $ingreso_resta = $oIngreso->mostrar_por_modulo($mod_id, $stockunidad_id, 0, 0);
  if ($ingreso_resta['estado'] == 1) {
    foreach ($ingreso_resta['data'] as $key => $value3) {
      if ($value3['tb_moneda_id'] == 1)
        $total_desembolsado += $value3['tb_ingreso_imp'];

      if ($value3['tb_moneda_id'] == 2)
        $total_desembolsado += ($value3['tb_ingreso_imp'] * $value3['tb_ingreso_tipcam']);
    }
  }
  $ingreso_resta = null;

  /*  $oIngreso->mostrar_por_modulo($mod_id, $modide, $mon_id, $est); //retorna lista de ingresos,mostrar cuanto falta para completar el pago,también sirve al momneto de vender
  //mod_id=moduloid,modide=id del stock unidad, mon_id=monedaid,est=estado del ingreso pero se mandará en 0 */

  $mon_id = $result['data']['tb_moneda_id'];

  $result2 = $oStockunidad->mostrar_vehiculo($credito_id,$creditotipo_id);

  if ($result2['estado'] == 1) {
    $vehiculo = $result2['data']['tb_vehiculomarca_nom'] . ' ' . $result2['data']['tb_vehiculomodelo_nom'];
    $anio = $result2['data']['tb_credito_vehano'];
  } else {
    $vehiculo = 'EEEE';
    $anio = 'AAAA';
  }
  $result2 = NULL;
} else {
  $pre_base = 5;
}
$result = NULL;

$pre_tot = formato_numero($pre_base + $pre_soat + $pre_str + $pre_gps + $pre_gas);



if ($total_desembolsado > 0) {
  $pre_base = $pre_ajus_bas;
  $pre_tot = $pre_tot - $total_desembolsado;
}
$result = NULL;
?>

<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_stockunidad_separar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <?php if ($listo == 0) { ?>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4><?php echo $mensaje; ?></h4>
        </div>
      <?php } else { ?>
        <form id="form_stockunidad" method="post">
          <div class="modal-header">
            <div class="row">
              <div class="form-group col-md-8">
                <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
              </div>
              <div class="form-group col-md-4">
                <label class="control-label">Tipo de Cambio:</label>
                <input type="text" name="txt_tipocambio_id" id="txt_tipocambio_id" class="form-control input-sm" value="<?php echo $tipocambio; ?>" readonly>
              </div>
            </div>
          </div>

          <div class="modal-body">
            <input type="hidden" name="action" id="action" value="separar">
            <input type="hidden" name="hdd_stockunidad_id" id="hdd_stockunidad_id" value="<?php echo $stockunidad_id; ?>">
            <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id; ?>">
            <input type="hidden" name="hdd_creditotipo_id" id="hdd_creditotipo_id" value="<?php echo $creditotipo_id; ?>">
            <input type="hidden" name="tb_vehiculomarca_id" id="tb_vehiculomarca_id" id="tb_vehiculomarca_id" value="<?php echo $tb_vehiculomarca_id; ?>">
            <input type="hidden" name="marca" id="marca" value="<?php echo $marca; ?>">
            <input type="hidden" name="modelo" id="modelo" value="<?php echo $modelo; ?>">
            <input type="hidden" name="hdd_stockunidad_separar_reg" id="hdd_stockunidad_separar_reg" value='<?php echo json_encode($reg_origin); ?>'>
            <input type="hidden" name="montobase" id="montobase" value="<?php echo $pre_base; ?>">
            <input type="hidden" name="preciototal" id="preciototal" value="<?php echo $pre_tot; ?>">

            <div class="box box-primary shadow">
              <h3 style="text-align: center;">DATOS DE VEHÍCULO</h3>

              <hr>
              <div class="form-group">
                <div class="row">
                  <div class="form-group col-md-4">
                    <label for="txt_vehiculo_mostrar" class="control-label">Vehículo:</label>
                    <!-- Muestra el vehículo -->
                    <input type="text" name="txt_vehiculo_mostrar" id="txt_vehiculo_mostrar" class="form-control input-sm" value="<?php echo $vehiculo; ?>" readonly>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="txt_placa_mostrar" class="control-label">Placa:</label>
                    <!-- Muestra la placa -->
                    <input type="text" name="txt_placa_mostrar" id="txt_placa_mostrar" class="form-control input-sm" value="<?php echo $credito_vehpla; ?>" readonly>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="txt_anio_mostrar" class="control-label">Año:</label>
                    <!-- Muestra el año -->
                    <input type="text" name="txt_anio_mostrar" id="txt_anio_mostrar" class="form-control input-sm" value="<?php echo $anio; ?>" readonly>
                  </div>
                </div>
              </div>



              <hr>


              <div id="proveedor-grp" class="input-group">
                <input type="text" name="txt_stockunidad_clienteid" placeholder="ESCRIBE UN CLIENTE PARA BUSCAR" id="txt_stockunidad_clienteid" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $cliente; ?>">
                <input type="hidden" name="hdd_stockunidad_clienteid" id="hdd_stockunidad_clienteid" value="<?php echo $clienteid ?>">
                <span class="input-group-btn">
                  <button id="btn_agr" class="btn btn-primary btn-sm" type="button" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                    <span class="fa fa-plus icon"></span>
                  </button>
                </span>
              </div>

              <br>


              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="txt_monto" class="control-label">Ingresos:</label>
                    <input type="text" name="txt_ingresos" id="txt_ingresos" class="form-control input-sm" value="<?php echo mostrar_moneda($total_desembolsado); ?>" readonly>
                  </div>



                  <div class="col-md-6">
                    <label for="cbo_moneda_id">Moneda:</label>
                    <select name="cbo_moneda_id" id="cbo_moneda_id" class="form-control input-sm">
                      <?php require_once '../moneda/moneda_select.php'; ?>
                    </select>
                  </div>

                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="txt_monto" class="control-label">Monto:</label>
                    <input type="text" name="txt_monto" id="txt_monto" class="form-control input-sm">
                  </div>



                  <div class="form-group col-md-6">
                    <label for="txt_ajuste_tot" class="control-label">Monto Total:</label>
                    <input type="hidden" name="txt_tot" id="txt_tot" class="form-control input-sm" value="<?php echo $pre_tot; ?>">
                    <input type="text" name="txt_ajuste_tot" id="txt_ajuste_tot" class="form-control input-sm" value="<?php echo mostrar_moneda($pre_tot); ?>" readonly>
                  </div>

                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-6">
                  <label for="cbo_metodo_id">Método de pago:</label>
                  <select name="cbo_metodo_id" id="cbo_metodo_id" class="form-control input-sm">
                    <option value="1">Cheque</option>
                    <option value="2">Transferencia bancaria</option>
                    <option value="3">Efectivo</option>
                  </select>
                </div>
              </div>

              <!-- DIV QUE SE VA A OCULTAR-->

              <p>

              <div class="row" style="display: none;" id="precio_banco">
                <div class="col-md-6">
                  <label>N° Cuenta:</label>
                  <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                    <?php require_once '../cuentadeposito/cuentadeposito_select.php'; ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Monto Depósito:</label>
                  <input style="text-align:right;" class="form-control input-sm mayus moneda3" name="txt_montodeposito" type="text" id="txt_montodeposito">
                </div>
              </div>
              <p>

              <div class="row" style="display: none;" id="precio_banco2">
                <div class="col-md-6">
                  <label>Comisión Banco:</label>
                  <input style="text-align:right;" class="form-control input-sm moneda3" name="txt_combanco" type="text" id="txt_combanco" value="0.00">
                </div>
                <div class="col-md-6">
                  <label>Monto Pagado:</label>
                  <input style="text-align:right;" class="form-control input-sm moneda3" name="txt_montopag" type="text" id="txt_montopag" value="0" readonly>
                </div>
              </div>

              <div class="row" style="display: none;" id="precio_banco3">
                <div class="col-md-6">
                  <label>N° Operación:</label>
                  <input type="text" name="txt_numoperacion" id="txt_numoperacion" class="form-control input-sm">
                </div>
              </div>
              <hr>

              <!-- FIN DIV QUE SE VA A OCULTAR-->

              <div class="row form-group">
                <div class="form-group col-md-12">
                  <label for="txta_detalle" class="control-label">Detalle:</label>
                  <textarea name="txta_detalle" id="txta_detalle" class="form-control input-sm"></textarea>
                </div>
              </div>
            </div>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="stockunidad_mensaje" style="display: none;">
            </div>
          </div>

          <div class="modal-footer">
            <div class="f1-buttons">

              <button type="submit" class="btn btn-info" id="btn_guardar_stockunidad">Guardar</button>
              <button type="button" id="btn_cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      <?php } ?>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/stockunidad/stockunidad_separar_form.js'; ?>"></script>