$(document).ready(function () {
  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999999.00",
  });
  
  validarDataAjustar();

  $("#form_stockunidad_ajustar").validate({
    submitHandler: function () {
      $.confirm({
        title: "¿Está seguro que desea ajustar?",
        content: "",
        buttons: {
          formSubmit: {
            text: "Aceptar",
            btnClass: "btn-blue",
            action: function () {
              arrayDetalle.splice(0, arrayDetalle.length);

              var credito_id = $("#hdd_credito_id").val();
              credito_id = "&hdd_credito_id=" + parseInt(credito_id);

              var tipo_id = $("#hdd_tipo_id").val();
              tipo_id = "&hdd_tipo_id=" + parseInt(tipo_id);

              var item = 0;
              $("#lista-body-detalle tr").each(function () {
                var descripcion = $(this).find("td").eq(1).html();
                item = item + 1;

                var objDetalle = new Object();
                objDetalle.item = item;
                objDetalle.descripcion = descripcion;
                arrayDetalle.push(objDetalle);
              });

              var jsonDetalle = JSON.stringify(arrayDetalle);

              $.ajax({
                type: "POST",
                url: VISTA_URL + "stockunidad/stockunidad_controller.php",
                async: true,
                dataType: "JSON",
                data: {
                  action: "ajustar_update",
                  p_array_datos:
                    $("#form_stockunidad_ajustar").serialize() +
                    credito_id +
                    tipo_id,
                  p_detalle: jsonDetalle,
                },
                beforeSend: function () {
                  $("#stockunidad_mensaje_tbl").show(400);
                  $("#btn_guardar").prop("disabled", true);
                },
                success: function (data) {
                  console.log(data);
                  if (parseInt(data.estado) > 0) {
                    $("#div_modal_ajustar").modal("hide");
                    notificacion_success(data.mensaje, 6000);
                  } else {
                    $("#stockunidad_mensaje_tbl")
                      .removeClass("callout-info")
                      .addClass("callout-warning");
                    $("#stockunidad_mensaje_tbl").html("Alerta: " + data);
                    $("#btn_guardar").prop("disabled", false);
                  }
                },
                complete: function (data) {},
                error: function (data) {
                  $("#stockunidad_mensaje_tbl")
                    .removeClass("callout-info")
                    .addClass("callout-danger");
                  $("#stockunidad_mensaje_tbl").html(
                    "ALERTA DE ERROR: " + data.responseText
                  );
                },
              });
            },
          },
          Cancelar: function () {
            //close
          },
        },
        onContentReady: function () {
          // bind to events
          var jc = this;
          this.$content.find("form").on("submit", function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger("click"); // reference the button and click it
          });
        },
      });
    },
    rules: {
      txt_stockunidad_montoresolucion: {
        required: true,
      },
      txt_stockunidad_monbas: {
        required: true,
      },
      cmb_moneda_id: {
        required: true,
      },
      txt_stockunidad_soatval: {
        required: true,
      },
      // txt_stockunidad_revtecval: {
      //   required: true,
      // },
      txt_stockunidad_kil: {
        required: true,
      },
      cmb_stockunidad_tarjeta: {
        required: true,
      },
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else if (element.parent("div.input-group").length == 1) {
        error.insertAfter(element.parent("div.input-group")[0]);
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });

});

var arrayDetalle = new Array();

function validarDataAjustar() {
  var validar_credito_id = $("#hdd_credito_id").val();
  var validar_tipo_id = $("#hdd_tipo_id").val();

  console.log(validar_credito_id);
  console.log(validar_tipo_id);

  if (validar_credito_id == null) {
    validar_credito_id = 0;
  }

  if (validar_tipo_id == null) {
    validar_tipo_id = 0;
  }

  validar_credito_id = parseInt(validar_credito_id);
  validar_tipo_id = parseInt(validar_tipo_id);

  if ($.trim(validar_credito_id) == "" || validar_credito_id == 0) {
    alerta_error("Error", "Crédito ID es inválido");
    return 0;
  } else if ($.trim(validar_tipo_id) == "" || validar_tipo_id == 0) {
    alerta_error("Error", "Tipo ID es inválido");
    return 0;
  } else {
    var credito_id = parseInt($("#hdd_credito_id").val());
    var tipo_id = parseInt($("#hdd_tipo_id").val());

    $.ajax({
      type: "POST",
      url: VISTA_URL + "stockunidad/stockunidad_controller.php",
      async: true,
      dataType: "json",
      data: {
        action: "ajustar_validar",
        hdd_credito_id: credito_id,
        hdd_tipo_id: tipo_id,
      },
      beforeSend: function () {
        // $('#stockunidad_mensaje').hide(300);
        console.log(
          "/Stock Unidad/ Credito ID: " +
            credito_id +
            " - Credito tipo ID: " +
            tipo_id
        );
      },
      success: function (data) {
        console.log(data);

        if (parseInt(data.estado) > 0) {
          // var sumaMontoResolucion =
          //   parseFloat(data.datos.tb_credito_cap1) +
          //   parseFloat(data.datos.tb_credito_cuoven1) +
          //   parseFloat(data.datos.tb_credito_gasto1);
          // $("#txt_stockunidad_montoresolucion").val(
          //   sumaMontoResolucion.toFixed(2)
          // );
          // $("#txt_stockunidad_monbas").val(data.datos.tb_stockunidad_monbas);
          // $("#cmb_moneda_id").val(data.datos.tb_moneda_id);
          // $("#txt_stockunidad_soatval").val(data.datos.tb_stockunidad_soatval);
          // $("#txt_stockunidad_revtecval").val(data.datos.tb_stockunidad_revtecval);
          // $("#txt_stockunidad_kil").val(data.datos.tb_stockunidad_kil);
          // $("#txt_stockunidad_det").val(data.datos.tb_stockunidad_det);
          $("#cmb_stockunidad_tarjeta").val(data.datos.tb_stockunidad_tarjeta);
          // var listElement = data.datos.tb_stockunidad_det;
          // var dato = listElement.split("/&/&/", 1);
          // var fila =
          //   '<tr>' +
          //     '<td style="vertical-align: middle;"> 0 </td>' +
          //     '<td style="vertical-align: middle;">' + dato + '</td>' +
          //     '<td style="vertical-align: middle;" id="celiminar" align="center" ><a><i class="fa fa-trash text-orange"></i></a></td>' +
          //   '</tr>';
          // $("#lista-body-detalle").append(fila);
          // updateContadorTabla();
        } else {
          $("#btn_guardar").hide();
          $("#div_modal_ajustar").modal("hide");
          alerta_warning(
            "Advertencia",
            "Proceso de preparación del vehículo PENDIENTE"
          );
        }
      },
      complete: function (data) {
        // console.log(data);
      },
      error: function (data) {
        $("#stockunidad_mensaje_tbl")
          .removeClass("callout-info")
          .addClass("callout-danger");
        $("#stockunidad_mensaje_tbl").html(
          "ALERTA DE ERROR: " + data.responseText
        );
        alerta_error("Advertencia", "" + data.responseText);
      },
    });
  }
}

$("#div_modal_ajustar").on("shown.bs.modal", function () {
  $("#txt_stockunidad_montoresolucion").focus();
});

$("#txt_stockunidad_det").keypress(function (evento) {
  if (evento.which === 13) {
    evento.preventDefault(); //ignore el evento
    $("#btn_añadir").click();
  }
});

$("#btn_añadir").click(function () {
  if ($("#txt_stockunidad_det").val().toString() === "") {
    alerta_warning("Advertencia", "Debe ingresar la descripción");
    $("#txt_stockunidad_det").focus();
    return 0;
  }

  var cont = parseInt($("#txtcontador").val());
  var descripcion = $("#txt_stockunidad_det").val();

  var fila =
    "<tr>" +
      '<td style="vertical-align: middle;">' +
      parseInt(cont + 1) +
      "</td>" +
      '<td style="vertical-align: middle;" id="modcampo">' +
        descripcion +
      "</td>" +
      '<td style="vertical-align: middle;" align="center"><a><i class="fa fa-trash text-orange" onclick="eliminarFila(this)"></i></a></td>' +
    "</tr>";

  $("#lista-body-detalle").append(fila);
  updateContadorTabla();
  $("#txt_stockunidad_det").val("");
  $("#txt_stockunidad_det").focus();
});

function updateContadorTabla() {
  var item = 0;
  $("#lista-body-detalle tr").each(function () {
    item = item + 1;
    $(this).find("td").eq(0).html(item);
  });
  $("#txtcontador").val(item);
}

function eliminarFila(element)
{
  var fila = $(element).parents().get(2);
  $.confirm({
    title: "¡Cuidado!",
    content: "¿Esta seguro de elimina el registro seleccionado?",
    type: "orange",
    escapeKey: "close",
    backgroundDismiss: true,
    columnClass: "small",
    buttons: {
      Confirmar: {
        btnClass: "btn-orange",
        action: function () {
          fila.remove();
          updateContadorTabla();
        },
      },
      cancelar: function () {},
    },
  });
}

$(document).on("dblclick", "#modcampo", function(){
   var valor = $(this).html();

    if (valor.substring(0,6)==="<input"){
      return 0;
    }

   $(this).empty().append('<input type="text" id="txtactualizar" class="form-control" value = "'+valor+'"/>');
   $("#txtactualizar").focus();
});

$(document).on("keypress", "#txtactualizar", function(evento){
  if (evento.which === 13)
  {
    var new_valor = $(this).val();
        new_valor = new_valor.toString();
    // console.log('Valor actualizado: '+new_valor)
    $(this).closest('td').empty().html(new_valor);
  }
});


function filestorage_form(){
  var filestorage_creditoID = $("#hdd_credito_id").val();
  console.log('Enviando el parametro credito id:'+filestorage_creditoID)
  $.ajax({
      type: "POST",
      url: VISTA_URL + "filestorage/filestorage_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: "I", //insertar
        public_carpeta: 'images',
        modulo_nom: 'stockunidad',
        modulo_id: filestorage_creditoID,
        modulo_subnom: 'stockunidad_ajustar',
        filestorage_uniq: '',
        filestorage_des: 'CGV-'+filestorage_creditoID
      }),
      beforeSend: function () {
        
      },
      success: function (data) {
        console.log(data)
        if (data != 'sin_datos') {
          $("#spnadjfotos").empty().append('Archivo(s) adjuntados correctamente');
          $('#div_filestorage_form').html(data);
          $('#modal_registro_filestorage').modal('show');
          modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function (data) {
      },
      error: function (data) {
        
      }
  });
}

function filestorage_form_pdf(){
  var filestorage_creditoID = $("#hdd_credito_id").val();
  console.log('Enviando el parametro credito id:'+filestorage_creditoID)
  $.ajax({
      type: "POST",
      url: VISTA_URL + "filestorage/filestorage_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: "I", //insertar
        public_carpeta: 'pdf',
        modulo_nom: 'stockunidad',
        modulo_id: filestorage_creditoID,
        modulo_subnom: 'stockunidad_ajustar_pdf',
        filestorage_uniq: '',
        filestorage_des: 'pdf-CGV-'+filestorage_creditoID
      }),
      beforeSend: function () {
        
      },
      success: function (data) {
        console.log(data)
        if (data != 'sin_datos') {
          $('#div_filestorage_form').html(data);
          $('#modal_registro_filestorage').modal('show');
          modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function (data) {
      },
      error: function (data) {
        
      }
  });
}

function refreshSpanPDF(){
  $("#spnadjpdf").empty().append('Archivo(s) PDF adjuntado correctamente, cierra la ventana y vuelve a abrir');
}

function refreshSpanImages(){
  $("#spnadjfotos").empty().append('Fotos(s) adjuntado correctamente, cierra la ventana y vuelve a abrir');
}

// DETECTAR CERRAR FRM ADJUNTAR ARCHIVOS Y FOTOS

// $("#btn_cerrar_filestorage").click(function() {
//   alert('cerro formulario')
// });

