<?php
  if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Stockunidad extends Conexion
  {

    public $tb_credito_id;
    public $tb_creditotipo_id;
    public $tb_creditomoneda_id;
    public $tb_credito_vehpla;
    public $tb_credito_cuoven1;
    public $tb_credito_cap1;
    public $tb_credito_gasto1;
    public $tb_credito_cuoven2;
    public $tb_credito_cap2;
    public $tb_credito_gasto2;

    function preparar_insertar()
    {
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_stockunidad(
            tb_credito_id, tb_creditotipo_id, tb_credito_vehpla, tb_moneda_id, tb_moneda_id_anterior,
            tb_credito_cuoven1, tb_credito_cap1, tb_credito_gasto1, tb_stockunidad_usuven, tb_stockunidad_usuaut, tb_stockunidad_xac)
            VALUES (
              :credito_id, :creditotipo_id, :credito_vehpla, :moneda_id, :moneda_id, :credito_cuoven1, :credito_cap1, :credito_gasto1, 0, 0, 1)
          ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $this->tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_vehpla", $this->tb_credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_id", $this->tb_creditomoneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->tb_creditomoneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_cuoven1", $this->tb_credito_cuoven1, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_cap1", $this->tb_credito_cap1, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_gasto1", $this->tb_credito_gasto1, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function preparar_actualizar01()
    {
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_stockunidad SET
              tb_credito_vehpla = :credito_vehpla, tb_credito_cuoven1 = :credito_cuoven1, tb_credito_cap1 = :credito_cap1, tb_credito_gasto1 = :credito_gasto1
            WHERE 
              tb_credito_id = :credito_id AND tb_creditotipo_id = :creditotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $this->tb_credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_cuoven1", $this->tb_credito_cuoven1, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_cap1", $this->tb_credito_cap1, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_gasto1", $this->tb_credito_gasto1, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_id", $this->tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function preparar_actualizar02()
    {
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_stockunidad SET
              tb_credito_vehpla = :credito_vehpla, tb_credito_cuoven2 = :credito_cuoven2, tb_credito_cap2 = :credito_cap2, tb_credito_gasto2 = :credito_gasto2
            WHERE 
              tb_credito_id = :credito_id AND tb_creditotipo_id = :creditotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $this->tb_credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_cuoven2", $this->tb_credito_cuoven2, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_cap2", $this->tb_credito_cap2, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_gasto2", $this->tb_credito_gasto2, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_id", $this->tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function validar_montos02_esten_ingresados($credito_id,$credito_tip)
    {
      try {
        $sql = "SELECT tb_credito_cuoven2, tb_credito_cap2, tb_credito_gasto2 FROM tb_stockunidad WHERE tb_credito_id = :credito_id AND tb_creditotipo_id = :creditotipo_id AND tb_stockunidad_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $credito_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $resultado = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function validar_venta_no_registrada_por_credito($credito_id)
    {
      try {
        $sql = "SELECT tb_ventavehiculo_id FROM tb_ventavehiculo WHERE tb_ventavehiculo_xac = 1 AND tb_credito_id = :credito_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();
        if($sentencia->rowCount() > 0)
        {
          $sentencia->closeCursor(); //para libera memoria de la consulta
          return true;
        }
        else
        {
          return false;
        }
      } catch (Exception $e) {
        throw $e;
      }
    }

    function modificar($stockunidad_id, $credito_id, $credito_vehpla, $stockunidad_soatval, $stockunidad_strval, $stockunidad_gpsval, $stockunidad_kil, $stockunidad_det, $creditotipo_id)
    {
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_stockunidad SET 
            tb_credito_id =:credito_id, tb_credito_vehpla =:credito_vehpla, tb_stockunidad_soatval =:stockunidad_soatval, 
            tb_stockunidad_strval =:stockunidad_strval, tb_stockunidad_gpsval =:stockunidad_gpsval, 
            tb_stockunidad_kil =:stockunidad_kil, tb_stockunidad_det =:stockunidad_det, 
            tb_creditotipo_id =:creditotipo_id
            WHERE tb_stockunidad_id =:stockunidad_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":stockunidad_soatval", $stockunidad_soatval, PDO::PARAM_STR);
        $sentencia->bindParam(":stockunidad_strval", $stockunidad_strval, PDO::PARAM_STR);
        $sentencia->bindParam(":stockunidad_gpsval", $stockunidad_gpsval, PDO::PARAM_STR);
        $sentencia->bindParam(":stockunidad_kil", $stockunidad_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":stockunidad_det", $stockunidad_det, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function eliminar($stockunidad_id)
    {
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_stockunidad WHERE tb_stockunidad_id =:stockunidad_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUno($stockunidad_id)
    {
      try {
        $sql = "SELECT * FROM tb_stockunidad WHERE tb_stockunidad_id =:stockunidad_id AND tb_stockunidad_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_datos_stock_placa($credito_id, $credito_vehpla)
    {
      try {
        $sql = "SELECT * FROM tb_stockunidad WHERE UPPER(tb_credito_vehpla) = UPPER(:credito_vehpla) and tb_credito_id =:credito_id AND tb_stockunidad_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_stockunidad_placa($credito_vehpla)
    {
      try {
        $sql = "SELECT * FROM tb_stockunidad WHERE UPPER(tb_credito_vehpla) = UPPER(:credito_vehpla) AND tb_stockunidad_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_fotos_credito($tabla,$credito_vehpla)
    {
      try
      {
        $tabla_file = $tabla . 'file';
        $sql = "SELECT cre.tb_credito_id, ".$tabla_file."_url as img_url 
          FROM $tabla cre 
          INNER JOIN $tabla_file fil on (fil.tb_credito_id = cre.tb_credito_id) 
          WHERE tb_credito_vehpla =:credito_vehpla and tb_credito_xac = 1 limit 5";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      }
      catch (Exception $e)
      {
        throw $e;
      }
    }

    function mostrar_archivos_stockunidad_ajustar($credito_id, $tabla, $submod)
    {
      try
      {
        $sql = 'SELECT * FROM filestorage WHERE modulo_id = ? AND modulo_nom = ? AND modulo_subnom = ? AND filestorage_xac = 1';
        $sentencia = $this->dblink->prepare($sql);
        // $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(1, $credito_id, PDO::PARAM_STR);
        $sentencia->bindParam(2, $tabla, PDO::PARAM_STR);
        $sentencia->bindParam(3, $submod, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      }
      catch (Exception $e)
      {
        throw $e;
      }
    }

    function stockunidades_tabla($cmb_estado)
    {
      try {
        $sql = "SELECT * FROM tb_stockunidad WHERE (case :p_cmbestado when 0 then 1=1 else tb_stockunidad_est = :p_cmbestado end) AND tb_stockunidad_xac = 1 ";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":p_cmbestado", $cmb_estado, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_vehiculo($credito_id,$credito_tip)
    {
      try
      {
        $tabla = 'tb_creditogarveh';
        if ($credito_tip == 2)
          $tabla = 'tb_creditoasiveh';

        $sql = "SELECT * 
          FROM $tabla cg
          LEFT JOIN tb_vehiculomarca vm ON cg.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
          LEFT JOIN tb_vehiculomodelo vd ON cg.tb_vehiculomodelo_id=vd.tb_vehiculomodelo_id
          LEFT JOIN tb_vehiculotipo vt ON cg.tb_vehiculotipo_id=vt.tb_vehiculotipo_id
          LEFT JOIN tb_vehiculoclase vc ON cg.tb_vehiculoclase_id=vc.tb_vehiculoclase_id
          WHERE tb_credito_id =:credito_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    // function anularVenta($stockunidad_id)
    // {
    //   $this->dblink->beginTransaction();
    //   try {
    //     $sql = "UPDATE tb_stockunidad SET tb_stockunidad_xac = 0 WHERE tb_stockunidad_id =:stockunidad_id";

    //     $sentencia = $this->dblink->prepare($sql);
    //     $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);

    //     $result = $sentencia->execute();

    //     $this->dblink->commit();

    //     return $result; //si es correcto retorna 1

    //   } catch (Exception $e) {
    //     $this->dblink->rollBack();
    //     throw $e;
    //   }
    // }

    function modificar_campo($stockunidad_id, $stockunidad_columna, $stockunidad_valor, $param_tip)
    {
      $this->dblink->beginTransaction();
      try {
        if (!empty($stockunidad_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

          $sql = "UPDATE tb_stockunidad SET " . $stockunidad_columna . " =:stockunidad_valor WHERE tb_stockunidad_id =:stockunidad_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);
          if ($param_tip == 'INT') {
            $sentencia->bindParam(":stockunidad_valor", $stockunidad_valor, PDO::PARAM_INT);
          } else {
            $sentencia->bindParam(":stockunidad_valor", $stockunidad_valor, PDO::PARAM_STR);
          }

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        } else
          return 0;
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }


    // --------------------------- AJUSTAR ---------------------------

    function ajustar_consultar_existe($credito_id,$creditotipo_id)
    {
      try
      {
        $sql = "SELECT tb_stockunidad_id, tb_credito_id, tb_creditotipo_id, tb_stockunidad_monbas, tb_moneda_id, tb_stockunidad_soatval, tb_stockunidad_revtecval, tb_stockunidad_kil, tb_stockunidad_det,
            tb_credito_cuoven1, tb_credito_cap1, tb_credito_gasto1, tb_stockunidad_tarjeta, tb_moneda_id_anterior, tb_credito_vehpla, tb_stockunidad_ofrecer_credito
          FROM tb_stockunidad WHERE tb_credito_id = :p_credito_id AND tb_creditotipo_id = :p_creditotipo_id AND tb_stockunidad_xac = 1";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":p_credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":p_creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"]  = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"]    = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      }
      catch (Exception $e) 
      {
        throw $e;
      }
    }

    function ajustar_actualizar($credito_id, $creditotipo_id, $stockunidad_soatval, $stockunidad_revtecval, $stockunidad_kil, $stockunidad_tarjeta, $appendDetalle, $moneda_id, $stockunidad_monbas, $ofrecerCredito)
    {
      $this->dblink->beginTransaction();

      try
      {
        $sql = "UPDATE tb_stockunidad SET tb_stockunidad_soatval = :p_campo01, tb_stockunidad_revtecval = :p_campo02, tb_stockunidad_kil = :p_campo03,
            tb_stockunidad_tarjeta = :p_campo04, tb_stockunidad_det = :p_campo05, tb_moneda_id = :p_campo06, tb_stockunidad_monbas = :p_campo07, tb_stockunidad_ofrecer_credito = :p_campo08
          WHERE tb_credito_id = :p_credito_id AND tb_creditotipo_id = :p_tipo_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":p_campo01", $stockunidad_soatval);
        $sentencia->bindParam(":p_campo02", $stockunidad_revtecval);
        $sentencia->bindParam(":p_campo03", $stockunidad_kil);
        $sentencia->bindParam(":p_campo04", $stockunidad_tarjeta);
        $sentencia->bindParam(":p_campo05", $appendDetalle);
        $sentencia->bindParam(":p_campo06", $moneda_id);
        $sentencia->bindParam(":p_campo07", $stockunidad_monbas);
        $sentencia->bindParam(":p_campo08", $ofrecerCredito);
        $sentencia->bindParam(":p_credito_id", $credito_id);
        $sentencia->bindParam(":p_tipo_id", $creditotipo_id);
        $sentencia->execute();
        $this->dblink->commit();
      }
      catch (Exception $e)
      {
        $this->dblink->rollBack();
        throw $e;
      }
      return true;
    }

    function ajustar_actualizar_momento02($credito_id, $creditotipo_id, $stockunidad_soatval, $stockunidad_revtecval, $stockunidad_kil, $stockunidad_tarjeta, $appendDetalle, $moneda_id, $stockunidad_monbas)
    {
      $this->dblink->beginTransaction();

      try
      {
        $sql = "UPDATE tb_stockunidad SET tb_stockunidad_soatval = :p_campo01, tb_stockunidad_revtecval = :p_campo02, tb_stockunidad_kil = :p_campo03,
            tb_stockunidad_tarjeta = :p_campo04, tb_stockunidad_det = :p_campo05, tb_moneda_id = :p_campo06, tb_stockunidad_monbas = :p_campo07
          WHERE tb_credito_id = :p_credito_id AND tb_creditotipo_id = :p_tipo_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":p_campo01", $stockunidad_soatval);
        $sentencia->bindParam(":p_campo02", $stockunidad_revtecval);
        $sentencia->bindParam(":p_campo03", $stockunidad_kil);
        $sentencia->bindParam(":p_campo04", $stockunidad_tarjeta);
        $sentencia->bindParam(":p_campo05", $appendDetalle);
        $sentencia->bindParam(":p_campo06", $moneda_id);
        $sentencia->bindParam(":p_campo07", $stockunidad_monbas);
        $sentencia->bindParam(":p_credito_id", $credito_id);
        $sentencia->bindParam(":p_tipo_id", $creditotipo_id);
        $sentencia->execute();
        $this->dblink->commit();
      }
      catch (Exception $e)
      {
        $this->dblink->rollBack();
        throw $e;
      }
      return true;
    }

    function vender_consultar_por_id($codigoID)
    {
      try
      {
        $sql = "SELECT * FROM tb_stockunidad WHERE tb_stockunidad_id = :p_codid AND tb_stockunidad_xac = 1";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":p_codid", $codigoID, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
          $retorno["data"] = "";
        }

        return $retorno;
      }
      catch (Exception $e) 
      {
        throw $e;
      }
    }

    function separar_actualizar_cliente_id_userasesor_userautoriza($stockunidad_id,$cliente_id,$tb_stockunidad_usuven,$tb_stockunidad_usuaut)
    {
      $this->dblink->beginTransaction();

      try
      {
        $sql = "UPDATE tb_stockunidad SET tb_cliente_id = :p_cliente_id, tb_stockunidad_usuven = :p_stockunidad_usuven, tb_stockunidad_usuaut = :p_stockunidad_usuaut
          WHERE tb_stockunidad_id = :p_campo02";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":p_cliente_id", $cliente_id);
        $sentencia->bindParam(":p_stockunidad_usuven", $tb_stockunidad_usuven);
        $sentencia->bindParam(":p_stockunidad_usuaut", $tb_stockunidad_usuaut);
        $sentencia->bindParam(":p_campo02", $stockunidad_id);
        $sentencia->execute();
        $this->dblink->commit();
      }
      catch (Exception $e)
      {
        $this->dblink->rollBack();
        throw $e;
      }
      return true;
    }
    
  }
?>