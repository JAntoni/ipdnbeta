$(document).ready(function () {

  validarTC();

  $("#datetimepicker1, #datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999999.00",
  });
  
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $("#txt_filtro_fec1").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
  });

  $("#datetimepicker2").on("change", function (e) {
    var endVal = $("#txt_filtro_fec2").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
  });
  
  $('#cmb_estado').change(function (e) {
    stockunidad_tabla();
  });


  $("#txt_fil_cliente_nom").autocomplete({
    minLength: 1,
    source: VISTA_URL + "cliente/cliente_autocomplete.php",
    select: function (event, ui) {
      $("#hdd_fil_cliente_id").val(ui.item.cliente_id);
    },
  });

  stockunidad_tabla();

});

var datatable_global;

function validarTC(){
  // console.log($("#hdd_tipo_cambio_venta").val())
  if( $.trim($("#hdd_tipo_cambio_venta").val()) == "" || parseFloat($("#hdd_tipo_cambio_venta").val()) == 0 )
  {
    alerta_warning("Información","Debe registrar el tipo de cambio");
    return 0;
  }
}

function ventavehiculo_form(action,stockunidad_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "stockunidad/stockunidad_vender.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      stockunidad_id: stockunidad_id,
      tipocambio: $("#hdd_tipo_cambio_venta").val(),
      tipocambio_compra: $("#hdd_tipo_cambio_venta").val()
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_stockunidad_venta_form").html(html);
      $("#div_modal_stockunidad_vender").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("div_modal_stockunidad_vender", 40);
      modal_height_auto("div_modal_stockunidad_vender");
      modal_hidden_bs_modal("div_modal_stockunidad_vender", "limpiar");
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      alerta_error("Advertencia",''+data.responseText);
    }
  });
}

// FUNCION INHABILITADA
// function stockunidad_form(credito_id, creditotipo_id, credito_vehpla) {
//   $.ajax({
//     type: "POST",
//     url: VISTA_URL + "stockunidad/stockunidad_form.php",
//     async: true,
//     dataType: "html",
//     data: {
//       action: "L", //leer
//       credito_id: credito_id,
//       creditotipo_id: creditotipo_id,
//       credito_vehpla: credito_vehpla,
//       precio_bas: 0,
//       precio_tot: 0,
//       vista: "stockunidad",
//     },
//     beforeSend: function () {
//       $("#h3_modal_title").text("Cargando Formulario");
//       $("#modal_mensaje").modal("show");
//     },
//     success: function (data) {
//       $("#modal_mensaje").modal("hide");
//       if (data != "sin_datos") {
//         $("#div_modal_stockunidad_form").html(data);
//         $("#modal_registro_stockunidad").modal("show");

//         //desabilitar elementos del form si es L (LEER)
//         form_desabilitar_elementos("form_stockunidad"); //funcion encontrada en public/js/generales.js
//         modal_hidden_bs_modal("modal_registro_stockunidad", "limpiar"); //funcion encontrada en public/js/generales.js
//         modal_height_auto("modal_registro_stockunidad"); //funcion encontrada en public/js/generales.js
//       } else {
//         //llamar al formulario de solicitar permiso
//         var modulo = "stockunidad";
//         var div = "div_modal_stockunidad_form";
//         permiso_solicitud(usuario_act, stockunidad_id, modulo, div); //funcion ubicada en public/js/permiso.js
//       }
//     },
//     complete: function (data) {},
//     error: function (data) {
//       alerta_error("EROR", data.responseText);
//       console.log(data.responseText);
//     },
//   });
// }

function ajustar_form(credito_id, tipo_id) {
  var usuario_act = 'L';
  $.ajax({
    type: "POST",
    url: VISTA_URL + "stockunidad/stockunidad_ajustar_form.php",
    async: true,
    dataType: "html",
    data: {
      credito_id: credito_id,
      tipo_id: tipo_id,
      vista: 'stockunidad'
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando ...");
      $("#modal_mensaje").modal("show");
      console.log("/Gasto/ Credito ID: "+credito_id+ " - Credito tipo ID: "+tipo_id);
    },
    success: function (html) {
      $("#div_modal_ajustar_form").html(html);
      $("#div_modal_ajustar").modal("show");
      $("#modal_mensaje").modal("hide");
      if (usuario_act == 'L'){
        form_desabilitar_elementos('form_stockunidad_ajustar');
      }
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("div_modal_ajustar"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("div_modal_ajustar", "limpiar");
      modal_width_auto("div_modal_ajustar", 60);
      modal_height_auto("div_modal_ajustar");
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      $("#modal_mensaje").modal("hide");
      alerta_error("Advertencia", "" + data.responseText);
    },
  });
}

function stockunidad_tabla() {

  $.ajax({
    type: "POST",
    url: VISTA_URL + "stockunidad/stockunidad_tabla.php",
    async: false,
    dataType: "html",
    data: $("#form_stockunidad_filtro").serialize(),
    beforeSend: function () {
      $("#div_stockunidad_tabla").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#div_stockunidad_tabla").html(html);
    },
    complete: function (data) {
      estilos_datatable();
    },
    error: function (data) {
      $("#cgv_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CRÉDITO: " + data.responseText
      );
    }
  });
}

function estilos_datatable() {
  datatable_global = $("#tbl_stockunidad").DataTable({
    pageLength: 50,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsqueda",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar:",
    },
    order: [[0,'DESC']],
    columnDefs: [{ targets: [1,3], orderable: true }],
  });
  datatable_texto_filtrar();
}

function datatable_texto_filtrar() {
  $('input[aria-controls*="tbl_stockunidad"]')
    .attr("id", "txt_datatable_fil")
    .attr("placeholder", "escriba para buscar");

  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

function stockunidad_separar(
  credito_id,
  creditotipo_id,
  credito_vehpla,
  marca,
  modelo,
  stockunidad_id,
  stockunidad_est
) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "stockunidad/stockunidad_separar_form.php",
    async: true,
    dataType: "html",
    data: {
      stockunidad_id: stockunidad_id,
      stockunidad_est: stockunidad_est,
      credito_id: credito_id,
      creditotipo_id: creditotipo_id,
      credito_vehpla: credito_vehpla,
      marca: marca,
      modelo: modelo,
      ventaseparar: $("#hdd_tipo_cambio_venta").val(),

    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_stockunidad_separar_form").html(html);
      $("#div_modal_stockunidad_separar").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("div_modal_stockunidad_separar", 30);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("div_modal_stockunidad_separar"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("div_modal_stockunidad_separar", "limpiar");
    },
  });
}

function ventavehiculo_anular(stock_id,placa) {
  Swal.fire({
    title: "¿Seguro que desea anular esta venta?",
    icon: "info",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "SI",
    showDenyButton: true,
    denyButtonText: "NO",
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#21ba45",
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "stockunidad/stockunidad_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: "anular", // PUEDE SER: L, I, M , E
          stock_id: stock_id, //el primero se manda al controller
          placa:placa,
        },
        beforeSend: function () {
          $("#h3_modal_title").text("Venta Anulada");
          $("#modal_mensaje").modal("show");
        },
        success: function (data) {
          $("#modal_mensaje").modal("hide");
          stockunidad_tabla();          
          if (parseInt(data.estado) == 1) {
            notificacion_success(data.mensaje);
          } else if (parseInt(data.estado) == 0) {
            console.log(data);
            alerta_warning("Advertencia", data.mensaje);
          }
        },
        complete: function (data) {
          console.log(data);
        },
        error: function (data) {
          alerta_error("ERROR", data.responseText);
          console.log(data);
        },
      });
    } else if (result.isDenied) {
      swal.close();
    }
  });
}

function historial_form(tabla_nom, tabla_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "historial/historial_form.php",
    async: true,
    dataType: "html",
    data: {
      tabla_nom: tabla_nom,
      tabla_id: tabla_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_stockunidad_historial_form").html(html);
      $("#modal_" + tabla_nom + "_historial_form").modal("show");
      $("#modal_mensaje").modal("hide");

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal(
        "modal_" + tabla_nom + "_historial_form",
        "limpiar"
      ); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_" + tabla_nom + "_historial_form"); //funcion encontrada en public/js/generales.js
    },
  });
}

// ---------- BLOQUEAR ANTICLICK Y CONTROL + i ---------- //

// $(function () {
//   $(document).bind("contextmenu", function (e) {
//       return false;
//   });
// });

// $(document).keydown(function (event) {
//   if (event.keyCode == 123) { // Prevent F12
//       return false;
//   } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
//       return false;
//   }
// });

// ---------- BLOQUEAR ANTICLICK Y CONTROL + i ---------- //

function balance_venta(action, ventavehiculo_id, stockunidad_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "balanceventa/balanceventa_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      ventavehiculo_id: ventavehiculo_id,
      stockunidad_id: stockunidad_id,
      vista: "balanceventa"
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_balanceventa_form").html(html);
      $("#modal_balanceventa_form").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("modal_balanceventa_form", 40);
      modal_height_auto("modal_balanceventa_form");
      modal_hidden_bs_modal("modal_balanceventa_form", "limpiar");
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      alerta_error("Advertencia",''+data.responseText);
    }
  });
}


function ver_balanceventa(action, balanceventa_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "balanceventa/balanceventa_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      balanceventa_id: balanceventa_id,
      vista: "balanceventa"
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_balanceventa_form").html(html);
      $("#modal_balanceventa_form").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("modal_balanceventa_form", 40);
      modal_height_auto("modal_balanceventa_form");
      modal_hidden_bs_modal("modal_balanceventa_form", "limpiar");
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      alerta_error("Advertencia",''+data.responseText);
    }
  });
}