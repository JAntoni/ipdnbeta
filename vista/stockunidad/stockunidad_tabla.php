<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  if (defined('VISTA_URL')) {
    require_once(APP_URL . "core/usuario_sesion.php");
    require_once(VISTA_URL . 'stockunidad/Stockunidad.class.php');
    require_once(VISTA_URL . 'ventavehiculo/Ventavehiculo.class.php');
    require_once(VISTA_URL . 'balanceventa/Balanceventa.class.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
  } else {
    require_once("../../core/usuario_sesion.php");
    require_once('../stockunidad/Stockunidad.class.php');
    require_once('../ventavehiculo/Ventavehiculo.class.php');
    require_once('../balanceventa/Balanceventa.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  require_once('../cliente/Cliente.class.php');

  $oVentavehiculo = new Ventavehiculo();
  $oStockunidad = new Stockunidad();
  $oCliente     = new Cliente();
  $oBalanceventa     = new Balanceventa();

  $filtro_estado = $_POST["cmb_estado"];
  $result = $oStockunidad->stockunidades_tabla($filtro_estado);
?>
<table id="tbl_stockunidad" class="table table-hover dataTable display">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">ID <br>STOCK DE UNIDAD</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">CRED. <br> ID / TIPO</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">CLIENTE</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">VEHICULO <br>Marca | Modelo | Clase | Tipo</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">NÚMERO <br>DE PLACA</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">AÑO VEHICULO / AÑO DEL MODELO</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">ESTADO</th>
      <th id="tabla_cabecera_fila" style="vertical-align: middle;" class="text-center">OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach ($result['data'] as $key => $value)
      {
        $ventavehiculo_fec = '';
        $tb_ventavehiculo_id = 0;
        $ventavehiculo_cliente_id = 0;
        $marca    = 'sin marca';
        $modelo   = 'sin modelo';
        $clase    = 'sin clase';
        $tipo     = 'sin tipo';
        $vehiculo = 'sin datos';
        $cliente_name = 'sin cliente';

        $stockunidad_id  = $value["tb_stockunidad_id"];
        $credito_id      = $value["tb_credito_id"];
        $creditotipo_id  = $value["tb_creditotipo_id"];
        $stockunidad_est = $value['tb_stockunidad_est'];
        $stockunidad_cliente_id = $value['tb_cliente_id'];
        $credito_vehpla = $value['tb_credito_vehpla'];
        
        $result2 = $oStockunidad->mostrar_vehiculo($credito_id,$creditotipo_id);
        if ($result2['estado'] == 1) {
          $credito_vehpla_data  = $value['tb_credito_vehpla'];
          $marca    = $result2['data']['tb_vehiculomarca_nom'];
          $modelo   = $result2['data']['tb_vehiculomodelo_nom'];
          $clase    = $result2['data']['tb_vehiculoclase_nom'];
          $tipo     = $result2['data']['tb_vehiculotipo_nom'];
          $vehiculo = $marca . ' | ' . $modelo . ' | ' . $clase . ' | ' . $tipo . '('. $credito_vehpla_data.' - '.$credito_id .')';
          $anio     = $result2['data']['tb_credito_vehano'] .' / '. $result2['data']['tb_credito_vehmode'];
        }
        $result2 = NULL;

        $result3 = $oVentavehiculo->consultar_datos_venta_vehiculo($credito_id, $creditotipo_id);
        if ($result3['estado'] == 1) {
          $ventavehiculo_fec        = mostrar_fecha($result3['data']['tb_ventavehiculo_fec']);
          $tb_ventavehiculo_id      = intval($result3['data']['tb_ventavehiculo_id']); 
          $ventavehiculo_cliente_id = intval($result3['data']['tb_cliente_id']);
        }        
      
        if( intval($stockunidad_cliente_id) != 0) {
          $consultarDatosCliente = $oCliente->mostrarUno($stockunidad_cliente_id); 
            $cliente_name = $consultarDatosCliente["data"]["tb_cliente_nom"];
          $consultarDatosCliente = null;
        }
        else {
          if(intval($ventavehiculo_cliente_id) != 0){
            $consultarDatosCliente = $oCliente->mostrarUno($ventavehiculo_cliente_id); 
              $cliente_name = $consultarDatosCliente["data"]["tb_cliente_nom"];
            $consultarDatosCliente = null;
          }
        }
        $result3 = NULL;

        $result4 = $oBalanceventa->consultar_balance_stockunidad($stockunidad_id);
        $balanceventa_id = 0;
        if ($result4['estado'] == 1) {
          $balanceventa_id  = $result4['data']['tb_balanceventa_id'];
        }
        $result4 = null;
      ?>
      <tr>
        <td id="tabla_fila" style="vertical-align: middle;" class="text-center"><?php echo $stockunidad_id; ?></td>
        <td id="tabla_fila" style="vertical-align: middle;" class="text-center"><?php echo $credito_id.' / '.$creditotipo_id; ?></td>
        <td id="tabla_fila" style="vertical-align: middle;"><?php echo $cliente_name; ?></td>
        <td id="tabla_fila" style="vertical-align: middle;"><?php echo $vehiculo; ?></td>
        <td id="tabla_fila" style="vertical-align: middle;" class="text-center"><?php echo $credito_vehpla; ?></td>
        <td id="tabla_fila" style="vertical-align: middle;" class="text-center"><?php echo $anio; ?></td>
        <td id="tabla_fila" style="vertical-align: middle;">
          <?php
            if ($stockunidad_est == 1)
            {
              $estado = '<span class="badge bg-green">Disponible</span>';
            }
            elseif ($stockunidad_est == 2)
            {
              $estado = '<span class="badge bg-yellow">Separado</span>'; 
            }
            elseif ($stockunidad_est == 3)
            {
              $estado = '<span class="badge bg-yellow">Vendido y pendiente de cobro | Día: ' . $ventavehiculo_fec . '</span>';
            }
            elseif ($stockunidad_est == 4)
            {
              if(empty($ventavehiculo_fec)){
                $estado = '<span class="badge badge-default"> Vendido al contado. </span>';
              } else{
                $estado = '<span class="badge badge-default">Vendido al contado | Nro. de Venta: '.$tb_ventavehiculo_id.' | Día: ' . $ventavehiculo_fec . ' </span>';
              }
            }
            elseif ($stockunidad_est == 5)
            {
              $estado = '<span class="badge bg-blue">Vendido al crédito. ID Crédito = ' . $credito_id . '</span>';
            }
            else
            {
              $estado = 'SIN ESTADO';
            }
            echo $estado;
          ?>
        </td>
        <td id="tabla_fila" style="vertical-align: middle;" class="text-left">
          <?php
            echo
            '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick=\'ajustar_form('.$credito_id.','.$creditotipo_id.')\'> <i class="fa fa-eye fa-fw"></i> Ver </a> &nbsp;';
            // elseif ($stockunidad_est == 0) NO DISPONIBLE O NO PREPARADO AUN / AJUSTADO
            if ($stockunidad_est == 1)
            {
              echo
              '<a class="btn btn-success btn-xs" onClick=\'ventavehiculo_form("vender",'.$stockunidad_id.')\'> <i class="fa fa-file-text fa-fw"></i> Vender </a> &nbsp;';
            }
            // elseif ($stockunidad_est == 3) PENDIENTE DE COBRO
            elseif ($stockunidad_est == 4 || $stockunidad_est == 5 )
            {
              echo
              '<a class="btn btn-danger btn-xs" onClick=\'ventavehiculo_anular('.$stockunidad_id.',"'.$credito_vehpla.'")\'> <i class="fa fa-ban fa-fw"></i> Anular Venta </a> &nbsp';
              if($balanceventa_id > 0){
                echo 
                '<a class="btn btn-warning btn-xs" onClick=\'ver_balanceventa("leer", '.$balanceventa_id.')\'> <i class="fa fa-eye fa-fw"></i> Balance </a> &nbsp;';
              }else{
                echo
                '<a class="btn btn-success btn-xs" onClick=\'balance_venta("insertar", '.$tb_ventavehiculo_id.', '.$stockunidad_id.')\'> <i class="fa fa-plus fa-fw"></i> Balance </a> &nbsp;';
              }
            }
            echo
            '<a class="btn btn-info btn-xs btn_historial" onClick=\'historial_form("tb_stockunidad",'.$stockunidad_id.')\'> <i class="fa fa-calendar-day fa-fw"></i> Historial </a>';
          ?>
        </td>
      </tr>
      <?php
    }
    $result = NULL;
    ?>
  </tbody>
</table>