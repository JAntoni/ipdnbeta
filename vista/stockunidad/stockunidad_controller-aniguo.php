<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once("../../core/usuario_sesion.php");
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../permiso/Permiso.class.php');
    require_once('../stockunidad/Stockunidad.class.php');
    require_once '../ingreso/Ingreso.class.php';
    require_once '../egreso/Egreso.class.php';
    require_once('../deposito/Deposito.class.php');
    require_once('../ventavehiculo/Ventavehiculo.class.php');
    require_once('../historial/Historial.class.php');
    // require_once('../monedacambio/Monedacambio.class.php');
    // $oMonedaCambio = new Monedacambio();

    $oPermiso = new Permiso();
    $oStockunidad = new Stockunidad();
    $oIngreso = new Ingreso();
    $oEgreso = new Egreso();
    $oDeposito = new Deposito();
    $oVenta = new Ventavehiculo();
    $oHist = new Historial();

    $action = $_POST['action'];

    if ($action == 'anular') {
        try {
            $fecha_actual   = date("Y-m-d");
            $placa          = $_POST['placa'];
            $stockunidad_id = $_POST['stock_id'];

            $result = $oStockunidad->mostrarUno($stockunidad_id);

            $resultValidarExisteVenta = $oVenta->anularVenta_validar_exite_venta($placa, $stockunidad_id);

            if ($resultValidarExisteVenta["estado"] == true) {

                // -------------- CONSULTAR SI HAY SEPARACIÓN -------------- //
                $validacion_moneda_padre = $result['data']['tb_moneda_id'];
                $result = null;
                $suma_total_ingresos = 0;
                $mod_id = 62; // INGRESO POR SEPARACIÓN DE VEHICULO
                $consultarDatosIngresosPrevios = $oIngreso->mostrar_por_modulo($mod_id, $stockunidad_id, 0, 0);
                if ($consultarDatosIngresosPrevios['estado'] == 1) {
                    foreach ($consultarDatosIngresosPrevios['data'] as $key => $value2) {
                        if ($validacion_moneda_padre == 1) {
                            if ($value2['tb_moneda_id'] == 1) {
                                $suma_total_ingresos += formato_numero($value2['tb_ingreso_imp']);
                            } elseif ($value2['tb_moneda_id'] == 2) {
                                $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] * $value2['tb_ingreso_tipcam']));
                            } else {
                                $suma_total_ingresos += 0;
                            }
                        } elseif ($validacion_moneda_padre == 2) {
                            if ($value2['tb_moneda_id'] == 1) {
                                $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] / $value2['tb_ingreso_tipcam']));
                            } elseif ($value2['tb_moneda_id'] == 2) {
                                $suma_total_ingresos += formato_numero($value2['tb_ingreso_imp']);
                            } else {
                                $suma_total_ingresos += 0;
                            }
                        } else {
                            $suma_total_ingresos = 0;
                        }
                    }
                }
                $consultarDatosIngresosPrevios = null;
                // -------------- CONSULTAR SI HAY SEPARACIÓN -------------- //

                $ventavehiculo_mon = formato_numero($resultValidarExisteVenta["data"]["tb_ventavehiculo_mon"]);

                $suma_validar = formato_numero( ($ventavehiculo_mon + $suma_total_ingresos) );

                if ($suma_validar > 0) {
                    $data['estado'] = 0;
                    $data['mensaje'] = 'Existen ingresos previos y una venta registrada que deben anularse primero. <br>N° de Placa: ' . $placa . ' <br>Fecha de Venta: ' . $resultValidarExisteVenta["data"]["tb_ventavehiculo_fec"] . ' <br>Monto: ' . $resultValidarExisteVenta["data"]["tb_ventavehiculo_mon"];
                } else {
                    if ($oVenta->anularVenta_anular($stockunidad_id)) {
                        $data['estado'] = 1;
                        $data['mensaje'] = 'Venta anulada correctamente.';
                    }
                }
            }
            else {
                $data['estado']  = 0;
                $data['mensaje'] = 'No se encotró ninguna venta asociada, consulte con soporte técnico.';
            }
            $resultValidarExisteVenta = null;
        }
        catch (\Throwable $th)
        {
            $data['estado']  = 0;
            $data['mensaje'] = $th->getMessage();
        }
        echo json_encode($data);

    } elseif ($action == 'ajustar_validar') {
        $credito_id     = $_POST["hdd_credito_id"];
        $creditotipo_id = $_POST["hdd_tipo_id"];
        $array_ajustar  = array();

        $resultado = $oStockunidad->ajustar_consultar_existe($credito_id, $creditotipo_id);

        if ($resultado['estado'] == 1) {
            $array_ajustar['estado']  = 1;
            $array_ajustar['mensaje'] = $resultado['mensaje'];
            $array_ajustar['datos']   = $resultado['data'];
        } else {
            $array_ajustar['estado']  = 0;
            $array_ajustar['mensaje'] = "Sin resultados";
        }

        echo json_encode($array_ajustar);

    } elseif ($action == 'ajustar_update') {
        $array_ajustar = array();
        $jsonDetalle  = $_POST["p_detalle"];
        parse_str($_POST["p_array_datos"], $datosFrm);

        $credito_id                  = $datosFrm["hdd_credito_id"];
        $creditotipo_id              = $datosFrm["hdd_tipo_id"];
        $stockunidad_montoresolucion = $datosFrm["txt_stockunidad_montoresolucion"];
        $stockunidad_monbas          = formato_numero($datosFrm["txt_stockunidad_monbas"]);
        $moneda_id                   = $datosFrm["cmb_moneda_id"];
        $stockunidad_soatval         = $datosFrm["txt_stockunidad_soatval"];
        $stockunidad_revtecval       = $datosFrm["txt_stockunidad_revtecval"];
        $stockunidad_kil             = $datosFrm["txt_stockunidad_kil"];
        $stockunidad_tarjeta         = $datosFrm["cmb_stockunidad_tarjeta"];
        $json_detalle                = $_POST["p_detalle"];        

        $appendDetalle = "";
        $arrayDataDetalle = json_decode($json_detalle);
        foreach ($arrayDataDetalle as $key => $value) {
            $appendDetalle .= $value->descripcion.'/&/&/';
        }
        // $appendDetalle = substr($appendDetalle,0,-5);

        $resultado = $oStockunidad->ajustar_consultar_existe($credito_id,$creditotipo_id);

        if ($oStockunidad->ajustar_actualizar(
            $credito_id,
            $creditotipo_id,
            $stockunidad_soatval,
            $stockunidad_revtecval,
            $stockunidad_kil,
            $stockunidad_tarjeta,
            $appendDetalle,
            $moneda_id,
            $stockunidad_monbas
        ) == true) {
            $array_ajustar['estado'] = 1;
            $array_ajustar['mensaje'] = "Proceso de ajuste realizado correctamente";
        } else {
            $array_ajustar['estado'] = 0;
            $array_ajustar['mensaje'] = "Se presentó un inconveniente al momento de realizar el proceso de ajuste: Actualizar información. <br> Error SQL: StockUnidad.class :: ajustar_actualizar() :: linea: 545";
        }

        echo json_encode($array_ajustar);

    } elseif ($action == 'preparar_registrar') {
        
        $credito_id       = intval($_POST["hdd_credito_id"]);
        $creditotipo_id   = intval($_POST["hdd_tipo_id"]);
        $creditomoneda_id = intval($_POST["txt_moneda_credito_id"]);
        $credito_vehpla   = trim($_POST["hdd_numero_placa"]);
        $credito_cuoven   = formato_moneda($_POST["txt_credito_cuoven1"]);
        $credito_cap      = formato_moneda($_POST["txt_credito_cap1"]);
        $credito_gasto    = formato_moneda($_POST["txt_credito_gasto1"]);
        $montoresolucion  = formato_numero( $credito_cuoven + $credito_cap + $credito_gasto);   
        $vista            = trim($_POST["vista"]);
        $array_preparar   = array();

        $oStockunidad->tb_credito_id       = $credito_id;
        $oStockunidad->tb_creditotipo_id   = $creditotipo_id;
        $oStockunidad->tb_creditomoneda_id = $creditomoneda_id;
        $oStockunidad->tb_credito_vehpla   = $credito_vehpla;
        $oStockunidad->tb_credito_cuoven1  = $credito_cuoven;
        $oStockunidad->tb_credito_cap1     = $credito_cap;
        $oStockunidad->tb_credito_gasto1   = $credito_gasto;
        // $oStockunidad->tb_credito_cuoven2  = $credito_cuoven;
        // $oStockunidad->tb_credito_cap2     = $credito_cap;
        // $oStockunidad->tb_credito_gasto2   = $credito_gasto;
        
        if($vista = 'gasto')
        {
            $consultarExisteData = $oStockunidad->ajustar_consultar_existe($credito_id, $creditotipo_id);  // VALIDAMOS QUE EXISTA 
            if( $consultarExisteData["estado"] == 0 ) // NO -> REGISTRAR
            {
                if ($oStockunidad->preparar_insertar() == true) {
                    $array_preparar['estado'] = 1;
                    $array_preparar['mensaje'] = "Proceso de preparacion registrados correctamente";
                } else {
                    $array_preparar['estado'] = 0;
                    $array_preparar['mensaje'] = "Se presentó un inconveniente al momento de realizar el proceso de prepación: Registrar. <br> Error SQL: StockUnidad.class :: preparar_insertar() :: linea: 20";
                }
            }
            else                                      // SI - ACTUALIZAR
            {
                if ($oStockunidad->preparar_actualizar01() == true) {
                    $array_preparar['estado'] = 1;
                    $array_preparar['mensaje'] = "Proceso de preparacion actualizados correctamente";
                } else {
                    $array_preparar['estado'] = 0;
                    $array_preparar['mensaje'] = "Se presentó un inconveniente al momento de realizar el proceso de prepación: Actualizar. <br> Error SQL: StockUnidad.class :: preparar_actualizar01() :: linea: 53";
                }
            }
            $consultarExisteData = null;            
        }
        // elseif( $vista == 'stockunidad')
        // {
        //     if ($oStockunidad->preparar_actualizar02() == true) {
        //         $array_preparar['estado'] = 1;
        //         $array_preparar['mensaje'] = "Datos de ejecución guardados correctamente";
        //     } else {
        //         $array_preparar['estado'] = 0;
        //         $array_preparar['mensaje'] = "Se presentó un inconveniente al momento de guadar los datos de ejecución en el segundo momento. <br> Error SQL: StockUnidad.class :: preparar_actualizar02() :: linea: 82";
        //     }
        // }
        else
        {
            $array_preparar_error = array();
            $array_preparar_error["estado"]  = 0;
            $array_preparar_error["mensaje"] = "Vista invalida";
            echo json_encode($array_preparar_error);
            exit;
        }

        echo json_encode($array_preparar);

    } elseif ($action == 'preparar_registrar_momento02') {
        $preparar_registrar_momento02 = array();

        $credito_id       = intval($_POST["hdd_credito_id"]);
        $creditotipo_id   = intval($_POST["hdd_tipo_id"]);
        $creditomoneda_id = intval($_POST["txt_moneda_credito_id"]);
        $credito_vehpla   = trim($_POST["hdd_numero_placa"]);
        $credito_cuoven   = formato_moneda($_POST["txt_credito_cuoven2"]);
        $credito_cap      = formato_moneda($_POST["txt_credito_cap2"]);
        $credito_gasto    = formato_moneda($_POST["txt_credito_gasto2"]);
        $vista            = trim($_POST["vista"]);

        $oStockunidad->tb_credito_id       = $credito_id;
        $oStockunidad->tb_creditotipo_id   = $creditotipo_id;
        $oStockunidad->tb_creditomoneda_id = $creditomoneda_id;
        $oStockunidad->tb_credito_vehpla   = $credito_vehpla;
        $oStockunidad->tb_credito_cuoven2  = $credito_cuoven;
        $oStockunidad->tb_credito_cap2     = $credito_cap;
        $oStockunidad->tb_credito_gasto2   = $credito_gasto;

        if ($oStockunidad->preparar_actualizar02() == true) {
            $preparar_registrar_momento02['estado'] = 1;
            $preparar_registrar_momento02['mensaje'] = "Datos de ejecución guardados correctamente";
        } else {                              // SI - ACTUALIZAR
            $preparar_registrar_momento02['estado'] = 0;
            $preparar_registrar_momento02['mensaje'] = "Se presentó un inconveniente al momento de guadar los datos de ejecución en el segundo momento. <br> Error SQL: StockUnidad.class :: preparar_actualizar02() :: linea: 82";
        }

        echo json_encode($preparar_registrar_momento02);

    } elseif ($action == 'vender_registrar') {

        $validarMetodosPagos            = array(1,2);
        $validation                     = false;
        $empresa_id                     = $_SESSION['empresa_id'];
        parse_str($_POST["p_array_datos"], $datosFrm);
        $stockunidad_id                 = $datosFrm["hdd_stockunidad_id"];
        $vehiculo_pla                   = $datosFrm["hdd_numero_placa"];
        $credito_id                     = $datosFrm["hdd_credito_id"];
        $creditotipo_id                 = $datosFrm["hdd_creditotipo_id"];
        $ventavehiculo_fec              = $datosFrm["txt_ventavehiculo_fec"];
        $ventavehiculo_tipcam           = $datosFrm["txt_ventavehiculo_tipcam"];
        $ventavehiculo_bas              = formato_numero($datosFrm["hdd_precio_base"]);      // txt_ventavehiculo_bas
        $moneda_id_padre                = $datosFrm["cmb_moneda_id_padre"];
        $moneda_id_conversion           = $datosFrm["cmb_moneda_id_conversion"];
        $cliente_id                     = $datosFrm["hdd_cliente_id"];                       // txt_cliente_id --> name
        $usuario_id2_name               = $datosFrm["txt_usuario_id2"];                      // ASESOR NAME
        $usuario_id2_id                 = $datosFrm["hdd_usuario_id2"];                      // ASESOR ID
        $usuario_id_name                = $datosFrm["txt_usuarioautoriza_id"];               // USUARIO NAME
        $usuario_id                     = $datosFrm["hdd_usuarioautoriza_id"];               // USUARIO ID
        $metodo_pago                    = $datosFrm["cmb_metodo_pago"];
        $monto_previos                  = formato_numero($datosFrm["hdd_monto_previos"]);     // txt_monto_previos
        $saldo_a_pagar                  = formato_numero($datosFrm["hdd_saldo_a_pagar"]);     // txt_saldo_a_pagar
        $ventavehiculo_mon              = formato_numero($datosFrm["hdd_ventavehiculo_mon"]); // txt_ventavehiculo_mon
        $suma_ing_prev_mont_pagar       = formato_numero(($monto_previos + $ventavehiculo_mon));
        $descripMonedaConver            = "";
        $descripMetodoPago              = "";

        if (in_array($metodo_pago, $validarMetodosPagos))
        {
            if( empty($ventavehiculo_tipcam) || $ventavehiculo_tipcam == 0 ){
                $data = array();
                $data['estado']  = 0;
                $data['mensaje'] = 'Tipo de cambio no obtenido: '.formato_moneda($ventavehiculo_tipcam);
                echo json_encode($data);
                exit();
            }

            if ($moneda_id_padre == "SOLES") { $moneda_id_padre = 1; } else { $moneda_id_padre = 2; }

            if ($moneda_id_conversion == 1) { $descripMonedaConver = "SOLES"; } else { $descripMonedaConver = "DOLARES"; }

            if ($metodo_pago == 1) { $descripMetodoPago = "EFECTIVO"; } else { $descripMetodoPago = "TRANSFERENCIA BANCARIA"; }

            $array_vender         = array();
            $fecha_deposito       = '';
            $banco_id             = 0;
            $cuedep_id            = 0;
            $numoperacion         = '';
            $montodeposito        = 0;
            $combanco             = 0;
            $montopag             = 0;
            $subaction            = '';
            $monto_sobre_costo    = 0;
            $mensaje              = "";
            $mensajeIngresoEgreso = "";
            $ventavehiculo_xac    = 1; // 1 = VENDIDO || 0 = POR COBRAR

            $oHist->setTbHistUsureg($_SESSION['usuario_id']);
            $oHist->setTbHistNomTabla('tb_stockunidad');
            $oHist->setTbHistRegmodid($stockunidad_id); // Asumiendo que insertar() devuelve el ID de la nueva llave

            if ($ventavehiculo_mon < $saldo_a_pagar)
            {
                $subaction = "separar";
            }
            else
            {
                if($ventavehiculo_mon >= $saldo_a_pagar)
                {
                    if ($ventavehiculo_mon == $saldo_a_pagar)
                    {
                        $subaction = "completo";
                    }
                    else
                    {
                        $subaction = "sobrecosto";
                        $monto_sobre_costo = formato_numero(($ventavehiculo_mon - $ventavehiculo_bas));
                    }
                }
            }

            if ($metodo_pago != 1) // 1 = EFECTIVO || 2 = Transferencia Bancaria
            {
                $validation      = false;

                $descripBanco_id = "";
                $fecha_deposito  = $datosFrm["txt_fecha_deposito"];
                $banco_id        = $datosFrm["cmb_banco_id"];
                $descripValidatorBancoId = $datosFrm["hdd_cuenta_dep"];
                $cuedep_id       = $datosFrm["cmb_cuedep_id"];
                $numoperacion    = $datosFrm["txt_numoperacion"];
                $montodeposito   = $datosFrm["txt_montodeposito"];
                $combanco        = $datosFrm["txt_comisionbanco"];
                $montopag        = $datosFrm["txt_montoDepositoValido"];

                if( empty($numoperacion) )
                {
                    $data = array();
                    $data['estado']  = 0;
                    $data['mensaje'] = 'Debe ingresar el número de operación';
                    echo json_encode($data);
                    exit();
                }
                elseif( empty($fecha_deposito) )
                {
                    $data = array();
                    $data['estado']  = 0;
                    $data['mensaje'] = 'Debe ingresar la fecha del depósito';
                    echo json_encode($data);
                    exit();
                }
                elseif( empty($banco_id) )
                {
                    $data = array();
                    $data['estado']  = 0;
                    $data['mensaje'] = 'Debe seleccionar el <b>banco</b> para el número de cuenta:<br>'.$descripValidatorBancoId;
                    echo json_encode($data);
                    exit();
                }
                else
                {
                    if( $banco_id == 1 ){ $descripBanco_id = "BCP"; } elseif( $banco_id == 2 ){ $descripBanco_id = "Interbank"; } else { $descripBanco_id = "BBVA"; }

                    if( empty($combanco) ){ $combanco = formato_moneda(0); }

                    $mensajeIngresoEgreso = 'Registro por separación: <br>Placa del vehículo: '.$vehiculo_pla. ', Monto abonado: '.$ventavehiculo_mon. ', Tipo de cambio: '.$ventavehiculo_tipcam.
                            ', Moneda de conversión: '.$descripMonedaConver. ', Método de pago: '.$descripMetodoPago.
                            ', Fecha de deposito: '.mostrar_fecha($fecha_deposito). ', Banco: '.$descripBanco_id. ', N° Operación: '.$numoperacion. ', Monto Depósito: '.$montodeposito. 
                            ', Comisión del banco: '.$combanco;

                    $mensaje = 'Registro por separación: <br>Placa del vehículo: '.$vehiculo_pla.
                                '<br>Monto abonado: '.$ventavehiculo_mon.
                                '<br>Tipo de cambio: '.$ventavehiculo_tipcam.
                                '<br>Moneda de conversión: '.$descripMonedaConver.
                                '<br>Método de pago: '.$descripMetodoPago. 
                                '<ul><li>Fecha de deposito: '.mostrar_fecha($fecha_deposito).'</li><li>Banco: '.$descripBanco_id.'</li><li>N° Operación: '.$numoperacion.'</li><li>Monto Depósito: '.$montodeposito.'</li><li>Comisión del banco: '.$combanco.'</li></ul>';
                    
                    $deposito_mon   = 0;
                    $result = $oDeposito->validar_no_operacion_fecha($numoperacion, $fecha_deposito);
                    if ($result['estado'] == 1) {
                        foreach ($result['data'] as $key => $value) {
                            $deposito_mon += floatval($value['tb_deposito_mon']);
                        }
                    }
                    $result = NULL;
        
                    if ($deposito_mon == 0) {
                        $data = array();
                        $data['estado']  = 0;
                        $data['mensaje'] = 'El número de operación que ingresaste no se encuentra asociado a la Cuenta seleccionada o la fecha de depósito no corresponde, consulta con ADMINISTRACIÓN por favor.';
                        echo json_encode($data);
                        exit();
                    }
                    
                    $deposito_mon = formato_numero($deposito_mon);

                    $ingreso_importe = 0;
                    $dts = $oIngreso->lista_ingresos_num_operacion($numoperacion); //OBTIENE TODOS LOS INGRESOS USANDO EL NUMERO DE OPERACION, ES DECIR EL SALDO USADO

                    if ($dts['estado'] == 1) {
                        foreach ($dts['data'] as $key => $dt) {
                            $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
                            $ingreso_mon_id = intval($dt['tb_moneda_id']);

                            if( $moneda_id_padre == 1 )  // SOLES - DÓLARES
                            {
                                if( $ingreso_mon_id == 1 )
                                {
                                    $ingreso_importe += $tb_ingreso_imp;
                                }
                                elseif( $ingreso_mon_id == 2 )
                                {
                                    $tb_ingreso_imp = ($tb_ingreso_imp * $ventavehiculo_tipcam);
                                }
                                else
                                {
                                    $ingreso_importe += 0;
                                }
                            }
                            elseif( $moneda_id_padre == 2 )  // DÓLARES - SOLES
                            {
                                if( $ingreso_mon_id == 1 )
                                {
                                    $tb_ingreso_imp = ($tb_ingreso_imp / $ventavehiculo_tipcam);
                                }
                                elseif( $ingreso_mon_id == 2 )
                                {
                                    $ingreso_importe += $tb_ingreso_imp;
                                }
                                else
                                {
                                    $ingreso_importe += 0;
                                }
                            }
                            else
                            {
                                $ingreso_importe = 0;
                            }                            
                        }
                    }
                    $dts = null;

                    $monto_usar = $deposito_mon - $ingreso_importe; // Es el monto que tiene disponible para poder usar en otro pago
                    $monto_usar = formato_moneda($monto_usar); //FONDOS DISPONIBLES

                    //SI EL MONTO DEPOSITO ES MAYOR QUE LOS FONDOS DISPONIBLES LANZA UNA ALERTA Y DETIENE EL PROCESO
                    if( formato_numero($montodeposito) > formato_numero($monto_usar) )
                    {
                        $data['estado'] = 0;
                        $data['mensaje'] = 'El MONTO ingresado es mayor que el MONTO disponible, <br>no puede pagar mas de lo depositado. <br>Monto ingresado: ' . mostrar_moneda($montodeposito) . 
                            ', monto disponible: ' . mostrar_moneda($monto_usar) . ', <br>TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . 
                            ' | validar: ' . $montodeposito . ' | usar: ' . $monto_usar . ' | cambio: ' . $ventavehiculo_tipcam .
                            ' | ingre: ' . $ingreso_importe  . ' | mond ing: ' . $moneda_id_conversion;
                        echo json_encode($data);
                        exit();
                    }

                }

                $validation = true;
            }
            else
            {
                $mensajeIngresoEgreso = 'Registro por separación: <br>Placa del vehículo: '.$vehiculo_pla. ', Monto abonado: '.$ventavehiculo_mon. ', Tipo de cambio: '.$ventavehiculo_tipcam.
                            ', Moneda de conversión: '.$descripMonedaConver. ', Método de pago: '.$descripMetodoPago;

                $mensaje = 'Registro por separación: <br>Placa del vehículo: '.$vehiculo_pla.
                            '<br>Monto abonado: '.$ventavehiculo_mon.
                            '<br>Tipo de cambio: '.$ventavehiculo_tipcam.
                            '<br>Moneda de conversión: '.$descripMonedaConver.
                            '<br>Método de pago: '.$descripMetodoPago;

                $validation = true;
            }

            $set_tbcuentadepositoid = 0;

            if( $metodo_pago == 1 && $moneda_id_conversion == 1)
            {
                $set_tbcuentadepositoid = 18; // CUENTA VENTA VEHICULAR SOLES
            }
            elseif( $metodo_pago == 1 && $moneda_id_conversion == 2)
            {
                $set_tbcuentadepositoid = 19; // CUENTA VENTA VEHICULAR DOLARES
            }
            elseif( $metodo_pago == 2 && $moneda_id_conversion == 1)
            {
                $set_tbcuentadepositoid = 18; // CUENTA VENTA VEHICULAR DOLARES
            }
            else
            {
                $set_tbcuentadepositoid = 19;
            }

            // ---------------------- DATOS VENTA VEHICULO ------------------------- //

            $oVenta->ventavehiculo_xac  = $ventavehiculo_xac;
            $oVenta->ventavehiculo_fec  = $ventavehiculo_fec;
            $oVenta->vehiculo_pla       = $vehiculo_pla;
            $oVenta->moneda_id          = $moneda_id_conversion;
            $oVenta->montobase          = $ventavehiculo_bas;
            $oVenta->montototal         = $ventavehiculo_mon;
            $oVenta->tipcam             = $ventavehiculo_tipcam;
            $oVenta->montosobr          = $monto_sobre_costo;
            $oVenta->metpag             = $metodo_pago;
            $oVenta->usuario_id         = $usuario_id;
            $oVenta->usuario_id2        = $usuario_id2_id;
            $oVenta->cliente            = $cliente_id;
            $oVenta->credito_id         = $credito_id;
            $oVenta->creditotipo_id     = $creditotipo_id;
            $oVenta->vehiculo_est       = 1;                 // 0 por cobrar - 1 vendido
            $oVenta->empresa_id         = $empresa_id;
            $oVenta->stockunidad        = $stockunidad_id;

            // ---------------------- DATOS VENTA VEHICULO ------------------------- //

            // ---------------------- DATOS INGRESOS ------------------------- //

            $oIngreso->ingreso_usureg    = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod    = intval($_SESSION['usuario_id']);
            // $oIngreso->ingreso_xac       = 1;
            $oIngreso->ingreso_fec       = date('Y-m-d');
            $oIngreso->documento_id      = 8;                // OTROS INGRESOS 
            $oIngreso->ingreso_numdoc    = 0;                // UPDATE AUTOMARICO
            $oIngreso->ingreso_det       = 'INGRESO POR: '.$mensajeIngresoEgreso;
            $oIngreso->ingreso_imp       = moneda_mysql($ventavehiculo_mon);
            // $oIngreso->ingreso_est       = 1;
            $oIngreso->cliente_id        = $cliente_id;
            $oIngreso->caja_id           = 1;                // CAJA
            $oIngreso->moneda_id         = $moneda_id_conversion;
            $oIngreso->ingreso_modide    = $stockunidad_id;
            $oIngreso->empresa_id        = $empresa_id;
            $oIngreso->ingreso_fecdep    = $fecha_deposito;
            $oIngreso->ingreso_numope    = $numoperacion;
            $oIngreso->ingreso_mondep    = $montodeposito;
            $oIngreso->ingreso_comi      = $combanco;
            $oIngreso->cuentadeposito_id = $cuedep_id;
            $oIngreso->banco_id          = $banco_id;
            $oIngreso->ingreso_ap        = 0;                // 1 = ACUERDO DE PAGO  | 0 = INGRESO NORMAL
            // $oIngreso->ingreso_mot       = 1;                // 0 = retenciones, motivo 1: para pagos tramites, 2: pagos de seguros
            // $oIngreso->ingreso_pro       = 0.00;             // monto de prorrateo cuando se liquida o amortiza un crédito
            // $oIngreso->ingreso_detex     = '';               // ADD DETALLE SEPARADO POR ||
            // $oIngreso->ingreso_tipcam    = $ventavehiculo_tipcam;
            // $oIngreso->ingreso_array     = '-//---';

            // ---------------------- DATOS INGRESOS ------------------------- //

            // ---------------------- DATOS EGRESOS ------------------------- //

            if ($moneda_id_conversion == 1) // SOLES
            {
                $validacion_egreso_subcuenta_id = 71;
            } else {
                $validacion_egreso_subcuenta_id = 72;
            }

            $oEgreso->egreso_usureg     = intval($_SESSION['usuario_id']);
            $oEgreso->egreso_fec        = date('Y-m-d');
            $oEgreso->documento_id      = 9;                                // OTROS EGRESOS
            $oEgreso->egreso_numdoc     = 0;                                // UPDATE AUTOMARICO
            $oEgreso->egreso_det        = 'EGRESO POR: '.$mensajeIngresoEgreso;
            $oEgreso->egreso_imp        = moneda_mysql($ventavehiculo_mon);
            $oEgreso->egreso_tipcam     = 0;                                // TIPO DE CAMBIO DE VENTA ES 0
            $oEgreso->egreso_est        = 1;
            $oEgreso->cuenta_id         = 28;                               // POR DEFECTO
            $oEgreso->subcuenta_id      = $validacion_egreso_subcuenta_id;  // VARIABLE 71 O 72
            $oEgreso->proveedor_id      = 1;                                // POR DEFECTO
            $oEgreso->cliente_id        = 0;                                // NO VA
            $oEgreso->usuario_id        = 0;                                // NO VA
            $oEgreso->caja_id           = 1;                                // CAJA = 1
            $oEgreso->moneda_id         = $moneda_id_conversion;            // MONEDA EN QUE PAGA
            $oEgreso->modulo_id         = 61;                               // 61 fijo defecto
            $oEgreso->egreso_modide     = $stockunidad_id;                  // id del stockunidad
            $oEgreso->empresa_id        = $empresa_id;
            $oEgreso->egreso_ap         = 0;                                // ACUERDO DE PAGO

            // ---------------------- DATOS EGRESOS ------------------------- //


            if ($subaction == "separar")  // SÓLO REGISTRAR EL INGRESO
            {
                $oIngreso->modulo_id       = 62;
                $oIngreso->cuenta_id       = 37;
                $oIngreso->subcuenta_id    = 188;

                if( $oStockunidad->separar_actualizar_cliente_id_userasesor_userautoriza($stockunidad_id,$cliente_id,$usuario_id2_id,$usuario_id) == true )
                {
                    $resultadoRegistrarIngreso = $oIngreso->insertar();

                    if ($metodo_pago == 1) // EFECTIVO  ---> sólo registrar ingresos
                    {
                        if ($resultadoRegistrarIngreso['estado'] == true)
                        {
                            $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                            if ($resultadoMofificacionIngreso == true)
                            {
                                // ----------- REGISTRAMOS EL HISTORIAL ----------- //

                                $oHist->setTbHistDet($mensaje);
                                $oHist->insertar();

                                // ----------- REGISTRAMOS EL HISTORIAL ----------- //

                                $array_vender["estado"]  = 1;
                                $array_vender["mensaje"] = "Separación registrada correctamente";
                            }
                            else
                            {
                                $array_vender["estado"]  = 0;
                                $array_vender["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                            }
                        }
                        else
                        {
                            $array_vender["estado"]  = 0;
                            $array_vender["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                        }
                    }
                    else
                    {
                        if ($resultadoRegistrarIngreso['estado'] == true)
                        {
                            $oHist->setTbHistDet($mensaje);
                            $oHist->insertar();

                            $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                            if ($resultadoMofificacionIngreso == true)
                            {
                                $resultadoRegistrarEgreso = $oEgreso->insertar();
        
                                if ($resultadoRegistrarEgreso['estado'] == true)
                                {
                                    $array_vender["estado"]  = 1;
                                    $array_vender["mensaje"] = "Separación registrada correctamente";
                                }
                                else
                                {
                                    $array_vender["estado"]  = 0;
                                    $array_vender["mensaje"] = "Se presentó un inconveniente al registrar el egreso. <br> Error SQL: Egreso.class :: insertar() :: linea: 30";
                                }
                            }
                            else
                            {
                                $array_vender["estado"]  = 0;
                                $array_vender["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                            }
                        }
                        else
                        {
                            $array_vender["estado"]  = 0;
                            $array_vender["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                        }
                    }
        
                    $resultadoRegistrarIngreso = null;
                }
                else
                {
                    $array_vender["estado"]  = 0;
                    $array_vender["mensaje"] = "Error al actualizar el cliente por separación de la unidad vehicular.";
                }

            }
            elseif ($subaction == "sobrecosto")
            {
                $resultaDataValidate = $oStockunidad->validar_montos02_esten_ingresados($credito_id,$creditotipo_id);

                if( ! isset($resultaDataValidate['data']['tb_credito_cuoven2']) || $resultaDataValidate['data']['tb_credito_cuoven2'] == 0 || 
                    ! isset($resultaDataValidate['data']['tb_credito_cap2'])    || $resultaDataValidate['data']['tb_credito_cap2']    == 0 )
                    // ! isset($resultaDataValidate['data']['tb_credito_gasto2'])  || $resultaDataValidate['data']['tb_credito_gasto2']  == 0 
                {
                    $array_vender["estado"]  = 0;
                    $array_vender["mensaje"] = "Proceso de venta detenido, está PENDIENTE guardar los valores de: Capital, Cuotas Vencidas y Gastos para el segundo momento.";
                }
                else
                {
                    $resultadoRegistrarVenta = $oVenta->insertar();

                        if ($resultadoRegistrarVenta["estado"] == true) {

                            $oHist->setTbHistDet($mensaje);
                            $oHist->insertar();

                            $consultarDataDeposito = $oDeposito->calcularProximoDpositoID();
                            
                            if( $consultarDataDeposito["estado"] == 1 )
                            {
                                $array_vender["estado"]  = 1;
                                $array_vender["mensaje"] = "Nueva venta " . $resultadoRegistrarVenta["ventavehiculo_id"] . " registrada correctamente";
                            }
                            else
                            {
                                $array_vender['estado']  = 0;
                                $array_vender['mensaje'] = 'Se presentó un inconveniente al consultar nuevo id. <br> Error SQL: Deposito.class :: insertar() :: linea: 55';
                            }
                        } else {
                            $array_vender["estado"]  = 0;
                            $array_vender["mensaje"] = "Se presentó un inconveniente al registrar la venta. <br> Error SQL: Venta.class :: insertar() :: linea: 30";
                        }
                        
                    $resultadoRegistrarVenta = null;
                }

                $resultaDataValidate = null;
            }
            else  // venta completa
            {
                $resultaDataValidate = $oStockunidad->validar_montos02_esten_ingresados($credito_id,$creditotipo_id);

                    if( ! isset($resultaDataValidate['data']['tb_credito_cuoven2']) || $resultaDataValidate['data']['tb_credito_cuoven2'] == 0 || 
                        ! isset($resultaDataValidate['data']['tb_credito_cap2'])    || $resultaDataValidate['data']['tb_credito_cap2']    == 0 ) 
                        // ! isset($resultaDataValidate['data']['tb_credito_gasto2'])  || $resultaDataValidate['data']['tb_credito_gasto2']  == 0
                    {
                        $array_vender["estado"]  = 0;
                        $array_vender["mensaje"] = "Proceso de venta detenido, está PENDIENTE guardar los valores de: Capital, Cuotas Vencidas y Gastos para el segundo momento.";
                    }
                    else
                    {
                        $resultadoRegistrarVenta = $oVenta->insertar();

                            if ($resultadoRegistrarVenta["estado"] == true)
                            {
                                $oIngreso->modulo_id       = 10;
                                $oIngreso->cuenta_id       = 40;
                                $oIngreso->subcuenta_id    = 133;
                                $resultadoRegistrarIngreso = $oIngreso->insertar();
                
                                    if ($metodo_pago == 1) // EFECTIVO  --> sólo registrar ingresos
                                    {
                                        if ($resultadoRegistrarIngreso['estado'] == true) {
                                            $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                                            if ($resultadoMofificacionIngreso == true) {
                                                $array_vender["estado"]  = 1;
                                                $array_vender["mensaje"] = "Nueva venta " . $resultadoRegistrarVenta["ventavehiculo_id"] . " registrada correctamente";
                                            } else {
                                                $array_vender["estado"]  = 0;
                                                $array_vender["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                                            }
                                            $resultadoMofificacionIngreso = null;
                                        } else {
                                            $array_vender["estado"]  = 0;
                                            $array_vender["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                                        }
                                    }
                                    else // TRANSFERENCIA BANCARIA --> registrar ingreso | egreso
                                    {
                                        if ($resultadoRegistrarIngreso['estado'] == true) {
                                            $resultadoMofificacionIngreso = $oIngreso->modificar_campo($resultadoRegistrarIngreso['ingreso_id'], 'tb_ingreso_tipcam', $ventavehiculo_tipcam, 'STR');
                                            if ($resultadoMofificacionIngreso == true) {
                                                $resultadoRegistrarEgreso = $oEgreso->insertar();
                    
                                                if ($resultadoRegistrarEgreso['estado'] == true) {
                                                    $array_vender["estado"]  = 1;
                                                    $array_vender["mensaje"] = "Nueva venta " . $resultadoRegistrarVenta["ventavehiculo_id"] . " registrada correctamente";
                                                } else {
                                                    $array_vender["estado"]  = 0;
                                                    $array_vender["mensaje"] = "Se presentó un inconveniente al registrar el egreso. <br> Error SQL: Egreso.class :: insertar() :: linea: 30";
                                                }
                                            } else {
                                                $array_vender["estado"]  = 0;
                                                $array_vender["mensaje"] = "Se presentó un inconveniente al modificar ingreso. <br> Error SQL: Ingreso.class :: modificar_campo() :: linea: 876";
                                            }
                                            $resultadoMofificacionIngreso = null;
                                        } else {
                                            $array_vender["estado"]  = 0;
                                            $array_vender["mensaje"] = "Se presentó un inconveniente al registrar ingreso. <br> Error SQL: Ingreso.class :: insertar() :: linea: 38";
                                        }
                                    }

                                $resultadoRegistrarIngreso = null;


                                // ----------- REGISTRAR DEPOSITO ----------- //

                                $consultarDataDeposito = $oDeposito->calcularProximoDpositoID();
                                
                                    if( $consultarDataDeposito["estado"] == 1 )
                                    {
                                        $oDeposito->tb_deposito_fec=fecha_mysql($ventavehiculo_fec);
                                        $oDeposito->tb_deposito_des=''; 
                                        $oDeposito->tb_deposito_mon=moneda_mysql($ventavehiculo_bas);
                                        $oDeposito->tb_deposito_num='VENTA-CGV-'.$credito_id.'-'.$consultarDataDeposito["data"]["newid"]; 
                                        $oDeposito->tb_cuentadeposito_id=$set_tbcuentadepositoid;
                                        $oDeposito->insertar($set_tbdeptipo,$cliente_id);

                                    }

                                    // ----------- REGISTRAMOS EL HISTORIAL ----------- //

                                    $oHist->setTbHistDet($mensaje);
                                    $oHist->insertar();

                                    // ----------- REGISTRAMOS EL HISTORIAL ----------- //
                                    
                                
                                $consultarDataDeposito = null;
                            }
                            else
                            {
                                $array_vender["estado"]  = 0;
                                $array_vender["mensaje"] = "Se presentó un inconveniente al registrar la venta. <br> Error SQL: Venta.class :: insertar() :: linea: 30";
                            }
            
                        $resultadoRegistrarVenta = null;
                    }

                $resultaDataValidate = null;            
            }

            echo json_encode($array_vender);
            
        }
        else
        {
            $data = array();
            $data['estado']  = 0;
            $data['mensaje'] = 'Método de pago inválido, verifique.';
            echo json_encode($data);
            exit();
        }

    } else {
        $array_error = array();
        $array_error["estado"] = 0;
        $array_error["mensaje"] = "Operacion invalida";
        echo json_encode($array_error);
    }
?>