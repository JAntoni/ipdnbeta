<?php
  require_once ("../../core/usuario_sesion.php");
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../stockunidad/Stockunidad.class.php');
  $oStockunidad = new Stockunidad();
  require_once("../gasto/Sugerir.class.php");
  $oSugerir = new Sugerir();
  require_once('../funciones/funciones.php');

  $direc = 'stockunidad';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $stockunidad_id = $_POST['stockunidad_id'];

  $credito_id = $_POST['credito_id'];
  $creditotipo_id = $_POST['creditotipo_id'];
  $credito_vehpla = $_POST['credito_vehpla'];
  $pre_ajus_bas = moneda_mysql($_POST['precio_bas']);
  $pre_ajus_tot = moneda_mysql($_POST['precio_tot']);
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Stock Unidad Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Stock Unidad';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Stock Unidad';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Stock Unidad';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en stockunidad
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'stockunidad'; $modulo_id = $stockunidad_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del stockunidad por su ID
    if(intval($stockunidad_id) == 0){
      $result = $oStockunidad->mostrar_datos_stock_placa($credito_id, $credito_vehpla);
        if($result['estado'] == 1){
          $stockunidad_id = $result['data']['tb_stockunidad_id'];
          $stockunidad_soatval = $result['data']['tb_stockunidad_soatval'];
          $stockunidad_strval = $result['data']['tb_stockunidad_strval'];
          $stockunidad_gpsval = $result['data']['tb_stockunidad_gpsval'];
          $stockunidad_kil = $result['data']['tb_stockunidad_kil'];
          $stockunidad_det = $result['data']['tb_stockunidad_det'];
        }
      $result = NULL;

      $result = $oSugerir->mostrar_datos_sugerir_credito($credito_id, $creditotipo_id, 2); //de tipo 2 para que devuelva datos de ajustes
        if($result['estado'] == 1){
          $sug_id = $result['data']['tb_sugerir_id'];
          $pre_base = (!empty($result['data']['tb_sugerir_prebas'])) ? floatval($result['data']['tb_sugerir_prebas']) : 0;
          $pre_soat = (!empty($result['data']['tb_sugerir_presoat'])) ? floatval($result['data']['tb_sugerir_presoat']) : 0;
          $pre_str = (!empty($result['data']['tb_sugerir_prestr'])) ? floatval($result['data']['tb_sugerir_prestr']) : 0;
          $pre_gps = (!empty($result['data']['tb_sugerir_pregps'])) ? floatval($result['data']['tb_sugerir_pregps']) : 0;
          $pre_gas = (!empty($result['data']['tb_sugerir_pregas'])) ? floatval($result['data']['tb_sugerir_pregas']) : 0;
          $mon_id = $result['data']['tb_moneda_id'];
        }
      $result = NULL;

      $pre_tot = formato_numero($pre_base + $pre_soat + $pre_str + $pre_gps + $pre_gas);
      if($pre_base <= 0){
        $pre_base = $pre_ajus_bas;
        $pre_tot = $pre_ajus_tot;
      }
      if($mon_id == 2)
        $moneda = 'US$';
      elseif($mon_id == 1)
        $moneda = 'S/';
      else
        $moneda = 'SN';

      $tabla = '';
      if($creditotipo_id == 2)
        $tabla = 'tb_creditoasiveh';
      if($creditotipo_id == 3)
        $tabla = 'tb_creditogarveh';
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_stockunidad" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_stockunidad" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_stockunidad_id" value="<?php echo $stockunidad_id;?>">
          
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_ajuste_bas" class="control-label">Monto Base:</label>
                <input type="text" name="txt_ajuste_bas" id="txt_ajuste_bas" class="form-control input-sm" value="<?php echo mostrar_moneda($pre_base);?>">
              </div>
              <div class="form-group col-md-6">
                <label for="txt_stockunidad_soatval" class="control-label">SOAT Válido:</label>
                <input type="text" name="txt_stockunidad_soatval" id="txt_stockunidad_soatval" class="form-control input-sm" value="<?php echo $stockunidad_soatval;?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_ajuste_tot" class="control-label">Monto Total:</label>
                <input type="text" name="txt_ajuste_tot" id="txt_ajuste_tot" class="form-control input-sm" value="<?php echo mostrar_moneda($pre_tot);?>" readonly>
              </div>
              <div class="form-group col-md-6">
                <label for="txt_stockunidad_strval" class="control-label">STR Válido:</label>
                <input type="text" name="txt_stockunidad_strval" id="txt_stockunidad_strval" class="form-control input-sm" value="<?php echo $stockunidad_strval;?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_stockunidad_kil" class="control-label">Kilometraje:</label>
                <input type="text" name="txt_stockunidad_kil" id="txt_stockunidad_kil" class="form-control input-sm" value="<?php echo $stockunidad_kil;?>">
              </div>
              <div class="form-group col-md-6">
                <label for="txt_stockunidad_gpsval" class="control-label">GSP Válido:</label>
                <input type="text" name="txt_stockunidad_gpsval" id="txt_stockunidad_gpsval" class="form-control input-sm" value="<?php echo $stockunidad_gpsval;?>">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h4 class="title">Detalles:</h4>
                <?php
                  if(!empty($stockunidad_det)){
                    $arr_det = explode('/&/&/', $stockunidad_det);
                    for ($i=0; $i < count($arr_det); $i++) { 
                      echo '
                      <div class="direct-chat-text">
                        '.$arr_det[$i].'
                        <!--a href="javascript:void(0)" class="btn_eliminar_det">X</a-->
                        <input type="hidden" name="hdd_stock_det[]" value="'.$arr_det[$i].'">
                      </div>';
                    }
                  }
                ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h4 class="title">Fotos:</h4>
                <?php 
                  $ol_indicators = '';
                  $div_images = '';
                  $result = $oStockunidad->mostrar_fotos_credito($tabla, $credito_vehpla);
                    if($result['estado'] == 1){
                      foreach ($result['data'] as $key => $value) {
                        $slide_to = 0;
                        if($slide_to == 0){
                          $ol_indicators .= '<li data-target="#carousel-example-generic" data-slide-to="'.$slide_to.'" class="active"></li>';
                          $div_images .= '
                            <div class="item active">
                              <img src="'.$value['img_url'].'" alt="First slide">
                              <div class="carousel-caption">
                                First Slide
                              </div>
                            </div>';
                        }
                        else{
                          $ol_indicators .= '<li data-target="#carousel-example-generic" data-slide-to="'.$slide_to.'" class=""></li>';
                          $div_images .= '
                            <div class="item">
                              <img src="'.$value['img_url'].'" alt="First slide">
                              <div class="carousel-caption">
                                First Slide
                              </div>
                            </div>';
                        }

                        $slide_to ++;
                      }
                    }
                  $result = NULL;
                ?>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <?php echo $ol_indicators;?>
                  </ol>
                  <div class="carousel-inner">
                    <?php echo $div_images;?>
                  </div>
                  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                  </a>
                  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                  </a>
                </div>
              </div>
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta stockunidad?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="stockunidad_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_stockunidad">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_stockunidad">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_stockunidad">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/stockunidad/stockunidad_form.js';?>"></script>
