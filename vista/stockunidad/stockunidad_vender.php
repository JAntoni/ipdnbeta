<?php

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

    require_once('../../core/usuario_sesion.php');
    
    if( empty($_POST['action']) ){
        echo "Complete datos requerido: action";
        exit();
    }
    elseif( empty($_POST['stockunidad_id']) ){
        echo "Complete datos requerido: stockunidad_id";
        exit();
    }
    elseif( empty($_POST['tipocambio']) ){
        echo "Complete datos requerido: tipocambio";
        exit();
    }
    else
    {
        require_once('../funciones/funciones.php');
        require_once("../gasto/Sugerir.class.php");    
        require_once '../ingreso/Ingreso.class.php';
        require_once('../stockunidad/Stockunidad.class.php');
        // require_once '../creditogarveh/Creditogarveh.class.php';
        require_once '../cliente/Cliente.class.php';
        require_once '../usuario/Usuario.class.php';
        require_once '../monedacambio/Monedacambio.class.php';

        $oSugerir       = new Sugerir();
        $oIngreso       = new Ingreso();
        $oStockunidad   = new Stockunidad();
        // $oCreditoGarveh = new Creditogarveh();
        $oCliente       = new Cliente();
        $oUsuario       = new Usuario();
        $oMonedacambio= new Monedacambio();

        $action         = $_POST['action'];
        $stockunidad_id = $_POST['stockunidad_id'];
        $tipocambio     = $_POST['tipocambio'];

        $fec=date('Y-m-d');
        $result=$oMonedacambio->consultar($fec);

        $venta                  = 0.00;
        $compra                 = 0.00;
        $validacion_moneda      = "";
        $validacion_monto_base  = 0;
        $validacion_saldo_pagar = 0;
        $suma_total_ingresos    = 0;
        $selectedMonedaConversion = "";
        $selectMonedaSoles      = "";
        $selectMonedaDolares    = "";
        $simboloMoneda          = "";
        $add_venta_al_credito   = "";

        if($result['estado'] == 1){
            $venta  = ($result['data']['tb_monedacambio_val']);
            $compra = ($result['data']['tb_monedacambio_com']);
        }

        $consultarDatosCajaCambio = null;

        $consultarDatosStockUnidad = $oStockunidad->vender_consultar_por_id($stockunidad_id);
            if( $consultarDatosStockUnidad['estado'] == 1 )
            {
                $array_data['estado']  = $consultarDatosStockUnidad['estado'];
                $array_data['mensaje'] = $consultarDatosStockUnidad['mensaje'];
                $monID = $consultarDatosStockUnidad["data"]["tb_moneda_id"];

                if( $consultarDatosStockUnidad["data"]["tb_moneda_id"] == 1 ) { $validacion_moneda = "SOLES"; } elseif( $consultarDatosStockUnidad["data"]["tb_moneda_id"] == 2 ) { $validacion_moneda = "DOLARES"; } else { $validacion_moneda = "No identificada"; }
        
                $validacion_monto_base         = $consultarDatosStockUnidad["data"]["tb_stockunidad_monbas"];
                // $validacion_monto_resolucion   = formato_numero( $consultarDatosStockUnidad["data"]["tb_credito_cuoven1"] + $consultarDatosStockUnidad["data"]["tb_credito_cap1"] + $consultarDatosStockUnidad["data"]["tb_credito_gasto1"]);
                $credito_id                    = $consultarDatosStockUnidad["data"]["tb_credito_id"];
                $creditotipo_id                = $consultarDatosStockUnidad["data"]["tb_creditotipo_id"];
                $numero_placa                  = $consultarDatosStockUnidad["data"]["tb_credito_vehpla"];
                $cliente_id                    = $consultarDatosStockUnidad["data"]["tb_cliente_id"];
                $usuario_asesor_id             = $consultarDatosStockUnidad["data"]["tb_stockunidad_usuven"];
                $usuario_autoriza_id           = $consultarDatosStockUnidad["data"]["tb_stockunidad_usuaut"];
                $monedaResolucionId            = $consultarDatosStockUnidad["data"]["tb_moneda_id"];

                if( $monedaResolucionId == 1 ){
                    $selectMonedaSoles   = "selected";
                    $selectMonedaDolares = "";
                    $simboloMoneda       = "S/ ";
                }
                elseif( $monedaResolucionId == 2 ){
                    $selectMonedaSoles   = "";
                    $selectMonedaDolares = "selected";
                    $simboloMoneda       = "USD $";
                }
                else{
                    $selectedMonedaConversion = "";
                }
                

                // $consultarDatosCredito = $oCreditoGarveh->mostrarUno($credito_id);
                // $ID_CLIENTE_CREIDTO    = $consultarDatosCredito["data"]["tb_cliente_id"];

                // --- CONSULTAR LA DATA DEL CLIENTE --- //
                $cliente_name = '';
                $readonly_cliente_name = '';
                if( $cliente_id != 0)
                {
                    $consultarDatosCliente = $oCliente->mostrarUno($cliente_id);
                    $cliente_name = $consultarDatosCliente["data"]["tb_cliente_nom"];
                    $readonly_cliente_name = 'readonly';
                }

                $DatosUsuario_asesor_name = '';
                $readonly_DatosUsuario_asesor_name = '';
                $DatosUsuario_autoriza_name = '';
                $readonly_DatosUsuario_autoriza_name = '';
                if($usuario_asesor_id != 0 && $usuario_autoriza_id != 0)
                {
                    $consultarDatosUsuario = $oUsuario->mostrarUno($usuario_asesor_id);
                    $DatosUsuario_asesor_name = $consultarDatosUsuario["data"]["tb_usuario_nom"].' '.$consultarDatosUsuario["data"]["tb_usuario_ape"];
                    $readonly_DatosUsuario_asesor_name = 'readonly';

                    $consultarDatosUsuario = $oUsuario->mostrarUno($usuario_autoriza_id);
                    $DatosUsuario_autoriza_name = $consultarDatosUsuario["data"]["tb_usuario_nom"].' '.$consultarDatosUsuario["data"]["tb_usuario_ape"];
                    $readonly_DatosUsuario_autoriza_name = 'readonly';
                }
            }
            else
            {
                $array_data['estado']  = 0;
                $array_data['mensaje'] = "No se encontró resultados";
            }
        $consultarDatosStockUnidad = null;

        $mod_id = 62; // INGRESO POR SEPARACIÓN DE VEHICULO
        $consultarDatosIngresosRestantes = $oIngreso->mostrar_por_modulo($mod_id,$stockunidad_id,0,0);
            if ($consultarDatosIngresosRestantes['estado'] == 1) {
                foreach ($consultarDatosIngresosRestantes['data'] as $key => $value2) {
                    if( $validacion_moneda == "SOLES")
                    {
                        if( $value2['tb_moneda_id'] == 1 )
                        {
                            $suma_total_ingresos += formato_numero($value2['tb_ingreso_imp']);
                        }
                        elseif( $value2['tb_moneda_id'] == 2 )
                        {
                            $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] * $value2['tb_ingreso_tipcam']));
                        }
                        else
                        {
                            $suma_total_ingresos += 0;
                        }
                    }
                    elseif( $validacion_moneda == "DOLARES" )
                    {
                        if( $value2['tb_moneda_id'] == 1 )
                        {
                            $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp'] / $value2['tb_ingreso_tipcam']));
                        }
                        elseif( $value2['tb_moneda_id'] == 2 )
                        {
                            $suma_total_ingresos += formato_numero(($value2['tb_ingreso_imp']));
                        }
                        else
                        {
                            $suma_total_ingresos += 0;
                        }
                    }
                    else
                    { $suma_total_ingresos = 0; }
                }
            }
        $consultarDatosIngresosRestantes = null;
        
        $validacion_saldo_pagar = formato_numero($validacion_monto_base - $suma_total_ingresos);
        
        if( formato_numero($validacion_monto_base) > 0.00 && formato_numero($suma_total_ingresos) > 0.00 ){ $add_venta_al_credito   = '<option value="3">Al crédito</option>'; } else{ $add_venta_al_credito = ''; }
    }
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_stockunidad_vender" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" id="modalSizeFormSUVenta" role="document">
        <div class="modal-content">
            <form id="form_stockunidad_vender" name="form_stockunidad_vender" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="text-white"><b>REGISTRAR VENTA VEHICULAR</b></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2">
                                <input type="hidden" class="form-control" name="action" value="vender">
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" class="form-control" name="hdd_stockunidad_id" id="hdd_stockunidad_id" value="<?php echo $stockunidad_id; ?>">
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" class="form-control" name="hdd_numero_placa" id="hdd_numero_placa" value="<?php echo $numero_placa;?>">
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" class="form-control" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id;?>">
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" class="form-control" name="hdd_creditotipo_id" id="hdd_creditotipo_id" value="<?php echo $creditotipo_id;?>">
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" name="txt_ventavehiculo_tipcam" id="txt_ventavehiculo_tipcam" class="form-control text-right" readonly/>
                            </div>
                        </div>
                        <div class="row mt-4 mb-4">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_ventavehiculo_fec" class="control-label">Fecha de Venta:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-calendar-day"></i></span>
                                        </div>
                                        <input type="date" name="txt_ventavehiculo_fec" id="txt_ventavehiculo_fec" class="form-control text-center" value="<?php echo date('Y-m-d');?>" readonly style="margin-right: -50px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_ventavehiculo_tipcam_compra" class="control-label">T.C. Compra:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-coins"></i></span>
                                        </div>
                                        <input type="text" name="txt_ventavehiculo_tipcam_compra" id="txt_ventavehiculo_tipcam_compra" class="form-control text-right" value="<?php echo $compra; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_ventavehiculo_tipcam_venta" class="control-label">T.C. Venta:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-coins"></i></span>
                                        </div>
                                        <input type="text" name="txt_ventavehiculo_tipcam_venta" id="txt_ventavehiculo_tipcam_venta" class="form-control text-right" value="<?php echo $venta; ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="cmb_moneda_id_padre" class="control-label">Moneda Original: </label>
                                    <div class="input-group">
                                        <input type="text" name="cmb_moneda_id_padre" id="cmb_moneda_id_padre" class="form-control text-center" value="<?php echo $validacion_moneda;?>" readonly>
                                        <input type="hidden" name="hdd_moneda_id_padre" id="hdd_moneda_id_padre" class="form-control" value="<?php echo $monID;?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_ventavehiculo_bas" class="control-label">Monto Base S/:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><?php echo $simboloMoneda;?></span>
                                        </div>
                                        <input type="text" name="txt_ventavehiculo_bas" id="txt_ventavehiculo_bas" class="form-control moneda text-right" value="<?php echo mostrar_moneda($validacion_monto_base); ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_monto_previos" class="control-label">Ingresos Previos:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><?php echo $simboloMoneda;?></span>
                                        </div>
                                        <input type="text" name="txt_monto_previos" id="txt_monto_previos" class="form-control moneda text-right" value="<?php echo $suma_total_ingresos; ?>" readonly> <!-- onblur="calcularSaldoPagar()" -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_saldo_a_pagar" class="control-label">Saldo a Pagar:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-info-circle"></i></span>
                                        </div>
                                        <input type="text" name="txt_saldo_a_pagar" id="txt_saldo_a_pagar" class="form-control moneda text-right" value="<?php echo $validacion_saldo_pagar;?>" readonly/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_cliente_id" class="control-label">Cliente:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" name="txt_cliente_id" id="txt_cliente_id"  placeholder="ESCRIBE UN CLIENTE PARA BUSCAR" class="form-control ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $cliente_name;?>" <?php echo $readonly_cliente_name;?>>
                                        <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id ?>">
                                        <span class="input-group-btn">
                                            <button id="btn_agr" class="btn btn-github" type="button" onclick="cliente_form('I', 0)" title="Agregar Cliente">
                                                <span class="fa fa-plus icon"></span>
                                            </button>
                                            <button id="btn_edit" class="btn btn-warning" type="button" onclick="cliente_form('M', 0)" title="Editar Cliente">
                                                <span class="fa fa-edit icon"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_usuario_id2" class="control-label">Asesor:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" name="txt_usuario_id2" id="txt_usuario_id2" class="form-control ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $DatosUsuario_asesor_name;?>">
                                        <input type="hidden" name="hdd_usuario_id2" id="hdd_usuario_id2" value="<?php echo $usuario_asesor_id;?>">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_usuarioautoriza_id" class="control-label">Usuario que autoriza:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" name="txt_usuarioautoriza_id" id="txt_usuarioautoriza_id" class="form-control ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $DatosUsuario_autoriza_name;?>" <?php echo $readonly_DatosUsuario_autoriza_name;?>>
                                        <input type="hidden" name="hdd_usuarioautoriza_id" id="hdd_usuarioautoriza_id" value="<?php echo $usuario_autoriza_id;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="cmb_metodo_pago">Método de pago:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-filter"></i></span>
                                        </div>
                                        <select name="cmb_metodo_pago" id="cmb_metodo_pago" class="form-control">
                                            <option value="1">Efectivo</option>
                                            <option value="2">Transferencia bancaria</option>
                                            <?php echo $add_venta_al_credito;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="cmb_moneda_id_conversion">Moneda de Conversión: </label>
                                    <select name="cmb_moneda_id_conversion" id="cmb_moneda_id_conversion" class="form-control">
                                        <!-- < ?php require_once('../moneda/moneda_select.php'); ?> -->
                                        <option value="">Seleccione:</option>
                                        <option value="1" <?php echo $selectMonedaSoles;?>>S/.</option>
                                        <option value="2" <?php echo $selectMonedaDolares;?>>US$</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="fragment_transferencia" class="container-fluid mb-4" style="display: none; border: solid 1px teal;">
                            <div class="row mt-4 mb-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>N° de Operación:</label>
                                        <input type="text" name="txt_numoperacion" id="txt_numoperacion" class="form-control" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txt_fecha_deposito" class="control-label">Fecha de Depósito:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span><i class="fa fa-calendar-day"></i></span>
                                            </div>
                                            <input type="date" name="txt_fecha_deposito" id="txt_fecha_deposito" class="form-control" onchange="validar_no_operacion_fecha()">
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cmb_banco_id" class="control-label">En Banco:</label>
                                        <select name="cmb_banco_id" id="cmb_banco_id" class="form-control">
                                            <option value="">Seleccione:</option>
                                            <option value="1">Banco BCP</option>
                                            <option value="2">Banco Interbank</option>
                                            <option value="3">Banco BBVA</option>
                                            <option value="4">OTROS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="cmb_cuedep_id">N° Cuenta:</label>
                                        <input type="hidden" class="form-control" id="hdd_cuenta_dep" name="hdd_cuenta_dep">                               
                                        <input type="hidden" class="form-control" id="hdd_mon_iddep" name="hdd_mon_iddep">                               
                                        <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control">
                                            <?php require_once '../cuentadeposito/cuentadeposito_select.php'; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Monto Depósito:</label>
                                        <input type="text" class="form-control moneda text-right" id="txt_montodeposito" name="txt_montodeposito" placeholder="0.00">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Comisión del Banco:</label>
                                        <input type="text" class="form-control moneda text-right" id="txt_comisionbanco" name="txt_comisionbanco" placeholder="0.00">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Monto dep&oacute;sito v&aacute;lido:</label>
                                        <input type="text" class="form-control moneda text-right" id="txt_montoDepositoValido" name="txt_montoDepositoValido" placeholder="0.00" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="fragment_alcredito" class="container-fluid mb-4" style="display: none; border: solid 1px teal;">
                            <div class="row mt-4 mb-4">
                                <!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="cmb_tipo_credito">Tipo de Crédito: </label>
                                        <select name="cmb_tipo_credito" id="cmb_tipo_credito" class="form-control">
                                            <option value="">Seleccione:</option>
                                            <option value="1">Créd. Menor:</option>
                                            <option value="2">Créd. Asiveh:</option>
                                            <option value="3">Créd. Vehicular:</option>
                                            <option value="4">Créd. Hipotecario:</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txt_credito_id" class="control-label">ID Crédito:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span><i class="fa fa-file-text"></i></span>
                                            </div>
                                            <input type="text" name="txt_credito_id" id="txt_credito_id" placeholder="Buscar ID" class="form-control">
                                            <span class="input-group-btn">
                                                <button id="btn_search_idcred" class="btn btn-info" type="button" onclick="buscarCredito()" title="Buscar por ID del Crédito">
                                                    <span class="fa fa-search icon"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="txt_return_monto" class="control-label">Monto del Crédito:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span id="getMoneda"><i class="fa fa-coins"></i></span>
                                            </div>
                                            <input type="text" name="txt_return_monto" id="txt_return_monto" class="form-control text-right" readonly placeholder="0.00">
                                            <input type="hidden" name="txt_return_moneda" id="txt_return_moneda" class="form-control text-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-9 col-md-7 col-xs-12"></div>
                            <div class="col-lg-3 col-md-5 col-xs-12">
                                <div class="form-group">
                                    <label for="txt_ventavehiculo_mon" class="control-label">Monto a Pagar:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-info-circle"></i></span>
                                        </div>
                                        <input type="text" name="txt_ventavehiculo_mon" id="txt_ventavehiculo_mon" class="input-sm form-control moneda text-right"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="callout callout-info" id="stockunidad_vender_mensaje" style="display: none;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php 
                        if ($_SESSION['usuariogrupo_id'] == 2 )
                        {
                            echo 
                            '
                            <div class="pull-left">
                                <button type="button" class="btn btn-success" onclick="preparar_momento2_form('.$credito_id.')"><i class="fa fa-info-circle fa-fw"></i> Consultar datos de ejecución</button>
                            </div>
                            ';
                        }
                    ?>
                    <div class="f1-buttons">
                        <button type="submit" id="btn_vender_unidad_vehicular" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo "vista/stockunidad/stockunidad_vender.js?ver=0901251050"; ?>"></script>
<script>
    function adjustModalSizeFormSUVenta() {
        var modalDialogForm = document.getElementById('modalSizeFormSUVenta');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogForm.classList.remove('modal-lg');
            modalDialogForm.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogForm.classList.remove('modal-xl');
            modalDialogForm.classList.add('modal-lg');
        }
    }

    adjustModalSizeFormSUVenta();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizeFormSUVenta);
</script>