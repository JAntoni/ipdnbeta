<form id="form_stockunidad_filtro" role="form">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label for="cmb_estado">Estado :</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><i class="fa fa-filter"></i></span>
                    </div>
                    <select class="form-control input-sm" name="cmb_estado" id="cmb_estado">
                        <option value="0">-</option>
                        <option value="1">DISPONIBLE</option>
                        <option value="4">VENDIDO</option>
                        <option value="5">VENDIDO AL CREDITO</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</form>