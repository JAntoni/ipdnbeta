<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    require_once('../stockunidad/Stockunidad.class.php');
    $oStockunidad = new Stockunidad();
    require_once('../funciones/funciones.php');
    $credito_id = intval($_POST['credito_id']);
    $creditotipo_id = intval($_POST['tipo_id']);
    $vista = $_POST['vista'];

    if( ! empty($credito_id) && ! empty($creditotipo_id) )
    {
        $resultado = $oStockunidad->ajustar_consultar_existe($credito_id, $creditotipo_id);

        $sumaTotalResolucion = 0;
        $moneda_id_anterior  = "";
        $montoBase           = 0;
        $monedaBase          = "";
        $soat                = "";
        $kilometraje         = 0;
        $revTec              = "";
        $tarjetaProp         = 0; // 0: NINGUNA | 1: TP FÍSICA | 2: TIVE
        $detalleTablaDraw    = "";
        $credito_vehpla      = "";
        
        $descripMoneda       = "";
        $selectMonedaSoles   = "";
        $selectMonedaDolares = "";
        $ocultarBtnGuardar   = "";
        $ocultarBtnAñadir    = "";
        $bloquearBtnPDF      = "";
        $bloquearBtnImages   = "";
        $bloquearBtnDropList = "";
        $addFragmentOfrecerCredito = "";
        $drawFragmentOfrecerCredito = "";
        $selectedOfrecerCredito01 = "";
        $selectedOfrecerCredito02 = "";
        $slide_to = 0;
        $ol_indicators = '';
        $div_images = '';
        $drawCarousel = '';
        $drawPDF = '';
        
        $rutaPDF             = "./public/pdf/stockunidad/";

        if( isset($resultado['data']['tb_moneda_id']) ) // sólo bastará con validar la moneda de resolución para obtener los datos, caso contrario ya se inicializaron los valores
        {
            if( $resultado['data']['tb_moneda_id'] == 1)
            {
                $descripMoneda = "SOLES";
                $selectMonedaSoles = "selected";
                $selectMonedaDolares = "";
            }
            elseif( $resultado['data']['tb_moneda_id'] == 2)
            {
                $descripMoneda = "DOLARES";
                $selectMonedaSoles = "";
                $selectMonedaDolares = "selected";
            }
            else
            {
                $descripMoneda = "NO IDENTIFICADA";
            }

            $capital            = $resultado['data']['tb_credito_cap1'];
            $cuotasVencidas     = $resultado['data']['tb_credito_cuoven1'];
            $gastos             = $resultado['data']['tb_credito_gasto1'];

            $credito_vehpla     = $resultado['data']['tb_credito_vehpla'];
            $montoBase          = $resultado['data']['tb_stockunidad_monbas'];
            $moneda_id_anterior = $resultado['data']['tb_moneda_id_anterior'];

            $soat               = $resultado['data']['tb_stockunidad_soatval'];
            $revTec             = $resultado['data']['tb_stockunidad_revtecval'];
            $kilometraje        = intval($resultado['data']['tb_stockunidad_kil']);
            $tarjetaProp        = $resultado['data']['tb_stockunidad_tarjeta'];
            $detalleTabla       = $resultado['data']['tb_stockunidad_det'];
            $tb_stockunidad_ofrecer_credito = $resultado['data']['tb_stockunidad_ofrecer_credito'];

            if( $tb_stockunidad_ofrecer_credito == 1){
                $selectedOfrecerCredito01 = "selected";
                $selectedOfrecerCredito02 = "";
                $tb_stockunidad_ofrecer_credito = 'CRÉDITO DIRECTO';
            }
            elseif( $tb_stockunidad_ofrecer_credito == 2){
                $selectedOfrecerCredito01 = "";
                $selectedOfrecerCredito02 = "selected";
                $tb_stockunidad_ofrecer_credito = 'AL CONTADO';
            }
            else{
                $selectedOfrecerCredito01 = "";
                $selectedOfrecerCredito02 = "";
                $tb_stockunidad_ofrecer_credito = 'AUN NO ACTUALIZADO';
            }

            if( $moneda_id_anterior == 1 ){ $moneda_id_anterior = "SOLES"; } elseif( $moneda_id_anterior == 2 ){ $moneda_id_anterior = "DÓLARES"; } else { $moneda_id_anterior = $moneda_id_anterior; }
        
            if( ! isset($resultado['data']['tb_stockunidad_kil']) ) { $kilometraje = 0;} else { $kilometraje = $kilometraje;}
            
            $sumaTotalResolucion = formato_numero( $capital + $cuotasVencidas + $gastos );
            
            $result = $oStockunidad->mostrar_archivos_stockunidad_ajustar($credito_id,'stockunidad','stockunidad_ajustar');
            if($result['estado'] == 1){
                foreach ($result['data'] as $key => $value) {
                    if($slide_to == 0) {
                        $ol_indicators .= '<li data-target="#carouselExampleIndicators" data-slide-to="'.$slide_to.'" class="active"></li>';
                        $div_images .= 
                        '<div class="item active"> <img class="d-block w-100" src="'.$value['filestorage_url'].'" alt="'.$slide_to.' slider"> </div>';
                    }
                    else {
                        $ol_indicators .= '<li data-target="#carouselExampleIndicators" data-slide-to="'.$slide_to.'"></li>';
                        $div_images .= 
                        '<div class="item"> <img class="d-block w-100" src="'.$value['filestorage_url'].'" alt="'.$slide_to.' slider"> </div>
                        ';
                    }
                    $slide_to ++;
                }
                $drawCarousel = '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> <ol class="carousel-indicators"> '.$ol_indicators.' </ol> <div class="carousel-inner"> '.$div_images.' </div> <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="fa fa-angle-left"></span> </a> <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="fa fa-angle-right"></span> </a> </div>';
            }
            else
            {
                $drawCarousel = '<span id="spnadjfotos">Aún no se han adjutado ninguna foto</span>';
            }
            $result = NULL;

            $getRutaFileBD = '';
            $resultPDF = $oStockunidad->mostrar_archivos_stockunidad_ajustar($credito_id,'stockunidad','stockunidad_ajustar_pdf');
            if($resultPDF['estado'] == 1){
                foreach ($resultPDF['data'] as $key => $value) {
                    $getRutaFileBD = $value['filestorage_url'];
                    $drawPDF .= '<iframe src="'.$getRutaFileBD.'" style="width: 100%; height: 450px; boerder: noe;"></iframe>';
                }
            }
            else
            {
                $drawPDF = '<span id="spnadjpdf">Aún no se ha adjutado nigún archivo PDF</span>';
            }
            $resultPDF = NULL;
        }

        if( $vista != 'gasto')
        {
            $ocultarBtnGuardar   = "hidden";
            $ocultarBtnAñadir    = "disabled";
            $bloquearBtnPDF      = "disabled";
            $bloquearBtnImages   = "disabled";
            $bloquearBtnDropList = '<td style="vertical-align: middle;" align="center" ><a><i class="fa fa-trash text-black" style="cursor: not-allowed;"></i></a></td>';

            $addFragmentOfrecerCredito = "";
            $drawFragmentOfrecerCredito = '<div class="bg-success text-center rounded"> <h5 class="p-4"><b>'.$tb_stockunidad_ofrecer_credito.'</b></h5> </div>';
        }
        else
        {
            $bloquearBtnDropList = '<td style="vertical-align: middle;" align="center" ><a><i class="fa fa-trash text-orange" onclick="eliminarFila(this)"></i></a></td>';

            $drawFragmentOfrecerCredito = ' ';
            $addFragmentOfrecerCredito = 
            '
            <div class="row mt-4 mb-4">
                <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12"></div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="text-center mb-1"> <label class="bg-teal text-white mayus p-4 rounded mb-4">¿Se puede ofrecer como crédito?</label> </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span><i class="fa fa-info-circle"></i></span>
                            </div>
                            <select name="cmb_ofrecercred" id="cmb_ofrecercred" class="form-control" required required/>
                                <option value="">Seleccione opción:</option>
                                <option value="1" '.$selectedOfrecerCredito01.'>Crédito Directo</option>
                                <option value="2" '.$selectedOfrecerCredito02.'>Al contado</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            ';
        }

        if( ! empty($detalleTabla) )
        {
            $descripcionData = '';
            $array = (explode("/&/&/",$detalleTabla));

            for($i=0;$i<(count($array)-1);$i++)
            {
                $descripcionData = $array[$i];
                $detalleTablaDraw .= '<tr> <td>'.($i+1).'</td> <td>'.$descripcionData.'</td> '.$bloquearBtnDropList.' </tr>';
            }
        }
        
        echo 
        '
        <form id="form_stockunidad_ajustar" name="form_stockunidad_ajustar" method="post" autocomplete="off">
            <div class="modal fade" tabindex="-1" role="dialog" id="div_modal_ajustar" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" id="modalSizeFormSUVer" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title" style="text-align: center;"> Venta de CGV-'.$credito_id.'"</h4>
                            <h4 style="text-align: center;">Placa de Vehiculo: '.$credito_vehpla.'</h4>
                            <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="'.$credito_id.'">
                            <input type="hidden" name="hdd_tipo_id" id="hdd_tipo_id" value="'.$creditotipo_id.'">
                            <input type="hidden" name="hdd_tipo_id" id="hdd_tipo_id" value="'.$credito_vehpla.'">
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="callout callout-info" id="stockunidad_mensaje_tbl" style="display: none;">
                                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="container-fluid">
                                        '.$addFragmentOfrecerCredito.'
                                        <div class="row mt-4 mb-4">
                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txt_stockunidad_montoresolucion" class="control-label">Monto de Resolución:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-info-circle"></i></span>
                                                        </div>
                                                        <input type="text" name="txt_stockunidad_montoresolucion" id="txt_stockunidad_montoresolucion" class="form-control text-right moneda" placeholder="0.00" value="'.$sumaTotalResolucion.'" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txt_stockunidad_monedaresolucion" class="control-label">Moneda de Resolución:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-coins"></i></span>
                                                        </div>
                                                        <input type="text" name="txt_stockunidad_monedaresolucion" id="txt_stockunidad_monedaresolucion" class="form-control text-center" value="'.$moneda_id_anterior.'" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <div class="text-center"> <label class="bg-warning text-warning mayus p-4 rounded mb-4">Verificar el monto a ingresar, sea el mismo a la moneda de resolución</label> </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txt_stockunidad_monbas" class="control-label">Monto Base:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-info-circle"></i></span>
                                                        </div>
                                                        <input type="text" name="txt_stockunidad_monbas" id="txt_stockunidad_monbas" class="form-control text-right moneda" value="'.$montoBase.'" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cmb_moneda_id" class="control-label">Moneda de venta:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-coins"></i></span>
                                                        </div>
                                                        <select name="cmb_moneda_id" id="cmb_moneda_id" class="form-control" required>
                                                            <option value="0">Seleccione:</option>
                                                            <option value="1" '.$selectMonedaSoles.'>Soles (S/)</option>
                                                            <option value="2" '.$selectMonedaDolares.'>Dolares ($)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="row mt-4 mb-4">
                                                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="txt_stockunidad_soatval" class="control-label">SOAT válido hasta:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-info-circle"></i></span>
                                                                </div>
                                                                <input type="date" name="txt_stockunidad_soatval" id="txt_stockunidad_soatval" class="form-control" value="'.$soat.'" required/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="txt_stockunidad_revtecval" class="control-label">Revisión Técnica válido hasta:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-info-circle"></i></span>
                                                                </div>
                                                                <input type="date" name="txt_stockunidad_revtecval" id="txt_stockunidad_revtecval" class="form-control" value="'.$revTec.'"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="txt_stockunidad_kil" class="control-label">Kilometraje:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-road"></i></span>
                                                                </div>
                                                                <input type="text" name="txt_stockunidad_kil" id="txt_stockunidad_kil" class="form-control text-right" min="0" step="1" value="'.$kilometraje.'" required/>
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-new-default bt-sm">Km/h</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="cmb_stockunidad_tarjeta" class="control-label">Tarjeta de Propiedad:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-file-text"></i></span>
                                                                </div>
                                                                <select name="cmb_stockunidad_tarjeta" id="cmb_stockunidad_tarjeta" class="form-control" required required/>
                                                                    <option value="">Seleccione:</option>
                                                                    <option value="0">Ninguna</option>
                                                                    <option value="1">TP Fisica</option>
                                                                    <option value="2">TIVE</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="row mt-4 mb-4">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="txt_stockunidad_det" class="control-label">Detalles:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-list"></i></span>
                                                                </div>
                                                                <input type="text" id="txt_stockunidad_det" name="txt_stockunidad_det" class="form-control" placeholder="Ingrese dato"/>
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-primary" '.$ocultarBtnAñadir.' id="btn_añadir"><i class="fa fa-plus-circle fa-fw"></i> Añadir</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive">
                                                        <input type="hidden" id="txtcontador" name="txtcontador" class="form-control moneda" value="0">
                                                        <table id="tabla-listado" class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>ITEM</th>
                                                                <th>DESCRIPCIÓN</th>
                                                                <th>&nbsp;</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="lista-body-detalle">
                                                                '.$detalleTablaDraw.'
                                                            </tbody>
                                                        </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                            '.$drawFragmentOfrecerCredito.'
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset style="padding: 5px;"> <br>
                                        <form class="form-inline mb-4">
                                            <span class="mayus mt-4" style="font-size: 18px;">Documentos PDF</span> <!-- legend -->
                                        </form>
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-danger mb-4" '.$bloquearBtnPDF.' onclick="filestorage_form_pdf()"><i class="fa fa-file-pdf fa-fw"></i> PDF </button>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            '.$drawPDF.'
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset style="padding: 5px;"> <br>
                                        <form class="form-inline mb-4">
                                            <span class="mayus mt-4" style="font-size: 18px;">Fotos del vehículo</span> <!-- legend -->
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-warning mb-4" '.$bloquearBtnImages.' onclick="filestorage_form()"><i class="fa fa-photo fa-fw"></i> Fotos</button> &nbsp;
                                            </div>
                                        </form>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-md-12">
                                            '.$drawCarousel.'
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="f1-buttons">
                                <button type="submit" class="btn btn-success '.$ocultarBtnGuardar.'">Guardar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        ';
    }

    // <div role="tabpanel">
    //     <ul class="nav nav-tabs">
    //     <li role="presentation" class="active">
    //         <a href="#tab-seccion-01" aria-controls="tab-seccion-01" role="tab" data-toggle="tab">Docs PDF</a>
    //     </li>
    //     <li role="presentation">
    //         <a href="#tab-seccion-02" aria-controls="tab-seccion-02" role="tab" data-toggle="tab">Imágenes</a>
    //     </li>
    //     </ul>
    //     <div class="tab-content">
    //         <div role="tabpanel" class="tab-pane active" id="tab-seccion-01">
    //             <div class="container-fluid">
    //                 <div class="row">
    //                     <div class="col-md-12"> <br>
    //                         <fieldset style="padding: 5px;"> <br>
    //                             <form class="form-inline mb-4">
    //                                 <span class="mayus mt-4" style="font-size: 18px;">Documentos PDF</span> <!-- legend -->
    //                             </form>
    //                             <div class="pull-right">
    //                                 <button type="button" class="btn btn-danger mb-4" '.$bloquearBtnPDF.' onclick="filestorage_form_pdf()"><i class="fa fa-file-pdf fa-fw"></i> PDF </button>
    //                             </div>
    //                         </fieldset>
    //                         <div class="row">
    //                             <div class="col-xs-12">
    //                                 '.$drawPDF.'
    //                             </div>
    //                         </div>
    //                     </div>
    //                 </div>
    //             </div>
    //         </div>
    //         <div role="tabpanel" class="tab-pane" id="tab-seccion-02">
    //             <div class="container-fluid">
    //                 <div class="row">
    //                     <div class="col-md-12"> <br>
    //                         <fieldset style="padding: 5px;"> <br>
    //                             <form class="form-inline mb-4">
    //                                 <span class="mayus mt-4" style="font-size: 18px;">Fotos del vehículo</span> <!-- legend -->
    //                                 <div class="pull-right">
    //                                     <button type="button" class="btn btn-warning mb-4" '.$bloquearBtnImages.' onclick="filestorage_form()"><i class="fa fa-photo fa-fw"></i> Fotos</button> &nbsp;
    //                                 </div>
    //                             </form>
    //                         </fieldset>
    //                         <div class="row">
    //                             <div class="col-md-12">
    //                                 '.$drawCarousel.'
    //                             </div>
    //                         </div>
    //                     </div>
    //                 </div>
    //             </div>
    //         </div>
    //     </div>
    // </div>
?>
<script type="text/javascript" src="<?php echo "vista/stockunidad/stockunidad_ajustar_form.js?ver=211024"; ?>"></script>
<script>
    function adjustModalSizeFormSUVer() {
        var modalDialogForm = document.getElementById('modalSizeFormSUVer');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogForm.classList.remove('modal-lg');
            modalDialogForm.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogForm.classList.remove('modal-xl');
            modalDialogForm.classList.add('modal-lg');
        }
    }

    adjustModalSizeFormSUVer();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizeFormSUVer);
</script>