<?php
    require_once("../../core/usuario_sesion.php");
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../deposito/Deposito.class.php');

    $oIngreso     = new Ingreso();
    $oDeposito    = new Deposito();

    $action          = trim($_POST['action']);
    $numoperacion    = trim($_POST['numoperacion']);
    $fecha_deposito  = trim($_POST['fecha_deposito']);
    $moneda_id_padre = intval($_POST['moneda_id_padre']);
    $arrayResultado  = array();

    $deposito_mon = 0;
    $result = $oDeposito->validar_no_operacion_fecha($numoperacion, $fecha_deposito);
    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            $deposito_mon += floatval($value['tb_deposito_mon']);
        }
    }
    $result = NULL;

    if ($deposito_mon == 0) {
        $arrayResultado['estado']  = 0;
        $arrayResultado['mensaje'] = 'El número de operación que ingresaste no se encuentra asociado a la Cuenta seleccionada o la fecha de depósito no corresponde, consulta con ADMINISTRACIÓN por favor.';
        echo json_encode($arrayResultado);
        exit();
    }
   
    $ingreso_importe = 0;
    $dts = $oIngreso->lista_ingresos_num_operacion($numoperacion); //OBTIENE TODOS LOS INGRESOS USANDO EL NUMERO DE OPERACION, ES DECIR EL SALDO USADO
        if ($dts['estado'] == 1) {
            foreach ($dts['data'] as $key => $dt) {
                $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
                $ingreso_mon_id = intval($dt['tb_moneda_id']);

                if ($moneda_id_padre == 1)  // SOLES
                {
                    if ($ingreso_mon_id == 1) {
                        $ingreso_importe += $tb_ingreso_imp;
                    } elseif ($ingreso_mon_id == 2)  // DÓLARES - SOLES
                    {
                        $tb_ingreso_imp = ($tb_ingreso_imp * $ventavehiculo_tipcam);
                    } else {
                        $ingreso_importe += 0;
                    }
                }
                elseif ($moneda_id_padre == 2) // DÓLARES
                {
                    if ($ingreso_mon_id == 1) // SOLES - DÓLARES
                    {
                        $tb_ingreso_imp = ($tb_ingreso_imp / $ventavehiculo_tipcam);
                    } elseif ($ingreso_mon_id == 2) {
                        $ingreso_importe += $tb_ingreso_imp;
                    } else {
                        $ingreso_importe += 0;
                    }
                }
                else
                { $suma_total_ingresos = 0; }
            }
        }
    $dts = null;

    $monto_usar = formato_moneda($deposito_mon) - formato_moneda($ingreso_importe); // MONTO DISPONIBLE PARA UTILIZAR EN OTRO PAGO

    $arrayResultado["estado"]     = 1;
    $arrayResultado["mensaje"]    = 'Monto depósito: '. number_format($deposito_mon,2).' <br>Monto utilizado: '.number_format($ingreso_importe,2).' <br>Saldo a utilizar: '.number_format($monto_usar,2);
    $arrayResultado["monto_usar"] = formato_moneda($monto_usar);

    echo json_encode($arrayResultado);
?>