<?php
  session_name("ipdnsac");
  session_start();

  unset($_SESSION['usuario_id']); 
  unset($_SESSION['usuario_usu']); 
  unset($_SESSION['usuario_nom']); 
  unset($_SESSION['usuario_ape']); 
  unset($_SESSION['usuariogrupo_id']);
  unset($_SESSION['usuario_ema']); 
  unset($_SESSION['usuario_tel']); 
  unset($_SESSION['usuario_dir']); 
  unset($_SESSION['usuario_dni']); 
  unset($_SESSION['usuario_tur']); 
  unset($_SESSION['usuario_per']); 
  unset($_SESSION['usuario_fot']);

  session_destroy();
  
 	header('location: ../../');
?>