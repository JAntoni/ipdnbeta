<?php
	require_once('core/core.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IPDN | Inicio de Sesión</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="static/css/bootstrap/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="static/css/waves.css" rel="stylesheet">
	<!-- Ionicons -->
	<link rel="stylesheet" href="static/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="static/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="static/css/AdminLTE.min.css">
	<link rel="stylesheet" href="static/css/AdminLTE.custom.css">
	<link href="static/css/waves.css" rel="stylesheet">


	<link rel="stylesheet" href="public/css/generales.css">
	  <link rel="shortcut icon" type="image/x-icon" href="public/images/logopres.ico" />
</head>

<body class="hold-transition login-page" style="">

<div class="login-box" >
   <div style="color: #f4f4f4; font-family: verdana;font-size: 7pt;margin: 10px 0 0;    text-align: center; border: 2px;border-radius: 50%;height: 100px;left: 46%;position: absolute;top: 40%;width: 100px;text-align: center;">    
		<img class="img-responsive fa-spin"  
		src="public/images/logopres1.png" alt="Photo" style="width: 60px; height: 60px">
		
	</div>
</div>
  <!-- /.login-box-body -->
<!-- /.login-box -->

<!--MODAL PARA LOS MENSAJES -->
<?php include(VISTA_URL.'templates/modal_mensaje.php');?>

<!-- jQuery 2.2.3 -->
<script src="static/js/jquery/jquery-3.2.1.min.js"></script>
<script src="static/js/jquery/jquery.validate.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="static/js/bootstrap/bootstrap.min.js"></script>
<!-- SCRIPT PROPIOS-->
<script src="public/js/generales.js"></script>
<script src="<?php echo VISTA_URL.'/login/login.js';?>"></script>
<script type="text/javascript" src="static/js/waves.js"></script>
<script src="static/js/admin.js"></script>
</body>
</html>


