<?php
	require_once('Sesion.class.php');

  require_once('../funciones/funciones.php');

  $usuario = $_POST['txt_usuario_usu'];
  $clave = $_POST['txt_usuario_pass'];
  $frase = $_POST['txt_usuario_frase'];

  if(!empty($usuario) && !empty($clave)){
    $oSesion = new Sesion();
    $oSesion->setCorreo($usuario);
    $oSesion->setClave($clave);
    $oSesion->setFrase($frase);

    $result = $oSesion->iniciar_sesion();

    if($result['estado'] == 200){
      //EXISTE EL USUARIO CON ESA CLAVE
      $navegador = getBrowser(); //funcion ubicada en funciones.php
      $ip = $_SERVER['REMOTE_ADDR'];
      $locacion = getLocation(); //function ubicada en funciones.php

      define('DURACION_SESION','36000'); //10 horas
      ini_set("session.cookie_lifetime",DURACION_SESION);
      ini_set("session.gc_maxlifetime",DURACION_SESION); 
      //ini_set("session.save_path","/home/ipdnsac/tmp/sesiones");
      session_cache_expire(DURACION_SESION);
      session_name("ipdnsac");
      session_start();
      session_regenerate_id(true);
      
      foreach ($result["data"] as $key => $value) {
        $_SESSION['usuario_id'] = $value['tb_usuario_id']; //id del usuario
        $_SESSION['usuario_usu'] = $value['tb_usuario_use']; //nombre de usuario
        $_SESSION['usuario_nom'] = $value['tb_usuario_nom']; //nombre del usuario
        $_SESSION['usuario_nombre'] = $value['tb_usuario_nom']; //nombre del usuario
        $_SESSION['usuario_ape'] = $value['tb_usuario_ape']; //apellidos del usuario
        $_SESSION['usuariogrupo_id'] = $value['tb_usuariogrupo_id']; //grupo id al que pertence el usuario
				$_SESSION['usuarioperfil_id'] = $value['tb_usuarioperfil_id']; //perfil del usuario de este depende el menu a mostrar
        $_SESSION['usuario_ema'] = $value['tb_usuario_ema']; //email del usuario
        $_SESSION['usuario_tel'] = $value['tb_usuario_tel']; //telefono del usuario
        $_SESSION['usuario_dir'] = $value['tb_usuario_dir']; //direccion del usuario
        $_SESSION['usuario_dni'] = $value['tb_usuario_dni']; //DNI del usuario
        $_SESSION['usuario_tur'] = $value['tb_usuario_tur']; //tipo de turno del usuario
        $_SESSION['usuario_per'] = $value['tb_usuario_per']; //tipo de personal del usuario
        $_SESSION['usuario_fot'] = $value['tb_usuario_fot']; //foto del usuario
        $_SESSION['empresa_id'] = $value['tb_empresa_id']; //empresa id
        $_SESSION['empresa_nombre'] = $value['tb_empresa_nomcom']; //empresa id
      }
      
      $resultados = $oSesion->consultarNumeroDeEmpresas($_SESSION['usuario_id']);
      $_SESSION['nsucursales'] = $resultados['data']['cantidad'];

      $result2 = $oSesion->insertar_inicio_sesion($_SESSION['usuario_id'], $navegador, $ip, $locacion);

      if($result2 == 1){
        $result['tipo_usuario'] = $_SESSION['usuariogrupo_id']; // 2 admin, 3 ventas
        echo json_encode($result); //incio correcto
      }
      else
        echo 'ERROR!: al insertar el incio de sesión';
    }
    else{
      echo json_encode($result);
    }
  }
  else{
    $result["estado"] = 500;
    $result["mensaje"] = "Debe ingresar un usuario y una contraseña";
    $result["data"] = "";

    echo json_encode($result);
  }
?>
