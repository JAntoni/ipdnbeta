<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Sesion extends Conexion {
  	private $correo;
  	private $clave;
    private $frase;

    function iniciar_sesion(){
      try {
        //xac = 1: quiere decir que está activo, blo = 0: quiere decir que el usuario no está bloqueado
        $sql = "SELECT * FROM tb_usuario u 
          inner join tb_empresa e on e.tb_empresa_id=u.tb_empresa_id 
          WHERE (tb_usuario_use =:correo OR tb_usuario_ema =:correo) AND tb_usuario_pas = md5(:clave) AND tb_usuario_frase = :frase AND tb_usuario_xac = 1 AND tb_usuario_estado = 0";
        $sentencia = $this->dblink->prepare($sql);

        $correo = $this->getCorreo();
        $clave = $this->getClave();
        $frase = $this->getFrase();

        $sentencia->bindParam(":correo", $correo, PDO::PARAM_STR);
        $sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
        $sentencia->bindParam(":frase", $frase, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 200;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 500;
          $retorno["mensaje"] = "El usuario o la clave son incorrectas";
          $retorno["data"] = "";
        }

        return $retorno;

      } catch (Exception $e) {
        return "¡Error!: " . $e->getMessage() . "<hr/>";
      }
    }

    function insertar_inicio_sesion($usu_id, $navega, $ip, $lugar){

      $this->dblink->beginTransaction();
      try {

        $sql = "INSERT INTO tb_sesion(tb_usuario_id, tb_sesion_nav, tb_sesion_ip, tb_sesion_lug)
              VALUES(:usu_id, :navega, :ip, :lugar)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usu_id", $usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":navega", $navega, PDO::PARAM_STR);
        $sentencia->bindParam(":ip", $ip, PDO::PARAM_STR);
        $sentencia->bindParam(":lugar", $lugar, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function iniciar_sesion_fb($correo){
      try {
          $sql = "select * from usuario where correo = :correo";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":correo",$correo,PDO::PARAM_STR);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 200;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
          }
          else{
              $retorno["estado"] = 500;
              $retorno["mensaje"] = "No hay usuario registrado";
              $retorno["data"] = "";
          }
          return json_encode($retorno);
      } catch (Exception $e) {
          throw $e->getMessage();
      }
    }
    
    
    function consultarNumeroDeEmpresas($usuario_id){
      try {
          $sql = "SELECT COUNT(tb_usuario_id) as cantidad from tb_usuarioempresa where tb_usuario_id = :usuario_id;";
         $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay empresas registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }

    function setCorreo($correo){
      $this->correo = $correo;
    }
    function getCorreo(){
      return $this->correo;
    }
    function setClave($clave){
      $this->clave = $clave;
    }
    function getClave(){
      return $this->clave;
    }
    function setFrase($frase){
      $this->frase = $frase;
    }
    function getFrase(){
      return $this->frase;
    }
  }
?>
