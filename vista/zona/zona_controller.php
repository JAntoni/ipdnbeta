<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../zona/Zona.class.php');
  $oZona = new Zona();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$zona_nom = mb_strtoupper($_POST['txt_zona_nom'], 'UTF-8');
 		$zona_pro = $_POST['txt_zona_pro'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Zona Registral.';
 		if($oZona->insertar($zona_nom, $zona_pro)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Zona Registral registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$zona_id = intval($_POST['hdd_zona_id']);
 		$zona_nom = mb_strtoupper($_POST['txt_zona_nom'], 'UTF-8');
 		$zona_pro = $_POST['txt_zona_pro'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Zona Registral.';

 		if($oZona->modificar($zona_id, $zona_nom, $zona_pro)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Zona Registral modificado correctamente. '.$zona_pro;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$zona_id = intval($_POST['hdd_zona_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Zona Registral.';

 		if($oZona->eliminar($zona_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Zona Registral eliminada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>