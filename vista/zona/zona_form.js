
$(document).ready(function(){
  $('#form_zona').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"zona/zona_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_zona").serialize(),
				beforeSend: function() {
					$('#zona_mensaje').show(400);
					$('#btn_guardar_zona').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#zona_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#zona_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		zona_tabla();
		      		$('#modal_registro_zona').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#zona_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#zona_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_zona').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#zona_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#zona_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_zona_nom: {
				required: true,
				minlength: 2
			},
			txt_zona_pro: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			txt_zona_nom: {
				required: "Ingrese un nombre para el Zona",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_zona_pro: {
				required: "Ingrese una Provincia para el Zona",
				minlength: "La Provincia debe tener como mínimo 2 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
