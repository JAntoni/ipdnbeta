<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Zona extends Conexion{

    function insertar($zona_nom, $zona_pro){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_zonaregistral(tb_zona_nom, tb_zona_pro)
          VALUES (:zona_nom, :zona_pro)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":zona_nom", $zona_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":zona_pro", $zona_pro, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($zona_id, $zona_nom, $zona_pro){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_zonaregistral SET tb_zona_nom =:zona_nom, tb_zona_pro =:zona_pro WHERE tb_zona_id =:zona_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":zona_id", $zona_id, PDO::PARAM_INT);
        $sentencia->bindParam(":zona_nom", $zona_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":zona_pro", $zona_pro, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($zona_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_zonaregistral WHERE tb_zona_id =:zona_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":zona_id", $zona_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($zona_id){
      try {
        $sql = "SELECT * FROM tb_zonaregistral WHERE tb_zona_id =:zona_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":zona_id", $zona_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }


    function listar_zonas(){
      try {
        $sql = "SELECT * FROM tb_zonaregistral";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
