<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Zona.class.php');
  $oZona = new Zona();

  $result = $oZona->listar_zonas();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_zona_id'].'</td>
          <td>'.$value['tb_zona_nom'].'</td>
          <td>'.$value['tb_zona_pro'].'</td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="zona_form(\'M\','.$value['tb_zona_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="zona_form(\'E\','.$value['tb_zona_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_zonas" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Provincia</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
