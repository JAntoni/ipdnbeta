<?php
require_once('Zona.class.php');
$oZona = new Zona();

$zona_id = (empty($zona_id))? 0 : $zona_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oZona->listar_zonas();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($zona_id == $value['tb_zona_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_zona_id'].'" '.$selected.'>'.$value['tb_zona_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>