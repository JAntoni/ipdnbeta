<?php
  require_once ("../../core/usuario_sesion.php");
  require_once('../balanceventa/Balanceventa.class.php');
  $oBalanceventa = new Balanceventa();
  require_once("../ventavehiculo/Ventavehiculo.class.php");
  $oVentavehiculo = new Ventavehiculo();
  require_once('../balanceventadetalle/Balanceventadetalle.class.php');
  $oBalanceventadetalle = new Balanceventadetalle();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  
  $usuario_action = $_POST['action']?$_POST['action']:"leer"; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $balanceventa_id = intval($_POST['balanceventa_id']);

  //$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

    //si la accion es modificar, mostramos los datos del stockunidad por su ID
    if(intval($balanceventa_id) > 0){
      $bandera = 1;

        $filas = '';
        $detalles = $oBalanceventadetalle->listar_balancedetalle_balance($balanceventa_id);
        if($detalles['estado'] == 1){
            foreach ($detalles['data'] as $key => $value) {
                $filas .='<tr class="odd" style="font-weight:bold;" id="tabla_fila">';

                $filas .= '
                            <td id="tabla_fila">'.$value['tb_credito_id'].'</td>
                            <td id="tabla_fila">'.moneda_mysql($value['tb_balanceventa_detalle_colocacion']).'</td>
                            <td id="tabla_fila">'.moneda_mysql($value['tb_balanceventa_detalle_pagos']).'</td>
                        ';

                $filas .='</tr>';

            }
        }else{
            $filas .='<tr class="odd" style="font-weight:bold;" id="tabla_fila">
                        <td id="tabla_fila" colspan="4">NO SE ENCONTRARON DATOS</td>
                    </tr>';
        }
    }
    elseif(intval($balanceventa_id) == 0){
            $bandera = 4;
            $mensaje = "NO SE HA ENCONTRADO EL BALANCE INDICADO";
    }
    

?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_balanceventa_detalle" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_balanceventa" method="post">
          <input type="hidden" name="action" value="<?php echo $usuario_action;?>">
          <input type="hidden" name="hdd_balanceventa_id" value="<?php echo $balanceventa_id;?>">
          <input type="hidden" name="hdd_stockunidad_id" value="<?php echo $stockunidad_id;?>">
          <input type="hidden" name="hdd_venta_fec" value="<?php echo $venta_fec;?>">
          <input type="hidden" name="hdd_arr_datos_balance" value='<?php echo json_encode($arr_balance);?>'>
          
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <table cellspacing="1" class="table table-hover">
                        <thead>
                            <tr id="tabla_cabecera">
                                <th id="tabla_cabecera_fila">ID CREDITO</th>
                                <th id="tabla_cabecera_fila">COLOCACION</th>
                                <th id="tabla_cabecera_fila">PAGOS</th>
                            </tr>
                        </thead>
                        <tbody id="balance_boby">
                            <?php echo $filas;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta stockunidad?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="stockunidad_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'insertar'): ?>
                <button type="submit" class="btn btn-info">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_stockunidad">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/balanceventa/balanceventa_form.js';?>"></script>
