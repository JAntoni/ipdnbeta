<?php
  require_once ("../../core/usuario_sesion.php");
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../balanceventa/Balanceventa.class.php');
  $oBalanceventa = new Balanceventa();
  require_once("../ingreso/Ingreso.class.php");
  $oIngreso = new Ingreso();
  require_once("../gasto/Gasto.class.php");
  $oGasto = new Gasto();
  require_once("../ventavehiculo/Ventavehiculo.class.php");
  $oVentavehiculo = new Ventavehiculo();
  require_once("../stockunidad/Stockunidad.class.php");
  $oStockunidad = new Stockunidad();
  require_once('../balanceventadetalle/Balanceventadetalle.class.php');
  $oBalanceventadetalle = new Balanceventadetalle();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  
  $direc = 'balanceventa';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $balanceventa_id = intval($_POST['balanceventa_id']);
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'leer')
    $titulo = 'Balance Venta Registrado';
  elseif($usuario_action == 'insertar')
    $titulo = 'Registrar Balance Venta';
  elseif($usuario_action == 'modificar')
    $titulo = 'Editar Balance Venta';
  elseif($usuario_action == 'eliminar')
    $titulo = 'Eliminar Balance Venta';
  else
    $titulo = 'Acción de Usuario Desconocido';
    
  //$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

    //si la accion es modificar, mostramos los datos del stockunidad por su ID
    $bandera = 1;
    if(intval($balanceventa_id) == 0){
        $ventavehiculo_id = $_POST['ventavehiculo_id'];
        $stockunidad_id = $_POST['stockunidad_id'];
        $arr_balance = [];

        $result2 = $oStockunidad->mostrarUno($stockunidad_id);
        if ($result2['estado'] == 1) {
        $vehiculoplaca  = $result2['data']['tb_credito_vehpla'];
        $credito_id  = $result2['data']['tb_credito_id'];
        $creditotipo_id  = $result2['data']['tb_creditotipo_id'];
        }
        $result2 = NULL;

        if ($creditotipo_id == 1) {
            $tabla = 'tb_creditomenor';
            require_once("../creditomenor/Creditomenor.class.php");
            $oCredito = new Creditomenor();
        }
        if ($creditotipo_id == 2) {
            $tabla = 'tb_creditoasiveh';
            require_once('../creditoasiveh/Creditoasiveh.class.php');
            $oCredito = new Creditoasiveh();
        }
        if ($creditotipo_id == 3) {
            $tabla = 'tb_creditogarveh';
            require_once('../creditogarveh/Creditogarveh.class.php');
            $oCredito = new Creditogarveh();
        }
        if ($creditotipo_id == 4) {
            $tabla = 'tb_creditohipo';
            require_once('../creditohipo/Creditohipo.class.php');
            $oCredito = new Creditohipo();
        }
        
        $resultCredito = $oCredito->mostrarUno($credito_id);
        if($resultCredito['estado'] == 1){
          $fecha_credito = $resultCredito['data']['tb_credito_fecfac'];
        }

        $filas = '';
        $creditos = $oBalanceventa->listar_creditos_balance($tabla, $credito_id, $vehiculoplaca);
        if($creditos['estado'] == 1){
            $readonly = "readonly";
            $gasto_total = 0;
            $gasto_soles = 0;
            $gasto_dolares = 0;
            $colocacion_total = 0;
            $pagos_total = 0;
            $venta_total = 0;

            $result3 = $oVentavehiculo->consultar_datos_venta_vehiculo($credito_id, $creditotipo_id);
            if ($result3['estado'] == 1) {
                $monto_base = intval($result3['data']['tb_ventavehiculo_bas']); 
                $monto_sob = intval($result3['data']['tb_ventavehiculo_sob']);
                $venta_fec = $result3['data']['tb_ventavehiculo_fec'];

                $venta_total = $monto_base + $monto_sob;
            }    
            $gastos_s = $oGasto->gasto_total_por_credito_moneda($credito_id, $creditotipo_id, 1);
            if($gastos_s['estado']==1)
                $gasto_soles = $gastos_s['data']['gasto_total'];
            $gastos_d = $oGasto->gasto_total_por_credito_moneda($credito_id, $creditotipo_id, 2);
            if($gastos_d['estado']==1)
                $gasto_dolares = $gastos_d['data']['gasto_total'];
            
            $gasto_total = $gasto_soles + $gasto_dolares;

            foreach ($creditos['data'] as $key => $value) {
                $filas .='<tr class="odd" style="font-weight:bold;" id="tabla_fila">';
                $ingreso_total = 0;
                $ingreso_soles = 0;
                $ingreso_dolares = 0;

                if($value['tb_moneda_id'] == 1){
                  $credito_preaco = $value['tb_credito_preaco'];
                  $colocacion_total += $credito_preaco;
                }elseif($value['tb_moneda_id'] == 2){
                  $credito_preaco = $value['tb_credito_preaco'] * $value['tb_credito_tipcam'];
                  $colocacion_total += ($value['tb_credito_preaco'] * $value['tb_credito_tipcam']);
                }
                
                $ingresos_s = $oIngreso->ingreso_total_por_credito_cuotadetalle_fecha($value['tb_credito_id'], $creditotipo_id, 1, $fecha_credito, $venta_fec);
                if($ingresos_s['estado']==1)
                    $ingreso_soles = $ingresos_s['data']['importe_total'];
                $ingresos_d = $oIngreso->ingreso_total_por_credito_cuotadetalle_fecha($value['tb_credito_id'], $creditotipo_id, 2, $fecha_credito, $venta_fec);
                if($ingresos_d['estado']==1)
                    $ingreso_dolares = $ingresos_d['data']['importe_total'];



                $ingreso_total = $ingreso_soles + $ingreso_dolares;
                $pagos_total += $ingreso_total;

                $filas .= '
                            <td id="tabla_fila">'.$value['tb_credito_id'].'</td>
                            <td id="tabla_fila">'.moneda_mysql($credito_preaco).'</td>
                            <td id="tabla_fila">'.moneda_mysql($ingreso_total).'</td>
                        ';

                $filas .='</tr>';

                $arr = [
                            'IDCREDITO' => $value['tb_credito_id'],
                            'COLOCACION' => $value['tb_credito_preaco'],
                            'PAGOS' => $ingreso_total
                        ];
                array_push($arr_balance, $arr);
            }

            $rentabilidad = $venta_total - ($colocacion_total + $gasto_total - $pagos_total);
        }else{
            $filas .='<tr class="odd" style="font-weight:bold;" id="tabla_fila">
                        <td id="tabla_fila" colspan="4">NO SE ENCONTRARON DATOS</td>
                    </tr>';
        }
    }
    elseif(intval($balanceventa_id)>0 && $usuario_action=="leer"){
        $result = $oBalanceventa->mostrarUno($balanceventa_id);
        if ($result['estado'] == 1) {
            $readonly = "readonly";
            $colocacion_total  = $result['data']['tb_balanceventa_colocacion'];
            $pagos_total  = $result['data']['tb_balanceventa_pagos'];
            $gasto_total  = $result['data']['tb_balanceventa_gastos'];
            $venta_total  = $result['data']['tb_balanceventa_ventafinal'];
            $rentabilidad  = $result['data']['tb_balanceventa_rentabilidad'];

        $result1 = $oBalanceventadetalle->listar_balancedetalle_balance($balanceventa_id);
        if($result1['estado'] == 1){
          foreach ($result1['data'] as $key => $value) {
            $filas .='<tr class="odd" style="font-weight:bold;" id="tabla_fila">
                        <td id="tabla_fila">'.$value['tb_credito_id'].'</td>
                        <td id="tabla_fila">'.moneda_mysql($value['tb_balanceventa_detalle_colocacion']).'</td>
                        <td id="tabla_fila">'.moneda_mysql($value['tb_balanceventa_detalle_pagos']).'</td>
                      </tr>';
          }
        }
        }else{
            $bandera = 1;
            $mensaje = "NO SE HA ENCONTRADO EL BALANCE INDICADO";
        }
    }
    

?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_balanceventa_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_balanceventa" method="post">
          <input type="hidden" name="action" value="<?php echo $usuario_action;?>">
          <input type="hidden" name="hdd_balanceventa_id" value="<?php echo $balanceventa_id;?>">
          <input type="hidden" name="hdd_stockunidad_id" value="<?php echo $stockunidad_id;?>">
          <input type="hidden" name="hdd_venta_fec" value="<?php echo $venta_fec;?>">
          <input type="hidden" name="hdd_arr_datos_balance" value='<?php echo json_encode($arr_balance);?>'>
          
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <table cellspacing="1" class="table table-hover">
                        <thead>
                            <tr id="tabla_cabecera">
                                <th id="tabla_cabecera_fila">ID CREDITO</th>
                                <th id="tabla_cabecera_fila">COLOCACION</th>
                                <th id="tabla_cabecera_fila">PAGOS</th>
                            </tr>
                        </thead>
                        <tbody id="balance_boby">
                            <?php echo $filas;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label for="txt_colocacion" class="control-label">COLOCACION :</label>
                    <input <?php echo $readonly;?> type="text" name="txt_colocacion" id="txt_colocacion" class="form-control input-sm moneda" value="<?php echo $colocacion_total;?>">
                </div>
                <div class="col-md-3">
                    <label for="txt_pagos" class="control-label">PAGOS :</label>
                    <input <?php echo $readonly;?> type="text" name="txt_pagos" id="txt_pagos" class="form-control input-sm moneda" value="<?php echo $pagos_total;?>">
                </div>
                <div class="col-md-3">
                    <label for="txt_gastos" class="control-label">GASTOS :</label>
                    <input <?php echo $readonly;?> type="text" name="txt_gastos" id="txt_gastos" class="form-control input-sm moneda" value="<?php echo $gasto_total;?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 p-4 border border-red">
                    <label for="txt_ventafinal" class="control-label">VENTA FINAL :</label>
                    <input <?php echo $readonly;?> type="text" name="txt_ventafinal" id="txt_ventafinal" class="form-control input-sm moneda" value="<?php echo $venta_total;?>">
                </div>
                <div class="col-md-4 p-4 border border-red">
                    <label for="txt_rentabilidad" class="control-label">RENTABILIDAD :</label>
                    <input style="text-align:center; color: blue; font-weight: bold; font-size: 15px;" <?php echo $readonly;?> type="text" name="txt_rentabilidad" id="txt_rentabilidad" class="form-control input-sm moneda" value="<?php echo $rentabilidad;?>">
                </div>
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta stockunidad?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="stockunidad_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'insertar'): ?>
                <button type="submit" class="btn btn-info">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_stockunidad">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/balanceventa/balanceventa_form.js';?>"></script>
