$(document).ready(function() {
    balanceventa_tabla();
    //rendicioncuentas_tabla()
    $('#datetimepicker1, #datetimepicker2').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy",
      //startDate: "-0d"
      endDate : new Date()
    });
    $("#datetimepicker1").on("change", function (e) {
      var startVal = $('#txt_filtro_fec1').val();
      $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
      var endVal = $('#txt_filtro_fec2').val();
      $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });
});


function balanceventa_tabla() {
    $.ajax({
      type: "POST",
      url: VISTA_URL + "balanceventa/balanceventa_tabla.php",
      async: false,
      dataType: "html",
      data: $("#form_balanceventa_filtro").serialize(),
      beforeSend: function () {
        $("#div_balanceventa_tabla").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#div_balanceventa_tabla").html(html);
      },
      complete: function (data) {
        //estilos_datatable();
      },
      error: function (data) {
        $("#cgv_mensaje_tbl").html(
          "ERROR AL CARGAR DATOS: " + data.responseText
        );
      }
    });
}
function estilos_datatable() {
    datatable_global = $("#tbl_balanceventa").DataTable({
      pageLength: 50,
      responsive: true,
      language: {
        lengthMenu: "Mostrar _MENU_ registros por página",
        zeroRecords: "Ninguna coincidencia para la búsqueda",
        info: "Mostrado _END_ registros",
        infoEmpty: "Ningún registro disponible",
        infoFiltered: "(filtrado de _MAX_ registros totales)",
        search: "Buscar:",
      },
      order: [[0,'DESC']],
      columnDefs: [{ targets: [1,3], orderable: true }],
    });
    datatable_texto_filtrar();
}


function limpiar_filtro(){
    $("#txt_filtro_fec1").val($("#fecha_hoy").val())
    $("#txt_filtro_fec2").val($("#fecha_hoy").val())
    $('#txt_vehiculo_pla').val("");
}



function ver_detalle(balanceventa_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "balanceventa/balanceventa_detalle_form.php",
        async: true,
        dataType: "html",
        data: {
          balanceventa_id: balanceventa_id
        },
        beforeSend: function () {
          $("#h3_modal_title").text("Cargando Formulario");
          $("#modal_mensaje").modal("show");
        },
        success: function (html) {
          $("#div_balanceventa_detalle_venta_form").html(html);
          $("#modal_balanceventa_detalle").modal("show");
          $("#modal_mensaje").modal("hide");
    
          modal_width_auto("modal_balanceventa_detalle", 40);
          modal_height_auto("modal_balanceventa_detalle");
          modal_hidden_bs_modal("modal_balanceventa_detalle", "limpiar");
        },
        complete: function (data) {
          // console.log(data);
        },
        error: function (data) {
          alerta_error("Advertencia",''+data.responseText);
        }
    });
}