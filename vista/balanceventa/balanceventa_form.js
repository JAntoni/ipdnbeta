$(document).ready(function () {

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.99'
    });


    $("#form_balanceventa").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"balanceventa/balanceventa_controller.php",
				async:true,
				dataType: "json",
				data: $("#form_balanceventa").serialize(),
				beforeSend: function() {
					
				},
				success: function(data){       
                    if(data.estado>0){
                        swal_success("SISTEMA",data.mensaje,2000);
                        $('#modal_balanceventa_form').modal('hide');
                        stockunidad_tabla()
                    }
                    else{
                        swal_warning("AVISO",data.mensaje,5000);
                    }
				},
				complete: function(data){
					console.log(data);
				}
			});
		},
		rules: {

		},
		messages: {

		}
	});
})