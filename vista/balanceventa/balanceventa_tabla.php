<?php

    if (defined('APP_URL')) {
        require_once(APP_URL.'core/usuario_sesion.php');
        require_once(VISTA_URL.'funciones/funciones.php');
        require_once(VISTA_URL.'funciones/fechas.php');
    } else {
        require_once('../../core/usuario_sesion.php');
        require_once('../funciones/funciones.php');
        require_once('../funciones/fechas.php');
    }

  require_once('Balanceventa.class.php');
  $oBalanceventa = new Balanceventa();


  $filtro_fec1 = fecha_mysql($_POST['txt_filtro_fec1']);
  $filtro_fec2 = fecha_mysql($_POST['txt_filtro_fec2']);
  $vehiculo_placa = $_POST['txt_vehiculo_pla'];

  $result = $oBalanceventa->listar_balances($filtro_fec1, $filtro_fec2, $vehiculo_placa);
  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_balanceventa_id'].'</td>
          <td>'.mostrar_moneda($value['tb_balanceventa_colocacion']).'</td>
          <td>'.mostrar_moneda($value['tb_balanceventa_pagos']).'</td>
          <td>'.mostrar_moneda($value['tb_balanceventa_gastos']).'</td>
          <td>'.mostrar_moneda($value['tb_balanceventa_ventafinal']).'</td>
          <td>'.mostrar_moneda($value['tb_balanceventa_rentabilidad']).'</td>
          <td>'.mostrar_fecha($value['tb_balanceventa_fec']).'</td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="ver_detalle('.$value['tb_balanceventa_id'].')"><i class="fa fa-eye"></i></a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    $tr ='<tr><td id="tabla_cabecera_fila">0</td><td id="tabla_cabecera_fila" colspan="7">'.$result['mensaje'].'</td></tr>';
    //exit();
  }

?>
<table id="tbl_balanceventa" class="table table-bordered table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila">COLOCACION TOTAL</th>
      <th id="tabla_cabecera_fila">PAGOS TOTAL</th>
      <th id="tabla_cabecera_fila">GASTOS TOTAL</th>
      <th id="tabla_cabecera_fila">VENTA FINAL</th>
      <th id="tabla_cabecera_fila">RENTABILIDAD</th>
      <th id="tabla_cabecera_fila">FECHA</th>
      <th id="tabla_cabecera_fila">OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>

