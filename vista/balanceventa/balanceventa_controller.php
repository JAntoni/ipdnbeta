<?php
require_once('../../core/usuario_sesion.php');

require_once ("../balanceventa/Balanceventa.class.php");
$oBalanceventa = new Balanceventa();
require_once ("../balanceventadetalle/Balanceventadetalle.class.php");
$oBalanceventadetalle = new Balanceventadetalle();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if($_POST['action']=="insertar")
{
    $response = [
        'estado' => 0,
        'mensaje' => 'Hace falta Datos'
    ];

    if(!empty($_POST['hdd_arr_datos_balance']) and !empty($_POST['txt_rentabilidad']))
	{
        $arr_detalle = json_decode($_POST['hdd_arr_datos_balance']);
        
        $oBalanceventa->stockunidad_id = $_POST['hdd_stockunidad_id'];
        $oBalanceventa->balanceventa_colocacion = moneda_mysql($_POST['txt_colocacion']);
        $oBalanceventa->balanceventa_pagos = moneda_mysql($_POST['txt_pagos']);
        $oBalanceventa->balanceventa_gastos = moneda_mysql($_POST['txt_gastos']);
        $oBalanceventa->balanceventa_ventafinal = moneda_mysql($_POST['txt_ventafinal']);
        $oBalanceventa->balanceventa_rentabilidad = moneda_mysql($_POST['txt_rentabilidad']);
        $oBalanceventa->balanceventa_fec = fecha_mysql(date('Y-m-d'));
        $oBalanceventa->balanceventa_usureg = $_SESSION['usuario_id'];
        $oBalanceventa->balanceventa_xac = 1;

        $result = $oBalanceventa->insertar();
            if(intval($result['estado']) == 1){
                $response['estado'] = 2;
                $response['mensaje'] = "Balance Venta Registrado. ";
                $balanceventa_id = $result['balanceventa_id'];
                foreach ($arr_detalle as $key => $detalle) {
                    //var_dump($detalle->IDCREDITO);
                    $oBalanceventadetalle->balanceventa_id = $balanceventa_id;
                    $oBalanceventadetalle->credito_id = $detalle->IDCREDITO;
                    $oBalanceventadetalle->balanceventa_detalle_colocacion = moneda_mysql($detalle->COLOCACION);
                    $oBalanceventadetalle->balanceventa_detalle_pagos = moneda_mysql($detalle->PAGOS);
                    $oBalanceventadetalle->balanceventa_detalle_fec = fecha_mysql(date('Y-m-d'));
                    $oBalanceventadetalle->balanceventa_detalle_usureg = $_SESSION['usuario_id'];  
                    $oBalanceventadetalle->balanceventa_detalle_xac = 1;    
        
                    $result1 = $oBalanceventadetalle->insertar();
                    if(intval($result1['estado']) == 1){
                        $response['estado'] = 1;
                        $response['mensaje'] = "Detalles Registrados.";
                    }
                    $result1 = null;
                }
            }
        $result = null;
    }
    echo json_encode($response);

}
?>