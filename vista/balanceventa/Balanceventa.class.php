<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Balanceventa extends Conexion {

    public $balanceventa_id;
    public $stockunidad_id;
    public $balanceventa_colocacion;
    public $balanceventa_pagos;
    public $balanceventa_gastos;
    public $balanceventa_ventafinal;
    public $balanceventa_rentabilidad;
    public $balanceventa_fec;
    public $balanceventa_usureg;
    public $balanceventa_usumod;
    public $balanceventa_xac;

    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_balanceventa (
                tb_stockunidad_id,
                tb_balanceventa_colocacion,
                tb_balanceventa_pagos,
                tb_balanceventa_gastos,
                tb_balanceventa_ventafinal,
                tb_balanceventa_rentabilidad,
                tb_balanceventa_fec,
                tb_balanceventa_usureg,
                tb_balanceventa_xac)
              VALUES (
                :stockunidad_id,
                :balanceventa_colocacion,
                :balanceventa_pagos,
                :balanceventa_gastos,
                :balanceventa_ventafinal,
                :balanceventa_rentabilidad,
                :balanceventa_fec,
                :balanceventa_usureg,
                :balanceventa_xac);";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":stockunidad_id", $this->stockunidad_id, PDO::PARAM_INT);
            $sentencia->bindParam(":balanceventa_colocacion", $this->balanceventa_colocacion, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_pagos", $this->balanceventa_pagos, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_gastos", $this->balanceventa_gastos, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_ventafinal", $this->balanceventa_ventafinal, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_rentabilidad", $this->balanceventa_rentabilidad, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_fec", $this->balanceventa_fec, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_usureg", $this->balanceventa_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":balanceventa_xac", $this->balanceventa_xac, PDO::PARAM_INT);

            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['balanceventa_id'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Gasto registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function listar_creditos_balance($tabla, $credito_id, $vehiculo_placa){
        try {
          $sql = "SELECT * FROM $tabla
                  WHERE tb_credito_vehpla = :vehplaca
                  AND (
                    (tb_credito_id = :credito_id OR tb_credito_est in (7))
                    OR (tb_credito_id > :credito_id OR tb_credito_est in (3,4))
                    )";
    
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":vehplaca", $vehiculo_placa, PDO::PARAM_STR);
          $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
          $sentencia->execute();
    
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }
    
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function listar_balances($balance_fec1, $balance_fec2, $vehiculo_placa){

        $placa = "";
        if (intval($vehiculo_placa) != "")
            $placa = " AND su.tb_credito_vehpla = :vehiculo_placa";

        try {
            $sql = "SELECT * 
                    FROM tb_balanceventa bv 
                    INNER JOIN tb_stockunidad su ON bv.tb_stockunidad_id=su.tb_stockunidad_id
                    INNER JOIN tb_ventavehiculo vv ON su.tb_stockunidad_id = vv.tb_stockunidad_id
                    WHERE vv.tb_ventavehiculo_fec BETWEEN :balance_fec1 AND :balance_fec2" . $placa;
    
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":balance_fec1", $balance_fec1, PDO::PARAM_STR);
          $sentencia->bindParam(":balance_fec2", $balance_fec2, PDO::PARAM_STR);
          if (intval($vehiculo_placa) != "")
            $sentencia->bindParam(":vehiculo_placa", $vehiculo_placa, PDO::PARAM_STR);
          $sentencia->execute();
    
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay Balances registrados";
            $retorno["data"] = "";
          }
    
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function consultar_balance_stockunidad($stockunidad_id){
        try {
            $sql = "SELECT * FROM tb_balanceventa WHERE tb_stockunidad_id =:stockunidad_id AND tb_balanceventa_xac = 1";
    
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":stockunidad_id", $stockunidad_id, PDO::PARAM_INT);
            $sentencia->execute();
    
            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay resultados";
              $retorno["data"] = "";
            }
    
            return $retorno;
        } catch (Exception $e) {
        throw $e;
        }
    }

    function mostrarUno($balanceventa_id)
    {
      try {
        $sql = "SELECT * FROM tb_balanceventa WHERE tb_balanceventa_id =:balanceventa_id AND tb_balanceventa_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":balanceventa_id", $balanceventa_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay resultados";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
}