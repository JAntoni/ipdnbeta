<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
} else {
  require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();

$action = $_POST['hdd_action_tasacionsug'];
//action  garantia_id  articulo_id

if ($action == 'consultar') {
  $articulo_id = intval($_POST['cbo_tasacionsugfil_articulo']);
  $tiempo = intval($_POST['cbo_tasacionsugfil_tiempo']);

  $tabla_cabecera =
    '<thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila" style="width: 17%;">Promedio tasac.</th>
                <th id="tabla_cabecera_fila" style="width: 17%;">Promedio prest.</th>
                <th id="tabla_cabecera_fila" style="width: 17%;">Último tasac.</th>
                <th id="tabla_cabecera_fila" style="width: 17%;">Último prest.</th>
                <th id="tabla_cabecera_fila" style="width: 42%;">Recientes(5)</th>
            </tr>
        </thead>';
  $tabla_cuerpo = "<tbody>";

  if ($articulo_id > 0 && $articulo_id != 63) {
    //casos por cada artículo
    if ($articulo_id == 1) { //laptop
      $marca_opt = intval($_POST['cbo_tasacionsugfil_marcalap']);
      $core = intval($_POST['cbo_tasacionsugfil_core']);
      if ($marca_opt >= 0) {
        if ($marca_opt > 0) {
          $marca_nom = strtoupper($_POST['lapmarca_nom']);
          $extension = "AND (UPPER(tb_garantia_pro) LIKE '%$marca_nom%' OR UPPER(tb_garantia_det) LIKE '%$marca_nom%')";
        } else {
          $i = 0;
          $lista = array('ACER', 'ASUS', 'COMPAQ', 'DELL', 'HP', 'LENOVO', 'TOSHIBA');
          $extension = "AND (";
          foreach ($lista as $marca) {
            $extension .= $i > 0 ? ' AND ' : '';
            $extension .= "UPPER(tb_garantia_pro) NOT LIKE '%$marca%' AND UPPER(tb_garantia_det) NOT LIKE '%$marca%'";
            $i++;
          }
          $extension .= ")";
        }
      }
      if ($core > 0) {
        $core = (2 * $core) + 1;
        $extension .= " AND (UPPER(tb_garantia_pro) LIKE '%I$core%' OR UPPER(tb_garantia_det) LIKE '%I$core%')";
      }
    } elseif ($articulo_id == 20) { //iphone
      $numero = intval($_POST['cbo_tasacionsugfil_modelo']);
      if ($numero < 4) {
        $numero += 5;
      } elseif ($numero > 4) {
        $numero += 6;
      } else {
        $numero = 'X';
      }

      $extension = "AND (UPPER(tb_garantia_pro) LIKE '%IPHONE $numero%' OR UPPER(tb_garantia_det) LIKE '%IPHONE $numero%')";
    } elseif ($articulo_id == 16) { //tablet
      $marca_opt = intval($_POST['cbo_tasacionsugfil_marcatab']);
      if ($marca_opt >= 0) {
        if ($marca_opt > 0) {
          $marca_nom = strtoupper($_POST['tabmarca_nom']);
          $texto = "(UPPER(tb_garantia_pro) LIKE '%$marca_nom%' OR UPPER(tb_garantia_det) LIKE '%$marca_nom%')";
          $extension = $marca_opt == 4 ? "AND ($texto OR (UPPER(tb_garantia_pro) LIKE '%GALAXY%' OR UPPER(tb_garantia_det) LIKE '%GALAXY%'))" : "AND $texto";
        } else {
          $i = 0;
          $lista = array('ADVANCE', 'HUAWEI', 'LENOVO', 'SAMSUNG', 'GALAXY');
          $extension = "AND (";
          foreach ($lista as $marca) {
            $extension .= $i > 0 ? ' AND ' : '';
            $extension .= "UPPER(tb_garantia_pro) NOT LIKE '%$marca%' AND UPPER(tb_garantia_det) NOT LIKE '%$marca%'";
            $i++;
          }
          $extension .= ")";
        }
      }
    }

    $res = $oGarantia->tasacionsug($extension, $tiempo, $articulo_id);

    if ($res['estado'] == 1) {
      $tabla_cuerpo .= armar_tbody($res);
      //$tabla_cuerpo .= "<tr><td colspan=\"5\">{$res['sql']}</td></tr>";
    } else {
      $tabla_cuerpo .= "<tr><td colspan=\"5\">No hay coincidencias con los filtros</td></tr>";
    }
  } else {
    $tabla_cuerpo .= "<tr><td colspan=\"5\">No hay coincidencias con los filtros</td></tr>";
  }

  $tabla_cuerpo .= "</tbody>";
  $data['tabla'] = $tabla_cabecera . $tabla_cuerpo;
  echo json_encode($data);
}

if($action == 'buscar_producto'){
  $product = trim($_POST['product']);
  $palabras = strlen($product);

  if($palabras > 8){
    $product = str_replace(" ", "%20", $product);
    $web_services = "http://buscaproducto.ipdnsac.com/?product=".$product;

    // Realizar la solicitud al servicio web y obtener la respuesta
    $response = file_get_contents($web_services);

    // Verificar si se pudo obtener la respuesta
    if ($response === false) {
      echo "Error al realizar la búsqueda en línea del producto ".$web_services;
    } 
    else {
      // Decodificar la respuesta JSON en un objeto o array de PHP
      $data = json_decode($response);

      // Verificar si la decodificación fue exitosa
      if ($data === null) {
        echo "Error al decodificar la respuesta JSON";
      } else {
        $table = '<div class="row" style="padding: 15px;">';

        foreach($data as $item){
          if(is_array($item)){
            $page = $item['page'];
            $name = $item['name'];
            $price = $item['price'];
            $image_url = $item['image_url'];
          }
          elseif(is_object($item)){
            $page = $item->page;
            $name = $item->name;
            $price = $item->price;
            $image_url = $item->image_url;
          }
          // Utilizar los valores de cada diccionario
          $table .= '
            <div class="col-md-3 shadow">
              <div class="" style="width: 100%">
                <img src="'.$image_url.'" class="card-img-top" alt="...">
                <div class="">
                  <h4><b>'.$price.'</b></h4>
                  <h5>'.$name.'</h5>
                  <a href="#" class="btn btn-block btn-primary">Ver en '.$page.'</a>
                </div>
              </div>
            </div>';
        }

        $table .= '</div>';
        echo $table;
      }
    }
  }
  else{
    echo 'Detalla mejor el producto, debe tener como mínimo 9 letras';
  }
  
}

function armar_tbody($res)
{
  //calc. promedio tasado, promedio prest
  $total_tasado = 0;
  $total_prestado = 0;
  $i = 0;
  $botones = '';
  foreach ($res['data'] as $key => $garantia) {
    if ($i == 0) {
      $ult_tasado = $garantia['tb_garantia_valtas'];
      $ult_prestado = $garantia['tb_garantia_val'];
    }
    $total_tasado += $garantia['tb_garantia_valtas'];
    $total_prestado += $garantia['tb_garantia_val'];
    $i++;
  }
  $i = null;
  $maximo_botones = count($res['data']) > 5 ? 5 : count($res['data']);
  for ($i = 0; $i < $maximo_botones; $i++) {
    $botones .= " <a class=\"btn btn-primary btn-xs\" style=\"font-size: 11.5px; line-height: 1;\" href=\"javascript:void(0)\" onclick=\"garantia_resumen({$res['data'][$i]['tb_garantia_id']})\">CM-{$res['data'][$i]['tb_credito_id']}</a> ";
  }

  $tabla_cuerpo = "<tr id=\"tabla_cabecera_fila\">";
  $tabla_cuerpo .= "<td id=\"tabla_fila\"> S/. " . mostrar_moneda($total_tasado / $res['cantidad']) . "</td>";
  $tabla_cuerpo .= "<td id=\"tabla_fila\"> S/. " . mostrar_moneda($total_prestado / $res['cantidad']) . "</td>";
  $tabla_cuerpo .= "<td id=\"tabla_fila\"> S/. " . mostrar_moneda($ult_tasado) . "</td>";
  $tabla_cuerpo .= "<td id=\"tabla_fila\"> S/. " . mostrar_moneda($ult_prestado) . "</td>";
  $tabla_cuerpo .= "<td id=\"tabla_fila\">";
  $tabla_cuerpo .= $botones;
  $tabla_cuerpo .= "</td>";
  $tabla_cuerpo .= "</tr>";
  return $tabla_cuerpo;
}
