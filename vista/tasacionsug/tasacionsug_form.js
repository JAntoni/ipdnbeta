$(document).ready(function () {
    $('#cbo_tasacionsugfil_articulo option[value="0"]').remove().change();

    $('.select2-tasac').select2({
        dropdownParent: $('#modal_tasacionsug_form')
    });

    $('#cbo_tasacionsugfil_articulo').change(function(){
        if ($(this).val() == '1') {//laptop
            $('.col_lap').show(150);
            
            $('#cbo_tasacionsugfil_modelo').val(-1).change();
            $('.col_iphone').hide(150);

            $('#cbo_tasacionsugfil_marcatab').val(-1).change();
            $('.col_tab').hide(150);
        } else if ($(this).val() == '20') {//iphone
            $('.col_iphone').show(150);

            $('#cbo_tasacionsugfil_marcalap').val(-1).change();
            $('#cbo_tasacionsugfil_core').val(-1).change();
            $('.col_lap').hide(150);

            $('#cbo_tasacionsugfil_marcatab').val(-1).change();
            $('.col_tab').hide(150);
        } else if ($(this).val() == '16'){//tablet
            $('.col_tab').show(150);

            $('#cbo_tasacionsugfil_marcalap').val(-1).change();
            $('#cbo_tasacionsugfil_core').val(-1).change();
            $('.col_lap').hide(150);

            $('#cbo_tasacionsugfil_modelo').val(-1).change();
            $('.col_iphone').hide(150);
        } else {
            $('#cbo_tasacionsugfil_marcalap').val(-1).change();
            $('#cbo_tasacionsugfil_core').val(-1).change();
            $('.col_lap').hide(150);

            $('#cbo_tasacionsugfil_modelo').val(-1).change();
            $('.col_iphone').hide(150);

            $('#cbo_tasacionsugfil_marcatab').val(-1).change();
            $('.col_tab').hide(150);
        }
    });
});

function tasacionsug(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "tasacionsug/tasacionsug_controller.php",
        async: true,
        dataType: "json",
        data: serializar_form(),
        success: function(data) {
            $('#tbl_tasacionsug').html(data.tabla);
        }
    });
}

function serializar_form(){
    var extra = `&lapmarca_nom=${$('#cbo_tasacionsugfil_marcalap option:selected').text()}`;
    extra += `&tabmarca_nom=${$('#cbo_tasacionsugfil_marcatab option:selected').text()}`;
    return $('#form_tasacionsug').serialize() + extra;
}

function garantia_resumen(garantia_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantia/garantia_resumen.php",
        async: true,
        dataType: "html",
        data: ({
            garantia_id: garantia_id,
            vista: 'tasacionsug',
            action: 'L',
            formenor_rec: $('#hdd_formenor_rec').val(),
            formenor_pro: $('#hdd_formenor_pro').val()
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_garantia_resumen').html(data);
                $('#modal_garantia_resumen').modal('show');
                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                //modal_width_auto('modal_garantia_resumen',50);
                modal_height_auto('modal_garantia_resumen'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_garantia_resumen', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        error: function (data) {
            alerta_error('Error', data.responseText);
        }
    });
}

function buscar_producto(){
  product = $('#txt_buscar_producto').val();
  product = product.trim();

  if(!product)
    return false;

  $.ajax({
    type: "POST",
    url: VISTA_URL + "tasacionsug/tasacionsug_controller.php",
    async: true,
    dataType: "html",
    data: ({
      hdd_action_tasacionsug: 'buscar_producto',
      product: product
    }),
    beforeSend: function () {
      notificacion_info('Buscando el producto en línea...', 5000);
    },
    success: function (data) {
      $('#tbl_tasacionsug').html(data);
    },
    error: function (data) {
      console.log(data);
      alerta_error('Error', data.responseText);
    }
  });
}
