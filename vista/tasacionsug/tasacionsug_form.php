<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
} else {
  require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../formulaventa/Formulamenor.class.php');
$oFormula = new Formulamenor();
$dts1 = $oFormula->mostrar_vigente();

if ($dts1['estado'] == 1) {
  $formenor_id =  $dts1['data']['tb_formula_id'];
  $formenor_nom = $dts1['data']['tb_formula_nom']; //nombre de la formula
  $formenor_rec = $dts1['data']['tb_formula_rec']; //valor de recuperacion, generalmente es 1.5 a mas
  $formenor_pro = $dts1['data']['tb_formula_pro']; //numero de meses de proyeccion
}

$listo = 1;
$mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
$titulo = 'Tasación sugerida de garantías';
$creditotipo_id = 1;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_tasacionsug_form" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo; ?></h4>
      </div>
      <form id="form_tasacionsug" method="post">
        <input type="hidden" name="hdd_formenor_rec" id="hdd_formenor_rec" value="<?php echo $formenor_rec; ?>">
        <input type="hidden" name="hdd_formenor_pro" id="hdd_formenor_pro" value="<?php echo $formenor_pro; ?>">
        <div class="modal-body">
          <label>Filtros</label>
          <div class="panel panel-danger">
            <input type="hidden" name="hdd_action_tasacionsug" id="hdd_action_tasacionsug" value="consultar">
            <div class="panel-body" style="padding: 10px 15px;">
              <div class="row form-group">
                <!-- col articulo -->
                <div class="col-md-3">
                    <label for="cbo_tasacionsugfil_articulo">Artículo:</label><br>
                    <select name="cbo_tasacionsugfil_articulo" id="cbo_tasacionsugfil_articulo" class="select2-tasac narrow wrap form-control mayus" style="width: 146.5px;">
                      <?php require_once('../articulo/articulo_select.php'); ?>
                    </select>
                </div>
                <!-- filtros personalizados laptop -->
                <div class="col-md-3 col_lap" style="display: none; width: 22%;">
                  <label for="cbo_tasacionsugfil_marcalap">Marca:</label><br>
                  <select name="cbo_tasacionsugfil_marcalap" id="cbo_tasacionsugfil_marcalap" class="select2-tasac narrow wrap form-control mayus" style="width: 125px;">
                    <option value="-1">SELECCIONE</option>
                    <option value="1" style="font-weight: bold;">ACER</option>
                    <option value="2" style="font-weight: bold;">ASUS</option>
                    <option value="3" style="font-weight: bold;">COMPAQ</option>
                    <option value="4" style="font-weight: bold;">DELL</option>
                    <option value="5" style="font-weight: bold;">HP</option>
                    <option value="6" style="font-weight: bold;">LENOVO</option>
                    <option value="7" style="font-weight: bold;">TOSHIBA</option>
                    <option value="0" style="font-weight: bold;">OTRAS</option>
                  </select>
                </div>
                <div class="col-md-2 col_lap" style="display: none;">
                  <label for="cbo_tasacionsugfil_core">Core:</label><br>
                  <select name="cbo_tasacionsugfil_core" id="cbo_tasacionsugfil_core" class="select2-tasac narrow wrap form-control mayus" style="width: 95px;">
                    <option value="-1">SELECCIONE</option>
                    <option value="1" style="font-weight: bold;">I3</option>
                    <option value="2" style="font-weight: bold;">I5</option>
                    <option value="3" style="font-weight: bold;">I7</option>
                    <option value="4" style="font-weight: bold;">I9</option>
                  </select>
                </div>
                <!-- filtros iphone -->
                <div class="col-md-3 col_iphone" style="display: none; width: 22%;">
                  <label for="cbo_tasacionsugfil_modelo">Modelo:</label><br>
                  <select name="cbo_tasacionsugfil_modelo" id="cbo_tasacionsugfil_modelo" class="select2-tasac narrow wrap form-control mayus" style="width: 125px;">
                    <option value="-1">SELECCIONE</option>
                    <option value="1">IPHONE 6</option>
                    <option value="2">IPHONE 7</option>
                    <option value="3">IPHONE 8</option>
                    <option value="4">IPHONE X (10)</option>
                    <option value="5">IPHONE 11</option>
                    <option value="6">IPHONE 12</option>
                    <option value="7">IPHONE 13</option>
                    <option value="8">IPHONE 14</option>
                  </select>
                </div>
                <!-- filtros personalizados tablet -->
                <div class="col-md-3 col_tab" style="display: none; width: 22%;">
                    <label for="cbo_tasacionsugfil_marcatab">Marca:</label><br>
                    <select name="cbo_tasacionsugfil_marcatab" id="cbo_tasacionsugfil_marcatab" class="select2-tasac narrow wrap form-control mayus" style="width: 125px;">
                        <option value="-1">SELECCIONE</option>
                        <option value="1" style="font-weight: bold;">ADVANCE</option>
                        <option value="2" style="font-weight: bold;">HUAWEI</option>
                        <option value="3" style="font-weight: bold;">LENOVO</option>
                        <option value="4" style="font-weight: bold;">SAMSUNG</option>
                        <option value="0" style="font-weight: bold;">OTRAS</option>
                    </select>
                </div>
                <!-- filtros tiempo -->
                <div class="col-md-3">
                    <label for="cbo_tasacionsugfil_tiempo">Tiempo:</label><br>
                    <select name="cbo_tasacionsugfil_tiempo" id="cbo_tasacionsugfil_tiempo" class="select2-tasac narrow wrap form-control mayus" style="width: 140px;">
                        <option value="1" style="font-weight: bold;">ULTIMOS 3 MESES</option>
                        <option value="2" style="font-weight: bold;">ULTIMOS 6 MESES</option>
                        <option value="3" style="font-weight: bold;">ULTIMOS 1 AÑO</option>
                        <option value="4" style="font-weight: bold;">ULTIMOS 2 AÑOS</option>
                        <option value="5" style="font-weight: bold;">ULTIMOS 4 AÑOS</option>
                    </select>
                </div>

                <div class="col-md-1" style="width: 4%">
                  <label for="btn_buscar" style="color:white;">.</label>
                  <span class="input-group-btn">
                    <button id="btn_buscar" class="btn btn-info btn-sm" type="button" onclick="tasacionsug()" title="Buscar">
                      <span class="fa fa-search icon"></span>
                    </button>
                  </span>
                </div>
              </div>

              <!-- INPUT Y BOTON PARA BUSCAR PRODUCTOS FUERA DEL SISTEMA-->
              <!--div class="row">
                <div class="col-md-8">
                  <input type='text' class="form-control input-sm" name="txt_buscar_producto" id="txt_buscar_producto" value=""/>
                </div>
                <div class="col-md-4">
                  <button id="btn_buscar_producto" class="btn btn-info btn-sm" type="button" onclick="buscar_producto()" title="Buscar">
                    <span class="fa fa-search icon"></span> Buscar Produto
                  </button>
                </div>
              </div -->
            </div>
          </div>
            
          <div class="row form-group">
            <div class="col-md-12">
              <table id="tbl_tasacionsug" class="table table-hover" style="word-wrap:break-word;"></table>
            </div>
          </div>
          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="tasacionsug_mensaje" style="display: none;">
          </div>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                  <button type="submit" class="btn btn-info" id="btn_guardar_tasacionsug">Guardar</button>
              <?php endif ?>
              <?php if ($usuario_action == 'E') : ?>
                  <button type="submit" class="btn btn-info" id="btn_guardar_tasacionsug">Aceptar</button>
              <?php endif ?>
              <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo "vista/tasacionsug/tasacionsug_form.js?ver=0075587776"; ?>"></script>
