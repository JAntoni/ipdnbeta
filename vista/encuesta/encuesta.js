function encuesta_form(usuario_act, encuesta_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"encuesta/encuesta_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      encuesta_id: encuesta_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_encuesta_form').html(data);
      	$('#modal_registro_encuesta').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_encuesta'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_encuesta', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'encuesta';
      	var div = 'div_modal_encuesta_form';
      	permiso_solicitud(usuario_act, encuesta_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function encuesta_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"encuesta/encuesta_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#encuesta_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_encuesta_tabla').html(data);
      $('#encuesta_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#encuesta_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function guardar_motivo_encuesta(encuesta_id, cliente_id){
  var motivoprestamo_id = $('#cmb_motivo_'+encuesta_id).val();
  var tipodedicacion_id = $('#cmb_tipodedicacion_'+encuesta_id).val();

  console.log('el motivo id: es' + motivoprestamo_id + ' // el tipo dedicacin es: ' + tipodedicacion_id);

  if(motivoprestamo_id <= 0 || tipodedicacion_id <= 0){
    alerta_warning('Importante', 'Selecciona un Motivo de Préstamo y una Dedicación de Cliente');
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"encuesta/encuesta_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'motivo',
      encuesta_id: encuesta_id,
      cliente_id: cliente_id,
      motivoprestamo_id: motivoprestamo_id,
      tipodedicacion_id: tipodedicacion_id
    }),
    beforeSend: function() {
      
    },
    success: function(data){
      if(parseInt(data.estado) > 0){
        notificacion_success(data.mensaje)

        $('.no_listo_'+encuesta_id).css('background-color', 'green');
        $('.no_tipo_'+encuesta_id).css('background-color', 'blue');
      }
      else
        alerta_error('ERROR', data.mensaje)
    },
    complete: function(data){
      console.log(data);
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}
$(document).ready(function() {
  console.log('cambios de encuesta 55');

});