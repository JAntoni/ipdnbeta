<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Encuesta extends Conexion{
    public $usuario_id;
    public $credito_id;
    public $cliente_id;
    public $credito_preaco;
    public $motivoprestamo_id;
    public $encuesta_motivo;
    public $encuesta_ocu;
    public $encuesta_des;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $columns = implode(', ', [
          'tb_usuario_id',
          'tb_credito_id',
          'tb_cliente_id',
          'tb_credito_preaco',
          'tb_motivoprestamo_id',
          'tb_encuesta_motivo',
          'tb_encuesta_ocu',
          'tb_encuesta_des',
        ]);

        $placeholders = implode(', ', array_map(function($key) {
            return ":$key";
          }, [
            'tb_usuario_id',
            'tb_credito_id',
            'tb_cliente_id',
            'tb_credito_preaco',
            'tb_motivoprestamo_id',
            'tb_encuesta_motivo',
            'tb_encuesta_ocu',
            'tb_encuesta_des',
          ])
        ); //function itera cada valor del array que se le asigne, devuelve: :tb_usuario_id, :tb_credito_id, :tb_cliente_id

        $sql = "INSERT INTO tb_encuesta($columns) VALUES ($placeholders)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_motivoprestamo_id", $this->motivoprestamo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_encuesta_motivo", $this->encuesta_motivo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_encuesta_ocu", $this->encuesta_ocu, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_encuesta_des", $this->encuesta_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_encuesta SET 
            tb_motivoprestamo_id =:motivoprestamo_id, 
            tb_encuesta_motivo =:encuesta_motivo,
            tb_encuesta_ocu =:encuesta_ocu,
            tb_encuesta_des =:encuesta_des
            WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":motivoprestamo_id", $this->motivoprestamo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":encuesta_motivo", $this->encuesta_motivo, PDO::PARAM_STR);
        $sentencia->bindParam(":encuesta_ocu", $this->encuesta_ocu, PDO::PARAM_STR);
        $sentencia->bindParam(":encuesta_des", $this->encuesta_des, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_encuesta SET tb_encuesta_xac = 0 WHERE tb_encuesta_id =:encuesta_id";

        $sentencia = $this->dblink->prepare($sql);
        

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar_por_credito($credito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_encuesta SET tb_encuesta_xac = 0 WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno(){
      try {
        $sql = "SELECT * FROM tb_encuesta WHERE tb_encuesta_id =:encuesta_id";

        $sentencia = $this->dblink->prepare($sql);
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrarUnoCreditoMenor($credito_id){
      try {
        $sql = "SELECT * FROM tb_encuesta WHERE tb_credito_id =:credito_id AND tb_encuesta_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_encuestas(){
      try {
        $sql = "SELECT * FROM tb_encuesta  encu
                INNER JOIN tb_cliente cli ON cli.tb_cliente_id = encu.tb_cliente_id ORDER BY tb_encuesta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }

    function modificar_campo($encuesta_id, $encuesta_columna, $encuesta_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($encuesta_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_encuesta SET ".$encuesta_columna." =:encuesta_valor WHERE tb_encuesta_id =:encuesta_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":encuesta_id", $encuesta_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":encuesta_valor", $encuesta_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":encuesta_valor", $encuesta_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el egreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  }

?>
