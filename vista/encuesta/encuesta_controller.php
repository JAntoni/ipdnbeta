<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../encuesta/Encuesta.class.php');
  $oEncuesta = new Encuesta();
	require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$encuesta_nom = mb_strtoupper($_POST['txt_encuesta_nom'], 'UTF-8');
 		$encuesta_des = $_POST['txt_encuesta_des'];
 		$area_id = $_POST['cmb_area_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Usuario Grupo.';
 		if($oEncuesta->insertar($encuesta_nom, $encuesta_des, $area_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$encuesta_id = intval($_POST['hdd_encuesta_id']);
 		$encuesta_nom = mb_strtoupper($_POST['txt_encuesta_nom'], 'UTF-8');
 		$encuesta_des = $_POST['txt_encuesta_des'];
 		$area_id = $_POST['cmb_area_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Usuario Grupo.';

 		if($oEncuesta->modificar($encuesta_id, $encuesta_nom, $encuesta_des, $area_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo modificado correctamente. '.$encuesta_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$encuesta_id = intval($_POST['hdd_encuesta_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Usuario Grupo.';

 		if($oEncuesta->eliminar($encuesta_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo eliminado correctamente. '.$encuesta_des;
 		}

 		echo json_encode($data);
 	}

	 elseif($action == 'motivo'){
		$encuesta_id = intval($_POST['encuesta_id']);
		$cliente_id = intval($_POST['cliente_id']);
		$motivoprestamo_id = intval($_POST['motivoprestamo_id']);
		$tipodedicacion_id = intval($_POST['tipodedicacion_id']);

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error para la encuesta';

		if($oEncuesta->modificar_campo($encuesta_id, 'tb_motivoprestamo_id', $motivoprestamo_id, 'INT')){
			$oCliente->modificar_campo($cliente_id, 'tb_tipodedicacion_id', $tipodedicacion_id, 'INT');
			$data['estado'] = 1;
			$data['mensaje'] = 'Motivo de Préstamo actualizado para la encuesta';
		}

		echo json_encode($data);
	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>