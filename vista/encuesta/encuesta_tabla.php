<?php
  require_once('Encuesta.class.php');
  $oEncuesta = new Encuesta();

  $result = $oEncuesta->listar_encuestas();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $motivoprestamo_id = $value['tb_motivoprestamo_id']; //se encuentra en la tabla encuesta
      $tipodedicacion_id = $value['tb_tipodedicacion_id']; //se encuenta en la tabla del cliente
      $select_motivo = lista_motivos_prestamo($motivoprestamo_id);
      $select_tipo = lista_tipo_dedicacion($tipodedicacion_id);

      $clase_listo = 'no_listo_'.$value['tb_encuesta_id'];
      if($motivoprestamo_id > 0)
        $clase_listo = 'clase_listo';
      
      $clase_listo_tipo = 'no_tipo_'.$value['tb_encuesta_id'];
      if($tipodedicacion_id > 0)
        $clase_listo_tipo = 'clase_listo';

      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_encuesta_id'].'</td>
          <td>'.$value['tb_credito_id'].'</td>
          <td>'.$value['tb_encuesta_motivo'].' / '.$value['tb_encuesta_ocu'].' / '.$value['tb_encuesta_des'].'</td>
          <td align="center" class="'.$clase_listo.'">
            <select name="cmb_motivoencuesta" id="cmb_motivo_'.$value['tb_encuesta_id'].'" class="selectpicker form-control" data-live-search="true" data-max-options="1">
              '.$select_motivo.'
            </select>
          </td>
          <td align="center" class="'.$clase_listo_tipo.'">
            <select name="cmb_tipodedicacion" id="cmb_tipodedicacion_'.$value['tb_encuesta_id'].'" class="selectpicker form-control" data-live-search="true" data-max-options="1">
              '.$select_tipo.'
            </select>
          </td>
          <td>
            <a class="btn btn-success btn-xs" title="Subir Imagen" onclick="guardar_motivo_encuesta('.$value['tb_encuesta_id'].', '.$value['tb_cliente_id'].')"><i class="fa fa-save"></i> Guardar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
  }

?>
<table id="tbl_encuestas" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Crédito ID</th>
      <th>Motivo</th>
      <th>Motivo Préstamo</th>
      <th>Dedicación</th>
      <th>Guardar</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>

<?php
  function lista_motivos_prestamo($motivoprestamo_id){
    require_once(VISTA_URL.'motivoprestamo/Motivoprestamo.class.php');
    $oMotivoprestamo = new Motivoprestamo();

    $option = '<option value="0"></option>';
    //PRIMER NIVEL
    $result = $oMotivoprestamo->listar_motivoprestamos();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value){
          $selected = '';
          if($motivoprestamo_id == $value['tb_motivoprestamo_id'])
            $selected = 'selected';

          $option .= '<option value="'.$value['tb_motivoprestamo_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_motivoprestamo_nom'].'</option>';
        }
      }
    $result = NULL;
    //FIN PRIMER NIVEL
    return $option;
  }
  function lista_tipo_dedicacion($tipodedicacion_id){
    require_once(VISTA_URL.'tipodedicacion/Tipodedicacion.class.php');
    $oTipodedicacion = new Tipodedicacion();

    $tipodedicacion_id =  (empty($tipodedicacion_id))? intval($_POST['tipodedicacion_id']) : $tipodedicacion_id;

    $option = '<option value="0"></option>';
    //PRIMER NIVEL
    $result = $oTipodedicacion->listar_tipodedicaciones();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value){
          $selected = '';
          if($tipodedicacion_id == $value['tb_tipodedicacion_id'])
            $selected = 'selected';

          $option .= '<option value="'.$value['tb_tipodedicacion_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_tipodedicacion_nom'].'</option>';
        }
      }
    $result = NULL;
    //FIN PRIMER NIVEL
    return $option;
  }
?>
<style>
  .clase_listo{
    background-color: green !important;
  }
</style>