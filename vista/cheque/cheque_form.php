<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../cheque/Cheque.class.php');
  $oCheque = new Cheque();
  require_once('../funciones/funciones.php');

  $direc = 'cheque';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cheque_id = $_POST['cheque_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Cheque Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Cheque';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Cheque';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Cheque';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cheque
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cheque'; $modulo_id = $cheque_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cheque por su ID
    $usuario_id = 0;
    if(intval($cheque_id) > 0){
      $result = $oCheque->mostrarUno($cheque_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para la cartaa de probación seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $cheque_tip = $result['data']['tb_cheque_tip'];
          $cheque_fec = $result['data']['tb_cheque_fec'];
          $moneda_id = $result['data']['tb_moneda_id'];
          $cheque_mon = $result['data']['tb_cheque_mon'];
          $cheque_tipcam = $result['data']['tb_cheque_tipcam'];
          $cheque_num = $result['data']['tb_cheque_num'];
          $cheque_ban = $result['data']['tb_cheque_ban'];
          $cheque_cretip = $result['data']['tb_cheque_cretip'];
          $cheque_creid = $result['data']['tb_cheque_creid'];
          $transferente_id = $result['data']['tb_transferente_id'];
          $transferente_id = $result['data']['tb_transferente_id'];
          $transferente_doc = $result['data']['tb_transferente_doc'];
          $transferente_nom = $result['data']['tb_transferente_nom'];
          $cheque_des = $result['data']['tb_cheque_des'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cheque" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cheque" method="post">
          <input type="hidden" id="cheque_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" id="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">
          <input type="hidden" name="hdd_transferente_id" id="hdd_transferente_id" value="<?php echo $transferente_id;?>">
          <input type="hidden" name="hdd_cheque_id" id="hdd_cheque_id" value="<?php echo $cheque_id;?>">
          
          <div class="modal-body">
            
            <div class="row">

              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cheque_fec" class="control-label">Fecha</label>
                  <input type="text" name="txt_cheque_fec" id="txt_cheque_fec" class="form-control input-sm" value="<?php echo date('d-m-Y');?>">
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label for="cmb_cheque_tip" class="control-label">Tipo Operación</label>
                  <select class="form-control input-sm" name="cmb_cheque_tip" id="cmb_cheque_tip">
                      <option value="1" <?php if($cheque_tip == 1) echo 'selected'?>>Cheque Gerencia</option>
                      <option value="2" <?php if($cheque_tip == 2) echo 'selected'?>>Depósito Bancario</option>
                  </select>
                </div>
              </div>
              
              <div class="col-md-4">
                <div class="form-group">
                  <label for="cmb_moneda_id" class="control-label">Tipo Moneda</label>
                  <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id" onchange="cambioMoneda()">
                      <option value="1" <?php if($moneda_id == 1) echo 'selected'?>>Soles</option>
                      <option value="2" <?php if($moneda_id == 2) echo 'selected'?>>Dólares</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cheque_tipcam" class="control-label">Tipo Cambio</label>
                  <input type="text" name="txt_cheque_tipcam" id="txt_cheque_tipcam" onkeypress="return solo_decimal(event)" class="form-control input-sm" style="text-align: right;" value="<?php echo $cheque_tipcam;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cheque_mon" class="control-label">Monto</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="" id="icono_moneda"><strong>S/</strong></i></span>
                    <input type="text" name="txt_cheque_mon" id="txt_cheque_mon" onkeypress="return solo_decimal(event)" class="form-control input-sm" placeholder="" style="text-align: right;" value="<?php echo $cheque_mon;?>">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_cheque_num" class="control-label">N° Operación</label>
                  <input type="text" name="txt_cheque_num" id="txt_cheque_num" class="form-control input-sm " placeholder="Ingrese transferente" value="<?php echo $cheque_num;?>">
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_cheque_ban" class="control-label">Banco</label>
                  <input type="text" name="txt_cheque_ban" id="txt_cheque_ban" class="form-control input-sm " placeholder="Ingrese transferente" value="<?php echo $cheque_ban;?>">
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label for="cmb_cheque_cretip" class="control-label">Tipo de Crédito</label>
                  <select class="form-control input-sm" name="cmb_cheque_cretip" id="cmb_cheque_cretip">
                      <option value="3" <?php if($cheque_cretip == 3) echo 'selected'?>>Garantía Vehicular</option>
                      <option value="4" <?php if($cheque_cretip == 4) echo 'selected'?>>Hipotecario</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cheque_creid" class="control-label">ID Crédito</label>
                  <input type="text" name="txt_cheque_creid" id="txt_cheque_creid" onkeypress="return solo_numero(event)" class="form-control input-sm" value="<?php echo $cheque_creid;?>">
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label for="txt_transferente_nom" class="control-label">Nombre de Transferente</label>
                  <input type="text" name="txt_transferente_nom" id="txt_transferente_nom" class="form-control input-sm " placeholder="Ingrese transferente" value="<?php echo $transferente_nom;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_transferente_doc" class="control-label">RUC/DNI Transferente</label>
                  <input type="text" name="txt_transferente_doc" id="txt_transferente_doc" onkeypress="return solo_numero(event)" class="form-control input-sm" readonly value="<?php echo $transferente_doc;?>">
                </div>
              </div>
             
              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_cheque_des" class="control-label">Detalle</label>
                  <textarea class="form-control input-sm" name="txt_cheque_des" id="txt_cheque_des" rows="3" style="resize:none;"><?php echo $cheque_des; ?></textarea>
                </div>
              </div>
              
            </div>
            

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cheque de Aprobación?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cheque_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cheque">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cheque">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cheque">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cheque/cheque_form.js?ver=26052023';?>"></script>
