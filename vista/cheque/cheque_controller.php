<?php
	session_name("ipdnsac");
  	session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cheque/Cheque.class.php');
  	$oCheque = new Cheque();
	require_once('../funciones/funciones.php');
	require_once('../funciones/fechas.php');

 	$action = $_POST['action'];

 	if($action == 'insertar'){

 		$cheque_tip = intval($_POST['cmb_cheque_tip']);
 		$cheque_fec = fecha_mysql($_POST['txt_cheque_fec']);
 		$moneda_id = intval($_POST['cmb_moneda_id']);
		$cheque_mon = moneda_mysql($_POST['txt_cheque_mon']);
 		$cheque_tipcam = moneda_mysql($_POST['txt_cheque_tipcam']);
 		$cheque_num = $_POST['txt_cheque_num'];
 		$cheque_ban = mb_strtoupper($_POST['txt_cheque_ban'], 'UTF-8');
 		$cheque_cretip = intval($_POST['cmb_cheque_cretip']);
 		$cheque_creid = $_POST['txt_cheque_creid'];
 		$transferente_id = $_POST['hdd_transferente_id'];
 		$cheque_des = $_POST['txt_cheque_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar Cheque.';
		$res= $oCheque->insertar($cheque_tip, $cheque_fec, $moneda_id, $cheque_mon, $cheque_tipcam, $cheque_num, $cheque_ban, $cheque_cretip, $cheque_creid, $transferente_id, $cheque_des);
 		if(intval($res['estado']) == 1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cheque registrado correctamente.';
			$data['registro'] = $oCheque->mostrarUno($res['nuevo'])['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
		$cheque_id = intval($_POST['hdd_cheque_id']);
		$cheque_tip = intval($_POST['cmb_cheque_tip']);
 		$cheque_fec = fecha_mysql($_POST['txt_cheque_fec']);
 		$moneda_id = intval($_POST['cmb_moneda_id']);
		$cheque_mon = moneda_mysql($_POST['txt_cheque_mon']);
 		$cheque_tipcam = moneda_mysql($_POST['txt_cheque_tipcam']);
 		$cheque_num = $_POST['txt_cheque_num'];
 		$cheque_ban = mb_strtoupper($_POST['txt_cheque_ban'], 'UTF-8');
 		$cheque_cretip = intval($_POST['cmb_cheque_cretip']);
 		$cheque_creid = $_POST['txt_cheque_creid'];
 		$transferente_id = $_POST['hdd_transferente_id'];
 		$cheque_des = $_POST['txt_cheque_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Cheque.';

 		if($oCheque->modificar($cheque_id, $cheque_tip, $cheque_fec, $moneda_id, $cheque_mon, $cheque_tipcam, $cheque_num, $cheque_ban, $cheque_cretip, $cheque_creid, $transferente_id, $cheque_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cheque modificado correctamente.';
			$data['registro'] = $oCheque->mostrarUno($cheque_id)['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$cheque_id = intval($_POST['hdd_cheque_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Cheque.';

 		if($oCheque->eliminar($cheque_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cheque eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>