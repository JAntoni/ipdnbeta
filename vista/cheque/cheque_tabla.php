<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
if (defined('VISTA_URL')) {
    require_once(VISTA_URL . 'cheque/Cheque.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
} else {
    require_once('../cheque/Cheque.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
}
$oCheque = new Cheque();
$fecha_ini = fecha_mysql($_POST['fecha_ini']);
$fecha_fin = fecha_mysql($_POST['fecha_fin']);
?>
<table id="tbl_cheque" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila" width="6%">FECHA</th>
            <th id="tabla_cabecera_fila">MONTO CHEQUE</th>
            <th id="tabla_cabecera_fila" width="15%">N° OPERACIÓN</th>
            <th id="tabla_cabecera_fila">BANCO</th>
            <th id="tabla_cabecera_fila">TIPO CRÉDITO</th>
            <th id="tabla_cabecera_fila">ID CRÉDITO</th>
            <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
            <th id="tabla_cabecera_fila" width="6%"></th>
        </tr>
    </thead>
    <tbody>
<?php
//PRIMER NIVEL
$result = $oCheque->listar_cheques($fecha_ini, $fecha_fin);
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value):
        $tipo_cre = '';
        switch ($value['tb_cheque_cretip']) {
            case 1:
                $tipo_cre = 'CRÉDITO MENOR';
                break;
            case 2:
                $tipo_cre = 'CRÉDITO ASCVEH';
                break;
            case 3:
                $tipo_cre = 'CRÉDITO GARVEH';
                break;
            case 4:
                $tipo_cre = 'CRÉDITO HIPOTECARIO';
                break;
            case 5:
                $tipo_cre = 'ADENDAS';
                break;
            default:
                $tipo_cre = '';
                break;
        }
        $moneda = '';
        if($value['tb_moneda_id']==1){
            $moneda = 'S/ ';

        }elseif($value['tb_moneda_id']==2){
            $moneda = 'US$ ';

        }
        ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['tb_cheque_id']; ?></td>
                    <td align="left" id="tabla_fila"><?php echo mostrar_fecha($value['tb_cheque_fec']); ?></td>
                    <td align="right" id="tabla_fila"><?php echo $moneda.''.mostrar_moneda($value['tb_cheque_mon']); ?></td>
                    <td align="right" id="tabla_fila"><?php echo $value['tb_cheque_num']; ?></td>
                    <td align="right" id="tabla_fila"><?php echo $value['tb_cheque_ban']; ?></td>
                    <td align="left" id="tabla_fila"><?php echo $tipo_cre; ?></td>
                    <td align="left" id="tabla_fila"><?php echo $value['tb_cheque_creid']; ?></td>
                    <td align="left" id="tabla_fila"><?php echo $value['tb_cheque_des']; ?></td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="cheque_form(<?php echo "'M', " . $value['tb_cheque_id']; ?>)"><i class="fa fa-edit"></i> Editar</a>
                        <!-- <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cheque_form(<?php echo "'E', " . $value['tb_cheque_id']; ?>)"><i class="fa fa-trash"></i> Eliminar</a> -->
                    </td>
                </tr>
                <?php
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
</table>
