<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
      		<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<button class="btn btn-primary btn-sm" onclick="cheque_form('I',0)"><i class="fa fa-plus"></i> Nuevo Cheque</button>
			</div>
			<div class="box-body">
				<div class="row">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtrar por</label>
                        <div class="panel panel-primary">
                            <div class="panel-body">
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Fecha </label>
										<div class="input-group">
											<div class='input-group date' id='datetimepicker1'>
												<input type='text' class="form-control input-sm" name="txt_resumen_fec1" id="txt_resumen_fec1" value="<?php echo date('01-m-Y'); ?>"/>
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
											<span class="input-group-addon">-</span>
											<div class='input-group date' id='datetimepicker2'>
												<input type='text' class="form-control input-sm" name="txt_resumen_fec2" id="txt_resumen_fec2" value="<?php echo date('d-m-Y'); ?>"/>
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>

										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label for="">&zwj;</label>
										<div class="input-group">
											<button type="button" class="btn btn-info btn-sm" onclick="cheque_tabla()" title="Filtrar"><i class="fa fa-search"></i></button>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="cheque_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Cartas de Aprobación...</h4>
        </div>

				<div class="row">
					<div class="col-sm-12">
						<!-- Input para guardar el valor ingresado en el search de la tabla-->
						<input type="hidden" id="hdd_datatable_fil">

						<div id="div_cheque_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php require_once('cheque_tabla.php');?>
						</div>
					</div>
				</div>
			</div>
			<div id="div_modal_cheque_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
