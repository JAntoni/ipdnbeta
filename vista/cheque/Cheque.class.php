<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cheque extends Conexion{

    function insertar($cheque_tip, $cheque_fec, $moneda_id, $cheque_mon, $cheque_tipcam, $cheque_num, $cheque_ban, $cheque_cretip, $cheque_creid, $transferente_id, $cheque_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cheque(tb_cheque_tip, tb_cheque_fec, tb_moneda_id, tb_cheque_mon, tb_cheque_tipcam, tb_cheque_num, tb_cheque_ban, tb_cheque_cretip, tb_cheque_creid, tb_transferente_id, tb_cheque_des)
          VALUES (:cheque_tip, :cheque_fec, :moneda_id, :cheque_mon, :cheque_tipcam, :cheque_num, :cheque_ban, :cheque_cretip, :cheque_creid, :transferente_id, :cheque_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cheque_tip", $cheque_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_fec", $cheque_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_mon", $cheque_mon, PDO::PARAM_STR);
        $sentencia->bindParam(":cheque_tipcam", $cheque_tipcam, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_num", $cheque_num, PDO::PARAM_STR);
        $sentencia->bindParam(":cheque_ban", $cheque_ban, PDO::PARAM_STR);
        $sentencia->bindParam(":cheque_cretip", $cheque_cretip, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_creid", $cheque_creid, PDO::PARAM_INT);
        $sentencia->bindParam(":transferente_id", $transferente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_des", $cheque_des, PDO::PARAM_STR);

        $result['estado'] = $sentencia->execute();
        $result['nuevo'] = $this->dblink->lastInsertId();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($cheque_id, $cheque_tip, $cheque_fec, $moneda_id, $cheque_mon, $cheque_tipcam, $cheque_num, $cheque_ban, $cheque_cretip, $cheque_creid, $transferente_id, $cheque_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cheque SET tb_cheque_tip =:cheque_tip, tb_cheque_fec =:cheque_fec, tb_moneda_id =:moneda_id, tb_cheque_mon =:cheque_mon, tb_cheque_tipcam =:cheque_tipcam, tb_cheque_tipcam =:cheque_tipcam, tb_cheque_num =:cheque_num, tb_cheque_ban =:cheque_ban, tb_cheque_cretip =:cheque_cretip, tb_cheque_creid =:cheque_creid, tb_transferente_id =:transferente_id, tb_cheque_des =:cheque_des WHERE tb_cheque_id =:cheque_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_tip", $cheque_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_fec", $cheque_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_mon", $cheque_mon, PDO::PARAM_STR);
        $sentencia->bindParam(":cheque_tipcam", $cheque_tipcam, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_num", $cheque_num, PDO::PARAM_STR);
        $sentencia->bindParam(":cheque_ban", $cheque_ban, PDO::PARAM_STR);
        $sentencia->bindParam(":cheque_cretip", $cheque_cretip, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_creid", $cheque_creid, PDO::PARAM_INT);
        $sentencia->bindParam(":transferente_id", $transferente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cheque_des", $cheque_des, PDO::PARAM_STR);

        $result['estado'] = $sentencia->execute();
        $result['nuevo'] = $this->dblink->lastInsertId();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cheque_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cheque WHERE tb_cheque_id =:cheque_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cheque_id){
      try {
        $sql = "SELECT ch.*, tr.tb_transferente_doc, tr.tb_transferente_nom FROM tb_cheque ch
        INNER JOIN tb_transferente tr ON ch.tb_transferente_id = tr.tb_transferente_id
        WHERE ch.tb_cheque_id =:cheque_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cheques($fecha_ini, $fecha_fin){
      try {
        $sql = "SELECT * FROM tb_cheque
        WHERE tb_cheque_fec >= :fecha_ini AND tb_cheque_fec <= :fecha_fin";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function mostrar_credito_id($cre_id, $cre_tip){
      try {
        $sql = "SELECT * FROM tb_cheque WHERE tb_cheque_creid = :cre_id AND tb_cheque_cretip = :cre_tip";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = NULL;
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
