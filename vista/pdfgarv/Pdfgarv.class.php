<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Pdfgarv extends Conexion{

    function insertar($credito_id, $credito_vehpla, $ventavehiculo_soatval, $ventavehiculo_strval, $ventavehiculo_gpsval, $ventavehiculo_kil, $ventavehiculo_det, $creditotipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "
          INSERT INTO tb_ventavehiculo(
          tb_credito_id, tb_credito_vehpla, tb_ventavehiculo_soatval, 
          tb_ventavehiculo_strval, tb_ventavehiculo_gpsval, tb_ventavehiculo_kil, 
          tb_ventavehiculo_det, tb_creditotipo_id) 
          VALUES (
            :credito_id, :credito_vehpla, :ventavehiculo_soatval, 
            :ventavehiculo_strval, :ventavehiculo_gpsval, :ventavehiculo_kil, 
            :ventavehiculo_det, :creditotipo_id)
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_soatval", $ventavehiculo_soatval, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_strval", $ventavehiculo_strval, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_gpsval", $ventavehiculo_gpsval, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_kil", $ventavehiculo_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_det", $ventavehiculo_det, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($ventavehiculo_id, $credito_id, $credito_vehpla, $ventavehiculo_soatval, $ventavehiculo_strval, $ventavehiculo_gpsval, $ventavehiculo_kil, $ventavehiculo_det, $creditotipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_ventavehiculo SET 
          tb_credito_id =:credito_id, tb_credito_vehpla =:credito_vehpla, tb_ventavehiculo_soatval =:ventavehiculo_soatval, 
          tb_ventavehiculo_strval =:ventavehiculo_strval, tb_ventavehiculo_gpsval =:ventavehiculo_gpsval, 
          tb_ventavehiculo_kil =:ventavehiculo_kil, tb_ventavehiculo_det =:ventavehiculo_det, 
          tb_creditotipo_id =:creditotipo_id
          WHERE tb_ventavehiculo_id =:ventavehiculo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventavehiculo_id", $ventavehiculo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_soatval", $ventavehiculo_soatval, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_strval", $ventavehiculo_strval, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_gpsval", $ventavehiculo_gpsval, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_kil", $ventavehiculo_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":ventavehiculo_det", $ventavehiculo_det, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($ventavehiculo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_ventavehiculo WHERE tb_ventavehiculo_id =:ventavehiculo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventavehiculo_id", $ventavehiculo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    // funcion original programada por Anthony Alarcón
    function mostrarPorDocumento($credito_id, $cliente_id, $documento_id){
      try {
        $sql = "SELECT DISTINCT * FROM pdfgarv 
                WHERE tb_tb_cliente_id=:cliente_id 
                AND tb_tb_credito_id=:credito_id 
                AND tb_tipogarvdocpersdetalle_id=:docdetalleid AND tb_pdfgarv_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":docdetalleid", $documento_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //funcion copia, por Daniel Odar
    function mostrarPorDocumento1($credito_id, $documento_id){
      try {
        $sql = "SELECT * FROM pdfgarv 
                WHERE tb_tb_credito_id= :credito_id
                AND tb_tipogarvdocpersdetalle_id= :docdetalleid
                ORDER BY tb_pdfgarv_id DESC
                LIMIT 1 ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":docdetalleid", $documento_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarRegistrosPorCred_Doc($credito_id, $documento_id){
      try {
        $sql = "SELECT * FROM pdfgarv 
                WHERE tb_tb_credito_id= :credito_id 
                AND tb_tipogarvdocpersdetalle_id= :docdetalleid ORDER BY tb_pdfgarv_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":docdetalleid", $documento_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $retorno["cant_reg"] = $sentencia->rowCount();
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
