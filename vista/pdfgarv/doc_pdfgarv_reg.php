<?php
	session_name("ipdnsac");
  session_start();
	require_once('../historial/Historial.class.php');
  $oHistorial = new Historial();
  require_once('../funciones/fechas.php');
  require_once('../comentario/Comentario.class.php');
  $oComentario = new Comentario();
  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();

  $tb_pdfgarv_id = intval($_POST['tb_pdfgarv_id']);
  $proceso_id = (isset($_POST['proceso_id']))? intval($_POST['proceso_id']) : 0;
  $item_id = (isset($_POST['item_id']))? intval($_POST['item_id']) : 0;
  $lista = '';

  if($tb_pdfgarv_id>0){

    $result = $oHistorial->filtrar_historial_por('pdfgarv', $tb_pdfgarv_id);

    
  }else{

    // GERSON (26-01-24)
    $result = $oProceso->obtenerComentarioXProcesoYItem('tb_proc_proceso_fase_item', $proceso_id, $item_id);

  }

  // alt + shift + a para comentar y descomentar en bloque  --- ctrl k c : para comentar con barras, ctrl k u: para descomentar con barras.
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $icono = 'fa-file';
      $background = 'bg-blue';
      $detalle = explode(' ', $value['tb_hist_det']);
      if($detalle[1] == 'exonerado'){
        $icono = 'fa-check-square';
      }
      elseif($detalle[1] == 'eliminado'){
        $background = 'bg-red';
        if($detalle[3] != 'subida'){
          $icono = 'fa-check-square';
        }
      }
      $lista .='
                <li>
                  <i class="fa '.$icono.' '.$background.'"></i>
                  <input name="hist_id" id="hist_id" type="hidden" value="'.$value['tb_hist_id'].'">
                  <div class="timeline-item">
                    <div class="user-block">
                      <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="user image">
                      <span class="username">
                        <a href="#">'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</a>
                      </span>
                      <span class="description">'.$value['tb_hist_det'].' <i class="fa fa-clock-o"></i></span>
                    </div>
                    <div class="timeline-body">';
      $result1 = $oComentario->filtrar_comentarios_por($value['tb_hist_id'], 'tb_hist');
      $lista .= '<input type="hidden" id="cantidad_comentarios-'.$value['tb_hist_id'].'" value="'.intval($result1['cantidad']).'">';
      if($result1['estado'] == 1){
        $lista .= '<div class="box-footer box-comments" id="div_comentarios_anteriores-'.$value['tb_hist_id'].'">';
        foreach($result1['data'] as $key1 => $value1){
          $lista .= '
                            <div class="box-comment" id="com-'.$value1['tb_coment_id'].'">
                              <img class="img-circle img-sm" src="'.$value1['tb_usuario_fot'].'" alt="User Image">
                              <div class="comment-text">
                                <span class="username">
                                '.$value1['tb_usuario_nom'].' '.$value1['tb_usuario_ape'];
                    if(intval($_SESSION['usuarioperfil_id']) == 1){
                      $lista .='<button type="button" class="close" data-dismiss="alert" aria-hidden="true" title="Eliminar comentario" onclick="comentario_eliminar('.$value1['tb_coment_id'].','.$value['tb_hist_id'].')">x</button>';
                    }
                      $lista .='</span>'.$value1['tb_coment_det'].'
                                <span class="text-muted pull-right">'. date("h:i a, j-m-y", strtotime($value1['tb_coment_fecreg'])) .'</span>
                              </div>
                            </div>
          ';
        }
        $lista .= '</div>';
      }
             $lista .= '<div class="box-footer" id="div_comentar-'.$value['tb_hist_id'].'">
                            <img class="img-responsive img-circle img-sm" src="'.$_SESSION['usuario_fot'].'" alt="Alt Text">
                            <div class="img-push">
                              <input type="text" class="form-control input-sm" id="inp_comentario-'.$value['tb_hist_id'].'" placeholder="Escribe y presiona Enter para postear un comentario" onclick=action_click(this)>
                            </div>
                        </div>

                      </div>
                  </div>
              </li>
      ';
    }
  }
  $result = NULL;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_hist_doc_pdf_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <input type="hidden" id="pdfgarv_id" value="<?php echo $tb_pdfgarv_id; ?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Historial del documento pdf</h4>
      </div>
      <div class="modal-body">
        <ul class="timeline">
				  <?php echo $lista;?>
				</ul>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/pdfgarv/pdfgarv.js?ver=6528509009885'; ?>"></script>