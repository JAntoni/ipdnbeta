<?php
  session_name("ipdnsac");
  session_start();
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 admin, 3 ventas

  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'cuota/Cuota.class.php');
    require_once(VISTA_URL.'cuota/Cuotadetalle.class.php');
    require_once(VISTA_URL.'ingreso/Ingreso.class.php');
    require_once(VISTA_URL.'morapago/Morapago.class.php');
    require_once(VISTA_URL.'clientecaja/Clientecaja.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../cuota/Cuota.class.php');
    require_once('../cuota/Cuotadetalle.class.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../morapago/Morapago.class.php');
    require_once('../clientecaja/Clientecaja.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }
  $oCuota = new Cuota();
  $oCuotadetalle = new Cuotadetalle();
  $oIngreso = new Ingreso();
  $oMorapago = new Morapago();
  $oClientecaja = new Clientecaja();

  $credito_id = (isset($_POST['credito_id']))? intval($_POST['credito_id']) : $credito_id;
  $creditotipo_id = (isset($_POST['creditotipo_id']))? intval($_POST['creditotipo_id']) : $creditotipo_id;
  
  if($creditotipo_id == 1)
  	$result = $oCuota->listar_moras_menor($credito_id, $creditotipo_id); //para CMENOR, SOLO ES EN BASE A CUOTAS
  else
  	$result = $oCuotadetalle->listar_moras_garveh($credito_id, $creditotipo_id); //los demas son cuotadetalle

  function estado_cuota($cuota_est){
    $estado = '<span class="badge bg-orange">POR COBRAR</span>';
    if($cuota_est == 2)
      $estado = '<span class="badge bg-green">CANCELADO</span>';
    if($cuota_est == 3)
      $estado = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
    return $estado;
  }
?>
<table id="tbl_cuota" class="table table-bordered">
  <thead>
    <tr>
      <th>N° Cuota</th>
      <th>Fecha Cuota</th>
      <th>Monto Mora</th>
      <th>Estado</th>
      <th>Detalle</th>
      <th>Pagos</th>
      <th>Saldo</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if($result['estado'] == 1 && $creditotipo_id == 1){
        foreach ($result['data'] as $key => $value): 
        	$modulo_id = 1; $morapago_modid = $value['tb_cuota_id'];
        	$pagos_mora = 0;

        	$result2 = $oMorapago->mostrar_morapago_ingresos_por_tipo_cuota($modulo_id, $morapago_modid);
        		if($result2['estado'] == 1){
        			foreach ($result2['data'] as $key => $value2) {
        				$pagos_mora += formato_numero($value2['tb_ingreso_imp']);
        			}
        		}
        	$result2 = NULL;

        	$saldo_mora = formato_numero($value['tb_cuota_mor']) - $pagos_mora; ?>
          <tr>
            <td><?php echo 'Cuota: '.$value['tb_cuota_num'];?></td>
            <td><?php echo mostrar_fecha($value['tb_cuota_fec']);?></td>
            <td><?php echo 'S/. '.mostrar_moneda($value['tb_cuota_mor']);?></td>
            <td><?php echo estado_cuota($value['tb_cuota_morest']); ?></td>
            <td>
              <?php
                $pagos_cuota = 0; $saldo_cuota = 0;

                $result3 = $oMorapago->mostrar_morapago_ingresos_por_tipo_cuota2($modulo_id, $morapago_modid);
                  if($result3['estado'] == 1){
                    $numero_pago = 1;
                    foreach ($result3['data'] as $key => $value3): ?>
                      <table class="table" style="border: 2px solid #333; margin-bottom: 0px;">
                        <tr>
                          <th width="40%">
                            <?php 
                              //echo '<a href="javascript:void(0)" onclick="morapago_imppos_datos3('.$value3['tb_morapago_id'].')">Pago '.$numero_pago.'</a>';
                              echo '<a href="javascript:void(0)" onclick="ingreso_imppos_datos3('.$value3['tb_ingreso_id'].','.$value3['tb_empresa_id'].')">Pago '.$numero_pago.'</a>';
                              echo '<a href="javascript:void(0)" onclick="morapago_menor_eliminar('.$value3['tb_morapago_id'].')" style="margin-left: 12%;">Anular</a>';
                            ?>
                          </th>
                          <th width="30%"><?php
                            echo '<a href="javascript:void(0)" onclick="morapago_detalle('.$value3['tb_morapago_id'].')">Ingreso</a>';?>
                          </th>
                          <th width="30%">Caj Cli</th>
                        </tr>
                        <tr>
                          <td><?php echo mostrar_fecha($value3['tb_morapago_fec']).' | '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_morapago_mon']);?></td>
                          <td>
                            <?php
                              $modulo_id = 50; $ingreso_modide = $value3['tb_morapago_id'];
                              $result3 = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
                               if($result3['estado'] == 1){
                                foreach ($result3['data'] as $key => $value3){
                                  echo '<dd>'.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_ingreso_imp']).'</dd>';
                                  $pagos_cuota += formato_numero($value3['tb_ingreso_imp']);
                                }
                               }
                               else
                                echo mostrar_moneda(0);
                              $result3 = NULL;
                            ?>
                          </td>
                          <td>
                            <?php
                              $modulo_id = 50; $clientecaja_modid = $value3['tb_morapago_id'];
                              $result3 = $oClientecaja->mostrar_clientecaja_modulo($modulo_id, $clientecaja_modid);
                               if($result3['estado'] == 1){
                                foreach ($result3['data'] as $key => $value3){
                                  if(intval($value3['tb_clientecaja_tip']) == 1){
                                    echo '<dd>+ '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_clientecaja_mon']).'</dd>';
                                    $clientecaja_mas += formato_numero($value3['tb_clientecaja_mon']);
                                  }
                                  if(intval($value3['tb_clientecaja_tip']) == 2){
                                    echo '<dd>- '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_clientecaja_mon']).'</dd>';
                                    $clientecaja_menos += formato_numero($value3['tb_clientecaja_mon']);
                                  }
                                }
                               }
                               else
                                echo $value3['tb_moneda_nom'].' '.mostrar_moneda(0);
                              $result3 = NULL;
                            ?>
                          </td>
                        </tr>
                      </table><?php
                      $numero_pago ++;
                    endforeach;
                  }
                $result3 = NULL;
                /* $clientecaja_cuota = $clientecaja_mas - $clientecaja_menos;
                $saldo_cuota = formato_numero($value['tb_cuota_cuo']) - formato_numero($pagos_cuota);
                $deuda_total += formato_numero($saldo_cuota); */
              ?>
            </td>
            <td><?php echo 'S/. '.mostrar_moneda($pagos_mora);?></td>
            <td><?php echo 'S/. '.mostrar_moneda($saldo_mora);?></td>
            <td align="center">
            <?php 
            
                if ($value['tb_cuotatipo_id'] == 1) {
                  $action = 'pagar_libre';
                }
                if ($value['tb_cuotatipo_id'] == 2) {
                  $action = 'pagar_fijo';
                }
                $tabla = 'cuota';
                if($value['tb_cuota_morest']!=2 && $value['tb_cuota_mor']>0){
                ?>
                  <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="morapago_menor_form('<?php echo $action;?>','<?php echo $value['tb_cuota_id']?>')"><i class="fa fa-money"></i></a>
                  <?php }?>
                <?php if($value['tb_cuota_morest']!=2 && $value['tb_cuota_morest']!=3 && $value['tb_cuota_mor']>0){?>
                  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="mora_eliminar(<?php echo '1,'.$value['tb_cuota_id']; ?>)"><i class="fa fa-trash"></i></a>
                <?php }?>
                <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="mora_historial_form('<?php echo $tabla;?>','<?php echo $value['tb_cuota_id']?>')">Hist</a>
            </td>
            <!-- <td>
            	<a class="btn btn-danger btn-xs" title="" onclick="morapago_eliminar(<?php echo '1,'.$value['tb_cuota_id']; ?>)">
            		<i class="fa fa-trash"></i> Eliminar
            	</a>
            </td>    -->                
          </tr>
          <?php
        endforeach;
      }
      if($result['estado'] == 1 && $creditotipo_id != 1){

        if($creditotipo_id == 2) $credito_tabla = 'tb_creditoasiveh';
        if($creditotipo_id == 3) $credito_tabla = 'tb_creditogarveh';
        if($creditotipo_id == 4) $credito_tabla = 'tb_creditohipo';
        
        foreach ($result['data'] as $key => $value): 
        	$modulo_id = 2; $morapago_modid = $value['tb_cuotadetalle_id'];
        	$pagos_mora = 0;

        	$result2 = $oMorapago->mostrar_morapago_ingresos_por_tipo_cuota($modulo_id, $morapago_modid);
        		if($result2['estado'] == 1){
        			foreach ($result2['data'] as $key => $value2) {
        				$pagos_mora += formato_numero($value2['tb_ingreso_imp']);
        			}
        		}
        	$result2 = NULL;

        	$saldo_mora = formato_numero($value['tb_cuotadetalle_mor']) - $pagos_mora; ?>
          <tr>
            <td><?php echo 'Cuota: '.$value['tb_cuota_num'].' | Detalle: '.$value['tb_cuotadetalle_num'].'/'.$value['tb_cuota_persubcuo'];?></td>
            <td><?php echo mostrar_fecha($value['tb_cuotadetalle_fec']);?></td>
            <td><?php echo 'S/. '.mostrar_moneda($value['tb_cuotadetalle_mor']);?></td>
            <td><?php 
                echo estado_cuota($value['tb_cuotadetalle_morest']); 
                ?></td>
            <td>
              <?php
                $pagos_cuota = 0; $saldo_cuota = 0;

                $result3 = $oMorapago->mostrar_morapago_ingresos_por_tipo_cuota2($modulo_id, $morapago_modid);
                  if($result3['estado'] == 1){
                    $numero_pago = 1;
                    foreach ($result3['data'] as $key => $value3): ?>
                      <table class="table" style="border: 2px solid #333; margin-bottom: 0px;">
                        <tr>
                          <th width="40%">
                            <?php 
                              //echo '<a href="javascript:void(0)" onclick="morapago_imppos_datos3('.$value3['tb_morapago_id'].')">Pago '.$numero_pago.'</a>';
                              echo '<a href="javascript:void(0)" onclick="ingreso_imppos_datos3('.$value3['tb_ingreso_id'].','.$value3['tb_empresa_id'].')">Pago '.$numero_pago.'</a>';
                              echo '<a href="javascript:void(0)" onclick="morapago_eliminar('.$value3['tb_morapago_id'].')" style="margin-left: 12%;">Anular</a>';
                            ?>
                          </th>
                          <th width="30%"><?php
                            echo '<a href="javascript:void(0)" onclick="morapago_detalle('.$value3['tb_morapago_id'].')">Ingreso</a>';?>
                          </th>
                          <th width="30%">Caj Cli</th>
                        </tr>
                        <tr>
                          <td><?php echo mostrar_fecha($value3['tb_morapago_fec']).' | '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_morapago_mon']);?></td>
                          <td>
                            <?php
                              $modulo_id = 50; $ingreso_modide = $value3['tb_morapago_id'];
                              $result3 = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
                               if($result3['estado'] == 1){
                                foreach ($result3['data'] as $key => $value3){
                                  echo '<dd>'.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_ingreso_imp']).'</dd>';
                                  $pagos_cuota += formato_numero($value3['tb_ingreso_imp']);
                                }
                               }
                               else
                                echo mostrar_moneda(0);
                              $result3 = NULL;
                            ?>
                          </td>
                          <td>
                            <?php
                              $modulo_id = 50; $clientecaja_modid = $value3['tb_morapago_id'];
                              $result3 = $oClientecaja->mostrar_clientecaja_modulo($modulo_id, $clientecaja_modid);
                               if($result3['estado'] == 1){
                                foreach ($result3['data'] as $key => $value3){
                                  if(intval($value3['tb_clientecaja_tip']) == 1){
                                    echo '<dd>+ '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_clientecaja_mon']).'</dd>';
                                    $clientecaja_mas += formato_numero($value3['tb_clientecaja_mon']);
                                  }
                                  if(intval($value3['tb_clientecaja_tip']) == 2){
                                    echo '<dd>- '.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_clientecaja_mon']).'</dd>';
                                    $clientecaja_menos += formato_numero($value3['tb_clientecaja_mon']);
                                  }
                                }
                               }
                               else
                                echo $value3['tb_moneda_nom'].' '.mostrar_moneda(0);
                              $result3 = NULL;
                            ?>
                          </td>
                        </tr>
                      </table><?php
                      $numero_pago ++;
                    endforeach;
                  }
                $result3 = NULL;
                /* $clientecaja_cuota = $clientecaja_mas - $clientecaja_menos;
                $saldo_cuota = formato_numero($value['tb_cuota_cuo']) - formato_numero($pagos_cuota);
                $deuda_total += formato_numero($saldo_cuota); */
              ?>
            </td>
            <td><?php echo 'S/. '.mostrar_moneda($pagos_mora);?></td>
            <td><?php echo 'S/. '.mostrar_moneda($saldo_mora);?></td>
            <td align="center">
              <?php 
                if ($value['tb_cuotatipo_id'] == 3) {
                  $action = 'pagar_libre';
                }
                if ($value['tb_cuotatipo_id'] == 4) {
                  $action = 'pagar_fijo';
                }
                $tabla = 'cuotadetalle';
                if($value['tb_cuotadetalle_morest']!=2 && $value['tb_cuotadetalle_mor']>0){?>
                  <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="morapago_garveh_form('<?php echo $action;?>','<?php echo $value['tb_cuotadetalle_id']?>')"><i class="fa fa-money"></i></a>
                <?php }?>
                <?php if($value['tb_cuotadetalle_morest']!=2 && $value['tb_cuotadetalle_morest']!=3 && $value['tb_cuotadetalle_mor']>0){?>
                  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="mora_eliminar(<?php echo '2,'.$value['tb_cuotadetalle_id']; ?>)"><i class="fa fa-trash"></i></a>
                <?php }?>
                <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="mora_historial_form('<?php echo $tabla;?>','<?php echo $value['tb_cuotadetalle_id']?>')">Hist</a>
            </td>
            <!-- <td>
            	<a class="btn btn-danger btn-xs" title="" onclick="morapago_eliminar(<?php echo '1,'.$value['tb_cuota_id']; ?>)">
            		<i class="fa fa-trash"></i> Eliminar
            	</a>
            </td>    -->                
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
<script type="text/javascript" src="<?php echo 'vista/morapago/morapago.js?ver=1407244';?>"></script>