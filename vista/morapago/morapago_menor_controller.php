<?php
	session_name("ipdnsac");
  	session_start();
	require_once('../morapago/Morapago.class.php');
	require_once('../ingreso/Ingreso.class.php');
	require_once('../egreso/Egreso.class.php');
	require_once('../cuota/Cuota.class.php');
	require_once('../cuota/Cuotadetalle.class.php');
	require_once('../cajacambio/Cajacambio.class.php');
	require_once('../creditomenor/Creditomenor.class.php');
	require_once('../creditolinea/Creditolinea.class.php');
	
	$oMorapago = new Morapago();
	$oIngreso = new Ingreso();
	$oEgreso = new Egreso();
	$oCuota = new Cuota();
	$oCuotadetalle = new Cuotadetalle();
	$oCredito = new Creditomenor();
	$oCajacambio = new Cajacambio();
	$oCreditolinea = new Creditolinea();

	require_once('../funciones/funciones.php');
	require_once('../funciones/fechas.php');

	$action = $_POST['action'];
	$modulo_id = intval($_POST['modulo_id']);
	$morapago_id = intval($_POST['morapago_id']);
  	$morapago_modid = intval($_POST['morapago_modid']);
  	$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);

  	if($action == 'eliminar' && $morapago_id > 0){
		if($usuariogrupo_id != 2){
			$data['estado'] = 0;
			$data['mensaje'] = 'Solo los usuarios administradores pueden anular los pagos, comunícate con ellos por favor.';
			echo json_encode($data);
			exit();
	  	}

		$fecha_hoy = date('d-m-Y');
		$result = $oMorapago->mostrarUno($morapago_id);
		if ($result['estado'] == 1) {
			$mora_tipo = $result['data']['tb_modulo_id']; // 1 cuota, 2 cuotadetalle
			$morapago_modid = $result['data']['tb_morapago_modid']; //id de cuota o cuotadetalle
			$morapago_fec = mostrar_fecha($result['data']['tb_morapago_fec']);
		}
		$result = NULL;
	
		if ($fecha_hoy != $morapago_fec) {
			$data['estado'] = 0;
			$data['mensaje'] = 'No se puede anular pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $fecha_hoy . ', fecha anular: ' . $morapago_fec;
			echo json_encode($data);
			exit();
		}

		$detalle_ingreso = '';


		//1. Anulamos todos los ingresos que pertenezcan al mismo morapago_id
		$ingreso_valor = 0;
		$modulo_id = 50;
		$ingreso_modide = $morapago_id; // tb_ingreso_xac = 0, modulo 30 de cuotapago

		$result2 = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
		if($result2['estado'] == 1){
			foreach ($result2['data'] as $key => $value) {
				if($detalle_ingreso == '')
					$detalle_ingreso = '<b>INGRESO ID: </b>'.$value['tb_ingreso_id'].' / '.$value['tb_ingreso_det'];
				else
					$detalle_ingreso .= ' / <b>INGRESO ID: </b>'.$value['tb_ingreso_id'].' / '.$value['tb_ingreso_det'];
			}
		}
		$result2 = NULL;

		$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $modulo_id, $ingreso_modide);


		//2. Cuando el pago es mediante BANCO, se genera un egreso para cuadrar la caja, también se debe anularel egreso tiene como modulo_id = 30 (cuotapago), egreso_modid = cuotapago_id
		$egreso_valor = 0;
		$modulo_id = 50;
		$egreso_modide = $morapago_id;
		$oEgreso->anular_egresos_por_modulo($egreso_valor, $modulo_id, $egreso_modide);


		$cuota_id = 0; //para poder obtener información del crédito

		//3. Si el morapago pertenece a una cuota
		if ($mora_tipo == 1) {
			$mora_est = 1; //1 por cobrar, 2 pagado, 3 pago parcial
			$modulo_id = $mora_tipo; // 1 tb_cuota

			$result = $oMorapago->mostrar_morapago_ingresos_por_tipo_cuota($modulo_id, $morapago_modid);
			if ($result['estado'] == 1) {
				//aun sigue teniendo pagos esta cuota
				$mora_est = 3; //pago parcial
			}
			$result = NULL;

			$cuota_id = $morapago_modid;
			$oCuota->modificar_campo($cuota_id, 'tb_cuota_morest', $mora_est, 'INT');

			$resultado = $oCuota->mostrarUno_CreditoMenor($cuota_id);
			if ($resultado['estado'] == 1) {
				$tb_cuota_cap = $resultado['data']['tb_cuota_cap'];
				$tb_credito_id = $resultado['data']['tb_credito_id'];
				$tb_cuota_num = $resultado['data']['tb_cuota_num'];
				$tb_cuotatipo_id = $resultado['data']['tb_cuotatipo_id'];
				$tb_cuota_cuo = $resultado['data']['tb_cuota_cuo'];
				$creditotipo_id = intval($resultado['data']['tb_creditotipo_id']);
			}
			$resultado = NULL;
		}

		//4. si el pago se hizo con diferente moneda, tenemos que anular las cajas cambio
		$modulo_id = 50; $cajacambio_modid = $morapago_id;
		$result = $oCajacambio->mostrar_cajacambio_modulo($modulo_id, $cajacambio_modid);
		if($result['estado'] == 1){
			foreach ($$result['data'] as $key => $value) {
				$cajacambio_id = intval($value['tb_cajacambio_id']);
				$oCajacambio->modificar_campo($cajacambio_id, 'tb_cajacambio_xac', 0, 'INT');

				$ingreso_valor = 0; $modulo_id = 60; $ingreso_modide = $cajacambio_id; //60 es el modulo de cajacambio en ingreso
				$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $modulo_id, $ingreso_modide);

				$egreso_valor = 0; $modulo_id = 60; $egreso_modide = $cajacambio_id; //60 es el modulo de cajacambio en egreso
				$oEgreso->anular_egresos_por_modulo($egreso_valor, $modulo_id, $egreso_modide);
			}
		}
		$result = NULL;

		//5. Finalmente anulamos el cuotapago
		$oMorapago->modificar_campo($morapago_id, 'tb_morapago_est', 0, 'INT');
		
		//6. GUARDAMOS UN HISTORIAL DEL PAGO ANULADO PARA PODER TENER CONTROL DE QUÉ USUARIO REALIZA ESTAS ACCIÓNES
		$creditolinea_det = 'Ha eliminado el pago: '.$detalle_ingreso;
		$oCreditolinea->insertar($creditotipo_id, $tb_credito_id, intval($_SESSION['usuario_id']), $creditolinea_det);

		$data['estado'] = 1;
		$data['mensaje'] = 'Cuotapago eliminado correctamente';
		echo json_encode($data);
  	}
  /*if($action == 'eliminar'){
  	if($usuariogrupo_id != 2){
	    $data['estado'] = 0;
	    $data['mensaje'] = 'Solo los usuarios administradores pueden anular los pagos, comunícate con ellos por favor.';
	    echo json_encode($data);
	    exit();
	  }

	  $result = $oMorapago->mostrar_morapagos_modulo($modulo_id, $morapago_modid);
	  	if($result['estado'] == 1){
	  		foreach ($result['data'] as $key => $value) {
	  			$morapago_id = $value['tb_morapago_id'];
	  			$oMorapago->modificar_campo($morapago_id, 'tb_morapago_xac', 0, 'INT');

	  			$ingreso_valor = 0; $ingreso_modulo_id = 50; $ingreso_modide = $value['tb_morapago_id'];
  				$oIngreso->anular_ingresos_por_modulo($ingreso_valor, $ingreso_modulo_id, $ingreso_modide);
	  		}
	  	}
	  $result = NULL;

	  if($modulo_id == 1){
	  	//la mora es de una cuota, C-MENOR
	  	$cuota_id = $morapago_modid;
	  	$oCuota->modificar_campo($cuota_id, 'tb_cuota_mor', 0, 'INT');
	  }
	  if($modulo_id == 2){
	  	//la mora es de una cuotadetalle, TODOS LOS CREDITOS
	  	$cuotadetalle_id = $morapago_modid;
	  	$oCuotadetalle->modificar_campo($cuotadetalle_id, 'tb_cuotadetalle_mor', 0, 'INT');
	  }

	  $data['estado'] = 1;
	  $data['mensaje'] = 'La mora se ha eliminado correctamente';
	  echo json_encode($data);
  }*/

?>