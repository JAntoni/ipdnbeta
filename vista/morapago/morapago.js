function morapago_menor_eliminar(morapago_id){
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Anular',
    content: '¿Desea Anular este pago?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"morapago/morapago_menor_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "eliminar",
            morapago_id: morapago_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Anulando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              alerta_success('Bien', data.mensaje); //en generales.js
              morapago_pagos_tabla()
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}

function morapago_eliminar(morapago_id){
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Anular',
    content: '¿Desea Anular este pago?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"morapago/morapago_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "eliminar",
            morapago_id: morapago_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Anulando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              alerta_success('Bien', data.mensaje); //en generales.js
              morapago_pagos_tabla()
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}

/* GERSON (14-07-24) */
function morapago_detalle(morapago_id){  
  $.ajax({
    type: "POST",
    url: VISTA_URL+"morapago/morapago_detalle.php",
    async: true,
    dataType: "html",
    data: ({
      morapago_id: morapago_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Obteniendo datos...');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_cuotapago_detalle').html(data);
      $('#modal_cuotapago_detalle').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_cuotapago_detalle'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_cuotapago_detalle', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}

function morapago_imppos_datos3(morapago_id) {
  var codigo=morapago_id;
  window.open("http://ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?morapago_id="+codigo);
  //  window.open("http://localhost/ipdnsac/vista/creditomenor/creditomenor_ticket.php?morapago_id="+codigo);
}
function ingreso_imppos_datos3(id_ing,empresa_id) {
  var codigo=id_ing;
  window.open("http://ipdnsac.com/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
  //  window.open("http://localhost/ipdnsac/vista/ingreso/ingreso_ticket.php?ingreso_id="+codigo+"&empresa_id="+empresa_id);
}
/*  */

function morapago_garveh_form(act,idf){
  console.log(act,idf);
  
  Swal.close();
  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO DE LA MORA EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      Swal.fire(
        '¡Has elegido pagar en Oficina!',
        '',
        'success'
      )
      cuotamorapago_garveh_form(act,idf);
    } else if (result.isDenied) {
      Swal.fire(
        '¡Has elegido pagar en Banco!',
        '',
        'success'
      )
      morapago_banco('garveh', idf);

    }
  });
}

function cuotamorapago_garveh_form(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_garveh_form.php",
    async:true,
    dataType: "html",                    
    data: ({
        action: cred,
        cuodet_id:	idf,
        vista:	'historial_mora'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function morapago_banco(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_banco.php",
    async:true,
    dataType: "html",                    
    data: ({
      credito: cred,
      cuota_id: idf,
      vista: 'historial_mora'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function mora_historial_form(tabla,mora_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "morapago/mora_historial_form.php",
      async: true,
      dataType: "html",
      data: ({
          mora_id: mora_id,
          tabla: tabla
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (html) {
        console.log(html);
        
          $('#div_mora_historial_form').html(html);
          $('#div_modal_mora_historial_form').modal('show');
          $('#modal_mensaje').modal('hide');

          //funcion js para limbiar el modal al cerrarlo
          modal_hidden_bs_modal('div_modal_mora_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
          //funcion js para agregar un largo automatico al modal, al abrirlo
          modal_height_auto('div_modal_mora_historial_form'); //funcion encontrada en public/js/generales.js
      },
      complete: function (html) {
      }
  });
}

function cuotamorapago_menor_form(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_menor_form.php",
    async:true,
    dataType: "html",                    
    data: ({
        action: cred,
        cuo_id:	idf,
        vista:	'historial_mora'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function morapago_menor_form(act,idf){

  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      Swal.fire(
        '¡Has elegido pagar en Oficina!',
        '',
        'success'
      )
      cuotamorapago_menor_form(act,idf);
    } else if (result.isDenied) {
      Swal.fire(
        '¡Has elegido pagar en Banco!',
        '',
        'success'
      )
      morapago_banco('menor', idf);

    }
  });
}

function mora_eliminar(modulo_id, mora_id){
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Anular',
    content: '¿Desea Anular esta mora?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"cobranzamora/mora_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "eliminar",
            modulo_id: modulo_id,
            mora_id: mora_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Anulando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              alerta_success('Bien', data.mensaje); //en generales.js
              morapago_pagos_tabla()
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}
