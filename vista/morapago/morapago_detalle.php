<?php
	session_name("ipdnsac");
  	session_start();
  	require_once('../morapago/Morapago.class.php');
	require_once('../ingreso/Ingreso.class.php');
	require_once('../funciones/fechas.php');

	$oMorapago = new Morapago();
	$oIngreso = new Ingreso();

	$morapago_id = intval($_POST['morapago_id']);

	$result = $oMorapago->mostrarUno($morapago_id);
		$est = $result['estado'];
	$result = NULL;

	$lista = '';
	if($est > 0){
		$modulo_id = 50; $ingreso_modide = $morapago_id;

  		$result = $oIngreso->mostrar_ingreso_usuario_modulo($modulo_id, $ingreso_modide);
  		if($result['estado'] == 1){
  			foreach ($result['data'] as $key => $value) {
  				if(empty($value['tb_ingreso_numope'])){
  					$lista .='
	  					<div class="box-comment">
								<img class="img-circle img-sm" src="'.$value['tb_usuario_fot'].'">
								<div class="comment-text">
									<span class="username">'.$value['tb_usuario_nom'].'</span>
									<span class="text-muted pull-right">'.mostrar_fecha_hora($value['tb_ingreso_fecreg']).'</span>
									<p style="text-align: justify;">Pago realizado por el cliente en oficinas</p>
								</div>
							</div>
						';
  				}
  				else{
  					$lista .='
	  					<div class="box-comment">
								<img class="img-circle img-sm" src="'.$value['tb_usuario_fot'].'">
								<div class="comment-text">
									<span class="username">'.$value['tb_usuario_nom'].'</span>
									<span class="text-muted pull-right">'.mostrar_fecha_hora($value['tb_ingreso_fecreg']).'</span>
									<p style="text-align: justify;">Pago realizado con depósito en banco</p>
								</div>
							</div>
						';
  				}
  				$lista .='
  					<div class="box-comment">
							<img class="img-circle img-sm" src="'.$value['tb_usuario_fot'].'">
							<div class="comment-text">
								<span class="username">'.$value['tb_usuario_nom'].'</span>
								<span class="text-muted pull-right">'.mostrar_fecha_hora($value['tb_ingreso_fecreg']).'</span>
								<p style="text-align: justify;">El detalle registrado por el ingreso es el siguiente: '.$value['tb_ingreso_det'].'</p>
							</div>
						</div>
					';
  			}
  		}
  	$result = NULL;
  }
  else{
  	$array = explode('||&&', $cuotapago_his);
		for ($i =0; $i < (count($array) - 1); $i++) { 
			$lista .='
				<div class="box-comment">
					<img class="img-circle img-sm" src="public/images/avatar.png">
					<div class="comment-text">
						<span class="username">Sistema</span>
						<p style="text-align: justify;">'.trim($array[$i]).'</p>
					</div>
				</div>
			';
		}
  }
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_cuotapago_detalle" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Detalle del Pago</h4>
      </div>
      <div class="modal-body">
        <div class="box-body">
					<div class="box-footer box-comments">
						<?php echo $lista;?>
					</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>