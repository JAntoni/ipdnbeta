<?php
if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

  class Morapago extends Conexion{
    public $tb_morapago_id;
    public $tb_morapago_xac;
    public $tb_morapago_reg;
    public $tb_morapago_mod;
    public $tb_morapago_usureg;
    public $tb_morapago_usumod;
    public $tb_morapago_fec;
    public $tb_modulo_id;
    public $tb_morapago_modid;
    public $tb_moneda_id;
    public $tb_morapago_mon;
    public $tb_cliente_id;
    public $tb_morapago_est;


    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO 
                        tb_morapago (
                            tb_morapago_xac, 
                            tb_morapago_reg, 
                            tb_morapago_mod, 
                            tb_morapago_usureg,
                            tb_morapago_usumod,  
                            tb_modulo_id,	
                            tb_morapago_modid,
                            tb_morapago_fec,  
                            tb_moneda_id,
                            tb_morapago_mon, 
                            tb_cliente_id, 
                            tb_morapago_est) 
                    VALUES (
                            :tb_morapago_xac, 
                            NOW(), 
                            NOW(), 
                            :tb_morapago_usureg, 
                            :tb_morapago_usumod, 
                            :tb_modulo_id, 
                            :tb_morapago_modid, 
                            :tb_morapago_fec,
                            :tb_moneda_id, 
                            :tb_morapago_mon, 
                            :tb_cliente_id, 
                            1);"; 

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_morapago_xac", $this->tb_morapago_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morapago_usureg", $this->tb_morapago_usureg, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morapago_usumod", $this->tb_morapago_usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_modulo_id", $this->tb_modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morapago_modid", $this->tb_morapago_modid, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morapago_fec", $this->tb_morapago_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morapago_mon", $this->tb_morapago_mon, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cliente_id", $this->tb_cliente_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $morapago_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['morapago_id'] = $morapago_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_morapago(
                :morapago_id,
                :morapago_xac, :credito_id, :morapagotipo_id,
                :morapago_can, :morapago_pro, :morapago_val,
                :morapago_valtas, :morapago_ser, :morapago_kil,
                :morapago_pes, :morapago_tas, :morapago_det,
                :morapago_web)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":morapago_id", $this->morapago_id, PDO::PARAM_INT);
        $sentencia->bindParam(":morapago_xac", $this->morapago_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":morapagotipo_id", $this->morapagotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":morapago_can", $this->morapago_can, PDO::PARAM_INT);
        $sentencia->bindParam(":morapago_pro", $this->morapago_pro, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_val", $this->morapago_val, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_valtas", $this->morapago_valtas, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_ser", $this->morapago_ser, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_kil", $this->morapago_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_pes", $this->morapago_pes, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_tas", $this->morapago_tas, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_det", $this->morapago_det, PDO::PARAM_STR);
        $sentencia->bindParam(":morapago_web", $this->morapago_web, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($morapago_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_morapago WHERE tb_morapago_id =:morapago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":morapago_id", $morapago_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($morapago_id){
      try {
        $sql = "SELECT cuo.*, mon.*, tb_usuario_nom FROM tb_morapago cuo 
          INNER JOIN tb_moneda mon ON mon.tb_moneda_id = cuo.tb_moneda_id 
          INNER JOIN tb_usuario usu on usu.tb_usuario_id = cuo.tb_morapago_usureg
          WHERE tb_morapago_id =:morapago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":morapago_id", $morapago_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_morapagos_modulo($modulo_id, $morapago_modid){
      try {
        $sql = "SELECT * FROM tb_morapago cp INNER JOIN tb_moneda m ON cp.tb_moneda_id = m.tb_moneda_id WHERE tb_morapago_xac = 1 AND tb_modulo_id =:modulo_id AND tb_morapago_modid =:morapago_modid ORDER BY tb_morapago_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":morapago_modid", $morapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_cuotatipo_ingreso($mod_id,$modid){
      try {
        $sql = "SELECT 
                        *
                FROM 
                        tb_morapago cp 
                        INNER JOIN tb_ingreso i ON cp.tb_morapago_id=i.tb_ingreso_modide
                        INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
                WHERE 
                        tb_morapago_xac=1 AND tb_ingreso_xac=1
                        AND i.tb_modulo_id=50 AND cp.tb_modulo_id=:tb_modulo_id AND cp.tb_morapago_modid=:tb_morapago_modid ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_modulo_id", $mod_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morapago_modid", $modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_morapago_ingresos_por_tipo_cuota($modulo_id, $morapago_modid){
      try {
        $sql = "SELECT tb_morapago_id, i.tb_ingreso_id, tb_ingreso_imp
              FROM tb_morapago cp 
              INNER JOIN tb_ingreso i ON cp.tb_morapago_id=i.tb_ingreso_modide
              INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
              WHERE tb_morapago_xac=1 AND tb_ingreso_xac=1
              AND i.tb_modulo_id = 50 AND cp.tb_modulo_id =:modulo_id AND cp.tb_morapago_modid =:morapago_modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":morapago_modid", $morapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_morapago_ingresos_por_tipo_cuota2($modulo_id, $morapago_modid){
      try {
        $sql = "SELECT *
              FROM tb_morapago cp 
              INNER JOIN tb_ingreso i ON cp.tb_morapago_id=i.tb_ingreso_modide
              INNER JOIN tb_moneda m ON i.tb_moneda_id = m.tb_moneda_id
              WHERE tb_morapago_xac=1 AND tb_ingreso_xac=1
              AND i.tb_modulo_id = 50 AND i.tb_ingreso_modide = cp.tb_morapago_id AND cp.tb_modulo_id =:modulo_id AND cp.tb_morapago_modid =:morapago_modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":morapago_modid", $morapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function modificar_campo($morapago_id, $morapago_columna, $morapago_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($morapago_columna) && ($param_tip == INT || $param_tip == STR)){

          $sql = "UPDATE tb_morapago SET ".$morapago_columna." =:morapago_valor WHERE tb_morapago_id =:morapago_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":morapago_id", $morapago_id, PDO::PARAM_INT);
          if($param_tip == INT)
            $sentencia->bindParam(":morapago_valor", $morapago_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":morapago_valor", $morapago_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  }

?>
