
$(document).ready(function(){
	var options =
    {
      thumbBox: '.thumbBox',
      spinner: '.spinner',
      imgSrc: 'public/images/avatar.png'
    }
  var cropper = $('.imageBox').cropbox(options);

  $('#file').on('change', function(){
    var reader = new FileReader();
    reader.onload = function(e) {
      options.imgSrc = e.target.result;
      cropper = $('.imageBox').cropbox(options);
    }
    reader.readAsDataURL(this.files[0]);
    this.files.length = 0;
  })
  $('#btn_guardar_perfil').on('click', function(){
    var img = cropper.getDataURL();
    $('.cropped').append('<input value="'+img+'" name="hdd_usuario_fot">');
    $('#form_perfil').submit();
  })
  $('#btnZoomIn').on('click', function(){
    cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
    cropper.zoomOut();
  })

  $('#form_perfil').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"perfil/perfil_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_perfil").serialize(),
				beforeSend: function() {
					$('#perfil_mensaje').show(400);
					$('#btn_guardar_perfil').prop('disabled', 'disabled');
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#perfil_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#perfil_mensaje').html(data.mensaje);
		      	setTimeout(function(){
		      		$('#modal_registro_perfil').modal('hide');
		      		location.reload();
		      		 }, 1000
		      	);
					}
					else{
		      	$('#perfil_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#perfil_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_perfil').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#perfil_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#perfil_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  }
	});
});
