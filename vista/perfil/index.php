<?php
	session_name("ipdnsac");
	session_start();
	if(!isset($_SESSION['usuario_id'])){
		echo 'Terminó la sesión';
		exit();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php include(VISTA_URL.'templates/head.php'); ?>
		<link rel="stylesheet" type="text/css" href="static/plugins/cropbox/cropbox.css">
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
	</head>
	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>

		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php include(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php include(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php include('perfil_vista.php');?>
			<!-- INCLUIR FOOTER-->
				<?php include(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include(VISTA_URL.'templates/script.php'); ?>
		
		<script type="text/javascript" src="<?php echo VISTA_URL.'perfil/perfil.js';?>"></script>
		<script src="static/plugins/cropbox/cropbox.js"></script>
	</body>
</html>

