<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('../menu/Menu.class.php');
  $oMenu = new Menu();
  require_once('PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();

  $usuarioperfil_id = $_POST['cmb_usuarioperfil_id'];
?>
<table id="tbl_perfilmenus" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID Menú</th>
      <th>Nombre</th> 
      <th>Permiso</th>
      <th>Leer</th>
      <th>Insertar</th>
      <th>Modificar</th>
      <th>Eliminar</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oMenu->listar_menus(0);
    if($result['estado'] == 1 && intval($usuarioperfil_id) > 0){
      foreach ($result['data'] as $key => $value): ?>
        <tr>
          <td><?php echo $value['tb_menu_id'];?></td>
          <td>
            <?php 
              $caret = (intval($value['cant_submenu']) > 0)? 'down': 'right';
              echo '
                <div>
                  <span class="fa fa-fw fa-chevron-circle-'.$caret.'"></span> '.$value['tb_menu_tit'].'
                </div>
              ';
            ?>
          </td>
          <?php
            $perfilmenu_id = 0;
            $esta_habilitado = '<span class="badge bg-red">Denegado</span>';
            $permiso_leer = '<span class="badge bg-red">NO</span>';
            $permiso_insertar = '<span class="badge bg-red">NO</span>';
            $permiso_modificar = '<span class="badge bg-red">NO</span>';
            $permiso_eliminar = '<span class="badge bg-red">NO</span>';

            $resultPermisos = $oPerfilMenu->permiso_usuarioperfil_menu($usuarioperfil_id, $value['tb_menu_id']);
              if($resultPermisos['estado'] == 1){
                $perfilmenu_id = $resultPermisos['data']['tb_perfilmenu_id'];
                if($resultPermisos['data']['tb_perfilmenu_xac'] == 1)
                  $esta_habilitado = '<span class="badge bg-green">Habilitado</span>';
                $perfilmenu_per = $resultPermisos['data']['tb_perfilmenu_per']; //permisos de tipo L-I-M-E
                $array_permisos = explode('-', $perfilmenu_per);

                if(count($array_permisos)){
                  if(in_array("L", $array_permisos))
                    $permiso_leer = '<span class="badge bg-green">SI</span>';
                  if(in_array("I", $array_permisos))
                    $permiso_insertar = '<span class="badge bg-green">SI</span>';
                  if(in_array("M", $array_permisos))
                    $permiso_modificar = '<span class="badge bg-green">SI</span>';
                  if(in_array("E", $array_permisos))
                    $permiso_eliminar = '<span class="badge bg-green">SI</span>';
                }
              }
            $resultPermisos = NULL;
          ?>
          <td><?php echo $esta_habilitado; ?></td>
          <td><?php echo $permiso_leer; ?></td>
          <td><?php echo $permiso_insertar; ?></td>
          <td><?php echo $permiso_modificar; ?></td>
          <td><?php echo $permiso_eliminar; ?></td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="perfilmenu_form(<?php echo "'M', ".$perfilmenu_id.",".$value['tb_menu_id'];?>)"><i class="fa fa-edit"></i> Editar</a>
          </td>
        </tr>
        <?php
        //SEGUNDO NIVEL
        $result2 = $oMenu->listar_menus($value['tb_menu_id']);
        if($result2['estado'] == 1){
          foreach ($result2['data'] as $key => $value): ?>
            <tr>
              <td><?php echo $value['tb_menu_id'];?></td>
              <td>
                <?php 
                  $caret = (intval($value['cant_submenu']) > 0)? 'down': 'right';
                  echo '
                    <div class="col-md-offset-1"> 
                      <span class="fa fa-fw fa-chevron-circle-'.$caret.'"></span> '.$value['tb_menu_tit'].'
                    </div>
                  ';
                ?>
              </td>
              <?php
                $perfilmenu_id = 0;
                $esta_habilitado = '<span class="badge bg-red">Denegado</span>';
                $permiso_leer = '<span class="badge bg-red">NO</span>';
                $permiso_insertar = '<span class="badge bg-red">NO</span>';
                $permiso_modificar = '<span class="badge bg-red">NO</span>';
                $permiso_eliminar = '<span class="badge bg-red">NO</span>';

                $resultPermisos = $oPerfilMenu->permiso_usuarioperfil_menu($usuarioperfil_id, $value['tb_menu_id']);
                  if($resultPermisos['estado'] == 1){
                    $perfilmenu_id = $resultPermisos['data']['tb_perfilmenu_id'];
                    if($resultPermisos['data']['tb_perfilmenu_xac'] == 1)
                      $esta_habilitado = '<span class="badge bg-green">Habilitado</span>';
                    $perfilmenu_per = $resultPermisos['data']['tb_perfilmenu_per']; //permisos de tipo L-I-M-E
                    $array_permisos = explode('-', $perfilmenu_per);

                    if(count($array_permisos)){
                      if(in_array("L", $array_permisos))
                        $permiso_leer = '<span class="badge bg-green">SI</span>';
                      if(in_array("I", $array_permisos))
                        $permiso_insertar = '<span class="badge bg-green">SI</span>';
                      if(in_array("M", $array_permisos))
                        $permiso_modificar = '<span class="badge bg-green">SI</span>';
                      if(in_array("E", $array_permisos))
                        $permiso_eliminar = '<span class="badge bg-green">SI</span>';
                    }
                  }
                $resultPermisos = NULL;
              ?>
              <td><?php echo $esta_habilitado; ?></td>
              <td><?php echo $permiso_leer; ?></td>
              <td><?php echo $permiso_insertar; ?></td>
              <td><?php echo $permiso_modificar; ?></td>
              <td><?php echo $permiso_eliminar; ?></td>
              <td align="center">
                <a class="btn btn-warning btn-xs" title="Editar" onclick="perfilmenu_form(<?php echo "'M', ".$perfilmenu_id.",".$value['tb_menu_id'];?>)"><i class="fa fa-edit"></i> Editar</a>
              </td>
            </tr>
            <?php
            //TERCER NIVEL
            $result3 = $oMenu->listar_menus($value['tb_menu_id']);
            if($result3['estado'] == 1){
              foreach ($result3['data'] as $key => $value): ?>
                <tr>
                  <td><?php echo $value['tb_menu_id'];?></td>
                  <td>
                    <?php
                      $caret = (intval($value['cant_submenu']) > 0)? 'down': 'right';
                      echo '
                        <div class="col-md-offset-2">
                          <span class="fa fa-fw fa-chevron-circle-'.$caret.'"></span> '.$value['tb_menu_tit'].'
                        </div>
                      ';
                    ?>
                  </td>
                  <?php
                    $perfilmenu_id = 0;
                    $esta_habilitado = '<span class="badge bg-red">Denegado</span>';
                    $permiso_leer = '<span class="badge bg-red">NO</span>';
                    $permiso_insertar = '<span class="badge bg-red">NO</span>';
                    $permiso_modificar = '<span class="badge bg-red">NO</span>';
                    $permiso_eliminar = '<span class="badge bg-red">NO</span>';

                    $resultPermisos = $oPerfilMenu->permiso_usuarioperfil_menu($usuarioperfil_id, $value['tb_menu_id']);
                      if($resultPermisos['estado'] == 1){
                        $perfilmenu_id = $resultPermisos['data']['tb_perfilmenu_id'];
                        if($resultPermisos['data']['tb_perfilmenu_xac'] == 1)
                          $esta_habilitado = '<span class="badge bg-green">Habilitado</span>';
                        $perfilmenu_per = $resultPermisos['data']['tb_perfilmenu_per']; //permisos de tipo L-I-M-E
                        $array_permisos = explode('-', $perfilmenu_per);

                        if(count($array_permisos)){
                          if(in_array("L", $array_permisos))
                            $permiso_leer = '<span class="badge bg-green">SI</span>';
                          if(in_array("I", $array_permisos))
                            $permiso_insertar = '<span class="badge bg-green">SI</span>';
                          if(in_array("M", $array_permisos))
                            $permiso_modificar = '<span class="badge bg-green">SI</span>';
                          if(in_array("E", $array_permisos))
                            $permiso_eliminar = '<span class="badge bg-green">SI</span>';
                        }
                      }
                    $resultPermisos = NULL;
                  ?>
                  <td><?php echo $esta_habilitado; ?></td>
                  <td><?php echo $permiso_leer; ?></td>
                  <td><?php echo $permiso_insertar; ?></td>
                  <td><?php echo $permiso_modificar; ?></td>
                  <td><?php echo $permiso_eliminar; ?></td>
                  <td align="center">
                    <a class="btn btn-warning btn-xs" title="Editar" onclick="perfilmenu_form(<?php echo "'M', ".$perfilmenu_id.",".$value['tb_menu_id'];?>)"><i class="fa fa-edit"></i> Editar</a>
                  </td>
                </tr>
                <?php
                //FIN TERCER NIVEL
              endforeach;
            }
            $result3 = NULL;
            //FIN SEGUNDO NIVEL
          endforeach;
        }
        $result2 = NULL;
        //FIN PRIMER NIVEL
      endforeach;
    }
    $result = NULL;
    ?>
  </tbody>
</table>
