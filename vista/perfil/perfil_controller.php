<?php
  session_name("ipdnsac");
  session_start();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../funciones/funciones.php');

  $action = $_POST['action'];

  if($action == 'insertar'){
    $usuario_per = '';
    $usuarioperfil_id = intval($_POST['hdd_usuarioperfil_id']);
    $menu_id = intval($_POST['hdd_menu_id']);
    $usuario_xac = intval($_POST['rad_usuario_xac']);
    $array_permisos = $_POST['che_usuario_per'];
    

    if(count($array_permisos) > 0){
      $usuario_per = implode(',', $array_permisos);
      $usuario_per = str_replace(',', '-', $usuario_per);
    }

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar los permisos. ';

    if($usuarioperfil_id > 0 && $menu_id > 0){
      if($ousuario->insertar($usuario_per, $usuarioperfil_id, $menu_id, $usuario_xac)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Permisos para el Perfil Menú registrado correctamente.';
      }
    }
    else
      $data['mensaje'] = 'No se ha identificado los IDS del UsuarioPerfil o del Menú';

    echo json_encode($data);
  }

  elseif($action == 'modificar'){
    $usuario_id = intval($_POST['hdd_usuario_id']);
    $usuario_fot = $_POST['hdd_usuario_fot'];

    $imageData = base64ToImage($usuario_fot); //funcion encontrada en funciones.php, devuelve data['extension'], data['image']

    $data['estado'] = 0;
    $data['mensaje'] = 'Error al guardar la imagen en el directorio';

    $nombre_foto = 'avatar_usu_'.$usuario_id.'.'.$imageData['extension'];
    $ruta = '../../public/images/colaborador/';
    
    if($usuario_id > 0){
      if(file_exists($ruta.$nombre_foto))
        unlink($ruta.$nombre_foto);

      if(file_put_contents($ruta.$nombre_foto, $imageData['image'])){

        $usuario_columna = 'tb_usuario_fot';
        $usuario_valor = 'public/images/colaborador/'.$nombre_foto;
        $param_tip = 'STR';

        if($oUsuario->modificar_campo($usuario_id, $usuario_columna, $usuario_valor, $param_tip)){
          $data['estado'] = 1;
          $data['mensaje'] = 'Imagen guardada con éxito';
        }
      }
    }
    else
      $data['mensaje'] = 'No se ha identificado el ID del: usuario';

    echo json_encode($data);
  }

  elseif($action == 'clave_frase'){
    $usuario_cla = trim($_POST['usuario_cla']);
    $usuario_frase = trim($_POST['usuario_frase']);
    $usuario_id = intval($_POST['usuario_id']);

    if($usuario_id > 0){
      $oUsuario->moficar_clave_frase($usuario_cla, $usuario_frase, $usuario_id);
      $data['estado'] = 1;
      $data['mensaje'] = 'Datos actualizados correctamente';
    }
    else{
      $data['estado'] = 0;
      $data['mensaje'] = 'El usuario no está identificado, consulte con sistemas';
    }
    echo json_encode($data);
  }
  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'Action no identificado para: '.$action;

    echo json_encode($data);
  }
  
?>