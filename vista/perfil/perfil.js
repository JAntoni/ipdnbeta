
function usuario_perfil(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"perfil/perfil_usuario_perfil.php",
    async: true,
    dataType: "html",
    data: ({
      
    }),
    beforeSend: function() {

    },
    success: function(data){
      $('#div_usuario_perfil').html(data);
    },
    complete: function(data){

    },
    error: function(data){
      
    }
  });
}
function perfil_form(usuario_act, usuario_id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"perfil/perfil_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      usuario_id: usuario_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_perfil_form').html(data);
      	$('#modal_registro_perfil').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_perfil'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_perfil', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'perfil';
      	var div = 'div_modal_perfil_form';
      	permiso_solicitud(usuario_act, perfil_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function cambiar_clave_frase(){
  var usuario_cla = $('#txt_usuario_cla').val();
  var usuario_frase = $('#txt_usuario_frase').val();
  var usuario_id = $('#hdd_usuario_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL+"perfil/perfil_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'clave_frase',
      usuario_cla: usuario_cla,
      usuario_frase: usuario_frase,
      usuario_id: usuario_id
    }),
    beforeSend: function() {

    },
    success: function(data){
      console.log(data)
      if(parseInt(data.estado) > 0)
        notificacion_success(data.mensaje, 3000);
      else
        alerta_warning('ERROR', data.mensaje);
    },
    complete: function(data){
      
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}

$(document).ready(function() {
	console.log('cambio 2222')
});