<?php
	session_name("ipdnsac");
	session_start();
	if(defined('VISTA_URL')){
    require_once(VISTA_URL.'usuario/Usuario.class.php');
    require_once(VISTA_URL.'colaborador/Colaborador.class.php');
	}
  else{
		require_once('../usuario/Usuario.class.php');
		require_once('../colaborador/Colaborador.class.php');
  }
  
  $oUsuario = new Usuario();
  $oColaborador = new Colaborador();

  $usuario_id = (isset($_POST['usuario_id']))? intval($_POST['usuario_id']) : intval($_SESSION['usuario_id']);

  //datos del colaborador
  $result = $oUsuario->mostrarUno($usuario_id);
  	$usuario_nom = $result['data']['tb_usuario_nom'];	
    $usuario_ape = $result['data']['tb_usuario_ape'];
    $usuario_dni = $result['data']['tb_usuario_dni'];
    $usuario_dir = $result['data']['tb_usuario_dir'];
    $usuario_tel = $result['data']['tb_usuario_tel'];
    $usuario_ema = $result['data']['tb_usuario_ema'];
    $usuario_use = $result['data']['tb_usuario_use'];
    $usuario_per = (intval($result['data']['tb_usuario_per']) == 1)? 'Gefatura' : 'Ventas';
    $usuarioperfil_nom = $result['data']['tb_usuarioperfil_nom'];
    $usuariogrupo_nom = $result['data']['tb_usuariogrupo_nom'];
    $usuario_fot = $result['data']['tb_usuario_fot'];
		$usuario_cla = $result['data']['tb_usuario_cla'];
		$usuario_frase = $result['data']['tb_usuario_frase'];
  $result = NULL;
  //datos del cargo que cumple el colaborador
  $result = $oColaborador->colaborador_cargo_usuario($usuario_id);
  	if($result['estado'] == 1)
  		$cargo_nom = $result['data']['tb_cargo_nom'];
  $result = NULL;
?>

	<div class="box-body">
		<div class="col-md-8">
			<strong>Nombres</strong>
			<p class="text-muted"><?php echo $usuario_nom.' '.$usuario_ape;?></p>
			<hr>

			<strong>DNI</strong>
			<p class="text-muted"><?php echo $usuario_dni;?></p>
			<hr>

			<strong>Dirección</strong>
			<p class="text-muted"><?php echo $usuario_dir;?></p>
			<hr>

			<strong>Teléfono</strong>
			<p class="text-muted"><?php echo $usuario_tel;?></p>
			<hr>

			<strong>Usuario / Email</strong>
			<p class="text-muted"><?php echo $usuario_use;?></p>
			<hr>

			<strong>Tipo de Personal</strong>
			<p class="text-muted"><?php echo $usuario_per;?></p>
			<hr>

			<strong>Perfil Usuario</strong>
			<p class="text-muted"><?php echo $usuarioperfil_nom;?></p>
			<hr>

			<strong>Grupo Usuario</strong>
			<p class="text-muted"><?php echo $usuariogrupo_nom;?></p>
			<hr>

			<div class="row">
				<input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">
				<div class="col-md-5 form-group">
					<label for="txt_usuario_cla" class="control-label">Cambiar Contraseña</label>
					<input type="text" name="txt_usuario_cla" id="txt_usuario_cla" class="form-control input-sm" autocomplete="off" value="<?php echo $usuario_cla;?>">
				</div>
				<div class="col-md-5 form-group">
					<label for="txt_usuario_frase" class="control-label">Cambiar Frase secreta</label>
					<input type="text" name="txt_usuario_frase" id="txt_usuario_frase" class="form-control input-sm" autocomplete="off" value="<?php echo $usuario_frase;?>">
				</div>
				<div class="col-md-2 form-group">
					<br>
					<button class="btn btn-info" onclick="cambiar_clave_frase()">Guardar Cambios</button>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<img src="<?php echo $usuario_fot;?>">
		</div>
	</div>
