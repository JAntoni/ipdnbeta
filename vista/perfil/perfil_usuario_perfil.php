<?php
	session_name("ipdnsac");
	session_start();
	if(defined('VISTA_URL')){
    require_once(VISTA_URL.'usuario/Usuario.class.php');
    require_once(VISTA_URL.'colaborador/Colaborador.class.php');
	}
  else{
		require_once('../usuario/Usuario.class.php');
		require_once('../colaborador/Colaborador.class.php');
  }
  
  $oUsuario = new Usuario();
  $oColaborador = new Colaborador();

  $usuario_id = (isset($_POST['usuario_id']))? intval($_POST['usuario_id']) : intval($_SESSION['usuario_id']);

  //datos del colaborador
  $result = $oUsuario->mostrarUno($usuario_id);
  	$usuario_nom = $result['data']['tb_usuario_nom'];
  	$usuario_fot = $result['data']['tb_usuario_fot'];
  $result = NULL;
  //datos del cargo que cumple el colaborador
  $cargo_nom = 'SIN CARGO';
  $result = $oColaborador->colaborador_cargo_usuario($usuario_id);
  	if($result['estado'] == 1)
  		$cargo_nom = $result['data']['tb_cargo_nom'];
  $result = NULL;
?>
<div class="box box-primary">
	<div class="box-body box-profile">
		<img class="profile-user-img img-responsive img-circle" id="img_usuario_logrado" src="<?php echo $usuario_fot; ?>">
		<h3 class="profile-username text-center"><?php echo $usuario_nom; ?></h3>
		<p class="text-muted text-center"><?php echo $cargo_nom; ?></p>
		<?php 
			if(!isset($_POST['usuario_id']))
				echo '<p class="text-muted text-center"><a href="javascript:void(0)" onclick="perfil_form(\'M\', '.$usuario_id.')"><i class="fa fa-fw fa-image"></i> Cambiar Imagen</a></p>';
		?>
		<ul class="list-group list-group-unbordered">
			<li class="list-group-item">
				<b>Créditos Menores</b>
				<span class="pull-right badge bg-blue">25</span>
			</li>
			<li class="list-group-item">
				<b>Créditos de ASVEH</b>
				<span class="pull-right badge bg-aqua">25</span>
			</li>
			<li class="list-group-item">
				<b>Créditos de GARVEH</b>
				<span class="pull-right badge bg-red">25</span>
			</li>
			<li class="list-group-item">
				<b>Créditos Hipotecarios</b>
				<span class="pull-right badge bg-green">25</span>
			</li>
		</ul>
	</div>
</div>