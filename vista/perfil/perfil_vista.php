<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Opciones</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="perfil_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Perfil Menú...</h4>
        </div>
			<div class="col-sm-3 col-md-3" id="div_usuario_perfil">
				<?php require_once('perfil_usuario_perfil.php');?>
			</div>

			<div class="col-sm-9 col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#misdatos" data-toggle="tab" aria-expanded="true">Mis Datos</a></li>
						<li class=""><a href="#mislogros" data-toggle="tab" aria-expanded="true">Mis Logros</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="misdatos">
							<?php require_once('perfil_usuario_datos.php');?>
						</div>

						<div class="tab-pane" id="mislogros">
							<?php require_once('perfil_usuario_datos.php');?>
						</div>
					</div>
				</div>
			</div>

			<div id="div_modal_perfil_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
