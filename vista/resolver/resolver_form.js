$(document).ready(function() {

    
    
    $('#frm_cre_opc').submit(function (event) {
            
            event.preventDefault();
            
            
            var det = $('#txt_cre_det').val();
            var fil = $('#fil_pdf').val();

            if (!det)
                return false;

            datos = new FormData();
            datos.append('action', $('#action').val());
            datos.append('formData', $('#frm_cre_opc').serialize());

            if ($('#action').val() == 'resolver') {
                datos.append('archivo', $('input[type=file]')[0].files[0]);
                datos.append('imagen', $('input[type=file]')[1].files[0]);
            }

            $.ajax({
                type: "POST",
                url: VISTA_URL + 'creditogarveh/credito_reg.php',
                dataType: "html",
                data: datos,
                contentType: false,
                processData: false,
                beforeSend: function () {
//                    $('#msj_credito').html("Guardando datos...");
//                    $('#msj_credito').show(100);
                },
                success: function (data) {
                    console.log(data);
//                    $('#msj_credito').text(data);
                    alerta_success('EXITO',data)
                    $('#modal_creditogarveh_resolver').modal('hide');

                },
                complete: function (data) {
                    console.log(data);
                    if (data.statusText == "parsererror") {
                        console.log(data);
                        $('#msj_resolver').text(data.responseText);
                    } else {
                        $('#modal_creditogarveh_resolver').modal('hide');
                        //$('#div_resolver_form').empty();
                        creditogarveh_tabla();
                    }
                }
            });
        });
});