<?php
if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

$cre_id = $_POST['cre_id'];
$cre_tip = $_POST['cre_tip']; //3 credito garveh, 4 credito hipo

?>
<div id="msj_renovacion" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_renovar_cuotas" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Renovar Cuotas</h4>
      </div>
      <form id="frm_renovar_cuotas">
        <input type="hidden" id="hdd_cre_id" value="<?php echo $cre_id;?>">
	<input type="hidden" id="hdd_cre_tip" value="<?php echo $cre_tip;?>">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="txt_renov_det" class="control-label" >Ingrese Detalle: :</label>
                            <div class="form-group">
                              <textarea name="txt_renov_det" id="txt_renov_det" rows="2" cols="70"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardar_renovacion">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_credito_renovar" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/resolver/renovar_cuotas_form.js?ver=437433';?>"></script>
