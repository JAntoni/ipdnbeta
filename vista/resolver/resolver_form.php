<?php
require_once ("../../core/usuario_sesion.php");

require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once ("../refinanciar/Refinanciar.class.php");
$oRefinanciar = new Refinanciar();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../cuota/Cuotadetalle.class.php');
$oCuotadetalle = new Cuotadetalle();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
if($action == 'resolver'){
    $titulo = "DATOS PARA RESOLVER EL CRÉDITO";
}
if($action == 'paralizar'){
    $titulo = "DATOS PARA PARALIZAR EL CRÉDITO";
}
if($action == 'vigente'){
    $titulo = "DATOS PARA PONER EL CRÉDITO VIGENTE";
}
$cre_tip_cre_aden = $_POST['cre_tip_cre_aden']; //si es 1 es credito normal si es 2 es adenda si es 3 es acuerdo de pago
$cre_tip = $_POST['cre_tip']; //tipo de crédito 1 menor, 2 asiveh, 3 garveh, 4 hipo
$cre_id = $_POST['cre_id'];
$link = '';

if ($cre_tip == 2) {
    require_once ("../creditoasiveh/Creditoasiveh.class.php");
    $oCredito = new Creditoasiveh();
    $link = 'creditoasiveh/Creditoasiveh.php';
}
if ($cre_tip == 3) {
    require_once ("../creditogarveh/Creditogarveh.class.php");
    $oCredito = new Creditogarveh();
    $link = 'creditogarveh/credito_reg.php';
}
if ($cre_tip == 4) {
    require_once ("../creditohipo/Creditohipo.class.php");
    $oCredito = new cCredito();
    $link = 'creditohipo/credito_reg.php';
}

$result = $oCredito->mostrarUno($cre_id);
if ($result['estado'] == 1) {
    $cre_mon_id = $result['data']['tb_moneda_id'];
    $cli_id = $result['data']['tb_cliente_id'];
    $cre_preaco = $result['data']['tb_credito_preaco'];
    $cre_subper_id = $result['data']['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
    $cre_fecren = mostrar_fecha($result['data']['tb_credito_fecren']); //fecha de vencimiento del SOAT
    $cre_fecseg = mostrar_fecha($result['data']['tb_credito_fecseg']); //fecha de vencimiento del STR
    $cre_fecgps = mostrar_fecha($result['data']['tb_credito_fecgps']); //fecha de vencimiento del GPS
    $cre_fpagimp = mostrar_fecha($result['data']['tb_credito_pagimp']); //fecha del pago del inpuesto vehicular
}
$result = null;

if ($action == 'resolver' || $action == 'paralizar') {
    //en primer lugar verificamos si el credito tiene acuerdos de pago activos, si los tiene entonces no se podrá refinanciar hasta que pague su acuerdo
    $result = $oAcuerdopago->listar_acuerdopago_credito_xac($cre_id, $cre_tip, 0, $cli_id); //si xac = 0 el acuerdo está activo
    if ($result['estado'] == 1) {
        $rows = count($result['data']);
    }
    $result = null;

    if ($rows > 0) {
        echo '<h2 style="color: red;">NO SE PUEDE ' . strtoupper($action) . ' YA QUE TIENE ' . $rows . ' ACUERDO(S) ACTIVOS, POR FAVOR APLIQUE EL ACUERDO DE PAGO LUEGO RESUELVA O PARALICE EL CRÉDITO.</h2>';
        exit();
    }

    //verificamos que si el cliente activó el monto de la caja AP para pagar cuotas vencidas, haya aplicado todo el monto
    //consultamos si de este crédito hay acuerdos de pago activos para que sus montos pagados sean aplicados totalmente
    $result1 = $oAcuerdopago->listar_por_credito_aplicar($cre_id, $cre_tip, $cli_id); //devuelve todos los AP que su columna tb_acuerdopago_apl = 1: disponibles para aplicar
    if ($result1['estado'] == 1) {
        $rows_apl = count($result1['data']);
    }
    $result1 = null;
    //si hay AP disponibles para aplicar obtenemos todos sus ingresos guardados en la CAJA AP restanto egresos de la misma caja, para saber cuanto hay disponible para aplicar
    $monto_aplicar = 0;

    if ($rows_apl > 0) {
        $monto_ingre_ap = 0;
        $monto_egre_ap = 0;
        $caja_id = 14; //caja ap

        $dts2 = $oAcuerdopago->listar_ap_aplicar_por_cliente(2, $cli_id);
        if ($dts2['estado'] == 1) {
            foreach ($dts2['data'] as $key => $value) {
                $acuerdo_id = $value['tb_credito_nuevo']; //tomará el último ACUERDO DE PAGO registrado, ya que ese debe ser
                $result = $oIngreso->ingreso_total_por_credito_cuotadetalle($value['tb_credito_nuevo'], 2, $cre_mon_id); //parametro 2: CREDITO ASIVEH
                if ($result['estado'] == 1) {
                    foreach ($result['data'] as $key => $value1) {
                        $monto_ingre_ap += formato_moneda($value1['importe_total']);
                    }
                }
                $result = null;
            }
        }
        $dts2 = null;

        $modulo_id = 100; //ya que egreso no tiene cliente_id, el egreso para CAJA AP tendra como modulo = 100 e egreso_modide = cliente_id
        $dts = $oEgreso->egresos_caja_cliente($caja_id, $cre_mon_id, $modulo_id, $cli_id);
        if ($dts['estado'] == 1) {
            foreach ($dts['data'] as $key => $value) {
                $monto_egre_ap += $value['tb_egreso_imp'];
            }
        }
        $dts = null;

        $monto_aplicar = $monto_ingre_ap - $monto_egre_ap;
        $monto_aplicar = number_format($monto_aplicar, 2, '.', '');
    }

    if ($monto_aplicar > 0) {
        echo '<h2 style="color: red;">NO SE PUEDE ' . strtoupper($action) . ' YA QUE HAY MONTO DISPONIBLE EN CAJA AP PARA SER APLICACO A LAS CUOTAS VENCIDAS. Monto (' . $monto_aplicar . ')</h2>';
        exit();
    }
}

if ($action == 'resolver') {
    //calculamos SALDOS CAPITALES de las adendas y del crédito para sumar todo y ese será el precio acordado, para ello consultamos tipo de cambio del día
    $result = $oMonedacambio->consultar(fecha_mysql(date('d-m-Y')));
    if ($result['estado'] == 1) {
        $cambio = $result['data']['tb_monedacambio_val'];
    }
    $result = null;

    if (empty($cambio)) {
        echo '<strong style="color: red;">INGRESE EL TIPO DE CAMBIO DEL DÍA POR FAVOR</strong><br><br>';
        exit();
    }

    $monto_aden_cap = 0;
    $capitales = '';
    $dts = $oCredito->listar_adendas_credito($cre_id); //lista adendas que esten en estado 3: desembolsado o vigentes
    if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $value) {
            $aden_id = $dt['tb_credito_id'];

            $dts2 = $oCredito->mostrarUno($aden_id);
            if ($dts2['estado'] == 1) {
                $mon_id = $dts2['data']['tb_moneda_id'];
                $subper_id = $dts2['data']['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
                $aden_preaco = $dts2['data']['tb_credito_preaco'];
            }
            $dts2 = null;

            $refs = $oCuotadetalle->ultima_cuotadetalle_pagada_pagoparcial($aden_id, $cre_tip); //devuelve la ultima cuotadetalle pagada o en pago parcial
            if ($refs['estado'] == 1) {
                $rowsRef = count($refs['data']);

                if ($rowsRef > 0) {
                    $cuodet_num = $refs['data']['tb_cuotadetalle_num']; //numero de cuotas detalle que están por pagar de la cuota
                    $cuodet_fec = $refs['data']['tb_cuotadetalle_fec']; //fecha de pago de la cuotadetalle
                    $cuo_amor = $refs['data']['tb_cuota_amo']; //amortización hasta esa fecha
                    $cuo_cap = $refs['data']['tb_cuota_cap']; //monto del capital hasta ese momento
                    $periodo = 1;

                    if ($subper_id == 2)
                        $periodo = 2;
                    if ($subper_id == 3)
                        $periodo = 4;

                    $moneda = 'S/.';
                    if ($mon_id == 2)
                        $moneda = 'US$';

                    $res = $cuo_cap - (($cuo_amor / $periodo) * $cuodet_num);
                    $deuda_anterior = 0;
                    $pagos_cuotadetalle = 0;
                    $mon_cuodet = 0;

                    //consultamos las cuotasdetalle vencidas impagas o en pago parcial anteiores a la fecha de esta última cuotadetalle, /devuelve cuotas vencidas en pago parcial o impagas
                    $result2 = $oCuotadetalle->listar_cuotasdetalle_menor_fecha_impagas_pagoparcial($aden_id, $cre_tip, '<=', $cuodet_fec);
                    if ($result2['estado'] == 1) {
                        foreach ($result2['data'] as $key => $value1) {
                            $mon_cuodet += $value1['tb_cuotadetalle_cuo'];

                            $result3 = $oCuotapago->importe_total_cuotadetalle($dt2['tb_cuotadetalle_id']); //la columa de la suma se llama importe
                            if ($result3['estado'] == 1) {
                                $pagos_cuotadetalle += $result3['data']['importe'];
                            }
                        }
                    }
                    $result2 = "";
                    
                    $deuda_anterior = $mon_cuodet - $pagos_cuotadetalle;

                    if ($cre_mon_id == 1 && $mon_id == 1) {
                        $moneda2 = 'S/.';
                        $total = $res;
                    }
                    if ($cre_mon_id == 1 && $mon_id == 2) {
                        $moneda2 = 'S/.';
                        $total = $res * $cambio;
                        $deuda_anterior = $deuda_anterior * $cambio;
                    }
                    if ($cre_mon_id == 2 && $mon_id == 1) {
                        $moneda2 = 'US$.';
                        $total = $res / $cambio;
                        $deuda_anterior = $deuda_anterior / $cambio;
                    }
                    if ($cre_mon_id == 2 && $mon_id == 2) {
                        $moneda2 = 'US$.';
                        $total = $res;
                    }

                    $monto_aden_cap += ($total + $deuda_anterior);
                    $capitales .= '
                        <tr>
                          <td><b>Adenda</b></td>
                          <td>' . $aden_id . '</td>
                          <td>' . $moneda . ' ' . formato_moneda($res) . '</td>
                          <td>' . $cambio . '</td>
                          <td>' . $moneda2 . ' ' . formato_moneda($total) . '</td>
                          <td>' . $moneda2 . ' ' . formato_moneda($deuda_anterior) . '</td>
                          <td>' . $moneda2 . ' ' . formato_moneda($total + $deuda_anterior) . '</td>
                        </tr>';
                } else {
                    $moneda = 'S/.';
                    if ($mon_id == 2)
                        $moneda = 'US$';

                    $res = $aden_preaco;

                    if ($cre_mon_id == 1 && $mon_id == 1) {
                        $moneda2 = 'S/.';
                    }
                    if ($cre_mon_id == 1 && $mon_id == 2) {
                        $moneda2 = 'S/.';
                        $res = $aden_preaco * $cambio;
                    }
                    if ($cre_mon_id == 2 && $mon_id == 1) {
                        $moneda2 = 'US$.';
                        $res = $aden_preaco / $cambio;
                    }
                    if ($cre_mon_id == 2 && $mon_id == 2) {
                        $moneda2 = 'US$.';
                    }

                    $monto_aden_cap += $res;
                    $capitales .= '
                        <tr>
                          <td><b>Adenda</b></td>
                          <td>' . $aden_id . '</td>
                          <td>' . $moneda . ' ' . formato_moneda($aden_preaco) . '</td>
                          <td>' . $cambio . '</td>
                          <td>' . $moneda2 . ' ' . formato_moneda($res) . '</td>
                          <td>' . $moneda2 . ' 0.00</td>
                          <td>' . $moneda2 . ' ' . formato_moneda($res) . '</td>
                        </tr>';
                }
            }
            $refs = null;
        }
    }
    $dts = null;

    //capital del crédito
    $monto_cre_cap = 0;
    $moneda_cre = 'S/.';
    if ($cre_mon_id == 2)
        $moneda_cre = 'US$.';

    $refs = $oCuotadetalle->ultima_cuotadetalle_pagada_pagoparcial($cre_id, $cre_tip); //parametro 2: credito asiveh
    if ($refs['estado'] == 1) {
        $rowsRef = count($refs['data']);
    
        if ($rowsRef > 0) {
            $cuodet_num = $refs['data']['tb_cuotadetalle_num']; //numero de cuotas detalle que están por pagar de la cuota
            $cuodet_fec = $refs['data']['tb_cuotadetalle_fec']; //fecha de pago de la cuotadetalle
            $cuo_amor = $refs['data']['tb_cuota_amo']; //amortización hasta esa fecha
            $cuo_cap = $refs['data']['tb_cuota_cap']; //monto del capital hasta ese momento
            $periodo = 1;

            if ($cre_subper_id == 2)
                $periodo = 2;
            if ($cre_subper_id == 3)
                $periodo = 4;

            $res = $cuo_cap - (($cuo_amor / $periodo) * $cuodet_num);
            $deuda_anterior = 0;
            $pagos_cuotadetalle = 0;
            $mon_cuodet = 0;

            //consultamos las cuotasdetalle vencidas impagas o en pago parcial anteiores a la fecha de esta última cuotadetalle, /devuelve cuotas vencidas en pago parcial o impagas
            $result = $oCuotadetalle->listar_cuotasdetalle_menor_fecha_impagas_pagoparcial($cre_id, $cre_tip, '<=', $cuodet_fec);
            if ($result['estado'] == 1) {
                foreach ($result['data'] as $key => $value) {
                    $mon_cuodet += $value['tb_cuotadetalle_cuo'];

                    $result1 = $oCuotapago->importe_total_cuotadetalle($dt2['tb_cuotadetalle_id']); //la columa de la suma se llama importe
                    if ($result1['estado'] == 1) {
                        $pagos_cuotadetalle += $result1['data']['importe'];
                    }
                    $result1 = null;
                }
            }
            $result = null;

            //$deuda_anterior = $mon_cuodet + $pagos_cuotadetalle; //ANTES SE TOMABA LA DEUDA ANTERIOR PARA RESOLVER AHORA YA NO SE TOMARÁ
            $deuda_anterior = 0;
            $monto_cre_cap += $res + $deuda_anterior;

            $capitales .= '
            <tr>
              <td><b>CRÉDITO</b></td>
              <td>' . $cre_id . '</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($res) . '</td>
              <td>' . $cambio . '</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($res) . '</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($deuda_anterior) . '</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($res + $deuda_anterior) . '</td>
            </tr>';
        } else {
            $monto_cre_cap = $cre_preaco;
            $capitales .= '
            <tr>
              <td><b>CRÉDITO</b></td>
              <td>' . $cre_id . '</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($cre_preaco) . '</td>
              <td>' . $cambio . '</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($cre_preaco) . '</td>
              <td>' . $moneda_cre . ' 0.00</td>
              <td>' . $moneda_cre . ' ' . formato_moneda($cre_preaco) . '</td>
            </tr>';
        }
    }
    $refs = null;

    $total_capital = $monto_aden_cap + $monto_cre_cap;
    $total_capital = number_format($total_capital, 2, '.', '');
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_resolver" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo; ?></h4>
      </div>
        <form id="frm_cre_opc" enctype="multipart/form-data">
            <input name="action" id="action" type="hidden" value="<?php echo $action; ?>">
            <input name="hdd_cre_tip" type="hidden" value="<?php echo $cre_tip_cre_aden; ?>">
            <input name="hdd_cre_id" type="hidden" value="<?php echo $cre_id; ?>">
            <input name="hdd_link" type="hidden" value="<?php echo $link; ?>">
            <input name="hdd_mon_result" type="hidden" value="<?php echo $total_capital; ?>">
            <input type="hidden" name="hdd_moneda" value="<?php echo $moneda_cre; ?>">
            <div class="modal-body">
                      <div class="row">
                        <div class="col-md-12 col-lg-12">
                          <div class="panel panel-info">
                            <div class="panel-body">
                            <?php if ($action == 'resolver'): ?>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">    
                                        <legend>Capital de Adendas y Crédito</legend>
                                        <table id="tbl_cap" class="table table-bordered table-hover">
                                            <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                                                <th id="filacabecera">TIPO</th>
                                                <th id="filacabecera">ID</th>
                                                <th id="filacabecera">CAPITAL</th>
                                                <th id="filacabecera">CAMBIO</th>
                                                <th id="filacabecera">TOTAL</th>
                                                <th id="filacabecera">DEUDA ANTERIOR</th>
                                                <th id="filacabecera">SALDO</th>
                                            </tr>
                                            <?php echo $capitales; ?>
                                            <tr>
                                                <td colspan="6"><b>Total Capital:</b></td>
                                                <td><?php echo $moneda_cre . ' ' . formato_moneda($monto_aden_cap + $monto_cre_cap); ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-7 col-lg-7">
                                    <label class="control-label">Ingrese Detalle:</label>
                                    <textarea name="txt_cre_det" id="txt_cre_det" rows="2" cols="72"></textarea>
                                </div>
                            </div>
                            <?php if ($action == 'resolver'): ?>
                                <p>    
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <label class="control-label">Suba Documento PDF:</label>
                                    <input class="form-control input-sm" name="fil_pdf" id="fil_pdf" type="file" accept="application/pdf" required>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <label class="control-label">Suba Imagen (opcional):</label>
                                    <input class="form-control input-sm" name="fil_img" id="fil_img" type="file" accept="image/*">
                                </div>
                            </div>
                            <?php endif; ?>

                            <div id="msj_resolver" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-info" id="btn_guardar_resolver">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
        </form>

    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/resolver/resolver_form.js?ver=437433';?>"></script>