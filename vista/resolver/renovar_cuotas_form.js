$(document).ready(function() {
    //console.log("VER");
    $('#frm_renovar_cuotas').submit(function(event) {
      event.preventDefault();
      var renov_det = $('#txt_renov_det').val();
      if(!renov_det){
      	$('#txt_renov_det').focus();
      	return false;
      }
      $.ajax({
        type: "POST",
        url: VISTA_URL + "resolver/renovar_cuotas_reg.php",
        dataType: "html",
        data: {
        	renov_det: renov_det,
        	cre_id: $('#hdd_cre_id').val(),
        	cre_tip: $('#hdd_cre_tip').val()
        },
        beforeSend: function() {
          //$('#msj_credito_renovar').html("Guardando datos...");
          //$('#msj_credito_renovar').show(100);
        },
        success: function(data){
          if(data == 'exito'){
          	//$('#msj_credito_renovar').show(100);
            //$('#msj_credito_renovar').text('El número máximo de cuotas ha sido renovado');
            alerta_success("EXITO", "El número máximo de cuotas ha sido renovado");
            $('#txt_renov_det').val("");
            $('#modal_renovar_cuotas').modal('hide');
          }
          else
            console.log(data);
        },
        complete: function(data){
          console.log(data);
          if(data.statusText == "parsererror"){
            console.log(data);
            $('#msj_credito_renovar').text(data.responseText);
          }
        }
      });
    });
});