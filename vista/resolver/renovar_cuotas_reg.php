<?php
session_start();
require_once ("../../core/usuario_sesion.php");
require_once("../lineatiempo/Lineatiempo.class.php");
$oLineaTiempo = new Lineatiempo();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();

$usuario_id = intval($_SESSION['usuario_id']);

$cre_id = $_POST['cre_id'];
$cre_tip = $_POST['cre_tip']; //3 credito garveh, 4 credito hipo
$renov_det = $_POST['renov_det'];

$credito_nom = ''; $cre_cod = '';

if($cre_tip == 3){
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
  $credito_nom = 'Crédito GARVEH';
	$cre_cod = 'CGV';
  $creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
}
if($cre_tip == 4){
  require_once ("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
  $credito_nom = 'Crédito HIPOTECARIO';
	$cre_cod = 'CH';
  $creditotipo_id = 4; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
}

$result = $oCredito->mostrarUno($cre_id);
if($result['estado']==1){
    $cre_maxcuo = intval($result['data']['tb_credito_numcuomax']);
}
$result = "";

$num_renov = $cre_maxcuo + 12;
//registramos el historial para el credito guardado
$his = '<span>Se renovó las cuotas de este crédito por un año más, con el siguiente detalle: '.$renov_det.'. La renovación fue realizada por: <b>'.$_SESSION['usuario_nombre'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
//$oCredito->registro_historial($cre_id, $his);
$oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);

$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha renovado las cuotas  del '.$credito_nom.' de número: '.$cre_cod.'-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
$oLineaTiempo->tb_lineatiempo_det = $line_det;
$oLineaTiempo->insertar();

$oCredito->modificar_campo($cre_id, 'tb_credito_numcuomax', $num_renov, 'INT');
//guardamos la fecha de renovación
$fecha_hoy = date('Y-m-d');
$oCredito->modificar_campo($cre_id, 'tb_credito_fecrenov', $fecha_hoy,'STR');

echo 'exito';

?>