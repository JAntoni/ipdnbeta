<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();

 	$action = $_POST['action'];
        $transferente_doc = mb_strtoupper($_POST['txt_transferente_doc'], 'UTF-8');
        $transferente_nom = mb_strtoupper($_POST['txt_transferente_nom'], 'UTF-8');
        $transferente_dir = mb_strtoupper($_POST['txt_transferente_dir'], 'UTF-8');
        $transferente_cel = $_POST['txt_transferente_cel'];
        
        $oTransferente->setTransferente_doc($transferente_doc);
        $oTransferente->setTransferente_nom($transferente_nom);
        $oTransferente->setTransferente_dir($transferente_dir);
        $oTransferente->setTransferente_cel($transferente_cel);
      
 	if($action == 'insertar'){ 

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Transferente.';
                $result = $oTransferente->insertar();
 		if($result['estado']==1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Transferente registrado correctamente.';
 			$data['transferente_doc'] = $transferente_doc;
 			$data['transferente_nombre'] = $transferente_nom;
 			$data['transferente_id'] = $result['transferente_id'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$transferente_id = intval($_POST['hdd_transferente_id']);
 		$oTransferente->setTransferente_id($transferente_id);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Transferente.';

 		if($oTransferente->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Transferente modificado correctamente. '.$transferente_des;
                        $data['transferente_doc'] = $transferente_doc;
 			$data['transferente_nombre'] = $transferente_nom;
 			$data['transferente_id'] = $transferente_id;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$transferente_id = intval($_POST['hdd_transferente_id']);
                
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Transferente.';

 		if($oTransferente->eliminar($transferente_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Transferente eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}
        
        elseif ($action == 'buscar') {
            $transferente_doc = $_POST['doc'];
            $result = $oTransferente->mostrarPorDoc($transferente_doc);
            $data['estado'] = 0;
            $data['mensaje'] = 'No se encuentra un cliente con ese documento.';
            if($result['estado']){
                $id_transferente = $result['data']['tb_transferente_id'];
                $data['estado'] = 1;
                $data['id_transferente'] = $id_transferente;
                $data['mensaje'] = 'Transferente encontrado correctamente. ' . $id_transferente;
            }
            echo json_encode($data);
        }

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>