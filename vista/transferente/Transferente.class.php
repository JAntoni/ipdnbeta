<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Transferente extends Conexion{
    private $transferente_id;
    private $transferente_doc;
    private $transferente_nom;
    private $transferente_cel;
    private $transferente_dir;
    
    public function getTransferente_id() {
        return $this->transferente_id;
    }
    
    public function setTransferente_id($transferente_id){
        $this->transferente_id = $transferente_id;
    }
    
    public function getTransferente_doc() {
        return $this->transferente_doc;
    }
    
    public function setTransferente_doc($transferente_doc){
        $this->transferente_doc = $transferente_doc;
    }
    
    public function getTransferente_nom() {
        return $this->transferente_nom;
    }
    
    public function setTransferente_nom($transferente_nom){
        $this->transferente_nom = $transferente_nom;
    }
    
    public function getTransferente_cel() {
        return $this->transferente_cel;
    }
    
    public function setTransferente_cel($transferente_cel){
        $this->transferente_cel = $transferente_cel;
    }
    
    public function getTransferente_dir() {
        return $this->transferente_dir;
    }
    
    public function setTransferente_dir($transferente_dir){
        $this->transferente_dir = $transferente_dir;
    }
    
    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_transferente(
                tb_transferente_doc, tb_transferente_nom, 
                tb_transferente_cel, tb_transferente_dir)
                VALUES (
                :transferente_doc,:transferente_nom, 
                :transferente_cel, :transferente_dir)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":transferente_doc", $this->getTransferente_doc(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_nom", $this->getTransferente_nom(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_cel", $this->getTransferente_cel(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_dir", $this->getTransferente_dir(), PDO::PARAM_STR);

        $result = $sentencia->execute();
        $transferente_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['transferente_id'] = $transferente_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_transferente SET 
                tb_transferente_nom =:transferente_nom, 
                tb_transferente_doc =:transferente_doc, 
                tb_transferente_dir =:transferente_dir, 
                tb_transferente_cel =:transferente_cel 
                WHERE tb_transferente_id =:transferente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":transferente_nom", $this->getTransferente_nom(), PDO::PARAM_INT);
        $sentencia->bindParam(":transferente_doc", $this->getTransferente_doc(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_dir", $this->getTransferente_dir(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_cel", $this->getTransferente_cel(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_id", $this->getTransferente_id(), PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($transferente_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_transferente SET tb_transferente_xac=0 WHERE tb_transferente_id =:transferente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":transferente_id", $transferente_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($transferente){
      try {
        $sql = "SELECT * FROM tb_transferente WHERE tb_transferente_id=:transferente";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":transferente", $transferente, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarPorDoc($transferente_doc){
      try {
        $sql = "SELECT * FROM tb_transferente WHERE tb_transferente_doc=:transferente_doc";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":transferente_doc", $transferente_doc, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos(){
      try {
        $sql = "SELECT * FROM tb_transferente WHERE tb_transferente_xac=1 ORDER BY tb_transferente_nom";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    function transferente_autocomplete($dato, $transferente_emp){
      try {

        $filtro = "%".$dato."%";
        $transferente_empresa = '';
        if(intval($transferente_emp) == 1)
          $transferente_empresa = 'AND tb_transferente_emp = 1';

        $sql = "SELECT * FROM tb_transferente WHERE (tb_transferente_nom LIKE :filtro OR tb_transferente_doc LIKE :filtro) GROUP BY CONCAT(tb_transferente_nom) LIMIT 0,12";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay transferentes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
