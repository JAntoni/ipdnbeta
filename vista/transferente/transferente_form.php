<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'transferente';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $transferente_id = $_POST['transferente_id'];
  $vista = $_POST['vista'];

  $titulo = '';
    if ($usuario_action == 'L') {
    $titulo = 'Transferente Registrado';
}
if ($usuario_action == 'I') {
    $titulo = 'Registrar Transferente';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Transferente';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Transferente';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en transferente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'transferente'; $modulo_id = $transferente_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del transferente por su ID
    if(intval($transferente_id) > 0){
      $result = $oTransferente->mostrarUno($transferente_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el transferente seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $transferente_doc = $result['data']['tb_transferente_doc'];
          $transferente_nom = $result['data']['tb_transferente_nom'];
          $transferente_cel = $result['data']['tb_transferente_cel'];
          $transferente_dir = $result['data']['tb_transferente_dir'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_transferente" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_transferente" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_transferente_id" value="<?php echo $transferente_id;?>">
          <input type="hidden" id="transferente_vista" value="<?php echo $vista;?>">          
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_transferente_doc" class="control-label">RUC/DNI :</label>
                                      <input type="text" name="txt_transferente_doc" id="txt_transferente_doc" class="form-control input-sm mayus" value="<?php echo $transferente_doc;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_transferente_nom" class="control-label">Transferente :</label>
                                      <input type="text" name="txt_transferente_nom" id="txt_transferente_nom" class="form-control input-sm" value="<?php echo $transferente_nom;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_transferente_cel" class="control-label">Celular :</label>
                                      <input type="text" name="txt_transferente_cel" id="txt_transferente_cel" class="form-control input-sm mayus" value="<?php echo $transferente_cel;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="txt_transferente_dir" class="control-label">Dirección :</label>
                                      <input type="text" name="txt_transferente_dir" id="txt_transferente_dir" class="form-control input-sm" value="<?php echo $transferente_dir;?>">
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="transferente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_transferente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_transferente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_transferente">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/transferente/transferente_form.js';?>"></script>
