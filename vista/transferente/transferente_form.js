
$(document).ready(function(){
  $('#form_transferente').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"transferente/transferente_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_transferente").serialize(),
				beforeSend: function() {
					$('#transferente_mensaje').show(400);
					$('#btn_guardar_transferente').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#transferente_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#transferente_mensaje').html(data.mensaje);
                        var vista = $('#transferente_vista').val();
                        if (vista == 'transferente')
                            transferente_tabla();
                        if (vista == 'creditogarveh') {
                            llenar_campos_transferente(data);
                        }
		      	setTimeout(function(){ 
		      		//transferente_tabla();
		      		$('#modal_registro_transferente').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#transferente_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#transferente_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_transferente').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#transferente_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#transferente_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_transferente_doc: {
				required: true,
				minlength: 8
			},
			txt_transferente_nom: {
				required: true,
				minlength: 2
			},
			txt_transferente_dir: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_transferente_doc: {
				required: "Ingrese un documento para el Transferente",
				minlength: "Como mínimo el documento debe tener 8 caracteres"
			},
			txt_transferente_nom: {
				required: "Ingrese un nombre para el Transferente",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_transferente_dir: {
				required: "Ingrese una dirección para el Transferente",
				minlength: "El celular debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
