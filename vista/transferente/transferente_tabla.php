<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Transferente.class.php');
  $oTransferente = new Transferente();

  $result = $oTransferente->mostrarTodos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr class="filaTabla">';
        $tr.='
          <td  id="fila" style="border-left: 1px solid #135896;">'.$value['tb_transferente_id'].'</td>
          <td  id="fila">'.$value['tb_transferente_nom'].'</td>
          <td  id="fila">'.$value['tb_transferente_doc'].'</td>
          <td  id="fila">'.$value['tb_transferente_cel'].'</td>
          <td  id="fila">'.$value['tb_transferente_dir'].'</td>
          <td align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="transferente_form(\'L\','.$value['tb_transferente_id'].')"><i class="fa fa-eye"></i> Ver</a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="transferente_form(\'M\','.$value['tb_transferente_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="transferente_form(\'E\','.$value['tb_transferente_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_transferentes" class="table table-bordered table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th id="filacabecera">ID</th>
      <th id="filacabecera">Nombre</th>
      <th id="filacabecera">Documento</th>
      <th id="filacabecera">Telefono</th>
      <th id="filacabecera">Direccion</th>
      <th id="filacabecera">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
