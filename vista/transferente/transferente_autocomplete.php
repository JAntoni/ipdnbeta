<?php
  require_once ("Transferente.class.php");
  $oTransferente = new Transferente();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $transferente_id;
    var $transferente_nom;
    var $transferente_doc;


    function __construct($label, $value, $transferente_id, $transferente_nom, $transferente_doc){ 
      $this->label = $label;
      $this->value = $value;
      $this->transferente_id = $transferente_id;
      $this->transferente_nom = $transferente_nom;	
      $this->transferente_doc = $transferente_doc; 
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $transferente_emp = $_GET['transferente_emp']; // si es 1, solo mostrar los transferentes de la empresa, si es 0: todos los transferentes

  //busco un valor aproximado al dato escrito
  $result = $oTransferente->transferente_autocomplete($datoBuscar, $transferente_emp);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    foreach ($result['data'] as $key => $value) {
      array_push(
        $arrayElementos, 
        new ElementoAutocompletar(
                $value["tb_transferente_doc"].'-'.$value["tb_transferente_nom"],
                $value["tb_transferente_doc"].'-'.$value["tb_transferente_nom"], 
                $value["tb_transferente_id"], 
                $value["tb_transferente_nom"], 
                $value['tb_transferente_doc'])
      );
    }

    print_r(json_encode($arrayElementos));
  $result = NULL;
?>