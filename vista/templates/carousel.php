<?php
	session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'garantiafile/Garantiafile.class.php');
    require_once(VISTA_URL.'creditogarvehfile/Creditogarvehfile.class.php');
    require_once(VISTA_URL.'garantiafilecredito/Garantiafilecredito.class.php');
  }
  else{
    require_once('../garantiafile/Garantiafile.class.php');
    require_once('../creditogarvehfile/Creditogarvehfile.class.php');
    require_once('../garantiafilecredito/Garantiafilecredito.class.php');
  }
  
  $modulo_nom = $_POST['modulo_nom'];
  $modulo_id = $_POST['modulo_id'];
  $comentario_id = $_POST['comentario_id'];
  $cont = 0;
  $result = null;

  if($modulo_nom == 'garantiafile'){
  	$oGarantiafile = new Garantiafile();
  	$result = $oGarantiafile->listar_garantiafiles($modulo_id);
  	$img_src = 'tb_garantiafile_url';
  }
  elseif($modulo_nom == 'garantiafilecredito'){
    $oGarantiafile = new Garantiafilecredito();
  	$result = $oGarantiafile->listar_garantiafilecreditos($modulo_id);
    $img_src = 'tb_garantiafilecredito_url';
  }
  elseif($modulo_nom == 'creditogarvehfile'){
    $oCreditogarvehfile = new Creditogarvehfile();
  	$result = $oCreditogarvehfile->filtrar($modulo_id);
  }
  elseif($modulo_nom == 'garantia_fijarprecio'){
    require_once('../upload/Upload.class.php');
    $oUpload = new Upload();
    $result = $oUpload->listar_uploads('garantia_fijarprecio', $modulo_id);
    $img_src = 'upload_url';
  }
  elseif($modulo_nom == 'proc_proceso_fase_comentario'){
    require_once('../proceso/Proceso.class.php');
    $oProceso = new Proceso();
    $result = $oProceso->listar_comentarios_fotos($modulo_id, $comentario_id);
    $img_src = 'upload_url';
  }
  elseif($modulo_nom == 'ejecucion') {
    require_once '../filestorage/Filestorage.class.php';
    $oFilestorage = new Filestorage();
    $tabla_id2 = $_POST['tabla_id2'];
    $modulo_subnom_orig = $_POST['modulo_subnom'];
    $modulo_subnom = $modulo_subnom_orig == 'comentario_galeria' ? $modulo_subnom_orig.$tabla_id2 : $modulo_subnom_orig;
    $result = $oFilestorage->listar_files_modulo_submodulo($modulo_nom, $modulo_id, $modulo_subnom);
    $img_src = 'filestorage_url';
  }
  else
  	$result['estado'] = 0;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_carousel_galeria">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Galería de Imágenes</h4>
      </div>
      <div class="modal-body">
      	<?php 
      	if($result['estado'] == 1){ ?>
	        <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
						<ol class="carousel-indicators">
							<?php 
	    					foreach ($result['data'] as $key => $value): ?>
									<li data-target="#carousel-example-generic" data-slide-to="<?php echo $cont;?>" <?php if($cont == 0) echo 'class="active"';?> ></li>
									<?php
									$cont ++;
								endforeach;
							?>
						</ol>
						<div class="carousel-inner">
							<?php 
								$cont = 0;
	    					foreach ($result['data'] as $key => $value):                         
                  if($modulo_nom == 'creditogarvehfile'){
                    $servidor = $value['tb_creditogarvehfile_url'];
                    // if($value['tb_creditogarvehfile_id'] <= 2733){
                    //     $url = ucfirst(mb_substr($value['tb_creditogarvehfile_url'],6,null, 'UTF-8')) ;
                    //     $url = strtolower($url);
                    //     $servidor = 'https://www.ipdnsac.com/app/'.$url;
                    // }
                  } ?>
									<div <?php ($cont == 0)? print 'class="item active"' : print 'class="item"';?> >
                    <?php if($modulo_nom == 'garantiafile' || $modulo_nom == 'garantiafilecredito' || $modulo_nom == 'garantia_fijarprecio' || $modulo_nom == 'ejecucion' || $modulo_nom == 'proc_proceso_fase_comentario'): ?>
										  <img src="<?php echo $value[$img_src];?>">
                    <?php endif; ?>

                    <?php if($modulo_nom == 'creditogarvehfile'):?>
                      <img src="<?php echo $servidor;?>">
                    <?php endif; ?>

										<div class="carousel-caption"><?php echo 'Foto '.$cont;?></div>
									</div>
									<?php
									$cont ++;
								endforeach;
							?>
							<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								<span class="fa fa-angle-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								<span class="fa fa-angle-right"></span>
							</a>
						</div>
					</div>
					<?php 
				}
				else
					echo '<h4>NO SE HAN ENCONTRADO FOTOS O NO EXISTE EL MODULO: '.$modulo_nom.' MODULO ID: '.$modulo_id.'</h4>';
				?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
			