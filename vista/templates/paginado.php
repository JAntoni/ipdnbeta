<?php 
function getFooter($pagina=1, $total_pag=1, $total_datos=1, $cantidad=12, $inicio=1, $fin=1,$sufijo=''){
	$previo=$pagina-1;
	$next=$pagina+1;
	if($pagina==1){$previo=1;}
	if($total_pag==$pagina){$next=$pagina;}
	if($total_datos==0){$inicio=0;$total_pag=1; $next=1;$previo=1;}
		$footer="<ul class='pagination pagination-sm no-margin pull-right'>
                    <li><a onClick=\"verPagina2".$sufijo."('1')\" href='javascript:void(0)'><i class='fa fa-angle-double-left'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina2".$sufijo."('".$previo."')\" href='javascript:void(0)'><i class='fa  fa-angle-left'></i>&nbsp;</a></li>
                    <li><a href='javascript:void(0)' style='padding: 4.7px 10px;'>Pág. <input id='txtNroPaginaFooter".$sufijo."' type='text' value='".$pagina."' class='form-control' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina2".$sufijo."(this.value);}\" size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;'  > de ".$total_pag."</a></li>
                    <li><a onClick=\"verPagina2".$sufijo."('".$next."')\" href='javascript:void(0)'><i class='fa  fa-angle-right'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina2".$sufijo."('".$total_pag."')\" href='javascript:void(0)'><i class='fa fa-angle-double-right'></i>&nbsp;</a></li>
					<li><a href='javascript:void(0)' style='padding: 4.7px 10px;'><input value='".$cantidad."' name='cboCantidadBusqueda".$sufijo."' id='cboCantidadBusqueda".$sufijo."' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina2".$sufijo."('1');}\" type='text' class='form-control' size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;' > Filas</a></li>
					<li><a href='javascript:void(0)'>Mostrando ".$inicio." al ".$fin." de ".$total_datos." Filas</a></li>
				</ul>";
	return $footer;
}


function getFooterCase($pagina=1, $total_pag=1, $total_datos=1, $cantidad=12, $inicio=1, $fin=1,$sufijo=''){
	$previo=$pagina-1;
	$next=$pagina+1;
	if($pagina==1){$previo=1;}
	if($total_pag==$pagina){$next=$pagina;}
	if($total_datos==0){$inicio=0;$total_pag=1; $next=1;$previo=1;}
		$footer="<ul class='pagination pagination-sm no-margin pull-right'>
                    <li><a onClick=\"verPagina".$sufijo."('1')\" href='javascript:void(0)'><i class='fa fa-angle-double-left'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina".$sufijo."('".$previo."')\" href='javascript:void(0)'><i class='fa  fa-angle-left'></i>&nbsp;</a></li>
                    <li><a href='javascript:void(0)' style='padding: 4.7px 10px;'>Pág. <input id='txtNroPaginaFooter".$sufijo."' type='text' value='".$pagina."' class='form-control' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina".$sufijo."(this.value);}\" size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;'  > de ".$total_pag."</a></li>
                    <li><a onClick=\"verPagina".$sufijo."('".$next."')\" href='javascript:void(0)'><i class='fa  fa-angle-right'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina".$sufijo."('".$total_pag."')\" href='javascript:void(0)'><i class='fa fa-angle-double-right'></i>&nbsp;</a></li>
					<li><a href='javascript:void(0)' style='padding: 4.7px 10px;'><input value='".$cantidad."' name='cboCantidadBusqueda".$sufijo."' id='cboCantidadBusqueda".$sufijo."' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina".$sufijo."('1');}\" type='text' class='form-control' size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;' > Filas</a></li>
					<li><a href='javascript:void(0)'>Mostrando ".$inicio." al ".$fin." de ".$total_datos." Filas</a></li>
				</ul>";
	return $footer;
}



function getFooterLibro($pagina=1, $total_pag=1, $total_datos=1, $cantidad=12, $inicio=1, $fin=1,$sufijo=''){
	$previo=$pagina-1;
	$next=$pagina+1;
	if($pagina==1){$previo=1;}
	if($total_pag==$pagina){$next=$pagina;}
	if($total_datos==0){$inicio=0;$total_pag=1; $next=1;$previo=1;}
		$footer="<ul class='pagination pagination-sm no-margin pull-right'>
                    <li><a onClick=\"verPagina2".$sufijo."('1')\" href='javascript:void(0)'><i class='fa fa-angle-double-left'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina2".$sufijo."('".$previo."')\" href='javascript:void(0)'><i class='fa  fa-angle-left'></i>&nbsp;</a></li>
                    <li><a href='javascript:void(0)' style='padding: 4.7px 10px;'>Pág. <input id='txtNroPaginaFooter".$sufijo."' type='text' value='".$pagina."' class='form-control' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina2".$sufijo."(this.value);}\" size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;'  > de ".$total_pag."</a></li>
                    <li><a onClick=\"verPagina2".$sufijo."('".$next."')\" href='javascript:void(0)'><i class='fa  fa-angle-right'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina2".$sufijo."('".$total_pag."')\" href='javascript:void(0)'><i class='fa fa-angle-double-right'></i>&nbsp;</a></li>
					<li><a href='javascript:void(0)' style='padding: 4.7px 10px;'><input value='".$cantidad."' name='cboCantidadBusqueda".$sufijo."' id='cboCantidadBusqueda".$sufijo."' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina2".$sufijo."('1');}\" type='text' class='form-control' size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;' > Libros</a></li>
					<li><a href='javascript:void(0)'>Mostrando ".$inicio." al ".$fin." de ".$total_datos." Libros</a></li>
				</ul>";
	return $footer;
}

function getFooterCaseUpper($pagina=1, $total_pag=1, $total_datos=1, $cantidad=12, $inicio=1, $fin=1,$sufijo=''){
	$previo=$pagina-1;
	$next=$pagina+1;
	if($pagina==1){$previo=1;}
	if($total_pag==$pagina){$next=$pagina;}
	if($total_datos==0){$inicio=0;$total_pag=1; $next=1;$previo=1;}
		$footer="<ul class='pagination pagination-sm no-margin pull-right'>
                    <li><a onClick=\"verPagina".$sufijo."('1')\" href='javascript:void(0)'><i class='fa fa-angle-double-left'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina".$sufijo."('".$previo."')\" href='javascript:void(0)'><i class='fa  fa-angle-left'></i>&nbsp;</a></li>
                    <li><a href='javascript:void(0)' style='padding: 4.7px 10px;'>Pág. <input id='txtNroPaginaFooter".$sufijo."' type='text' value='".$pagina."' class='form-control' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina".$sufijo."(this.value);}\" size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;'  > de ".$total_pag."</a></li>
                    <li><a onClick=\"verPagina".$sufijo."('".$next."')\" href='javascript:void(0)'><i class='fa  fa-angle-right'></i>&nbsp;</a></li>
                    <li><a onClick=\"verPagina".$sufijo."('".$total_pag."')\" href='javascript:void(0)'><i class='fa fa-angle-double-right'></i>&nbsp;</a></li>
					<li><a href='javascript:void(0)' style='padding: 4.7px 10px;'><input value='".$cantidad."' name='cboCantidadBusqueda".$sufijo."' id='cboCantidadBusqueda".$sufijo."' onkeypress=\"return solo_numero(event)\" onKeyUp=\"if(event.keyCode=='13'){verPagina".$sufijo."('1');}\" type='text' class='form-control' size='1' style='height:18px; text-align:center; display: initial;width: 30px;line-height: normal; padding: 0px;' > Filas</a></li>
				</ul>";
	return $footer;
}


?>