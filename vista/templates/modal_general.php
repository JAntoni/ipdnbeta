<div class="modal fade" id="modal_general" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" style="display: none;">
  <div class="modal-dialog" role="document">                       
	  <div class="modal-content">
		  <div class="modal-header">
		    <h4 class="modal-title" id="general_title">Validando Datos</h4>
		  </div>
		  <div class="modal-body" id="body_modal_general">
		  	Por favor espere unos segundos...
		  </div>
		  <div class="modal-footer">
		  	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		  </div>
	  </div>
  </div>
</div>