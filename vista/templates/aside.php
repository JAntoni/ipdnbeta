<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style="box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.1)">
<!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo $_SESSION['usuario_fot'];?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $_SESSION['usuario_nom']?></p>
        <!--<a><i class="fa fa-circle text-success"></i> En linea</a>-->
        <a><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['empresa_nombre']?></a>
      </div>
    </div>
    <ul class="sidebar-menu">
      <li class="header">Menu de navegación</li>
      <?php
        $menu = '';
        require_once(VISTA_URL.'perfilmenu/PerfilMenu.class.php');
        $oPerfilMenu = new PerfilMenu();

        $result = $oPerfilMenu->retorna_menu_perfilmenu($_SESSION['usuarioperfil_id'], 0);

        if($result['estado'] == 1){
          foreach ($result['data'] as $key => $value) {
            $icono = 'fa-home';
            if(!empty($value['tb_menu_ico']))
              $icono = $value['tb_menu_ico'];
            $menu.= '
              <li class="treeview">
                <a href="'.$value['tb_menu_dir'].'" id="'.$value['tb_menu_dir'].'"><i class="fa fa-fw '.$icono.'"></i><span>'.$value['tb_menu_tit'].'</span>';
                  if($value['cant_submenu'] > 0){
                    $menu .='
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>';
                  }
                $menu.='</a>';

                if($value['cant_submenu'] > 0){
                  $menu .='<ul class="treeview-menu">';

                  $result2 = $oPerfilMenu->retorna_menu_perfilmenu($_SESSION['usuarioperfil_id'], $value['tb_menu_id']);

                  foreach ($result2['data'] as $key => $value) {
                    $icono = 'fa-circle-o';
                    if(!empty($value['tb_menu_ico']))
                      $icono = $value['tb_menu_ico'];
                    $menu .='
                      <li>
                        <a href="'.$value['tb_menu_dir'].'" id="'.$value['tb_menu_dir'].'"><i class="fa fa-fw '.$icono.'"></i>'.$value['tb_menu_tit'];
                        if($value['cant_submenu'] > 0){
                          $menu .='
                          <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>';
                        }
                        $menu.='</a>';

                        if($value['cant_submenu'] > 0){
                          $menu .='<ul class="treeview-menu">';

                          $result3 = $oPerfilMenu->retorna_menu_perfilmenu($_SESSION['usuarioperfil_id'], $value['tb_menu_id']);

                          foreach ($result3['data'] as $key => $value) {
                            $icono = 'fa-circle-o';
                            if(!empty($value['tb_menu_ico']))
                              $icono = $value['tb_menu_ico'];
                            if($value['tb_menu_url'] != "#"){
                              $menu .='
                                <li>
                                  <a href="'.$value['tb_menu_dir'].'" id="'.$value['tb_menu_dir'].'"><i class="fa fa-fw '.$icono.'"></i>'.$value['tb_menu_tit'].'
                                  </a>
                                </li>';
                            }
                          }
                          $result3 = NULL;
                          $menu .='</ul>';
                        }
                      $menu .='</li>';
                  }
                  $result2 = NULL;
                  $menu .='</ul>';
                }
              $menu .='</li>';
          }
          $result = NULL; // PARA LIBEAR MEMORIA DEL ARRAY
          echo $menu;
        }
        else {
          echo '
            <li class="treeview">
              <a href="javascript:void(0)">
                <i class="fa fa-share"></i><span>USUARIO SIN MENU</span>
              </a>
            </li>';
        }
      ?>
    </ul>
  </section>
</aside>
