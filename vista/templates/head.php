<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="static/css/bootstrap/bootstrap.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="static/css/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="static/css/ionicons.min.css">

<link rel="stylesheet" href="static/css/bootstrap/dataTables.bootstrap.css">
<!-- link rel="stylesheet" href="static/css/jquery/jquery.dataTables_1.13.5.min.css" -->
<link rel="stylesheet" href="static/css/jquery/fixedColumns.dataTables_4.3.0.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="static/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="static/css/_all-skins.min.css">
<link rel="stylesheet" href="static/css/bootstrap/bootstrap-select.css">

<link rel="stylesheet" type="text/css" href="static/css/jquery/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="static/css/icheck/flat/_all.css">

<link rel="stylesheet" href="public/css/generales.css?ver=22855414455222">

<link href="static/css/waves.css" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="public/images/logopres.ico" />
<link rel="stylesheet" href="static/css/AdminLTE.custom.css" >
<!-- Animation Css -->
<link href="static/css/sweetalert/animate.css" rel="stylesheet" />
<!-- Sweetalert Css -->
<link href="static/css/sweetalert/sweetalert.css" rel="stylesheet" />
<link href="static/css/sweetalert/sweetalert_animation.css" rel="stylesheet" />
<!-- Sweetalert 2 Css -->
<link rel="stylesheet" href="static/css/sweetalert/sweetalert2.min.css">

<link rel="stylesheet" type="text/css" href="static/plugins/datepicker/bootstrap-datepicker3.min.css">
<link rel="stylesheet" type="text/css" href="static/plugins/timepicker/bootstrap-timepicker.css">
<!-- ALERT-->
<link rel="stylesheet" type="text/css" href="static/plugins/jquery-confirm/jquery-confirm.min.css">
<!-- Toastr -->
<link rel="stylesheet" href="static/plugins/toastr/toastr.min.css">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<!--link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css" -->