<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="public/images/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $_SESSION['usuario_nom']?></p>
        <a><i class="fa fa-circle text-success"></i> En linea</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Buscar...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">Menu de navegación</li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Mantenimiento</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">

          <li>
            <a href="#"><i class="fa fa-money"></i> Creditos
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="pages/tables/tipo_credito.html"><i class="fa fa-circle-o"></i>Tipo de Créditos</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Tipo de Cuota</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Periodo de Cuotas</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Subperiodo de Cuotas</a></li>

              <li class="divider"></li>

              <li><a href="#"><i class="fa fa-circle-o"></i>Tipo de Garantía</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Representante</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Cuentas para pagos</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Fórmulas de Ventas</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Nivel 3
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>


          <li>
            <a href="#"><i class="fa fa-car"></i> Vehículos
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Clases</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Tipo</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Marca</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Modelos</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Adicionales</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Tipo de Proveedor</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Tipo de Cuota
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>


          <li>
            <a href="#"><i class="fa fa-car"></i> Operaciones
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Clientes</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Proveedores</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Tipo de Cuota
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li>
            <a href="#"><i class="fa fa-share"></i> Caja
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Caja</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Monedas</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Tipo de cambio</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Cuentas Sub Cuentas</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Impresora</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Tipo de Cuota
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li>
            <a href="#"><i class="fa fa-users"></i> Usuarios
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Grupos</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Perfiles</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Menú</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Perfil Menú</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Áreas</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Cargos</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Tipo de Cuota
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>


          <li>
            <a href="#"><i class="fa fa-users"></i> Configuración
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Empresa</a></li>
            </ul>
          </li>

          <li>
            <a href="#"><i class="fa fa-home"></i> General
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Ubigeo</a></li>
            </ul>
          </li>

          <li>
            <a href="#"><i class="fa fa-home"></i> Almacén
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Almacén</a></li>
            </ul>
          </li>


        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-money"></i> <span>Créditos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Créditos - Menores</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Créditos - Asistencia Vehicular</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Créditos - Garantía Vehicular</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Créditos - Hipotecario</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-envelope-o"></i> <span>Operaciones</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Pagos de Cuotas</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Pagos de Mora</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Gestión de Cobranza</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Crédito Menor - Garantías</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-file-code-o"></i> <span>Caja</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Consulta de Caja</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Ingresos</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Egresos</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Camnbio de Moneda</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Apertura y Cierre de Caja</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-file-code-o"></i> <span>Cobranza</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Gestión de Cobranza</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Mis Tareas de Cobranza</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Tareas de Cobranza Asignada</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-gears"></i> <span>Inventario</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Garantías de Oficina</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Almacén</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-file-code-o"></i> <span>Opciones</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Modificar mis datos</a></li>
        </ul>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
