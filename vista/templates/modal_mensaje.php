
<div class="modal fade bs-example-modal-sm" id="modal_mensaje" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm" style="margin-top: 20%;">                       
	  <div class="box box-info box-solid" id="box_modal_mensaje">
		  <div class="box-header">
		    <h3 class="box-title" id="h3_modal_title">Validando Datos</h3>
		  </div>
		  <div class="box-body" id="body_modal_mensaje">
		    Por favor espere unos segundos...
		  </div>
		  <div class="overlay" id="overlay_modal_mensaje">
		    <i class="fa fa-refresh fa-spin"></i>
		  </div>
	  </div>
  </div>
</div>