<!-- jQuery 2.2.3 -->
<script src="static/js/jquery/jquery-3.7.0.js"></script>
<!-- jQuery UI 1.11.4 -->
<!--script src="static/js/jquery/jquery-ui.min.js"></script-->
<script src="static/js/jquery/jquery-ui.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="static/js/bootstrap/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="static/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<script src="static/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="static/js/demo.js"></script>
<script src="static/js/jquery/jquery.dataTables_1.13.5.min.js"></script>
															
<script src="static/js/jquery/dataTables.fixedColumns_4.3.0.min.js"></script>

<script src="static/js/jquery/accent-neutralise.js"></script>
<script src="static/js/bootstrap/dataTables.bootstrap.min.js"></script>

<script src="static/js/bootstrap/bootstrap-select.js"></script>
<script src="static/js/jquery/jquery.validate.min.js"></script>
<script src="static/js/jquery/additional-methods.min.js"></script>
<script src="static/js/jquery/autoNumeric.js"></script>
<script type="text/javascript" src="static/js/jquery/jquery.mask.min.js"></script>

<script src="static/js/sweetalert/sweetalert2.all.min.js"></script>
<!-- <script src="static/js/sweetalert2/sweetalert2.min.js"></script> -->
<script type="text/javascript" src="static/js/waves.js"></script>
<script src="static/js/sweetalert/dialogs.js"></script>
<script type="text/javascript" src="static/js/icheck/icheck.js"></script>

<script type="text/javascript" src="static/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="static/plugins/timepicker/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="static/plugins/datepicker/bootstrap-datepicker.es.js"></script>
<!-- Toastr -->
<script src="static/plugins/toastr/toastr.min.js"></script>
<!-- ALERT CONFIRM-->
<script type="text/javascript" src="static/plugins/jquery-confirm/jquery-confirm.min.js"></script>

<script src="public/js/generales.js?ver=223347321"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.12.1/api/sum().js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="static/js/jspdf/html2canvas.js"></script>
<script type="text/javascript" src="static/js/jspdf/jspdf.min.js"></script>


<!--script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js
"></script-->
