<?php 
    require_once 'vista/monedacambio/Monedacambio.class.php';
    $oMonedacambio= new Monedacambio();
    $fec=date('Y-m-d');
    $result=$oMonedacambio->consultar($fec);
    
    $venta=0.00;
    $compra=0.00;
            
       if($result['estado'] == 1){
            $venta= ($result['data']['tb_monedacambio_val']);
            $compra=($result['data']['tb_monedacambio_com']);
       }     
    
?>
<header class="main-header" style="box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3);">
  <!-- Logo -->
  <a href="index2.html" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">   <img src="public/images/logopres1.png" class="img-responsive" style="height:50px; width: 50px">  </span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>IPDN</b>SGE</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="click">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        
        <li class="dropdown">
          <a href="monedacambio" target="_blank">
              <span class="hidden-xs" style="font-family: cambria;font-weight: bold">TIPO DE CAMBIO</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="hidden-xs" style="font-family: cambria;font-weight: bold">COMPRA : <?php echo $compra;?></span>
          </a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="hidden-xs" style="font-family: cambria;font-weight: bold">VENTA : <?php echo $venta;?></span>
          </a>
          <input type="hidden" id="hdd_tipo_cambio_venta" value="<?php echo $venta;?>"/>
        </li>
        
        
        <li class="dropdown messages-menu"><!-- comienza dropdown mensajes -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">4</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">Tienes 4 mensajes</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li><!-- comienza mensaje 1-->
                  <a href="#">
                    <div class="pull-left">
                      <img src="<?php echo $_SESSION['usuario_fot'];?>" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Equipo de soporte
                      <small><i class="fa fa-clock-o"></i> 5 mins</small>
                    </h4>
                    <p>Cuando actualizan su navegador?</p>
                  </a>
                </li>
                <!-- termina mensaje 1 -->

              </ul>
            </li>
            <li class="footer"><a href="#">Ver todos los mensajes</a></li>
          </ul>
        </li><!-- termina dropdown mensajes -->
        <!-- Notifications: style can be found in dropdown.less -->
        <li class="dropdown notifications-menu"><!-- comienza dropdown notifiaciones -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning">10</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">Tines 10 notificaciones</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li><!-- comienza li notifiacion 1 -->
                  <a href="#">
                    <i class="fa fa-users text-aqua"></i> 5 nuevos mienbros agregados
                  </a>
                </li>
               
              </ul>
            </li>
            <li class="footer"><a href="#">Ver todas</a></li>
          </ul>
        </li><!-- temrina dropdown notifiaciones -->
        <!-- Tasks: style can be found in dropdown.less -->
        <li class="dropdown tasks-menu"><!-- comienza dropdown tareas -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-flag-o"></i>
            <span class="label label-danger">9</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">Tienes 9 tareas</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li><!-- Task item -->
                  <a href="#">
                    <h3>
                      Design some buttons
                      <small class="pull-right">20%</small>
                    </h3>
                    <div class="progress xs">
                      <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                        <span class="sr-only">20% Complete</span>
                      </div>
                    </div>
                  </a>
                </li>
                <!-- end task item -->
                
              </ul>
            </li>
            <li class="footer">
              <a href="#">Ver todas las tareas</a>
            </li>
          </ul>
        </li><!-- termina dropdown tareas -->
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo $_SESSION['usuario_fot'];?>" class="user-image" alt="User Image">
            <span class="hidden-xs"><?php echo $_SESSION['usuario_nom'];?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo $_SESSION['usuario_fot'];?>" class="img-circle" alt="User Image">

              <p>
                Administrador - Web Developer
                <small>Miembro desde Nov. 2016</small>
              </p>
            </li>
            <!-- Menu Body -->
            <li class="user-body">
              <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Seguidores</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Ventas</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Amigos</a>
                </div>
              </div>
              <!-- /.row -->
            </li><!--Menu Body -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="perfil" class="btn btn-default btn-flat">Perfil</a>
              </div>
              <div class="pull-right">
                <a href="<?php echo VISTA_URL.'login/cerrar_sesion.php';?>" class="btn btn-default btn-flat">Finalizar</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Tab panes -->
  <div class="tab-content">
    <!-- Home tab content -->
    <div class="tab-pane" id="control-sidebar-home-tab">
    </div>
  </div>
</aside>
<!-- /.control-sidebar -->