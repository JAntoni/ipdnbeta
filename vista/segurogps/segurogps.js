function seguro_gps_form(usuario_act, seguro_gps_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"segurogps/segurogps_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: 'segurogps', // PUEDE SER: L, I, M , E
      seguro_gps_id: seguro_gps_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_seguro_gps_form').html(data);
      	$('#modal_registro_seguro_gps').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_seguro_gps'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_seguro_gps', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'seguro_gps';
      	var div = 'div_modal_seguro_gps_form';
      	permiso_solicitud(usuario_act, seguro_gps_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function seguro_gps_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"segurogps/segurogps_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#seguro_gps_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_seguro_gps_tabla').html(data);
      $('#seguro_gps_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#seguro_gps_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
$(document).ready(function() {
  console.log('loader de cseguro_gps');

  seguro_gps_tabla();
});