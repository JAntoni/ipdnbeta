<?php
	require_once('../../core/usuario_sesion.php');
 	require_once('../segurogps/Seguro_Gps.class.php');
  $oSeguro_Gps = new Seguro_Gps();

 	$action = $_POST['action'];

	$oSeguro_Gps->usuario_id = $_SESSION['usuario_id']; 
	$oSeguro_Gps->seguro_gps_tipo = $_POST['cmb_seguro_gps_tipo']; 
	$oSeguro_Gps->seguro_gps_costo = $_POST['txt_seguro_gps_costo'];
	$oSeguro_Gps->seguro_gps_porcentaje = $_POST['txt_seguro_gps_porcentaje'];
	$oSeguro_Gps->seguro_gps_precio = $_POST['txt_seguro_gps_precio'];
	$oSeguro_Gps->seguro_gps_meses = $_POST['txt_seguro_gps_meses'];

 	if($action == 'insertar'){

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar al Representante.';
 		if($oSeguro_Gps->insertar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Representante registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar al Representante.';

		$oSeguro_Gps->seguro_gps_id = intval($_POST['hdd_seguro_gps_id']);

 		if($oSeguro_Gps->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Representante modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$seguro_gps_id = intval($_POST['hdd_seguro_gps_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al Representante.';

 		if($oSeguro_Gps->eliminar($seguro_gps_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Representante eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}
	 elseif($action == 'valores'){
		$seguro_gps_meses = intval($_POST['numero_cuotas']);
		$valor_seguro = 0;
		$valor_gps = 0;
		$texto = '';
		if($seguro_gps_meses < 12)
			$seguro_gps_meses = 12;

		$data['estado'] = 0;
		$data['mensaje'] = 'No hay valores: Número de Cuotas: '.$seguro_gps_meses.' / post '.$_POST['numero_cuotas'];

		$result = $oSeguro_Gps->obtener_valor_adicional(1, $seguro_gps_meses); // obtenemos el valor del seguro, el porcentaje para aplicar a las cuotas
			if($result['estado'] == 1){
				$valor_seguro = $result['data']['tb_seguro_gps_porcentaje'];
				$texto .= '% de seguro es '.$valor_seguro.'%';
			}
		$result = NULL;

		$result = $oSeguro_Gps->obtener_valor_adicional(2, $seguro_gps_meses); // obtenemos el valor del GPS
			if($result['estado'] == 1){
				$valor_gps = $result['data']['tb_seguro_gps_precio'];
				$texto .= ' y el costo de GPS es $ '.$valor_gps;
			}
		$result = NULL;

		$data['valor_seguro'] = $valor_seguro;
		$data['valor_gps'] = $valor_gps;
		$data['texto'] = $texto;

		echo json_encode($data);
	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>