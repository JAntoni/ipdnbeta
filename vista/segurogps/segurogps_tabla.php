<?php
  require_once('../../core/usuario_sesion.php');
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'segurogps/Seguro_Gps.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
  }
  else{
    require_once('../segurogps/Seguro_Gps.class.php');
    require_once('../funciones/funciones.php');
  }
  $oSeguro_Gps = new Seguro_Gps();

  $tabla = '';
  $result = $oSeguro_Gps->listar_seguro_gps();
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value){
        $tipo = '<span class="badge bg-green">Póliza de Seguro</span>';
        if($value['tb_seguro_gps_tipo'] == 2)
          $tipo = '<span class="badge bg-aqua">GPS</span>';

        $tabla .='
          <tr>
            <td>'.$value['tb_seguro_gps_id'].'</td>
            <td>'.$tipo.'</td>
            <td>'.mostrar_moneda($value['tb_seguro_gps_costo']).'</td>
            <td>'.mostrar_moneda($value['tb_seguro_gps_porcentaje']).'</td>
            <td>'.mostrar_moneda($value['tb_seguro_gps_precio']).'</td>
            <td>'.$value['tb_seguro_gps_meses'].'</td>
            <td align="center">
              <a class="btn btn-warning btn-xs" title="Editar" onclick="seguro_gps_form(\'M\','.$value['tb_seguro_gps_id'].')"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="seguro_gps_form(\'E\', '.$value['tb_seguro_gps_id'].')"><i class="fa fa-trash"></i></a>
            </td>
          </tr>';
      }
    }
  $result = NULL;
?>
<table id="tbl_horario" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Tipo Adicional</th> 
      <th>Costo del Adicional</th>
      <th>Porcentaje Utilidad</th>
      <th>Precio al Cliente</th>
      <th>Meses a Financiar</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tabla;?>
  </tbody>
</table>
