<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../segurogps/Seguro_Gps.class.php');
  $oSeguro_Gps = new Seguro_Gps();
  require_once('../funciones/funciones.php');

  $direc = 'segurogps';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $seguro_gps_id = intval($_POST['seguro_gps_id']);
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Porcentajes para Seguro y GPS Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Porcentajes para Seguro y GPS';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Porcentajes para Seguro y GPS';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Porcentajes para Seguro y GPS';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en seguro_gps
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'segurogps'; $modulo_id = $seguro_gps_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    $style_solo_gps = '';
    //si la accion es modificar, mostramos los datos del seguro_gps por su ID
    if(intval($seguro_gps_id) > 0){
      $result = $oSeguro_Gps->mostrarUno($seguro_gps_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el seguro_gps seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $seguro_gps_id = $result['data']['tb_seguro_gps_id'];
          $usuario_id = $result['data']['tb_usuario_id'];
          $seguro_gps_tipo = $result['data']['tb_seguro_gps_tipo'];
          $seguro_gps_costo = $result['data']['tb_seguro_gps_costo'];
          $seguro_gps_porcentaje = $result['data']['tb_seguro_gps_porcentaje'];
          $seguro_gps_precio = $result['data']['tb_seguro_gps_precio'];
          $seguro_gps_meses = $result['data']['tb_seguro_gps_meses'];

          if($seguro_gps_tipo == 1)
            $style_solo_gps = 'style="display: none;"';
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_seguro_gps" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_seguro_gps" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_seguro_gps_id" value="<?php echo $seguro_gps_id;?>">
          <input type="hidden" name="hdd_vista2" id="hdd_vista2" value="<?php echo $vista;?>">
          
          <div class="modal-body">
            <div class="callout callout-success" id="seguro_gps_info" style="display: none;">
              <h4 id="h4_informacion"></h4>
            </div>

            <div class="form-group">
              <label for="cmb_seguro_gps_tipo" class="control-label">Tipo de Adicional</label>
              <select class="form-control input-sm mayus" name="cmb_seguro_gps_tipo" id="cmb_seguro_gps_tipo">
                <option value="">Selecciona tipo...</option>
                <option value="1" <?php if($seguro_gps_tipo == 1) echo 'selected';?> >Póliza Seguro</option>
                <option value="2" <?php if($seguro_gps_tipo == 2) echo 'selected';?> >GPS</option>
              </select>
            </div>
            <div class="form-group solo_gps" <?php echo $style_solo_gps;?> >
              <label for="txt_seguro_gps_costo" class="control-label">Costo del GPS a IPDN USD$</label>
              <input type="text" name="txt_seguro_gps_costo" id="txt_seguro_gps_costo" class="form-control input-sm moneda" value="<?php echo $seguro_gps_costo;?>">
            </div>
            <div class="form-group">
              <label for="txt_seguro_gps_meses" class="control-label">Meses a Financiar</label>
              <input type="text" name="txt_seguro_gps_meses" id="txt_seguro_gps_meses" class="form-control input-sm moneda" value="<?php echo $seguro_gps_meses;?>">
            </div>
            <div class="form-group">
              <label for="txt_seguro_gps_porcentaje" class="control-label">Porcentaje a Aplicar</label>
              <input type="text" name="txt_seguro_gps_porcentaje" id="txt_seguro_gps_porcentaje" class="form-control input-sm moneda" value="<?php echo $seguro_gps_porcentaje;?>">
            </div>
            <div class="form-group solo_gps" <?php echo $style_solo_gps;?> >
              <label for="txt_seguro_gps_precio" class="control-label">Precio al Cliente USD$</label>
              <input type="text" name="txt_seguro_gps_precio" id="txt_seguro_gps_precio" class="form-control input-sm moneda" value="<?php echo $seguro_gps_precio;?>" readonly>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este seguro_gps?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="seguro_gps_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_seguro_gps">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_seguro_gps">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_seguro_gps">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/segurogps/segurogps_form.js?ver=11';?>"></script>
