
$(document).ready(function(){
	console.log('cambios guardados 111');

	$(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "999999.99",
  });

	$('#cmb_seguro_gps_tipo').change(function(){
		var tipo = $(this).val();
		console.log('asasa ' + tipo)
		if(tipo == 1){ // POLIZA DE SEGURO
			$('.solo_gps').hide();
			$('#h4_informacion').html('<i class="fa fa-info"></i> EL PORCENTAJE INDICADO SE APLICARÁ AL VALOR DE LA CUOTA MENSUAL EN BASE A LOS MESES FINANCIADOS');
		}
		if(tipo == 2){ // GPS
			$('.solo_gps').show();
			$('#h4_informacion').html('<i class="fa fa-info"></i> EL PRECIO FINAL DEL GPS SE DIVIDIRÁ ENTRE LOS MESES A FINANCIAR');
		}
		$('#seguro_gps_info').show();
	});

	$('#txt_seguro_gps_costo, #txt_seguro_gps_porcentaje').change(function(){
		var gps_costo = Number($("#txt_seguro_gps_costo").autoNumeric("get"));
		var gps_porcentaje = Number($("#txt_seguro_gps_porcentaje").autoNumeric("get"));
		var tipo = parseInt($('#cmb_seguro_gps_tipo').val());

		if(tipo == 2 && gps_costo > 0 && gps_porcentaje > 0){
			gps_porcentaje = 1 + (gps_porcentaje / 100); //nos dará un resultado similar a: 1.20, 20% utilidad
			var precio_sin_igv = (gps_costo / 1.18).toFixed(2); // obtenemos el valor del gps sin IGV
			var precio_final = precio_sin_igv * gps_porcentaje; //obtenemos el valor para vender
			precio_final = (precio_final * 1.18).toFixed(2);; //aplicamos el 18% de IGV que lo pagará el cliente y eso será el precio final para el cliente
			
			$("#txt_seguro_gps_precio").autoNumeric("set", precio_final);
		}
		console.log('miraaa: ' + gps_costo)
	});

  $('#form_seguro_gps').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"segurogps/segurogps_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_seguro_gps").serialize(),
				beforeSend: function() {
					$('#seguro_gps_mensaje').show(400);
					$('#btn_guardar_seguro_gps').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#seguro_gps_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#seguro_gps_mensaje').html(data.mensaje);
						
						seguro_gps_tabla();
						$('#modal_registro_seguro_gps').modal('hide');
					}
					else{
		      	$('#seguro_gps_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#seguro_gps_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_seguro_gps').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#seguro_gps_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#seguro_gps_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			cmb_seguro_gps_tipo: {
				required: true
			},
			txt_seguro_gps_costo: {
				required: function(){
					var tipo = $('#cmb_seguro_gps_tipo').val();
					if(tipo == 2)
						return true;
					else
						return false;
				}
			},
			txt_seguro_gps_meses: {
				required: true
			},
			txt_seguro_gps_porcentaje: {
				required: true
			}
		},
		messages: {
			cmb_seguro_gps_tipo: {
				required: "Selecciona un tipo"
			},
			txt_seguro_gps_costo: {
				required: "Ingresa el Costo del GPS"
			},
			txt_seguro_gps_meses: {
				required: "Ingresa los meses a financiar"
			},
			txt_seguro_gps_porcentaje: {
				required: "Igresa el % a aplicar"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
