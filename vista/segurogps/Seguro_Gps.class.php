<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Seguro_Gps extends Conexion{
    public $seguro_gps_id;
    public $usuario_id; 
    public $seguro_gps_tipo; 
    public $seguro_gps_costo; 
    public $seguro_gps_porcentaje; 
    public $seguro_gps_precio; 
    public $seguro_gps_meses;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $columns = [
          'tb_usuario_id',
          'tb_seguro_gps_tipo', 
          'tb_seguro_gps_costo', 
          'tb_seguro_gps_porcentaje', 
          'tb_seguro_gps_precio', 
          'tb_seguro_gps_meses'
        ];
  
        // Lista de placeholders
        $placeholders = implode(',', array_map(function ($column) {
            return ':' . $column;
        }, $columns));

        $sql = "INSERT INTO tb_seguro_gps (" . implode(',', $columns) . ") VALUES ($placeholders)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_gps_tipo", $this->seguro_gps_tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_gps_costo", $this->seguro_gps_costo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_gps_porcentaje", $this->seguro_gps_porcentaje, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_gps_precio", $this->seguro_gps_precio, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_gps_meses", $this->seguro_gps_meses, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_seguro_gps SET 
          tb_seguro_gps_tipo =:tb_seguro_gps_tipo,
          tb_seguro_gps_costo =:tb_seguro_gps_costo,
          tb_seguro_gps_porcentaje =:tb_seguro_gps_porcentaje, 
          tb_seguro_gps_precio =:tb_seguro_gps_precio, 
          tb_seguro_gps_meses =:tb_seguro_gps_meses WHERE tb_seguro_gps_id =:tb_seguro_gps_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_gps_id", $this->seguro_gps_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_gps_tipo", $this->seguro_gps_tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_gps_costo", $this->seguro_gps_costo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_gps_porcentaje", $this->seguro_gps_porcentaje, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_gps_precio", $this->seguro_gps_precio, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_gps_meses", $this->seguro_gps_meses, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($seguro_gps_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_seguro_gps WHERE tb_seguro_gps_id =:tb_seguro_gps_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_gps_id", $seguro_gps_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($seguro_gps_id){
      try {
        $sql = "SELECT * FROM tb_seguro_gps WHERE tb_seguro_gps_id =:tb_seguro_gps_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_gps_id", $seguro_gps_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function obtener_valor_adicional($seguro_gps_tipo, $seguro_gps_meses){
      try {
        $sql = "SELECT * FROM tb_seguro_gps 
        WHERE tb_seguro_gps_tipo =:tb_seguro_gps_tipo AND tb_seguro_gps_meses <= :tb_seguro_gps_meses 
        ORDER BY tb_seguro_gps_meses DESC LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_gps_tipo", $seguro_gps_tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_gps_meses", $seguro_gps_meses, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_seguro_gps(){
      try {
        $sql = "SELECT * FROM tb_seguro_gps";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
