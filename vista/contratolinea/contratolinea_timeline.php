<?php
require_once ('../../core/usuario_sesion.php');
require_once('Contratolinea.class.php');
$oContratolinea = new Contratolinea();
require_once('../vacaciones/Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$vacaciones = $_POST['vacaciones_id'];
$result = $oContratolinea->listar_contratolineas($vacaciones);

if ($result['estado'] == 1) {
    foreach ($result['data']as $key => $value) {

        $dts = $oUsuario->mostrarUno($value['tb_usuario_id']);
        if ($dts['estado'] == 1) {
            $usuario_nombre = $dts['data']['tb_usuario_nom'];
            $usuario_apellido = $dts['data']['tb_usuario_ape'];
            $tb_usuario_fot = $dts['data']['tb_usuario_fot'];
        }
        $lista .= '<div class="item">
                <img src="' . $tb_usuario_fot . '" alt="user image" class="online">
                <p class="message" style="font-family:cambria">
                    <a href="javascript:void(0)" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> ' . mostrar_fecha_hora($value['tb_contratolinea_reg']) . '</small>
                        ' . $usuario_nombre . ' ' . $usuario_apellido . ' 
                    </a>' . $value['tb_contratolinea_det'] . '
                </p>
            </div>';
    }
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_contratolinea_timeline" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000099">HISTORIAL DE CONTRATO</h4>
            </div>
            <div class="modal-body">
                <!--                <ul class="timeline">
                                </ul>    -->
                <div class="box box-success">

                    <!--<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">-->
                        <div class="box-body chat" id="chat-box" >
                            <!-- chat item -->
                            <?php echo $lista; ?>
                            <!-- /.item -->
                        </div>
                    <!--</div>-->

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/contratolinea/contratolinea_timeline.js'; ?>"></script>



<!--



<div class="item">
    <img src="dist/img/user2-160x160.jpg" alt="user image" class="offline">

    <p class="message">
        <a href="#" class="name">
            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
            Susan Doe
        </a>
        I would like to meet you to discuss the latest news about
        the arrival of the new theme. They say it is going to be one the
        best themes on the market
    </p>
</div>-->


