<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Contratolinea extends Conexion {

    public $tb_contratolinea_id;
    public $tb_contratolinea_xac;
    public $tb_contratolinea_reg;
    public $tb_contratolinea_det;
    public $tb_vacaciones_id;
    public $tb_usuario_id;
    public $tb_contratolinea_vacini;
    public $tb_contratolinea_vacfin;

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_contratolinea(
                                        tb_contratolinea_det, 
                                        tb_vacaciones_id, 
                                        tb_usuario_id)
                                VALUES (
                                        :tb_contratolinea_det, 
                                        :tb_vacaciones_id, 
                                        :tb_usuario_id)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_contratolinea_det", $this->tb_contratolinea_det, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vacaciones_id", $this->tb_vacaciones_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    /* GERSON (03-04-23) */
    function insertarVacaciones() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_contratolinea(
                                        tb_contratolinea_det, 
                                        tb_vacaciones_id, 
                                        tb_usuario_id,
                                        tb_contratolinea_vacini,
                                        tb_contratolinea_vacfin)
                                VALUES (
                                        :tb_contratolinea_det, 
                                        :tb_vacaciones_id, 
                                        :tb_usuario_id,
                                        :tb_contratolinea_vacini,
                                        :tb_contratolinea_vacfin)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_contratolinea_det", $this->tb_contratolinea_det, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_vacaciones_id", $this->tb_vacaciones_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_contratolinea_vacini", $this->tb_contratolinea_vacini, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_contratolinea_vacfin", $this->tb_contratolinea_vacfin, PDO::PARAM_STR);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    /*  */

    function eliminar($contratolinea_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_contratolinea SET tb_contratolinea_xac = 0 WHERE tb_contratolinea_id =:contratolinea_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":contratolinea_id", $contratolinea_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($contratolinea_id) {
        try {
            $sql = "SELECT * FROM tb_contratolinea WHERE tb_contratolinea_id =:contratolinea_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":contratolinea_id", $contratolinea_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_contratolineas($vacaciones_id) {
        try {
            $sql = "SELECT * FROM tb_contratolinea WHERE tb_vacaciones_id =:tb_vacaciones_id and tb_contratolinea_xac = 1 order by tb_contratolinea_reg desc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_vacaciones_id",$vacaciones_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>
