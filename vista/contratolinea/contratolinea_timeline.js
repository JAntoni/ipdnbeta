function contratolinea_eliminar(contratolinea_id, contrato_id){ 
  $.confirm({
    icon: 'fa fa-remove',
    title: 'Eliminar',
    content: '¿Desea eliminar este mensaje?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"contratolinea/contratolinea_controller.php",
          async:true,
          dataType: "json",
          data: ({
            action: "eliminar",
            contratolinea_id: contratolinea_id
          }),
          beforeSend: function() {
            $('#h3_modal_title').text('Eliminando...');
            $('#modal_mensaje').modal('show');
            $('#body_modal_mensaje').html('Espere');
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              $('#modal_contratolinea_timeline').modal('hide');
              contratolinea_timeline(contrato_id);
            }
            else{
              alerta_warning('Alerta', data.mensaje); //en generales.js
            }
          },
          complete: function(data){
            $('#modal_mensaje').modal('hide');
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
            console.log(data.responseText);
          }
        });
      },
      no: function () {}
    }
  });
}
$(document).ready(function() {
//  console.log('Perfil menu');

});