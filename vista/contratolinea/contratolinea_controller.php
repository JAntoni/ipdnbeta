<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../contratolinea/Contratolinea.class.php');
  $oCreditolinea = new Creditolinea();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$contratolinea_nom = mb_strtoupper($_POST['txt_contratolinea_nom'], 'UTF-8');
 		$contratolinea_des = $_POST['txt_contratolinea_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el contratolinea.';
 		if($oCreditolinea->insertar($contratolinea_nom, $contratolinea_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'contratolinea registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$contratolinea_id = intval($_POST['hdd_contratolinea_id']);
 		$contratolinea_nom = mb_strtoupper($_POST['txt_contratolinea_nom'], 'UTF-8');
 		$contratolinea_des = $_POST['txt_contratolinea_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el contratolinea.';

 		if($oCreditolinea->modificar($contratolinea_id, $contratolinea_nom, $contratolinea_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'contratolinea modificado correctamente. '.$contratolinea_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$contratolinea_id = intval($_POST['contratolinea_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el mensaje de la Línea de Tiempo';

 		if($oCreditolinea->eliminar($contratolinea_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Mensaje de Línea de Tiempo eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>