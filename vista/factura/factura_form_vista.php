<div class="box box-primary">
    <div class="box-header">
        <div class="row">
            <div class="col-md-4">
                <label>Tipo Documento</label>
                <select class="form-control input-sm mayus" id="cbo_tipodocumento" name="cbo_tipodocumento">
                    <option value=""<?php if ($factura_tipo =="") echo 'selected'; ?> >...Seleccionar...</option>
                    <option value="1"<?php if ($factura_tipo ==1) echo 'selected'; ?> >Boleta</option>
                    <option value="2"<?php if ($factura_tipo ==2) echo 'selected'; ?> >Factura</option>
                    <option value="3"<?php if ($factura_tipo ==3) echo 'selected'; ?> >Nota Crédito</option>
                    <option value="4"<?php if ($factura_tipo ==4) echo 'selected'; ?> >OTROS</option>
                </select>
            </div>
            <div class="col-md-4">
                <label>N° Ruc</label>
                <input class="form-control input-sm mayus" id="txt_factura_ruc" name="txt_factura_ruc" value="<?php echo $factura_ruc; ?>" maxlength="11">
            </div>
            <div class="col-md-4">
                <label>N° Documento</label>
                <input class="form-control input-sm mayus" id="txt_factura_numero" name="txt_factura_numero" value="<?php echo $factura_numero; ?>">
            </div>
        </div>
        <p>
        <div class="row">
            <div class="col-md-4">
                <label>Emitente</label>
                <input class="form-control input-sm mayus" id="txt_factura_emitente" name="txt_factura_emitente" value="<?php echo $factura_emitente; ?>">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Emisión</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type="text" class="form-control input-sm" name="txt_factura_fecha_emision" id="txt_factura_fecha_emision" value="<?php echo $factura_fecha_emision; ?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Vencimiento</label>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type="text" class="form-control input-sm" name="txt_factura_fecha_vencimiento" id="txt_factura_fecha_vencimiento" value="<?php echo $factura_fecha_vencimiento; ?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
        </div>
        <p>
        <div class="row">
            <div class="col-md-4">
                <label>N° Ruc Destinatario</label>
                <input class="form-control input-sm mayus" id="txt_factura_ruc_destinatario" name="txt_factura_ruc_destinatario">
            </div>
            <div class="col-md-4">
                <label>Destinatario</label>
                <input class="form-control input-sm mayus" id="txt_factura_destinatario" name="txt_factura_destinatario">
            </div>
            <div class="col-md-4">
                <label>Moneda</label>
                <select class="form-control input-sm mayus" name="cbo_moneda_id" id="cbo_moneda_id">
                    <?php $moneda_id=2;
                    require_once '../moneda/moneda_select.php';?>
                </select>
            </div>
        </div>
        <p>
        <div class="row">
            <div class="col-md-4">
                <label>Monto</label>
                <input class="form-control input-sm moneda" id="txt_factura_importe" name="txt_factura_importe">
            </div>
             <?php // if($usuario_action != 'I'): ?>
            <div class="col-md-4">
                <label>Subir PDF</label><br>
                <a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="upload_formpdf('I',<?php echo $id_mod; ?>,'<?php echo $upload_modulo; ?>')" title="Subir Pdf de la Poliza SRT o GPS"><i class="fa fa-file-pdf-o"></i></a>
            </div>
             <?php // endif; ?>
            <div class="col-md-4">
                <label>Uploal PDF</label><br>
                <input type="hidden" class="form-control input-sm mayus" id="txt_uploap" name="txt_uploap" readonly="">
                <input type="hidden" value="<?php echo uniqid('',true).''.hash('sha256', openssl_random_pseudo_bytes(20)); ?>" id="upload_uniq" name="hdd_upload_uniq">
            </div>
        </div>
    </div>
</div>