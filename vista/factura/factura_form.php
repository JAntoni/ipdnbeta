<?php
require_once('../../core/usuario_sesion.php');

require_once('../factura/Factura.class.php');
$oFactura = new Factura();
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once('../funciones/funciones.php');
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$direc = 'factura';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$factura_id = $_POST['factura_id'];

$poliza_id = $_POST['poliza_id'];
$polizadetalle_id = $_POST['polizadetalle_id'];
$factura_fecha_emision=date('d-m-Y');
$factura_fecha_vencimiento=date('d-m-Y');
$id_mod=0;

$vista=$_POST['vista'];
 if($vista == 'control_seguro'){
     $upload_modulo="seguros";$id_mod=$poliza_id;
     
 } // 0 SI LLEGA DESDE CONTROL SEGUROS -> POLIZA

if($vista == 'control_seguro_detalle'){$upload_modulo="segurosdetalle";$id_mod=$polizadetalle_id;

} // 1 SI LLEGA DESDE CONTROL SEGUROS DETALLE -> POLIZADETALLE

    if($vista == 'PDFcontrol_seguro_detalle'){
        $upload_modulo="segurosdetalle";
        $id_mod=$polizadetalle_id;
        
    } // 1 SI LLEGA DESDE CONTROL SEGUROS DETALLE -> POLIZADETALLE


$titulo = '';
if ($usuario_action == 'L') {
    $titulo = 'Documento Registrado';
    $action="leer";
}
if ($usuario_action == 'I') {
    $titulo = 'Registrar Documento';
    $action="insertar";
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Documento';
    $action="modificar";
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Documento';
    $action="eliminar";
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
      
if(intval($polizadetalle_id > 0)){
    $result = $oPolizadetalle->mostarUno($polizadetalle_id);
    $poliza_id = $result['data']['tb_poliza_id'];
}


$dts = $oPoliza->mostarUno($poliza_id);
    if ($dts['estado'] == 1) {
        $poliza_num = $dts['data']['tb_poliza_num'];
        $cliente_id = $dts['data']['tb_cliente_id'];
        $cliente_nom = $dts['data']['tb_cliente_nom'];
        $cliente_doc = $dts['data']['tb_cliente_doc'];
        $poliza_aseg = $dts['data']['tb_poliza_aseg'];
        $poliza_tip = $dts['data']['tb_poliza_tip'];
        $poliza_fecini = mostrar_fecha($dts['data']['tb_poliza_fecini']);
        $poliza_fecfin = mostrar_fecha($dts['data']['tb_poliza_fecfin']);
        $credito_id = $dts['data']['tb_credito_id'];
        $creditotipo_id = intval($dts['data']['tb_creditotipo_id']);
        $poliza_respag = $dts['data']['tb_poliza_respag'];
        $poliza_pre = $dts['data']['tb_poliza_pre'];
        $poliza_cuo = $dts['data']['tb_poliza_cuo'];
        $poliza_estcomi = $dts['data']['tb_poliza_estcomi'];
        $poliza_ver = intval($dts['data']['tb_poliza_ver']);
        $poliza_comi = $dts['data']['tb_poliza_comi'];
        $poliza_tipo = $dts['data']['tb_poliza_tipo']; //si es Seguro todo riesgo o es GPS
        $poliza_cate = $dts['data']['tb_poliza_categoria'];
        $vehiculomarca_id = $dts['data']['tb_vehiculomarca_id'];
        $vehiculomodelo_id = $dts['data']['tb_vehiculomodelo_id'];
        $vehiculo_tasacion = $dts['data']['tb_vehiculo_tasacion'];
        $usuario_id = $dts['data']['tb_asesor_id'];
        $vendedor_id = $dts['data']['tb_vendedor_id'];
        $poliza_formapago = $dts['data']['tb_poliza_formapago'];
    }
$dts =NULL;
//  echo 'hasta aki estamos entrando al sistema normal';exit();
 
    $modulo_id = $creditotipo_id;// 2. Asistencia Vehicular, 3. Garantia Vehicular 
    $modid = $credito_id; // id del credito dependiendo del modulo



//si la accion es modificar, mostramos los datos del factura por su ID
if (intval($factura_id) > 0) {
    $result = $oFactura->mostrarUno($factura_id);
    if ($result['estado'] != 1) {
        $mensaje = 'No se ha encontrado ningún registro para el factura seleccionado, inténtelo nuevamente.';
        $bandera = 4;
    } else {
        $factura_tipo = $result['data']['tb_factura_tipo'];
        $factura_ruc = $result['data']['tb_factura_ruc'];
        $factura_numero = $result['data']['tb_factura_numero'];
        $factura_emitente = $result['data']['tb_factura_emitente'];
        $factura_fecha_emision = $result['data']['tb_factura_fecha_emision'];
        $factura_fecha_vencimiento = $result['data']['tb_factura_fecha_vencimiento'];
        $factura_destinatario = $result['data']['tb_factura_destinatario'];
        $factura_ruc_destinatario = $result['data']['tb_factura_ruc_destinatario'];
        $factura_importe = $result['data']['tb_factura_importe'];
        $moneda_id = $result['data']['tb_moneda_id'];
        $modulo_id = $result['data']['tb_modulo_id'];
        $modid = $result['data']['tb_modid'];

    }
    $result = NULL;
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_factura" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo $titulo;?></h4>
                <!--<h4 class="modal-title"><?php echo $titulo.' || $factura_id='.$factura_id.' || $poliza_id='.$poliza_id.'  || $id_mod = '.$id_mod;?></h4>-->
            </div>
            <form id="form_factura" method="post">
                <input type="hidden" name="action" value="<?php echo $action; ?>">
                <input type="hidden" name="hdd_factura_id" id="hdd_factura_id" value="<?php echo $factura_id; ?>">
                <input type="hidden" name="hdd_modulo_id1" id="hdd_modulo_id1" value="<?php echo $modulo_id; ?>">
                <input type="hidden" name="hdd_modid" id="hdd_modid" value="<?php echo $modid; ?>">
                <input type="hidden" name="hdd_poliza_id" id="hdd_poliza_id" value="<?php echo $poliza_id; ?>">
                <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista; ?>">

                <div class="modal-body">


                    <?php include_once 'factura_form_vista.php'; ?>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($action == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Documento?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="factura_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if ($usuario_action == 'I' || $usuario_action == 'M'): ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_factura">Guardar</button>
                        <?php endif; ?>
                        <?php if ($usuario_action == 'E'): ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_factura">Aceptar</button>
                        <?php endif; ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/factura/factura_form.js?ver=646467464646464634646465'; ?>"></script>
