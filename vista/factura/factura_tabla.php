<?php
if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}

require_once('Factura.class.php');
$oFactura = new Factura();
$tipo_documento= intval($_POST['tipo']);
$fecha1= ($_POST['fecha1']);
$fecha2= ($_POST['fecha2']);
//echo 'hasta ki estoy entrando normal al sistema';exit();
$result = $oFactura->listar_facturas($tipo_documento, fecha_mysql($fecha1),fecha_mysql($fecha2));



$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        
        if(intval($value['tb_factura_tipo']==1)){
            $tipo='<span class="badge bg-green">BOLETA</span>';
        }
        if(intval($value['tb_factura_tipo']==2)){
            $tipo='<span class="badge bg-blue">FACTURAS</span>';
        }
        if(intval($value['tb_factura_tipo']==3)){
            $tipo='<span class="badge bg-red">NOTA DE CREDITO</span>';
        }
        if(intval($value['tb_factura_tipo']==4)){
            $tipo='<span class="badge bg-black">OTROS</span>';
        }
        /*COMPRAR MONEDAS*/
        if(intval($value['tb_moneda_id']==1)){
            $moneda='S./ ';
        }
        if(intval($value['tb_moneda_id']==2)){
            $moneda='US$./ ';
        }
        if(intval($value['tb_modulo_id']==0)){
            $modulo='MANUAL';
        }
        if(intval($value['tb_modulo_id']==3)){
            $modulo='<span class="badge bg-green">GARANTIA VEHICULAR</span>';
        }

        
        
        
        
        
        $tr .= '<tr id="tabla_cabecera_fila">';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_factura_id'] . '</td>
          <td id="tabla_fila">' . $tipo.'</td>
          <td id="tabla_fila">' . $value['tb_factura_ruc'] . ' - ' . $value['tb_factura_numero'] . '</td>
          <td id="tabla_fila">' . $value['tb_factura_emitente'] . '</td>
          <td id="tabla_fila">' . mostrar_fecha($value['tb_factura_fecha_emision']) . '</td>
          <td id="tabla_fila">' . mostrar_fecha($value['tb_factura_fecha_vencimiento']) . '</td>
          <td id="tabla_fila">' . $value['tb_factura_ruc_destinatario'] . '</td>
          <td id="tabla_fila">' . $value['tb_factura_destinatario'] . '</td>
          <td id="tabla_fila">' . $moneda . '</td>
          <td id="tabla_fila">' . $value['tb_factura_importe'] . '</td>
          <td id="tabla_fila">' . $modulo . '</td>
          <td id="tabla_fila">' . $value['tb_modid'] . '</td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="factura_form(\'L\',' . $value['tb_factura_id'] . ')"><i class="fa fa-eye"></i></a> 
            <a class="btn btn-warning btn-xs" title="Editar" onclick="factura_form(\'M\',' . $value['tb_factura_id'] . ')"><i class="fa fa-edit"></i></a> 
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="factura_form(\'E\',' . $value['tb_factura_id'] . ')"><i class="fa fa-trash"></i></a> 
          </td>
        ';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
?>
<table id="tbl_facturas" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">TIPO</th>
            <th id="tabla_cabecera_fila">RUC</th>
            <th id="tabla_cabecera_fila">EMITENTE</th>
            <th id="tabla_cabecera_fila">FEC. EMISION</th>
            <th id="tabla_cabecera_fila">FEC.VENCIMIENTO</th>
            <th id="tabla_cabecera_fila">RUC DESTINATARIO</th>
            <th id="tabla_cabecera_fila">DESTINATARIO</th>
            <th id="tabla_cabecera_fila">MONEDA</th>
            <th id="tabla_cabecera_fila">IMPORTE</th>
            <th id="tabla_cabecera_fila">MODULO</th>
            <th id="tabla_cabecera_fila">CREDITO ID</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>
