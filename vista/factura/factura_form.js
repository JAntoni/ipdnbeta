/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy"
        //startDate: "-0d"
//        endDate: new Date()
    });


$("#cbo_tipodocumento").change(function (){
    const string = $('#cbo_tipodocumento option:selected').html();
    var letra;

letra=string.charAt(0).toUpperCase(); // Retorna "F"
        console.log(letra);
        $("#txt_factura_numero").mask(letra+"999-999999999");
        $("#txt_factura_numero").val("");
});


  
//    $("#tin").mask("99-9999999");
//    $("#ssn").mask("999-99-9999");





    $('#form_factura').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "factura/factura_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_factura").serialize(),
                beforeSend: function () {
                    $('#factura_mensaje').show(400);
                    $('#btn_guardar_factura').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        var vista=$("#hdd_vista").val();
//                        $('#factura_mensaje').removeClass('callout-info').addClass('callout-success')
//                        $('#factura_mensaje').html(data.mensaje);
                        swal_success("Registro Correcto",data.mensaje,3000);
                        setTimeout(function () {
                            if(vista=="factura")
                                facturas_tabla();
                            if(vista=="PDFcontrol_seguro_detalle")
                                pdfcontrolsegurodetallereporte_tabla();
                            $('#modal_registro_factura').modal('hide');
                        }, 1000
                                );
                    } else {
//                        $('#factura_mensaje').removeClass('callout-info').addClass('callout-warning')
//                        $('#factura_mensaje').html('Alerta: ' + data.mensaje);
                        swal_warning("Aviso",data.mensaje,3000);
                        $('#btn_guardar_factura').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
//                    $('#factura_mensaje').removeClass('callout-info').addClass('callout-danger');
//                    $('#factura_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                    swal_error("ERROR",data.responseText,3000);
                }
            });
        },
        rules: {
            cbo_tipodocumento: {
                required: true
            },
            txt_factura_ruc: {
                required: true,
                minlength: 11
            },
            txt_factura_emitente: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            cbo_tipodocumento: {
                required: "Seleccione un tipo de Documento",
            },
            txt_factura_ruc: {
                required: "Ingrese el RUC emitente",
                minlength: "El RUC debe tener como mínimo 11 caracteres"
            },
            txt_factura_emitente: {
                required: "Ingrese el emitente",
                minlength: "El Nombre del Emitente debe tener 5 caracteres como mínimo"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });














});




/*FUNCION PARA SUBIR ARCHIVOS PDF DE MAXIMO 20MG PARA LOS CONTRATOS DE POLIZAS DE SEGUROS Y GPS*/
function upload_formpdf(usuario_act, upload_id, vista_cod) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "uploadpdf/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: vista_cod, //nombre de la tabla a relacionar
            modulo_id: upload_id,
            upload_uniq:$("#upload_uniq").val(),
            vista:'seguros'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_uploadpdf_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
        }
    });
}