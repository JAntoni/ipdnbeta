<?php

require_once('../../core/usuario_sesion.php');

require_once('../factura/Factura.class.php');
$oFactura = new Factura();
require_once('../uploadpdf/Upload.class.php');
$oUpload = new Upload();

require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';

$action = $_POST['action'];

$factura_id =  intval($_POST['hdd_factura_id']);
$factura_tipo =  intval($_POST['cbo_tipodocumento']);
$factura_ruc = $_POST['txt_factura_ruc'];
$factura_numero = $_POST['txt_factura_numero'];
$factura_emitente=mb_strtoupper($_POST['txt_factura_emitente'], 'UTF-8');
$factura_fecha_emision = fecha_mysql($_POST['txt_factura_fecha_emision']);
$factura_fecha_vencimiento = fecha_mysql($_POST['txt_factura_fecha_vencimiento']);
$factura_destinatario = mb_strtoupper($_POST['txt_factura_destinatario'], 'UTF-8');
$factura_ruc_destinatario = $_POST['txt_factura_ruc_destinatario'];
$factura_importe = moneda_mysql($_POST['txt_factura_importe']);
$moneda_id = intval($_POST['cbo_moneda_id']);
$modulo_id = intval($_POST['hdd_modulo_id1']);
$modid = intval($_POST['hdd_modid']);
$upload_uniq = $_POST['hdd_upload_uniq'];
$usuario_id = intval($_SESSION['usuario_id']);


$oFactura->setFactura_id($factura_id);
$oFactura->setFactura_tipo($factura_tipo);
$oFactura->setFactura_ruc($factura_ruc);
$oFactura->setFactura_numero($factura_numero);
$oFactura->setFactura_emitente($factura_emitente);
$oFactura->setFactura_fecha_emision($factura_fecha_emision);
$oFactura->setFactura_fecha_vencimiento($factura_fecha_vencimiento);
$oFactura->setFactura_destinatario($factura_destinatario);
$oFactura->setFactura_ruc_destinatario($factura_ruc_destinatario);
$oFactura->setFactura_importe($factura_importe);
$oFactura->setMoneda_id($moneda_id);
$oFactura->setModulo_id($modulo_id);
$oFactura->setModid($modid);
$oFactura->setUsuario_id($usuario_id);




if ($action == 'insertar') {

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar la Factura.';
    $resultFactura=$oFactura->insertar();
    
    if ($resultFactura['estado']==1) {
        $factura_id1=$resultFactura['factura_id'];
        
        $resultUpload=$oUpload->mostrarUpload_uniq($upload_uniq);
        if($resultUpload['estado']==1){
            $upload_id=$resultUpload['data']['upload_id'];
        }
        $resultUpload=NULL;
        
        
        $oFactura->modificar_campo($factura_id1, 'tb_uploap_id', $upload_id, 'INT');
        
        $data['estado'] = 1;
        $data['mensaje'] = 'Factura registrada correctamente.';

    }
    $resultFactura=NULL;
    echo json_encode($data);
    
} elseif ($action == 'modificar') {
    $factura_id = intval($_POST['hdd_factura_id']);
    $factura_nom = mb_strtoupper($_POST['txt_factura_nom'], 'UTF-8');
    $factura_des = $_POST['txt_factura_des'];

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar el Area.';

    if ($oFactura->modificar($factura_id, $factura_nom, $factura_des)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Area modificado correctamente. ' . $factura_des;
    }

    echo json_encode($data);
} elseif ($action == 'eliminar') {
    $factura_id = intval($_POST['hdd_factura_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el Area.';

    if ($oFactura->eliminar($factura_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Area eliminado correctamente.';
    }

    echo json_encode($data);
} else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>