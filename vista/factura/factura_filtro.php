

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_controlsefuro_filtro" class="form-inline" role="form">
                    <!--<button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</button>-->
                    <button type="button" class="btn btn-primary btn-sm" onclick="factura_form('I', 0)"><i class="fa fa-plus"></i> Nueva Factura</button>
                    <div class="form-group">
                        <select class="form-control input-sm mayus" id="cbo_tipodocumento1" name="cbo_tipodocumento">
                            <option value=""<?php if ($$factura_tipo == "") echo 'selected'; ?> >...Seleccionar...</option>
                            <option value="1"<?php if ($$factura_tipo == 1) echo 'selected'; ?> >Boleta</option>
                            <option value="2"<?php if ($$factura_tipo == 2) echo 'selected'; ?> >Factura</option>
                            <option value="3"<?php if ($$factura_tipo == 3) echo 'selected'; ?> >Nota Crédito</option>
                            <option value="4"<?php if ($$factura_tipo == 4) echo 'selected'; ?> >Otros</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <!--<label for="">Fecha </label>-->
                        <div class="input-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo date('01-m-Y'); ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="input-group-addon">-</span>
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo date('d-m-Y'); ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <a href="javascript:void(0)" onClick="facturas_tabla()" class="btn btn-primary btn-sm" title="Buscar"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>