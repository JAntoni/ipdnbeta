/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
facturas_tabla();
$("#click").click();

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });
    
     $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_ing_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
        facturas_tabla();
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_ing_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
        facturas_tabla();
    });

});



function factura_form(usuario_act, factura_id,poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "factura/factura_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            factura_id: factura_id,
            poliza_id:poliza_id,
            vista:'factura'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_factura_form').html(data);
                $('#modal_registro_factura').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_factura'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_factura', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'factura';
                var div = 'div_modal_factura_form';
                permiso_solicitud(usuario_act, factura_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

$('#cbo_tipodocumento1').change(function (){
    facturas_tabla();
});


function facturas_tabla() {

    var tipo = $('#cbo_tipodocumento1').val();
    var fecha1 = $('#txt_fil_ing_fec1').val();
    var fecha2 = $('#txt_fil_ing_fec2').val();

    $.ajax({
        type: "POST",
        url: VISTA_URL + "factura/factura_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            tipo: tipo,
            fecha1: fecha1,
            fecha2: fecha2
        }),
        beforeSend: function () {
            $('#factura_mensaje_tbl').show(400);
        },
        success: function (html) {
            $('#div_factura_tabla').html(html);
            $('#factura_mensaje_tbl').hide(400);
        },
        complete: function () {
            estilos_datatable();
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tbl_facturas').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [12], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}
