<?php
error_reporting(1);
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Factura extends Conexion {

    private $factura_id;
    private $factura_reg;
    private $factura_xac;
    private $factura_tipo;
    private $factura_ruc;
    private $factura_numero;
    private $factura_emitente;
    private $factura_fecha_emision;
    private $factura_fecha_vencimiento;
    private $factura_destinatario;
    private $factura_ruc_destinatario;
    private $factura_importe;
    private $moneda_id;
    private $modulo_id;
    private $modid;
    private $usuario_id;

    public function getFactura_id() {
        return $this->factura_id;
    }

    public function getFactura_reg() {
        return $this->factura_reg;
    }

    public function getFactura_xac() {
        return $this->factura_xac;
    }

    public function getFactura_tipo() {
        return $this->factura_tipo;
    }

    public function getFactura_ruc() {
        return $this->factura_ruc;
    }

    public function getFactura_numero() {
        return $this->factura_numero;
    }

    public function getFactura_emitente() {
        return $this->factura_emitente;
    }

    public function getFactura_fecha_emision() {
        return $this->factura_fecha_emision;
    }

    public function getFactura_fecha_vencimiento() {
        return $this->factura_fecha_vencimiento;
    }

    public function getFactura_destinatario() {
        return $this->factura_destinatario;
    }

    public function getFactura_ruc_destinatario() {
        return $this->factura_ruc_destinatario;
    }

    public function getFactura_importe() {
        return $this->factura_importe;
    }

    public function getMoneda_id() {
        return $this->moneda_id;
    }

    public function getModulo_id() {
        return $this->modulo_id;
    }

    public function getModid() {
        return $this->modid;
    }

    public function getUsuario_id() {
        return $this->usuario_id;
    }

    public function setFactura_id($factura_id){
        $this->factura_id = $factura_id;
    }

    public function setFactura_reg($factura_reg){
        $this->factura_reg = $factura_reg;
    }

    public function setFactura_xac($factura_xac){
        $this->factura_xac = $factura_xac;
    }

    public function setFactura_tipo($factura_tipo){
        $this->factura_tipo = $factura_tipo;
    }

    public function setFactura_ruc($factura_ruc){
        $this->factura_ruc = $factura_ruc;
    }

    public function setFactura_numero($factura_numero){
        $this->factura_numero = $factura_numero;
    }

    public function setFactura_emitente($factura_emitente){
        $this->factura_emitente = $factura_emitente;
    }

    public function setFactura_fecha_emision($factura_fecha_emision){
        $this->factura_fecha_emision = $factura_fecha_emision;
    }

    public function setFactura_fecha_vencimiento($factura_fecha_vencimiento){
        $this->factura_fecha_vencimiento = $factura_fecha_vencimiento;
    }

    public function setFactura_destinatario($factura_destinatario){
        $this->factura_destinatario = $factura_destinatario;
    }

    public function setFactura_ruc_destinatario($factura_ruc_destinatario){
        $this->factura_ruc_destinatario = $factura_ruc_destinatario;
    }

    public function setFactura_importe($factura_importe){
        $this->factura_importe = $factura_importe;
    }

    public function setMoneda_id($moneda_id){
        $this->moneda_id = $moneda_id;
    }

    public function setModulo_id($modulo_id){
        $this->modulo_id = $modulo_id;
    }

    public function setModid($modid){
        $this->modid = $modid;
    }

    public function setUsuario_id($usuario_id){
        $this->usuario_id = $usuario_id;
    }

        
    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_factura(
                                        tb_factura_tipo, 
                                        tb_factura_ruc, 
                                        tb_factura_numero,
                                        tb_factura_emitente,
                                        tb_factura_fecha_emision,
                                        tb_factura_fecha_vencimiento,
                                        tb_factura_destinatario,
                                        tb_factura_ruc_destinatario,
                                        tb_factura_importe,
                                        tb_moneda_id,
                                        tb_modulo_id,
                                        tb_modid,
                                        tb_usuario_id)
                            VALUES (                      
                                        :factura_tipo, 
                                        :factura_ruc, 
                                        :factura_numero,
                                        :factura_emitente,
                                        :factura_fecha_emision,
                                        :factura_fecha_vencimiento,
                                        :factura_destinatario,
                                        :factura_ruc_destinatario,
                                        :factura_importe,
                                        :moneda_id,
                                        :modulo_id,
                                        :modid,
                                        :usuario_id)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":factura_tipo", $this->getFactura_tipo(), PDO::PARAM_INT);
            $sentencia->bindParam(":factura_ruc", $this->getFactura_ruc(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_numero", $this->getFactura_numero(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_emitente", $this->getFactura_emitente(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_fecha_emision", $this->getFactura_fecha_emision(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_fecha_vencimiento", $this->getFactura_fecha_vencimiento(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_destinatario", $this->getFactura_destinatario(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_ruc_destinatario", $this->getFactura_ruc_destinatario(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_importe", $this->getFactura_importe(), PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $this->getMoneda_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":modulo_id", $this->getModulo_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":modid", $this->getModid(), PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $this->getUsuario_id(), PDO::PARAM_INT);

            $result = $sentencia->execute();
            $factura_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            $data['estado'] = $result;
            $data['factura_id'] = $factura_id;
            return $data; //si es correcto el factura retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_factura SET 
                                        tb_factura_tipo=:factura_tipo, 
                                        tb_factura_ruc=:factura_ruc, 
                                        tb_factura_numero=:factura_numero,
                                        tb_factura_emitente=:factura_emitente,
                                        tb_factura_fecha_emision=:factura_fecha_emision,
                                        tb_factura_fecha_vencimiento=:factura_fecha_vencimiento,
                                        tb_factura_destinatario=:factura_destinatario,
                                        tb_factura_ruc_destinatario=:factura_ruc_destinatario,
                                        tb_factura_importe=:factura_importe,
                                        tb_moneda_id=:moneda_id,
                                        tb_modulo_id=:modulo_id,
                                        tb_modid=:modid
                                WHERE
                                        tb_factura_id=:factura_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":factura_tipo", $this->getFactura_tipo(), PDO::PARAM_INT);
            $sentencia->bindParam(":factura_ruc", $this->getFactura_ruc(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_numero", $this->getFactura_numero(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_emitente", $this->getFactura_emitente(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_fecha_emision", $this->getFactura_fecha_emision(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_fecha_vencimiento", $this->getFactura_fecha_vencimiento(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_destinatario", $this->getFactura_destinatario(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_ruc_destinatario", $this->getFactura_ruc_destinatario(), PDO::PARAM_STR);
            $sentencia->bindParam(":factura_importe", $this->getFactura_importe(), PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $this->getMoneda_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":modulo_id", $this->getModulo_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":modid", $this->getModid(), PDO::PARAM_INT);
            $sentencia->bindParam(":factura_id", $this->getFactura_id(), PDO::PARAM_INT);

            $result = $sentencia->execute();
            $this->dblink->commit();
            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_factura SET tb_factura_xac=0 WHERE tb_factura_id =:factura_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":factura_id", $this->getFactura_id(), PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();
            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($factura_id) {
        try {
            $sql = "SELECT * FROM tb_factura WHERE tb_factura_id =:factura_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":factura_id", $factura_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_facturas($tipodocumento,$fecha1,$fecha2) {
        try {
             $concatenar="";
            if($tipodocumento>0){
                $concatenar="AND tb_factura_tipo=:factura_tipo";
            }
            
            $sql = "SELECT * FROM tb_factura WHERE tb_factura_xac=1 AND tb_factura_fecha_emision between :fecha1 AND :fecha2 ".$concatenar;

            $sentencia = $this->dblink->prepare($sql);
            if($tipodocumento>0){
            $sentencia->bindParam(":factura_tipo", $tipodocumento, PDO::PARAM_INT);
            }
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    
    
    
    function modificar_campo($factura_id, $factura_columna, $factura_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($factura_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_factura SET " . $factura_columna . " =:factura_valor WHERE tb_factura_id =:factura_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":factura_id", $factura_id, PDO::PARAM_INT);
                if ($param_tip == 'INT') {
                    $sentencia->bindParam(":factura_valor", $factura_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":factura_valor", $factura_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el factura retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

}

?>
