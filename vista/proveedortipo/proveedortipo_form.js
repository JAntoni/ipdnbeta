
$(document).ready(function(){
  $('#form_proveedortipo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"proveedortipo/proveedortipo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_proveedortipo").serialize(),
				beforeSend: function() {
					$('#proveedortipo_mensaje').show(400);
					$('#btn_guardar_proveedortipo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#proveedortipo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#proveedortipo_mensaje').html(data.mensaje);
                        var vista = $("#hdd_vista").val();
                        console.log(vista);
                        alerta_success("EXITO","TIPO AGREGADO");
                        if(vista =='proveedortipo'){
                            proveedortipo_tabla();
                        }
                        if(vista =='proveedorpoliza'){
                            cargar_proveedortipo();
                        }
                        $('#modal_registro_proveedortipo').modal('hide');
		      	
					}
					else{
		      	$('#proveedortipo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#proveedortipo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_proveedortipo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#proveedortipo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#proveedortipo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_proveedortipo_nom: {
				required: true,
				minlength: 2
			},
			txt_proveedortipo_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_proveedortipo_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_proveedortipo_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
