<?php
require_once('Proveedortipo.class.php');
$oProveedortipo = new Proveedortipo();

$proveedortipo_id = (empty($proveedortipo_id))? 0 : $proveedortipo_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oProveedortipo->listar_proveedortipos();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($proveedortipo_id == $value['tb_proveedortipo_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_proveedortipo_id'].'" '.$selected.'>'.$value['tb_proveedortipo_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>