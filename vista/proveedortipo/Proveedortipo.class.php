<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Proveedortipo extends Conexion{

    function insertar($proveedortipo_nom, $proveedortipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_proveedortipo(tb_proveedortipo_xac, tb_proveedortipo_nom, tb_proveedortipo_des)
          VALUES (1, :proveedortipo_nom, :proveedortipo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":proveedortipo_nom", $proveedortipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":proveedortipo_des", $proveedortipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($proveedortipo_id, $proveedortipo_nom, $proveedortipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_proveedortipo SET tb_proveedortipo_nom =:proveedortipo_nom, tb_proveedortipo_des =:proveedortipo_des WHERE tb_proveedortipo_id =:proveedortipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedortipo_nom", $proveedortipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":proveedortipo_des", $proveedortipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($proveedortipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_proveedortipo WHERE tb_proveedortipo_id =:proveedortipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($proveedortipo_id){
      try {
        $sql = "SELECT * FROM tb_proveedortipo WHERE tb_proveedortipo_id =:proveedortipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_proveedortipos(){
      try {
        $sql = "SELECT * FROM tb_proveedortipo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
