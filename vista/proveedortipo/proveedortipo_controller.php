<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../proveedortipo/Proveedortipo.class.php');
  $oProveedortipo = new Proveedortipo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$proveedortipo_nom = mb_strtoupper($_POST['txt_proveedortipo_nom'], 'UTF-8');
 		$proveedortipo_des = $_POST['txt_proveedortipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Tipo de Proveedor.';
 		if($oProveedortipo->insertar($proveedortipo_nom, $proveedortipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Proveedor registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$proveedortipo_id = intval($_POST['hdd_proveedortipo_id']);
 		$proveedortipo_nom = mb_strtoupper($_POST['txt_proveedortipo_nom'], 'UTF-8');
 		$proveedortipo_des = $_POST['txt_proveedortipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Tipo de Proveedor.';

 		if($oProveedortipo->modificar($proveedortipo_id, $proveedortipo_nom, $proveedortipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Proveedor modificado correctamente. ';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$proveedortipo_id = intval($_POST['hdd_proveedortipo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Tipo de Proveedor.';

 		if($oProveedortipo->eliminar($proveedortipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Proveedor eliminado correctamente. '.$proveedortipo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>