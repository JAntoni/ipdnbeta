$(document).ready(function () {
  console.log('descuentos 555')

  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '999999999.00'
  });

  $("#datetimepicker1").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  $("#datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    startDate: "-0d",
    //endDate: new Date(),
  });

  $("#datetimepicker1").on("changeDate", function (e) {
    var startVal = $("#txt_descuento_fecini").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
  });

  $("#datetimepicker2").on("changeDate", function (e) {
    var endVal = $("#txt_descuento_fecfin").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
  });
  

  $('#form_descuento').validate({
    submitHandler: function () {

      $.ajax({
        type: "POST",
        url: VISTA_URL + "descuento/descuento_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_descuento").serialize(),
        beforeSend: function () {
          
        },
        success: function (data) {
          var vista = $('#hdd_vista').val();

          if (parseInt(data.estado) > 0) {
            if(vista == 'personalcontrato')
              personalcontrato_tabla();
            else 
              descuento_tabla();
            
            swal_success("SISTEMA", data.mensaje, 2000);
            $('#modal_registro_descuento').modal('hide');
            
          } 
          else {
            alerta_error('Error', data.mensaje);
            console.log(data)
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      txt_descuento_fecini: {
        required: true
      },
      txt_descuento_fecfin: {
        required: true
      },
      cmb_descuento_tip: {
        required: true
      },
      txt_descuento_mon: {
        required: true
      },
      txt_descuento_det: {
        required: true
      }
    },
    messages: {
      txt_descuento_fecini: {
        required: "Selecciona fecha de inicio"
      },
      txt_descuento_fecfin: {
        required: "Selecciona fecha fin"
      },
      cmb_descuento_tip: {
        required: "Selecciona un tipo de descuento"
      },
      txt_descuento_mon: {
        required: "Ingresa el monto de descuento"
      },
      txt_descuento_det: {
        required: "Ingresa el detalle del descuento"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });
});