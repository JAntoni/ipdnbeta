<?php
require_once('../../core/usuario_sesion.php');

require_once('../descuento/Descuento.class.php');
$oDescuento = new Descuento();
require_once('../upload/Upload.class.php');
$oUpload = new Upload();
require_once('../funciones/fechas.php');

$personal_usuario_id = isset($_POST['usuario_id']) ? intval($_POST['usuario_id']) : $personal_usuario_id;

$tabla_contratos = '';
$result = $oDescuento->listar_descuentos($personal_usuario_id);
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tipo_descuento = 'SIN TIPO';
      if(intval($value['tb_descuento_tip']) == 1)
        $tipo_descuento = 'Renta';
      if(intval($value['tb_descuento_tip']) == 2)
        $tipo_descuento = 'Regularizar Créditos';
      if(intval($value['tb_descuento_tip']) == 3)
        $tipo_descuento = 'Otro tipo';

      $tabla_descuentos .= '
        <tr>
          <td>'.$value['tb_descuento_id'].'</td>
          <td>'.mostrar_fecha($value['tb_descuento_fecini']).'</td>
          <td>'.mostrar_fecha($value['tb_descuento_fecfin']).'</td>
          <td>'.$tipo_descuento.'</td>
          <td>'.$value['tb_descuento_mon'].'</td>
          <td>'.$value['tb_descuento_det'].'</td>
          <td>
            <a class="btn btn-warning btn-xs" onclick="descuento_form(\'M\', '.$value['tb_descuento_id'].')"><i class="fa fa-pencil"></i></a> 
            <a class="btn btn-danger btn-xs" onclick="descuento_form(\'E\', '.$value['tb_descuento_id'].')"><i class="fa fa-trash"></i></a> 
          </td>
        </tr>
      ';
    }
    
  }
$result = NULL;
?>

<table class="table table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Fecha Inicio</th>
      <th>Fecha Fin</th>
      <th>Tipo Descuento</th>
      <th>Monto Descuento</th>
      <th>Detalle</th>
      <th style="width: 8%;">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tabla_descuentos;?>
  </tbody>
</table>