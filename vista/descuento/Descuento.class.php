<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class Descuento extends Conexion
{

  public $descuento_id; 
  public $usuario_id;
  public $descuento_fec; 
  public $descuento_tip; 
  public $descuento_fecini; 
  public $descuento_fecfin; 
  public $descuento_mon;
  public $descuento_det;

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $columns = [
        'tb_usuario_id',
        'tb_descuento_fec',
        'tb_descuento_tip',
        'tb_descuento_fecini', 
        'tb_descuento_fecfin',
        'tb_descuento_mon',
        'tb_descuento_det'
      ];

      // Lista de placeholders
      $placeholders = implode(',', array_map(function ($column) {
        return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_descuento (" . implode(',', $columns) . ") VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_descuento_fec", $this->descuento_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_tip", $this->descuento_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_descuento_fecini", $this->descuento_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_fecfin", $this->descuento_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_mon", $this->descuento_mon, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_det", $this->descuento_det, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_descuento SET 
            tb_descuento_tip = :tb_descuento_tip,
            tb_descuento_fecini = :tb_descuento_fecini, 
            tb_descuento_fecfin = :tb_descuento_fecfin,
            tb_descuento_mon = :tb_descuento_mon,
            tb_descuento_det = :tb_descuento_det
            WHERE tb_descuento_id = :tb_descuento_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_descuento_tip", $this->descuento_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_descuento_fecini", $this->descuento_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_fecfin", $this->descuento_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_mon", $this->descuento_mon, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_det", $this->descuento_det, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_descuento_id", $this->descuento_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function eliminar($descuento_id)
  {
    $this->dblink->beginTransaction();
    try {

      $sql = "UPDATE tb_descuento SET tb_descuento_xac = 0 WHERE tb_descuento_id = :tb_descuento_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_descuento_id", $descuento_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  function mostrarUno($descuento_id)
  {
    try {
      $sql = "SELECT * FROM tb_descuento WHERE tb_descuento_id =:descuento_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":descuento_id", $descuento_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_descuentos($usuario_id)
  {
    try {
      $sql = "SELECT * FROM tb_descuento 
        WHERE tb_usuario_id =:usuario_id  ORDER BY tb_descuento_fecini DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function listar_descuentos_peridio($usuario_id, $fecha)
  {
    try {
      $sql = "SELECT * FROM tb_descuento 
        WHERE tb_usuario_id =:usuario_id AND :fecha BETWEEN tb_descuento_fecini AND tb_descuento_fecfin";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_campo($descuento_id, $descuento_columna, $descuento_valor, $param_tip){
    $this->dblink->beginTransaction();
    try {
      if (!empty($descuento_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_descuento SET " . $descuento_columna . " =:descuento_valor WHERE tb_descuento_id =:descuento_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":descuento_id", $descuento_id, PDO::PARAM_INT);
        if ($param_tip == 'INT') {
          $sentencia->bindParam(":descuento_valor", $descuento_valor, PDO::PARAM_INT);
        } else {
          $sentencia->bindParam(":descuento_valor", $descuento_valor, PDO::PARAM_STR);
        }

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
}
