<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../descuento/Descuento.class.php');
  $oDescuento = new Descuento();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'descuento';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $descuento_id = $_POST['descuento_id'];
  $personal_usuario_id = $_POST['usuario_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Descuento u Obligación del Personal';
  }
  elseif ($usuario_action == 'I') {
      $titulo = 'Registrar Nuevo Descuento u Obligación';
  } elseif ($usuario_action == 'M') {
      $titulo = 'Editar Descuento u Obligación del Personal';
  } elseif ($usuario_action == 'E') {
      $titulo = 'Eliminar Descuento u Obligación del Personal';
  } else {
      $titulo = 'Acción de Usuario Desconocido';
  }

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cliente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'descuento'; $modulo_id = $descuento_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $bandera = 4;
          $mensaje =  'No tienes el permiso para la acción que deseas realizar: '.$action;
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cliente por su ID
    $descuento_tip = 0;
    $descuento_fecini = date('01-m-Y');
    $descuento_fecfin = date('d-m-Y');

    if(intval($descuento_id) > 0){
      $result = $oDescuento->mostrarUno($descuento_id);
        if($result['estado'] == 1){
          $usuario_id = $result['data']['tb_usuario_id'];
          $descuento_fec = $result['data']['tb_descuento_fec'];
          $descuento_tip = $result['data']['tb_descuento_tip'];
          $descuento_fecini = mostrar_fecha($result['data']['tb_descuento_fecini']);
          $descuento_fecfin = mostrar_fecha($result['data']['tb_descuento_fecfin']);
          $descuento_mon = $result['data']['tb_descuento_mon'];
          $descuento_det = $result['data']['tb_descuento_det'];
        }
      $result = NULL;
    } 
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_descuento" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_descuento" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_descuento_id" value="<?php echo $descuento_id;?>">
          <input type="hidden" name="hdd_usuario_id" value="<?php echo $personal_usuario_id;?>">
          <input type="hidden" id="hdd_vista" name="hdd_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_descuento_fecini" class="control-label">Fecha de Inicio</label>
                  <div class="input-group date" id="datetimepicker1">
                    <input type="text" name="txt_descuento_fecini" id="txt_descuento_fecini" class="form-control input-sm" value="<?php echo $descuento_fecini;?>">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group personal_ocultar fecha_finaliza">
                  <label for="txt_descuento_fecfin" class="control-label">Fecha de Finalización</label>
                  <div class="input-group date" id="datetimepicker2">
                    <input type="text" name="txt_descuento_fecfin" id="txt_descuento_fecfin" class="form-control input-sm" value="<?php echo $descuento_fecfin;?>">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_descuento_tip" class="control-label">Tipo o Estado</label>
                  <select class="form-control input-sm" id="cmb_descuento_tip" name="cmb_descuento_tip">
                    <option value="">Selecciona...</option>
                    <option value="1" <?php if($descuento_tip == 1) echo 'selected';?> >Renta de Quinta</option>
                    <option value="2" <?php if($descuento_tip == 2) echo 'selected';?> >Regularizar Créditos</option>
                    <option value="3" <?php if($descuento_tip == 3) echo 'selected';?> >Otro tipo</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_descuento_mon" class="control-label">Monto de Descuento</label>
                  <input type="text" class="form-control input-sm moneda" id="txt_descuento_mon" name="txt_descuento_mon" value="<?php echo $descuento_mon;?>" >
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="txt_descuento_det" class="control-label">Detalle Descuento</label>
                  <input type="text" class="form-control input-sm" id="txt_descuento_det" name="txt_descuento_det" value="<?php echo $descuento_det;?>" >
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Descuento u Obligación?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cliente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>

          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_descuento">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/descuento/descuento_form.js?ver=1';?>"></script>
