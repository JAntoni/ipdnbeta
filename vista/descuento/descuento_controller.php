<?php
require_once('../../core/usuario_sesion.php');

require_once('../descuento/Descuento.class.php');
$oDescuento = new Descuento();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];

$oDescuento->usuario_id = intval($_POST['hdd_usuario_id']);
$oDescuento->descuento_fec = date('Y-m-d');
$oDescuento->descuento_tip = intval($_POST['cmb_descuento_tip']); 
$oDescuento->descuento_fecini = fecha_mysql($_POST['txt_descuento_fecini']); 
$oDescuento->descuento_fecfin = fecha_mysql($_POST['txt_descuento_fecfin']); 
$oDescuento->descuento_mon = moneda_mysql($_POST['txt_descuento_mon']); 
$oDescuento->descuento_det = $_POST['txt_descuento_det'];

if ($action == 'insertar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El Descuento ha sido registrado correctamente';

  $oDescuento->insertar();

  echo json_encode($data);
}
elseif ($action == 'modificar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El Descuento ha sido Modificado correctamente';

  $oDescuento->descuento_id = intval($_POST['hdd_descuento_id']);
  $oDescuento->modificar();

  echo json_encode($data);
}
elseif ($action == 'eliminar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El Descuento ha sido ELIMINADO correctamente';

  $oDescuento->eliminar(intval($_POST['hdd_descuento_id']));

  echo json_encode($data);
}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
?>