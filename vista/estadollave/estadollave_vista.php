<?php
$ellave_fec1 = date('01-m-Y');
$ellave_fec2 = date('d-m-Y');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="estadollave_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de estado de llavees...</h4>
				</div>
				<!-- inputs para filtrado de fechas -->
				<form id="form_creditomenor_filtro" class="form-inline" role="form">
					<div class="form-group">
						<a class="btn btn-primary btn-sm" onclick="estadollave_form('I',0)"><i class="fa fa-plus"></i> Nuevo estado llave</a>
					</div>
					<div class="form-group">

						<div class='input-group date' id='datetimepicker1'>
							<input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $ellave_fec1; ?>" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<div class='input-group date' id='datetimepicker2'>
							<input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $ellave_fec2; ?>" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<button type="button" class="btn btn-info btn-sm" onclick="estadollave_tabla()"><i class="fa fa-search"></i></button>
				</form>

				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="estadollave_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-12">
						<!-- Input para guardar el valor ingresado en el search de la tabla-->
						<input type="hidden" id="hdd_datatable_fil">

						<div id="div_estadollave_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php //require_once('estadollave_tabla.php'); 
							?>
						</div>
					</div>
				</div>
			</div>

			<div id="div_modal_estadollave_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<div id="div_estadollave_historial_form"></div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>

			<!-- Div para la data de las imagenes-->
			<div id="div_modal_estadollave_upload"></div>

		</div>
	</section>
	<!-- /.content -->
</div>