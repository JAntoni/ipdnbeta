
function motivo_mostrar_ocultar() {
	var cliente_tip = $('input[name="txt_estadollave_tip"]:checked').val();

	/* var estadollave_vista = $('#estadollave_vista').val(); */

	if (parseInt(cliente_tip) == 1)
		$('.estado_salida_llave').show();
	else
		$('.estado_salida_llave').hide();
}


function mostrar_ocultar_documentos() {
    var tipo_llave = $('select[name="cmb_estado"]').val(); // Obtener el valor seleccionado del combo box
    
    if (parseInt(tipo_llave) === 4) {
        $('.shadow_ocultar').show(); // Mostrar elementos con la clase 'documentos'
    } else {
        $('.shadow_ocultar').hide(); // Ocultar elementos con la clase 'documentos'
    }
}



$(document).ready(function () {
	//$('.shadow_ocultar').hide();
	$('#cmb_tipo').change();
	motivo_mostrar_ocultar();
	

	

	//console.log('estadio llave form 11111');
	upload_lista_docs();

	$("#txt_llave_clienteid")
    .autocomplete({
		minLength: 1,
		source: function (request, response) {
		  $.getJSON(
			VISTA_URL + "cliente/cliente_autocomplete.php",
			{ term: request.term }, //1 solo clientes de la empresa
			response
		  );
		},
		select: function (event, ui) {
		  $("#txt_llave_clienteid").val(ui.item.cliente_nom);
		  $("#hdd_estadollave_clienteid").val(ui.item.cliente_id);
		  credito_cliente_select(ui.item.cliente_id,0);// cuando el cliente esta insertando, no hay datos preseleccionados por eso el valor es 0, en las otras acciones si hay valores preseleccionados por eso el valor es diferente de 0
		  event.preventDefault();
		},
	  });


	$('input[name="txt_estadollave_tip"]').on('ifChecked', function (event) {
        if (parseInt($(this).val()) == 1)
            $('.estado_salida_llave').show(300);
        else
            $('.estado_salida_llave').hide(300);
    });

	/*$('select[name="cmb_tipo"]').on('change', function() {
		var tipo_llave = $(this).val(); // Obtener el valor seleccionado del combo box
		
		if (parseInt(tipo_llave) === 4 ) {
			$('.documentos').show(300); // Mostrar elementos con la clase 'documentos'
		} else {
			$('.documentos').hide(300); // Ocultar elementos con la clase 'documentos'
		}
	});*/
	
	
	
	$("#txt_estadollave_usurecibe").autocomplete({
        minLength: 1,
        source: function(request, response){
			$.getJSON(
			  VISTA_URL+"usuario/usuario_autocomplete.php",
			  {term: request.term}, //1 solo clientes de la empresa
			  response
			);
		  },
        select: function (event, ui) {
            $('#txt_estadollave_usurecibe').val(ui.item.tb_usuario_nom + " " + ui.item.tb_usuario_ape);
            $("#hdd_estadollave_usurecibe_id").val(ui.item.tb_usuario_id);
            $("#hdd_estadollave_clienteid").val(ui.item.tb_cliente_id);
            $("#hdd_estadollave_llaveid").val(ui.item.llave);
            event.preventDefault();
        }
    })
    .keyup(function () {
		console.log(VISTA_URL+"usuario/usuario_autocomplete.php");
        if ($('#txt_estadollave_usurecibe').val() === '') {
            $('#hdd_estadollave_usurecibe_id').val('');
        }
    });
	$('input[type="radio"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	/*$("#radio_persnat, #radio_persjur, ins").click(function () {
    $("#txt_llave_tip-error").css("display", "none");
  });*/


	$('#form_estadollave').validate({
		submitHandler: function () {
			var usuario_recibe = $('#hdd_estadollave_usurecibe_id').val();
			
			if(!usuario_recibe){
				alerta_warning('Importante', 'Debes digitar el usuario y seleccionarlos de la lista que aparezca')
				return false;
			}

			$.ajax({
				type: "POST",
				url: VISTA_URL + "estadollave/estadollave_controller.php",
				async: true,
				dataType: "json",
				data: serializar_form_estadollave(),
				beforeSend: function () {
					$('#estadollave_mensaje').show(400);
					$('#btn_guardar_estadollave').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						$('#estadollave_mensaje').removeClass('callout-info').addClass('callout-success')
						$('#estadollave_mensaje').html(data.mensaje);

						
						setTimeout(function () {
								estadollave_tabla();
							$('#modal_registro_estadollave').modal('hide');
							}
						, 1000
						);
					}
					
					else {
						$('#estadollave_mensaje').removeClass('callout-info').addClass('callout-warning')
						$('#estadollave_mensaje').html('Alerta: ' + data.mensaje);
						$('#btn_guardar_estadollave').prop('disabled', false);
					}
				},
				complete: function (data) {
					//console.log(data);
				},
				error: function (data) {
					$('#estadollave_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#estadollave_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
		},
		rules: {
			txt_estadollave_tip: {
				required: true,
				minlength: 1
			},
			cmb_motivo_salida: {
				min: 1
			},

			txt_estadollave_usurecibe:{
				required: true,
			},

			txt_llave_clienteid:{
				required: true,
			},

			cmb_llave: {
				min: 1
			},

			cmb_estado:{
				min:1
			}

		},
		messages: {
			txt_estadollave_tip: {
				required: "Este campo es requerido",
				minlength: "Seleccione el Motivo de estado"
			},
			cmb_motivo_salida: {
				min: "Seleccione alguna opción",
			},
			txt_estadollave_usurecibe: {
				required: "Asigne usuario que recibe la llave",
			},
			txt_llave_clienteid: {
				required: "Asigne un cliente",
			},
			cmb_llave: {
				min: "Ingrese una opción válida"
			},
			cmb_estado: {
				min: "Ingrese una opción válida"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "radio") {
				error.insertAfter($("#radio_persjur"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	var action= $("#action").val();

  if (action != 'insertar') {

    credito_cliente_select($("#hdd_estadollave_clienteid").val(), $("#hdd_estadollave_llaveid").val());

	if(action=='modificar'){

		disabled($('#form_estadollave').find('input:text, select').not($('#radio_persnat, #radio_persjur, #cmb_motivo_salida,#cmb_tipo')));
	}
	else if(action=='eliminar'){
		disabled($('#form_estadollave').find('input, select, textarea'));
  }
}

	/*Funcion Para seleccionar credito de cliente*/
	
});

$('#cmb_tipo').change(function(){
	//console.log("X");
	let estado = $(this).val();
	let action =$("#action").val();
	if (parseInt(estado) === 4 && action=='modificar') {
		//console.log("Y");
		$('.shadow_ocultar').show(); // Mostrar elementos con la clase 'documentos'
	} else {
		//console.log("C");
		$('.shadow_ocultar').hide(); // Ocultar elementos con la clase 'documentos'
	}
})

function serializar_form_estadollave() {
	var extra = `&tipo_nombre_nuevo=`+ $('#cmb_tipo option:selected').text();

    var elementos_disabled = $('#form_estadollave').find('input:disabled, select:disabled').removeAttr('disabled');
    var form_serializado = $('#form_estadollave').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

function credito_cliente_select(cliente_id,llaveid) {
	//console.log(cliente_id,llaveid);
	$.ajax({
	  type: "POST",
	  url: VISTA_URL + "estadollave/estadollave_cliente_select.php",
	  async: true,
	  dataType: "html",
	  data: {
		cliente_id: cliente_id, //estos datos se envian con el metodo post
		llaveid: llaveid
	  },
	  beforeSend: function () {
		$("#cmb_llave").html("<option>Cargando...</option>");
	  },
	  success: function (data) {
		$("#cmb_llave").html(data);
		$("#cmb_llave").change();
	  },
	  complete: function (data) {
		//console.log(data);
	  },
	  error: function (data) {
		alert(data.responseText);
	  },
	});
  }

  



