<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Estadollave extends Conexion
{

    public $estadollave_id;
    public $estadollave_xac;
    public $estadollave_reg;
    public $estadollave_motivo;
    public $estadollave_usuentrega;
    public $estadollave_usurecibe;
    public $estadollave_obs;
    public $estadollave_tipo;
    public $motivollave_id;
    public $llaveid;
    public $cliente_id;
    public $llave_numcasillero;
    public $credito_id;

    function insertar($motivo_id)
    {
        $this->dblink->beginTransaction();

        try {
            $sql = "INSERT INTO tb_estadollave (
                    tb_estadollave_motivo,
                    tb_estadollave_usuentrega,
                    tb_estadollave_usurecibe,
                    tb_llave_id,
                    tb_estadollave_obs,
                    tb_estadollave_tipo,
                    tb_motivollave_id
                )
                VALUES (
                    :estadollave_motivo,
                    :estadollave_usuentrega,
                    :estadollave_usurecibe,
                    :llave_id,
                    :estadollave_obs,
                    :estadollave_tipo,
                    :motivollave_id
                );";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":estadollave_motivo", $this->estadollave_motivo, PDO::PARAM_STR);
            $sentencia->bindParam(":estadollave_usuentrega", $this->estadollave_usuentrega, PDO::PARAM_INT);
            $sentencia->bindParam(":estadollave_usurecibe", $this->estadollave_usurecibe, PDO::PARAM_INT);
            $sentencia->bindParam(":llave_id", $this->llaveid, PDO::PARAM_INT);
            $sentencia->bindParam(":estadollave_obs", $this->estadollave_obs, PDO::PARAM_STR);
            $sentencia->bindParam(":estadollave_tipo", $this->estadollave_tipo, PDO::PARAM_STR);
            $sentencia->bindParam(":motivollave_id", $motivo_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $estadollave_id = $this->dblink->lastInsertId();

            $this->dblink->commit();

            $data['estado'] = $result;
            $data['estadollave_id'] = $estadollave_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertar_Recepcion()
    {
        return $this->insertar(null);
    }

    function insertar_Salida()
    {
        return $this->insertar($this->motivollave_id);
    }



    function modificar($tipo_modificacion)
    {
        $this->dblink->beginTransaction();

        try {
            $sql = "UPDATE tb_estadollave
                    SET
                    tb_estadollave_motivo = :estadollave_motivo,
                    tb_estadollave_usuentrega = :estadollave_usuentrega,
                    tb_estadollave_usurecibe = :estadollave_usurecibe,
                    tb_estadollave_obs = :estadollave_obs,
                    tb_estadollave_tipo = :estadollave_tipo";

            if ($tipo_modificacion == 'Recepcion') {
                $sql .= ", tb_llave_id = :param6";
            } elseif ($tipo_modificacion == 'Salida') {
                $sql .= ", tb_motivollave_id = :param6";
            }

            $sql .= " WHERE tb_estadollave_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':estadollave_motivo', $this->estadollave_motivo, PDO::PARAM_STR);
            $sentencia->bindParam(':estadollave_usuentrega', $this->estadollave_usuentrega, PDO::PARAM_INT);
            $sentencia->bindParam(':estadollave_usurecibe', $this->estadollave_usurecibe, PDO::PARAM_INT);
            $sentencia->bindParam(':estadollave_obs', $this->estadollave_obs, PDO::PARAM_STR);
            $sentencia->bindParam(':estadollave_tipo', $this->estadollave_tipo, PDO::PARAM_STR);

            if ($tipo_modificacion == 'Recepcion') {
                $sentencia->bindParam(':param6', $this->llaveid, PDO::PARAM_INT);
            } elseif ($tipo_modificacion == 'Salida') {
                $sentencia->bindParam(':param6', $this->motivollave_id, PDO::PARAM_INT);
                //$sentencia->bindParam(':param7', $this->llaveid, PDO::PARAM_INT);
            }

            $sentencia->bindParam(':paramx', $this->estadollave_id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();

            if ($resultado == 1) {
                $this->dblink->commit();
            }

            return $resultado; //si es correcto retorna 1
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_Recepcion()
    {
        return $this->modificar('Recepcion');
    }

    function modificar_Salida()
    {
        return $this->modificar('Salida');
    }


    function eliminar($estadollave_id)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_estadollave 
            SET tb_estadollave_xac = 0 
            WHERE tb_estadollave_id = :estadollave_id;
            ";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":estadollave_id", $estadollave_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($estadollave_id)
    {
        try {
            $sql = "SELECT
            tll.tb_estadollave_id,
            tll.tb_estadollave_motivo,
            mtll.tb_motivollave_id,
            llav.tb_cliente_id as clienteid,
            CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) AS Usuario_entrega,
            CONCAT(u1.tb_usuario_nom, ' ', u1.tb_usuario_ape) AS Usuario_recibe,
            DATE(tll.tb_estadollave_reg) AS FECHA,
            DATE_FORMAT(tll.tb_estadollave_reg, '%r') AS HORA, -- Formato AM/PM
            llav.tb_llave_id as llave,
            llav.tb_llave_numcasillero as Casillero,
            llav.tb_credito_id as Credito,
            cli.tb_cliente_nom as Nombre_Cliente,
            tll.tb_estadollave_obs AS OBSERVACION,
            tll.tb_estadollave_tipo AS TIPO,
            CASE
                WHEN tll.tb_estadollave_motivo = 1 THEN 'Salida'
                WHEN tll.tb_estadollave_motivo = 2 THEN 'Recepción'
            END AS MOTIVO,
            CASE
                WHEN tll.tb_estadollave_tipo = 1 THEN 'Almacenada'
                WHEN tll.tb_estadollave_tipo = 2 THEN 'En manos de asesor'
                WHEN tll.tb_estadollave_tipo = 3 THEN 'Pendiente de entrega'
                WHEN tll.tb_estadollave_tipo = 4 THEN 'Entregado/Liquidado'
            END AS TIPO_Nombre,
            tll.tb_estadollave_usurecibe
        FROM
            tb_estadollave tll
        INNER JOIN
            tb_usuario u ON tll.tb_estadollave_usuentrega = u.tb_usuario_id 
        INNER JOIN    
            tb_usuario u1 ON tll.tb_estadollave_usurecibe = u1.tb_usuario_id
        LEFT JOIN 
            tb_llave llav on tll.tb_llave_id=llav.tb_llave_id
        LEFT JOIN 
            tb_cliente cli on llav.tb_cliente_id=cli.tb_cliente_id
        LEFT JOIN 
            tb_motivollave mtll on tll.tb_motivollave_id=mtll.tb_motivollave_id
        WHERE 
            tb_estadollave_id = :estadollave_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":estadollave_id", $estadollave_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay llaves registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarUnoLlaveId($llaveid, $creditotipo_id){
        try {
            if (intval($creditotipo_id) == 1) {
                $nombre_tabla = 'tb_creditomenor';
                $sql = "SELECT  llav.tb_llave_id as id,
                c.tb_cliente_nom as Nombre_Cliente,
                c.tb_cliente_empruc as ruc_empresa,
                c.tb_cliente_emprs as nombre_empresa,
                c.tb_cliente_nom as cliente_empresa_nombre,
                c.tb_cliente_doc as dni_cliente_empresa,
                c.tb_cliente_dir as direccion,
                c.tb_cliente_numpar as numparcliente,
                estadollav.tb_estadollave_obs as obs,
                c.tb_cliente_id as clienteid,
                c.tb_cliente_doc as dni,
                vc.tb_vehiculoclase_nom as clase_vehiculo,
                vm.tb_vehiculomarca_nom as marca_vehiculo,
                gar.tb_garantia_vehpla as placa,
                gar.tb_garantia_vehsercha numero_serie,
                gar.tb_garantia_vehsermot numero_motor
                FROM tb_estadollave estadollav
                INNER JOIN tb_llave llav ON estadollav.tb_llave_id = llav.tb_llave_id
                INNER JOIN tb_creditomenor cgv ON llav.tb_credito_id = cgv.tb_credito_id
                INNER JOIN tb_garantia gar ON gar.tb_credito_id = cgv.tb_credito_id
                LEFT JOIN tb_cliente c ON c.tb_cliente_id = llav.tb_cliente_id
                LEFT JOIN tb_vehiculoclase vc ON vc.tb_vehiculoclase_id = llav.tb_vehiculoclase_id
                LEFT JOIN tb_vehiculomarca vm ON vm.tb_vehiculomarca_id = llav.tb_vehiculomarca_id
                WHERE llav.tb_llave_id = :llave_id AND (gar.tb_garantiatipo_id = 3 and gar.tb_articulo_id=75)";
            } elseif (intval($creditotipo_id) == 2) {
                $nombre_tabla = 'tb_creditoasiveh';

                $sql = "SELECT  llav.tb_llave_id as id,
                c.tb_cliente_nom as Nombre_Cliente,
                c.tb_cliente_empruc as ruc_empresa,
                c.tb_cliente_emprs as nombre_empresa,
                c.tb_cliente_nom as cliente_empresa_nombre,
                c.tb_cliente_doc as dni_cliente_empresa,
                c.tb_cliente_dir as direccion,
                c.tb_cliente_numpar as numparcliente,
                estadollav.tb_estadollave_obs as obs,
                c.tb_cliente_id as clienteid,
                c.tb_cliente_doc as dni,
                vc.tb_vehiculoclase_nom as clase_vehiculo,
                vm.tb_vehiculomarca_nom as marca_vehiculo,
                casv.tb_credito_vehpla as placa,
                casv.tb_credito_vehsercha numero_serie,
                casv.tb_credito_vehsermot numero_motor
                FROM tb_estadollave estadollav
                INNER JOIN tb_llave llav ON estadollav.tb_llave_id = llav.tb_llave_id
                INNER JOIN tb_creditoasiveh casv ON llav.tb_credito_id = casv.tb_credito_id
                LEFT JOIN tb_cliente c ON c.tb_cliente_id = llav.tb_cliente_id
                LEFT JOIN tb_vehiculoclase vc ON vc.tb_vehiculoclase_id = llav.tb_vehiculoclase_id
                LEFT JOIN tb_vehiculomarca vm ON vm.tb_vehiculomarca_id = llav.tb_vehiculomarca_id
                WHERE 
                llav.tb_llave_id = :llave_id";
            } elseif (intval($creditotipo_id) == 3) {
                $nombre_tabla = 'tb_creditogarveh';

                $sql = "SELECT  llav.tb_llave_id as id,
                    c.tb_cliente_nom as Nombre_Cliente,
                    c.tb_cliente_empruc as ruc_empresa,
                    c.tb_cliente_emprs as nombre_empresa,
                    c.tb_cliente_nom as cliente_empresa_nombre,
                    c.tb_cliente_doc as dni_cliente_empresa,
                    c.tb_cliente_dir as direccion,
                    c.tb_cliente_numpar as numparcliente,
                    estadollav.tb_estadollave_obs as obs,
                    c.tb_cliente_id as clienteid,
                    c.tb_cliente_doc as dni,
                    vc.tb_vehiculoclase_nom as clase_vehiculo,
                    vm.tb_vehiculomarca_nom as marca_vehiculo,
                    cgv.tb_credito_vehpla as placa,
                    c.tb_zona_id as cliente_zona_id,
                    cgv.tb_credito_vehsercha numero_serie,
                    cgv.tb_credito_vehsermot numero_motor
                    FROM tb_estadollave estadollav
                    INNER JOIN tb_llave llav ON estadollav.tb_llave_id = llav.tb_llave_id
                    INNER JOIN $nombre_tabla cgv ON llav.tb_credito_id = cgv.tb_credito_id 
                    LEFT JOIN tb_cliente c ON c.tb_cliente_id = llav.tb_cliente_id
                    LEFT JOIN tb_vehiculoclase vc ON vc.tb_vehiculoclase_id = llav.tb_vehiculoclase_id
                    LEFT JOIN tb_vehiculomarca vm ON vm.tb_vehiculomarca_id = llav.tb_vehiculomarca_id
                    WHERE 
                    llav.tb_llave_id = :llave_id";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":llave_id", $llaveid, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Estados de llave registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }





    function listar_estadollave($fecha1, $fecha2)
{
    try {
        $sql = "SELECT
            tll.tb_estadollave_id,
            llav.tb_llave_id,
            llav.tb_creditotipo_id,
            tll.tb_estadollave_motivo,
            llav.tb_cliente_id as clienteid,
            mtll.tb_motivollave_nom AS Salida_llave,
            CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) AS Usuario_entrega,
            CONCAT(u1.tb_usuario_nom, ' ', u1.tb_usuario_ape) AS Usuario_recibe,
            DATE(tll.tb_estadollave_reg) AS FECHA,
            DATE_FORMAT(tll.tb_estadollave_reg, '%r') AS HORA, -- Formato AM/PM
            llav.tb_llave_id as llave,
            llav.tb_llave_numcasillero as Casillero,
            llav.tb_credito_id as Credito,
            cli.tb_cliente_nom as Nombre_cliente,
            cli.tb_cliente_emprs,
            tll.tb_estadollave_obs AS OBSERVACION,
            tll.tb_estadollave_tipo,
            CASE
                WHEN tll.tb_estadollave_motivo = 1 THEN 'Salida'
                WHEN tll.tb_estadollave_motivo = 2 THEN 'Recepción'
            END AS MOTIVO,
            CASE
                WHEN tll.tb_estadollave_tipo = 1 THEN 'Almacenada'
                WHEN tll.tb_estadollave_tipo = 2 THEN 'En manos de asesor'
                WHEN tll.tb_estadollave_tipo = 3 THEN 'Pendiente de entrega'
                WHEN tll.tb_estadollave_tipo = 4 THEN 'Entregado/Liquidado'
            END AS TIPO,
            CASE
            WHEN llav.tb_creditotipo_id = '1' THEN 'CREDITO MENOR' 
            WHEN llav.tb_creditotipo_id = '2' THEN 'CREDITO ASCVEH' 
            WHEN llav.tb_creditotipo_id = '3' THEN 'CREDITO GARVEH' 
            END as tipo_credito
        FROM
            tb_estadollave tll
        INNER JOIN
            tb_usuario u ON tll.tb_estadollave_usuentrega = u.tb_usuario_id 
        INNER JOIN    
            tb_usuario u1 ON tll.tb_estadollave_usurecibe = u1.tb_usuario_id
        LEFT JOIN 
            tb_llave llav on tll.tb_llave_id=llav.tb_llave_id
        LEFT JOIN 
            tb_cliente cli on llav.tb_cliente_id=cli.tb_cliente_id
        LEFT JOIN 
            tb_motivollave mtll on tll.tb_motivollave_id=mtll.tb_motivollave_id
        WHERE 
            tll.tb_estadollave_xac = 1 AND DATE(tll.tb_estadollave_reg) BETWEEN :fecha1 AND :fecha2;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $retorno["cantidad_resultados"] = $sentencia->rowCount();
            $sentencia->closeCursor(); //para liberar memoria de la consulta
        } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay estados de llave registradas";
            $retorno["data"] = "";
        }

        return $retorno;
    } catch (Exception $e) {
        throw $e;
    }
}


    function Estadollave_autocomplete($dato)
    {
        try {

            $filtro = "%" . $dato . "%";

            $sql = "SELECT *
		    FROM tb_estadollave 
                WHERE tb_estadollave_motivo LIKE :filtro OR tb_estadollave_tipo LIKE :filtro";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay estados de llave registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function llenarComboLlave($cliente_id)
    {
        try {
            $sql = " SELECT llav.tb_llave_id as llaveid,llav.tb_llave_numcasillero as casillero,
            llav.tb_credito_id as credito, cli.tb_cliente_nom as cliente 
             from tb_llave llav
             inner join tb_cliente cli on llav.tb_cliente_id=cli.tb_cliente_id
             where llav.tb_cliente_id=$cliente_id and llav.tb_llave_xac=1;
            ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad_resultados"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se puede listar combo";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
