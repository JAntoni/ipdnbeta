var datatable_global;

function estadollave_form(usuario_act, estadollave_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "estadollave/estadollave_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      estadollave_id: estadollave_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      //console.log(data);
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_estadollave_form").html(data);
        $("#modal_registro_estadollave").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_estadollave"); //funcion encontrada en public/js/generales.js

        modal_height_auto("modal_registro_estadollave"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_estadollave", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "estadollave";
        var div = "div_modal_estadollave_form";
        permiso_solicitud(usuario_act, estadollave_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}
function estadollave_tabla() {

  $.ajax({
    type: "POST",
    url: VISTA_URL + "estadollave/estadollave_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha1: $("#txt_filtro_fec1").val(),
      fecha2: $("#txt_filtro_fec2").val()
    }),
    
    beforeSend: function () {
      $("#estadollave_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_estadollave_tabla").html(data);
      $("#estadollave_mensaje_tbl").hide(300);
      estilos_datatable();
    },
    complete: function (data) {},
    error: function (data) {
      $("#estadollave_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS ESTADO LLAVE: " + data.responseText
      );
    },
  });
}

function estilos_datatable() {
  datatable_global = $("#tbl_estadollave").DataTable({
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Página mostrada _PAGE_ de _PAGES_",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    columnDefs: [{ orderable: false, targets: [4, 6] }],
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
  $(".dataTables_filter input").attr("id", "txt_datatable_fil");

  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

$(document).ready(function () {

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //startDate: "-0d"
    //endDate: new Date()
  });

  estadollave_tabla();


  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    //estadollave_tabla();
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    //estadollave_tabla();
  });

  $("#btn_cerrar_upload").click(function (event) {
    estadollave_tabla();
  });


  $("#cmb_estado").change(function () {
    console.log("X");
    let estado = $(this).val();
    if (parseInt(estado) === 4) {
      console.log("Y");
      $(".documentos").show(); // Mostrar elementos con la clase 'documentos'
    } else {
      console.log("C");
      $(".documentos").hide(); // Ocultar elementos con la clase 'documentos'
    }
  });
});

function estadollave_historial_form(estadollave_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "estadollave/estadollave_historial_form.php",
    async: true,
    dataType: "html",
    data: {
      estadollave_id: estadollave_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_estadollave_historial_form").html(html); //se carga el html en la vista del php
      $("#div_modal_estadollave_historial_form").modal("show"); //es para el archivo de destino(URL)
      $("#modal_mensaje").modal("hide");

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal("div_modal_estadollave_historial_form", "limpiar"); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("div_modal_estadollave_historial_form"); //funcion encontrada en public/js/generales.js
    },
    complete: function (html) {},
  });
}

function upload_form(usuario_act, upload_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_form_docs.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      upload_id: upload_id,
      modulo_nom: "estadollave", //nombre de la tabla a relacionar
      modulo_id: upload_id, //aun no se guarda este modulo
      upload_uniq: $("#upload_uniq").val(), //ID temporal
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_estadollave_upload").html(data);
        $("#modal_upload_docs").modal("show");

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_upload_docs"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_upload_docs", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      swal_error("ERRROR", data.responseText, 5000);
    },
  });
}

function upload_lista_docs() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_lista_docs.php",
    async: true,
    dataType: "html",
    data: {
      modulo_id: $("#hdd_estadollave_id").val(),
      modulo_nom: "estadollave",
    },
    beforeSend: function () {
      $(".tabla_docs_estadollave").html(
        '<h3 class="box-title">Cargando documentos...</h3>'
      );
    },
    success: function (data) {
      $(".tabla_docs_estadollave").html(data);
    },
    complete: function (data) {
      console.log(data);
    },
    error: function (data) {
      $(".tabla_docs_estadollave").html(
        "ERROR AL CARGAR DATOS: " + data.responseText
      );
    },
  });
}

function GenerarActa(llaveid, casillero,tipo_credito_id) {
  Swal.fire({
    title: "¿SEGURO(A) QUE QUIERES GENERAR EL ACTA?",
    icon: "info",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "GENERAR ACTA",
    showDenyButton: true,
    denyButtonText: "CANCELAR",
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#21ba45",
  }).then((result) => {


    if (result.isConfirmed) {
      //MODAL PARA MODIFICAR DATOS
      reportePDF(llaveid, casillero,tipo_credito_id);
      //logica para liberar la llave
    } else if (result.isDenied) {
      //MODAL DE CAMBIO DE PROPIETARIO
      swal.close();
    }
  });
}

//aqui va la funcion para liberar el casillero

function reportePDF(llaveid, casillero,tipo_credito_id){
  //console.log(llaveid,casillero,tipo_credito_id);
  //return;
  //console.log(llaveid);
  //ACA TIENES QUE HACER TU LOGICA DEL REQ QUE TE PIDIERON
  $.ajax({
    type: "POST",
    url: VISTA_URL + "estadollave/estadollave_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "liberarcasillero",
      llaveid: llaveid,
      casillero: casillero,
      tipo_credito_id: tipo_credito_id,
    },
    beforeSend: function () {
      $(".tabla_docs_estadollave").html(
        '<h3 class="box-title">viendo...</h3>'
      );
    },
    success: function (data) {
      console.log(data);
      //$(".tabla_docs_estadollave").html(data);
      if (parseInt(data.estado) > 0) {
        $('#estadollave_mensaje').removeClass('callout-info').addClass('callout-success')
        $('#estadollave_mensaje').html(data.mensaje);
        setTimeout(function () {
          window.open("vista/estadollave/estadollave_actaentrega_pdf.php?llave_id=" + llaveid + "&tipo_credito_id=" + tipo_credito_id, "_blank");
        }
      , 1000
      );
      }
      else {
        $('#estadollave_mensaje').removeClass('callout-info').addClass('callout-warning')
        $('#estadollave_mensaje').html('Alerta: ' + data.mensaje);
      }
    },
    complete: function (data) {
      console.log(data);
    },
    error: function (data) {
      $(".tabla_docs_estadollave").html(
        "ERROR AL CARGAR DATOS: " + data.responseText
      );
    },
  });
  //ALGO ASI xd le mandas el parametro de la llave o el casillero y lo liberas y en el sucess tienes que mover el window.open de abajo
}
