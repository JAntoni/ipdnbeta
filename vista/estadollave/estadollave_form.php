<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../estadollave/Estadollave.class.php');
$oEstadollave = new Estadollave();
require_once('../funciones/funciones.php');

$direc = 'estadollave';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$estadollave_id = $_POST['estadollave_id'];

$titulo = '';
if ($usuario_action == 'L')
  $titulo = 'Estado llave Registrado';
if ($usuario_action == 'I')
  $titulo = 'Registrar Estado llave';
elseif ($usuario_action == 'M')
  $titulo = 'Editar estado llave';
elseif ($usuario_action == 'E')
  $titulo = 'Eliminar estado llave';
else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en estadollave
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'estadollave';
    $modulo_id = $estadollave_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }

  //si la accion es modificar, mostramos los datos del estadollave por su ID
  if (intval($estadollave_id) > 0) {
    $result = $oEstadollave->mostrarUno($estadollave_id);
    //print_r($result['data']);
    if ($result['estado'] != 1) {
      $mensaje =  'No se ha encontrado ningún registro para el estado llave seleccionado, inténtelo nuevamente.';
      $bandera = 4;
    } else {
      //se reciben los datos de la consulta y se asignan a  variables y se usa en el formulario
      $estadollave_recibeid = $result['data']['tb_estadollave_usurecibe'];
      $estadollave_motivo = $result['data']['tb_estadollave_motivo'];
      $estadollave_usuentrega = $result['data']['Usuario_entrega'];
      $estadollave_usurecibe = $result['data']['Usuario_recibe'];
      $tipo_nombre_actual = $result['data']['TIPO_Nombre'];
      $motivo_estado_actual = $result['data']['tb_motivollave_id'];

      //estos dos datos son los que se estan filtrando
      $llaveid = intval($result['data']['llave']);
      $clienteid = $result['data']['clienteid'];
      $cliente = $result['data']['Nombre_Cliente'];


      $motivollave_id = intval($result['data']['tb_motivollave_id']); //motivo de salida de llave
      $estadollave_obs = $result['data']['OBSERVACION'];
      $estadollave_tipo = intval($result['data']['TIPO']);
      $motivo_salida = $result['data']['tb_motivollave_nom'];
    }
    $result = NULL;
  }
} else {
  $mensaje =  $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_estadollave" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo; ?></h4>
        </div>
        <form id="form_estadollave" method="post">
          <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_estadollave_id" id="hdd_estadollave_id" value="<?php echo $estadollave_id; ?>">
          <input type="hidden" id="estadollave_vista" name="estadollave_vista" value="<?php echo $estadollave_vista; ?>">
          <!--salida o recepcion -->
          <input type="hidden" id="motivo_de_estado" name="motivo_de_estado" value="<?php echo $estadollave_motivo; ?>">

          <div class="modal-body">
            <!-- motivo salida -->
            <input type="hidden" name="motivo_estado_actual" id="motivo_estado_actual" value="<?php echo $motivo_estado_actual ?>">
            <div class="form-group" id="radio_grupo">
              
              <label for="txt_estadollave_tip" class="control-label">Motivo de Estado</label>
              <br>
              <label id="radio_persnat" class="radio-inline" style="padding-left: 0px;">
                <input type="radio" name="txt_estadollave_tip" class="flat-green" value="1" <?php if ($estadollave_motivo == 1) echo 'checked'; ?>> Salida
              </label>
              <label id="radio_persjur" class="radio-inline">
                <input type="radio" name="txt_estadollave_tip" class="flat-green" value="2" <?php if ($estadollave_motivo == 2) echo 'checked'; ?>> Recepción
              </label>
              <!--Boton para agregar documentos  -->



            </div>

            <!-- DATOS PARA MOTIVO DE ESTADO DE SALIDA-->
            <div class="form-group estado_salida_llave">
              <label for="cmb_ubigeo_coddep" class="control-label">Motivo de salida de llave:</label>
              <select id="cmb_motivo_salida" name="cmb_motivo_salida" class="form-control input-sm">
                <?php require_once('../motivollave/motivollave_select.php') ?>
              </select>
            </div>
            <!-- FIN DATOS PARA MOTIVO DE ESTADO DE SALIDA-->

            <div class="form-group">
              <label for="txt_estadollave_usuentrega">Usuario que entrega llave: </label>
              <input type="text" name="txt_estadollave_usuentrega" id="txt_estadollave_usuentrega" class="form-control input-sm" value="<?php echo $_SESSION['usuario_nombre'] . " " . $_SESSION['usuario_ape'];  ?>" disabled>
            </div>
            <div class="form-group">
              <label for="txt_estadollave_usurecibe" class="control-label">Usuario que recibe llave</label>
              <input type="text" name="txt_estadollave_usurecibe" id="txt_estadollave_usurecibe" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $estadollave_usurecibe; ?>">
              <input type="hidden" name="hdd_estadollave_usurecibe_id" id="hdd_estadollave_usurecibe_id" value="<?php echo $estadollave_recibeid; ?>">
            </div>

            <!-- Datos de cliente -->
            <div class="form-group">
              <label for="txt_llave_clienteid" class="control-label">Cliente:</label>
              <input type="text" name="txt_llave_clienteid" id="txt_llave_clienteid" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $cliente; ?>">
              <input type="hidden" name="hdd_estadollave_clienteid" id="hdd_estadollave_clienteid" value="<?php echo $clienteid ?>">
            </div>

            <!-- Información de llave -->
            <div class="form-group">
              <input type="hidden" name="hdd_estadollave_llaveid" id="hdd_estadollave_llaveid" value="<?php echo $llaveid ?>">
              <label>Información de llave: </label>
              <select id="cmb_llave" name="cmb_llave" class="form-control">
              </select>
            </div>

            <div class="form-group">
              <label for="txt_comentario">Observaciones:</label>
              <textarea class="form-control mayus" name="txta_obs" id="txt_comentario"><?php echo $estadollave_obs ?></textarea>
            </div>
            <div class="form-group">
              <input type="hidden" name="tipo_nombre_actual" id="tipo_nombre_actual" value="<?php echo $tipo_nombre_actual ?>">
              <label>Tipo:</label>
              <select id="cmb_tipo" name="cmb_estado" class="form-control" ;">
                <option value="1" <?php if ($estadollave_tipo == 1) echo 'selected'; ?>>Almacenada</option>
                <option value="2" <?php if ($estadollave_tipo == 2) echo 'selected'; ?>>En manos de asesor</option>
                <option value="3" <?php if ($estadollave_tipo == 3) echo 'selected'; ?>>Pendiente de entrega</option>
                <option value="4" <?php if ($estadollave_tipo == 4) echo 'selected'; ?>>Entregado/Liquidado</option>
              </select>
            </div>

            <!--Shadow para registrar documentos del cliente-->
            <div class="form-group shadow_ocultar">
              <div class="box box-primary shadow">
                <div class="box-header with-border">
                  <h3 class="box-title">Listado de Fotos y Documentos</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-primary btn-xs btn-file" onclick="upload_form('I', <?php echo $estadollave_id; ?>)">
                      Subir Imágenes
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <div class="box-body no-padding tabla_docs_estadollave">

                </div>
              </div>
            </div>
            <!--Shadow para registrar documentos del cliente-->
            <!--Botón para el acta-->
            
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este estado de llave?</h4>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="estadollave_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_estadollave">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_estadollave">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_estadollave">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/estadollave/estadollave_form.js?ver=1122'; ?>"></script>