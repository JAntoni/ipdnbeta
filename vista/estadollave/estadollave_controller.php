<?php
session_name("ipdnsac");
session_start();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../estadollave/Estadollave.class.php');
$oEstadollave = new Estadollave();
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../motivollave/MotivoLlave.class.php');
$oMotiv = new MotivoLlave();
require_once('../llave/Llave.class.php');
$oLlave = new Llave();
//$_SESSION['usuario_id']
$action = $_POST['action'];
$casillero = $_POST['casillero']?$_POST['casillero']:0;

$estadollave_motivo = intval($_POST['txt_estadollave_tip']);
$estadollave_usuentrega = $_SESSION['usuario_id']; //id del usuario
$estadollave_usurecibe = intval($_POST['hdd_estadollave_usurecibe_id']);
$llaveid = intval($_POST['cmb_llave']);
$cliente_id = $_POST['hdd_llave_clienteid']; //id del usuario
$estadollave_obs = trim(strtoupper($_POST['txta_obs']));
$estadollave_tipo = $_POST['cmb_estado'];
$motivollave_id = $_POST['cmb_motivo_salida'];

$data['estado'] = 0;
$data['mensaje'] = 'Existe un error al guardar estado llave';
//recogiendo los datos
$oEstadollave->estadollave_motivo = $estadollave_motivo;
$oEstadollave->estadollave_usuentrega = $estadollave_usuentrega;
$oEstadollave->estadollave_usurecibe = $estadollave_usurecibe;
$oEstadollave->llaveid = $llaveid;
$oEstadollave->estadollave_obs = $estadollave_obs;
$oEstadollave->estadollave_tipo = $estadollave_tipo;
$oEstadollave->motivollave_id = $motivollave_id;

if ($action == 'insertar') {

	if($estadollave_usurecibe <= 0 || !empty($cliente_id)){
		$data['mensaje'] = 'No se ha seleccionado el Usuario que recibe o el cliente: Usuario ID: '.$estadollave_usurecibe.' | Cliente ID: '.$cliente_id;
        echo json_encode($data);
        exit();
	}

    // Registrar estado de la llave (entrada o salida)
    if ($estadollave_motivo == 2) {
        $res = $oEstadollave->insertar_Recepcion();
    } else {
        $res = $oEstadollave->insertar_Salida();
    }

    if (intval($res['estado']) != 1) {
        $data['mensaje'] = 'Error al registrar estado de la llave.';
        echo json_encode($data);
        return;
    }

	//? EL ESTADO DE DEL MOVIMIENTO DEBE SER OTORGADO AL ESTADO DE LA MISMA LLAVE
	$oLlave->modificarestado($llaveid, $estadollave_tipo);

    // Registrar historial de la acción
    $oHist->setTbHistUsureg($_SESSION['usuario_id']);
    $oHist->setTbHistNomTabla('tb_estadollave');
    $oHist->setTbHistRegmodid($res['estadollave_id']);

    if ($estadollave_motivo == 2) {
        $mensaje = 'Registró la entrada de la llave ' . $llaveid . '. Con las observaciones: ' . $estadollave_obs;
    } else {
        $mensaje = 'Registró la salida de la llave ' . $llaveid . '. Con las observaciones: ' . $estadollave_obs;
    }

    $oHist->setTbHistDet($mensaje);
    $oHist->insertar();

    // Mostrar mensaje de éxito
    $data['estado'] = 1;
    $data['mensaje'] = 'Estado de la llave y el historial se registraron correctamente.';

    echo json_encode($data);
}
 elseif ($action == 'modificar') {

	
	$data['estado'] = 0;
	$data['mensaje'] = 'Estado de llave modificado correctamente. // lave id: ' + $llaveid; 

	$oModifcar = intval($_POST['hdd_estadollave_id']);
	//asigna al objeto
	$oEstadollave->estadollave_id = $oModifcar;

	if ($estadollave_motivo == 2) {
		$res = $oEstadollave->modificar_Recepcion();
		if (intval($res) == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Estado de llave modificado correctamente.';
		}
	}else{
		$res = $oEstadollave->modificar_Salida();
		if (intval($res) == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Estado de llave modificado correctamente.';
		}
	}

	//? EL ESTADO DE DEL MOVIMIENTO DEBE SER OTORGADO AL ESTADO DE LA MISMA LLAVE
	$oLlave->modificarestado($llaveid, $estadollave_tipo);

	if($data['estado'] == 1){
		/* ************* MOTIVO *************** */
		$motivoactual = "";
		switch ($_POST["motivo_de_estado"]) {
			case 1:
				$motivoactual = "Salida";
				break;
			case 2:
				$motivoactual = "Recepción";
				break;
			default:
				$motivoactual = "ERROR";
		}
		$motivonuevo = "";
		switch ($_POST["txt_estadollave_tip"]) {
			case 1:
				$motivonuevo = "Salida";
				break;
			case 2:
				$motivonuevo = "Recepción";
				break;
			default:
				$motivonuevo = "ERROR";
		}
		$oHist->setTbHistUsureg($_SESSION['usuario_id']);
		$oHist->setTbHistNomTabla('tb_estadollave');
		$oHist->setTbHistRegmodid($oModifcar);
		$mensaje = 'Modificó el Motivo de la llave a nuevo motivo: "' . $motivonuevo . '"'.'. Con las observaciones: ' . $estadollave_obs;
		$oHist->setTbHistDet($mensaje);
		$oHist->insertar();
		/* ************* MOTIVO *************** */

		/* ************* MOTIVO SALIDA *************** */
		if ($estadollave_motivo == 1){
			$motivosalidaactual = $oMotiv->mostrarUno($_POST["motivo_estado_actual"]);
			$motivosalidanuevo = $oMotiv->mostrarUno($_POST["cmb_motivo_salida"]);
			$mensaje = 'Modificó el Motivo de salida de la llave al nuevo motivo de salida: "' . $motivosalidanuevo['data']['tb_motivollave_nom'] . '"';
			$oHist->setTbHistDet($mensaje);
			$oHist->insertar();
		}  
		/* ************* MOTIVO SALIDA *************** */

		/* ************* TIPO *************** */


		$mensaje = 'Modificó el Tipo de llave: "' . $_POST["tipo_nombre_actual"] . '" , a nuevo Tipo : "' . $_POST["tipo_nombre_nuevo"] . '"';
		$oHist->setTbHistDet($mensaje);
		$oHist->insertar();
		/* ************* TIPO *************** */
	}

	if ($estadollave_tipo == 3 || $estadollave_tipo == 4) {
		if ($oLlave->modificar_campo($llaveid, 'tb_llave_numcasillero', null, 'INT')['estado'] == 1) {
			$data['estado'] = 1;
		} 
	}

	echo json_encode($data);

} elseif ($action == 'eliminar') {
	$estadollave_id = intval($_POST['hdd_estadollave_id']);

	$data['estado'] = 0;
	$data['mensaje'] = 'Existe un error al eliminar el Estado de llave.';

	if ($oEstadollave->eliminar($estadollave_id)) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Estado de llave eliminado correctamente.';
	}

	echo json_encode($data);
} elseif($action == 'liberarcasillero'){
	if($oLlave->modificar_campo($_POST['llaveid'], 'tb_llave_numcasillero', null, 'INT') == 1){
		$oLlave->modificar_campo($_POST['llaveid'], 'tb_estadollave_tipo', 4, 'INT'); //damos por culminado la entrega de la llave

		$data['estado'] = 1;
		$data['mensaje'] = 'Casillero Liberado';
	}else{
		$data['estado'] = 0;
		$data['mensaje'] = 'Error al liberar casillero';
	}

	echo json_encode($data);
}else {
	$data['estado'] = 0;
	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

	echo json_encode($data);
}
