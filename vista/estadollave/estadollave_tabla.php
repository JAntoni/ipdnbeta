<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL . 'estadollave/Estadollave.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../estadollave/Estadollave.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
}
$oEstadollave = new Estadollave();

$fecha1=fecha_mysql($_POST['fecha1']);
$fecha2=fecha_mysql($_POST['fecha2']);
?>


<table id="tbl_estadollave" class="table table-hover <?php echo $fecha2?>">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">OPERACION</th>
            <th id="tabla_cabecera_fila">MOTIVO SALIDA DE LLAVE</th>
            <th id="tabla_cabecera_fila">USUARIO QUE ENTREGA</th>
            <th id="tabla_cabecera_fila">USUARIO QUE RECIBE</th>
            <th id="tabla_cabecera_fila">FECHA</th>
            <th id="tabla_cabecera_fila">HORA</th>
            <th id="tabla_cabecera_fila">LLAVE</th>
            <th id="tabla_cabecera_fila">CASILLERO</th>
            <th id="tabla_cabecera_fila">CREDITO QUE REFIERE</th>
            <th id="tabla_cabecera_fila" >TIPO DE CREDITO</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">OBSERVACIONES</th>
            <th id="tabla_cabecera_fila">TIPO </th>
            <th id="tabla_cabecera_fila" width="6%">ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //PRIMER NIVEL
        $result = $oEstadollave->listar_estadollave($fecha1,$fecha2);
        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value) :

                $nombre_cliente = $value['Nombre_cliente'];
        if($value['tb_cliente_emprs'] != '')
            $nombre_cliente = $value['tb_cliente_emprs'].' | '.$value['Nombre_cliente'];
        ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['tb_estadollave_id']; ?></td>
                    <td id="tabla_fila"><?php echo $value['MOTIVO']; ?></td>
                    <td id="tabla_fila"><?php echo $value['Salida_llave']; ?></td>
                    <td id="tabla_fila"><?php echo $value['Usuario_entrega']; ?></td>
                    <td id="tabla_fila"><?php echo $value['Usuario_recibe']; ?></td>
                    <td id="tabla_fila"><?php echo $value['FECHA']; ?></td>
                    <td id="tabla_fila"><?php echo $value['HORA']; ?></td>
                    <td id="tabla_fila"><?php echo $value['llave']; ?></td>
                    <td id="tabla_fila"><?php echo $value['Casillero']; ?></td>
                    <td id="tabla_fila"><?php echo $value['Credito']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tipo_credito']; ?></td>
                    <td id="tabla_fila"><?php echo $nombre_cliente; ?></td>
                    <td id="tabla_fila"><?php echo $value['OBSERVACION']; ?></td>
                    <td id="tabla_fila"><?php echo $value['TIPO']; ?></td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="estadollave_form(<?php echo "'M', " . $value['tb_estadollave_id']; ?>)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="estadollave_form(<?php echo "'E', " . $value['tb_estadollave_id']; ?>)"><i class="fa fa-trash"></i></a>
                        <a class="btn btn-github btn-xs" title="Historial" onclick="estadollave_historial_form(<?php echo $value['tb_estadollave_id']; ?>)"><i class="fa fa-history"></i></a>
                        <?php
                        if($value['tb_estadollave_tipo'] == 4){
                        ?>
                        <a class="btn bg-purple btn-xs" title="Acta" onclick="GenerarActa(<?php echo $value['tb_llave_id'].','.$value['Casillero'].','.$value['tb_creditotipo_id']; ?>)"><i class="fa fa-file-pdf-o"></i> ACTA</a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
        <?php
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
</table>