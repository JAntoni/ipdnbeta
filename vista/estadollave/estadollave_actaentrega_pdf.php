<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');

require_once ("../estadollave/Estadollave.class.php");
$estadollave = new Estadollave();
require_once ("../zona/Zona.class.php");
$oZona = new Zona();

/* require_once("../formatos/formato.php");
require_once("../formatos/numletras.php");
require_once("../formatos/fechas.php");
require_once("../formatos/operaciones.php"); */

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$title = 'Actade Entrega de Llave';
$codigo = 'LLA-'.str_pad($_GET['id'], 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {

  public function Header() {
    //$image_file = K_PATH_IMAGES.'logo.jpg';
    //$this->Image($image_file, 10, 10, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  //$pdf->setLanguageArray();

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');


$result = $estadollave->mostrarUnoLlaveId($_GET['llave_id'],$_GET['tipo_credito_id']);
  if ($result['estado'] == 1) {
    $placa = $result['data']['placa'];
    $numero_serie = $result['data']['numero_serie'];
    $numero_motor = $result['data']['numero_motor'];
    $nombre_cliente_empresa = $result['data']['cliente_empresa_nombre'];
    $dni_cliente_empresa = $result['data']['dni_cliente_empresa'];
    $obs = $result['data']['obs'];
    $direccion =  $result['data']['direccion'];
    $numpartida = $result['data']['numparcliente'];
    $numparcredito = $result['data']['numpartidacredito'];
    $cliente_zona_id = $result['data']['cliente_zona_id'];

    $esEmpresa = !empty($result['data']['nombre_empresa']);
    $nombreCliente = $esEmpresa ? $result['data']['nombre_empresa'] : $result['data']['Nombre_Cliente'];
    $dni = $esEmpresa ? $result['data']['ruc_empresa'] : $result['data']['dni'];
    $rucEmpresa = $esEmpresa ? $result['data']['ruc_empresa'] : '';
  }
$result = null;


$zona = '';
if($cliente_zona_id > 0){
    $result = $oZona->mostrarUno($cliente_zona_id);
        if($result['estado'] == 1){
            $zona = $result['data']['tb_zona_nom'];
        }
    $result = NULL;
}

if (!empty($obs)) {
  $html .= '
  <tr>
    <td colspan="9" style="text-align: justify; font-size: 40px;">
      Observaciones adicionales:
      - '.$obs.'
    </td>
  </tr>
  ';
}

$mensajeCliente = $esEmpresa ? 
    'por la empresa: <b>'.$nombreCliente.'</b>, identificada con  RUC: <b>'.$rucEmpresa.'</b>, con domicilio en <b>'.$direccion.'</b>, debidamente 
    representada por su gerente general el Señor(a) : <b>'.$nombre_cliente_empresa.'</b>,  identificado(a) con DNI: <b>'.$dni_cliente_empresa.'</b>,  
    segun poderes inscritos en la partida registral N° <b>'.$numpartida.'</b> del registro de Personas jurídicas de la <b>'.$zona.'</b>':
    'por el Cliente : <b>'.$nombreCliente.'</b>, debidamente identificado con DNI : <b>'.$dni. '</b>.' ;


  $html= '
  <header>

  <table>
      <tr>
        <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 40px; height: 50px;"></td>
      </tr>
    </table>
    <p></p>
    <p></p>
    <table>
      <tr>
      <td colspan="10" style="text-align: center; font-size: 60px;"><br/><br/><strong><u>ACTA DE ENTREGA DE LLAVE</u></strong></td>
      </tr>
    </table>
    <p></p>
  </header>
  <tr>
  <td></td>
  <td colspan="9" style="text-align: justify; font-size: 40px;">
En la oficina de Inversiones y Préstamos del Norte S.A.C., ubicada en Av. Mariscal Nieto N° 480, Ext. A7, oficina Boulevard, distrito y provincia de
Chiclayo, departamento  de Lambayeque, siendo el día <b>'.get_nombre_dia(date('Y-m-d')).', '.fechaActual(date('Y-m-d')).'</b> , se hace entrega de la llave de contacto para acceso y encendido
del vehículo de placa <b>'.$placa.'</b>, con N° de serie: <b>'.$numero_serie.'</b>
Y N° de motor: <b>'.$numero_motor.'</b>; el cual será recepcionado
satisfactoriamente '.$mensajeCliente.'

'.(!empty($obs) ? '<br><br> <b>Observaciones adicionales :</b> <br> 
- '.$obs : '').'
</td>

</tr>
    <p></p>
  <tr>
      <td></td>
      <td colspan="19" style="font-size: 40px;">En señal de conformidad, ambas partes suscriben la presente acta.</td>
    </tr>
    <p></p>
    <tr>
      <td></td>
      <td colspan="9" class="fecha" style="text-align: right;"><strong> Chiclayo<b>' . date('d-m-Y') . '</b></strong></td>
    </tr>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
  <table style="width: 100%;">
    <tr>
      <td style="width: 50%; text-align: center;">
      <p class="firma">............................................</p>

      <h4 style="margin-bottom: 5px;"><strong>INVERSIONES Y <br> PRÉSTAMOS DEL NORTE <br> S.A.C.</strong></h4>
      <p style="margin-bottom: 5px;">Entregué conforme</p>
      
      </td>
      <td style="width: 50%; text-align: center;">
      <p class="firma">.............................................</p>
        <h4> <strong>'.$nombreCliente.' </strong></h4>
        
        <p>Recibí conforme</p>
      </td>
    </tr>
  </table>
  ';

// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$codigo."_46464".$title.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>