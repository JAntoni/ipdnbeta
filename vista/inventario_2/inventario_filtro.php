<form id="inventario_filtro" name="inventario_filtro">
    <div class="row">
        <div class="col-md-2">
            <input type="hidden" id="hdd_almacen_id">
                <input type="hidden" id="hdd_tipo_tabla" value="normal"  class="form-control input-sm">
                <label for="">&zwj;</label>
                <select name="cmb_inven_est" id="cmb_inven_est"  class="form-control input-sm">
                  <option value="0">En Oficinas</option>
                  <option value="1">Eviado a Almacén</option>
                  <option value="3">Recibir en Oficina</option>
                  <option value="">Todos...</option>
                </select>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <!--<label for="">&zwj;</label>-->
              <label>Código Crédito: </label>
              <input type="number" name="txt_cre_id" id="txt_cre_id" class="form-control input-sm">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Tipo: </label>
                <select name="cmb_gar_tip" id="cmb_gar_tip" class="form-control input-sm"></select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Garantía: </label>
                <input type="text" name="txt_garan_nom" id="txt_garan_nom" class="form-control input-sm">
                <input type="hidden" name="hdd_garan_id" id="hdd_garan_id" class="form-control input-sm">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label>Cliente:</label>
                <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm">
                <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" class="form-control input-sm">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>SUCURSAL:</label>
                <select class="form-control input-sm mayus" name="cmb_empresa_id" id="cmb_empresa_id">
                    <?php require_once 'vista/empresa/empresa_select.php';?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">

            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label id="lbl_tipo_tar" style="font-weight: bold;color: green;">Enviar a: </label>
                <select name="cmb_usu_id" id="cmb_usu_id"  class="form-control input-sm"></select>
            </div>
        </div>
    </div>
</form>