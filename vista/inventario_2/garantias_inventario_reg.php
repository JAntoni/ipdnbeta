<?php

require_once '../../core/usuario_sesion.php';
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once('../inventario/TareaInventario.class.php');
$oTarea = new TareaInventario();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ("../asistencia/Asistencia.class.php");
$oAsistencia = new asistencia();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();



$gar_id = $_POST['gar_id'];
$usu_id = $_POST['usu_id'];
$gar_nom = '';

//consultamos datos de la garantia
if (!empty($gar_id)) {
    $dts = $oGarantia->mostrarUnoDetalle($gar_id);
    if ($dts['estado'] == 1) {
        $alm_id = $dts['data']['tb_almacen_id'];
        $gar_nom = str_replace("'", '*', $dts['data']['tb_garantia_pro']);
        $gar_nom = str_replace('"', '*', $gar_nom);
        $cre_cod = 'CM-' . str_pad($dts['data']['tb_credito_id'], 4, '0', STR_PAD_LEFT);
        $cli_nom = $dts['data']['tb_cliente_nom'];
    }
    $dts = NULL;
}

if (!empty($usu_id)) {
    $dts = $oUsuario->mostrarUno($usu_id);
    if ($dts['estado'] == 1) {
        $usu_nom = $dts['data']['tb_usuario_nom'];
    }
}

if ($_POST['action'] == "enviar" && $_POST['vista'] == 'oficina') {
    $tipo = $_POST['tipo'];
    $gar_id = $_POST['gar_id'];
    $usu_id = $_POST['usu_id']; //a quien va la tarea
    $usu_tip = 2; //el tipo de usuario a quien va la tarea, se envia al almacenero y entonces es de tipo 2(almacen)
    $est = 1; //enviado desde oficina hasta almacén
    $enviado = '<b>Enviado por: ' . $_SESSION['usuario_nom'] . ' a ' . $usu_nom . ' | ' . date('d-m-Y h:i a') . '</b><br>';

    $oGarantia->estado_almacen_enviado($gar_id, $est, $enviado);
    $det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha enviado la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b> | ' . date('d-m-Y h:i a');

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha enviado la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;
    $oLineaTiempo->insertar();

    //verificamos si el enviar garantia era una tarea, si fue una tarea entonces la resolvemos sino no pasa nada
    $oTarea->realizar_tarea($gar_id, 0, 'enviar'); //estado de tarea cambia a 1, where estado = 0 y tipo = enviar
//    $data['msj'] = 'hasta aqui estoy entrando normaldsgdsgdsg ';echo json_encode($data);exit();

    //ejem: pedro envia la garantia a victor, enonces victor tiene la tarea de recibir
    $oTarea->tb_tarea_usureg=$_SESSION['usuario_id']; 
    $oTarea->tb_tarea_det=$det; 
    $oTarea->tb_usuario_id=$usu_id;
    $oTarea->tb_tarea_usutip=$usu_tip;
    $oTarea->tb_garantia_id=$gar_id;
    $oTarea->tb_tarea_tipo='recibir'; 
    $oTarea->insertar();

    $data['msj'] = 'Se guardó el envio de la garantía.';
    echo json_encode($data);
}

if ($_POST['action'] == "enviar" && $_POST['vista'] == 'almacen') {
    $tipo = $_POST['tipo'];
    $gar_id = $_POST['gar_id'];
    $usu_id = $_POST['usu_id']; //a quien va la tarea
    $usu_tip = 1; //el tipo de usuario a quien va la tarea, se envia a un usuario de oficina entonces su MOS es 1
    $est = 3; //enviado desde almacén hasta oficina
    $enviado = '<b>Enviado por: ' . $_SESSION['usuario_nom'] . ' a ' . $usu_nom . ' | ' . date('d-m-Y h:i a') . '</b><br>';

    $oGarantia->estado_almacen_enviado($gar_id, $est, $enviado); //cambia de estado 2 al estado 3

    $det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha enviado la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b> | ' . date('d-m-Y h:i a');

    $line_det = '<b> '.$_SESSION['usuario_nom'] . '</b> ha enviado la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;
    $oLineaTiempo->insertar();

    //verificamos si el enviar garantia era una tarea, si fue una tarea entonces la resolvemos sino no pasa nada
    $oTarea->realizar_tarea($gar_id, 0, 'enviar'); //estado de tarea cambia a 1, where estado = 0 y tipo = enviar
    //ejem: pedro envia la garantia a victor, enonces victor tiene la tarea de recibir
    
    $oTarea->tb_tarea_usureg=$_SESSION['usuario_id']; 
    $oTarea->tb_tarea_det=$det; 
    $oTarea->tb_usuario_id=$usu_id;
    $oTarea->tb_tarea_usutip=$usu_tip;
    $oTarea->tb_garantia_id=$gar_id;
    $oTarea->tb_tarea_tipo='recibir'; 
    $oTarea->insertar();

    $data['msj'] = 'Se guardó el envio de la garantía.';
    echo json_encode($data);
}

if ($_POST['action'] == "cancelar") {
    $gar_id = $_POST['gar_id'];
    $cancelado = '<strong style="color: red;">Cancelado envío por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</strong><br>';

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha cancelado el envío de la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;    
    $oLineaTiempo->insertar();

    if ($alm_id == 1) {
        $oGarantia->modificar_campo($gar_id, 'tb_garantia_almest', 0,'INT');
        //guardamos al usuario que cancela el envio
        $oGarantia->estado_almacen_enviado($gar_id, 0, $cancelado);
    }

    if ($alm_id == 2) {
        $oGarantia->modificar_campo($gar_id, 'tb_garantia_almest', 2,'INT');
        //guardamos al usuario que cancela el envio
        $oGarantia->estado_almacen_enviado($gar_id, 2, $cancelado);
    }


    //cuando se envia se genera una tarea de recibir para un usuaro, pero si se cancela el envio entonces eliminamos esa tarea
    $est = 0;
    $tipo = 'recibir';
    $oTarea->eliminar_tarea($gar_id, $est, $tipo); //where estado = 0 y tipo tarea = 'recibir'

    $data['msj'] = 'Se canceló el envío de la garantía';
    echo json_encode($data);
}
if ($_POST['action'] == "recibir") {
    $tipo = $_POST['tipo'];
    $gar_id = $_POST['gar_id'];
    $recibido = '<strong style="color: green;">Recibido por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</strong><br>';

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha recibido la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;
    $oLineaTiempo->insertar();
  
    if ($alm_id == 1) {
        $oGarantia->cambiar_de_almacen($gar_id, 2); //si está en alamacen 1 entonces lo recibimos en almacen 2
        $oGarantia->estado_almacen_recibido($gar_id, 2, $recibido);
    }
    if ($alm_id == 2) {
        $oGarantia->cambiar_de_almacen($gar_id, 1); //si está en alamacen 2 entonces lo recibimos en almacen 1
        $oGarantia->estado_almacen_recibido($gar_id, 0, $recibido);
    }

    //verificamos si recibir era una tarea, si fue una tarea entonces la resolvemos
    $oTarea->realizar_tarea($gar_id, 0, 'recibir'); //estado de tarea cambia a 1, where estado_tarea = 0 y tipo = recibir
    //verificamos si fue pedido esta garantia para cambiar su estado a no pedido, estado = 0
    $oGarantia->modificar_campo($gar_id, 'tb_garantia_ped', 0,'INT');

    $data['msj'] = 'Se recibió la garantía correctamente.';
    echo json_encode($data);
}

if ($_POST['action'] == "pedir") {
    $tipo = $_POST['tipo'];
    $gar_id = $_POST['gar_id'];
    $usu_id = $_POST['usu_id']; //a quien va la tarea

    if ($_POST['vista'] == 'oficina')
        $usu_tip = 1; //si se pide a oficina entonces lo usuarios no son almaceneros y su mos es de tipo 1
    else
        $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

    if ($usu_tip == 2) {
        //si se hace un pedido a almacén, se tiene que respetar la regla de que el CREDITO de la garantia tenga estado de REMATE y que el usuario que pide, tiene que haber registrado su ingreso
        $dts = $oGarantia->mostrarUnoDetalle($gar_id);
        if($dts['estado']==1){
            $cre_est = $dts['data']['tb_credito_est']; // 5 es remate, 7 liquidado pendiente de entrega
        }

        $fecha_hoy = date('d-m-y');
        $dts = $oAsistencia->listar_asistencia_codigo($fecha_hoy, $_SESSION['usuario_id']);
        if($dts['estado']==1){
        $asi_ing1 = $dts['data']['tb_asistencia_ing1'];
        }
        $dts=NULL;

        if (($cre_est != 5 || $cre_est != 7) || ($asi_ing1 == '00:00:00' || $asi_ing1 == null || $asi_ing1 == "")) {
            $data['est'] = 0;
            $data['msj'] = 'Mensaje: no se puede pedir esta garantía: Las razones para Pedir Garantías son: 1- el crédito debe estar en Remate, 2- el crédito debe estar en Pendiente de Entrega, 3- debes haber registrado tu asistencia al trabajo. Estado actuales: Crédito en estado (' . $cre_est . '), tu Asistencia está así (' . $asi_ing1 . ')';

            echo json_encode($data);
            exit();
        }
    }

    $det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho el pedido de la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b> | ' . date('d-m-Y h:i a');
    $pedido = '<strong style="color: blue;">Pedido por: ' . $_SESSION['usuario_nom'] . ' a ' . $usu_nom . ' | ' . date('d-m-Y h:i a') . '</strong><br>';

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho el pedido de la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;
    $oLineaTiempo->insertar();

    //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
    $dts = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');
    

    if ($dts['estado'] == 0) {
        //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
        $oTarea->tb_tarea_usureg=$_SESSION['usuario_id']; 
        $oTarea->tb_tarea_det=$det; 
        $oTarea->tb_usuario_id=$usu_id;
        $oTarea->tb_tarea_usutip=$usu_tip;
        $oTarea->tb_garantia_id=$gar_id;
        $oTarea->tb_tarea_tipo='enviar'; 
        $oTarea->insertar();

        //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
        $oGarantia->modificar_campo($gar_id, 'tb_garantia_ped', 1,'INT');

        //registramos al usuario que pide la garantía
        $oGarantia->estado_usuario_pedido($gar_id, $pedido);
        $data['est'] = 1;
        $data['msj'] = 'Se guardó el pedido de la garantía.';
    } else {
        $data['est'] = 0;
        $data['msj'] = 'Esta garantía ya se ha pedido.';
    }

    echo json_encode($data);
}
if ($_POST['action'] == "cancelar_pedido") {
    $gar_id = $_POST['gar_id'];

    //cambiamos el estado_pedido de la garantia a no pedido, que es el estado 0
    $oGarantia->modificar_campo($gar_id, 'tb_garantia_ped', 0,'INT');

    //guardamos al usuario que canceló elpedido
    $pedido_can = '<strong style="color: red;">Pedido cancelado por: ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</strong><br>';
    $oGarantia->estado_usuario_pedido($gar_id, $pedido_can);

    $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha cancelado el pedido de la garantía: <b>' . $gar_nom . '</b>. Cliente: ' . $cli_nom . ' y código de crédito: <b>' . $cre_cod . '</b>. && <b>' . date('d-m-Y h:i a') . '</b>';
    $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det=$line_det;
    $oLineaTiempo->insertar();

    //cuando se pide una garantia se genera una tarea de enviar esa garantia, entonces debemos eliminarla
    $est = 0;
    $tipo = 'enviar';
    $oTarea->eliminar_tarea($gar_id, $est, $tipo); //where estado = 0 y tipo tarea = 'enviar'

    $data['msj'] = 'Se canceló el pedio de la garantía';
    echo json_encode($data);
}

//detalle de usuarios enviados y recibidos
if ($_POST['action'] == 'detalle_envio') {
    $gar_id = $_POST['gar_id'];

    $dts = $oGarantia->mostrarUno($gar_id);
    if($dts['estado']==1){
    $gar_envi = $dts['data']['tb_garantia_envi']; //se guarda a todos los usuarios que envian o cancelan el envio
    $gar_reci = $dts['data']['tb_garantia_reci']; //se concatena cada vez que un usuario recibe
    $gar_ped = $dts['data']['tb_garantia_usuped']; //se guarda al usuario que pide y cancela
    }
    $dts=NULL;
    
    $content = $gar_envi . '' . $gar_ped . '' . $gar_reci;
    $arr_cont = explode('<br>', $content);
    $count = count($arr_cont);
    $array = array();

    for ($i = 0; $i < $count; $i++) {
        if (!empty(trim($arr_cont[$i]))) {
            $arr_fecha = explode('|', $arr_cont[$i]);

            $fecha_limpia = str_replace('</b>', '', $arr_fecha[1]);
            $fecha_limpia = str_replace('</strong>', '', $fecha_limpia);
            $fecha_limpia = trim($fecha_limpia);

            if (array_key_exists(strtotime($fecha_limpia), $array))
                $array[strtotime($fecha_limpia) . $i] = $arr_cont[$i];
            else
                $array[strtotime($fecha_limpia)] = $arr_cont[$i];
        }
    }
    ksort($array);
    //echo strip_tags($content);
    //echo strtotime('02-05-2017 05:17 pm'); exit();
    //echo json_encode($array); exit();

    foreach ($array as $key => $value) {
        echo $value . '<br>';
    }
}
?>