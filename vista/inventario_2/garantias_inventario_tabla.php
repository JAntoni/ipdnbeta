<?php
require_once '../../core/usuario_sesion.php';
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once ("../garantiafile/Garantiafile.class.php");
$oGarantiafile = new Garantiafile();
require_once ("../funciones/funciones.php");

$estado = $_POST['estado'];
$alm_id = $_POST['alm_id'];
$gar_id = $_POST['gar_id'];
$cre_id = $_POST['cre_id'];
$gartip_id = $_POST['gartip_id'];
$cli_id = $_POST['cli_id'];

if ($gartip_id == 'null')
    $gartip_id = '';

$tipo_tabla = $_POST['tipo_tabla'];

if ($tipo_tabla == 'normal' || $tipo_tabla == 'pedir') {
    $dts = $oGarantia->mostrar_todos_filtro($estado, $alm_id, $gar_id, $cre_id, $gartip_id, $cli_id);
}

/*
  if($tipo_tabla == 'pedir' && $alm_id == 1){
  $estado = 0; // si pido a la oficina la garantia tiene estado 0

  $dts=$oGarantia->mostrar_todos_filtro($estado, $alm_id, $gar_id, $cre_id, $gartip_id, $cli_id);
  $num_rows= mysql_num_rows($dts);
  }
  if($tipo_tabla == 'pedir' && $alm_id == 2){
  $estado = 2; // si pido al almacen la garantia tiene estado 2

  $dts=$oGarantia->mostrar_todos_filtro($estado, $alm_id, $gar_id, $cre_id, $gartip_id, $cli_id);
  $num_rows= mysql_num_rows($dts);
  }
 */
////echo $tipo_tabla.' // '.$estado.' / '.$alm_id.' / '.$gar_id; exit(); 
?>
<input type="hidden" id="usuario_mos" value="<?php echo $_SESSION['usuario_mos']; ?>">
<input type="hidden" id="almacen_id" value="<?php echo $alm_id; ?>">
<input type="hidden" id="tipo_tabla" value= "<?php echo $tipo_tabla; ?>">

<?php if ($dts['estado'] == 1): ?>
    <table cellspacing="1" id="tabla_vehiculomarca" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">ID</th>
                <th id="tabla_cabecera_fila">CREDITO ID</th>
                <th id="tabla_cabecera_fila">CLIENTE</th>
                <th id="tabla_cabecera_fila">TIPO</th>
                <th id="tabla_cabecera_fila">CAN</th>
                <th id="tabla_cabecera_fila">PRODUCTO</th>
                <th id="tabla_cabecera_fila">DETALLE</th>
                <th id="tabla_cabecera_fila">ALMACEN</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th  id="tabla_cabecera_fila" width="12%">&nbsp;</th>
            </tr>
        </thead>  
        <tbody> <?php
            foreach ($dts['data']as $key => $dt) {
                if (intval($dt['tb_garantiatipo_id']) != 5) {
                    ?>
                    <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_id'] ?></td>
                        <td id="tabla_fila"><?php echo 'CM-' . str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_cliente_nom'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantiatipo_nom'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_can'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_pro'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_garantia_det'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_almacen_nom'] ?></td>
                        <td id="tabla_fila">
                            <?php
                            $pedido = '';
                            if ($dt['tb_garantia_ped'] == 1)
                                $pedido = '<br><span style="color: blue;">(Pedido)</span>';

                            $estado = '<b>EN OFICINAS</b>' . $pedido;
                            if ($dt['tb_garantia_almest'] == 1)
                                $estado = '<strong style="color: red;">ENVIADO A ALMACÉN</strong>' . $pedido;
                            if ($dt['tb_garantia_almest'] == 2)
                                $estado = '<strong style="color: green;">EN ALMACÉN</strong>' . $pedido;
                            if ($dt['tb_garantia_almest'] == 3)
                                $estado = '<strong style="color: red;">ENVIADO A OFICINAS</strong>' . $pedido;
                            echo $estado;
                            ?>
                            <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="detalle_envios(<?php echo $dt['tb_garantia_id'] ?>)">Det.</a>
                        </td>
                        <td  id="tabla_fila" align="center" class="td_botones">
                            <?php if ($tipo_tabla == 'normal'): ?>
                                <?php if ($_POST['vista'] == 'oficina' && $dt['tb_garantia_almest'] == 0): ?>
                                    <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="enviar_garantia('a_almacen',<?php echo $dt['tb_garantia_id'] ?>)">Enviar a Almacén</a>
                                <?php endif; ?>

                                <?php if ($_POST['vista'] == 'oficina' && $dt['tb_garantia_almest'] == 1): ?>
                                    <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="cancelar_envio('a_almacen',<?php echo $dt['tb_garantia_id'] ?>)">Cancelar</a>
                                <?php endif; ?>

                                <?php if ($_POST['vista'] == 'oficina' && $dt['tb_garantia_almest'] == 3): ?>
                                    <a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="recibir_garantia('en_oficina',<?php echo $dt['tb_garantia_id'] ?>)">Recibir en Oficina</a>
                                <?php endif; ?>

                                <?php if ($_POST['vista'] == 'almacen' && $dt['tb_garantia_almest'] == 2): ?>
                                    <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="enviar_garantia('a_oficina',<?php echo $dt['tb_garantia_id'] ?>)">Enviar a Oficina</a>
                                <?php endif; ?>

                                <?php if ($_POST['vista'] == 'almacen' && $dt['tb_garantia_almest'] == 1): ?>
                                    <a class="btn btn-green btn-xs" href="javascript:void(0)" onClick="recibir_garantia('en_almacen',<?php echo $dt['tb_garantia_id'] ?>)">Recibir en Almacén</a>
                                <?php endif; ?>

                                <?php if ($_POST['vista'] == 'almacen' && $dt['tb_garantia_almest'] == 3): ?>
                                    <a class="btn_cancelar" href="javascript:void(0)" onClick="cancelar_envio('a_oficina',<?php echo $dt['tb_garantia_id'] ?>)">Cancelar</a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <!-- POR EL MOMENTO SOLO SE PDORÁ PEDIR DESDE ALMACÉN HACIA OFICINAS-->
                            <?php if ($tipo_tabla == 'pedir' && $dt['tb_garantia_ped'] == 0 && $dt['tb_garantia_almest'] == 2): ?>
                                <a class="btn_pedir" href="javascript:void(0)" onClick="pedir_cancelar_garantia('pedir',<?php echo $dt['tb_garantia_id'] ?>)">Pedir de Almacén</a>
                            <?php endif; ?>
                            <?php if ($_POST['vista'] == 'almacen' && $dt['tb_garantia_ped'] == 1 && $dt['tb_garantia_almest'] == 2): ?>
                                <a class="btn_pedir" href="javascript:void(0)" onClick="pedir_cancelar_garantia('cancelar_pedido',<?php echo $dt['tb_garantia_id'] ?>)">Cancelar Pedido</a>
            <?php endif; ?>
                        </td>
                    </tr> <?php
                } //IF DE SOLO MOSTRAR INVENTARIOS DIFERENTES A LETRAS DE CAMBIO
            } //FIN DE WHILE
            ?>
        </tbody>
<!--        <tr class="even">
            <td colspan="10"><?php // echo $num_rows . ' registros' ?></td>
        </tr>-->
    </table>
<?php endif; ?>

