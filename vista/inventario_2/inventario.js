/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ALMACEN_ID = 0;

$(document).ready(function () {
    cmb_inven_alm();
    cmb_gar_tip('');
    cmb_usu_id(2);
    $("#cmb_empresa_id").attr('disabled', 'disabled');
    garantias_inventario_tabla(ALMACEN_ID);


    $("#txt_cliente_nom").focus();
    $("#txt_cliente_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            $('#hdd_cliente_id').val(ui.item.cliente_id);
            $('#txt_cliente_nom').val(ui.item.cliente_nom);
            garantias_inventario_tabla(ALMACEN_ID);
            event.preventDefault();
            $('#txt_cliente_nom').focus();
        }
    });
}
);

function cmb_inven_alm() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "almacen/cmb_alm_id.php",
        async: true,
        dataType: "html",
        data: ({
        }),
        beforeSend: function () {
        },
        success: function (data) {
            $('#cmb_inven_alm').html(data);
        },
        complete: function (data) {
//            console.log(data);
            if (data.statusText != 'success') {
                $('#msj_garantias').text(data.responseText);
//        console.log(data);
            }

        }
    });
}

function tipo_ubicacion_garantias(almacen_id) {
    ALMACEN_ID = almacen_id;

    $('#hdd_tipo_tabla').val('normal');
    if (almacen_id == 1) { //garantías en oficia
        cmb_usu_id(2); //para enviarles a los de almacén
        $("#cmb_empresa_id").attr('disabled', 'disabled');
        var option = '<option value="0">En Oficinas</option>' +
                '<option value="1">Eviado a Almacén</option>' +
                '<option value="3">Recibir en Oficina</option>' +
                '<option value="">Todos...</option>';
        $('#cmb_inven_est').html(option);
    }
    if (almacen_id == 2) { //garantías en ALMACEN
        cmb_usu_id(1); //para enviarles a los de OFICINA
        $("#cmb_empresa_id").removeAttr('disabled');
        var option = '<option value="2">En almacen</option>' +
                '<option value="3">Eviado a Oficina</option>' +
                '<option value="1">Recibir en Almacen</option>' +
                '<option value="">Todos...</option>';
        $('#cmb_inven_est').html(option);
    }

    garantias_inventario_tabla(ALMACEN_ID);
}

$("#cmb_empresa_id").change(function (){
    cmb_usu_id(1);
});

function cmb_usu_id(mostrar) {
    var codigo=$("#cmb_empresa_id").val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/cmb_usu_id_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
            empresa_id: codigo
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
            var op = '<option value="0">PARA TODOS</option>';
            $('#cmb_usu_id').append(op);
        }
    });
}


function garantias_inventario_tabla(almacen_id)
{
    var tip_tbl = $('#hdd_tipo_tabla').val();
    if (tip_tbl == 'pedir')
        $('#lbl_tipo_tar').text('Pedir de Almacén a: ');
    if (tip_tbl != 'pedir')
        $('#lbl_tipo_tar').text('Enviar a: ');

    var vista = 'oficina';
    if (ALMACEN_ID == 2) // 2 es almacén
        vista = 'almacen';

//
//  console.log(' estado de almancen 1 oficinas, 2 almacen: ' + 
//    almacen_id + ' / estado: ' + $('#cmb_inven_est').val() + ' / '+tip_tbl+' vista ' + vista);
    $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/garantias_inventario_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            vista: vista,
            estado: $('#cmb_inven_est').val(),
            alm_id: almacen_id, //esta vista es solo para los garantias que se encuentran en tienda, id de almacen tienda es 1
            gar_id: $('#hdd_garan_id').val(),
            cre_id: $('#txt_cre_id').val(),
            tipo_tabla: tip_tbl, //inventario, pedir
            gartip_id: $('#cmb_gar_tip').val(),
            cli_id: $('#hdd_cliente_id').val()
        }),
        beforeSend: function () {
//            $('#div_garantias_inventario_tabla').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
//            $('#div_garantias_inventario_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_garantias_inventario_tabla').html(html);
        },
        complete: function (html) {
//            $('#div_garantias_inventario_tabla').removeClass("ui-state-disabled");
        }
    });
}


function validar_almacenero() {
    var mos = $('#usuario_mos').val();
    var alm_id = $('#almacen_id').val();
    var tipo_tabla = $('#tipo_tabla').val();

//  if(tipo_tabla == 'normal' && parseInt(mos) == 1 && parseInt(alm_id) == 2)
//    $('.td_botones').find('a').hide();
}


function cmb_gar_tip(tip) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantiatipo/garantiatipo_select.php",
        async: true,
        dataType: "html",
        data: ({
            gartip_id: tip
        }),
        beforeSend: function () {
        },
        success: function (data) {
            $('#cmb_gar_tip').html(data);
        },
        complete: function (data) {
        }
    });
}

function detalle_envios(id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "inventario/garantias_inventario_reg.php",
        async: true,
        data: ({
            action: 'detalle_envio',
            gar_id: id
        }),
        beforeSend: function () {
        },
        success: function (data) {
            $('#div_detalle_envios').dialog('open');
            $('#div_detalle_envios').html(data);
        },
        complete: function (data) {
            if (data.statusText != 'success') {
                $('#msj_garantias').text(data.responseText);
//                console.log(data);
            }

        }
    });
}


function enviar_garantia(tip, id) {
    console.log('estoyu en tra');
    var usu_id = $('#cmb_usu_id').val();
    if (usu_id==0) {
        swal_warning("AVIDO", 'Seleccione un usuario para enviar la tarea', 5000);
        return false;
    }

    var vista = 'oficina';
    if (ALMACEN_ID == 2) // 2 es almacén
        vista = 'almacen';

    $.ajax({
        type: "POST",
        url: VISTA_URL+"inventario/garantias_inventario_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'enviar',
            vista: vista,
            tipo: tip,
            usu_id: $('#cmb_usu_id').val(),
            gar_id: id
        }),
        beforeSend: function () {
//      $('#msj_garantias').show(100);
//      $('#msj_garantias').text("Guardando envio de garantías...");
        },
        success: function (data) {
//      $('#msj_garantias').text(data.msj);
            swal_success("SISTEMA", data.msj, 5000);
            garantias_inventario_tabla(ALMACEN_ID);
        },
        complete: function (data) {
            if (data.statusText != 'success') {
                $('#msj_garantias').text(data.responseText);
//        console.log(data);
            }

        }
    });
}


$('#cmb_inven_est, #cmb_gar_tip').change(function (event) {
    garantias_inventario_tabla();
});

function tabla_pedidos() {
    $('#hdd_tipo_tabla').val('pedir');
    $('#cmb_inven_est').attr('disabled', true);
    cmb_usu_id(2); //listar solo los usuarios alamaceneros, que su MOS es 2
    garantias_inventario_tabla();
}



function cancelar_envio(tip,id){
  var vista = 'oficina';
  if(ALMACEN_ID == 2) // 2 es almacén
    vista = 'almacen';

  $.ajax({
    type: "POST",
    url: VISTA_URL+"inventario/garantias_inventario_reg.php",
    async:true,
    dataType: "json",     
    data: ({
      action: 'cancelar',
      vista: vista,
      tipo: tip,
      gar_id: id
    }),
    beforeSend: function() {
//      $('#msj_garantias').show(100);
//      $('#msj_garantias').text("Cancelando envio de garantías...");
    },
    success: function(data){
        swal_success('SISTEMA',data.msj,3000);
      garantias_inventario_tabla(ALMACEN_ID);
    },
    complete: function(data){     
      if(data.statusText != 'success'){
        $('#msj_garantias').text(data.responseText);
//        console.log(data);
      }

    }
  });
}


function recibir_garantia(tip,id){
  var vista = 'oficina';
  if(ALMACEN_ID == 2) // 2 es almacén
    vista = 'almacen';

  $.ajax({
    type: "POST",
    url: VISTA_URL+"inventario/garantias_inventario_reg.php",
    async:true,
    dataType: "json",     
    data: ({
      action: 'recibir',
      vista: vista,
      tipo: tip,
      gar_id: id
    }),
    beforeSend: function() {
//      $('#msj_garantias').show(100);
//      $('#msj_garantias').text("Recibiendo envio de garantías...");
    },
    success: function(data){
      swal_success('SISTEMA',data.msj,3000);
      garantias_inventario_tabla(ALMACEN_ID);
    },
    complete: function(data){     
      if(data.statusText != 'success'){
        $('#msj_garantias').text(data.responseText);
        console.log(data);
      }

    }
  });
}