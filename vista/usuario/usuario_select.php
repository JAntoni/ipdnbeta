<?php
require_once('Usuario.class.php');
$oUsuario = new Usuario();

if(isset($_POST['usuario_columna'])){
	$usuario_columna = (empty($_POST['usuario_columna']))? '' : $_POST['usuario_columna'];
	$usuario_valor = (empty($_POST['usuario_valor']))? '' : $_POST['usuario_valor'];
	$param_tip = (empty($_POST['param_tip']))? '' : $_POST['param_tip'];
	$empresa_id = (empty($_POST['empresa_id']))? '' : $_POST['empresa_id'];
}
//esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$usuario_columna = (empty($usuario_columna))? '' : $usuario_columna;
$usuario_valor = (empty($usuario_valor))? '' : $usuario_valor;
$param_tip = (empty($param_tip))? '' : $param_tip;
$usuario_id = (empty($usuario_id))? 0 : $usuario_id;

$option = '<option id="option_cero" value="0"></option>';

if(!empty($usuario_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

	$result = $oUsuario->listar_usuarios_campo($usuario_columna, $usuario_valor, $param_tip, $empresa_id);

		if($result['estado'] == 1)
		{
		  foreach ($result['data'] as $key => $value)
		  {
		  	$selected = '';
		  	if($usuario_id == $value['tb_usuario_id'])
		  		$selected = 'selected';

		    $option .= '<option value="'.$value['tb_usuario_id'].'" data-estado="'.$value['tb_usuario_estado'].'" '.$selected.'>'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</option>';
		  }
		}
		else
			echo '<option value="0">'.$result['mensaje'].'</option>';
	$result = NULL;
}
else{
    $result = $oUsuario->listar_usuarios();

		if($result['estado'] == 1)
		{
		  foreach ($result['data'] as $key => $value)
		  {
		  	$selected = '';
		  	if($usuario_id == $value['tb_usuario_id'])
		  		$selected = 'selected';

		    $option .= '<option value="'.$value['tb_usuario_id'].'" '.$selected.'>'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</option>';
		  }
		}
		else
			echo '<option value="0">'.$result['mensaje'].'</option>';
	$result = NULL;
}

echo $option;
?>