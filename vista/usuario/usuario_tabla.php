<?php
  if(defined('APP_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    }
    else{
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    }
  require_once('Usuario.class.php');
  $oUsuario = new Usuario();
  
  $usuario_id = intval($_SESSION['usuario_id']);
  $bandera = 0;
  if($usuario_id == 11 || $usuario_id == 2){
    $bandera = 1;
    $result = $oUsuario->listar_usuarios();
    $tr = '';
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $actividad = '<span class="badge bg-green">En Actividad</span>';
        $lugar_trabajo = '<span class="badge bg-black">Externo</span>';

        if(intval($value['tb_usuario_estado']) == 1)
          $actividad = '<span class="badge bg-red">Inactivo</span>';
        if(intval($value['tb_usuario_ofi']) == 1)
          $lugar_trabajo = '<span class="badge bg-purple">En Oficina</span>';
        
          $tr .='<tr>';
            $tr.='
              <td align="right">'.$value['tb_usuario_id'].'</td>
              <td align="right">'.$value['tb_usuario_use'].'</td>
              <td align="right">'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</td>
              <td align="center">'.$value['tb_usuario_tel'].'</td>
              <td align="center">'.$value['tb_usuario_dni'].'</td>
              <td align="right">'.$value['tb_usuarioperfil_nom'].'</td>
              <td align="right">'.$value['tb_usuariogrupo_nom'].'</td>
              <td align="right">'.$lugar_trabajo.'</td>
              <td align="right">'.$actividad.'</td>
              <td align="center">
                <div class="btn-group">
                  <a class="btn btn-info btn-xs" title="Ver" onclick="usuario_form(\'L\','.$value['tb_usuario_id'].')"><i class="fa fa-eye"></i></a>
                  <a class="btn btn-warning btn-xs" title="Editar" onclick="usuario_form(\'M\','.$value['tb_usuario_id'].')"><i class="fa fa-edit"></i></a>

                  <button type="button" class="btn bg-olive btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="javascript:void(0)" title="Remuneración" onclick="usuario_remuneracion('.$value['tb_usuario_id'].')">Remuneración</a>
                    </li>
                    <li>
                      <a href="javascript:void(0)" title="Eliminar" onclick="usuario_form(\'E\','.$value['tb_usuario_id'].')">Eliminar Usuario</a>
                    </li>
                    <li>
                      <a href="javascript:void(0)" title="Informe">Información</a>
                    </li>
                  </ul>
                  
                </div>
              </td>
            ';
          $tr.='</tr>';
        }
      $result = NULL;
    }
    else {
      echo $result['mensaje'];
      $result = NULL;
      exit();
    }
  }
?>
<?php if($bandera == 1):?>
  <table id="tbl_usuarios" class="table table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>USUARIO</th>
        <th>NOMBRES</th>
        <th>TELÉFONO</th>
        <th>DNI</th>
        <th>PERFIL</th>
        <th>GRUPO</th>
        <th>LUGAR DE TRABAJO</th>
        <th>ACTIVIDAD</th>
        <th>OPCIONES</th>
      </tr>
    </thead>
    
    <tbody>
      <?php echo $tr;?>
    </tbody>
  </table>
<?php endif;?>
<?php if($bandera == 0):?>
  <div class="alert alert-danger">
    <strong>Aviso!</strong> Usted no tiene permisos para ver esta página.
  </div>
<?php endif;?>
