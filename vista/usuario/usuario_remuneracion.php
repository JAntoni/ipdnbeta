<?php
require_once('../../core/usuario_sesion.php');

/* require_once ("Cobranza.class.php");
$oCobranza = new Cobranza(); */

require_once ("Usuario.class.php");
$oUsuario = new Usuario();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$usuario_id = intval($_POST['usuario_id']);

$result = $oUsuario->mostrarUno($usuario_id);
$afps = $oUsuario->listar_afps();

$hoy = date('Y-m-d');

$fecha_hoy = explode("-", $hoy);
$anio = $fecha_hoy[0];
$mes = $fecha_hoy[1];
$fecha_fin = $hoy;
$month = $anio.'-'.$mes;
$aux = date('Y-m-d', strtotime("{$month} + 1 month"));
$fecha_fin = date('Y-m-d', strtotime("{$aux} - 1 day"));
$fecha_inicio = $anio.'-'.$mes.'-01';
//$detalle_usuario = $oUsuario->mostrarSueldoUsuario($usuario_id, $fecha_inicio, $fecha_fin);
$detalle_usuario = $oUsuario->mostrarUltimoSueldoNew($usuario_id);
if($detalle_usuario['estado'] == 1){
    $usu_suel = $detalle_usuario['data']['tb_usuario_detalle_suel']; //sueldo base del colaborador NUEVO MODO RESPETANDO HISTORIAL
}else{
    /* $detalle_usuario2 = $oUsuario->mostrarSueldoUsuarioActual($usuario_id, $fecha_inicio);
    if($detalle_usuario2['estado'] == 1){
        $usu_suel = $detalle_usuario2['data']['tb_usuario_detalle_suel']; //sueldo base del colaborador NUEVO MODO RESPETANDO HISTORIAL
    }else{ */
        $usu_suel = $detalle_usuario['data']['tb_usuario_suel']; //sueldo base del colaborador
    /* } */
}

//$sueldo = $oUsuario->mostrarUltimoSueldoNew($usuario_id);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_usuario_remuneracion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">REMUNERACIÓN</h4>
            </div>
            <form id="form_remuneracion" method="post">
                <input name="action" id="action" type="hidden" value="remuneracion">
                <input name="hdd_usuario_id" id="hdd_usuario_id" type="hidden" value="<?php echo $usuario_id; ?>">

                <div class="modal-body">

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_usuario_nrocuenta" class="control-label">Nro Cuenta</label>
                                <div class="input-group" style="width: 100% !important;">
                                    <input type="text" name="txt_usuario_nrocuenta" id="txt_usuario_nrocuenta" class="form-control input-sm" value="<?php echo $result['data']['tb_usuario_nrocuenta']; ?>" placeholder="Nro Cuenta" style="text-align: right;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cmb_usuario_tip_tur" class="control-label">Tipo de Turno</label>
                                <select name="cmb_usuario_tip_tur" id="cmb_usuario_tip_tur" class="form-control input-sm">
                                    <option value="1" <?php if($result['data']['tb_usuario_tur'] == 1) echo 'selected'; ?>>Full time</option>
                                    <option value="2" <?php if($result['data']['tb_usuario_tur'] == 2) echo 'selected'; ?>>Part time mañana</option>
                                    <option value="3" <?php if($result['data']['tb_usuario_tur'] == 3) echo 'selected'; ?>>Part time tarde</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cmb_usuario_afp" class="control-label">AFP / ONP</label>
                                <select name="cmb_usuario_afp" id="cmb_usuario_afp" class="form-control input-sm">
                                    <?php
                                        $seguro_id = $result['data']['tb_usuario_afp'];
                                        require_once('../aportaciones/cmb_seguro_id.php');
                                    ?>
                                    <?php
                                        // if($afps['estado'] == 1) {
                                        //     foreach ($afps['data'] as $key => $value) {
                                        ?>
                                            <!--option value="<?php echo $value['tb_afps_id']; ?>" <?php if($value['tb_afps_id']==$result['data']['tb_usuario_afp']) echo "selected='selected'"; ?> style="font-weight: bold;"><?php echo $value['tb_afps_nom']; ?></option -->
                                    <?php
                                    //     }
                                    // }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_usuario_suel" class="control-label">Sueldo Base</label>
                                <div class="input-group" style="width: 100% !important;">
                                <input type="text" name="txt_usuario_suel" id="txt_usuario_suel" onkeypress="return solo_decimal(event)" class="form-control input-sm" value="<?php echo $usu_suel; ?>" placeholder="Sueldo Base" style="text-align: right;">
                                    <input type="hidden" name="txt_usuario_suel_old" id="txt_usuario_suel_old" class="form-control input-sm" value="<?php echo $usu_suel; ?>" placeholder="Sueldo Base" style="text-align: right;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_usuario_hor" class="control-label">Número de Horas</label>
                                <div class="input-group" style="width: 100% !important;">
                                    <input type="text" name="txt_usuario_hor" id="txt_usuario_hor" onkeypress="return solo_numero(event)" class="form-control input-sm" value="<?php echo $result['data']['tb_usuario_hor']; ?>" placeholder="Número de Horas" style="text-align: right;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-left: 0px; padding-right:0px;">
                            
                            
                        </div>

                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="usuario_remuneracion_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_remuneracion">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/usuario/usuario_form.js?ver=20230328';?>"></script>