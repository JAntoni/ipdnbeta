<?php
  require_once ("Usuario.class.php");
  $oUsuario= new Usuario();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $tb_usuario_id;
    var $tb_usuario_nom;
    var $tb_usuario_ape;
    var $tb_usuario_dni;
    var $tb_usuariogrupo_id;
    var $tb_usuarioperfil_id;
    var $tb_empresa_id;


    function __construct($label, $value, $tb_usuario_id, $tb_usuario_nom, $tb_usuario_ape,
    $tb_usuario_dni,$tb_usuariogrupo_id,$tb_usuarioperfil_id,$tb_empresa_id){
      $this->label = $label;
      $this->value = $value;
      $this->tb_usuario_id = $tb_usuario_id;
      $this->tb_usuario_nom = $tb_usuario_nom;
      $this->tb_usuario_ape = $tb_usuario_ape;
      $this->tb_usuario_dni = $tb_usuario_dni;
      $this->tb_usuariogrupo_id = $tb_usuariogrupo_id;
      $this->tb_usuarioperfil_id = $tb_usuarioperfil_id;
      $this->tb_empresa_id = $tb_empresa_id;
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $datoBuscar = mb_strtoupper($datoBuscar, 'UTF-8');

  //busco un valor aproximado al dato escrito
  $result = $oUsuario->usuario_autocomplete($datoBuscar);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    if(!empty($result['data'])){
      foreach ($result['data'] as $key => $value) {
        array_push(
          $arrayElementos,
          new ElementoAutocompletar(
            $value["tb_usuario_dni"].' - '.$value["tb_usuario_nom"]. " ".$value["tb_usuario_ape"],
            $value["tb_usuario_dni"].' - '.$value["tb_usuario_nom"]. " ".$value["tb_usuario_ape"],
            $value["tb_usuario_id"],
            $value["tb_usuario_nom"],
            $value["tb_usuario_ape"],
            $value["tb_usuario_dni"],
            $value["tb_usuariogrupo_id"],
            $value["tb_usuarioperfil_id"],
            $value["tb_empresa_id"]
            )
        );
      }
    }
    print_r(json_encode($arrayElementos));
  $result = NULL;
?>
