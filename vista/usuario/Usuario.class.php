<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class Usuario extends Conexion
{

  public $tb_usuario_id;
  public $ttb_usuario_reg;
  public $tb_usuario_xac;
  public $tb_usuario_use;
  public $tb_usuario_pas;
  public $tb_usuario_cla;
  public $tb_usuario_nom;
  public $tb_usuario_ape;
  public $tb_usuario_ema;
  public $tb_usuario_tel;
  public $tb_usuario_dir;
  public $tb_usuario_dni;
  public $tb_usuario_mod;
  public $tb_usuario_ultvis;
  public $tb_usuario_estado;
  public $tb_usuario_mos;
  public $tb_usuariogrupo_id;
  public $tb_usuarioperfil_id;
  public $tb_empresa_id;
  public $tb_usuario_ofi;
  public $tb_usuario_tur;
  public $tb_usuario_ing1;
  public $tb_usuario_sal1;
  public $tb_usuario_ing2;
  public $tb_usuario_sal2;
  public $tb_usuario_per;
  public $tb_usuario_suel;
  public $tb_usuario_hor;
  public $tb_usuario_afp;
  public $tb_usuario_eps;
  public $tb_usuario_onp;
  public $tb_usuario_fot;
  public $tb_usuario_tem;
  public $tb_usuario_asig_fam;
  public $tb_usuario_frase;

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      //$sql = "INSERT INTO tb_usuario(tb_usuario_use, tb_usuario_pas, tb_usuario_nom, tb_usuario_ape, tb_usuario_ema, tb_usuario_tel, tb_usuario_dir, tb_usuario_dni, tb_usuariogrupo_id, tb_usuarioperfil_id, tb_usuario_per) VALUES (:usuario_use, md5(:usuario_pas), :usuario_nom, :usuario_ape, :usuario_ema, :usuario_tel, :usuario_dir, :usuario_dni, :usuariogrupo_id, :usuarioperfil_id, :usuario_per, :usuario_blo)";
      $sql = "INSERT INTO tb_usuario (
              tb_usuario_use, 
              tb_usuario_pas, 
              tb_usuario_nom, 
              tb_usuario_ape, 
              tb_usuario_ema, 
              tb_usuario_tel, 
              tb_usuario_dir, 
              tb_usuario_dni, 
              tb_usuariogrupo_id, 
              tb_usuarioperfil_id, 
              tb_usuario_per, 
              tb_usuario_estado, 
              tb_usuario_asig_fam, 
              tb_usuario_cla,
              tb_empresa_id,
              tb_usuario_ofi,
              tb_usuario_frase)
          VALUES (
                  :usuario_use, 
                  md5(:usuario_pas), 
                  :usuario_nom, 
                  :usuario_ape, 
                  :usuario_ema, 
                  :usuario_tel, 
                  :usuario_dir, 
                  :usuario_dni, 
                  :usuariogrupo_id, 
                  :usuarioperfil_id, 
                  :usuario_per, 
                  :usuario_estado, 
                  :usuario_asig_fam, 
                  :usuario_pas,
                  :empresa_id,
                  :usuario_ofi,
                  :usuario_frase) ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_use", $this->tb_usuario_use, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_pas", $this->tb_usuario_pas, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_nom", $this->tb_usuario_nom, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_ape", $this->tb_usuario_ape, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_ema", $this->tb_usuario_ema, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_tel", $this->tb_usuario_tel, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_dir", $this->tb_usuario_dir, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_dni", $this->tb_usuario_dni, PDO::PARAM_STR);
      $sentencia->bindParam(":usuariogrupo_id", $this->tb_usuariogrupo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuarioperfil_id", $this->tb_usuarioperfil_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_per", $this->tb_usuario_per, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_estado", $this->tb_usuario_estado, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_asig_fam", $this->tb_usuario_asig_fam, PDO::PARAM_INT);
      $sentencia->bindParam(":empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_ofi", $this->tb_usuario_ofi, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_frase", $this->tb_usuario_frase, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      //$sql = "UPDATE tb_usuario SET tb_usuario_use =:usuario_use, tb_usuario_pas =:usuario_pas, tb_usuario_nom =:usuario_nom, tb_usuario_ape =:usuario_ape, tb_usuario_ema =:usuario_ema, tb_usuario_tel =:usuario_tel, tb_usuario_dir =:usuario_dir, tb_usuario_dni =:usuario_dni, tb_usuariogrupo_id =:usuariogrupo_id, tb_usuarioperfil_id =:usuarioperfil_id, tb_usuario_per =:usuario_per  WHERE tb_usuario_id =:usuario_id";
      $sql = " UPDATE tb_usuario SET 
                                    tb_usuario_use=:usuario_use, 
                                    tb_usuario_pas=md5(:usuario_pas), 
                                    tb_usuario_nom=:usuario_nom, 
                                    tb_usuario_ape=:usuario_ape, 
                                    tb_usuario_ema=:usuario_ema, 
                                    tb_usuario_tel=:usuario_tel, 
                                    tb_usuario_dir=:usuario_dir, 
                                    tb_usuario_dni=:usuario_dni, 
                                    tb_usuariogrupo_id=:usuariogrupo_id, 
                                    tb_usuarioperfil_id=:usuarioperfil_id, 
                                    tb_usuario_per=:usuario_per, 
                                    tb_usuario_estado=:usuario_estado, 
                                    tb_usuario_asig_fam=:usuario_asig_fam, 
                                    tb_usuario_cla=:usuario_pas,
                                    tb_empresa_id=:empresa_id,
                                    tb_usuario_ofi =:usuario_ofi
                            WHERE 
                                    tb_usuario_id =:usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_use", $this->tb_usuario_use, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_pas", $this->tb_usuario_pas, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_nom", $this->tb_usuario_nom, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_ape", $this->tb_usuario_ape, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_ema", $this->tb_usuario_ema, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_tel", $this->tb_usuario_tel, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_dir", $this->tb_usuario_dir, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_dni", $this->tb_usuario_dni, PDO::PARAM_STR);
      $sentencia->bindParam(":usuariogrupo_id", $this->tb_usuariogrupo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuarioperfil_id", $this->tb_usuarioperfil_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_per", $this->tb_usuario_per, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_estado", $this->tb_usuario_estado, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_asig_fam", $this->tb_usuario_asig_fam, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_ofi", $this->tb_usuario_ofi, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function mostrarUno($usuario_id)
  {
    try {
      $sql = "SELECT * FROM tb_usuario usu INNER JOIN tb_usuariogrupo gru on gru.tb_usuariogrupo_id = usu.tb_usuariogrupo_id INNER JOIN tb_usuarioperfil per on per.tb_usuarioperfil_id = usu.tb_usuarioperfil_id WHERE tb_usuario_id =:usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay usuarios registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function moficar_clave_frase($usuario_cla, $usuario_frase, $usuario_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_usuario SET tb_usuario_pas = md5(:usuario_cla), tb_usuario_cla = :usuario_cla, tb_usuario_frase = :usuario_frase WHERE tb_usuario_id = :usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_cla", $usuario_cla, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_frase", $usuario_frase, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_usuarios()
  {
    try {
      $sql = "SELECT usu.*,tb_usuariogrupo_nom,tb_usuarioperfil_nom FROM tb_usuario usu 
        INNER JOIN tb_usuariogrupo gru on gru.tb_usuariogrupo_id = usu.tb_usuariogrupo_id 
        INNER JOIN tb_usuarioperfil per on per.tb_usuarioperfil_id = usu.tb_usuarioperfil_id 
        WHERE tb_usuario_xac = 1 AND tb_usuario_blo=0 ORDER BY tb_usuario_nom asc";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay usuarios registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

  function filtrar($usugru_id, $mos, $empresa_id)
  {
    try {
      $sql = "SELECT * FROM tb_usuario u
                    INNER JOIN tb_usuariogrupo ug ON u.tb_usuariogrupo_id=ug.tb_usuariogrupo_id
                    INNER JOIN tb_usuarioperfil up ON u.tb_usuarioperfil_id=up.tb_usuarioperfil_id
                    WHERE tb_usuario_xac=1 and tb_usuario_blo = 0 AND tb_empresa_id=:tb_empresa_id";

      if ($usugru_id > 0) $sql .= " AND u.tb_usuariogrupo_id=$usugru_id ";
      if (intval($mos) > 0) {
        if (intval($mos) == 1)
          $sql .= " AND u.tb_usuario_mos in(1,2)";
        else
          $sql .= " AND u.tb_usuario_mos =" . $mos;
      }

      $sql .= "	ORDER BY tb_usuario_nom";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No usuarios Registrados en Esta Sucursal";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

  function filtrar_todos()
  {
    try {
      $sql = "SELECT * FROM tb_usuario u
                    INNER JOIN tb_usuariogrupo ug ON u.tb_usuariogrupo_id=ug.tb_usuariogrupo_id
                    INNER JOIN tb_usuarioperfil up ON u.tb_usuarioperfil_id=up.tb_usuarioperfil_id
                    WHERE tb_usuario_xac=1 and tb_usuario_blo = 0 ORDER BY tb_usuario_nom";


      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No usuarios Registrados en Esta Sucursal";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
  function sueldoforma_usuario_fecha($usu_id, $mes_anio)
  {
    try {
      $sql = "SELECT * FROM tb_sueldoforma WHERE tb_usuario_id = :tb_usuario_id AND tb_sueldoforma_fec = :tb_sueldoforma_fec";


      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_sueldoforma_fec", $mes_anio, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No usuarios Registrados en Esta Sucursal";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

  function listar_usuarios_campo($usuario_columna, $usuario_valor, $param_tip, $empresa_id)
  {
    try {
      $param_tip = strtoupper($param_tip);

      if (!empty($usuario_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
        $filter = '';
        if(intval($empresa_id) > 0)
          $filter = ' AND usu.tb_empresa_id ='.intval($empresa_id);

        $sql = "SELECT usu.*,tb_usuariogrupo_nom,tb_usuarioperfil_nom FROM tb_usuario usu 
          INNER JOIN tb_usuariogrupo gru on gru.tb_usuariogrupo_id = usu.tb_usuariogrupo_id 
          INNER JOIN tb_usuarioperfil per on per.tb_usuarioperfil_id = usu.tb_usuarioperfil_id 
          WHERE tb_usuario_xac = 1 $filter AND " . $usuario_columna . ' =:usuario_valor ORDER BY tb_usuario_nom';

        $sentencia = $this->dblink->prepare($sql);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":usuario_valor", $usuario_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":usuario_valor", $usuario_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay usuarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } else
        return NULL;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
  function modificar_campo($usuario_id, $usuario_columna, $usuario_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      $param_tip = strtoupper($param_tip);

      if (!empty($usuario_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_usuario SET " . $usuario_columna . " =:usuario_valor WHERE tb_usuario_id =:usuario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":usuario_valor", $usuario_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":usuario_valor", $usuario_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }


  function registrar_sueldoforma($usu_id, $mes_anio, $sueldo_tip)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_sueldoforma(tb_usuario_id, tb_sueldoforma_fec, tb_sueldoforma_tip) 
  					VALUES (:tb_usuario_id,:tb_sueldoforma_fec,:tb_sueldoforma_tip)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_sueldoforma_fec", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_sueldoforma_tip", $sueldo_tip, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function actualizar_sueldoforma($usu_id, $mes_anio, $sueldo_tip)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_sueldoforma SET tb_sueldoforma_tip =:tb_sueldoforma_tip WHERE tb_usuario_id =:tb_usuario_id AND tb_sueldoforma_fec =:tb_sueldoforma_fec";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_sueldoforma_tip", $sueldo_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_sueldoforma_fec", $mes_anio, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_trabajadores_oficina()
  {
    try {
      $sql = "SELECT * FROM tb_usuario WHERE tb_usuario_ofi = 1"; // los quetiene 1 en columna ofi son los que trabajan en oficina

      $sentencia = $this->dblink->prepare($sql);
      $result = $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay usuarios registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

  /* GERSON (28-04-24) */
  function listar_trabajadores_sede($sede_id)
  {
    try {
      $sql = "SELECT * FROM tb_usuario WHERE tb_usuario_tur = 1";
      if(intval($sede_id) > 0){
        $sql .= " AND tb_empresa_id = ".$sede_id;
      }
      $sentencia = $this->dblink->prepare($sql);
      $result = $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay usuarios registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
  /*  */

  /* GERSON (14-03-23) */
  function mostrarSueldoUsuario($usuario_id, $fecini, $fecfin)
  {
    try {
      $sql = "SELECT det.*
                FROM tb_usuario_detalle det 
                INNER JOIN tb_usuario usu on usu.tb_usuario_id = det.tb_usuario_id 
                WHERE det.tb_usuario_id = :usuario_id 
                AND det.tb_usuario_detalle_fecini <= :usuario_detalle_fecini 
                AND det.tb_usuario_detalle_fecfin >= :usuario_detalle_fecfin 
                AND det.tb_usuario_detalle_xac = 1 ORDER BY det.tb_usuario_detalle_id DESC LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_detalle_fecini", $fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_detalle_fecfin", $fecfin, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay sueldos de usuario registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarSueldoUsuarioActual($usuario_id, $fecini)
  {
    try {
      $sql = "SELECT * 
                FROM tb_usuario_detalle det 
                INNER JOIN tb_usuario usu on usu.tb_usuario_id = det.tb_usuario_id 
                WHERE det.tb_usuario_id = :usuario_id 
                AND det.tb_usuario_detalle_fecini <= :usuario_detalle_fecini 
                AND det.tb_usuario_detalle_xac = 1 AND det.tb_usuario_detalle_fecfin IS NULL ORDER BY det.tb_usuario_detalle_id DESC LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_detalle_fecini", $fecini, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay sueldos de usuario registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarUltimoSueldoNew($usuario_id)
  {
    try {
      $sql = "SELECT * 
                FROM tb_usuario_detalle det
                INNER JOIN tb_usuario usu on usu.tb_usuario_id = det.tb_usuario_id 
                WHERE det.tb_usuario_id = :usuario_id 
                AND det.tb_usuario_detalle_xac = 1 ORDER BY det.tb_usuario_detalle_id DESC LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay sueldos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarUltimoSueldo($usuario_id)
  {
    try {
      $sql = "SELECT * 
                FROM tb_usuario_detalle det 
                INNER JOIN tb_usuario usu on usu.tb_usuario_id = det.tb_usuario_id 
                INNER JOIN tb_usuariogrupo gru on gru.tb_usuariogrupo_id = usu.tb_usuariogrupo_id 
                INNER JOIN tb_usuarioperfil per on per.tb_usuarioperfil_id = usu.tb_usuarioperfil_id 
                WHERE det.tb_usuario_id = :usuario_id 
                AND tb_usuario_detalle_xac = 1 ORDER BY det.tb_usuario_detalle_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay sueldos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function actualizar_sueldo_anterior($usuario_detalle_id, $tb_usuario_detalle_fecfin)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_usuario_detalle SET tb_usuario_detalle_fecfin =:tb_usuario_detalle_fecfin WHERE tb_usuario_detalle_id =:tb_usuario_detalle_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_detalle_fecfin", $tb_usuario_detalle_fecfin, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_detalle_id", $usuario_detalle_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function registrar_sueldo_nuevo($user_reg, $tb_usuario_detalle_fecini, $nuevo_sueldo, $usuario_id, $tipo_personal)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_usuario_detalle(tb_usuario_detalle_usereg, tb_usuario_detalle_fecini, tb_usuario_detalle_fecfin, tb_usuario_detalle_suel, tb_usuario_id, tb_usuario_per) 
  					VALUES (:tb_user_reg,:tb_usuario_detalle_fecini,NULL,:tb_usuario_detalle_suel,:tb_usuario_id,:tb_usuario_per)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_user_reg", $user_reg, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_detalle_fecini", $tb_usuario_detalle_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_detalle_suel", $nuevo_sueldo, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_per", $tipo_personal, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  /* GERSON (29-05-23) */
  function registrar_tipo_personal_nuevo($user_reg, $tb_usuario_detalle_fecini, $mismo_sueldo, $usuario_id, $tipo_personal)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_usuario_detalle(tb_usuario_detalle_usereg, tb_usuario_detalle_fecini, tb_usuario_detalle_fecfin, tb_usuario_detalle_suel, tb_usuario_id, tb_usuario_per) 
  					VALUES (:tb_user_reg,:tb_usuario_detalle_fecini,NULL,:tb_usuario_detalle_suel,:tb_usuario_id,:tb_usuario_per)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_user_reg", $user_reg, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_detalle_fecini", $tb_usuario_detalle_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_detalle_suel", $mismo_sueldo, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_per", $tipo_personal, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  /*  */

  /* GERSON (27-03-23) */
  function listar_afps()
  {
    try {
      $sql = "SELECT tb_afps_id, tb_afps_nom 
                FROM tb_afps
                LIMIT 0,11";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay sueldos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  /* GERSON (28-03-23) */
  function modificar_campo_detalle($usuariodetalle_id, $usuariodetalle_columna, $usuariodetalle_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      $param_tip = strtoupper($param_tip);

      if (!empty($usuariodetalle_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_usuario_detalle SET " . $usuariodetalle_columna . " =:usuariodetalle_valor WHERE tb_usuario_detalle_id =:usuario_detalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_detalle_id", $usuariodetalle_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":usuariodetalle_valor", $usuariodetalle_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":usuariodetalle_valor", $usuariodetalle_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  /*  */

  /* GERSON (26-12-23) */
  function registrar_comision_asesor($tipo, $valor, $mes, $anio, $usuario_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_comisionasesor(
                            tb_comisionasesor_xac, 
                            tb_comisionasesor_fecreg, 
                            tb_comisionasesor_tip, 
                            tb_comisionasesor_valor, 
                            tb_comisionasesor_mes, 
                            tb_comisionasesor_anio, 
                            tb_usuario_id,
                            tb_comisionasesor_est) 
                    VALUES (1,
                            NOW(),
                            :comisionasesor_tip,
                            :comisionasesor_valor,
                            :comisionasesor_mes,
                            :comisionasesor_anio,
                            :usuario_id,
                            1)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":comisionasesor_tip", $tipo, PDO::PARAM_STR);
      $sentencia->bindParam(":comisionasesor_valor", $valor, PDO::PARAM_STR);
      $sentencia->bindParam(":comisionasesor_mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":comisionasesor_anio", $anio, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  

  function obtener_comision_asesor($mes, $anio, $usuario_id)
  {
    try {
      $sql = "SELECT *
                FROM tb_comisionasesor
                WHERE tb_comisionasesor_xac = 1
                AND tb_comisionasesor_mes = :mes
                AND tb_comisionasesor_anio = :anio
                AND tb_usuario_id = :usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay sueldos registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function eliminar_comision_asesor($mes, $anio, $usuario_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_comisionasesor SET tb_comisionasesor_xac = 0 WHERE tb_usuario_id = :usuario_id AND tb_comisionasesor_mes = :mes AND tb_comisionasesor_anio = :anio";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function usuario_autocomplete($dato)
  {
    try {

      $filtro = "%" . $dato . "%";

      $sql = "SELECT *
  FROM tb_usuario 
              WHERE CONCAT(tb_usuario_nom, ' ', tb_usuario_ape) LIKE :filtro OR tb_usuario_dni LIKE :filtro";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Usuarios registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */
}
