<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../funciones/funciones.php');

  $direc = 'usuario';
  $session_usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $usuario_id = $_POST['usuario_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Usuario Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Usuario';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Usuario';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Usuario';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en usuario
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $session_usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'usuario'; $modulo_id = $usuario_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    $usuario_ofi = NULL;
    //si la accion es modificar, mostramos los datos del usuario por su ID
    if(intval($usuario_id) > 0){
      $result = $oUsuario->mostrarUno($usuario_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el usuario seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $usuario_nom = $result['data']['tb_usuario_nom'];
          $usuario_ape = $result['data']['tb_usuario_ape'];
          $usuario_dni = $result['data']['tb_usuario_dni'];
          $usuario_dir = $result['data']['tb_usuario_dir'];
          $usuario_tel = $result['data']['tb_usuario_tel'];
          $usuario_ema = $result['data']['tb_usuario_ema'];
          $usuario_use = $result['data']['tb_usuario_use'];
          $usuario_cla = $result['data']['tb_usuario_cla'];
          $usuario_per = $result['data']['tb_usuario_per'];
          $usuario_estado = $result['data']['tb_usuario_estado'];
          $usuario_asig_fam = $result['data']['tb_usuario_asig_fam'];
          $usuarioperfil_id = $result['data']['tb_usuarioperfil_id'];
          $usuariogrupo_id = $result['data']['tb_usuariogrupo_id'];
          $empresa_id = $result['data']['tb_empresa_id'];
          $usuario_frase = $result['data']['tb_usuario_frase'];
          $usuario_ofi = intval($result['data']['tb_usuario_ofi']);
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_usuario" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_usuario" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_usuario_id" value="<?php echo $usuario_id;?>">
          
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-6 col-sm-6">
            		<div class="form-group">
		              <label for="txt_usuario_nom" class="control-label">Nombres</label>
		              <input type="text" name="txt_usuario_nom" id="txt_usuario_nom" class="form-control input-sm mayus" value="<?php echo $usuario_nom;?>">
		            </div>       
		            <div class="form-group">
		              <label for="txt_usuario_dni" class="control-label">DNI</label>
		              <input type="text" name="txt_usuario_dni" id="txt_usuario_dni" class="form-control input-sm" value="<?php echo $usuario_dni;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_usuario_tel" class="control-label">Telefono</label>
		              <input type="text" name="txt_usuario_tel" id="txt_usuario_tel" class="form-control input-sm" value="<?php echo $usuario_tel;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_usuario_use" class="control-label">Usuario</label>
		              <input type="text" name="txt_usuario_use" id="txt_usuario_use" class="form-control input-sm" value="<?php echo $usuario_use;?>">
		            </div>
		            <div class="form-group">
		              <label for="cmb_usuario_per" class="control-label">Tipo Personal</label>
		              <select name="cmb_usuario_per" id="cmb_usuario_per" class="form-control">
		                <option value="0"></option>
		                <option value="1" <?php if($usuario_per == 1) echo 'selected'; ?>>Gefatura</option>
		                <option value="2" <?php if($usuario_per == 2) echo 'selected'; ?>>Ventas</option>
		                <option value="3" <?php if($usuario_per ==3) echo 'selected'; ?>>Administrador de Sede</option>
		              </select>
		              <input type="hidden" name="txt_usuario_per_old" id="txt_usuario_per_old" class="form-control input-sm" value="<?php echo $usuario_per;?>">
		            </div>
		            <div class="form-group">
		              <label for="cmb_usuariogrupo_id" class="control-label">Grupo Usuario</label>
		              <select name="cmb_usuariogrupo_id" id="cmb_usuariogrupo_id" class="form-control">
		                <?php require_once('../usuariogrupo/usuariogrupo_select.php'); ?>
		              </select>
		            </div>
                <div class="form-group">
                  <label for="">Sede :</label>
                  <select class="form-control input-sm" id="cmb_empresa_id" name="cmb_empresa_id">
                    <?php require_once '../empresa/empresa_select.php';?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">¿Trabajará en Oficina? : <?php echo $usuario_ofi;?></label>
                  <select class="form-control input-sm" id="cmb_usuario_ofi" name="cmb_usuario_ofi">
                    <option value="">Seleciona...</option>
                    <option value="0" <?php if($usuario_ofi === 0) echo 'selected'; ?> >NO</option>
                    <option value="1" <?php if($usuario_ofi == 1) echo 'selected'; ?> >SI</option>
                  </select>
                </div>
            	</div>
            	<!-- GRUPO DERECHO-->
            	<div class="col-md-6 col-sm-6">
            		<div class="form-group">
		              <label for="txt_usuario_ape" class="control-label">Apellidos</label>
		              <input type="text" name="txt_usuario_ape" id="txt_usuario_ape" class="form-control input-sm mayus" value="<?php echo $usuario_ape;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_usuario_dir" class="control-label">Dirección</label>
		              <input type="text" name="txt_usuario_dir" id="txt_usuario_dir" class="form-control input-sm" value="<?php echo $usuario_dir;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_usuario_ema" class="control-label">Email</label>
		              <input type="text" name="txt_usuario_ema" id="txt_usuario_ema" class="form-control input-sm" value="<?php echo $usuario_ema;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_usuario_pas" class="control-label">Contraseña</label>
                              <input type="password" name="txt_usuario_pas" id="txt_usuario_pas" class="form-control input-sm" value="<?php echo $usuario_cla;?>">
		            </div>
		            <div class="form-group">
		              <label for="cmb_usuarioperfil_id" class="control-label">Perfil Usuario</label>
		              <select name="cmb_usuarioperfil_id" id="cmb_usuarioperfil_id" class="form-control">
		                <?php require_once('../usuarioperfil/usuarioperfil_select.php'); ?>
		              </select>
		            </div>
                <div class="form-group">
                  <label for="cmb_usuario_estado" class="control-label">Opción de Bloqueo</label>
                  <select name="cmb_usuario_estado" id="cmb_usuario_estado" class="form-control">
                    <option value="0">Activo</option>
                    <option value="1" <?php if($usuario_estado == 1) echo 'selected'; ?>>Bloqueado</option>
                  </select>
                </div>
                <!-- GERSON (27-03-23) -->
                <div class="form-group">
                  <label for="cmb_usuario_asig_fam" class="control-label">Asignación Familiar</label>
                  <select name="cmb_usuario_asig_fam" id="cmb_usuario_asig_fam" class="form-control">
                    <option value="0">NO</option>
                    <option value="1" <?php if($usuario_asig_fam == 1) echo 'selected'; ?>>SI</option>
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="txt_usuario_frase" class="control-label">Frase secreta de Verificación</label>
                  <input type="text" name="txt_usuario_frase" id="txt_usuario_frase" class="form-control input-sm" value="<?php echo $usuario_frase;?>">
                </div>
            	</div>
            </div>
          
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Usuario?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="usuario_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_usuario">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_usuario">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_usuario">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/usuario/usuario_form.js?ver=11';?>"></script>
