
$(document).ready(function(){

  $('#form_usuario').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"usuario/usuario_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_usuario").serialize(),
				beforeSend: function() {
					$('#usuario_mensaje').show(400);
					$('#btn_guardar_usuario').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#usuario_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#usuario_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		usuario_tabla();
		      		$('#modal_registro_usuario').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#usuario_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#usuario_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_usuario').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#usuario_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#usuario_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_usuario_nom: {
				required: true,
				minlength: 4
			},
			txt_usuario_ape: {
				required: true,
				minlength: 4
			},
			txt_usuario_dni: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 8
			},
			txt_usuario_dir: {
				required: true
			},
			txt_usuario_tel: {
				required: true,
				digits: true,
				minlength: 9,
				maxlength: 9
			},
			txt_usuario_ema: {
				required: true,
				email: true
			},
			txt_usuario_use: {
				required: true,
				nowhitespace: true
				//lettersonly: true
			},
			txt_usuario_pas: {
				required: true
			},
			cmb_usuario_per: {
				required: true,
				min: 1
			},
			cmb_usuarioperfil_id: {
				required: true,
				min: 1
			},
			cmb_usuariogrupo_id: {
				required: true,
				min: 1
			},
			cmb_empresa_id: {
				required: true,
				min: 1
			},
			cmb_usuario_ofi: {
				required: true,
				min: 0
			}
		},
		messages: {
			txt_usuario_nom: {
				required: "Ingrese los nombres del Usuario",
				minlength: "Como mínimo el nombre debe tener 4 caracteres"
			},
			txt_usuario_ape: {
				required: "Ingrese los nombres del Usuario",
				minlength: "Como mínimo los apellidos deben tener 4 caracteres"
			},
			txt_usuario_dni: {
				required: "Ingrese el DNI del Usuario",
				digits: "Solo números por favor",
				minlength: "El DNI como mínimo debe tener 8 números",
				maxlength: "El DNI como máximo debe tener 8 números"
			},
			txt_usuario_dir: {
				required: "Ingrese dirección del Usuario"
			},
			txt_usuario_tel: {
				required: "Ingrese el Teléfono del Usuario",
				digits: "Solo números por favor",
				minlength: "El Teléfono como mínimo debe tener 9 números",
				maxlength: "El Teléfono como máximo debe tener 9 números"
			},
			txt_usuario_ema: {
				required: "Ingrese el Email del Usuario",
				email: "Ingrese un Email válido"
			},
			txt_usuario_use: {
				required: "Ingrese un nombre de Usuario",
				nowhitespace: "No puede contener espacios"
			},
			txt_usuario_pas: {
				required: "Ingrese una contraseña para el Usuario"
			},
			cmb_usuario_per: {
				required: "Seleccione un Tipo de Personal",
				min: "Seleccione un Tipo de Personal"
			},
			cmb_usuarioperfil_id: {
				required: "Seleccione un Perfil de Personal",
				min: "Seleccione un Tipo de Personal"
			},
			cmb_usuariogrupo_id: {
				required: "Seleccione un Grupo de Personal",
				min: "Seleccione un Grupo de Personal"
			},
			cmb_empresa_id: {
				required: "Debes seleccionar la sede",
				min: "Debes seleccionar una sede"
			},
			cmb_usuario_ofi: {
				required: "Indique si es trabajador de oficina",
				min: "Indique si es trabajador de oficina"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});

	//Gerson (28-03-23)
    $("#form_remuneracion").validate({
        submitHandler: function (vista) {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "usuario/usuario_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_remuneracion").serialize(),
                beforeSend: function () {
					$('#usuario_remuneracion_mensaje').show(400);
					$('#btn_guardar_remuneracion').prop('disabled', true);
                },
                success: function (data) {
                    swal_success("SISTEMA", data.mensaje, 2000);
                    usuario_tabla();
					
                },
                complete: function (data) {
					$("#usuario_remuneracion_mensaje").hide(400);
                    $('#modal_usuario_remuneracion').modal('hide');
                }

            });
        },
        rules: {
            txt_usuario_suel: {
                required: true
            },
            txt_usuario_hor: {
                required: true
            }
        },
        messages: {
            txt_usuario_suel: {
                required: 'Debe ingresar sueldo válido'
            },
            txt_usuario_hor: {
                required: 'Debe ingresar una cantidad de horas válida'
            }
        }
    });
    //
});

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}

function solo_numero(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode > 31 && (charCode < 48 || charCode > 57)){
		return false;
	}
	return true;
} 