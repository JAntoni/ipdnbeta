<?php
	session_name("ipdnsac");
	session_start();

	require_once('Usuario.class.php');
  $oUsuario = new Usuario();

  $action = $_POST['action'];

  if($action == 'tema'){
  	$tema_clase = $_POST['tema_clase'];

  	$usuario_id = intval($_SESSION['usuario_id']);
  	$usuario_columna = 'tb_usuario_tem';
  	$usuario_valor = $tema_clase;
  	$param_tip = 'STR';
  	
  	$data['estado'] = 0;
  	$data['mensaje'] = 'Error al guardar el tema del Usuario / '.$usuario_id.' / colum: '.$usuario_columna.' / valor: '.$tema_clase.' / param: '.$param_tip;
  	
  	if($usuario_id > 0){
	  	if($oUsuario->modificar_campo($usuario_id, $usuario_columna, $usuario_valor, $param_tip)){
	  		$data['estado'] = 1;
	  		$data['mensaje'] = 'Tema de Usuario Guardado';
	  	}
	  }
	  else
	  	$data['mensaje'] = 'No ha iniciado sesión, el Usuario no tiene ID';

	  echo json_encode($data);
  }
  
?>