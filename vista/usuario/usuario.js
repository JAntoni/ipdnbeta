var datatable_global;

function usuario_form(usuario_act, usuario_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"usuario/usuario_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      usuario_id: usuario_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_usuario_form').html(data);
      	$('#modal_registro_usuario').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_usuario'); //funcion encontrada en public/js/generales.js

      	modal_hidden_bs_modal('modal_registro_usuario', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'usuario';
      	var div = 'div_modal_usuario_form';
      	permiso_solicitud(usuario_act, usuario_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function usuario_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"usuario/usuario_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#usuario_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_usuario_tabla').html(data);
      $('#usuario_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#usuario_mensaje_tbl').html('ERROR AL LISTAR: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_usuarios').DataTable({
    "responsive": true,
    "pageLength": 100,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
      columnDefs: [
         { orderable: false, targets: [-1,-5] }
      ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}
$(document).ready(function(){
  
  estilos_datatable();
});

/* GERSON (27-03-23) */
function usuario_remuneracion(usuario_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"usuario/usuario_remuneracion.php",
		async:true,
		dataType: "html",                      
		data: ({
			usuario_id: usuario_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_remuneracion').html(html);		
      $('#modal_usuario_remuneracion').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_usuario_remuneracion', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_usuario_remuneracion'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}
/*  */
