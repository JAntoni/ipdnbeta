<?php
	session_name("ipdnsac");
  session_start();

 	require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();

 	$action = $_POST['action'];
        
        
	$oUsuario->tb_usuario_id = intval($_POST['hdd_usuario_id']);
	$oUsuario->tb_usuario_use = $_POST['txt_usuario_use']; 
	$oUsuario->tb_usuario_pas = $_POST['txt_usuario_pas']; 
	$oUsuario->tb_usuario_nom =mb_strtoupper($_POST['txt_usuario_nom'], 'UTF-8'); 
	$oUsuario->tb_usuario_ape = mb_strtoupper($_POST['txt_usuario_ape'], 'UTF-8');
	$oUsuario->tb_usuario_ema = $_POST['txt_usuario_ema']; 
	$oUsuario->tb_usuario_tel = $_POST['txt_usuario_tel']; 
	$oUsuario->tb_usuario_dir = $_POST['txt_usuario_dir']; 
	$oUsuario->tb_usuario_dni = $_POST['txt_usuario_dni']; 
	$oUsuario->tb_usuariogrupo_id = intval($_POST['cmb_usuariogrupo_id']); 
	$oUsuario->tb_usuarioperfil_id = intval($_POST['cmb_usuarioperfil_id']); 
	$oUsuario->tb_usuario_per = intval($_POST['cmb_usuario_per']); 
	$usuario_per_old = intval($_POST['cmb_usuario_per_old']); 
	$oUsuario->tb_usuario_estado = intval($_POST['cmb_usuario_estado']);
	$oUsuario->tb_usuario_asig_fam = intval($_POST['cmb_usuario_asig_fam']);
	$oUsuario->tb_usuario_pas = $_POST['txt_usuario_pas']; 
	$oUsuario->tb_empresa_id = intval($_POST['cmb_empresa_id']);
	$oUsuario->tb_usuario_ofi = intval($_POST['cmb_usuario_ofi']);
	$oUsuario->tb_usuario_frase = $_POST['txt_usuario_frase'];

 	if($action == 'insertar'){

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Usuario';

 		if($oUsuario->insertar()){
			$data['estado'] = 1;
			$data['mensaje'] = 'Usuario registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Usuario';

 		if($oUsuario->modificar()){
			// Gerson(29-05-23)
			$usuario_det = $oUsuario->mostrarUltimoSueldoNew(intval($_POST['hdd_usuario_id']));
			$fecha_hoy = explode("-", date('Y-m-d'));
			$anio = $fecha_hoy[0];
			$mes = $fecha_hoy[1];
			$month = $anio.'-'.$mes;
			$aux = $month."-01";
			$fecha_fin = date('Y-m-d', strtotime("{$aux} - 1 day"));
			if($_POST['cmb_usuario_per']!='' || $_POST['cmb_usuario_per']!=null){
				if($usuario_per_old != intval($_POST['cmb_usuario_per'])){
					if($usuario_det['estado']==1){
						if($fecha_fin <= $usuario_det['data']['tb_usuario_detalle_fecini']){ // si ya se hizo un cambio en el mismo mes o se cometió un error solo cambia el tipo personal
							$oUsuario->modificar_campo_detalle($usuario_det['data']['tb_usuario_detalle_id'], 'tb_usuario_per', intval($_POST['cmb_usuario_per']), 'INT');
						}else{ // Se actualiza la fecha fin del ultimo sueldo y se crea el nuevo registro con el nuevo tipo personal que parte desde el dia 1 del mes en curso
							$oUsuario->actualizar_sueldo_anterior($usuario_det['data']['tb_usuario_detalle_id'], $fecha_fin); // Se reutiliza la función de sueldo
							$oUsuario->registrar_tipo_personal_nuevo($_SESSION['usuario_id'], $aux, $usuario_det['data']['tb_usuario_detalle_suel'], intval($_POST['hdd_usuario_id']), intval($_POST['cmb_usuario_per']));
						}
					}
				}
				//
				$data['estado'] = 1;
				$data['mensaje'] = 'Usuario modificado correctamente. '.$usuario_des;
			}else{
				$data['estado'] = 0;
				$data['mensaje'] = 'No se puede modificar usuario, tipo personal inválido.';
			}
			
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$usuario_id = intval($_POST['hdd_usuario_id']);
 		$usuario_columna = 'tb_usuario_xac';
 		$usuario_valor = 0; //el valor de xac en usuario es eliminado
 		$param_tip = 'INT';

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Usuario ';

 		if($oUsuario->modificar_campo($usuario_id, $usuario_columna, $usuario_valor, $param_tip)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario eliminado correctamente. '.$usuario_des;
 		}

 		echo json_encode($data);
 	}

	 elseif($action == 'remuneracion'){

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al modificar remuneración del Usuario';

		$tb_usuario_id = intval($_POST['hdd_usuario_id']);
        $tb_usuario_nrocuenta = $_POST['txt_usuario_nrocuenta']; 
        $tb_usuario_tip_tur = intval($_POST['cmb_usuario_tip_tur']); 
        $tb_usuario_afp = intval($_POST['cmb_usuario_afp']); 
        $tb_usuario_suel = floatval($_POST['txt_usuario_suel']); 
        $tb_usuario_suel_old = floatval($_POST['txt_usuario_suel_old']); 
        $tb_usuario_hor = intval($_POST['txt_usuario_hor']); 
		$hoy = date('Y-m-d');

		if($tb_usuario_id){

			$oUsuario->modificar_campo($tb_usuario_id, 'tb_usuario_tur', $tb_usuario_tip_tur, 'INT');
			$oUsuario->modificar_campo($tb_usuario_id, 'tb_usuario_afp', $tb_usuario_afp, 'INT');
			$oUsuario->modificar_campo($tb_usuario_id, 'tb_usuario_hor', $tb_usuario_hor, 'INT');
			$oUsuario->modificar_campo($tb_usuario_id, 'tb_usuario_nrocuenta', $tb_usuario_nrocuenta, 'STR');


			// GERSON (03-11-23)
			$tipo_personal = 0;
			$usuario_select = $oUsuario->mostrarUno($tb_usuario_id);
			if($usuario_select['estado'] == 1){
				$tipo_personal = $usuario_select['data']['tb_usuario_per'];
			}
			//

			if($tb_usuario_suel_old != $tb_usuario_suel){
				$usuario_det = $oUsuario->mostrarUltimoSueldoNew($tb_usuario_id);

				$fecha_hoy = explode("-", $hoy);
				$anio = $fecha_hoy[0];
				$mes = $fecha_hoy[1];
				$month = $anio.'-'.$mes;
				$aux = $month."-01";
				$fecha_fin = date('Y-m-d', strtotime("{$aux} - 1 day"));

				if($usuario_det['estado']==1){

					if($fecha_fin <= $usuario_det['data']['tb_usuario_detalle_fecini']){ // si ya se hizo un cambio en el mismo mes o se cometió un error solo cambia el sueldo del mismo registro
						$oUsuario->modificar_campo_detalle($usuario_det['data']['tb_usuario_detalle_id'], 'tb_usuario_detalle_suel', $tb_usuario_suel, 'INT');
						/* var_dump('cambio hecho en mas de 1 vez en el mismo mes');
						echo "<br>";
						echo $usuario_det['data']['tb_usuario_detalle_id']." | ".$tb_usuario_suel;
						echo "<br>"; */

					}else{ // Se actualiza la fecha fin del ultimo sueldo y se crea el nuevo registro con el nuevo sueldo que parte desde el dia 1 del mes en curso
						$oUsuario->actualizar_sueldo_anterior($usuario_det['data']['tb_usuario_detalle_id'], $fecha_fin);
						$oUsuario->registrar_sueldo_nuevo($_SESSION['usuario_id'], $aux, $tb_usuario_suel, $tb_usuario_id, $tipo_personal);
						/* var_dump('cambio hecho por primera vez en muchos meses');
						echo "<br>";
						echo $usuario_det['data']['tb_usuario_detalle_id']." | ".$fecha_fin." | ".$_SESSION['usuario_id']." | ".$aux." | ".$tb_usuario_suel." | ".$tb_usuario_id;
						echo "<br>"; */

					}
					
				}else{
					$oUsuario->registrar_sueldo_nuevo($_SESSION['usuario_id'], $aux, $tb_usuario_suel, $tb_usuario_id, $tipo_personal);
				}
			}

			$data['estado'] = 1;
			$data['mensaje'] = 'Remuneración de usuario modificado correctamente.';

		}

		

		echo json_encode($data);
	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>