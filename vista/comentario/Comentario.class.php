<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Comentario extends Conexion{
  private $tb_coment_id;
  private $tb_coment_usureg;
  private $tb_coment_fecreg;
  private $tb_coment_det;
  private $tb_coment_xac;
  private $tb_coment_nom_tabla;
  private $tb_coment_regmodid;

  /**
   * Get the value of tb_coment_id
   */
  public function getTbComentId()
  {
    return $this->tb_coment_id;
  }

  /**
   * Set the value of tb_coment_id
   */
  public function setTbComentId($tb_coment_id)
  {
    $this->tb_coment_id = $tb_coment_id;

    return $this;
  }

  /**
   * Get the value of tb_coment_usureg
   */
  public function getTbComentUsureg()
  {
    return $this->tb_coment_usureg;
  }

  /**
   * Set the value of tb_coment_usureg
   */
  public function setTbComentUsureg($tb_coment_usureg)
  {
    $this->tb_coment_usureg = $tb_coment_usureg;

    return $this;
  }

  /**
   * Get the value of tb_coment_fecreg
   */
  public function getTbComentFecreg()
  {
    return $this->tb_coment_fecreg;
  }

  /**
   * Set the value of tb_coment_fecreg
   */
  public function setTbComentFecreg($tb_coment_fecreg)
  {
    $this->tb_coment_fecreg = $tb_coment_fecreg;

    return $this;
  }

  /**
   * Get the value of tb_coment_det
   */
  public function getTbComentDet()
  {
    return $this->tb_coment_det;
  }

  /**
   * Set the value of tb_coment_det
   */
  public function setTbComentDet($tb_coment_det)
  {
    $this->tb_coment_det = $tb_coment_det;

    return $this;
  }

  /**
   * Get the value of tb_coment_xac
   */
  public function getTbComentXac()
  {
    return $this->tb_coment_xac;
  }

  /**
   * Set the value of tb_coment_xac
   */
  public function setTbComentXac($tb_coment_xac)
  {
    $this->tb_coment_xac = $tb_coment_xac;

    return $this;
  }

  /**
   * Get the value of tb_coment_regmodid
   */
  public function getTbComentRegmodid()
  {
    return $this->tb_coment_regmodid;
  }

  /**
   * Set the value of tb_coment_regmodid
   */
  public function setTbComentRegmodid($tb_coment_regmodid)
  {
    $this->tb_coment_regmodid = $tb_coment_regmodid;

    return $this;
  }

  /**
   * Get the value of tb_coment_nom_tabla
   */
  public function getTbComentNomTabla()
  {
    return $this->tb_coment_nom_tabla;
  }

  /**
   * Set the value of tb_coment_nom_tabla
   */
  public function setTbComentNomTabla($tb_coment_nom_tabla)
  {
    $this->tb_coment_nom_tabla = $tb_coment_nom_tabla;

    return $this;
  }

  public function insertar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_coment (tb_coment_usureg, tb_coment_det, tb_coment_nom_tabla, tb_coment_regmodid)
              VALUES ( :tb_coment_usureg, :tb_coment_det, :tb_coment_nom_tabla, :tb_coment_regmodid );";
      
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_coment_usureg", $this->tb_coment_usureg,PDO::PARAM_INT);
      $sentencia->bindParam(":tb_coment_det", $this->tb_coment_det, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_coment_nom_tabla", $this->tb_coment_nom_tabla, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_coment_regmodid", $this->tb_coment_regmodid, PDO::PARAM_INT);

      $result = $sentencia->execute(); //si se ejecuta, retorna 1
      $comentario_id = $this->dblink->lastInsertId(); //ultimo id insertado
      $this->dblink->commit();

      $data['estado'] = $result;
      $data['comentario_id'] = $comentario_id;
      return $data;
    }
    catch (Exception $th) {
      $this->dblink->rollBack();
      throw $th;
    }
  }

  public function eliminar(){
    $this->dblink->beginTransaction();
    try {
      //code...
      $sql = "UPDATE tb_coment set tb_coment_xac=0 WHERE tb_coment_id = :coment_id ;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':coment_id', $this->tb_coment_id,PDO::PARAM_INT);
      
      $result = $sentencia->execute();
      $this->dblink->commit();

      $data['estado'] = $result;
      return $data;
    } catch (Exception $th) {
      //throw $th;
      $this->dblink->rollBack();
      throw $th->getMessage();
    }
  }

  public function filtrar_comentarios_por($coment_regmodid, $nombre_tabla){ //nombre tabla puede ser tb_hist o tb_creditolinea o la tabla que contenga el historial
    try {
      $sql = "SELECT c.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM
              tb_coment c
              INNER JOIN tb_usuario ON tb_coment_usureg = tb_usuario_id
              WHERE tb_coment_regmodid = :coment_regmodid AND tb_coment_nom_tabla = :tb_coment_nom_tabla AND tb_coment_xac=1 ORDER BY tb_coment_fecreg";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":coment_regmodid", $coment_regmodid, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_coment_nom_tabla", $nombre_tabla, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $retorno["cantidad"] = intval($sentencia->rowCount());
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["cantidad"] = 0;
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function filtrar_comentarios1_por($id_comentario, $arreglo){
      try {
        $sql = "SELECT c.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot".$arreglo[0].
                " FROM tb_coment c
                INNER JOIN tb_usuario ON tb_coment_usureg = tb_usuario_id
                INNER JOIN ".$arreglo[1]." ON c.tb_coment_regmodid = ". $arreglo[2].
                " WHERE tb_coment_id = :coment_id ORDER BY tb_coment_fecreg;";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(':coment_id', $id_comentario, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $retorno["cantidad"] = intval($sentencia->rowCount());
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
      throw $e;
    }
  }

}
?>