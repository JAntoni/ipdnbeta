<?php
require_once '../../core/usuario_sesion.php';
require_once('../comentario/Comentario.class.php');
$oComentario = new Comentario();
//echo 'en que estás 🙌';exit();

$accion     = $_POST['accion'];
$coment_det = $_POST['coment_det'];
$regmodid    = intval($_POST['regmodid']);
$cant_coment= intval($_POST['cant_coment']);
$nom_tabla = $_POST['nom_tabla'];
$fecha_hoy  = date('d-m-Y h:i a');

if($accion == 'insertar'){
    if(verificar_detalle_comentario_vacio()){
        $oComentario->setTbComentUsureg($_SESSION['usuario_id']);
        $oComentario->setTbComentDet($coment_det);
        $oComentario->setTbComentRegmodid($regmodid);
        $oComentario->setTbComentNomTabla($nom_tabla);
        $resultado = $oComentario->insertar();
    }
    evaluar($resultado, 'registró');
}
elseif ($accion == 'eliminar') {
    # code...
    $oComentario->setTbComentId(intval($_POST['coment_id']));
    $resultado = $oComentario->eliminar();
    evaluar($resultado, 'eliminó');
}

function evaluar($resultado, $accion){
    global $oComentario, $cant_coment, $regmodid;
    if($resultado['estado'] > 0){
        $data['estado'] = 1;
        $data['mensaje'] = 'Se ' . $accion . ' el comentario.';

        if($accion == 'registró'){
            $data['datos'] = $resultado['comentario_id'];
            $data['html_new_coment'] = '';

            $arreglo[0] = $_POST['columnas']; //columnas que deseas añadir en el select
            $arreglo[1] = $_POST['tabla_alias']; //nombre y alias de la tabla donde se guarda el historial
            $arreglo[2] = $_POST['alias_columnaPK']; //alias y nombre de la columna primary key de la tabla donde esta el historial

            $result1 = $oComentario->filtrar_comentarios1_por(intval($resultado['comentario_id']), $arreglo);

            if($result1['estado'] == 1){
                if($cant_coment == 0){
                    $data['html_new_coment'] .= '<div class="box-footer box-comments" id="div_comentarios_anteriores-'.$regmodid.'">';
                }
                $data['html_new_coment'] .= '<div class="box-comment" id="com-'.intval($resultado['comentario_id']).'">
                                                <img class="img-circle img-sm" src="'.$result1['data'][0]['tb_usuario_fot'].'" alt="User Image">
                                                <div class="comment-text">
                                                    <span class="username">
                                                    '.$result1['data'][0]['tb_usuario_nom'].' '.$result1['data'][0]['tb_usuario_ape'];
                                                    if(intval($_SESSION['usuarioperfil_id']) == 1){
                                                        $data['html_new_coment'] .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true" onclick="comentario_eliminar('.intval($resultado['comentario_id']).','.$regmodid.')">x</button>';
                                                    }
                                                    $data['html_new_coment'] .= '</span>'.$result1['data'][0]['tb_coment_det'].'
                                                    <span class="text-muted pull-right">'. date("h:i a, j-m-y", strtotime($result1['data'][0]['tb_coment_fecreg'])) .'</span>
                                                </div>
                                            </div>';
                if($cant_coment == 0){
                    $data['html_new_coment'] .= '</div>';
                }
            }
        }
    }
    else{
        $data['estado'] = 0;
        $data['mensaje'] = 'Hubo un error';
    }
    echo json_encode($data);
}

function verificar_detalle_comentario_vacio(){
    global $coment_det;
    if(!empty($coment_det)){
        return true;
    }else{
        return false;
    }
}
