<?php
  date_default_timezone_set("America/Lima");
  require_once('../creditogarveh/Creditogarveh.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oCreditogarveh = new Creditogarveh();

  $estado = $_POST['filtro_est'];
  $tipogarveh = '';
  $cliente_id = '';
  $fecha1 = date("Y-m-d", strtotime($_POST['filtro_fec1']));
  $fecha2 = date("Y-m-d", strtotime($_POST['filtro_fec2']));
  $fecha_hoy = date('d-m-Y');
  
  $tr = '';
  //EJECUTA LA FUNCION 
  $result = $oCreditogarveh->filtro_creditos2($fecha1, $fecha2, $estado, $tipogarveh, $cliente_id);
    //SI ENCONTRÓ RESULTADOS
    if ($result['estado'] == 1) {
      //RECORRE LOS RESULTADOS ALMACENADOS EN DATA Y CADA LLAVE LA ASIGNA A VALUE
      foreach ($result['data'] as $key => $value) {
        $cuo_tipo = ' FIJA';
        if ($value['tb_cuotatipo_id'] == 3) {
          $cuo_tipo = ' LIBRE';
        }
        if ($value['tb_credito_tip'] == 1) $tip = "GARVEH";
        if ($value['tb_credito_tip'] == 2) $tip = "ADENDA";
        if ($value['tb_credito_tip'] == 3) $tip = "ACUERDO PAGO";
        if ($value['tb_credito_tip'] == 4) $tip = "GAR MOBILIARIA";
  
        if ($value['tb_credito_tip2'] == 1) $tip2 = " CREDITO REGULAR";
        if ($value['tb_credito_tip2'] == 2) $tip2 = " CREDITO ESPECÍFICO";
        if ($value['tb_credito_tip2'] == 3) $tip2 = " REPROGRAMADO";
        if ($value['tb_credito_tip2'] == 4) $tip2 = " CUOTA BALON";
        if ($value['tb_credito_tip2'] == 5) $tip2 = " REFINANCIADO AMORTIZADO";
        if ($value['tb_credito_tip2'] == 6) $tip2 = " REFINANCIADO";
  
        if ($value['tb_cliente_tip'] == 1) {
          $cliente = $value['tb_cliente_nom'] . ' | ' . $value['tb_cliente_doc'];
        }
        if ($value['tb_cliente_tip'] == 2) {
          $cliente = $value['tb_cliente_emprs'] . ' | ' . $value['tb_cliente_empruc'];
        }
  
        if ($value['tb_moneda_id'] == 1) $moneda = "S/.";
        if ($value['tb_moneda_id'] == 2) $moneda = "US$";
  
        if ($value['tb_cuotatipo_id'] == 3) {
          $cuotas = $value['tb_credito_numcuomax'];
        } else {
          $cuotas = $value['tb_credito_numcuo'];
        }
        $aprobar =  '';
        $desembolsar =  '';
        if ($value['tb_credito_est'] == 1 && $_SESSION['usuariogrupo_id'] == 2) {
          //SE AGREGO UN NUEVO VALOR PARAMETRO DENTRO DE LA LLAMADA A LA FUNCION APROBAR
          $aprobar =  '<a class="btn btn-default" href="javascript:void(0)" onClick="credito_aprobar(' . $value['tb_credito_id'] . ',' . $value['tb_credito_cus'] . ')">Aprobar</a>';
        }
        if ($value['tb_credito_est'] == 2) {
          if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 3) {
            $desembolsar =  '<a class="btn btn-default" href="javascript:void(0)" onClick="credito_desembolso_form(\'insertar\',' . $value['tb_credito_id'] . ')">Desembolsar</a>';
          }
        }
        $titulo = '';
        $documentos =  '';
        $placa =  '';
        $verificacion =  '';
        $eliminar = '';
        $comentar = '';
        $multiple = '';
        $refinanciar = '';
  
        if ($value['tb_credito_est'] == 9 && ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6)) {
          $titulo =  '<a class="btn btn-default" href="javascript:void(0)" onClick="titulo_form(' . $value['tb_credito_id'] . ')">Titulo</a>';
        }
        if ($value['tb_credito_est'] == 10 && ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6)) {
          $documentos =  '<a class="btn btn-default" href="javascript:void(0)" onClick="credito_documentos(' . $value['tb_credito_id'] . ')">Documentos</a>';
        }
        if (trim($value['tb_credito_vehpla']) == '') {
          $placa =  '<a class="btn btn-default" href="javascript:void(0)" onClick="placa_form(' . $value['tb_credito_id'] . ')">Placa</a>';
        }
        if (trim($value['tb_credito_vehpla']) != '' && $value['tb_credito_vertit'] == 0 && $value['tb_credito_est'] == 3 && ($value['tb_credito_tip'] == 1 || $value['tb_credito_tip'] == 4)) {
          $verificacion =  '<a class="btn btn-default" href="javascript:void(0)" onClick="verificacion_titulos_form(' . $value['tb_credito_id'] . ')">Verificación</a>';
        }
  
        if ($_SESSION['usuariogrupo_id'] == 2) {
          $eliminar = '<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="eliminar_credito(' . $value['tb_credito_id'] . ',' . $value['tb_credito_est'] . ')" title="ELIMINAR"><i class="fa fa-trash"></i></a>';
        }
  
        if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6) {
          $comentar = '<a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="cliente_nota(' . $value['tb_credito_id'] . ')"><i class="fa fa-commenting" title="COMENTAR"></i></a>';
        }
  
        if($fecha_hoy >= '29-11-2023' && ($value['tb_credito_tip2'] == 5 || $value['tb_credito_tip2'] == 6) && $value['tb_credito_doc_ref'] == '')
          $refinanciar = '<span class="badge bg-red">Subir Doc Refinanciado</span>';
  
        switch ($value['tb_credito_est']) {
          case 1:
            $estado = 'PENDIENTE';
            break;
          case 2:
            $estado = 'APROBADO';
            break;
          case 3:
            $estado = 'VIGENTE';
            break;
          case 4:
            $estado = 'PARALIZADO';
            break;
          case 5:
            $estado = 'REFINANCIADO AMORTIZADO';
            break;
          case 6:
            $estado = 'REFINANCIADO SIN AMORTIZADO';
            break;
          case 7:
            $estado = 'LIQUIDADO';
            break;
          case 8:
            $estado = 'RESUELTO';
            break;
          case 9:
            $estado = 'N° TÍTULO';
            break;
          case 10:
            $estado = 'DOCUMENTOS';
            break;
          default:
            $estado = 'SIN ESTADO, REPÓRTELO';
            break;
        }
  
        //ASIGNA LOS VALORES POR FILA EN LA COLUMNA CORRESPONDIENTE 
        $tr .= '<tr class="tabla_filaTabla">';
        $tr .= '
              <td  id="tabla_fila" style="border-left: 1px solid #135896;">' . $value['tb_credito_id'] . '</td>
              <td  id="tabla_fila">' . mostrar_fecha($value['tb_credito_reg']) . '</td>
              <td  id="tabla_fila">' . $value['tb_credito_subgar'] . '</td>
              <td  id="tabla_fila">' . $tip . ' ' . $tip2 . ' ' . $cuo_tipo . '</td>
              <td  id="tabla_fila">' . $value['tb_credito_vehpla'] . '</td>
              <td  id="tabla_fila">' . $value['tb_vehiculomarca_nom'] . ' / ' . $value['tb_vehiculomodelo_nom'] . '</td>
              <td  id="tabla_fila">' . $cliente . '</td>
              <td  id="tabla_fila">' . $moneda . '</td>
              <td  id="tabla_fila">' . mostrar_moneda($value['tb_credito_preaco']) . '</td>
              <td  id="tabla_fila">' . $value['tb_credito_int'] . '</td>
              <td  id="tabla_fila">' . $cuotas . '</td>
              <td  id="tabla_fila">' . $value['tb_usuario_nom'] . ' ' . $value['tb_usuario_ape'] . '</td>
              <td  id="tabla_fila">' . $estado . '</td>
            ';
        $tr .= '</tr>';
      }
    }
  $result = null;
  ?>
  <!-- CREA LA CABECERA DE LA TABLA CON LAS COLUMNAS QUE SE MOSTRARÁt -->
  <table id="tbl_creditogarveh" class="table table-hover" style="width: 100%;">
    <thead>
      <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
        <th>ID</th>
        <th>FECHA</th>
        <th>TIPO</th>
        <th>SUB TIPO</th>
        <th>PLACA</th>
        <th>MARCA / MODELO</th>
        <th>CLIENTE</th>
        <th>MON</th>
        <th>MONTO</th>
        <th>INT(%)</th>
        <th>CUOTAS</th>
        <th>ASESOR</th>
        <th>ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $tr; ?>
    </tbody>
  </table>