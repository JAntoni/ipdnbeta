<?php
  //require_once('../../core/usuario_sesion.php');
  date_default_timezone_set("America/Lima");
  require_once('../cuota/Cuota.class.php');
  require_once('../creditogarveh/Creditogarveh.class.php');
  require_once('../ingreso/Ingreso.class.php');
  require_once('../ventagarantia/Ventagarantia.class.php');
  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../reportecreditomejorado/AsignarOpcionGc.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');
  require_once('../monedacambio/Monedacambio.class.php');

  $oCreditogarveh = new Creditogarveh();
  $oReporteCredito = new ReporteCredito();
  $oIngreso = new Ingreso();
  $oAsignar = new AsignarOpcionGc();
  $oCambio = new Monedacambio();

  $fecha_hoy = date('Y-m-d');

  $fecha_ultimo = new DateTime();
  $fecha_ultimo->modify('last day of this month');

  $cuota_fec1 = '2000-10-20';
  $cuota_fec2 = $fecha_ultimo->format('Y-m-d');

  $cuota_fec1 = (isset($_POST['cuota_fec1']))? fecha_mysql($_POST['cuota_fec1']) : fecha_mysql($cuota_fec1);
  $cuota_fec2 = (isset($_POST['cuota_fec2']))? fecha_mysql($_POST['cuota_fec2']) : fecha_mysql($cuota_fec2);
  $tipo_cambio = (isset($_POST['tipo_cambio']))? floatval($_POST['tipo_cambio']) : $moneda_cambio; //tipo cambio del día
  $vista_modulo = (isset($_POST['vista_modulo']))? $_POST['vista_modulo'] : 'reportecredito';

  $data = '';
  $return_mejorado['estado'] = 0;
  $return_mejorado['mensaje'] = 'Existe un error al parecer en la carga de datos';

  $SUMA_TOTAL_DESEMBOLSADO = 0;
  $SUMA_TOTAL_CAPITAL_RECUPERAR = 0;
  $SUMA_TOTAL_FACTURADO = 0;
  $SUMA_TOTAL_COBRADO = 0;
  $SUMA_TOTAL_POR_COBRAR = 0;
  $DEUDA_ACUMULADA = 0;

  if($vista_modulo == 'reportecredito_mejorado'){
    //* LIMPIAMOS LA TABLA TEMP COBRANZA PARA PODER REGISTRAR LOS NUEVOS, ESTO EJECUTA: TRUNCATE TABLE TEMP_CORBRANZA
    $oReporteCredito->limpiar_tabla_temp_cobranza();
  }
  $validar = '';
  // 3 para vigentes, 4 paralizados
  $result = $oReporteCredito->credito_garveh_comportamiento_cliente($cuota_fec1, $cuota_fec2, '3');
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $cuo_tipo = ' FIJA';
        if($value['tb_cuotatipo_id'] == 3){ $cuo_tipo = ' LIBRE';}

        $credito_general = 'SIN GENERAL'; $credito_simbolo = 'REVISAR';
        if(intval($value['credito_general']) == 1) $credito_general = 'Crédito Menor';
        if(intval($value['credito_general']) == 2){ $credito_general = 'Crédito ASIVEH'; $credito_simbolo = 'ASIVEH';}
        if(intval($value['credito_general']) == 3){ $credito_general = 'Crédito GARVEH'; $credito_simbolo = 'GARVEH';}
        if(intval($value['credito_general']) == 4) $credito_general = 'Crédito HIPOTECARIO';

        $credito_subtipo1 = 'SIN TIPO CREDITO';
        if($value['credito_tipo'] == 1) $credito_subtipo1 = $credito_simbolo;
        if($value['credito_tipo'] == 2) $credito_subtipo1 = $credito_simbolo." ADENDA";
        if($value['credito_tipo'] == 3) $credito_subtipo1 = $credito_simbolo." ACUERDO PAGO";
        if($value['credito_tipo'] == 4) $credito_subtipo1 = "GARVEH MOBILIARIA";

        if(intval($value['credito_general']) == 4){ // PARA CREDITO HIPOTECARIO CAMBIA SUS SUBTIPOS
          if($value['credito_tipo'] == 1) $credito_subtipo1 = "GARANTIA INMUEBLE C/V";
          if($value['credito_tipo'] == 2) $credito_subtipo1 = "HIPO ADENDA";
          if($value['credito_tipo'] == 3) $credito_subtipo1 = "HIPO ACUERDO PAGO";
          if($value['credito_tipo'] == 4) $credito_subtipo1 = "HIPOTECARIA";
        }

        $credito_subtipo2 = 'SIN SUBTIPO';
        if($value['credito_subtipo'] == 1) $credito_subtipo2 = " CREDITO REGULAR";
        if($value['credito_subtipo'] == 2) $credito_subtipo2 = " CREDITO ESPECÍFICO";
        if($value['credito_subtipo'] == 3) $credito_subtipo2 = " REPROGRAMADO";
        if($value['credito_subtipo'] == 4) $credito_subtipo2 = " CUOTA BALON";
        if($value['credito_subtipo'] == 5) $credito_subtipo2 = " REFINANCIADO AMORTIZADO";
        if($value['credito_subtipo'] == 6) $credito_subtipo2 = " REFINANCIADO";

        $estado_cuota = 'SIN ESTADO CUOTA';
        if(intval($value['tb_cuota_est']) == 1) $estado_cuota = 'Pendiente';
        if(intval($value['tb_cuota_est']) == 2) $estado_cuota = 'Pagada';
        if(intval($value['tb_cuota_est']) == 3) $estado_cuota = 'Pago Parcial';

        $moneda_simbolo = 'S/.';
        $capital_desembolsado = $value['tb_credito_preaco'];
        $capital_restante = $value['tb_cuota_cap']; //si la cuota está impaga o pago parcial debe el capital hasta esa fecha
        $capital_restante_soles = $capital_restante;
        $capital_desembolsado_soles = $capital_desembolsado;

        $monto_cuota = $value['tb_cuota_cuo'];
        $monto_cuota_soles = $monto_cuota;

        $estado_credito = 'Vigente';
        $class_td = 'class="success"';

        $dias_atraso = 0;
        $inactivo_15 = 0;
        $ultima_fecha_pago = '';
        $monto_ultimo_pago = 0;
        $porcentage_ultimo_pago = 0;

        $cliente_documento = $value['tb_cliente_doc'];
        $cliente_nombres = $value['tb_cliente_nom'];
        $representante_dni = '';
        $representante_nombres = '';

        if(!empty($value['tb_cliente_emprs'])){
          $cliente_documento = $value['tb_cliente_empruc'];
          $cliente_nombres = $value['tb_cliente_emprs'];
          $representante_dni = $value['tb_cliente_doc'];
          $representante_nombres = $value['tb_cliente_nom'];
        }

        $result2 = $oIngreso->ultima_fecha_que_pago_cliente($value['tb_credito_id'], $value['credito_general'] );
          if($result2['estado'] == 1){
            $ultima_fecha_pago = $result2['data']['ultima_fecha'];
            $monto_ultimo_pago = $result2['data']['tb_cuotapago_mon'];
            $porcentage_ultimo_pago = $monto_ultimo_pago * 100 / $monto_cuota;
            $porcentage_ultimo_pago = formato_numero($porcentage_ultimo_pago);
          }
          else{
            $ultima_fecha_pago = $value['tb_credito_fecfac']; //el crédito no tiene pago alguno
          }
        $result2 = NULL;
        //? --------------------------- VALIDACION ----------------------------------
        if($value['tb_credito_id'] == 824)
          $validar .= 'Menciona que ultimo dia de pago es: '.$ultima_fecha_pago.' | ';
        //? --------------------------- FIN VALIDACION ----------------------------------

        if($value['tb_moneda_id'] == 2){
          $moneda_simbolo = 'US$';
          $capital_restante_soles = formato_numero($capital_restante * $tipo_cambio);
          $capital_desembolsado_soles = formato_numero($capital_desembolsado * $tipo_cambio);
          $monto_cuota_soles = formato_numero($monto_cuota * $tipo_cambio);
        }

        $pago_parcial = 0;

        $tipo_cuota_pago = 1; // 1 es para pago de cuota
        $cuota_id = 0; //intval($value['tb_cuota_id']);

        //? --------------------------- VALIDACION ----------------------------------
        if($value['tb_credito_id'] == 824)
          $validar .= 'Tiene una cuota en estado parcial: cuota id: '.$cuota_id.' | ';
        //? --------------------------- FIN VALIDACION ----------------------------------

        if(intval($value['tb_cuotadetalle_id']) > 0){
          $tipo_cuota_pago = 2; //2 para pago de cuota detalle
          $cuota_id = intval($value['tb_cuotadetalle_id']);
          //? --------------------------- VALIDACION ----------------------------------
          if($value['tb_credito_id'] == 824)
            $validar .= 'Este credito tiene una cuotadetalle_id:  '.$cuota_id.' | ';
          //? --------------------------- FIN VALIDACION ----------------------------------
        }

        if(intval($value['tb_cuota_est']) == 3){ //todas las cuotas en pago parcial
          
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
            if($result2['estado'] == 1){
              
              $pago_parcial = $result2['data']['importe_total'];
              if($value['tb_moneda_id'] == 2)
                $pago_parcial = formato_numero($pago_parcial * $tipo_cambio);
              
              $SUMA_TOTAL_COBRADO += $pago_parcial;
              //? --------------------------- VALIDACION ----------------------------------
              if($value['tb_credito_id'] == 824)
                $validar .= 'El importe total del pago parcial es:  '.$pago_parcial.' | ';
              //? --------------------------- FIN VALIDACION ----------------------------------
            }
          $result2 = NULL;
        }
        
        if(intval($value['tb_cuota_est']) == 2){
          $capital_restante = $value['tb_cuota_cap'] - $value['tb_cuota_amo']; //si la cuota está pagada le descontamos la amortizacion
          $pago_parcial = $monto_cuota_soles;
          $SUMA_TOTAL_COBRADO += $monto_cuota_soles;
        }
        if(intval($value['tb_cuota_est']) == 1){
          $SUMA_TOTAL_POR_COBRAR += $monto_cuota_soles;
        }
        if($value['tb_cuota_est'] != 2){
          if(strtotime($fecha_hoy) > strtotime($value['tb_cuota_fec'])){ //todo codigo aquí dentro es para cuiando la cuota está vencida
            $estado_credito = 'Vencido';
            $class_td = 'class="danger"';
            $dias_atraso = restaFechas($value['tb_cuota_fec'], $fecha_hoy); //* fecha inicio, fecha fin
            $inactivo_15 = restaFechas($ultima_fecha_pago, $fecha_hoy); //* fecha inicio, fecha fin
          } 
        }

        // VAMOS A OBTENER EL TOTAL DE DEUDA HASTA LA FECHA DE LAS CUOTAS VENCIDAS POR CREDITO
        $total_deuda_acumulada = 0;
        $pagos_parciales_adicionales = 0;
        $result2 = $oReporteCredito->total_cuotas_impagas_por_credito($fecha_hoy, intval($value['credito_general']), $value['tb_credito_id']);
          if($result2['estado'] == 1){
            foreach($result2['data'] as $key => $cuota){
              $monto_impago = $cuota['cuota_real'];
              if($value['tb_moneda_id'] == 2)
                $monto_impago = formato_numero($monto_impago * $tipo_cambio);
              $total_deuda_acumulada += $monto_impago;
              
              if(intval($value['tb_cuotadetalle_id']) != $cuota['tb_cuotadetalle_id'] && $cuota['tb_cuotadetalle_est'] == 3){
                $result2 = $oIngreso->mostrar_importe_total_cuota($cuota['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']);
                  if($result2['estado'] == 1){
                    
                    $pago_otras_cuotas = $result2['data']['importe_total'];
                    if($value['tb_moneda_id'] == 2)
                      $pago_otras_cuotas = formato_numero($pago_otras_cuotas * $tipo_cambio);
                    
                    $SUMA_TOTAL_COBRADO += $pago_otras_cuotas;
                    $pagos_parciales_adicionales += $pago_otras_cuotas;
                  }
                $result2 = NULL;
              }

              //? --------------------------- VALIDACION ----------------------------------
              if($value['tb_credito_id'] == 824)
                $validar .= 'Cuotas detalles impagas:  '.$monto_impago.' | '.$cuota['tb_cuotadetalle_fec'].' | ';
              //? --------------------------- FIN VALIDACION ----------------------------------
            }
          }
        $result2 = NULL;
        if(intval($value['tb_cuota_est']) == 3 || $pagos_parciales_adicionales > 0){ // solo si hay pago parcial restamos a la deuda acumulada
          $total_deuda_acumulada = $total_deuda_acumulada - $pago_parcial - $pagos_parciales_adicionales;
          //? --------------------------- VALIDACION ----------------------------------
          if($value['tb_credito_id'] == 824)
            $validar .= 'Aqui se resta los pagos parciales de la deuda acumulada, deuda acumulada: '.$total_deuda_acumulada.' - pago parcial: '.$pago_parcial.' | ';
          //? --------------------------- FIN VALIDACION ----------------------------------
        }
        
        $DEUDA_ACUMULADA += $total_deuda_acumulada;
        // if($total_deuda_acumulada <= 0)
        //   $total_deuda_acumulada = 0;
        //restamos los pagos parciales al total deuda acumulada, para mostrar el monto real que falta pagar para que el cliente se ponga al día
        if($vista_modulo == 'reportecredito'){
          $SUMA_TOTAL_DESEMBOLSADO += $capital_desembolsado_soles;
          $SUMA_TOTAL_CAPITAL_RECUPERAR += $capital_restante_soles ;
          $SUMA_TOTAL_FACTURADO += $monto_cuota_soles;
          $SUMA_CAPITAL_RECUPERADO = $SUMA_TOTAL_DESEMBOLSADO - $SUMA_TOTAL_CAPITAL_RECUPERAR;
          $data .= '
            <tr>
              <td>'.$value['tb_credito_id'].'</td>
              <td><b>'.$credito_general.'</b></td>
              <td>'.$cliente_documento.'</td>
              <td>'.$cliente_nombres.'</td>
              <td>'.$representante_dni.'</td>
              <td>'.$representante_nombres.'</td>
              <td>'.$value['tb_cliente_dir'].'</td>
              <td>'.$value['tb_cliente_cel'].'</td>
              <td>'.$credito_subtipo1.'</td>
              <td>'.$credito_subtipo2.' '.$cuo_tipo.'</td>
              <td>'.mostrar_fecha($value['tb_credito_fecfac']).'</td>
              <td>'.$value['tb_cuota_num'].' / '.$value['num_cuotas_totales'].'</td>
              <td>'.$moneda_simbolo.'</td>
              <td>'.$value['tb_credito_preaco'].'</td>
              <td>'.$capital_restante.'</td>
              <td class="info">'.$capital_restante_soles.'</td>
              <td><b>'.$estado_cuota.'</b></td>
              <td>'.$monto_cuota.'</td>
              <td class="info">'.$monto_cuota_soles.'</td>
              <td '.$class_td.'>'.$pago_parcial.'</td>
              <td>'.mostrar_fecha($value['tb_cuota_fec']).'</td>
              <td>'.mostrar_fecha($ultima_fecha_pago).'</td>
              <td>'.$dias_atraso.'</td>
              <td>'.$inactivo_15.'</td>
              <td>'.$porcentage_ultimo_pago.'</td>
              <td>'.$total_deuda_acumulada.'</td>
              <td '.$class_td.'>'.$estado_credito.'</td>
            </tr>
          ';
        }
        
        if($vista_modulo == 'reportecredito_mejorado'){
          //? INSERTAMOS CADA FILA DE LA CONSULTA A LA TABLA TEMPORAL QUE GUARDA LA COBRANZA
          $oReporteCredito->cuotadetalle_id = $value['tb_cuotadetalle_id']; 
          $oReporteCredito->credito_id = $value['tb_credito_id']; 
          $oReporteCredito->credito_tipo = $credito_general;
          $oReporteCredito->cliente_id = $value['tb_cliente_id']; 
          $oReporteCredito->cliente_dni = $cliente_documento; 
          $oReporteCredito->cliente_nom = $cliente_nombres; 
          $oReporteCredito->representante_dni = $representante_dni; 
          $oReporteCredito->representante_nom = $representante_nombres; 
          $oReporteCredito->cliente_dir = $value['tb_cliente_dir']; 
          $oReporteCredito->cliente_cel = $value['tb_cliente_cel']; 
          $oReporteCredito->credito_subtipo = $credito_subtipo1; 
          $oReporteCredito->fecha_facturacion = $value['tb_credito_fecfac']; 
          $oReporteCredito->cuota_actual = $value['tb_cuota_num'].' / '.$value['num_cuotas_totales']; 
          $oReporteCredito->moneda = $moneda_simbolo; 
          $oReporteCredito->capital_desembolsado = $value['tb_credito_preaco']; 
          $oReporteCredito->capital_restante = $capital_restante; 
          $oReporteCredito->restante_soles = $capital_restante_soles; 
          $oReporteCredito->cuota_estado = $estado_cuota; 
          $oReporteCredito->cuota_monto = $monto_cuota;
          $oReporteCredito->cuota_soles = $monto_cuota_soles; 
          $oReporteCredito->cuota_pago = $pago_parcial; 
          $oReporteCredito->cuota_fecha = $value['tb_cuota_fec']; 
          $oReporteCredito->ultimopago_fecha = $ultima_fecha_pago;
          $oReporteCredito->dias_atraso = $dias_atraso; 
          $oReporteCredito->inactividad = $inactivo_15; 
          $oReporteCredito->ultimo_pago = $porcentage_ultimo_pago; 
          $oReporteCredito->deuda_acumulada = $total_deuda_acumulada; 
          $oReporteCredito->estado_credito = $estado_credito;
          $oReporteCredito->usuario_id = intval($value['tb_credito_usureg']);

          $oReporteCredito->guardar_lista_cobranza_temporal();
        }
      }
    }
  $result = NULL;
  
  $return['deuda_acumulada'] = $alerta_deudadiaria.' | '.$DEUDA_ACUMULADA.' | '.$cuota_fec2.' | cam: '.$moneda_cambio;

  if($vista_modulo == 'reportecredito'){
    $inputs['suma_total_desembolsado'] = mostrar_moneda($SUMA_TOTAL_DESEMBOLSADO);
    $inputs['suma_total_capital_recuperar'] = mostrar_moneda($SUMA_TOTAL_CAPITAL_RECUPERAR);
    $inputs['suma_capital_recuperado'] = mostrar_moneda($SUMA_CAPITAL_RECUPERADO);

    $inputs['suma_total_facturado'] = mostrar_moneda($SUMA_TOTAL_FACTURADO);
    $inputs['suma_total_cobrado'] = mostrar_moneda($SUMA_TOTAL_COBRADO);
    $inputs['suma_total_por_cobrar'] = mostrar_moneda($SUMA_TOTAL_POR_COBRAR);
    $inputs['validar'] = $validar;

    $return['tabla'] = $data;
    $return['inputs'] = $inputs;
    echo json_encode($return);
  }
  else if($vista_modulo == 'reportecredito_mejorado'){
    $return_mejorado['estado'] = 1;
    $return_mejorado['mensaje'] = 'exito';
    echo json_encode($return_mejorado);
  }
  else{
    echo json_encode($return);
  }
?>

