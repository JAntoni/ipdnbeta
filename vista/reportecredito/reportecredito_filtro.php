<?php 
  $fecha = new DateTime();
  $fecha->modify('last day of this month');

  $cuota_fec1 = date('01-m-Y');
  $cuota_fec2 = $fecha->format('d-m-Y');
?>
<form id="form_creditomenor_filtro" class="form-inline" role="form">
  <div class="form-group">
    <div class='input-group date' id='datetimepicker1'>
      <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $cuota_fec1;?>"/>
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
  </div>
  <div class="form-group">
    <div class='input-group date' id='datetimepicker2'>
      <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $cuota_fec2;?>"/>
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
  </div>
  <div class="form-group">
    <select id="cmb_filtro_est" name="cmb_filtro_est" class="form-control input-sm">
      <option>Selecciona estado...</option>
      <option value="1">Pendiente</option>
      <option value="2">Aprobado</option>
      <option value="3">Vigente</option>
      <option value="4">Paralizado</option>
      <option value="7">Liquidado</option>
      <option value="8">Resuelto</option>
      <option value="9">Título Pendiente</option>
      <option value="10">Completar Documentos</option>
    </select>
  </div>
  <button type="button" class="btn btn-info btn-sm" onclick="reportecredito_lista_garveh()"><i class="fa fa-search"></i></button>
</form>