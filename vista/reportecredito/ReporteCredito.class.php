<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class ReporteCredito extends Conexion{
    public $cuotadetalle_id;
    public $credito_id; 
    public $credito_tipo;
    public $cliente_id; 
    public $cliente_dni; 
    public $cliente_nom; 
    public $representante_dni; 
    public $representante_nom; 
    public $cliente_dir; 
    public $cliente_cel; 
    public $credito_subtipo; 
    public $fecha_facturacion; 
    public $cuota_actual; 
    public $moneda; 
    public $capital_desembolsado; 
    public $capital_restante; 
    public $restante_soles; 
    public $cuota_estado; 
    public $cuota_monto;
    public $cuota_soles; 
    public $cuota_pago; 
    public $cuota_fecha; 
    public $ultimopago_fecha;
    public $dias_atraso; 
    public $inactividad; 
    public $ultimo_pago; 
    public $deuda_acumulada; 
    public $estado_credito;
    public $usuario_id;

    function credito_garveh_comportamiento_cliente($fecha_filtro1, $fecha_filtro2, $credito_est){
      try {
        /*
          tb_cliente_empruc, -- RUC DEL CLIENTE
          tb_cliente_emprs, -- RAZON SOCIAL DEL CLIENTE
          cre.tb_credito_tip as credito_tipo, -- para credito ASIVEH esto es tb_credito_tip1 (no aplica c-menor)
          cre.tb_credito_tip2 as credito_subtipo, -- esto es igual para todos los créditos (no - aplica c-menor)
          cre.tb_cuotatipo_id, -- se refiere al tipo de cuota en CREDITOS ES 4 FIJAS, 3 LIBRES
          tb_credito_fecfac, -- fecha en que inicial el crédito
          tb_cuota_num, -- numero de cuota a partir de las cuotas acordadas en el crédito
        */
        $sql = "
          SELECT 
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_credito_id,
            cre.tb_credito_tip AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo,
            tb_credito_usureg, 
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuota_cuo
              ELSE tb_cuota_cuo
            END) AS cuota_real
            
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN tb_creditogarveh cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 3)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND (tb_cuotadetalle_xac = 1 OR tb_cuotadetalle_xac IS NULL) 
            AND tb_cuota_fec <= :fecha_filtro2 
            AND tb_credito_est IN (:credito_est)
            AND (LEFT(tb_cuota_fec, 7) = LEFT(:fecha_filtro2, 7) OR tb_cuota_est in (1,3) AND tb_cuotadetalle_est != 2) 
            GROUP BY cuo.tb_credito_id
          
          UNION
          -- unimos CREDITOASIVEH
          SELECT 
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_credito_id,
            cre.tb_credito_tip1 AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo, 
            tb_credito_usureg,
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuota_cuo
              ELSE tb_cuota_cuo
            END) AS cuota_real
            
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN tb_creditoasiveh cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 2)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND (tb_cuotadetalle_xac = 1 OR tb_cuotadetalle_xac IS NULL) 
            AND tb_cuota_fec <= :fecha_filtro2 
            AND tb_credito_est IN (:credito_est)
            AND (LEFT(tb_cuota_fec, 7) = LEFT(:fecha_filtro2, 7) OR tb_cuota_est in (1,3) AND tb_cuotadetalle_est != 2) 
            GROUP BY cuo.tb_credito_id
          
          UNION

          SELECT 
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_empruc,
            tb_cliente_emprs,
            tb_cliente_cel,
            tb_cliente_dir,
            cuo.tb_creditotipo_id AS credito_general,
            cre.tb_credito_id,
            cre.tb_credito_tip AS credito_tipo,
            cre.tb_credito_tip2 AS credito_subtipo,
            cre.tb_cuotatipo_id,
            tb_credito_fecfac,
            tb_cuota_num,
            (CASE WHEN tb_credito_numcuo >= tb_credito_numcuomax 
              THEN tb_credito_numcuo ELSE tb_credito_numcuomax END
            ) AS num_cuotas_totales,
            cuo.tb_moneda_id,
            tb_credito_preaco,
            tb_cuota_cuo,
            tb_cuota_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuota_est, 
            tb_cuotadetalle_id,
            tb_cuotadetalle_fec, 
            tb_cuotadetalle_cuo, 
            tb_credito_usureg,
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuota_cuo
              ELSE tb_cuota_cuo
            END) AS cuota_real
            
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN tb_creditohipo cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 4)
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND (tb_cuotadetalle_xac = 1 OR tb_cuotadetalle_xac IS NULL) 
            AND tb_cuota_fec <= :fecha_filtro2 
            AND tb_credito_est IN (:credito_est)
            AND (LEFT(tb_cuota_fec, 7) = LEFT(:fecha_filtro2, 7) OR tb_cuota_est in (1,3) AND tb_cuotadetalle_est != 2) 
            GROUP BY cuo.tb_credito_id
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_est", $credito_est, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function temp_cobranza_lista($opcionesgc_id,$pdp_dias,$resultado_nom,$fecha_hoy,$encargado_id,$ejecucion_confirmada,$filter_num_cuotas,$filter_dias_atraso){
      try {
        $sql = "SELECT cob.*, det.tb_cuotadetalle_est, opc.*, asig.resultado_nom, asig.comentario_des, asig.opciongc_fechapdp, asig.tb_asignaropciongc_usuapr, tb_usuario_nom, usu.tb_usuario_fot, asig.asignaropciongc_fecges
        FROM temp_cobranza cob 
        INNER JOIN tb_cuotadetalle det ON det.tb_cuotadetalle_id = cob.cuotadetalle_id 
        LEFT JOIN tb_usuario usu ON usu.tb_usuario_id = cob.usuario_id 
        LEFT JOIN tb_asignaropciongc asig ON asig.cuotadetalle_id = cob.cuotadetalle_id 
        LEFT JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id 
        WHERE 1=1 ";

        $adddesc_cuotas = 'Antigüedad de cuota: '; $adddesc_diasvenc = 'Vencidas acumuladas: '; $fecha_suma = date("Y-m-d", strtotime($fecha_hoy));

        if( $filter_num_cuotas == 0 )
        {
          $adddesc_cuotas .= 'todos'; $sql .= " ";
        }
        elseif( $filter_num_cuotas == 1 )
        {
          $adddesc_cuotas .= '0 - 3';      $sql .= " AND (SUBSTRING(cuota_actual, 1, 3) = '0 /' OR SUBSTRING(cuota_actual, 1, 3) = '1 /' OR SUBSTRING(cuota_actual, 1, 3) = '2 /' OR SUBSTRING(cuota_actual, 1, 3) = '3 /') ";
        }
        elseif( $filter_num_cuotas == 2 )
        {
          $adddesc_cuotas .= '4 - 9';      $sql .= " AND (SUBSTRING(cuota_actual, 1, 3) BETWEEN '4 /' AND '9 /') ";
        }
        elseif( $filter_num_cuotas == 3 )
        {
          $adddesc_cuotas .= '10 - 18';    $sql .= " AND (SUBSTRING(cuota_actual, 1, 4) BETWEEN '10 / ' AND '18 /') ";
        }
        else // if( $filter_num_cuotas == 4 )
        {
          $adddesc_cuotas .= 'mayor a 19'; $sql .= " AND (SUBSTRING(cuota_actual, 1, 2) >= 19) ";
        }
    
        if( $filter_dias_atraso == 0 )
        {
          $adddesc_diasvenc .= 'Todos';                   $sql .= " ";
        }
        elseif( $filter_dias_atraso == 1 )
        {
          $adddesc_diasvenc .= 'Vigentes de 0 a 1 mes';   $sql .= " AND (dias_atraso BETWEEN 0 AND 30) ";
        }
        elseif( $filter_dias_atraso == 2 )
        {
          $adddesc_diasvenc .= 'Vigentes de 1 a 2 meses'; $sql .= " AND (dias_atraso BETWEEN 31 AND 60) ";
        }
        elseif( $filter_dias_atraso == 3 )
        {
          $adddesc_diasvenc .= 'Vigentes de 2 a 3 meses'; $sql .= " AND (dias_atraso BETWEEN 61 AND 90) ";
        }
        else
        {
          $adddesc_diasvenc .= 'Vigentes de 3 a más';     $sql .= " AND (dias_atraso >= 91) ";
        }

        if(intval($opcionesgc_id) > 0)
          $sql .= " AND asig.tb_opcionesgc_id = ".intval($opcionesgc_id);
        if($pdp_dias != ''){
          $fecha_suma = date("Y-m-d", strtotime($fecha_hoy."+ ".intval($pdp_dias)." days"));
          if(intval($pdp_dias) == 0){
            $sql .= " AND asig.opciongc_fechapdp = '".$fecha_suma."'";
          }
          else{
            $sql .= " AND asig.opciongc_fechapdp <= '".$fecha_suma."'";
          }
        }
        if(trim($resultado_nom) != '')
          $sql .= " AND asig.resultado_nom = '".$resultado_nom."'";
        if(intval($encargado_id) > 0)
          $sql .= " AND asig.encargado_id = ".intval($encargado_id);

        //daniel odar 14-10-24
        if (!empty($ejecucion_confirmada)) {
          $sql .= $ejecucion_confirmada == 1 ? " AND (asig.tb_asignaropciongc_usuapr IS NOT NULL AND TRIM(asig.tb_asignaropciongc_usuapr) <> '')" : " AND (asig.tb_asignaropciongc_usuapr IS NULL OR TRIM(asig.tb_asignaropciongc_usuapr) = '')";//(tb_gastopago_documentossunat IS NOT NULL AND TRIM(tb_gastopago_documentossunat) <> '')
        }
        
        $sentencia = $this->dblink->prepare($sql);
        //$sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $retorno["sql"] = $sql;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LO SELECCIONADO: opc: ".$opcionesgc_id.' | pdp dias: '.$pdp_dias.' | nom: '.$resultado_nom.' | fec: '.$fecha_hoy.' | '.$fecha_suma.' | '.$adddesc_cuotas.' | '.$adddesc_diasvenc;
          $retorno["data"] = "";
          $retorno["sql"] = $sql;
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_segmentacion_embudo($fecha_hoy,$filter_dias_atraso,$sinGestion){
      try {
        // $restar_fechas = date("Y-m-d", strtotime($fecha_hoy."- ".intval(5)." days"));
        $sql = "SELECT DATE_ADD(:fecha_hoy, INTERVAL -5 DAY) AS restar_cinco_dias"; // SELECT DATE_ADD(CURRENT_DATE, INTERVAL -5 DAY) AS restar_cinco_dias;  || // SELECT DATEDIFF (CURRENT_DATE, "2025-01-25") AS restar_fechas; = 5
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_hoy", $fecha_hoy, PDO::PARAM_STR);
        $sentencia->execute();
        if ($sentencia->rowCount() > 0)
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        
        $restar_fechas = $resultado["restar_cinco_dias"];

        $sql = "SELECT cob.*, det.tb_cuotadetalle_est, opc.*, asig.resultado_nom, asig.comentario_des, asig.opciongc_fechapdp, asig.tb_asignaropciongc_usuapr, tb_usuario_nom, usu.tb_usuario_fot, asig.asignaropciongc_fecges
          FROM temp_cobranza cob 
          INNER JOIN tb_cuotadetalle det ON det.tb_cuotadetalle_id = cob.cuotadetalle_id 
          LEFT JOIN tb_usuario usu ON usu.tb_usuario_id = cob.usuario_id 
          LEFT JOIN tb_asignaropciongc asig ON asig.cuotadetalle_id = cob.cuotadetalle_id 
          LEFT JOIN tb_opcionesgc opc ON opc.tb_opcionesgc_id = asig.tb_opcionesgc_id 
          WHERE 1=1 ";        
        if( $filter_dias_atraso == 1 )     { $sql .= " AND (dias_atraso BETWEEN 0 AND 30)"; }
        elseif( $filter_dias_atraso == 2 ) { $sql .= " AND (dias_atraso BETWEEN 31 AND 60)"; }
        elseif( $filter_dias_atraso == 3 ) { $sql .= " AND (dias_atraso BETWEEN 61 AND 90)"; }
        elseif( $filter_dias_atraso == 4 ) { $sql .= " AND (dias_atraso BETWEEN 91 AND 120)"; }
        elseif( $filter_dias_atraso == 5 ) { $sql .= " AND (dias_atraso BETWEEN 121 AND 150)"; } 
        elseif( $filter_dias_atraso == 6 ) { $sql .= " AND (dias_atraso BETWEEN 151 AND 180)"; }
        else { $sql .= " AND (dias_atraso >= 181) "; }
        if($sinGestion > 0) { $sql .= " AND (asig.asignaropciongc_fecges >= '".$restar_fechas."' OR (opc.tb_opcionesgc_nom = 'SIN RESPUESTA' OR asig.resultado_nom = '' OR asig.resultado_nom IS NULL))"; }
        $sql .= " ORDER BY temp_cobranza_id DESC";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $retorno["sql"] = $sql;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "Sin resultados.";
          $retorno["data"] = "";
          $retorno["sql"] = $sql;
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarUno_tb_reportecreditomejorado_embudo($mes,$anho){
      try {
        $sql = "SELECT * FROM tb_reportecreditomejorado_embudo WHERE mes = :mes AND anho = :anho";  // (EXTRACT(YEAR FROM ultimopago_fecha)) = 2024 ";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mes", $mes, PDO::PARAM_INT);
        $sentencia->bindParam(":anho", $anho, PDO::PARAM_INT);
        $sentencia->execute();
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay datos registrados";
          $retorno["data"] = "";
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function temp_cobranza_lista_por_cliente($cliente_id, $cuotadetalle_id){
      try {
        $detalle = '';
        if(intval($cuotadetalle_id) > 0)
          $detalle = ' AND cuotadetalle_id = '.intval($cuotadetalle_id);

        $sql = "SELECT * FROM temp_cobranza WHERE cliente_id =:cliente_id".$detalle;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = " NO CREDITOS REGISTRADOS PARA EL CLIENTE ID: ".$cliente_id.' | '.$sql;
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function temp_cobranza_por_ids($temp_cobranza_id){
      try {
        $sql = "SELECT * FROM temp_cobranza WHERE temp_cobranza_id IN($temp_cobranza_id)";

        $sentencia = $this->dblink->prepare($sql);
        //$sentencia->bindParam(":temp_cobranza_id", $temp_cobranza_id, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito / ".$temp_cobranza_id;
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function total_cuotas_impagas_por_credito($fecha_filtro, $credito_tipo, $credito_id){
      try {
        $tabla = 'tb_creditomenor';
        if(intval($credito_tipo) == 2) $tabla = 'tb_creditoasiveh';
        if(intval($credito_tipo) == 3) $tabla = 'tb_creditogarveh';
        if(intval($credito_tipo) == 4) $tabla = 'tb_creditohipo';

        $sql = "
          SELECT 
            cre.tb_credito_id,
            tb_cuota_num,
            cuo.tb_moneda_id,
            tb_cuotadetalle_id,
            tb_cuotadetalle_cuo,
            tb_cuotadetalle_fec,
            tb_cuota_cap, 
            tb_cuota_amo, 
            tb_cuota_int,
            tb_cuotadetalle_est, 
            (CASE
              WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int
              WHEN cre.tb_cuotatipo_id = 4 THEN tb_cuotadetalle_cuo
              ELSE tb_cuotadetalle_cuo
            END) AS cuota_real
          
          FROM tb_cuota cuo
          LEFT JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id	
          LEFT JOIN ".$tabla." cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = :credito_tipo)
          WHERE 
            tb_credito_xac = 1 AND tb_cuota_xac = 1 AND (tb_cuotadetalle_xac = 1 OR tb_cuotadetalle_xac IS NULL) 
            AND tb_cuotadetalle_est IN(1,3) AND tb_cuotadetalle_fec < :fecha_filtro AND cre.tb_credito_id = :credito_id
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro", $fecha_filtro, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_tipo", $credito_tipo, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function guardar_lista_cobranza_temporal(){
      $this->dblink->beginTransaction();
      try {
        $columns = [
          'cuotadetalle_id',
          'credito_id',
          'credito_tipo',
          'cliente_id',
          'cliente_dni',
          'cliente_nom',
          'representante_dni',
          'representante_nom',
          'cliente_dir',
          'cliente_cel',
          'credito_subtipo',
          'fecha_facturacion',
          'cuota_actual',
          'moneda',
          'capital_desembolsado',
          'capital_restante',
          'restante_soles',
          'cuota_estado',
          'cuota_monto',
          'cuota_soles',
          'cuota_pago',
          'cuota_fecha',
          'ultimopago_fecha',
          'dias_atraso',
          'inactividad',
          'ultimo_pago',
          'deuda_acumulada',
          'estado_credito',
          'usuario_id'
        ];
  
        // Lista de placeholders
        $placeholders = implode(',', array_map(function ($column) {
            return ':' . $column;
        }, $columns));

        $sql = "INSERT INTO temp_cobranza (" . implode(',', $columns) . ") VALUES ($placeholders)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(':cuotadetalle_id', $this->cuotadetalle_id, PDO::PARAM_INT);
        $sentencia->bindParam(':credito_id', $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(':credito_tipo', $this->credito_tipo, PDO::PARAM_STR);
        $sentencia->bindParam(':cliente_id', $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(':cliente_dni', $this->cliente_dni, PDO::PARAM_STR);
        $sentencia->bindParam(':cliente_nom', $this->cliente_nom, PDO::PARAM_STR);
        $sentencia->bindParam(':representante_dni', $this->representante_dni, PDO::PARAM_STR);
        $sentencia->bindParam(':representante_nom', $this->representante_nom, PDO::PARAM_STR);
        $sentencia->bindParam(':cliente_dir', $this->cliente_dir, PDO::PARAM_STR);
        $sentencia->bindParam(':cliente_cel', $this->cliente_cel, PDO::PARAM_STR);
        $sentencia->bindParam(':credito_subtipo', $this->credito_subtipo, PDO::PARAM_STR);
        $sentencia->bindParam(':fecha_facturacion', $this->fecha_facturacion, PDO::PARAM_STR);
        $sentencia->bindParam(':cuota_actual', $this->cuota_actual, PDO::PARAM_STR);
        $sentencia->bindParam(':moneda', $this->moneda, PDO::PARAM_STR);
        $sentencia->bindParam(':capital_desembolsado', $this->capital_desembolsado, PDO::PARAM_STR);
        $sentencia->bindParam(':capital_restante', $this->capital_restante, PDO::PARAM_STR);
        $sentencia->bindParam(':restante_soles', $this->restante_soles, PDO::PARAM_STR);
        $sentencia->bindParam(':cuota_estado', $this->cuota_estado, PDO::PARAM_STR);
        $sentencia->bindParam(':cuota_monto', $this->cuota_monto, PDO::PARAM_STR);
        $sentencia->bindParam(':cuota_soles', $this->cuota_soles, PDO::PARAM_STR);
        $sentencia->bindParam(':cuota_pago', $this->cuota_pago, PDO::PARAM_STR);
        $sentencia->bindParam(':cuota_fecha', $this->cuota_fecha, PDO::PARAM_STR);
        $sentencia->bindParam(':ultimopago_fecha', $this->ultimopago_fecha, PDO::PARAM_STR);
        $sentencia->bindParam(':dias_atraso', $this->dias_atraso, PDO::PARAM_INT);
        $sentencia->bindParam(':inactividad', $this->inactividad, PDO::PARAM_INT);
        $sentencia->bindParam(':ultimo_pago', $this->ultimo_pago, PDO::PARAM_STR);
        $sentencia->bindParam(':deuda_acumulada', $this->deuda_acumulada, PDO::PARAM_STR);
        $sentencia->bindParam(':estado_credito', $this->estado_credito, PDO::PARAM_STR);
        $sentencia->bindParam(':usuario_id', $this->usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function limpiar_tabla_temp_cobranza(){
      $this->dblink->beginTransaction();
      try {
        $sql = "TRUNCATE TABLE temp_cobranza;";
        $sentencia = $this->dblink->prepare($sql);
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUnoTempCobranza($temp_cobranza_id){
      try {
        $sql = "SELECT * FROM temp_cobranza WHERE temp_cobranza_id IN(:temp_cobranza_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":temp_cobranza_id", $temp_cobranza_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_areas(){
      try {
        $sql = "SELECT * FROM tb_area";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
