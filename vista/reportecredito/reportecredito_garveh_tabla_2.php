<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../cuota/Cuota.class.php');
  require_once('../creditogarveh/Creditogarveh.class.php');
  require_once('../ingreso/Ingreso.class.php');
  require_once('../ventagarantia/Ventagarantia.class.php');
  require_once('../reportecredito/ReporteCredito.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oCreditogarveh = new Creditogarveh();
  $oReporteCredito = new ReporteCredito();
  $oIngreso = new Ingreso();

  $cuota_fec1 = '2022-10-20';
  $cuota_fec2 = '2022-10-20';
  $data = '';
  $cli = ['Juan Antonio', 'Maria Argedas', 'Jose Felicianos', 'Ya no mas'];

  for($i = 1; $i < 10; $i++){
    $rand = rand(0,3);
    $data .= '
      <tr>
        <td>'.($i + 10).'</td>
        <td>GARVEH '.$i.'</td>
        <td>DNI '.$i.'</td>
        <td>'.$cli[$rand].'</td>
        <td>0</td>
        <td>'.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
        <td>Direccion '.$i.'</td>
      </tr>
    ';
  }

  $inputs['suma_total_desembolsado'] = "451,222.00";
  $inputs['suma_total_capital_recuperar'] = "51,222.00";
  $inputs['suma_capital_recuperado'] = "300,222.00";
  $inputs['suma_total_facturado'] = "45,222.00";
  $inputs['suma_total_cobrado'] = "1,222.00";
  $inputs['suma_total_por_cobrar'] = "222.00";

  $return['tabla'] = $data;
  $return['inputs'] = $inputs;

  echo json_encode($return);
?>


