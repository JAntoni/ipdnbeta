<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<?php require_once('reportecredito_filtro.php');?>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="reportecredito_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>
				
				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">TODOS LOS CREDITOS VEHICULARES REGISTRADOS</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						
							<div class="table-responsive lista_garveh">
								
							</div>
						
					</div>
				</div>

				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<!--div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">SUMA DE CAPITAL Y CUOTAS</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body row">
						<!- TOTAL DESEMBOLSADO--
						<div class="col-md-4">
							<div class="info-box bg-aqua">
								<span class="info-box-icon">
									<i class="fa fa-bar-chart"></i>
								</span>
								<div class="info-box-content">
									<span class="info-box-text">Total Desembolsado</span>
									<span class="info-box-number" id="span_desembolsado">S/. 560,141.00</span>
									<div class="progress">
										<div class="progress-bar" style="width: 100%;"></div>
									</div>
									<span class="progress-description">Total Desembolsado en Créditos</span>
								</div>
							</div>
						</div>
						<!- TOTAL CAPITAL RECUPERADO--
						<div class="col-md-4">
							<div class="info-box bg-green">
								<span class="info-box-icon">
									<i class="fa fa-bar-chart"></i>
								</span>
								<div class="info-box-content">
									<span class="info-box-text">Total Capital Recuperado</span>
									<span class="info-box-number" id="span_recuperado">S/. 560,141.00</span>
									<div class="progress">
										<div class="progress-bar" style="width: 100%;"></div>
									</div>
									<span class="progress-description">El capital recuperado hasta la fecha</span>
								</div>
							</div>
						</div>
						<!- CAPITAL POR RECUPERAR--
						<div class="col-md-4">
							<div class="info-box bg-yellow">
								<span class="info-box-icon">
									<i class="fa fa-bar-chart"></i>
								</span>
								<div class="info-box-content">
									<span class="info-box-text">Total Capital por Recuperar</span>
									<span class="info-box-number" id="span_por_recuperar">S/. 560,141.00</span>
									<div class="progress">
										<div class="progress-bar" style="width: 100%;"></div>
									</div>
									<span class="progress-description">El capital que falta recuperar a la fecha</span>
								</div>
							</div>
						</div>

						<!- CUOTAS FACTURADAS--
						<div class="col-md-4">
							<div class="info-box bg-aqua">
								<span class="info-box-icon">
									<i class="fa fa-bar-chart"></i>
								</span>
								<div class="info-box-content">
									<span class="info-box-text">Total Cuotas Facturadas</span>
									<span class="info-box-number" id="span_total_facturado">S/. 560,141.00</span>
									<div class="progress">
										<div class="progress-bar" style="width: 100%;"></div>
									</div>
									<span class="progress-description">Cuotas facturadas en este mes + 1 cuota vencida de cada crédito</span>
								</div>
							</div>
						</div>
						<!- CUOTAS COBRADAS--
						<div class="col-md-4">
							<div class="info-box bg-green">
								<span class="info-box-icon">
									<i class="fa fa-bar-chart"></i>
								</span>
								<div class="info-box-content">
									<span class="info-box-text">Total Cuotas Cobradas</span>
									<span class="info-box-number" id="span_total_cobrado">S/. 560,141.00</span>
									<div class="progress">
										<div class="progress-bar" style="width: 100%;"></div>
									</div>
									<span class="progress-description">Cuotas cobradas hasta la fecha/span>
								</div>
							</div>
						</div>
						<!- CUOTAS POR COBRAR--
						<div class="col-md-4">
							<div class="info-box bg-red">
								<span class="info-box-icon">
									<i class="fa fa-bar-chart"></i>
								</span>
								<div class="info-box-content">
									<span class="info-box-text">Total Cuotas por Cobrar</span>
									<span class="info-box-number" id="span_total_por_cobrar">S/. 560,141.00</span>
									<div class="progress">
										<div class="progress-bar" style="width: 100%;"></div>
									</div>
									<span class="progress-description">Cuotas impagas hasta la fecha</span>
								</div>
							</div>
						</div>
					</div --
				</div -->

				
			</div>
			<div id="div_modal_reportecredito_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISthO DEL TIPO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
