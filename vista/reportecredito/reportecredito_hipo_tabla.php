<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'cuota/Cuota.class.php');
    require_once(VISTA_URL.'creditohipo/Creditohipo.class.php');
    require_once(VISTA_URL.'ingreso/Ingreso.class.php');
    require_once(VISTA_URL.'ventagarantia/Ventagarantia.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    require_once(VISTA_URL.'funciones/funciones.php');
  }
  else{
    require_once('../cuota/Cuota.class.php');
    require_once('../creditohipo/Creditohipo.class.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../ventagarantia/Ventagarantia.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
  }
  $oCreditohipo = new Creditohipo();
  $oIngreso = new Ingreso();

  $cuota_fec1 = (isset($_POST['cuota_fec1']))? fecha_mysql($_POST['cuota_fec1']) : fecha_mysql($cuota_fec1);
  $cuota_fec2 = (isset($_POST['cuota_fec2']))? fecha_mysql($_POST['cuota_fec2']) : fecha_mysql($cuota_fec2);
  
  // CONMSULTAMOS LA CUOTAS FACTURADAS DEL TIPO: HIPOTECARIO
  $fac_hipo_dolares = 0; $fac_hipo_soles = 0;
  $subcredito_id = '1'; $credito_est = '3';
  $pag_hipo_dolares = 0; $pag_hipo_soles = 0;
  $dif_hipo_dolares = 0; $dif_hipo_soles = 0;

  $result = $oCreditohipo->listar_creditos_cuotas_facturadas_subcredito_detalle($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        if($value['tb_moneda_id'] == 1)
          $fac_hipo_soles += floatval($value['tb_cuotadetalle_cuo']);
        if($value['tb_moneda_id'] == 2)
          $fac_hipo_dolares += floatval($value['tb_cuotadetalle_cuo']);

        //aprovechamos esta misma funcion para consultar cuanto se ha pagado de cada cuota en la fecha del filtro
        $cuota_id = $value['tb_cuotadetalle_id']; $tipo_cuota = 2; $moneda_id = $value['tb_moneda_id'];
        $ingreso_fec1 = $cuota_fec1; $ingreso_fec2 = $cuota_fec2; $importe_cuota = 0;
        $result2 = $oIngreso->mostrar_importe_total_cuota_por_fecha($cuota_id, $tipo_cuota, $moneda_id, $ingreso_fec1, $ingreso_fec2);
          foreach ($result2['data'] as $key => $value2) {
            $importe_cuota = floatval($value2['importe_total']);
          }
        $result2 = NULL;

        if($value['tb_moneda_id'] == 1)
          $pag_hipo_soles += $importe_cuota;
        if($value['tb_moneda_id'] == 2)
          $pag_hipo_dolares += $importe_cuota;
      }  
    }
  $result = NULL;

  //DIFERENCIA ENTRE COBRADO VS FACTURADO GARVEH
  $dif_hipo_dolares = $fac_hipo_dolares - $pag_hipo_dolares;
  $dif_hipo_soles = $fac_hipo_soles - $pag_hipo_soles;

  // CONMSULTAMOS LA CUOTAS FACTURADAS DEL TIPO: ADENDAS
  $fac_adenda_dolares = 0; $fac_adenda_soles = 0;
  $subcredito_id = '2'; $credito_est = '3';
  $pag_adenda_dolares = 0; $pag_adenda_soles = 0;
  $dif_adenda_dolares = 0; $dif_adenda_soles = 0;

  $result = $oCreditohipo->listar_creditos_cuotas_facturadas_subcredito_detalle($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        if($value['tb_moneda_id'] == 1)
          $fac_adenda_soles += floatval($value['tb_cuotadetalle_cuo']);
        if($value['tb_moneda_id'] == 2)
          $fac_adenda_dolares += floatval($value['tb_cuotadetalle_cuo']);
        
        //aprovechamos esta misma funcion para consultar cuanto se ha pagado de cada cuota en la fecha del filtro
        $cuota_id = $value['tb_cuotadetalle_id']; $tipo_cuota = 2; $moneda_id = $value['tb_moneda_id'];
        $ingreso_fec1 = $cuota_fec1; $ingreso_fec2 = $cuota_fec2; $importe_cuota = 0;
        $result2 = $oIngreso->mostrar_importe_total_cuota_por_fecha($cuota_id, $tipo_cuota, $moneda_id, $ingreso_fec1, $ingreso_fec2);
          foreach ($result2['data'] as $key => $value2) {
            $importe_cuota = floatval($value2['importe_total']);
          }
        $result2 = NULL;

        if($value['tb_moneda_id'] == 1)
          $pag_adenda_soles += $importe_cuota;
        if($value['tb_moneda_id'] == 2)
          $pag_adenda_dolares += $importe_cuota;
      }  
    }
  $result = NULL;

  //DIFERENCIA ENTRE COBRADO VS FACTURADO ADENDAS
  $dif_adenda_dolares = $fac_adenda_dolares - $pag_adenda_dolares;
  $dif_adenda_soles = $fac_adenda_soles - $pag_adenda_soles;

  // CONMSULTAMOS LA CUOTAS FACTURADAS DEL TIPO: ACUERDOS DE PAGO
  $fac_acuerdo_dolares = 0; $fac_acuerdo_soles = 0;
  $subcredito_id = '3'; $credito_est = '3';
  $pag_acuerdo_dolares = 0; $pag_acuerdo_soles = 0;
  $dif_acuerdo_dolares = 0; $dif_acuerdo_soles = 0;

  $result = $oCreditohipo->listar_creditos_cuotas_facturadas_subcredito_detalle($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        if($value['tb_moneda_id'] == 1)
          $fac_acuerdo_soles += floatval($value['tb_cuotadetalle_cuo']);
        if($value['tb_moneda_id'] == 2)
          $fac_acuerdo_dolares += floatval($value['tb_cuotadetalle_cuo']);

        //aprovechamos esta misma funcion para consultar cuanto se ha pagado de cada cuota en la fecha del filtro
        $cuota_id = $value['tb_cuotadetalle_id']; $tipo_cuota = 2; $moneda_id = $value['tb_moneda_id'];
        $ingreso_fec1 = $cuota_fec1; $ingreso_fec2 = $cuota_fec2; $importe_cuota = 0;
        $result2 = $oIngreso->mostrar_importe_total_cuota_por_fecha($cuota_id, $tipo_cuota, $moneda_id, $ingreso_fec1, $ingreso_fec2);
          foreach ($result2['data'] as $key => $value2) {
            $importe_cuota = floatval($value2['importe_total']);
          }
        $result2 = NULL;

        if($value['tb_moneda_id'] == 1)
          $pag_acuerdo_soles += $importe_cuota;
        if($value['tb_moneda_id'] == 2)
          $pag_acuerdo_dolares += $importe_cuota;
      }  
    }
  $result = NULL;

  //DIFERENCIA ENTRE COBRADO VS FACTURADO ACUERDOS DE PAGO
  $dif_acuerdo_dolares = $fac_acuerdo_dolares - $pag_acuerdo_dolares;
  $dif_acuerdo_soles = $fac_acuerdo_soles - $pag_acuerdo_soles;

  //REPORTE DE EXEDENTES, CUOTAS PAGADAS EN FECHAS VENCIDAS
  $excedente_hipo_dolares = 0; $excedente_hipo_soles = 0;
  $excedente_adenda_dolares = 0; $excedente_adenda_soles = 0;
  $excedente_acuerdo_dolares = 0; $excedente_acuerdo_soles = 0;

  $tabla = 'tb_creditohipo'; $creditotipo_id = 4; $subcredito_id = '1'; $credito_est = '3';
  $result = $oIngreso->mostrar_importe_total_credito_subcredito_fecha($tabla, $creditotipo_id, $credito_est, $subcredito_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $excedente_hipo_dolares = floatval($value['importe_total_dolares']); 
        $excedente_hipo_soles = floatval($value['importe_total_soles']);
      }  
    }
  $result = NULL;

  $subcredito_id = '2';
  $result = $oIngreso->mostrar_importe_total_credito_subcredito_fecha($tabla, $creditotipo_id, $credito_est, $subcredito_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $excedente_adenda_dolares = floatval($value['importe_total_dolares']); 
        $excedente_adenda_soles = floatval($value['importe_total_soles']);
      }  
    }
  $result = NULL;

  $subcredito_id = '3';
  $result = $oIngreso->mostrar_importe_total_credito_subcredito_fecha($tabla, $creditotipo_id, $credito_est, $subcredito_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $excedente_acuerdo_dolares = floatval($value['importe_total_dolares']); 
        $excedente_acuerdo_soles = floatval($value['importe_total_soles']);
      }  
    }
  $result = NULL;

  //LISTA DE CREDITOS EN ESTADO PARALIZADO
  $credito_est = '4'; $num_paralizado_soles = 0; $num_paralizado_dolares = 0;
  $result = $oCreditohipo->listar_creditos_estado($credito_est);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value){
        if($value['tb_moneda_id'] == 1)
          $num_paralizado_soles ++;
        if($value['tb_moneda_id'] == 2)
          $num_paralizado_dolares ++;
      }  
    }
  $result = NULL;

  //LISTA DE CREDITOS LIQUIDADOS, Y EL MONTO LIQUIDADO ----- SOLES
  $cuenta_id = 36; //CUENTA DE LIQUIDACIÓN
  $subcuenta_id = 112; //SUBCUENTA DE CREDITO-HIPO
  $moneda_id = 1; // CONSULTAMOS PRIMERO LAS LIQUIDACIONES EN SOLES
  $num_liqui_soles = 0; $monto_liqui_soles = 0;
  $result = $oIngreso->mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $monto_liqui_soles = floatval($value['importe_total']);
        $num_liqui_soles = floatval($value['total_ingresos']);
      }
    }
  $result = NULL;

  //LISTA DE CREDITOS LIQUIDADOS, Y EL MONTO LIQUIDADO ----- DOLARES
  $cuenta_id = 36; //CUENTA DE LIQUIDACIÓN
  $subcuenta_id = 112; //SUBCUENTA DE CREDITO-HIPO
  $moneda_id = 2; // CONSULTAMOS LUEGO LAS LIQUIDACIONES EN DOLARES
  $num_liqui_dolares = 0; $monto_liqui_dolares = 0;
  $result = $oIngreso->mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $monto_liqui_dolares = floatval($value['importe_total']);
        $num_liqui_dolares = floatval($value['total_ingresos']);
      }
    }
  $result = NULL;

  //------------------------------------------------------------------------------------------------//
  //LISTA DE CREDITOS AMORTIZADOS, Y EL MONTO AMORTIZADO ----- SOLES
  $cuenta_id = 35; //CUENTA DE AMORTIZACION
  $subcuenta_id = 108; //SUBCUENTA DE CREDITO-HIPO
  $moneda_id = 1; // CONSULTAMOS PRIMERO LAS LIQUIDACIONES EN SOLES
  $num_amor_soles = 0; $monto_amor_soles = 0;
  $result = $oIngreso->mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $monto_amor_soles = floatval($value['importe_total']);
        $num_amor_soles = floatval($value['total_ingresos']);
      }
    }
  $result = NULL;

  //LISTA DE CREDITOS AMORTIZADOS, Y EL MONTO AMORTIZADO ----- DOLARES
  $cuenta_id = 35; //CUENTA DE AMORTIZACION
  $subcuenta_id = 108; //SUBCUENTA DE CREDITO-HIPO
  $moneda_id = 2; // CONSULTAMOS LUEGO LAS LIQUIDACIONES EN DOLARES
  $num_amor_dolares = 0; $monto_amor_dolares = 0;
  $result = $oIngreso->mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $cuota_fec1, $cuota_fec2);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $monto_amor_dolares = floatval($value['importe_total']);
        $num_amor_dolares = floatval($value['total_ingresos']);
      }
    }
  $result = NULL;
?>
<div class="row">
  <div class="col-md-12">
    <!-- FACTURADO -->
    <div class="col-sm-6">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Reporte Facturado Crédito HIPOTECARIO</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>Tipo Crédito</th>
                  <th>Facturado Dólares</th>
                  <th>Facturado Soles</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Crédito hipo</td>
                  <td><?php echo 'US$ '.mostrar_moneda($fac_hipo_dolares);?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($fac_hipo_soles);?></td>
                </tr>
                <tr>
                  <td>Adendas</td>
                  <td><?php echo 'US$ '.mostrar_moneda($fac_adenda_dolares);?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($fac_adenda_soles);?></td>
                </tr>
                <tr>
                  <td>Acuerdos de Pago</td>
                  <td><?php echo 'US$ '.mostrar_moneda($fac_acuerdo_dolares);?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($fac_acuerdo_soles);?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- COBRADO--->
    <div class="col-sm-6">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Reporte Cobrado Crédito HIPOTECARIO</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>Cobrado Dólares</th>
                  <th class="success">Exedente Dólares</th>
                  <th>Cobrado Soles</th>
                  <th class="success">Exedente Soles</th>
                  <th class="danger">Diferencia Dólares</th>
                  <th class="danger">Diferencia Soles</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo 'US$ '.mostrar_moneda($pag_hipo_dolares);?></td>
                  <td class="success"><?php echo 'US$ '.mostrar_moneda($excedente_hipo_dolares);?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($pag_hipo_soles);?></td>
                  <td class="success"><?php echo 'S/. '.mostrar_moneda($excedente_hipo_soles);?></td>
                  <td class="danger"><?php echo 'US$ '.mostrar_moneda($dif_hipo_dolares);?></td>
                  <td class="danger"><?php echo 'S/. '.mostrar_moneda($dif_hipo_soles);?></td>
                </tr>
                <tr>
                  <td><?php echo 'US$ '.mostrar_moneda($pag_adenda_dolares);?></td>
                  <td class="success"><?php echo 'US$ '.mostrar_moneda($excedente_adenda_dolares);?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($pag_adenda_soles);?></td>
                  <td class="success"><?php echo 'S/. '.mostrar_moneda($excedente_adenda_soles);?></td>
                  <td class="danger"><?php echo 'US$ '.mostrar_moneda($dif_adenda_dolares);?></td>
                  <td class="danger"><?php echo 'S/. '.mostrar_moneda($dif_adenda_soles);?></td>
                </tr>
                <tr>
                  <td><?php echo 'US$ '.mostrar_moneda($pag_acuerdo_dolares);?></td>
                  <td class="success"><?php echo 'US$ '.mostrar_moneda($excedente_acuerdo_dolares);?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($pag_acuerdo_soles);?></td>
                  <td class="success"><?php echo 'S/. '.mostrar_moneda($excedente_acuerdo_soles);?></td>
                  <td class="danger"><?php echo 'US$ '.mostrar_moneda($dif_acuerdo_dolares);?></td>
                  <td class="danger"><?php echo 'S/. '.mostrar_moneda($dif_acuerdo_soles);?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- CREDITOS PARALIZADOS, LIQUIDADOS, AMORTIZADOS-->
    <div class="col-sm-6">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Detalle de Crédito Hipotecario</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>Detalle</th>
                  <th>N° Créditos en Soles</th>
                  <th>N° Créditos en Dólares</th>
                  <th>Dinero en Soles</th>
                  <th>Dinero en Dólares</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Créditos Paralizados</td>
                  <td><?php echo $num_paralizado_soles;?></td>
                  <td><?php echo $num_paralizado_dolares;?></td>
                  <td>-</td>
                  <td>-</td>
                </tr>
                <tr>
                  <td>Créditos Liquidados</td>
                  <td><?php echo $num_liqui_soles;?></td>
                  <td><?php echo $num_liqui_dolares;?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($monto_liqui_soles);?></td>
                  <td><?php echo 'US$ '.mostrar_moneda($monto_liqui_dolares);?></td>
                </tr>
                <tr>
                  <td>Créditos Amortizados</td>
                  <td><?php echo $num_amor_soles;?></td>
                  <td><?php echo $num_amor_dolares;?></td>
                  <td><?php echo 'S/. '.mostrar_moneda($monto_amor_soles);?></td>
                  <td><?php echo 'US$ '.mostrar_moneda($monto_amor_dolares);?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>