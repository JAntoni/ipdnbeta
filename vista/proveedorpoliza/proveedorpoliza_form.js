
$(document).ready(function(){
	
	$('input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#form_proveedorpoliza').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"proveedorpoliza/proveedorpoliza_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_proveedorpoliza").serialize(),
				beforeSend: function() {
					$('#proveedorpoliza_mensaje').show(400);
					$('#btn_guardar_proveedorpoliza').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#proveedorpoliza_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#proveedorpoliza_mensaje').html(data.mensaje);

                        var vista = $("#proveedorpoliza_vista").val();
                        console.log(vista);
                        alerta_success("EXITO","PROVEEDOR POLIZA AGREGADO");
                        if(vista =='proveedorpoliza'){
                            proveedorpoliza_tabla();
                        }
                        if(vista =='controlseguro'){
                            cargar_proveedorpoliza();
                        }
                        $('#modal_registro_proveedorpoliza').modal('hide');
					}
					else{
		      	$('#proveedorpoliza_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#proveedorpoliza_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_proveedorpoliza').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#proveedorpoliza_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#proveedorpoliza_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	txt_proveedorpoliza_tip: {
	  		required: true,
				minlength: 1
	  	},
			txt_proveedorpoliza_nom: {
				required: true,
				minlength: 2
			},
			txt_proveedorpoliza_doc: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 11
			},
			txt_proveedorpoliza_dir: {
				required: true,
				minlength: 5
			},
			txt_proveedorpoliza_con: {
				required: true,
				minlength: 2
			},
			txt_proveedorpoliza_tel: {
				required: true,
				digits: true,
				minlength: 6,
				maxlength: 9
			},
			txt_proveedorpoliza_ema: {
				required: false,
				email: true
			}
		},
		messages: {
			txt_proveedorpoliza_tip: {
				required: "Seleccione el tipo de Persona",
				minlength: "Seleccione el tipo de Persona"
			},
			txt_proveedorpoliza_nom: {
				required: "Ingrese un nombre para el Proveedor",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_proveedorpoliza_doc: {
				required: "Ingrese el Documento del Proveedor",
				digits: "Solo números por favor",
				minlength: "El Documento como mínimo debe tener 8 números",
				maxlength: "El Documento como máximo debe tener 11 números"
			},
			txt_proveedorpoliza_dir: {
				required: "Ingrese una dirección para el Proveedor",
				minlength: "La dirección debe tener como mínimo 5 caracteres"
			},
			txt_proveedorpoliza_con: {
				required: "Ingrese un nombre de contacto para el Proveedor",
				minlength: "El contacto debe tener como mínimo 2 caracteres"
			},
			txt_proveedorpoliza_tel: {
				required: "Ingrese el Teléfono del Proveedor",
				digits: "Solo números por favor",
				minlength: "El Teléfono como mínimo debe tener 6 números",
				maxlength: "El Teléfono como máximo debe tener 9 números"
			},
			txt_proveedorpoliza_ema: {
				email: "Ingrese un Email válido"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});

function proveedortipo_form2(usuario_act, proveedortipo_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"proveedortipo/proveedortipo_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: 'proveedorpoliza',
      proveedortipo_id: proveedortipo_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_proveedortipo_form').html(data);
      	$('#modal_registro_proveedortipo').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_proveedortipo'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_proveedortipo', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'proveedortipo';
      	var div = 'div_modal_proveedortipo_form';
      	permiso_solicitud(usuario_act, proveedortipo_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function cargar_proveedortipo()
{
    console.log('B');
    $.ajax({
        type: "POST",
        url: VISTA_URL + "proveedortipo/proveedortipo_select.php",
        async: true,
        dataType: "html",
        data: ({
        }),
        beforeSend: function () {
            $('#cmb_rep_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_rep_id').html(html);
        }
    });
}
