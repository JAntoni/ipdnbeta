<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../proveedorpoliza/Proveedorpoliza.class.php');
  $oProveedorpoliza = new Proveedorpoliza();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$proveedorpoliza_tip = $_POST['txt_proveedorpoliza_tip']; //1 natural, 2 juridica
 		$proveedorpoliza_nom = mb_strtoupper($_POST['txt_proveedorpoliza_nom'], 'UTF-8');
 		$proveedorpoliza_doc = $_POST['txt_proveedorpoliza_doc'];
 		$proveedorpoliza_dir = $_POST['txt_proveedorpoliza_dir'];
 		$proveedorpoliza_con = $_POST['txt_proveedorpoliza_con'];
 		$proveedorpoliza_tel = $_POST['txt_proveedorpoliza_tel'];
 		$proveedorpoliza_ema = $_POST['txt_proveedorpoliza_ema'];
 		$proveedortipo_id = $_POST['cmb_rep_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar al Proveedor.';
 		if($oProveedorpoliza->insertar($proveedorpoliza_tip, $proveedorpoliza_nom, $proveedorpoliza_doc, $proveedorpoliza_dir, $proveedorpoliza_con, $proveedorpoliza_tel, $proveedorpoliza_ema, $proveedortipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proveedor registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$proveedorpoliza_id = intval($_POST['hdd_proveedorpoliza_id']);
 		$proveedorpoliza_tip = $_POST['txt_proveedorpoliza_tip']; //1 natural, 2 juridica
 		$proveedorpoliza_nom = mb_strtoupper($_POST['txt_proveedorpoliza_nom'], 'UTF-8');
 		$proveedorpoliza_doc = $_POST['txt_proveedorpoliza_doc'];
 		$proveedorpoliza_dir = $_POST['txt_proveedorpoliza_dir'];
 		$proveedorpoliza_con = $_POST['txt_proveedorpoliza_con'];
 		$proveedorpoliza_tel = $_POST['txt_proveedorpoliza_tel'];
 		$proveedorpoliza_ema = $_POST['txt_proveedorpoliza_ema'];
                $proveedortipo_id = $_POST['cmb_rep_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar al Proveedor.';

 		if($oProveedorpoliza->modificar($proveedorpoliza_id, $proveedorpoliza_tip, $proveedorpoliza_nom, $proveedorpoliza_doc, $proveedorpoliza_dir, $proveedorpoliza_con, $proveedorpoliza_tel, $proveedorpoliza_ema, $proveedortipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proveedor modificado correctamente. '.$proveedorpoliza_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$proveedorpoliza_id = intval($_POST['hdd_proveedorpoliza_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al Proveedor.';

 		if($oProveedorpoliza->eliminar($proveedorpoliza_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Proveedor eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>