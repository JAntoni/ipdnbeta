<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../proveedorpoliza/Proveedorpoliza.class.php');
  $oProveedorpoliza = new Proveedorpoliza();
  require_once('../funciones/funciones.php');

  $direc = 'proveedorpoliza';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $proveedorpoliza_id = $_POST['proveedorpoliza_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Proveedor Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Proveedor';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Proveedor';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Proveedor';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en proveedorpoliza
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'proveedorpoliza'; $modulo_id = $proveedorpoliza_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del proveedorpoliza por su ID
    if(intval($proveedorpoliza_id) > 0){
      $result = $oProveedorpoliza->mostrarUno($proveedorpoliza_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el proveedorpoliza seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $proveedorpoliza_tip = $result['data']['tb_proveedor_tip'];
          $proveedorpoliza_nom = $result['data']['tb_proveedor_poliza_nom'];
          $proveedorpoliza_doc = $result['data']['tb_proveedor_poliza_doc'];
          $proveedorpoliza_dir = $result['data']['tb_proveedor_poliza_dir'];
          $proveedorpoliza_con = $result['data']['tb_proveedor_poliza_con'];
          $proveedorpoliza_tel = $result['data']['tb_proveedor_poliza_tel'];
          $proveedorpoliza_ema = $result['data']['tb_proveedor_poliza_ema'];
          $proveedortipo_id = $result['data']['tb_proveedortipo_id'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_proveedorpoliza" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_proveedorpoliza" method="post">
          <input type="hidden" name="proveedorpoliza_vista" id="proveedorpoliza_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_proveedorpoliza_id" value="<?php echo $proveedorpoliza_id;?>">
          <div class="row">
                  <div class="col-md-12">
          <div class="modal-body">
              
            <div class="form-group">
              <label for="txt_proveedorpoliza_tip" class="control-label">Persona</label>
              <br>
              <label class="radio-inline for" style="padding-left: 0px;">
                <input type="radio" name="txt_proveedorpoliza_tip" class="flat-green" value="1" <?php if($proveedorpoliza_tip == 1) echo 'checked';?> > Natural
              </label>
              <label class="radio-inline">
                <input type="radio" name="txt_proveedorpoliza_tip" class="flat-green" value="2" <?php if($proveedorpoliza_tip == 2) echo 'checked';?> > Jurídica
              </label>
            </div>
            <div class="form-group">
                <label for="cmb_rep_id">Tipo Proveedor</label><br/>
                <div class="input-group">
                <select name="cmb_rep_id" id="cmb_rep_id" class="form-control input-sm mayus">
                    <?php require_once '../proveedortipo/proveedortipo_select.php'; ?>
                </select>
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm" type="button" onclick="proveedortipo_form2('I', 0)" title="AGREGAR TIPO PROVEEDOR">
                        <span class="fa fa-plus icon"></span>
                    </button>
                </span>
                    </div> 
            </div> 
            <div class="form-group">
              <label for="txt_proveedorpoliza_nom" class="control-label">Nombre de Proveedor</label>
              <input type="text" name="txt_proveedorpoliza_nom" id="txt_proveedorpoliza_nom" class="form-control input-sm mayus" value="<?php echo $proveedorpoliza_nom;?>">
            </div>
            <div class="form-group">
              <label for="txt_proveedorpoliza_doc" class="control-label">RUC/DNI</label>
              <input type="text" name="txt_proveedorpoliza_doc" id="txt_proveedorpoliza_doc" class="form-control input-sm" value="<?php echo $proveedorpoliza_doc;?>">
            </div>
            <div class="form-group">
              <label for="txt_proveedorpoliza_dir" class="control-label">Dirección de Proveedor</label>
              <input type="text" name="txt_proveedorpoliza_dir" id="txt_proveedorpoliza_dir" class="form-control input-sm" value="<?php echo $proveedorpoliza_dir;?>">
            </div>
            <div class="form-group">
              <label for="txt_proveedorpoliza_con" class="control-label">Contacto</label>
              <input type="text" name="txt_proveedorpoliza_con" id="txt_proveedorpoliza_con" class="form-control input-sm" value="<?php echo $proveedorpoliza_con;?>">
            </div>
            <div class="form-group">
              <label for="txt_proveedorpoliza_tel" class="control-label">Teléfono</label>
              <input type="text" name="txt_proveedorpoliza_tel" id="txt_proveedorpoliza_tel" class="form-control input-sm" value="<?php echo $proveedorpoliza_tel;?>">
            </div>
            <div class="form-group">
              <label for="txt_proveedorpoliza_ema" class="control-label">Email</label>
              <input type="text" name="txt_proveedorpoliza_ema" id="txt_proveedorpoliza_ema" class="form-control input-sm" value="<?php echo $proveedorpoliza_ema;?>">
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Proveedor?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="proveedorpoliza_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
            
          </div>
                      </div>
                  </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_proveedorpoliza">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_proveedorpoliza">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_proveedorpoliza">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/proveedorpoliza/proveedorpoliza_form.js';?>"></script>
