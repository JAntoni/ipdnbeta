<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Proveedorpoliza extends Conexion {

    function insertar($proveedor_poliza_tip, $proveedor_poliza_nom, $proveedor_poliza_doc, $proveedor_poliza_dir, $proveedor_poliza_con, $proveedor_poliza_tel, $proveedor_poliza_ema, $proveedortipo_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proveedor_poliza(tb_proveedor_tip, tb_proveedor_poliza_nom, tb_proveedor_poliza_doc, tb_proveedor_poliza_dir, tb_proveedor_poliza_con, tb_proveedor_poliza_tel, tb_proveedor_poliza_ema, tb_proveedortipo_id)
          VALUES (:proveedor_poliza_tip, :proveedor_poliza_nom, :proveedor_poliza_doc, :proveedor_poliza_dir, :proveedor_poliza_con, :proveedor_poliza_tel, :proveedor_poliza_ema, :proveedortipo_id)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_poliza_tip", $proveedor_poliza_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_poliza_nom", $proveedor_poliza_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_doc", $proveedor_poliza_doc, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_dir", $proveedor_poliza_dir, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_con", $proveedor_poliza_con, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_tel", $proveedor_poliza_tel, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_ema", $proveedor_poliza_ema, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar($proveedor_poliza_id, $proveedor_poliza_tip, $proveedor_poliza_nom, $proveedor_poliza_doc, $proveedor_poliza_dir, $proveedor_poliza_con, $proveedor_poliza_tel, $proveedor_poliza_ema, $proveedortipo_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proveedor_poliza 
          SET tb_proveedor_tip =:proveedor_poliza_tip, tb_proveedor_poliza_nom =:proveedor_poliza_nom, 
          tb_proveedor_poliza_doc =:proveedor_poliza_doc, tb_proveedor_poliza_dir =:proveedor_poliza_dir, 
          tb_proveedor_poliza_con =:proveedor_poliza_con, tb_proveedor_poliza_tel =:proveedor_poliza_tel, 
          tb_proveedor_poliza_ema =:proveedor_poliza_ema, tb_proveedortipo_id=:proveedortipo_id WHERE tb_proveedor_poliza_id =:proveedor_poliza_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_poliza_id", $proveedor_poliza_id, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_poliza_tip", $proveedor_poliza_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_poliza_nom", $proveedor_poliza_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_doc", $proveedor_poliza_doc, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_dir", $proveedor_poliza_dir, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_con", $proveedor_poliza_con, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_tel", $proveedor_poliza_tel, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_poliza_ema", $proveedor_poliza_ema, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($proveedor_poliza_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proveedor_poliza SET tb_proveedor_poliza_xac=0 WHERE tb_proveedor_poliza_id =:proveedor_poliza_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_poliza_id", $proveedor_poliza_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($proveedor_poliza_id) {
        try {
            $sql = "SELECT * FROM tb_proveedor_poliza WHERE tb_proveedor_poliza_id =:proveedor_poliza_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_poliza_id", $proveedor_poliza_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_proveedor_polizas() {
        try {
            $sql = "SELECT * FROM tb_proveedor_poliza pp INNER JOIN tb_proveedortipo pt ON pt.tb_proveedortipo_id=pp.tb_proveedortipo_id where tb_proveedor_poliza_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function Proveedorpoliza_autocomplete($dato) {
        try {

            $filtro = "%" . $dato . "%";

            $sql = "SELECT *
		FROM tb_proveedor_poliza 
                WHERE tb_proveedor_poliza_nom LIKE :filtro OR tb_proveedor_poliza_doc LIKE :filtro";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Proveedores registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrar_por_dni($dni) {
        try {
            $sql = "SELECT * FROM tb_proveedor_poliza where tb_proveedor_poliza_doc =:tb_proveedor_poliza_doc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proveedor_poliza_doc", $dni, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontro ningún proveedor_poliza con el número de documento ingresado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>
