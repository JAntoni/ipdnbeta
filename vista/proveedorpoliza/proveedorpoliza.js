var datatable_global;

function proveedorpoliza_form(usuario_act, proveedorpoliza_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"proveedorpoliza/proveedorpoliza_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: 'proveedorpoliza',
      proveedorpoliza_id: proveedorpoliza_id,
      vista: 'proveedorpoliza'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_proveedorpoliza_form').html(data);
      	$('#modal_registro_proveedorpoliza').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_proveedorpoliza'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_proveedorpoliza', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'proveedorpoliza';
      	var div = 'div_modal_proveedorpoliza_form';
      	permiso_solicitud(usuario_act, proveedorpoliza_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function proveedorpoliza_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proveedorpoliza/proveedorpoliza_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#proveedorpoliza_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_proveedorpoliza_tabla').html(data);
      $('#proveedorpoliza_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#proveedorpoliza_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_proveedorpoliza').DataTable({
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
      columnDefs: [
         { orderable: false, targets: [1, 2, 3, 4, 5, 6, 7] }
      ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}
$(document).ready(function() {
  //estilos_datatable();
  proveedorpoliza_tabla();
});