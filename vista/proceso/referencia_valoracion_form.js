
$(document).ready(function(){
	
	detalleValoracion($('#hdd_det_valoracion_id').val());

  	$('#form_referencia_valoracion').validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"proceso/proceso_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_referencia_valoracion").serialize(),
				beforeSend: function() {
					//$('#proveedor_mensaje').show(400);
					$('#btn_guardar_referencia_valoracion').prop('disabled', true);
				},
				success: function(data){
					console.log(data);
					
					if(parseInt(data.estado) > 0){
						$('#modal_referencia_valoracion').modal('hide');
						Swal.fire(
							'Excelente!',
							data.mensaje,
							'success'
						);
						refValoracion('actualizar_valoracion', data.proceso_id, data.usuario_id, data.proceso_fase_item_id);
					}
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					console.log(data);
				}
			});
	  	},
	  
	});

});

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}

function refValoracion(accion, proceso_id, usuario_id, proceso_fase_item_id){
	$.ajax({
	  type: "POST",
	  url: VISTA_URL+"proceso/referencia_valoracion.php",
	  async: true,
	  dataType: "html",
	  data: ({
		accion: accion,
		proceso_id: proceso_id,
		usuario_id: usuario_id,
		proceso_fase_item_id: proceso_fase_item_id
	  }),
	  beforeSend: function() {
	  },
	  success: function(data){
		$('#div_modal_ref_valoracion').html(data);
		$('#modal_ref_valoracion').modal('show');
		modal_height_auto('modal_ref_valoracion'); //funcion encontrada en public/js/generales.js
		modal_hidden_bs_modal('modal_ref_valoracion', 'limpiar'); //funcion encontrada en public/js/generales.js
	  },
	  complete: function(data){
		$('#modal_mensaje').modal('hide');
	  },
	  error: function(data){
		alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
	  }
	});
}

function detalleValoracion(valoracion_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "proceso/proceso_controller.php",
		async:true,
		dataType: "html",                      
		data: ({
		  action: 'detalle_valoracion',
		  valoracion_id: valoracion_id,
		}),
		beforeSend: function() {
		},
		success: function(html){
			$('#listaValoraciones').html(html);
		}
	});
}

function guardarValoracionDetalle(accion, valoracion_id) {
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/proceso_controller.php",
		async: true,
		dataType: "json",
		data: {
			action: accion,
			valoracion_id: valoracion_id,
			moneda: $('#valoraciondetalle_moneda').val(),
			monto: $('#valoraciondetalle_monto').val(),
			anio: $('#valoraciondetalle_anio').val(),
			km: $('#valoraciondetalle_km').val(),
			link: $('#valoraciondetalle_link').val(),
		},
		beforeSend: function() {
		},
		success: function(data){
			if(data.estado==1){
				notificacion_success(data.mensaje, 3000);
        		detalleValoracion(valoracion_id);
				$('#valoraciondetalle_monto').val('');
				$('#valoraciondetalle_anio').val('');
				$('#valoraciondetalle_km').val('');
				$('#valoraciondetalle_link').val('');
			}else{
				notificacion_warning(data.mensaje, 3000);
			}
		},
		complete: function(data){
		},
			
	});
}

function valoracionFile(usuario_act, valoracion_id, valoracionfile_id, proceso_id, usuario_id, proceso_fase_item_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/referencia_valoracion_file.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            valoracion_id: valoracion_id,
            valoracionfile_id: valoracionfile_id,
            vista: 'valoracion'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_valoracion_file').html(data);
                $('#modal_valoracion_file').modal('show');

                //desabilitar elementos del form si es L (LEER)
                //if (usuario_act == 'L' || usuario_act == 'E')
                    //form_desabilitar_elementos('form_garantiafile');

				//refValoracion('actualizar_valoracion', proceso_id, usuario_id, proceso_fase_item_id);

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_valoracion_file'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_valoracion_file', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

// EXPORT PFD VALORACION
function exportarValoracionPDF(proceso_id, usuario_id, proceso_fase_item_id, valoracion_id) {
	window.open("vista/proceso/referencia_valoracion_pdf.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&proceso_fase_item_id="+proceso_fase_item_id+"&valoracion_id="+valoracion_id,"_blank");
}