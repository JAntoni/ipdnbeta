<?php
	session_name("ipdnsac");
  session_start();
	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();

  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);

  $proceso = $oProceso->mostrarUno($proceso_id);
  $usuario = $oUsuario->mostrarUno($usuario_id);

  $banca = 0;
  $perfil_id = 0;
  $bloqueo_boton = '';

  if($proceso['estado']==1){
    $banca = intval($proceso['data']['tb_proceso_banca']);
  }
  if($usuario['estado']==1){
    $perfil_id = intval($usuario['data']['tb_usuarioperfil_id']);
  }
  if($banca>0){
    if($perfil_id!=1){
      $bloqueo_boton = 'disabled';
    }
  }
  
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_bancarizacion" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <input type="hidden" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Escoger Bancarización</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="form-group" style="padding-top: 15px; padding-bottom: 15px;">
              <label>Bancarización: </label>
              <select id="cmb_banca_id" name="cmb_banca_id" class="form-control" data-live-search="true" data-max-options="1">
                <option value="0">-- Selecciona una opción --</option>
                <option value="1" <?php if($banca==1) echo 'selected="selected"'?>>Cheque</option>
                <option value="2" <?php if($banca==2) echo 'selected="selected"'?>>Transferencia</option>
                <option value="3" <?php if($banca==3) echo 'selected="selected"'?>>Depósito</option>
                <option value="4" <?php if($banca==4) echo 'selected="selected"'?>>Efectivo</option>
              </select>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="btn_guardar_bancarizacion" class="btn btn-info" onclick="registrarBanca()" <?php echo $bloqueo_boton; ?>>Guardar</button>
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>