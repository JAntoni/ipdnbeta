
$(document).ready(function(){
	$('#btn_cerrar_valoracionfile').click(function(event) {
		//garantiafile_galeria();
	});

	$('#file_upload').uploadifive({
	  'auto'             : false,
	  //'checkScript'      : 'programacion_file_check.php',
	  'formData'         : {
	                         'action'    : 'insertar_valoracionfile',
	                         'valoracion_id' : $('#hdd_valoracion_id').val()
	                       },
	  'queueID'          : 'queue',
	  'uploadScript'     : VISTA_URL+'proceso/proceso_controller.php',
	  'queueSizeLimit'   : 10,
	  'uploadLimit'      : 10,
	  'multi'            : true,
	  'buttonText'       : 'Seleccionar Archivo',
	  'height'           : 20,
	  'width'            : 180,
	  'fileSizeLimit'    : '5MB',
	  'fileType'         : ["image\/gif","image\/jpeg","image\/png","image\/jpg"],
	  'onUploadComplete' : function(file, data) {
	   	if(data != 'exito'){
	   		$('#alert_img').show(300);
	   		$('#alert_img').append('<strong>El archivo: '+file.name+', no se pudo subir -> '+data+'</strong><br>');
	   	} 
	  },
	  'onError': function (errorType) {
	      var message = "Error desconocido.";
	      if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
	          message = "Tamaño de archivo debe ser menor a 3MB.";
	      } else if (errorType == 'FORBIDDEN_FILE_TYPE') {
	          message = "Tipo de archivo no válido.";
	          alert(message);
	      }
	      //$(".uploadifive-queue-item.error").hide();
	      //
	      //$.FormFeedbackError(message);
	  },
	  'onAddQueueItem' : function(file) {
	      //console.log('The file ' + file.name + ' was added to the queue!');
	      //alert(file.type);
	  },
	  /*'onCheck'      : function(file, exists) {
	      if (exists) {
	          alert('El archivo ' + file.name + ' ya existe.');
	      }
	  }*/
	});

	$('#form_valoracionfile').submit(function(event) {
		event.preventDefault();
		$.ajax({
				type: "POST",
				url: VISTA_URL+"proceso/proceso_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_valoracionfile").serialize(),
				beforeSend: function() {
					$('#valoracionfile_mensaje').show(400);
					$('#btn_guardar_valoracionfile').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#valoracionfile_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#valoracionfile_mensaje').html(data.mensaje);
		      	var garantiafile_id = $('#hdd_garantiafile_id').val();
		      	$('#garantiafileid_'+garantiafile_id).hide(400);
		      	$('#modal_registro_garantiafile').modal('hide');
					}
					else{
		      	$('#valoracionfile_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#valoracionfile_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_valoracionfile').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#valoracionfile_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#valoracionfile_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	});
			
});
