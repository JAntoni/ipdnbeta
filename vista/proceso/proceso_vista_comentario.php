<?php 
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

setlocale(LC_ALL,"es_ES");

if($_POST['proceso_fase_id']==NULL){
    $proceso_fase_id = intval($value['tb_proceso_fase_id']); // valor traído de la vista de la fase al estar dentro del foreach
}else{
    $proceso_fase_id = intval($_POST['proceso_fase_id']);
}
$fechas_comentarios = $oProceso->listar_fechas_comentarios($proceso_fase_id);
$comentarios = $oProceso->listar_comentarios($proceso_fase_id);

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>

<div class="tab-pane" id="timeline">
    <ul class="timeline timeline-inverse">
    
        <?php 
            if($fechas_comentarios['estado']==1){
                foreach ($fechas_comentarios['data'] as $key => $value1) {

        ?>
                <li class="time-label">
                    <span class="bg-red">
                        <?php echo date_format(date_create($value1['fecha_formateada']),'d M. Y'); ?>
                    </span>
                </li>
                <?php 
                    foreach ($comentarios['data'] as $key => $value2) {
                        if($value1['fecha_formateada'] == $value2['fecha_formateada']){
                            $hora = explode(' ', $value2['tb_comentario_fecreg']);
                            if($value2['tb_comentario_des']!=''){ // se trata de comentario de texto

                ?>
                                <li>
                                    <i class="fa fa-comments bg-yellow"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> <?php echo date("g:i a", strtotime($hora[1])); ?></span>
                                        <div class="user-block timeline-header" style="padding: 5px;">
                                            <img class="img-circle img-bordered-sm" src="<?php echo $value2['tb_usuario_fot']; ?>" alt="user image">
                                            <span class="username">
                                                <a href="#"><?php echo $value2['tb_usuario_nom'].' '.$value2['tb_usuario_ape']; ?></a>
                                            </span>
                                            <span class="description">hizo un comentario. </span>
                                        </div>
                                        <!-- <h5 class="timeline-header"><a href="#"><?php echo $value2['tb_usuario_nom'].' '.$value2['tb_usuario_ape']; ?></a> hizo un comentario.</h5> -->
                                        <div class="timeline-body" style="font-size: 14px;">
                                            <?php echo $value2['tb_comentario_des']; ?>
                                        </div>
                                    </div>
                                </li>
                <?php       }else{ 
                ?>
                                <li>
                                    <i class="fa fa-camera bg-purple"></i>

                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> <?php echo date("g:i a", strtotime($hora[1])); ?></span>
                                        <div class="user-block timeline-header" style="padding: 5px;">
                                            <img class="img-circle img-bordered-sm" src="<?php echo $value2['tb_usuario_fot']; ?>" alt="user image">
                                            <span class="username">
                                                <a href="#"><?php echo $value2['tb_usuario_nom'].' '.$value2['tb_usuario_ape']; ?></a>
                                            </span>
                                            <span class="description">subió archivo multimedia. </span>
                                        </div>
                                        <!-- <h5 class="timeline-header"><a href="#"><?php echo $value2['tb_usuario_nom'].' '.$value2['tb_usuario_ape']; ?></a> subió archivo multimedia.</h5> -->
                                        <div class="timeline-body">
                <?php     
                                        $comentarios_fotos = $oProceso->listar_comentarios_fotos($proceso_fase_id, $value2['tb_comentario_id']);
                                        if($comentarios_fotos['estado']==1){
                                            foreach ($comentarios_fotos['data'] as $key => $value3) {
                                                        //$hora2 = explode(' ', $value3['upload_reg']);
                ?>
                                                <img class="margin img-responsive" alt="Photo" style="width: 80px; cursor: pointer;" src="<?php echo $value3['upload_url']; ?>" onclick="carousel('proc_proceso_fase_comentario', <?php echo $proceso_fase_id; ?>, <?php echo $value2['tb_comentario_id']; ?>)">
                <?php 
                                            }
                                        }
                ?>
                                        </div>
                                    </div>
                                </li>
                <?php 
                            }
                        }
                    }
                ?>
                <!-- <li class="time-label">
                    <span class="bg-green">
                    3 Jan. 2014
                    </span>
                </li> -->
                
                <!-- <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                </li> -->
        <?php 
                }
            }
        ?>
    </ul>
</div>