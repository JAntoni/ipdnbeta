
$(document).ready(function () {
  console.log('cambioas Antonio 08-11 -->111')
  verPaginaproceso('1');

  $("#txt_num_credito").focus();

  $("#txt_cliente_nom").autocomplete({
      minLength: 1,
      source: function (request, response) {
          $.getJSON(
              VISTA_URL + "cliente/cliente_autocomplete.php",
              { term: request.term }, //
              response
          );
      },
      select: function (event, ui) {
          $('#hdd_cliente_id').val(ui.item.cliente_id);
          $('#txt_cliente_nom').val(ui.item.cliente_nom);
          
          verPaginaproceso('1');
          event.preventDefault();
          
          $("#txt_num_credito").focus();
      }
  });

  $('#form_anular_proceso').validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL+"proceso/proceso_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_anular_proceso").serialize(),
        beforeSend: function() {
          $('#btn_guardar_anular_proceso').prop('disabled', true);
        },
        success: function(data){
          if(parseInt(data.estado) == 0){
            Swal.fire(
              'Error!',
              data.mensaje,
              'error'
            );
            $('#btn_guardar_anular_proceso').prop('disabled', false);
  
          }else
          if(parseInt(data.estado) == 1){
            $('#modal_anular_proceso').modal('hide');
            Swal.fire(
              'Excelente!',
              data.mensaje,
              'success'
            );
          }else
          if(parseInt(data.estado) == 2){
            Swal.fire(
              'Alerta!',
              data.mensaje,
              'alert'
            );
            $('#btn_guardar_anular_proceso').prop('disabled', false);
  
          }
          
        },
        complete: function(data){
          verPaginaproceso('1'); // volvemos a traer el listado de procesos
        },
          
      });
      },
    
  });

  $('#form_eliminar_cheque').validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL+"proceso/proceso_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_eliminar_cheque").serialize(),
        beforeSend: function() {
          $('#btn_guardar_eliminar_cheque').prop('disabled', true);
        },
        success: function(data){
          if(parseInt(data.estado) == 0){
            Swal.fire(
              'Error!',
              data.mensaje,
              'error'
            );
            $('#btn_guardar_eliminar_cheque').prop('disabled', false);
  
          }else
          if(parseInt(data.estado) == 1){
            $('#modal_eliminar_cheque').modal('hide');
            Swal.fire(
              'Excelente!',
              data.mensaje,
              'success'
            );
            generarCheche(data.proceso_id, data.usuario_id);
          }else
          if(parseInt(data.estado) == 2){
            Swal.fire(
              'Alerta!',
              data.mensaje,
              'alert'
            );
            $('#btn_guardar_eliminar_cheque').prop('disabled', false);
  
          }
          
        },
        complete: function(data){
          verPaginaproceso('1'); // volvemos a traer el listado de procesos
        },
          
      });
      },
    
  });

});

$('#txt_num_credito, #txt_fecha_ini, #txt_fecha_fin, #hdd_cliente_id, #cmb_usuario_id').change(function (event) {
  //proceso_tabla();
  verPaginaproceso('1');
});

$('#datetimepicker1, #datetimepicker2').datepicker({
  language: 'es',
  autoclose: true,
  format: "dd-mm-yyyy",
  //startDate: "-0d",
  //endDate : new Date()
});

//se usa 
function verPaginaproceso(pag) {
  var pagina = pag;
  var cantidad = '';
  document.getElementById('nroPagina').value = pag;
  sufijo = $('#sufijo').val();
  if (document.getElementById('cboCantidadBusquedaproceso')) {
      cantidad = document.getElementById('cboCantidadBusquedaproceso').value;
  }
  $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_tabla.php",
      async: true,
      dataType: "html",
      data: ({
          fecini: $('#txt_fecha_ini').val(),
          fecfin: $('#txt_fecha_fin').val(),
          cre_id: $('#txt_num_credito').val(),
          cli_id: $('#hdd_cliente_id').val(),
          use_id: $('#cmb_usuario_id').val(),
          estado: $('#cmb_estado').val(),
          pagina: pagina,
          cantidad: cantidad
      }),
      beforeSend: function () {
      },
      success: function (html) {
          $('#div_proceso_tabla').html(html);
          estilos_datatable()
      },
      complete: function (html) {

      }
  });

}

// en uso para paginado / numero celular
function solo_numero(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}


// uso para generar nuevo proceso
function btnNuevoProceso() {
  $("#vista_main").addClass("hidden");
  $("#vista_nuevo").removeClass("hidden");
  $('#div_proceso_tabla').html(''); // borramos la tabla de listado de procesos para aliviar la carga en pantalla
}

// uso para volver a la lista de procesos
function btnVolverMenu(vista) {
  $("#vista_fase").addClass("hidden");
  if(vista=='editar'){
    document.getElementById('vista_fase').innerHTML = ''; // dejar limpio el contenido de fases
  }
  $("#vista_nuevo").addClass("hidden");
  $("#vista_main").removeClass("hidden");
  verPaginaproceso('1'); // volvemos a traer el listado de procesos
  resetCreditoTipo();
}

// uso para volver a la lista de procesos
function verFases() {
  $("#vista_fase").removeClass("hidden");
  $("#vista_nuevo").addClass("hidden");
  $("#vista_main").addClass("hidden");
  //$('#div_proceso_tabla').html('');
}

function resetCreditoTipo(){
  document.getElementById("cmb_cgarvtipo_id").selectedIndex = 0; //1 = option 2
}

// seleccion tipo credito generando nuevo proceso
function selectTipoCredito(value) {
  if(value==1){
    $("#tipo-garv").addClass("hidden");
    //$("#cmb_cgarvtipo_id").val("0");
    //$("#cmb_cgarvtipo_id option").attr("selected",false);
    $("#cmb_cgarvtipo_id option").attr("selected",false).change();
  }else if(value==2){
    $("#tipo-garv").addClass("hidden");
    $("#cmb_cgarvtipo_id").val("0");

  }else if(value==3){
    $("#tipo-garv").removeClass("hidden");
  }else if(value==4){
    $("#tipo-garv").addClass("hidden");
    $("#cmb_cgarvtipo_id").val("0");

  }else if(value==5){
    $("#tipo-garv").addClass("hidden");
    $("#cmb_cgarvtipo_id").val("0");

  }
}

function btnGenerarProceso() {

  var creditotipo_id = $("#cmb_creditotipo_id").val();
  var cgarvtipo_id = $("#cmb_cgarvtipo_id").val(); // para tipo de crédico garveh

  if(creditotipo_id=='0' || creditotipo_id==''){
    Swal.fire(
      '¡Aviso!',
      'Seleccione un tipo de crédito válido.',
      'warning'
    );
  }else{
    if(creditotipo_id==1){ // CREDITO MENOR 
      // aún no hay proceso
      Swal.fire(
        '¡Aviso!',
        'Se está trabajando en los procesos.',
        'info'
      );
    }else if(creditotipo_id==2){ // CRÉDITO ASCVEH
      // aún no hay proceso
      Swal.fire(
        '¡Aviso!',
        'Se está trabajando en los procesos.',
        'info'
      );
    }else if(creditotipo_id==3){ // CRÉDITO GARVEH

      if(cgarvtipo_id=='0' || cgarvtipo_id==''){
        Swal.fire(
          '¡Aviso!',
          'Seleccione un tipo de garantía válido.',
          'warning'
        );
      }else{
        $.ajax({
          type: "POST",
          url: VISTA_URL + "proceso/proceso_controller.php",
          async: false,
          dataType: "json",
          data: ({
          action: 'insertar',
          creditotipo_id: creditotipo_id, 
          cgarvtipo_id: cgarvtipo_id
          //... para posteriores valores de difentess tipo de credito
        }),
            beforeSend: function () {
            },
            success: function (data) {
              // cargar la vista de fases del nuevo proceso creado
              Swal.fire(
                'Excelente!',
                data.mensaje,
                'success'
              );
              //verFases();
              //$("#hdd_proceso_id").val(data.proceso_id);
              verFaseProceso('R', data.proceso_id); // R=Register
            },
            complete: function (data) {
            }
        });
      }
      
    }else if(creditotipo_id==4){ // CREDITO HIPOTECARIO
      // aún no hay proceso
      Swal.fire(
        '¡Aviso!',
        'Se está trabajando en los procesos.',
        'info'
      );
    }else if(creditotipo_id==5){ // ADENDAS
      // aún no hay proceso
      Swal.fire(
        '¡Aviso!',
        'Se está trabajando en los procesos.',
        'info'
      );
    }
    
  }
  
}

function verFaseProceso(action, proceso_id, usuario_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_vista_fase.php",
      async: true,
      dataType: "html",
      data: ({
        proceso_id: proceso_id,
        usuario_id: usuario_id
      }),
      beforeSend: function () {
      },
      success: function (html) {
          $('#div_proceso_fase').html(html);
      },
      complete: function (html) {
      verFases();
      detalleParticipante();

      }
  });
}

function btnProcesoFase(action, proceso_id, usuario_id) {
  verFaseProceso(action, proceso_id, usuario_id);
}

function registrarComentario(usuario_id,proceso_fase_id) {
  var comentario = $('#txt_comentario'+proceso_fase_id).val();
  if(comentario == null || comentario == ''){
    Swal.fire(
      'Alerta!',
      'Ingrese un comentario.',
      'warning'
    );
  }else{
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
        action: 'registro_comentario',
        usuario_id: usuario_id,
        proceso_fase_id: proceso_fase_id,
        txt_comentario: comentario,
      }),
      beforeSend: function () {
        $('#btn_comentario'+proceso_fase_id).prop('disabled', true);
      },
      success: function (data) {
        if(data.estado==1){
          notificacion_success(data.mensaje, 3000);
          verComentariosFase(data.proceso_fase_id);
        }else{
          notificacion_warning(data.mensaje, 3000);
        }
      },
      complete: function (data) {
        $('#txt_comentario'+proceso_fase_id).val('')
        $('#btn_comentario'+proceso_fase_id).prop('disabled', false);
      }
    });
  }
}

function verComentariosFase(proceso_fase_id) {
  var id = 0;
  if(proceso_fase_id==null || proceso_fase_id=='' || proceso_fase_id==undefined){
    id = $('#hdd_proceso_fase_id_'+proceso_fase_id).val();
  }else{
    id = proceso_fase_id;
  }
  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_vista_comentario.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: $('#hdd_usuario_id').val(),
      proceso_id: $('#hdd_proceso_id').val(),
      proceso_fase_id: id,
    }),
    beforeSend: function () {
      $('#btn_comentario').prop('disabled', true);
    },
    success: function (html) {
      $('#div_comentarios'+proceso_fase_id).html(html);
    },
    complete: function (html) {
      $('#btn_comentario').prop('disabled', false);
    }
  });
}

// SUBIDA DE IMAGEN A COMENTARIO
function upload_form(usuario_act, usuario_id, proceso_fase_id) {
  var id = 0;
  if(proceso_fase_id==null || proceso_fase_id=='' || proceso_fase_id==undefined){
    id = $('#hdd_proceso_fase_id_'+proceso_fase_id).val();
  }else{
    id = proceso_fase_id;
  }
  $.ajax({
      type: "POST",
      url: VISTA_URL + "upload/upload_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          usuario_id: usuario_id,
          upload_id: id,
          modulo_nom: 'proc_proceso_fase_comentario', //nombre de la tabla a relacionar
          modulo_id: id, //aun no se guarda este modulo
          upload_uniq: $('#upload_uniq').val() //ID temporal
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
          $('#modal_mensaje').modal('hide');
          if (data != 'sin_datos') {
              $('#div_modal_upload_form').html(data);
              $('#modal_registro_upload').modal('show');

              //notificacion_success('Imagen(es) añadidas correctamente.', 3000);
              //verComentariosFase(id);

              //funcion js para agregar un largo automatico al modal, al abrirlo
              modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
          }
      },
      complete: function (data) {
        
      },
      error: function (data) {
          alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
          swal_error('ERRROR', data.responseText, 5000);
      }
  });

}
//

// VISTA DE IMAGENES EN COMENTARIO
function carousel(modulo_nom, modulo_id, comentario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id,
      comentario_id: comentario_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
//

// EXONERAR SUBIDA DE ARCHIVO
function exonerar_multimedia_form(proceso_fase_item_id, item_des, item_idx, pdf_is){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/exonerar_multimedia.php",
    async:true,
    dataType: "html",                      
    data: ({
        proceso_fase_item_id:	proceso_fase_item_id,
        //credito_id: $("#hdd_creditoid").val(),
        //cliente_id: $("#hdd_clienteid").val(),
        item_des: item_des,
        item_idx: item_idx,
        pdf_is: pdf_is //nuevo o editar
    }),
    beforeSend: function() {
    },
    success: function(data){
        /* if(data != 'sin_datos'){ */
          $('#div_exonerar_proceso_fase_item').html(data);
          modal_width_auto('modal_proceso_fase_item_exonerar',80);
          modal_height_auto('modal_proceso_fase_item_exonerar');
          $('#modal_proceso_fase_item_exonerar').modal('show');
        /* }	 */
    }
  });
}
//

// SUBIDA DOCUMENTO PARA ITEM
function subida_doc_form(proceso_fase_id,proceso_fase_item_id, item_des, item_idx, pdf_is){
  $.ajax({
      type: "POST",
      url: VISTA_URL +"proceso/subida_multimedia_form.php",
      async:true,
      dataType: "html",                      
      data: ({           
          proceso_fase_item_id: proceso_fase_item_id,
          //cre_id:	$('#hdd_cre_id').val(),
          //cli_id: $('#hdd_cre_cli_id').val(),
          item_des: item_des,
          item_idx: item_idx,
          pdf_is: pdf_is //nuevo o editar
      }),
      beforeSend: function() {
      },
      success: function(html){
          $('#div_proceso_fase_item_file').html(html);   
          $('#modal_proceso_fase_item_form').modal('show');
      }
  });
}
//

// VER DOCUMENTO PARA ITEM
function ver_subida_doc_form(proceso_fase_id,proceso_fase_item_id, item_des, item_idx, pdf_is, multimedia_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/ver_subida_multimedia_form.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_fase_item_id: proceso_fase_item_id,
      multimedia_id: multimedia_id,
      item_des: item_des,
      item_idx: item_idx
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_proceso_fase_item_file_ver').html(data);
      $('#modal_proceso_fase_item_ver').modal('show');
      modal_height_auto('modal_proceso_fase_item_ver'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_proceso_fase_item_ver', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}
//

// HISTORIAL DE ITEM
function hist_doc_form(tb_proceso_fase_item_id, vista){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/historial_multimedia.php",
    async: true,
    dataType: "html",
    data: ({
      tb_proceso_fase_item_id: tb_proceso_fase_item_id,
      vista: vista
    }),
    beforeSend: function() {
    },
    success: function(data){
      $('#div_historial_doc').html(data);
      $('#modal_hist_doc_timeline').modal('show');
      modal_height_auto('modal_hist_doc_timeline'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_hist_doc_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(){
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}
//

// FORMULARIO REGISTRAR CLIENTE DESDE LA FASE 1
function cliente_form(usuario_act, cliente_id){ 

  /* GERSON 23-11-23 */
  var proceso_id = $("#hdd_proceso_id").val();
  var precliente_id = $("#hdd_precliente_id").val();
  var proceso_fase_id = $("#hdd_proc_fase_id_temp").val();
  // valores precliente/cliente
  var credito_cliente_id = cliente_id;
  if(usuario_act == 'M'){
    var credito_cliente_id = Number($('#hdd_proc_cliente_id_'+proceso_fase_id).val());
  }

  if(parseInt(credito_cliente_id) <= 0 && usuario_act == 'M'){
    alerta_warning('Información', 'Debes buscar por documento o nombres');
    return false;
  }
  console.log(credito_cliente_id);
  var cliente_dni = $("#cliente_dni_"+proceso_fase_id).val();
  var cliente_nom = $("#cliente_nom_"+proceso_fase_id).val();
  var cliente_cel = $("#cliente_cel_"+proceso_fase_id).val();
  /*  */
  
  $.ajax({
		type: "POST",
		url: VISTA_URL+"cliente/cliente_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: credito_cliente_id,
      vista: 'proceso',
      proceso_id: proceso_id,
      precliente_id: precliente_id,
      cliente_dni: cliente_dni,
      cliente_nom: cliente_nom,
      cliente_cel: cliente_cel
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_cliente_form').html(data);
      	$('#modal_registro_cliente').modal('show');

        console.log('data::'+data);

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_cliente'); //funcion encontrada en public/js/generales.js
        
        //funcion js para limbiar el modal al cerrarlo
      	modal_hidden_bs_modal('modal_registro_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_registro_cliente'); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto('modal_registro_cliente', 75); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'cliente';
      	var div = 'div_modal_cliente_form';
      	permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			console.log('Aqui listo22')
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}
//


// GUARDAR CAMBIO DE CAMPOS PARA PRE CONSTITUCION
function guardar_preconsti_fase1(proceso_fase_id) {

  var proceso_fase_id = $("#hdd_proc_fase_id_temp").val();
  var cliente_id = $("#hdd_proc_cliente_id_"+proceso_fase_id).val();
  var cliente_id_ori = $("#hdd_proc_cliente_id_ori_"+proceso_fase_id).val();
  var cliente_nom = $("#cliente_nom_"+proceso_fase_id).val();
  var cliente_cel = $("#cliente_cel_"+proceso_fase_id).val();
  var fecha_inspeccion = $("#fecha_inspeccion").val();
  var fecha_deposito = $("#fecha_deposito").val();
  var inicial = $("#inicial_"+proceso_fase_id).val();
  var inicial_est = $('input[name="inicial_est_'+proceso_fase_id+'"]:checked').val();

  if(cliente_id=='0' || cliente_id=='' || cliente_id==null){
    Swal.fire(
      '¡Aviso!',
      'Seleccione un cliente.',
      'warning'
    );
    $("#cliente_nom_"+proceso_fase_id).focus();
  }else{
    
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
      action: 'guardar_preconsti_fase1',
      proceso_fase_id: proceso_fase_id,
      cliente_id: cliente_id, 
      cliente_id_ori: cliente_id_ori,
      cliente_nom: cliente_nom,
      cliente_cel: cliente_cel,
      fecha_inspeccion: fecha_inspeccion,
      fecha_deposito: fecha_deposito,
      inicial: inicial,
      inicial_est: inicial_est,
    }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado == 1){
            //notificacion_success(data.mensaje, 3000);
          }else{
            notificacion_warning(data.mensaje, 3000);
          }
        },
        complete: function (data) {
        }
    });
    
  }

}
//

// GUARDAR CAMBIO DE CAMPOS PARA CONSTITUCION
function guardar_consti_fase1(proceso_fase_id, tipo_garv) {

  var proceso_fase_id = $("#hdd_proc_fase_id_temp").val();
  var cliente_id = $("#hdd_proc_cliente_id_"+proceso_fase_id).val();
  var cliente_id_ori = $("#hdd_proc_cliente_id_ori_"+proceso_fase_id).val();
  var cliente_nom = $("#cliente_nom_"+proceso_fase_id).val();
  var cliente_cel = $("#cliente_cel_"+proceso_fase_id).val();
  var inspeccion_vehiculo = $("#inspeccion_vehiculo_const_sc").val();
  var prueba_vehiculo = $('input[name="prueba_vehiculo_const_sc"]:checked').val();
  var cliente_acepta = $('input[name="cliente_acepta_const_sc"]:checked').val();
  var cliente_acepta_des = $("#cliente_acepta_des_const_sc").val();

  if(cliente_id=='0' || cliente_id=='' || cliente_id==null){
    Swal.fire(
      '¡Aviso!',
      'Seleccione un cliente.',
      'warning'
    );
    $("#cliente_nom_"+proceso_fase_id).focus();
  }else{
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
      action: 'guardar_consti_fase1',
      proceso_fase_id: proceso_fase_id,
      tipo_garv: tipo_garv,
      cliente_id: cliente_id, 
      cliente_id_ori: cliente_id_ori,
      cliente_nom: cliente_nom,
      cliente_cel: cliente_cel,
      inspeccion_vehiculo: inspeccion_vehiculo,
      prueba_vehiculo: prueba_vehiculo,
      cliente_acepta: cliente_acepta,
      cliente_acepta_des: cliente_acepta_des,
    }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado == 1){
            //notificacion_success(data.mensaje, 3000);
            console.log('datos actualizados correctamente - supuestamente');
          }else{
            notificacion_warning(data.mensaje, 3000);
          }
        },
        complete: function (data) {
        }
    });
    
  }

}
//

function siguienteFase(proceso_id, proceso_fase_id, orden) {

  var inspeccion_vehiculo = $("#inspeccion_vehiculo_const_sc").val();
  var prueba_vehiculo = $('input[name="prueba_vehiculo_const_sc"]:checked').val();
  var cliente_acepta = $('input[name="cliente_acepta_const_sc"]:checked').val();
  var cliente_acepta_des = $("#cliente_acepta_des_const_sc").val();

  Swal.fire({
    title: '¿SEGURO QUE DESEA PASAR A LA SIGUIENTE FASE?',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
    }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_controller.php",
        async: false,
        dataType: "json",
        data: ({
          action: 'pasar_siguiente_fase',
          proceso_id: proceso_id,
          proceso_fase_id: proceso_fase_id,
          orden: orden,
          inspeccion_vehiculo: inspeccion_vehiculo,
          prueba_vehiculo: prueba_vehiculo,
          cliente_acepta: cliente_acepta,
          cliente_acepta_des: cliente_acepta_des
        }),
        beforeSend: function () {
          $('#fase_'+proceso_fase_id+'_orden_'+orden).prop('disabled', true);
        },
        success: function (data) {
          // Guardar cambios fase 1
          if(parseInt(orden)==1){
            if(data.tipo_garv==1 || data.tipo_garv==6){
              guardar_preconsti_fase1(data.proceso_fase_id);
            }
            if(data.tipo_garv==2 || data.tipo_garv==3 || data.tipo_garv==4 || data.tipo_garv==5){
              console.log('debe guardar los datoooooosss');
              guardar_consti_fase1(data.proceso_fase_id, data.tipo_garv);
            }
          }
          //
          if(data.estado==1){
            notificacion_success(data.mensaje, 3000);
            verFaseProceso('editar', proceso_id, data.usuario_id);
          }else if(data.estado==0){
            Swal.fire(
              'Alerta!',
              data.mensaje,
              'warning'
            );
          }else{
            Swal.fire(
              'Alerta!',
              'No se puede pasar a la siguiente fase.',
              'warning'
            );
          }
        },
        complete: function (data) {
          $('#fase_'+proceso_fase_id+'_orden_'+orden).prop('disabled', false);
        }
      });
    }
  });

}

function votoCom(name, value, usuario_id, sesion_usuario, proceso_id) {

  if(usuario_id!==sesion_usuario){

    // control para comité
    Swal.fire(
      'Alerta!',
      'No puedes modificar una votación de otro miembro.',
      'warning'
    );
    document.querySelector('#'+name+'_si').checked = false;
    document.querySelector('#'+name+'_no').checked = true;

  }else{

    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
        action: 'votacion_comite',
        usuario_id: usuario_id,
        proceso_id: proceso_id,
        value: value,
      }),
      success: function (data) {
        if(data.estado==1){
          notificacion_success(data.mensaje, 3000);
        }else{
          Swal.fire(
            'Alerta!',
            'No se puede registrar la votación.',
            'warning'
          );
        }
      },
      complete: function (data) {
      }
    });

  }

}

function votoComiteOtro(value, usuario_id, proceso_id) {

  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async: false,
    dataType: "json",
    data: ({
      action: 'votacion_comite_otro',
      usuario_id: usuario_id,
      proceso_id: proceso_id,
      value: value,
    }),
    success: function (data) {
      if(data.estado==1){
        notificacion_success(data.mensaje, 3000);
      }else{
        Swal.fire(
          'Alerta!',
          'No se puede registrar el cambio.',
          'warning'
        );
      }
    },
    complete: function (data) {
    }
  });

}

// BANCARIZACION
function elegirBancarizacion(proceso_id, usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/bancarizacion.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_bancarizacion').html(data);
      $('#modal_bancarizacion').modal('show');
      modal_height_auto('modal_bancarizacion'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_bancarizacion', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function registrarBanca(){

  var banca = parseInt($('#cmb_banca_id').val());
  var proceso_id = parseInt($('#hdd_proceso_id').val());

  if(banca==0){
    Swal.fire(
      '¡Aviso!',
      'Seleccione un opción.',
      'warning'
    );
  }else{
    
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
      action: 'guardar_bancarizacion',
      proceso_id: proceso_id,
      banca: banca
    }),
        beforeSend: function () {
          $('#btn_guardar_bancarizacion').prop('disabled', true);
        },
        success: function (data) {
          if(data.estado == 1){
            notificacion_success(data.mensaje, 3000);
            $("#modal_bancarizacion").modal('hide');
          }else{
            notificacion_warning(data.mensaje, 3000);
          }
        },
        complete: function (data) {
          $('#btn_guardar_bancarizacion').prop('disabled', false);
        }
    });
    
  }
}
//

// CHEQUES
function generarCheche(proceso_id, usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/generar_cheque.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_generar_cheque').html(data);
      $('#modal_generar_cheque').modal('show');
      modal_height_auto('modal_generar_cheque'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_generar_cheque', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function generarCheque(){
  var cant_cheque = $("#cant_cheque").val();
  if(cant_cheque=='' || cant_cheque==null){
    notificacion_warning("Ingrese el número de cheques a generar", 3000);
  }else{
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async:true,
      dataType: "html",                      
      data: ({
        action: 'detalle_cheque',
        cant_cheque: cant_cheque,
        garvtipo_id: $("#hdd_tipogarantia_id").val(),
        proceso_id: $("#hdd_proceso_id").val(),
        usuario_id: $("#hdd_usuario_id").val()
      }),
      beforeSend: function() {
      },
      success: function(html){
        $('#div_cheques').html(html);  
      }
    });
  }
  
}

function generarChequeExcel(proceso_id, usuario_id) {
  window.open(VISTA_URL + "proceso/generar_cheque_excel.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id);
}
//

// SIMULADOR DE CUOTAS
function generarSimulador(proceso_id, usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/generar_simulador.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_generar_simulador').html(data);
      $('#modal_generar_simulador').modal('show');
      modal_height_auto('modal_generar_simulador'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_generar_simulador', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function obtener_valor_seguro_gps(){

  var cuota = $("#simu_cuota").val();

  if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else{

    var numero_cuotas = parseInt(cuota);
    if(numero_cuotas <= 0)
      return false;

    $.ajax({
      type: "POST",
      url: VISTA_URL + "segurogps/segurogps_controller.php",
      async: true,
      dataType: "json",
      data: {
        action: "valores",
        numero_cuotas: numero_cuotas
      },
      beforeSend: function () {
      },
      success: function (data) {
        console.log(data);
        $('#hdd_seguro_porcentaje').val(data.valor_seguro);
        $('#hdd_gps_precio').val(data.valor_gps);
        $('#span_seguro_gps').val(data.texto);
        calcularSimulacion();
      },
      complete: function (data) {},
    });

  }
  
}

function guardarSimulador(id, value, tipo_dato) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async: false,
    dataType: "json",
    data: ({
      action: 'guardar_campo_simulador',
      proceso_id: $("#hdd_simu_proceso_id").val(),
      id: id, 
      value: value,
      tipo_dato: tipo_dato,
    }),
      beforeSend: function () {
      },
      success: function (data) {
        if(data.estado == 1){
          notificacion_success(data.mensaje, 3000);
        }else{
          notificacion_warning(data.mensaje, 3000);
        }
      },
      complete: function (data) {
      }
  });

}

function calcularSimulacion(){

  var chk_gps = 0;
  if($('input[name="chk_gps"]:checked').val()){
    chk_gps = 1;
  }

  var tipo_gar = parseInt($("#hdd_tipo_gar").val());

  var fecha = $("#simu_fecha").val();
  var moneda = $("#simu_moneda").val();
  var cambio = $("#simu_cambio").val();
  var cuota = $("#simu_cuota").val();
  var interes = $("#simu_interes").val();
  var costo = $("#simu_costo").val();
  var inicial = $("#simu_inicial").val();
  var valor = $("#simu_valor").val();

  if(tipo_gar != 2 && tipo_gar != 3 && tipo_gar != 6){
    if(inicial=='' ){ // || parseFloat(inicial)==0){
      notificacion_warning("Ingrese el porcentaje de inicial.", 3000);
      return;
    }
  }

  if(cambio=='' || parseFloat(cambio)==0){
    notificacion_warning("Ingrese el tipo de cambio.", 3000);
  }else if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else if(interes=='' || parseFloat(interes)==0){
    notificacion_warning("Ingrese el monto de interés.", 3000);
  }else if(costo=='' || parseFloat(costo)==0){
    notificacion_warning("Ingrese el costo de vehículo.", 3000);
  }else if(valor=='' || parseFloat(valor)==0){
    notificacion_warning("Ingrese el valor del préstamo.", 3000);
  }else{

    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async:true,
      dataType: "html",                      
      data: ({
        action: 'detalle_simulacion',
        proceso_id: parseInt($('#hdd_simu_proceso_id').val()),
        usuario_id: parseInt($('#hdd_simu_usuario_id').val()),
        fecha: fecha,
        moneda: moneda,
        cambio: cambio,
        cuota: cuota,
        interes: interes,
        costo: costo,
        inicial: inicial,
        valor: valor,
        chk_gps: chk_gps,
        seguro_porcentaje: $('#hdd_seguro_porcentaje').val(),
        gps_precio: $('#hdd_gps_precio').val(),
        span_seguro_gps: $('#span_seguro_gps').val(),
      }),
      beforeSend: function() {
      },
      success: function(html){
        $('#div_simulacion').html(html);  
      }
    });

  }
  
}

function generarSimuladorExcelPDF(proceso_id, usuario_id, tipo) {

  var chk_gps = 0;
  if($('input[name="chk_gps"]:checked').val()){
    chk_gps = 1;
  }

  var tipo_gar = parseInt($("#hdd_tipo_gar").val());

  var fecha = $("#simu_fecha").val();
  var moneda = $("#simu_moneda").val();
  var cambio = $("#simu_cambio").val();
  var cuota = $("#simu_cuota").val();
  var interes = $("#simu_interes").val();
  var costo = $("#simu_costo").val();
  var inicial = $("#simu_inicial").val();
  var valor = $("#simu_valor").val();

  var seguro_porcentaje = $('#hdd_seguro_porcentaje').val();
  var gps_precio        = $('#hdd_gps_precio').val();
  var span_seguro_gps   = $('#span_seguro_gps').val();

  if(tipo_gar != 2 && tipo_gar != 3 && tipo_gar != 6){
    if(inicial=='' ){ // || parseFloat(inicial)==0){
      notificacion_warning("Ingrese el monto de inicial.", 3000);
      return;
    }
  }

  if(cambio=='' || parseFloat(cambio)==0){
    notificacion_warning("Ingrese el tipo de cambio.", 3000);
  }else if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else if(interes=='' || parseFloat(interes)==0){
    notificacion_warning("Ingrese el monto de interés.", 3000);
  }else if(costo=='' || parseFloat(costo)==0){
    notificacion_warning("Ingrese el costo de vehículo.", 3000);
  }else if(valor=='' || parseFloat(valor)==0){
    notificacion_warning("Ingrese el valor del préstamo.", 3000);
  }else{

    //window.open("http://www.ipdnsac.com/ipdnsac/vista/cuenta/cuenta_reporte_excel.php?fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&cuenta_tipo=" + cuenta_tipo + "&sede=" + sede);
    if(tipo=='excel'){
      window.open(VISTA_URL + "proceso/generar_simulador_excel.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&fecha="+fecha+"&moneda="+moneda+"&cambio="+cambio+"&cuota="+cuota+"&interes="+interes+"&costo="+costo+"&inicial="+inicial+"&valor="+valor+"&seguro_porcentaje="+seguro_porcentaje+"&gps_precio="+gps_precio+"&span_seguro_gps="+span_seguro_gps+"&chk_gps="+chk_gps);
    }else{
      window.open(VISTA_URL + "proceso/generar_simulador_pdf.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&fecha="+fecha+"&moneda="+moneda+"&cambio="+cambio+"&cuota="+cuota+"&interes="+interes+"&costo="+costo+"&inicial="+inicial+"&valor="+valor+"&seguro_porcentaje="+seguro_porcentaje+"&gps_precio="+gps_precio+"&span_seguro_gps="+span_seguro_gps+"&chk_gps="+chk_gps);
    }
  }
}
//

// HOJA PRECALIFICACION
function hojaPrecalificacion(accion, proceso_id, usuario_id, proceso_fase_item_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/hoja_precalificacion.php",
    async: true,
    dataType: "html",
    data: ({
      accion: accion,
      proceso_id: proceso_id,
      usuario_id: usuario_id,
      proceso_fase_item_id: proceso_fase_item_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_hoja_precalificacion').html(data);
      $('#modal_hoja_precalificacion').modal('show');
      modal_height_auto('modal_hoja_precalificacion'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_hoja_precalificacion', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

// EXPORT PFD HOJA DE PRECALIFICACION
function exportarHojaPDF(proceso_id, accion) {
	window.open("vista/proceso/hoja_precalificacion_pdf.php?proceso_id="+proceso_id+"&accion="+accion,"_blank");
}

// INGRESO Y EGRESOS
function ingresosEgresos(accion, proceso_id, usuario_id, proceso_fase_item_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/ingresos_egresos.php",
    async: true,
    dataType: "html",
    data: ({
      accion: accion,
      proceso_id: proceso_id,
      usuario_id: usuario_id,
      proceso_fase_item_id: proceso_fase_item_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_ingresos_egresos').html(data);
      $('#modal_ingresos_egresos').modal('show');
      modal_height_auto('modal_ingresos_egresos'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_ingresos_egresos', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function exportarIEExcel(proceso_id, usuario_id) {
  window.open(VISTA_URL + "proceso/ingresos_egresos_excel.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id);
}

function detalleParticipante(){
  
	$.ajax({
		type: "POST",
		url: VISTA_URL + "proceso/proceso_controller.php",
		async:true,
		dataType: "html",                      
		data: ({
		  action: 'detalle_participante_cliente',
		  proceso_id: parseInt($('#hdd_proceso_id').val()),
		}),
		beforeSend: function() {
		},
		success: function(html){

			$('#listaParticipante').html(html);  
			
		}
	});
  
}

function guardarParticipanteDetalle(form, type, prefijo) {
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/proceso_controller.php",
		async: true,
		dataType: "json",
		data: $("#"+form).serialize(),
		beforeSend: function() {
		},
		success: function(data){
			if(data.estado==1){
				notificacion_success(data.mensaje, 3000);
        detalleParticipante();
        $('#divNuevoParticipante').addClass('hidden'); $('#frmNuevoParticipante').trigger('reset'); $('#rowPlusParticipante').removeClass('hidden');
			}else{
				notificacion_warning(data.mensaje, 3000);
			}
		},
		complete: function(data){
		},
			
	});
}

function eliminar_participante(participante_id, usuario_id, usuario_id_session, type){

  if(usuario_id !== usuario_id_session){
    notificacion_warning('Los participantes solo puedden ser eliminado por el responsable del proceso.', 3000);
  }else{
    Swal.fire({
      title: '¿SEGURO QUE DESEA ELIMINAR PARTICIPANTE?',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
      showDenyButton: true,
      denyButtonText: '<i class="fa fa-band"></i> Cancelar',
      confirmButtonColor: '#3b5998', 
      denyButtonColor: '#DD3333',
      }).then((result) => {
    
      if (result.isConfirmed) {
  
        $.ajax({
          type: "POST",
          url: VISTA_URL + "proceso/proceso_controller.php",
          async: false,
          dataType: "json",
          data: ({
            action: 'eliminar_participante',
            participante_id: participante_id
          }),
          beforeSend: function () {
          },
          success: function (html) {
            detalleParticipante();
            notificacion_success('Registro eliminado correctamente.', 2000);
          },
        });
      }
    });
  }
	
}

function limpiarCamposParticipante(){
	$("#nombre_p").val('');
	$("#documento_p").val('');
	$("#celular_p").val('');
}

function creditogarveh_form(usuario_act, creditogarveh_id, proceso_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarveh/creditogarveh_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          creditogarveh_id: creditogarveh_id,
          proceso_id: proceso_id,
          vista: 'credito_tabla'
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
//            console.log(data);
          $('#modal_mensaje').modal('hide');
          if (data != 'sin_datos') {
              $('#div_modal_creditogarveh_form').html(data);
              $('#modal_registro_creditogarveh').modal('show');

              //desabilitar elementos del form si es L (LEER)
              if (usuario_act == 'L'){
                  form_desabilitar_elementos('for_cre');
              } //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_creditogarveh', 'limpiar'); //funcion encontrada en public/js/generales.js
              modal_width_auto('modal_registro_creditogarveh',80);
              modal_height_auto('modal_registro_creditogarveh');
          } else {
              //llamar al formulario de solicitar permiso
              var modulo = 'creditogarveh';
              var div = 'div_modal_creditogarveh_form';
              permiso_solicitud(usuario_act, creditogarveh_id, modulo, div); //funcion ubicada en public/js/permiso.js
          }
      },
      complete: function (data) {
//console.log(data);
      },
      error: function (data) {
          $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
          $('#overlay_modal_mensaje').removeClass('overlay').empty();
          $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
          console.log(data.responseText);
      }
  });
}

function adenda_form(action, cre_id, proceso_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/adenda_form.php",
    async:true,
    dataType:"html",                   
    data:({
      action: action,
      cre_id: cre_id, //tipo d credito 3: garveh
      proceso_id: proceso_id
    }),
    beforeSend: function() {
      //$('#modal_creditogarveh_anular_acuerdopago').modal('show');
    },
    success: function(data){
      
      $('#div_modal_adenda_form').html(data);  
      $('#modal_adenda_form').modal('show');
      modal_height_auto('modal_adenda_form');
    },
    complete: function(data){     
      //console.log(data);
    }
  });
}

function credito_inicial_form() {
  var pre_veh = Number($("#txt_veh_pre").autoNumeric("get"));
  //console.log(pre_veh);
  if (pre_veh <= 0) {
    alert("Ingrese primero el precio del vehículo por favor.");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/credito_inicial_form.php",
    async: true,
    dataType: "html",
    data: {},
    beforeSend: function () {
      //$('#modal_creditogarveh_anular_acuerdopago').modal('show');
    },
    success: function (data) {
      ////console.log(data);
      $("#div_modal_creditogarveh_inicial").html(data);
      $("#modal_creditogarveh_inicial").modal("show");
    },
    complete: function (data) {
      ////console.log(data);
    },
  });
}

// SUBIDA DE CHEQUES 
function subidaCheques(proceso_id, usuario_id, proceso_fase_item_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/subida_cheque.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      usuario_id: usuario_id,
      proceso_fase_item_id: proceso_fase_item_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_subida_cheque').html(data);
      $('#modal_subida_cheque').modal('show');
      modal_height_auto('modal_subida_cheque'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_subida_cheque', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function guardarNumeroCheque(proceso_id, usuario_id, chequedetalle_id, proceso_fase_item_id, pos) {
  if($("#num_cheque"+pos).val() === '' || $("#num_cheque"+pos).val() === null){
    notificacion_warning("Faltan datos del cheque.", 3000);
  }else{
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
        action: 'guardar_datos_chequedetalle',
        proceso_id: proceso_id,
        usuario_id: usuario_id, 
        proceso_fase_item_id: proceso_fase_item_id,
        chequedetalle_id: chequedetalle_id,
        num_cheque: $("#num_cheque"+pos).val(),
        empresa_id: $("#cmb_empresa_id"+pos).val(),
        uso: $("#cmb_uso"+pos).val(),
        fecha_subida: $("#fecha_subida"+pos).val(),
      }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado == 1){
            notificacion_success(data.mensaje, 3000);
            subidaCheques(proceso_id, usuario_id);
          }else{
            notificacion_warning(data.mensaje, 3000);
          }
        },
        complete: function (data) {
        }
    });
  }
  
}

function aprobarCheques(proceso_id, usuario_id, cheque_id, proceso_fase_item_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async: false,
    dataType: "json",
    data: ({
      action: 'guardar_aprobacion_cheque',
      proceso_id: proceso_id,
      usuario_id: usuario_id, 
      proceso_fase_item_id: proceso_fase_item_id,
      cheque_id: cheque_id,
      aprobado: $('input[name="r_aprobado"]:checked').val(),
    }),
      beforeSend: function () {
      },
      success: function (data) {
        if(data.estado == 1){
          notificacion_success(data.mensaje, 3000);
          subidaCheques(proceso_id, usuario_id);
        }else{
          if(data.estado == 2){
            notificacion_warning(data.mensaje, 3000);
          }
          if(data.estado == 3){
            subidaCheques(proceso_id, usuario_id);
            notificacion_warning(data.mensaje, 3000);
          }
          if(data.estado == 4){
            subidaCheques(proceso_id, usuario_id);
            notificacion_warning(data.mensaje, 3000);
          }
        }
      },
      complete: function (data) {
      }
  });
  
}
//

//
function credito_desembolso_form(act,idf, proceso_fase_item_id,banca){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_desembolso_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: act,
			cre_id:	idf,
			banca:	banca,
			proceso_fase_item_id:	proceso_fase_item_id,
			vista:	'proceso'
		}),
		beforeSend: function() {
    },
		success: function(data){
			if(data != 'sin_datos'){
        $('#div_credito_desembolso_form').html(data);
        modal_width_auto('modal_creditogarveh_desembolso',80);
        modal_height_auto('modal_creditogarveh_desembolso');
        $('#modal_creditogarveh_desembolso').modal('show');
        modal_hidden_bs_modal('modal_creditogarveh_desembolso', 'limpiar'); //funcion encontrada en public/js/generales.js
      }	
		},
    complete: function(data){
    }
	});
}

function documento_desembolso(id_egr, empresa_id)
{
    var codigo = id_egr;
    window.open("http://ipdnsac.com/ipdnsac/vista/egreso/egreso_ticket.php?egreso_id=" + codigo + "&empresa_id=" + empresa_id);
    //window.open("http://localhost/ipdnsac/vista/egreso/egreso_ticket.php?egreso_id="+codigo+"&empresa_id="+empresa_id);
}

function comprimirArchivos(proceso_id) {
  var items = JSON.parse($("#items_comprimidos").val());

  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async: false,
    dataType: "json",
    data: ({
      action: 'comprimir_archivos',
      proceso_id: proceso_id,
      items: items
    }),
      beforeSend: function () {
      },
      success: function (data) {
        if(data.estado == 1){
          notificacion_success(data.mensaje, 3000);
          //document.getElementById('downloadZip').setAttribute('href', 'http://ipdnsac.com/ipdnsac/vista/proceso/test.zip');
          document.getElementById('downloadZip').setAttribute('href', 'vista/proceso/'+data.archivo);
          document.getElementById("downloadZip").click();
          limpiarComprimido(data.archivo);
        }else{
          notificacion_warning(data.mensaje, 3000);
        }
      },
      complete: function (data) {
        
      }
  });
  
}

function limpiarComprimido(archivo) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async: false,
    dataType: "json",
    data: ({
      action: 'limpiar_comprimido',
      archivo: archivo,
    }),
      beforeSend: function () {
      },
      success: function (data) {
        
      },
      complete: function (data) {
      }
  });
  
}

function comentarioPreCalificacion(proceso_id, usuario_id, proceso_fase_item_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/comentario_precalificacion.php",
		async:true,
		dataType: "html",                      
		data: ({
			proceso_id: proceso_id,
			usuario_id: usuario_id,
      proceso_fase_item_id: proceso_fase_item_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_comentario_precalificacion').html(html);		
      $('#modal_comentario_precalificacion').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_comentario_precalificacion', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_comentario_precalificacion'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

function titulo_form(cre_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "titulo/titulo_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cre_tip:	3, //3 garveh
			cre_id:	cre_id
		}),
		beforeSend: function() {
			//$('#div_titulo_form').dialog("open");
			//$('#div_titulo_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_titulo_form').html(data);
        modal_width_auto('modal_titulo',80);
        modal_height_auto('modal_titulo');
        $('#modal_titulo').modal('show');
        modal_hidden_bs_modal('modal_titulo', 'limpiar'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

      }	
		}
	});
}

/* GERSON (11-01-24) */
function btnFinalizarProceso(proceso_id) {

  Swal.fire({
    title: '¿DESEA FINALIZAR EL PROCESO?',
    icon: 'success',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
    }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_controller.php",
        async: false,
        dataType: "json",
        data: ({
          action: 'finalizar_proceso',
          proceso_id: proceso_id
        }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado==1){
            notificacion_success(data.mensaje, 3000);
            verPaginaproceso('1'); // volvemos a traer el listado de procesos
          }else{
            Swal.fire(
              'Alerta!',
              'No se puede dar por finalizado el proceso.',
              'warning'
            );
          }
        },
        complete: function (data) {
        }
      });
    }
  });

}

function btnAnularProceso(proceso_id, accion){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/anular_proceso.php",
		async:true,
		dataType: "html",                      
		data: ({
			proceso_id: proceso_id,
      accion: accion // R=Registro, L=Lectura
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_anular_proceso').html(html);		
      $('#modal_anular_proceso').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_anular_proceso', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_anular_proceso'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

function btnEliminarProceso(proceso_id) {

  Swal.fire({
    title: '¿DESEA ELIMINAR EL PROCESO?',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
    }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_controller.php",
        async: false,
        dataType: "json",
        data: ({
          action: 'eliminar_proceso',
          proceso_id: proceso_id
        }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado==1){
            notificacion_success(data.mensaje, 3000);
            verPaginaproceso('1'); // volvemos a traer el listado de procesos
          }else{
            Swal.fire(
              'Alerta!',
              'No se puede eliminar el proceso.',
              'warning'
            );
          }
        },
        complete: function (data) {
        }
      });
    }
  });

}

function btnEliminarCheque(cheque_id, proceso_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/eliminar_cheque.php",
		async:true,
		dataType: "html",                      
		data: ({
			cheque_id: cheque_id,
			proceso_id: proceso_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_eliminar_cheque').html(html);		
      $('#modal_eliminar_cheque').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_eliminar_cheque', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_eliminar_cheque'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}
/*  */

function transferente_form(usuario_act, transferente_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "transferente/transferente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      transferente_id: transferente_id,
      vista: "creditogarveh",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      ////console.log(data);
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_transferente_form").html(data);
        $("#modal_registro_transferente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_transferente"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_transferente", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "transferente";
        var div = "div_modal_transferente_form";
        permiso_solicitud(usuario_act, transferente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function verHistorialPagos(cliente_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proceso/historial_pago_cuota.php",
		async:true,
		dataType: "html",                      
		data: ({
			cliente_id: cliente_id,
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_pago_cuotas_cliente').html(html);		
      $('#modal_pago_cuotas_cliente').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_pago_cuotas_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_pago_cuotas_cliente'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

/* GERSON (03-01-25) */
// REFERENCIAS DE VALORACION
function refValoracion(accion, proceso_id, usuario_id, proceso_fase_item_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/referencia_valoracion.php",
    async: true,
    dataType: "html",
    data: ({
      accion: accion,
      proceso_id: proceso_id,
      usuario_id: usuario_id,
      proceso_fase_item_id: proceso_fase_item_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_ref_valoracion').html(data);
      $('#modal_ref_valoracion').modal('show');
      modal_height_auto('modal_ref_valoracion'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_ref_valoracion', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}
/*  */

function estilos_datatable() {
  datatable_global = $('#tabla_lista_procesos').DataTable({
      "pageLength": 50,
      "responsive": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
      order: [],
      columnDefs: [
          { targets: [7], orderable: false }
      ]
  });

  //datatable_texto_filtrar();
}

