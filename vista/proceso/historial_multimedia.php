<?php
	session_name("ipdnsac");
  session_start();
	require_once('../historial/Historial.class.php');
  $oHistorial = new Historial();
  require_once('../funciones/fechas.php');
  require_once('../comentario/Comentario.class.php');
  $oComentario = new Comentario();
  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();

  $tb_proceso_fase_item_id = intval($_POST['tb_proceso_fase_item_id']);
  $vista = $_POST['vista'];
  $lista = '';
  $proceso_id = 0;
  $item_id = 0;

  $proceso_fase_item = $oProceso->mostrarUnoProcesoFaseItem($tb_proceso_fase_item_id);
  if($proceso_fase_item['estado'] == 1){
    $item_id = $proceso_fase_item['data']['tb_item_id'];
    $proceso_fase = $oProceso->mostrarUnoProcesoFase($proceso_fase_item['data']['tb_proceso_fase_id']);
    if($proceso_fase['estado'] == 1){
      $proceso_id = intval($proceso_fase['data']['tb_proceso_id']);
    }
  }

  //if($proceso_fase_item['estado'] == 1){
    // Al realizar la actualización en cuanto a items que se repetían en las fases y no se repitan registros, estos al manejarse por el 'tb_proceso_fase_item_id' 
    // solo aparecían una vez en la primera fase que apareciera y si luego se repetía ya no aparecía el historial.
    // Se procedió a modificar la consulta que se guíe del 'item_id' que es el dato que comparten.
    //$result = $oHistorial->filtrar_historial_proceso_item('tb_proc_proceso_fase_item', 'tb_proc_precalificacion', 'tb_proc_ingresoegreso', 'tb_proc_chequedetalle', 'tb_proc_chequeretiro', $tb_proceso_fase_item_id, $proceso_fase_item['data']['tb_item_id']);
  //}else{
    // Consulta antigua de historial
    $result = $oHistorial->filtrar_historial_proceso_item('tb_proc_proceso_fase_item', 'tb_proc_precalificacion', 'tb_proc_ingresoegreso', 'tb_proc_chequedetalle', 'tb_proc_chequeretiro', 'tb_proc_valoracion', $tb_proceso_fase_item_id);
  //}

  /* GERSON (09-01-24) */
  if($vista == 'tb_proc_proceso_fase_item'){ // Si la tabla encontrada es 'tb_proc_proceso_fase_item' se trata de los items
    // Se procede hacer otra busqueda ahora con el 'proceso_id' y 'item_id'
    $result1 = $oHistorial->filtrar_historial_item('tb_proc_proceso_fase_item', $proceso_id, $item_id); // ignoramos el 'proceso_fase_item_id' para mostrar el historial en cualquier fase
    if($result1['estado'] == 1){
      $result = null;
      $result = $result1;
    }
  }
    
  /*  */
  
  // alt + shift + a para comentar y descomentar en bloque  --- ctrl k c : para comentar con barras, ctrl k u: para descomentar con barras.
  if($result['estado'] == 1){
    
    foreach ($result['data'] as $key => $value) {
      $icono = 'fa-file';
      $background = 'bg-blue';
      $detalle = explode(' ', $value['tb_hist_det']);
      if($detalle[1] == 'exonerado'){
        $icono = 'fa-check-square';
      }
      elseif($detalle[1] == 'eliminado'){
        $background = 'bg-red';
        if($detalle[3] != 'subida'){
          $icono = 'fa-check-square';
        }
      }
      elseif($value['tb_hist_nom_tabla'] == 'tb_proc_precalificacion' || $value['tb_hist_nom_tabla'] == 'tb_proc_ingresoegreso' || $value['tb_hist_nom_tabla'] == 'tb_proc_chequedetalle' || $value['tb_hist_nom_tabla'] == 'tb_proc_chequeretiro' || $value['tb_hist_nom_tabla'] == 'tb_proc_valoracion'){
        $icono = 'fa-file-text-o';
      }
      $lista .='
                <li>
                  <i class="fa '.$icono.' '.$background.'"></i>
                  <input name="hist_id" id="hist_id" type="hidden" value="'.$value['tb_hist_id'].'">
                  <div class="timeline-item">
                    <div class="user-block">
                      <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="user image">
                      <span class="username">
                        <a href="#">'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</a>
                      </span>
                      <span class="description">'.$value['tb_hist_det'].' <i class="fa fa-clock-o"></i></span>
                    </div>
                    <div class="timeline-body">';
      $result1 = $oComentario->filtrar_comentarios_por($value['tb_hist_id'], 'tb_hist');
      $lista .= '<input type="hidden" id="cantidad_comentarios-'.$value['tb_hist_id'].'" value="'.intval($result1['cantidad']).'">';
      if($result1['estado'] == 1){
        $lista .= '<div class="box-footer box-comments" id="div_comentarios_anteriores-'.$value['tb_hist_id'].'">';
        foreach($result1['data'] as $key1 => $value1){
          $lista .= '
                            <div class="box-comment" id="com-'.$value1['tb_coment_id'].'">
                              <img class="img-circle img-sm" src="'.$value1['tb_usuario_fot'].'" alt="User Image">
                              <div class="comment-text">
                                <span class="username">
                                '.$value1['tb_usuario_nom'].' '.$value1['tb_usuario_ape'];
                    if(intval($_SESSION['usuarioperfil_id']) == 1){
                      $lista .='<button type="button" class="close" data-dismiss="alert" aria-hidden="true" title="Eliminar comentario" onclick="comentario_eliminar('.$value1['tb_coment_id'].','.$value['tb_hist_id'].')">x</button>';
                    }
                      $lista .='</span>'.$value1['tb_coment_det'].'
                                <span class="text-muted pull-right">'. date("h:i a, j-m-y", strtotime($value1['tb_coment_fecreg'])) .'</span>
                              </div>
                            </div>
          ';
        }
        $lista .= '</div>';
      }
             $lista .= '<div class="box-footer" id="div_comentar-'.$value['tb_hist_id'].'">
                            <img class="img-responsive img-circle img-sm" src="'.$_SESSION['usuario_fot'].'" alt="Alt Text">
                            <div class="img-push">
                              <input type="text" class="form-control input-sm" id="inp_comentario-'.$value['tb_hist_id'].'" placeholder="Escribe y presiona Enter para postear un comentario" onclick=action_click(this)>
                            </div>
                        </div>

                      </div>
                  </div>
              </li>
      ';
    }
  }
  else{
    $lista = 'SIN HISTORIAL PARA EL ITEM SELECCIONADO';
  }
  $result = NULL;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_hist_doc_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <input type="hidden" id="tb_proceso_fase_item_id" value="<?php echo $tb_proceso_fase_item_id; ?>">
      <input type="hidden" id="iteeeemm_id" value="<?php echo $item_id; ?>">
      <input type="hidden" id="proceeeesooo_id" value="<?php echo $proceso_id; ?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Historial del documento</h4>
      </div>
      <div class="modal-body">
        <ul class="timeline">
				  <?php echo $lista;?>
				</ul>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/proceso/historial_multimedia.js?ver=03082023'; ?>"></script>