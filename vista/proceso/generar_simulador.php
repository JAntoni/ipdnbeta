<?php
	session_name("ipdnsac");
  session_start();
	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();

  require_once('../funciones/funciones.php');
	require_once ("../funciones/fechas.php");

  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);

  $vista = 'procesos';

  $moneda             = "";
  $tipo_cambio        = "";
  $cuotas             = "";
  $tasa               = "";
  $costo_vehiculo     = "";
  $inicial_porcentaje = "";
  $valor_prestamo     = "";
  $tipo_gar           = 0;

  if($proceso_id>0){ // proveniente de procesos

    $proceso = $oProceso->mostrarUno($proceso_id);
    $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
    if($proceso['estado']==1 && $precalificacion['estado']==1){

      $moneda             = $proceso['data']['tb_proceso_moneda'] ? $proceso['data']['tb_proceso_moneda'] : ($precalificacion['data']['tb_precalificacion_moneda_credito'] ? $precalificacion['data']['tb_precalificacion_moneda_credito'] : NULL);
      $tipo_cambio        = $proceso['data']['tb_proceso_tip_cambio'] ? floatval($proceso['data']['tb_proceso_tip_cambio']) : NULL;
      $cuotas             = $proceso['data']['tb_proceso_plazo'] ? intval($proceso['data']['tb_proceso_plazo']) : ($precalificacion['data']['tb_precalificacion_plazo'] ? intval($precalificacion['data']['tb_precalificacion_plazo']) : NULL);
      $tasa               = $proceso['data']['tb_proceso_tasa'] ? floatval($proceso['data']['tb_proceso_tasa']) : ($precalificacion['data']['tb_precalificacion_tasa'] ? floatval($precalificacion['data']['tb_precalificacion_tasa']) : NULL);
      $costo_vehiculo     = $proceso['data']['tb_proceso_monto'] ? floatval($proceso['data']['tb_proceso_monto']) : ($precalificacion['data']['tb_precalificacion_monto'] ? floatval($precalificacion['data']['tb_precalificacion_monto']) : NULL);
      $inicial_porcentaje = $proceso['data']['tb_proceso_inicial_porcen'] ? floatval($proceso['data']['tb_proceso_inicial_porcen']) : ($precalificacion['data']['tb_precalificacion_inicial_porcen'] ? floatval($precalificacion['data']['tb_precalificacion_inicial_porcen']) : NULL);
      $inicial            = $proceso['data']['tb_proceso_inicial'] ? floatval($proceso['data']['tb_proceso_inicial']) : ($precalificacion['data']['tb_precalificacion_inicial_monto'] ? floatval($precalificacion['data']['tb_precalificacion_inicial_monto']) : NULL);
      $valor_prestamo     = $proceso['data']['tb_proceso_prestamo'] ? floatval($proceso['data']['tb_proceso_prestamo']) : NULL;
      $tipo_gar     = $proceso['data']['tb_cgarvtipo_id'] ? intval($proceso['data']['tb_cgarvtipo_id']) : 0;

      /* $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
      if($precalificacion['estado']==1){
        $moneda             = $precalificacion['data']['tb_precalificacion_moneda_credito'] ? $precalificacion['data']['tb_precalificacion_moneda_credito'] : NULL;
        $cuotas             = $precalificacion['data']['tb_precalificacion_plazo'] ? intval($precalificacion['data']['tb_precalificacion_plazo']) : NULL;
        $tasa               = $precalificacion['data']['tb_precalificacion_tasa'] ? floatval($precalificacion['data']['tb_precalificacion_tasa']) : NULL;
        $costo_vehiculo     = $precalificacion['data']['tb_precalificacion_monto'] ? floatval($precalificacion['data']['tb_precalificacion_monto']) : NULL;
        $inicial_porcentaje = $precalificacion['data']['tb_precalificacion_inicial_porcen'] ? floatval($precalificacion['data']['tb_precalificacion_inicial_porcen']) : NULL;
        $inicial            = $precalificacion['data']['tb_precalificacion_inicial_monto'] ? floatval($precalificacion['data']['tb_precalificacion_inicial_monto']) : NULL;
      } */
      
    }

  }else{ // proveniente de garveh
    $vista = 'garveh';
  }
  
?>
<div class="modal fade in" tabindex="-1" role="dialog" id="modal_generar_simulador" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <input type="hidden" id="hdd_simu_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" id="hdd_simu_usuario_id" value="<?php echo $usuario_id; ?>">
      <input type="hidden" id="hdd_vista" value="<?php echo $vista; ?>">
      <input type="hidden" id="hdd_tipo_gar" value="<?php echo $tipo_gar; ?>">
      <input type="hidden" id="hdd_seguro_porcentaje" value="">
      <input type="hidden" id="hdd_gps_precio" value="">
      <input type="hidden" id="span_seguro_gps" value="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Simulador de Cuotas</h4>
      </div>
      <div class="modal-body">

        <div class="row">

          <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group">
              <label>Fecha Desembolso: </label>
              <div class="input-group date" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_fecha" id="simu_fecha" value="<?php echo date('d-m-Y'); ?>" readonly>
                <!-- <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span> -->
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group" style="">
						  <label>Moneda: </label>
              <div class="input-group" style="width: 100% !important">
                <select id="simu_moneda" name="simu_moneda" class="form-control input-sm" data-live-search="true" data-max-options="1" onchange="guardarSimulador(this.id, this.value, 'text', '<?php echo $vista ?>')">
                  <option value="">- Seleccione una opción -</option>
                  <option <?php if($moneda == 'sol') { echo 'selected'; } ?> value="sol">Soles</option>
                  <option <?php if($moneda == 'sol_d') { echo 'selected'; } ?> value="sol_d">Soles / Dólares</option>
                  <option <?php if($moneda == 'dolar') { echo 'selected'; } ?> value="dolar">Dólares</option>
                  <option <?php if($moneda == 'dolar_s') { echo 'selected'; } ?> value="dolar_s">Dólares / Soles</option>
                </select>
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-2">
						<div class="form-group" style="">
              <label>Tip. Cambio: </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_cambio" id="simu_cambio" onkeypress="return solo_decimal(event)" onchange="guardarSimulador(this.id, this.value, 'float', '<?php echo $vista ?>')" style="text-align:right;" placeholder="0.00" value="<?php echo number_format(floatval($tipo_cambio), 3); ?>">
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-2">
						<div class="form-group" style="">
              <label>Num. Cuotas: </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_cuota" id="simu_cuota" onkeypress="return solo_numero(event)" onchange="guardarSimulador(this.id, this.value, 'int', '<?php echo $vista ?>'); obtener_valor_seguro_gps()" style="text-align:right;" placeholder="0" value="<?php echo $cuotas; ?>">
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-2">
						<div class="form-group" style="">
              <label>Tasa Interés (%): </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_interes" id="simu_interes" onkeypress="return solo_decimal(event)" onchange="guardarSimulador(this.id, this.value, 'float', '<?php echo $vista ?>');" style="text-align:right;" placeholder="0.00" value="<?php echo number_format(floatval($tasa), 2); ?>">
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group" style="">
              <label>Costo Vehículo: </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_costo" id="simu_costo" onchange="guardarSimulador(this.id, this.value, 'float', '<?php echo $vista ?>'); calcularPrecio('simu_costo');" onkeypress="return solo_decimal(event)" style="text-align:right;" placeholder="0.00" value="<?php if($vista=='garveh'){echo $costo_vehiculo;}else{echo formato_moneda($costo_vehiculo);} ?>">
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group" style="">
              <label>Inicial (%): </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_inicial" id="simu_inicial" onchange="guardarSimulador(this.id, this.value, 'float', '<?php echo $vista ?>'); calcularPrecio('simu_inicial');" onkeypress="return solo_decimal(event)" style="text-align:right;" placeholder="0.00" value="<?php if($vista=='garveh'){echo $inicial_porcentaje;}else{echo formato_moneda($inicial_porcentaje);} ?>">
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group" style="">
              <label>Monto Inicial: </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_inicial_monto" id="simu_inicial_monto" onchange="guardarSimulador(this.id, this.value, 'float', '<?php echo $vista ?>'); calcularPrecio('simu_inicial_monto');" onkeypress="return solo_decimal(event)" style="text-align:right;" placeholder="0.00" value="<?php if($vista=='garveh'){echo $inicial;}else{echo formato_moneda($inicial);} ?>">
              </div>
						</div>
					</div>

          <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group" style="">
              <label>Valor Préstamo: </label>
              <div class="input-group" style="width: 100% !important">
                <input type="text" class="form-control input-sm" name="simu_valor" id="simu_valor" onkeypress="return solo_decimal(event)" style="text-align:right;" onchange="guardarSimulador(this.id, this.value, 'float', '<?php echo $vista ?>')" value="<?php echo formato_moneda($valor_prestamo); ?>" readonly>
              </div>
						</div>
					</div>

          <div class="col-md-3" id="cre_fijo">
            <div class="form-group">
              <!-- <label for="txt_credito_montoche" class="control-label">CHEK PARA AGREGAR ADICIONALES: <?php //echo $credito_gps;?></label><br/> -->
              <!-- <label>
                SEGURO AGREGADO: <input class="flat-green" type="checkbox" name="che_seguro" id="che_seguro" value="1" checked="true"/>
              </label> -->
              <label>
                AGREGAR GPS: <input class="flat-green" type="checkbox" name="chk_gps" id="chk_gps" onchange="obtener_valor_seguro_gps()" checked="true" />
              </label>
            </div>
            <!-- <input type="hidden" id="hdd_seguro_porcentaje" name="hdd_seguro_porcentaje"/>
            <input type="hidden" id="hdd_gps_precio" name="hdd_gps_precio"/>
            <span class="badge bg-green" id="span_seguro_gps"></span> -->
          </div>

          <div align="center" class="col-md-12" style="padding-top: 5px;">
            <div class="">
              <button type="button" class="btn btn-sm btn-primary" onclick="obtener_valor_seguro_gps(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>);"><strong><i class="fa fa-calendar"></i> Generar Cronograma</strong></button>
              <button type="button" class="btn btn-sm btn-success" onclick="generarSimuladorExcelPDF(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, 'excel');"><strong><i class="fa fa-file-excel-o"></i> Exportar Excel</strong></button>
              <button type="button" class="btn btn-sm btn-danger" onclick="generarSimuladorExcelPDF(<?php echo $proceso_id;?>, <?php echo $usuario_id;?>, 'pdf');"><strong><i class="fa fa-file-pdf-o"></i> Exportar PDF</strong></button>
            </div>
          </div>

          <div id="div_simulacion" class="col-md-12">

          </div>

        </div>

      </div>
      <div class="modal-footer">
        <!-- < ?php if($banca==1){ ?> -->
          <!-- <button type="button" id="btn_guardar_bancarizacion" class="btn btn-info" onclick="registrarBanca()">Guardar</button> -->
        <!-- < ?php } ?> -->
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script>
  $('#simu_fecha').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d",
    //endDate : new Date()
  });

  function calcularPrecio(id) {
    var costo_vehiculo = 0.00;
    var inicial = 0.00;
    var prestamo = 0.00;
    var monto_inicial = 0.00;

    if($("#simu_costo").val()=='') {
      costo_vehiculo = 0.00;
    }else{
      costo_vehiculo = parseFloat($("#simu_costo").val());
    }
    if($("#simu_inicial").val()=='') {
      inicial = 0.00;
    }else{
      inicial = parseFloat($("#simu_inicial").val());
    }
    if($("#simu_inicial_monto").val()=='') {
      monto_inicial = 0.00;
    }else{
      costo_vehiculo = parseFloat($("#simu_costo").val());
      monto_inicial = parseFloat($("#simu_inicial_monto").val());
    }

    if(id=='simu_costo'){
      if(inicial==0.00){
        prestamo = costo_vehiculo;
      }else{
        prestamo = costo_vehiculo - (costo_vehiculo*(inicial/100));
      }
      $("#simu_valor").val(prestamo);
      $('#simu_valor').trigger('change');

    }else if(id=='simu_inicial'){
      if(costo_vehiculo==0.00){
        prestamo = "0.00";
      }else{
        prestamo = costo_vehiculo - (costo_vehiculo*(inicial/100));
        monto_inicial = costo_vehiculo*(inicial/100);
        $("#simu_inicial_monto").val(monto_inicial);
        $("#simu_valor").val(prestamo);
        $('#simu_valor').trigger('change');
      }

    }else if(id=='simu_inicial_monto'){
      if(costo_vehiculo==0.00){
        prestamo = "0.00";
      }else{
        prestamo = costo_vehiculo - monto_inicial;
        $("#simu_valor").val(prestamo);
        $('#simu_valor').trigger('change');
      }
    }

    //guardarSimulador("simu_valor", prestamo, "float");
  } 
</script>