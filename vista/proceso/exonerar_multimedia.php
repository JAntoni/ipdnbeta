<?php
  require_once('../../core/usuario_sesion.php');
  $cliente_id = 0;
  $proceso_fase_item_id = $_POST['proceso_fase_item_id'];
  $item_des = $_POST['item_des'];
  $item_idx = $_POST['item_idx'];
  $user = $_SESSION['usuario_id'];
  $user_name = $_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'];

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_proceso_fase_item_exonerar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
          <h4 class="modal-title">EXONERAR ARCHIVOS EN <b><?php echo $item_des; ?></b></h4>
      </div>
      <form id="frm_exonerar_proceso_fase_item">
        <input type="hidden" name="action" value="exonerar">
        <input type="hidden" name="hdd_proceso_fase_item_id" id="hdd_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id;?>">
        <input type="hidden" name="hdd_cliente_id" value="<?php echo $cliente_id;?>">
        <input type="hidden" name="hdd_usuario_id" value="<?php echo $user;?>">
        <input type="hidden" name="hdd_usuario" value="<?php echo $user_name;?>">
        <input type="hidden" name="hdd_item_des" value="<?php echo $item_des;?>">
        <input type="hidden" name="hdd_item_idx" id="hdd_item_idx" value="<?php echo $item_idx;?>">
        <?php /* if($_POST['pdf_is'] != 'nuevo'){
                  $id_reg_mod = explode('-', $_POST['pdf_is']);
                  $mod_or_new = $id_reg_mod[0];
                  $id_reg_mod = $id_reg_mod[1]; */
                  ?>
                  <!-- <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $mod_or_new;?>">
                  <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo intval($id_reg_mod);?>"> -->
                <?php
                /* }
                else{ */
                  ?>
                  <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $_POST['pdf_is'];?>">
                  <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo 0;?>">
                <?php
                /* } */
                 ?>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Motivo: </label>
                                <textarea class="form-control" rows="3" name="txt_motivo_exonerar" id="txt_motivo_exonerar" placeholder="Ingrese motivo ..." style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info" id="btn_guardar_exonerar">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_creditoexonerarpdf" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/proceso/exonerar_multimedia.js?ver=123';?>"></script>
