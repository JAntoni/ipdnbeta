<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
require_once('../../static/tcpdf/tcpdf.php');

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$proceso_id = intval($_GET['proceso_id']);
$usuario_id = intval($_GET['usuario_id']);
$fecha      = $_GET['fecha'];
$moneda     = $_GET['moneda'];
$cambio     = floatval($_GET['cambio']);
$cuota      = intval($_GET['cuota']);
$interes    = floatval($_GET['interes']);
$costo      = floatval($_GET['costo']);
$inicial    = floatval($_GET['inicial']);
$valor      = floatval($_GET['valor']);

$chk_gps    = intval($_GET['chk_gps']);

$seguro_porcentaje  = floatval($_GET['seguro_porcentaje']);
$gps_precio         = floatval($_GET['gps_precio']);
$span_seguro_gps    = $_GET['span_seguro_gps'];

if($seguro_porcentaje > 0){ // nuevo modo de interes al 14-11-23, se suma el 0.27 a la tasa de interes fijada
  $interes = $interes + $seguro_porcentaje;
}

$colaborador = '';
$celular = '';
$usuario = $oUsuario->mostrarUno($usuario_id);
if($usuario['estado']==1){
  $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
  $celular = $usuario['data']['tb_usuario_tel'];
}

$title = 'SIMULADOR';
$codigo = "";
//$codigo = 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {

  public function Header() {
    //$image_file = K_PATH_IMAGES.'logo.jpg';
    //$this->Image($image_file, 10, 10, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.prestamosdelnorte.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.prestamosdelnorte.com');
  $pdf->SetKeywords('www.prestamosdelnorte.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  //$pdf->setLanguageArray();

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$titulo_moneda = '';
$mon = '';
$sub_mon = '';
if($moneda=='sol'){
  $titulo_moneda = 'SOLES';
  $mon = 'S/ ';
}elseif($moneda=='dolar'){
  $titulo_moneda = 'DOLARES';
  $mon = '$ ';
  $sub_mon = 'S/ ';
}elseif($moneda=='sol_d'){
  $titulo_moneda = 'SOLES_DOLARES';
  $mon = '$ ';
  $sub_mon = 'S/ ';
}elseif($moneda=='dolar_s'){
  $titulo_moneda = 'DÓLARES_SOLES';
  $mon = 'S/ ';
  $sub_mon = '$ ';
}

$nombre_tip_cre = 'CREDITO GARANTÍA MOBILIARIA';

// Seccion para obtener el total a pagar del prestamo
$total_pagar = '';
$total_prestamo = 0.00;

if($moneda=='sol' || $moneda=='dolar'){

  for ($i=0; $i < $cuota; $i++) { 

    $seguro_mensual = 0.00;
    $gps_mensual = 0.00;

    if($i==0){
      $new_date = $fecha;
      $capital = $valor;
    }
    $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

    $cuota_pagar = 0.00;
    $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*$valor/((pow(1+($interes/100),$cuota))-1);


    $sobrecosto = 0.00;
    $sobrecosto = $capital*($interes/100);

    $amortizacion = 0.00;
    if($cuota_pagar>0){
      $amortizacion = $cuota_pagar - $sobrecosto;
    }

    if($chk_gps > 0){
      if($gps_precio > 0 && $cuota > 0){
        if($moneda=='sol'){
          $gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
        }elseif($moneda=='dolar'){
          $gps_mensual = formato_moneda($gps_precio / $cuota);
        }
        $cuota_pagar = $cuota_pagar + $gps_mensual;
      }
    }

    $total_prestamo = $total_prestamo + $cuota_pagar - $gps_mensual;

    $c++;

  }

  $total_pagar = $mon.' '.number_format($total_prestamo, 2);
  if($moneda=='dolar'){
    $total_pagar .= '<br/>'.$sub_mon.' '.number_format($total_prestamo*$cambio, 2);
  }

}elseif($moneda=='dolar_s'){

  for ($i=0; $i < $cuota; $i++) { 

    $seguro_mensual = 0.00;
    $gps_mensual = 0.00;

    if($i==0){
      $new_date = $fecha;
      $capital = $valor*$cambio;
    }

    $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

    $cuota_pagar = 0.00;
    $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor*$cambio)/((pow(1+($interes/100),$cuota))-1);

    $sobrecosto = 0.00;
    $sobrecosto = $capital*($interes/100);

    $amortizacion = 0.00;
    if($cuota_pagar>0){
      $amortizacion = $cuota_pagar - $sobrecosto;
    }

    if($chk_gps > 0){
      if($gps_precio > 0 && $cuota > 0){
        $gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
        $cuota_pagar = $cuota_pagar + $gps_mensual;
      }
    }

    /* $capital = $capital - $amortizacion;
    $total_sobrecosto = $total_sobrecosto + $sobrecosto; */

    $total_prestamo = $total_prestamo + $cuota_pagar - $gps_mensual;

    $c++;

  }

  $total_pagar = $mon.' '.number_format($total_prestamo, 2);

}elseif($moneda=='sol_d'){

  for ($i=0; $i < $cuota; $i++) { 

    $seguro_mensual = 0.00;
    $gps_mensual = 0.00;

    if($i==0){
      $new_date = $fecha;
      $capital = $valor/$cambio;
    }

    $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

    $cuota_pagar = 0.00;
    $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor/$cambio)/((pow(1+($interes/100),$cuota))-1);

    $sobrecosto = 0.00;
    $sobrecosto = $capital*($interes/100);

    $amortizacion = 0.00;
    if($cuota_pagar>0){
      $amortizacion = $cuota_pagar - $sobrecosto;
    }

    if($chk_gps > 0){
      if($gps_precio > 0 && $cuota > 0){
        $gps_mensual = formato_moneda($gps_precio / $cuota);
        $cuota_pagar = $cuota_pagar + $gps_mensual;
      }
    }

    $total_prestamo = $total_prestamo + $cuota_pagar - $gps_mensual;

    /* $capital = $capital - $amortizacion;
    $total_sobrecosto = $total_sobrecosto + $sobrecosto; */

    $c++;

  }

  $total_pagar = $mon.' '.number_format($total_prestamo, 2);
  $total_pagar .= '<br/>'.$sub_mon.' '.number_format($total_prestamo*$cambio, 2);

}
//

  $html = '
    <style>
      .tb_crono{
        font-size: 16pt;
      }
      td{
        padding-top: 5px;
        padding-bottom: 5px;
      }
    </style>
  ';

  $html= '
    <table>
      <tr>
        <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 40px; height: 50px;"></td>
        <td colspan="12" style="text-align: center;">
          <b><u>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</u></b>
          <br/>Av. Mariscal Nieto N° 480, Ext. A – 7 Primer Piso
          <br/>Chiclayo, Chiclayo, Lambayeque
        </td>
        <td colspan="3"><br/><br/><b>'.$codigo.'</b></td>
      </tr>
    </table>
    <br/>
    <br/>
    <table>
      <tr>
        <td colspan="5"></td>
        <td colspan="9" style="text-align: center;"></td>
        <td colspan="6">'.get_nombre_dia(date('Y-m-d')).', '.fechaActual(date('Y-m-d')).'</td>
      </tr>
    </table>
    <table>
      <tr>
        <td colspan="5"></td>
        <td colspan="12" style="text-align: center;"><br/><br/><strong><u>SIMULADOR DE CRONOGRAMA</u></strong></td>
        <td colspan="3"></td>
      </tr>
    </table>
    <br/>
    <br/>';

  $html .= '
    <table width="80%" border="1" cellpadding="3" style="font-size:30px">
      <tr>
        <td colspan="4" style="background-color: rgb(33,175,57); color: white;"><b>'.$nombre_tip_cre.'</b></td>
      </tr>
      <tr>
        <td>Cliente</td>
        <td colspan="3"><b></b></td>
      </tr>
      <tr>
        <td>Fecha Desembolso</td>
        <td align="right">'.mostrar_fecha($fecha).'</td>
        <td colspan="2" align="center" valign="middle" rowspan="5">
          <h2>'.$mon.'</h2>
        </td>
      </tr>';
      if($moneda=='sol_d' || $moneda=='dolar_s'){
        $html .= '<tr>
          <td>Valor del Préstamo</td>
          <td align="right">'.$sub_mon.' '.number_format($valor, 2).'</td>
        </tr>';
      }else{
        $html .= '<tr>
          <td>Valor del Préstamo</td>
          <td align="right">'.$mon.' '.number_format($valor, 2).'</td>
        </tr>';
      }
      $html .= '<tr>
        <td>Tasa de Interés</td>
        <td align="right">'.number_format($interes, 2).'%</td>
      </tr>
      <tr>
        <td>Total a Pagar</td>
        <td align="right">'.$total_pagar.'</td>
      </tr>
      <tr>
        <td>Cantidad de Cuotas</td>
        <td align="right">'.$cuota.'</td>
      </tr>
    </table>
    ';
   
  $html .='
    <p></p>
    <table width="100%" border="1" class="tb_crono" cellpadding="3">
      <thead>
        <tr style="font-size:25px; font-weight: bold">
          <th align="center" width="10%">FECHA</th>
          <th align="center" width="8%">N° DE CUOTA</th>
          <th align="center">CAPITAL</th>
          <th align="center" width="14%">AMORTIZACIÓN</th>
          <th align="center">SOBRECOSTO DEL PERIODO</th>
          <th align="center">GPS</th>
          <th align="center">CUOTA</th>';
          if($moneda=='dolar' || $moneda=='sol_d'){
            $html .= '<th align="center">CUOTA SOLES</th>';
          }
  $html .= '</tr>
      </thead>
      <tbody>';

      if($moneda=='sol' || $moneda=='dolar'){

        for ($i=0; $i < $cuota; $i++) { 
    
          $seguro_mensual = 0.00;
          $gps_mensual = 0.00;
    
          if($i==0){
            $new_date = $fecha;
            $capital = $valor;
          }
          $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));
    
          $cuota_pagar = 0.00;
          $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*$valor/((pow(1+($interes/100),$cuota))-1);
    
    
          $sobrecosto = 0.00;
          $sobrecosto = $capital*($interes/100);
    
          $amortizacion = 0.00;
          if($cuota_pagar>0){
            $amortizacion = $cuota_pagar - $sobrecosto;
          }
 
          if($chk_gps > 0){
            if($gps_precio > 0 && $cuota > 0){
              if($moneda=='sol'){
                $gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
              }elseif($moneda=='dolar'){
                $gps_mensual = formato_moneda($gps_precio / $cuota);
              }
              $cuota_pagar = $cuota_pagar + $gps_mensual;
            }
          }
    
          $html .='<tr class="even" style="font-size:22px">
                    <td align="right" width="10%">'.$new_date.'</td>
                    <td align="right" width="8%">'.($i+1).'</td>
                    <td align="right">'.$mon.' '.number_format($capital, 2).'</td>
                    <td align="right" width="14%">'.$mon.' '.number_format($amortizacion, 2).'</td>
                    <td align="right">'.$mon.' '.number_format($sobrecosto, 2).'</td>
                    <td align="right">'.$mon.' '.number_format($gps_mensual, 2).'</td>
                    <td align="right">'.$mon.' '.number_format($cuota_pagar, 2).'</td>';
                    if($moneda=='dolar'){
                      $html .= '<td align="right">'.$sub_mon.''.number_format($cuota_pagar*$cambio, 2).'</td>';
                    }
          $html .= '</tr>';

    
          $capital = $capital - $amortizacion;
    
          $c++;
    
        }
    
      }elseif($moneda=='dolar_s'){
    
        for ($i=0; $i < $cuota; $i++) { 
    
          $seguro_mensual = 0.00;
          $gps_mensual = 0.00;
    
          if($i==0){
            $new_date = $fecha;
            $capital = $valor*$cambio;
          }
    
          $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));
    
          $cuota_pagar = 0.00;
          $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor*$cambio)/((pow(1+($interes/100),$cuota))-1);
    
          $sobrecosto = 0.00;
          $sobrecosto = $capital*($interes/100);
    
          $amortizacion = 0.00;
          if($cuota_pagar>0){
            $amortizacion = $cuota_pagar - $sobrecosto;
          }
    
          if($chk_gps > 0){
            if($gps_precio > 0 && $cuota > 0){
              $gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
              $cuota_pagar = $cuota_pagar + $gps_mensual;
            }
          }
    
          $html .='<tr class="even" style="font-size:22px">
                    <td align="right" width="10%">'.$new_date.'</td>
                    <td align="right" width="8%">'.($i+1).'</td>
                    <td align="right">'.$mon.' '.number_format($capital, 2).'</td>
                    <td align="right" width="14%">'.$mon.' '.number_format($amortizacion, 2).'</td>
                    <td align="right">'.$mon.' '.number_format($sobrecosto, 2).'</td>
                    <td align="right">'.$mon.' '.number_format($gps_mensual, 2).'</td>
                    <td align="right">'.$mon.' '.number_format($cuota_pagar, 2).'</td>
                  </tr>';

          $capital = $capital - $amortizacion;
          $total_sobrecosto = $total_sobrecosto + $sobrecosto;
    
          $c++;
    
        }
    
      }elseif($moneda=='sol_d'){
    
        for ($i=0; $i < $cuota; $i++) { 
    
          $seguro_mensual = 0.00;
          $gps_mensual = 0.00;
    
          if($i==0){
            $new_date = $fecha;
            $capital = $valor/$cambio;
          }
    
          $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));
    
          $cuota_pagar = 0.00;
          $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor/$cambio)/((pow(1+($interes/100),$cuota))-1);
    
          $sobrecosto = 0.00;
          $sobrecosto = $capital*($interes/100);
    
          $amortizacion = 0.00;
          if($cuota_pagar>0){
            $amortizacion = $cuota_pagar - $sobrecosto;
          }
    
          if($chk_gps > 0){
            if($gps_precio > 0 && $cuota > 0){
              $gps_mensual = formato_moneda($gps_precio / $cuota);
              $cuota_pagar = $cuota_pagar + $gps_mensual;
            }
          }
    
          $html .='<tr class="even" style="font-size:22px">
                  <td align="right" width="10%">'.$new_date.'</td>
                  <td align="right" width="8%">'.($i+1).'</td>
                  <td align="right">'.$mon.' '.number_format($capital, 2).'</td>
                  <td align="right" width="14%">'.$mon.' '.number_format($amortizacion, 2).'</td>
                  <td align="right">'.$mon.' '.number_format($sobrecosto, 2).'</td>
                  <td align="right">'.$mon.' '.number_format($gps_mensual, 2).'</td>
                  <td align="right">'.$mon.' '.number_format($cuota_pagar, 2).'</td>
                  <td align="right">'.$sub_mon.''.number_format($cuota_pagar*$cambio, 2).'</td>
                </tr>';

          $capital = $capital - $amortizacion;
          $total_sobrecosto = $total_sobrecosto + $sobrecosto;
    
          $c++;
    
        }
    
      }

  $html .='
      </tbody>
    </table>
    <p></p>';
  $html .= '
    <p></p></p></p>

    <tr>
      <td colspan="20" align="center">_______________________________________ <br/>
      GALVEZ ROJAS HERBERT ENGEL <br/>
      SUB GRTE GRAL INVERSIONES Y PRÉSTAMOS DEL NORTE SAC
      </td>
    </tr>
  ';



// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$title."_".$titulo_moneda.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>

