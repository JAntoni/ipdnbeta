<?php
session_name("ipdnsac");
session_start();

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ('../vehiculomarca/Vehiculomarca.class.php');
$oVehiculomarca = new Vehiculomarca();
require_once ('../vehiculomodelo/Vehiculomodelo.class.php');
$oVehiculomodelo = new Vehiculomodelo();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$proceso_id = $_POST['proceso_id'];
$usuario_id = $_POST['usuario_id'];
$proceso_fase_item_id = $_POST['proceso_fase_item_id'];

$proceso = $oProceso->mostrarUno($proceso_id);
$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
$ingresos = $oProceso->mostrarIEXProceso($proceso_id, 'I');
$egresos = $oProceso->mostrarIEXProceso($proceso_id, 'E');

$ocupacion = '';
$antiguedad = '';
$forma_ingreso = '';
$marca_veh = '';
$modelo_veh = '';
$anio_veh = '';
$km_veh = '';
$datos_vehiculo = '';
$ingreso = 0.00;
$egreso = 0.00;
$saldo = 0.00;
$residencial = '';
$casa_antiguedad = '';
$finalidad = '';
$comentario = '';
$familiar = '';
$observacion = '';
$conclusion = '';
$monto_aprob = 0.00;

if($precalificacion['estado'] == 1){
    $ocupacion = $precalificacion['data']['tb_precalificacion_ocupacion'];
    $antiguedad = $precalificacion['data']['tb_precalificacion_cargo_antig'];

    $marca = $precalificacion['data']['tb_precalificacion_marca'] ? intval($precalificacion['data']['tb_precalificacion_marca']) : 0;
    if($marca > 0){
        $marca_vehiculo = $oVehiculomarca->mostrarUno($marca);
        if($marca_vehiculo['estado']==1){
            $marca_veh = $marca_vehiculo['data']['tb_vehiculomarca_nom'];
        }
    }
    $modelo = $precalificacion['data']['tb_precalificacion_modelo'] ? intval($precalificacion['data']['tb_precalificacion_modelo']) : 0;
    if($modelo > 0){
        $modelo_vehiculo = $oVehiculomodelo->mostrarUno($modelo);
        if($modelo_vehiculo['estado']==1){
            $modelo_veh = $modelo_vehiculo['data']['tb_vehiculomarca_nom'];
        }
    }
    $anio_veh = $precalificacion['data']['tb_precalificacion_anio'];
    $km_veh = floatval($precalificacion['data']['tb_precalificacion_km']).' Km';
    $datos_vehiculo = $marca_veh.' / '.$modelo_veh.' / '.$anio_veh.' / '.$km_veh;

    if($precalificacion['data']['tb_precalificacion_ingreso_forma'] == 'formal'){
        $forma_ingreso = 'FORMAL';
    }elseif($precalificacion['data']['tb_precalificacion_ingreso_forma'] == 'informal'){
        $forma_ingreso = 'INFORMAL';
    }elseif($precalificacion['data']['tb_precalificacion_ingreso_forma'] == 'mixto'){
        $forma_ingreso = 'MIXTO';
    }else{
        $forma_ingreso = 'NO REGISTRADO';
    }
    
    if(intval($precalificacion['data']['tb_precalificacion_vivienda']) == 'propia'){
        $residencial = 'PROPIA';
    }elseif(intval($precalificacion['data']['tb_precalificacion_vivienda']) == 'alquilada'){
        $residencial = 'ALQUILADA';
    }elseif(intval($precalificacion['data']['tb_precalificacion_vivienda']) == 'familiar'){
        $residencial = 'FAMILIAR';
    }else{
        $residencial = 'NO REGISTRADO';
    }

    $casa_antiguedad = $precalificacion['data']['tb_precalificacion_casa_antig'];
    $score = $precalificacion['data']['tb_precalificacion_score'];
    $finalidad = $precalificacion['data']['tb_precalificacion_uso_vehiculo'];

    $comentario = $precalificacion['data']['tb_precalificacion_observacion'];
    $familiar = $precalificacion['data']['tb_precalificacion_coment_fam'];
    $observacion = $precalificacion['data']['tb_precalificacion_coment_obs'];
    $conclusion = $precalificacion['data']['tb_precalificacion_coment_con'];
    if($precalificacion['data']['tb_precalificacion_precio_propuesto'] == null || $precalificacion['data']['tb_precalificacion_precio_propuesto'] == 0.00){
        if($proceso['data']['tb_proceso_prestamo'] == null){
          $monto_aprob = floatval($proceso['data']['tb_proceso_monto']) - (floatval($proceso['data']['tb_proceso_monto']) * (floatval($proceso['data']['tb_proceso_inicial_porcen'])/100));
        }else{
          $monto_aprob = floatval($proceso['data']['tb_proceso_prestamo']);
        }
      }else{
        $monto_aprob = floatval($precalificacion['data']['tb_precalificacion_precio_propuesto']);
      }

    // Campos llenados por comité
    $familia = $precalificacion['data']['tb_precalificacion_familiar'] != null ? $precalificacion['data']['tb_precalificacion_familiar'] : '';
    $calificacion = $precalificacion['data']['tb_precalificacion_calif_finan'] != null ? intval($precalificacion['data']['tb_precalificacion_calif_finan']) : 0;
    $valorizacion = $proceso['data']['tb_proceso_moneda'] != null ? $proceso['data']['tb_proceso_moneda'] : 'sol';
    $valorizacion_monto = $precalificacion['data']['tb_precalificacion_valor_veh'] != null ? floatval($precalificacion['data']['tb_precalificacion_valor_veh']) : (($precalificacion['data']['tb_precalificacion_monto'] != null) ? floatval($precalificacion['data']['tb_precalificacion_monto']) : 0.00);
    $depreciacion = $precalificacion['data']['tb_precalificacion_depreciacion'] != null ? intval($precalificacion['data']['tb_precalificacion_depreciacion']) : 0;
    $porcentaje_asignado = $precalificacion['data']['tb_precalificacion_calif_veh_porc'] != null ? floatval($precalificacion['data']['tb_precalificacion_calif_veh_porc']) : 0.00;
    $monto_calificado = $precalificacion['data']['tb_precalificacion_calif_veh'] != null ? floatval($precalificacion['data']['tb_precalificacion_calif_veh']) : 0.00;
    //

}

if($ingresos['estado'] == 1){
    foreach ($ingresos['data'] as $key => $ing) {
        $ingreso = $ingreso + $ing['tb_ingresoegreso_monto'];
    }
}
if($egresos['estado'] == 1){
    foreach ($egresos['data'] as $key => $ing) {
        $egreso = $egreso + $ing['tb_ingresoegreso_monto'];
    }
}

$saldo = $ingreso - $egreso;

$mon = '';
if($proceso['estado'] == 1){
    // moneda
    if($proceso['data']['tb_proceso_moneda']=='sol' || $proceso['data']['tb_proceso_moneda']=='dolar_s'){
        $mon = 'S/ ';
    }elseif($proceso['data']['tb_proceso_moneda']=='dolar' || $proceso['data']['tb_proceso_moneda']=='sol_d'){
        $mon = '$ ';
    }
}

?>

<style>
    .bold {
        font-weight: 600;
    }
    .celda{
        padding: 6px 12px;
    }
    .columna {
        border: #000000 1px solid;
        background-color: #279944;
        color: #FFFFFF;
        font-family: 'Cambria';
        font-size: 12px
    }
    .asesor {
        background-color: #D5D5D5;
    }
    .asesor-cabecera {
        background-color: #C1C1C1;
    }
    .comite {
        background-color: #C3D3FF;
    }
    .comite-cabecera {
        background-color: #A6BEFF;
    }
    .radio-block {
        margin-right: 5px;
    }
    .radio-block label {
        margin-right: 2px;
    }
    .radio-inline {
        display: inline-block;
        margin-right: 5px;
    }
    .radio-inline label {
        margin-right: 2px;
    }
    .valor {
        display: flex;
    }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_comentario_precalificacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">OBSERVACIONES PRE CALIFICACIÓN</h4>
            </div>
            <form id="form_comentario_precalificacion" method="post">
                <input name="action" id="action" type="hidden" value="comentario_precalificacion">
                <input name="hdd_proceso_id" id="hdd_proceso_id" type="hidden" value="<?php echo $proceso_id; ?>">
                <input name="hdd_usuario_id" id="hdd_usuario_id" type="hidden" value="<?php echo $usuario_id; ?>">
                <input name="hdd_proceso_fase_item_id" id="hdd_proceso_fase_item_id" type="hidden" value="<?php echo $proceso_fase_item_id; ?>">

                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-3 pb-3">
                            <div class="form-group">
                                <label>Tipo:</label>
                                <div class="input-group" style="width: 100%;">
                                    <select name="tipo_coment" id="tipo_coment" class="form-control input-sm">
                                        <option value="procede">Procede / No Procede</option>
                                        <option value="familiar">Familiar en ciudad</option>
                                        <option value="observacion">Observación</option>
                                        <option value="conclusion">Conclusión</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9 pb-3">
                            <div class="form-group">
                                <label>Comentario:</label>
                                <textarea id="txt_comentario" name="txt_comentario" class="form-control input-sm" rows="3"; style="resize: none;" placeholder="Registre comentario breve sobre la hoja de pre calificación..."><?php //echo $comentario; ?></textarea>
                            </div>
                        </div>

                        <div class="col-md-12" style="justify-content: end; text-align: right; margin-bottom: 10px;">
                            <button type="submit" class="btn btn-sm btn-primary" id="btn_guardar_comentario_precalificacion">Guardar Comentario</button>
                        </div>

                        <hr>

                        <div class="col-md-12 pb-3">

                            <table class="pb-3 mt-2" border="1" width="100%">
                                <tr>
                                    <td align="center" class="columna"><strong>OCUPACIÓN</strong></td>
                                    <td colspan="4" class="celda asesor bold"><?php if($ocupacion != ''){ echo $ocupacion; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                    <td align="center" colspan="5" rowspan="7" class="celda asesor-cabecera"><strong>DATOS LLENADOS POR ASESOR</strong></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>ANTIGÜEDAD</strong></td>
                                    <td colspan="3" class="celda asesor bold"><?php if($antiguedad != ''){ echo $antiguedad; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>FORMA DE INGRESOS</strong></td>
                                    <td colspan="3" class="celda asesor bold"><?php if($forma_ingreso != ''){ echo $forma_ingreso; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>INGRESOS REF.</strong></td>
                                    <td colspan="3" class="celda asesor bold"><?php echo $mon.''.formato_moneda($ingreso); ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>GASTOS REF.</strong></td>
                                    <td colspan="3" class="celda asesor bold"><?php echo $mon.''.formato_moneda($egreso); ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>EXCEDENTE DE SALDO</strong></td>
                                    <td colspan="3" class="celda asesor bold"><?php echo $mon.''.formato_moneda($saldo); ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>MARCA / MODELO / AÑO / KM</strong></td>
                                    <td colspan="3" class="celda asesor bold"><?php echo $datos_vehiculo; ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>OPINIÓN DE EVALUADORES</strong></td>
                                    <td class="celda comite bold">
                                        <div class="radio-block">
                                            <label><input type="radio" name="procede" value="n" checked> SIN VOTO</label>
                                        </div>
                                        <div class="radio-block">
                                            <label><input type="radio" name="procede" value="s"> PROCEDE</label>
                                        </div>
                                        <div class="radio-block">
                                            <label><input type="radio" name="procede" value="s"> NO PROCEDE</label>
                                        </div>
                                    </td>
                                    <td colspan="3" class="celda comite bold"><?php if($comentario != ''){ echo $comentario; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                    <td align="center" colspan="3" rowspan="1" class="celda comite-cabecera"><strong>DATOS LLENADOS POR EVALUADORES</strong></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>RESIDENCIA</strong></td>
                                    <td colspan="4" class="celda asesor bold"><?php if($residencial != ''){ echo $residencial; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                    <td align="center" colspan="5" rowspan="2" class="celda asesor-cabecera"><strong>DATOS LLENADOS POR ASESOR</strong></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>ANTIGÜEDAD</td>
                                    <td colspan="3" class="celda asesor bold"><?php if($casa_antiguedad != ''){ echo $casa_antiguedad; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>FAMILIARES EN LA CIUDAD</strong></td>
                                    <td colspan="1" class="celda comite bold">
                                        <div class="radio-inline">
                                            <label><input type="radio" name="familia" onchange="guardarDatosComite(this.name, this.value, 'text');" value="n" <?php if($familia=='n'){echo 'checked';} ?>> NO</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="familia" onchange="guardarDatosComite(this.name, this.value, 'text');" value="s" <?php if($familia=='s'){echo 'checked';} ?>> SI</label>
                                        </div>
                                    </td>
                                    <td colspan="5" class="celda comite bold"><?php if($familiar != ''){ echo $familiar; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                    <td align="center" colspan="3" rowspan="1" class="celda comite-cabecera"><strong>DATOS LLENADOS POR EVALUADORES</strong></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>FINALIDAD DE PRÉSTAMO</strong></td>
                                    <td colspan="4" class="celda asesor bold"><?php if($finalidad != ''){ echo $finalidad; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                    <td align="center" colspan="5" rowspan="2" class="celda asesor-cabecera"><strong>DATOS LLENADOS POR ASESOR</strong></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>SCORE CREDITICIO</strong></td>
                                    <td colspan="4" class="celda asesor bold"><?php if($score != ''){ echo $score; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>CALIFICACIÓN FINANCIERA</strong></td>
                                    <td colspan="6" class="celda comite bold">
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="1" <?php if($calificacion==1){echo 'checked';} ?>> 1</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="2" <?php if($calificacion==2){echo 'checked';} ?>> 2</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="3" <?php if($calificacion==3){echo 'checked';} ?>> 3</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="4" <?php if($calificacion==4){echo 'checked';} ?>> 4</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="5" <?php if($calificacion==5){echo 'checked';} ?>> 5</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="6" <?php if($calificacion==6){echo 'checked';} ?>> 6</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="7" <?php if($calificacion==7){echo 'checked';} ?>> 7</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="8" <?php if($calificacion==8){echo 'checked';} ?>> 8</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="9" <?php if($calificacion==9){echo 'checked';} ?>> 9</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="calificacion" onchange="guardarDatosComite(this.name, this.value, 'int');" value="10" <?php if($calificacion==10){echo 'checked';} ?>> 10</label>
                                        </div>
                                    </td>
                                    <td align="center" colspan="1" rowspan="8" class="celda comite-cabecera"><strong>DATOS LLENADOS POR EVALUADORES</strong></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>VALORIZACIÓN DE UNIDAD</strong></td>
                                    <td colspan="6" class="celda comite bold">
                                        <div class="valor" style="justify-content: end;">
                                            <div class="radio-inline">
                                                <label><input type="radio" name="valorizacion" onchange="guardarDatosComite(this.name, this.value, 'text');" value="sol" <?php if($valorizacion=='sol'){echo 'checked';} ?>> S/</label>
                                            </div>
                                            <div class="radio-inline">
                                                <label><input type="radio" name="valorizacion" onchange="guardarDatosComite(this.name, this.value, 'text');" value="dolar" <?php if($valorizacion=='dolar'){echo 'checked';} ?>> $</label>
                                            </div>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" name="valorizacion_monto" id="valorizacion_monto" placeholder="0.00" value="<?php echo formato_moneda($valorizacion_monto); ?>" onchange="guardarDatosComite(this.id, this.value, 'float'); calcularPrestamo();" onkeypress="return solo_decimal(event)" style="text-align: right;">
                                            </div>
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>DEPRECIACIÓN</strong></td>
                                    <td colspan="3" class="celda comite bold">
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="1" <?php if($depreciacion==1){echo 'checked';} ?>> 1</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="2" <?php if($depreciacion==2){echo 'checked';} ?>> 2</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="3" <?php if($depreciacion==3){echo 'checked';} ?>> 3</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="4" <?php if($depreciacion==4){echo 'checked';} ?>> 4</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="5" <?php if($depreciacion==5){echo 'checked';} ?>> 5</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="6" <?php if($depreciacion==6){echo 'checked';} ?>> 6</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="7" <?php if($depreciacion==7){echo 'checked';} ?>> 7</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="8" <?php if($depreciacion==8){echo 'checked';} ?>> 8</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="9" <?php if($depreciacion==9){echo 'checked';} ?>> 9</label>
                                        </div>
                                        <div class="radio-inline">
                                            <label><input type="radio" name="depreciacion" onchange="guardarDatosComite(this.name, this.value, 'int'); calcularPrestamo();" value="10" <?php if($depreciacion==10){echo 'checked';} ?>> 10</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>VEHÍCULO CALIFICA PARA</strong></td>
                                    <td colspan="6" class="celda comite bold">
                                        <div class="valor" style="justify-content: end;">
                                            <div class="radio-inline">
                                                <label><input type="radio" name="porcentaje_asignado" value="0.50" onchange="guardarDatosComite(this.name, this.value, 'float'); calcularPrestamo();" <?php if($porcentaje_asignado==0.50){echo 'checked';} ?>> 50%</label>
                                            </div>
                                            <div class="radio-inline">
                                                <label><input type="radio" name="porcentaje_asignado" value="0.70" onchange="guardarDatosComite(this.name, this.value, 'float'); calcularPrestamo();" <?php if($porcentaje_asignado==0.70){echo 'checked';} ?>> 70%</label>
                                            </div>
                                            <div class="input-group">
                                                <input readonly type="text" name="monto_calificado" id="monto_calificado" class="form-control input-sm" placeholder="0.00" onchange="guardarDatosComite(this.id, this.value, 'float');" onkeypress="return solo_decimal(event)" style="text-align: right;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>OBS</strong></td>
                                    <td colspan="4" class="celda comite bold"><?php if($observacion != ''){ echo $observacion; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="columna"><strong>CONCLUSIÓN</strong></td>
                                    <td colspan="4" class="celda comite bold"><?php if($conclusion != ''){ echo $conclusion; }else{ echo "<i>(Sin datos)</i>"; } ?></td>
                                </tr>
                            </table>

                        </div>

                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <!-- <button type="submit" class="btn btn-info" id="btn_guardar_comentario_precalificacion">Guardar</button> -->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/proceso/hoja_precalificacion_form.js?ver=02012025';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#monto_calificado").val('0.00');
        calcularPrestamo();
    });

    function calcularPrestamo() {
        var valorizacion = parseFloat($('#valorizacion_monto').val());
        var selectedDepreciacion = document.querySelector('input[name="depreciacion"]:checked');
        var depreciacion = selectedDepreciacion ? selectedDepreciacion.value : 0;
        var selectedPorcentaje = document.querySelector('input[name="porcentaje_asignado"]:checked');
        var porcentaje = selectedPorcentaje ? selectedPorcentaje.value : 0;
        
        var total = 0.00;
        //total = valorizacion - (depreciacion*porcentaje);
        total = valorizacion - (valorizacion*((depreciacion/10)*(1-porcentaje)));
        $("#monto_calificado").val(total.toFixed(2));

    }

</script>