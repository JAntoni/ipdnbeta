<?php ?>
<!-- Content Wrapper. Contains page content -->
<div id="vista_nuevo" class="content-wrapper hidden">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Créditos</a></li>
            <li class=""><i class="fa fa-dashboard"></i>  <?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
            <li class="active"><a href="javascript:void(0)"></i>Nuevo</a></li>

        </ol>
    </section>

    <section class="content-header">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div style="padding-top: 22px">
                        <button type="button" class="btn btn-danger" onclick="btnVolverMenu('nuevo')"><i class="fa fa-arrow-left"></i> Atrás</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="login-box">

            <div align="center" class="login-box">
                <h3>CREAR PROCESO</h3>
            </div>
 
            <div class="login-box-body">

                <!--- MESAJES DE GUARDADO -->
                <!-- <div class="callout callout-info" id="inventario_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div> -->

                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tipo Crédito: </label>
                            <select id="cmb_creditotipo_id" name="cmb_creditotipo_id" class="selectpicker form-control" onchange="selectTipoCredito(this.value)" data-live-search="true" data-max-options="1">
                                <?php 
                                    include VISTA_URL.'creditotipo/creditotipo_select.php';
                                ?>
                            </select>
                        </div>
                    </div>

                    <div id="tipo-garv" class="col-md-12 hidden">
                        <div class="form-group">
                            <label>Tipo Garantía: </label>
                            <select id="cmb_cgarvtipo_id" name="cmb_cgarvtipo_id" class="selectpicker form-control" data-live-search="true" data-max-options="1">
                                <?php 
                                    include VISTA_URL.'proceso/cgarvtipo_select.php';
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary" onclick="btnGenerarProceso()">Generar Proceso</button>
                    </div>
                   
                </div>
            </div>
          
            <div id="div_modal_creditomenor_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE CREDITO-->
            </div>
        
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>

<style>
    #new-process{
        position: relative;
        align-items: center;
    }
    #new-process-body{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    #new-process .box{
        width: 50% !important;
    }
</style>