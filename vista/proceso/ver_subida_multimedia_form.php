<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

$proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);
$multimedia_id = intval($_POST['multimedia_id']);
$item_des = $_POST['item_des'];
$item_idx = $_POST['item_idx'];


$result = $oProceso->mostrarUnoMultimedia($multimedia_id);
//var_dump($result);
if ($result['estado'] == 1) {
    if($result['data']['tb_multimedia_est']==2){ // registro con archivo
        $url = explode("/procesos/",$result['data']['tb_multimedia_url']);
        //var_dump($url);
        $ruta = $url[0];
        $archivo = $url[1];

        $ver = '<div class="item">
                <div class="row">
                  <div class="col-md-12">
                    <input type="hidden" name="hdd_tipo_registro" id="hdd_tipo_registro" value="subida">
                    <p class="message" style="font-family:cambria">
                        <a href="'.$result['data']['tb_multimedia_url'].'" class="name"  target="_blank">
                              '.$archivo.' <i class="fa fa-file-pdf-o" title="VER"></i> 
                        </a>
                        <small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminar('.$multimedia_id.')">Eliminar <i class="fa fa-trash"></i></a></small>
                    </p>
                  </div>
                  
                </div>
            </div>';
    }
    /* if ($result['data']['tb_multimedia_est']==2) {
        $ver = '<div class="item">
            <div class="row">
                <div class="col-md-12">
                <input type="hidden" name="hdd_tipo_registro" id="hdd_tipo_registro" value="exoneración">
                <p class="message name" style="font-family:cambria">
                    '.$result['data']['tb_pdfgarv_his'].'
                </p>
                <small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminar('.$multimedia_id.')">Eliminar <i class="fa fa-trash"></i></a></small>
                </div>
                
            </div>
        </div>';
        
    } */
}
if ($result['estado'] == 0) {
    $ver = '<div class="item">
                <div class="row">
                  <div class="col-md-12">
                    <p class="message name" style="font-family:cambria">
                        <span style="color: red;"> ERROR EN EL CODIGO INGRESADO </span>
                    </p>
                  </div>
                  
                </div>
            </div>';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_proceso_fase_item_ver" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000099">ARCHIVO SUBIDO EN "<?php echo $item_des; ?>"</h4>
            </div>
            <div class="modal-body">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        
                        <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                            <div class="btn-group" data-toggle="btn-toggle">

                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hdd_multimedia_id" id="hdd_multimedia_id" value="<?php echo $multimedia_id;?>">
                    <input type="hidden" name="hdd_proceso_fase_item_id" id="hdd_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id;?>">
                    <input type="hidden" name="hdd_item_des" id="hdd_item_des" value="<?php echo $item_des;?>">
                    <div class="box-body" id="chat-box">
                        <?php echo $ver ?>
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/proceso/subida_multimedia_form.js?ver=07082023'; ?>"></script>

