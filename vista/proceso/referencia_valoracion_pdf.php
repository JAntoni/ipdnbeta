<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
require_once('../../static/tcpdf/tcpdf.php');

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../vehiculomarca/Vehiculomarca.class.php');
$oVehiculomarca = new Vehiculomarca();
require_once('../vehiculomodelo/Vehiculomodelo.class.php');
$oVehiculomodelo = new Vehiculomodelo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$proceso_id = intval($_GET['proceso_id']);
$usuario_id = intval($_GET['usuario_id']);
$proceso_fase_item_id = intval($_GET['proceso_fase_item_id']);
$valoracion_id = intval($_GET['valoracion_id']);

$proceso = $oProceso->mostrarUno($proceso_id);
$precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
$valoracion = $oProceso->mostrarUnoValoracion($proceso_id);
$colaborador='';
$tipogarveh=0;
$tip_cambio = 0.00;

$descripcion   = '';
$observacion   = '';
$extra   = '';
// Depreciacion Interna
$estetica   = '';
$electrica  = '';
$interior   = '';
$mecanica   = '';
$repuesto   = '';
$otro       = '';
// Depreciacion Externa
$des1   = "";
$monto1 = '';
$des2   = "";
$monto2 = '';
$des3   = "";
$monto3 = '';
$des4   = "";
$monto4 = '';
$des5   = "";
$monto5 = '';
$des6   = "";
$monto6 = '';
// Montos
$monto_final_mn = '';
$monto_final_me = '';
$monto_oferta_sc_mn = '';
$monto_oferta_sc_me = '';
$monto_oferta_cc_mn = '';
$monto_oferta_cc_me = '';
//

$fecha_prueba = '';
$marca = '';
$modelo = '';
$anio = '';
$km = '';
$moneda = '';
$cambio = '';

if($proceso['estado']==1){
  $usuario = $oUsuario->mostrarUno($proceso['data']['tb_usuario_id']);
  $client = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
  $preclient = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);
  $tipogarveh = intval($proceso['data']['tb_cgarvtipo_id']);
  if($usuario['estado']==1){
    $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
  }
  if($client['estado']==1){
    $cliente = $client['data']['tb_cliente_nom'];
    $dni = $client['data']['tb_cliente_doc'];
  }else{
    if($preclient['estado']==1){
      $cliente = $preclient['data']['tb_precliente_nombres'];
      $dni = $preclient['data']['tb_precliente_numdoc'];
    }
  }

  $tip_cambio = $proceso['data']['tb_proceso_tip_cambio'];

  // obtener firma engel
  $firma_engel = '';
  $miembro = $oProceso->firma_miembro_comite(11);
  if($miembro['estado'] == 1){
    $firma_engel = $miembro['data']['tb_comite_firma'];
  }

  // Datos traídos de precalificaion
  if($precalificacion['estado']==1){
    $fecha_prueba = ($precalificacion['data']['tb_precalificacion_fec_inspeccion'] != NULL) ? mostrar_fecha($precalificacion['data']['tb_precalificacion_fec_inspeccion']) : '';
    $marca_id = $precalificacion['data']['tb_precalificacion_marca'] ? intval($precalificacion['data']['tb_precalificacion_marca']) : 0;
    $marca = '';
    if($marca_id>0){
      $dt_marca = $oVehiculomarca->mostrarUno($marca_id);
      if($dt_marca['estado']==1){
        $marca = $dt_marca['data']['tb_vehiculomarca_nom'];
      }
    }
    $modelo_id = $precalificacion['data']['tb_precalificacion_modelo'] ? intval($precalificacion['data']['tb_precalificacion_modelo']) : 0;
    $modelo = '';
    if($modelo_id>0){
      $dt_modelo = $oVehiculomodelo->mostrarUno($modelo_id);
      if($dt_modelo['estado']==1){
        $modelo = $dt_modelo['data']['tb_vehiculomodelo_nom'];
      }
    }
    $anio = $precalificacion['data']['tb_precalificacion_anio'] ? $precalificacion['data']['tb_precalificacion_anio'] : '';
    $km = $precalificacion['data']['tb_precalificacion_km'] ? floatval($precalificacion['data']['tb_precalificacion_km']) : '';
  }
  
  $moneda = $proceso['data']['tb_proceso_moneda']; // sol, dolar
  $cambio = $proceso['data']['tb_proceso_tip_cambio'];
  //

  $valoracion = $oProceso->mostrarUnoValoracion($proceso_id);

  if($valoracion['estado']==1){

    $valoracion_id = $valoracion['data']['tb_valoracion_id'];

    $descripcion   = $valoracion['data']['tb_valoracion_des'] ? $valoracion['data']['tb_valoracion_des'] : '';
    $observacion  = $valoracion['data']['tb_valoracion_obs'] ? $valoracion['data']['tb_valoracion_obs'] : '';
    $extra        = $valoracion['data']['tb_valoracion_extra'] ? floatval($valoracion['data']['tb_valoracion_extra']) : '';
    // Depreciacion Interna
    $estetica   = $valoracion['data']['tb_valoracion_estetica'] ? floatval($valoracion['data']['tb_valoracion_estetica']) : '';
    $electrica  = $valoracion['data']['tb_valoracion_electrica'] ? floatval($valoracion['data']['tb_valoracion_electrica']) : '';
    $interior   = $valoracion['data']['tb_valoracion_interior'] ? floatval($valoracion['data']['tb_valoracion_interior']) : '';
    $mecanica   = $valoracion['data']['tb_valoracion_mecanica'] ? floatval($valoracion['data']['tb_valoracion_mecanica']) : '';
    $repuesto   = $valoracion['data']['tb_valoracion_repuesto'] ? floatval($valoracion['data']['tb_valoracion_repuesto']) : '';
    $otro       = $valoracion['data']['tb_valoracion_otro'] ? floatval($valoracion['data']['tb_valoracion_otro']) : '';
    // Depreciacion Externa
    $des1   = $valoracion['data']['tb_valoracion_ext_des1'] ? $valoracion['data']['tb_valoracion_ext_des1'] : '';
    $monto1 = $valoracion['data']['tb_valoracion_ext_monto1'] ? floatval($valoracion['data']['tb_valoracion_ext_monto1']) : '';
    $des2   = $valoracion['data']['tb_valoracion_ext_des2'] ? $valoracion['data']['tb_valoracion_ext_des2'] : '';
    $monto2 = $valoracion['data']['tb_valoracion_ext_monto2'] ? floatval($valoracion['data']['tb_valoracion_ext_monto2']) : '';
    $des3   = $valoracion['data']['tb_valoracion_ext_des3'] ? $valoracion['data']['tb_valoracion_ext_des3'] : '';
    $monto3 = $valoracion['data']['tb_valoracion_ext_monto3'] ? floatval($valoracion['data']['tb_valoracion_ext_monto3']) : '';
    $des4   = $valoracion['data']['tb_valoracion_ext_des4'] ? $valoracion['data']['tb_valoracion_ext_des4'] : '';
    $monto4 = $valoracion['data']['tb_valoracion_ext_monto4'] ? floatval($valoracion['data']['tb_valoracion_ext_monto4']) : '';
    $des5   = $valoracion['data']['tb_valoracion_ext_des5'] ? $valoracion['data']['tb_valoracion_ext_des5'] : '';
    $monto5 = $valoracion['data']['tb_valoracion_ext_monto5'] ? floatval($valoracion['data']['tb_valoracion_ext_monto5']) : '';
    $des6   = $valoracion['data']['tb_valoracion_ext_des6'] ? $valoracion['data']['tb_valoracion_ext_des6'] : '';
    $monto6 = $valoracion['data']['tb_valoracion_ext_monto6'] ? floatval($valoracion['data']['tb_valoracion_ext_monto6']) : '';
    // Montos
    $monto_final_mn = $valoracion['data']['tb_valoracion_valor_final_mn'] ? floatval($valoracion['data']['tb_valoracion_valor_final_mn']) : '';
    $monto_final_me = $valoracion['data']['tb_valoracion_valor_final_me'] ? floatval($valoracion['data']['tb_valoracion_valor_final_me']) : '';
    $monto_oferta_sc_mn = $valoracion['data']['tb_valoracion_valor_oferta_sc_mn'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_sc_mn']) : '';
    $monto_oferta_sc_me = $valoracion['data']['tb_valoracion_valor_oferta_sc_me'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_sc_me']) : '';
    $monto_oferta_cc_mn = $valoracion['data']['tb_valoracion_valor_oferta_cc_mn'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_cc_mn']) : '';
    $monto_oferta_cc_me = $valoracion['data']['tb_valoracion_valor_oferta_cc_me'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_cc_me']) : '';
  }
}

$title = 'VALORACIONES';
$codigo = "";
//$codigo = 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {

  public function Header() {
    //$image_file = K_PATH_IMAGES.'logo.jpg';
    //$this->Image($image_file, 10, 10, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.prestamosdelnorte.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.prestamosdelnorte.com');
  $pdf->SetKeywords('www.prestamosdelnorte.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  //$pdf->setLanguageArray();

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$nombre_tip_cre = 'CREDITO GARANTÍA MOBILIARIA';

  $html = '
    <style>
      .tb_crono{
        font-size: 16pt;
      }
      td{
        padding-top: 4px;
        padding-bottom: 4px;
        padding-left: 4px;
        padding-right: 4px;
      }
    </style>
  ';

  $html= '
    <table cellpadding="5">
      <tr>
        <td colspan="10" style="text-align: center;">
          <table style="padding-bottom: 5px;">
            <tr>
              <td colspan="6" style="text-align: center;"><h3><b><u>FORMATO DE VALORACIÓN</u></b></h3></td>
            </tr>
          </table>
          <table cellpadding="5">
            <tr>
              <td width="30%" style="text-align: left; border: 1px solid black;"><b>SUSCRIBE:</b></td>
              <td width="70%" style="text-align: left; border: 1px solid black;"><b>Herbert Engel Gálvez Rojas</b></td>
            </tr>
            <tr>
              <td style="text-align: left; border: 1px solid black;"><b>CARGO:</b></td>
              <td style="text-align: left; border: 1px solid black;"><b>SUB GERENTE GENERAL</b></td>
            </tr>
          </table>
        </td>
        <td colspan="6"><br/><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 60px; height: 55px; padding-top: 20px"></td>
      </tr>
    </table>
    
    <br/>

    <table cellpadding="5">
      <tr>
        <td colspan="10" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="30%" style="text-align: left; border: 1px solid black;"><b>MARCA:</b></td>
              <td width="70%" style="text-align: left; border: 1px solid black;">'.$marca.'</td>
            </tr>
            <tr>
              <td style="text-align: left; border: 1px solid black;"><b>MODELO:</b></td>
              <td style="text-align: left; border: 1px solid black;">'.$modelo.'</td>
            </tr>
          </table>
        </td>
         <td colspan="10" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="30%" style="text-align: left; border: 1px solid black;"><b>AÑO:</b></td>
              <td width="70%" style="text-align: left; border: 1px solid black;">'.$anio.'</td>
            </tr>
            <tr>
              <td style="text-align: left; border: 1px solid black;"><b>KM:</b></td>
              <td style="text-align: left; border: 1px solid black;">'.$km.'</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <br/>

    <table cellpadding="5">
      <tr>
        <td colspan="16" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black;"><b>PRUEBA MANEJO SGG:</b></td>
              <td width="50%" style="text-align: left; border: 1px solid black;">Herbert Engel Gálvez Rojas</td>
            </tr>
          </table>
        </td>
        <td colspan="10" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="60%" style="text-align: left; border: 1px solid black;"><b>FECHA PRUEBA:</b></td>
              <td width="00%" style="text-align: left; border: 1px solid black;">'.$fecha_prueba.'</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="16" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="100%" style="text-align: left; border: 1px solid black;">'.nl2br($descripcion).'</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="16" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="100%" style="text-align: center; border: 1px solid black; font-size: 32px;"><b>DEPRECIACIONES CALCULADAS POR PRUEBA DE CONDUCCIÓN</b></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="9" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="100%" style="text-align: center; border: 1px solid black;"><b>INTERNA IPDN</b></td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>ESTÉTICA</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($estetica).'</td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>ELÉCTRICA</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($electrica).'</td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>INTERIORES</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($interior).'</td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>MECÁNICA</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($mecanica).'</td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>REPUESTOS</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($repuesto).'</td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>OTROS</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($otro).'</td>
            </tr>
            <tr>
              <td width="50%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>TOTAL</b></td>
              <td width="50%" style="text-align: right; border: 1px solid black; font-size: 32px;"><b>S/ '.formato_moneda($estetica+$electrica+$interior+$mecanica+$repuesto+$otro).'</b></td>
            </tr>
          </table>
        </td>
        <td colspan="17" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="100%" style="text-align: center; border: 1px solid black;"><b>EXTERNA / TALLER CONVENIO</b></td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.$des1.'</td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($monto1).'</td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.$des2.'</td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($monto2).'</td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.$des3.'</td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($monto3).'</td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.$des4.'</td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($monto4).'</td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.$des5.'</td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($monto5).'</td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.$des6.'</td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($monto6).'</td>
            </tr>
            <tr>
              <td width="75%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>TOTAL</b></td>
              <td width="25%" style="text-align: right; border: 1px solid black; font-size: 32px;"><b>S/ '.formato_moneda($monto1+$monto2+$monto3+$monto4+$monto5+$monto6).'</b></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="30" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="70%" style="text-align: center; border: 1px solid black; font-size: 32px;"><b>OBSERVACIONES</b></td>
              <td width="30%" style="text-align: center; border: 1px solid black; font-size: 32px;"><b>DEPRECIACIÓN EXTRA (%)</b></td>
            </tr>
            <tr>
              <td width="70%" style="text-align: left; border: 1px solid black; font-size: 32px;">'.nl2br($observacion).'</td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 32px;">'.$extra.'%</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="16" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="100%" style="text-align: center; border: 1px solid black; font-size: 32px;"><b>VALORACIONES</b></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $valoracion = $oProceso->mostrarUnoValoracionById($valoracion_id);
  $valoraciones = $oProceso->mostrarValoracionDetalle($valoracion_id);

  $tip_cambio = 0.00;
  $proceso = $oProceso->mostrarUno($valoracion['data']['tb_proceso_id']);
  if($proceso['estado'] == 1){
    $tip_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);
  }
  $extra = 0.00;
  $extra_porc = 0.00;
  $extra_porcentaje = '';
  if($valoracion['estado'] == 1){
    if(floatval($valoracion['data']['tb_valoracion_extra']) > 0){
      $extra_porc = floatval($valoracion['data']['tb_valoracion_extra']);
      $extra_porcentaje = ' (Dcto: '.$extra_porc.'%)';
      $extra = 1 -((floatval($valoracion['data']['tb_valoracion_extra'])) / 100);
    }
  }

  $suma_valoracion = 0.00;
  $promedio_mn = 0.00;
  $promedio_me = 0.00;
  $depreciacion_mn_int = 0.00;
  $depreciacion_mn_ext = 0.00;
  $depreciacion_mn = 0.00;
  $depreciacion_me = 0.00;
  $valor_final_mn = 0.00;
  $valor_final_me = 0.00;
  $valor_oferta_sc_mn = 0.00;
  $valor_oferta_sc_me = 0.00;
  $valor_oferta_cc_mn = 0.00;
  $valor_oferta_cc_me = 0.00;

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="16" style="text-align: center;">
          <table cellpadding="5">
            <thead>
              <tr>
                <td width="5%" style="text-align: center; border: 1px solid black; font-size: 28px;"><b>#</b></td>
                <td width="20%" style="text-align: center; border: 1px solid black; font-size: 28px;"><b>VALORACIÓN</b></td>
                <td width="20%" style="text-align: center; border: 1px solid black; font-size: 28px;"><b>AÑO</b></td>
                <td width="20%" style="text-align: center; border: 1px solid black; font-size: 28px;"><b>KM</b></td>
                <td width="35%" style="text-align: center; border: 1px solid black; font-size: 28px;"><b>LINK</b></td>
              </tr>
            </thead>';

  $html .= '<tbody>';

						if($valoraciones['estado']==1){
							foreach ($valoraciones['data'] as $key => $value) {

								$moneda = '';
								if($value['tb_valoraciondetalle_moneda']==1){
									$moneda = 'S/ ';
								}else{
									$moneda = 'US$ ';
								}

								$html .= '<tr>
											<td width="5%" style=" border: 1px solid black; font-size: 26px;">'.($i+1).'</td>
											<td width="20%" style="text-align: right; border: 1px solid black; font-size: 26px;">'.$moneda.''.formato_moneda($value['tb_valoraciondetalle_monto']).'</td>
											<td width="20%" style="text-align: center; border: 1px solid black; font-size: 26px;">'.$value['tb_valoraciondetalle_des'].'</td>
											<td width="20%" style="text-align: center; border: 1px solid black; font-size: 26px;">'.$value['tb_valoraciondetalle_km'].' km</td>
											<td width="35%" style="text-align: left; border: 1px solid black;" font-size: 26px;>
                        <a href="'.$value['tb_valoraciondetalle_link'].'" target="_blank">'.$value['tb_valoraciondetalle_link'].'</a>
                      </td>
										</tr>';

								if($value['tb_valoraciondetalle_moneda']==1){
									$suma_valoracion = $suma_valoracion + floatval($value['tb_valoraciondetalle_monto']);
								}else{
									$suma_valoracion = $suma_valoracion + (floatval($value['tb_valoraciondetalle_monto']) * ($tip_cambio));
								}
								$i++;
							}
						}
						
	$html .= '</tbody>
					</table>
          </td>
      </tr>
    </table>
    
    <br/>';

    $promedio_mn = $suma_valoracion / $i;
    $promedio_me = ($suma_valoracion/($tip_cambio)) / $i;

    $depreciacion_mn_int = floatval($valoracion['data']['tb_valoracion_estetica']) + floatval($valoracion['data']['tb_valoracion_electrica'] + floatval($valoracion['data']['tb_valoracion_interior']) + floatval($valoracion['data']['tb_valoracion_mecanica']) + floatval($valoracion['data']['tb_valoracion_repuesto']) + floatval($valoracion['data']['tb_valoracion_otro']));
    $depreciacion_mn_ext = floatval($valoracion['data']['tb_valoracion_ext_monto1']) + floatval($valoracion['data']['tb_valoracion_ext_monto2']) + floatval($valoracion['data']['tb_valoracion_ext_monto3']) + floatval($valoracion['data']['tb_valoracion_ext_monto4']) + floatval($valoracion['data']['tb_valoracion_ext_monto5']) + floatval($valoracion['data']['tb_valoracion_ext_monto6']);
    $depreciacion_mn = $depreciacion_mn_int + $depreciacion_mn_ext;
    $depreciacion_me = $depreciacion_mn / $tip_cambio;

    if(floatval($valoracion['data']['tb_valoracion_extra']) > 0){
      $extra = 1 -((floatval($valoracion['data']['tb_valoracion_extra'])) / 100);
      $valor_final_mn = ($promedio_mn - $depreciacion_mn) * $extra;
      $valor_final_me = ($promedio_me - $depreciacion_me) * $extra;
    }else{
      $valor_final_mn = $promedio_mn - $depreciacion_mn;
      $valor_final_me = $promedio_me - $depreciacion_me;
    }

    $valor_oferta_sc_mn = $valor_final_mn * 0.5;
    $valor_oferta_sc_me = $valor_final_me * 0.5;
    $valor_oferta_cc_mn = $valor_final_mn * 0.7;
    $valor_oferta_cc_me = $valor_final_me * 0.7;

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="17" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="40%" style="text-align: center; border: 1px solid black; font-size: 32px;"></td>
              <td width="30%" style="text-align: center; border: 1px solid black; font-size: 32px;"><b>DÓLARES</b></td>
              <td width="30%" style="text-align: center; border: 1px solid black; font-size: 32px;"><b>SOLES</b></td>
            </tr>
            <tr>
              <td width="40%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>PROMEDIO</b></td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">US$ '.formato_moneda($promedio_me).'</td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($promedio_mn).'</td>
            </tr>
            <tr>
              <td width="40%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>DEPRECIACIÓN</b></td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">US$ '.formato_moneda($depreciacion_me).'</td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($depreciacion_mn).'</td>
            </tr>
          </table>
        </td>
        <td colspan="9" style="text-align: center;"></td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="17" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="40%" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>VALOR FINAL</b></td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">US$ '.formato_moneda($valor_final_me).'</td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($valor_final_mn).'</td>
            </tr>
          </table>
        </td>
        <td colspan="9" style="text-align: center;"></td>
      </tr>
    </table>
    
    <br/>';

  $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="17" style="text-align: center;">
          <table cellpadding="5">
            <tr>
              <td width="40%" rowspan="2" style="text-align: left; border: 1px solid black; font-size: 32px;"><b>VALOR OFERTA</b></td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">US$ '.formato_moneda($valor_oferta_sc_me).'</td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($valor_oferta_sc_mn).'</td>
              <td width="55%" style="text-align: right; border: 1px solid black; font-size: 28px;"><b>SIN CUSTODIA</b></td>
            </tr>
            <tr>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">US$ '.formato_moneda($valor_oferta_cc_me).'</td>
              <td width="30%" style="text-align: right; border: 1px solid black; font-size: 28px;">S/ '.formato_moneda($valor_oferta_cc_mn).'</td>
              <td width="55%" style="text-align: right; border: 1px solid black; font-size: 28px;"><b>CON CUSTODIA</b></td>
            </tr>
          </table>
        </td>
        <td colspan="9" style="text-align: center;"></td>
      </tr>
    </table>
    
    <br/>
    <br/>
    <br/>';

    $html .= '
    <table cellpadding="5">
      <tr>
        <td colspan="30" style="text-align: center;">
          <table cellpadding="9">
            <tr>
              <td width="100%" style="text-align: center; border: 1px solid black;"><b>IMÁGENES</b></td>
            </tr>';
            $imagenes = $oProceso->listar_valoracionfiles($valoracion_id);
            if($imagenes['estado'] == 1){
              foreach ($imagenes['data'] as $key => $value) {
                $html .= '
                  <tr>
                    <td width="100%" style="text-align: center; border: 1px solid black;">
                      <img src="../../'.$value['tb_valoracionfile_url'].'" style="with: 100%; height: auto; padding-top: 20px">
                    </td>
                  </tr>';
              }
              
            }else{
              $html .= '
              <tr>
                <td width="100%" style="text-align: center; border: 1px solid black; font-size: 32px;"><i>No hay imágenes</i></td>
              </tr>';
            }
           
    $html .= '</table>
        </td>
      </tr>
    </table>
    
    <br/>';

  $html .= '

    <table width="100%">
      <tr width="100%">
        <td colspan="17" style="text-align: center;">
          <table>
            <tr>
              <td colspan="20" align="center" style="position: relative !important;">
              <img src="../../'.$firma_engel.'" style="with: 150px; height: 75px; position: absolute !important; bottom: -40px; left: 50%; transform: translateY(-50%);">
              </td>
            </tr>
            <tr>
              <td colspan="20" align="center" style="padding-top: 60px;"><b>_______________________________________ <br/>
              GALVEZ ROJAS HERBERT ENGEL <br/>
              SUB GRTE GRAL INVERSIONES Y PRÉSTAMOS DEL NORTE SAC
              </b></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  ';



// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$title."_".$dni.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>

