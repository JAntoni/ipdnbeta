<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Proceso extends Conexion {

    function insertar($usuario_id, $creditotipo_id, $cgarvtipo_id, $precliente_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_proceso (
                                        tb_proceso_xac, 
                                        tb_proceso_fecreg,
                                        tb_usuario_id, 
                                        tb_cliente_id, 
                                        tb_precliente_id, 
                                        tb_credito_id, 
                                        tb_creditotipo_id, 
                                        tb_cgarvtipo_id, 
                                        tb_proceso_moneda,
                                        tb_proceso_tip_cambio,
                                        tb_proceso_plazo,
                                        tb_proceso_tasa,
                                        tb_proceso_monto,
                                        tb_proceso_inicial_porcen,
                                        tb_proceso_inicial,
                                        tb_proceso_prestamo,
                                        tb_proceso_banca,
                                        tb_proceso_ing_det,
                                        tb_proceso_fec_inspeccion,
                                        tb_proceso_prueba_veh,
                                        tb_proceso_cli_acepta,
                                        tb_proceso_cli_acepta_det,
                                        tb_proceso_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :usuario_id, 
                                        NULL, 
                                        :precliente_id, 
                                        NULL, 
                                        :creditotipo_id, 
                                        :cgarvtipo_id, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        0,
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cgarvtipo_id", $cgarvtipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":precliente_id", $precliente_id, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $proceso_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['proceso_id'] = $proceso_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarProcesoFase($proceso_id, $fase_id, $orden, $procede) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_proceso_fase (
                                        tb_proceso_fase_xac, 
                                        tb_proceso_fase_fecreg,
                                        tb_proceso_id, 
                                        tb_fase_id, 
                                        tb_proceso_fase_obs, 
                                        tb_proceso_fase_orden, 
                                        tb_proceso_fase_proc, 
                                        tb_proceso_fase_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :fase_id, 
                                        '', 
                                        :orden, 
                                        :procede,
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fase_id", $fase_id, PDO::PARAM_INT);
            $sentencia->bindParam(":orden", $orden, PDO::PARAM_INT);
            $sentencia->bindParam(":procede", $procede, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $proceso_fase_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['proceso_fase_id'] = $proceso_fase_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarProcesoFaseItem($proceso_fase_id, $item_id, $orden) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_proceso_fase_item (
                                        tb_proceso_fase_item_xac, 
                                        tb_proceso_fase_item_fecreg,
                                        tb_proceso_fase_id, 
                                        tb_item_id, 
                                        tb_proceso_fase_item_valor, 
                                        tb_proceso_fase_item_orden, 
                                        tb_proceso_fase_item_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_fase_id, 
                                        :item_id, 
                                        NULL, 
                                        :orden, 
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
            $sentencia->bindParam(":item_id", $item_id, PDO::PARAM_INT);
            $sentencia->bindParam(":orden", $orden, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarComentario($proceso_fase_id, $usuario_id, $txt_comentario, $doc_comentario) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_comentario (
                                        tb_comentario_xac, 
                                        tb_comentario_fecreg,
                                        tb_proceso_fase_id, 
                                        tb_usuario_id, 
                                        tb_comentario_des, 
                                        tb_comentario_doc, 
                                        tb_comentario_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_fase_id, 
                                        :usuario_id, 
                                        :txt_comentario, 
                                        :doc_comentario,
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":txt_comentario", $txt_comentario, PDO::PARAM_STR);
            $sentencia->bindParam(":doc_comentario", $doc_comentario, PDO::PARAM_STR);
            $result = $sentencia->execute();

            $comentario_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['comentario_id'] = $comentario_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarDocExoneracion($proceso_id, $item_id, $usuario_id, $url, $his) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_multimedia (
                                        tb_multimedia_xac, 
                                        tb_multimedia_fecreg,
                                        tb_proceso_id, 
                                        tb_item_id, 
                                        tb_usuario_id, 
                                        tb_multimedia_url, 
                                        tb_multimedia_obs, 
                                        tb_multimedia_his, 
                                        tb_multimedia_exo, 
                                        tb_multimedia_exomot, 
                                        tb_multimedia_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :item_id, 
                                        :usuario_id, 
                                        :url, 
                                        NULL,
                                        :his,
                                        0,
                                        NULL,
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":item_id", $item_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":url", $url, PDO::PARAM_STR);
            $sentencia->bindParam(":his", $his, PDO::PARAM_STR);
            $result = $sentencia->execute();

            $tb_multimedia_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['tb_multimedia_id'] = $tb_multimedia_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarDocProcesoFaseItem($proceso_id, $item_id, $proceso_fase_id, $usuario_id, $his, $directorio, $tempFile, $fileParts) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_multimedia (
                                        tb_multimedia_xac, 
                                        tb_multimedia_fecreg,
                                        tb_proceso_id, 
                                        tb_item_id, 
                                        tb_usuario_id, 
                                        tb_multimedia_url, 
                                        tb_multimedia_obs, 
                                        tb_multimedia_his, 
                                        tb_multimedia_exo, 
                                        tb_multimedia_exomot, 
                                        tb_multimedia_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :item_id, 
                                        :usuario_id, 
                                        '...', 
                                        NULL,
                                        :his,
                                        0,
                                        NULL,
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":item_id", $item_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":his", $his, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $tb_multimedia_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $nombre_imagen = 'proceso_'.$proceso_id.'_item_'.$item_id.'_doc_'. $tb_multimedia_id .'.'. $fileParts['extension'];
            $url_doc = $directorio. strtolower($nombre_imagen);

            if(move_uploaded_file($tempFile, '../../'. $url_doc)){
                //insertamos las imágenes
                $sql = "UPDATE tb_proc_multimedia SET tb_multimedia_url =:tb_multimedia_url WHERE tb_multimedia_id =:tb_multimedia_id";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_multimedia_id", $tb_multimedia_id, PDO::PARAM_INT);
                $sentencia->bindParam(":tb_multimedia_url", $url_doc, PDO::PARAM_STR);
                $result2 = $sentencia->execute();
            }else{
                $this->eliminarDocProcesoFaseItem($tb_multimedia_id);
            }
            
            if($result){
                $data['estado'] = 1; //si es correcto el ingreso retorna 1
            }else{
                $data['estado'] = 0; //si es incorrecto el ingreso retorna 0
            }
            $data['multimedia_id'] = $tb_multimedia_id;
            $data['proceso_fase_id'] = $proceso_fase_id;
            return $data;
            //return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarComiteVoto($proceso_id, $usuario_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_comitevoto (
                                        tb_comitevoto_xac, 
                                        tb_comitevoto_fecreg,
                                        tb_proceso_id, 
                                        tb_usuario_id, 
                                        tb_comitevoto_voto)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :usuario_id, 
                                        0);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $tb_multimedia_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarCheque($proceso_id, $usuario_id, $banco_id, $moneda, $enlace, $detalle) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_cheque (
                                        tb_cheque_xac, 
                                        tb_cheque_fecreg,
                                        tb_proceso_id, 
                                        tb_usuario_id, 
                                        tb_cheque_banco, 
                                        tb_cheque_moneda, 
                                        tb_cheque_conector,
                                        tb_cheque_det,
                                        tb_cheque_aprobado)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :usuario_id, 
                                        :cheque_banco, 
                                        :cheque_moneda, 
                                        :cheque_conector,
                                        :cheque_det,
                                        NULL);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cheque_banco", $banco_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cheque_moneda", $moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":cheque_conector", $enlace, PDO::PARAM_STR);
            $sentencia->bindParam(":cheque_det", $detalle, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $cheque_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['cheque_id'] = $cheque_id;

            return $data;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    

    function registrarChequeDetalle($cheque_id, $beneficiario, $monto, $tipo) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_chequedetalle (
                                        tb_chequedetalle_xac, 
                                        tb_chequedetalle_fecreg,
                                        tb_cheque_id, 
                                        tb_chequedetalle_bene, 
                                        tb_chequedetalle_monto, 
                                        tb_chequedetalle_tipo, 
                                        tb_chequedetalle_nro_cheque, 
                                        tb_chequedetalle_uso, 
                                        tb_empresa_id)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :cheque_id, 
                                        :beneficiario, 
                                        :monto, 
                                        :tipo, 
                                        NULL, 
                                        NULL, 
                                        NULL);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);
            $sentencia->bindParam(":beneficiario", $beneficiario, PDO::PARAM_STR);
            $sentencia->bindParam(":monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function insertarPreCalificación($proceso_id, $modalidad , $tipo_cliente, $nacionalidad, $edad, $razon, $ruc, $cliente, $dni, $marca, $modelo, $proveedor_id, $anio, $licencia, $tipo_licencia, $vigencia_licencia, $papeleta, $monto, $cuota, $tasa, $plazo, $km, $placa, $precio_propuesto, $verificado, $metodo_verificado, $tipo_operacion, $comentario, $vigencia_str, $vigencia_gps, $inicial_moneda, $inicial_porcentaje, $inicial_monto, $fec_inicial, $inicial_est, $finalidad, $moneda, $uso, $ocupacion, $grado_instruccion, $carga_familiar, $carga_familiar_e, $descuento, $estado_civil, $gastos, $direccion, $vivienda, $tipo_resi, $tipo_describir, $fec_inspeccion, $uso_vehiculo, $zona_vehiculo, $semaforizacion, $score, $cargo_antiguedad, $ingreso_forma, $casa_antiguedad) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_precalificacion (
                                        tb_precalificacion_xac, 
                                        tb_precalificacion_fecreg,
                                        tb_proceso_id, 
                                        tb_precalificacion_modalidad, 
                                        tb_precalificacion_tipo_cliente, 
                                        tb_precalificacion_nacionalidad, 
                                        tb_precalificacion_edad, 
                                        tb_precalificacion_razon_social, 
                                        tb_precalificacion_ruc, 
                                        tb_precalificacion_cliente, 
                                        tb_precalificacion_dni, 
                                        tb_precalificacion_marca, 
                                        tb_precalificacion_modelo, 
                                        tb_precalificacion_proveedor_id, 
                                        tb_precalificacion_anio, 
                                        tb_precalificacion_licencia, 
                                        tb_precalificacion_tipo_licencia, 
                                        tb_precalificacion_licencia_vigencia, 
                                        tb_precalificacion_papeleta, 
                                        tb_precalificacion_monto, 
                                        tb_precalificacion_cuota, 
                                        tb_precalificacion_tasa, 
                                        tb_precalificacion_plazo, 
                                        tb_precalificacion_km, 
                                        tb_precalificacion_placa, 
                                        tb_precalificacion_precio_propuesto, 
                                        tb_precalificacion_verificado, 
                                        tb_precalificacion_verificado_metodo, 
                                        tb_precalificacion_tipo_operacion, 
                                        tb_precalificacion_comentario, 
                                        tb_precalificacion_vigencia_str, 
                                        tb_precalificacion_vigencia_gps, 
                                        tb_precalificacion_inicial_moneda, 
                                        tb_precalificacion_inicial_porcen, 
                                        tb_precalificacion_inicial_monto, 
                                        tb_precalificacion_fec_inicial,
                                        tb_precalificacion_inicial_est,
                                        tb_precalificacion_finalidad, 
                                        tb_precalificacion_moneda_credito, 
                                        tb_precalificacion_uso, 
                                        tb_precalificacion_ocupacion, 
                                        tb_precalificacion_grado, 
                                        tb_precalificacion_carga_familiar, 
                                        tb_precalificacion_edad_escolar, 
                                        tb_precalificacion_descuento, 
                                        tb_precalificacion_estado_civil, 
                                        tb_precalificacion_gastos, 
                                        tb_precalificacion_direccion, 
                                        tb_precalificacion_vivienda, 
                                        tb_precalificacion_tipo_resid, 
                                        tb_precalificacion_resid_otro,
                                        tb_precalificacion_fec_inspeccion,
                                        tb_precalificacion_est,
                                        tb_precalificacion_uso_vehiculo,
                                        tb_precalificacion_zona_vehiculo,
                                        tb_precalificacion_semaforizacion,
                                        tb_precalificacion_score,
                                        tb_precalificacion_cargo_antig,
                                        tb_precalificacion_ingreso_forma,
                                        tb_precalificacion_casa_antig)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :modalidad, 
                                        :tipo_cliente, 
                                        :nacionalidad, 
                                        :edad, 
                                        :razon, 
                                        :ruc, 
                                        :cliente, 
                                        :dni, 
                                        :marca, 
                                        :modelo, 
                                        :proveedor_id, 
                                        :anio, 
                                        :licencia, 
                                        :tipo_licencia, 
                                        :vigencia_licencia, 
                                        :papeleta, 
                                        :monto, 
                                        :cuota, 
                                        :tasa, 
                                        :plazo, 
                                        :km, 
                                        :placa, 
                                        :precio_propuesto, 
                                        :verificado, 
                                        :metodo_verificado, 
                                        :tipo_operacion, 
                                        :comentario, 
                                        :vigencia_str, 
                                        :vigencia_gps, 
                                        :inicial_moneda, 
                                        :inicial_porcentaje, 
                                        :inicial_monto, 
                                        :fec_inicial,
                                        :inicial_est,
                                        :finalidad, 
                                        :moneda, 
                                        :uso, 
                                        :ocupacion, 
                                        :grado_instruccion, 
                                        :carga_familiar, 
                                        :carga_familiar_e, 
                                        :descuento, 
                                        :estado_civil, 
                                        :gastos, 
                                        :direccion, 
                                        :vivienda, 
                                        :tipo_resi, 
                                        :tipo_describir, 
                                        :fec_inspeccion,
                                        1,
                                        :uso_vehiculo,
                                        :zona_vehiculo,
                                        :semaforizacion,
                                        :score,
                                        :cargo_antig,
                                        :ingreso_forma,
                                        :casa_antig);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":modalidad", $modalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_cliente", $tipo_cliente, PDO::PARAM_STR);
            $sentencia->bindParam(":nacionalidad", $nacionalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":edad", $edad, PDO::PARAM_INT);
            $sentencia->bindParam(":razon", $razon, PDO::PARAM_STR);
            $sentencia->bindParam(":ruc", $ruc, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente", $cliente, PDO::PARAM_STR);
            $sentencia->bindParam(":dni", $dni, PDO::PARAM_INT);
            $sentencia->bindParam(":marca", $marca, PDO::PARAM_INT);
            $sentencia->bindParam(":modelo", $modelo, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
            $sentencia->bindParam(":anio", $anio, PDO::PARAM_INT);
            $sentencia->bindParam(":licencia", $licencia, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_licencia", $tipo_licencia, PDO::PARAM_STR);
            $sentencia->bindParam(":vigencia_licencia", $vigencia_licencia, PDO::PARAM_STR);
            $sentencia->bindParam(":papeleta", $papeleta, PDO::PARAM_STR);
            $sentencia->bindParam(":monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":cuota", $cuota, PDO::PARAM_INT);
            $sentencia->bindParam(":tasa", $tasa, PDO::PARAM_INT);
            $sentencia->bindParam(":plazo", $plazo, PDO::PARAM_INT);
            $sentencia->bindParam(":km", $km, PDO::PARAM_INT);
            $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
            $sentencia->bindParam(":precio_propuesto", $precio_propuesto, PDO::PARAM_INT);
            $sentencia->bindParam(":verificado", $verificado, PDO::PARAM_STR);
            $sentencia->bindParam(":metodo_verificado", $metodo_verificado, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_operacion", $tipo_operacion, PDO::PARAM_INT);
            $sentencia->bindParam(":comentario", $comentario, PDO::PARAM_STR);
            $sentencia->bindParam(":vigencia_str", $vigencia_str, PDO::PARAM_STR);
            $sentencia->bindParam(":vigencia_gps", $vigencia_gps, PDO::PARAM_STR);
            $sentencia->bindParam(":inicial_moneda", $inicial_moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":inicial_porcentaje", $inicial_porcentaje, PDO::PARAM_INT);
            $sentencia->bindParam(":inicial_monto", $inicial_monto, PDO::PARAM_INT);
            $sentencia->bindParam(":fec_inicial", $fec_inicial, PDO::PARAM_STR);
            $sentencia->bindParam(":inicial_est", $inicial_est, PDO::PARAM_STR);
            $sentencia->bindParam(":finalidad", $finalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda", $moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":uso", $uso, PDO::PARAM_STR);
            $sentencia->bindParam(":ocupacion", $ocupacion, PDO::PARAM_STR);
            $sentencia->bindParam(":grado_instruccion", $grado_instruccion, PDO::PARAM_STR);
            $sentencia->bindParam(":carga_familiar", $carga_familiar, PDO::PARAM_STR);
            $sentencia->bindParam(":carga_familiar_e", $carga_familiar_e, PDO::PARAM_STR);
            $sentencia->bindParam(":descuento", $descuento, PDO::PARAM_INT);
            $sentencia->bindParam(":estado_civil", $estado_civil, PDO::PARAM_STR);
            $sentencia->bindParam(":gastos", $gastos, PDO::PARAM_STR);
            $sentencia->bindParam(":direccion", $direccion, PDO::PARAM_STR);
            $sentencia->bindParam(":vivienda", $vivienda, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_resi", $tipo_resi, PDO::PARAM_INT);
            $sentencia->bindParam(":tipo_describir", $tipo_describir, PDO::PARAM_STR);
            $sentencia->bindParam(":fec_inspeccion", $fec_inspeccion, PDO::PARAM_STR);
            $sentencia->bindParam(":uso_vehiculo", $uso_vehiculo, PDO::PARAM_STR);
            $sentencia->bindParam(":zona_vehiculo", $zona_vehiculo, PDO::PARAM_STR);
            $sentencia->bindParam(":semaforizacion", $semaforizacion, PDO::PARAM_STR);
            $sentencia->bindParam(":score", $score, PDO::PARAM_INT);
            $sentencia->bindParam(":cargo_antig", $cargo_antiguedad, PDO::PARAM_STR);
            $sentencia->bindParam(":ingreso_forma", $ingreso_forma, PDO::PARAM_STR);
            $sentencia->bindParam(":casa_antig", $casa_antiguedad, PDO::PARAM_STR);


            $result = $sentencia->execute();
            $precalificacion_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['precalificacion_id'] = $precalificacion_id;

            return $data;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarIngresoCliente($proceso_id, $cliente_id, $moneda, $monto, $origen, $antiguedad, $sustento, $verificacion) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_ingresoegreso (
                                        tb_ingresoegreso_xac, 
                                        tb_ingresoegreso_fecreg,
                                        tb_proceso_id, 
                                        tb_cliente_id, 
                                        tb_ingresoegreso_tipo, 
                                        tb_ingresoegreso_moneda, 
                                        tb_ingresoegreso_monto, 
                                        tb_ingresoegreso_montome, 
                                        tb_ingresoegreso_subtipo, 
                                        tb_ingresoegreso_entidad, 
                                        tb_ingresoegreso_det, 
                                        tb_ingresoegreso_origen, 
                                        tb_ingresoegreso_finalidad, 
                                        tb_ingresoegreso_antiguedad, 
                                        tb_ingresoegreso_frecuencia, 
                                        tb_ingresoegreso_sustento, 
                                        tb_ingresoegreso_verificacion, 
                                        tb_ingresoegreso_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :cliente_id, 
                                        'I', 
                                        :moneda, 
                                        :monto, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        NULL, 
                                        :origen, 
                                        NULL, 
                                        :antiguedad, 
                                        NULL, 
                                        :sustento, 
                                        :verificacion, 
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda", $moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":origen", $origen, PDO::PARAM_STR);
            $sentencia->bindParam(":antiguedad", $antiguedad, PDO::PARAM_STR);
            $sentencia->bindParam(":sustento", $sustento, PDO::PARAM_STR);
            $sentencia->bindParam(":verificacion", $verificacion, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarParticipanteCliente($proceso_id, $nombre, $documento, $celular) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_participante (
                                        tb_participante_xac, 
                                        tb_participante_fecreg,
                                        tb_proceso_id, 
                                        tb_participante_nom, 
                                        tb_participante_doc, 
                                        tb_participante_tel)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :nombre, 
                                        :documento, 
                                        :celular);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $sentencia->bindParam(":documento", $documento, PDO::PARAM_STR);
            $sentencia->bindParam(":celular", $celular, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarEgresoCliente($proceso_id, $cliente_id, $moneda, $monto, $montome, $subtipo, $entidad, $detalle, $finalidad, $frecuencia, $sustento, $verificacion) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_ingresoegreso (
                                        tb_ingresoegreso_xac, 
                                        tb_ingresoegreso_fecreg,
                                        tb_proceso_id, 
                                        tb_cliente_id, 
                                        tb_ingresoegreso_tipo, 
                                        tb_ingresoegreso_moneda, 
                                        tb_ingresoegreso_monto, 
                                        tb_ingresoegreso_montome, 
                                        tb_ingresoegreso_subtipo, 
                                        tb_ingresoegreso_entidad, 
                                        tb_ingresoegreso_det, 
                                        tb_ingresoegreso_origen, 
                                        tb_ingresoegreso_finalidad, 
                                        tb_ingresoegreso_antiguedad, 
                                        tb_ingresoegreso_frecuencia, 
                                        tb_ingresoegreso_sustento, 
                                        tb_ingresoegreso_verificacion, 
                                        tb_ingresoegreso_est)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :cliente_id, 
                                        'E', 
                                        :moneda, 
                                        :monto, 
                                        :montome, 
                                        :subtipo, 
                                        :entidad, 
                                        :detalle, 
                                        NULL, 
                                        :finalidad, 
                                        NULL, 
                                        :frecuencia, 
                                        :sustento, 
                                        :verificacion, 
                                        1);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda", $moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":montome", $montome, PDO::PARAM_INT);
            $sentencia->bindParam(":subtipo", $subtipo, PDO::PARAM_STR);
            $sentencia->bindParam(":entidad", $entidad, PDO::PARAM_STR);
            $sentencia->bindParam(":detalle", $detalle, PDO::PARAM_STR);
            $sentencia->bindParam(":finalidad", $finalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":frecuencia", $frecuencia, PDO::PARAM_STR);
            $sentencia->bindParam(":sustento", $sustento, PDO::PARAM_STR);
            $sentencia->bindParam(":verificacion", $verificacion, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificarPreCalificación($precalificacion_id, $proceso_id, $modalidad , $tipo_cliente, $nacionalidad, $edad, $razon, $ruc, $cliente, $dni, $marca, $modelo, $proveedor_id, $anio, $licencia, $tipo_licencia, $vigencia_licencia, $papeleta, $monto, $cuota, $tasa, $plazo, $km, $placa, $precio_propuesto, $verificado, $metodo_verificado, $tipo_operacion, $comentario, $vigencia_str, $vigencia_gps, $inicial_moneda, $inicial_porcentaje, $inicial_monto, $fec_inicial, $inicial_est, $finalidad, $moneda, $uso, $ocupacion, $grado_instruccion, $carga_familiar, $carga_familiar_e, $descuento, $estado_civil, $gastos, $direccion, $vivienda, $tipo_resi, $tipo_describir, $fec_inspeccion, $uso_vehiculo, $zona_vehiculo, $semaforizacion, $score, $cargo_antiguedad, $ingreso_forma, $casa_antiguedad) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_precalificacion SET
                            tb_proceso_id = :proceso_id, 
                            tb_precalificacion_modalidad = :modalidad, 
                            tb_precalificacion_tipo_cliente = :tipo_cliente, 
                            tb_precalificacion_nacionalidad = :nacionalidad, 
                            tb_precalificacion_edad = :edad, 
                            tb_precalificacion_razon_social = :razon, 
                            tb_precalificacion_ruc = :ruc, 
                            tb_precalificacion_cliente = :cliente, 
                            tb_precalificacion_dni = :dni, 
                            tb_precalificacion_marca = :marca, 
                            tb_precalificacion_modelo = :modelo, 
                            tb_precalificacion_proveedor_id = :proveedor_id, 
                            tb_precalificacion_anio = :anio,
                            tb_precalificacion_licencia = :licencia,
                            tb_precalificacion_tipo_licencia = :tipo_licencia, 
                            tb_precalificacion_licencia_vigencia = :vigencia_licencia,
                            tb_precalificacion_papeleta = :papeleta, 
                            tb_precalificacion_monto = :monto,
                            tb_precalificacion_cuota = :cuota,
                            tb_precalificacion_tasa = :tasa, 
                            tb_precalificacion_plazo = :plazo, 
                            tb_precalificacion_km = :km,
                            tb_precalificacion_placa = :placa, 
                            tb_precalificacion_precio_propuesto = :precio_propuesto, 
                            tb_precalificacion_verificado = :verificado, 
                            tb_precalificacion_verificado_metodo = :metodo_verificado, 
                            tb_precalificacion_tipo_operacion = :tipo_operacion, 
                            tb_precalificacion_comentario = :comentario,
                            tb_precalificacion_vigencia_str = :vigencia_str,
                            tb_precalificacion_vigencia_gps = :vigencia_gps, 
                            tb_precalificacion_inicial_moneda = :inicial_moneda, 
                            tb_precalificacion_inicial_porcen = :inicial_porcentaje, 
                            tb_precalificacion_inicial_monto = :inicial_monto, 
                            tb_precalificacion_fec_inicial = :fec_inicial,
                            tb_precalificacion_inicial_est = :inicial_est,
                            tb_precalificacion_finalidad = :finalidad, 
                            tb_precalificacion_moneda_credito = :moneda, 
                            tb_precalificacion_uso = :uso,
                            tb_precalificacion_ocupacion = :ocupacion, 
                            tb_precalificacion_grado = :grado_instruccion, 
                            tb_precalificacion_carga_familiar = :carga_familiar,  
                            tb_precalificacion_edad_escolar = :carga_familiar_e, 
                            tb_precalificacion_descuento = :descuento, 
                            tb_precalificacion_estado_civil = :estado_civil, 
                            tb_precalificacion_gastos = :gastos,
                            tb_precalificacion_direccion = :direccion,
                            tb_precalificacion_vivienda = :vivienda,
                            tb_precalificacion_tipo_resid = :tipo_resi,
                            tb_precalificacion_resid_otro = :tipo_describir, 
                            tb_precalificacion_fec_inspeccion = :fec_inspeccion,
                            tb_precalificacion_uso_vehiculo = :uso_vehiculo,
                            tb_precalificacion_zona_vehiculo = :zona_vehiculo,
                            tb_precalificacion_semaforizacion = :semaforizacion,
                            tb_precalificacion_score = :score,
                            tb_precalificacion_cargo_antig = :cargo_antig,
                            tb_precalificacion_ingreso_forma = :ingreso_forma,
                            tb_precalificacion_casa_antig = :casa_antig WHERE tb_precalificacion_id = :precalificacion_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":modalidad", $modalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_cliente", $tipo_cliente, PDO::PARAM_STR);
            $sentencia->bindParam(":nacionalidad", $nacionalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":edad", $edad, PDO::PARAM_INT);
            $sentencia->bindParam(":razon", $razon, PDO::PARAM_STR);
            $sentencia->bindParam(":ruc", $ruc, PDO::PARAM_STR);
            $sentencia->bindParam(":cliente", $cliente, PDO::PARAM_STR);
            $sentencia->bindParam(":dni", $dni, PDO::PARAM_INT);
            $sentencia->bindParam(":marca", $marca, PDO::PARAM_INT);
            $sentencia->bindParam(":modelo", $modelo, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
            $sentencia->bindParam(":anio", $anio, PDO::PARAM_INT);
            $sentencia->bindParam(":licencia", $licencia, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_licencia", $tipo_licencia, PDO::PARAM_STR);
            $sentencia->bindParam(":vigencia_licencia", $vigencia_licencia, PDO::PARAM_STR);
            $sentencia->bindParam(":papeleta", $papeleta, PDO::PARAM_STR);
            $sentencia->bindParam(":monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":cuota", $cuota, PDO::PARAM_INT);
            $sentencia->bindParam(":tasa", $tasa, PDO::PARAM_INT);
            $sentencia->bindParam(":plazo", $plazo, PDO::PARAM_INT);
            $sentencia->bindParam(":km", $km, PDO::PARAM_INT);
            $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
            $sentencia->bindParam(":precio_propuesto", $precio_propuesto, PDO::PARAM_INT);
            $sentencia->bindParam(":verificado", $verificado, PDO::PARAM_STR);
            $sentencia->bindParam(":metodo_verificado", $metodo_verificado, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_operacion", $tipo_operacion, PDO::PARAM_INT);
            $sentencia->bindParam(":comentario", $comentario, PDO::PARAM_STR);
            $sentencia->bindParam(":vigencia_str", $vigencia_str, PDO::PARAM_STR);
            $sentencia->bindParam(":vigencia_gps", $vigencia_gps, PDO::PARAM_STR);
            $sentencia->bindParam(":inicial_moneda", $inicial_moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":inicial_porcentaje", $inicial_porcentaje, PDO::PARAM_INT);
            $sentencia->bindParam(":inicial_monto", $inicial_monto, PDO::PARAM_INT);
            $sentencia->bindParam(":fec_inicial", $fec_inicial, PDO::PARAM_STR);
            $sentencia->bindParam(":inicial_est", $inicial_est, PDO::PARAM_STR);
            $sentencia->bindParam(":finalidad", $finalidad, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda", $moneda, PDO::PARAM_STR);
            $sentencia->bindParam(":uso", $uso, PDO::PARAM_STR);
            $sentencia->bindParam(":ocupacion", $ocupacion, PDO::PARAM_STR);
            $sentencia->bindParam(":grado_instruccion", $grado_instruccion, PDO::PARAM_STR);
            $sentencia->bindParam(":carga_familiar", $carga_familiar, PDO::PARAM_STR);
            $sentencia->bindParam(":carga_familiar_e", $carga_familiar_e, PDO::PARAM_STR);
            $sentencia->bindParam(":descuento", $descuento, PDO::PARAM_INT);
            $sentencia->bindParam(":estado_civil", $estado_civil, PDO::PARAM_STR);
            $sentencia->bindParam(":gastos", $gastos, PDO::PARAM_STR);
            $sentencia->bindParam(":direccion", $direccion, PDO::PARAM_STR);
            $sentencia->bindParam(":vivienda", $vivienda, PDO::PARAM_STR);
            $sentencia->bindParam(":tipo_resi", $tipo_resi, PDO::PARAM_INT);
            $sentencia->bindParam(":tipo_describir", $tipo_describir, PDO::PARAM_STR);
            $sentencia->bindParam(":fec_inspeccion", $fec_inspeccion, PDO::PARAM_STR);
            $sentencia->bindParam(":uso_vehiculo", $uso_vehiculo, PDO::PARAM_STR);
            $sentencia->bindParam(":zona_vehiculo", $zona_vehiculo, PDO::PARAM_STR);
            $sentencia->bindParam(":semaforizacion", $semaforizacion, PDO::PARAM_STR);
            $sentencia->bindParam(":score", $score, PDO::PARAM_INT);
            $sentencia->bindParam(":cargo_antig", $cargo_antiguedad, PDO::PARAM_STR);
            $sentencia->bindParam(":ingreso_forma", $ingreso_forma, PDO::PARAM_STR);
            $sentencia->bindParam(":casa_antig", $casa_antiguedad, PDO::PARAM_STR);
            $sentencia->bindParam(":precalificacion_id", $precalificacion_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $this->dblink->commit();

            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['precalificacion_id'] = $precalificacion_id;

            return $data;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrar_proceso_paginado($fecini, $fecfin, $cre_id, $use_id, $cli_id, $estado, $inicio, $cantidad, $total=false) {
        try {
            $sql = "SELECT pro.*, CONCAT(usu.tb_usuario_nom,' ',usu.tb_usuario_ape) AS asesor, cli.tb_cliente_nom, cre.tb_credito_est, ctip.tb_creditotipo_nom, pcli.tb_precliente_nombres, cgarv.cgarvtipo_nombre ";
		
            if($total){
                $sql="SELECT COUNT(DISTINCT(pro.tb_proceso_id)) ";
            }

            $sql .= "FROM tb_proc_proceso pro 
                    LEFT JOIN tb_creditogarveh cre on pro.tb_credito_id = cre.tb_credito_id 
                    INNER JOIN tb_usuario usu on pro.tb_usuario_id = usu.tb_usuario_id
                    LEFT JOIN tb_cliente cli on pro.tb_cliente_id = cli.tb_cliente_id
                    LEFT JOIN tb_precliente pcli on pro.tb_precliente_id = pcli.tb_precliente_id
                    INNER JOIN tb_creditotipo ctip on pro.tb_creditotipo_id = ctip.tb_creditotipo_id
                    INNER JOIN cgarvtipo cgarv on pro.tb_cgarvtipo_id = cgarv.cgarvtipo_id
                    WHERE pro.tb_proceso_xac = 1";
            if($fecini!= '' || $fecini != null)
                $sql .= ' AND pro.tb_proceso_fecreg >= :fecini ';
            
            if($fecfin!= '' || $fecfin != null)
                $sql .= ' AND pro.tb_proceso_fecreg <= :fecfin ';
            
            if($cre_id > 0)
                $sql .= ' AND pro.tb_credito_id =:tb_credito_id';
            
            if($use_id > 0)
                $sql .= ' AND pro.tb_usuario_id =:tb_usuario_id';
            
            if($cli_id > 0)
                $sql .= ' AND pro.tb_cliente_id =:tb_cliente_id';
            
            if($estado > 0)
                if($estado == 11){
                    $sql .= ' AND pro.tb_proceso_est = 4';
                }else{
                    $sql .= ' AND cre.tb_credito_est =:tb_estado';
                }

            if(!$total){
                $sql .= ' GROUP BY pro.tb_proceso_id order by pro.tb_proceso_id DESC';
			    $sql .= " LIMIT ".$inicio.",".$cantidad.";";
            }

            $sentencia = $this->dblink->prepare($sql);
            if($fecini!= '' || $fecini != null)
                $sentencia->bindParam(":fecini", $fecini, PDO::PARAM_STR);
            if($fecfin!= '' || $fecfin != null)
                $sentencia->bindParam(":fecfin", $fecfin, PDO::PARAM_STR);
            if($cre_id > 0)
                $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
            if($use_id > 0)
                $sentencia->bindParam(":tb_usuario_id", $use_id, PDO::PARAM_INT);
            if($cli_id > 0)
                $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            if($estado > 0)
                if($estado != 11){
                    $sentencia->bindParam(":tb_estado", $estado, PDO::PARAM_INT);
                }

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                if($total){
                    $resultado = $sentencia->fetch(PDO::FETCH_NUM);
                    $resultado = $resultado[0];
                }else{
                    $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                }
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay procesos registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_cgarvehtipos(){
        try {
          $sql = "SELECT * FROM cgarvtipo WHERE cgarvtipo_xac = 1";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de garantía vehicular registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function mostrarUnoGarvtipo($cgarvtipo_id){
        try {
          $sql = "SELECT * FROM cgarvtipo WHERE cgarvtipo_id = :cgarvtipo_id AND cgarvtipo_xac = 1";
  
          $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cgarvtipo_id", $cgarvtipo_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos de Garantía Tipo";
                $retorno["data"] = "";
            }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function obtenerFases($creditotipo_id, $cgarvtipo_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_fase
                    WHERE tb_fase_xac = 1 AND tb_creditotipo_id = :tb_creditotipo_id AND tb_cgarvtipo_id = :tb_cgarvtipo_id";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cgarvtipo_id", $cgarvtipo_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay fases registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerItemPorIdx($idx) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_item
                    WHERE tb_item_xac = 1 AND tb_item_idx = :tb_item_idx";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_item_idx", $idx, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay item registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerItemPorId($id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_item
                    WHERE tb_item_xac = 1 AND tb_item_id = :tb_item_id";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_item_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay item registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerItemsPorProcesoFase($proceso_fase_id) {
        try {
            $sql = "SELECT 
                        pfi.*, 
                        m.tb_multimedia_exo,
                        m.tb_multimedia_exomot,
                        m.tb_multimedia_est,
                        m.tb_multimedia_xac,
                        m.tb_multimedia_id,
                        m.tb_multimedia_url,
                        pf.tb_proceso_id,
                        it.tb_item_des, 
                        it.tb_item_idx
                    FROM tb_proc_proceso_fase_item pfi
                    INNER JOIN tb_proc_proceso_fase pf ON pfi.tb_proceso_fase_id = pf.tb_proceso_fase_id
                    INNER JOIN tb_proc_item it ON pfi.tb_item_id = it.tb_item_id
                    LEFT JOIN tb_proc_multimedia m ON pf.tb_proceso_id = m.tb_proceso_id AND it.tb_item_id = m.tb_item_id AND m.tb_multimedia_xac = 1
                    WHERE pfi.tb_proceso_fase_item_xac = 1 AND pfi.tb_proceso_fase_id = :tb_proceso_fase_id
                    ORDER BY pfi.tb_proceso_fase_item_orden;";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay items registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerContenidoPorProcesoFaseItem($proceso_id, $item_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_multimedia
                    WHERE tb_multimedia_xac = 1 
                    AND tb_proceso_id = :tb_proceso_id 
                    AND tb_item_id = :tb_item_id 
                    ORDER BY tb_multimedia_fecreg DESC;";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_item_id", $item_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                //$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay items registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerFasesProceso($proceso_id) {
        try {
            $sql = "SELECT profa.*, fa.tb_fase_des, fa.tb_fase_nota
                    FROM tb_proc_proceso_fase profa
                    INNER JOIN tb_proc_fase fa ON profa.tb_fase_id = fa.tb_fase_id 
                    WHERE profa.tb_proceso_fase_xac = 1 AND profa.tb_proceso_id = :tb_proceso_id 
                    ORDER BY profa.tb_proceso_fase_orden ASC;";
       
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay fases de proceso registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarUno($proceso_id) {
        try {
            $sql = "SELECT * FROM tb_proc_proceso WHERE tb_proceso_id =:proceso_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos del Proceso";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarProcesoXCredito($credito_id) {
        try {
            $sql = "SELECT * FROM tb_proc_proceso WHERE tb_credito_id =:credito_id AND tb_proceso_xac = 1;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos del Proceso";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarUnoPrecalificacion($proceso_id) {
        try {
            $sql = "SELECT * FROM tb_proc_precalificacion WHERE tb_proceso_id =:proceso_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos de la Precalificación";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarUnoProcesoFase($proceso_fase_id) {
        try {
            $sql = "SELECT * FROM tb_proc_proceso_fase WHERE tb_proceso_fase_id =:proceso_fase_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos del Proceso Fase";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarProcesoFase($proceso_id, $orden) {
        try {
            $sql = "SELECT * FROM tb_proc_proceso_fase WHERE tb_proceso_id =:proceso_id AND tb_proceso_fase_orden =:proceso_fase_orden AND tb_proceso_fase_xac = 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":proceso_fase_orden", $orden, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos del Proceso Fase";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_fechas_comentarios($proceso_fase_id){
        try {
          $sql = "SELECT date_format(tb_comentario_fecreg, '%Y-%m-%d') as fecha_formateada, tb_comentario_fecreg
                FROM tb_proc_comentario
                WHERE tb_comentario_xac = 1 AND tb_proceso_fase_id=:proceso_fase_id
                GROUP BY fecha_formateada
                ORDER BY tb_comentario_fecreg DESC;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay comentarios registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function listar_comentarios($proceso_fase_id){
        try {
          $sql = "SELECT com.*, date_format(com.tb_comentario_fecreg, '%Y-%m-%d') as fecha_formateada, usu.tb_usuario_nom, usu.tb_usuario_ape, usu.tb_usuario_fot 
                FROM tb_proc_comentario com
                INNER JOIN tb_usuario usu ON com.tb_usuario_id = usu.tb_usuario_id
                WHERE com.tb_comentario_xac = 1 AND com.tb_proceso_fase_id=:proceso_fase_id
                ORDER BY com.tb_comentario_fecreg DESC;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay comentarios registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function listar_comentarios_fotos($proceso_fase_id, $comentario_id){
        try {
          $sql = "SELECT img.*, date_format(img.upload_reg, '%Y-%m-%d') as fecha_formateada, usu.tb_usuario_nom 
                FROM upload img
                INNER JOIN tb_usuario usu ON img.tb_usuario_id = usu.tb_usuario_id
                WHERE img.upload_xac = 1 AND modulo_nom = 'proc_proceso_fase_comentario' AND img.modulo_id = :proceso_fase_id AND img.tb_comentario_id = :comentario_id
                ORDER BY img.upload_reg DESC;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
          $sentencia->bindParam(":comentario_id", $comentario_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay fotos registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    

    function eliminarDocProcesoFaseItem($tb_multimedia_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_multimedia SET tb_multimedia_xac=0 WHERE tb_multimedia_id =:tb_multimedia_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_multimedia_id", $tb_multimedia_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_campo_proceso($proceso_id, $proceso_columna, $proceso_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
          if(!empty($proceso_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){
  
            $sql = "UPDATE tb_proc_proceso SET `$proceso_columna` =:tb_proceso WHERE tb_proceso_id =:tb_proceso_id"; 
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_id", $proceso_id, PDO::PARAM_INT);
            if($param_tip == 'INT')
              $sentencia->bindParam(":tb_proceso", $proceso_valor, PDO::PARAM_INT);
            else
              $sentencia->bindParam(":tb_proceso", $proceso_valor, PDO::PARAM_STR);
  
            $result = $sentencia->execute();
  
            $this->dblink->commit();
  
            return $result; //si es correcto el ingreso retorna 1
          }
          else
            return 0;
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }
    

    function modificar_campo_proceso_fase($proceso_fase_id, $proceso_fase_columna, $proceso_fase_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
          if(!empty($proceso_fase_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){
  
            $sql = "UPDATE tb_proc_proceso_fase SET `$proceso_fase_columna` =:tb_proceso_fase WHERE tb_proceso_fase_id =:tb_proceso_fase_id"; 
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
            if($param_tip == 'INT')
              $sentencia->bindParam(":tb_proceso_fase", $proceso_fase_valor, PDO::PARAM_INT);
            else
              $sentencia->bindParam(":tb_proceso_fase", $proceso_fase_valor, PDO::PARAM_STR);
  
            $result = $sentencia->execute();
  
            $this->dblink->commit();
  
            return $result; //si es correcto el ingreso retorna 1
          }
          else
            return 0;
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }

    function modificar_campo_item($item_id, $item_columna, $item_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
          if(!empty($item_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){
  
            $sql = "UPDATE tb_proc_proceso_fase_item SET `$item_columna` =:tb_proceso_fase_item WHERE tb_proceso_fase_item_id =:tb_proceso_fase_item_id"; 
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_fase_item_id", $item_id, PDO::PARAM_INT);
            if($param_tip == 'INT')
              $sentencia->bindParam(":tb_proceso_fase_item", $item_valor, PDO::PARAM_INT);
            else
              $sentencia->bindParam(":tb_proceso_fase_item", $item_valor, PDO::PARAM_STR);
  
            $result = $sentencia->execute();
  
            $this->dblink->commit();
  
            return $result; //si es correcto el ingreso retorna 1
          }
          else
            return 0;
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }

    function modificar_campo_multimedia($multimedia_id, $multimedia_columna, $multimedia_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
          if(!empty($multimedia_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){
  
            $sql = "UPDATE tb_proc_multimedia SET `$multimedia_columna` =:tb_multimedia WHERE tb_multimedia_id =:tb_multimedia_id"; 
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_multimedia_id", $multimedia_id, PDO::PARAM_INT);
            if($param_tip == 'INT')
              $sentencia->bindParam(":tb_multimedia", $multimedia_valor, PDO::PARAM_INT);
            else
              $sentencia->bindParam(":tb_multimedia", $multimedia_valor, PDO::PARAM_STR);
  
            $result = $sentencia->execute();
  
            $this->dblink->commit();
  
            return $result; //si es correcto el ingreso retorna 1
          }
          else
            return 0;
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }

    function modificar_campo_precalificacion($precalificacion_id, $precalificacion_columna, $precalificacion_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
          if(!empty($precalificacion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){
  
            $sql = "UPDATE tb_proc_precalificacion SET `$precalificacion_columna` =:tb_precalificacion_valor WHERE tb_precalificacion_id =:tb_precalificacion_id"; 
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_precalificacion_id", $precalificacion_id, PDO::PARAM_INT);
            if($param_tip == 'INT')
              $sentencia->bindParam(":tb_precalificacion_valor", $precalificacion_valor, PDO::PARAM_INT);
            else
              $sentencia->bindParam(":tb_precalificacion_valor", $precalificacion_valor, PDO::PARAM_STR);
  
            $result = $sentencia->execute();
  
            $this->dblink->commit();
  
            return $result; //si es correcto el ingreso retorna 1
          }
          else
            return 0;
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }

    function mostrarUnoMultimedia($multimedia_id) {
        try {
            $sql = "SELECT * FROM tb_proc_multimedia WHERE tb_multimedia_id =:multimedia_id AND tb_multimedia_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":multimedia_id", $multimedia_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function mostrarUnoMultimediaEliminado($multimedia_id) {
        try {
            $sql = "SELECT * FROM tb_proc_multimedia WHERE tb_multimedia_id =:multimedia_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":multimedia_id", $multimedia_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function mostrarUnoProcesoFaseItem($proceso_fase_item_id) {
        try {
            $sql = "SELECT * FROM tb_proc_proceso_fase_item WHERE tb_proceso_fase_item_id =:proceso_fase_item_id AND tb_proceso_fase_item_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_fase_item_id", $proceso_fase_item_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarDocXItem($proceso_id, $item_id) {
        try {
            $sql = "SELECT * 
                    FROM tb_proc_multimedia 
                    WHERE tb_proceso_id =:proceso_id 
                    AND tb_item_id = :item_id 
                    AND tb_multimedia_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":item_id", $item_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_comite_todos(){
        try {
          $sql = "SELECT com.*, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario
          FROM tb_proc_comite com 
          INNER JOIN tb_usuario usu ON com.tb_usuario_id = usu.tb_usuario_id
          WHERE com.tb_comite_xac = 1 AND com.tb_comite_est = 1;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay miembros registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function listar_comite(){
        try {
          $sql = "SELECT com.*, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario
          FROM tb_proc_comite com 
          INNER JOIN tb_usuario usu ON com.tb_usuario_id = usu.tb_usuario_id
          WHERE com.tb_comite_xac = 1 AND com.tb_comite_est = 1 AND com.tb_comite_voto = 1;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay fotos registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function firma_miembro_comite($usuario_id){
        try {
          $sql = "SELECT com.*, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario
          FROM tb_proc_comite com 
          INNER JOIN tb_usuario usu ON com.tb_usuario_id = usu.tb_usuario_id
          WHERE com.tb_comite_xac = 1 
          AND com.tb_usuario_id = :usuario_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay fotos registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function listar_comite_otros(){
        try {
          $sql = "SELECT com.*, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario
          FROM tb_proc_comite com 
          INNER JOIN tb_usuario usu ON com.tb_usuario_id = usu.tb_usuario_id
          WHERE com.tb_comite_xac = 1 AND com.tb_comite_est = 1 AND com.tb_comite_grado = 3;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay fotos registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function listar_comite_votos($proceso_id){
        try {
          $sql = "SELECT comvo.*, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario, com.tb_comite_firma
          FROM tb_proc_comitevoto comvo 
          INNER JOIN tb_usuario usu ON comvo.tb_usuario_id = usu.tb_usuario_id
          INNER JOIN tb_proc_comite com ON comvo.tb_usuario_id = com.tb_usuario_id
          WHERE comvo.tb_comitevoto_xac = 1 
          AND comvo.tb_comitevoto_voto = 2 
          AND comvo.tb_proceso_id = :proceso_id;";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay votos registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function obtenerCofig($config_id) {
        try {
            $sql = "SELECT * FROM tb_config WHERE tb_config_id = :config_id AND tb_config_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":config_id", $config_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerVotoComite($proceso_id, $usuario_id) {
        try {
            $sql = "SELECT * FROM tb_proc_comitevoto WHERE tb_proceso_id = :proceso_id AND tb_usuario_id = :usuario_id AND tb_comitevoto_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarChequeXProceso($proceso_id) {
        try {
            $sql = "SELECT ched.*, che.tb_cheque_fecreg, che.tb_proceso_id, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario, che.tb_cheque_banco, che.tb_cheque_moneda, che.tb_cheque_conector, che.tb_cheque_det
                    FROM tb_proc_cheque che
                    INNER JOIN tb_proc_chequedetalle ched ON che.tb_cheque_id = ched.tb_cheque_id
                    INNER JOIN tb_usuario usu ON che.tb_usuario_id = usu.tb_usuario_id
                    WHERE che.tb_cheque_xac = 1 AND che.tb_proceso_id = :proceso_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarCheques($fec_ini, $fec_fin, $moneda) {
        try {
            $sql = "SELECT ched.*, che.tb_cheque_fecreg, che.tb_proceso_id, CONCAT(usu.tb_usuario_nom, ' ', usu.tb_usuario_ape) as usuario, che.tb_cheque_banco, che.tb_cheque_moneda, che.tb_cheque_conector, che.tb_cheque_det
                    FROM tb_proc_cheque che
                    INNER JOIN tb_proc_chequedetalle ched ON che.tb_cheque_id = ched.tb_cheque_id
                    INNER JOIN tb_usuario usu ON che.tb_usuario_id = usu.tb_usuario_id
                    WHERE che.tb_cheque_xac = 1 AND che.tb_cheque_fecreg >= :fec_ini AND che.tb_cheque_fecreg <= :fec_fin"; 
            if($moneda!=''){        
                $sql = "AND che.tb_cheque_moneda = :moneda;";       
            }        

            $sentencia = $this->dblink->prepare($sql);
            if($fec_ini!=''){
                $sentencia->bindParam(":fec_ini", $fec_ini, PDO::PARAM_STR);
            }
            if($fec_fin!=''){
                $sentencia->bindParam(":fec_fin", $fec_fin, PDO::PARAM_STR);
            }
            if($moneda!=''){
                $sentencia->bindParam(":moneda", $moneda, PDO::PARAM_STR);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cheques registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON(06-04-24) */
    function mostrarChequesDelDia() {
        try {

            $sql = "SELECT ing.*, che.tb_proceso_id, cre.tb_credito_id
                    FROM tb_ingreso ing
                    INNER JOIN tb_proc_chequedetalle det ON ing.tb_chequedetalle_id = det.tb_chequedetalle_id
                    INNER JOIN tb_proc_cheque che ON det.tb_cheque_id = che.tb_cheque_id
                    INNER JOIN tb_proc_proceso pro ON che.tb_proceso_id = pro.tb_proceso_id
                    INNER JOIN tb_creditogarveh cre ON pro.tb_credito_id = cre.tb_credito_id
                    WHERE ing.tb_ingreso_xac = 1
                    AND cre.tb_credito_xac = 1
                    AND ing.tb_cuenta_id = 2
                    AND ing.tb_subcuenta_id = 176;";
         

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cheques registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function detectarDesembolsosXCheque($credito_id) {
        try {

            $sql = "SELECT *
                    FROM tb_egreso
                    WHERE tb_egreso_xac = 1
                    AND tb_modulo_id = 53
                    AND tb_egreso_modide = :credito_id
                    AND tb_cuenta_id = 19
                    AND (tb_subcuenta_id = 178 OR tb_subcuenta_id = 180);"; 
         

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cheques registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */

    function mostrarParticipanteXProceso($proceso_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_participante
                    WHERE tb_participante_xac = 1 AND tb_proceso_id = :proceso_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay participantes registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarCabeceraChequeXProceso($proceso_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_cheque che
                    WHERE tb_cheque_xac = 1 AND tb_proceso_id = :proceso_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (06-04-24) */
    function mostrarCabeceraChequeXId($cheque_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_cheque che
                    WHERE tb_cheque_xac = 1 AND tb_cheque_id = :cheque_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cabecera de cheque registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */

    function mostrarDetalleCheque($cheque_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_chequedetalle
                    WHERE tb_chequedetalle_xac = 1 AND tb_cheque_id = :cheque_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cheques registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function eliminarCheque($cheque_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_cheque SET tb_cheque_xac=0 WHERE tb_cheque_id =:tb_cheque_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cheque_id", $cheque_id, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminarChequeDetalle($chequedetalle_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_chequedetalle SET tb_chequedetalle_xac=0 WHERE tb_chequedetalle_id =:tb_chequedetalle_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_chequedetalle_id", $chequedetalle_id, PDO::PARAM_INT);
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    function registrarVotacionComite($proceso_id, $usuario_id, $value) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_comitevoto SET tb_comitevoto_voto=:value WHERE tb_proceso_id =:tb_proceso_id AND tb_usuario_id =:tb_usuario_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":value", $value, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarBancarizacion($proceso_id, $banca, $fecha_original) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_proceso SET tb_proceso_banca=:tb_proceso_banca, tb_proceso_fecreg=:tb_proceso_fecreg WHERE tb_proceso_id =:tb_proceso_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_proceso_banca", $banca, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_proceso_fecreg", $fecha_original, PDO::PARAM_STR);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarIEXProceso($proceso_id, $type) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_ingresoegreso
                    WHERE tb_ingresoegreso_xac = 1 AND tb_ingresoegreso_tipo = :type AND tb_proceso_id = :proceso_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":type", $type, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay ingresos o egresos registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function eliminarIngresoEgreso($ingresoegreso_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_ingresoegreso SET tb_ingresoegreso_xac=0 WHERE tb_ingresoegreso_id =:tb_ingresoegreso_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_ingresoegreso_id", $ingresoegreso_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminarParticipante($participante_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_participante SET tb_participante_xac=0 WHERE tb_participante_id =:tb_participante_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_participante_id", $participante_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarSubidaCheque($chequedetalle_id, $num_cheque, $uso, $empresa_id, $fecha) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_chequedetalle 
                    SET tb_chequedetalle_nro_cheque = :num_cheque, 
                        tb_chequedetalle_uso = :uso,
                        tb_empresa_id = :empresa_id,
                        tb_chequedetalle_fecsub = :fecha 
                    WHERE tb_chequedetalle_id =:chequedetalle_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":chequedetalle_id", $chequedetalle_id, PDO::PARAM_INT);
            $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->bindParam(":num_cheque", $num_cheque, PDO::PARAM_STR);
            $sentencia->bindParam(":uso", $uso, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarChequeDetalle($chequedetalle_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_chequedetalle
                    WHERE tb_chequedetalle_xac = 1 AND tb_chequedetalle_id = :chequedetalle_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":chequedetalle_id", $chequedetalle_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function registrarAprobacionCheque($cheque_id, $aprobado, $usuario_id, $fecha) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_cheque 
                    SET tb_cheque_aprobado = :aprobado, 
                        tb_cheque_usu_aprob = :usuario_id,
                        tb_cheque_fec_aprob = :fecha 
                    WHERE tb_cheque_id =:cheque_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cheque_id", $cheque_id, PDO::PARAM_INT);
            $sentencia->bindParam(":aprobado", $aprobado, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function obtenerProcesoFaseItemXItem($proceso_fase_id, $item_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_proceso_fase_item
                    WHERE tb_proceso_fase_item_xac = 1 
                    AND tb_proceso_fase_id = :proceso_fase_id
                    AND tb_item_id = :item_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_fase_id", $proceso_fase_id, PDO::PARAM_INT);
            $sentencia->bindParam(":item_id", $item_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function obtenerComentarioXProcesoYItem($tabla, $proceso_id, $item_id) {
        try {
            $sql = "SELECT *
                    FROM tb_hist
                    WHERE tb_hist_nom_tabla = :tabla
                    AND tb_hist_regmodid2 = :proceso_id
                    AND tb_hist_regmodid3 = :item_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tabla", $tabla, PDO::PARAM_STR);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":item_id", $item_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function movimientosChequeXChequedetalle($chequedetalle_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_chequeretiro
                    WHERE tb_chequeretiro_xac = 1 AND tb_chequedetalle_id = :chequedetalle_id
                    ORDER BY tb_chequeretiro_fecreg DESC;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":chequedetalle_id", $chequedetalle_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay retiros registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarEgresoXCheque($egreso_id) {
        try {
            $sql = "SELECT *
                    FROM tb_egreso
                    WHERE tb_egreso_xac = 1 AND tb_egreso_id = :egreso_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuracion registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function insertarRetiro($chequedetalle_id, $egreso_id, $usuario_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_chequeretiro (
                                        tb_chequeretiro_xac, 
                                        tb_chequeretiro_fecreg,
                                        tb_chequedetalle_id, 
                                        tb_egreso_id, 
                                        tb_usuario_id)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :chequedetalle_id, 
                                        :egreso_id, 
                                        :usuario_id);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":chequedetalle_id", $chequedetalle_id, PDO::PARAM_INT);
            $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarBancarizacionXProceso($proceso_id, $tipo) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_bancarizacion
                    WHERE tb_bancarizacion_xac = 1 
                    AND tb_proceso_id = :proceso_id
                    AND tb_bancarizacion_tipo = :tipo
                    ORDER BY tb_bancarizacion_fecreg DESC;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay bancarización registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function insertarRetiroDeposito($proceso_id, $egreso_id, $tipo, $fecha, $nro_operacion, $banca, $caja_id, $cta_origen, $cta_destino) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_bancarizacion (
                                        tb_bancarizacion_xac, 
                                        tb_bancarizacion_fecreg,
                                        tb_proceso_id, 
                                        tb_egreso_id, 
                                        tb_bancarizacion_tipo, 
                                        tb_bancarizacion_fec_ope, 
                                        tb_bancarizacion_num_ope, 
                                        tb_bancarizacion_banco, 
                                        tb_caja_id, 
                                        tb_bancarizacion_cta_origen, 
                                        tb_bancarizacion_cta_destino)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :proceso_id, 
                                        :egreso_id, 
                                        :tipo, 
                                        :fecha, 
                                        :nro_operacion, 
                                        :banca, 
                                        :caja_id, 
                                        :cta_origen, 
                                        :cta_destino);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
            $sentencia->bindParam(":nro_operacion", $nro_operacion, PDO::PARAM_STR);
            $sentencia->bindParam(":banca", $banca, PDO::PARAM_STR);
            $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cta_origen", $cta_origen, PDO::PARAM_STR);
            $sentencia->bindParam(":cta_destino", $cta_destino, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function actualizarEstadoProceso($proceso_id, $usuario_id, $estado) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_proceso 
                    SET tb_proceso_fecmod = NOW(), 
                        tb_proceso_usumod = :usuario_id,
                        tb_proceso_est = :estado 
                    WHERE tb_proceso_id =:proceso_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminarProcesoFaseItem($proceso_fase_item_id){
        $this->dblink->beginTransaction();
        try {
          $sql = "DELETE FROM tb_proc_proceso_fase_item WHERE tb_proceso_fase_item_id =:proceso_fase_item_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":proceso_fase_item_id", $proceso_fase_item_id, PDO::PARAM_INT);
  
          $result = $sentencia->execute();
  
          $this->dblink->commit();
  
          return $result; //si es correcto retorna 1
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }

    function lista_precredito($mes, $anio, $empresa_id) {
        try {
            $sql = "SELECT 
                        COUNT(IF(cgarvtipo_id = 1, tb_precred_id, NULL)) AS cant_pre,
                        COUNT(IF(cgarvtipo_id = 2, tb_precred_id, NULL)) AS cant_const_s,
                        COUNT(IF(cgarvtipo_id = 3, tb_precred_id, NULL)) AS cant_const_c,
                        COUNT(IF(cgarvtipo_id = 4, tb_precred_id, NULL)) AS cant_compra_s,
                        COUNT(IF(cgarvtipo_id = 5, tb_precred_id, NULL)) AS cant_compra_c,
                        COUNT(IF(cgarvtipo_id = 6, tb_precred_id, NULL)) AS cant_adenda
                    FROM tb_precred
                    WHERE tb_precred_xac = 1";
            if(intval($mes) != ''){
                $sql .= " AND MONTH(tb_precred_fecreg) = :mes";
            }
            if(intval($anio) != ''){
                $sql .= " AND YEAR(tb_precred_fecreg) = :anio";
            }

            $sentencia = $this->dblink->prepare($sql);
            if(intval($mes) != ''){
                $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
            }
            if(intval($anio) != ''){
                $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay bancarización registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function reporte_proceso_general($mes, $anio, $empresa_id) {
        try {
            $sql = "SELECT 
                        COUNT(IF(tb_cgarvtipo_id = 1, tb_proceso_id, NULL)) AS cant_pre,
                        COUNT(IF(tb_cgarvtipo_id = 2, tb_proceso_id, NULL)) AS cant_const_s,
                        COUNT(IF(tb_cgarvtipo_id = 3, tb_proceso_id, NULL)) AS cant_const_c,
                        COUNT(IF(tb_cgarvtipo_id = 4, tb_proceso_id, NULL)) AS cant_compra_s,
                        COUNT(IF(tb_cgarvtipo_id = 5, tb_proceso_id, NULL)) AS cant_compra_c,
                        COUNT(IF(tb_cgarvtipo_id = 6, tb_proceso_id, NULL)) AS cant_adenda,
                        COUNT(IF(IF(tb_credito_id IS NULL, 0, 1) = 1 AND tb_cgarvtipo_id = 1, tb_credito_id, NULL)) AS concluido_pre,
                        COUNT(IF(IF(tb_credito_id IS NULL, 0, 1) = 1 AND tb_cgarvtipo_id = 2, tb_credito_id, NULL)) AS concluido_const_s,
                        COUNT(IF(IF(tb_credito_id IS NULL, 0, 1) = 1 AND tb_cgarvtipo_id = 3, tb_credito_id, NULL)) AS concluido_const_c,
                        COUNT(IF(IF(tb_credito_id IS NULL, 0, 1) = 1 AND tb_cgarvtipo_id = 4, tb_credito_id, NULL)) AS concluido_compra_s,
                        COUNT(IF(IF(tb_credito_id IS NULL, 0, 1) = 1 AND tb_cgarvtipo_id = 5, tb_credito_id, NULL)) AS concluido_compra_c,
                        COUNT(IF(IF(tb_credito_id IS NULL, 0, 1) = 1 AND tb_cgarvtipo_id = 6, tb_credito_id, NULL)) AS concluido_adenda
                    FROM tb_proc_proceso
                    WHERE tb_proceso_xac = 1";
            if(intval($mes) != ''){
                $sql .= " AND MONTH(tb_proceso_fecreg) = :mes";
            }
            if(intval($anio) != ''){
                $sql .= " AND YEAR(tb_proceso_fecreg) = :anio";
            }
            /* if(intval($empresa_id) > 0){
                $sql .= " AND tb_empresa_id = :empresa_id;";
            } */

            $sentencia = $this->dblink->prepare($sql);
            if(intval($mes) != ''){
                $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
            }
            if(intval($anio) != ''){
                $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
            }
            /* if(intval($empresa_id) > 0){
                $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
            } */
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay bancarización registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (20-05-24) */
	function insertar_cronjob($det, $tipo, $usu_id){
		$this->dblink->beginTransaction();
		try {
		  $sql = "INSERT INTO tb_cron(
									tb_cron_fec,
									tb_cron_det,
									tb_cron_tip, 
									tb_usuario_id)
							  values(
									NOW(),
									:tb_cron_det,
									:tb_cron_tip, 
									:tb_usuario_id)";

		  $sentencia = $this->dblink->prepare($sql);
		  $sentencia->bindParam(":tb_cron_det", $det, PDO::PARAM_STR);
		  $sentencia->bindParam(":tb_cron_tip", $tipo, PDO::PARAM_INT);
		  $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);

		  $result = $sentencia->execute();
		  $this->dblink->commit();
		  return $result; //si es correcto retorna 1

		} catch (Exception $e) {
		  $this->dblink->rollBack();
		  throw $e;
		}
	}
	/*  */

    /* GERSON (30-05-24) */
    function mostrarCreditosActivosXCliente($cliente_id) {
        try {
            $sql = "SELECT COUNT(*) AS creditos
                    FROM (
                        SELECT tb_cliente_id
                        FROM tb_creditomenor
                        WHERE tb_cliente_id = :cliente_id
                        AND tb_credito_xac = 1
                        
                        UNION
                        
                        SELECT tb_cliente_id
                        FROM tb_creditogarveh
                        WHERE tb_cliente_id = :cliente_id
                        AND tb_credito_xac = 1
                    ) AS combinado;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay bancarización registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /* GERSON (16-06-24) */
    function mostrarMenorXCliente($cliente_id) {
        try {
            $sql = "SELECT tb_credito_id
                    FROM tb_creditomenor
                    WHERE tb_cliente_id = :cliente_id
                    AND tb_credito_xac = 1
                    ORDER BY tb_credito_reg ASC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay créditos registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarMenorCuotasXCliente($cliente_id) {
        try {
            $sql = "SELECT 
                        DISTINCT(cuo.tb_cuota_id), 
                        cre.tb_cliente_id,
                        cre.tb_credito_id,
                        cre.tb_credito_reg,
                        IF(cre.tb_credito_est = 1, 'PENDIENTE', IF(cre.tb_credito_est = 2, 'APROBADO', IF(cre.tb_credito_est = 3, 'VIGENTE', IF(cre.tb_credito_est = 4, 'LIQUIDADO', IF(cre.tb_credito_est = 5, 'REMATE', IF(cre.tb_credito_est = 6, 'VENDIDO', IF(cre.tb_credito_est = 7, 'PENDIENTE ENVÍO', 'SIN ESTADO'))))))) AS estado,
                        cuo.tb_cuota_num,
                        cuo.tb_cuota_fec,
                        IF( ISNULL(pag.tb_cuotapago_fec), '', pag.tb_cuotapago_fec) AS fecha_pago,
                        IF( ISNULL(pag.tb_cuotapago_fec), '', DATEDIFF(pag.tb_cuotapago_fec, cuo.tb_cuota_fec)) AS dias_diff
                    FROM tb_creditomenor cre
                    INNER JOIN tb_cuota cuo ON cre.tb_credito_id = cuo.tb_credito_id
                    LEFT JOIN tb_cuotapago pag ON cuo.tb_cuota_id = pag.tb_cuotapago_modid
                    WHERE cre.tb_cliente_id = :cliente_id
                    AND cuo.tb_creditotipo_id = 1
                    AND cuo.tb_cuota_xac = 1
                    AND cre.tb_credito_xac = 1
                    GROUP BY cuo.tb_cuota_id
                    ORDER BY cuo.tb_cuota_fec ASC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cuotas registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarGarvehXCliente($cliente_id) {
        try {
            $sql = "SELECT tb_credito_id
                    FROM tb_creditogarveh
                    WHERE tb_cliente_id = :cliente_id
                    AND tb_credito_xac = 1
                    ORDER BY tb_credito_reg ASC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay créditos registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarGarvehCuotasXCliente($cliente_id) {
        try {
            $sql = "SELECT 
                        DISTINCT(cuo.tb_cuota_id), 
                        cre.tb_cliente_id,
                        cre.tb_credito_id,
                        cre.tb_credito_reg,
                        IF(cre.tb_credito_est = 1, 'PENDIENTE', IF(cre.tb_credito_est = 2, 'APROBADO', IF(cre.tb_credito_est = 3, 'VIGENTE', IF(cre.tb_credito_est = 4, 'PARALIZADO', IF(cre.tb_credito_est = 5, 'REFINANCIADO AMORT.', IF(cre.tb_credito_est = 6, 'REFINANCIADO SIN AMORT.', IF(cre.tb_credito_est = 7, 'LIQUIDADO', IF(cre.tb_credito_est = 8, 'RESUELTO', IF(cre.tb_credito_est = 9, 'TÍTULO', IF(cre.tb_credito_est = 10, 'DOCUMENTOS', 'SIN ESTADO')))))))))) AS estado,
                        cuo.tb_cuota_num,
                        cuod.tb_cuotadetalle_fec,
                        IF( ISNULL(pag.tb_cuotapago_fec), '', pag.tb_cuotapago_fec) AS fecha_pago,
                        IF( ISNULL(pag.tb_cuotapago_fec), '', DATEDIFF(pag.tb_cuotapago_fec, cuod.tb_cuotadetalle_fec)) AS dias_diff
                    FROM tb_creditogarveh cre
                    INNER JOIN tb_cuota cuo ON cre.tb_credito_id = cuo.tb_credito_id
                    INNER JOIN tb_cuotadetalle cuod ON cuo.tb_cuota_id = cuod.tb_cuota_id
                    LEFT JOIN tb_cuotapago pag ON cuod.tb_cuotadetalle_id = pag.tb_cuotapago_modid
                    WHERE cre.tb_cliente_id = :cliente_id
                    AND cuo.tb_creditotipo_id = 3
                    AND cuo.tb_cuota_xac = 1
                    AND cre.tb_credito_xac = 1
                    GROUP BY cuo.tb_cuota_id
                    ORDER BY cuod.tb_cuotadetalle_fec ASC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cuotas registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /* GERSON (03-07-24) */
    function mostrarProcesosXCliente($cliente_id) {
        try {
            $sql = "SELECT tb_proceso_id, tb_proceso_fecreg
                    FROM tb_proc_proceso
                    WHERE tb_cliente_id = :cliente_id
                    AND tb_proceso_xac = 1
                    ORDER BY tb_proceso_fecreg DESC;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay procesos registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (03-01-25) */
    function mostrarUnoValoracion($proceso_id) {
        try {
            $sql = "SELECT * FROM tb_proc_valoracion WHERE tb_proceso_id =:proceso_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos de la Valoración";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarUnoValoracionById($valoracion_id) {
        try {
            $sql = "SELECT * FROM tb_proc_valoracion WHERE tb_valoracion_id =:valoracion_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":valoracion_id", $valoracion_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos de la Valoración";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function mostrarValoracionDetalle($valoracion_id) {
        try {
            $sql = "SELECT *
                    FROM tb_proc_valoraciondetalle
                    WHERE tb_valoracion_id = :valoracion_id
                    AND tb_valoraciondetalle_xac = 1;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":valoracion_id", $valoracion_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay valoraciones registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function lista_cuotas_vencidas_cmenor($cliente_id){
        try {
          
          $sql = "SELECT co.tb_cobranza_id, cre.tb_credito_id,cuo.tb_cuota_id, cuo.tb_cuota_fec, cuo.tb_cuota_cuo, DATEDIFF(CURDATE(), tb_cuota_fec) AS dias_diferencia,cuo.tb_cuota_est, co.tb_cobranza_estado, cuo.tb_moneda_id, cre.tb_credito_tipcam, cre.tb_credito_est 
                    FROM `tb_cobranza` co
                    INNER JOIN tb_cuota cuo ON (cuo.tb_cuota_id = co.tb_cuota_id) 
                    INNER JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 1)
                    INNER JOIN tb_garantia gar ON (gar.tb_credito_id = cre.tb_credito_id)
                    WHERE tb_cuota_xac = 1 
                    AND co.tb_cobranza_xac = 1 
                    AND cre.tb_cliente_id = :cliente_id
                    AND cuo.tb_cuota_est IN(1,3) 
                    AND cre.tb_credito_est = 3 
                    AND gar.tb_garantiatipo_id IN(1,3) 
                    AND DATEDIFF(CURDATE(), cuo.tb_cuota_fec) >= 1
                    GROUP BY 1;";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          $retorno["sql"] = $sql;
          $retorno["cantidad"] = $sentencia->rowCount();
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor();
          } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "error";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function lista_cuotas_vencidas_garveh($cliente_id){
        try {
          
          $sql = "SELECT cre.tb_credito_id,cuo.tb_cuota_id, cuo.tb_cuota_fec, cuo.tb_cuota_cuo, DATEDIFF(CURDATE(), tb_cuota_fec) AS dias_diferencia,cuo.tb_cuota_est, cuo.tb_moneda_id, cre.tb_credito_tipcam, cre.tb_credito_est 
                    FROM `tb_cuota` cuo
                    INNER JOIN tb_creditogarveh cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 3)
                    WHERE tb_cuota_xac = 1 
                    AND cre.tb_cliente_id = :cliente_id
                    AND cuo.tb_cuota_est IN(1,3) 
                    AND cre.tb_credito_est = 3
                    AND DATEDIFF(CURDATE(), cuo.tb_cuota_fec) >= 1
                    GROUP BY 1;";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          $retorno["sql"] = $sql;
          $retorno["cantidad"] = $sentencia->rowCount();
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor();
          } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "error";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function registrarValoracion($usuario_id, $proceso_id, $valoracion_des, $valoracion_obs, $valoracion_extra, $valoracion_estetica, $valoracion_electrica, $valoracion_interior, $valoracion_mecanica, $valoracion_repuesto, $valoracion_otro, $valoracion_ext_des1, $valoracion_ext_monto1, $valoracion_ext_des2, $valoracion_ext_monto2, $valoracion_ext_des3, $valoracion_ext_monto3, $valoracion_ext_des4, $valoracion_ext_monto4, $valoracion_ext_des5, $valoracion_ext_monto5, $valoracion_ext_des6, $valoracion_ext_monto6) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_valoracion (
                                        tb_valoracion_xac, 
                                        tb_valoracion_fecreg,
                                        tb_usuario_id, 
                                        tb_proceso_id, 
                                        tb_valoracion_des, 
                                        tb_valoracion_obs, 
                                        tb_valoracion_extra, 
                                        tb_valoracion_estetica, 
                                        tb_valoracion_electrica, 
                                        tb_valoracion_interior, 
                                        tb_valoracion_mecanica, 
                                        tb_valoracion_repuesto, 
                                        tb_valoracion_otro, 
                                        tb_valoracion_ext_des1, 
                                        tb_valoracion_ext_monto1, 
                                        tb_valoracion_ext_des2,
                                        tb_valoracion_ext_monto2,
                                        tb_valoracion_ext_des3,
                                        tb_valoracion_ext_monto3,
                                        tb_valoracion_ext_des4,
                                        tb_valoracion_ext_monto4,
                                        tb_valoracion_ext_des5,
                                        tb_valoracion_ext_monto5,
                                        tb_valoracion_ext_des6,
                                        tb_valoracion_ext_monto6,
                                        tb_valoracion_valor_final_mn,
                                        tb_valoracion_valor_final_me,
                                        tb_valoracion_valor_oferta_sc_mn,
                                        tb_valoracion_valor_oferta_sc_me,
                                        tb_valoracion_valor_oferta_cc_mn,
                                        tb_valoracion_valor_oferta_cc_me)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :usuario_id, 
                                        :proceso_id, 
                                        :valoracion_des, 
                                        :valoracion_obs, 
                                        :valoracion_extra, 
                                        :valoracion_estetica,
                                        :valoracion_electrica,
                                        :valoracion_interior,
                                        :valoracion_mecanica,
                                        :valoracion_repuesto,
                                        :valoracion_otro,
                                        :valoracion_ext_des1,
                                        :valoracion_ext_monto1,
                                        :valoracion_ext_des2,
                                        :valoracion_ext_monto2,
                                        :valoracion_ext_des3,
                                        :valoracion_ext_monto3,
                                        :valoracion_ext_des4,
                                        :valoracion_ext_monto4,
                                        :valoracion_ext_des5,
                                        :valoracion_ext_monto5,
                                        :valoracion_ext_des6,
                                        :valoracion_ext_monto6,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_des", $valoracion_des, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_obs", $valoracion_obs, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_extra", $valoracion_extra, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_estetica", $valoracion_estetica, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_electrica", $valoracion_electrica, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_interior", $valoracion_interior, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_mecanica", $valoracion_mecanica, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_repuesto", $valoracion_repuesto, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_otro", $valoracion_otro, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des1", $valoracion_ext_des1, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto1", $valoracion_ext_monto1, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des2", $valoracion_ext_des2, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto2", $valoracion_ext_monto2, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des3", $valoracion_ext_des3, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto3", $valoracion_ext_monto3, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des4", $valoracion_ext_des4, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto4", $valoracion_ext_monto4, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des5", $valoracion_ext_des5, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto5", $valoracion_ext_monto5, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des6", $valoracion_ext_des6, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto6", $valoracion_ext_monto6, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $valoracion_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['valoracion_id'] = $valoracion_id;

            return $data;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function actualizarValoracion($valoracion_id, $valoracion_des, $valoracion_obs, $valoracion_extra, $valoracion_estetica, $valoracion_electrica, $valoracion_interior, $valoracion_mecanica, $valoracion_repuesto, $valoracion_otro, $valoracion_ext_des1, $valoracion_ext_monto1, $valoracion_ext_des2, $valoracion_ext_monto2, $valoracion_ext_des3, $valoracion_ext_monto3, $valoracion_ext_des4, $valoracion_ext_monto4, $valoracion_ext_des5, $valoracion_ext_monto5, $valoracion_ext_des6, $valoracion_ext_monto6) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proc_valoracion SET
                                        tb_valoracion_des = :valoracion_des, 
                                        tb_valoracion_obs = :valoracion_obs, 
                                        tb_valoracion_extra = :valoracion_extra, 
                                        tb_valoracion_estetica = :valoracion_estetica, 
                                        tb_valoracion_electrica = :valoracion_electrica, 
                                        tb_valoracion_interior = :valoracion_interior, 
                                        tb_valoracion_mecanica = :valoracion_mecanica, 
                                        tb_valoracion_repuesto = :valoracion_repuesto, 
                                        tb_valoracion_otro = :valoracion_otro, 
                                        tb_valoracion_ext_des1 = :valoracion_ext_des1, 
                                        tb_valoracion_ext_monto1 = :valoracion_ext_monto1, 
                                        tb_valoracion_ext_des2 = :valoracion_ext_des2,
                                        tb_valoracion_ext_monto2 = :valoracion_ext_monto2,
                                        tb_valoracion_ext_des3 = :valoracion_ext_des3,
                                        tb_valoracion_ext_monto3 = :valoracion_ext_monto3,
                                        tb_valoracion_ext_des4 = :valoracion_ext_des4,
                                        tb_valoracion_ext_monto4 = :valoracion_ext_monto4,
                                        tb_valoracion_ext_des5 = :valoracion_ext_des5,
                                        tb_valoracion_ext_monto5 = :valoracion_ext_monto5,
                                        tb_valoracion_ext_des6 = :valoracion_ext_des6,
                                        tb_valoracion_ext_monto6 = :valoracion_ext_monto6 WHERE tb_valoracion_id = :valoracion_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":valoracion_id", $valoracion_id, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_des", $valoracion_des, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_obs", $valoracion_obs, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_extra", $valoracion_extra, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_estetica", $valoracion_estetica, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_electrica", $valoracion_electrica, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_interior", $valoracion_interior, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_mecanica", $valoracion_mecanica, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_repuesto", $valoracion_repuesto, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_otro", $valoracion_otro, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des1", $valoracion_ext_des1, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto1", $valoracion_ext_monto1, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des2", $valoracion_ext_des2, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto2", $valoracion_ext_monto2, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des3", $valoracion_ext_des3, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto3", $valoracion_ext_monto3, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des4", $valoracion_ext_des4, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto4", $valoracion_ext_monto4, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des5", $valoracion_ext_des5, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto5", $valoracion_ext_monto5, PDO::PARAM_INT);
            $sentencia->bindParam(":valoracion_ext_des6", $valoracion_ext_des6, PDO::PARAM_STR);
            $sentencia->bindParam(":valoracion_ext_monto6", $valoracion_ext_monto6, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $this->dblink->commit();

            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['valoracion_id'] = $valoracion_id;

            return $data;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function registrarValoracionDetalle($valoracion_id, $moneda, $monto, $anio, $km, $link) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proc_valoraciondetalle (
                                        tb_valoraciondetalle_xac, 
                                        tb_valoraciondetalle_fecreg,
                                        tb_valoracion_id, 
                                        tb_valoraciondetalle_moneda, 
                                        tb_valoraciondetalle_des, 
                                        tb_valoraciondetalle_monto, 
                                        tb_valoraciondetalle_km, 
                                        tb_valoraciondetalle_link, 
                                        tb_valoraciondetalle_img)
                                VALUES (
                                        1, 
                                        NOW(),
                                        :valoracion_id, 
                                        :valoraciondetalle_moneda,
                                        :valoraciondetalle_des,
                                        :valoraciondetalle_monto,
                                        :valoraciondetalle_km,
                                        :valoraciondetalle_link,
                                        NULL);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":valoracion_id", $valoracion_id, PDO::PARAM_INT);
            $sentencia->bindParam(":valoraciondetalle_moneda", $moneda, PDO::PARAM_INT);
            $sentencia->bindParam(":valoraciondetalle_des", $anio, PDO::PARAM_STR);
            $sentencia->bindParam(":valoraciondetalle_monto", $monto, PDO::PARAM_INT);
            $sentencia->bindParam(":valoraciondetalle_km", $km, PDO::PARAM_INT);
            $sentencia->bindParam(":valoraciondetalle_link", $link, PDO::PARAM_STR);
            
            $result = $sentencia->execute();
            $valoraciondetalle_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['valoraciondetalle_id'] = $valoraciondetalle_id;

            return $data;

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_campo_valoracion($valoracion_id, $valoracion_columna, $valoracion_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
          if(!empty($valoracion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){
  
            $sql = "UPDATE tb_proc_valoracion SET `$valoracion_columna` =:tb_valoracion_valor WHERE tb_valoracion_id =:tb_valoracion_id"; 
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_valoracion_id", $valoracion_id, PDO::PARAM_INT);
            if($param_tip == 'INT')
              $sentencia->bindParam(":tb_valoracion_valor", $valoracion_valor, PDO::PARAM_INT);
            else
              $sentencia->bindParam(":tb_valoracion_valor", $valoracion_valor, PDO::PARAM_STR);
  
            $result = $sentencia->execute();
  
            $this->dblink->commit();
  
            return $result; //si es correcto el ingreso retorna 1
          }
          else
            return 0;
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }
    /*  */

    /* GERSON (08-01-25) */
    function existeIngresoByChequedetalle($chequedetalle_id, $empresa_id) {
        try {

            $sql = "SELECT *
                    FROM tb_ingreso
                    WHERE tb_ingreso_xac = 1
                    AND tb_empresa_id = :empresa_id
                    AND tb_chequedetalle_id = :chequedetalle_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->bindParam(":chequedetalle_id", $chequedetalle_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cheques registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */

    function mostrarProcesXCliente($cliente_id, $proceso_id, $fecha) {
        try {
            $sql = "SELECT pro.*, CONCAT(usu.tb_usuario_nom,' ',usu.tb_usuario_ape) AS asesor, cre.tb_credito_est, ctip.tb_creditotipo_nom, cgarv.cgarvtipo_nombre 
                    FROM tb_proc_proceso pro
                    LEFT JOIN tb_creditogarveh cre on pro.tb_credito_id = cre.tb_credito_id 
                    INNER JOIN tb_usuario usu on pro.tb_usuario_id = usu.tb_usuario_id
                    INNER JOIN tb_creditotipo ctip on pro.tb_creditotipo_id = ctip.tb_creditotipo_id
                    INNER JOIN cgarvtipo cgarv on pro.tb_cgarvtipo_id = cgarv.cgarvtipo_id
                    WHERE pro.tb_proceso_xac = 1 
                    AND pro.tb_cliente_id = :cliente_id
                    AND pro.tb_proceso_id != :proceso_id
                    AND pro.tb_proceso_fecreg < :fecha;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay procesos para el cliente registrado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (09-01-25) */
    function mostrarUnoValoracionFile($valoracionfile_id){
        try {
          $sql = "SELECT * FROM tb_proc_valoracionfile WHERE tb_valoracionfile_id =:valoracionfile_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valoracionfile_id", $valoracionfile_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    function insertar_valoracionfile($valoracion_id,$valoracionfile_his,$directorio,$tempFile,$targetFil,$fileParts){
        $this->dblink->beginTransaction();
        try {
          $sql = "INSERT INTO tb_proc_valoracionfile
                                (tb_valoracionfile_xac, 
                                tb_valoracion_id, 
                                tb_valoracionfile_url, 
                                tb_valoracionfile_his) 
                         VALUES(
                                1, 
                                :valoracion_id, 
                                '', 
                                :valoracionfile_his)";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valoracion_id", $valoracion_id, PDO::PARAM_INT);
          $sentencia->bindParam(":valoracionfile_his", $valoracionfile_his, PDO::PARAM_STR);
  
          $result1 = $sentencia->execute();
          $valoracionfile_id = $this->dblink->lastInsertId();
          $this->dblink->commit();
  
          $nombre_imagen = 'valoracion_'.$valoracion_id.'_valoracionfile_'. $valoracionfile_id .'.'. $fileParts['extension'];
          $valoracionfile_url = $directorio . strtolower($nombre_imagen);
  
          if(move_uploaded_file($tempFile, '../../'. $valoracionfile_url)){
                //insertamos las imágenes
                $sql = "UPDATE tb_proc_valoracionfile SET tb_valoracionfile_url =:valoracionfile_url WHERE tb_valoracionfile_id =:valoracionfile_id";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":valoracionfile_id", $valoracionfile_id, PDO::PARAM_INT);
                $sentencia->bindParam(":valoracionfile_url", $valoracionfile_url, PDO::PARAM_STR);
                $result2 = $sentencia->execute();
            }
          else{
            $this->eliminar_valoracionfile($valoracionfile_id);
          }
  
          if($result1 == 1)
            return 'exito';
          else
            return 'Erro al insertar en Valoración File';
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }
    
    function eliminar_valoracionfile($valoracionfile_id){
        $this->dblink->beginTransaction();
        try {
          $sql = "DELETE FROM tb_proc_valoracionfile WHERE tb_valoracionfile_id =:valoracionfile_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valoracionfile_id", $valoracionfile_id, PDO::PARAM_INT);
  
          $result = $sentencia->execute();
  
          $this->dblink->commit();
  
          return $result; //si es correcto retorna 1
  
        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }

    function listar_valoracionfiles($valoracion_id){
        try {
          $sql = "SELECT * FROM tb_proc_valoracionfile where tb_valoracion_id =:valoracion_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valoracion_id", $valoracion_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay imágenes subidas";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
  
    }
    /*  */

}

?>