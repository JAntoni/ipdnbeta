<?php
//echo 'hasta aki estoy entrando normal '; exit();
//require_once('../../core/usuario_sesion.php');
//error_reporting(0);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';

require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$proceso_id = intval($_GET['proceso_id']);
$usuario_id = intval($_GET['usuario_id']);
$fecha      = $_GET['fecha'];
$moneda     = $_GET['moneda'];
$cambio     = floatval($_GET['cambio']);
$cuota      = intval($_GET['cuota']);
$interes    = floatval($_GET['interes']);
$costo      = floatval($_GET['costo']);
$inicial    = floatval($_GET['inicial']);
$valor      = floatval($_GET['valor']);

$chk_gps    = intval($_GET['chk_gps']);

$seguro_porcentaje  = floatval($_GET['seguro_porcentaje']);
$gps_precio         = floatval($_GET['gps_precio']);
$span_seguro_gps    = $_GET['span_seguro_gps'];

// GERSON (14-11-23)
if($seguro_porcentaje > 0){ // nuevo modo de interes al 14-11-23, se suma el 0.27 a la tasa de interes fijada
  $interes = $interes + $seguro_porcentaje;
}
//

$colaborador = '';
$celular = '';
$usuario = $oUsuario->mostrarUno($usuario_id);
if($usuario['estado']==1){
  $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
  $celular = $usuario['data']['tb_usuario_tel'];
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("contacto@prestamosdelnortechiclayo.com")
	->setLastModifiedBy("contacto@prestamosdelnortechiclayo.com")
	->setTitle("CRONOGRAMA DE CRÉDITO ACVEH")
	->setSubject("Cronograma")
	->setDescription("Reporte generado por contacto@prestamosdelnortechiclayo.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
    'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '279944')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);
$estiloTituloFilas2 = array(
    'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '135825')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas3 = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas4 = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

  $titulo_moneda = '';
  $mon = '';
	$sub_mon = '';
  if($moneda=='sol'){
    $titulo_moneda = 'SOLES';
    $mon = 'S/ ';
  }elseif($moneda=='dolar'){
    $titulo_moneda = 'DOLARES';
    $mon = '$ ';
		$sub_mon = 'S/ ';
  }elseif($moneda=='sol_d'){
    $titulo_moneda = 'SOLES_DOLARES';
    $mon = '$ ';
		$sub_mon = 'S/ ';
  }elseif($moneda=='dolar_s'){
    $titulo_moneda = 'DÓLARES_SOLES';
    $mon = 'S/ ';
		$sub_mon = '$ ';

  }
  
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

  //$xlsx->MultiCell(190,40, $xlsx->Image($img, $xlsx->GetX()+40, $xlsx->GetY()+3, 100) ,0,"A");

  $objDrawing = new PHPExcel_Worksheet_Drawing();
  $objDrawing->setName('Logo IPDN');
  $objDrawing->setDescription('Logo IPDN');
  $img = '../../public/images/logopres.png'; // Provide path to your logo file
  $objDrawing->setPath($img);
  $objDrawing->setCoordinates("E2");
  $objDrawing->setWidth(70);
  $objDrawing->setHeight(55);
  $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

  $nombre_archivo = "SIMULADOR ".$titulo_moneda;

  $c = 1;
  $titulo = "CRONOGRAMA DE CRÉDITO ACVEH";
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:H$c");
  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("A$c:H$c")->applyFromArray($estiloTituloFilas2);

  $c = 2;

  $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'Costo Vehículo');
  $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);
  if($moneda=='sol' || $moneda=='dolar'){
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $mon.''.number_format($costo, 2));
  }elseif($moneda=='sol_d' || $moneda=='dolar_s'){
    $objPHPExcel->getActiveSheet()->setCellValue("B$c", $sub_mon.''.number_format($costo, 2));
  }
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas3);

  $c = 3;

  $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'Tipo de Cambio');
  $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", number_format($cambio, 3));
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas3);

  $c = 4;

  $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'Inicial');
  $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", number_format($inicial, 2).'%');
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas3);
  if($moneda=='sol' || $moneda=='dolar'){
    $objPHPExcel->getActiveSheet()->setCellValue("C$c", $mon.''.number_format($costo - ($costo*($inicial/100)), 2));
  }elseif($moneda=='sol_d' || $moneda=='dolar_s'){
    $objPHPExcel->getActiveSheet()->setCellValue("C$c", $sub_mon.''.number_format($costo - ($costo*($inicial/100)), 2));
  }
  $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas3);

  if($moneda=='dolar'){
    $objPHPExcel->getActiveSheet()->setCellValue("D$c", $sub_mon.''.number_format(($costo - ($costo*($inicial/100)))*$cambio, 2));
    $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas3);
  }elseif($moneda=='dolar_s'){
    $objPHPExcel->getActiveSheet()->setCellValue("D$c", $mon.''.number_format(($costo - ($costo*($inicial/100)))*$cambio, 2));
    $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas3);
  }
  
  $c = $c + 2;

  if($moneda=='sol' || $moneda=='dolar_s'){
    $titulosColumnas = array(
      'FECHA PAGO',
      'N° DE CUOTA',
      'CAPITAL AL INICIO DE PERIODO',
      'AMORTIZACIÓN',
      'SOBRECOSTO DEL PERIODO',
      'GPS',
      'CUOTA'
    );

  }elseif($moneda=='dolar' || $moneda=='sol_d'){
    $titulosColumnas = array(
      'FECHA PAGO',
      'N° DE CUOTA',
      'CAPITAL AL INICIO DE PERIODO',
      'AMORTIZACIÓN',
      'SOBRECOSTO DEL PERIODO',
      'GPS',
      'CUOTA',
      'CUOTA SOLES'
    );
  }


  $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
  ;
  if($moneda=='dolar' || $moneda=='sol_d'){
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$c,  $titulosColumnas[7]);
  }
  
  if($moneda=='sol' || $moneda=='dolar_s'){
    $objPHPExcel->getActiveSheet()->getStyle("A$c:G$c")->applyFromArray($estiloTituloFilas2);
  }elseif($moneda=='dolar' || $moneda=='sol_d'){
    $objPHPExcel->getActiveSheet()->getStyle("A$c:H$c")->applyFromArray($estiloTituloFilas2);
  }

  $c = $c+1;  

  if($moneda=='sol' || $moneda=='dolar'){

    for ($i=0; $i < $cuota; $i++) { 

      $seguro_mensual = 0.00;
			$gps_mensual = 0.00;

      if($i==0){
        $new_date = $fecha;
        $capital = $valor;
      }
      $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

      $cuota_pagar = 0.00;
      $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*$valor/((pow(1+($interes/100),$cuota))-1);


      $sobrecosto = 0.00;
      $sobrecosto = $capital*($interes/100);

      $amortizacion = 0.00;
      if($cuota_pagar>0){
        $amortizacion = $cuota_pagar - $sobrecosto;
      }

      // GERSON (10-11-23)
      /* if($seguro_porcentaje > 0){
        $seguro_mensual = formato_moneda($seguro_porcentaje * $cuota_pagar / 100);
        $sobrecosto = $sobrecosto + $seguro_mensual;
        $cuota_pagar = $cuota_pagar + $seguro_mensual;
      } */
      if($chk_gps > 0){
        if($gps_precio > 0 && $cuota > 0){
          if($moneda=='sol'){
            $gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
          }elseif($moneda=='dolar'){
            $gps_mensual = formato_moneda($gps_precio / $cuota);
          }
          $cuota_pagar = $cuota_pagar + $gps_mensual;
        }
      }
      //

      $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
      $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
      $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("F$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($estiloTituloFilas3);
      if($moneda=='dolar'){
        $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloFilas3);
      }

      $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$c, $new_date)
        ->setCellValue('B'.$c, ($i+1))
        ->setCellValue('C'.$c, $mon.''.number_format($capital, 2))
        ->setCellValue('D'.$c, $mon.''.number_format($amortizacion, 2))
        ->setCellValue('E'.$c, $mon.''.number_format($sobrecosto, 2))
        ->setCellValue('F'.$c, $mon.''.number_format($gps_mensual, 2))
        ->setCellValue('G'.$c, $mon.''.number_format($cuota_pagar, 2))
      ;
      if($moneda=='dolar'){
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$c, $sub_mon.''.number_format($cuota_pagar*$cambio, 2));
      }

      //$objPHPExcel->getActiveSheet(0)->getStyle('F'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
      //$objPHPExcel->getActiveSheet(0)->getStyle('G'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

      $capital = $capital - $amortizacion;

      $c++;

    }

  }elseif($moneda=='dolar_s'){

    for ($i=0; $i < $cuota; $i++) { 

      $seguro_mensual = 0.00;
			$gps_mensual = 0.00;

      if($i==0){
        $new_date = $fecha;
        $capital = $valor*$cambio;
      }

      $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

      $cuota_pagar = 0.00;
      $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor*$cambio)/((pow(1+($interes/100),$cuota))-1);

      $sobrecosto = 0.00;
      $sobrecosto = $capital*($interes/100);

      $amortizacion = 0.00;
      if($cuota_pagar>0){
        $amortizacion = $cuota_pagar - $sobrecosto;
      }

      // GERSON (10-11-23)
      /* if($seguro_porcentaje > 0){
        $seguro_mensual = formato_moneda($seguro_porcentaje * $cuota_pagar / 100);
        $sobrecosto = $sobrecosto + $seguro_mensual;
        $cuota_pagar = $cuota_pagar + $seguro_mensual;
      } */

      if($chk_gps > 0){
        if($gps_precio > 0 && $cuota > 0){
          $gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
          //$gps_mensual = formato_moneda($gps_precio / $cuota);
          $cuota_pagar = $cuota_pagar + $gps_mensual;
        }
      }
      //

      $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
      $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
      $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("F$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($estiloTituloFilas3);

      $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$c, $new_date)
        ->setCellValue('B'.$c, ($i+1))
        ->setCellValue('C'.$c, $mon.''.number_format($capital, 2))
        ->setCellValue('D'.$c, $mon.''.number_format($amortizacion, 2))
        ->setCellValue('E'.$c, $mon.''.number_format($sobrecosto, 2))
        ->setCellValue('F'.$c, $mon.''.number_format($gps_mensual, 2))
        ->setCellValue('G'.$c, $mon.''.number_format($cuota_pagar, 2));

      $capital = $capital - $amortizacion;
      $total_sobrecosto = $total_sobrecosto + $sobrecosto;

      $c++;

    }

  }elseif($moneda=='sol_d'){

    for ($i=0; $i < $cuota; $i++) { 

      $seguro_mensual = 0.00;
			$gps_mensual = 0.00;

      if($i==0){
        $new_date = $fecha;
        $capital = $valor/$cambio;
      }

      $new_date = date("d-m-Y",strtotime($new_date."+ 1 month"));

      $cuota_pagar = 0.00;
      $cuota_pagar = (($interes/100)*pow(1+($interes/100),$cuota))*($valor/$cambio)/((pow(1+($interes/100),$cuota))-1);

      $sobrecosto = 0.00;
      $sobrecosto = $capital*($interes/100);

      $amortizacion = 0.00;
      if($cuota_pagar>0){
        $amortizacion = $cuota_pagar - $sobrecosto;
      }

      // GERSON (10-11-23)
      /* if($seguro_porcentaje > 0){
        $seguro_mensual = formato_moneda($seguro_porcentaje * $cuota_pagar / 100);
        $sobrecosto = $sobrecosto + $seguro_mensual;
        $cuota_pagar = $cuota_pagar + $seguro_mensual;
      } */

      if($chk_gps > 0){
        if($gps_precio > 0 && $cuota > 0){
          //$gps_mensual = formato_moneda(($gps_precio / $cuota)*$cambio);
          $gps_mensual = formato_moneda($gps_precio / $cuota);
          $cuota_pagar = $cuota_pagar + $gps_mensual;
        }
      }
      //

      $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
      $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
      $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("F$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($estiloTituloFilas3);
      $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloFilas3);

      $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$c, $new_date)
        ->setCellValue('B'.$c, ($i+1))
        ->setCellValue('C'.$c, $mon.''.number_format($capital, 2))
        ->setCellValue('D'.$c, $mon.''.number_format($amortizacion, 2))
        ->setCellValue('E'.$c, $mon.''.number_format($sobrecosto, 2))
        ->setCellValue('F'.$c, $mon.''.number_format($gps_mensual, 2))
        ->setCellValue('F'.$c, $mon.''.number_format($cuota_pagar, 2))
        ->setCellValue('G'.$c, $sub_mon.''.number_format($cuota_pagar*$cambio, 2));

      $capital = $capital - $amortizacion;
      $total_sobrecosto = $total_sobrecosto + $sobrecosto;

      $c++;

    }

  }

  // cuadros reverso
  if($moneda=='sol' || $moneda=='dolar_s'){

    $c = 4;

    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O".($c+1));
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '*Tasa de cambio únicamente referencial y sujeta a variaciones del mercado a la fecha de pago.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O".($c+1))->applyFromArray($estiloTituloFilas4);

    $c = 7;

    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", 'INDISPENSABLE');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloColumnas);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- BOLETAS, ESTADOS DE CUENTA, COMPRAS Y VENTAS,PDT´S.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- VISITA DOMILICIARIA.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- C4 (DNI) - LICENCIA DE CONDUCIR.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- RECIBO DE LUZ (CHICLAYO).');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);

    $d = 7;

    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", 'REQUISITOS');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:U$d")->applyFromArray($estiloTituloColumnas);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:S$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", '- GPS.');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:S$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("T$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("T$d", '$150.00');
    $objPHPExcel->getActiveSheet()->getStyle("T$d:U$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:S$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", '- SEGURO RIMAC.');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:S$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("T$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("T$d", '');
    $objPHPExcel->getActiveSheet()->getStyle("T$d:U$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:S$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", '- BOLETÍN INFORMATIVO.');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:S$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("T$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("T$d", 'S/ 15.00');
    $objPHPExcel->getActiveSheet()->getStyle("T$d:U$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:S$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", '- INSPECCIÓN MEC.');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:S$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("T$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("T$d", 'S/ 30.00');
    $objPHPExcel->getActiveSheet()->getStyle("T$d:U$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:S$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", '- NOTARÍA Y CHEQUE.');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:S$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("T$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("T$d", 'S/ 500.00 APROX.');
    $objPHPExcel->getActiveSheet()->getStyle("T$d:U$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("Q$d:S$d");
    $objPHPExcel->getActiveSheet()->setCellValue("Q$d", '- 1 LLAVE.');
    $objPHPExcel->getActiveSheet()->getStyle("Q$d:S$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("T$d:U$d");
    $objPHPExcel->getActiveSheet()->setCellValue("T$d", '');
    $objPHPExcel->getActiveSheet()->getStyle("T$d:U$d")->applyFromArray($estiloTituloFilas3);

    $e = 14;

    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", 'BENEFICIOS');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloColumnas);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- RÁPIDA APROBACIÓN.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- CREAS HISTORIAL CREDITICIO.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- EL CLIENTE DECIDE EN CUANTO TIEMPO PAGAR EL CRÉDITO.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- AMORTIZACIÓN A CAPITAL DESDE EL 2DO MES.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- NO COBRAMOS PENALIDADES POR LA LIQUIDACIÓN.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);

    $e = 21;

    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", 'ASESOR');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloColumnas);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", $colaborador);
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", $celular);
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);

  }elseif($moneda=='dolar' || $moneda=='sol_d'){
    $c = 7;

    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", 'INDISPENSABLE');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloColumnas);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- BOLETAS, ESTADOS DE CUENTA, COMPRAS Y VENTAS,PDT´S.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- VISITA DOMILICIARIA.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- C4 (DNI) - LICENCIA DE CONDUCIR.');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);
    $c++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$c:O$c");
    $objPHPExcel->getActiveSheet()->setCellValue("J$c", '- RECIBO DE LUZ (CHICLAYO).');
    $objPHPExcel->getActiveSheet()->getStyle("J$c:O$c")->applyFromArray($estiloTituloFilas4);

    $d = 7;

    $objPHPExcel->getActiveSheet()->mergeCells("P$d:T$d");
    $objPHPExcel->getActiveSheet()->setCellValue("P$d", 'REQUISITOS');
    $objPHPExcel->getActiveSheet()->getStyle("P$d:T$d")->applyFromArray($estiloTituloColumnas);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("P$d:R$d");
    $objPHPExcel->getActiveSheet()->setCellValue("P$d", '- GPS.');
    $objPHPExcel->getActiveSheet()->getStyle("P$d:R$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("S$d:T$d");
    $objPHPExcel->getActiveSheet()->setCellValue("S$d", '$150.00');
    $objPHPExcel->getActiveSheet()->getStyle("S$d:T$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("P$d:R$d");
    $objPHPExcel->getActiveSheet()->setCellValue("P$d", '- SEGURO RIMAC.');
    $objPHPExcel->getActiveSheet()->getStyle("P$d:R$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("S$d:T$d");
    $objPHPExcel->getActiveSheet()->setCellValue("S$d", '');
    $objPHPExcel->getActiveSheet()->getStyle("S$d:T$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("P$d:R$d");
    $objPHPExcel->getActiveSheet()->setCellValue("P$d", '- NOTARÍA Y CHEQUE.');
    $objPHPExcel->getActiveSheet()->getStyle("P$d:R$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("S$d:T$d");
    $objPHPExcel->getActiveSheet()->setCellValue("S$d", 'S/ 500.00 APROX.');
    $objPHPExcel->getActiveSheet()->getStyle("S$d:T$d")->applyFromArray($estiloTituloFilas3);
    $d++;
    $objPHPExcel->getActiveSheet()->mergeCells("P$d:R$d");
    $objPHPExcel->getActiveSheet()->setCellValue("P$d", '- 1 LLAVE.');
    $objPHPExcel->getActiveSheet()->getStyle("P$d:R$d")->applyFromArray($estiloTituloFilas4);
    $objPHPExcel->getActiveSheet()->mergeCells("S$d:T$d");
    $objPHPExcel->getActiveSheet()->setCellValue("S$d", '');
    $objPHPExcel->getActiveSheet()->getStyle("S$d:T$d")->applyFromArray($estiloTituloFilas3);

    $e = 14;

    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", 'BENEFICIOS');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloColumnas);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- RÁPIDA APROBACIÓN.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- CREAS HISTORIAL CREDITICIO.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- EL CLIENTE DECIDE EN CUANTO TIEMPO PAGAR EL CRÉDITO.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- AMORTIZACIÓN A CAPITAL DESDE EL 2DO MES.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", '- NO COBRAMOS PENALIDADES POR LA LIQUIDACIÓN.');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);

    $e = 21;

    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", 'ASESOR');
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloColumnas);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", $colaborador);
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);
    $e++;
    $objPHPExcel->getActiveSheet()->mergeCells("J$e:O$e");
    $objPHPExcel->getActiveSheet()->setCellValue("J$e", $celular);
    $objPHPExcel->getActiveSheet()->getStyle("J$e:O$e")->applyFromArray($estiloTituloFilas4);

  }
  //  


  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
  

  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('SIMULADOR '.$titulo_moneda);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

header("Content-type: text/xls");
header("Content-Disposition: attachment; filename=".$nombre_archivo.".xls");
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
