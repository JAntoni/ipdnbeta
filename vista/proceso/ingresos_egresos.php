<?php
	session_name("ipdnsac");
  session_start();

	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();

  require_once ("../funciones/funciones.php");
	require_once ("../funciones/fechas.php");

  $accion = $_POST['accion'];
  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);
  $proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);

  $action = 'registrar_precalificacion';

  $proceso = $oProceso->mostrarUno($proceso_id);
  $detalle = "";
  $tipo_cambio = 0.000;
  if($proceso['estado'] == 1){
    $detalle = $proceso['data']['tb_proceso_ing_det'];
    $tipo_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);
    $cliente_id = $proceso['data']['tb_cliente_id'];
    $fecha_ie = fecha_mysql($proceso['data']['tb_proceso_fecreg']);

    $procesosantiguos = 0;
    //BUSCAR PROCESOS ANTIGUOS
    $proceosCliente = $oProceso->mostrarProcesXCliente($cliente_id, $proceso_id, $fecha_ie);
    if($proceosCliente['estado'] == 1){
      $procesosantiguos = count($proceosCliente['data']);
      $idsprocesos = $proceosCliente['data'];
    }
    $proceosCliente = null;
  }
  $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

  $colaborador='';
  $tipogarveh=0;

  
?>
<div class="modal fade in" tabindex="-1" role="dialog" id="modal_ingresos_egresos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><strong>REPORTE DE INGRESOS Y GASTOS</strong></h4>
      </div>

      <input type="hidden" name="hdd_ie_proceso_id" id="hdd_ie_proceso_id" value="<?php echo $proceso_id; ?>">
      <input type="hidden" name="hdd_ie_usuario_id" id="hdd_ie_usuario_id" value="<?php echo $usuario_id; ?>">
      <input type="hidden" name="hdd_ie_tipo_cambio" id="hdd_ie_tipo_cambio" value="<?php echo $tipo_cambio; ?>">
      <input type="hidden" name="hdd_ie_proceso_fase_item_id" id="hdd_ie_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id; ?>">
      <input type="hidden" name="hdd_procesosantiguos" id="hdd_procesosantiguos" value="<?php echo $procesosantiguos; ?>">
      <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">
      <input type="hidden" name="hdd_fecha_ie" id="hdd_fecha_ie" value="<?php echo $fecha_ie; ?>">

      <div class="modal-body">

        <div class="box box-success class_menor">
              <div class="box-header with-border">
                  <h3 class="box-title"><strong>PROCESOS ANTIGUOS</strong></h3>
                  <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                          <i class="fa fa-minus"></i>
                      </button>
                  </div>
              </div>
              <div class="box-body">
                    <button type="button" class="btn btn-primary" onclick="verProcesosAntiguos(<?php echo $proceso_id;?>);">
                      <?php echo '<strong>VER PROCESOS : </strong>'.$procesosantiguos; ?>
                    </button>
              </div>
          </div>

        <div class="row">

          <?php if($tipo_cambio>0 || $tipo_cambio!=null || $tipo_cambio!=''){ ?>

            <div class="col-md-12">
              <section id="payment">
                <h5 class="page-header"><strong>Ingresos</strong></h5>

                <div class="row">

                  <div class="col-md-12">
                    <div class='col-md-3 h5'><label>Añadir Ingreso</label></div>
                    <div class='col-md-9' id="rowPlusIngreso">
                      <div class="input-group" id="buttonPlusIngreso">
                        <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoIngreso').removeClass('hidden'); $('#rowPlusIngreso').addClass('hidden'); limpiarCampos('I');" data-toggle="tooltip" title="Añadir Ingreso"><li class="fa fa-plus"></li></button>
                      </div>
                      <br>
                    </div>
                  </div>

                  <div class="col-md-12 hidden" id="divNuevoIngreso">
                    <form name="frmNuevoIngreso" id="frmNuevoIngreso">
                      <input type="hidden" name="action" value="registrar_ingreso">
                      <input type="hidden" name="hdd_i_proceso_id" id="hdd_i_proceso_id" value="<?php echo $proceso_id; ?>">
                      <input type="hidden" name="hdd_i_cliente_id" id="hdd_i_cliente_id" value="<?php echo $proceso['data']['tb_cliente_id']; ?>">
                      <input type="hidden" name="hdd_i_usuario_id" id="hdd_i_usuario_id" value="<?php echo $usuario_id; ?>">
                      <input type="hidden" name="hdd_i_proceso_fase_item_id" id="hdd_i_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id; ?>">


                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                        
                          <div class='col-md-4'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Ingreso</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="monto_i" id="monto_i" type="text" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="Monto"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-4 hidden'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Moneda</strong><span class="text-red">*</span></span>
                              <select name="moneda_i" id="moneda_i" class="form-control input-sm">
                                <option value="sol">Soles</option>
                                <!-- <option value="dolar">Dólares</option> -->
                              </select>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-8'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Origen</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="origen_i" id="origen_i" type="text" value="" placeholder="Ingrese Origen"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-5'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Antigüedad negocio</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="antiguedad_i" id="antiguedad_i" type="text" value="" placeholder="Ingrese Antigüedad"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-7'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Sustento</strong></span>
                              <input autocomplete="off" class="form-control input-sm" name="sustento_i" id="sustento_i" type="text" value="" placeholder="Ingrese Sustento"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-12'>
                            <div class="input-group">
                              <span class="input-group-addon"><strong>Comentario</strong></span>
                              <textarea class="form-control" name="verificacion_i" id="verificacion_i" rows="2" placeholder="Ingrese comentario ..." style="resize: none;"></textarea>
                            </div>
                            <br>
                          </div>

                          

                        </div>

                        <div class="col-md-12" align="right" >
                          <button class="btn btn-sm btn-primary" type="button" id="btnIngreso" onClick="guardarIngresoEgreso('frmNuevoIngreso', 'Ingreso','I')"><li class="fa fa-save"></li> Guardar</button>
                          <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoIngreso').addClass('hidden'); $('#frmNuevoIngreso').trigger('reset'); $('#rowPlusIngreso').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                        </div>
                      </div>
                    </form>
                    <br>
                  </div>

                  <div class="col-md-12" id="listaIngresoCliente">

                  </div>

                  <div class='col-md-12'>
                    <form name="frmNuevoIngresoDetalle" id="frmNuevoIngresoDetalle">
                      <input type="hidden" name="action" value="registrar_ingreso_detalle">
                      <input type="hidden" name="hdd_i_proceso_d_id" id="hdd_i_proceso_d_id" value="<?php echo $proceso_id; ?>">
                      <input type="hidden" name="hdd_i_cliente_d_id" id="hdd_i_cliente_d_id" value="<?php echo $proceso['data']['tb_cliente_id']; ?>">
                      <input type="hidden" name="hdd_i_usuario_d_id" id="hdd_i_usuario_d_id" value="<?php echo $usuario_id; ?>">
                      <input type="hidden" name="hdd_i_proceso_fase_item_d_id" id="hdd_i_proceso_fase_item_d_id" value="<?php echo $proceso_fase_item_id; ?>">

                      <div class='col-md-12'>
                        <div class="input-group">
                          <span class="input-group-addon"><strong>Detalle / Comentario</strong></span>
                          <textarea class="form-control" name="detalle_i" id="detalle_i" rows="3" placeholder="Ingrese Detalle / Comentario ..." onChange="guardarIngresoEgresoDetalle('frmNuevoIngresoDetalle', 'Ingreso','I')" onKeyDown="if(event.keyCode==13){guardarIngresoEgresoDetalle('frmNuevoIngresoDetalle', 'Ingreso','I');}" style="resize: none;"><?php echo $detalle; ?></textarea>
                        </div>
                        <br>
                      </div>
                    </form>
                  </div>


                </div>
              </section>
            </div>

            
            <div class="col-md-12">
              <section id="payment">
                <h5 class="page-header"><strong>Gastos</strong></h5>

                <div class="row">

                  <div class="col-md-12">
                    <div class='col-md-3 h5'><label>Añadir Gasto</label></div>
                    <div class='col-md-9' id="rowPlusEgreso">
                      <div class="input-group" id="buttonPlusEgreso">
                        <button class="btn btn-success" style="border-radius:50%;" type="button" onClick="$('#divNuevoEgreso').removeClass('hidden'); $('#rowPlusEgreso').addClass('hidden'); limpiarCampos('E');" data-toggle="tooltip" title="Añadir Egreso"><li class="fa fa-plus"></li></button>
                      </div>
                      <br>
                    </div>
                  </div>

                  <div class="col-md-12 hidden" id="divNuevoEgreso">
                    <form name="frmNuevoEgreso" id="frmNuevoEgreso">
                      <input type="hidden" name="action" value="registrar_egreso">
                      <input type="hidden" name="hdd_e_proceso_id" id="hdd_e_proceso_id" value="<?php echo $proceso_id; ?>">
                      <input type="hidden" name="hdd_e_cliente_id" id="hdd_e_cliente_id" value="<?php echo $proceso['data']['tb_cliente_id']; ?>">
                      <input type="hidden" name="hdd_e_usuario_id" id="hdd_e_usuario_id" value="<?php echo $usuario_id; ?>">
                      <input type="hidden" name="hdd_e_proceso_fase_item_id" id="hdd_e_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id; ?>">

                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                        
                          <div class='col-md-3'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Gasto</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="monto_e" id="monto_e" type="text" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="Monto"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-4'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Tipo</strong><span class="text-red">*</span></span>
                              <select name="tipo_e" id="tipo_e" class="form-control input-sm" onchange="tipoGasto()">
                                <option value="financiero">Financiero</option>
                                <option value="familiar">Familiar</option>
                                <option value="empresa">Empresa</option>
                              </select>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-4 hidden'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Moneda</strong><span class="text-red">*</span></span>
                              <select name="moneda_e" id="moneda_e" class="form-control input-sm">
                                <option value="sol">Soles</option>
                                <!-- <option value="dolar">Dólares</option> -->
                              </select>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-5'>	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Finalidad</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="finalidad_e" id="finalidad_e" type="text" value="" placeholder="Ingrese Finalidad"/>
                            </div>
                            <br>
                          </div>

                          <!-- Hoja azul -->
                          <div class='col-md-4 hidden' id="divGastoMe">	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Gasto M.E. ($)</strong></span>
                              <input autocomplete="off" class="form-control input-sm" name="montome_e" id="montome_e" type="text" onkeypress="return solo_decimal(event)" style="text-align: right;" placeholder="Monto M.E."/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-8 hidden' id="divEntidad">	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Entidad</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="entidad_e" id="entidad_e" type="text" value="" placeholder="Ingrese Entidad"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-4 hidden' id="divDetalleFamiliar">	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Detalle</strong><span class="text-red">*</span></span>
                              <select name="familiar_det_e" id="familiar_det_e" class="form-control input-sm">
                                <option value="alimetacion">Alimentación</option>
                                <option value="alquiler">Aquiler de casa</option>
                                <option value="vestido">Vestido y calzado</option>
                                <option value="educacion">Educación</option>
                                <option value="salud">Salud</option>
                                <option value="transporte">Transporte</option>
                                <option value="servicio">Servicio (Luz, agua, teléfono)</option>
                                <option value="junta">Juntas</option>
                                <option value="gasto_personal">Gastos personales</option>
                                <option value="otro">Otros</option>
                              </select>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-4 hidden' id="divDetalleEmpresa">	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Detalle</strong><span class="text-red">*</span></span>
                              <select name="empresa_det_e" id="empresa_det_e" class="form-control input-sm">
                                <option value="personal">Gasto de Personal</option>
                                <option value="servicio">Luz, agua, teléfono</option>
                                <option value="alquiler">Alquiler de local</option>
                                <option value="transporte">Transporte</option>
                                <option value="activo">Cuota activo fijo</option>
                                <option value="impuesto">Impuesto</option>
                                <option value="otro">Otros</option>
                              </select>
                            </div>
                            <br>
                          </div>
                          <!--  -->

                          <div class='' id="divFrecuencia">	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Frecuencia</strong><span class="text-red">*</span></span>
                              <input autocomplete="off" class="form-control input-sm" name="frecuencia_e" id="frecuencia_e" type="text" value="" placeholder="Ingrese Frecuencia"/>
                            </div>
                            <br>
                          </div>

                          <div class='' id="divSustento">	
                            <div class="input-group" >
                              <span class="input-group-addon"><strong>Sustento</strong></span>
                              <input autocomplete="off" class="form-control input-sm" name="sustento_e" id="sustento_e" type="text" value="" placeholder="Ingrese Sustento"/>
                            </div>
                            <br>
                          </div>

                          <div class='col-md-12'>
                            <div class="input-group">
                              <span class="input-group-addon"><strong>Comentario</strong></span>
                              <textarea class="form-control" name="verificacion_e" id="verificacion_e" rows="2" placeholder="Ingrese comentario ..." style="resize: none;"></textarea>
                            </div>
                            <br>
                          </div>

                        </div>

                        <div class="col-md-12" align="right" >
                          <button class="btn btn-sm btn-primary" type="button" id="btnEgreso" onClick="guardarIngresoEgreso('frmNuevoEgreso', 'Egreso','E')"><li class="fa fa-save"></li> Guardar</button>
                          <button class="btn btn-sm btn-danger" type="button" onClick="$('#divNuevoEgreso').addClass('hidden'); $('#frmNuevoEgreso').trigger('reset'); $('#rowPlusEgreso').removeClass('hidden');"><li class="fa fa-ban"></li> Cancelar</button>
                        </div>
                      </div>
                    </form>
                    <br>
                  </div>
            
                  <div class="col-md-12" id="listaEgresoCliente">

                  </div>

                </div>
              </section>
            </div>

          <?php }else{ ?>
            <h3>Aún no ha fijado un tipo de cambio.</h3>
          <?php } ?>

        </div>

      </div>

      <div class="modal-footer">
        <input type="hidden" id="monto_ingreso" value="0.00">
        <input type="hidden" id="monto_egreso" value="0.00">
        <?php if($tipo_cambio>0 || $tipo_cambio!=null || $tipo_cambio!=''){ ?>
          <span class="h4" id="total_IE" style="margin-right: 10px;"></span>
          <button type="button" id="btn_exportar_ingresosegresos" class="btn btn-success" onclick="exportarIEExcel(<?php echo $proceso_id; ?>, <?php echo $usuario_id; ?>)"><span class="fa fa-file-excel-o"></span> EEGGPP</button>
          <?php if($precalificacion['estado'] == 1) { ?>
            <button type="button" id="btn_exportar_precalificacion" class="btn btn-primary" onclick="exportarHojaPDF(<?php echo $proceso_id; ?>)"><span class="fa fa-file-pdf-o"></span> Exportar</button>
          <?php } ?>
        <?php }else{ ?>
          <!-- <h3>Aún no ha fijado un tipo de cambio.</h3> -->
        <?php } ?>
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>


    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/proceso/ingresos_egresos_form.js?ver=19102023';?>"></script>
<!-- <script type="text/javascript" src="<?php //echo 'vista/proceso/reloj.js?ver=31082023'; ?>"></script> -->
