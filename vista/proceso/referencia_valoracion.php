<?php
	session_name("ipdnsac");
  session_start();

	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../precliente/Precliente.class.php');
  $oPrecliente = new Precliente();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../vehiculomarca/Vehiculomarca.class.php');
  $oVehiculomarca = new Vehiculomarca();
  require_once('../vehiculomodelo/Vehiculomodelo.class.php');
  $oVehiculomodelo = new Vehiculomodelo();

  require_once ("../funciones/funciones.php");
	require_once ("../funciones/fechas.php");

  $accion = $_POST['accion'];
  $proceso_id = intval($_POST['proceso_id']);
  $usuario_id = intval($_POST['usuario_id']);
  $proceso_fase_item_id = intval($_POST['proceso_fase_item_id']);
  $valoracion_id = 0;

  $action = 'registrar_valoracion';

  $proceso = $oProceso->mostrarUno($proceso_id);
  $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
  $valoracion = $oProceso->mostrarUnoValoracion($proceso_id);
  $colaborador='';
  $tipogarveh=0;
  $tip_cambio = 0.00;

  $descripcion   = '';
  $observacion   = '';
  $extra   = '';
  // Depreciacion Interna
  $estetica   = '';
  $electrica  = '';
  $interior   = '';
  $mecanica   = '';
  $repuesto   = '';
  $otro       = '';
  // Depreciacion Externa
  $des1   = "";
  $monto1 = '';
  $des2   = "";
  $monto2 = '';
  $des3   = "";
  $monto3 = '';
  $des4   = "";
  $monto4 = '';
  $des5   = "";
  $monto5 = '';
  $des6   = "";
  $monto6 = '';
  // Montos
  $monto_final_mn = '';
  $monto_final_me = '';
  $monto_oferta_sc_mn = '';
  $monto_oferta_sc_me = '';
  $monto_oferta_cc_mn = '';
  $monto_oferta_cc_me = '';
  //
  
  $fecha_prueba = '';
  $marca = '';
  $modelo = '';
  $anio = '';
  $km = '';
  $moneda = '';
  $cambio = '';
  $bandera = '';

  if($proceso['estado']==1){
    $usuario = $oUsuario->mostrarUno($proceso['data']['tb_usuario_id']);
    $client = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
    $preclient = $oPrecliente->mostrarUno($proceso['data']['tb_precliente_id']);
    $tipogarveh = intval($proceso['data']['tb_cgarvtipo_id']);
    if($usuario['estado']==1){
      $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
    }
    if($client['estado']==1){
      $cliente = $client['data']['tb_cliente_nom'];
      $dni = $client['data']['tb_cliente_doc'];
    }else{
      if($preclient['estado']==1){
        $cliente = $preclient['data']['tb_precliente_nombres'];
        $dni = $preclient['data']['tb_precliente_numdoc'];
      }
    }

		$tip_cambio = $proceso['data']['tb_proceso_tip_cambio'];

    // Datos traídos de precalificaion
    if($precalificacion['estado']==1){
      $fecha_prueba = ($precalificacion['data']['tb_precalificacion_fec_inspeccion'] != NULL) ? mostrar_fecha($precalificacion['data']['tb_precalificacion_fec_inspeccion']) : '';
      $marca_id = $precalificacion['data']['tb_precalificacion_marca'] ? intval($precalificacion['data']['tb_precalificacion_marca']) : 0;
      $marca = '';
      if($marca_id>0){
        $dt_marca = $oVehiculomarca->mostrarUno($marca_id);
        if($dt_marca['estado']==1){
          $marca = $dt_marca['data']['tb_vehiculomarca_nom'];
        }
      }
      $modelo_id = $precalificacion['data']['tb_precalificacion_modelo'] ? intval($precalificacion['data']['tb_precalificacion_modelo']) : 0;
      $modelo = '';
      if($modelo_id>0){
        $dt_modelo = $oVehiculomodelo->mostrarUno($modelo_id);
        if($dt_modelo['estado']==1){
          $modelo = $dt_modelo['data']['tb_vehiculomodelo_nom'];
        }
      }
      $anio = $precalificacion['data']['tb_precalificacion_anio'] ? $precalificacion['data']['tb_precalificacion_anio'] : '';
      $km = $precalificacion['data']['tb_precalificacion_km'] ? floatval($precalificacion['data']['tb_precalificacion_km']) : '';
    }
    
    $moneda = $proceso['data']['tb_proceso_moneda']; // sol, dolar
    $cambio = $proceso['data']['tb_proceso_tip_cambio'];

    if($fecha_prueba == '' || $marca == '' || $modelo == '' || $cambio == NULL || $anio == '' || $km == ''){
      $bandera = 'no_procede';
    }
    //
  }
  
  $valoracion = $oProceso->mostrarUnoValoracion($proceso_id);

  if($valoracion['estado']==1){

    $accion = 'edit';

    $valoracion_id = $valoracion['data']['tb_valoracion_id'];
    $action = 'actualizar_valoracion';

    $descripcion  = $valoracion['data']['tb_valoracion_des'] ? $valoracion['data']['tb_valoracion_des'] : '';
    $observacion  = $valoracion['data']['tb_valoracion_obs'] ? $valoracion['data']['tb_valoracion_obs'] : '';
    $extra        = $valoracion['data']['tb_valoracion_extra'] ? floatval($valoracion['data']['tb_valoracion_extra']) : '';
    // Depreciacion Interna
    $estetica   = $valoracion['data']['tb_valoracion_estetica'] ? floatval($valoracion['data']['tb_valoracion_estetica']) : '';
    $electrica  = $valoracion['data']['tb_valoracion_electrica'] ? floatval($valoracion['data']['tb_valoracion_electrica']) : '';
    $interior   = $valoracion['data']['tb_valoracion_interior'] ? floatval($valoracion['data']['tb_valoracion_interior']) : '';
    $mecanica   = $valoracion['data']['tb_valoracion_mecanica'] ? floatval($valoracion['data']['tb_valoracion_mecanica']) : '';
    $repuesto   = $valoracion['data']['tb_valoracion_repuesto'] ? floatval($valoracion['data']['tb_valoracion_repuesto']) : '';
    $otro       = $valoracion['data']['tb_valoracion_otro'] ? floatval($valoracion['data']['tb_valoracion_otro']) : '';
    // Depreciacion Externa
    $des1   = $valoracion['data']['tb_valoracion_ext_des1'] ? $valoracion['data']['tb_valoracion_ext_des1'] : '';
    $monto1 = $valoracion['data']['tb_valoracion_ext_monto1'] ? floatval($valoracion['data']['tb_valoracion_ext_monto1']) : '';
    $des2   = $valoracion['data']['tb_valoracion_ext_des2'] ? $valoracion['data']['tb_valoracion_ext_des2'] : '';
    $monto2 = $valoracion['data']['tb_valoracion_ext_monto2'] ? floatval($valoracion['data']['tb_valoracion_ext_monto2']) : '';
    $des3   = $valoracion['data']['tb_valoracion_ext_des3'] ? $valoracion['data']['tb_valoracion_ext_des3'] : '';
    $monto3 = $valoracion['data']['tb_valoracion_ext_monto3'] ? floatval($valoracion['data']['tb_valoracion_ext_monto3']) : '';
    $des4   = $valoracion['data']['tb_valoracion_ext_des4'] ? $valoracion['data']['tb_valoracion_ext_des4'] : '';
    $monto4 = $valoracion['data']['tb_valoracion_ext_monto4'] ? floatval($valoracion['data']['tb_valoracion_ext_monto4']) : '';
    $des5   = $valoracion['data']['tb_valoracion_ext_des5'] ? $valoracion['data']['tb_valoracion_ext_des5'] : '';
    $monto5 = $valoracion['data']['tb_valoracion_ext_monto5'] ? floatval($valoracion['data']['tb_valoracion_ext_monto5']) : '';
    $des6   = $valoracion['data']['tb_valoracion_ext_des6'] ? $valoracion['data']['tb_valoracion_ext_des6'] : '';
    $monto6 = $valoracion['data']['tb_valoracion_ext_monto6'] ? floatval($valoracion['data']['tb_valoracion_ext_monto6']) : '';
    // Montos
    $monto_final_mn = $valoracion['data']['tb_valoracion_valor_final_mn'] ? floatval($valoracion['data']['tb_valoracion_valor_final_mn']) : '';
    $monto_final_me = $valoracion['data']['tb_valoracion_valor_final_me'] ? floatval($valoracion['data']['tb_valoracion_valor_final_me']) : '';
    $monto_oferta_sc_mn = $valoracion['data']['tb_valoracion_valor_oferta_sc_mn'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_sc_mn']) : '';
    $monto_oferta_sc_me = $valoracion['data']['tb_valoracion_valor_oferta_sc_me'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_sc_me']) : '';
    $monto_oferta_cc_mn = $valoracion['data']['tb_valoracion_valor_oferta_cc_mn'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_cc_mn']) : '';
    $monto_oferta_cc_me = $valoracion['data']['tb_valoracion_valor_oferta_cc_me'] ? floatval($valoracion['data']['tb_valoracion_valor_oferta_cc_me']) : '';
  }
  
?>
<?php if($bandera == 'no_procede'):?>
  <div class="modal fade in" tabindex="-1" role="dialog" id="modal_ref_valoracion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><strong>IMPORTANTE</strong></h4>
          <div class="modal-body">
            <h4 class="modal-title"><strong>Debes llenar todos los datos de la simulación y precalificación antes de llenar los datos de esta REFERENCIA DE VALORACIÓN</strong></h4>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Fecha de Prueba: <?php echo $fecha_prueba;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Marca: <?php echo $marca;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Modelo: <?php echo $modelo;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Año: <?php echo $anio;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Tipo de Cambio: <?php echo $cambio;?></span>
            </label>
            <label class="col-sm-8 control-label">
              <span class="fa fa-circle text-green" style="font-size: 15px;">Kilometraje: <?php echo $km;?></span>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif;?>

<?php if($bandera == ''):?>
  <div class="modal fade in" tabindex="-1" role="dialog" id="modal_ref_valoracion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><strong>REFERENCIA DE VALORACIÓN <?php echo 'PROCESO ID: '.$proceso_id;?></strong></h4>
        </div>

        <form id="form_referencia_valoracion" method="post">
          <input type="hidden" name="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_ref_valoracion_id" id="hdd_ref_valoracion_id" value="<?php echo $valoracion_id; ?>">
          <input type="hidden" name="hdd_ref_proceso_id" id="hdd_ref_proceso_id" value="<?php echo $proceso_id; ?>">
          <input type="hidden" name="hdd_ref_usuario_id" id="hdd_ref_usuario_id" value="<?php echo $usuario_id; ?>">
          <input type="hidden" name="hdd_ref_proceso_fase_item_id" id="hdd_ref_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id; ?>">

          <div class="modal-body">

            <div class="row">

              <div class="col-md-12">
                <div class="form-group">
                  <p><strong>Colaborador: </strong><span><i><?php echo $colaborador; ?></i></span></p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Marca: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;"><?php echo $marca; ?></label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Modelo: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;"><?php echo $modelo; ?></label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Año: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;"><?php echo $anio; ?></label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Kilometraje: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;"><?php echo $km; ?>Km</label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Prueba de manejo: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;">Herbert Engel Gálvez Rojas - SUB GERENTE GENERAL</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Fecha de prueba: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;"><?php echo $fecha_prueba; ?></label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Tipo Cambio: </label>
                  <div class="input-group date">
                    <label class="radio-inline" style="padding-left: 0px;"><strong><?php echo $tip_cambio; ?></strong></label>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Descripción de estado de vehículo: </label>
                  <div class="">
                    <textarea class="form-control input-sm" rows="3" name="valoracion_des" id="valoracion_des" placeholder="" style="resize: none;"><?php echo $descripcion; ?></textarea>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <section id="payment">
                  <h5 class="page-header"><strong>Depreciación Interna IPDN</strong></h5>

                  <div class="row">

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">ESTÉTICA</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_estetica" id="valoracion_estetica" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $estetica ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">ELÉCTRICA</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_electrica" id="valoracion_electrica" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $electrica ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">INTERIORES</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_interior" id="valoracion_interior" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $interior ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">MECÁNICA</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_mecanica" id="valoracion_mecanica" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $mecanica ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">REPUESTOS</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_repuesto" id="valoracion_repuesto" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $repuesto ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">OTROS</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_otro" id="valoracion_otro" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $otro ?>">
                        </div>
                      </div>
                    </div>

                  </div>
                </section>
              </div>

              <div class="col-md-8">
                <section id="payment">
                  <h5 class="page-header"><strong>Depreciación Externa Taller Convenio</strong></h5>

                  <div class="row">

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-1 control-label">1°</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_des1" id="valoracion_ext_des1" placeholder="Descripción" value="<?php echo $des1 ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_monto1" id="valoracion_ext_monto1" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $monto1 ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-1 control-label">2°</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_des2" id="valoracion_ext_des2" placeholder="Descripción" value="<?php echo $des2 ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_monto2" id="valoracion_ext_monto2" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $monto2 ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-1 control-label">3°</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_des3" id="valoracion_ext_des3" placeholder="Descripción" value="<?php echo $des3 ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_monto3" id="valoracion_ext_monto3" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $monto3 ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-1 control-label">4°</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_des4" id="valoracion_ext_des4" placeholder="Descripción" value="<?php echo $des4 ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_monto4" id="valoracion_ext_monto4" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $monto4 ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-1 control-label">5°</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_des5" id="valoracion_ext_des5" placeholder="Descripción" value="<?php echo $des5 ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_monto5" id="valoracion_ext_monto5" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $monto5 ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 3px">
                      <div class="form-group">
                        <label class="col-sm-1 control-label">6°</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_des6" id="valoracion_ext_des6" placeholder="Descripción" value="<?php echo $des6 ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control input-sm" name="valoracion_ext_monto6" id="valoracion_ext_monto6" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)" value="<?php echo $monto6 ?>">
                        </div>
                      </div>
                    </div>

                  </div>
                </section>
                <br>
              </div>

              <div class="col-md-9">
                <div class="form-group">
                  <label>Observación: </label>
                  <div class="">
                    <textarea class="form-control input-sm" rows="3" name="valoracion_obs" id="valoracion_obs" placeholder="Observación" style="resize: none;"><?php echo $observacion; ?></textarea>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Depreciación (%): </label>
                    <div class="input-group">
                      <input type="text" class="form-control input-sm" name="valoracion_extra" id="valoracion_extra" placeholder="0.00%" style="text-align: right" onkeypress="return solo_decimal(event)" value="<?php echo $extra ?>">
                    </div>
                </div>
              </div>
              
              <div class="col-md-12 <?php if($action == 'registrar_valoracion'){ echo 'hidden'; } ?>" id="divValoraciones">
                <section id="payment">
                  <h5 class="page-header"><strong>Valoraciones</strong></h5>

                  <input type="hidden" name="hdd_det_valoracion_id" id="hdd_det_valoracion_id" value="<?php echo $valoracion_id ?>">

                  <div class="row">

                    <div class="col-md-1" style="padding-right: 0px">
                      <div class="form-group">
                        <label>Moneda: </label>
                        <div class="input-group">
                          <select class="form-control input-sm" name="valoraciondetalle_moneda" id="valoraciondetalle_moneda">
                            <option selected="selected" value="1">S/</option>
                            <option value="2">$</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Monto: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="valoraciondetalle_monto" id="valoraciondetalle_monto" style="text-align: right" placeholder="S/ 0.00" onkeypress="return solo_decimal(event)">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Año: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="valoraciondetalle_anio" id="valoraciondetalle_anio" placeholder="Año vehículo">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Km: </label>
                        <div class="input-group">
                          <input type="text" class="form-control input-sm" name="valoraciondetalle_km" id="valoraciondetalle_km" style="text-align: right" placeholder="0.00" onkeypress="return solo_decimal(event)">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Link: </label>
                        <div class="input-group" style="width: 100%">
                          <input type="text" class="form-control input-sm" name="valoraciondetalle_link" id="valoraciondetalle_link" placeholder="Link">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-1">
                      <div class="form-group">
                        <label> </label>
                        <div class="input-group">
                          <button type="button" class="btn btn-primary btn-sm" id="guardar_valoraciondetalle" name="guardar_valoraciondetalle" onclick="guardarValoracionDetalle('registrar_valoracion_detalle',<?php echo $valoracion_id; ?>)" /><strong><i class="fa fa-save"></i> Añadir</strong></button>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12" id="listaValoraciones">

                    </div>
                      
                  </div>
                </section>
              </div>

            </div>

          </div>

          <div class="modal-footer">
            <?php if($accion === 'reg') { ?>
              <button type="submit" id="btn_guardar_referencia_valoracion" class="btn btn-success">Guardar</button>
            <?php }else{ ?>
              <button type="button" id="btn_exportar_referencia_valoracion" class="btn btn-primary" onclick="exportarValoracionPDF(<?php echo $proceso_id; ?>,<?php echo $usuario_id; ?>,<?php echo $proceso_fase_item_id; ?>,<?php echo $valoracion_id; ?>)"><span class="fa fa-file-pdf-o"></span> Exportar</button>
              <button type="button" class="btn btn-info" id="btn_referencia_valoracion_file" onclick="valoracionFile('I',<?php echo $valoracion_id; ?>, 0, <?php echo $proceso_id; ?>,<?php echo $usuario_id; ?>,<?php echo $proceso_fase_item_id; ?>)" title="Subir fotos referenciales"><i class="fa fa-camera"></i> Subir Fotos</button>
              <button type="submit" id="btn_guardar_referencia_valoracion" class="btn btn-success" onclick="">Actualizar</button>
            <?php } ?>
            <button type="button" id="btn_cerrar" class="btn btn-danger" onclick="" data-dismiss="modal">Cerrar</button>
          </div>

        </form>

      </div>
    </div>
  </div>
  <style>
    .color {
      background-color: #FFE7E6;
    }
    input:required {
      border: 1px solid #ccc !important;
    }
  </style>
  <script type="text/javascript" src="<?php echo 'vista/proceso/referencia_valoracion_form.js?ver=1234567';?>"></script>
<?php endif;?>