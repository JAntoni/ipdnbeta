<?php
session_name("ipdnsac");
session_start();

define('FPDF_FONTPATH','font/'); 
include('../../static/fpdf/fpdf.php');

require_once ('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ('../vehiculomarca/Vehiculomarca.class.php');
$oVehiculomarca = new Vehiculomarca();
require_once ('../vehiculomodelo/Vehiculomodelo.class.php');
$oVehiculomodelo = new Vehiculomodelo();
require_once('../transferente/Transferente.class.php');
$oTransferente = new Transferente();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
	
$proceso_id = "";
if(isset($_GET['proceso_id'])){
	$proceso_id=$_GET['proceso_id'];
}
$accion = "sin_firma";
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}

$proceso = $oProceso->mostrarUno($proceso_id);
$user = "";
$tipo_cambio = 0.00;
$otro_id = 0;

if($proceso['estado'] == 1){

    if(!empty($proceso['data']['tb_proceso_usu_firma_otro'])){
        $otro_id = intval($proceso['data']['tb_proceso_usu_firma_otro']);
    }

    $usuario = $oUsuario->mostrarUno($proceso['data']['tb_usuario_id']);
    $tipogarveh = intval($proceso['data']['tb_cgarvtipo_id']);
    if($usuario['estado']==1){
      $colaborador = $usuario['data']['tb_usuario_nom'].' '.$usuario['data']['tb_usuario_ape'];
    }
    $tipo_cambio = floatval($proceso['data']['tb_proceso_tip_cambio']);
}

$fecha_hoy = date("d/m/Y H:i:s");

$dts = $oProceso->mostrarUnoPrecalificacion($proceso_id);
$ingresos = $oProceso->mostrarIEXProceso($proceso_id, 'I');
$egresos = $oProceso->mostrarIEXProceso($proceso_id, 'E');


$pdf=new FPDF();
$pdf->AddPage('P','A4');


if($dts['estado'] == 1){

    // firma de miembro invitado 
    if($dts['data']['']){

    }
    //

    $pdf->SetFillColor(220,220,220);
    $pdf->SetTextColor(0, 0, 0);
    $alto=5;

    $pdf->Ln(1);
    $pdf->SetFont('Arial','I',8);
    $pdf->SetXY(5, $alto);
    $pdf->Cell(15, 6, utf8_decode("Asesor: ".$colaborador), 0 , 1, 'L');
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(190, $alto);
    $pdf->Cell(15, 6, utf8_decode($fecha_hoy), 0 , 1, 'R');

    $pdf->Ln(4);
    $pdf->SetFont('Arial','B',12);
    //$pdf->Cell(190,5,utf8_decode("FORMATO DE DE EVALUACIÓN CREDITICIA - COMITÉ DE CRÉDITO"),0,1,'C');



    $pdf->SetXY(5, 12);
    $pdf->Cell(80, 14, utf8_decode("FORMATO DE DE EVALUACIÓN CREDITICIA - COMITÉ DE CRÉDITO"), 0 , 1, 'L');
    $pdf->SetXY(145, 12);
    $pdf->Cell(15, 3,$pdf->Image('../../public/images/logopres.png', null, null, 55, 15, 'PNG'), 0 , 1, 'R');


    $pdf->Ln(2);


    //$pdf->Image('../../public/images/logopres.png', 40, 110, 130, 65, 'PNG'); // MARCA DE AGUA 


    /* CLIENTE */

    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,3,utf8_decode("CLIENTE"), 0, 1, 'L');

    $pdf->Ln(4);

    $alto=40;

    // Tipo CLiente
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('TIPO CLIENTE: '), 0, 1);
    $pdf->SetXY(28, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_tipo_cliente']!=null || $dts['data']['tb_precalificacion_tipo_cliente']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_tipo_cliente']=='natural'){
            $pdf->Cell(34, 6, utf8_decode('Persona Natural'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_tipo_cliente']=='juridica'){
            $pdf->Cell(34, 6, utf8_decode('Persona Jurídica'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(34, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Modalidad
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(63, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('MODALIDAD: '), 0, 1);
    $pdf->SetXY(83, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_modalidad']!=null || $dts['data']['tb_precalificacion_modalidad']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_modalidad']=='presencial'){
            $pdf->Cell(25, 6, utf8_decode('Presencial'), 1 , 1);
        }elseif($dts['data']['tb_precalificacion_modalidad']=='virtual'){
            $pdf->Cell(25, 6, utf8_decode('Virtual'), 1 , 1);
        }elseif($dts['data']['tb_precalificacion_modalidad']=='exonerada'){
            $pdf->Cell(25, 6, utf8_decode('Exonerado'), 1 , 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(25, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Nacionalidad
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(110, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('NACIONALIDAD: '), 0, 1);
    $pdf->SetXY(135, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_nacionalidad']!=null || $dts['data']['tb_precalificacion_nacionalidad']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(23, 6, utf8_decode($dts['data']['tb_precalificacion_nacionalidad']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(23, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Edad
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(160, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('EDAD: '), 0, 1);
    $pdf->SetXY(172, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_edad']!=null || $dts['data']['tb_precalificacion_edad']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(32, 6, utf8_decode($dts['data']['tb_precalificacion_edad']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(32, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Razón Social
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('RAZÓN SOCIAL: '), 0, 1);
    $pdf->SetXY(30, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_razon_social']!=null || $dts['data']['tb_precalificacion_razon_social']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(108, 6, utf8_decode($dts['data']['tb_precalificacion_razon_social']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(108, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // RUC
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(140, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('RUC: '), 0, 1);
    $pdf->SetXY(150, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_ruc']!=null || $dts['data']['tb_precalificacion_ruc']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(54, 6, utf8_decode($dts['data']['tb_precalificacion_ruc']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(54, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Cliente
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('CLIENTE: '), 0, 1);
    $pdf->SetXY(20, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_cliente']!=null || $dts['data']['tb_precalificacion_cliente']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(118, 6, utf8_decode($dts['data']['tb_precalificacion_cliente']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(118, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // DNI
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(140, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('DNI: '), 0, 1);
    $pdf->SetXY(148, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_dni']!=null || $dts['data']['tb_precalificacion_dni']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(56, 6, utf8_decode($dts['data']['tb_precalificacion_dni']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(56, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    /*  */

    /* VEHÍCULO */

    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,3,utf8_decode("VEHÍCULO"), 0, 1, 'L');

    $pdf->Ln(3);

    $alto=70;

    // Marca
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('MARCA: '), 0, 1);
    $pdf->SetXY(19, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_marca']!=null || $dts['data']['tb_precalificacion_marca']!=''){
        $marca = $oVehiculomarca->mostrarUno($dts['data']['tb_precalificacion_marca']);
        if($marca['estado']==1){
            $pdf->SetFont('Arial','',8);
        $pdf->Cell(67, 6, utf8_decode($marca['data']['tb_vehiculomarca_nom']), 1, 1);
        }else{
            $pdf->SetFont('Arial','I',8);
            $pdf->SetTextColor(205, 205, 205);
            $pdf->Cell(67, 6, 'Sin datos', 1 , 1);
            $pdf->SetTextColor(0, 0, 0);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(67, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Modelo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(90, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('MODELO: '), 0, 1);
    $pdf->SetXY(105, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_modelo']!=null || $dts['data']['tb_precalificacion_modelo']!=''){
        $modelo = $oVehiculomodelo->mostrarUno($dts['data']['tb_precalificacion_modelo']);
        if($modelo['estado']==1){
            $pdf->SetFont('Arial','',8);
        $pdf->Cell(99, 6, utf8_decode($modelo['data']['tb_vehiculomodelo_nom']), 1, 1);
        }else{
            $pdf->SetFont('Arial','I',8);
            $pdf->SetTextColor(205, 205, 205);
            $pdf->Cell(99, 6, 'Sin datos', 1 , 1);
            $pdf->SetTextColor(0, 0, 0);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(99, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }


    $alto=$alto+7;

    // Proveedor
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('PROVEEDOR: '), 0, 1);
    $pdf->SetXY(27, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_proveedor_id']!=null || $dts['data']['tb_precalificacion_proveedor_id']!=''){
        $provee = $oTransferente->mostrarUno($dts['data']['tb_precalificacion_proveedor_id']);
        if($provee['estado']==1){
            $pdf->SetFont('Arial','',8);
            $pdf->Cell(129, 6, utf8_decode($provee['data']['tb_transferente_nom']), 1, 1);
        }else{
            $pdf->SetFont('Arial','I',8);
            $pdf->SetTextColor(205, 205, 205);
            $pdf->Cell(129, 6, 'Sin datos', 1 , 1);
            $pdf->SetTextColor(0, 0, 0);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(129, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Año
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(160, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('AÑO: '), 0, 1);
    $pdf->SetXY(170, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_anio']!=null || $dts['data']['tb_precalificacion_anio']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(34, 6, utf8_decode($dts['data']['tb_precalificacion_anio']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(34, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Licencia MTC
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('LICENCIA MTC: '), 0, 1);
    $pdf->SetXY(28, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_licencia']!=null || $dts['data']['tb_precalificacion_licencia']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_licencia']=='s'){
            $pdf->Cell(8, 6, utf8_decode('SI'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_licencia']=='n'){
            $pdf->Cell(8, 6, utf8_decode('NO'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(8, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Tipo Licencia
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(38, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('TIPO LICENCIA: '), 0, 1);
    $pdf->SetXY(62, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_tipo_licencia']!=null || $dts['data']['tb_precalificacion_tipo_licencia']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(15, 6, utf8_decode($dts['data']['tb_precalificacion_tipo_licencia']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(15, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Vigencia Licencia
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(79, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VIGENCIA LICENCIA: '), 0, 1);
    $pdf->SetXY(109, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_licencia_vigencia']!=null || $dts['data']['tb_precalificacion_licencia_vigencia']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(17, 6, utf8_decode(mostrar_fecha($dts['data']['tb_precalificacion_licencia_vigencia'])), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(17, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Papeletas
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(129, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('PAPELETAS: '), 0, 1);
    $pdf->SetXY(149, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_papeleta']!=null || $dts['data']['tb_precalificacion_papeleta']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_papeleta']=='s'){
            $pdf->Cell(7, 6, utf8_decode('SI'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_papeleta']=='n'){
            $pdf->Cell(7, 6, utf8_decode('NO'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(7, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Valor Vehículo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(159, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VALOR VEHÍCULO: '), 0, 1);
    $pdf->SetXY(188, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_monto']!=null || $dts['data']['tb_precalificacion_monto']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(16, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_monto'])), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(16, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Valor de cuota
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VALOR CUOTA: '), 0, 1);
    $pdf->SetXY(29, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_cuota']!=null || $dts['data']['tb_precalificacion_cuota']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(17, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_cuota'])), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(17, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Tasa propuesta
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(49, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('TASA PROPUESTA: '), 0, 1);
    $pdf->SetXY(78, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_tasa']!=null || $dts['data']['tb_precalificacion_tasa']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(14, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_tasa']).'%'), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(14, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Plazo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(95, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('PLAZO: '), 0, 1);
    $pdf->SetXY(108, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_licencia_plazo']!=null || $dts['data']['tb_precalificacion_licencia_plazo']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(14, 6, utf8_decode($dts['data']['tb_precalificacion_licencia_plazo']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(14, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Kilometros
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(125, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('KILOMETROS: '), 0, 1);
    $pdf->SetXY(148, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_km']!=null || $dts['data']['tb_precalificacion_km']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(18, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_km']).'Km'), 1, 1, 'L');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(18, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Placa
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(169, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('PLACA: '), 0, 1);
    $pdf->SetXY(183, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_placa']!=null || $dts['data']['tb_precalificacion_placa']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(21, 6, utf8_decode($dts['data']['tb_precalificacion_placa']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(21, 6, 'Sin datos', 1, 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Valor de prestamo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VALOR DE PRÉSTAMO: '), 0, 1);
    $pdf->SetXY(40, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_precio_propuesto']!=null || $dts['data']['tb_precalificacion_precio_propuesto']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(20, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_precio_propuesto'])), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(20, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Verificado
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(64, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VERIFICADO: '), 0, 1);
    $pdf->SetXY(84, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_verificado']!=null || $dts['data']['tb_precalificacion_verificado']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_verificado']=='s'){
            $pdf->Cell(15, 6, utf8_decode('SI'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_verificado']=='n'){
            $pdf->Cell(15, 6, utf8_decode('NO'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(15, 6, 'Sin datos', 1, 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Metodo verificacion
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(103, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('MÉTODO VERIFICACIÓN: '), 0, 1);
    $pdf->SetXY(140, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_verificado_metodo']!=null || $dts['data']['tb_precalificacion_verificado_metodo']!=''){

        $pdf->SetFont('Arial','',8);
		$y1 = $pdf->GetY(); // inicio de la fila
		$pdf->MultiCell(64,5,utf8_decode($dts['data']['tb_precalificacion_verificado_metodo']), 1, 1);
		$y2 = $pdf->GetY(); // final de de la fila en el eje Y

    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(64, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    //$alto=$y2;
    if($y2>$y1){
        $alto=$y2+1;
    }else{
        $alto=$alto+7;
    }

    // Tipo operacion
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('TIPO OPERACIÓN: '), 0, 1);
    $pdf->SetXY(33, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_tipo_operacion']!=null || $dts['data']['tb_precalificacion_tipo_operacion']!=''){
        $pdf->SetFont('Arial','',8);
        
        if(intval($dts['data']['tb_precalificacion_tipo_operacion'])==1){
            $pdf->Cell(38, 6, utf8_decode('Pre Constitución'), 1, 1);
        }elseif(intval($dts['data']['tb_precalificacion_tipo_operacion'])==2){
            $pdf->Cell(38, 6, utf8_decode('Constitución sin custodia'), 1, 1);
        }elseif(intval($dts['data']['tb_precalificacion_tipo_operacion'])==3){
            $pdf->Cell(38, 6, utf8_decode('Constitución con custodia'), 1, 1);
        }elseif(intval($dts['data']['tb_precalificacion_tipo_operacion'])==4){
            $pdf->Cell(38, 6, utf8_decode('Compra Tercero S/C'), 1, 1);
        }elseif(intval($dts['data']['tb_precalificacion_tipo_operacion'])==5){
            $pdf->Cell(38, 6, utf8_decode('Compra Tercero C/C'), 1, 1);
        }elseif(intval($dts['data']['tb_precalificacion_tipo_operacion'])==6){
            $pdf->Cell(38, 6, utf8_decode('Adenda'), 1, 1);
        }elseif(intval($dts['data']['tb_precalificacion_tipo_operacion'])==7){
            $pdf->Cell(38, 6, utf8_decode('IPDN Venta'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(38, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Comentario
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(75, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('COMENTARIO: '), 0, 1);
    $pdf->SetXY(98, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_comentario']!=null || $dts['data']['tb_precalificacion_comentario']!=''){

        $pdf->SetFont('Arial','',8);
		$y1 = $pdf->GetY(); // inicio de la fila
		$pdf->MultiCell(106,5,utf8_decode($dts['data']['tb_precalificacion_comentario']), 1, 1);
		$y2 = $pdf->GetY(); // final de de la fila en el eje Y

    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(106, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    //$alto=$y2;
    if($y2>$y1){
        $alto=$y2+1;
    }else{
        $alto=$alto+7;
    }

    // Vigencia de STR
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VIGENCIA DE STR: '), 0, 1);
    $pdf->SetXY(34, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_vigencia_str']!=null || $dts['data']['tb_precalificacion_vigencia_str']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(32, 6, utf8_decode($dts['data']['tb_precalificacion_vigencia_str']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(32, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Vigencia de GPS
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(70, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VIGENCIA DE GPS: '), 0, 1);
    $pdf->SetXY(99, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_vigencia_gps']!=null || $dts['data']['tb_precalificacion_vigencia_gps']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(37, 6, utf8_decode($dts['data']['tb_precalificacion_vigencia_gps']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(37, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Moneda inicial
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(140, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('MONEDA INICIAL: '), 0, 1);
    $pdf->SetXY(167, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_inicial_moneda']!=null || $dts['data']['tb_precalificacion_inicial_moneda']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_inicial_moneda']=='dolar'){
            $pdf->Cell(37, 6, utf8_decode('Dólares'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_inicial_moneda']=='sol'){
            $pdf->Cell(37, 6, utf8_decode('Soles'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(37, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Inicial porcentaje
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('INICIAL PORCENTAJE: '), 0, 1);
    $pdf->SetXY(40, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_inicial_porcen']!=null || $dts['data']['tb_precalificacion_inicial_porcen']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(15, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_inicial_porcen']).'%'), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(15, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Inicial monto
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(58, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('INICIAL MONTO: '), 0, 1);
    $pdf->SetXY(82, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_inicial_monto']!=null || $dts['data']['tb_precalificacion_inicial_monto']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(17, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_inicial_monto'])), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(17, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Estado inicial
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(102, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('ESTADO INICIAL: '), 0, 1);
    $pdf->SetXY(129, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_inicial_est']!=null || $dts['data']['tb_precalificacion_inicial_est']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_inicial_est']=='deposito'){
            $pdf->Cell(33, 6, utf8_decode('Depositada'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_inicial_est']=='deposito_parcial'){
            $pdf->Cell(33, 6, utf8_decode('Depositada parcialmente'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_inicial_est']=='sin_deposito'){
            $pdf->Cell(33, 6, utf8_decode('No depositada'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(33, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Fecha inicial
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(165, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('FECHA INICIAL: '), 0, 1);
    $pdf->SetXY(188, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_fec_inicial']!=null || $dts['data']['tb_precalificacion_fec_inicial']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(16, 6, utf8_decode(mostrar_fecha($dts['data']['tb_precalificacion_fec_inicial'])), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(16, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Credito a colocarse
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('CRÉDITO A COLOCARSE EN: '), 0, 1);
    $pdf->SetXY(48, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_moneda_credito']!=null || $dts['data']['tb_precalificacion_moneda_credito']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_moneda_credito']=='dolar'){
            $pdf->Cell(12, 6, utf8_decode('Dólares'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_moneda_credito']=='sol'){
            $pdf->Cell(12, 6, utf8_decode('Soles'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(12, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    if($tipogarveh==1){ 

        // Uso de Vehiculo
        $pdf->SetFont('Arial','B',8);
        $pdf->SetXY(62, $alto); // Posición en fila
        $pdf->Cell(15, 6, utf8_decode('USO DE VEHÍCULO: '), 0, 1);
        $pdf->SetXY(92, $alto); // Posición en fila
        if($dts['data']['tb_precalificacion_uso']!=null || $dts['data']['tb_precalificacion_uso']!=''){

            $pdf->SetFont('Arial','',8);
            $y1 = $pdf->GetY(); // inicio de la fila
            $pdf->MultiCell(112,5,utf8_decode($dts['data']['tb_precalificacion_uso']), 1, 1);
            $y2 = $pdf->GetY(); // final de de la fila en el eje Y

        }else{
            $pdf->SetFont('Arial','I',8);
            $pdf->SetTextColor(205, 205, 205);
            $pdf->Cell(112, 6, 'Sin datos', 1 , 1);
            $pdf->SetTextColor(0, 0, 0);
        }

        //$alto=$y2;
        if($y2>$y1){
            $alto=$y2+1;
        }else{
            $alto=$alto+7;
        }

    }else{

        // Finalidad de crédito
        $pdf->SetFont('Arial','B',8);
        $pdf->SetXY(62, $alto); // Posición en fila
        $pdf->Cell(15, 6, utf8_decode('FINALIDAD DE CRÉDITO: '), 0, 1);
        $pdf->SetXY(100, $alto); // Posición en fila
        if($dts['data']['tb_precalificacion_finalidad']!=null || $dts['data']['tb_precalificacion_finalidad']!=''){

            $pdf->SetFont('Arial','',8);
            $y1 = $pdf->GetY(); // inicio de la fila
            $pdf->MultiCell(104,5,utf8_decode($dts['data']['tb_precalificacion_finalidad']), 1, 1);
            $y2 = $pdf->GetY(); // final de de la fila en el eje Y

            if($y2>$y1){
                $alto=$y2+1;
            }else{
                $alto=$alto+7;
            }

        }else{
            $pdf->SetFont('Arial','I',8);
            $pdf->SetTextColor(205, 205, 205);
            $pdf->Cell(104, 6, 'Sin datos', 1 , 1);
            $pdf->SetTextColor(0, 0, 0);

            $alto=$alto+7;

        }

        // Uso de Vehiculo
        $pdf->SetFont('Arial','B',8);
        $pdf->SetXY(5, $alto); // Posición en fila
        $pdf->Cell(15, 6, utf8_decode('USO DE VEHÍCULO: '), 0, 1);
        $pdf->SetXY(35, $alto); // Posición en fila
        if($dts['data']['tb_precalificacion_uso']!=null || $dts['data']['tb_precalificacion_uso']!=''){

            $pdf->SetFont('Arial','',8);
            $y1 = $pdf->GetY(); // inicio de la fila
            $pdf->MultiCell(169,6,utf8_decode($dts['data']['tb_precalificacion_uso']), 1, 1);
            $y2 = $pdf->GetY(); // final de de la fila en el eje Y

            //$alto=$y2;
            if($y2>$y1){
                $alto=$y2+1;
            }else{
                $alto=$alto+7;
            }

        }else{
            $pdf->SetFont('Arial','I',8);
            $pdf->SetTextColor(205, 205, 205);
            $pdf->Cell(169, 6, 'Sin datos', 1 , 1);
            $pdf->SetTextColor(0, 0, 0);

            $alto=$alto+6;

        }

    }

    /*  */

    /* DATOS CLIENTE */

    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,3,utf8_decode("DATOS DE CLIENTE"), 0, 1, 'L');

    $pdf->Ln(3);

    $alto=$alto+11;

    // Ocupación
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('OCUPACIÓN: '), 0, 1);
    $pdf->SetXY(26, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_ocupacion']!=null || $dts['data']['tb_precalificacion_ocupacion']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(178, 6, utf8_decode($dts['data']['tb_precalificacion_ocupacion']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(178, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Antiguedad trabajo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('ANTIGÜEDAD: '), 0, 1);
    $pdf->SetXY(28, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_cargo_antig']!=null || $dts['data']['tb_precalificacion_cargo_antig']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(23, 6, utf8_decode($dts['data']['tb_precalificacion_cargo_antig']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(23, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Forma de ingresos
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(53, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('FORMA INGRESOS: '), 0, 1);
    $pdf->SetXY(83, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_ingreso_forma']!=null || $dts['data']['tb_precalificacion_ingreso_forma']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_ingreso_forma']=='formal'){
            $pdf->Cell(20, 6, utf8_decode('Formal'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_ingreso_forma']=='informal'){
            $pdf->Cell(20, 6, utf8_decode('Informal'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_ingreso_forma']=='mixto'){
            $pdf->Cell(20, 6, utf8_decode('Mixto'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(20, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Grado instrucción
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(105, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('GRADO DE INSTRUCCIÓN: '), 0, 1);
    $pdf->SetXY(144, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_grado']!=null || $dts['data']['tb_precalificacion_grado']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(60, 6, utf8_decode($dts['data']['tb_precalificacion_grado']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(60, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Carga Familiar
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('CARGA FAMILIAR: '), 0, 1);
    $pdf->SetXY(33, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_carga_familiar']!=null || $dts['data']['tb_precalificacion_carga_familiar']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(60, 6, utf8_decode($dts['data']['tb_precalificacion_carga_familiar']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(60, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Edad de estudio
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(95, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('EN EDAD DE ESTUDIO: '), 0, 1);
    $pdf->SetXY(129, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_edad_escolar']!=null || $dts['data']['tb_precalificacion_edad_escolar']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(18, 6, utf8_decode($dts['data']['tb_precalificacion_edad_escolar']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(18, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Descuento judiciiales
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(150, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('DSCTOS JUDICIALES: '), 0, 1);
    $pdf->SetXY(183, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_edad_escolar']!=null || $dts['data']['tb_precalificacion_edad_escolar']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(21, 6, utf8_decode(formato_moneda($dts['data']['tb_precalificacion_edad_escolar'])), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(21, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Estado civil
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('ESTADO CIVIL: '), 0, 1);
    $pdf->SetXY(28, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_estado_civil']!=null || $dts['data']['tb_precalificacion_estado_civil']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(31, 6, utf8_decode($dts['data']['tb_precalificacion_estado_civil']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(31, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Describir gastos
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(62, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('DESCRIBIR GASTOS: '), 0, 1);
    $pdf->SetXY(94, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_gastos']!=null || $dts['data']['tb_precalificacion_gastos']!=''){

        $pdf->SetFont('Arial','',8);
        $y1 = $pdf->GetY(); // inicio de la fila
        $pdf->MultiCell(110,5,utf8_decode($dts['data']['tb_precalificacion_gastos']), 1, 1);
        $y2 = $pdf->GetY(); // final de de la fila en el eje Y

        if($y2>$y1){
            $alto=$y2+1;
        }else{
            $alto=$alto+7;
        }

    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(110, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);

        $alto=$alto+7;

    }

    /* // Uso de vehículo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('USO DE VEHÍCULO: '), 0, 1);
    $pdf->SetXY(34, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_uso_vehiculo']!=null || $dts['data']['tb_precalificacion_uso_vehiculo']!=''){

        $pdf->SetFont('Arial','',8);
        $y1 = $pdf->GetY(); // inicio de la fila
        $pdf->MultiCell(65,5,utf8_decode($dts['data']['tb_precalificacion_uso_vehiculo']), 1, 1);
        $y2 = $pdf->GetY(); // final de de la fila en el eje Y

        if($y2>$y1){
            $alto=$y2+1;
        }else{
            $alto=$alto+7;
        }

    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(65, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);

        $alto=$alto+7;

    } */

    // Uso de vehículo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('USO DE VEHÍCULO: '), 0, 1);
    $pdf->SetXY(34, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_uso_vehiculo']!=null || $dts['data']['tb_precalificacion_uso_vehiculo']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(80, 6, utf8_decode($dts['data']['tb_precalificacion_uso_vehiculo']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(80, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Semaforizacion
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(115, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('SEMAFORIZACIÓN: '), 0, 1);
    $pdf->SetXY(144, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_semaforización']!=null || $dts['data']['tb_precalificacion_semaforización']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30, 6, utf8_decode($dts['data']['tb_precalificacion_semaforización']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(30, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Score
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(175, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('SCORE: '), 0, 1);
    $pdf->SetXY(189, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_score']!=null || $dts['data']['tb_precalificacion_score']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(15, 6, utf8_decode($dts['data']['tb_precalificacion_score']), 1, 1, 'R');
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(15, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;


    // Zona de uso del vehículo
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('ZONA DE USO DEL VEHÍCULO: '), 0, 1);
    $pdf->SetXY(50, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_zona_vehiculo']!=null || $dts['data']['tb_precalificacion_zona_vehiculo']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(154, 6, utf8_decode($dts['data']['tb_precalificacion_zona_vehiculo']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(154, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Domicilio
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('DOMICILIO: '), 0, 1);
    $pdf->SetXY(23, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_direccion']!=null || $dts['data']['tb_precalificacion_direccion']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(181, 6, utf8_decode($dts['data']['tb_precalificacion_direccion']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(181, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Antiguedad domicilio
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('ANTIGÜEDAD EN VIVIENDA: '), 0, 1);
    $pdf->SetXY(46, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_casa_antig']!=null || $dts['data']['tb_precalificacion_casa_antig']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(40, 6, utf8_decode($dts['data']['tb_precalificacion_casa_antig']), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(40, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Vivienda
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(90, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('VIVIENDA: '), 0, 1);
    $pdf->SetXY(107, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_vivienda']!=null || $dts['data']['tb_precalificacion_vivienda']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_vivienda']=='propia'){
            $pdf->Cell(20, 6, utf8_decode('Propia'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_vivienda']=='alquilada'){
            $pdf->Cell(20, 6, utf8_decode('Alquilada'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_vivienda']=='familiar'){
            $pdf->Cell(20, 6, utf8_decode('Familiar'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(20, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Tipo residencia
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(130, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('TIPO RESIDENCIA: '), 0, 1);
    $pdf->SetXY(159, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_tipo_resid']!=null || $dts['data']['tb_precalificacion_tipo_resid']!=''){
        $pdf->SetFont('Arial','',8);
        if($dts['data']['tb_precalificacion_tipo_resid']==1){
            $pdf->Cell(45, 6, utf8_decode('Casa/Departamento'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_tipo_resid']==2){
            $pdf->Cell(45, 6, utf8_decode('Urbano/Residencia'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_tipo_resid']==3){
            $pdf->Cell(45, 6, utf8_decode('Módulo/Terreno'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_tipo_resid']==4){
            $pdf->Cell(45, 6, utf8_decode('Rural'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_tipo_resid']==5){
            $pdf->Cell(45, 6, utf8_decode('Habitación'), 1, 1);
        }elseif($dts['data']['tb_precalificacion_tipo_resid']==6){
            $pdf->Cell(45, 6, utf8_decode('Otro'), 1, 1);
        }
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(45, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    $alto=$alto+7;

    // Inspección domiiciliaria
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('INSPECCIÓN DOMICILIARIA: '), 0, 1);
    $pdf->SetXY(46, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_fec_inspeccion']!=null || $dts['data']['tb_precalificacion_fec_inspeccion']!=''){
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(21, 6, utf8_decode(mostrar_fecha($dts['data']['tb_precalificacion_fec_inspeccion'])), 1, 1);
    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(21, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);
    }

    // Describir otro
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(70, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('DESCRIBIR OTRO TIP. RESIDENCIA: '), 0, 1);
    $pdf->SetXY(122, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_resid_otro']!=null || $dts['data']['tb_precalificacion_resid_otro']!=''){

        $pdf->SetFont('Arial','',8);
        $y1 = $pdf->GetY(); // inicio de la fila
        $pdf->MultiCell(82,5,utf8_decode($dts['data']['tb_precalificacion_resid_otro']), 1, 1);
        $y2 = $pdf->GetY(); // final de de la fila en el eje Y

        if($y2>$y1){
            $alto=$y2+1;
        }else{
            $alto=$alto+7;
        }

    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(82, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);

        $alto=$alto+7;

    }

    /*  */

    /* OBSERVACIONES */
    $alto=$alto+11;

    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,3,utf8_decode("OBSERVACIONES"), 0, 1, 'L');

    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(6, $alto); // Posición en fila
    if($dts['data']['tb_precalificacion_observacion']!=null || $dts['data']['tb_precalificacion_observacion']!=''){

        $pdf->SetFont('Arial','',8);
        $y1 = $pdf->GetY(); // inicio de la fila
        $pdf->MultiCell(198,5,utf8_decode($dts['data']['tb_precalificacion_observacion']), 1, 1);
        $y2 = $pdf->GetY(); // final de de la fila en el eje Y

        if($y2>$y1){
            $alto=$y2+1;
        }else{
            $alto=$alto+7;
        }

    }else{
        $pdf->SetFont('Arial','I',8);
        $pdf->SetTextColor(205, 205, 205);
        $pdf->Cell(82, 6, 'Sin datos', 1 , 1);
        $pdf->SetTextColor(0, 0, 0);

        $alto=$alto+7;

    }
    /*  */


    $pdf->AddPage('P','A4');
    /* INGRESOS */

    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,3,utf8_decode("TABLA DESCRIPTIVA DE INGRESOS CLIENTE"), 0, 1, 'L');

    $pdf->Ln(3);

    $alto=25;

    $total_ingresos = 0.00;

    if($ingresos['estado']==1){
        foreach ($ingresos['data'] as $key => $value1) {

            // Monto
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(5, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('INGRESO: '), 0, 1);
            $pdf->SetXY(19, $alto); // Posición en fila
            if($value1['tb_ingresoegreso_monto']!=null || $value1['tb_ingresoegreso_monto']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(17, 5, utf8_decode(formato_moneda($value1['tb_ingresoegreso_monto']+($value1['tb_ingresoegreso_montome']*$tipo_cambio))), 1, 1, 'R');
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(17, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Origen
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(37, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('ORIGEN: '), 0, 1);
            $pdf->SetXY(50, $alto); // Posición en fila
            if($value1['tb_ingresoegreso_origen']!=null || $value1['tb_ingresoegreso_origen']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(25, 5, utf8_decode($value1['tb_ingresoegreso_origen']), 1, 1);
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(25, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Antiguedad
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(76, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('ANTIGÜEDAD: '), 0, 1);
            $pdf->SetXY(95, $alto); // Posición en fila
            if($value1['tb_ingresoegreso_antiguedad']!=null || $value1['tb_ingresoegreso_antiguedad']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(20, 5, utf8_decode($value1['tb_ingresoegreso_antiguedad']), 1, 1);
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(20, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Sustento
            $pdf->SetFont('Arial','B',6);
            $pdf->SetXY(116, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('SUSTENTO: '), 0, 1);
            $pdf->SetXY(132, $alto); // Posición en fila
            if($value1['tb_ingresoegreso_origen']!=null || $value1['tb_ingresoegreso_origen']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(20, 5, utf8_decode($value1['tb_ingresoegreso_origen']), 1, 1);
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(20, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Verificacion
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(152, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('COMENTARIO: '), 0, 1);
            $pdf->SetXY(172, $alto); // Posición en fila
            if($value1['tb_ingresoegreso_verificacion']!=null || $value1['tb_ingresoegreso_verificacion']!=''){

                $pdf->SetFont('Arial','',6);
                $y1 = $pdf->GetY(); // inicio de la fila
                $pdf->MultiCell(32,5,utf8_decode($value1['tb_ingresoegreso_verificacion']), 1, 1);
                $y2 = $pdf->GetY(); // final de de la fila en el eje Y

                if($y2>$y1){
                    $alto=$y2+1;
                }else{
                    $alto=$alto+6;
                }

            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(82, 6, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);

                $alto=$alto+6;

            }

            $total_ingresos = $total_ingresos + ($value1['tb_ingresoegreso_monto']+($value1['tb_ingresoegreso_montome']*$tipo_cambio));

            //$alto=$alto+7;

        }
    }

    /*  */

    /* EGRESOS */

    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,3,utf8_decode("TABLA DESCRIPTIVA DE EGRESOS CLIENTE"), 0, 1, 'L');

    $pdf->Ln(3);

    $alto=$alto+11;

    $total_egresos = 0.00;

    if($egresos['estado']==1){
        foreach ($egresos['data'] as $key => $value2) {

            // Monto
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(5, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('EGRESO: '), 0, 1);
            $pdf->SetXY(19, $alto); // Posición en fila
            if($value2['tb_ingresoegreso_monto']!=null || $value2['tb_ingresoegreso_monto']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(17, 5, utf8_decode(formato_moneda($value2['tb_ingresoegreso_monto']+($value2['tb_ingresoegreso_montome']*$tipo_cambio))), 1, 1, 'R');
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(17, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Finalidad
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(37, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('FINALIDAD: '), 0, 1);
            $pdf->SetXY(53, $alto); // Posición en fila
            if($value2['tb_ingresoegreso_finalidad']!=null || $value2['tb_ingresoegreso_finalidad']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(22, 5, utf8_decode($value2['tb_ingresoegreso_finalidad']), 1, 1);
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(22, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Frecuencia
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(76, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('FRECUENCIA: '), 0, 1);
            $pdf->SetXY(95, $alto); // Posición en fila
            if($value2['tb_ingresoegreso_frecuencia']!=null || $value2['tb_ingresoegreso_frecuencia']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(20, 5, utf8_decode($value2['tb_ingresoegreso_frecuencia']), 1, 1);
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(20, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Sustento
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(116, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('SUSTENTO: '), 0, 1);
            $pdf->SetXY(132, $alto); // Posición en fila
            if($value2['tb_ingresoegreso_sustento']!=null || $value2['tb_ingresoegreso_sustento']!=''){
                $pdf->SetFont('Arial','',6);
                $pdf->Cell(20, 5, utf8_decode($value2['tb_ingresoegreso_sustento']), 1, 1);
            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(20, 5, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);
            }

            // Verificacion
            $pdf->SetFont('Arial','B',7);
            $pdf->SetXY(152, $alto); // Posición en fila
            $pdf->Cell(15, 6, utf8_decode('COMENTARIO: '), 0, 1);
            $pdf->SetXY(172, $alto); // Posición en fila
            if($value2['tb_ingresoegreso_verificacion']!=null || $value2['tb_ingresoegreso_verificacion']!=''){

                $pdf->SetFont('Arial','',6);
                $y1 = $pdf->GetY(); // inicio de la fila
                $pdf->MultiCell(32,5,utf8_decode($value2['tb_ingresoegreso_verificacion']), 1, 1);
                $y2 = $pdf->GetY(); // final de de la fila en el eje Y

                if($y2>$y1){
                    $alto=$y2+1;
                }else{
                    $alto=$alto+6;
                }

            }else{
                $pdf->SetFont('Arial','I',7);
                $pdf->SetTextColor(205, 205, 205);
                $pdf->Cell(82, 6, 'Sin datos', 1 , 1);
                $pdf->SetTextColor(0, 0, 0);

                $alto=$alto+6;

            }

            $total_egresos = $total_egresos + ($value2['tb_ingresoegreso_monto']+($value2['tb_ingresoegreso_montome']*$tipo_cambio));

            //$alto=$alto+7;

        }
    }

    /*  */

    $pdf->SetFont('Arial','B',7);

    $pdf->Ln(6);

    $alto=$alto+7;

    $pdf->Cell(195, 6, utf8_decode("TOTAL INGRESOS: S/ ".formato_moneda($total_ingresos)), 0, 1, 'R');

    $alto=$alto+7;

    $pdf->Cell(195, 6, utf8_decode("TOTAL EGRESOS: S/ ".formato_moneda($total_egresos)), 0, 1, 'R');

    $alto=$alto+7;

    $pdf->Cell(195, 6, utf8_decode("TOTAL INGRESOS - EGRESOS: S/ ".formato_moneda($total_ingresos-$total_egresos)), 0, 1, 'R');

    $alto=$alto+7;

    /* FIRMAS */

    $alto=265;

    $pdf->SetFont('Arial','B',10);

    if($accion == 'con_firma'){

        $votos = $oProceso->listar_comite_votos($proceso_id);
        $gerencia_id    = 20; //local=2 (Juan)
        $comercial_id   = 11; //local=11 (Engel)
        $cobranza_id    = 32; //local=56 (Gerson)
        //$otro_id        = 0; //local=42 || local=52 (Gio | Judith)
        
        if($votos['estado'] == 1){

            // GERENCIA -> Manuel Vargas = 20
                foreach ($votos['data'] as $key => $v1) {
                    if($v1['tb_usuario_id'] == $gerencia_id){
                        $pdf->SetXY(3, 255);
                        $pdf->Cell(15, 3,$pdf->Image('../../'.$v1['tb_comite_firma'], null, null, 55, 15, 'PNG'), 0 , 1, 'R');
                    }
                }
            //

            // GERENCIA COMERCIAL -> Engel Galvez = 11
                foreach ($votos['data'] as $key => $v2) {
                    if($v2['tb_usuario_id'] == $comercial_id){
                        $pdf->SetXY(52, 253);
                        $pdf->Cell(15, 3,$pdf->Image('../../'.$v2['tb_comite_firma'], null, null, 55, 15, 'PNG'), 0 , 1, 'R');
                    }
                }
            //
            
            // GESTION DE COBRANZA -> CARLOS = 32
                foreach ($votos['data'] as $key => $v3) {
                    if($v3['tb_usuario_id'] == $cobranza_id){
                        $pdf->SetXY(102, 256);
                        $pdf->Cell(15, 3,$pdf->Image('../../'.$v3['tb_comite_firma'], null, null, 55, 15, 'PNG'), 0 , 1, 'R');
                    }
                }
            //

            // MIEMBRO INVITADO
                if($otro_id > 0){
                    $miembro_comite = $oProceso->firma_miembro_comite($otro_id);
                    if($miembro_comite['estado'] == 1){
                        $pdf->SetXY(150, 253);
                        $pdf->Cell(15, 3,$pdf->Image('../../'.$miembro_comite['data']['tb_comite_firma'], null, null, 55, 15, 'PNG'), 0 , 1, 'R');
                    }
                }
            //

        }
    }

    $pdf->SetXY(23, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('______________'), 0, 1, 'C');

    $pdf->SetXY(70, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('____________________'), 0, 1, 'C');

    $pdf->SetXY(122, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('____________________'), 0, 1, 'C');

    $pdf->SetXY(170, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('_________________'), 0, 1, 'C');

    $alto=270;

    $pdf->SetFont('Arial','B',10);

    $pdf->SetXY(23, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('GERENCIA'), 0, 1, 'C');

    $pdf->SetXY(70, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('JEFATURA COMERCIAL'), 0, 1, 'C');

    $pdf->SetXY(122, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('GESTIÓN DE COBRANZA'), 0, 1, 'C');

    $pdf->SetXY(170, $alto); // Posición en fila
    $pdf->Cell(15, 6, utf8_decode('OTRO - INVITADO'), 0, 1, 'C');
    /*  */


}else{
    $pdf->SetFillColor(220,220,220);
    $alto=5;

    $pdf->Ln(1);
    $pdf->SetFont('Arial','I',8);
    $pdf->SetXY(5, $alto);
    $pdf->Cell(15, 6, utf8_decode("Asesor: ".$colaborador), 0 , 1, 'L');
    $pdf->SetFont('Arial','B',8);
    $pdf->SetXY(190, $alto);
    $pdf->Cell(15, 6, utf8_decode($fecha_hoy), 0 , 1, 'R');

    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',14);
    $pdf->Cell(190,5,utf8_decode("FORMATO DE DE EVALUACIÓN CREDITICIA - COMITÉ DE CRÉDITO"),0,1,'C');

    $pdf->Ln(2);
    $pdf->SetFont('Arial','I',14);
    $pdf->Cell(190,5,utf8_decode("No se tienen datos de Precalificación"),0,1,'L');
}
	
$pdf->SetAutoPageBreak('auto',2); 
$pdf->SetDisplayMode(75);
$pdf->Output();
?>