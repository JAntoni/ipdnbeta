<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../vehiculo/Vehiculo.class.php');
  $oVehiculo = new Vehiculo();
  require_once('../funciones/funciones.php');

  $direc = 'vehiculo';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $vehiculo_id = $_POST['vehiculo_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Vehículo Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Vehículo';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Vehículo';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Vehículo';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en vehiculo
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'vehiculo'; $modulo_id = $vehiculo_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del vehiculo por su ID
    if(intval($vehiculo_id) > 0){
      $result = $oVehiculo->mostrarUno($vehiculo_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el vehiculo seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $vehiculo_tip = $result['data']['tb_vehiculo_tip'];
          $vehiculomarca_id = intval($result['data']['tb_vehiculomarca_id']);
          $vehiculomodelo_id = intval($result['data']['tb_vehiculomodelo_id']);
          $vehiculoclase_id = intval($result['data']['tb_vehiculoclase_id']);
          $vehiculotipo_id = intval($result['data']['tb_vehiculotipo_id']);
          $vehiculo_tan = intval($result['data']['tb_vehiculo_tan']);
          $galon_id = intval($result['data']['tb_galon_id']);

          $vehiculo_pla = strtoupper($result['data']['tb_vehiculo_pla']);
          $vehiculo_sermot = $result['data']['tb_vehiculo_sermot'];
          $vehiculo_sercha = $result['data']['tb_vehiculo_sercha'];
          $vehiculo_anio = $result['data']['tb_vehiculo_anio'];
          $vehiculo_col = $result['data']['tb_vehiculo_col'];
          $vehiculo_numpas = $result['data']['tb_vehiculo_numpas'];
          $vehiculo_numasi = $result['data']['tb_vehiculo_numasi'];
          $vehiculo_des = $result['data']['tb_vehiculo_des'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vehiculo" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_vehiculo" method="post">
          <input type="hidden" id="vehiculo_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_vehiculo_id" value="<?php echo $vehiculo_id;?>">
          
          <div class="modal-body">
            <div class="row">
              <!-- COLUMNA IZQUIERDA-->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_vehiculomarca_id" class="control-label">Marca</label>
                  <select id="cmb_vehiculomarca_id" name="cmb_vehiculomarca_id" class="form-control input-sm">
                    <?php require_once('../vehiculomarca/vehiculomarca_select.php');?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="cmb_vehiculoclase_id" class="control-label">Clase</label>
                  <select id="cmb_vehiculoclase_id" name="cmb_vehiculoclase_id" class="form-control input-sm">
                    <?php require_once('../vehiculoclase/vehiculoclase_select.php');?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="cmb_vehiculo_tan" class="control-label">Tanque</label>
                  <select id="cmb_vehiculo_tan" name="cmb_vehiculo_tan" class="form-control input-sm">
                    <option value="0"></option>
                    <option value="1" <?php if($vehiculo_tan ==1) echo "selected";?> >Lenteja</option>
                    <option value="2" <?php if($vehiculo_tan ==2) echo "selected";?> >Cilídrico</option>
                    <option value="3" <?php if($vehiculo_tan ==3) echo "selected";?> >Toroidal</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_pla" class="control-label">Placa</label>
                  <input type="text" name="txt_vehiculo_pla" id="txt_vehiculo_pla" class="form-control input-sm mayus" value="<?php echo $vehiculo_pla;?>">
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_sercha" class="control-label">Serie Chasis</label>
                  <input type="text" name="txt_vehiculo_sercha" id="txt_vehiculo_sercha" class="form-control input-sm mayus" value="<?php echo $vehiculo_sercha;?>">
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_col" class="control-label">Color</label>
                  <input type="text" name="txt_vehiculo_col" id="txt_vehiculo_col" class="form-control input-sm mayus" value="<?php echo $vehiculo_col;?>">
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_numasi" class="control-label">Numero de Asientos</label>
                  <input type="text" name="txt_vehiculo_numasi" id="txt_vehiculo_numasi" class="form-control input-sm mayus" value="<?php echo $vehiculo_numasi;?>">
                </div>
              </div>
              <!-- COLUMNA DERECHA-->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cmb_vehiculomodelo_id" class="control-label">Modelo</label>
                  <select id="cmb_vehiculomodelo_id" name="cmb_vehiculomodelo_id" class="form-control input-sm">
                    <?php require_once('../vehiculomodelo/vehiculomodelo_select.php');?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="cmb_vehiculotipo_id" class="control-label">Tipo</label>
                  <select id="cmb_vehiculotipo_id" name="cmb_vehiculotipo_id" class="form-control input-sm">
                    <?php require_once('../vehiculotipo/vehiculotipo_select.php');?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="cmb_galon_id" class="control-label">Capacidad de Tanque</label>
                  <select id="cmb_galon_id" name="cmb_galon_id" class="form-control input-sm">
                    <?php require_once('../galon/galon_select.php');?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_sermot" class="control-label">Serie Motor</label>
                  <input type="text" name="txt_vehiculo_sermot" id="txt_vehiculo_sermot" class="form-control input-sm mayus" value="<?php echo $vehiculo_sermot;?>">
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_anio" class="control-label">Año</label>
                  <input type="text" name="txt_vehiculo_anio" id="txt_vehiculo_anio" class="form-control input-sm mayus" value="<?php echo $vehiculo_anio;?>">
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_numpas" class="control-label">Número de Pasajeros</label>
                  <input type="text" name="txt_vehiculo_numpas" id="txt_vehiculo_numpas" class="form-control input-sm mayus" value="<?php echo $vehiculo_numpas;?>">
                </div>
                <div class="form-group">
                  <label for="txt_vehiculo_des" class="control-label">Descripción Breve</label>
                  <input type="text" name="txt_vehiculo_des" id="txt_vehiculo_des" class="form-control input-sm" value="<?php echo $vehiculo_des;?>">
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Vehículo?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="vehiculo_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_vehiculo">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_vehiculo">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_vehiculo">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/vehiculo/vehiculo_form.js';?>"></script>
