<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculo/Vehiculo.class.php');
  $oVehiculo = new Vehiculo();

 	$action = $_POST['action'];

 	$oVehiculo->vehiculomarca_id = intval($_POST['cmb_vehiculomarca_id']);
  $oVehiculo->vehiculomodelo_id = intval($_POST['cmb_vehiculomodelo_id']);
  $oVehiculo->vehiculoclase_id = intval($_POST['cmb_vehiculoclase_id']);
  $oVehiculo->vehiculotipo_id = intval($_POST['cmb_vehiculotipo_id']);
  $oVehiculo->vehiculo_tan = intval($_POST['cmb_vehiculo_tan']);
  $oVehiculo->galon_id = intval($_POST['cmb_galon_id']);

  $oVehiculo->vehiculo_pla = strtoupper($_POST['txt_vehiculo_pla']);
  $oVehiculo->vehiculo_sermot = $_POST['txt_vehiculo_sermot'];
  $oVehiculo->vehiculo_sercha = $_POST['txt_vehiculo_sercha'];
  $oVehiculo->vehiculo_anio = $_POST['txt_vehiculo_anio'];
  $oVehiculo->vehiculo_col = strtoupper($_POST['txt_vehiculo_col']);
  $oVehiculo->vehiculo_numpas = $_POST['txt_vehiculo_numpas'];
  $oVehiculo->vehiculo_numasi = $_POST['txt_vehiculo_numasi'];
  $oVehiculo->vehiculo_des = $_POST['txt_vehiculo_des'];

  $oVehiculo->usuario_id = $_SESSION['usuario_id'];

 	if($action == 'insertar'){
 
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Vehículo.';
 		if($oVehiculo->insertar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Vehículo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
    $vehiculo_id = intval($_POST['hdd_vehiculo_id']);
    $oVehiculo->vehiculo_id = $vehiculo_id;

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Vehículo.';

 		if($oVehiculo->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Vehiculo modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculo_id = intval($_POST['hdd_vehiculo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al vehiculo.';

 		if($oVehiculo->eliminar($vehiculo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'vehiculo eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>