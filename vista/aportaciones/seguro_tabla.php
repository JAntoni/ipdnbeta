<?php

require_once ("../../core/usuario_sesion.php");
require_once ("Seguro.class.php");

$oSeguro = new Seguro();

$dts=$oSeguro->mostrarTodos('');




?>

<table cellspacing="1" id="tabla_seguro" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila">TIPO</th>
      <th id="tabla_cabecera_fila">SEGURO</th>
      <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
      <th id="tabla_cabecera_fila">&nbsp;</th>
    </tr>
  </thead>
    <?php
      if($dts['estado'] == 1){
		?>  
    <tbody>
			<?php
 foreach ($dts['data']as $key=>$dt){?>
          <tr id="tabla_cabecera_fila">
            <td id="tabla_fila"><?php echo $dt['tb_seguro_id']?></td>
            <td id="tabla_fila">
              <?php
                $tipo = 'SIN TIPO';
                if($dt['tb_seguro_tip'] == 1)
                  $tipo = 'Aportación Obligatoria';
                if($dt['tb_seguro_tip'] == 2)
                  $tipo = 'Seguro EPS';
                if($dt['tb_seguro_tip'] == 3)
                  $tipo = 'UIT';
                echo $tipo;
              ?>  
            </td>
            <td id="tabla_fila"><?php echo $dt['tb_seguro_nom']?></td>
            <td id="tabla_fila"><?php echo $dt['tb_seguro_des']?></td>
            <td id="tabla_fila"align="center">
              <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="seguro_form('editar','<?php echo $dt['tb_seguro_id']?>')"><i class="fa fa-edit"></i></a> 
              <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="eliminar_seguro('<?php echo $dt['tb_seguro_id']?>')"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
            <?php
                    }
            ?>
    </tbody>
     	<?php
        }
		?>
</table>