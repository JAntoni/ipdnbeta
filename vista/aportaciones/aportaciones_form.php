<?php
require_once ("../../core/usuario_sesion.php");
require_once ("../aportaciones/Afps.class.php");
$oAfps = new Afps();

$action = $_POST['action'];
$afp_id = $_POST['afp_id'];

if($action == 'editar' && !empty($afp_id)){

  $dts = $oAfps->mostrarUno($afp_id);
    if($dts['estado']==1){
        $afp_per = $dts['data']['tb_afps_per']; //periodo de tiempo de los porcentajes
        $seguro_id = $dts['data']['tb_seguro_id'];
        //$afp_tip = $dts['data']['tb_afps_tip']; //tipo de entidad 1 afp, 2 EPS
        //$afp_nom = $dts['data']['tb_afps_nom']; //nombre de la entidad
        $afp_apor = $dts['data']['tb_afps_apor']; //aporte, puede ser en porcentaje si es AFP y monto fijo si es EPS
        $afp_apor2 = $dts['data']['tb_afps_apor2']; //aporte, puede ser en porcentaje si es AFP y monto fijo si es EPS
        $afp_apor3 = $dts['data']['tb_afps_apor3']; //aporte, puede ser en porcentaje si es AFP y monto fijo si es EPS
        $afp_apor4 = $dts['data']['tb_afps_apor4']; //aporte, puede ser en porcentaje si es AFP y monto fijo si es EPS
        $afp_his = $dts['data']['tb_afps_his']; //historial sobre este registro
    }
  list($mes, $anio) = split('-', $afp_per);

  $hoy_mes = $mes;
  $hoy_anio = $anio;
}
else{
  $hoy_mes = date('m');
  $hoy_anio = date('Y');
}
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_aportaciones" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Detalle de Aportaciones</h4>
            </div>
            <form id="form_aportaciones">
                <input type="hidden" name="action" value="<?php echo $action;?>">
                <input type="hidden" name="hdd_afp_id" value="<?php echo $afp_id;?>">

                <div class="modal-body">
                    <h5 style="font-family: cambria;font-weight: bold;color: #000000">SEGUROS Y UIT</h5>
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Mes :</label>
                                    <select name="cmb_apor_mes" id="cmb_apor_mes" class="form-control input-sm">
                                        <?php 
                                          $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                                          $num = array('01','02','03','04','05','06','07','08','09','10','11','12');
                                          $sel = '';

                                          for ($i=0; $i < 12 ; $i++) { 
                                            if($num[$i] == $hoy_mes)
                                              echo '<option value="'.$num[$i].'" selected>'.$mes[$i].'</option>';
                                            else
                                              echo '<option value="'.$num[$i].'">'.$mes[$i].'</option>';
                                          }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Año :</label>
                                    <select name="cmb_apor_anio" id="cmb_apor_anio" class="form-control input-sm">
                                        <?php
                                          $year = 2016;
                                          for ($i = 0; $i < 10 ; $i++) { 
                                            $year ++;
                                            if($year == intval($hoy_anio))
                                              echo '<option value="'.$year.'" selected>'.$year.'</option>';
                                            else
                                              echo '<option value="'.$year.'">'.$year.'</option>';
                                          }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row"> 
                                <div class="col-md-12">
                                    <label>Seguro</label>
                                    <select name="cmb_seguro_id" id="cmb_seguro_id" class="form-control input-sm">
                                        <?php require_once('cmb_seguro_id.php');?>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row"> 
                                <div class="col-md-6">
                                    <label>Aporte %:</label>
                                    <input type="text" name="txt_apor_val" id="txt_apor_val" class="moneda form-control input-sm" value="<?php echo $afp_apor;?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Prima %:</label>
                                    <input type="text" name="txt_apor_val2" id="txt_apor_val2" class="form-control input-sm moneda" value="<?php echo $afp_apor2;?>" required>
                                </div>
                            </div>
                            <p>
                            <div class="row"> 
                                <div class="col-md-6">
                                    <label>Comisión Flujo %:</label>
                                    <input type="text" name="txt_apor_val3" id="txt_apor_val3" class="form-control input-sm moneda" value="<?php echo $afp_apor3;?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Comisión Anual %:</label>
                                    <input type="text" name="txt_apor_val4" id="txt_apor_val4" class="form-control input-sm moneda" value="<?php echo $afp_apor4;?>" required>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>*Los porcentajes de los aportes son variables por mes, la UIT es por año.</label>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($_POST['action'] == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="seguro_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_seguro">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/aportaciones/aportaciones_form.js';?>"></script>

