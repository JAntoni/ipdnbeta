<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit,'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-default">
            <div class="box-header">
                <!--<button class="btn btn-primary btn-sm" onclick="aportaciones_form('I', 0)"><i class="fa fa-plus"></i> Nueva Area</button>-->
            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="aportaciones_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>
                                    
                <div class="row">
                    <div class="col-md-6">
                        <h5 style="font-family: cambria;font-weight: bold;color: #000000">EMPRESAS y UIT</h5>
                    </div>
                    <div class="col-md-6">
                        <h5 style="font-family: cambria;font-weight: bold;color: #000000">DETALLE DE APORTACIONES</h5>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <button class="btn btn-primary btn-sm" onclick="seguro_form('insertar', 0)"><i class="fa fa-plus"></i> Agregar Seguros</button>
                            </div>
                            <div class="box-body">
                                <div id="div_seguro_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                    <?php // require_once('aportaciones_tabla.php'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <button class="btn btn-primary btn-sm" onclick="aportaciones_form('insertar', 0)"><i class="fa fa-plus"></i> Agregar Detalle</button>
                            </div>
                            <div class="box-body">
                                
                                <div class="row">
                                    <div class="col-md-1">
                                        <p>
                                        <label>Mes: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <!--<label>Mes: </label>-->
                                        <select name="cmb_apor_fil_mes" id="cmb_apor_fil_mes" class="form-control input-sm">
                                            <?php 
                                              $hoy_mes = date('m');
                                              $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                                              $num = array('01','02','03','04','05','06','07','08','09','10','11','12');
                                              $sel = '';

                                              for ($i=0; $i < 12 ; $i++) { 
                                                if($num[$i] == $hoy_mes)
                                                  echo '<option value="'.$num[$i].'" selected>'.$mes[$i].'</option>';
                                                else
                                                  echo '<option value="'.$num[$i].'">'.$mes[$i].'</option>';
                                              }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <p>
                                        <label>Año: </label>
                                    </div>
                                    <div class="col-md-2">
                                        <select name="cmb_apor_fil_anio" id="cmb_apor_fil_anio" class="form-control input-sm">
                                            <?php 
                                              $anio = intval(date('Y'));
                                              for ($i = 2016; $i < ($anio + 5); $i++) { 
                                                if($anio == $i)
                                                  echo '<option value="'.$i.'" selected="true">'.$i.'</option>';
                                                else
                                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                              }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <p>
                                <div id="div_aportaciones_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                    <?php // require_once('aportaciones_tabla.php'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-6">
                        <h5 style="font-family: cambria;font-weight: bold;color: #000000">ASIGNACION APORTACION SUELDO BRUTO Y SUELDO BASE</h5>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-md-1">
                                        <p>
                                        <label>Mes: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="cmb_sueldo_mes" id="cmb_sueldo_mes" class="form-control input-sm">
                                            <?php 
                                              $hoy_mes = date('m');
                                              $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                                              $num = array('01','02','03','04','05','06','07','08','09','10','11','12');
                                              $sel = '';

                                              for ($i=0; $i < 12 ; $i++) { 
                                                if($num[$i] == $hoy_mes)
                                                  echo '<option value="'.$num[$i].'" selected>'.$mes[$i].'</option>';
                                                else
                                                  echo '<option value="'.$num[$i].'">'.$mes[$i].'</option>';
                                              }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <p>
                                        <label>Año: </label>
                                    </div>
                                    <div class="col-md-2">
                                        <select name="cmb_sueldo_anio" id="cmb_sueldo_anio" class="form-control input-sm">
                                            <?php 
                                              $anio = intval(date('Y'));
                                              for ($i = 2016; $i < ($anio + 5); $i++) { 
                                                if($anio == $i)
                                                  echo '<option value="'.$i.'" selected="true">'.$i.'</option>';
                                                else
                                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                              }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                
                                <div id="div_sueldo_usuario_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap" style="width: 100%">
                                    <?php // require_once('aportaciones_tabla.php'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <div id="div_modal_seguros_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_aportaciones_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
                <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
