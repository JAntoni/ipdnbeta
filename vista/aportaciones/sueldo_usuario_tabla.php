<?php
require_once ("../../core/usuario_sesion.php");
require_once ("../usuario/Usuario.class.php");
$oUsuario = new Usuario();

$mes_anio = $_POST['mes_anio'];
$dts = $oUsuario->filtrar_todos(); //todos los usuarios
$num_rows = 1;
?>

<?php if ($dts['estado'] == 1) { ?>
    <table cellspacing="1" id="tabla_sueldo" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">ID</th>
                <th id="tabla_cabecera_fila">COLABORADOR</th>
                <th id="tabla_cabecera_fila">FORMA APORTE</th>
            </tr>
        </thead>
        <tbody>
            <?php
            
            $cc = 0;
            foreach ($dts['data']as$key=>$dt) {
                $usuario_id = $dt['tb_usuario_id'];

                $dts2 = $oUsuario->sueldoforma_usuario_fecha($usuario_id, $mes_anio);
                $sueldo_tip = 1; //formula para AFP de solo sueldo base, 2 de todo el sueldo mas comisiones
                if ($dts2['estado'] == 1) {
                    $sueldo_tip = $dts2['data']['tb_sueldoforma_tip'];
                }
                ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $dt['tb_usuario_id']?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_usuario_nom']?></td>
                    <td id="tabla_fila">
                        <?php
                        if ($sueldo_tip == 1)
                            echo '<label>Sueldo Base <input type="radio" name="ra_forma' . $cc . '" value="1" checked="true" onclick="guardar_sueldo_forma(' . $usuario_id . ',1)"></label>';
                        else
                            echo '<label>Sueldo Base <input type="radio" name="ra_forma' . $cc . '" value="1" onclick="guardar_sueldo_forma(' . $usuario_id . ',1)"></label>';
                        if ($sueldo_tip == 2)
                            echo '<label>Sueldo Total <input type="radio" name="ra_forma' . $cc . '" value="2" checked="true" onclick="guardar_sueldo_forma(' . $usuario_id . ',2)"></label>';
                        else
                            echo '<label>Sueldo Total <input type="radio" name="ra_forma' . $cc . '" value="2" onclick="guardar_sueldo_forma(' . $usuario_id . ',2)"></label>';
                        ?>
                    </td>
                </tr> <?php
                $cc++;
            }
                    ?>
        </tbody>
    </table>
    <?php
} else {
    echo '<center><h2>NO HAY REGISTRO DE EMPRESAS PARA APORTAR</h2></center>';
}
?>
