<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Afps extends Conexion{
    public $tb_afps_id;
    public $tb_afps_reg;
    public $tb_afps_per;
    public $tb_seguro_id;
    public $tb_afps_tip;
    public $tb_afps_nom;
    public $tb_afps_apor;
    public $tb_afps_apor2;
    public $tb_afps_apor3;
    public $tb_afps_apor4;
    public $tb_afps_his;
    
  
  function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_afps(
                                        tb_afps_reg, 
                                        tb_afps_per, 
                                        tb_seguro_id, 
                                        tb_afps_apor, 
                                        tb_afps_apor2, 
                                        tb_afps_apor3, 
                                        tb_afps_apor4, 
                                        tb_afps_his) 
                                VALUES (
                                        NOW(),
                                        :tb_afps_per, 
                                        :tb_seguro_id, 
                                        :tb_afps_apor, 
                                        :tb_afps_apor2, 
                                        :tb_afps_apor3, 
                                        :tb_afps_apor4, 
                                        :tb_afps_his)";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_afps_per", $this->tb_afps_per, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_id", $this->tb_seguro_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_afps_apor", $this->tb_afps_apor, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_apor2", $this->tb_afps_apor2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_apor3", $this->tb_afps_apor3, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_apor4", $this->tb_afps_apor4, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_his", $this->tb_afps_his, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $tb_afps_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_afps_id'] = $tb_afps_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function editar(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE  tb_afps SET 
                                        tb_afps_per =:tb_afps_per,
                                        tb_seguro_id =:tb_seguro_id, 
                                        tb_afps_apor =:tb_afps_apor,
                                        tb_afps_apor2 =:tb_afps_apor2, 
                                        tb_afps_apor3 =:tb_afps_apor3,
                                        tb_afps_apor4 =:tb_afps_apor4,
                                        tb_afps_his = CONCAT(tb_afps_his,:tb_afps_his) 
                                WHERE 
                                        tb_afps_id =:tb_afps_id";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_afps_per", $this->tb_afps_per, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_id", $this->tb_seguro_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_afps_apor", $this->tb_afps_apor, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_apor2", $this->tb_afps_apor2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_apor3", $this->tb_afps_apor3, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_apor4", $this->tb_afps_apor4, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_his", $this->tb_afps_his, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_afps_id", $this->tb_afps_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($afp_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_afps where tb_afps_id =:tb_afps_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_afps_id", $afp_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($afp_id){
      try {
         $sql = "SELECT * FROM tb_afps where tb_afps_id =$afp_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_afps_id", $afp_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function listar_afps($tipo){
      try {
         $sql = "SELECT * FROM tb_afps";
                    if(!empty($tipo))
                      $sql .= " where tb_afps_tip =:tb_afps_tip";
    
        $sentencia = $this->dblink->prepare($sql);
        if(!empty($tipo))
        $sentencia->bindParam(":tb_afps_tip", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function listar_afps_periodo($mes_anio){
      try {
         $sql = "SELECT * FROM tb_afps afp INNER JOIN tb_seguro seg on seg.tb_seguro_id = afp.tb_seguro_id where tb_afps_per =:tb_afps_per";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_afps_per", $mes_anio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  
    
    function aportacion_usuario_tipo_seguro($seguro_id, $mes_anio){
      try {
         $sql = "SELECT * FROM tb_afps afp INNER JOIN tb_seguro seg on seg.tb_seguro_id = afp.tb_seguro_id WHERE afp.tb_seguro_id =:tb_seguro_id and tb_afps_per =:tb_afps_per";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_id", $seguro_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_afps_per", $mes_anio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  
  
}
