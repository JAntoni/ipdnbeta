<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Seguro  extends Conexion{
    public $tb_seguro_id;
    public $tb_seguro_reg;
    public $tb_seguro_tip;
    public $tb_seguro_nom;
    public $tb_seguro_des;
    
    function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT tb_seguro(
                                    tb_seguro_tip, 
                                    tb_seguro_nom,
                                    tb_seguro_des)
                            VALUES (
                                    :tb_seguro_tip,
                                    :tb_seguro_nom,
                                    :tb_seguro_des)"; 
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_tip", $this->tb_seguro_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_nom", $this->tb_seguro_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_des", $this->tb_seguro_des, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $tb_seguro_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_seguro_id'] = $tb_seguro_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function modificar(){ 
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_seguro SET  
                                    tb_seguro_tip = :tb_seguro_tip,
                                    tb_seguro_nom =  :tb_seguro_nom,
                                    tb_seguro_des =  :tb_seguro_des
                                WHERE  
                                    tb_seguro_id =:tb_seguro_id";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_tip", $this->tb_seguro_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_nom", $this->tb_seguro_nom, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_seguro_des", $this->tb_seguro_des, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_seguro_id", $this->tb_seguro_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($id){
      try {
         $sql="SELECT * 
                FROM 
                    tb_seguro
                WHERE 
                    tb_seguro_id=:tb_seguro_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Seguros registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos($tipo){
      try {
            $sql="SELECT * FROM tb_seguro";
            if(!empty($tipo))
            $sql .= " where tb_seguro_tip =:tb_seguro_tip";
    
        $sentencia = $this->dblink->prepare($sql);
        if(!empty($tipo))
        $sentencia->bindParam(":tb_seguro_tip", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function verifica_seguro_tabla($id,$tabla){
      try {
            $sql="SELECT * FROM tb_seguro";
            if(!empty($tipo))
            $sql .= " where tb_seguro_tip =:tb_seguro_tip";
    
        $sentencia = $this->dblink->prepare($sql);
        if(!empty($tipo))
        $sentencia->bindParam(":tb_seguro_tip", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($seguro_id, $seguro_columna, $seguro_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($seguro_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_seguro SET ".$seguro_columna." =:seguro_valor WHERE tb_seguro_id =:seguro_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":seguro_id", $seguro_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":seguro_valor", $seguro_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":seguro_valor", $seguro_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el seguro retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
     function eliminar($id){
      $this->dblink->beginTransaction();
      try {
         $sql="DELETE FROM tb_seguro WHERE tb_seguro_id=:tb_seguro_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_seguro_id", $id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
}
