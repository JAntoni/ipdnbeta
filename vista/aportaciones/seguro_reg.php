<?php
require_once ("../../core/usuario_sesion.php");
require_once("Seguro.class.php");
$oSeguro = new Seguro();

if($_POST['action_seguro']=="insertar")
{
	if(!empty($_POST['txt_seguro_nom']))
	{

            $oSeguro->tb_seguro_tip=intval($_POST['cmb_seguro_tip']);
            $oSeguro->tb_seguro_nom= $_POST['txt_seguro_nom'];
            $oSeguro->tb_seguro_des= $_POST['txt_seguro_des'];
            $result=$oSeguro->insertar();

		if(intval($result['estado']) == 1){
                    $seguro_id = $result['tb_seguro_id'];
                }

		$data['seguro_id']=$seguro_id;
		$data['seguro_msj']='Se registró el seguro correctamente.';

	}
	else
	{
		$data['seguro_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}



if($_POST['action_seguro']=="editar")
{
	if(!empty($_POST['txt_seguro_nom']))
	{
                $oSeguro->tb_seguro_tip=intval($_POST['cmb_seguro_tip']);
                $oSeguro->tb_seguro_nom= $_POST['txt_seguro_nom'];
                $oSeguro->tb_seguro_des= $_POST['txt_seguro_des'];
                $oSeguro->tb_seguro_id= $_POST['hdd_seguro_id'];
		$oSeguro->modificar();
		$data['seguro_id'] = $_POST['hdd_seguro_id'];
		$data['seguro_msj'] = 'Se modificó el seguro correctamente.';
	}
	else
	{
		$data['seguro_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
}

if($_POST['action']=="eliminar")
{
	if(!empty($_POST['seguro_id']))
	{

		$oSeguro->eliminar($_POST['seguro_id']);
		echo 'Se envió a la papelera correctamente.';
	}
	else
	{
	echo 'Intentelo nuevamente.';

	}
}

?>