/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {


    $('#txt_seguro_nom').change(function () {
        $(this).val($(this).val().toUpperCase());
    });



    $("#form_seguro").validate({

        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "aportaciones/seguro_reg.php",
                async: true,
                dataType: "json",
                data: $("#form_seguro").serialize(),
                beforeSend: function () {
//                    $('#msj_seguro').html("Guardando...");
//                    $('#msj_seguro').show(100);
                },

                success: function (data) {
                    if (parseInt(data.seguro_id) > 0) {
                        seguro_tabla();
                        swal_success("SISTEMA",data.seguro_msj,2000);
                        $('#modal_registro_seguro').modal('hide');
//                        $('#msj_aportes').show(300);
//                        $("#div_seguro_form").dialog('close');
                    } else
                        alert('ERROR: ' + data.seguro_msj)
                },

                complete: function (data) {
                    console.log(data.responseText);
                }
            });

        },
        rules: {
            txt_seguro_nom: {
                required: true
            },
            cmb_seguro_tip: {
                required: true
            }
        },
        messages: {
            txt_seguro_nom: {
                required: 'Debe escribir un Nombre'
            },
            cmb_seguro_tip: {
                required: 'Seleccione el tipo'
            }
        }
    });

});