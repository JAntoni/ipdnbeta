<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Seguro.class.php");
$oSeguro = new Seguro();
?>
<option value="">-</option>

<?php
$seguro_id = (empty($seguro_id)) ? $_POST['are_id'] : intval($seguro_id);
$tipo = (empty($tipo)) ? '' : $tipo;

$dts1 = $oSeguro->mostrarTodos($tipo); //parametro vacio para listar todos los tipos

if ($dts1['estado'] == 1) {
    foreach ($dts1['data']as $key => $dt1) {
        ?>
        <option value="<?php echo $dt1['tb_seguro_id'] ?>" <?php if ($dt1['tb_seguro_id'] == $seguro_id) echo 'selected' ?> data-tipo="<?php echo $dt1['tb_seguro_tip'] ?>"><?php echo $dt1['tb_seguro_nom'] ?></option>
        <?php
    }
}

?>