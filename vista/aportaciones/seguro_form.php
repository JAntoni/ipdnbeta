<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Seguro.class.php");
$oSeguro = new Seguro();

if ($_POST['action'] == "editar") {
    $dts = $oSeguro->mostrarUno($_POST['seguro_id']);
    if ($dts['estado'] == 1) {
        $tip = $dts['data']['tb_seguro_tip'];
        $nom = $dts['data']['tb_seguro_nom'];
        $des = $dts['data']['tb_seguro_des'];
    }
}
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_seguro" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">EGUROS Y UIT</h4>
            </div>
            <form id="form_seguro">
                <input name="action_seguro" id="action_seguro" type="hidden" value="<?php echo $_POST['action'] ?>">
                <input name="hdd_seguro_id" id="hdd_seguro_id" type="hidden" value="<?php echo $_POST['seguro_id'] ?>">

                <div class="modal-body">
                    <!--<h5 style="font-family: cambria;font-weight: bold;color: #000000">SEGUROS Y UIT</h5>-->
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Tipo Aportación</label>
                                    <select name="cmb_seguro_tip" id="cmb_seguro_tip" class="form-control input-sm mayus">
                                        <option value="">--</option>
                                        <option value="1" <?php if ($tip == 1) echo 'selected'; ?> >Aportación Obligatoria</option>
                                        <option value="2" <?php if ($tip == 2) echo 'selected'; ?> >EPS</option>
                                        <option value="3" <?php if ($tip == 3) echo 'selected'; ?> >UIT</option>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row"> 
                                <div class="col-md-12">
                                    <label>Seguro</label>
                                    <input name="txt_seguro_nom" type="text" id="txt_seguro_nom" value="<?php echo $nom ?>" class="form-control input-sm mayus">
                                </div>
                            </div>
                            <p>
                            <div class="row"> 
                                <div class="col-md-12">
                                    <label>Descripción</label>
                                    <input name="txt_seguro_des" type="text" id="txt_seguro_des" value="<?php echo $des ?>" class="form-control input-sm mayus">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($_POST['action'] == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="seguro_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_seguro">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/aportaciones/seguro_form.js';?>"></script>
