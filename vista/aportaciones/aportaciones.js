/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    aportaciones_tabla();
    seguro_tabla();
    sueldo_usuario_tabla();

});


function aportaciones_form(act, id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "aportaciones/aportaciones_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            afp_id: id
        }),
        beforeSend: function () {
            $('#div_aportaciones_form').dialog('open');
        },
        success: function (html) {
            $('#div_modal_aportaciones_form').html(html);
            $('#modal_registro_aportaciones').modal('show');
            modal_hidden_bs_modal('modal_registro_aportaciones', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);
        }
    });
}


function seguro_form(act, id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "aportaciones/seguro_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            seguro_id: id
        }),
        beforeSend: function () {
//      $('#div_seguro_form').dialog('open');
        },
        success: function (html) {
            $('#div_modal_seguros_form').html(html);
            $('#modal_registro_seguro').modal('show');
            modal_hidden_bs_modal('modal_registro_seguro', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);
        }
    });
}



function aportaciones_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "aportaciones/aportaciones_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            mes_anio: $('#cmb_apor_fil_mes').val() + '-' + $('#cmb_apor_fil_anio').val()
        }),
        beforeSend: function () {
//      $('#div_aportaciones_tabla').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
//      $('#div_aportaciones_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_aportaciones_tabla').html(html);
        },
        complete: function (data) {
            if (data.status != 200) {
                console.log(data);
                $('#div_aportaciones_tabla').html('EXISTE UN ERROR QUE NO PERMITE LISTAR LAS aportacionesS, REPÓRTELO');
            }
            $('#div_aportaciones_tabla').removeClass("ui-state-disabled");
        }
    });
}

function seguro_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "aportaciones/seguro_tabla.php",
        async: true,
        dataType: "html",
        data: ({

        }),
        beforeSend: function () {
//      $('#div_seguro_tabla').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
//      $('#div_seguro_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_seguro_tabla').html(html);
        },
        complete: function (data) {
            if (data.status != 200) {
                console.log(data);
                $('#div_seguro_tabla').html('EXISTE UN ERROR QUE NO PERMITE LISTAR LAS aportacionesS, REPÓRTELO');
            }
            $('#div_seguro_tabla').removeClass("ui-state-disabled");
        }
    });
}

function sueldo_usuario_tabla() {
//    console.log("estoy entrando aki.....");
    $.ajax({
        type: "POST",
        url: VISTA_URL + "aportaciones/sueldo_usuario_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            mes_anio: $('#cmb_sueldo_mes').val() + '-' + $('#cmb_sueldo_anio').val()
        }),
        beforeSend: function () {
//      $('#div_sueldo_usuario_tabla').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
//      $('#div_sueldo_usuario_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_sueldo_usuario_tabla').html(html);
        },
        complete: function (data) {
            if (data.status != 200) {
                console.log(data);
                $('#div_sueldo_usuario_tabla').html('EXISTE UN ERROR QUE NO PERMITE LISTAR LAS aportacionesS, REPÓRTELO');
            }
            $('#div_sueldo_usuario_tabla').removeClass("ui-state-disabled");
        }
    });
}

function eliminar_aportacines(act, id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿Está seguro de Eliminar?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "aportaciones/aportaciones_reg.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: act,
                        afp_id: id
                    }),
                    beforeSend: function () {
//      $('#msj_aportes').html("Guardando...");
//      $('#msj_aportes').show(100);
                    },
                    success: function (data) {
                        swal_success("SISTEMA", data.msj, 2000);
                        aportaciones_tabla();
                    },
                    complete: function (data) {
                        if (data.status != 200)
                            console.log(data);

                    }
                });
            },
            no: function () {}
        }
    });
}

function guardar_sueldo_forma(usu_id, tipo){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"aportaciones/sueldo_usuario_reg.php",
    async:true,
    dataType: "html",                      
    data: ({
      usuario_id: usu_id,
      tipo: tipo,
      mes_anio: $('#cmb_sueldo_mes').val()+'-'+$('#cmb_sueldo_anio').val()
    }),
    beforeSend: function() {
//      $('#msj_sueldo').show(300);
//      $('#msj_sueldo').html("Guardando...");
    },
    success: function(html){    
//      $('#msj_sueldo').html(html); 
            swal_success("SISTEMA",html,2000);
    },
    complete: function(data){
      if(data.status != 200){
        console.log(data);
        $('#msj_sueldo').html("ERRROOOORRRR CONSULTE POR FAVOR");
      }
      
    }
  });
}


$('#cmb_apor_fil_mes, #cmb_apor_fil_anio').change(function (event) {
    aportaciones_tabla();
});

$('#cmb_sueldo_mes, #cmb_sueldo_anio').change(function (event) {
    sueldo_usuario_tabla();
});
