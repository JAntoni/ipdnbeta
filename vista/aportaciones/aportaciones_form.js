/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    $('.moneda').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0.00',
        vMax: '99999.00'
    });
    $('#cmb_seguro_id').change(function (event) {
        /*if($(this).val() == 1)
         $('#td_valor').text('Porcentaje:');
         else if($(this).val() == 2)
         $('#td_valor').text('Pago Fijo:');
         else
         $('#td_valor').text('Valor 1 UIT:');*/
        var tipo = parseInt($(this).find(':selected').data('tipo'));

        if (tipo == 1) {
            $('.tr_afp').show();
            $('#txt_apor_val2').val();
            $('#txt_apor_val3').val();
            $('#txt_apor_val4').val();
        } else {
            $('.tr_afp').hide();
            $('#txt_apor_val2').val(0);
            $('#txt_apor_val3').val(0);
            $('#txt_apor_val4').val(0);
        }
    });


    $("#form_aportaciones").validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL+"aportaciones/aportaciones_reg.php",
                async: true,
                dataType: "json",
                data: $("#form_aportaciones").serialize(),
                beforeSend: function () {

                },
                success: function (data) {
                    aportaciones_tabla();
                    swal_success("SISTEMA", data.msj, 2000);
                    $('#modal_registro_aportaciones').modal('hide');
                },
                complete: function (data) {
                    if (data.status != 200) {
                        $('#msj_aportes').html("Error al editar: " + data.responseText);
                        console.log(data);
                    }
                }
            });
        },
        rules: {
            txt_apor_nom: {
                required: true
            },
            txt_apor_val: {
                required: true
            },
            cmb_seguro_id: {
                required: true
            }
        },
        messages: {
            txt_apor_nom: {
                required: 'Nombre'
            },
            txt_apor_val: {
                required: '*'
            },
            cmb_seguro_id: {
                required: 'Seleccione el seguro'
            }
        }
    });


});