<?php 
require_once ("../../core/usuario_sesion.php");
require_once ("../aportaciones/Afps.class.php");
$oAfps = new Afps();

if($_POST['action'] == 'insertar'){
  $mes = $_POST['cmb_apor_mes'];
  $anio = $_POST['cmb_apor_anio'];

  //$tipo = $_POST['cmb_apor_tip'];
  //$nombre = $_POST['txt_apor_nom'];
  $seguro_id = intval($_POST['cmb_seguro_id']);
  $aporte = floatval($_POST['txt_apor_val']);
  $aporte2 = floatval($_POST['txt_apor_val2']);
  $aporte3 = floatval($_POST['txt_apor_val3']);
  $aporte4 = floatval($_POST['txt_apor_val4']);

  $his = '<b>Registrado por: '.$_SESSION['usuario_nom'].' | '.date('d-m-Y h:i a').'</b><br>';

  $oAfps->tb_afps_per=$mes.'-'.$anio; 
  $oAfps->tb_seguro_id=$seguro_id;
  $oAfps->tb_afps_apor=$aporte; 
  $oAfps->tb_afps_apor2=$aporte2; 
  $oAfps->tb_afps_apor3=$aporte3;
  $oAfps->tb_afps_apor4=$aporte4; 
  $oAfps->tb_afps_his=$his;
  
  $oAfps->insertar();

  $data['msj'] = 'Registrado correctamente';

  echo json_encode($data);
}
if($_POST['action'] == 'editar'){
  $mes = $_POST['cmb_apor_mes'];
  $anio = $_POST['cmb_apor_anio'];
  $seguro_id = intval($_POST['cmb_seguro_id']);
  //$tipo = $_POST['cmb_apor_tip'];
  //$nombre = $_POST['txt_apor_nom'];
  $aporte = floatval($_POST['txt_apor_val']);
  $aporte2 = floatval($_POST['txt_apor_val2']);
  $aporte3 = floatval($_POST['txt_apor_val3']);
  $aporte4 = floatval($_POST['txt_apor_val4']);
  $afp_id = $_POST['hdd_afp_id'];
  $his = '<b>Editado por: '.$_SESSION['usuario_nom'].' | '.date('d-m-Y h:i a').'</b><br>';

  $oAfps->tb_afps_per =$mes.'-'.$anio;
  $oAfps->tb_seguro_id =$seguro_id; 
  $oAfps->tb_afps_apor =$aporte;
  $oAfps->tb_afps_apor2 =$aporte2;
  $oAfps->tb_afps_apor3 =$aporte3;
  $oAfps->tb_afps_apor4 =$aporte4;
  $oAfps->tb_afps_his = $his;
  $oAfps->tb_afps_id=$afp_id;
  $oAfps->editar();

  $data['msj'] = 'Editado correctamente';

  echo json_encode($data);
}
if($_POST['action'] == 'eliminar'){
  $afp_id = $_POST['afp_id'];

  $oAfps->eliminar($afp_id);

  $data['msj'] = 'Eliminado correctamente';

  echo json_encode($data);
}

?>