<?php
require_once ("../../core/usuario_sesion.php");
require_once ("../aportaciones/Afps.class.php");
$oAfps = new Afps();

$mes_anio = $_POST['mes_anio'];
if (empty($mes_anio)) {
    echo 'No hay mes ni año seleccionados';
    exit();
}
$dts = $oAfps->listar_afps_periodo($mes_anio); //parámetro vacío para que liste todos los registros
//$num_rows = mysql_num_rows($dts);
?>

<input type="hidden" id="usuario_mos" value="<?php echo $_SESSION['usuario_mos']; ?>">
<?php if ($dts['estado'] == 1) { ?>
    <table cellspacing="1" id="tabla_asistencia" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila" WIDTH="10%">ID</th>
                <th id="tabla_cabecera_fila">SEGURO</th>
                <th id="tabla_cabecera_fila">APORTE</th>
                <th id="tabla_cabecera_fila">PRIMA</th>
                <th id="tabla_cabecera_fila">COMISION SOBRE FLUJO</th>
                <th id="tabla_cabecera_fila" WIDTH="20%">COMISION ANUAL SOBRE SALDO</th>
                <!--<th id="tabla_cabecera_fila">MIXTA</th>-->
                <th id="tabla_cabecera_fila" WIDTH="15%"></th>
            </tr>
        </thead>
        <tbody> 
            <?php foreach($dts['data']as $key=>$dt){ ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila">
                        <?php echo $dt['tb_afps_id']; ?>
                    </td>
                    <td id="tabla_fila">
                            <?php 
                                echo $dt['tb_seguro_nom']; 
                            ?>
                    </td>
                    <td id="tabla_fila">
                            <?php
                               echo $dt['tb_afps_apor'];
                            ?> 
                    </td>
                    <td id="tabla_fila">
                            <?php 
                                echo $dt['tb_afps_apor2']; 
                            ?> 
                    </td>
                    <td id="tabla_fila">
                            <?php 
                                echo $dt['tb_afps_apor3']; 
                            ?> 
                    </td>
                    <td id="tabla_fila">
                            <?php 
                                echo $dt['tb_afps_apor4']; 
                            ?>
                    </td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="aportaciones_form('editar',<?php echo $dt['tb_afps_id']; ?>)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="eliminar_aportacines('eliminar',<?php echo $dt['tb_afps_id']; ?>)"><i class="fa fa-trash"></i></a>
                    </td>
                </tr> 
            <?php
                }
            ?>
        </tbody>
    </table>
    <?php
} else {
    echo '<center><h2>NO HAY REGISTRO DE EMPRESAS PARA APORTAR</h2></center>';
}
?>
