<?php
require_once('Mediocom.class.php');
$oMediocom = new Mediocom();

$mediocom_id = (empty($mediocom_id))? 0 : $mediocom_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oMediocom->listar_mediocomunicaciones();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($mediocom_id == $value['tb_mediocom_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_mediocom_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_mediocom_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>