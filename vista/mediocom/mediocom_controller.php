<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../mediocom/Mediocom.class.php');
  $oMediocom = new Mediocom();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$mediocom_nom = mb_strtoupper($_POST['txt_mediocom_nom'], 'UTF-8');
 		$mediocom_des = $_POST['txt_mediocom_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Medio de Comunicación.';
 		if($oMediocom->insertar($mediocom_nom, $mediocom_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Medio de Comunicación registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$mediocom_id = intval($_POST['hdd_mediocom_id']);
 		$mediocom_nom = mb_strtoupper($_POST['txt_mediocom_nom'], 'UTF-8');
 		$mediocom_des = $_POST['txt_mediocom_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Medio de Comunicación.';

 		if($oMediocom->modificar($mediocom_id, $mediocom_nom, $mediocom_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Medio de Comunicación modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$mediocom_id = intval($_POST['hdd_mediocom_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Medio de Comunicación.';

 		if($oMediocom->eliminar($mediocom_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Medio de Comunicación eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>