
$(document).ready(function(){
  $('#form_mediocom').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"mediocom/mediocom_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_mediocom").serialize(),
				beforeSend: function() {
					$('#mediocom_mensaje').show(400);
					$('#btn_guardar_mediocom').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#mediocom_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#mediocom_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		mediocom_tabla();
		      		$('#modal_registro_mediocom').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#mediocom_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#mediocom_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_mediocom').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#mediocom_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#mediocom_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_mediocom_nom: {
				required: true,
				minlength: 2
			},
			txt_mediocom_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_mediocom_nom: {
				required: "Ingrese un nombre para el Medio de Comunicación",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_mediocom_des: {
				required: "Ingrese una descripción para el Medio de Comunicación",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
