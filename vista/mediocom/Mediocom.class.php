<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Mediocom extends Conexion{

    function insertar($mediocom_nom, $mediocom_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_mediocom(tb_mediocom_nom, tb_mediocom_des)
          VALUES (:mediocom_nom, :mediocom_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mediocom_nom", $mediocom_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":mediocom_des", $mediocom_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($mediocom_id, $mediocom_nom, $mediocom_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_mediocom SET tb_mediocom_nom =:mediocom_nom, tb_mediocom_des =:mediocom_des WHERE tb_mediocom_id =:mediocom_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mediocom_id", $mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":mediocom_nom", $mediocom_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":mediocom_des", $mediocom_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($mediocom_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_mediocom WHERE tb_mediocom_id =:mediocom_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mediocom_id", $mediocom_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($mediocom_id){
      try {
        $sql = "SELECT * FROM tb_mediocom WHERE tb_mediocom_id =:mediocom_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":mediocom_id", $mediocom_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_mediocomunicaciones(){
      try {
        $sql = "SELECT * FROM tb_mediocom";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
