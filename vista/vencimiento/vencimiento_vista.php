<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Créditos</a></li>
            <li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-primary">
            
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php include 'vencimiento_filtro.php'; ?>
                            </div>
                            <div id="msj_vencimiento" class="ui-state-highlight ui-corner-all" style="width:auto; float:left; padding:2px; display:none">

                            </div>
                            <div id="msj_vencimiento_tabla" class="ui-state-highlight ui-corner-all" style="width:auto; float:left; padding:2px; display:none">

                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="vencimiento_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Garantías...</h4>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- Input para guardar el valor ingresado en el search de la tabla-->
                        <input type="hidden" id="hdd_datatable_fil">
                        <div id="div_vencimiento_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php //require_once('vencimiento_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_vencimiento_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>


            <div id="div_modal_creditomenor_pagos"></div>
            <div id="div_modal_creditomenor_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE CREDITO-->
            </div>
            <div id="div_modal_carousel">
                <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
            </div>
            <div id="div_modal_creditolinea_timeline">
            </div>
            <div id="div_filestorage_form">
				<!-- MODAL PARA SUBIR DOCUMENTOS-->
			</div>
            <!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DE PAGOS-->
            <div id="div_modal_creditogarveh_pagos"></div>
            <!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DE PAGOS-->
            <div id="div_modal_creditogarveh_form"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <div id="div_creditogarveh_his">
                <!-- INCLUIMOS EL MODAL DE CARGANDO NOTAS-->
            </div>
            <div id="div_modal_cobranzamora">
                        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA MORA-->
            </div>
            <div id="div_mora_historial_form">
                <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
            </div>
            <div id="div_modal_vencimiento_menor_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_vencimiento_garveh_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_vencimiento_menor_banco_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_vencimiento_menor_form_liquidar">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_acuerdopago_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL AP-->
            </div>
            <div id="div_acuerdopago_anular_form">
                <!-- INCLUIMOS EL MODAL PARA ANULAR AP-->
            </div>
            <div id="div_acuerdopago_pagos_form">
                <!-- INCLUIMOS EL MODAL PARA ANULAR AP-->
            </div>
            <div id="div_modal_liquidacion_menor_form">
                <!-- INCLUIMOS EL MODAL PARA ANULAR AP-->
            </div>
            <div id="div_credito_pdf_ver"></div>
            
            <div id="div_modal_cliente_form"></div>
            
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
            <?php require_once(VISTA_URL . 'templates/modal_general.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
