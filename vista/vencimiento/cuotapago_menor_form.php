<?php
require_once('../../core/usuario_sesion.php');

require_once ("./Vencimiento.class.php");
$oVencimiento = new Vencimiento();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota;
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../tarifario/Tarifario.class.php");
$oTarifario = new Tarifario();
require_once ("../cobranza/Cobranza.class.php");
$oCobranza = new Cobranza;
require_once("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ("../morasugerida/Morasugerida.class.php");
$oMorasugerida = new Morasugerida();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

if ($_POST['action'] == "pagar_fijo" or $_POST['action'] == "pagar_libre") {

    $fecha_hoy = date('d-m-Y');
    $dt = $oCuota->mostrarUno($_POST['cuo_id']);
    $cobranza = $oCobranza->mostrarUnoByCuotaId($_POST['cuo_id']);

    if ($dt['estado'] == 1) {
        $cre_id = $dt['data']['tb_credito_id'];
        $cuo_fec = mostrar_fecha($dt['data']['tb_cuota_fec']);
        $cuo_num = $dt['data']['tb_cuota_num'];
        $mon_id = $dt['data']['tb_moneda_id'];
        $mon_nom = $dt['data']['tb_moneda_nom'];
        $cuo_cuo = $dt['data']['tb_cuota_cuo'];
        $cuo_int = $dt['data']['tb_cuota_int'];
        $cuo_cap = $dt['data']['tb_cuota_cap'];
        $cuo_persubcuo = $dt['data']['tb_cuota_persubcuo'];
        $cuo_per = $dt['data']['tb_cuota_per']; //p
        $cuota_interes = $dt['data']['tb_cuota_interes']; //p
        $cuota_not = $dt['data']['tb_cuota_not']; // notas de la cobranza // gerson (10-03-23)
    }

    $fec = $cuo_fec;
    $readonly = '';

//verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
    $vencida = 0;
    $mensaje_tiulo="";
    $diasvencidos = "";
    //$morasugerida = floatval(0);
    if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
        $vencida = 1;
        $readonly = 'readonly=""';
        $mensaje_tiulo='<b style="font-family: cambria;font-weight: bold;font-size: 15px;color:red;"> - [ Su Cuota se Encuentra Vencida]</b>';
        // Convierte ambas fechas a timestamp
        $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($cuo_fec);
        // Convierte la diferencia en segundos a días
        $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));
        $diasvencidos = '<b style="font-family: cambria;font-weight: bold;font-size: 15px;color:red">'.$diferencia_en_dias.' DIAS</b>';
        /*$resultmora = $oMorasugerida->obtener_morasugerida($cuo_int, $diferencia_en_dias, 1);
        if($resultmora['estado']==1){
        //echo $cuo_int . ' //' . $diferencia_en_dias;
        $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
        $morasugerida = $diferencia_en_dias * $porcentajemora * $cuo_int;
        }*/
    }

    $dts = $oCredito->mostrarUno($cre_id);
    if ($dts['estado'] == 1) {
        $cre_preaco = floatval($dts['data']['tb_credito_preaco']);
        $cuotip_id = $dts['data']['tb_cuotatipo_id'];
        $cuotip_nom = $dts['data']['tb_cuotatipo_nom'];
        $cre_int = $dts['data']['tb_credito_int'];
        $cre_numcuo = $dts['data']['tb_credito_numcuo'];
        $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
        $cli_id = $dts['data']['tb_cliente_id'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom = $dts['data']['tb_cliente_nom'];
        $cliente = $cli_nom . ' | ' . $cli_doc;
    }

    /* CALCULAMOS SI EL CREDITO DEL CLIENTE TIENE MAS DE UNA CUOTA VECIDA */
    if ($_POST['action'] == "pagar_libre") {
        $result = $oVencimiento->nro_cuotas_Pendientes($cli_id, fecha_mysql($cuo_fec), $cre_id);
        if ($result['estado'] == 1) {
            $total = $result['data']['cuotas'];
        }
        if ($total > 0) {
            $mensaje .= 'USTED TIENE ' . ($total) . ' CUOTA(S) VENCIDAS.';
            $mensaje .= ' PARA PODER CANCELAR ESTA CUOTA DEBE DE CANCELAR LA(S) CUOTA(S) ANTERIOR(ES)';
        }
        $result = NULL;
    }
    /* FIN CALCULAMOS SI EL CREDITO DEL CLIENTE TIENE MAS DE UNA CUOTA VECIDA */


    $codigo = 'CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);

    if ($cuotip_id == 2)
        $cuota = $cuo_num . '/' . $cre_numcuo . '';
    if ($cuotip_id == 1)
        $cuota = $cuo_num . '/' . $cuo_num . ' (' . $cre_numcuomax . ')';

    $pag_det = "PAGO DE CUOTA $codigo N° CUOTA: $cuota";

    $mod_id = '1'; //cuota
    $modid = $_POST['cuo_id'];

    $dts1 = $oClientecaja->filtrar_por_cliente_creditomenor($cli_id, $mon_id); //filtramos su caja por cliente de todos sus creditos
    $clientecaja = 0;
    if ($dts1['estado'] == 1) {
        foreach ($dts1['data'] as $key => $dt1) {
            if ($dt1['tb_clientecaja_tip'] == 1) {
                $clientecaja = $clientecaja + $dt1['tb_clientecaja_mon'];
            }
            if ($dt1['tb_clientecaja_tip'] == 2) {
                $clientecaja = $clientecaja - $dt1['tb_clientecaja_mon'];
            }
        }
    }

    $cliente_caja = mostrar_moneda($clientecaja);

//pagos parciales
    $pagos_cuota = 0;
    $mod_id = 1;
    $dts3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $_POST['cuo_id']);
    if ($dts3['estado'] == 1) {
        foreach ($dts3['data'] as $key => $dt3) {
            $pagos_cuota += floatval($dt3['tb_ingreso_imp']);
        }
    }
    $pagos = $pagos_cuota;
    $pagos_parciales = mostrar_moneda($pagos);

    $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
    $diasdelmes = date('t', strtotime($diasdelmes1));

    $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
    $fechahoy = date('d-m-Y'); // la fecha actual del dia
    $fechaPago = $cuo_fec; // cuando me toca pagar la cuota


    $variableopcional = "";
    if ($cuo_num == 1) {
        $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
    }
    if ($cuo_num > 1) {
        $fechaFacturacion = strtotime($fechaFacturacion);
        $fechahoy = strtotime($fechahoy);
        $fechaPago = strtotime($fechaPago);

        if ($fechahoy < $fechaFacturacion) {
            $dias = 0;
            $variableopcional = 1;
        }
        if ($fechahoy > $fechaFacturacion) {

            $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
            $variableopcional = 0;
        }
    }

    $diastranscurridos = $dias;

    $monto_pagar = formato_moneda($cuo_cuo) - formato_moneda($pagos);

    $ultimo_pago = 0;
    if ($cuotip_id == 2) {//fijo
        if ($cuo_num == $cre_numcuo) {
            $ultimo_pago = 1;
            $ultima_cuota = " <span style='color:red;'>ÚLTIMA CUOTA</span>";
            $vMax = formato_moneda($monto_pagar);
        } else {
            $vMax = '99999.00';
        }
    }

    if ($cuotip_id == 1) {//libre
        $vMax = formato_moneda($monto_pagar);
        if ($cuo_num == $cre_numcuomax) {
            $ultimo_pago = 1;
            $ultima_cuota = " <span style='color:red;'>ÚLTIMA CUOTA</span>";

            $monto_pagar = $cuo_int - $pagos;
            if ($monto_pagar <= 0)
            $monto_pagar = 0;
            else
            $monto_pagar = mostrar_moneda($monto_pagar);
        
        } else {
            $monto_pagar = $cuo_int - $pagos;
            if ($monto_pagar <= 0)
                $monto_pagar = 0;
            else
                $monto_pagar = mostrar_moneda($monto_pagar);

            if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
                
            } else {
                if ($cuo_num > 1) {
                    $monto_pagar = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
                }
            }
        }
    }
}



if ($usureg > 0) {
    $rws = $oUsuario->mostrarUno($usureg);
    $usureg = $rw['data']['tb_usuario_nom'] . " " . $rw['data']['tb_usuario_ape'];
}


/*$result4 = $oCliente->listar_nota_cliente_credito2($cli_id,$cre_id,3,'1,4'); //ASIVEH de tipo ACUERDO DE PAGO
$vernotas = '';
if ($result4['estado'] == 1) {
    $vernotas .= '<ul>';
    foreach ($result4['data'] as $key => $value) {
      $vernotas .= '<li><strong>'.$value['tb_usuario_nom'].':</strong> '.$value['tb_notacliente_det'].'. <strong> '.mostrar_fecha_hora($value['tb_notacliente_reg']).'</strong></li>';
    }
      $vernotas .= '</ul>';
}*/


//verificar si es una liquidación anticipada
$hoy = date('d--m-Y');
restaFechas($_POST['fecha'], $_POST['fechaPago']);
?>

<?php if ($total == 0) { ?>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenor" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">INFORMACIÓN DE CUOTA A PAGAR <?php echo $mensaje_tiulo ?></h4>
                </div>
                <form id="for_cuopag" method="post">
                    <input type="hidden" id="hdd_cuo_per" value="<?php echo $cuo_per; ?>">
                    <input type="hidden" id="hdd_usuariogrupo" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">

                    <input type="hidden" name="hdd_chk_mor_aut" id="hdd_chk_mor_aut" value="0">
                    <input type="hidden" name="hdd_diasatraso" id="hdd_diasatraso" value="<?php echo $diferencia_en_dias ?>">
                    <input type="hidden" name="hdd_morasugerida" id="hdd_morasugerida" value="<?php //echo $morasugerida ?>">
                    <input type="hidden" name="xxxx" id="xxxx" value="<?php echo $resultmora['estado'] ?>">
                    <input type="hidden" name="hdd_monto" id="hdd_monto" value="<?php echo $monto_pagar ?>">
                    <input type="hidden" name="hdd_tipo_credito" id="hdd_tipo_credito" value="1">
                    <input type="hidden" name="xxxxXX" id="xxxxXX" value="<?php echo $cuo_int ?>">
                    <input type="hidden" id="hdd_fec_ven" value="<?php echo $cuo_fec; ?>">

                    <input name="action_cuotapago" id="action_cuotapago" type="hidden" value="<?php echo $_POST['action'] ?>">
                    <input name="hdd_cuo_id" id="hdd_cuo_id" type="hidden" value="<?php echo $_POST['cuo_id'] ?>">
                    <input type="hidden" id="hdd_vencida" value="<?php echo $vencida ?>">
                    <input type="hidden" id="hdd_cuotip_id" value="<?php echo $cuotip_id ?>">

                    <input name="hdd_cli_id" id="hdd_cli_id" type="hidden" value="<?php echo $cli_id ?>">
                    <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id ?>">

                    <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $mon_id ?>">

                    <input name="hdd_mod_id" id="hdd_mod_id" type="hidden" value="<?php echo $mod_id ?>">
                    <input name="hdd_modid" id="hdd_modid" type="hidden" value="<?php echo $modid ?>">

                    <input name="hdd_cre_int" id="hdd_cre_int" type="hidden" value="<?php echo $cre_int ?>">
                    <input name="hdd_cuo_fec" id="hdd_cuo_fec" type="hidden" value="<?php echo $cuo_fec ?>">
                    <input name="hdd_cuo_num" id="hdd_cuo_num" type="hidden" value="<?php echo $cuo_num ?>">
                    <input name="hdd_cuo_max" id="hdd_cuo_max" type="hidden" value="<?php echo $cre_numcuomax ?>">
                    <input name="hdd_cuo_cap" id="hdd_cuo_cap" type="hidden" value="<?php echo $cuo_cap ?>">
                    <input name="hdd_var_op" id="hdd_var_op" type="hidden" value="<?php echo $variableopcional ?>">

                    <input name="hdd_pag_det" id="hdd_pag_det" type="hidden" value="<?php echo $pag_det ?>">
                    <input type="hidden" name="hdd_ultimo_pago" id="hdd_ultimo_pago" value="<?php echo $ultimo_pago; ?>">
                    <input type="hidden" id="hdd_cre_preaco" value="<?php echo $cre_preaco; ?>">

                    <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $_POST['vista'] ?>">

                    <input type="hidden" name="hdd_pc_nombre" id="hdd_pc_nombre" value="<?php echo gethostname() ?>">
                    <input type="hidden" name="hdd_pc_mac" id="hdd_pc_mac"  value="<?php echo exec('getmac') ?>">   

                    <div class="modal-body">

                        <!-- GERSON (10-03-23) -->
                        <div class="box box-danger box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><strong>Compromiso de Pago</strong></h3>
                            </div>
                            <div class="box-body">
                                <?php
                                $arr_not = array();
                                if($cobranza['estado']==1){
                                    $arr_not = explode('<br>', $cuota_not);

                                    if(mostrar_fecha($cobranza['data']['tb_cobranza_fec_compromiso'])!=$cuo_fec){// si las fechas son distintas quiere decir que tiene un compromiso de pago
                                        // Muestra la ultima nota
                                        echo "<span>".$arr_not[0]."</span>";
                                    }else{
                                        // Muestra las 2 ultiams notas
                                        echo "<span>".$arr_not[0]."</span><br>";
                                        echo "<span>".$arr_not[1]."</span>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <!--  -->
                        <div class="box box-warning box-solid" id="notas">
                            <div class="box-header with-border">
                            <h3 class="box-title"><strong>NOTAS</strong><strong id="nota_gc"></strong></h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" id="body_notas">
                                
                            </div>
                        </div>
                        <!--  -->
            
                    
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label style="font-family: cambria;color: #006633;">DATOS DEL CLIENTE Y CREDITO</label>
                                        <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;font-size:15px">
                                            <thead>
                                                <tr id="tabla_cabecera">
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>CRÉDITO</b></td>
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>CLIENTE</b></td>
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>TIPO CRÉDITO</b></td>
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>CAPITAL</b></td>
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>% INTERES</b></td>
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>N° CUOTA</b></td>
                                                    <td id="tabla_cabecera_fila" style="height: 22px"><b>FECHA VENCIMIENTO</b></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id="tabla_fila" style="height: 20px">
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $codigo; ?></b></td>
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $cliente; ?></b></td>
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo 'CRÉDITO MENOR - ' . $cuotip_nom ?></b></td>
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $mon_nom . ' ' . mostrar_moneda($cuo_cap); ?></b></td>
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $cuota_interes . ' %'; ?></b></td>
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $cuota . $ultima_cuota; ?></b></td>
                                                    <td id="tabla_fila" align="center" style="height: 22px"><b style="<?php if ($vencida == 1) echo 'color: #cc0000' ?>"><?php echo $cuo_fec; ?></b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br>
                                <?php
                                    if($mensaje_tiulo!=""){
                                    ?>
                                    <h3 style="font-family: cambria;font-weight: bold;font-size: 15px">DIAS VENCIDOS : <?php echo $diasvencidos ?></h3>
                                    <?php
                                    }
                                ?>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-5 control-label" style="text-align:left">CUOTA (<?php echo $mon_nom ?>):</label>
                                            <div class="col-md-7">
                                                <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuodet_cuo" type="text" id="txt_cuodet_cuo" value="<?php echo mostrar_moneda($cuo_cuo) ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <?php // if ($pagos > 0): ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-5 control-label" style="text-align:left">Pagos Parciales (<?php echo $mon_nom ?>):</label>
                                            <div class="col-md-7">
                                                <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuopag_pagpar" type="text" id="txt_cuopag_pagpar" value="<?php echo $pagos_parciales; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <?php // endif ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Mora (<?php echo "S/"//$mon_nom ?>):</label>
                                            <div class="col-md-7">
                                                <div class="input-group">
                                                    <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda3" name="txt_cuopag_mor" type="text" id="txt_cuopag_mor" value="<?php echo $morasugerida ?>" readonly>
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-danger btn-sm" onclick="comprobar()" title="Editar Mora">
                                                            <span class="fa fa-check-square-o"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-12 coment">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-md-2 control-label" style="text-align:left">Comentario:</label>
                                            <div class="col-md-10">
                                                <textarea name="txt_coment" id="txt_coment" rows="2" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p></p>
                                    <?php if ($cuotip_id == 2) { ?>    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-md-5 control-label" style="text-align:left">Caja Cliente (<?php echo $mon_nom ?>):</label>
                                                <div class="col-md-7">
                                                    <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuopag_clicaj" type="text" id="txt_cuopag_clicaj" value="<?php //echo $cliente_caja;                   ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Caja Cliente (<?php echo $mon_nom ?>):</label>
                                                <div class="col-md-7">
                                                    <div class="input-group">
                                                        <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_clicaj_tot" type="text" id="txt_clicaj_tot" value="<?php echo $cliente_caja; ?>" readonly>
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-success btn-sm" onclick="usarSaldo()" title="Usar Saldo de Cliente">
                                                                <span class="fa fa-money icon"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>    
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item" style="border-color: #000000">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Fecha de Pago:</label>
                                                    <div class="col-md-7">
                                                        <div class='input-group date' id='ingreso_fil_picker1'>
                                                            <input name="txt_cuopag_fec" type="text" class="form-control input-sm mayus" class="fecha" id="txt_cuopag_fec" value="<?php echo $fecha_hoy ?>" readonly style="text-align:center;font-weight: bold;">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-md-5 control-label" style="text-align:center">Capital (<?php echo $mon_nom ?>):</label>
                                                    <div class="col-md-7">
                                                        <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="txt_capital" type="text" id="txt_capital" value="<?php echo $cuo_cap; ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                        </div>
                                        <p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Interes (<?php echo $mon_nom ?>):</label>
                                                    <div class="col-md-7">
                                                        <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_interes" type="text" id="txt_interes" value="<?php echo $cuo_int; ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Interés %:</label>
                                                    <div class="col-md-7">
                                                        <div class="input-group">
                                                            <input style="text-align:center;font-weight: bold;font-size: 15px;color: red" class="form-control input-sm mayus" name="txt_cuopag_por" type="text" id="txt_cuopag_por" value="<?php echo $cre_int; ?>" readonly> 
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-primary btn-sm" onclick="Editar()" title="Editar Interés">
                                                                    <span class="fa fa-check-square-o"></span>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Total (<?php echo $mon_nom ?>):</label>
                                                    <div class="col-md-7">
                                                        <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuopag_tot" type="text" id="txt_cuopag_tot" value="<?php echo $monto_pagar; ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <p>
                                        <div class="row">
                                            <?php
                                            if ($cuotip_id == 1):

                                                if ($cuo_num == $cre_numcuomax) {
                                                    
                                                } else {

                                                    $monto_pagar = $cuo_int - $pagos;
                                                    if ($monto_pagar <= 0)
                                                        $monto_pagar = 0;
                                                    else
                                                        $monto_pagar = mostrar_moneda($monto_pagar);

                                                    if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
                                                        
                                                    } else {
                                                        if ($cuo_num > 1) {
                                                            $monto_pagar = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Pago Mínimo (<?php echo $mon_nom ?>):</label>
                                                        <div class="col-md-7">
                                                            <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm mayus" name="txt_cuopag_min" type="text" id="txt_cuopag_min" value="<?php echo $monto_pagar; ?>" readonly>
                                                            <label style="color: red"><?php echo $diastranscurridos ?></label> &nbsp;&nbsp;dias Transcurridos
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>

                                            <div class="col-md-4">
                                                <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Monto Pago (<?php echo $mon_nom ?>):</label>
                                                <div class="col-md-7">  
                                                    <div class="input-group">

                                                        <input style="text-align:center;font-weight: bold;font-size: 15px;color: #000099" class="form-control input-sm moneda3" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $monto_pagar ?>" <?php echo $readonly ?> >
                                                        <span class="input-group-btn">

                                                            <button class="btn btn-success btn-sm" type="button"  onClick="Activar_input_monto_pago_banco()" title="Activar Casilla para ingresar Monto">
                                                                <span class="fa fa-check-square-o"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <p>
                                <div class="row">
                                    <?php
                                    $buton = '';
                                    if ($cuo_num == $cre_numcuo && $cuotip_id == 1) {
                                        ?>
                                        <strong style="font-weight: bold; font-size: 14px; color: red;">Esta es la ultima cuota del credito por lo tando debe Habilitar 2 cuotas mas antes de proceder con el cobro.</strong>

                                        <?php
                                    } else {
                                        $buton = '<button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <!--            <button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>-->
                            <?php echo $buton ?>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>

                </form>

            </div>
        </div>
    </div>


    <?php
} else {
    ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimientomenor" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">MENSAJE IMPORTANTE</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <p  style="font-family: cambria;font-size: 15px;color: #cc0000"><?php echo $mensaje; ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<script type="text/javascript" src="<?php echo 'vista/vencimiento/cuotapago_menor_form.js?ver=031267884'; ?>"></script>