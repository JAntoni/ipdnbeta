<?php

require_once('../../core/usuario_sesion.php');

require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../cajacambio/Cajacambio.class.php");
$oCajacambio = new Cajacambio();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once ('../historial/Historial.class.php');
$oHistorial = new Historial();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$nombre_pc = (!empty($_POST['hdd_pc_nombre'])) ? $_POST['hdd_pc_nombre'] : 'SIN NOMBRE PC';
$mac_pc = (!empty($_POST['hdd_pc_mac'])) ? $_POST['hdd_pc_mac'] : 'SIN NOMBRE MAC';
$cuota_ultima = $_POST['hdd_cuota_ultima']; // 1 al pagarla por completo la desaparecemos de la lista, estado 2 directo

$moneda_credito = 'S/.';
if ($_POST['hdd_mon_id'] == 2) {
    $moneda_credito = 'US$';
}

$moneda_seleccionada = 'S/.';
if ($_POST['cmb_mon_id'] == 2) {
    $moneda_seleccionada = 'US$';
}

$cuotapago_historial = ''; //registramos todo lo que sucede al pagar una cuota
$cuotapago_historial .= 'Este pago fue realizado con asistencia del cliente a oficinas, registrado por: ' . trim($_SESSION['usuario_nombre']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

if ($_POST['action_cuotapago'] == "pagar_fijo") {
    $hoy = date('d-m-Y');
    $fecha_pago = $_POST['txt_cuopag_fec'];
    $data['action_cuotapago'] = '';

    if ($hoy != $fecha_pago) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $hoy . ', fecha a pagar: ' . $fecha_pago;
        echo json_encode($data);
        exit();
    }

    if (!empty($_POST['txt_cuopag_mon'])) {
        //$fecha = date('Y-m-d');
        $acuerdo_ap = $_POST['hdd_acupag']; //si el valor es 1 entonces es un acuerdo de pago
        $tipo_caja = 1; //las cajas a tener en cuenta es 1 CAJA y 14 CAJA AP
        $paga_con_ap = $_POST['che_mon_ap']; //0 pago normal, 1 paga con el monto de un acuerdo de pago

        if ($acuerdo_ap == 1) {
            $tipo_caja = 14; //CAJA AP
            $cuotapago_historial .= 'Esta cuota es de un Acuerdo de Pago, por ende el monto fue directamente a la CAJA AP ||&& ';
        }
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $cuotapago_total = $_POST['txt_cuopag_tot'];
        $monto_pagado = $_POST['txt_cuopag_mon'];

        if ($paga_con_ap == 1) {
            $cuotapago_historial .= 'Este pago se hizo mediante la aplicación de un Acuerdo de Pago, dicho AP tiene como código: CAV-' . str_pad($_POST['hdd_acuerdo_id'], 4, "0", STR_PAD_LEFT) . ' ||&& ';
            if (moneda_mysql($_POST['hdd_monto_aplicar']) >= moneda_mysql($_POST['txt_cuopag_tot']))
                $monto_pagado = $_POST['txt_cuopag_tot'];
            else
                $monto_pagado = $_POST['hdd_monto_aplicar']; //paga con el monto acumulado en su ACUERDO DE PAGO
        }

        $xac = 1;
        //parametro nulo ya que no tiene observacion para pagar un garveh, dos veces el cuopag_mon ya que para asiveh hay un campo que paga con
        $cuotapago_detalle = 'PAGO REALIZADO EN CAJA EN EFECTIVO' . ' | ' . $_POST['txt_cuopag_obs'];
        $oCuotapago->tb_cuotapago_xac = $xac;
        $oCuotapago->tb_cuotapago_usureg = $_SESSION['usuario_id'];
        $oCuotapago->tb_cuotapago_usumod = $_SESSION['usuario_id'];
        $oCuotapago->tb_modulo_id = $_POST['hdd_mod_id'];
        $oCuotapago->tb_cuotapago_modid = $_POST['hdd_modid'];
        $oCuotapago->tb_cuotapago_fec = $fecha;
        $oCuotapago->tb_moneda_id = $_POST['hdd_mon_id'];
        $oCuotapago->tb_cuotapago_mon = moneda_mysql($monto_pagado);
        $oCuotapago->tb_cuotapago_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
        $oCuotapago->tb_cuotapago_moncam = moneda_mysql($_POST['txt_cuopag_moncam']);
        $oCuotapago->tb_cuotapago_pagcon = moneda_mysql($monto_pagado);
        $oCuotapago->tb_cliente_id = $_POST['hdd_cli_id'];
        $oCuotapago->tb_cuotapago_obs = $cuotapago_detalle;

        $result = $oCuotapago->insertar();
        if (intval($result['estado']) == 1) {
            $cuotapago_id = $result['tb_cuotapago_id'];
        }
        $result = NULL;

        //estado de cuota detalle
        if (moneda_mysql($monto_pagado) == moneda_mysql($cuotapago_total)) {
            $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_est', 2, 'INT');
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT');
            //registro de mora
            $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
            if ($_POST['chk_mor_aut'] == 1) {
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_moraut', 1, 'INT');
            }
            if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                $data['cuotadetalle_id'] = $_POST['hdd_modid'];
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_morest', 1, 'INT');
                $cuotapago_historial .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . '  ||&&';
                
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }

            $monto = moneda_mysql($monto_pagado);
            if ($monto > 0) {
                $ingresar = 1;
            }
        }

        if (moneda_mysql($monto_pagado) < moneda_mysql($cuotapago_total)) {
            $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_est', 3, 'INT');
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 3, 'INT');

            $monto = moneda_mysql($monto_pagado);
            if ($monto > 0) {
                $ingresar = 1;
            }
        }

        if (moneda_mysql($monto_pagado) > moneda_mysql($cuotapago_total)) {

            $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_est', 2, 'INT');
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT');

            //registro de mora
            $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
            if ($_POST['hdd_chk_mor_aut'] == 1) {
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_moraut', 1, 'INT');
            }
            if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                $data['cuotadetalle_id'] = $_POST['hdd_modid'];
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_morest', 1, 'INT');
                $cuotapago_historial .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
            
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }


            $monto = moneda_mysql($cuotapago_total);
            if ($monto > 0) {
                $ingresar = 1;
            }

            $abono = moneda_mysql($monto_pagado) - moneda_mysql($cuotapago_total);
            if ($abono > 0) {
                $abonar = 1;
            }
        }


        if ($ingresar == 1) {
            //registro de ingreso

            $cuenta_id = 1; //CANCEL CUOTAS
            $subcuenta_id = 3; //CUOTAS GARVEH
            $mod_id = 30; //cuota pago
            $ingreso_detalle = $_POST['hdd_pag_det'];
            if ($paga_con_ap == 1) {
                $ingreso_detalle = $ingreso_detalle . ' | Pago aplicado con ACUERDO DE PAGO.';
            }
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuotapago_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = $tipo_caja;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuotapago_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingreso_id = $result['ingreso_id'];
            }
            $result = NULL;

            /*$oIngreso->ingreso_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // FECHA DEPOSITO
            $oIngreso->ingreso_id = $ingreso_id; // ID DE INGRESO
            $oIngreso->registrar_tipocambio();*/
            $oIngreso->modificar_campo_nuevo($ingreso_id, $_SESSION['usuario_id'], 'tipcam', moneda_mysql($_POST['txt_cuopag_tipcam']));

            if ($paga_con_ap == 1) {
                $oIngreso->modificar_campo($ingreso_id, 'tb_ingreso_ap', 1, 'INT'); //1 pago hecho con ACUERDO DE PAGO
                $oIngreso->modificar_campo($ingreso_id, 'tb_ingreso_usumod', $_SESSION['usuario_id'], 1); //1 pago hecho con ACUERDO DE PAGO
            }

            $cuotapago_historial .= 'El ingreso generado en caja tuvo el siguiente detalle: ' . $ingreso_detalle . ' ||&& ';

            $linea_detalle = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha hecho la transacción: ' . $ingreso_detalle . '. El monto válido fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $linea_detalle;
            $oLineaTiempo->insertar();
        }
        //si acu_mon es null es porq esta cuota no tiene q pagar un acuerdo de pago, si se paga con monto AP no se abona nada
        if ($abonar == 1 && $paga_con_ap != 1) {
            //abono cliente caja
            $xac = 1;
            $clientecaja_tipo = 1; //ingreso
            $mod_id = 30; //cuota pago

            $oClientecaja->tb_clientecaja_xac = $xac;
            $oClientecaja->tb_cliente_id = $_POST['hdd_cli_id'];
            $oClientecaja->tb_credito_id = $_POST['hdd_cre_id'];
            $oClientecaja->tb_clientecaja_tip = $clientecaja_tipo;
            $oClientecaja->tb_clientecaja_fec = $fecha;
            $oClientecaja->tb_moneda_id = $_POST['hdd_mon_id'];
            $oClientecaja->tb_clientecaja_mon = moneda_mysql($abono);
            $oClientecaja->tb_modulo_id = $mod_id;
            $oClientecaja->tb_clientecaja_modid = $cuotapago_id;
            //abono cliente caja
            $result = $oClientecaja->insertar();
            if (intval($result['estado']) == 1) {
                $clicaj_id = $result['tb_clientecaja_id'];
            }
            $result = NULL;

            $cuenta_id = 21; //saldo a favor
            $subcuenta_id = 62; //saldo a favor
            $mod_id = 40; //cliente caja
            $ingreso_detalle = "ABONO CLIENTE " . $_POST['hdd_pag_det'];
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $clicaj_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($abono);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuotapago_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $oIngreso->insertar();

            $cuotapago_historial .= 'El ingreso generado fue mayor a la cuota facturada, por eso a la Caja Cliente fue un saldo a favor de: ' . $moneda_credito . ' ' . mostrar_moneda($abono) . ' ||&& ';
        }

        //pago con caja cliente, EL PAGO DE ACUERDO DE PAGO VA A OTRA CAJA
        if (moneda_mysql($_POST['txt_cuopag_clicaj']) > 0) {
            //retiro de caja cliente
            $xac = 1;
            $clientecaja_tipo = 2; //egreso
            $mod_id = 30; //cuota pago

            $oClientecaja->tb_clientecaja_xac = $xac;
            $oClientecaja->tb_cliente_id = $_POST['hdd_cli_id'];
            $oClientecaja->tb_credito_id = $_POST['hdd_cre_id'];
            $oClientecaja->tb_clientecaja_tip = $clientecaja_tipo;
            $oClientecaja->tb_clientecaja_fec = $fecha;
            $oClientecaja->tb_moneda_id = $_POST['hdd_mon_id'];
            $oClientecaja->tb_clientecaja_mon = moneda_mysql($_POST['txt_cuopag_clicaj']);
            $oClientecaja->tb_modulo_id = $mod_id;
            $oClientecaja->tb_clientecaja_modid = $cuotapago_id;
            //abono cliente caja
            $result1 = $oClientecaja->insertar();
            if (intval($result1['estado']) == 1) {
                $clicaj_id = $result1['tb_clientecaja_id'];
            }
            $result1 = NULL;

            //registro de egreso
            $xac = 1;
            $cuenta_id = 20; //saldo a favor
            $subcuenta_id = 61; //descuento saldo a favor
            $pro_id = 1; //sistema
            $mod_id = 40; //cliente caja
            $egreso_detalle = "RETIRO CLIENTE " . $_POST['hdd_pag_det'];
            $caja_id = 1;
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $clicaj_id;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_clicaj']);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = $_POST['hdd_mon_id'];
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $clicaj_id;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

            //registro de ingreso
            $cuenta_id = 1; //CANCEL CUOTAS
            $subcuenta_id = 3; //CUOTAS garveh
            $mod_id = 30; //cuota pago
            $ingreso_detalle = $_POST['hdd_pag_det'] . " | CON CAJA CLIENTE";
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuotapago_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_clicaj']);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = $tipo_caja;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuotapago_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $oIngreso->insertar();

            $cuotapago_historial .= 'Del saldo a favor del cliente se hizo uso para abonar a esta cuota, fue un total de: ' . $moneda_credito . ' ' . mostrar_moneda($_POST['txt_cuopag_clicaj']) . ' ||&& ';
        }

        //pago con diferente moneda, teniendo en cuenta que no se está pagando con AP, porque la moneda del monto AP es igual a la de la cuota
        if ($_POST['hdd_mon_id'] != $_POST['cmb_mon_id'] and $_POST['txt_cuopag_moncam'] > 0 && $paga_con_ap != 1) {
            //cambio
            $xac = 1;
            if ($_POST['hdd_mon_id'] == 2 and $_POST['cmb_mon_id'] == 1) {
                $descripcion = "DOLARES A SOLES | " . $_POST['hdd_pag_det'];
                $cuotapago_historial .= 'El pago se hizo con moneda diferente al crédito por eso en en caja hay un cambio de DOLARES A SOLES. Se ingresó ' . $moneda_seleccionada . ' ' . mostrar_moneda($_POST['txt_cuopag_moncam']) . ' y con cambio de moneda el monto pagado fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto_pagado) . ' ||&& ';
            }

            if ($_POST['hdd_mon_id'] == 1 and $_POST['cmb_mon_id'] == 2) {
                $descripcion = "SOLES A DOLARES | " . $_POST['hdd_pag_det'];
                $cuotapago_historial .= 'El pago se hizo con moneda diferente al crédito por eso en en caja hay un cambio de SOLES A DOLARES. Se ingresó ' . $moneda_seleccionada . ' ' . mostrar_moneda($_POST['txt_cuopag_moncam']) . ' y con cambio de moneda el monto pagado fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto_pagado) . ' ||&& ';
            }

            $caja_id = 1;
            $mod_id = 30;

            $oCajacambio->tb_cajacambio_usureg = $_SESSION['usuario_id'];
            $oCajacambio->tb_cajacambio_usumod = $_SESSION['usuario_id'];
            $oCajacambio->tb_cajacambio_xac = $xac;
            $oCajacambio->tb_caja_id = $caja_id;
            $oCajacambio->tb_moneda_id = $_POST['hdd_mon_id'];
            $oCajacambio->tb_cajacambio_fec = $fecha;
            $oCajacambio->tb_cajacambio_imp = moneda_mysql($monto_pagado);
            $oCajacambio->tb_cajacambio_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
            $oCajacambio->tb_cajacambio_cam = moneda_mysql($_POST['txt_cuopag_moncam']);
            $oCajacambio->tb_cajacambio_des = $descripcion;
            $oCajacambio->tb_modulo_id = $mod_id;
            $oCajacambio->tb_cajacambio_modid = $cuotapago_id;
            $oCajacambio->tb_empresa_id = $_SESSION['empresa_id'];

            $result = $oCajacambio->insertar();
            if (intval($result['estado']) == 1) {
                $cajcam_id = $result['cajacambio_id'];
            }
            $result = NULL;

            $egreso_detalle = 'CAMBIO DE MONEDA: ' . $descripcion;
            $cuenta_id = 23;
            $subcuenta_id = 0;
            $doc_id = 9;
            $mod_id = 60;
            $numerodocumento = $mod_id . '-' . $cajcam_id;
            $egr_est = 1;
            $pro_id = 1;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = $egr_est;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = $_POST['hdd_mon_id'];
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $cajcam_id;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

            $ingreso_detalle = 'CAMBIO DE MONEDA: ' . $descripcion;
            $doc_id = 8;
            $mod_id = 60;
            $numerodocumento = $mod_id . '-' . $cajcam_id;
            $cuenta_id = 24;
            $subcuenta_id = 0;
            $ing_est = '1';
            //$cli_id=1;
            $cli_id = $_POST['hdd_cli_id'];

            if ($_POST['hdd_mon_id'] == '1')
                $mon_id = '2';
            if ($_POST['hdd_mon_id'] == '2')
                $mon_id = '1';

            //registro de ENTRADA
            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_moncam']);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = $cli_id;
            $oIngreso->caja_id = $caja_id;
            $oIngreso->moneda_id = $mon_id;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cajcam_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingreso_id = $result['ingreso_id'];
            }
            $result = NULL;

            /*$oIngreso->ingreso_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // FECHA DEPOSITO
            $oIngreso->ingreso_id = $ingreso_id; // ID DE INGRESO
            $oIngreso->registrar_tipocambio();*/
            $oIngreso->modificar_campo_nuevo($ingr_id, $_SESSION['usuario_id'], 'tipcam', moneda_mysql($_POST['txt_cuopag_tipcam']));
        }
        //si se paga con el monto del AP, entonces hacemos un egreso a la CAJA AP, por el monto si es igual al total, egresaos el total sino solo lo que es debido
        if ($paga_con_ap == 1) {

            $xac = 1;
            $cuenta_id = 19; //tb_cuenta id 19 ya que es de CREDITOS
            $subcuenta_id = 58; //TB_SUBCUENTA ID 58 ya que es de credito GARVEH
            $pro_id = 1; //sistema
            $mod_id = 100; //EL EGRESO DE CAJA AP tedrá como mod_id 100 ya que no tiene tb_cliente_id, por ello egreso_modide guardará el id del cliente
            $egreso_detalle = 'EGRESO DE CAJA AP PARA APLICAR MONTO A CUOTA VENCIDA EN ACUERDO DE PAGO, CREDITO GARVEH (ID: CGV-' . $_POST['hdd_cre_id'] . ')';
            $caja_id = 14; //caja id 14 ya que es CAJA AP
            $numerodocumento = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuotapago_id;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = $_POST['hdd_mon_id'];
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $_POST['hdd_cli_id']; //guardamos el id del cliente en la columnna egreso_modide
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            //$oEgreso->insertar();

            $result = $oEgreso->insertar();
            if (intval($result['estado']) == 1) {
                $egre_id = $result['egreso_id'];
            }
            $result = NULL;

            //agregamos el id del cuatapago a la columna egreso_ap para cuando se anule el pago el egreso de CAJA AP también se anule este egreso
            $oEgreso->modificar_campo($egre_id, 'tb_egreso_ap', $cuotapago_id, 'INT');
            $oEgreso->modificar_campo($egre_id, 'tb_egreso_usumod', $_SESSION['usuario_id'], 'INT');

            $cuotapago_historial .= 'El monto aplicado mediante ACUERDO DE PAGO generó un egereso en caja con el siguiente detalle: ' . $egreso_detalle . ' ||&& ';
        }
        //estado cuota
        $resultadoscd = $oCuotadetalle->filtrar($_POST['hdd_cuo_id']);
        //$num_cuo = mysql_num_rows($resultadoscd);
        foreach ($resultadoscd['data']as $key => $restultadocd) {
            $est = 3;
            if ($restultadocd['tb_cuotadetalle_est'] != 2)
                break;
            $est = 2;
        }

        $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', $est, 'INT'); //pago parcial
        //si se ha pagado todo el credito, cambia a estado LIQUIDADO
        $resultcredito = $oCredito->mostrarUno($_POST['hdd_cre_id']);
        if ($resultcredito['estado'] == 1) {
            $total_cuotas = $resultcredito['data']['tb_credito_numcuo'];
        }
        $resultcredito = NULL;

        //listamos las cuotas pagadas del crédito
        $resultpagadas = $oCuota->listar_cuotas_pagadas($_POST['hdd_cre_id'], 3);
        if($resultpagadas['estado'] == 1){
            $cuotas_pagadas = count($resultpagadas['data']);
        }else{
            $cuotas_pagadas = 0;
        }

        //si el numero de cuotas del credito e igual al numero de cuotas pagadas entonces el credito pasa a LIQUIDADO, est = 7
        if ($total_cuotas == $cuotas_pagadas) {
            $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 7, 'INT'); //cambiamos a liquidado al estado
            $liquidado = ' | EL crédito ha pasado a liquidado';

            //registramos el historial
            $his = '<span style="color: green;">El crédito pasó a liquidado ya que se pagó la última cuota. | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
            $oCredito->registro_historial($_POST['hdd_cre_id'], $his);
        }

        $oCuotapago->registro_historial($cuotapago_id, $cuotapago_historial); //registramos toda la información obtenida del pago

        $data['cuopag_id'] = $cuotapago_id;
        $data['action_cuotapago'] = $_POST['action_cuotapago'];
        $data['cuopag_msj'] = 'Se registró el pago correctamente.' . $liquidado;
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}

if ($_POST['action_cuotapago'] == "pagar_libre") {
    $hoy = date('d-m-Y');
    $fecha_pago = $_POST['txt_cuopag_fec'];

    if ($hoy != $fecha_pago) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $hoy . ', fecha a pagar: ' . $fecha_pago;
        echo json_encode($data);
        exit();
    }

    if (!empty($_POST['txt_cuopag_mon'])) {
        //$fecha = date('Y-m-d');
        $fecha_pago = $_POST['txt_cuopag_fec'];
        $fecha_vence = $_POST['hdd_cuodet_fec'];

        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $paga_con_ap = $_POST['che_mon_ap']; //0 pago normal, 1 paga con el monto de un acuerdo de pago
        $cuopag_tot = $_POST['txt_cuopag_tot'];
        $monto_pagado = $_POST['txt_cuopag_mon'];

        if ($paga_con_ap == 1) {
            $cuopag_his .= 'Este pago se hizo mediante la aplicación de un Acuerdo de Pago, dicho AP tiene como código: CAV-' . str_pad($_POST['hdd_acuerdo_id'], 4, "0", STR_PAD_LEFT) . ' ||&& ';
            if (moneda_mysql($_POST['hdd_monto_aplicar']) >= moneda_mysql($_POST['txt_cuopag_tot']))
                $monto_pagado = $_POST['txt_cuopag_tot'];
            else
                $monto_pagado = $_POST['hdd_monto_aplicar']; //paga con el monto acumulado en su ACUERDO DE PAGO
        }
        //echo 'chec selec '.$paga_con_ap.' // monto total: '.$monto_pagado.' // total mon ap: '.$_POST['hdd_monto_aplicar']; exit();
        $cuotapago_detalle = 'PAGO REALIZADO EN CAJA EN EFECTIVO' . ' | ' . $_POST['txt_cuopag_obs'];
        $xac = 1;

        $oCuotapago->tb_cuotapago_xac = $xac;
        $oCuotapago->tb_cuotapago_usureg = $_SESSION['usuario_id'];
        $oCuotapago->tb_cuotapago_usumod = $_SESSION['usuario_id'];
        $oCuotapago->tb_modulo_id = $_POST['hdd_mod_id'];
        $oCuotapago->tb_cuotapago_modid = $_POST['hdd_modid'];
        $oCuotapago->tb_cuotapago_fec = $fecha;
        $oCuotapago->tb_moneda_id = $_POST['hdd_mon_id'];
        $oCuotapago->tb_cuotapago_mon = moneda_mysql($monto_pagado);
        $oCuotapago->tb_cuotapago_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
        $oCuotapago->tb_cuotapago_moncam = moneda_mysql($_POST['txt_cuopag_moncam']);
        $oCuotapago->tb_cuotapago_pagcon = moneda_mysql($monto_pagado);
        $oCuotapago->tb_cliente_id = $_POST['hdd_cli_id'];
        $oCuotapago->tb_cuotapago_obs = $cuotapago_detalle;

        $result = $oCuotapago->insertar();
        if (intval($result['estado']) == 1) {
            $cuotapago_id = $result['tb_cuotapago_id'];
        }
        $result = NULL;

        //estado de cuota
        if (moneda_mysql($monto_pagado) >= moneda_mysql($_POST['txt_cuopag_tot'])) {
            $data['cuopag_id'] = 0;
            $data['cuopag_msj'] = 'Para liquidar todo el crédito, por favor en la parte superior seleccione Refinanciar / Liquidar';

            echo json_encode($data);
            exit();
        }

        if (moneda_mysql($monto_pagado) < moneda_mysql($_POST['txt_cuopag_tot'])) {

            if (moneda_mysql($monto_pagado) < moneda_mysql($_POST['txt_cuopag_min'])) {
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_est', 3, 'INT');
                //cuando se genera un ingreso entonces la cuota de esa cuotadetalle le cambiamos a estado 3 en pago parcial
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 3, 'INT'); //pago parcial

                $monto = moneda_mysql($monto_pagado);
                if ($monto > 0) {
                    $ingresar = 1;
                }
            }

            if (moneda_mysql($monto_pagado) >= moneda_mysql($_POST['txt_cuopag_min'])) {
                //antes cuando se pagaba mayor al minimo la cuota tenia estado 2, pero el cliente puede seguir avonando en esa misma cuota si es que su fecha aun no vence, entonces para que siga saliendo la cuota le damos estado 3 de pago parcial
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_est', 3, 'INT');
                //cuando se genera un ingreso entonces la cuota de esa cuotadetalle le cambiamos a estado 3 en pago parcial
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 3, 'INT'); //pago parcial
                //registro de mora
                $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuota_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
                if ($_POST['chk_mor_aut'] == 1)
                    $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuota_moraut', 1, 'INT');
                if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                    $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuota_morest', 1, 'INT');
                    $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
                }

                $monto = moneda_mysql($monto_pagado);
                if ($monto > 0) {
                    $ingresar = 1;
                }

                $capital = moneda_mysql($_POST['txt_cuopag_tot']) - moneda_mysql($monto_pagado);
                //$generar_cuota=1;
                //agregamos el monto de amortización que se hace al capital
                $amorti = moneda_mysql($monto_pagado) - moneda_mysql($_POST['txt_cuopag_min']);
                if ($amorti > 0) {
                    $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($amorti));
                }
                if ($cuota_ultima == 1) {
                    // es decir después de esta cuota hay más generadas ya, por ello es que hay que cambiarla de estado a 2 directamente
                    $oCuotadetalle->modificar_campo($_POST['hdd_cuodet_id'], 'tb_cuotadetalle_est', 2, 'INT');
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT');
                }
            }
        }

        if ($ingresar == 1) {
            //registro de ingreso

            $cuenta_id = 1; //CANCEL CUOTAS
            $subcuenta_id = 3; //CUOTAS garveh
            $mod_id = 30; //cuota pago
            $ingreso_detalle = $_POST['hdd_pag_det'];
            if ($paga_con_ap == 1)
                $ingreso_detalle = $ingreso_detalle . ' | Pago aplicado con ACUERDO DE PAGO.';
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuotapago_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuotapago_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingreso_id = $result['ingreso_id'];
            }
            $result = NULL;

            /*$oIngreso->ingreso_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // FECHA DEPOSITO
            $oIngreso->ingreso_id = $ingreso_id; // ID DE INGRESO
            $oIngreso->registrar_tipocambio();*/
            $oIngreso->modificar_campo_nuevo($ingr_id, $_SESSION['usuario_id'], 'tipcam', moneda_mysql($_POST['txt_cuopag_tipcam']));

            $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: ' . $ingreso_detalle . ' ||&& ';

            $linea_detalle = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha hecho la transacción: ' . $ingreso_detalle . '. El monto válido fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $linea_detalle;
            $oLineaTiempo->insertar();
        }

        //pago con caja cliente
        if (moneda_mysql($_POST['txt_cuopag_clicaj']) > 0) {
            //retiro de caja cliente
            $xac = 1;
            $clientecaja_tipo = 2; //egreso
            $mod_id = 30; //cuota pago

            $oClientecaja->tb_clientecaja_xac = $xac;
            $oClientecaja->tb_cliente_id = $_POST['hdd_cli_id'];
            $oClientecaja->tb_credito_id = $_POST['hdd_cre_id'];
            $oClientecaja->tb_clientecaja_tip = $clientecaja_tipo;
            $oClientecaja->tb_clientecaja_fec = $fecha;
            $oClientecaja->tb_moneda_id = $_POST['hdd_mon_id'];
            $oClientecaja->tb_clientecaja_mon = moneda_mysql($_POST['txt_cuopag_clicaj']);
            $oClientecaja->tb_modulo_id = $mod_id;
            $oClientecaja->tb_clientecaja_modid = $cuotapago_id;
            //abono cliente caja
            $result1 = $oClientecaja->insertar();
            if (intval($result1['estado']) == 1) {
                $clicaj_id = $result1['tb_clientecaja_id'];
            }
            $result1 = NULL;

            //registro de egreso
            $xac = 1;
            $cuenta_id = 20; //saldo a favor
            $subcuenta_id = 61; //descuento saldo a favor
            $pro_id = 1; //sistema
            $mod_id = 40; //cliente caja
            $egreso_detalle = "RETIRO CLIENTE " . $_POST['hdd_pag_det'];
            $caja_id = 1;
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $clicaj_id;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_clicaj']);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = $_POST['hdd_mon_id'];
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $clicaj_id;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

            //registro de ingreso
            $cuenta_id = 1; //CANCEL CUOTAS
            $subcuenta_id = 3; //CUOTAS garveh
            $mod_id = 30; //cuota pago
            $ingreso_detalle = $_POST['hdd_pag_det'] . " | CON CAJA CLIENTE";
            $numerodocumento = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuotapago_id;

            $oIngreso->ingreso_usureg = $_SESSION['usuario_id'];
            $oIngreso->ingreso_usumod = $_SESSION['usuario_id'];
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_clicaj']);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = $_POST['hdd_cli_id'];
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = intval($_POST['hdd_mon_id']);
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuotapago_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];

            $oIngreso->insertar();

            $cuopag_his .= 'Del saldo a favor del cliente se hizo uso para abonar a esta cuota, fue un total de: ' . $moneda_credito . ' ' . mostrar_moneda($_POST['txt_cuopag_clicaj']) . ' ||&& ';
        }

        //pago con diferente moneda
        if ($_POST['hdd_mon_id'] != $_POST['cmb_mon_id'] and $_POST['txt_cuopag_moncam'] > 0) {
            //cambio
            $xac = 1;
            if ($_POST['hdd_mon_id'] == 2 and $_POST['cmb_mon_id'] == 1) {
                $descripcion = "DOLARES A SOLES | " . $_POST['hdd_pag_det'];
                $cuopag_his .= 'El pago se hizo con moneda diferente al crédito por eso en en caja hay un cambio de DOLARES A SOLES. Se ingresó ' . $moneda_seleccionada . ' ' . mostrar_moneda($_POST['txt_cuopag_moncam']) . ' y con cambio de moneda el monto pagado fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto_pagado) . ' ||&& ';
            }

            if ($_POST['hdd_mon_id'] == 1 and $_POST['cmb_mon_id'] == 2) {
                $descripcion = "SOLES A DOLARES | " . $_POST['hdd_pag_det'];
                $cuopag_his .= 'El pago se hizo con moneda diferente al crédito por eso en en caja hay un cambio de SOLES A DOLARES. Se ingresó ' . $moneda_seleccionada . ' ' . mostrar_moneda($_POST['txt_cuopag_moncam']) . ' y con cambio de moneda el monto pagado fue: ' . $moneda_credito . ' ' . mostrar_moneda($monto_pagado) . ' ||&& ';
            }

            $caja_id = 1;
            $mod_id = 30;

            $oCajacambio->tb_cajacambio_usureg = $_SESSION['usuario_id'];
            $oCajacambio->tb_cajacambio_usumod = $_SESSION['usuario_id'];
            $oCajacambio->tb_cajacambio_xac = $xac;
            $oCajacambio->tb_caja_id = $caja_id;
            $oCajacambio->tb_moneda_id = $_POST['hdd_mon_id'];
            $oCajacambio->tb_cajacambio_fec = $fecha;
            $oCajacambio->tb_cajacambio_imp = moneda_mysql($_POST['txt_cuopag_mon']);
            $oCajacambio->tb_cajacambio_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
            $oCajacambio->tb_cajacambio_cam = moneda_mysql($_POST['txt_cuopag_moncam']);
            $oCajacambio->tb_cajacambio_des = $descripcion;
            $oCajacambio->tb_modulo_id = $mod_id;
            $oCajacambio->tb_cajacambio_modid = $cuotapago_id;
            $oCajacambio->tb_empresa_id = $_SESSION['empresa_id'];
            
            $result = $oCajacambio->insertar();
            if (intval($result['estado']) == 1) {
                $cajcam_id = $result['cajacambio_id'];
            }
            $result = NULL;

            $egreso_detalle = 'CAMBIO DE MONEDA: ' . $descripcion;
            $cuenta_id = 23;
            $subcuenta_id = 0;
            $doc_id = 9;
            $mod_id = 60;
            $numerodocumento = $mod_id . '-' . $cajcam_id;
            $egr_est = '1';
            $pro_id = '1';

            //registro de SALIDA

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = $doc_id; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_mon']);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = $egr_est;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = intval($_POST['hdd_mon_id']);
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $modide;
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

            $ingreso_detalle = 'CAMBIO DE MONEDA: ' . $descripcion;
            $doc_id = 8;
            $mod_id = 60;
            $numerodocumento = $mod_id . '-' . $cajcam_id;
            $cuenta_id = 24;
            $subcuenta_id = 0;
            $ing_est = '1';
            //$cli_id=1;
            $cli_id = $_POST['hdd_cli_id'];

            if ($_POST['hdd_mon_id'] == 1){
                $mon_id = 2;
            }
            if ($_POST['hdd_mon_id'] == 2){
                $mon_id = 1;
            }

            //registro de ENTRADA
            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = $doc_id;
            $oIngreso->ingreso_numdoc = $numerodocumento;
            $oIngreso->ingreso_det = $ingreso_detalle;
            $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_moncam']);
            $oIngreso->cuenta_id = $cuenta_id;
            $oIngreso->subcuenta_id = $subcuenta_id;
            $oIngreso->cliente_id = $cli_id;
            $oIngreso->caja_id = $caja_id;
            $oIngreso->moneda_id = $mon_id;
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cajcam_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingreso_id = $result['ingreso_id'];
            }
            $result = NULL;

            /*$oIngreso->ingreso_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // FECHA DEPOSITO
            $oIngreso->ingreso_id = $ingreso_id; // ID DE INGRESO
            $oIngreso->registrar_tipocambio();*/
            $oIngreso->modificar_campo_nuevo($ingr_id, $_SESSION['usuario_id'], 'tipcam', moneda_mysql($_POST['txt_cuopag_tipcam']));
            
        }

        //si se paga con el monto del AP, entonces hacemos un egreso a la CAJA AP, por el monto si es igual al total, egresaos el total sino solo lo que es debido
        if ($paga_con_ap == 1) {

            $xac = 1;
            $cuenta_id = 19; //tb_cuenta id 19 ya que es de CREDITOS
            $subcuenta_id = 59; //TB_SUBCUENTA ID 59 ya que es de credito HIPOTECARIO
            $pro_id = 1; //sistema
            $mod_id = 100; //EL EGRESO DE CAJA AP tedrá como mod_id 100 ya que no tiene tb_cliente_id, por ello egreso_modide guardará el id del cliente
            $egreso_detalle = 'EGRESO DE CAJA AP PARA APLICAR MONTO A CUOTA VENCIDA EN ACUERDO DE PAGO, CREDITO HIPOTECARIO (ID: CH-' . $_POST['hdd_cre_id'] . ')';
            $caja_id = 14; //caja id 14 ya que es CAJA AP
            $numerodocumento = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuotapago_id;

            $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
            $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
            $oEgreso->egreso_fec = $fecha;
            $oEgreso->documento_id = 9; //otros egresos
            $oEgreso->egreso_numdoc = $numerodocumento;
            $oEgreso->egreso_det = $egreso_detalle;
            $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
            $oEgreso->egreso_tipcam = 0;
            $oEgreso->egreso_est = 1;
            $oEgreso->cuenta_id = $cuenta_id;
            $oEgreso->subcuenta_id = $subcuenta_id;
            $oEgreso->proveedor_id = $pro_id;
            $oEgreso->cliente_id = 0;
            $oEgreso->usuario_id = 0;
            $oEgreso->caja_id = $caja_id;
            $oEgreso->moneda_id = $_POST['hdd_mon_id'];
            $oEgreso->modulo_id = $mod_id;
            $oEgreso->egreso_modide = $_POST['hdd_cli_id']; //guardamos el id del cliente en la columnna egreso_modide
            $oEgreso->empresa_id = $_SESSION['empresa_id'];
            $oEgreso->egreso_ap = 0; //si
            $oEgreso->insertar();

            $result = $oEgreso->insertar();
            if (intval($result['estado']) == 1) {
                $egre_id = $result['egreso_id'];
            }
            $result = NULL;

            //agregamos el id del cuatapago a la columna egreso_ap para cuando se anule el pago el egreso de CAJA AP también se anule este egreso
            $oEgreso->modificar_campo($egre_id, 'tb_egreso_ap', $cuotapago_id, 'INT');
            $oEgreso->modificar_campo($egre_id, 'tb_egreso_usumod', $_SESSION['usuario_id'], 'INT');

            $cuopag_his .= 'El monto aplicado mediante ACUERDO DE PAGO generó un egereso en caja con el siguiente detalle: ' . $egreso_detalle . ' ||&& ';
        }
        //la generación de una cuota nueva se hace aparte mediante un botón en el form del pago

        $oCuotapago->registro_historial($cuotapago_id, $cuopag_his); //registramos toda la información obtenida del pago

        $data['cuopag_id'] = $cuotapago_id;
        $data['cuopag_msj'] = 'Se registró el pago correctamente.';
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}

if ($_POST['action'] == "calcular_mora") {
    $moraXdia = 20;
    $mor_dia = restaFechas($_POST['fecha'], $_POST['fechaPago']);

    if ($mor_dia >= 0) {
        $mor = $mor_dia * $moraXdia;
    }
    $data['mora'] = mostrar_moneda($mor);
    $data['estado'] = 1;
    $data['msj'] = '¡exito. se logro conectar con la bd!';
    echo json_encode($data);
}

if ($_POST['action'] == 'cuota_nueva') {

    $cre_id = $_POST['cre_id'];
    $cuo_id = $_POST['cuo_id'];
    $cuodet_id = $_POST['cuodet_id'];
    $cuo_max = $_POST['cuo_max'];
    $cre_int = $_POST['int'];
    $fecha = $_POST['fecha'];
    $mon_id = $_POST['mon_id'];

    //echo $cre_id.' // '.$cuo_id.' // '.$cuodet_id.' // '.$cuo_max.' // '.$cre_int.' // '.$fecha.' // '.$mon_id;
    //el tipo del credito es 3 de garveh, eso va en el primer parametro
    $dts = $oCuota->filtrar(3, $cre_id);
    $num_rows_cuo = count($dts['data']);
    $dts = null;

    if ($num_rows_cuo >= $cuo_max) {
        $data['estado'] = 2;
        $data['msj'] = 'No se puede generar otra cuota ya que el máximo es ' . $cuo_max;
    } else {
        //obtenemos el monto de la cuota con la que fue creada
        $dts = $oCuota->mostrarUno($cuo_id);
        if($dts['estado'] == 1){
            $cuota_cap = $dts['data']['tb_cuota_cap']; //capital de la cuota
            $cuota_int = $dts['data']['tb_cuota_int']; //monto del interes de esa cuota
            $cuota_cuo = $dts['data']['tb_cuota_cuo']; //la cuota fue creada con este monto
        }
        $dts = null;

        //obtenemos todos los ingresos de la cuotadetalle, su mod_id es 2 por ser detalle, 1 por ser cuota
        $dts = $oCuotapago->mostrar_cuotatipo_ingreso(2, $cuodet_id);
        $pagos_cuodet = 0;
        if($dts['estado'] == 1){
            foreach ($dts['data'] as $key => $data) {
                $pagos_cuodet += $dt['tb_ingreso_imp'];
            }
        }
        $dts = null;

        //actualizamos los estado de la cuotadetalle y cuota que ya vencieron, si los pagos pasan al pago minimo las cuotas cambian a canceladas sino se quedan con el estado anterior
        if ($pagos_cuodet >= $cuota_int) {
            $oCuotadetalle->modificar_campo($cuodet_id, 'tb_cuotadetalle_est', 2, 'INT');
            $oCuota->modificar_campo($cuo_id, 'tb_cuotadetalle_est', 2, 'INT');
        }
        //generamos otra cuota con su cuota detalle, para ello obetenemos el nuevo capital que es el mismo capital de la cuota anteior, restado el monto sobrante del pago mínimo
        $capital = $cuota_cap;
        if ($pagos_cuodet > $cuota_int)
            $capital = $cuota_cap - ($pagos_cuodet - $cuota_int);
        //echo $capital.' // '.$cuota_cap.' // '.$pagos_cuodet; exit();
        $C = $capital;
        $i = $cre_int;
        $n = 1; //num cuotas

        $uno = $i / 100;
        $dos = $uno + 1;
        $tres = pow($dos, $n);
        $cuatroA = ($C * $uno) * $tres;
        $cuatroB = $tres - 1;
        $R = $cuatroA / $cuatroB;
        $r_sum = $R * $n;
        //echo $C.' // '.$uno.' // '.$dos.' // '.$tres.' // '.$cuatroA.' // '.$cuatroB; exit();
        list($day, $month, $year) = explode('-', $fecha);

        for ($j = 1; $j <= $n; $j++) {
            if ($j > 1) {
                $C = $C - $amo;
                $int = $C * ($i / 100);
                $amo = 0; //$R - $int;
            } else {
                $int = $C * ($i / 100);
                $amo = 0; //$R - $int;
            }

            //fecha facturacion
            $month = $month + 1;
            if ($month == '13') {
                $month = 1;
                $year = $year + 1;
            }
            $cuota_fecha = validar_fecha_facturacion($day, $month, $year);
            //echo $C.' // '.$amo.' // '.$int.' // '.$R; exit();
            $xac = 1;
            $cretip_id = 3; //credito tipo 3 ya que es garveh
            $est = 1;
            $cuo_num = $num_rows_cuo + 1;
            $persubcuo = 1; //por el momento es 1 mensual

            $oCuota->insertar(
                    $xac,
                    $cre_id,
                    $cretip_id,
                    $mon_id,
                    $cuo_num,
                    fecha_mysql($cuota_fecha),
                    $C,
                    $amo,
                    $int,
                    $R,
                    $pro,
                    $persubcuo,
                    $est
            );

            $dts = $oCuota->ultimoInsert();
            $dt = mysql_fetch_array($dts);
            $cuo_id = $dt['last_insert_id()'];
            mysql_free_result($dts);

            //automatico
            $oCuota->modificar_campo($cuo_id, 'tb_cuota_aut', 1, 'INT');

            $oCuotadetalle->insertar(
                    $xac,
                    $cuo_id,
                    $mon_id,
                    1,
                    fecha_mysql($cuota_fecha),
                    ($R / $persubcuo),
                    $est
            );
        }

        $data['estado'] = 1;
        $data['msj'] = 'Se ha generado la nueva cuota';
    }

    echo json_encode($data);
}

if ($_POST['action'] == 'permiso_cuodet') {

    $cuodet_id = $_POST['cuodet_id'];
    $cuodet_per = $_POST['cuodet_per'];
    $oCuotadetalle->modificar_campo($cuodet_id, 'tb_cuotadetalle_per', $cuodet_per, 'INT');

    echo 1;
}
?>