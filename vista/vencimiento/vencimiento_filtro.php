<form id="vencimiento_filtro" name="vencimiento_filtro">
    <div class="row">
        <div class="col-md-5">
            <!--<label for="">Cliente</label>-->
            <div class="input-group">
                <input type="text" class="form-control input-sm mayus" id="txt_fil_cliente" name="txt_fil_cliente" placeholder="ingrese dni o nombre de cliente a buscar">
                <input type="hidden" id="hdd_fil_cli_id" name="hdd_fil_cli_id" class="form-control input-sm" >
                <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-sm" onclick="vencimiento_tabla()" title="Filtrar" id="btnSearch">
                            <span class="fa fa-search icon"></span>
                        </button>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <button type="button" class="btn bg-purple btn-sm" onclick="vencimiento_completa_tabla()" title="Filtrar" id="btnSearchComplete">
                <span class="fa fa-search icon"></span> Búsqueda Completa
            </button>
        </div>
    </div>
</form>