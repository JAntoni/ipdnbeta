<?php
require_once('../../core/usuario_sesion.php');

require_once ("Vencimiento.class.php");
$oVencimiento = new Vencimiento();
require_once ("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../tarifario/Tarifario.class.php");
$oTarifario = new Tarifario();

require_once("../funciones/fechas.php");
require_once("../funciones/funciones.php");

$tipo_pago = $_POST['tipo'];
$action = 'liquidar';

if (intval($tipo_pago) == 2)
    $action = 'amortizar';

if ($_POST['action'] == "pagar_fijo" or $_POST['action'] == "pagar_libre") {
    $fecha_hoy = date('d-m-Y');
    $dts = $oCuota->mostrarUno($_POST['cuo_id']);

    if ($dts['estado'] == 1) {
        $cre_id = $dts['data']['tb_credito_id'];
        $cuo_fec = mostrar_fecha($dts['data']['tb_cuota_fec']);
        $cuo_num = $dts['data']['tb_cuota_num'];
        $mon_id = $dts['data']['tb_moneda_id'];
        $mon_nom = $dts['data']['tb_moneda_nom'];
        $cuo_cuo = $dts['data']['tb_cuota_cuo'];
        $cuo_int = floatval($dts['data']['tb_cuota_int']);
        $cuo_cap = floatval($dts['data']['tb_cuota_cap']);
        $cuo_persubcuo = $dts['data']['tb_cuota_persubcuo'];
        $cuo_per = $dts['data']['tb_cuota_per']; //permiso para poder hacer pagos sobre esta cuota, 0 solo el admin, 1 tienen todos el permiso
    }
    $dts = NULL;
    $fec = $cuo_fec;

    $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
    $diasdelmes = date('t', strtotime($diasdelmes1));

    $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
    $fechahoy = date('d-m-Y'); // la fecha actual del dia
    $fechaPago = $cuo_fec; // cuando me toca pagar la cuota


    $variableopcional = "";
    if ($cuo_num == 1) {
        $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
    }
    if ($cuo_num > 1) {
        $fechaFacturacion = strtotime($fechaFacturacion);
        $fechahoy = strtotime($fechahoy);
        $fechaPago = strtotime($fechaPago);

        if ($fechahoy < $fechaFacturacion) {
            $dias = 0;
            $variableopcional = 1;
        }
        if ($fechahoy > $fechaFacturacion) {

            $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
            $variableopcional = 0;
        }
    }

    $diastranscurridos = $dias;

    //verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
    $vencida = 0;
    if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
        $vencida = 1;
    }
    $dts = $oCredito->mostrarUno($cre_id);

    if ($dts['estado'] == 1) {
        $cre_preaco = floatval($dts['data']['tb_credito_preaco']);
        $cre_fecdes = mostrar_fecha($dts['data']['tb_credito_fecdes']);
        $cuotip_id = $dts['data']['tb_cuotatipo_id']; //1 libre, 2 fija
        $cuotip_nom = $dts['data']['tb_cuotatipo_nom'];
        $cre_int = floatval($dts['data']['tb_credito_int']);
        $cre_numcuo = $dts['data']['tb_credito_numcuo'];
        $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
        $cli_id = $dts['data']['tb_cliente_id'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom = $dts['data']['tb_cliente_nom'];
        $cliente = $cli_nom . ' | ' . $cli_doc;
    }

    $result = $oVencimiento->nro_cuotas_Pendientes($cli_id, fecha_mysql($fecha_hoy), $cre_id);
    if ($result['estado'] == 1) {
        $total = $result['data']['cuotas'];
    }
    if ($total > 0) {
        $mensaje = 'USTED TIENE ' . $total . ' CUOTAS VENCIDAS NO PUEDE AMORTIZAR NI LIQUIDAR EL CRÉDITO SIN ANTES PAGAR LAS CUOTAS ANTERIRORES';
    }


    $codigo = 'CM-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    if ($cuotip_id == 2) {
        $cuota = $cuo_num . '/' . $cre_numcuo . '';
    }
    if ($cuotip_id == 1) {
        $cuota = $cuo_num . '/' . $cuo_num . ' (' . $cre_numcuomax . ')';
    }

    $pag_det = "PAGO DE CUOTA $codigo N° CUOTA: $cuota";

    $tipo_credito = 'CRÉDITO MENOR - CUOTA LIBRE';
    if ($cuotip_id == 2) {
        $tipo_credito = 'CRÉDITO MENOR - CUOTA FIJA';
    }
}

$hoy = date('d-m-Y');
$interes_pago_ant = 0;
$mon_pag_ant = 0;

if (intval($cuo_num) == 1) {
    //cuando se intenta liquidar en la primera cuota
    $dias_pasados = restaFechas($cre_fecdes, $hoy);
    $interes_pago_ant = 0;
} else {
    //cuando se quiere liquidar pero después de la primera cuota
    $dts = $oCuota->listar_ultima_cuota_pagada_pagoparcial_facturada($cre_id, 1, fecha_mysql($hoy)); //parametro 1 creditotipo c-menor
    $num_cuo_ult = count($dts);
    if ($dts['estado'] == 1) {

        $cuo_ult_fec = mostrar_fecha($dts['data']['tb_cuota_fec']);
    }
    if ($num_cuo_ult == 0)
        $cuo_ult_fec = $hoy;

    if (strtotime($cuo_ult_fec) >= strtotime($hoy))
        $dias_pasados = 0;
    else
        $dias_pasados = restaFechas($cuo_ult_fec, $hoy);
}

//echo 'fecha = '.$cuo_ult_fec.' hoyyy ==  '.$hoy; exit();

$pagos_cuota = 0;
$mod_id = 1;
$dts3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $_POST['cuo_id']);
if ($dts3['estado'] == 1) {
    foreach ($dts3['data'] as $key => $dt3) {
        $pagos_cuota += floatval($dt3['tb_ingreso_imp']);
    }
}

$pago_anticipado = 'NO APLICA 0%';
if ($interes_pago_ant > 0)
    $pago_anticipado = 'SI APLICA ' . $interes_pago_ant . '%';

$detalle_extra = 'Pago Anticipado (PA): ' . $pago_anticipado . '||Monto (PA): S/. ' . mostrar_moneda($interes_pago_ant);

//CALCULAMOS EL INTERÉS HASTA LA FECHA EN QUE SE ESTÁ LIQUIDANDO
list($cuo_dia, $cuo_mes, $cuo_anio) = explode('-', $cuo_fec);
$dias_del_mes = cal_days_in_month(CAL_GREGORIAN, $cuo_mes, $cuo_anio);

//if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
//    
//} else {
//    $cuo_int = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos));
//}





if ($cuo_num == $cre_numcuomax) {
    
} else {

    $monto_pagar = $cuo_int - $pagos_cuota;
    if ($monto_pagar <= 0){
        $monto_pagar = 0;
    }
    else{
        $monto_pagar = ($monto_pagar);
    }

    if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
        
    } else {
        if ($cuo_num > 1) {
            $monto_pagar = ($cuo_int / $diasdelmes) * $diastranscurridos;
        }
    }
}













//$suma_total = $mon_pag_ant + $cuo_int + $cuo_cap; antes pagaba un anticipado, ahora ya no
//$suma_total = $cuo_int + $cuo_cap;
$suma_total = $monto_pagar + $cuo_cap;
$total_pagar = $suma_total - $pagos_cuota;

$_SESSION['total_pagar'] = formato_moneda($total_pagar);
//$_SESSION['interes'] = formato_moneda($cuo_int);
$_SESSION['interes'] = formato_moneda($monto_pagar);

$vMax = formato_moneda($total_pagar + 100);
?>

<?php if ($total == 0) { ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimiento_liquidar" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">INFORMACIÓN DE CUOTA A PAGAR  DIAS ==<?php  echo $monto_pagar .'   ||     '.$cuo_cap  ?></h4>
                </div>

                <form id="for_cuopag">
                    <input type="hidden" name="hdd_pc_nombre" id="hdd_pc_nombre">
                    <input type="hidden" name="hdd_pc_mac" id="hdd_pc_mac">
                    <input type="hidden" name="action" id="hdd_action" value="<?php echo $action; ?>">
                    <input name="hdd_mod_id" id="hdd_mod_id" type="hidden" value="<?php echo 1; ?>">
                    <input name="hdd_modid" id="hdd_modid" type="hidden" value="<?php echo $_POST['cuo_id']; ?>">
                    <input name="hdd_cuo_id" id="hdd_cuo_id" type="hidden" value="<?php echo $_POST['cuo_id']; ?>">
                    <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo 1; ?>">
                    <input name="hdd_cli_id" id="hdd_cli_id" type="hidden" value="<?php echo $cli_id; ?>">
                    <input name="hdd_pag_det" id="hdd_pag_det" type="hidden" value="<?php echo $pag_det; ?>">
                    <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id; ?>">
                    <input type="hidden" name="hdd_cre_int" value="<?php echo $cre_int; ?>">
                    <input type="hidden" id="hdd_cuo_cuo" value="<?php echo formato_moneda($cuo_cuo); ?>">
                    <input type="hidden" id="hdd_cuo_int" value="<?php echo formato_moneda($cuo_int); ?>">
                    <input type="hidden" id="hdd_cuo_cap" value="<?php echo formato_moneda($cuo_cap); ?>">
                    <input type="hidden" name="hdd_dias_pasados" id="hdd_dias_pasados" value="<?php echo $dias_pasados; ?>">
                    <input type="hidden" name="hdd_detalle_ext" id="hdd_detalle_ext" value="<?php echo $detalle_extra; ?>">
                    <input type="hidden" name="hdd_cuo_fec" id="hdd_cuo_fec" value="<?php echo $cuo_fec; ?>">
                    <input type="hidden" id="hdd_total_pagar" value="<?php echo $_SESSION['total_pagar'];?>">

                    <div class="modal-body" style="font-family: cambria">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="text-align: right">Forma de Pago :</label>
                                    <div class="col-md-5">
                                        <select name="cmb_forma_pag" id="cmb_forma_pag" class="form-control input-sm">
                                            <option value="">--</option>
                                            <option value="1">En Oficinas</option>
                                            <option value="2">Pago en Banco</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                        <div class="col-md-7">
                            <!--<h5 style="font-weight: bold;font-family: cambria">Datos Generales</h5>-->
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="font-weight: bold;font-family: cambria;font-size: 15px">Datos Generales</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Cliente:</label>
                                                <div class="col-md-8">
                                                    <?php echo $cli_nom . ' | ' . $cli_doc; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Crédito:</label>
                                                <div class="col-md-8">
                                                    <?php echo $tipo_credito; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Código:</label>
                                                <div class="col-md-8">
                                                    <?php echo $codigo; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">N° de Cuota:</label>
                                                <div class="col-md-8">
                                                    <?php echo $cuota; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Fecha de Vencimiento:</label>
                                                <div class="col-md-8">
                                                    <?php echo $cuo_fec; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item" style="border-color: #000000">
                                            <div class="row">
                                                <!--<hr>-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Interés moratorio:</label>
                                                        <div class="col-md-1">
                                                            S/. 
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-sm" name="txt_cuopag_mor" id="txt_cuopag_mor" value="" size="12" style="text-align:right; font-size: 9pt;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--<hr>-->
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Fecha de Pago:</label>
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-sm"  name="txt_cuopag_fec" id="txt_cuopag_fec" size="10" maxlength="10" value="<?php echo date('d-m-Y') ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Días Transcurridos:</label>
                                                <div class="col-md-8">
                                                    <label id="lbl_dias_pasados"><?php echo $diastranscurridos; ?></label> días
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Interés del Crédito:</label>
                                                <div class="col-md-8">
                                                    <?php echo $cre_int . '%'; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Interés a la Fecha:</label>
                                                <div class="col-md-8">

                                                    S/. <label id="lbl_cuo_int"><?php echo mostrar_moneda($monto_pagar); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($action == 'amortizar'): ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Monto Cuota:</label>
                                                    <div class="col-md-8">
                                                        <?php echo 'S/. ' . mostrar_moneda($cuo_cuo); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Capital:</label>
                                                <div class="col-md-8">
                                                    <?php echo 'S/. ' . mostrar_moneda($cuo_cap); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item" style="border-color: #000000">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Suma Total:</label>
                                                        <div class="col-md-4">
                                                            S/. <label id="lbl_suma_total"><?php echo mostrar_moneda($suma_total); ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Pagos Previos:</label>
                                                        <div class="col-md-4">
                                                            <?php echo 'S/. ' . mostrar_moneda($pagos_cuota); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Total a Pagar:</label>
                                                <div class="col-md-1">
                                                    S/. 
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-sm moneda3" name="txt_cuopag_mon_ofi" id="txt_cuopag_mon_ofi" style="text-align:right; font-size: 9pt;" value="<?php echo mostrar_moneda($total_pagar); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <!--<h5 style="font-weight: bold;font-family: cambria">Llenar en caso sea un depósito</h5>-->
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="font-weight: bold;font-family: cambria;font-size: 15px">Llenar en caso sea un depósito</div>
                                <div class="panel-body"  id="banco">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">N° Operación:</label>
                                                <div class="col-md-6">
                                                    <input type="text"  class="form-control input-sm" name="txt_cuopag_numope" id="txt_cuopag_numope">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-md-6 control-label">Fecha Depósito:</label>
                                                <div class="col-md-6">
                                                    <div class='input-group date' id='cuota_fil_picker1'>
                                                        <input type="text" class="form-control input-sm" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" readonly autocomplete="off">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<p>-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">Fecha Validación:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control input-sm" name="txt_cuopag_fec" id="txt_cuopag_fec" value="<?php echo date('d-m-Y') ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">Monto Depósito:</label>
                                                <div class="col-md-6">
                                                    <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" size="12" maxlength="10">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">Comisión Banco:</label>
                                                <div class="col-md-6">
                                                    <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" size="12" maxlength="10" value="0.00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">Monto Validar:</label>
                                                <div class="col-md-6">
                                                    <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" size="12" maxlength="10" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">N° Cuenta:</label>
                                                <div class="col-md-6">
                                                    <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">En Banco:</label>
                                                <div class="col-md-6">
                                                    <select name="cmb_banco_id"  class="form-control input-sm mayus">
                                                        <option value="">--</option>
                                                        <option value="1" selected="true">Banco BCP</option>
                                                        <option value="2">Banco Interbak</option>
                                                        <option value="3">Banco BBVA</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 control-label">Monto Pagado:</label>
                                                <div class="col-md-6">
                                                    <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_montot" type="text" id="txt_cuopag_montot" value="0" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--</div>-->
                            <p>
                                <?php if ($action == 'amortizar'): ?>
                                    <!--<div class="col-md-5">-->
                                    <!--<h5 style="font-weight: bold;font-family: cambria">Llenar en caso sea un depósito</h5>-->
                                <div class="panel panel-primary">
                                    <div class="panel-heading" style="font-weight: bold;font-family: cambria;font-size: 15px">Datos para refinanciar el crédito</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label">Precio Acordado:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="txt_preaco_ref" id="txt_preaco_ref" class="form-control input-sm moneda3" style="text-align:right; font-size: 9pt;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label">Interés:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="txt_interes_ref" id="txt_interes_ref" class="form-control input-sm moneda3" style="text-align:right; font-size: 9pt;" value="<?php echo mostrar_moneda($cre_int); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label">Num Cuotas:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="txt_numcuo_ref" id="txt_numcuo_ref" class="form-control input-sm numero" style="text-align:right; font-size: 9pt;" value="1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                        <div id="div_credito_cronograma" class="col-md-12">
                            <div >

                            </div> 
                        </div> 
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="submit" class="btn btn-info" id="btn_for_cuopag">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vencimiento_liquidar" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">MENSAJE IMPORTANTE</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <p  style="font-family: cambria;font-size: 15px;color: #cc0000"><?php echo $mensaje; ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<script type="text/javascript" src="<?php echo 'vista/vencimiento/cuotapago_menor_form_liq.js?ver=123'; ?>"></script>