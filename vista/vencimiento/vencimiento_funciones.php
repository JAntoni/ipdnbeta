<?php
require_once('../../core/usuario_sesion.php');
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$funcion = $_POST['funcion'];

//para validar el número de operación y la fecha
if ($funcion == 'validar_operacion_fecha'){
    $fecha_deposito = fecha_mysql($_POST['fecha_deposito']);
    $num_operacion = $_POST['num_operacion'];

    $fecha_vencimiento = fecha_mysql($_POST['fecha_vencimiento']); //nos va a permitir no aumentar las fechas a meses posteriores
    $numero_cuota = $_POST['numero_cuota']; //nos va a permitir no aumentar las fechas a meses posteriores
    $fecha_vencimiento = date("Y-m-d", strtotime($fecha_vencimiento . "- 1 month"));
    $mantiene_mismo_mes = 0; //aumenta mes a mes cada pago de cuota, 1 mantiene el mismo mes para seguir creando cuotas
    
    if (intval($numero_cuota) > 1 && (strtotime($fecha_deposito) < strtotime($fecha_vencimiento)))
        $mantiene_mismo_mes = 1; //mantenemos la misma fecha para crear las cuotas

    $response['estado'] = 0;
    $response['mensaje'] = 'Verifique la fecha de depósito, el N° de Operación '.$num_operacion.' no corresponde a la fecha seleccionada: '.mostrar_fecha($fecha_deposito);
    $response['mismo_mes'] = $mantiene_mismo_mes;

    $result = $oDeposito->validar_no_operacion_fecha($num_operacion, $fecha_deposito);
        if($result['estado'] == 1){
            $response['estado'] = 1;
            $response['mensaje'] = 'N° de operación y fecha válidos';
            $response['cuentaid'] = $result['data'][0]['tb_cuentadeposito_id'];
            // $response['deposito_mon'] = $result['data'][0]['tb_deposito_mon'];
        }
    $result = NULL;
    
    echo json_encode($response);
}

//para calcular el monto mínimo a pagar, cuando se paga en fechas diferentes o puntuales
if($funcion == 'monto_minimo'){
    $fecha_deposito = $_POST['fecha_deposito'];
    $fecha_cuota_vencimiento = date("d-m-Y", strtotime($_POST['fecha_cuota_vencimiento'] . "- 1 month"));
    $diasdelmes = date('t', strtotime($fecha_cuota_vencimiento)); //DIAS DEL MES EN QUE SE GENERÓ LA CUOTA ACTUAL
    $cuota_id = $_POST['cuota_id'];
    $modulo_id = $_POST['modulo_id'];
    $mensaje = 'fec depo: '.$fecha_deposito.' /fec vencimi: '.$fecha_cuota_vencimiento.' /dias mes: '.$diasdelmes.' /cuota id: '.$cuota_id.' /modulo id: '.$modulo_id;
    
    $pagos_cuota = 0;
    $diastranscurridos = 0;

    $result = $oCuotapago->mostrar_cuotatipo_ingreso($modulo_id, $cuota_id);
        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value3) {
                $pagos_cuota += floatval($value3['tb_ingreso_imp']); // SUMATORIA DE TODOS LOS PAGOS REALIZADOS SOBRE LA CUOTA
            }
        }
    $result = NULL;

    $resultcuo = $oCuota->mostrarUno($cuota_id);
        if ($resultcuo['estado'] == 1) {
            $cuopag_tot1 = $resultcuo['data']['tb_cuota_cuo']; //MONTO TOTAL DEL MES CON TODO INTERÉS
            $cuo_int = formato_moneda($resultcuo['data']['tb_cuota_int']); //MONTO INTERES SOBRE CAPITAL
            $cuo_fec = $resultcuo['data']['tb_cuota_fec']; //FECHA DE PAGO DE LA CUOTA
            $cuo_num = $resultcuo['data']['tb_cuota_num']; //numero de cuota
        }
    $resultcuo = NULL;

    $diastranscurridos = (strtotime($fecha_deposito) - strtotime($fecha_cuota_vencimiento)) / 86400;
    
    if ($cuo_num > 1) {
        $fechaFacturacion = strtotime($fecha_cuota_vencimiento);
        $fechaPago = strtotime($fecha_deposito);
    
        if ($fechaPago < $fechaFacturacion)
            $diastranscurridos = 0;
        if ($fechaPago > $fechaFacturacion)
            $diastranscurridos = (($fechaPago) - ($fechaFacturacion)) / 86400;
    }
    
    $vencida = 0;
    if (strtotime($fecha_deposito) > strtotime($cuo_fec)) {
        $vencida = 1;
    }
    
    $pago_minimo = $cuo_int - formato_moneda($pagos_cuota);
    
    if ($vencida == 0) {
        if ($cuo_num == 1)
            $pago_minimo = ($cuo_int - formato_moneda($pagos_cuota));
        
        if ($cuo_num > 1) 
            $pago_minimo = ((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos_cuota);
    }

    $pago_minimo = formato_moneda($pago_minimo);

    $response['estado'] = 1;
    $response['valor'] = mostrar_moneda($pago_minimo);
    $response['mensaje'] = '';

    echo json_encode($response);
}

if ($_POST['action'] == 'datos_cuota') {

  $fecha_validar = $_POST['cuopag_fecval'];
  $cuota_id = $_POST['cuota_id'];

  $result = $oCuota->mostrarUno($cuota_id);
    if ($result['estado'] == 1) {
      $cre_id = $result['data']['tb_credito_id'];
      $cuo_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
      $cuo_num = $result['data']['tb_cuota_num'];
      $mon_id = $result['data']['tb_moneda_id'];
      $mon_nom = $result['data']['tb_moneda_nom'];
      $cuo_cuo = $result['data']['tb_cuota_cuo'];
      $cuo_int = floatval($result['data']['tb_cuota_int']);
      $cuo_cap = floatval($result['data']['tb_cuota_cap']);
      $cuo_persubcuo = $result['data']['tb_cuota_persubcuo'];
      $cuo_per = $result['data']['tb_cuota_per']; //permiso para poder hacer pagos sobre esta cuota, 0 solo el admin, 1 tienen todos el permiso
    }
  $result = NULL;

  //verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
  $vencida = 0;
  if (strtotime($fecha_validar) > strtotime($cuo_fec))
    $vencida = 1;

  $result = $oCredito->mostrarUno($cre_id);
    if ($result['estado'] == 1) {
      $cre_preaco = floatval($result['data']['tb_credito_preaco']);
      $cre_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
      $cuotip_id = $result['data']['tb_cuotatipo_id']; //1 libre, 2 fija
      $cuotip_nom = $result['data']['tb_cuotatipo_nom'];
      $cre_int = floatval($result['data']['tb_credito_int']);
      $cre_numcuo = $result['data']['tb_credito_numcuo'];
      $cre_numcuomax = $result['data']['tb_credito_numcuomax'];
      $cli_id = $result['data']['tb_cliente_id'];
      $cli_doc = $result['data']['tb_cliente_doc'];
      $cli_nom = $result['data']['tb_cliente_nom'];
      $cliente = $cli_nom . ' | ' . $cli_doc;
    }
  $result = NULL;

    $interes_pago_ant = 0;
    $mon_pag_ant = 0;
    $cuota_fec_menos = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando empieza el día uno de interés
    list($cuo_dia, $cuo_mes, $cuo_anio) = explode('-', $cuota_fec_menos);
    $dias_del_mes = cal_days_in_month(CAL_GREGORIAN, $cuo_mes, $cuo_anio);

    if (intval($cuo_num) == 1) {
      //cuando se intenta liquidar en la primera cuota
      $dias_pasados = restaFechas($cre_fecdes, $fecha_validar);
      $interes_pago_ant = 0;
    }
    else {
      $fecha_cuota_vencimiento = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
      $dias_pasados = (strtotime($fecha_validar) - strtotime($fecha_cuota_vencimiento)) / 86400;

      if ($dias_pasados < $dias_del_mes) {
        $cuo_int = floatval($dias_pasados * ($cuo_int / $dias_del_mes));
      }
    }

    $pagos_cuota = 0;
    $mod_id = 1;
    $result3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
      if ($result3['estado'] == 1) {
        foreach ($result3['data']as $key => $value3) {
          $pagos_cuota += floatval($value3['tb_ingreso_imp']);
        }
      }
    $result3 = NULL;

    // $pago_anticipado = 'NO APLICA 0%';
    // if ($interes_pago_ant > 0)
    //   $pago_anticipado = 'SI APLICA ' . $interes_pago_ant . '%';

    // $detalle_extra = 'Pago Anticipado (PA): ' . $pago_anticipado . '||Monto (PA): S/. ' . mostrar_moneda($interes_pago_ant);
    
    // VALIDAMOS QUE LAS FECHAS PASADAS NO SEAN MENORES A 0 PARA EVITAR EL CÁLCULO EN NEGATIVO
    if($dias_pasados <= 0){
      $dias_pasados = 0;
      $cuo_int = 0;
    }
    //CALCULAMOS EL INTERÉS HASTA LA FECHA EN QUE SE ESTÁ LIQUIDANDO
    $suma_total = $mon_pag_ant + $cuo_int + $cuo_cap;
    $total_pagar = $suma_total - $pagos_cuota;

    $_SESSION['total_pagar'] = moneda_mysql($total_pagar);
    $_SESSION['interes'] = moneda_mysql($cuo_int);

    $data['dias_pasados'] = $dias_pasados;
    $data['pago_anticipado'] = $pago_anticipado;
    $data['mon_pag_ant'] = mostrar_moneda($mon_pag_ant);
    $data['cuo_int'] = mostrar_moneda($cuo_int);
    $data['suma_total'] = mostrar_moneda($suma_total);
    $data['total_pagar'] = mostrar_moneda($total_pagar);
    $data['total_pagar_v2'] = formato_numero($total_pagar);
    $data['detalle_extra'] = $detalle_extra;

    echo json_encode($data);
}
?>