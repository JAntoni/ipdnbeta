<?php
if (defined('APP_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}
require_once("Vencimiento.class.php");
$oPagocuota = new Vencimiento();
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once("../creditomenor/Creditomenor.class.php");
$oCredito = new Creditomenor();
require_once("../garantia/Garantia.class.php");
$oGarantia = new Garantia();

$fecha_hoy = date('d-m-Y');

/* FILTROPARA CREDITO MENORES */

$bloqueobtns = '';
if( isset($_POST['vista']) && $_POST['vista'] === 'reportecreditomejorado' ) { $bloqueobtns = 'disabled'; }

$result = $oPagocuota->filtrar_menor($_POST['hdd_fil_cli_id'], $cre_id_cm, fecha_mysql($fecha_hoy));
  if ($result['estado'] == 1): ?>
    <h3 style="color: #000" class="subtitulo_pago">CRÉDITO MENOR</h3>
    <table cellspacing="1" class="table table-hover">
      <thead>
        <tr id="tabla_cabecera">
          <th id="tabla_cabecera_fila">CLIENTE</th>
          <th id="tabla_cabecera_fila">TIPO</th>
          <th id="tabla_cabecera_fila">N° CUOTA</th>
          <th id="tabla_cabecera_fila">VENCIMIENTO</th>
          <!-- GERSON (14-03-23) -->
          <th id="tabla_cabecera_fila">CAPITAL</th>
          <th id="tabla_cabecera_fila">CUOTA</th>
          <th id="tabla_cabecera_fila">PAGO MÍNIMO</th>
          <!--  -->
          <th id="tabla_cabecera_fila">PAGOS</th>
          <th id="tabla_cabecera_fila">SALDO</th>
          <th id="tabla_cabecera_fila">ESTADO</th>
          <th id="tabla_cabecera_fila"></th>
          <th id="tabla_cabecera_fila">CRÉDITO</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($result['data'] as $key => $value) {
          $contar = 0;
          $vencida = 1;
          $tipo = $value['tb_cuotatipo_id'];

          $cuo_cuo = $value['tb_cuota_cuo'];
          $cuo_cap = $value['tb_cuota_cap'];
          $cuo_int = $value['tb_cuota_int'];
          $cuo_num = $value['tb_cuota_num'];
          $cre_numcuomax = $value['tb_credito_numcuomax'];
          $cuo_fec = mostrar_fecha($value['tb_cuota_fec']);

          if (strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])) {
            $vencida = 0;
          }
          $credito_estado = '';
          $clase_fila = 'class="even"';
          if(intval($value['tb_credito_est']) == 5){
            $credito_estado = '<span class="badge bg-red">Crédito en Remate</span>';
            $clase_fila = 'class="danger"';
          }
          ?>
          <tr id="tabla_fila" <?php echo $clase_fila;?> >
            <td id="tabla_fila"><?php echo $value['tb_cliente_nom'] . " | " . $value['tb_cliente_doc']; ?></td>
            <td id="tabla_fila"><?php echo $value['tb_cuotatipo_nom'] ?></td>
            <td align="center" id="tabla_fila">
              <?php
              if ($value['tb_cuotatipo_id'] == 2) {
                echo $value['tb_cuota_num'] . '/' . $value['tb_credito_numcuo'];
              } else {
                echo $value['tb_cuota_num'] . '/' . $value['tb_credito_numcuomax'];
              }
              ?>
            </td>
            <td align="center" id="tabla_fila" <?php if ($vencida == 1) { echo 'style="color:red;font-weight:bold;"'; } ?> >
              <?php echo mostrar_fecha($value['tb_cuota_fec']) ?>
            </td>
            <?php
            $pagos_cuota = 0;
            $mod_id = 1;

            $values3 = $oCuotapago->mostrar_cuotapago_ingresos_por_tipo_cuota($mod_id, $value['tb_cuota_id']);
              if ($values3['estado'] == 1) {
                foreach ($values3['data'] as $key => $values) {
                  $pagos_cuota += $values['tb_ingreso_imp'];
                }
              }
            $values3 = NULL;

            $saldo = $value['tb_cuota_cuo'] - $pagos_cuota;

            /* GERSON (13-03-2023) */
            $cuotip_id = $value['tb_cuotatipo_id'];
            $monto_pagar = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos);

            /* NUEVO */
            $pagos_cuota = 0;
            $mod_id = 1;
            $dts3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value['tb_cuota_id']);
              if ($dts3['estado'] == 1) {
                foreach ($dts3['data'] as $key => $dt3) {
                  $pagos_cuota += floatval($dt3['tb_ingreso_imp']);
                }
              }
            $dt3 = NULL;
            
            $pagos = $pagos_cuota;
            $pagos_parciales = mostrar_moneda($pagos);

            $diasdelmes1 = date("d-m-Y", strtotime($cuo_fec . "- 1 month"));
            $diasdelmes = date('t', strtotime($diasdelmes1));

            $fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
            $fechahoy = date('d-m-Y'); // la fecha actual del dia
            $fechaPago = $cuo_fec; // cuando me toca pagar la cuota

            $variableopcional = "";
            if ($cuo_num == 1) {
              $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
            }
            if ($cuo_num > 1) {
              $fechaFacturacion = strtotime($fechaFacturacion);
              $fechahoy = strtotime($fechahoy);
              $fechaPago = strtotime($fechaPago);

              if ($fechahoy < $fechaFacturacion) {
                $dias = 0;
                $variableopcional = 1;
              }
              if ($fechahoy > $fechaFacturacion) {

                $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
                $variableopcional = 0;
              }
            }

            $diastranscurridos = $dias;
            $monto_pagar = formato_moneda($cuo_cuo) - formato_moneda($pagos);

            if ($cuotip_id == 1):

              if ($cuo_num == $cre_numcuomax) {
              } 
              else {

                $monto_pagar = $cuo_int - $pagos;
                if ($monto_pagar <= 0)
                  $monto_pagar = 0;
                else
                  $monto_pagar = mostrar_moneda($monto_pagar);

                if (strtotime($fecha_hoy) > strtotime(mostrar_fecha($value['tb_cuota_fec']))) {
                } else {
                  if ($cuo_num > 1) {
                    $monto_pagar = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
                  }
                }
              }
            endif;
            /*  */
            ?>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($cuo_cap); ?></td>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($cuo_int); ?></td>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($monto_pagar); ?></td>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($pagos_cuota); ?></td>
            <td align="right" style="padding-right:10px;" id="tabla_fila"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($saldo); ?></td>
            <td id="tabla_fila">
              <?php
              $estado = "";
              if ($value['tb_cuota_est'] == 1) {
                $estado = 'POR COBRAR';
              }
              if ($value['tb_cuota_est'] == 2) {
                $estado = 'CANCELADA';
              }
              if ($value['tb_cuota_est'] == 3) {
                $estado = 'PAGO PARCIAL';
              }
              /* GERSON (20-03-23) */
              $cant_garantias = 0;
              $gar_vendidas = 0;

              $garantias = $oGarantia->filtrar_oro($value['tb_credito_id'], 1);
                if ($garantias['estado'] == 1) {
                  $cant_garantias = count($garantias['data']);
                  foreach ($garantias['data'] as $key => $gar) {
                    if ($gar['tb_garantia_est'] == 1) {
                      // garantias vendidas
                      $gar_vendidas++;
                    }
                  }
                }
              $garantias = NULL;

              if ($gar_vendidas > 0) {
                echo $estado . " <span style='color:red'><strong>(Vendido: " . $gar_vendidas . "/" . ($cant_garantias) . ")</strong></span>";
              } else {
                echo $estado;
              }
              /*  */
              if ($value['tb_cuotatipo_id'] == 1) {
                $action = 'pagar_libre';
              }
              if ($value['tb_cuotatipo_id'] == 2) {
                $action = 'pagar_fijo';
              }
              ?>
            </td>
            <td align="left" id="tabla_fila">
              <?php
                if(intval($value['tb_credito_est']) != 5):
                  if ($value['tb_cuota_est'] != 2) { ?>
                    <a class="btn bg-light-blue btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="notascobranza_cuotaid(<?php echo $value["tb_cuota_id"]; ?>)" title="Ver notas de cobranza"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a>

                    <a class="btn btn-success btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="cuotapago_menor_form('<?php echo $action ?>', '<?php echo $value['tb_cuota_id'] ?>')" title="Pagar Cuota"><i class="fa fa-money" aria-hidden="true"></i></a>
                  <?php } ?>

                  <?php if (strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])) { 
                    if ($value['tb_cuotatipo_id'] == 2) :
                      ?>
                      <a class="btn btn-success btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="calcular_liqui_menor_form('<?php echo $value['tb_credito_id'] ?>')" title="Calcular Liquidacion"><i class="fa fa-dollar" aria-hidden="true"></i></a>
                      <?php
                    endif;
                    ?>
                    <a class="btn btn-danger btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="cuotapago_menor_form_liq('<?php echo $action ?>', '<?php echo $value['tb_cuota_id'] ?>', 1)" title="Liquidar Crédito"><i class="fa fa-ban" aria-hidden="true"></i></a>
                  <?php } ?>

                  <?php
                  if ($value['tb_cuotatipo_id'] == 2) :
                    if (strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])) {
                    ?>
                      <a class="btn btn-info btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="cuotapago_menor_form_liq('<?php echo $action ?>', '<?php echo $value['tb_cuota_id'] ?>', 2)" title="Amortizar Crédito"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                    <?php } ?>

                    <?php 
                  endif;
                endif;
                if ($value['tb_cuotatipo_id'] == 1) :
                  ?>
                  <a class="btn btn-success btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="calcular_liqui_menor_form('<?php echo $value['tb_credito_id'] ?>')" title="Calcular Liquidacion"><i class="fa fa-dollar" aria-hidden="true"></i></a>
                  <?php
                endif;
                if(intval($value['tb_credito_est']) == 5)
                  echo $credito_estado;
                ?>
            </td>
            <td align="center" id="tabla_fila">
              <a class="btn btn-primary btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="creditomenor_pagos('<?php echo $value['tb_credito_id'] ?>', 1)" title="HISTORIAL DE PAGOS"><i class="fa fa-credit-card" aria-hidden="true"></i></a>
              <a class="btn btn-warning btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="creditomenor_form('L',<?php echo $value['tb_credito_id']; ?>)">CM-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT); ?></a>
              <?php if ($value['tb_cuotatipo_id'] == 1) { // AHORA TODOS PUEDEN RENOVAR  
              ?>
                <a href="javascript:void(0)" class="btn btn-facebook btn-xs" <?php echo $bloqueobtns;?> onclick="agregar_cuotas('<?php echo $value['tb_credito_id'] ?>')" title="Agregar 2 cuotas"><i class="fa fa-plus-square" aria-hidden="true"></i></a>
              <?php } ?>
            </td>
          </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
    <?php 
    else:
      ?>
      <h3 style="color: #000" class="subtitulo_pago">CRÉDITO MENOR</h3>
      <table cellspacing="1" class="table table-hover">
      <thead>
        <tr id="tabla_cabecera">
          <th id="tabla_cabecera_fila">CLIENTE</th>
          <th id="tabla_cabecera_fila">TIPO</th>
          <th id="tabla_cabecera_fila">N° CUOTA</th>
          <th id="tabla_cabecera_fila">VENCIMIENTO</th>
          <!-- GERSON (14-03-23) -->
          <th id="tabla_cabecera_fila">CAPITAL</th>
          <th id="tabla_cabecera_fila">CUOTA</th>
          <th id="tabla_cabecera_fila">PAGO MÍNIMO</th>
          <!--  -->
          <th id="tabla_cabecera_fila">PAGOS</th>
          <th id="tabla_cabecera_fila">SALDO</th>
          <th id="tabla_cabecera_fila">ESTADO</th>
          <th id="tabla_cabecera_fila"></th>
          <th id="tabla_cabecera_fila">CRÉDITO</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <td colspan="12" class="text-center align-middle "><i>(Sin datos)</i></td>
        </tr>
      </tbody>
      </table>
      <?php 
  endif;
$result = null;  
?>



<?php include_once 'vencimiento_tabla_GarVeh.php'; ?>