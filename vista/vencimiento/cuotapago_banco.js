/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#notas").hide();
    mostrar_nota_gc();

    if($("#hdd_tipo_credito").val()=='1'){
        cambio_moneda("compra");
    }else if($("#hdd_tipo_credito").val()=='3'){
        cambio_moneda("venta");
    }
    
    obtener_morasugerida()
    
    //console.log('aaa 33');

    if($("#hdd_vencida").val()==1 && $("#hdd_morasugerida").val()==0 ){
        alerta_warning("NO SE HA REGISTRADO MORA SUGERIDA");
    }

    mostrar_notas()

    $('.coment').hide();

    cmb_cuedep_id(0);

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    }).on('changeDate', function(selected){
        if($("#hdd_tipo_credito").val()=='1'){
            cambio_moneda("compra");
        }else if($("#hdd_tipo_credito").val()=='3'){
            cambio_moneda("venta");
        }
        //calcular_mora(); la mora será a criterio ahora
        var cuotatip_id = parseInt($('#hdd_cuotatip_id').val())
        if(cuotatip_id == 1) //calcular el mínimo siempre y cuando sea cuotas libres, las fijas quedan tal cual
            calcular_monto_minimo();
        //verificar si el n° de operación existe en la fecha indicada
        validar_no_operacion_fecha();
        obtener_morasugerida();
    });

    $('#ingreso_fil_picker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $('.moneda3').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999.99'
    });
    $('.moneda4').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });

    var pc_nom = $('#hdd_vista_pc_nombre').val();
    var pc_mac = $('#hdd_vista_pc_mac').val();
    $('#hdd_pc_nombre').val(pc_nom);
    $('#hdd_pc_mac').val(pc_mac);


    
    //calcular_mora(); la mora ahora se asigna a criterio

    /*$( "#txt_cuopag_mor" ).blur(function(e) {
        let hddmorasugerida = parseFloat($("#hdd_morasugerida").val());
        let moraingresada = parseFloat($('#txt_cuopag_mor').autoNumeric('get'));
        if(moraingresada > hddmorasugerida && hddmorasugerida>0){
            alerta_warning("NO PUEDE INGRESAR UNA MORA MAYOR A LA SUGERIDA");
            $('#txt_cuopag_mor').val(hddmorasugerida.toFixed(2));
        }
    })*/
    
    $("#chk_mor_aut").change(function () {
        if ($('#chk_mor_aut').is(':checked'))
        {
            $('#hdd_chk_mor_aut').val(1);
            $("#txt_cuopag_mor").prop('readonly', false);
            $('.coment').show();
        } else {
            let hddmorasugerida = parseFloat($("#hdd_morasugerida").val());
            $('#txt_cuopag_mor').val(hddmorasugerida.toFixed(2));
            $('#hdd_chk_mor_aut').val(0);
            $("#txt_cuopag_mor").prop('readonly', true);
            $('.coment').hide();
        }
    });

    $('#txt_cuopag_mon, #txt_cuopag_comi').change(function (event) {
        var monto_deposito = Number($('#txt_cuopag_mon').autoNumeric('get'));
        var monto_comision = Number($('#txt_cuopag_comi').autoNumeric('get'));
        var cuenta_deposito = $('#cmb_cuedep_id').val();
        var saldo = 0;

        saldo = monto_deposito - monto_comision;

        if (monto_deposito > 0 && monto_comision >= 0 && saldo > 0) {
            $('#txt_cuopag_montot').autoNumeric('set', saldo.toFixed(2));
            $('#txt_cuopag_monval').autoNumeric('set', saldo.toFixed(2));
        } else {
            $('#txt_cuopag_montot').autoNumeric('set', 0);
            $('#txt_cuopag_monval').autoNumeric('set', 0);
        }

        if (cuenta_deposito)
            $('#cmb_cuedep_id').change();
    });

    $('#cmb_cuedep_id').change(function (event) {
        //console.log("estoy entrando al sistema select ");
        var moneda_sel = $(this).find(':selected').data('moneda'); //este tipo de moneda corresponde al tipo de cuenta seleccionada
        var moneda_id = $('#hdd_mon_id').val(); //esta moneda pertenece al credito
        var monto_validar = Number($('#txt_cuopag_monval').autoNumeric('get')); //es el restanto de lo depositado menos la comision que cobra el banco
        var monto_cambio = 0; //monto del tipo de cambio del día del deposito
        var total_pagado = 0;
        if (parseInt(moneda_sel) == 2 && parseInt(moneda_id) == 1) {
            cambio_moneda("compra");
            monto_cambio = Number($('#txt_cuopag_tipcam').val());
            total_pagado = parseFloat(monto_validar * monto_cambio);
            $('#txt_cuopag_montot').autoNumeric('set', total_pagado);
            console.log('dolar a soles monto validar: ' + monto_validar + ' cambio: ' + monto_cambio);
        } else if (parseInt(moneda_sel) == 1 && parseInt(moneda_id) == 2) {
            cambio_moneda("venta");
            monto_cambio = Number($('#txt_cuopag_tipcam').val());
            total_pagado = parseFloat(monto_validar / monto_cambio);
            $('#txt_cuopag_montot').autoNumeric('set', total_pagado);
            console.log('soles a dolares monto validar: ' + monto_validar + ' cambio: ' + monto_cambio);
        } else
            $('#txt_cuopag_montot').autoNumeric('set', monto_validar);

        var cuenta = $(this).find("option:selected").text();
        $('#hdd_cuenta_dep').val(cuenta);
        $('#hdd_mon_iddep').val(moneda_sel);
    });


    $('#txt_cuopag_mon').keypress(function(event) {
        if (event.keyCode === 13) {
            event.preventDefault(); // Evita que el evento "Enter" se propague
            $(this).change()
        }
    });

});

function validar_no_operacion_fecha() {

    var fecha_deposito = $('#txt_cuopag_fecdep').val();
    var num_operacion = $('#txt_cuopag_numope').val();
    var fecha_vencimiento = $('#hidden_cuodet_fec').val();
    var numero_cuota = $('#hdd_cuo_num').val()
    if(num_operacion == ""){
        swal_warning('AVISO', "DEBE REGISTRAR NUMERO DE OPERACION", 6000);
        $('#txt_cuopag_fecdep').val('')
        return false;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/vencimiento_funciones.php",
        async: true,
        dataType: "json",
        data: ({
            funcion: "validar_operacion_fecha",
            fecha_deposito: fecha_deposito,
            num_operacion: num_operacion,
            fecha_vencimiento: fecha_vencimiento,
            numero_cuota: numero_cuota
        }),
        success: function (data) {
            console.log(data);
            if(data.estado == 0){
                swal_warning('AVISO', data.mensaje, 6000);
                $('#txt_cuopag_fecdep').val('')
            }
            else {
                notificacion_success('N° operación y fecha correctos', 6000)
                $('#cmb_cuedep_id').val(data.cuentaid).change();
            }
            
            $('#hdd_var_op').val(data.mismo_mes) //nos permite mantener la misma fecha para crear las cuotas

        },
        complete: function (data) {
            console.log(data)
        }
    });
}

function cambio_moneda(tipo){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"monedacambio/monedacambio_controller.php",
            async:false,
            dataType: "json",
            data: ({
                    action: 'obtener_cambio',
                    fecha:	$('#txt_cuopag_fecdep').val(),
                    tipo:	tipo
            }),
            beforeSend: function(){
                    $('#msj_pagobanco').hide(100);
            },
            success: function(data){
                console.log(data);
                    if(data.estado == 0){
                            $('#txt_cuopag_tipcam').val('');
                            $('#msj_pagobanco').show(100);
                            $('#msj_pagobanco').html('Por favor registre el tipo de cambio del día de la fecha de depósito');
                            swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
                    }
                    else
                            $('#txt_cuopag_tipcam').val(data.moncam_val);	
            },
            complete: function(data){			
//                 console.log(data);
            }
        });		
}

function cmb_cuedep_id(ids)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuentadeposito/cuentadeposito_select.php",
        async: false,
        dataType: "html",
        data: ({
            cuedep_id: ids
        }),
        beforeSend: function () {
            $('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_cuedep_id').html(html);
        }
    });
}

function calcular_mora() {

    var url_mora = $('#hidden_url_mora').val();

    var cuodet_fec = $('#hidden_cuodet_fec').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/" + url_mora,
        async: true,
        dataType: "json",
        data: ({
            action: "calcular_mora",
            fechaPago: $('#txt_cuopag_fecdep').val(),
            fecha: cuodet_fec
        }),
        success: function (data) {
            $('#txt_cuopag_mor').val(data.mora);
            if (data.estado != 1) {
                swal_warning('AVISO MORA ??', data.msj, 6000);
            }
        },
        complete: function () {
        }
    });
}

function calcular_monto_minimo() {

    var fecha_deposito = $('#txt_cuopag_fecdep').val();
    var fecha_cuota_vencimiento = $('#hidden_cuodet_fec').val();
    
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/vencimiento_funciones.php",
        async: true,
        dataType: "json",
        data: ({
            funcion: "monto_minimo",
            fecha_deposito: fecha_deposito,
            fecha_cuota_vencimiento: fecha_cuota_vencimiento,
            cuota_id: $('#hdd_cuo_id').val(),
            modulo_id: 1 //1 para cuota
        }),
        success: function (data) {
            if(data.estado == 1){
                $('#txt_cuopag_tot').val(data.valor)
                $('#txt_cuopag_min').val(data.valor)
                notificacion_success('Se calculó el pago mínimo del cliente', 6000)
            }
            else
                swal_error('AVISO', data, 6000);
        },
        complete: function (data) {
            //console.log(data)
        }
    });
}


function Activar_input_monto_pago_banco() {
    $('#txt_cuopag_mon').attr("readonly", false);
}


function generar_nueva_cuota() {
    var vencida = $('#hdd_vencida').val();
    var cuo_num = $('#hdd_cuo_num').val();
    var opc = $('#hdd_var_op').val();
    var fecha;
    
    if (vencida == 1 || cuo_num == 1) {
        fecha = $('#hidden_cuodet_fec').val();
    } 
    else {
        if (opc == 1) {
            fecha = $('#hidden_cuodet_fec').val();
        } else {
            fecha = $('#txt_cuopag_fecdep').val();
        }
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/cuotapago_menor_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: "cuota_nueva",
            cre_id: $('#hdd_cre_id').val(),
            cuo_id: $('#hdd_cuo_id').val(),
            int: $('#hdd_cre_int').val(),
            cuo_max: $('#hdd_cuo_max').val(),
            fecha: fecha,
            opc: opc, //para evitar que al crear cuotas aumente los meses sino se mantenga el mes, cuando se pague cuotas antes de tiempo
            mon_id: $('#hdd_mon_id').val()
        }),
        beforeSend: function () {
            $('#msj_vencimiento').html("Guardando...");
            $('#msj_vencimiento').show(100);
        },
        success: function (data) {
            if (data.estado == 1) {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_success('SISTEMA', data.msj, 2000);
                $('#msj_vencimiento').hide();
            } else if (data.estado == 2) {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_warning('AVISO', data.msj, 5000);
                $('#msj_vencimiento').hide();
            } else {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_warning('AVISO', 'Error al momento de generar nueva cuota', 5000);
            }
        },
        complete: function (data) {
            if (data.statusText != 'success')
                console.log(data);
        }
    });
}

function generar_nueva_cuota_garveh() {
    var vencida = $('#hdd_vencida').val();
    var fecha;
    if (vencida == 1) {
        fecha = $('#hidden_cuodet_fec').val();
    } else {
        fecha = $('#txt_cuopag_fec').val();
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/cuotapago_garveh_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: "cuota_nueva",
            cuodet_id: $('#hdd_modid').val(),
            cre_id: $('#hdd_cre_id').val(),
            cuo_id: $('#hdd_cuo_id').val(),
            int: $('#hdd_cre_int').val(),
            cuo_max: $('#hdd_cuo_max').val(),
            fecha: fecha,
            mon_id: $('#hdd_mon_id').val()
        }),
        beforeSend: function () {
            $('#msj_vencimiento').html("Guardando...");
            $('#msj_vencimiento').show(100);
        },
        success: function (data) {
            if (data.estado == 1) {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_success('SISTEMA', data.msj, 2000);
                $('#msj_vencimiento').hide();
//        var tipo=$("#action_cuotapago").val();
//            if(tipo=="pagar_fijo")
//            vencimiento_tabla();
            } else if (data.estado == 2) {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_warning('AVISO', data.msj, 5000);
                $('#msj_vencimiento').hide();
            } else {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_warning('AVISO', 'Error al momento de generar nueva cuota', 5000);
            }
        },
        complete: function (data) {
            console.log(data);
            if (data.statusText != 'success')
                console.log(data);
        }
    });
}

function notas_his(cliente_id, credito_id, creditotiponotas, subcreditonotas){
    //alert("MOVER DISEÑO");
    console.log(subcreditonotas);
	$.ajax({
		type: "POST",
		url: VISTA_URL + "cliente/clientenota_timeline.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: 'historial',
			cliente_id:	cliente_id,
			credito_id:	credito_id,
			creditotiponotas:	creditotiponotas,
			subcreditonotas:	subcreditonotas,
		}),
		beforeSend: function() {
			//$('#creditogarveh_mensaje_tbl').hide();
			//$('#div_credito_his').dialog("open");
			//$('#div_credito_his').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
      console.log(html);
			$('#div_creditogarveh_his').html(html);
      $('#modal_notas_timeline').modal('show');
      modal_hidden_bs_modal('modal_notas_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_notas_timeline'); //funcion encontrada en public/js/generales.js
		}
	});
}

$("#for_cuopag").validate({
    submitHandler: function () {

        $.ajax({
            type: "POST",
            url: VISTA_URL + "vencimiento/cuotapago_banco_reg.php",
            async: true,
            dataType: "json",
            data: $("#for_cuopag").serialize(),
            beforeSend: function () {
            },
            success: function (data) {

                if (parseInt(data.cuopag_id) > 0) {
                    $('#modal_registro_vencimientomenorbanco').modal('hide');
                    //if(confirm('¿Desea imprimir?'))
                    //cuotapago_imppos_datos_2(data.cuopag_id);
                    
                    var hdd_cuotip_id = $('#hdd_cuotip_id').val();
                    var cuota_nueva = parseInt(data.cuota_nueva);// 1 cuota nueva, 0 ya no se crea cuota
                    if (cuota_nueva == 1) {
                        generar_nueva_cuota();
                    }
                    if (hdd_cuotip_id == 3) {
                        generar_nueva_cuota_garveh();
                    }
                    
                    var hdd_vista = $('#hdd_vista').val();
                    if (hdd_vista == 'vencimiento_tabla') {
                        vencimiento_tabla();
                    }
                    swal_success('Sistema', data.cuopag_msj, 3000);
                    if(parseInt(data.mod_id)==1 && parseInt(data.cuota_id)>0){
                        setTimeout(function() {
                            //vencimiento_tabla();
                            morapago_menor_form(data.action_cuotapago,data.cuota_id);
                        }, 3000); // 3000 milisegundos = 3 segundos de retraso
                    }else if(parseInt(data.mod_id)==2 && parseInt(data.cuota_id)>0){
                        setTimeout(function() {
                            //vencimiento_tabla();
                            morapago_garveh_form(data.action_cuotapago,data.cuota_id);
                        }, 3000); // 3000 milisegundos = 3 segundos de retraso
                    }else{
                        vencimiento_tabla();
                    }

                    var codigo = data.cuopag_id;
                    window.open("http://www.ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id=" + codigo);

                    var acu_id = Number($('#hdd_cuodet_acupag_id').val());
                    if (acu_id > 0)
                        cuotapago_asiveh_form('pagar', acu_id);
                } else {
                    swal_warning('AVISO', data.cuopag_msj, 10000);
                }
            },
            complete: function (data) {
                console.log(data);
                if (data.statusText != "success" || data.statusText != "OK") {
                //$('#btn_guar_cuopag').show();
                //$('#msj_pagobanco').show(100);
                //$('#msj_pagobanco').text('ERROR: ' + data.responseText);
                //swal_error('ERROR',data.responseText,10000);
                //console.log(data);
                }
            }

        });
    },
    rules: {
        txt_cuopag_mon: {
            required: true
        },
        txt_cuopag_tipcam: {
            required: true
        },
        txt_cuopag_numope: {
            required: true
        },
        txt_cuopag_comi: {
            required: true
        },
        txt_cuopag_monval: {
            required: true
        },
        cmb_cuedep_id: {
            required: true
        },
        txt_cuopag_montot: {
            required: true
        },
        txt_cuopag_fecdep:{
            required: true
        }
    },
    messages: {
        txt_cuopag_mon: {
            required: '* registre el monto pagado'
        },
        txt_cuopag_tipcam: {
            required: '* registre el tipo de cambio'
        },
        txt_cuopag_numope: {
            required: '* registre el numero de operacion'
        },
        txt_cuopag_comi: {
            required: '* registre la comisión del banco'
        },
        txt_cuopag_monval: {
            required: '* registre el monto a validar'
        },
        cmb_cuedep_id: {
            required: '* registre la cuenta de deposito'
        },
        txt_cuopag_montot: {
            required: '* registre el monto total'
        },
        txt_cuopag_fecdep:{
            required: '* registra fecha de depósito'
        }
    }
});

/***MORA PAGOS**/

function morapago_garveh_form(act,idf){
    console.log(act,idf);
    
    Swal.close();
    Swal.fire({
      title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO DE LA MORA EL CLIENTE',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
      showDenyButton: true,
      denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
      confirmButtonColor: '#3b5998', 
      denyButtonColor: '#21ba45',
    }).then((result) => {
  
      if (result.isConfirmed) {
        Swal.fire(
          '¡Has elegido pagar en Oficina!',
          '',
          'success'
        )
        cuotamorapago_garveh_form(act,idf);
      } else if (result.isDenied) {
        Swal.fire(
          '¡Has elegido pagar en Banco!',
          '',
          'success'
        )
        morapago_banco('garveh', idf);
  
      }
    });
  }

  function cuotamorapago_garveh_form(cred, idf){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"cobranzamora/morapago_garveh_form.php",
      async:true,
      dataType: "html",                    
      data: ({
          action: cred,
          cuodet_id:	idf,
          vista:	'pago_cuota'
      }),
      beforeSend: function() {
          
      },
      success: function(html){
          $('#modal_mensaje').modal('hide');
          $('#div_modal_cobranzamora').html(html);
          $('#modal_registro_cobranzamora').modal('show');
          modal_height_auto('modal_registro_cobranzamora');
          modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
      },
      complete: function (html) {
  //        console.log(html);
      }
    });
  }

  function morapago_banco(cred, idf){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"cobranzamora/morapago_banco.php",
      async:true,
      dataType: "html",                    
      data: ({
        credito: cred,
        cuota_id: idf,
        vista: 'cobranza_mora'
      }),
      beforeSend: function() {
          
      },
      success: function(html){
          $('#modal_mensaje').modal('hide');
          $('#div_modal_cobranzamora').html(html);
          $('#modal_registro_cobranzamora').modal('show');
          modal_height_auto('modal_registro_cobranzamora');
          modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
      },
      complete: function (html) {
  //        console.log(html);
      }
    });
  }

  function morapago_menor_form(act,idf){

    Swal.fire({
      title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
      showDenyButton: true,
      denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
      confirmButtonColor: '#3b5998', 
      denyButtonColor: '#21ba45',
    }).then((result) => {
  
      if (result.isConfirmed) {
        Swal.fire(
          '¡Has elegido pagar en Oficina!',
          '',
          'success'
        )
        cuotamorapago_menor_form(act,idf);
      } else if (result.isDenied) {
        Swal.fire(
          '¡Has elegido pagar en Banco!',
          '',
          'success'
        )
        morapago_banco('menor', idf);

      }
    });
}


function cuotamorapago_menor_form(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_menor_form.php",
    async:true,
    dataType: "html",                    
    data: ({
        action: cred,
        cuo_id:	idf,
        vista:	'vencimiento_tabla'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function obtener_morasugerida() {
    let fecha_pago;
    if($('#txt_cuopag_fecdep').val() == ''){
        fecha_pago = $('#hdd_fec_hoy').val();
    }else{
        fecha_pago = $('#txt_cuopag_fecdep').val();
    }
    console.log(fecha_pago, $('#hdd_fec_ven').val());
    $.ajax({
        type: "POST",
        url: VISTA_URL + "morasugerida/morasugerida_controller.php",
        async: false,
        dataType: "json",
        data: ({
            action_morasugerida: "obtener",
            monto: $('#hdd_monto').val(),
            fecha_pago:  fecha_pago,
            cuota_fecha: $('#hdd_fec_ven').val(),
            tipo_credito: $('#hdd_tipo_credito').val(),
            tipo_camnbio: $('#hdd_tipcam').val(),
            moneda_id: $('#hdd_mon_id').val()
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            $('#hdd_morasugerida').val(data.morasugerida);
            $('#txt_cuopag_mor').val(data.morasugerida);
        },
        complete: function () {

        }
    });
}

function ocultar_nota(notacliente_id) {
    console.log(notacliente_id);
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "update_state",
            nota_id: notacliente_id,
            estado: 0,
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                alerta_success("CORRECTO", data.clientenota_msj)
                mostrar_notas()
            }else{
                alerta_error("ERROR", data.clientenota_msj);
            }
        },
        complete: function () {

        }
    });
  }

function mostrar_notas(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "ver_notas",
            cliente_id: $("#hdd_cli_id").val(),
            credito_id: $("#hdd_cre_id").val(),
            tipo_credito: $("#hdd_tipo_credito").val(),
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                $('#body_notas').html(data.html);
                $("#notas").show();
            }else{
                //$("#notas").hide();
            }
        },
        complete: function () {

        }
    });
  }

  function mostrar_nota_gc(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "ver_nota_gc",
            cliente_id: $("#hdd_cli_id").val(),
            credito_id: $("#hdd_cre_id").val(),
            tipo_credito: $("#hdd_tipo_credito").val(),
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                $('#nota_gc').html(data.html);
                $("#notas").show();
            }else{
                //$("#notas").hide();
            }
        },
        complete: function () {

        }
    });
  }