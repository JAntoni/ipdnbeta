/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $("#notas").hide();
    mostrar_nota_gc();

    $('#for_cuopag input[type="checkbox"].flat-green, #for_cuopag input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $('#che_mon_ap').iCheck('uncheck');
    

    mostrar_notas()

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });
    $('.moneda2').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });
    $('.moneda3').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.99'
    });

    $('#datapicker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    }).on('changeDate', function(selected){
        console.log(selected);
        obtener_morasugerida()
    });

    cambio_moneda("venta");

    obtener_morasugerida()

    if($("#hdd_vencida").val()==1 && $("#hdd_morasugerida").val()==0 ){
        alerta_warning("NO SE HA REGISTRADO MORA SUGERIDA");
    }

    $('#txt_cuopag_tipcam').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.000',
        vMax: '9.000'
    });
    $('#txt_cuopag_mor').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999.00'
    });
    $('#txt_cuopag_moncam').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999.00'
    });

    $('.coment').hide();

    //$('#txt_cuopag_mor').autoNumeric('set',0);

    $( "#txt_cuopag_mon" ).blur(function(e) {
        $( "#cmb_mon_id" ).change();
    })
    $( "#txt_cuopag_moncam" ).blur(function(e) {
        calcular_cambio_moneda();
    })
    /*$( "#txt_cuopag_mor" ).blur(function(e) {
        let hddmorasugerida = parseFloat($("#hdd_morasugerida").val());
        let moraingresada = parseFloat($('#txt_cuopag_mor').autoNumeric('get'));
        if(moraingresada > hddmorasugerida && hddmorasugerida>0){
            alerta_warning("NO PUEDE INGRESAR UNA MORA MAYOR A LA SUGERIDA");
            $('#txt_cuopag_mor').val(hddmorasugerida.toFixed(2));
        }
    })*/
    $( "#cmb_mon_id" ).change(function(e) {

        var cuo_tip = parseInt($('#hdd_cuotip_id').val());
  
        var mon_tot = Number($('#txt_cuopag_mon').autoNumeric('get'));
        if(cuo_tip == 3)
          mon_tot = Number($('#txt_cuopag_min').autoNumeric('get'));
        var tip_cam = 0; //Number($('#txt_cuopag_tipcam').val())
        var mon_id = $('#hdd_mon_id').val();
        var res = 0;
  
        if(parseInt($(this).val()) == parseInt(mon_id)){
            cambio_moneda("venta");
            $('#txt_cuopag_mon').autoNumeric('set',mon_tot);
        }
        else{
          if(parseInt($(this).val()) == 1){
            cambio_moneda("venta");
            tip_cam = Number($('#txt_cuopag_tipcam').val());
            res = mon_tot * tip_cam;
            $('#txt_cuopag_moncam').autoNumeric('set',res.toFixed(2));
          }
          else{
            cambio_moneda("compra");
            tip_cam = Number($('#txt_cuopag_tipcam').val());
            res = mon_tot / tip_cam;
            $('#txt_cuopag_moncam').autoNumeric('set',res.toFixed(2));
          }
        }
        console.log(Number($('#txt_cuopag_moncam').autoNumeric('get')));
        div_tipocambio();
      });

    $("#for_cuopag").validate({
        submitHandler: function () {
            if (parseInt($('#hdd_chk_mor_aut').val()) == 1 && $.trim($('#txt_coment').val()) === '') {
                // El textarea está vacío
                alerta_warning('El campo Comentario no puede estar vacío, si editó la mora');
            }
            
            $.ajax({
                type: "POST",
                url: VISTA_URL + "vencimiento/cuotapago_garveh_reg.php",
                async: true,
                dataType: "json",
                data: $("#for_cuopag").serialize(),
                beforeSend: function () {

                },
                success: function (data) {
                    if (parseInt(data.cuopag_id) > 0) {
                        $('#modal_registro_garveh').modal('hide');
                        //vencimiento_tabla();
//                            if(confirm('¿Desea imprimir?'))
//                            cuotapago_imppos_datos_2(data.cuopag_id);


                        $('#msj_vencimiento').hide();
                        $('#msj_cuopag_menor').html(data.cuopag_msj);
                        swal_success('SISTEMA', data.cuopag_msj, 2000);
                        var vista = $("#hdd_vista").val();

                        // if (vista == "cmb_ven_id") {
                        //     vista + "." + (data.ven_id);
                        // }
                        var tipo = $("#action_cuotapago").val();
                        var cuota_nueva = parseInt(data.cuota_nueva); // no se crea nueva cuota, 1 se crea una nueva
                        if (cuota_nueva == 1) {
                            generar_nueva_cuota();
                        }

                        if (vista == "vencimiento_tabla") {
                            vencimiento_tabla();
                        }

                        

                        var codigo = data.cuopag_id;
                        window.open("http://www.ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id=" + codigo);
//                            window.open("http://localhost/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);

                        swal_success('SISTEMA', data.cuopag_msj, 5000);
                        console.log('tipo = ' + data.cuopag_msj);
                        if(parseInt(data.cuotadetalle_id)>0){
                            setTimeout(function() {
                                vencimiento_tabla();
                                morapago_garveh_form(data.action_cuotapago,data.cuotadetalle_id);
                            }, 3000); // 3000 milisegundos = 3 segundos de retraso
                        }else{
                            vencimiento_tabla();
                        }

                    } else {
                        swal_warning("AVISO", data.cuopag_msj, 3500);
                    }
                },
                complete: function (data) {
                    if (data.statusText != 'success') {
                        $('#btn_guar_cuopag').show();
                    }
                }
            });
        },
        rules: {
            txt_cuopag_mon: {
                required: true
            }
        },
        messages: {
            txt_cuopag_mon: {
                required: '* ingrese un monto'
            }
        }
    });

});


/*function cambio_moneda(monid) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_controller.php",
        async: false,
        dataType: "json",
        data: ({
            action: "obtener_dato",
            fecha: $('#txt_cuopag_fec').val(),
            mon_id: monid
        }),
        beforeSend: function () {
        },
        success: function (data) {
            $('#txt_cuopag_tipcam').val(data.moncam_val);
            $('#hdd_tipcam').val(data.moncam_val);
        },
        complete: function () {

        }
    });
}*/

function cambio_moneda(tipo){
    $.ajax({
            type: "POST",
            url: VISTA_URL+"monedacambio/monedacambio_controller.php",
            async:false,
            dataType: "json",
            data: ({
                    action: 'obtener_cambio',
                    fecha:	$('#txt_cuopag_fec').val(),
                    tipo:	tipo
            }),
            beforeSend: function(){
                    $('#msj_pagobanco').hide(100);
            },
            success: function(data){
                console.log(data);
                    if(data.estado == 0){
                            $('#txt_cuopag_tipcam').val('');
                            $('#msj_pagobanco').show(100);
                            $('#msj_pagobanco').html('Por favor registre el tipo de cambio del día de la fecha de depósito');
                            swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
                    }
                    else
                            $('#txt_cuopag_tipcam').val(data.moncam_val);	
            },
            complete: function(data){			
//                 console.log(data);
            }
        });		
}

function obtener_morasugerida() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "morasugerida/morasugerida_controller.php",
        async: false,
        dataType: "json",
        data: ({
            action_morasugerida: "obtener",
            monto: $('#hdd_monto').val(),
            fecha_pago: $('#txt_cuopag_fec').val(),
            cuota_fecha: $('#hdd_fec_ven').val(),
            tipo_credito: $('#hdd_tipo_credito').val(),
            tipo_camnbio: $('#hdd_tipcam').val(),
            moneda_id: $('#hdd_mon_id').val()
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            $('#hdd_morasugerida').val(data.morasugerida);
            $('#txt_cuopag_mor').val(data.morasugerida);
        },
        complete: function () {

        }
    });
}

function monedacambio_form(act, idf)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_form.php",
        async: false,
        dataType: "html",
        data: ({
            action: act,
            moncam_id: idf,
            fec: $('#txt_cuopag_fec').val(),
            vista: 'cambio_moneda'
        }),
        beforeSend: function () {

        },
        success: function (html) {
//            $('#div_monedacambio_form').html(html);
            $('#div_modal_monedacambio_form').html(html);
            $('#modal_registro_monedacambio').modal('show');
            modal_hidden_bs_modal('modal_registro_monedacambio', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_height_auto('modal_registro_monedacambio'); //funcion encontrada en public/js/generales.js
        },
        complete: function () {
        }
    });
}

function div_tipocambio()
{
    var mon_select = $('#cmb_mon_id').val();
    var mon_id = $('#hdd_mon_id').val();

    if (mon_id != mon_select) {
        //$('.div_tipocambio').show();
        $('.div_mon_tipocambio').show();
    } else {
        //$('.div_tipocambio').hide();
        $('.div_mon_tipocambio').hide();
    }
}

function calcular_cambio_moneda() {
    var mon_select = $('#cmb_mon_id').val();

    var cambio = Number($('#txt_cuopag_moncam').val().replace(/[^0-9\.]+/g, ""));
    var result = 0;
    if (mon_select == 1) {
        //ha seleccionado soles
        cambio_moneda("compra");
        var tip_cam = $('#txt_cuopag_tipcam').val();
        result = cambio / tip_cam;
        $("#txt_cuopag_mon").autoNumeric('set', result.toFixed(2));
    } else {
        //ha seleccionado dolares
        cambio_moneda("venta");
        var tip_cam = $('#txt_cuopag_tipcam').val();
        result = cambio * tip_cam;
        $("#txt_cuopag_mon").autoNumeric('set', result.toFixed(2));
    }

}

function notas_his(cliente_id, credito_id, creditotiponotas, subcreditonotas){
    //alert("MOVER DISEÑO");
    console.log(subcreditonotas);
	$.ajax({
		type: "POST",
		url: VISTA_URL + "cliente/clientenota_timeline.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: 'historial',
			cliente_id:	cliente_id,
			credito_id:	credito_id,
			creditotiponotas:	creditotiponotas,
			subcreditonotas:	subcreditonotas,
		}),
		beforeSend: function() {
			//$('#creditogarveh_mensaje_tbl').hide();
			//$('#div_credito_his').dialog("open");
			//$('#div_credito_his').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
      console.log(html);
			$('#div_creditogarveh_his').html(html);
      $('#modal_notas_timeline').modal('show');
      modal_hidden_bs_modal('modal_notas_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_notas_timeline'); //funcion encontrada en public/js/generales.js
		}
	});
}


function calculo() {
    var cuo = $('#txt_cuodet_cuo').autoNumeric('get');
    if (Number($('#hdd_pagos_parciales').val()) > 0) {
        var pagpar = $('#hdd_pagos_parciales').autoNumeric('get');
    } else {
        var pagpar = 0;
    }

    if (Number($('#hdd_cajacliente').val()) > 0) {
        var clicaj = $('#txt_clicaj_tot').autoNumeric('get');
    } else {
        var clicaj = 0;
    }

    var resta1 = cuo - pagpar;

    resta1 = resta1.toFixed(2);


    var total = resta1 - clicaj;
    total = total.toFixed(2);

    if (resta1 * 1 >= clicaj * 1) {
        //alert(total);
        var cuotip_id = $('#hdd_cuotip_id').val();
        var pag_min = $('#txt_cuopag_min').val();

        $('#txt_cuopag_tot').autoNumeric('set',total);
        if (parseInt(cuotip_id) == 3)
            $('#txt_cuopag_mon').val(pag_min);
        else
            $('#txt_cuopag_mon').autoNumeric('set',total);
    } else {
        $('#txt_cuopag_clicaj').autoNumeric('set',resta1);
        $('#txt_cuopag_tot').val('0.00');
        $('#txt_cuopag_mon').val('0.00');
    }
}

function usarSaldo(){
var num=$('#txt_clicaj_tot').autoNumeric('get');
var cuo=$('#txt_cuodet_cuo').autoNumeric('get');
console.log(num);
console.log(cuo);
if(num*1<=cuo*1)
{
    $('#txt_cuopag_clicaj').autoNumeric('set',num);
}
else
{
    $('#txt_cuopag_clicaj').autoNumeric('set',cuo);
}
calculo();
$( "#cmb_mon_id" ).change();
}

function calcular_mora(){ 
$.ajax({
    type: "POST",
    url:  VISTA_URL + "vencimiento/cuotapago_garveh_reg.php",
    async:true,
    dataType: "json",                      
    data: ({
    action: "calcular_mora",
    fechaPago: $('#txt_cuopag_fec').val(),
    fecha: '<?php echo $fec?>'
    }),
    success: function(data){
    $('#txt_cuopag_mor').val(data.mora);

        $('#txt_cuopag_mor').val('0.00');

    }
});
}

function comprobar() {
    var input = $('#txt_cuopag_mor');
    if (input.is('[readonly]')) {
        input.removeAttr('readonly');
        $('#hdd_chk_mor_aut').val(1);
        $('.coment').show();
        $('.replacement').hide();
    } else {
        let hddmorasugerida = parseFloat($("#hdd_morasugerida").val());
        $('#txt_cuopag_mor').val(hddmorasugerida.toFixed(2));
        $('#hdd_chk_mor_aut').val(0);
        $('.replacement').show();
        $('.coment').hide();

        input.attr('readonly', true);
    }
}

function Activar_input_monto_pago_banco() {
    var input = $('#txt_cuopag_mon');
    if (input.is('[readonly]')) {
        input.removeAttr('readonly');
    } else {
        input.attr('readonly', true);
    }
}

/***MORA PAGOS**/

  function morapago_garveh_form(act,idf){
    console.log(act,idf);
    
    Swal.close();
    Swal.fire({
      title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO DE LA MORA EL CLIENTE',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
      showDenyButton: true,
      denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
      confirmButtonColor: '#3b5998', 
      denyButtonColor: '#21ba45',
    }).then((result) => {
  
      if (result.isConfirmed) {
        Swal.fire(
          '¡Has elegido pagar en Oficina!',
          '',
          'success'
        )
        cuotamorapago_garveh_form(act,idf);
      } else if (result.isDenied) {
        Swal.fire(
          '¡Has elegido pagar en Banco!',
          '',
          'success'
        )
        morapago_banco('garveh', idf);
  
      }
    });
  }

  function cuotamorapago_garveh_form(cred, idf){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"cobranzamora/morapago_garveh_form.php",
      async:true,
      dataType: "html",                    
      data: ({
          action: cred,
          cuodet_id:	idf,
          vista:	'pago_cuota'
      }),
      beforeSend: function() {
          
      },
      success: function(html){
          $('#modal_mensaje').modal('hide');
          $('#div_modal_cobranzamora').html(html);
          $('#modal_registro_cobranzamora').modal('show');
          modal_height_auto('modal_registro_cobranzamora');
          modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
      },
      complete: function (html) {
  //        console.log(html);
      }
    });
  }

  function morapago_banco(cred, idf){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"cobranzamora/morapago_banco.php",
      async:true,
      dataType: "html",                    
      data: ({
        credito: cred,
        cuota_id: idf,
        vista: 'cobranza_mora'
      }),
      beforeSend: function() {
          
      },
      success: function(html){
          $('#modal_mensaje').modal('hide');
          $('#div_modal_cobranzamora').html(html);
          $('#modal_registro_cobranzamora').modal('show');
          modal_height_auto('modal_registro_cobranzamora');
          modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
      },
      complete: function (html) {
  //        console.log(html);
      }
    });
  }


  function ocultar_nota(notacliente_id) {
    console.log(notacliente_id);
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "update_state",
            nota_id: notacliente_id,
            estado: 0,
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                alerta_success("CORRECTO", data.clientenota_msj)
                mostrar_notas()
            }else{
                alerta_error("ERROR", data.clientenota_msj);
            }
        },
        complete: function () {

        }
    });
  }

  function mostrar_notas(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "ver_notas",
            cliente_id: $("#hdd_cli_id").val(),
            credito_id: $("#hdd_cre_id").val(),
            tipo_credito: $("#hdd_tipo_credito").val(),
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                $('#body_notas').html(data.html);
                $("#notas").show();
            }else{
                //$("#notas").hide();
            }
        },
        complete: function () {

        }
    });
  }

  function mostrar_nota_gc(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "ver_nota_gc",
            cliente_id: $("#hdd_cli_id").val(),
            credito_id: $("#hdd_cre_id").val(),
            tipo_credito: $("#hdd_tipo_credito").val(),
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                $('#nota_gc').html(data.html);
                $("#notas").show();
            }else{
                //$("#notas").hide();
            }
        },
        complete: function () {

        }
    });
  }