<?php
  require_once('../creditomenor/Creditomenor.class.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
  $oCredito = new Creditomenor();

  $filtro_fec1 = '1990-01-01'; //en todas las fechas
  $filtro_fec2 = date('Y-m-d');
  $empresa_id = 0; // en todas las empresas
  $usuario_id = 0; //registrado por todos los asesores
  $cliente_id = intval($_POST['cliente_id']);
?>

<table id="tbl_creditomenor" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">FECHA</th>
      <th id="tabla_cabecera_fila">CLIENTE</th>
      <th id="tabla_cabecera_fila">TIPO</th>
      <th id="tabla_cabecera_fila">MONTO</th>
      <th id="tabla_cabecera_fila">INT(%)</th>
      <th id="tabla_cabecera_fila">CUOTAS</th>
      <th id="tabla_cabecera_fila">ASESOR</th>
      <th id="tabla_cabecera_fila">UBICACION</th>
      <th id="tabla_cabecera_fila">ESTADO</th>
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila" width="11%"></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $result = $oCredito->filtro_creditos($filtro_fec1, $filtro_fec2, $empresa_id, $usuario_id, $cliente_id);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr id="tabla_cabecera_fila">
            <td id="tabla_fila"><?php echo mostrar_fecha($value['tb_credito_feccre']);?></td>
            <td id="tabla_fila"><?php echo $value['tb_cliente_nom'];?></td>
            <td id="tabla_fila"><?php echo $value['tb_cuotatipo_nom'];?></td>
            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'].' '.mostrar_moneda($value['tb_credito_preaco']); ?></td>
            <td id="tabla_fila"><?php echo mostrar_moneda($value['tb_credito_int']); ?></td>
            <td id="tabla_fila"><?php echo ($value['tb_cuotatipo_id'] == 2)? $value['tb_credito_numcuo']: $value['tb_credito_numcuo'].' Max'; ?></td>
            <td id="tabla_fila"><?php echo $value['tb_usuario_nom']; ?></td>
            <td id="tabla_fila">
              <button type="button" class="btn btn-xs bg-navy" title="Editar" onclick="garantia_estado(<?php echo $value['tb_credito_id'];?>)">Ubicación</button>
            </td>
            <td id="tabla_fila">
              <?php 
              if (intval($value['tb_credito_est']) == 1) {
                echo '<span class="badge bg-yellow">Pendiente Aprobación</span>';
              }
              if(intval($value['tb_credito_est']) == 2) {
                echo '<span class="badge bg-aqua">Por Desembolsar</span>';
              }
              if(intval($value['tb_credito_est']) == 3) {
                echo '<span class="badge bg-green">Vigente</span>';
              }
              if(intval($value['tb_credito_est']) == 4) {
                echo '<span class="badge bg-blue">Liquidado</span>';
              }
              if(intval($value['tb_credito_est']) == 5) {
                echo '<span class="badge bg-red">Remate</span>';
              }
              if(intval($value['tb_credito_est']) == 6) {
                echo '<span class="badge bg-purple">Vendido</span>';
              }
              if(intval($value['tb_credito_est']) == 7) {
                echo '<span class="badge bg-teal">Por Entregar</span>';
              }
              ?>              
            </td>
            <td id="tabla_fila"><?php echo $value['tb_credito_id']; ?></td>
            <td id="tabla_fila" align="center">
              <?php if(intval($value['tb_credito_est']) > 2): ?>
                <a class="btn btn-success btn-xs" title="Pagos" onclick="creditomenor_pagos(<?php echo $value['tb_credito_id'];?>)"><i class="fa fa-money"></i></a>
              <?php endif;?>
              <a class="btn btn-info btn-xs" title="Ver" onclick="creditomenor_form(<?php echo "'L', ".$value['tb_credito_id'];?>)"><i class="fa fa-eye"></i></a>
              <div class="btn-group">
                <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="btn" onclick="creditolinea_timeline(<?php echo $value['tb_credito_id'];?>)"><b>Historial</b></a></li>
                </ul>
              </div>
            </td>
          </tr>
          <?php
        endforeach;
      }else{
        ?>
        <tr>
          <td colspan="11" class="text-center align-middle "><i>(Sin datos)</i></td>
        </tr>
        <?php
      }
      $result = NULL;
      ?>
  </tbody>
</table>