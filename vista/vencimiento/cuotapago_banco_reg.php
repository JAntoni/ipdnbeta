<?php

error_reporting(0);
require_once('../../core/usuario_sesion.php');
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../tareainventario/Tareainventario.class.php');
$oTarea = new TareaInventario();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once ('../historial/Historial.class.php');
$oHistorial = new Historial();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$hoy = date('d-m-Y');
$fec_pago = $_POST['txt_cuopag_fec'];

//Valida que la fecha de validacion del pago sea la fecha actual 
if ($hoy != $fec_pago) {
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'No se puede hacer pagos con fechas diferentes al día actual, consulte con administrador. Fecha hoy: ' . $hoy . ', fecha a pagar: ' . $fec_pago;
    echo json_encode($data);
    exit();
}

$cuota_id = $_POST['hdd_modid']; // ID DE LA TABLA A LA QUE PERTENECE EL $mod_id
//$mod_id=$_POST['hdd_cuotatip_id'];
$mod_id = $_POST['hdd_mod_id']; // ID DE MODULO PARA SABER SI ES 1 CUOTA Y 2 ES CUOTADETALLE


/* SUMATORIA DE TODOS LOS PAGOS REALIZADOS A LA CUOTA O CUOTADETALLE 
PARA SABER LO QUE YA PAGÓ Y OBTENER SOLO LO QUE FALTA PAGAR, LA DIFERENCIA */
$result = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $cuota_id);
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value3) {
        $pagos_cuota += formato_numero($value3['tb_ingreso_imp']); // SUMATORIA DE TODOS LOS PAGOS REALIZADOS SOBRE LA CUOTA 
    }
}

$resultcuo = $oCuota->mostrarUno($cuota_id);

/* OBTIENE TODOS LOS DATOS NECESARIOS DE LA CUOTA */
if ($resultcuo['estado'] == 1) {
    $cuopag_tot1 = $resultcuo['data']['tb_cuota_cuo']; //MONTO TOTAL DEL MES CON TODO INTERÉS
    $cuota_cap = $resultcuo['data']['tb_cuota_cap']; //MONTO TOTAL DEL MES CON TODO INTERÉS
    $cuo_int = formato_moneda($resultcuo['data']['tb_cuota_int']); //MONTO INTERES SOBRE CAPITAL
    $cuo_fec = $resultcuo['data']['tb_cuota_fec']; //FECHA DE PAGO DE LA CUOTA
    $cuo_num = $resultcuo['data']['tb_cuota_num']; //NUMERO DE LA CUOTA
}

$pagos = $pagos_cuota; //SUMA DE LOS PAGOS PARCIALES SOBRE LA CUOTA REALIZADOS
$cuopag_tot = $cuopag_tot1 - $pagos; //MONTO RESTANTE PARA COMPLETAR EL PAGO DE CUOTA

$fechaFacturacion = date("d-m-Y", strtotime($cuo_fec . "- 1 month")); //cuando se genera la cuota
$diasdelmes = date('t', strtotime($fechaFacturacion)); //DIAS DEL MES EN QUE SE GENERÓ LA CUOTA ACTUAL
$fechahoy = $_POST['txt_cuopag_fecdep']; //date('d-m-Y'); // la fecha actual del dia
$fechaPago = $cuo_fec; // cuando me toca pagar la cuota

/*** NO SE USA EN CUOTA FIJA ***/

$variableopcional = "";
//CALCULAR DIAS DESDE LA FECHA FACTURACION (1 MES ANTES DE LA FECHA DE PAGO) CUANDO LA CUOTA ES 1
if ($cuo_num == 1) {
    $dias = (strtotime($fechahoy) - strtotime($fechaFacturacion)) / 86400;
}

//CALCULAR DIAS DESDE LA FECHA FACTURACION (1 MES ANTES DE LA FECHA DE PAGO) CUANDO LA CUOTA ES MAYOR QUE 1
if ($cuo_num > 1) {
    $fechaFacturacion = strtotime($fechaFacturacion);
    $fechahoy = strtotime($fechahoy);
    $fechaPago = strtotime($fechaPago);

    if ($fechahoy < $fechaFacturacion) {
        $dias = 0;
        $variableopcional = 1;
    }
    if ($fechahoy > $fechaFacturacion) {

        $dias = (($fechahoy) - ($fechaFacturacion)) / 86400;
        $variableopcional = 0;
    }
}

$diastranscurridos = $dias;

$vencida = 0;
if (($fechahoy) > (strtotime($cuo_fec))) {
    $vencida = 1;
}

//CALCULAR PAGO MINIMO INTERES MENOS LO QUE SE HA PAGADO
$pago_minimo = ($cuo_int - ($pagos));

//CALCULAR PAGO MINIMO CUANDO LA CUOTA NO ESTA VENCIDA
if ($vencida == 0) {
    if ($cuo_num == 1) {
        $pago_minimo = ($cuo_int - formato_moneda($pagos));
    }
    if ($cuo_num > 1) {
        $pago_minimo = ((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
    }
}
/*** NO SE USA EN CUOTA FIJA ***/


/**  OBTENER DATOS DESDE EL FORMULARIO  **/
$numope = trim($_POST['txt_cuopag_numope']); //Obtener numero de operacion
$fecha_deposito = fecha_mysql($_POST['txt_cuopag_fecdep']); //Obtener fecha deposito
$monto_validar = formato_moneda($_POST['txt_cuopag_mon']); //elimina las comas y devuelve con 2 decimales - obtener monto deposito
$monto_validar_tot = formato_moneda($_POST['txt_cuopag_montot']);
$moneda_deposito_id = intval($_POST['hdd_mon_iddep']); //1 soles, 2 dolares
$moneda_credito_id = intval($_POST['hdd_mon_id']); //1 soles, 2 dolares
$tipocambio_dia = floatval($_POST['txt_cuopag_tipcam']);
$cuentadeposito_id=$_POST['cmb_cuedep_id']; //Obtener numero de cuenta
/**  OBTENER DATOS DESDE EL FORMULARIO  **/

$deposito_mon = 0;
//$dts = $oDeposito->numero_operacion_unico_cuenta($numope,$cuentadeposito_id);

/**  VALIDA NUMERO DE OPERACION  Y OBTIENE MONTO **/
$dts = $oDeposito->validar_no_operacion_fecha($numope, $fecha_deposito);
if ($dts['estado'] == 1) {
    foreach ($dts['data'] as $key => $dt) {
        $deposito_mon += floatval($dt['tb_deposito_mon']);
    }
}
/**  VALIDA NUMERO DE OPERACION Y OBTIENE MONTO **/

/**  VALIDA SI NO EXISTE MONTO DEPOSITO DEVUELVE ALERT CON ERROR Y DETIENE EL PROCESO **/
if ($deposito_mon == 0) {
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra asociado a la Cuenta seleccionada o la fecha de depósito no corresponde, consulta con ADMINISTRACIÓN por favor.';
    echo json_encode($data);
    exit();
}
/**  VALIDA SI NO EXISTE MONTO DEPOSITO DEVUELVE ALERT CON ERROR **/

/***  VALIDA QUE EL NUMERO DE OPERACION TENGA FONDOS PARA CUBRIR EL MONTO DEPOSITO ***/
$ingreso_importe = 0;
$dts = $oIngreso->lista_ingresos_num_operacion($numope);
//OBTIENE TODOS LOS INGRESOS USANDO EL NUMERO DE OPERACION, ES DECIR EL SALDO USADO
if ($dts['estado'] == 1) {
    foreach ($dts['data'] as $key => $dt) {
        $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
        $ingreso_mon_id = intval($dt['tb_moneda_id']);
        if ($ingreso_mon_id == 2) {//si el ingreso se hizo en dólares
            $tb_ingreso_imp = ($tb_ingreso_imp * $tipocambio_dia);
        }
        $ingreso_importe += $tb_ingreso_imp;
    }
}


$monto_usar = $deposito_mon - $ingreso_importe; // Es el monto que tiene disponible para poder usar en otro pago
$monto_usar = formato_moneda($monto_usar); //FONDOS DISPONIBLES
//echo 'tooo esto hay een importes: '.$ingreso_importe.' // depo: '.$deposito_mon.' // validar: '.$monto_validar.' // usar: '.$monto_usar.' // moneda ing: '.$ingreso_mon_id.' // tipo cambio: '.$tipocambio_dia; exit();

//SI EL MONTO DEPOSITO ES MAYOR QUE LOS FONDOS DISPONIBLES LANZA UNA ALERTA Y DETIENE EL PROCESO
if (($monto_validar_tot) > $monto_usar) {
    $data['cuopag_id'] = 0;
    $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar más de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPÓSITO: ' . mostrar_moneda($deposito_mon);
    //$data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . ' | validar: ' . $monto_validar . ' | usar: ' . $monto_usar . ' | ingre: ' . $ingreso_importe . ' | cambio: ' . $tipocambio_dia . ' / mond ing: ' . $ingreso_mon_id;
    echo json_encode($data);
    exit();
}
/***  VALIDA QUE EL NUMERO DE OPERACION TENGA FONDOS PARA CUBRIR EL MONTO DEPOSITO ***/

$monto_pagado_activado_campo = formato_numero($_POST['txt_cuopag_mon']);
$pago_minimo_mostrado_sistema = formato_moneda($pago_minimo);
$cuopag_tot = formato_numero($cuopag_tot);

/***  REVISA QUE TIPO DE CREDITO ES ***/
$cre_tip = 0;
if ($_POST['hdd_credito'] == "asiveh") {
    require_once("../creditoasiveh/Creditoasiveh.class.php");
    $oCredito = new Creditoasiveh();
    $cre_tip = 2;
}
if ($_POST['hdd_credito'] == "garveh") {
    require_once ("../creditogarveh/Creditogarveh.class.php");
    $oCredito = new Creditogarveh();
    $cre_tip = 3;
}
if ($_POST['hdd_credito'] == "hipo") {
    require_once ("../creditohipo/Creditohipo.class.php");
    $oCredito = new Creditohipo();
    $cre_tip = 4;
}
if ($_POST['hdd_credito'] == "menor") {
    require_once ("../creditomenor/Creditomenor.class.php");
    $oCredito = new Creditomenor();
    $cre_tip = 4;
}

$moneda_depo = ' ? ';
if ($_POST['hdd_mon_iddep'] != '') {
    if ($_POST['hdd_mon_iddep'] == 1)
        $moneda_depo = 'S/.';
    else
        $moneda_depo = 'US$';
}

$moneda_cred = ' ? ';
if ($_POST['hdd_mon_id'] != '') {
    if ($_POST['hdd_mon_id'] == 1)
        $moneda_cred = 'S/.';
    else
        $moneda_cred = 'US$';
}
/***  REVISA QUE TIPO DE CREDITO ES ***/

/***  OBTIENE DATOS DE LA PC ***/
$nombre_pc = (!empty($_POST['hdd_pc_nombre'])) ? $_POST['hdd_pc_nombre'] : 'SIN NOMBRE PC';
$mac_pc = (!empty($_POST['hdd_pc_mac'])) ? $_POST['hdd_pc_mac'] : 'SIN NOMBRE MAC';
/***  OBTIENE DATOS DE LA PC ***/

if ($_POST['hdd_cuotatip_id'] == 4 && ($_POST['hdd_credito'] == "asiveh" || $_POST['hdd_credito'] == "garveh" || $_POST['hdd_credito'] == "hipo")) {

    if (!empty($_POST['txt_cuopag_montot'])) {
        $acuerdo_ap = $_POST['hdd_acupag']; //si el valor es 1 entonces es un acuerdo de pago
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
        //monto total que debe pagar el cliente
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente 
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco 
        $tipo_caja = 1; //las cajas a tener en cuenta es 1 CAJA y 14 CAJA AP 

        $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: ' . trim($_SESSION['usuario_nom']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

        /***  VALIDA QUE SEA ACUERDO DE PAGO ***/
        if ($acuerdo_ap == 1) {
            $tipo_caja = 14; //CAJA AP
            $cuopag_his .= 'Esta cuota es de un Acuerdo de Pago, por ende el monto fue directamente a la CAJA AP ||&& ';
        }
        /***  VALIDA QUE SEA ACUERDO DE PAGO ***/

        /***  SETEA DATOS DE CUOTAPAGO ***/
        $xac = 1;
        $oCuotapago->tb_cuotapago_xac = $xac; //ESTADO XAC = 1
        $oCuotapago->tb_cuotapago_usureg = $_SESSION['usuario_id']; // USUARIO QUE INGRESÓ REGISTRO
        $oCuotapago->tb_cuotapago_usumod = $_SESSION['usuario_id']; // USUARIO QUE MODIFICÓ REGISTRO
        $oCuotapago->tb_modulo_id = $_POST['hdd_mod_id']; // MODULO: PARA GARVEH ES 2 (CUOTADETALLE)
        $oCuotapago->tb_cuotapago_modid = $_POST['hdd_modid']; // ID DE LA CUOTA
        $oCuotapago->tb_cuotapago_fec = $fecha; // FECHA VALIDACION
        $oCuotapago->tb_moneda_id = $_POST['hdd_mon_id']; // MONEDA ID DEL CREDITO
        $oCuotapago->tb_cuotapago_mon = moneda_mysql($mon_pagado); // MONTO QUE EL CLIENTE PAGA
        $oCuotapago->tb_cuotapago_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // TIPO DE CAMBIO
        $oCuotapago->tb_cuotapago_moncam = 0; // ?????
        $oCuotapago->tb_cuotapago_pagcon = moneda_mysql($mon_pagado); // ????
        $oCuotapago->tb_cliente_id = $_POST['hdd_cli_id']; // CLIENTE ID
        $oCuotapago->tb_cuotapago_obs = $_POST['txt_cuopag_obs']; // OBSERVACION
        /***  SETEA DATOS DE CUOTAPAGO ***/

        /*** INSERTA CUOTA PAGO ***/
        $result = $oCuotapago->insertar();
        if (intval($result['estado']) == 1) {
            $cuopag_id = $result['tb_cuotapago_id'];
        }
        $result = NULL;
        /*** INSERTA CUOTA PAGO ***/

        //estado de cuota detalle
        if (formato_moneda($mon_pagado) == formato_moneda($cuopag_tot)) {
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_est', 2, 'INT'); // REGISTRA ESTADO CANCELADA A CUOTADETALLE
            //registro de mora
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR'); // REGISTRA MORA DE LA CUOTA DETALLE
            if ($_POST['chk_mor_aut'] == 1) {
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_moraut', 1, 'INT'); // SE MODIFICA EL ESTADO DE CUOTA DETALLE CON MORA ALTERADA
            }
            /*** SI LA MORA ES MAYOR QUE 0 SE MODOIFICA EL CAMPO 'tb_cuotadetalle_morest' A 1 INDICANDO QUE SE GENERÓ MORA ***/
            if (formato_moneda($_POST['txt_cuopag_mor']) > 0) {
                $data['cuota_id'] = $cuota_id;
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_morest', 1, 'INT');
                $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
                
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }
            /*** SI LA MORA ES MAYOR QUE 0 SE MODOIFICA EL CAMPO 'tb_cuotadetalle_morest' A 1 INDICANDO QUE SE GENERÓ MORA ***/
            $monto = formato_moneda($mon_pagado);
            if ($monto > 0) {
                $ingresar = 1;
            }
        }

        if (formato_moneda($mon_pagado) < formato_moneda($cuopag_tot)) {
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_est', 3, 'INT'); // REGISTRA ESTADO PAGO PARCIAL A CUOTADETALLE
            $monto = formato_moneda($mon_pagado);
            if ($monto > 0) {
                $ingresar = 1;
            }
        }

        if (formato_moneda($mon_pagado) > formato_moneda($cuopag_tot)) {
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_est', 2, 'INT'); // REGISTRA ESTADO CANCELADA A CUOTADETALLE
            //registro de mora
            $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR'); // REGISTRA MORA DE LA CUOTA DETALLE
            if ($_POST['chk_mor_aut'] == 1) {
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_moraut', 1, 'INT'); // SE MODIFICA EL ESTADO DE CUOTA DETALLE CON MORA ALTERADA
            }
            /*** SI LA MORA ES MAYOR QUE 0 SE MODOIFICA EL CAMPO 'tb_cuotadetalle_morest' A 1 INDICANDO QUE SE GENERÓ MORA ***/
            if (formato_moneda($_POST['txt_cuopag_mor']) > 0) {
                $data['cuota_id'] = $cuota_id;
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_morest', 1, 'INT');
                $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
            
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }
            /*** SI LA MORA ES MAYOR QUE 0 SE MODOIFICA EL CAMPO 'tb_cuotadetalle_morest' A 1 INDICANDO QUE SE GENERÓ MORA ***/
            $monto = formato_moneda($cuopag_tot);
            if ($monto > 0) {
                $ingresar = 1;
            }

            /*** VARIABLE PARA CLIENTE CAJA ***/
            $abono = formato_moneda($mon_pagado) - formato_moneda($cuopag_tot);
            if ($abono > 0) {
                $abonar = 1;
            }
            /*** VARIABLE PARA CLIENTE CAJA ***/
        }

        //Verificar si ingresó dinero
        if ($ingresar == 1) {
            //registro de ingreso
            $subcue_id_ingre = 0; //CUOTAS asiveh
            if ($_POST['hdd_credito'] == "asiveh") {
                $subcue_id_ingre = 1;
            } //CUOTAS asiveh
            if ($_POST['hdd_credito'] == "garveh") {
                $subcue_id_ingre = 3;
            } //CUOTAS garveh
            if ($_POST['hdd_credito'] == "hipo") {
                $subcue_id_ingre = 4;
            } //CUOTAS hipo

            $cue_id_ingre = 1; //CANCEL CUOTAS
            $mod_id = 30; //cuota pago
            /** SE GENERA EL DETALLE DE PAGO **/
            $ing_det = $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda de :'.mostrar_moneda($_POST['txt_cuopag_tipcam']).'. El monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
            /** SE GENERA EL DETALLE DE PAGO **/

            if (!empty($_POST['txt_cuopag_obs'])) {
                $ing_det .= '. Obs: ' . $_POST['txt_cuopag_obs'];
            }

            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuopag_id; // SE GENERA EL NUMERO DE DOCUMENTO

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']); // USUARIO QUE REGISTRÓ EL INGRESO
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']); // USUARIO QUE MODIFICÓ EL INGRESO
            $oIngreso->ingreso_fec = $fecha; // FECHA DE VALIDACION
            $oIngreso->documento_id = 8; //INGRESÓ DINERO POR PAGO (ASÍ DIJO JUAN)
            $oIngreso->ingreso_numdoc = $numdoc; // NUMERO DE DOCUMENTO GENERADO ANTES
            $oIngreso->ingreso_det = $ing_det; // DETALLE DE INGRESO GENERADO ANTES
            $oIngreso->ingreso_imp = moneda_mysql($monto); // IMPORTE INGRESADO
            $oIngreso->cuenta_id = $cue_id_ingre; // SE SETTEO EN 1 QUE INDICA CANCELACION DE CUOTAS
            $oIngreso->subcuenta_id = $subcue_id_ingre; // SUBCUENTAS, PARA GARVEH ES 3
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); // CLIENTEID
            $oIngreso->caja_id = $tipo_caja; //TIPO CAJA, SI ES 1 ES CAJA NORMAL, SI ES 14 ES CAJA AP
            $oIngreso->moneda_id = $_POST['hdd_mon_id']; // ID MONEDA DEL INGRESO
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id; //ID DE MODULO PARA SABER SI ES 30 CUOTA PAGO
            $oIngreso->ingreso_modide = $cuopag_id; // ID DEL CUOTAPAGO REGISTRADO 
            $oIngreso->empresa_id = $_SESSION['empresa_id']; //EMPRESA ID
            $oIngreso->ingreso_fecdep = ''; //BLANCO
            $oIngreso->ingreso_numope = ''; //BLANCO
            $oIngreso->ingreso_mondep = 0; //BLANCO
            $oIngreso->ingreso_comi = 0; //BLANCO
            $oIngreso->cuentadeposito_id = 0; //BLANCO
            $oIngreso->banco_id = 0; //BLANCO
            $oIngreso->ingreso_ap = 0; //BLANCO
            $oIngreso->ingreso_detex = ''; //BLANCO

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_id = $result['ingreso_id'];
            }
            $result = NULL;

            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']); // FECHA DEPOSITO
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope']; // NUMERO DE OPERACION
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']); //MONTO DEPOSITO
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']); //COMISION DE BANCO
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id']; // CUENTA DEPOSITO ID
            $oIngreso->banco_id = $_POST['cmb_banco_id']; // BANCO ID
            $oIngreso->ingreso_id = $ingr_id; // ID DE INGRESO

            $oIngreso->registrar_datos_deposito_banco();

            /*$oIngreso->ingreso_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // FECHA DEPOSITO
            $oIngreso->ingreso_id = $ingreso_id; // ID DE INGRESO
            $oIngreso->registrar_tipocambio();*/
            $oIngreso->modificar_campo_nuevo($ingr_id, $_SESSION['usuario_id'], 'tipcam', moneda_mysql($_POST['txt_cuopag_tipcam']));

            $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: ' . $ing_det . ' ||&& ';
            $line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha hecho la transacción: ' . $ing_det . '. && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $line_det;
            $oLineaTiempo->insertar();

            $ingreso_detalle = $ing_det;
        } 

        //si acu_mon es null es porq esta cuota no tiene q pagar un acuerdo de pago, si se paga con monto AP no se abona nada
        if ($abonar == 1) {
            
            //            $data['cuopag_id'] = 0;
            //            $data['cuopag_msj'] = 'hasta aki normal entra el sistema  1';
            //            echo json_encode($data);
            //            exit();
            //abono cliente caja
            $xac = 1;
            $clicaj_tip = 1; //ingreso
            $mod_id = 30; //cuota pago

            $oClientecaja->tb_clientecaja_xac = 1; // ESTADO 1
            $oClientecaja->tb_cliente_id = $_POST['hdd_cli_id']; // CLIENTE ID
            $oClientecaja->tb_credito_id = $_POST['hdd_cre_id']; // CREDITO ID
            $oClientecaja->tb_clientecaja_tip = $clicaj_tip; // TIPO CLIENTE CAJA, POR INGRESO
            $oClientecaja->tb_clientecaja_fec = $fecha; // FECHA DE VALIDACION
            $oClientecaja->tb_moneda_id = $_POST['hdd_mon_id']; // MONEDA ID
            $oClientecaja->tb_clientecaja_mon = moneda_mysql($abono); // MONTO
            $oClientecaja->tb_modulo_id = $mod_id; // MODULO ID: CUOTA PAGO
            $oClientecaja->tb_clientecaja_modid = $cuopag_id; //ID DE MODULO: ID DE LA CUOTA PAGO
            //abono cliente caja
            $result1 = $oClientecaja->insertar();
            if (intval($result1['estado']) == 1) {
                $clicaj_id = $result1['tb_clientecaja_id'];
            }
            $result1 = NULL;


            $cue_id = 21; //saldo a favor
            $subcue_id = 62; //saldo a favor
            $mod_id = 40; //cliente caja
            $ing_det = "ABONO CLIENTE " . $_POST['hdd_pag_det'];
            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $clicaj_id;

            $oIngreso->ingreso_usureg = $_SESSION['usuario_id']; // USUARIO QUE REGISTRÓ EL INGRESO
            $oIngreso->ingreso_usumod = $_SESSION['usuario_id']; // USUARIO QUE MODIFICÓ EL INGRESO
            $oIngreso->ingreso_fec = $fecha; // FECHA DE VALIDACION
            $oIngreso->documento_id = 8; //INGRESÓ DINERO POR PAGO (ASÍ DIJO JUAN)
            $oIngreso->ingreso_numdoc = $numdoc; // NUMERO DE DOCUMENTO GENERADO ANTES
            $oIngreso->ingreso_det = $ing_det; // DETALLE DE INGRESO GENERADO ANTES
            $oIngreso->ingreso_imp = $abono; // IMPORTE INGRESADO
            $oIngreso->cuenta_id = $cue_id; // SE SETTEO EN 21 QUE INDICA SALDO A FAVOR
            $oIngreso->subcuenta_id = $subcue_id; // SUBCUENTAS, 62 ES SALDO A FAVOR
            $oIngreso->cliente_id = $_POST['hdd_cli_id']; // CLIENTEID
            $oIngreso->caja_id = 1; //TIPO CAJA, SI ES 1 ES CAJA NORMAL, SI ES 14 ES CAJA AP
            $oIngreso->moneda_id = $_POST['hdd_mon_id']; // ID MONEDA DEL INGRESO
            $oIngreso->modulo_id = $mod_id; // MODULO ID QUE ES 40 CLIENTE CAJA
            $oIngreso->ingreso_modide = $clicaj_id; // ID DEL MODULO QUE ES CAJA CLIENTE ID
            $oIngreso->empresa_id = $_SESSION['empresa_id']; // EMPRESA ID

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_abono_id = $result['ingreso_id'];
            }
            $result = NULL;

            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']); // FECHA DEPOSITO
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope']; // NUMERO DE OPERACION
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']); //MONTO DEPOSITO
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']); //COMISION DE BANCO
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id']; // CUENTA DEPOSITO ID
            $oIngreso->banco_id = $_POST['cmb_banco_id']; // BANCO ID
            $oIngreso->ingreso_id = $ingr_abono_id; // ID DE INGRESO

            $oIngreso->registrar_datos_deposito_banco();

            $cuopag_his .= 'El ingreso generado fue mayor a la cuota facturada, por eso a la Caja Cliente fue un saldo a favor de: ' . $moneda_cre . ' ' . mostrar_moneda($abono) . ' ||&& ';
        }
    
        //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1) { //hdd_mon_iddep DEPENDE DE LA CUENTA QUE SE SELECCIONE
            $subcue_id = 71; // 71 es cuenta en soles
        }

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1; //sistema
        $mod_id = 30; //30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
        $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuopag_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id']; // USUARIO QUE REGISTRÓ EL INGRESO
        $oEgreso->egreso_usumod = $_SESSION['usuario_id']; // USUARIO QUE MODIFICÓ EL INGRESO
        $oEgreso->egreso_fec = $fecha; // FECHA DE VALIDACION
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc; // NUMERO DE DOCUMENTO GENERADO ANTES
        $oEgreso->egreso_det = $egr_det; // DETALLE DE EGRESO GENERADO ANTES
        $oEgreso->egreso_imp = moneda_mysql($mon_pagado); // IMPORTE EGRESADO
        $oEgreso->egreso_tipcam = 0; // TIPO DE CAMBIO EN 0
        $oEgreso->egreso_est = 1; // ESTADO EGRESO
        $oEgreso->cuenta_id = $cue_id; // CUENTA ID EN 28 PAGO EN CUENTA DE BANCO
        $oEgreso->subcuenta_id = $subcue_id; // SUBCUENTA ID SI ES 72 ES DE DOLARES, SI ES 71 ES SOLES
        $oEgreso->proveedor_id = $pro_id; // PRO ID 1 ES SISTEMA
        $oEgreso->cliente_id = 0; // EN BLANCO
        $oEgreso->usuario_id = 0; // EN BLANCO
        $oEgreso->caja_id = $caj_id; // CAJA ID 1 ES CAJA NORMAL
        $oEgreso->moneda_id = $_POST['hdd_mon_id']; // MONEDA ID DEL CREDITO
        $oEgreso->modulo_id = $mod_id; // MODULO ID 30 ES EL MODULO DE INGRESO
        $oEgreso->egreso_modide = $modide; // MODIDE ID ES EL ID DEL MODULO ID QUE ES EL CUOTAPAGO ID
        $oEgreso->empresa_id = $_SESSION['empresa_id']; // EMPRESA ID
        $oEgreso->egreso_ap = 0; //EN 0 ES NO PORQUE NO ES ACUERDO DE PAGO
        $oEgreso->insertar();
        
        //registrarmos el ingreso del dinero en el tipo de moneda depositado
        $cue_id_ingre = 7; //cuenta de cancelar cuotas en deposito banco
        $subcue_id_ingre = 23; //CUOTAS menor
        if ($_POST['hdd_credito'] == "asiveh")
            $subcue_id_ingre = 22; //CUOTAS asiveh
        if ($_POST['hdd_credito'] == "garveh")
            $subcue_id_ingre = 24; //CUOTAS garveh
        if ($_POST['hdd_credito'] == "hipo")
            $subcue_id_ingre = 25; //CUOTAS hipo
        $numdoc = '';

        /*$oIngreso->ingreso_usureg = $_SESSION['usuario_id']; // USUARIO QUE REGISTRÓ EL INGRESO
        $oIngreso->ingreso_usumod = $_SESSION['usuario_id']; // USUARIO QUE MODIFICÓ EL INGRESO
        $oIngreso->ingreso_fec = $fecha; // FECHA DE VALIDACION
        $oIngreso->documento_id = 8; //INGRESÓ DINERO POR PAGO (ASÍ DIJO JUAN)
        $oIngreso->ingreso_numdoc = $numdoc; // NUMERO DE DOCUMENTO GENERADO ANTES
        $oIngreso->ingreso_det = $ingreso_detalle; // DETALLE DE INGRESO GENERADO ANTES
        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_mon']); // IMPORTE INGRESADO
        $oIngreso->cuenta_id = $cue_id_ingre; // SE SETTEO EN 21 QUE INDICA CUENTA DE CANCELAR CUOTAS EN DEPOSITO BANCO
        $oIngreso->subcuenta_id = $subcue_id_ingre; // SUBCUENTAS, 24 ES GARVEH
        $oIngreso->cliente_id = $_POST['hdd_cli_id']; // CLIENTEID
        $oIngreso->caja_id = $tipo_caja; //TIPO CAJA, SI ES 1 ES CAJA NORMAL, SI ES 14 ES CAJA AP
        $oIngreso->moneda_id = intval($_POST['hdd_mon_iddep']); // ID MONEDA DEL INGRESO
        $oIngreso->modulo_id = 5; // MODULO ID QUE ES 5
        $oIngreso->ingreso_modide = $cuopag_id; // ID DEL MODULO QUE ES CUOTA PAGO ID
        $oIngreso->empresa_id = $_SESSION['empresa_id']; // EMPRESA ID

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
            $ingr_id = $result['ingreso_id'];
        }
        $result = NULL;

        $oIngreso->modificar_campo($ingr_id, $_SESSION['usuario_id'], 'tb_ingreso_numdoc', $ingr_id, 'STR');

        //despues de ingresar el monto del banco, hay que egresarlo para no alterar la caja

        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1)
            $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1; //sistema
        $mod_id = 5; //el mod id sera 5 que indica deposito en banco
        $modide = $cuopag_id; //el modid será el id del cuotapago
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $ingr_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id']; // USUARIO QUE REGISTRÓ EL INGRESO
        $oEgreso->egreso_usumod = $_SESSION['usuario_id']; // USUARIO QUE MODIFICÓ EL INGRESO
        $oEgreso->egreso_fec = $fecha; // FECHA DE VALIDACION
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc; // NUMERO DE DOCUMENTO GENERADO ANTES
        $oEgreso->egreso_det = $egr_det; // DETALLE DE EGRESO GENERADO ANTES
        $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_mon']); // IMPORTE EGRESADO
        $oEgreso->egreso_tipcam = 0; // TIPO DE CAMBIO EN 0
        $oEgreso->egreso_est = 1; // ESTADO EGRESO
        $oEgreso->cuenta_id = $cue_id; // CUENTA ID EN 28 PAGO EN CUENTA DE BANCO
        $oEgreso->subcuenta_id = $subcue_id; // SUBCUENTA ID SI ES 72 ES DE DOLARES, SI ES 71 ES SOLES
        $oEgreso->proveedor_id = $pro_id; // PRO ID 1 ES SISTEMA
        $oEgreso->cliente_id = 0; // EN BLANCO
        $oEgreso->usuario_id = 0; // EN BLANCO
        $oEgreso->caja_id = $caj_id; // CAJA ID 1 ES CAJA NORMAL
        $oEgreso->moneda_id = intval($_POST['hdd_mon_iddep']); // MONEDA ID DEL CREDITO
        $oEgreso->modulo_id = $mod_id; // MODULO ID 5 ES EL DEPOSITO EN BANCO
        $oEgreso->egreso_modide = $modide; // MODIDE ID ES EL ID DEL MODULO ID QUE ES EL CUOTAPAGO ID
        $oEgreso->empresa_id = $_SESSION['empresa_id']; // EMPRESA ID
        $oEgreso->egreso_ap = 0; //EN 0 ES NO PORQUE NO ES ACUERDO DE PAGO
        $oEgreso->insertar();

        $cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: ' . $egr_det . ' ||&& ';*/

        //estado cuota
        $rws = $oCuotadetalle->filtrar($_POST['hdd_cuo_id']);


        /*** RECORRE TODAS LAS CUOTAS DETALLE Y SI ALGUNA NO ESTA CANCELADA LA CUOTA SE PONE COMO PAGO PARCIAL Y SI TODAS SON CANCELADAS, LA CUOTA SE CANCELA ***/
        if ($rws['estado'] == 1) {
            foreach ($rws['data'] as $key => $rw) {
                $est = 3;
                if ($rw['tb_cuotadetalle_est'] != 2)
                    break;
                $est = 2;
            }
        }

        $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', $est, 'INT'); //pago parcial
        /*** RECORRE TODAS LAS CUOTAS DETALLE Y SI ALGUNA NO ESTA CANCELADA LA CUOTA SE PONE COMO PAGO PARCIAL Y SI TODAS SON CANCELADAS, LA CUOTA SE CANCELA ***/

        //si se ha pagado todo el credito, cambia a estado LIQUIDADO
        $dts = $oCredito->mostrarUno($_POST['hdd_cre_id']);
        if ($dts['estado'] == 1) {
            $num_cuo = $dts['data']['tb_credito_numcuo']; //NUMERO DE CUOTAS TOTALES DEL CREDITO
        }
        $dts = NULL;

        //listamos las cuotas pagadas del crédito // CONTAMOS
        $dts = $oCuota->listar_cuotas_pagadas($_POST['hdd_cre_id'], $cre_tip);
        if ($dts['estado'] == 1) {
            $rows = count($dts['data']); //NUMERO DE CUOTAS PAGADAS
        }
        $dts = NULL;
        //si el numero de cuotas del credito e igual al numero de cuotas pagadas entonces el credito pasa a LIQUIDADO, est = 7
        if ($num_cuo == $rows) {
            $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 7, 'INT'); //cambiamos a liquidado al estado
            $liqui = ' | EL crédito ha pasado a liquidado';

            //registramos el historial
            $his = '<span style="color: green;">El crédito pasó a liquidado ya que se pagó la última cuota. | <b>' . date('d-m-Y h:i a') . '</b></span><br>';
            $oCredito->registro_historial($_POST['hdd_cre_id'], $his);
        }

        $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

        $data['cuopag_id'] = $cuopag_id;
        //$data['cuota_id'] = $cuota_id;
        $data['mod_id'] = $_POST['hdd_mod_id'];
        $data['action_cuotapago'] = 'pagar_fijo';
        $data['cuotadetalle_id'] = $_POST['hdd_modid'];
        $data['cuopag_msj'] = 'Se registró el pago correctamente.' . $abono_antiguo . ' ' . $dio_vuelto . $liqui;
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}

//SI EL CREDITO ES GARVEH O HIPO


if ($_POST['hdd_cuotatip_id'] == 3 && ($_POST['hdd_credito'] == "garveh" || $_POST['hdd_credito'] == "hipo")) {

    if (!empty($_POST['txt_cuopag_montot'])) {
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);
        $cuota_ultima = $_POST['hdd_cuota_ultima']; // 1 al pagarla por completo la desaparecemos de la lista, estado 2 directo

        $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: ' . trim($_SESSION['usuario_nom']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

        $xac = 1;
        $oCuotapago->tb_cuotapago_xac = 1;
        $oCuotapago->tb_cuotapago_usureg = $_SESSION['usuario_id'];
        $oCuotapago->tb_cuotapago_usumod = $_SESSION['usuario_id'];
        $oCuotapago->tb_modulo_id = $_POST['hdd_mod_id'];
        $oCuotapago->tb_cuotapago_modid = $_POST['hdd_modid'];
        $oCuotapago->tb_cuotapago_fec = $fecha;
        $oCuotapago->tb_moneda_id = $_POST['hdd_mon_id'];
        $oCuotapago->tb_cuotapago_mon = moneda_mysql($mon_pagado);
        $oCuotapago->tb_cuotapago_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
        $oCuotapago->tb_cuotapago_moncam = moneda_mysql(0);
        $oCuotapago->tb_cuotapago_pagcon = moneda_mysql($mon_pagado);
        $oCuotapago->tb_cliente_id = $_POST['hdd_cli_id'];
        $oCuotapago->tb_cuotapago_obs = $_POST['txt_cuopag_obs'];

        $result = $oCuotapago->insertar();
        if (intval($result['estado']) == 1) {
            $cuopag_id = $result['tb_cuotapago_id'];
        }
        $result = NULL;

        //estado de cuota   $ 8.57                     $ 210.12
        if (moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot)) {
            $data['cuopag_id'] = 0;
            $data['cuopag_msj'] = 'Para liquidar todo el crédito, por favor en la parte superior seleccione Refinanciar / Liquidar';

            echo json_encode($data);
            exit();
        }
        if (moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot)) {
            if (moneda_mysql($mon_pagado) < moneda_mysql($_POST['txt_cuopag_min'])) {
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_est', '3', 'INT');
                //cuando se genera un ingreso entonces la cuota de esa cuotadetalle le cambiamos a estado 3 en pago parcial
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 3, 'INT'); //pago parcial
                $monto = moneda_mysql($mon_pagado);
                if ($monto > 0){
                    $ingresar = 1;
                }
            }
            if (moneda_mysql($mon_pagado) >= moneda_mysql($_POST['txt_cuopag_min'])) {
                //antes cuando se pagaba mayor al minimo la cuota tenia estado 2, pero el cliente puede seguir avonando en esa misma cuota si es que su fecha aun no vence, entonces para que siga saliendo la cuota le damos estado 3 de pago parcial
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_est', '2', 'INT'); //SE CAMBIO EL ESTADO DE 3 A 2
                //cuando se genera un ingreso entonces la cuota de esa cuotadetalle le cambiamos a estado 3 en pago parcial
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT'); //pago parcial SE CAMBIO EL ESTADO DE 3 A 2
                //registro de mora
                $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
                if ($_POST['chk_mor_aut'] == 1)
                    $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_moraut', '1', 'INT');
                if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                    $data['cuota_id'] = $cuota_id;
                    $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_morest', '1', 'INT');
                    $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
                
                    $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                    //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                    if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                        $historial_detalle .= ' El usuario modificó la mora.';
                    }
                    $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                    $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                    $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                    $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                    $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                    $oHistorial->setTbHistDet($historial_detalle);
                    $oHistorial->insertar();
                }

                $monto = moneda_mysql($mon_pagado);
                if ($monto > 0)
                    $ingresar = 1;

                $capital = moneda_mysql($_POST['txt_cuopag_tot']) - moneda_mysql($mon_pagado);
                $amorti = moneda_mysql($_POST['txt_cuopag_montot']) - moneda_mysql($_POST['txt_cuopag_min']);
                //$generar_cuota=1;
                if ($cuota_ultima == 1) {
                    // es decir después de esta cuota hay más generadas ya, por ello es que hay que cambiarla de estado a 2 directamente
                    $oCuotadetalle->modificar_campo($_POST['hdd_modid'], 'tb_cuotadetalle_est', '2', 'INT');
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT');
                }

                if ($amorti > 0)
                    $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($amorti));
                $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($_POST['txt_cuopag_min']));
                $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']);
                //         if($capital > 0)
                //            $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($capital));
                //            $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'],moneda_mysql($_POST['txt_cuopag_min']));
                //            $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']);
                //
                //          //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
                //          //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
                //          $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3,'INT');
            }
        }

        if ($ingresar == 1) {
            //registro de ingreso
            $subcue_id = 0; //CUOTAS asiveh
            if ($_POST['hdd_credito'] == "garveh")
                $subcue_id = 3; //CUOTAS garveh
            if ($_POST['hdd_credito'] == "hipo")
                $subcue_id = 4; //CUOTAS hipo

            $cue_id = 1; //CANCEL CUOTAS
            $mod_id = 30; //cuota pago
            $ing_det = $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];

            if (!empty($_POST['txt_cuopag_obs']))
                $ing_det .= '. Obs: ' . $_POST['txt_cuopag_obs'];

            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuopag_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ing_det;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cue_id;
            $oIngreso->subcuenta_id = $subcue_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuopag_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_id = $result['ingreso_id'];
            }
            $result = NULL;
            
            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
            $oIngreso->banco_id = $_POST['cmb_banco_id'];
            $oIngreso->ingreso_id = $ingr_id;

            $oIngreso->registrar_datos_deposito_banco();

            /*$oIngreso->ingreso_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']); // FECHA DEPOSITO
            $oIngreso->ingreso_id = $ingreso_id; // ID DE INGRESO
            $oIngreso->registrar_tipocambio();*/
            $oIngreso->modificar_campo_nuevo($ingr_id, $_SESSION['usuario_id'], 'tipcam', moneda_mysql($_POST['txt_cuopag_tipcam']));

            $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: ' . $ing_det . ' ||&& ';
            $ingreso_detalle = $ing_det;
        }

        //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1)
            $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1; //sistema
        $mod_id = 30; //30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
        $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuopag_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc;
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = intval($_POST['hdd_mon_id']);
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $oEgreso->insertar();

        //registrarmos el ingreso del dinero en el tipo de moneda depositado
        $cue_id_ingre = 7; //cuenta de cancelar cuotas en deposito banco
        $subcue_id_ingre = 23; //CUOTAS menor
        if ($_POST['hdd_credito'] == "asiveh")
            $subcue_id_ingre = 22; //CUOTAS asiveh
        if ($_POST['hdd_credito'] == "garveh")
            $subcue_id_ingre = 24; //CUOTAS garveh
        if ($_POST['hdd_credito'] == "hipo")
            $subcue_id_ingre = 25; //CUOTAS hipo
        $numdoc = '';

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = $numdoc;
        $oIngreso->ingreso_det = $ingreso_detalle;
        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_mon']);
        $oIngreso->cuenta_id = $cue_id_ingre;
        $oIngreso->subcuenta_id = $subcue_id_ingre;
        $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
        $oIngreso->caja_id = 1;
        $oIngreso->moneda_id = intval($_POST['hdd_mon_iddep']);
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = 5;
        $oIngreso->ingreso_modide = $cuopag_id;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
            $ingr_id = $result['ingreso_id'];
        }
        $result = NULL;

        $oIngreso->modificar_campo($ingr_id, $_SESSION['usuario_id'], 'tb_ingreso_numdoc', $ingr_id, 'STR');

        //despues de ingresar el monto del banco, hay que egresarlo para no alterar la caja

        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1)
            $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1; //sistema
        $mod_id = 5; //el mod id sera 5 que indica deposito en banco
        $modide = $cuopag_id; //el modid será el id del cuotapago
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $ingr_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc;
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_mon']);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = intval($_POST['hdd_mon_iddep']);
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $oEgreso->insertar();

        //la generación de una cuota nueva se hace aparte mediante un botón en el form del pago

        $cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: ' . $egr_det . ' ||&& ';
        $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

        $data['cuopag_id'] = $cuopag_id;
        //$data['cuota_id'] = $cuota_id;
        $data['mod_id'] = $_POST['hdd_mod_id'];
        $data['action_cuotapago'] = 'pagar_libre';
        $data['cuopag_msj'] = 'Se registró el pago correctamente.';
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}


/* CUOTA FIJAS DE CREDITOS MENORES */

if ($_POST['hdd_cuotatip_id'] == 2 && $_POST['hdd_credito'] == "menor") { //cuotas FIJAS
    if (($monto_pagado_activado_campo > $cuopag_tot)) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO Mínimo, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_pagado_activado_campo) . ', monto mínimo: ' . (mostrar_moneda($cuopag_tot)) . ' pago_minimo_mostrado_sistema= ' . $pago_minimo_mostrado_sistema;
        echo json_encode($data);
        exit();
    }



    if (!empty($_POST['txt_cuopag_mon'])) {
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: ' . trim($_SESSION['usuario_nom']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

        $xac = 1;
        $oCuotapago->tb_cuotapago_xac = 1;
        $oCuotapago->tb_cuotapago_usureg = $_SESSION['usuario_id'];
        $oCuotapago->tb_cuotapago_usumod = $_SESSION['usuario_id'];
        $oCuotapago->tb_modulo_id = $_POST['hdd_mod_id'];
        $oCuotapago->tb_cuotapago_modid = $_POST['hdd_modid'];
        $oCuotapago->tb_cuotapago_fec = $fecha;
        $oCuotapago->tb_moneda_id = $_POST['hdd_mon_id'];
        $oCuotapago->tb_cuotapago_mon = moneda_mysql($mon_pagado);
        $oCuotapago->tb_cuotapago_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
        $oCuotapago->tb_cuotapago_moncam = 0;
        $oCuotapago->tb_cuotapago_pagcon = moneda_mysql($mon_pagado);
        $oCuotapago->tb_cliente_id = $_POST['hdd_cli_id'];
        $oCuotapago->tb_cuotapago_obs = $_POST['txt_cuopag_obs'];

        $result = $oCuotapago->insertar();
        if (intval($result['estado']) == 1) {
            $cuopag_id = $result['tb_cuotapago_id'];
        }
        $result = NULL;

        //estado de cuota
        if (moneda_mysql($mon_pagado) == moneda_mysql($cuopag_tot)) {
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', '2', 'INT');
            //registro de mora
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
            if ($_POST['chk_mor_aut'] == 1)
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_moraut', '1', 'INT');
            if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                $data['cuota_id'] = $cuota_id;
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_morest', '1', 'INT');
                $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
            
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }

            $monto = moneda_mysql($cuopag_tot);
            if ($monto > 0)
                $ingresar = 1;
        }

        if (moneda_mysql($mon_pagado) < moneda_mysql($cuopag_tot)) {
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', '3', 'INT');

            $monto = moneda_mysql($mon_pagado);
            if ($monto > 0)
                $ingresar = 1;
        }

        if (moneda_mysql($mon_pagado) > moneda_mysql($cuopag_tot)) {
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', '2', 'INT');
            //registro de mora
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
            if ($_POST['chk_mor_aut'] == 1)
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_moraut', '1', 'INT');
            if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                $data['cuota_id'] = $cuota_id;
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'morest', '1', 'INT');
                $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
            
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }

            $monto = moneda_mysql($cuopag_tot);
            if ($monto > 0)
                $ingresar = 1;

            $abono = moneda_mysql($mon_pagado) - moneda_mysql($cuopag_tot);
            if ($abono > 0)
                $abonar = 1;
        }


        if ($ingresar == 1) {
            //registro de ingreso
            $cue_id = 1; //CANCEL CUOTAS
            $subcue_id = 2; //CUOTAS menor
            $mod_id = 30; //cuota pago
            $ing_det = $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
            if (!empty($_POST['txt_cuopag_obs']))
                $ing_det .= '. Obs: ' . $_POST['txt_cuopag_obs'];
            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuopag_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ing_det;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cue_id;
            $oIngreso->subcuenta_id = $subcue_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuopag_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_id = $result['ingreso_id'];
            }
            $result = NULL;

            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
            $oIngreso->banco_id = $_POST['cmb_banco_id'];
            $oIngreso->ingreso_id = $ingr_id;
            $oIngreso->registrar_datos_deposito_banco();

            $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: ' . $ing_det . ' ||&& ';

            $line_det = '<b>' . $_SESSION['usuario_primernom'] . '</b> ha hecho la transacción: ' . $ing_det . '. && <b>' . date('d-m-Y h:i a') . '</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $line_det;
            $oLineaTiempo->insertar();
        }

        if ($abonar == 1) {
            //abono cliente caja
            $oClientecaja->tb_clientecaja_xac = 1;
            $oClientecaja->tb_cliente_id = $_POST['hdd_cli_id'];
            $oClientecaja->tb_credito_id = $_POST['hdd_cre_id'];
            $oClientecaja->tb_clientecaja_tip = $clicaj_tip;
            $oClientecaja->tb_clientecaja_fec = $fecha;
            $oClientecaja->tb_moneda_id = $_POST['hdd_mon_id'];
            $oClientecaja->tb_clientecaja_mon = moneda_mysql($abono);
            $oClientecaja->tb_modulo_id = $mod_id;
            $oClientecaja->tb_clientecaja_modid = $cuopag_id;
            //abono cliente caja
            $result1 = $oClientecaja->insertar();
            if (intval($result['estado']) == 1) {
                $clicaj_id = $result['tb_clientecaja_id'];
            }

            $cue_id = 21; //saldo a favor
            $subcue_id = 62; //saldo a favor
            $mod_id = 40; //cliente caja
            $ing_det = "ABONO CLIENTE " . $_POST['hdd_pag_det'];
            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $clicaj_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ing_det;
            $oIngreso->ingreso_imp = moneda_mysql($abono);
            $oIngreso->cuenta_id = $cue_id;
            $oIngreso->subcuenta_id = $subcue_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $clicaj_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_abono_id = $result['ingreso_id'];
            }
            $result = NULL;
            
            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
            $oIngreso->banco_id = $_POST['cmb_banco_id'];
            $oIngreso->ingreso_id = $ingr_abono_id;

            $oIngreso->registrar_datos_deposito_banco();

            $cuopag_his .= 'El ingreso generado fue mayor a la cuota facturada, por eso a la Caja Cliente fue un saldo a favor de: S/. ' . mostrar_moneda($abono) . ' ||&& ';
        }
        //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1)
            $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1; //sistema
        $mod_id = 30; //30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
        $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuopag_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc;
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = $_POST['hdd_mon_id'];
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $oEgreso->insertar();

        $cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: ' . $egr_det . ' ||&& ';

        //verificamos si es el ultimo pago, si es el ultimo pago verificamos que se haya pagado todo, para hacer el pedido de todas las garantias vinculadas a ese credito
        $pedidos = '';
        if ($_POST['hdd_ultimo_pago'] == 1) {
            //ultimo pago 0 aun no, 1 si es ultimo pago, verificamos si en el ultimo pago cancela todo el monto, de ser asi se piden las garantias sino se vuelve esperar el ultimo pago
            if (moneda_mysql($mon_pagado) >= moneda_mysql($cuopag_tot)) {

                $cuopag_his .= 'Este pago fue el último, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrea de las garantías. ||&& ';

                //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
                $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 7, 'INT');

                //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
                $dts = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito
            //if($dts['estado']==1){
                $rows = count($dts['data']);
            //}
                if ($dts['estado'] == 1) {

                    $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE ' . $rows . '. REVISE EN INVENTARIO';
                    foreach ($dts['data']as $key => $dt) {
                        $gar_id = $dt['tb_garantia_id'];
                        $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20
                        $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

                        $det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho el pedido de la garantía: <b>' . $dt['tb_garantia_pro'] . '</b> | ' . date('d-m-Y h:i a');
                        $pedido = '<strong style="color: blue;">Pedido por: ' . $_SESSION['usuario_nom'] . ' a VICTOR MANUEL | ' . date('d-m-Y h:i a') . '</strong><br>';

                        //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
                        $dts1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');
                        $num_rows = count($dts1['data']);
                    

                        if ($num_rows == 0) {
                            //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
                            $oTarea->tb_tarea_usureg = $_SESSION['usuario_id'];
                            $oTarea->tb_tarea_det = $det;
                            $oTarea->tb_usuario_id = $usu_id;
                            $oTarea->tb_tarea_usutip = $usu_tip;
                            $oTarea->tb_garantia_id = $gar_id;
                            $oTarea->tb_tarea_tipo = 'enviar';
                            $oTarea->insertar();

                            //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
                            $oGarantia->modificar_campo($gar_id, 'tb_garantia_ped', 1, 'INT');

                            //registramos al usuario que pide la garantía
                            $oGarantia->estado_usuario_pedido($gar_id, $pedido);
                        }
                    }
                } else
                    $pedidos = ' IPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';

            }
        }
        $oCuotapago->tb_cuotapago_id = $cuopag_id;
        $oCuotapago->tb_cuotapago_his = $cuopag_his;
        $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

        $data['cuopag_id'] = $cuopag_id;
        $data['cuopag_msj'] = 'Se registró el pago correctamente.' . $pedidos;
        $data['mod_id'] = $_POST['hdd_mod_id'];
        $data['action_cuotapago'] = 'pagar_fijo';
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}

/* PAGO DE CRÉDITO MENOR EN CUOTAS LIBRES */

if ($_POST['hdd_cuotatip_id'] == 1 && $_POST['hdd_credito'] == "menor") { //cuotas LIBRES       
    if ($vencida == 1 && ($monto_pagado_activado_campo > $pago_minimo_mostrado_sistema)) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO Mínimo, no puede pagar mas de lo depositado por que su cuota esta Vencida. Monto ingresado: ' . gettype($monto_pagado_activado_campo) . ', monto mínimo: ' . gettype($pago_minimo);
        echo json_encode($data);
        exit();
    }

    if (!empty($_POST['txt_cuopag_mon'])) {
        $mon_pagado = formato_numero($_POST['txt_cuopag_montot']); //monto saldo que paga el cliente despues de aplicar la comision del banco
        $cuopag_tot = formato_numero($cuota_cap + $pago_minimo_mostrado_sistema);
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: ' . trim($_SESSION['usuario_nom']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

        $pedidos = '';
        
        //estado de cuota  30.00                        399.00
        if ($mon_pagado >= $cuopag_tot) {
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT');
            //registro de mora
            $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
            if ($_POST['chk_mor_aut'] == 1){
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_moraut', 1, 'INT');
            }
            if (formato_moneda($_POST['txt_cuopag_mor']) > 0) {
                $data['cuota_id'] = $cuota_id;
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_morest', 1, 'INT');
                $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
            
                $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                    $historial_detalle .= ' El usuario modificó la mora.';
                }
                $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                $oHistorial->setTbHistDet($historial_detalle);
                $oHistorial->insertar();
            }

            $monto = $mon_pagado;
            if ($monto > 0){
                $ingresar = 1;
            }

            $cuopag_his .= 'Este pago fue igual al monto facturado de la cuota, por ende el crédito quedó cancelado en su totalidad y se procedió a la entrega de las garantías. ||&& ';

            //se ha pagado todo el credito, entonces el credito pasa a un estado 7 que es LIQUIDADO ESPERA DE ENTREGA
            $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 7, 'INT'); // se cambio el 2 por el 7
            //es el ultimo pago y está pagado todo, entonces procedemos al pedido de todas las garantias del credito
            $dts = $oGarantia->filtrar($_POST['hdd_cre_id']); //listar todas las garantias del credito
            $rows = count($dts['data']);

            if ($rows > 0) {

                $pedidos = ' SE HAN PEDIDO LAS GARANTIAS, TOTAL DE ' . $rows . '. REVISE EN INVENTARIO';
                foreach ($dts['data']as $key => $dt) {
                    $gar_id = $dt['tb_garantia_id'];
                    $usu_id = 20; //por defecto la tarea va a VICTOR MANUEL id = 20
                    $usu_tip = 2; //si se hace un pedido a almacén, el usuaro almacenero es de tipo 2 y su mos también

                    $det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho el pedido de la garantía: <b>' . str_replace("'", "", $dt['tb_garantia_pro']) . '</b> | ' . date('d-m-Y h:i a');
                    $pedido = '<strong style="color: blue;">Pedido por: ' . $_SESSION['usuario_nom'] . ' a VICTOR MANUEL | ' . date('d-m-Y h:i a') . '</strong><br>';

                    //verificar que no se haya pedido la misma garantia, estado 0 (pendiente), tipo tarea = 'enviar'
                    $dts1 = $oTarea->filtrar_tarea_estado_tipo($gar_id, 0, 'enviar');
                    $num_rows = count($dts1['data']);
                    /* $num_rows = mysql_num_rows($dts1);
                      mysql_free_result($dts1); */

                    if ($num_rows == 0) {
                        $oTarea->tb_tarea_usureg = $_SESSION['usuario_id'];
                        $oTarea->tb_tarea_det = $det;
                        $oTarea->tb_usuario_id = $usu_id;
                        $oTarea->tb_tarea_usutip = $usu_tip;
                        $oTarea->tb_garantia_id = $gar_id;
                        $oTarea->tb_tarea_tipo = 'enviar';
                        //ejem: pedro hace el pedido de garantia a victor, entonces victor tiene la tarea de enviar
                        $oTarea->insertar();
                        //cambiamos el estado_pedido de la garantia a pedido, que es el estado 1
                        $oGarantia->modificar_campo($gar_id, 'tb_garantia_ped', 1, 'INT');

                        //registramos al usuario que pide la garantía
                        $oGarantia->estado_usuario_pedido($gar_id, $pedido);
                    }
                }
            } 
            else
                $pedidos = ' IPORTANTE, SE HA LIQUIDADO TODO EL CREDITO PERO NO SE HA ENCONTRADO SUS GARANTIAS, REPORTE ESTE MENSAJE';

            // CUANDO SE LIQUIDE DESDE EL MODAL DE PAGO CUOTA YA NO SE DEBE CREAR CUOTA NUEVA
            $data['cuota_nueva'] = 0; //ya no sea crea nueva cuota
        }
        
        //                  30.00                           399.00
        if ($mon_pagado < $cuopag_tot) {
            
            // $data['cuopag_id'] = 0;
            // $data['cuopag_msj'] = 'mon_pagado: '.$mon_pagado.' / $vencida= '.$vencida.' $fechahoy= '.$fechahoy.' $cuopag_tot1 = '.$cuopag_tot1 .' $pago_minimo= '.$pago_minimo.' $pago_minimo_mostrado_sistema='.$pago_minimo_mostrado_sistema;
            // echo json_encode($data);
            // exit();
                                    
            if ($mon_pagado < $pago_minimo_mostrado_sistema) {
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', '3', 'INT');

                $monto = formato_moneda($mon_pagado);
                if ($monto > 0){
                    $ingresar = 1;
                }
            }
            if ($mon_pagado >= $pago_minimo_mostrado_sistema) {
                //si el pago es mayor que el minimo aun no toma por cancelada la cuota, ya que se tiene que verificar si la fecha de pago ya venció. SI ya venció se genera una nueva cuota y esta cambia a 2 si es que se pagó el mínimo
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_est', 2, 'INT');
                /* SE MODIFICO EL NUMERO 3 POR 2 */
                //registro de mora
                $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_mor', moneda_mysql($_POST['txt_cuopag_mor']), 'STR');
                if ($_POST['chk_mor_aut'] == 1){
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_moraut', 1, 'INT');
                }
                if (moneda_mysql($_POST['txt_cuopag_mor']) > 0) {
                    $oCuota->modificar_campo($_POST['hdd_cuo_id'], 'tb_cuota_morest', 1, 'INT');
                    $cuopag_his .= 'El pago se hizo fuera de la fecha, por ende se le generó una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ||&& ';
                
                    $historial_detalle = 'El pago se hizo fuera de la fecha, por ende el sistema le sugirió una mora de S/. ' . mostrar_moneda($_POST['hdd_morasugerida']) . '. ';
                    //$usuario = $oUsuario->mostrarUno($_SESSION['usuario_id']);
                    if (intval($_POST['hdd_chk_mor_aut']) == 1) {
                        $historial_detalle .= ' El usuario modificó la mora.';
                    }
                    $historial_detalle .= ' Se registtró una mora de S/. ' . mostrar_moneda($_POST['txt_cuopag_mor']) . ' ';
                    $historial_detalle .= '<strong>Comentario :</strong> ' . $_POST['txt_coment'];
                    $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                    $oHistorial->setTbHistNomTabla("tb_cuotadetalle");
                    $oHistorial->setTbHistRegmodid($_POST['hdd_modid']);
                    $oHistorial->setTbHistDet($historial_detalle);
                    $oHistorial->insertar();
                }

                $monto = $mon_pagado;
                if ($monto > 0)
                    $ingresar = 1;

                //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
                //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
                //$oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3,'INT');

                $generar_cuota = 1;
                //agregamos el monto de amortización que se hace al capital
                $amorti = formato_moneda($_POST['txt_cuopag_mon']) - formato_moneda($pago_minimo);
                if ($amorti > 0)
                    $oCuota->agregar_amortizacion_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($amorti));
                $suma_pago_actual_y_pagos_previos = $pago_minimo + $pagos;

                $oCuota->modificar_interes_cuota_libre($_POST['hdd_cuo_id'], moneda_mysql($suma_pago_actual_y_pagos_previos));
                $oCuota->modificar_cuota_libre($_POST['hdd_cuo_id']); //tb_cuota_cuo = a la suma del capital + pagos hechos
                //las garantías pueden estar en estado de remate, si el cliente paga su cuota y mora entonces sus credito vuelve a pasar a vigente, por ello
                //actualizamos el estado a vigente del credito en el caso esté en remate o sino seguiría estando en vigente
                $oCredito->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_est', 3, 'INT');
            }

            // CUALQUIER PAGO MENOR A LA CUOTA TOTAL DEBE CREAR CUOTA NUEVA
            $data['cuota_nueva'] = 1; //1 cuota nueva
        }
        if ($ingresar == 1) {
            //registro de ingreso
            $xac = 1;
            $oCuotapago->tb_cuotapago_xac = $xac;
            $oCuotapago->tb_cuotapago_usureg = $_SESSION['usuario_id'];
            $oCuotapago->tb_cuotapago_usumod = $_SESSION['usuario_id'];
            $oCuotapago->tb_modulo_id = $_POST['hdd_mod_id'];
            $oCuotapago->tb_cuotapago_modid = $_POST['hdd_modid'];
            $oCuotapago->tb_cuotapago_fec = $fecha;
            $oCuotapago->tb_moneda_id = $_POST['hdd_mon_id'];
            $oCuotapago->tb_cuotapago_mon = moneda_mysql($mon_pagado);
            $oCuotapago->tb_cuotapago_tipcam = moneda_mysql($_POST['txt_cuopag_tipcam']);
            $oCuotapago->tb_cuotapago_moncam = moneda_mysql(0);
            $oCuotapago->tb_cuotapago_pagcon = moneda_mysql($mon_pagado);
            $oCuotapago->tb_cliente_id = $_POST['hdd_cli_id'];
            $oCuotapago->tb_cuotapago_obs = $_POST['txt_cuopag_obs'];

            $result = $oCuotapago->insertar();
            if (intval($result['estado']) == 1) {
                $cuopag_id = $result['tb_cuotapago_id'];
            }
            $result = NULL;

            $cue_id = 1; //CANCEL CUOTAS
            $subcue_id = 2; //CUOTAS menor
            $mod_id = 30; //cuota pago
            $ing_det = $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
            if (!empty($_POST['txt_cuopag_obs'])) {
                $ing_det .= '. Obs: ' . $_POST['txt_cuopag_obs'];
            }
            $numdoc = $_POST['hdd_mod_id'] . '-' . $_POST['hdd_modid'] . '-' . $mod_id . '-' . $cuopag_id;

            $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
            $oIngreso->ingreso_fec = $fecha;
            $oIngreso->documento_id = 8;
            $oIngreso->ingreso_numdoc = $numdoc;
            $oIngreso->ingreso_det = $ing_det;
            $oIngreso->ingreso_imp = moneda_mysql($monto);
            $oIngreso->cuenta_id = $cue_id;
            $oIngreso->subcuenta_id = $subcue_id;
            $oIngreso->cliente_id = intval($_POST['hdd_cli_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = $_POST['hdd_mon_id'];
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = $mod_id;
            $oIngreso->ingreso_modide = $cuopag_id;
            $oIngreso->empresa_id = $_SESSION['empresa_id'];

            $result = $oIngreso->insertar();
            if (intval($result['estado']) == 1) {
                $ingr_id = $result['ingreso_id'];
            }
            $result = NULL;
            
            $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
            $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
            $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
            $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
            $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
            $oIngreso->banco_id = $_POST['cmb_banco_id'];
            $oIngreso->ingreso_id = $ingr_id;

            $oIngreso->registrar_datos_deposito_banco();

            $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: ' . $ing_det . ' ||&& ';
        }

        //generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
        $subcue_id = 72; //72 es la subcuenta de cuenta en dolares
        if ($_POST['hdd_mon_iddep'] == 1)
            $subcue_id = 71; // 71 es cuenta en soles

        $xac = 1;
        $cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
        $pro_id = 1; //sistema
        $mod_id = 30; //30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
        $modide = $cuopag_id; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
        $egr_det = 'EGRESO POR: ' . $_POST['hdd_pag_det'] . '. Pago abonado en el banco, en la siguente cuenta: ' . $_POST['hdd_cuenta_dep'] . ', la fecha de depósito fue: ' . $_POST['txt_cuopag_fecdep'] . ', la fecha de validación fue: ' . $_POST['txt_cuopag_fec'] . ', el N° de operación fue: ' . $_POST['txt_cuopag_numope'] . ', el monto depositado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ', la comisión del banco fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_comi'] . ', el monto validado fue: ' . $moneda_depo . ' ' . $_POST['txt_cuopag_monval'] . ' y con cambio de moneda el monto pagado a la cuota fue: ' . $moneda_cred . ' ' . $_POST['txt_cuopag_montot'];
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        $numdoc = $mod_id . '-' . $_POST['hdd_modid'] . '-' . $cuopag_id;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = $numdoc;
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($mon_pagado);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = $_POST['hdd_mon_id'];
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $oEgreso->insertar();

        $cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: ' . $egr_det . ' ||&& ';
        $oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

        $data['cuopag_id'] = $cuopag_id;
        //$data['cuota_id'] = $cuota_id;
        $data['mod_id'] = $_POST['hdd_mod_id'];
        $data['cuopag_msj'] = 'Se registró el pago correctamente.' . $pedidos;
        $data['action_cuotapago'] = 'pagar_fijo';
    } else {
        $data['cuopag_msj'] = 'Intentelo nuevamente.';
    }
    echo json_encode($data);
}
?>
