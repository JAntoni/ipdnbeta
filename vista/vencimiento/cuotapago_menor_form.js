/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $("#notas").hide();
    mostrar_nota_gc();

    obtener_morasugerida()

    if($("#hdd_vencida").val()==1 && $("#hdd_morasugerida").val()==0 ){
        alerta_warning("NO SE HA REGISTRADO MORA SUGERIDA");
    }

    mostrar_notas()

    var pc_nom = $('#hdd_vista_pc_nombre').val();
    var pc_mac = $('#hdd_vista_pc_mac').val();
    $('#hdd_pc_nombre').val(pc_nom);
    $('#hdd_pc_mac').val(pc_mac);
    $('.coment').hide();

    /*CALCULAR LA MORA DEL CREDITO*/
    //calcular_mora(); la mora ahora se asiganará a criterio

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });
    $('.moneda3').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });
    $('#txt_cuopag_mor').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });



    /*FORMATO DE FECHA PARA QUE NO SEA MAYOR A LA ACTUAL*/
    $('#ingreso_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    }).on('changeDate', function(selected){
        console.log(selected);
        obtener_morasugerida();
    });

    $( "#txt_cuopag_mor" ).blur(function(e) {
        let hddmorasugerida = parseFloat($("#hdd_morasugerida").val());
        let moraingresada = parseFloat($('#txt_cuopag_mor').autoNumeric('get'));
        if(moraingresada > hddmorasugerida && hddmorasugerida>0){
            alerta_warning("NO PUEDE INGRESAR UNA MORA MAYOR A LA SUGERIDA");
            $('#txt_cuopag_mor').val(hddmorasugerida.toFixed(2));
        }
    })

    $("#for_cuopag").validate({
        submitHandler: function () {
            if (parseInt($('#hdd_chk_mor_aut').val()) == 1 && $.trim($('#txt_coment').val()) === '') {
                // El textarea está vacío
                alerta_warning('El campo Comentario no puede estar vacío, si editó la mora');
            }
            $.ajax({
                type: "POST",
                url: VISTA_URL + "vencimiento/cuotapago_menor_reg.php",
                async: true,
                dataType: "json",
                data: $("#for_cuopag").serialize(),
                beforeSend: function () {

                },
                success: function (data) {
                    if (parseInt(data.cuopag_id) > 0) {
                        $('#modal_registro_vencimientomenor').modal('hide');
//                            if(confirm('¿Desea imprimir?'))
//                            cuotapago_imppos_datos_2(data.cuopag_id);


                        $('#msj_vencimiento').hide();
                        $('#msj_cuopag_menor').html(data.cuopag_msj);
                        swal_success('SISTEMA', data.cuopag_msj, 2000);
                        var vista = $("#hdd_vista").val();

                        // if (vista == "cmb_ven_id") {
                        //     vista + "." + (data.ven_id);
                        // }
                        var tipo = $("#action_cuotapago").val();
                        var cuota_nueva = parseInt(data.cuota_nueva); // no se crea nueva cuota, 1 se crea una nueva
                        if (cuota_nueva == 1) {
                            generar_nueva_cuota();
                        }

                        if (vista == "vencimiento_tabla") {
                            vencimiento_tabla();
                        }

                        var codigo = data.cuopag_id;
                        window.open("http://www.ipdnsac.com/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id=" + codigo);
//                            window.open("http://localhost/ipdnsac/vista/creditomenor/creditomenor_ticket.php?cuotapago_id="+codigo);

                        swal_success('SISTEMA', data.cuopag_msj, 5000);
                        console.log('tipo = ' + data.cuopag_msj);

                        if(parseInt(data.cuota_id)>0){
                            setTimeout(function() {
                                vencimiento_tabla();
                                morapago_menor_form(data.action_cuotapago,data.cuota_id);
                            }, 3000); // 3000 milisegundos = 3 segundos de retraso
                        }else{
                            vencimiento_tabla();
                        }

                    } else {
                        swal_warning("AVISO", data.cuopag_msj, 3500);
                    }
                },
                complete: function (data) {
                    if (data.statusText != 'success') {
                        $('#btn_guar_cuopag').show();
                    }
                }
            });
        },
        rules: {
            txt_cuopag_mon: {
                required: true
            }
        },
        messages: {
            txt_cuopag_mon: {
                required: '* ingrese un monto'
            }
        }
    });





});

function Activar_input_monto_pago_banco() {
    $('#txt_cuopag_mon').attr("readonly", false);
}

function usarSaldo() {
    var num = Number($('#txt_clicaj_tot').autoNumeric('get'));//3516.74
    var cuo = Number($('#txt_cuodet_cuo').autoNumeric('get'));/* cuota total que se va a pagar */


//    swal_success("num = "+num*1,"cuo = "+cuo*1,8000);
//    return false;

    if (num <= cuo)
    {
        $('#txt_cuopag_clicaj').autoNumeric('set', num.toFixed(2));

    } else
    {
        $('#txt_cuopag_clicaj').autoNumeric('set', cuo.toFixed(2));
    }

    calculo();
}

function calculo() {
    var cuota = Number($('#txt_cuodet_cuo').autoNumeric('get'));/* cuota total que se va a pagar */
    var pagos_parciales = Number($('#txt_cuopag_pagpar').autoNumeric('get'));/* Pagos Parciales que se han realizados*/
    var pagpar = 0;
    var cuota_parcial_caja_cliente = Number($('#txt_cuopag_clicaj').autoNumeric('get'));
    var clicaj = 0;

    if (pagos_parciales > 0) {
        pagpar = pagos_parciales.toFixed(2);
    }

    if (cuota_parcial_caja_cliente > 0) {
        clicaj = cuota_parcial_caja_cliente.toFixed(2);
    }

    var resta1 = cuota - pagpar;
    resta1 = resta1.toFixed(2);

    var total = resta1 - clicaj;
    total = total.toFixed(2);

    if (resta1 >= clicaj){
        $('#txt_cuopag_tot').autoNumeric('set', total);
        $('#txt_cuopag_mon').autoNumeric('set', total);
    } else{
        $('#txt_cuopag_clicaj').autoNumeric('set', resta1);
        $('#txt_cuopag_tot').val('0.00');
        $('#txt_cuopag_mon').val('0.00');
    }
}

function calcular_mora() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/cuotapago_menor_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: "calcular_mora",
            fechaPago: $('#txt_cuopag_fec').val(),
            fecha: $("#hdd_cuo_fec").val(),
            cre_preaco: $('#hdd_cre_preaco').val()
        }),
        success: function (data) {
            $('#txt_cuopag_mor').val(data.mora);
            if (parseInt(data.estado) == 0) {
                //no existe tarifario para esas fechas
                $('#msj_menor_form').html(data.msj);
                $('#msj_menor_form').show();
            }
        },
        complete: function (data) {
        }
    });
}


function comprobar() {
    var input = $('#txt_cuopag_mor');
    if (input.is('[readonly]')) {
        input.removeAttr('readonly');
        $('#hdd_chk_mor_aut').val(1);
        $('.coment').show();
    } else {
        let hddmorasugerida = parseFloat($("#hdd_morasugerida").val());
        $('#txt_cuopag_mor').val(hddmorasugerida.toFixed(2));
        $('#hdd_chk_mor_aut').val(0);
        $('.coment').hide();

        input.attr('readonly', true);
    }
}


function Editar() {
    $('#txt_cuopag_por').attr("readonly", false);
}


//function comprobar(obj) {
//    if (obj.checked) {
//        $("#txt_cuopag_mor").attr("readonly", false);
//    } else {
//        $("#txt_cuopag_mor").attr("readonly", true);
//    }
//}
//function Editar(obj) {
//    if (obj.checked) {
//        $("#txt_cuopag_por").attr("readonly", false);
//    } else {
//        $("#txt_cuopag_por").attr("readonly", true);
//    }
//}

function generar_nueva_cuota() {

    var vencida = $('#hdd_vencida').val();
    var cuo_num = $('#hdd_cuo_num').val();
    var opc = $('#hdd_var_op').val();
    var fecha;
    
    if (vencida == 1 || cuo_num == 1) {
        fecha = $('#hdd_cuo_fec').val();
    } else {
        if (opc == 1) {
            fecha = $('#hdd_cuo_fec').val();
        } else {
            fecha = $('#txt_cuopag_fec').val();
        }
    }

    console.log('fechaaaaa: ' + fecha);

    $.ajax({
        type: "POST",
        url: VISTA_URL + "vencimiento/cuotapago_menor_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: "cuota_nueva",
            cre_id: $('#hdd_cre_id').val(),
            cuo_id: $('#hdd_cuo_id').val(),
            int: $('#hdd_cre_int').val(),
            cuo_max: $('#hdd_cuo_max').val(),
            fecha: fecha,
            opc: opc,
            mon_id: $('#hdd_mon_id').val()
        }),
        beforeSend: function () {
            $('#msj_vencimiento').html("Guardando...");
            $('#msj_vencimiento').show(100);
        },
        success: function (data) {
            console.log('estado = ' + data.estado);
            if (data.estado == 1) {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_success('SISTEMA', data.msj, 2000);
                $('#msj_vencimiento').hide();
//        var tipo=$("#action_cuotapago").val();
//            if(tipo=="pagar_fijo")
//            vencimiento_tabla();
            } else if (data.estado == 2) {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_warning('AVISO', data.msj, 5000);
                $('#msj_vencimiento').hide();
            } else {
                $('#modal_registro_vencimientomenor').modal('hide');
                swal_warning('AVISO', 'Error al momento de generar nueva cuota', 5000);
            }
        },
        complete: function (data) {
            if (data.statusText != 'success') {
            }
            console.log(data);
        }
    });
}


function morapago_menor_form(act,idf){

    Swal.fire({
      title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
      showDenyButton: true,
      denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
      confirmButtonColor: '#3b5998', 
      denyButtonColor: '#21ba45',
    }).then((result) => {
  
      if (result.isConfirmed) {
        Swal.fire(
          '¡Has elegido pagar en Oficina!',
          '',
          'success'
        )
        cuotamorapago_menor_form(act,idf);
      } else if (result.isDenied) {
        Swal.fire(
          '¡Has elegido pagar en Banco!',
          '',
          'success'
        )
        morapago_banco('menor', idf);

      }
    });
}


function morapago_banco(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_banco.php",
    async:true,
    dataType: "html",                    
    data: ({
      credito: cred,
      cuota_id: idf,
      vista: 'vencimiento_tabla'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function cuotamorapago_menor_form(cred, idf){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranzamora/morapago_menor_form.php",
    async:true,
    dataType: "html",                    
    data: ({
        action: cred,
        cuo_id:	idf,
        vista:	'vencimiento_tabla'
    }),
    beforeSend: function() {
        
    },
    success: function(html){
        $('#modal_mensaje').modal('hide');
        $('#div_modal_cobranzamora').html(html);
        $('#modal_registro_cobranzamora').modal('show');
        modal_height_auto('modal_registro_cobranzamora');
        modal_hidden_bs_modal('modal_registro_cobranzamora', 'limpiar'); //funcion encontrada en public/js/generales.js 
    },
    complete: function (html) {
//        console.log(html);
    }
  });
}

function obtener_morasugerida() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "morasugerida/morasugerida_controller.php",
        async: false,
        dataType: "json",
        data: ({
            action_morasugerida: "obtener",
            monto: $('#hdd_monto').val(),
            fecha_pago: $('#txt_cuopag_fec').val(),
            cuota_fecha: $('#hdd_fec_ven').val(),
            tipo_credito: $('#hdd_tipo_credito').val()
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            $('#hdd_morasugerida').val(data.morasugerida);
            $('#txt_cuopag_mor').val(data.morasugerida);
        },
        complete: function () {

        }
    });
}

function ocultar_nota(notacliente_id) {
    console.log(notacliente_id);
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "update_state",
            nota_id: notacliente_id,
            estado: 0,
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                alerta_success("CORRECTO", data.clientenota_msj)
                mostrar_notas()
            }else{
                alerta_error("ERROR", data.clientenota_msj);
            }
        },
        complete: function () {

        }
    });
  }

function mostrar_notas(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "ver_notas",
            cliente_id: $("#hdd_cli_id").val(),
            credito_id: $("#hdd_cre_id").val(),
            tipo_credito: $("#hdd_tipo_credito").val(),
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                $('#body_notas').html(data.html);
                $("#notas").show();
            }else{
                //$("#notas").hide();
            }
        },
        complete: function () {

        }
    });
  }

  function mostrar_nota_gc(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/clientenota_reg.php",
        async: false,
        dataType: "json",
        data: ({
            action: "ver_nota_gc",
            cliente_id: $("#hdd_cli_id").val(),
            credito_id: $("#hdd_cre_id").val(),
            tipo_credito: $("#hdd_tipo_credito").val(),
        }),
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.estado == 1){
                $('#nota_gc').html(data.html);
                $("#notas").show();
            }else{
                //$("#notas").hide();
            }
        },
        complete: function () {

        }
    });
  }