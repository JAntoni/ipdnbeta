$(document).ready(function () {
  console.log('Venc Comple 44')
});

function vencimiento_completa_tabla() {
  var cliente_id = $('#hdd_fil_cli_id').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vencimiento/vencimiento_completa_tabla.php",
    async: true,
    dataType: "html",
    data:{
      cliente_id: cliente_id
    },
    beforeSend: function () {
      $('#msj_vencimiento_tabla').html("Cargando datos...");
      $('#msj_vencimiento_tabla').show(100);
      $('#div_vencimiento_tabla').addClass("ui-state-disabled");

      $('#btnSearchComplete').prop('disabled', true);
      $('#btnSearch').prop('disabled', true);
      //$('#div_vencimiento_tabla').hide();
      //$('#div_vencimiento_tabla').html('');
    },
    success: function (html) {
      $('#div_vencimiento_tabla').show(500);
      $('#div_vencimiento_tabla').html(html);
    },
    complete: function () {
      $('#btnSearchComplete').prop('disabled', false);
      $('#btnSearch').prop('disabled', false);
      $('#div_vencimiento_tabla').removeClass("ui-state-disabled");
      $('#msj_vencimiento_tabla').hide(100);
    }
  });
}

function garantia_estado(credito_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"garantia/garantia_estado.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: credito_id
    }),
    beforeSend: function() {
      $('#body_modal_general').html('');
      $('#general_title').text('Estado de los Productos');
      $('#modal_general').modal('show');
    },
    success: function(data){
      $('#body_modal_general').html(data);
      //funcion js para agregar un largo automatico al modal, al abrirlo
    },
    complete: function(data){

    },
    error: function(data){
      alerta_error('ERRROR', data.responseText)
    }
  });
}

function creditolinea_timeline(creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditolinea/creditolinea_timeline.php",
    async: true,
    dataType: "html",
    data: ({
      creditotipo_id: 1,
      credito_id: creditomenor_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_creditolinea_timeline').html(data);
      $('#modal_creditolinea_timeline').modal('show');
      modal_height_auto('modal_creditolinea_timeline'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_creditolinea_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}