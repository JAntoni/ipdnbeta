<?php
require_once('../../core/usuario_sesion.php');

require_once("Vencimiento.class.php");
$oVencimiento = new Vencimiento();
require_once("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota;
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../cobranza/Cobranza.class.php");
$oCobranza = new Cobranza;
require_once("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ("../morasugerida/Morasugerida.class.php");
$oMorasugerida = new Morasugerida();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

if ($_POST['action'] == "pagar_fijo" or $_POST['action'] == "pagar_libre") {

  $fecha_hoy = date('d-m-Y');

  //? 1. OBTENEMOS TODOS LOS DATOS DE LA CUOTADETALLE A PAGAR, LOS CRÉDITOS VEHICULARES TIENEN CUOTADETALLE Y LOS C-MENORES SOLO CUOTAS
  $result = $oCuotadetalle->mostrarUno($_POST['cuodet_id']);
    if ($result['estado'] == 1) {
      $cuo_id = $result['data']['tb_cuota_id'];
      $cuodet_fec = mostrar_fecha($result['data']['tb_cuotadetalle_fec']);
      $mon_id = $result['data']['tb_moneda_id'];
      $mon_nom = $result['data']['tb_moneda_nom'];
      $cuodet_num = $result['data']['tb_cuotadetalle_num'];
      $cuodet_cuo = $result['data']['tb_cuotadetalle_cuo'];
      $cuodet_acupag = $result['data']['tb_cuotadetalle_acupag']; //si es 1-> es un acuerdo de pago
      $cuodet_per = $result['data']['tb_cuotadetalle_per']; //permiso para poder pagarla si está vencida, solo el admin pude cambiar este permiso, 0 no tienen permisos, 1 si pueden pagarla
    }
  $result = NULL;

  //verificar si la fecha hoy es mayor o igual a la fecha de vencimiento
  $vencida = 0;
  $bandera = "";
  $diasvencidos = "";
  $readonly = '';
  if (strtotime($fecha_hoy) >= strtotime($cuodet_fec)) {
    $vencida = 1;
    $readonly = 'readonly=""';
    $bandera = '<b style="font-family: cambria;font-weight: bold;font-size: 15px;color:red">Esta Cuota esta vencida</b>';
    // Convierte ambas fechas a timestamp
    $diferencia_en_segundos = strtotime($fecha_hoy) - strtotime($cuodet_fec);
    // Convierte la diferencia en segundos a días
    $diferencia_en_dias = $diferencia_en_segundos / (60 * 60 * 24);
    $diasvencidos = '<b style="font-family: cambria;font-weight: bold;font-size: 15px;color:red">'.$diferencia_en_dias.' DIAS</b>';
    /*$resultmora = $oMorasugerida->obtener_morasugerida($cuodet_cuo, $diferencia_en_dias, 3);
    if($resultmora['estado']==1){
      //echo $cuodet_cuo . ' //' . $diferencia_en_dias;
      $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];
      $morasugerida = $diferencia_en_dias * $porcentajemora * $cuodet_cuo;
    }*/
  }

  $result = $oCuota->mostrarUno($cuo_id);
    //OBTENER LOS DATOS PARA VER SI EXISTE UN COMPROMISO DE PAGO
    $cobranza = $oCobranza->mostrarUnoByCuotaId($cuo_id);
    if ($result['estado'] == 1) {
      $cre_id = $result['data']['tb_credito_id'];
      $cuo_num = $result['data']['tb_cuota_num'];
      $cuo_persubcuo = $result['data']['tb_cuota_persubcuo'];
      $cuo_int = $result['data']['tb_cuota_int'];
      $cuota_not = $result['data']['tb_cuota_not']; // notas de la cobranza // gerson (10-03-23)
    }
  $result = NULL;

  $result = $oCredito->mostrarUno($cre_id);
    if ($result['estado'] == 1) {
      $cuotip_id = $result['data']['tb_cuotatipo_id'];
      $cuot_subper = $result['data']['tb_cuotasubperiodo_id'];
      $cuotip_nom = $result['data']['tb_cuotatipo_nom'];
      $cre_int = $result['data']['tb_credito_int'];
      $cre_tip = $result['data']['tb_credito_tip']; //tipo 1 normal, 2 adenda y 3 acuerdo de pago
      $cre_numcuo = $result['data']['tb_credito_numcuo'];
      $cre_numcuomax = $result['data']['tb_credito_numcuomax'];
      $cli_id = $result['data']['tb_cliente_id'];
      $cli_tip = $result['data']['tb_cliente_tip'];
      $cli_doc = $result['data']['tb_cliente_doc'];
      $cli_nom = $result['data']['tb_cliente_nom'];
      $credito_preaco = $result['data']['tb_credito_preaco'];

      $cliente = $cli_nom . ' | ' . $cli_doc;
    }
  $result = NULL;

  //CUANDO EL CREDITO ES LIBRE, TAMBIEN HAY MAS CUOTAS GENERADAS, PARA CAMBIARLAS DE ESTADO VERIFICAMOS QUE NUMERO SON Y DE CUANTAS CUOTAS EN TOTAL HAY
  $total_cuotas_hipo = 0;
  $result = $oCuota->obtener_cuotas_por_credito(3, $cre_id); //listamos las cuotas del credito 3: GARANTIA VEHICULAR
    $total_cuotas_hipo = count($result['data']);
  $result = NULL;

  $cuota_ultima = 0; //0 si la cuota es la última, a quien la dejaremos para poder generar otra
  if ($cuo_num < $total_cuotas_hipo) {
    $cuota_ultima = 1; // 1 si esta cuota está antes de la última, entonces al pagarla la desaparecemos de la lista
  }

  //verificamos si hay acuerdos de pago activos para consultar las fechas de pago en detalle
  $result = $oAcuerdopago->listar_acuerdopago_credito_xac($cre_id, 3, 0, $cli_id); //si xac = 0 el acuerdo está activo, parametro 3: garveh
    $rows = count($result['data']);
  $result = NULL;

  //VERIFICAMOS SI EN ESTA FECHA DE LA CUOTA EXISTE UN ACUERDO DE PAGO, SIEMPRE Y CUANDO HAYA ACUERDO DE PAGO ACTIVO
  if ($rows > 0) {
    $result = $oCuotadetalle->verificar_acuerdopago_en_una_fecha('tb_creditogarveh', $cre_id, $fecha_hoy);
      if ($result['estado'] == 1) {
        $cuode_acupag_id = $result['data']['tb_cuotadetalle_id'];
      }
    $result = NULL;
  }


  //obetenemos información de acuerdo de pago y de la cuotadetalle
  if ($cuodet_acupag == 1) {
    $result = $oAcuerdopago->informacion_acuerdopago_por_cuotadetalleid($_POST['cuodet_id']);
      if ($result['estado'] == 1) {
        $num_cuo = $result['data']['tb_acuerdopago_numcuo']; //numero de cuotas del acuerdopago
        $orden_cuodet = $result['data']['tb_acuerdodetalle_num']; //numero de orden de la cuotadetalle
        $instancia = $result['data']['tb_acuerdopago_ins']; //número de instancia del acuerdopago
      }
    $result = NULL;
  }


  $obs_acupag = '';
  $ap = '';
  $des_numero_cuota = 'N° de Cuota:';
  if ($cuodet_acupag == 1) {
    $obs_acupag = ' (AP) Instancia 0' . $instancia;
    $des_numero_cuota = 'N° de Cuota AP:';
    $ap = "AP";
  }

  $codigo = 'CGV-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT) . $obs_acupag;

  $nom_subper = 'MES';

  if ($cuot_subper == 2) {
    $nom_subper = 'QUINCENA';
  }

  if ($cuot_subper == 3) {
    $nom_subper = 'SEMANA';
  }

  //numero de cuotas
  if ($cuotip_id == 4) {
    $cuota = $nom_subper . ' ' . $cuodet_num . '/' . $cuo_persubcuo . ' (' . $cuo_num . '/' . $cre_numcuomax . ')';
  }

  if ($cuotip_id == 3) {
    $cuota = $cuo_num . '/' . $cre_numcuomax;
  }

  if ($cre_tip == 2) {
    $pag_det = "PAGO ADENDA A-" . $codigo . " N° CUOTA: $cuota";
  } else {
    $pag_det = "PAGO DE CUOTA " . $ap . " $codigo N° CUOTA: $cuota";
  }

  $mod_id = '2'; //cuota detalle
  $modid = $_POST['cuodet_id'];

  $clientecaja = 0;
  $result1 = $oClientecaja->filtrar_por_cliente($cli_id, $mon_id, 'tb_creditogarveh'); //filtramos su caja por cliente de todos sus creditos
    if($result1['estado']==1){
      foreach ($result1['data'] as $key => $dt1) {
        if (intval($dt1['tb_clientecaja_tip']) == 1) {
          $clientecaja = $clientecaja + $dt1['tb_clientecaja_mon'];
        }
        if (intval($dt1['tb_clientecaja_tip']) == 2) {
          $clientecaja = $clientecaja - $dt1['tb_clientecaja_mon'];
        }
      }
    }
  $result1 = NULL;


  $cliente_caja = mostrar_moneda($clientecaja);

  //pagos parciales
  $pagos_cuotadetalle = 0;
  $mod_id = 2; //cuotadetalle

  $result3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $_POST['cuodet_id']);
    if ($result3['estado'] > 0) {
      foreach ($result3['data'] as $key => $dt3) {
        $pagos_cuotadetalle += $dt3['tb_ingreso_imp'];
      }
    }
  $result3 = NULL;

  $pagos = $pagos_cuotadetalle;
  $pagos_parciales = mostrar_moneda($pagos);
  $monto_pagar = mostrar_moneda($cuodet_cuo - $pagos);

  /*if ($cuotip_id == 4) { //fijo
    if ($cuo_num == $cre_numcuo) {
      $ultima_cuota = " <span style='color:red;'>ÚLTIMA CUOTA</span>";
      $vMax = moneda_mysql($monto_pagar);
    } else {
      $vMax = '99999.00';
    }
  }

  if ($cuotip_id == 3) { //libre
    $vMax = moneda_mysql($monto_pagar);

    if ($cuo_num == $cre_numcuomax) {
      $ultima_cuota = " <span style='color:red;'>ÚLTIMA CUOTA</span>";
    } else {
      //$vMax='99999.00';
    }
    if ($vencida == 1) {
      $vMax = moneda_mysql($cuo_int - $pagos);
    }
  }*/



  //consultamos si de este crédito hay acuerdos de pago activos para que sus montos pagados sean aplicados a esta cuota
  $result = $oAcuerdopago->listar_por_credito_aplicar($cre_id, 3, $cli_id); //devuelve todos los AP que su columna tb_acuerdopago_apl = 1: disponibles para aplicar
  if($result['estado']==1){
    $rows_apl = count($result['data']);
  }else{
    $rows_apl = 0;
  }
  $result = NULL;

  //si hay AP disponibles para aplicar obtenemos todos sus ingresos guardados en la CAJA AP restanto egresos de la misma caja, para saber cuanto hay disponible para aplicar
  $monto_aplicar = 0;
  $bandera_ap = '';

  if ($rows_apl > 0) {
    $monto_ingre_ap = 0;
    $monto_egre_ap = 0;
    $caja_id = 14; //caja ap

    $result = $oIngreso->ingresos_caja_cliente($caja_id, $mon_id, $cli_id, 30); //modulo id = 30, que corresponde solo obtener pago de cuotas
    if ($result['estado'] == 1) {
      foreach ($result['data'] as $key => $dt) {
        $monto_ingre_ap += $dt['tb_ingreso_imp'];
      }
    }
    $result = NULL;

    $modulo_id = 100; //ya que egreso no tiene cliente_id, el egreso para CAJA AP tendra como modulo = 100 e egreso_modide = cliente_id
    $result = $oEgreso->egresos_caja_cliente($caja_id, $mon_id, $modulo_id, $cli_id);
    if ($result['estado'] == 1) {
      foreach ($result['data'] as $key => $dt) {
        $monto_egre_ap += $dt['tb_egreso_imp'];
      }
    }
    $result = NULL;

    $monto_aplicar = $monto_ingre_ap - $monto_egre_ap;
    $monto_aplicar = formato_numero($monto_aplicar);
    $bandera_ap = 'Total ingresos AP: '.$monto_ingre_ap.' // total egresos AP: '.$monto_egre_ap;
  }
}
if ($usureg > 0) {
  $rws = $oUsuario->mostrarUno($usureg);
  if ($rws['estado'] == 1) {
    $usureg = $rws['data']['tb_usuario_nom'] . " " . $rws['data']['tb_usuario_ape'];
  }
}

/*$result4 = $oCliente->listar_nota_cliente_credito2($cli_id,$cre_id,3,'1,4'); //ASIVEH de tipo ACUERDO DE PAGO
$vernotas = '';
if ($result4['estado'] == 1) {
    $vernotas .= '<ul>';
    foreach ($result4['data'] as $key => $value) {
      $vernotas .= '<li><strong>'.$value['tb_usuario_nom'].':</strong> '.$value['tb_notacliente_det'].'. <strong> '.mostrar_fecha_hora($value['tb_notacliente_reg']).'</strong></li>';
    }
      $vernotas .= '</ul>';
}*/
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_garveh" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">INFORMACIÓN DE CUOTA GAR VEH A PAGAR <?php echo $_POST['cuodet_id'] . '  - ' . $bandera ?></h4>
        
      </div>
      <form id="for_cuopag">
        <input type="hidden" name="hdd_chk_mor_aut" id="hdd_chk_mor_aut" value="0">
        <input type="hidden" name="hdd_diasatraso" id="hdd_diasatraso" value="<?php echo $diferencia_en_dias ?>">
        <input type="hidden" name="hdd_morasugerida" id="hdd_morasugerida" value="<?php //echo $morasugerida ?>">
        <input type="hidden" name="xxxx" id="xxxx" value="<?php echo $resultmora['estado'] ?>">
        <input type="hidden" name="hdd_monto" id="hdd_monto" value="<?php echo $cuodet_cuo ?>">
        <input type="hidden" name="hdd_monto_pagar" id="hdd_monto_pagar" value="<?php echo $monto_pagar ?>">
        <input type="hidden" name="hdd_tipo_credito" id="hdd_tipo_credito" value="3">
        <input type="hidden" name="hdd_acupag" id="hdd_acupag" value="<?php echo $cuodet_acupag ?>">
        <input type="hidden" id="hdd_usuariogrupo" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">
        <input type="hidden" id="hdd_cuodet_per" value="<?php echo $cuodet_per; ?>">
        <input type="hidden" id="hdd_vencida" name="hdd_vencida" value="<?php echo $vencida ?>">
        <input type="hidden" id="hdd_cuotip_id" value="<?php echo $cuotip_id ?>">
        <input type="hidden" id="hdd_fec_ven" value="<?php echo $cuodet_fec; ?>">
        <input type="hidden" id="hdd_cuota_ultima" name="hdd_cuota_ultima" value="<?php echo $cuota_ultima; ?>">

        <input name="action_cuotapago" id="action_cuotapago" type="hidden" value="<?php echo $_POST['action'] ?>">
        <input name="hdd_cuodet_id" id="hdd_cuodet_id" type="hidden" value="<?php echo $_POST['cuodet_id'] ?>">
        <input type="hidden" name="hdd_cuo_id" id="hdd_cuo_id" value="<?php echo $cuo_id; ?>">

        <input name="hdd_cli_id" id="hdd_cli_id" type="hidden" value="<?php echo $cli_id ?>">
        <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id ?>">

        <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $mon_id ?>">
        <input name="hdd_tipcam" id="hdd_tipcam" type="hidden" value="">

        <input name="hdd_mod_id" id="hdd_mod_id" type="hidden" value="<?php echo $mod_id ?>">
        <input name="hdd_modid" id="hdd_modid" type="hidden" value="<?php echo $modid ?>">

        <input name="hdd_cre_int" id="hdd_cre_int" type="hidden" value="<?php echo $cre_int ?>">
        <input name="hdd_cuodet_fec" id="hdd_cuodet_fec" type="hidden" value="<?php echo $cuodet_fec ?>">
        <input name="hdd_cuo_num" id="hdd_cuo_num" type="hidden" value="<?php echo $cuo_num ?>">
        <input name="hdd_cuo_max" id="hdd_cuo_max" type="hidden" value="<?php echo $cre_numcuomax ?>">
        <input name="hdd_cuo_cap" id="hdd_cuo_cap" type="hidden" value="<?php echo $cuo_cap ?>">

        <input name="hdd_pag_det" id="hdd_pag_det" type="hidden" value="<?php echo $pag_det ?>">

        <input type="hidden" id="hdd_pagos_parciales" value="<?php echo formato_moneda($pagos); ?>">
        <input type="hidden" id="hdd_cajacliente" value="<?php echo formato_moneda($clientecaja); ?>">

        <input type="hidden" name="hdd_pc_nombre" id="hdd_pc_nombre" value="<?php echo gethostname() ?>">
        <input type="hidden" name="hdd_pc_mac" id="hdd_pc_mac" value="<?php echo exec('getmac') ?>">
        <input type="hidden" name="hdd_cuode_acupag" id="hdd_cuode_acupag" value="<?php echo $cuode_acupag_id?>">
        <input type="hidden" name="monto_ingre_ap" id="monto_ingre_ap" value="<?php echo $monto_ingre_ap?>">
        <input type="hidden" name="monto_egre_ap" id="monto_egre_ap" value="<?php echo $monto_egre_ap?>">

        <div class="modal-body">
          <!-- GERSON (10-03-23) -->
          <div class="box box-danger box-solid">
              <div class="box-header with-border">
                  <h3 class="box-title"><strong>Compromiso de Pago</strong></h3>
              </div>
              <div class="box-body">
                  <?php
                  $arr_not = array();
                  if($cobranza['estado']==1){
                      $arr_not = explode('<br>', $cuota_not);

                      if(mostrar_fecha($cobranza['data']['tb_cobranza_fec_compromiso'])!=$cuo_fec){// si las fechas son distintas quiere decir que tiene un compromiso de pago
                          // Muestra la ultima nota
                          echo "<span>".$arr_not[0]."</span>";
                      }else{
                          // Muestra las 2 ultiams notas
                          echo "<span>".$arr_not[0]."</span><br>";
                          echo "<span>".$arr_not[1]."</span>";
                      }
                  }
                  ?>
              </div>
          </div>
          <div class="box box-warning box-solid" id="notas">
              <div class="box-header with-border">
                  <h3 class="box-title"><strong>NOTAS</strong><strong id="nota_gc"></strong></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
              </div>
              <div class="box-body" id="body_notas">
                  
              </div>
            </div>
          <!--  -->
              
          <div class="box box-default">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <label style="font-family: cambria;color: #006633;">DATOS DEL CLIENTE Y CREDITO</label>
                  <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;font-size:15px">
                    <thead>
                      <tr id="tabla_cabecera">
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>CRÉDITO</b></td>
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>CLIENTE</b></td>
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>TIPO CRÉDITO</b></td>
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>CAPITAL</b></td>
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>% INTERES</b></td>
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>N° CUOTA</b></td>
                        <td id="tabla_cabecera_fila" style="height: 22px"><b>FECHA VENCIMIENTO</b></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr id="tabla_fila" style="height: 20px">
                        <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $codigo; ?></b></td>
                        <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $cliente; ?></b></td>
                        <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo 'CRÉDITO GARANTÍA VEHICULAR - ' . $cuotip_nom ?></b></td>
                        <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $mon_nom . ' ' . mostrar_moneda($credito_preaco); ?></b></td>
                        <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $cre_int . ' %'; ?></b></td>
                        <td id="tabla_fila" align="center" style="height: 22px"><b><?php echo $cuota . $ultima_cuota; ?></b></td>
                        <td id="tabla_fila" align="center" style="height: 22px"><b style="<?php if ($vencida == 1) echo 'color: #cc0000' ?>"><?php echo $cuodet_fec; ?></b></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <br>
                <?php
                if($bandera!=""){
                  ?>
                  <h3 style="font-family: cambria;font-weight: bold;font-size: 15px">DIAS VENCIDOS : <?php echo $diasvencidos ?></h3>
                  <?php
                }
              ?>
              <br>
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align:left">CUOTA (<?php echo $mon_nom ?>):</label>
                          <div class="col-md-7">
                              <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuodet_cuo" type="text" id="txt_cuodet_cuo" value="<?php echo mostrar_moneda($monto_pagar) ?>" readonly>
                          </div>
                      </div>
                  </div>
                  <?php // if ($pagos > 0): ?>
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align:left">Pagos Parciales (<?php echo $mon_nom ?>):</label>
                          <div class="col-md-7">
                              <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuopag_pagpar" type="text" id="txt_cuopag_pagpar" value="<?php echo $pagos_parciales; ?>" readonly>
                          </div>
                      </div>
                  </div>
                  <?php // endif ?>
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Mora (<?php echo "S/"; //$mon_nom ?>):</label>
                          <div class="col-md-7">
                              <!--<input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuopag_mor" type="text" id="txt_cuopag_mor" value="" size="15" maxlength="10" readonly>--> 
                              <div class="input-group">
                                  <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm mayus moneda3" name="txt_cuopag_mor" type="text" id="txt_cuopag_mor" value="<?php //echo $morasugerida ?>" readonly>
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger btn-sm" onclick="comprobar()" title="Editar Mora">
                                          <span class="fa fa-check-square-o"></span>
                                      </button>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <p>
              <div class="row">
                <div class="col-md-8 coment">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-2 control-label" style="text-align:left">Comentario:</label>
                        <div class="col-md-10">
                            <textarea name="txt_coment" id="txt_coment" rows="3" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 replacement">
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="div_tipocambio">
                      <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Tipo Cambio:</label>
                      <div class="col-md-7">
                        <div class="input-group">
                          <input name="txt_cuopag_tipcam" type="text" value="<?php echo $cuopag_tipcam?>" id="txt_cuopag_tipcam" style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" readonly>
                        </div>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
              <p></p>
              <?php if ($clientecaja != 0) { ?>    
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align:left">Caja Cliente (<?php echo $mon_nom ?>):</label>
                          <div class="col-md-7">
                              <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuopag_clicaj" type="text" id="txt_cuopag_clicaj" value="<?php //echo $cliente_caja;                   ?>" readonly>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Caja Cliente (<?php echo $mon_nom ?>):</label>
                          <div class="col-md-7">
                              <div class="input-group">
                                  <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_clicaj_tot" type="text" id="txt_clicaj_tot" value="<?php echo $cliente_caja; ?>" readonly>
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-success btn-sm" onclick="usarSaldo()" title="Usar Saldo de Cliente">
                                          <span class="fa fa-money icon"></span>
                                      </button>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
                  
              </div>
              <?php } ?>    
              <p></p>
              <ul class="list-group list-group-unbordered">
                  <li class="list-group-item" style="border-color: #000000">
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="inputEmail3" class="col-md-5 control-label" style="text-align:center">Capital (<?php echo $mon_nom ?>):</label>
                                  <div class="col-md-7">
                                      <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="txt_capital" type="text" id="txt_capital" value="<?php echo mostrar_moneda($credito_preaco); ?>" readonly> 
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Fecha de Pago:</label>
                                  <div class="col-md-7">
                                      <div class='input-group date' id='datapicker1'>
                                          <input name="txt_cuopag_fec" type="text" class="form-control input-sm mayus" class="fecha" id="txt_cuopag_fec" value="<?php echo $fecha_hoy ?>" readonly style="text-align:center;font-weight: bold;">
                                          <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-calendar"></span>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          

                      </div>
                      <p>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Interes (<?php echo $mon_nom ?>):</label>
                                  <div class="col-md-7">
                                      <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_interes" type="text" id="txt_interes" value="<?php echo $cuo_int; ?>" readonly>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Interés %:</label>
                                  <div class="col-md-7">
                                      <!--<input style="text-align:right;" class="form-control input-sm mayus" name="txt_cuopag_mor" type="text" id="txt_cuopag_mor" value="" size="15" maxlength="10" readonly>--> 
                                      <div class="input-group">
                                          <input style="text-align:center;font-weight: bold;font-size: 15px;color: red" class="form-control input-sm mayus" name="txt_cuopag_por" type="text" id="txt_cuopag_por" value="<?php echo $cre_int; ?>" readonly> 
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <p>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Total (<?php echo $mon_nom ?>):</label>
                                  <div class="col-md-7">
                                      <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda" name="txt_cuopag_tot" type="text" id="txt_cuopag_tot" value="<?php echo $monto_pagar; ?>" readonly>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="" class="col-sm-5 control-label" style="text-align: left">Moneda:</label>
                                      <div class="col-md-7">
                                          <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm">
                                              <option value="1" <?php if($mon_id == 1) echo 'selected';?> >S/.</option>
                                              <option value="2" <?php if($mon_id == 2) echo 'selected';?> >US$</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                      </div>
                      <p>
                      <div class="row">
                          <?php
                          if ($cuotip_id == 1):

                              if ($cuo_num == $cre_numcuomax) {
                                  
                              } else {

                                  $monto_pagar = $cuo_int - $pagos;
                                  if ($monto_pagar <= 0)
                                      $monto_pagar = 0;
                                  else
                                      $monto_pagar = mostrar_moneda($monto_pagar);

                                  if (strtotime($fecha_hoy) > strtotime($cuo_fec)) {
                                      
                                  } else {
                                      if ($cuo_num > 1) {
                                          $monto_pagar = mostrar_moneda((($cuo_int / $diasdelmes) * $diastranscurridos) - $pagos);
                                      }
                                  }
                              }
                              ?>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Pago Mínimo (<?php echo $mon_nom ?>):</label>
                                      <div class="col-md-7">
                                          <input style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm mayus moneda" name="txt_cuopag_min" type="text" id="txt_cuopag_min" value="<?php echo $monto_pagar; ?>" readonly>
                                          <label style="color: red"><?php echo $diastranscurridos ?></label> &nbsp;&nbsp;dias Transcurridos
                                      </div>
                                  </div>
                              </div>
                          <?php endif ?>

                          <div class="col-md-4">
                              <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Monto Pago (<?php echo $mon_nom ?>):</label>
                              <div class="col-md-7">  
                                  <div class="input-group">
                                    
                                    <input style="text-align:center;font-weight: bold;font-size: 15px;color: #000099" class="form-control input-sm moneda3" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" value="<?php echo $monto_pagar ?>" <?php echo $readonly ?> >
                                    <span class="input-group-btn">
                                      <button class="btn btn-success btn-sm" type="button"  onClick="Activar_input_monto_pago_banco()" title="Activar Casilla para ingresar Monto">
                                        <span class="fa fa-check-square-o"></span>
                                      </button>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              
                              <div class="col-md-4">
                                <div class="form-group">
                                  <div class="div_mon_tipocambio" style="display: none;">
                                    <label for="inputEmail3" class="col-md-5 control-label" style="text-align: left">Monto</label>
                                    <div class="col-md-7">
                                        <input name="txt_cuopag_moncam" type="text" value="<?php //echo $cuopag_tipcam?>" id="txt_cuopag_moncam" style="text-align:center;font-weight: bold;font-size: 15px" class="form-control input-sm moneda">
                                    </div>
                                  </div>
                                  </div>
                              </div>
                      </div>
                  </li>
              </ul>
              <p>
              <div class="row">
              <?php if(empty($cuode_acupag_id) && $monto_aplicar > 0): ?>
                <row>
                  <div class="col-md-12">
                    <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px" >Hay monto de AP disponible para aplicar <?php echo $bandera_ap;?></h4>
                    <!--<table width="70%">
                      <tr>
                        <td><strong style="color: green;">Saldo disponible para aplicar:</strong></td>
                        <td>
                          <strong style="color: green;"><?php echo $mon_nom.' '.formato_moneda($monto_aplicar);?></strong>
                          <input type="hidden" name="hdd_monto_aplicar" value="<?php echo $monto_aplicar;?>">
                        </td>
                        <td align="right"><strong>Pagar esta cuota con AP:</strong></td>
                        <td><input style="height: 20px !important; width: 20px !important;" type="checkbox" name="che_mon_ap" id="che_mon_ap" value="0"></td>
                      </tr>
                    </table>-->
                    <br>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align:center">Saldo Disponible :</label>
                          <div class="col-md-7">
                              <input style="text-align:center;font-weight: bold;font-size: 15px;color: blue" class="form-control input-sm moneda" name="hdd_monto_aplicar" type="text" id="hdd_monto_aplicar" value="<?php echo mostrar_moneda($monto_aplicar); ?>" readonly> 
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-md-5 control-label" style="text-align:center">¿Pagar con AP? :</label>
                          <div class="col-md-7">
                            <div class="input-group date" id="datetimepicker2_cliente">
                              <label class="checkbox-inline">
                                <div class="icheckbox_flat-green" style="position: relative;">
                                  <input type="checkbox" name="che_mon_ap" id="che_mon_ap" value="1" class="flat-green" style="position: absolute; opacity: 0;">
                                  <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div> PAGAR
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </row>
              <?php endif; ?>

              <?php if(!empty($cuode_acupag_id)): ?>
                <fieldset>
                  <legend>Existe un acuerdo de pago en esta fecha</legend>
                  <strong style="color: green;">Al pagar esta cuota se abrirá automáticamente el formulario para el pago del AP (Acuerdo de Pago)</strong>
                </fieldset>
              <?php endif; ?>
              </div>
              <div class="row">
                  <?php
                  $buton = '';
                  if ($cuo_num == $cre_numcuo && $cuotip_id == 1) {
                      ?>
                      <strong style="font-weight: bold; font-size: 14px; color: red;">Esta es la ultima cuota del credito por lo tando debe Habilitar 2 cuotas mas antes de proceder con el cobro.</strong>

                      <?php
                  } else {
                      $buton = '<button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>';
                  }
                  ?>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



<script type="text/javascript" src="<?php echo 'vista/vencimiento/cuotapago_garveh_form.js?ver=123'; ?>"></script>