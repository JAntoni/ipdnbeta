<!-- INICIO GARNTIAS VEHICULARES GARVEH-->


<!-- INICIO FILTRAR CUOTAS GARVEH-->
<?php
    $boton_notas = "";
    $result = $oPagocuota->filtrar_garveh($_POST['hdd_fil_cli_id'], $cre_id_cgv, fecha_mysql($fecha_hoy));
    if ($result['estado'] == 1) {
        $notas = $oCliente->listar_nota_cliente_credito_3($_POST['hdd_fil_cli_id'], 3, '1,4'); //GARVEH de tipo GARANTIA NORMAL

        if($notas['estado'] == 1){?> 
        
        <!--  -->
            <div class="box box-danger box-solid" style="width: 70%; margin-left: auto; margin-right: auto;">
              <div class="box-header with-border">
                  <h3 class="box-title"><strong>¡IMPORTANTE! GARVEH</strong>
                </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
              </div>
              <div class="box-body">
                <ul>
            <?php foreach ($notas['data'] as $key => $value) { ?> 
                <div class="row" style="padding:2px">
                    <div class="col-md-11">
                    <p style="font-size: 15px;"><strong> <?php echo $value['tb_usuario_nom'];?> :</strong> <?php echo $value['tb_notacliente_det'];?><strong> <?php echo mostrar_fecha_hora($value['tb_notacliente_reg']);?></strong></p>
                    </div>
                    <div class="col-md-1">
                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="ocultar_nota(<?php echo $value['tb_notacliente_id'];?>)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                    </div>
                </div>
            <?php }?>   
                </ul>
              </div>
            </div>
          <!--  -->
        <?php }?>   

        <h3 class="subtitulo_pago">CRÉDITO GARANTÍA VEHICULAR</h3>
        <table cellspacing="1" class="table table-hover">
            <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila">CLIENTE</th>
                    <th id="tabla_cabecera_fila">N° CUOTA</th>
                    <th id="tabla_cabecera_fila">VENCIMIENTO</th>
                    <th id="tabla_cabecera_fila">CUOTA</th>
                    <th id="tabla_cabecera_fila">PAGOS</th>
                    <th id="tabla_cabecera_fila">SALDO</th>
                    <th id="tabla_cabecera_fila">ESTADO</th>
                    <th id="tabla_cabecera_fila"></th>
                    <th id="tabla_cabecera_fila">CRÉDITO</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($result['data'] as $key => $value) {
                    $vencida = 1;
                    if (strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec'])) {
                        $vencida = 0;
                    }

                    $orden_cuo = $value['tb_cuota_num'] . '/' . $value['tb_credito_numcuo'];
                    if ($value['tb_cuotatipo_id'] == 3) {
                        $orden_cuo = $value['tb_cuota_num'] . '/' . $value['tb_credito_numcuomax'];
                    }
                    ?>

                    <tr class="odd" style="font-weight:bold;" id="tabla_fila">
                        <td id="tabla_fila"><?php echo $value['tb_cliente_nom'] . " | " . $value['tb_cliente_doc']; ?></td>
                        <td id="tabla_fila" align="center"><?php echo $orden_cuo ?></td>
                        <td id="tabla_fila" align="center" <?php
                        if ($vencida == 1) {
                            echo 'style="color:red;font-weight:bold;"';
                        }
                        ?>><?php echo mostrar_fecha($value['tb_cuota_fec']) ?></td>
                            <?php
                            $pagos_cuota = 0;
                            $result3 = $oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
                            if ($result3['estado'] == 1) {
                                foreach ($result3['data'] as $key => $value3) {
                                    $pagos_cuota += $value3['tb_ingreso_imp'];
                                }
                            }
                            $saldo = formato_moneda($value['tb_cuota_cuo']) - formato_moneda($pagos_cuota);
                            ?>
                        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($value['tb_cuota_cuo']); ?></td>
                        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($pagos_cuota); ?></td>
                        <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom'] . " " . mostrar_moneda($saldo); ?></td>
                        <td id="tabla_fila">
                            <?php
                            $estado = "";
                            if ($value['tb_cuota_est'] == 1) {
                                $estado = 'POR COBRAR';
                            }
                            if ($value['tb_cuota_est'] == 2) {
                                $estado = 'CANCELADA';
                            }
                            if ($value['tb_cuota_est'] == 3) {
                                $estado = 'PAGO PARCIAL';
                            }
                            echo $estado;
                            ?>
                        </td>
                        <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" <?php echo $bloqueobtns;?> href="#his" onClick="creditogarveh_pagos('<?php echo $value['tb_credito_id'] ?>', 3)">Historial</a></td>
                        <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="creditogarveh_form('L', '<?php echo $value['tb_credito_id'] ?>')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT); ?></a></td>
                    </tr>
                    <?php
                    $result2 = $oPagocuota->mostrarUnoGarveh($value['tb_cuota_id']);

                    if ($result2['estado'] == 1) {
                        foreach ($result2['data'] as $key => $value2) {
                            $vencida = 1;
                            if (strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec'])) {
                                $vencida = 0;
                            };
                            ?>
                            <tr class="even" id="tabla_fila">
                                <td id="tabla_fila" style="padding-left:20px;"><?php echo "-"; ?></td>
                                <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value2['tb_cuota_persubcuo']?></td>
                                <td id="tabla_fila" align="center" <?php
                                if ($vencida == 1) {
                                    echo 'style="color:red;font-weight:bold;"';
                                }
                                ?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec']) ?></td>
                                    <?php
                                    $pagos_cuotadetalle = 0;
                                    $mod_id = 2;
                                    $result3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value2['tb_cuotadetalle_id']);
                                    if ($result3['estado'] == 1) {
                                        foreach ($result3['data'] as $key => $value3) {
                                            $pagos_cuotadetalle += $value3['tb_ingreso_imp'];
                                        }
                                    }
                                    $result3 = null;

                                    $cuota_detalle = $value2['tb_cuotadetalle_cuo'];
                                    $saldo_detalle = $value2['tb_cuotadetalle_cuo'] - $pagos_cuotadetalle;
                                    if ($value['tb_cuotatipo_id'] == 3) {
                                        $cuota_detalle = $value2['tb_cuota_int'];
                                        $saldo_detalle = $value2['tb_cuota_int'] - $pagos_cuotadetalle;
                                    }
                                    ?>
                                <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($cuota_detalle); ?>            
                                </td>
                                <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($pagos_cuotadetalle) ?></td>
                                <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($saldo_detalle); ?></td>
                                <td id="tabla_fila">
                                    <?php
                                    $estado = "";
                                    if ($value2['tb_cuotadetalle_est'] == 1) {
                                        $estado = 'POR COBRAR';
                                    }
                                    if ($value2['tb_cuotadetalle_est'] == 2) {
                                        $estado = 'CANCELADA';
                                    }
                                    if ($value2['tb_cuotadetalle_est'] == 3) {
                                        $estado = 'PAGO PARCIAL';
                                    }
                                    echo $estado;

                                    if ($value['tb_cuotatipo_id'] == 3) {
                                        $action = 'pagar_libre';
                                    }
                                    if ($value['tb_cuotatipo_id'] == 4) {
                                        $action = 'pagar_fijo';
                                    }
                                    ?>
                                </td>
                                <td id="tabla_fila" align="center">
                                    <?php if ($value2['tb_cuotadetalle_est'] != 2) { ?>
                                        <!--<a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_garveh_form('<?php echo $action ?>', '<?php echo $value2['tb_cuotadetalle_id'] ?>')">Pagar</a>-->
                                        <a class="btn btn-success btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="verificar_gastos('<?php echo $action ?>','<?php echo $value2['tb_credito_id'] ?>','<?php echo $value2['tb_cuotadetalle_id'] ?>')"><i class="fa fa-money" aria-hidden="true"></i></a>
                                    <?php } ?>
                                </td>
                                <td id="tabla_fila"></td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>
        <br>
        <?php
    }
    $result = null;
?>
<!-- FIN FILTRAR CUOTAS GARVEH-->


<!-- INICIO FILTRAR ACUERDO DE PAGO GARVEH-->
<?php
    $result = $oPagocuota->filtrar_garveh_acupag($_POST['hdd_fil_cli_id'], $cre_id_cgv, fecha_mysql($fecha_hoy));
    if ($result['estado'] == 1) {
        $notas = $oCliente->listar_nota_cliente_credito_3($_POST['hdd_fil_cli_id'], 3, '3'); //GARVEH de tipo GARANTIA NORMAL

        if($notas['estado'] == 1){?> 
        <!--  -->
            <div class="box box-danger box-solid" style="width: 70%; margin-left: auto; margin-right: auto;">
              <div class="box-header with-border">
                  <h3 class="box-title"><strong>¡IMPORTANTE! ACUERDOS GARVEH</strong>
                </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
              </div>
              <div class="box-body">
                <ul>
                <?php foreach ($notas['data'] as $key => $value) { ?> 
                <div class="row" style="padding-top:2px; padding-bottom:2px">
                    <div class="col-md-11">
                        <p style="font-size: 15px;"><strong> <?php echo $value['tb_usuario_nom']?> :</strong> <?php echo $value['tb_notacliente_det']?><strong> <?php echo mostrar_fecha_hora($value['tb_notacliente_reg'])?></strong></p>
                    </div>
                    <div class="col-md-1">
                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="ocultar_nota(<?php echo $value['tb_notacliente_id']?>)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php }?> 
                </ul>
              </div>
            </div>
          <!--  -->
        <?php }?> 
        <h3 class="subtitulo_pago">ACUERDOS DE PAGO CRÉDITO GARVEH</h3>
        <table cellspacing="1" class="table table-hover">
            <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila">CLIENTE</th>
                    <th id="tabla_cabecera_fila">N° CUOTA</th>
                    <th id="tabla_cabecera_fila">VENCIMIENTO</th>
                    <th id="tabla_cabecera_fila">CUOTA</th>
                    <th id="tabla_cabecera_fila">PAGOS</th>
                    <th id="tabla_cabecera_fila">SALDO</th>
                    <th id="tabla_cabecera_fila">ESTADO</th>
                    <th id="tabla_cabecera_fila"></th>
                    <th id="tabla_cabecera_fila">CRÉDITO</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($result['data'] as $key => $value) {
                //BANDERA VENCIDA
                $vencida = 1;
                if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec']))
                    $vencida = 0;

                //BANDERA ORDEN CUOTA
                $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
                if($value['tb_credito_numcuo'] == 0)
                    $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];

                ?>

                <tr class="odd" style="font-weight:bold;" id="tabla_fila">
                    <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
                    <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
                    <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
                        <?php
                            $pagos_cuota=0;
                            $result1=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
                            if ($result1['estado'] == 1){
                                foreach ($result1['data'] as $key => $value1){
                                    $pagos_cuota+=$value1['tb_ingreso_imp'];
                                }
                            }
                            $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
                        ?>
                    <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".formato_moneda($value['tb_cuota_cuo']);?></td>
                    <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".formato_moneda($pagos_cuota);?></td>
                    <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".formato_moneda($saldo);?></td>
                    <td id="tabla_fila">
                        <?php 
                        $estado="";
                        if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
                        if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
                        if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
                        echo $estado;
                        //credito_historial()
                        ?>
                    </td>
                    <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" <?php echo $bloqueobtns;?> href="#his" onClick="creditogarveh_pagos('<?php echo $value['tb_credito_id'] ?>', 3)">Historial</a></td> 
                    <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="creditogarveh_form('L', '<?php echo $value['tb_credito_id'] ?>')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT); ?></a></td>
                </tr>
                <?php
                $result2 = $oPagocuota->mostrarUnoGarveh($value['tb_cuota_id']);

                if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key => $value2) {
                        //BANDERA VENCIDA
                        $vencida = 1;
                        if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec']))
                            $vencida = 0;
                        ?>
                        <tr class="even" style="font-weight:bold;" id="tabla_fila">
                        <td id="tabla_fila"><?php echo "-"; ?></td>
                        <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value2['tb_cuota_persubcuo'] ?></td>
                        <td id="tabla_fila" align="center" <?php if ($vencida == 1) {echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec']) ?></td>
                        <?php
                                $pagos_cuotadetalle = 0;
                                $mod_id = 2;
                                $result3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value2['tb_cuotadetalle_id']);
                                if ($result3['estado'] == 1) {
                                    foreach ($result3['data'] as $key => $value3) {
                                        $pagos_cuotadetalle += $value3['tb_ingreso_imp'];
                                    }
                                }
                                $result3 = null;

                                $cuota_detalle = $value2['tb_cuotadetalle_cuo'];
                                $saldo_detalle = $value2['tb_cuotadetalle_cuo'] - $pagos_cuotadetalle;
                                if ($value['tb_cuotatipo_id'] == 3) {
                                    $cuota_detalle = $value2['tb_cuota_int'];
                                    $saldo_detalle = $value2['tb_cuota_int'] - $pagos_cuotadetalle;
                                }
                                ?>
                            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($cuota_detalle); ?>            
                            </td>
                            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($pagos_cuotadetalle) ?></td>
                            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($saldo_detalle); ?></td>
                            <td id="tabla_fila">
                                <?php
                                $estado = "";
                                if ($value2['tb_cuotadetalle_est'] == 1) {
                                    $estado = 'POR COBRAR';
                                }
                                if ($value2['tb_cuotadetalle_est'] == 2) {
                                    $estado = 'CANCELADA';
                                }
                                if ($value2['tb_cuotadetalle_est'] == 3) {
                                    $estado = 'PAGO PARCIAL';
                                }
                                echo $estado;

                                if ($value['tb_cuotatipo_id'] == 3) {
                                    $action = 'pagar_libre';
                                }
                                if ($value['tb_cuotatipo_id'] == 4) {
                                    $action = 'pagar_fijo';
                                }
                                ?>
                            </td>
                            <td id="tabla_fila" align="center">
                                <?php if ($value2['tb_cuotadetalle_est'] != 2) { ?>
                                    <!--<a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_garveh_form('<?php echo $action ?>', '<?php echo $value2['tb_cuotadetalle_id'] ?>')">Pagar</a>-->
                                    <a class="btn btn-success btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="verificar_gastos('<?php echo $action ?>','<?php echo $value2['tb_credito_id'] ?>','<?php echo $value2['tb_cuotadetalle_id'] ?>')"><i class="fa fa-money" aria-hidden="true"></i></a>
                                <?php } ?>
                            </td>
                            <td id="tabla_fila"></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php
            }
            ?>
            </tbody>
        </table>
        <br>
        <?php
    }
    $result = null;
?>
<!-- FIN FILTRAR ACUERDO DE PAGO GARVEH-->


<!-- INICIO FILTRAR ADENDAS GARVEH-->
<?php
    $result = $oPagocuota->filtrar_garveh_adendas($_POST['hdd_fil_cli_id'], $cre_id_cgv, fecha_mysql($fecha_hoy));
    if ($result['estado'] == 1){
        $notas = $oCliente->listar_nota_cliente_credito_3($_POST['hdd_fil_cli_id'], 3, '2'); //GARVEH de tipo GARANTIA NORMAL

        if($notas['estado'] == 1){?> 
        <!--  -->
            <div class="box box-danger box-solid" style="width: 70%; margin-left: auto; margin-right: auto;">
              <div class="box-header with-border">
                  <h3 class="box-title"><strong>¡IMPORTANTE! ADENDAS GARVEH</strong>
                </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
              </div>
              <div class="box-body">
                <ul>
                <?php foreach ($notas['data'] as $key => $value) { ?> 
                <div class="row" style="padding:2px">
                    <div class="col-md-11">
                    <p style="font-size: 15px;"><strong> <?php echo $value['tb_usuario_nom']?> :</strong> <?php echo $value['tb_notacliente_det']?><strong> <?php echo mostrar_fecha_hora($value['tb_notacliente_reg'])?></strong></p>
                    </div>
                    <div class="col-md-1">
                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="ocultar_nota(<?php echo $value['tb_notacliente_id']?>)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php }?> 
                </ul>
              </div>
            </div>
          <!--  -->
        <?php }?> 
        <h3 class="subtitulo_pago">ADENDAS CRÉDITO GARVEH</h3>
        <table cellspacing="1" class="table table-hover">
            <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila">CLIENTE</th>
                    <th id="tabla_cabecera_fila">N° CUOTA</th>
                    <th id="tabla_cabecera_fila">VENCIMIENTO</th>
                    <th id="tabla_cabecera_fila">CUOTA</th>
                    <th id="tabla_cabecera_fila">PAGOS</th>
                    <th id="tabla_cabecera_fila">SALDO</th>
                    <th id="tabla_cabecera_fila">ESTADO</th>
                    <th id="tabla_cabecera_fila"></th>
                    <th id="tabla_cabecera_fila">CRÉDITO</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($result['data'] as $key => $value) {
                //BANDERA VENCIDA
                $vencida = 1;
                if(strtotime($fecha_hoy) <= strtotime($value['tb_cuota_fec']))
                    $vencida = 0;

                //BANDERA ORDEN CUOTA
                $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuo'];
                if($value['tb_credito_numcuo'] == 0)
                    $orden_cuo =  $value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'];

                ?>

                <tr class="odd" style="font-weight:bold;" id="tabla_fila">
                    <td id="tabla_fila"><?php echo $value['tb_cliente_nom']." | ".$value['tb_cliente_doc'];?></td>
                    <td id="tabla_fila" align="center"><?php echo $orden_cuo?></td>
                    <td id="tabla_fila" align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value['tb_cuota_fec'])?></td>
                        <?php
                            $pagos_cuota=0;
                            $result1=$oCuotapago->mostrar_cuota_ingreso($value['tb_cuota_id']);
                            if ($result1['estado'] == 1){
                                foreach ($result1['data'] as $key => $value1){
                                    $pagos_cuota+=$value1['tb_ingreso_imp'];
                                }
                            }
                            $saldo=$value['tb_cuota_cuo']-$pagos_cuota;
                        ?>
                    <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".formato_moneda($value['tb_cuota_cuo']);?></td>
                    <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".formato_moneda($pagos_cuota);?></td>
                    <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value['tb_moneda_nom']." ".formato_moneda($saldo);?></td>
                    <td id="tabla_fila">
                        <?php 
                        $estado="";
                        if($value['tb_cuota_est']==1)$estado = 'POR COBRAR';
                        if($value['tb_cuota_est']==2)$estado = 'CANCELADA';
                        if($value['tb_cuota_est']==3)$estado = 'PAGO PARCIAL';
                        echo $estado;
                        //credito_historial()
                        ?>
                    </td>
                    <td id="tabla_fila" align="center"><a class="btn btn-primary btn-xs" <?php echo $bloqueobtns;?> href="#his" onClick="creditogarveh_pagos('<?php echo $value['tb_credito_id'] ?>', 3)">Historial</a></td> 
                    <td id="tabla_fila" align="center"><a class="btn btn-warning btn-xs" href="#cre" onClick="creditogarveh_form('L', '<?php echo $value['tb_credito_id'] ?>')">CGV-<?php echo str_pad($value['tb_credito_id'], 4, "0", STR_PAD_LEFT); ?></a></td>
                </tr>
                <?php
                $result2 = $oPagocuota->mostrarUnoGarveh($value['tb_cuota_id']);

                if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key => $value2) {
                        //BANDERA VENCIDA
                        $vencida = 1;
                        if(strtotime($fecha_hoy) <= strtotime($value2['tb_cuotadetalle_fec']))
                            $vencida = 0;
                        ?>
                        <tr class="even" style="font-weight:bold;" id="tabla_fila">
                        <td id="tabla_fila"><?php echo "-"; ?></td>
                        <td id="tabla_fila" align="center"><?php echo $value2['tb_cuotadetalle_num'].'/'.$value2['tb_cuota_persubcuo'] ?></td>
                        <td id="tabla_fila" align="center" <?php if ($vencida == 1) {echo 'style="color:red;font-weight:bold;"';}?>><?php echo mostrar_fecha($value2['tb_cuotadetalle_fec']) ?></td>
                        <?php
                                $pagos_cuotadetalle = 0;
                                $mod_id = 2;
                                $result3 = $oCuotapago->mostrar_cuotatipo_ingreso($mod_id, $value2['tb_cuotadetalle_id']);
                                if ($result3['estado'] == 1) {
                                    foreach ($result3['data'] as $key => $value3) {
                                        $pagos_cuotadetalle += $value3['tb_ingreso_imp'];
                                    }
                                }
                                $result3 = null;

                                $cuota_detalle = $value2['tb_cuotadetalle_cuo'];
                                $saldo_detalle = $value2['tb_cuotadetalle_cuo'] - $pagos_cuotadetalle;
                                if ($value['tb_cuotatipo_id'] == 3) {
                                    $cuota_detalle = $value2['tb_cuota_int'];
                                    $saldo_detalle = $value2['tb_cuota_int'] - $pagos_cuotadetalle;
                                }
                                ?>
                            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($cuota_detalle); ?>            
                            </td>
                            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($pagos_cuotadetalle) ?></td>
                            <td id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $value2['tb_moneda_nom'] . " " . mostrar_moneda($saldo_detalle); ?></td>
                            <td id="tabla_fila">
                                <?php
                                $estado = "";
                                if ($value2['tb_cuotadetalle_est'] == 1) {
                                    $estado = 'POR COBRAR';
                                }
                                if ($value2['tb_cuotadetalle_est'] == 2) {
                                    $estado = 'CANCELADA';
                                }
                                if ($value2['tb_cuotadetalle_est'] == 3) {
                                    $estado = 'PAGO PARCIAL';
                                }
                                echo $estado;

                                if ($value['tb_cuotatipo_id'] == 3) {
                                    $action = 'pagar_libre';
                                }
                                if ($value['tb_cuotatipo_id'] == 4) {
                                    $action = 'pagar_fijo';
                                }
                                ?>
                            </td>
                            <td id="tabla_fila" align="center">
                                <?php if ($value2['tb_cuotadetalle_est'] != 2) { ?>
                                    <!--<a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cuotapago_garveh_form('<?php echo $action ?>', '<?php echo $value2['tb_cuotadetalle_id'] ?>')">Pagar</a>-->
                                    <a class="btn btn-success btn-xs" <?php echo $bloqueobtns;?> href="javascript:void(0)" onClick="verificar_gastos('<?php echo $action ?>', '<?php echo $value2['tb_credito_id'] ?>','<?php echo $value2['tb_cuotadetalle_id'] ?>')"><i class="fa fa-money" aria-hidden="true"></i></a>
                                <?php } ?>
                            </td>
                            <td id="tabla_fila"></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php
            }
            ?>
            </tbody>
        </table>
        <br>
        <?php
    }
?>
<!-- FIN FILTRAR ADENDAS GARVEH-->



<!-- FIN GARNTIAS VEHICULARES GAAAAAAAAAAAAAAARVEEEEEEEEEEEEEEEEEEEEEH-->
