
$(document).ready(function () {
    cmb_cuedep_id(0);

    $("#for_deposito").validate({
        submitHandler: function () {
            datos = new FormData();

            datos.append('action', $('#action').val());
            datos.append('formData', $('#for_deposito').serialize());
            datos.append('file', $('input[type=file]')[0].files[0]);

            $.ajax({
                type: "POST",
                url: VISTA_URL+"deposito/deposito_reg.php",
                async: true,
                dataType: "json",
                data: datos,
                contentType: false,
                processData: false,
                beforeSend: function () {

                },
                success: function (data) {
                    if (parseInt(data.estado) == 1) {
                        swal_success("SISTEMA",data.mensaje,3000);
                        $('#modal_registro_deposito').modal('hide');
                        deposito_tabla();
                    } else {
//                        alert(data.mensaje);
                        swal_warning("AVISO",data.mensaje,3000);
                    }
                },
                complete: function (data) {
                    if (data.statusText != 'success') {
                        $('#form_msj_deposito').html(data.responseText);
                        console.log(data);
                    }
                },
                error: function (data) {
           
            console.log(data.responseText);
        }
            });
        },
        rules: {
            cmb_cuedep_id: {
                required: true
            },
            file: {
                required: true
            }
        },
        messages: {
            cmb_cuedep_id: {
                required: 'Elija una cuenta'
            },
            file: {
                required: 'Importe un archivo Excel'
            }
        }
    });



});


function cmb_cuedep_id(ids)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL+"cuentadeposito/cuentadeposito_select.php",
        async: false,
        dataType: "html",
        data: ({
            cuentadeposito_id: ids
        }),
        beforeSend: function () {
            $('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_cuedep_id').html(html);
        }
    });
}