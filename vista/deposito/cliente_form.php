<?php
require_once ("../../core/usuario_sesion.php");
require_once ('Deposito.class.php');
$oDeposito=new Deposito();
require_once ('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ('../funciones/fechas.php');

$action = $_POST['action'];
$deposito_id = $_POST['deposito_id'];

$result=$oDeposito->mostrarUno($deposito_id);

if($result['estado']==1){
    $id_cliente=$result['data']['tb_cliente_id'];
    $deposito_tipo=$result['data']['tb_deposito_tipo'];
}
$result=NULL;


//$tb_cliente_fecha_declaracion=date('d-m-Y h:i:s');
if($id_cliente>0){
$result=$oCliente->mostrarUno($id_cliente);
    if($result['estado']==1){
        $nombre_cliente=$result['data']['tb_cliente_nom'];
        $tb_cliente_fecha_declaracion= ($result['data']['tb_cliente_fecha_declaracion']);
    }
}


?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_deposito_cliente" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-family: cambria">REGISTRAR CLIENTE</h4>
                </div>
                <form id="for_cliente" method="post">
                    <input type="hidden" name="action" value="<?php echo $action;?>">
                    <input type="hidden" name="hdd_deposito_id" value="<?php echo $deposito_id;?>">
                    <div class="modal-body">
                        <div class="box box-primary">
                            <div class="box-header">
                                
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>CLIENTE</label>
                                        <input type="text" id="txt_cliente_nombre" name="txt_cliente_nombre" class="form-control" value="<?php echo $nombre_cliente?>">
                                        <input type="hidden" id="txt_cliente_id" name="txt_cliente_id" class="form-control" value="<?php echo $id_cliente?>">
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>TIPO DE DEPÓSITO</label>
                                        <select id="cbm_deposito_tipo"name="cbm_deposito_tipo" class="form-control input-sm mayus">
                                            <option value="0" <?php if($deposito_tipo==0)echo 'selected'?>>EFECTIVO</option>
                                            <option value="1" <?php if($deposito_tipo==1)echo 'selected'?>>TRANSFERENCIA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>FECHA</label>
                                        <input type="text" id="txt_fecha_declaracion" name="txt_fecha_declaracion" class="form-control input-sm" value="<?php echo $tb_cliente_fecha_declaracion; ?>" readonly="">
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="submit" class="btn btn-info" id="btn_guardar_area">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript" src="<?php echo 'vista/deposito/cliente_form.js';?>"></script>