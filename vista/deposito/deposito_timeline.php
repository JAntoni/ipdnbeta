<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Deposito.class.php");
$oDeposito = new Deposito();
require_once ("../cliente/Cliente.class.php");
$oCliente= new Cliente();
require_once ('../funciones/funciones.php');
require_once ('../funciones/fechas.php');

$deposito_num=$_POST['deposito_num'];

$result = $oDeposito->listar_Ingresos_Num($deposito_num);
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $cliente_id=$value['tb_cliente_id'];
        $result2 =$oCliente->mostrarUno($cliente_id);
                  if($result2['estado']==1){
                      $nombre=$result2['data']['tb_cliente_nom'];
                      $telefono=$result2['data']['tb_cliente_tel'];
                  }
        $cont++;
        $lista .= '
            <li>
                <i class="fa fa-envelope bg-blue"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> ' . mostrar_fecha_hora($value['tb_ingreso_fecreg']) . '</span>
                    <h3 class="timeline-header"><a>' . $nombre . ' - '.$telefono.'</a></h3>
                    <div class="timeline-body">';
                    if ($usuariogrupo_id == 2) {
                        $lista .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>';
                    }
                    $lista .= $value['tb_ingreso_det'] . '
                    </div>
                </div>
            </li>';
    }
}
 else {
     $lista .= '
     
        <li>
                <i class="fa fa-envelope bg-blue"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i></span>
                    <h3 class="timeline-header"><a>El Boucher todavía no ha sido utilizado</a></h3>
                    <div class="timeline-body">';
                    if ($usuariogrupo_id == 2) {
                        $lista .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>';
                    }
                    $lista .='El Boucher con el numero de operacion  '.$deposito_num.'  todavía no ha sido utilizado en el sistema
                    </div>
                </div>
            </li>';
     
     
}
$result = NULL;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_deposito_timeline" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">Historial</h4>
            </div>
            <div class="modal-body">
                <ul class="timeline">
                    <?php echo $lista; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditolinea/creditolinea_timeline.js'; ?>"></script>