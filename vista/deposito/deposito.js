$(document).ready(function () {
    deposito_tabla();
    cmb_cuedep_id(0);

    $("#txt_cliente_fil_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            console.log(ui);
            $('#hdd_cliente_fil_id').val(ui.item.cliente_id);
            $('#txt_cliente_fil_nom').val(ui.item.cliente_nom);
            deposito_tabla();
            event.preventDefault();
            $('#txt_cliente_fil_nom').focus();
        }
    });
    
    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });

    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });

    $("#cmbventaveh").on("change", function (e) {
        deposito_tabla();
    });
    

    $('.moneda2').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '1.00',
        vMax: '9999999.00'
    });

});

$("#txt_fil_fec1,#txt_fil_fec1,#cmb_fil_cuedep_id,#hdd_cliente_fil_id").change(function () {
    deposito_tabla();
});

function deposito_form(act,idf){
    $.ajax({
        type: "POST",
        url: VISTA_URL+"deposito/deposito_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            are_id: idf
        }),
        beforeSend: function () {
           
        },
        success: function (html) {
             $('#div_modal_deposito_form').html(html);
            $('#modal_registro_deposito').modal('show');
            modal_hidden_bs_modal('modal_registro_deposito', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
    });
}


function deposito_tabla(){
    var validarcmbVentaVehicular = 0;
    if( document.getElementById("cmbventaveh") == null )
    {
        validarcmbVentaVehicular = -1;
    }
    else
    {
        validarcmbVentaVehicular = $('#cmbventaveh').val();
    }

    console.log(validarcmbVentaVehicular)
    
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_fil_fec1').val(),
            fec2: $('#txt_fil_fec2').val(),
            cuedep_id: $('#cmb_fil_cuedep_id').val(),
            cliente_id: $('#hdd_cliente_fil_id').val(),
            ventaVeh: validarcmbVentaVehicular
        }),
        beforeSend: function () {
            $('#deposito_mensaje_tbl').show(300);
        },
        success: function (html) {
            $('#div_deposito_tabla').html(html);
            $('#deposito_mensaje_tbl').hide(300);
        },
        complete: function (html) {
            estilos_datatable();
        },
        error: function (html) {
            $('#deposito_mensaje_tbl').html('ERROR AL CARGAR DATOS DE GESTIÓN DE DEPÓSITOS: ' + html.responseText);
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tabla_deposito').DataTable({
        "pageLength": 100,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function modificar_num(deposito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'modificar_num',
            deposito_id: deposito_id,
            deposito_num: $('#input_' + deposito_id).val()
        }),
        beforeSend: function () {
//            $('#msj_deposito').html("Guardando Número de operación...");
//            $('#msj_deposito').show(300);
        },
        success: function (data) {
            if (parseInt(data.estado) == 1) {
//                $('#msj_deposito').html(data.mensaje);
                swal_success("SISTEMA", data.mensaje, 4500);
            } else {
//                alert(data.mensaje);
                swal_warning("AVISO", data.mensaje, 4500);
            }
        },
        complete: function (data) {
            if (data.statusText != 'success') {
                $('#msj_deposito').html(data.responseText);
                console.log(data);
            }
        }
    });
}

function deposito_form(act, idf) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            are_id: idf
        }),
        beforeSend: function () {


        },
        success: function (html) {
//            $('#div_deposito_form').html(html);
            $('#div_modal_deposito_form').html(html);
            $('#modal_registro_deposito').modal('show');
        }
    });
}

function agregar_detalle(deposito_id) {
    var detalle = $('#txt_deposito_detalle_' + deposito_id).val();

    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'detalle',
            deposito_id: deposito_id,
            detalle: detalle
        }),
        beforeSend: function () {
        },
        success: function (data) {
            if (parseInt(data.estado) == 1) {
                swal_success("SISTEMA", data.mensaje, 4500);
                deposito_tabla();
            } else {
                swal_warning("AVISO", data.mensaje, 4500);
                deposito_tabla();
            }
        },
        complete: function (data) {
            if (data.statusText != 'success') {
                $('#msj_deposito').html(data.responseText);
                console.log(data);
            }
        }
    });
}

function deposito_usar_form(action, deposito_id)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_usar_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            deposito_id: deposito_id
        }),
        beforeSend: function () {



        },
        success: function (html) {
            $('#div_modal_deposito_usar_form').html(html);
            $('#modal_deposito_usar_form').modal('show');

            modal_hidden_bs_modal('modal_deposito_usar_form', 'limpiar'); //funcion encontrada en public/js/generales.jsv
            console.log(html);
        },
        error: function (html) {

        }
    });
}

function eliminar_deposito(deposito_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-ban',
        title: 'Eliminar',
        content: '¿Está seguro de Eliminar?',
        type: 'red',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "deposito/deposito_reg.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: "eliminar",
                        deposito_id: deposito_id
                    }),
                    beforeSend: function () {
//				$('#msj_deposito').html("Cargando...");
//				$('#msj_deposito').show(100);
                    },
                    success: function (html) {
                        if(html.estado>0){
                        swal_success("ELIMINADO", html.mensaje, 2000);
                        deposito_tabla();
                    }
                    else{
                            swal_warning("AVISO", html.mensaje, 4500);
                    }
                    },
                    complete: function () {
                        
                    }
                });
            },
            no: function () {}
        }
    });
}

function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'deposito', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
//                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function upload_galeria(deposito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'deposito', //nombre de la tabla a relacionar
            modulo_id: deposito_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function Abrir_imagen(deposito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_deposito_img').modal('show');
            modal_hidden_bs_modal('modal_deposito_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(deposito_id);
        },
        complete: function (html) {
//            console.log(html);
        }
    });
}

function cmb_cuedep_id(ids)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuentadeposito/cuentadeposito_select.php",
        async: false,
        dataType: "html",
        data: ({
            cuentadeposito_id: ids
        }),
        beforeSend: function () {
            $('#cmb_fil_cuedep_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_fil_cuedep_id').html(html);
        }
    });
}


function deposito_manual(act) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_manual_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_deposito_manual_form').html(html);
            $('#modal_registro_deposito_manual').modal('show');
            modal_hidden_bs_modal('modal_registro_deposito_manual', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {

        }
    });
}

function deposito_timeline(deposito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/deposito_timeline.php",
        async: true,
        dataType: "html",
        data: ({
            deposito_num: $('#input_' + deposito_id).val()
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_modal_deposito_timeline_form').html(data);
            $('#modal_deposito_timeline').modal('show');
            modal_height_auto('modal_deposito_timeline'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_deposito_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            $('#modal_mensaje').modal('hide');
        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            console.log(data.responseText);
        }
    });
}

function agregar_cliente(deposito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "deposito/cliente_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cliente',
            deposito_id: deposito_id
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_deposito_cliente').html(html);
            $('#modal_deposito_cliente').modal('show');
            modal_hidden_bs_modal('modal_deposito_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
//            console.log(html);
        }
    });
}




function reporte_Clientes_Declarables(){
    
var fecha1=$("#txt_fil_fec1").val();
var fecha2=$("#txt_fil_fec2").val();

//  window.open("https://ipdsac.ipdnsac/ipdnsac/vista/flujocaja/reporte1_excel.php");
//  window.open("http://www.ipdnsac.com/ipdnsac/vista/deposito/reporte1_excel.php?fecha1="+fecha1+"&fecha2="+fecha2);
  window.open("https://localhost/ipdnsac/vista/deposito/reporte1_excel.php?fecha1="+fecha1+"&fecha2="+fecha2);
}