<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Deposito extends Conexion{
    
    public $tb_deposito_id;
    public $tb_deposito_reg;
    public $tb_deposito_fec;
    public $tb_deposito_des;
    public $tb_deposito_mon;
    public $tb_deposito_num;
    public $tb_cuentadeposito_id;
    public $tb_cliente_id;
    public $tb_deposito_est;
    public $tb_deposito_img;
    public $tb_deposito_det;
    public $tb_deposito_venta=0;

    function calcularProximoDpositoID(){
      try {
        $sql = "select coalesce((select max(tb_deposito_id)+1 from tb_deposito),1) as newid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT tb_deposito ( 
			tb_deposito_fec, tb_deposito_des, 
			tb_deposito_mon, tb_deposito_num, 
			tb_cuentadeposito_id, tb_deposito_venta
			)
                VALUES (
			 :tb_deposito_fec,:tb_deposito_des, 
			 :tb_deposito_mon, :tb_deposito_num, 
			 :tb_cuentadeposito_id, :tb_deposito_venta);"; 

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deposito_fec", $this->tb_deposito_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deposito_des", $this->tb_deposito_des, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deposito_mon", $this->tb_deposito_mon, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_deposito_num", $this->tb_deposito_num, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuentadeposito_id", $this->tb_cuentadeposito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_deposito_venta", $this->tb_deposito_venta, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function modificar($deposito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deposito_id", $deposito_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar_campo($deposito_id, $deposito_columna, $deposito_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        $param_tip = strtoupper($param_tip);

        if(!empty($deposito_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_deposito SET ".$deposito_columna."=:deposito_valor WHERE tb_deposito_id =:deposito_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":deposito_id", $deposito_id, PDO::PARAM_INT);
          if ($param_tip == 'INT') {
                    $sentencia->bindParam(":deposito_valor", $deposito_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":deposito_valor", $deposito_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function eliminar($deposito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_deposito SET tb_deposito_xac=0 WHERE tb_deposito_id =:deposito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deposito_id", $deposito_id, PDO::PARAM_INT);
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($deposito_id){
      try {
        $sql = "SELECT * FROM tb_deposito WHERE tb_deposito_id=:deposito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deposito_id", $deposito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos($fecha1, $fecha2, $cuedep_id, $cliente_id, $isVentaVeh){
      try {
        $where = "";
        if($cuedep_id > 0)
          $where .= " AND cue.tb_cuentadeposito_id in(".$cuedep_id.")";
        if($cliente_id > 0)
          $where .= " AND de.tb_cliente_id =".$cliente_id;

		    $sql = "SELECT * 
          FROM tb_deposito de 
          INNER JOIN tb_cuentadeposito cue ON cue.tb_cuentadeposito_id = de.tb_cuentadeposito_id 
          LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = de.tb_cliente_id 
          WHERE 
            DATE(tb_deposito_fec) BETWEEN :fecha1 AND :fecha2 
          $where
          AND
            (case :p_ventaVeh
              when -1 then
                1=1
              else
                de.tb_deposito_venta = :p_ventaVeh
            end)
          AND de.tb_deposito_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->bindParam(":p_ventaVeh", $isVentaVeh, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos_declarables($fecha1, $fecha2, $cuedep_id, $cliente_id){
      try {
        $where = "";
        if($cuedep_id > 0)
          $where .= " AND cue.tb_cuentadeposito_id in(".$cuedep_id.")";
        if($cliente_id > 0)
          $where .= " AND de.tb_cliente_id =".$cliente_id;

		    $sql = "SELECT * FROM tb_deposito de 
						INNER JOIN tb_cuentadeposito cue ON cue.tb_cuentadeposito_id = de.tb_cuentadeposito_id 
						LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = de.tb_cliente_id 
						WHERE DATE(tb_cliente_fecha_declaracion) BETWEEN :fecha1 AND :fecha2 AND tb_cliente_declaracion=1 ".$where;


        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_fecha_deposito($fecha1, $fecha2, $cuedep_id){
      try {
        $where = "";
        if ($cuedep_id != '') {
                $where .= " AND cue.tb_cuentadeposito_id in(" . $cuedep_id . ")";
            }

            $sql = "SELECT * FROM tb_deposito de 
						INNER JOIN tb_cuentadeposito cue ON cue.tb_cuentadeposito_id = de.tb_cuentadeposito_id 
						LEFT JOIN tb_cliente cli ON cli.tb_cliente_id = de.tb_cliente_id 
						WHERE tb_deposito_fec BETWEEN :fecha1 AND :fecha2 ".$where;
		      $sql .=	" ORDER BY tb_deposito_fec";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay fechas de despositos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function validar_num_operacion($numope){
      try {
        $sql = "SELECT * FROM tb_deposito WHERE TRIM(tb_deposito_num) = :numope order by tb_deposito_mon desc;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":numope", $numope, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay depósitos con el Número de Operación: ".$numope;
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function validar_no_operacion_fecha($numope, $fecha_deposito){
      try {
        $sql = "SELECT * FROM tb_deposito WHERE TRIM(tb_deposito_num) = :numope AND tb_deposito_fec =:fecha_deposito;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":numope", $numope, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_deposito", $fecha_deposito, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No existe este número de operación para dicha fecha";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function numero_operacion_unico($deposito_num, $deposito_id){
      try {
        $sql = "SELECT * FROM tb_deposito WHERE tb_deposito_num = :deposito_num";
		if($deposito_id > 0)
			$sql.= " AND tb_deposito_id <> :tb_deposito_id order by tb_deposito_mon";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deposito_num", $deposito_num, PDO::PARAM_STR);
        if($deposito_id > 0)
        $sentencia->bindParam(":tb_deposito_id", $deposito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function numero_operacion_unico_cuenta($deposito_num, $cuentadeposito_id){
      try {
          
        $sql = "SELECT * FROM tb_deposito WHERE TRIM(tb_deposito_num) = :deposito_num AND tb_cuentadeposito_id=:cuentadeposito_id order by tb_deposito_mon desc;";


        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deposito_num", $deposito_num, PDO::PARAM_STR);
        $sentencia->bindParam(":cuentadeposito_id", $cuentadeposito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function listar_Ingresos_Num($numope){
      try {
        $sql = "select * from tb_ingreso WHERE tb_ingreso_numope =:numope AND tb_ingreso_xac = 1;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":numope", $numope, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_depositos_paragasto($numope){
      try {
        $sql = "SELECT de.*,
              cue.tb_cuentadeposito_nom, cue.tb_cuentadeposito_ban, cue.tb_moneda_id,
              mon.tb_moneda_nom
              FROM tb_deposito de
              INNER JOIN tb_cuentadeposito cue ON cue.tb_cuentadeposito_id = de.tb_cuentadeposito_id 
              INNER JOIN tb_moneda mon ON cue.tb_moneda_id = mon.tb_moneda_id
              WHERE TRIM(tb_deposito_num) = :numope and tb_deposito_xac = 1;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":numope", $numope, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "no hay registro";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
  }

?>
