

$(document).ready(function () {
    var ids=$("#hdd_cuentadeposito_id").val();
    console.log("ID=="+ids);
    cmb_cuedep_id(ids);

    $("#txt_cliente_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            console.log(ui);
            $('#hdd_cliente_id').val(ui.item.cliente_id);
            $('#txt_cliente_nom').val(ui.item.cliente_nom);
//            deposito_tabla();
            event.preventDefault();
            $('#txt_cliente_nom').focus();
        }
    });

    $("#for_deposito_usar").validate({
            submitHandler: function () {

                $.ajax({
                    type: "POST",
                    url: VISTA_URL+"deposito/deposito_reg.php",
                    async: true,
                    dataType: "json",
                    data: $('#for_deposito_usar').serialize(),
                    beforeSend: function () {
                        
                    },
                    success: function (data) {
                        if (parseInt(data.estado) == 1) {
                            swal_success("SISTEMA",data.mensaje,2500);
                            deposito_tabla();
                            $('#modal_deposito_usar_form').modal('hide');
                        }
                    },
                    complete: function (data) {
                        if (data.statusText != 'success') {
//                            $('#msj_usar').html(data.responseText);
                            swal_success("SISTEMA",data.responseText,5500);
                            console.log(data);
                        }
                    }
                });
            },
            rules: {
                cmb_cuedep_id: {
                    required: true
                },
                hdd_cliente_id: {
                    required: true
                },
                txt_cliente_nom: {
                    required: true
                }
            },
            messages: {
                cmb_cuedep_id: {
                    required: 'Elija una cuenta'
                },
                hdd_cliente_id: {
                    required: 'Seleccione cliente'
                },
                txt_cliente_nom: {
                    required: 'Seleccione cliente'
                }
            }
        });$("#for_deposito_usar").validate({
            submitHandler: function () {

                $.ajax({
                    type: "POST",
                    url: "../deposito/deposito_reg.php",
                    async: true,
                    dataType: "json",
                    data: $('#for_deposito_usar').serialize(),
                    beforeSend: function () {
                        $('#msj_usar').html("Guardando...");
                        $('#msj_usar').show(100);
                    },
                    success: function (data) {
                        if (parseInt(data.estado) == 1) {
                            $('#msj_deposito').show(300);
                            $('#msj_deposito').html(data.mensaje);
                            deposito_tabla();
                            $('#div_deposito_usar_form').dialog("close");
                        }
                    },
                    complete: function (data) {
                        if (data.statusText != 'success') {
                            $('#msj_usar').html(data.responseText);
                            console.log(data);
                        }
                    }
                });
            },
            rules: {
                cmb_cuedep_id: {
                    required: true
                },
                hdd_cliente_id: {
                    required: true
                },
                txt_cliente_nom: {
                    required: true
                }
            },
            messages: {
                cmb_cuedep_id: {
                    required: 'Elija una cuenta'
                },
                hdd_cliente_id: {
                    required: 'Seleccione cliente'
                },
                txt_cliente_nom: {
                    required: 'Seleccione cliente'
                }
            }
        });

});



function cmb_cuedep_id(ids)
    {
        $.ajax({
            type: "POST",
            url:  VISTA_URL+"cuentadeposito/cuentadeposito_select.php",
            async: false,
            dataType: "html",
            data: ({
                cuentadeposito_id: ids
            }),
            beforeSend: function () {
                $('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
            },
            success: function (html) {
                $('#cmb_cuedep_id').html(html);
            }
        });
    }