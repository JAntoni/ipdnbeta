<?php
require_once ("../../core/usuario_sesion.php");
require_once("Deposito.class.php");
$oDeposito = new Deposito();
require_once ('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once ('../egreso/Egreso.class.php');
$oEgreso = new Egreso();

require('../../static/libreriasphp/php_excel_reader/php-excel-reader/excel_reader2.php');
require('../../static/libreriasphp/php_excel_reader/SpreadsheetReader.php');
//echo 'hasya aki estoy entrando normassssssl ';exit();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");



if ($_POST["action"] == 'insertar'){

	$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        
  parse_str($_POST["formData"], $datos);

  if(in_array($_FILES["file"]["type"],$allowedFileType)){
      
    $targetPath = 'subidas/'.basename($_FILES['file']['name']);
    if(file_exists($targetPath)){
    	unlink($targetPath);
    }
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    
    //echo 'archivo: '.$_FILES["file"]["type"].' // '.'subidas/'.$_FILES['file']['name'].' // cuenta_id: '.$datos['cmb_cuedep_id']; exit();

    $Reader = new SpreadsheetReader($targetPath);
    $msj = '';
    $sheetCount = count($Reader->sheets());
    for($i=0; $i<$sheetCount; $i++){
        
      $Reader->ChangeSheet($i);

      foreach ($Reader as $Row){
        $deposito_fec = "";
        if(isset($Row[0])) {
        	$deposito_fec = validar_fecha_excel($Row[0]);
        }

        $deposito_des = "";
        if(isset($Row[1])) {
          $deposito_des = $Row[1];
        }

        $deposito_mon = "";
        if(isset($Row[2])) {
          $deposito_mon = $Row[2];
        }

        $deposito_num = "";
        if(isset($Row[3])) {
          $deposito_num = $Row[3];
        }
        
        if (!empty($deposito_fec) || !empty($deposito_des) || !empty($deposito_mon) || !empty($deposito_num)) {
            $cuenta_id = $datos['cmb_cuedep_id'];
            $oDeposito->tb_deposito_fec=fecha_mysql($deposito_fec);
            $oDeposito->tb_deposito_des=$deposito_des; 
            $oDeposito->tb_deposito_mon=moneda_mysql($deposito_mon);
            $oDeposito->tb_deposito_num=$deposito_num; 
            $oDeposito->tb_cuentadeposito_id=$cuenta_id;

            $result2 = $oDeposito->validar_num_operacion($deposito_num);
              if($result2['estado'] == 1){
                $msj .= 'N° Operación ya registrada: '.$deposito_num.' | ';
              }
              else
                $oDeposito->insertar();
            $result2 = NULL;
        }
      }
    }

    $data['mensaje'] = 'Excel importado correctamente. '.$msj;
    $data['estado'] = 1;

    echo json_encode($data);
  }
  else
  { 
    echo "ERRO!: El archivo enviado es invalido. Por favor vuelva a intentarlo, type: ".$_FILES["file"]["type"];
  }
}

if($_POST['action'] == 'modificar_num'){
    $deposito_id = $_POST['deposito_id'];
    $deposito_num = trim($_POST['deposito_num']);

  //validamos que el número de operación no exista antes de registrar
  $result = $oDeposito->numero_operacion_unico($deposito_num, $deposito_id);
  $result2 = $oDeposito->validar_num_operacion($deposito_num);
  $result3 = $oDeposito->mostrarUno($deposito_id);
  
  
  if($result2['estado']==1){
    $tb_deposito_mon=$result2['data']['tb_deposito_mon'];
  }

  if($result3['estado']==1){
    $tb_deposito_mon3=$result3['data']['tb_deposito_mon'];
    $cuentadeposito_id=$result3['data']['tb_cuentadeposito_id'];
  }
   
  $result4 = $oDeposito->numero_operacion_unico_cuenta($deposito_num, $cuentadeposito_id);
  
  if($result4['estado']==1&&$tb_deposito_mon3>0){
        $data['estado'] = 0;
        $data['mensaje'] = 'El numero de operación ya se encuentra registrado para esta cuenta de déposito';
        echo json_encode($data); exit();
  }
  if($result['estado']==1){
    if($tb_deposito_mon3>=0&&$result4['estado']==1){
        $data['estado'] = 0;
        $data['mensaje'] = 'Debe ingresar un Monto menor a 0 para poder registrar en el sistema';
        echo json_encode($data); exit();
      }
  }
  
  
  
  $oDeposito->modificar_campo($deposito_id,'tb_deposito_num',$deposito_num,'STR');

  $data['mensaje'] = 'Número de operación modificado';
  $data['estado'] = 1;

  echo json_encode($data);
}

if($_POST['action'] == 'eliminar'){
	$deposito_id = $_POST['deposito_id'];
        $result=$oDeposito->mostrarUno($deposito_id);
        if($result['estado']==1){
            $deposito_num=$result['data']['tb_deposito_num'];
        }
        $result=NULL;

        if($deposito_num!=""){
            $result=$oIngreso->verificar_numero_voucher($deposito_num);
            if($result['estado']==1){
                $data['mensaje'] = 'Este numero de Operacion ya ha sido Ulizado en un pago y por lo tanto no puede eliminarse, consulte con el Administrador del Sistema';
                $data['estado'] = 0;
                echo json_encode($data);exit();
            }
        }
            $oDeposito->eliminar($deposito_id);

            $data['mensaje'] = 'Registro de depósito eliminado';
            $data['estado'] = 1;
            echo json_encode($data);

        $result=NULL;
        
}

if($_POST['action'] == 'usar'){
	$deposito_id = intval($_POST['hdd_deposito_id']);
	$cliente_id = intval($_POST['hdd_cliente_id']);

	if($deposito_id  > 0 && $cliente_id > 0){
	  $oDeposito->modificar_campo($deposito_id, 'tb_deposito_est', 1,'INT'); //estado 1, deposito USADO
	  $oDeposito->modificar_campo($deposito_id, 'tb_cliente_id', $cliente_id,'INT');
	  $data['mensaje'] = 'Información registrada correctamente';
  	$data['estado'] = 1;
  	echo json_encode($data);
	}
	else{
		echo 'Faltan datos, no hay cliente o deposito: cli: '.$cliente_id.' // dep: '.$deposito_id;
	}
  
}

if($_POST['action'] == 'manual'){
  
  $deposito_fec = $_POST['txt_deposito_fec'];
  $deposito_des = $_POST['txt_deposito_des'];
  $deposito_mon = $_POST['txt_deposito_mon'];
  $deposito_num = trim($_POST['txt_deposito_num']);
  $cuenta_id = $_POST['cmb_cuedep_manu_id'];

  //validamos que el número de operación no exista antes de registrar
  //$result = $oDeposito->numero_operacion_unico($deposito_num, 0); //0 ya que es un registro nuevo

  $result2 = $oDeposito->validar_num_operacion($deposito_num);
    if($result2['estado'] == 1){
      if($deposito_mon >= 0){
          $data['estado'] = 0;
          $data['mensaje'] = 'N° de Operación ya registrado, debe ingresar un Monto menor a 0 para poder registrar en el sistema';

          echo json_encode($data); exit();
        }
    }

  $oDeposito->tb_deposito_fec=fecha_mysql($deposito_fec);
  $oDeposito->tb_deposito_des=$deposito_des; 
  $oDeposito->tb_deposito_mon=moneda_mysql($deposito_mon);
  $oDeposito->tb_deposito_num=$deposito_num; 
  $oDeposito->tb_cuentadeposito_id=$cuenta_id;
  $oDeposito->insertar();

  $data['estado'] = 1;
  $data['mensaje'] = 'Depósito Registrado Correctamente';

  echo json_encode($data);
}

if($_POST['action']=="imagen")
{
  $uploadDir = '../../files/deposito/';

  // Set the allowed file extensions
  //$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions
  $fileTypes = array('jpg', 'png', 'jpeg');

  $verifyToken = md5('unique_salt' . $_POST['timestamp']);

  if (!empty($_FILES) && $_POST['token'] == $verifyToken)
  {
    $tempFile   = $_FILES['Filedata']['tmp_name'];
    //$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;

    $nombre=$_FILES['Filedata']['name'];

    $fileParts = pathinfo($_FILES['Filedata']['name']);

    // inserta
    $nuevo_nombre = 'deposito_'.$_POST['deposito_id'].'.'.$fileParts['extension'];
    $nuevo_nombre = strtolower($nuevo_nombre);
    //$targetFile = $uploadDir . $_FILES['Filedata']['name'];
    $targetFile = $uploadDir.$nuevo_nombre;

    // Validate the filetype
    if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

      //crear carpeta si no existe
      //mkdir(str_replace('//','/',$uploadDir), 0755, true);
      
      // Save the file
      $oDeposito->modificar_campo($_POST['deposito_id'], 'img', $targetFile, 'STR');
      
      move_uploaded_file($tempFile, $targetFile);

      echo '1';

    } else {

      // The file type wasn't allowed
      echo 'Tipo de archivo no permitido.';

    }
  }
}

if($_POST['action'] == 'subida'){
  $deposito_id = intval($_POST['deposito_id']);

  // if($deposito_id){
  //   $dts = $oDeposito->mostrarUno($deposito_id);
  //     $dt = mysql_fetch_array($dts);
  //     $img = $dt['tb_deposito_img'];
  //   mysql_free_result($dts);

  //   echo '
  //     <center>
  //       <img src="'.$img.'" width="70%" height="70%"/>
  //     </center>
  //   ';
  
  // else{
  //   echo 'Faltan datos, no hay cliente o deposito: cli: '.$cliente_id.' // dep: '.$deposito_id;
  // }
}

if ($_POST['action'] == 'detalle') {
  $deposito_id = $_POST['deposito_id'];
  $detalle = strtoupper(trim($_POST['detalle']));

  $oDeposito->modificar_campo($deposito_id, 'tb_deposito_det', $detalle,'STR');

  $data['mensaje'] = 'Detalle de depósito agregado';
  $data['estado'] = 1;

  echo json_encode($data);
}
?>