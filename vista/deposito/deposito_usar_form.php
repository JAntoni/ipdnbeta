<?php
require_once ("../../core/usuario_sesion.php");

require_once ("Deposito.class.php");
$oDeposito = new Deposito(); //

require_once("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$action = $_POST['action'];

$dts = $oDeposito->mostrarUno($_POST['deposito_id']);

if ($dts['estado'] == 1) {
    $deposito_mon = mostrar_moneda($dts['data']['tb_deposito_mon']);
    $deposito_des = $dts['data']['tb_deposito_des'];
    $deposito_num = $dts['data']['tb_deposito_num'];
    $cuentadeposito_id = $dts['data']['tb_cuentadeposito_id'];
    $cliente_id = $dts['data']['tb_cliente_id'];
}
$dts = NULL;

$cliente_nom = '';
if (intval($cliente_id) > 0) {
    $dts = $oCliente->mostrarUno($cliente_id);
    if ($dts['estado'] == 1) {
        $cliente_nom = $dts['data']['tb_cliente_nom'];
    }
}
?>




<div class="modal fade" tabindex="-1" role="dialog" id="modal_deposito_usar_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">USAR ESTE DEPÓSITO</h4>
            </div>
            <form id="for_deposito_usar">
                <input name="action" id="action" type="hidden" value="<?php echo $_POST['action'] ?>">
                <input name="hdd_deposito_id" id="hdd_deposito_id" type="hidden" value="<?php echo $_POST['deposito_id'] ?>">
                <input name="hdd_cuentadeposito_id" id="hdd_cuentadeposito_id" type="hidden" value="<?php echo $cuentadeposito_id ?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Cuenta Depósito:</label>
                            <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">

                            </select>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Monto Depositado:</label>
                            <input type="text" class="form-control input-sm moneda2" name="txt_deposito_mon" id="txt_deposito_mon" value="<?php echo $deposito_mon; ?>" readonly>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Descripción:</label>
                            <input type="text" name="txt_deposito_des" id="txt_deposito_des" value="<?php echo $deposito_des; ?>" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>N° Operación:</label>
                            <input type="text" name="txt_deposito_num" id="txt_deposito_num" value="<?php echo $deposito_num; ?>" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Cliente:</label>
                            <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>" class="form-control input-sm">
                            <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" value="<?php echo $cliente_nom; ?>" class="form-control input-sm">
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="deposito_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_deposito">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo 'vista/deposito/deposito_usar_form.js'; ?>"></script>






<!--<form id="for_deposito_usar">
    <input name="action" id="action" type="hidden" value="<?php echo $_POST['action'] ?>">
    <input name="hdd_deposito_id" id="hdd_deposito_id" type="hidden" value="<?php echo $_POST['deposito_id'] ?>">
    <input name="hdd_cuentadeposito_id" id="hdd_cuentadeposito_id" type="hidden" value="<?php echo $cuentadeposito_id ?>">
    <table>
        <tr>
            <td align="right" valign="top">Cuenta Depósito:</td>
            <td><select name="cmb_cuedep_id" id="cmb_cuedep_id" style="width: 230px;"></select></td>
        </tr>
        <tr>
            <td align="right" valign="top">Monto Depositado:</td>
            <td><input type="text" class="moneda2" name="txt_deposito_mon" id="txt_deposito_mon" value="<?php echo $deposito_mon; ?>" readonly></td>
        </tr>
        <tr>
            <td align="right" valign="top">Descripción:</td>
            <td><input type="text" name="txt_deposito_des" id="txt_deposito_des" value="<?php echo $deposito_des; ?>" readonly></td>
        </tr>
        <tr>
            <td align="right" valign="top">N° Operación:</td>
            <td><input type="text" name="txt_deposito_num" id="txt_deposito_num" value="<?php echo $deposito_num; ?>" readonly></td>
        </tr>
        <tr>
            <td align="right" valign="top">Cliente:</td>
            <td>
                <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">
                <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" size="30" value="<?php echo $cliente_nom; ?>">
            </td>
        </tr>
    </table>
</form>-->
<!--<div id="msj_usar" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>-->