<?php

require_once ("../../core/usuario_sesion.php");
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();
require_once ('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ('../funciones/fechas.php');
$action = $_POST['action'];

if ($action == 'insertar') {
    $deposito_nom = mb_strtoupper($_POST['txt_deposito_nom'], 'UTF-8');
    $deposito_des = $_POST['txt_deposito_des'];

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Area.';
    if ($oDeposito->insertar($deposito_nom, $deposito_des)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Area registrado correctamente.';
    }

    echo json_encode($data);
} 
elseif ($action == 'modificar') {
    $deposito_id = intval($_POST['hdd_deposito_id']);
    $deposito_nom = mb_strtoupper($_POST['txt_deposito_nom'], 'UTF-8');
    $deposito_des = $_POST['txt_deposito_des'];

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar el Area.';

    if ($oDeposito->modificar($deposito_id, $deposito_nom, $deposito_des)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Area modificado correctamente. ' . $deposito_des;
    }

    echo json_encode($data);
} 
elseif ($action == 'eliminar') {
    $deposito_id = intval($_POST['hdd_deposito_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el Area.';

    if ($oDeposito->eliminar($deposito_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Area eliminado correctamente.';
    }

    echo json_encode($data);
}
elseif ($action == 'cliente') {
    $cliente_id = $_POST['txt_cliente_id'];
    $deposito_tipo = intval($_POST['cbm_deposito_tipo']);
    $hdd_deposito_id = intval($_POST['hdd_deposito_id']);
    $cliente_declaracion_fecha = $_POST['txt_fecha_declaracion'];
    $fecha= (date('Y-m-d h:i:s a'));

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Cliente.';
    if ($oDeposito->modificar_campo($hdd_deposito_id, 'tb_cliente_id', $cliente_id, 'INT')) {
        $oDeposito->modificar_campo($hdd_deposito_id, 'tb_deposito_tipo', $deposito_tipo, 'INT');
        
        $result=$oCliente->mostrarUno($cliente_id);
            if($result['estado']==1){
                $tb_cliente_fecha_declaracion=$result['data']['tb_cliente_fecha_declaracion'];
            }
            
            if($deposito_tipo==1&&$tb_cliente_fecha_declaracion==NULL){
                $oCliente->modificar_campo($cliente_id, 'tb_cliente_declaracion',1,'STR');
                $oCliente->modificar_campo($cliente_id, 'tb_cliente_fecha_declaracion',$fecha,'STR');
            }

        $data['estado'] = 1;
        $data['mensaje'] = 'Cliente registrado al Depósito correctamente.';
    }

    echo json_encode($data);
}
elseif ($action == 'validar_numope'){
    $numero_ope = $_POST['numero_ope'];
    $data['estado'] = 0;
    $data['mensaje'] = 'El Número de Operación: '. $numero_ope . ', no está registrado en la gestión.';

    $result = $oDeposito->validar_num_operacion($numero_ope);
        if($result['estado'] == 1){
            $data['estado'] = 1;
            $data['mensaje'] = 'El Número de Operación: '. $numero_ope . ', ES VÁLIDO';
        }
    $result = NULL;

    echo json_encode($data);
}
else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>