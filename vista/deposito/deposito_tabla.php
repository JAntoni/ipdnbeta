<?php
    require_once('../../core/usuario_sesion.php');
    require_once ("Deposito.class.php");
    $oDeposito = new Deposito();
    require_once ('../cliente/Cliente.class.php');
    $oCliente = new Cliente();
    require_once ("../funciones/funciones.php");
    require_once ("../funciones/fechas.php");

    $fecha1 = $_POST['fec1'];
    $fecha2 = $_POST['fec2'];
    $cuedep_id = intval($_POST['cuedep_id']);
    $cliente_id = intval($_POST['cliente_id']);
    $ventaVeh = intval($_POST['ventaVeh']);

    $dts = $oDeposito->mostrarTodos(fecha_mysql($fecha1), fecha_mysql($fecha2), $cuedep_id, $cliente_id, $ventaVeh);
?>

<div class="table-responsive">
    <table cellspacing="1" id="tabla_deposito" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">ID</th>
                <th id="tabla_cabecera_fila">FECHA</th>
                <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
                <th id="tabla_cabecera_fila">MONTO DEPÓSITO</th>
                <th id="tabla_cabecera_fila" width="17%">N° OPERACIÓN</th>
                <th id="tabla_cabecera_fila" width="20%">CLIENTE</th>
                <th id="tabla_cabecera_fila">CUENTA</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th id="tabla_cabecera_fila" width="20%">CLIENTE/OBSERVACIONES</th>
                <th id="tabla_cabecera_fila" width="10%">&nbsp; <?php echo '' ?></th>
            </tr>
        </thead>
        <?php
        if ($dts['estado'] == 1) {
            ?>  
            <tbody>
                <?php
                foreach ($dts['data']as $key => $dt) {
                    ?>
                    <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila"><?php echo $dt['tb_deposito_id'] ?></td>
                        <td id="tabla_fila" data-sort="<?php echo mostrar_fecha($dt['tb_deposito_fec']) ?>"><?php echo mostrar_fecha($dt['tb_deposito_fec']) ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_deposito_des'] ?></td>
                        <td id="tabla_fila"><?php echo mostrar_moneda($dt['tb_deposito_mon']) ?></td>
                        <td id="tabla_fila">

                            <?php echo '<input type="text" value="' . $dt['tb_deposito_num'] . '" id="input_' . $dt['tb_deposito_id'] . '" class="form-control input-sm">'; ?>

                            <?php if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6): ?>
                                <a class="btn btn-facebook btn-xs" href="javascript:void(0)" onClick="modificar_num(<?php echo $dt['tb_deposito_id'] ?>)" title="Guardar"><i class="fa fa-floppy-o"></i></a>
                            <?php endif; ?>
                            <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="upload_form('I',<?php echo $dt['tb_deposito_id'] ?>);" title="Subir"><i class="fa fa-picture-o"></i></a>
                        </td>
                        <td id="tabla_fila"><!--registrar cliente-->
                            <?php 
                                $result=$oCliente->mostrarUno($dt['tb_cliente_id']);
                                $tb_cliente_nom="";
                                if($result['estado']==1){
                                    $tb_cliente_nom=$result['data']['tb_cliente_nom'];
                                }
                                echo $tb_cliente_nom;
                            
                            ?>
                        </td>
                        <td id="tabla_fila"><?php echo $dt['tb_cuentadeposito_nom'] ?></td>
                        <td id="tabla_fila"><?php echo ($dt['tb_deposito_est'] == 0) ? '<label style="color: green;"><b>Libre</b></label>' : '<b>Usado</b>'; ?></td>
                        <td id="tabla_fila" align="left" valign="top">
                            <div style="display: block; width: 100%;">
                                <textarea rows="1" id="<?php echo 'txt_deposito_detalle_' . $dt['tb_deposito_id']; ?>" cols="40"  class="form-control input-sm mayus"><?php echo $dt['tb_deposito_det']; ?></textarea>
                                <a  class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="agregar_detalle(<?php echo $dt['tb_deposito_id'] ?>)" title="Guardar"><i class="fa fa-floppy-o"></i></a>
                            </div>
                        </td>
                        <td id="tabla_fila" align="center">
                            <a  class="btn btn-info btn-xs" href="javascript:void(0)" onClick="Abrir_imagen(<?php echo $dt['tb_deposito_id'] ?>);" title="Ver Depósito"><i class="fa fa-eye"></i></a>

                            <?php if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6): ?>
                                <a  class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="deposito_usar_form('usar',<?php echo $dt['tb_deposito_id'] ?>)" title="Usar"><i class="fa fa-check-circle"></i></a>
                                <a  class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="eliminar_deposito('<?php echo $dt['tb_deposito_id'] ?>')" title="Eliminar"><i class="fa fa-trash"></i></a>
                            
                                <?php endif; ?>
                            <a  class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="agregar_cliente(<?php echo $dt['tb_deposito_id'] ?>)" title="Agregar cliente"><i class="fa fa-user"></i></a>
                            <div class="btn-group">
                                <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="true"><span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a class="btn" onclick="deposito_timeline('<?php echo $dt['tb_deposito_id'] ?>')"><b>Historial</b></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
            <?php
        }
        ?>
    </table>
</div>