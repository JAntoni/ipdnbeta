<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Deposito.class.php");
$oDeposito = new Deposito();

if ($_POST['action'] == "editar") {
    $dts = $oDeposito->mostrarUno($_POST['deposito_id']);
    if ($dts['estado'] == 1) {
        $nom = $dts['data']['tb_deposito_nom'];
        $des = $dts['data']['tb_deposito_des'];
    }
}
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_deposito_manual" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">DEPÓSITO MANUAL REGISTRO</h4>
            </div>
            <form id="for_manual">
                <input name="action" id="action" type="hidden" value="<?php echo $_POST['action'] ?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Cuenta Depósito:</label>
                            <select name="cmb_cuedep_manu_id" id="cmb_cuedep_manu_id" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Depósito Fecha: </label>
                                <div class="input-group">
                                    <div class='input-group date' id='datetimepicker'>
                                        <input type="text" name="txt_deposito_fec" id="txt_deposito_fec" class="form-control input-sm" value="<?php echo date('d-m-Y');?>">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Depósito Monto:</label>
                            <input type="text" name="txt_deposito_mon" id="txt_deposito_mon" class="form-control input-sm moneda text-right">
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Depósito N° Ope:</label>
                            <input type="text" name="txt_deposito_num" id="txt_deposito_num" class="form-control input-sm">
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Descripción</label>
                            <textarea name="txt_deposito_des" id="txt_deposito_des" rows="3" cols="40"  class="form-control input-sm"></textarea>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="deposito_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_deposito">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/deposito/deposito_manual_form.js'; ?>"></script>