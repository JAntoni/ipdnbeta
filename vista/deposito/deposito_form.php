<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Deposito.class.php");
$oDeposito = new Deposito();

if ($_POST['action'] == "editar") {
    $dts = $oDeposito->mostrarUno($_POST['deposito_id']);
    if ($dts['estado'] == 1) {
        $nom = $dts['data']['tb_deposito_nom'];
        $des = $dts['data']['tb_deposito_des'];
    }
}
?>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_deposito" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">INFORMACIÓN DE DEPÓSITO</h4>
                </div>
                <form id="for_deposito" enctype="multipart/form-data">
                    <input name="action" id="action" type="hidden" value="<?php echo $_POST['action']?>">
                    <input name="hdd_deposito_id" id="hdd_deposito_id" type="hidden" value="<?php echo $_POST['are_id']?>">

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txt_deposito_nom" class="control-label">Cuenta Depósito:</label>
                            <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm"></select>
                        </div>
                        <div class="form-group">
                            <label for="txt_deposito_des" class="control-label">Archivo</label>
                            <!--<input type="file"  class="form-control input-sm" name="file" id="file">-->
                            <input type="file"  class="form-control input-sm" name="file" id="file" accept=".xls,.xlsx,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        
                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="deposito_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                                <button type="submit" class="btn btn-info" id="btn_guardar_deposito">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>


<script type="text/javascript" src="<?php echo 'vista/deposito/deposito_form.js'; ?>"></script>
