/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
//    deposito_tabla();

    $("#txt_cliente_nombre").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            console.log(ui);
            $('#txt_cliente_id').val(ui.item.cliente_id);
            $('#txt_cliente_nombre').val(ui.item.cliente_nom);
//            deposito_tabla();
            event.preventDefault();
            $('#txt_cliente_nombre').focus();
        }
    });










    $('#for_cliente').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "deposito/deposito_controller.php",
                async: true,
                dataType: "json",
                data: $("#for_cliente").serialize(),
                beforeSend: function () {
//                    $('#area_mensaje').show(400);
//                    $('#btn_guardar_area').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        swal_success("SISTEMA",data.mensaje,3000);
                        setTimeout(function () {
                            deposito_tabla();
                            $('#modal_deposito_cliente').modal('hide');
                        }, 1000
                                );
                    }else{
                        swal_warning("AVISO",data.mensaje,3000);
                    } 
                },
                complete: function (data) {
                    //console.log(data);
                }
            });
        },
        rules: {
            txt_cliente_nombre: {
                required: true,
                minlength: 2
            },
            txt_cliente_id: {
                required: true
            }
        },
        messages: {
            txt_cliente_nombre: {
                required: "Ingrese un Cliente para poder Registrar",
                minlength: "Como mínimo el nombre debe tener 2 caracteres"
            },
            txt_cliente_id: {
                required: "Ingrese un Cliente para poder Registrar",
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });



});