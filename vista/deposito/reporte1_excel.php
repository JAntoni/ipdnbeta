<?php
//echo 'hasta aki estoy entrando normal '; exit();
//require_once('../../core/usuario_sesion.php');
//error_reporting(0);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';
//echo 'HASTA AKI ESTOY ENTRANDO AL SISTEMA DE MANERA NORMAL'; exit();

require_once ("Deposito.class.php");
$oDeposito = new Deposito();
require_once ('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");



//$fecha1=(empty($_POST['txt_filtro_fec1']))? fecha_mysql($fecha1) : fecha_mysql($_POST['txt_filtro_fec1']);
//$fecha2=(empty($_POST['txt_filtro_fec2']))? fecha_mysql($fecha2) : fecha_mysql($_POST['txt_filtro_fec2']);
$fecha1=fecha_mysql($_GET['fecha1']);
$fecha2=fecha_mysql($_GET['fecha2']);


//echo 'fecha 1= '.$fecha1.' fecha 2= '.$fecha2.' empresa 1='.$emp_id.' caja ='.$caj_id; exit();


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("cristhian.cyga@gmail.com")
	->setLastModifiedBy("cristhian.cyga@gmail.com")
	->setTitle("Reporte De Cierre de Caja")
	->setSubject("Flujo de Caja")
	->setDescription("Reporte generado por cristhian.cyga@gmail.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '135896')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

  $nombre_archivo = "IPDNSAC - ".mostrar_fecha($fecha1);

  $c = 1;
  $titulo = "LISTA DE CLIENTES DECLARABLES";
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("A$c:D$c")->applyFromArray($estiloTituloColumnas);
  $titulo2 = 'Desde '.mostrar_fecha($fecha1).' hasta '.mostrar_fecha($fecha2);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:H$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", $titulo2);
  $objPHPExcel->getActiveSheet()->getStyle("E$c:H$c")->applyFromArray($estiloTituloColumnas);
//  $objPHPExcel->getActiveSheet()->setCellValue("D1", mostrar_fecha($fecha1));

  $c = 2;
//  $nombre_fecha = "FORMATO DE APERTURA Y CIERRE DE CAJA DE LA FECHA: ".mostrar_fecha($fecha1)." HASTA ".mostrar_fecha($fecha2);
//  $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $nombre_fecha);

  $c = $c + 1;

  $titulosColumnas = array(
    'FECHA',
    'DESCRIPCIÓN',
    'MONTO',
    'N° OPERAACIÓN',
    'DNI',
    'CLIENTE',
    'CUENTA',
    'TIPO DE CLIENTE'
  );

  $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
  ;

  $objPHPExcel->getActiveSheet()->getStyle("A$c:H$c")->applyFromArray($estiloTituloColumnas);

  $c = $c+1;
  
  $tipo = "";
$estado = "";
$total_ingresos = 0;
//echo 'sfahsahfsa == '.$result['estado'];          exit();
$result = $oDeposito->mostrarTodos_declarables(fecha_mysql($fecha1), fecha_mysql($fecha2), 0, 0);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
          
          $result2=$oCliente->mostrarUno($value['tb_cliente_id']);
            $cliente_dni="";
            $cliente_nombre="";
            if($result2['estado']==1){
                $cliente_dni=$result2['data']['tb_cliente_doc'];
                $cliente_nombre=$result2['data']['tb_cliente_nom'];
                $cliente_declarable=$result2['data']['tb_cliente_declaracion'];
            }
            
            if($cliente_declarable==1){
                $tipo="DECLARABLE";
            }

        $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("F$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloFilas);
        
        $objPHPExcel->getActiveSheet()
          ->setCellValue('A'.$c, mostrar_fecha_hora($value['tb_cliente_fecha_declaracion']))
          ->setCellValue('B'.$c, $value['tb_deposito_des'])
          ->setCellValue('C'.$c, mostrar_moneda($value['tb_deposito_mon']))
          ->setCellValue('D'.$c, $value['tb_deposito_num'])
          ->setCellValue('E'.$c, $cliente_dni)
          ->setCellValue('F'.$c, $cliente_nombre)
          ->setCellValue('G'.$c, $value['tb_cuentadeposito_nom'])
          ->setCellValue('H'.$c, $tipo)
        ;
//        $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//        $objPHPExcel->getActiveSheet()->getStyle('I'.$c)->getNumberFormat()->setFormatCode('00000000');
        $objPHPExcel->getActiveSheet(0)->getStyle('C'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        

        $c++;
      }
    }
  $result = NULL;

//  $c=$c+1;
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:H$c");

  //FUNCIONA CUADO QUIERES MOSTRAR CEROS A LA IZQUIERDA: EJMP: 000001. 0000003
  //$objPHPExcel->getActiveSheet()->getStyle('K'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

  //for($z = 'A'; $z <= 'O'; $z++){
    //$objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
  //}
  
  
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
  

  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('CLIENTES DECLARABLES');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0*/
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=".$nombre_archivo.".csv");
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
