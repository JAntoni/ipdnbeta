
$(document).ready(function () {
    cmb_cuedep_id(0);

    $('#datetimepicker').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });


    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '-9999999.00',
        vMax: '9999999.00'
    });

});

function cmb_cuedep_id(ids){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuentadeposito/cuentadeposito_select.php",
        async: false,
        dataType: "html",
        data: ({
            cuentadeposito_id: ids
        }),
        beforeSend: function (){
            $('#cmb_cuedep_manu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_cuedep_manu_id').html(html);
        }
    });
}

$("#for_manual").validate({
    submitHandler: function () {

        $.ajax({
            type: "POST",
            url: VISTA_URL + "deposito/deposito_reg.php",
            async: true,
            dataType: "json",
            data: $('#for_manual').serialize(),
            beforeSend: function () {

            },
            success: function (data) {
                if (parseInt(data.estado) == 1) {
                    swal_success("SISTEMA", data.mensaje, 2500);
                    deposito_tabla();
                    $('#modal_registro_deposito_manual').modal('hide');
                } else {
                    swal_warning("SISTEMA", data.mensaje, 2500);

                }
            },
            complete: function (data) {
                if (data.statusText != 'success') {
//                    $('#form_msj_manual').html(data.responseText);
                    console.log(data);
                }
            }
        });
    },
    rules: {
        cmb_cuedep_manu_id: {
            required: true
        },
        txt_deposito_num: {
            required: true
        },
        txt_deposito_mon: {
            required: true
        }
    },
    messages: {
        cmb_cuedep_manu_id: {
            required: 'Elija una cuenta'
        },
        txt_deposito_num: {
            required: 'Número Operacion'
        },
        txt_deposito_mon: {
            required: 'Ingrese Monto'
        }
    }
});