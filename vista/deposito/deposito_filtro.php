
<?php
    $fec1 = date('d-m-Y');
    $fec2 = date('d-m-Y');
?>

<form id="deposito_filtro" name="deposito_filtro">
    <input type="hidden" class="form-control input-sm" id="usuariogrupo_id" name="usuariogrupo_id" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha </label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input name="txt_fil_fec1" type="text" id="txt_fil_fec1" value="<?php echo $fec1 ?>" size="8" readonly class="form-control input-sm">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon"> - </span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input name="txt_fil_fec2" type="text" id="txt_fil_fec2" value="<?php echo $fec2 ?>" size="8" readonly class="form-control input-sm">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Cuenta Depósito:</label>
                <select name="cmb_fil_cuedep_id" id="cmb_fil_cuedep_id" class="form-control input-sm"></select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Cliente</label>
                <input type="hidden" name="hdd_cliente_fil_id" id="hdd_cliente_fil_id"  class="form-control input-sm">
                <input type="text" name="txt_cliente_fil_nom" id="txt_cliente_fil_nom" class="form-control input-sm">
            </div>
        </div>
        <?php
            if ($_SESSION['usuariogrupo_id'] == 2 )
            {
                echo 
                '
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="cmbventaveh">Visualizar:</label>
                        <select name="cmbventaveh" id="cmbventaveh" class="form-control input-sm">
                            <option value="-1">Todos</option>
                            <option value="0">Depósitos</option>
                            <option value="1">Venta vehicular</option>
                        </select>
                    </div>
                </div>
                ';
            }
            else
            {
                echo 
                '
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="cmbventaveh">Visualizar:</label>
                        <select name="cmbventaveh" id="cmbventaveh" class="form-control input-sm">
                            <option value="0">Depósitos</option>
                        </select>
                    </div>
                </div>
                ';
            }
        ?>
        <div class="col-md-2">
            <div class="form-group">
                <label for=""> &nbsp;</label>
                <div class="input-group">
                    <button type="button" class="btn btn-primary btn-sm" onclick="deposito_tabla()" title="Filtrar"><i class="fa fa-search fa-fw"></i> Buscar</button>
                    <!--<span class="input-group-addon"></span>-->
                </div>
            </div>
        </div>

    </div>

</form>