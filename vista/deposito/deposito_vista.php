<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <?php if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6): ?>
                    <button class="btn btn-primary btn-sm" onclick="deposito_form('insertar', 0)"><i class="fa fa-plus"></i> Agregar</button>
                    <button class="btn btn-danger btn-sm" onclick="deposito_manual('manual')"><i class="fa fa-plus"></i> Depósito Manual</button>
                    <button class="btn btn-success btn-sm" onclick="reporte_Clientes_Declarables()"><i class="fa fa-file-excel-o"></i> Clientes Declarables</button>
                <?php endif; ?>


            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtro de Ingresos</label>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php include 'deposito_filtro.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="deposito_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_deposito_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('deposito_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_deposito_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_deposito_usar_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_deposito_manual_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_deposito_timeline_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_upload_form"></div>
            <div id="div_modal_imagen_form"></div>
            <div id="div_deposito_cliente"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
