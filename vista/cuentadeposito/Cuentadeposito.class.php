<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cuentadeposito extends Conexion{

    function insertar($moneda_id, $cuentadeposito_nom, $cuentadeposito_num, $cuentadeposito_ban, $representante_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuentadeposito(tb_cuentadeposito_xac, tb_moneda_id, tb_cuentadeposito_nom, tb_cuentadeposito_num, tb_cuentadeposito_ban, tb_representante_id)
          VALUES (1, :moneda_id, :cuentadeposito_nom, :cuentadeposito_num, :cuentadeposito_ban, :representante_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuentadeposito_nom", $cuentadeposito_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cuentadeposito_num", $cuentadeposito_num, PDO::PARAM_STR);
        $sentencia->bindParam(":cuentadeposito_ban", $cuentadeposito_ban, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($cuentadeposito_id, $moneda_id, $cuentadeposito_nom, $cuentadeposito_num, $cuentadeposito_ban, $representante_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuentadeposito 
                SET 
                tb_moneda_id =:moneda_id, 
                tb_cuentadeposito_nom =:cuentadeposito_nom, 
                tb_cuentadeposito_num =:cuentadeposito_num, 
                tb_cuentadeposito_ban =:cuentadeposito_ban,
                tb_representante_id =:representante_id
                WHERE tb_cuentadeposito_id =:cuentadeposito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuentadeposito_id", $cuentadeposito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuentadeposito_nom", $cuentadeposito_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cuentadeposito_num", $cuentadeposito_num, PDO::PARAM_STR);
        $sentencia->bindParam(":cuentadeposito_ban", $cuentadeposito_ban, PDO::PARAM_STR);
        $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cuentadeposito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuentadeposito SET tb_cuentadeposito_xac = 0 WHERE tb_cuentadeposito_id =:cuentadeposito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuentadeposito_id", $cuentadeposito_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cuentadeposito_id){
      try {
        $sql = "SELECT * FROM tb_cuentadeposito cudep INNER JOIN tb_moneda mon on mon.tb_moneda_id = cudep.tb_moneda_id WHERE tb_cuentadeposito_id =:cuentadeposito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuentadeposito_id", $cuentadeposito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cuentadepositos(){
      try {
        $sql = "SELECT 
                        * 
                FROM 
                        tb_cuentadeposito cd 
                        INNER JOIN tb_moneda m ON cd.tb_moneda_id=m.tb_moneda_id
                WHERE 
                        tb_cuentadeposito_xac=1 ORDER BY tb_cuentadeposito_nom";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function listar_cuentadepositos_representantexx($representante_id){
      try {
        $sql = "SELECT 
                        * 
                FROM 
                        tb_cuentadeposito cd 
                        INNER JOIN tb_moneda m ON cd.tb_moneda_id=m.tb_moneda_id
                        INNER JOIN tb_representante r ON r.tb_representante_id=cd.tb_representante_id
                WHERE 
                        tb_cuentadeposito_xac=1 AND r.tb_representante_id= :representante_id
                        ORDER BY tb_cuentadeposito_nom";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    function listar_cuentadepositos_representante($representante_id){
        $condicional = "";
        if(intval($representante_id)>0){
            $condicional = " AND r.tb_representante_id= :representante_id";
        }
      try {
        $sql = "SELECT 
                        * 
                FROM 
                        tb_cuentadeposito cd 
                        INNER JOIN tb_moneda m ON cd.tb_moneda_id=m.tb_moneda_id
                        INNER JOIN tb_representante r ON r.tb_representante_id=cd.tb_representante_id
                WHERE 
                        tb_cuentadeposito_xac=1 ".$condicional;

        $sentencia = $this->dblink->prepare($sql);
        if(intval($representante_id)>0){
            $sentencia->bindParam(":representante_id", $representante_id, PDO::PARAM_INT);
        }
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
