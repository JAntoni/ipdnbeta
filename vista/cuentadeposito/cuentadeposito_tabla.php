<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'cuentadeposito/Cuentadeposito.class.php');
    require_once(VISTA_URL.'representante/Representante.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../cuentadeposito/Cuentadeposito.class.php');
    require_once('../representante/Representante.class.php');
    require_once('../funciones/fechas.php');
  }
  $oCuentadeposito = new Cuentadeposito();
  $oRepresentante = new Representante();

?>
<table id="tbl_horario" class="table table-bordered table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th>ID</th>
      <th>Representante</th>
      <th>Nombre de Cuenta</th> 
      <th>Moneda</th>
      <th>Número de Cuenta</th>
      <th>Banco</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oCuentadeposito->listar_cuentadepositos();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): 
            $result1 = $oRepresentante->mostrarUno($value['tb_representante_id']);
            if($result1['estado'] == 1){
                $representante = $result1['data']['tb_representante_nom'];
            }
            else{
                $representante = '';
            }
            ?> 
          <tr> 
            <td><?php echo $value['tb_cuentadeposito_id'];?></td>
            <td><?php echo $representante?></td>
            <td><?php echo $value['tb_cuentadeposito_nom'];?></td>
            <td><?php echo $value['tb_moneda_nom'];?></td>
            <td><?php echo $value['tb_cuentadeposito_num']; ?></td>
            <td><?php echo $value['tb_cuentadeposito_ban']; ?></td>
            <td align="center">
              <a class="btn btn-warning btn-xs" title="Editar" onclick="cuentadeposito_form(<?php echo "'M', ".$value['tb_cuentadeposito_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cuentadeposito_form(<?php echo "'E', ".$value['tb_cuentadeposito_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
