<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cuentadeposito/Cuentadeposito.class.php');
  $oCuentadeposito = new Cuentadeposito();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$moneda_id = intval($_POST['cmb_moneda_id']);
 		$representante_id = intval($_POST['cmb_rep_id']);
 		$cuentadeposito_nom = mb_strtoupper($_POST['txt_cuentadeposito_nom'], 'UTF-8');
 		$cuentadeposito_num = $_POST['txt_cuentadeposito_num'];
 		$cuentadeposito_ban = $_POST['txt_cuentadeposito_ban'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar La Cuenta.';
 		if($oCuentadeposito->insertar($moneda_id, $cuentadeposito_nom, $cuentadeposito_num, $cuentadeposito_ban, $representante_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cuenta de Deposito registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$cuentadeposito_id = intval($_POST['hdd_cuentadeposito_id']);
 		$moneda_id = intval($_POST['cmb_moneda_id']);
 		$cuentadeposito_nom = mb_strtoupper($_POST['txt_cuentadeposito_nom'], 'UTF-8');
 		$cuentadeposito_num = $_POST['txt_cuentadeposito_num'];
 		$cuentadeposito_ban = $_POST['txt_cuentadeposito_ban'];
                $representante_id = intval($_POST['cmb_rep_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar La Cuenta.';

 		if($oCuentadeposito->modificar($cuentadeposito_id, $moneda_id, $cuentadeposito_nom, $cuentadeposito_num, $cuentadeposito_ban, $representante_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cuenta de Deposito modificado correctamente. '.$cuentadeposito_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$cuentadeposito_id = intval($_POST['hdd_cuentadeposito_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar La Cuenta.';

 		if($oCuentadeposito->eliminar($cuentadeposito_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cuenta de Deposito eliminado correctamente. '.$cuentadeposito_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>