<?php
require_once('Cuentadeposito.class.php');
$oCuentadeposito = new Cuentadeposito();

$cuentadeposito_id = (isset($_POST['cuentadeposito_id']))? intval($_POST['cuentadeposito_id']) : intval($cuentadeposito_id); //?esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$option = '<option value="">-</option>';

//PRIMER NIVEL
$result = $oCuentadeposito->listar_cuentadepositos();
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value){   
      $cantidad =intval(strlen($value['tb_cuentadeposito_num']))-4;
      $digitos = substr($value['tb_cuentadeposito_num'], $cantidad, 4);
      $selected = '';
      if($cuentadeposito_id == $value['tb_cuentadeposito_id'])
        $selected = 'selected';

      $option .= '<option value="'.$value['tb_cuentadeposito_id'].'" data-moneda="'.$value['tb_moneda_id'].'" '.$selected.'>'.$value['tb_cuentadeposito_nom'].' / '.$digitos.'</option>';
      
    }
  }
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>