
$(document).ready(function(){
	console.log('cambios guardados');
  $('#form_cuentadeposito').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"cuentadeposito/cuentadeposito_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cuentadeposito").serialize(),
				beforeSend: function() {
					$('#cuentadeposito_mensaje').show(400);
					$('#btn_guardar_cuentadeposito').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#cuentadeposito_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#cuentadeposito_mensaje').html(data.mensaje);
                        var vista = $("#hdd_vista2").val();
                        console.log(vista);
                        alerta_success("EXITO","CUENTA DEPOSITO AGREGADO");
                        if(vista =='cuentadeposito'){
                            cuentadeposito_tabla();
                        }
                        if(vista =='creditogarveh'){
                            cargar_cuentadeposito();
                        }
                        $('#modal_registro_cuentadeposito').modal('hide');
					}
					else{
		      	$('#cuentadeposito_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#cuentadeposito_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_cuentadeposito').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#cuentadeposito_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#cuentadeposito_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	cmb_moneda_id: {
	  		required: true,
				min: 1
	  	},
			txt_cuentadeposito_nom: {
				required: true,
				minlength: 2
			},
			txt_cuentadeposito_num: {
				required: true,
				minlength: 5
			},
			txt_cuentadeposito_ban: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			cmb_moneda_id: {
				required: "Seleccione un tipo de Moneda",
				min: "Seleccione un tipo de Moneda"
			},
			txt_cuentadeposito_nom: {
				required: "Ingrese un nombre para la Cuenta",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_cuentadeposito_num: {
				required: "Ingrese un número de Cuenta",
				minlength: "El número de cuenta debe tener como mínimo 5 caracteres"
			},
			txt_cuentadeposito_ban: {
				required: "Ingrese un nombre de Banco",
				minlength: "El nombre de banco debe tener como mínimo 2 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});

function representante_form2(usuario_act, representante_id){ 
    console.log("B");
  $.ajax({
		type: "POST",
		url: VISTA_URL+"representante/representante_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: 'cuentadeposito', // 
      representante_id: representante_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_representante_form').html(data);
      	$('#modal_registro_representante').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_representante'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_representante', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'representante';
      	var div = 'div_modal_representante_form';
      	permiso_solicitud(usuario_act, representante_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function cargar_representante(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "representante/representante_select.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#cmb_rep_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_rep_id').html(html);
        }
    });
}
