<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculouso/Vehiculouso.class.php');
  $oVehiculouso = new Vehiculouso();

 	$action = $_POST['action'];
        
        $vehiculouso_nom = mb_strtoupper($_POST['txt_vehiculouso_nom'], 'UTF-8');
 	$vehiculouso_des = $_POST['txt_vehiculouso_des'];
 	$vehiculouso_usureg = $_SESSION['usuario_id'];

        $oVehiculouso->setVehiculouso_nom($vehiculouso_nom);
        $oVehiculouso->setVehiculouso_des($vehiculouso_des);
        $oVehiculouso->setVehiculouso_usureg($vehiculouso_usureg);
        
 	if($action == 'insertar'){

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Vehiculouso.';
 		if($oVehiculouso->insertar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Vehiculouso registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$vehiculouso_id = intval($_POST['hdd_vehiculouso_id']);
                $oVehiculouso->setVehiculouso_id($vehiculouso_id);
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Vehiculouso.';

 		if($oVehiculouso->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Vehiculouso modificado correctamente. '.$vehiculouso_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculouso_id = intval($_POST['hdd_vehiculouso_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Vehiculouso.';

 		if($oVehiculouso->eliminar($vehiculouso_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Vehiculouso eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>