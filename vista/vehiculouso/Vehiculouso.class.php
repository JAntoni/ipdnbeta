<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculouso extends Conexion{
      private $vehiculouso_id;
      private $vehiculouso_nom;
      private $vehiculouso_des;
      private $vehiculouso_usureg;

      public function getVehiculouso_id() {
        return $this->vehiculouso_id;
      }
    
      public function setVehiculouso_id($vehiculouso_id){
        $this->vehiculouso_id = $vehiculouso_id;
      }
      
      public function getVehiculouso_nom() {
        return $this->vehiculouso_nom;
      }
    
      public function setVehiculouso_nom($vehiculouso_nom){
        $this->vehiculouso_nom = $vehiculouso_nom;
      }
      
      public function getVehiculouso_des() {
        return $this->vehiculouso_des;
      }
    
      public function setVehiculouso_des($vehiculouso_des){
        $this->vehiculouso_des = $vehiculouso_des;
      }
      
      public function getVehiculouso_usureg() {
        return $this->vehiculouso_usureg;
      }
    
      public function setVehiculouso_usureg($vehiculouso_usureg){
        $this->vehiculouso_usureg = $vehiculouso_usureg;
      }
    
    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_vehiculouso(tb_vehiculouso_nom, tb_vehiculouso_des,tb_vehiculouso_usureg)
          VALUES (:vehiculouso_nom, :vehiculouso_des, :vehiculouso_usureg)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculouso_nom", $this->getVehiculouso_nom(), PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculouso_des", $this->getVehiculouso_des(), PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculouso_usureg", $this->getVehiculouso_usureg(), PDO::PARAM_INT);

        $result = $sentencia->execute();
        $vehiculouso_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['transferente_id'] = $vehiculouso_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculouso SET 
                tb_vehiculouso_nom =:vehiculouso_nom, 
                tb_vehiculouso_des =:vehiculouso_des 
                WHERE tb_vehiculouso_id =:vehiculouso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculouso_nom", $this->getVehiculouso_nom(), PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculouso_des", $this->getVehiculouso_des(), PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculouso_id", $this->getVehiculouso_id(), PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculouso_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculouso SET tb_vehiculouso_xac=0 WHERE tb_vehiculouso_id =:vehiculouso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculouso_id", $vehiculouso_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculouso_id){
      try {
        $sql = "SELECT * FROM tb_vehiculouso WHERE tb_vehiculouso_id =:vehiculouso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculouso_id", $vehiculouso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculousos(){
      try {
        $sql = "SELECT * FROM tb_vehiculouso WHERE tb_vehiculouso_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
