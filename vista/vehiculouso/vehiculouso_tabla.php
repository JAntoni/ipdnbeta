<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Vehiculouso.class.php');
  $oVehiculouso = new Vehiculouso();

  $result = $oVehiculouso->listar_vehiculousos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td id="fila" style="border-left: 1px solid #135896;">'.$value['tb_vehiculouso_id'].'</td>
          <td  id="fila">'.$value['tb_vehiculouso_nom'].'</td>
          <td  id="fila">'.$value['tb_vehiculouso_des'].'</td>
          <td align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="vehiculouso_form(\'L\','.$value['tb_vehiculouso_id'].')"><i class="fa fa-eye"></i> Ver</a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="vehiculouso_form(\'M\','.$value['tb_vehiculouso_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="vehiculouso_form(\'E\','.$value['tb_vehiculouso_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_vehiculousos" class="table table-bordered table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th id="filacabecera">ID</th>
      <th id="filacabecera">Nombre</th>
      <th id="filacabecera">Descripción</th>
      <th id="filacabecera">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
