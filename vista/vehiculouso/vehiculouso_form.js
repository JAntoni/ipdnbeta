
$(document).ready(function(){
  $('#form_vehiculouso').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculouso/vehiculouso_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculouso").serialize(),
				beforeSend: function() {
					$('#vehiculouso_mensaje').show(400);
					$('#btn_guardar_vehiculouso').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculouso_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculouso_mensaje').html(data.mensaje);
                        var vista = $("#hdd_vista2").val();
                        console.log(vista);
                        alerta_success("EXITO","VEHICULO USO AGREGADO");
                        if(vista =='vehiculouso'){
                            vehiculouso_tabla();
                        }
                        if(vista =='controlseguro'){
                            cargar_vehiculousoo();
                        }
                        $('#modal_registro_vehiculouso').modal('hide');
					}
					else{
		      	$('#vehiculouso_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculouso_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculouso').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculouso_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculouso_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_vehiculouso_nom: {
				required: true,
				minlength: 2
			},
			txt_vehiculouso_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_vehiculouso_nom: {
				required: "Ingrese un nombre para el Vehiculouso",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_vehiculouso_des: {
				required: "Ingrese una descripción para el Vehiculouso",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
