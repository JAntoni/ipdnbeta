<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Compracontadetalle extends Conexion{
  public $tb_compracontadetalle_id;
  public $tb_cuentaconta_cod;
  public $tb_cuentaconta_idfk;
  public $tb_compracontadetalle_glosa;
  public $tb_compracontadetalle_porcentaje;
  public $tb_compracontadetalle_monto;
  public $tb_empresa_idfk;
  public $tb_gastogar_idfk;
  public $tb_gasto_idfk;
  public $tb_compracontadoc_idfk;
  public $tb_compracontadetalle_orden;
  public $tb_compracontadetalle_fecreg;
  public $tb_compracontadetalle_usureg;
  public $tb_compracontadetalle_fecmod;
  public $tb_compracontadetalle_usumod;
  public $tb_compracontadetalle_xac;
  public $tb_gastopago_idfk;

  public function getTbCompracontadetalleId(){ return $this->tb_compracontadetalle_id; }
  public function setTbCompracontadetalleId($tb_compracontadetalle_id){ $this->tb_compracontadetalle_id = $tb_compracontadetalle_id; return $this; }

  public function getTbCuentacontaCod(){ return $this->tb_cuentaconta_cod; }
  public function setTbCuentacontaCod($tb_cuentaconta_cod){ $this->tb_cuentaconta_cod = $tb_cuentaconta_cod; return $this; }

  public function getTbCuentacontaIdfk(){ return $this->tb_cuentaconta_idfk; }
  public function setTbCuentacontaIdfk($tb_cuentaconta_idfk){ $this->tb_cuentaconta_idfk = $tb_cuentaconta_idfk; return $this; }

  public function getTbCompracontadetalleGlosa(){ return $this->tb_compracontadetalle_glosa; }
  public function setTbCompracontadetalleGlosa($tb_compracontadetalle_glosa){ $this->tb_compracontadetalle_glosa = $tb_compracontadetalle_glosa; return $this; }

  public function getTbCompracontadetalleMonto(){ return $this->tb_compracontadetalle_monto; }
  public function setTbCompracontadetalleMonto($tb_compracontadetalle_monto){ $this->tb_compracontadetalle_monto = $tb_compracontadetalle_monto; return $this; }

  public function getTbCompracontadetallePorcentaje(){ return $this->tb_compracontadetalle_porcentaje; }
  public function setTbCompracontadetallePorcentaje($tb_compracontadetalle_porcentaje){ $this->tb_compracontadetalle_porcentaje = $tb_compracontadetalle_porcentaje; return $this; }

  public function getTbEmpresaIdfk(){ return $this->tb_empresa_idfk; }
  public function setTbEmpresaIdfk($tb_empresa_idfk){ $this->tb_empresa_idfk = $tb_empresa_idfk; return $this; }

  public function getTbGastogarIdfk(){ return $this->tb_gastogar_idfk; }
  public function setTbGastogarIdfk($tb_gastogar_idfk){ $this->tb_gastogar_idfk = $tb_gastogar_idfk; return $this; }

  public function getTbGastoIdfk(){ return $this->tb_gasto_idfk; }
  public function setTbGastoIdfk($tb_gasto_idfk){ $this->tb_gasto_idfk = $tb_gasto_idfk; return $this; }

  public function getTbCompracontadocIdfk(){ return $this->tb_compracontadoc_idfk; }
  public function setTbCompracontadocIdfk($tb_compracontadoc_idfk){ $this->tb_compracontadoc_idfk = $tb_compracontadoc_idfk; return $this; }

  public function getTbCompracontadetalleOrden(){ return $this->tb_compracontadetalle_orden; }
  public function setTbCompracontadetalleOrden($tb_compracontadetalle_orden){ $this->tb_compracontadetalle_orden = $tb_compracontadetalle_orden; return $this; }

  public function getTbCompracontadetalleFecreg(){ return $this->tb_compracontadetalle_fecreg; }
  public function setTbCompracontadetalleFecreg($tb_compracontadetalle_fecreg){ $this->tb_compracontadetalle_fecreg = $tb_compracontadetalle_fecreg; return $this; }

  public function getTbCompracontadetalleUsureg(){ return $this->tb_compracontadetalle_usureg; }
  public function setTbCompracontadetalleUsureg($tb_compracontadetalle_usureg){ $this->tb_compracontadetalle_usureg = $tb_compracontadetalle_usureg; return $this; }

  public function getTbCompracontadetalleFecmod(){ return $this->tb_compracontadetalle_fecmod; }
  public function setTbCompracontadetalleFecmod($tb_compracontadetalle_fecmod){ $this->tb_compracontadetalle_fecmod = $tb_compracontadetalle_fecmod; return $this; }

  public function getTbCompracontadetalleUsumod(){ return $this->tb_compracontadetalle_usumod; }
  public function setTbCompracontadetalleUsumod($tb_compracontadetalle_usumod){ $this->tb_compracontadetalle_usumod = $tb_compracontadetalle_usumod; return $this; }

  public function getTbCompracontadetalleXac(){ return $this->tb_compracontadetalle_xac; }
  public function setTbCompracontadetalleXac($tb_compracontadetalle_xac){ $this->tb_compracontadetalle_xac = $tb_compracontadetalle_xac; return $this; }

  public function insertar(){
    $this->dblink->beginTransaction();
    try {
      $column_opc = "";
      if(!empty($this->tb_gasto_idfk)){
        $column_opc = $this->tb_gastopago_idfk > 0 ? ",\ntb_gasto_idfk,\ntb_gastopago_idfk" : ",\ntb_gasto_idfk";
      } elseif(!empty($this->tb_gastogar_idfk)){
        $column_opc = ",\ntb_gastogar_idfk";
      }
      if(!empty($column_opc)){
        $param_opc = $this->tb_gastopago_idfk > 0 ? ",\n:param_opc,\n:param_opc1" : ",\n:param_opc";
      } else {
        $param_opc = '';
      }

      $sql = "INSERT INTO tb_compracontadetalle(
                tb_cuentaconta_cod,
                tb_cuentaconta_idfk,
                tb_compracontadetalle_glosa,
                tb_compracontadetalle_monto,
                tb_empresa_idfk,
                tb_compracontadoc_idfk,
                tb_compracontadetalle_orden,
                tb_compracontadetalle_usureg,
                tb_compracontadetalle_usumod$column_opc
              )
              VALUES(
                :param0,
                :param1,
                :param2,
                :param3,
                :param4,
                :param5,
                :param6,
                :param7,
                :param8$param_opc
              );";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":param0", $this->tb_cuentaconta_cod, PDO::PARAM_STR);
      $sentencia->bindParam(":param1", $this->tb_cuentaconta_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(":param2", $this->tb_compracontadetalle_glosa, PDO::PARAM_STR);
      $sentencia->bindParam(":param3", $this->tb_compracontadetalle_monto, PDO::PARAM_STR);
      $sentencia->bindParam(":param4", $this->tb_empresa_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(":param5", $this->tb_compracontadoc_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(":param6", $this->tb_compracontadetalle_orden, PDO::PARAM_INT);
      $sentencia->bindParam(":param7", $this->tb_compracontadetalle_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":param8", $this->tb_compracontadetalle_usumod, PDO::PARAM_INT);
      if(!empty($column_opc)){
        if(!empty($this->tb_gasto_idfk)) {
          $sentencia->bindParam(":param_opc", $this->tb_gasto_idfk, PDO::PARAM_INT);
          if($this->tb_gastopago_idfk > 0){
            $sentencia->bindParam(":param_opc1", $this->tb_gastopago_idfk, PDO::PARAM_INT);
          }
        } elseif (!empty($this->tb_gastogar_idfk)) {
          $sentencia->bindParam(":param_opc", $this->tb_gastogar_idfk, PDO::PARAM_INT);
        }
      }

      $resultado['estado'] = $sentencia->execute();
      $resultado['nuevo'] = $this->dblink->lastInsertId();

      if ($resultado['estado'] == 1) {
        $this->dblink->commit();
        $resultado['mensaje'] = 'Registrado correctamente';
      } else {
        $resultado['mensaje'] = 'No se pudo registrar';
      }
      return $resultado;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  public function buscar_por_compracontaid($compracontaid){
    try {
      $sql = "SELECT ccd.*, e.tb_empresa_nomcom
              FROM tb_compracontadetalle ccd
              LEFT JOIN tb_empresa e ON ccd.tb_empresa_idfk = e.tb_empresa_id
              WHERE tb_compracontadoc_idfk = :compraconta_id
              AND tb_compracontadetalle_xac = 1
              ORDER BY ccd.tb_compracontadetalle_orden ASC;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':compraconta_id', $compracontaid, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay detalles del documento de compras registrados";
        $retorno["data"] = "";
      }
      return $retorno;
    } catch (Exception $th) {
      throw $th;
    }
  }

  public function mostrarUno($array, $array1) {
    //array: bidimensional con formato $column_name, $param.$i, $datatype, $conector
    //array1: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
    try {
      $sql = "SELECT ccd.*, e.tb_empresa_nomcom";
      if(!empty($array1)){
        for($j=0; $j< count($array1); $j++){
          $sql .= ",\n" . $array1[$j]['alias_columnasparaver'];
        }
      }
      $sql .= "\nFROM tb_compracontadetalle ccd
                LEFT JOIN tb_empresa e ON ccd.tb_empresa_idfk = e.tb_empresa_id\n";
      if(!empty($array1)){
        for($k=0; $k< count($array1); $k++){
          $sql .= $array1[$k]['tipo_union']. " JOIN ". $array1[$k]['tabla_alias']. " ON ". $array1[$k]['columna_enlace']. " = ". $array1[$k]['alias_columnaPK']."\n";
        }
      }
      for ($i = 0; $i < count($array); $i++) {
        if (!empty($array[$i])) {
          $sql .= ($i > 0) ? " {$array[$i]["conector"]} " : "WHERE ";

          if (!empty($array[$i]["column_name"])) {
            if (stripos($array[$i]["column_name"], 'fec') !== FALSE) {
              $sql .= "DATE_FORMAT({$array[$i]["column_name"]}, '%Y-%m-%d')";
            } else {
              $sql .= "{$array[$i]["column_name"]}";
            }

            if (!empty($array[$i]["datatype"])) {
              $sql .= " = :param$i";
            } else {
              $sql .= " " . $array[$i]["param$i"];
            }
          } else {
            $sql .= " " . $array[$i]["param$i"];
          }
        }
      }

      $sentencia = $this->dblink->prepare($sql);
      for ($i = 0; $i < count($array); $i++) {
        if (!empty($array[$i]["column_name"]) && !empty($array[$i]["datatype"])) {
          $_PARAM = strtoupper($array[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
          $sentencia->bindParam(":param$i", $array[$i]["param$i"], $_PARAM);
        }
      }
      $sentencia->execute();

      $retorno['sql'] = $sql;
      $retorno['cantidad'] = $sentencia->rowCount();
      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No existe este registro";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
        throw $e;
    }
  }

  public function eliminar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_compracontadetalle SET tb_compracontadetalle_xac = 0, tb_compracontadetalle_usumod = :tb_compracontadetalle_usumod WHERE tb_compracontadetalle_id = :tb_compracontadetalle_id ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_compracontadetalle_usumod", $this->tb_compracontadetalle_usumod, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_compracontadetalle_id", $this->tb_compracontadetalle_id, PDO::PARAM_INT);
      $resultado = $sentencia->execute(); //retorna 1 si es correcto

      if ($resultado == 1) {
        $this->dblink->commit();
      }
      return $resultado;
    } catch (Exception $th) {
      $this->dblink->rollBack();
      throw $th;
    }
  }

  public function modificar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_compracontadetalle SET
                tb_cuentaconta_cod = :param0,
                tb_compracontadetalle_glosa = :param1,
                tb_compracontadetalle_monto = :param2,
                tb_empresa_idfk = :param3,
                tb_compracontadetalle_orden = :param4,
                tb_compracontadetalle_usumod = :param5,
                tb_cuentaconta_idfk = :param6,
                tb_gastogar_idfk = :param7,
                tb_gasto_idfk = :param8,
                tb_gastopago_idfk = :param10
              WHERE
                tb_compracontadetalle_id = :param9;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':param0', $this->tb_cuentaconta_cod, PDO::PARAM_STR);
      $sentencia->bindParam(':param1', $this->tb_compracontadetalle_glosa, PDO::PARAM_STR);
      $sentencia->bindParam(':param2', $this->tb_compracontadetalle_monto, PDO::PARAM_STR);
      $sentencia->bindParam(':param3', $this->tb_empresa_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(':param4', $this->tb_compracontadetalle_orden, PDO::PARAM_INT);
      $sentencia->bindParam(':param5', $this->tb_compracontadetalle_usumod, PDO::PARAM_INT);
      $sentencia->bindParam(':param6', $this->tb_cuentaconta_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(':param7', $this->tb_gastogar_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(':param8', $this->tb_gasto_idfk, PDO::PARAM_INT);
      $sentencia->bindParam(':param9', $this->tb_compracontadetalle_id, PDO::PARAM_INT);
      $sentencia->bindParam(':param10', $this->tb_gastopago_idfk, PDO::PARAM_INT);
      $resultado = $sentencia->execute();

      if ($resultado == 1) {
        $this->dblink->commit();
      }
      return $resultado;
    } catch (Exception $th) {
      $this->dblink->rollBack();
      throw $th;
    }
  }
}
