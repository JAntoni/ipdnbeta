
<div class="row" id="fil_inicial" <?php echo $style_ini;?>>

    <!--DATOS DEL CLIENTE-->
    <div class="col-md-12">
        <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">DATOS DE INICIAL <?php echo $opc_modificar;
        echo ($cre_tip == 2) ? ' <b style="font-family: cambria;font-weight: bold;color: red"> ESTO ES UNA ADENDA</b>' : ''; ?></label>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <label for="txt_ven_cli_ape">MONTO :</label><br/>
                        <input type="text" id="txt_credito_ini" name="txt_credito_ini" class="form-control input-sm moneda2" value="<?php echo formato_moneda($cre_ini); ?>"/>
                    </div> 
                    <div class="col-md-2">
                        <label for="txt_ven_cli_ape">FECHA :</label><br/>
                        <input type="text" id="txt_inicial_fec" name="txt_inicial_fec" class="form-control input-sm" value="<?php echo mostrar_fecha($inicial_fec); ?>"/>
                    </div> 
                    <div class="col-md-2">
                        <label for="txt_ven_cli_ape">MÉTODO :</label><br/>
                        <input type="text" id="txt_inicial_met" name="txt_inicial_met" class="form-control input-sm" value="<?php echo $inicial_met ?>"/>
                    </div> 
                    <div class="col-md-4">
                        <label for="txt_ven_cli_ape">DETALLE :</label><br/>
                        <textarea name="txt_inicial_det" id="txt_inicial_det" class="form-control input-sm mayus" cols="55" placeholder="Detalle..."><?php echo $inicial_det?></textarea>
                    </div> 
                    <div class="col-md-2"  style="padding-top: 20px">
                        <div class="form-group">
                        <a class="btn btn-primary btn-sm" title="AGREGAR INICIAL" onclick="credito_inicial_form()"><i class="fa fa-plus"></i> </a>
                        <a class="btn btn-primary btn-sm" title="QUITAR INICIAL" onclick="credito_inicial_reset()"><i class="fa fa-trash"></i></a>
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!--FIN DATOS DEL CLIENTE-->

</div>