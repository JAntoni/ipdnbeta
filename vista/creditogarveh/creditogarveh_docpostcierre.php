<div class="row">
    <div class="col-md-12">
        <label class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> OTROS DOCUMENTOS POST CIERRE</label>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 callout callout-info" id="postcierre_mensaje2" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Buscando otros documentos post cierre</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <a type="button" class="btn btn-primary" onclick="filestorage_form()"><i class="fa fa-plus"></i> Subir</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="result_otros_doc">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> DOCUMENTOS DE EJECUCION</label>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <input type="hidden" name="hdd_ejecucion_credito_id" id="hdd_ejecucion_credito_id" value="<?php echo $creditogarveh_id; ?>">
                    <div class="col-md-12 callout callout-info" id="postcierre_mensaje1" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Buscando procesos de ejecucion para este crédito</h4>
                    </div>
                    <div class="col-md-12" id="ejecucion_result">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>