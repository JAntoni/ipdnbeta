<?php
//Daniel odar 29-04-24
//verificar si tiene alguna modificacion desde ejecucion legal
require_once '../historial/Historial.class.php';
$oHist = new Historial();

$modificacion_legal= '';
$res_modificaciones = $oHist->filtrar_historial_por('tb_creditogarveh_vehiculo', $creditogarveh_id);
if ($res_modificaciones['estado'] == 1) {
    $modificacion_legal = '<b style="font-family: cambria;font-weight: bold;color: orange;">| Algunos datos fueron actualizados desde el módulo de ejecución legal. Ver <a onclick="historial_form(\'tb_creditogarveh_vehiculo\', '.$creditogarveh_id.')"><b>AQUI</b></a> |</b>';
}
unset($res_modificaciones);

?>
<div class="row">

    <!--DATOS DEL VEHICULO-->
    <div class="col-md-10">
        <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">DATOS DEL VEHÍCULO <?php echo $modificacion_legal.' '. $opc_modificar;
echo ($cre_tip == 2) ? ' <b  style="font-family: cambria;font-weight: bold;color: red">ESTO ES UNA ADENDA</b>' : ''; ?></label>
        <div class="box box-primary">
            <div class="box-header" style="font-family: cambria;">
                <div class="row mt-4 mb-4">
                    <div class="col-md-3">
                        <label>GARANTÍA :</label>
                        <select name="cmb_credito_tip" id="cmb_credito_tip" class="form-control input-sm mayus">
                            <option value="1" <?php if($cre_tip=='1'){echo 'selected';}?>>COMPRA VENTA</option>
                            <option value="4" <?php if($cre_tip=='4' || intval($cre_numescr) != 0){echo 'selected';}?>>GARANTÍA MOBILIARIA</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>FORMA GARANTÍA :</label>
                        <select name="cmb_credito_subgar" id="cmb_credito_subgar" class="form-control input-sm mayus">
                            <option value="1" <?php if($cre_subgar=='REGULAR'){echo 'selected';}?>>REGULAR</option>
                            <option value="2" <?php if($cre_subgar=='GARMOB COMPRA TERCERO'){echo 'selected';}?>>GARMOB COMPRA TERCERO</option>
                            <option value="3" <?php if($cre_subgar=='GARMOB IPDN VENTA'){echo 'selected';}?>>GARMOB IPDN VENTA</option>
                            <option value="5" <?php if($cre_subgar=='GARMOB IPDN VENTA CON RD'){echo 'selected';}?>>GARMOB IPDN VENTA CON RD</option>
                            <!-- GERSON (20-10-23) -->
                            <?php if($proceso_id>0 || $usuario_action == 'M'){?>
                            <option value="4" <?php if($cre_subgar=='PRE-CONSTITUCION'){echo 'selected';}?>>PRE-CONSTITUCION</option>
                            <?php } ?>
                            <!--  -->
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>NOTARÍA A ENVIAR :</label>
                        <select name="cmb_credito_not" id="cmb_credito_not" class="form-control input-sm mayus" required="required">
                            <?php
                                require_once '../notaria/notaria_select.php';
                            ?>
<!--                            <option value="">Seleccione...</option>
                            <option value="1" <?php //if($cre_not=='VALDIVIA DEXTRE'){echo 'selected';}?>>Valdivia Dextre</option>
                            <option value="2" <?php //if($cre_not=='VERA MENDEZ'){echo 'selected';}?>>Vera Mendez</option>
                            <option value="3" <?php //if($cre_not=='SANTA CRUZ'){echo 'selected';}?>>Santa Cruz</option>-->
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>TIPO :</label>
                        <select name="cmb_custodia_id" id="cmb_custodia_id" class="form-control input-sm mayus">
                            <option value="1" <?php if($cre_cus=='1'){echo 'selected';}?>>CON CUSTODIA</option>
                            <option value="2" <?php if($cre_cus=='2'){echo 'selected';}?>>SIN CUSTODIA</option>
                        </select>
                    </div>
                </div>
                <p>
<!--                <div class="row">
                    <div class="col-md-4">
                        <label>TIPO USO :</label>
                        <select name="cmb_cre_tipus" id="cmb_cre_tipus" class="form-control input-sm mayus">
                            < ?php
                                //require_once '../vehiculouso/vehiculouso_select.php';
                            ?>
                            <option value="1" < ?php //echo ($veh_tipus == 1)? 'selected':'';?>>PARA TAXI</option>
                            <option value="2" < ?php //echo ($veh_tipus == 2)? 'selected':'';?>>USO PARTICULAR</option>
                            <option value="3" < ?php //echo ($veh_tipus == 3)? 'selected':'';?>>TR. URBANO</option>
                        </select>
                    </div>
                </div>
                <p>-->
                <div class="row mt-4 mb-4">
                    <div class="col-md-3">
                        <label>MARCA :</label>
                        <div class="input-group">
                            <select name="cmb_vehmar_id_2" id="cmb_vehmar_id_2" class="selectpicker form-control input-sm" data-live-search="true" data-max-options="1" data-size="12">
                                <?php require_once '../vehiculomarca/vehiculomarca_select.php'; ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" onclick="vehiculomarca_form('I',0)" title="AGREGAR MARCA">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>MODELO :</label>
                        <div class="input-group">
                            <select name="cmb_vehmod_id_2" id="cmb_vehmod_id_2" class="selectpicker form-control input-sm" data-live-search="true" data-max-options="1" data-size="12">
                                <?php require_once '../vehiculomodelo/vehiculomodelo_select.php'; ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" onclick="vehiculomodelo_form('I', 0);" title="AGREGAR MODELO">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>CARROCERÍA :</label>
                        <div class="input-group">
                            <select name="cmb_vehcla_id_2" id="cmb_vehcla_id_2" class="selectpicker form-control input-sm mayus" data-live-search="true" data-max-options="1" data-size="12">
                                <?php
                            
                                require_once '../vehiculoclase/vehiculoclase_select.php';
                                ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" onclick="vehiculoclase_form('I', 0)" title="AGREGAR CARROCERIA">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>N° VERSIÓN :</label>
                        <div class="input-group">
                            <select name="cmb_vehtip_id_2" id="cmb_vehtip_id_2" class="selectpicker form-control input-sm mayus" data-live-search="true" data-max-options="1" data-size="12">
                                <?php
                                require_once '../vehiculotipo/vehiculotipo_select.php';
                                ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" onclick="vehiculotipo_form('I', 0)" title="AGREGAR NRO VERSION">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <p>
                <div class="row mt-4 mb-4">
                    <div class="col-md-3">
                        <label>PLACA :</label><br/>
                        <input name="txt_veh_pla" type="text" id="txt_veh_pla" value="<?php echo $veh_pla?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>SERIE MOTOR :</label><br/>
                        <input name="txt_veh_mot" type="text" id="txt_veh_mot" value="<?php echo $veh_mot?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>SERIE CHASIS :</label><br/>
                        <input name="txt_veh_cha" type="text" id="txt_veh_cha" value="<?php echo $veh_cha?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
<!--                        <label>AÑO FABRICACIÓN :</label><br/>
                        <input name="txt_veh_fab" type="text" id="txt_veh_fab" value="<?php echo $veh_ano?>" class="form-control input-sm mayus">-->
                        <label>AÑO FABRICACIÓN :</label>
                        <select name="txt_veh_fab" id="txt_veh_fab" class="selectpicker form-control input-sm mayus" data-live-search="true" data-max-options="1" data-size="12" required="required">
                            <?php $anio_inicial='1990'; echo devuelve_option_anios_garveh($anio_inicial,$veh_ano,4);?>
                        </select>
                    </div>
                </div>
                <p>
                <div class="row mt-4 mb-4">
                    <div class="col-md-3">
                        <label>AÑO MODELO :</label>
                        <select name="txt_veh_mode" id="txt_veh_mode" class="selectpicker form-control input-sm mayus" data-live-search="true" data-max-options="1" data-size="12" required="required">
                            <?php echo devuelve_option_anios_garveh($anio_inicial,$veh_mode,4);?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>CATEGORÍA :</label><br/>
                        <input name="txt_veh_cate" type="text" id="txt_veh_cate" value="<?php echo $veh_cate?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>TIPO COMBUSTIBLE :</label><br/>
                        <input name="txt_veh_comb" type="text" id="txt_veh_comb" value="<?php echo $veh_comb?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>KILOMETRAJE :</label><br/>
                        <input name="txt_veh_kil" type="text" id="txt_veh_kil" value="<?php echo $veh_kil?>" class="form-control input-sm mayus">
                    </div>
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col-md-3">
                        <label>COLOR :</label><br/>
                        <input name="txt_veh_col" type="text" id="txt_veh_col" value="<?php echo $veh_col?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>N° PASAJEROS :</label><br/>
                        <input name="txt_veh_numpas" type="text" id="txt_veh_numpas" value="<?php echo $veh_numpas?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>N° ASIENTOS :</label><br/>
                        <input name="txt_veh_numasi" type="text" id="txt_veh_numasi" value="<?php echo $veh_numasi?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>ZONA REGISTRAL :</label>
                        <div class="input-group">
                            <div id="div_vehiculotipo_form"></div>
                            <select name="cmb_zona_id" id="cmb_zona_id" class="form-control input-sm mayus">
                                <?php
                                require_once '../zona/zona_select.php';
                                ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button" onclick="zona_form('I', 0)" title="AGREGAR ZONA REGISTRAL">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
<!--                <div class="row con_custodia">
                    <div class="col-md-3">
                        <label>GPS :</label>
                        <div class="input-group">
                            <select name="cmb_gps_id" id="cmb_gps_id" class="form-control input-sm mayus">
                                < ?php //require_once '../vehiculomarca/vehiculomarca_select.php';?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>STR :</label>
                        <div class="input-group">
                            <select name="cmb_str_id" id="cmb_str_id" class="form-control input-sm mayus">
                                < ?php //require_once '../vehiculomodelo/vehiculomodelo_select.php';?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>SOAT :</label>
                        <div class="input-group">
                            <select name="cmb_soa_id" id="cmb_soa_id" class="form-control input-sm mayus">
                                < ?php //require_once '../vehiculoclase/vehiculoclase_select.php';?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>GAS :</label>
                        <div class="input-group">
                            <select name="cmb_gas_id" id="cmb_gas_id" class="form-control input-sm mayus">
                                < ?php //require_once '../vehiculotipo/vehiculotipo_select.php';?>
                            </select>
                        </div>
                    </div>
                </div>
                <p>
                <div class="row con_custodia">
                    <div class="col-md-3">
                        <label>GPS PRECIO :</label><br/>
                        <input name="txt_gps_pre" type="text" id="txt_gps_pre" value="< ?php //echo $gps_pre?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>STR PRECIO :</label><br/>
                        <input name="txt_str_pre" type="text" id="txt_str_pre" value="< ?php //echo $str_pre?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>SOAT PRECIO :</label><br/>
                        <input name="txt_soa_pre" type="text" id="txt_soa_pre" value="< ?php //echo $soa_pre?>" class="form-control input-sm mayus">
                    </div>
                    <div class="col-md-3">
                        <label>GAS PRECIO :</label><br/>
                        <input name="txt_gas_pre" type="text" id="txt_gas_pre" value="< ?php //echo $gas_pre?>" class="form-control input-sm mayus">
                    </div>
                </div>-->
                <p>
                <div class="row">
                    <div class="col-md-6">
                        <label>ESTADO :</label><br/>
                        <textarea name="txt_veh_est" id="txt_veh_est" cols="75" class="form-control input-sm mayus" placeholder="Detalle si tiene defectos. Ejem: tiene fallos en chasis..."><?php echo $veh_est?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>ENTREGADO CON :</label><br/>
                        <textarea name="txt_veh_ent" id="txt_veh_ent"" cols="75" class="form-control input-sm mayus" placeholder="Detalle lo entregado separado por comas"><?php echo $veh_ent?></textarea>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-3 gar_mobiliaria">
                        <label id="label_valor">TASACIÓN USD$ :</label><br/>
                        <input name="txt_cre_valtas" type="text" id="txt_cre_valtas" value="<?php echo $cre_valtas?>" class="form-control input-sm moneda2">
                    </div>
                    <div class="col-md-3 gar_mobiliaria">
                        <label>REALIZACIÓN USD$ :</label><br/>
                        <input name="txt_cre_valrea" type="text" id="txt_cre_valrea" value="<?php echo $cre_valrea?>" class="form-control input-sm mayus moneda2">
                    </div>
                    <div class="col-md-3 gar_mobiliaria">
                        <label>GRAVAMEN USD$ :</label><br/>
                        <input name="txt_cre_valgra" type="text" id="txt_cre_valgra" value="<?php echo $cre_valgra?>" class="form-control input-sm mayus moneda2">
                    </div>
                    <div class="col-md-3 opcional">
                        <label>VENTA S/ :</label><br/>
                        <input name="txt_cre_vent" type="text" id="txt_cre_vent" value="<?php echo $cre_vent?>" class="form-control input-sm moneda2">
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-2">
                        <label>OTROS :</label><br/>
                         <input name="txt_otr_pre" type="text" id="txt_otr_pre" value="<?php echo $otr_pre?>" class="form-control input-sm moneda2">
                    </div>
                    <div class="col-md-2">
                        <label>VEHICULO PRECIO :</label><br/>
                         <input name="txt_veh_pre" type="text" id="txt_veh_pre" value="<?php echo $veh_pre?>" class="form-control input-sm moneda2">
                    </div>
                    <div class="col-md-2" id="td_cambio">
                        <label>TIPO CAMBIO :</label><br/>
                         <input name="txt_cre_tc"  type="text" id="txt_cre_tc" value="<?php echo $cre_tc?>" class="form-control input-sm moneda3">
                    </div>
                    <div id="tbl_partidas"> 
                        <div class="col-md-2">
                            <label>PARTIDA ELECTRO. :</label><br/>
                             <input name="txt_veh_parele" type="text" id="txt_veh_parele" value="<?php echo $veh_parele?>" class="form-control input-sm mayus">
                        </div>
                        <div class="col-md-2">
                            <label>SOLICITUD REGIST. :</label><br/>
                             <input name="txt_veh_solreg" type="text" id="txt_veh_solreg" value="<?php echo $veh_solreg?>" class="form-control input-sm mayus">
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>FECHA SOLICITUD :</label><br/>
                                <div class="input-group">
                                    <div class='input-group date' id='datetimepicker5'>
                                        <input type='text' maxlength="10" class="form-control input-sm" name="txt_veh_fecsol" id="txt_veh_fecsol" value="<?php echo mostrar_fecha($veh_fecsol); ?>" readonly/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                <?php if($cre_subgar !='PRE-CONSTITUCION'):?>    
                <div class="row">
                    <div class="col-md-10" id="tr_cargas">
                        <label>DETALLE SI TIENE CARGAS :</label><br/>
                        <textarea name="txt_cre_carg" id="txt_cre_carg" cols="85" class="form-control input-sm" placeholder="Ejem: existe inscrita una papeleta"><?php echo $cre_cargas?></textarea>
                    </div>
                </div>
                <p>
                <?php endif;?>
<!--                <div class="row con_custodia">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">VENCIMIENTO GPS :</label>
                            <div class="input-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecgps" id="txt_cre_fecgps" value="<?php echo ($cre_fecgps); ?>" readonly/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">VENCIMIENTO STR :</label>
                            <div class="input-group">
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecseg" id="txt_cre_fecseg" value="<?php echo ($cre_fecstr); ?>" readonly/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">VENCIMIENTO SOAT :</label>
                            <div class="input-group">
                                <div class='input-group date' id='datetimepicker3'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecren" id="txt_cre_fecren" value="<?php echo ($cre_fecsoat); ?>" readonly/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">VENCIMIENTO I.V :</label>
                            <div class="input-group">
                                <div class='input-group date' id='datetimepicker4'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecvenimp" id="txt_cre_fecvenimp" value="<?php echo ($cre_fecvenimp); ?>" readonly/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <!--FIN DE DATOSS DEL VEHICULO-->
</div>