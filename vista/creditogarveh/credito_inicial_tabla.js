$(document).ready(function () {
    
    $('.ra_inicial_id').change(function(event){
      var INICIAL_ID = $('input[name=ra_inicial_id]:checked').val();
      var tip_cam = parseFloat($('#hdd_camb_'+INICIAL_ID).val());
      var monto = parseFloat($('#hdd_imp_'+INICIAL_ID).val());
      var moneda_matriz = parseInt($('#cmb_credito_moneda').val());
      var moneda_inicial = parseInt($('#cmb_inicial_mon_id').val());

      console.log(' //// '+tip_cam);
      console.log(' //// '+moneda_matriz);
      console.log(' //// '+moneda_inicial);
      if(tip_cam>0){
	      if(moneda_matriz == 1 && moneda_inicial == 2){
	        var monto_valido = parseFloat(monto * tip_cam);
	        $('#hdd_incial_id_select').val(INICIAL_ID);
	        $('#txt_inicial_utilizar').val(monto_valido.toFixed(2));
	      }
	      if(moneda_matriz == 2 && moneda_inicial == 1){
	        var monto_valido = parseFloat(monto / tip_cam);
	        $('#hdd_incial_id_select').val(INICIAL_ID);
	        $('#txt_inicial_utilizar').val(monto_valido.toFixed(2));
	      }
	      if((moneda_matriz == 1 && moneda_inicial == 1) || (moneda_matriz == 2 && moneda_inicial == 2)){
	      	$('#hdd_incial_id_select').val(INICIAL_ID);
	        $('#txt_inicial_utilizar').val(monto.toFixed(2));
	      }
	    }
	    else{
	    	alert('Registre EL TIPO DE CAMBIO DEL DÍA DE DEPÓSITO POR FAVOR');
	    	$('#hdd_incial_id_select').val(0);
	      $('#txt_inicial_utilizar').val('0.00');
	    }
      //console.log('ID SELEC: '+INICIAL_ID + ' // MATRIZ: ' + moneda_matriz + ' // inical: ' + moneda_inicial + ' / camb ' + tip_cam + ' // ' + monto);
    });
});