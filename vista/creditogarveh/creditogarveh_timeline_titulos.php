<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
  require_once('../creditogarveh/Creditogarveh.class.php');
  require_once('../creditolinea/Creditolinea.class.php');
  require_once('../usuario/Usuario.class.php');
  
  $oCreditogarveh = new Creditogarveh();
  $oCreditolinea = new Creditolinea();
  $oUsuario = new Usuario();

  $credito_id = $_POST['cre_id'];
  $nrotitulo  = $_POST['nrotitulo'];
  $seguimiento_id  = $_POST['seguimiento_id'];

  $titulo = 'Historial Número de Título: '.$nrotitulo;
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
  $timeline = '';
  $color = "";
  $descripEstado = "";
  $DatosUsuario_nombre = "";
  $creditotipo_id = 3;

  // ------------- fragment para mostrar también el ultimo estado ------------- //

  $contarDatosHistorialFinal = $oCreditogarveh->seguimiento_titulo_mostraruno($seguimiento_id);
    if($contarDatosHistorialFinal['estado'] == 1)
    {
      $DatosHistorialFinal_nrotitulo               = $contarDatosHistorialFinal['data']['tb_seguimiento_nrotitulo'];
      $DatosHistorialFinal_credito_id              = $contarDatosHistorialFinal['data']['tb_credito_id'];
      $DatosHistorialFinal_creditotipo_id          = $contarDatosHistorialFinal['data']['tb_creditotipo_id'];
      $DatosHistorialFinal_seguimiento_usureg      = $contarDatosHistorialFinal['data']['tb_seguimiento_usureg'];
      $DatosHistorialFinal_credestado_titulo_id    = $contarDatosHistorialFinal['data']['tb_credestado_titulo_id'];
      $DatosHistorialFinal_seguimiento_observacion = $contarDatosHistorialFinal['data']['tb_seguimiento_observacion'];
      $DatosHistorialFinal_fecha_reg               = $contarDatosHistorialFinal['data']['tb_seguimiento_fecreg'];
      $DatosHistorialFinal_fecha_mod               = $contarDatosHistorialFinal['data']['tb_seguimiento_fecmod'];
      // $fecha_mod = date("Y-m-d", strtotime($fecha_mod));
      switch($DatosHistorialFinal_credestado_titulo_id) {
        case 1:
        $color = '25dbcf';
        $descripEstado = "PRESENTADO";
        break;
        case 2:
        $color = '2057AC';
        $descripEstado = "REINGRESADO";
        break;
        case 3:
        $color = 'F38C05';
        $descripEstado = "APELADO";
        break;
        case 4:
        $color = 'B4B4B4';
        $descripEstado = "EN PROCESO";
        break;
        case 5:
        $color = '662485';
        $descripEstado = "EN CALIFICACIÓN";
        break;
        case 6:
        $color = '84BC18';
        $descripEstado = "INSCRITO";
        break;
        case 7:
        $color = '575755';
        $descripEstado = "RESERVADO";
        break;
        case 8:
        $color = 'EF1B52';
        $descripEstado = "DISTRIBUIDO";
        break;
        case 9:
        $color = '006632';
        $descripEstado = "LIQUIDADO";
        break;
        case 10:
        $color = '81D1FF';
        $descripEstado = "PRORROGADO";
        break;
        case 11:
        $color = 'FC0002';
        $descripEstado = "OBSERVADO";
        break;
        case 12:
        $color = '95141E';
        $descripEstado = "SUSPENDIDO";
        break;
        case 13:
        $color = '020202';
        $descripEstado = "TACHADO";
        break;
        case 14:
        $color = '6BA7CF';
        $descripEstado = "ANOTADO";
        break;
        case 15:
        $color = 'CD3C40';
        $descripEstado = "CANCELADO";
        break;
        default:
        $color = 'ffffff';
        $descripEstado = "-";
        break;
      }
      
      if( $DatosHistorialFinal_credestado_titulo_id  == 1)
      {
        $fecha_mod = $fecha_reg;
      }

      $consutlarDatosUsuario = $oUsuario->mostrarUno($DatosHistorialFinal_seguimiento_usureg);
      if($consutlarDatosUsuario['estado'] == 1)
      {
        $DatosUsuario_nombre = $consutlarDatosUsuario['data']['tb_usuario_nom'];
      }
    }
  $contarDatosHistorialFinal = null;

  // ------------- fragment para mostrar también el ultimo estado ------------- //

  $result = $oCreditolinea->listar_historial_titulo($creditotipo_id, $credito_id, $nrotitulo);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $timeline .= '
          <li class="mt-4">
            <i class="fa fa-clock-o bg-orange"></i>
            <div class="timeline-item">
              <span class="time" style="margin-top: 8px;"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($value['tb_creditolinea_reg']).'</span>
              <div class="user-block separador">
                <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="Usuario imagen">
                <span class="username" style="margin-top: 8px;"><a href="#">'.$value['tb_usuario_nom'].'</a></span>
                <span class="description mt-4">'.$value['tb_creditolinea_det'].'</span>';
                // if($usuariogrupo_id == 2){
                //   $timeline .= '
                //   <a class="btn btn-danger btn-xs" onclick="creditolinea_form(\'eliminar\', '.$value['tb_creditolinea_id'].', 3, '.$value['tb_credito_id'].')"><i class="fa fa-trash"></i></a>
                //   <a class="btn btn-warning btn-xs" onclick="creditolinea_form(\'modificar\', '.$value['tb_creditolinea_id'].', 3, '.$value['tb_credito_id'].')"><i class="fa fa-edit"></i></a>';
                // }
        $timeline .= '
              </div>
            </div>
          </li>
        ';
      }
    }
  $result = NULL;
?>
<style>
  .separador {
    padding: 10px;
    display: block;
    box-shadow: rgb(14 30 37 / 12%) 3px 2px 0px -2px, rgb(14 30 37 / 32%) 2px 2px 17px 3px;
    margin-bottom: 10px;
  }
  .padsquare{
    padding: 8px;
  }
  .timeline>li>.fa, .timeline>li>.glyphicon, .timeline>li>.ion {
    top: 40%;
  }
  .timeline:before {
    background: #FFB253;
    margin-top: 10%;
    margin-bottom: 10%;
  }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_timeline_titulo" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title text-orange"><?php echo $titulo;?></h4>
      </div>
      <div class="modal-body">
        <!-- <h4 class="text-blue">Historial</h4> -->
        <ul class="timeline">
          <?php echo $timeline;?>
        </ul>
      </div>
      <div class="modal-footer">
        <div class="f1-buttons">
          <!-- <button type="button" class="btn btn-info" onclick="creditolinea_form(< ?php echo "'insertar', 0, 3, ".$_POST['cre_id'];?>)">Nuevo</button> -->
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>
    </div>
  </div>
</div>