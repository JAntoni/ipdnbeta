/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
  console.log('cambios 13/11/2024')

  creditogarveh_tabla();
  usuario_select(0);
  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });
  $("#click").click(); // OCULTA EL SLIDEER
  $("#txt_creditogarveh_fil_cli_nom").autocomplete({
      minLength: 1,
      source: VISTA_URL + "cliente/cliente_autocomplete.php",
      select: function (event, ui) {
          var valor = ui.item.cliente_id;
          $("#hdd_creditogarveh_fil_cli_id").val(ui.item.cliente_id);
          //$("#txt_creditogarveh_fil_cli_nom").val(ui.item.nombre);
          //$("#txt_cliente_doc").val(ui.item.documento);
          creditogarveh_tabla();
      }
  });
  //  console.log('Perfil menu');
  $('#datetimepicker1, #datetimepicker2' ).datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_fil_ing_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_fil_ing_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  
  $("#txt_fil_ing_fec1,#txt_fil_ing_fec2,#cbo_estado,#cbo_tipogarveh").change(function (){
        creditogarveh_tabla();
    });
    
    $("#cmb_sede_id").change(function (){
       var sede_id = $("#cmb_sede_id").val();
       console.log(sede_id);
       usuario_select(sede_id);
    });
    
    $("#cmb_usuario_id").change(function() {
        var cre_radio = $("input[name=ra_estado]:checked").val();
        var asesor = $("#cmb_usuario_id").val();

        if(asesor==undefined || cre_radio==undefined)
            alerta_warning("ERROR","Seleccione asesor o credito");
        else{
            cambiar_asesor(asesor,cre_radio);
        }
    });

    $('#cmb_fil_opc_cre').change(function(event) {
        
        var opc = $(this).val();
        var radio = $('input[name=ra_estado]:checked').val();
        var cre_est = $('input[name=ra_estado]:checked').data('est');
        var cre_tip = $('input[name=ra_estado]:checked').data('tip');
        var cuo_tip = $('input[name=ra_estado]:checked').data('cuo_tip'); //tipo de cuota, 3 LIBRE, 4 FIJA
        console.log("opc ="+opc+" cre_est ="+cre_est+" cuo_tip ="+cuo_tip+" cre_tip ="+cre_tip);
        if(!opc)
                return false;

        var bandera = false;
        var act = '';
        if(parseInt(opc) == 3)
                act = 'vigente';
        if(parseInt(opc) == 4)
                act = 'paralizar';
        if(parseInt(opc) == 8)
                act = 'resolver';

        if(parseInt(opc) == 10){
          refinanciar_form(radio, opc, 'multiple'); //refinanciar multiple adendas o créditos de un mismo cliente
          //alerta_warning("ALERTA","ESTÁ EN CONSTRUCCION")
          return false;
        }

        if(parseInt(opc) == 9 && parseInt(cre_est) == 3 && parseInt(cuo_tip) == 3){
                //renovación de cuotas para los creditos vigentes de tipo cuotas LIBRES
                $('#cmb_fil_opc_cre').val('');
                renovar_cuotas(radio);
                return false;
        }

        if(parseInt(opc) == 3 && (parseInt(cre_est) == 4) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
          bandera = true;
        if(parseInt(opc) == 4 && parseInt(cre_est) == 3 && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
          bandera = true;
        if(parseInt(opc) == 8 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
          bandera = true;
        if(parseInt(opc) == 5 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 2 || parseInt(cre_tip) == 4))
          bandera = true;
        if(parseInt(opc) == 6 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4))
          bandera = true;
        if(parseInt(opc) == 11 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
          bandera = true;

        if(!bandera){
          alerta_warning('!IMPORTANTE','Debes seleccionar un crédito o quizás el estado del crédito no se puede modifcar, revisa nuevamente o consulta con Sistemas.')
          $('#cmb_fil_opc_cre').val('');
          return false;
        }

        if(radio != undefined){
          
          if(opc == 5 || opc == 6){
            //console.log(' opc = '+opc+' radio = '+radio+' cre_est = '+cre_est+' cre_tip = '+cre_tip+' cuo_tip = '+cuo_tip);
            $('#cmb_fil_opc_cre').val('');
            refinanciar_form(radio, opc, 'simple');  //enviamos el id del crédito seleccionado, id del credito es radio, refinanciar simple 1 solo credito
                  
            return false;
          }
          if(parseInt(opc) == 11){
            
            liquidacion_formato_form(radio);
            return false;
          }

          //eset form solo será para PONER EN VIGENCIA, PARALIZAR Y RESOLVER
          $('#cmb_fil_opc_cre').val('');
            credito_resolver_vigente(act, cre_tip, cre_est, radio); 
        }
        else{
          alerta_warning('!IMPORTANTE','Debes seleccionar un crédito o quizás el estado del crédito no se puede modifcar, revisa nuevamente o consulta con Sistemas.')
          $('#cmb_fil_opc_cre').val('');
        }
    });
});

//Juan Antonio 29-08-2023
function refinanciar_multiple(){

}

function creditogarveh_form(usuario_act, creditogarveh_id, proceso_id=0) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/creditogarveh_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditogarveh_id: creditogarveh_id,
            proceso_id: proceso_id,
            vista: 'credito_tabla'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
//            console.log(data);
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_creditogarveh_form').html(data);
                $('#modal_registro_creditogarveh').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L'){
                    form_desabilitar_elementos('for_cre');
                } //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditogarveh', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_creditogarveh',80);
                modal_height_auto('modal_registro_creditogarveh');
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'creditogarveh';
                var div = 'div_modal_creditogarveh_form';
                permiso_solicitud(usuario_act, creditogarveh_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {
//console.log(data);
        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

function creditogarveh_tabla() {
    var cliente_fil_id = $('#hdd_creditogarveh_fil_cli_id').val();
    var fecha1 = $('#txt_fil_ing_fec1').val();
    var fecha2 = $('#txt_fil_ing_fec2').val();
    var estado = $('#cbo_estado').val();
    var tipogarveh = $('#cbo_tipogarveh').val();
      // alert(fecha1+' '+fecha2+' '+estado+' '+tipogarveh);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_tabla.php",
    async: true,
    dataType: "html",
    data: ({
        fecha1: fecha1,
        fecha2: fecha2,
        estado: estado,
        tipogarveh: tipogarveh,
        cliente_id: cliente_fil_id
    }), 
    beforeSend: function () {
      $("#creditogarveh_mensaje_tbl").show(300);
    },
    success: function (data) {
      //INSERTA MEDIANTE HTML LA DATA QUE REGRESA DE CLIENTE_TABLA.PHP
      $("#div_creditogarveh_tabla").html(data);
      $("#creditogarveh_mensaje_tbl").hide(300);
    },
    complete: function (data) {
        estilos_datatable();
      // console.log(data); MUESTRA LA DATA QUE DEVUELVE ACA PUEDO VER ERROR
    },
    error: function (data) {
      $("#creditogarveh_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DEL CLIENTE: " + data.responseText
      );
    },
  });
}



function estilos_datatable() {
    datatable_global = $('#tbl_creditogarveh').DataTable({
        "pageLength": 100,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [13,14,15,16], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function credito_his(idf){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_timeline.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: 'historial',
			cre_id:	idf
		}),
		beforeSend: function() {
			//$('#creditogarveh_mensaje_tbl').hide();
			//$('#div_credito_his').dialog("open");
			//$('#div_credito_his').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
      console.log(html);
			$('#div_creditogarveh_his').html(html);
      $('#modal_creditogarveh_timeline').modal('show');
      modal_hidden_bs_modal('modal_creditogarveh_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_creditogarveh_timeline'); //funcion encontrada en public/js/generales.js
		}
	});
}

function cliente_nota(idf)
{
	$.ajax({
		type: "POST",
		url: VISTA_URL + "cliente/clientenota_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cre_id:	idf,
			cre_tip: 3 //tipo de credito 3: garveh
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_clientenota_form').dialog("open");
			//$('#div_clientenota_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
			$('#div_clientenota_form').html(html);	
      $('#modal_cliente_nota').modal('show');
      modal_hidden_bs_modal('modal_cliente_nota', 'limpiar'); //funcion encontrada en public/js/generales.js
		}
	});
}

function creditogarveh_pagos(creditogarveh_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarveh/creditogarveh_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditogarveh_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditogarveh_pagos').html(data);
        $('#modal_creditogarveh_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditogarveh_pagos', 95);
        modal_height_auto('modal_creditogarveh_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditogarveh_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function eliminar_credito(id,tipo)
{   
	if(confirm("Realmente desea eliminar?")){
            if(tipo < 3){
		$.ajax({
			type: "POST",
			url: VISTA_URL + "creditogarveh/credito_reg.php",
			async:true,
			dataType: "html",
			data: ({
				action: "eliminar",
				cre_id:	id
			}),
			beforeSend: function() {
				//$('#msj_credito').html("Cargando...");
				//$('#msj_credito').show(100);
			},
			success: function(html){
                           if(html==1){
                                alerta_success("Exito","Credito eliminado");
                                creditogarveh_tabla();
                            }
                            else{
                                alerta_warning("Hubo un error","No se pudo eliminar crédito");
                            }
			},
			complete: function(){
				creditogarveh_tabla();
			}
		});
            }
            else{
                alert("NO PUEDE ELIMINAR ESTE CREDITO");
            }
        }
}

function usuario_select(id) {

  var sede_id = $("#cmb_sede_id").val();
  $("#cmb_usuario_id").empty();
  console.log(sede_id);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "/usuario/usuario_select.php",
    async: true,
    dataType: "html",
    data: {
      // usuario_columna: '',
      // param_tip:'',
      // usuario_valor: 0
    },
    beforeSend: function () {
      $("#cmb_usuario_id").val("<option>' cargando '</option>");
    },
    success: function (data) {
      console.log('refresh selectpicker');
      $("#cmb_usuario_id").empty();
      $("#cmb_usuario_id").html(data);      
      $("#cmb_usuario_id").selectpicker('refresh');
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      $("#creditogarveh_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CANCHA: " + data.responseText
      );
    },
  });
}

function cambiar_asesor(asesor,cre_radio){
    $.ajax({
	    type: "POST",
	    url: VISTA_URL + "/creditogarveh/credito_reg.php",
	    async:false,
	    dataType: "html",                      
	    data: ({
	      action: 'asesor',
	      asesor_id: asesor,
	      cre_id: cre_radio
	    }),
	    beforeSend: function() {
	      //$('#msj_credito').html('Cambiando de Asesor...');
	      //$('#msj_credito').show(400)
	    },
	    success: function(data){
	      if(parseInt(data) == 1){
	      	alerta_success("Exito","Asesor cambiado")
	      	creditogarveh_tabla();
	      }
              else{
                  alerta_warning("Error","No se pudo cambiar de asesor")
              }
	    }
	  });
}

function placa_form(cre_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_placa.php",
		async:true,
		dataType: "html",                      
		data: ({
			credito_id:	cre_id
		}),
		beforeSend: function() {
			//$('#div_placa_form').dialog("open");
			//$('#div_placa_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_placa_form').html(data);
        modal_width_auto('modal_creditogarveh_placa',80);
        modal_height_auto('modal_creditogarveh_placa');
        $('#modal_creditogarveh_placa').modal('show');
        modal_hidden_bs_modal('modal_creditogarveh_placa', 'limpiar'); //funcion encontrada en public/js/generales.js
        creditogarveh_tabla();
        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      
      }		
		}
	});
}

function verificacion_titulos_form(cre_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/creditogarveh_verificacion.php",
        async:true,
        dataType: "html",                      
        data: ({
            credito_id:	cre_id
        }),
        beforeSend: function() {
                //$('#div_verificacion_titulo_form').dialog("open");
                //$('#div_verificacion_titulo_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function(data){
            if(data != 'sin_datos'){
              $('#div_verificacion_titulo_form').html(data);
              modal_width_auto('modal_creditogarveh_verificacion',80);
              modal_height_auto('modal_creditogarveh_verificacion');
              $('#modal_creditogarveh_verificacion').modal('show');
              modal_hidden_bs_modal('modal_creditogarveh_verificacion', 'limpiar'); //funcion encontrada en public/js/generales.js

              //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

            }	
        }
    });
}

function credito_documentos(cre_id){
	if(!confirm('¿Está seguro que los documentos están completos?'))
		return false;
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/credito_reg.php",
		async:true,
		dataType: "text",                      
		data: ({
			credito_id:	cre_id,
			action: 'documentos'
		}),
		beforeSend: function() {
			//$('#msj_credito').html('Guardando documentos completos...');
			//$('#msj_credito').show();
    },
		success: function(data){
                    if(data == 'exito'){
                        alerta_success("EXITO","Estado cambiado");
                        creditogarveh_tabla();
                    }
                    else
                    {
                      swal_warning("Advertencia",data,8000);
                    }
                }
	});
}

function titulo_form(cre_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "titulo/titulo_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cre_tip:	3, //3 garveh
			cre_id:	cre_id
		}),
		beforeSend: function() {
			//$('#div_titulo_form').dialog("open");
			//$('#div_titulo_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_titulo_form').html(data);
        modal_width_auto('modal_titulo',80);
        modal_height_auto('modal_titulo');
        $('#modal_titulo').modal('show');
        modal_hidden_bs_modal('modal_titulo', 'limpiar'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

      }	
		}
	});
}
//SE AGREGÓ UN NUEVO PARAMETRO A LA FUNCION
function credito_aprobar(idf,custodia){    
	if(confirm("Realmente desea aprobar?")){
		$.ajax({
			type: "POST",
			url: VISTA_URL + "creditogarveh/credito_reg.php",
			async:true,
			dataType: "json",
			data: ({
				action: "aprobar",
				cre_id:		idf,
				custodia:	custodia
			}),
			beforeSend: function() {
				//$('#msj_credito').html("Cargando...");
				//$('#msj_credito').show(100);
			},
			success: function(data){ //REVISAR PORQUE NO DEBERIA SER Q ABRA MODALES}
                            console.log(data);
                            //SE AGREGO UNA CONDICIONAL PARA VERIFICAR EL ID DE LA CUSTODIA 
				if(parseInt(data.estado) > 0){
                                    alerta_success("EXITO", "APROBADO");
                                    //if(data.custodia==2){
                                    //registrar_polizas(data.credito_id);}
                                    creditogarveh_tabla();
                                  }
                                  
			},
			complete: function(){
				//credito_tabla();
			}
		});
	}
}

function credito_desembolso_form(act,idf){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_desembolso_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: act,
			cre_id:	idf,
			vista:	'credito_tabla'
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_credito_desembolso_form').dialog("open");
			//$('#div_credito_desembolso_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
		success: function(data){
                        //console.log(data);
			if(data != 'sin_datos'){
        $('#div_credito_desembolso_form').html(data);
        modal_width_auto('modal_creditogarveh_desembolso',80);
        modal_height_auto('modal_creditogarveh_desembolso');
        $('#modal_creditogarveh_desembolso').modal('show');
        modal_hidden_bs_modal('modal_creditogarveh_desembolso', 'limpiar'); //funcion encontrada en public/js/generales.js


        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      }	
		},
                complete: function(data){
                
                }
	});
}

//COMBO ACCIONES CREDITOS
function renovar_cuotas(idf){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "resolver/renovar_cuotas_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: 'renovar',
			cre_id:	idf,
			cre_tip: 3 //tipo de credito 3: garveh
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_renovar_cuotas').dialog("open");
			//$('#div_renovar_cuotas').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
      if(html != 'sin_datos'){
        $('#div_renovar_cuotas').html(html);
        //modal_width_auto('modal_creditogarveh_desembolso',80);
        //modal_height_auto('modal_creditogarveh_desembolso');
        $('#modal_renovar_cuotas').modal('show');
        modal_hidden_bs_modal('modal_renovar_cuotas', 'limpiar'); //funcion encontrada en public/js/generales.js

      }	
					
		}
	});
}

function refinanciar_form(idf, est, tipo){
	var fecha = prompt('Ingrese una Fecha para el cálculo, formato: '+$('#txt_fecha_ref').val(), $('#txt_fecha_ref').val());
  var array_multiples = [];
  var id_multiples = '';

  if(tipo == 'multiple'){
    $('.che_multiple:checked').each(function() {
      var credito = $(this).data('credito');
      
      array_multiples.push(credito); // Agregar el valor de credito al array
    });

    if (array_multiples.length > 1) {
      id_multiples = array_multiples.join(','); // Unir los valores en una cadena separada por comas
      
    } else {
      alerta_warning('Importante', 'Para refinanciar múltiple debes seleccionar más de 1 crédito en estado Vigente, si solo es uno selecciona el Radio');
      return false;
    }

    console.log('los id selecc: ' + id_multiples);
  }

	if(fecha != undefined){
    if(fecha == ""){
      return false;
    }  
        
		$.ajax({
			type: "POST",
			url: VISTA_URL + "refinanciar/refinanciar_form.php",
			async:true,
			dataType: "html",                      
			data: ({
				action: 'refinanciar',
        tipo_refinanciar: tipo,
        id_multiples: id_multiples,
				cre_id:	idf,
				cre_tip: 3, //tipo de credito 3: garveh
				ref_tip: est,
				fecha_ref: fecha
			}),
			beforeSend: function() {
        $('#modal_mensaje').modal('show');
      },
			success: function(html){
        $('#modal_mensaje').modal('hide');
        if(html != 'sin_datos'){
          $('#div_refinanciar_form').html(html);
          modal_width_auto('modal_creditogarveh_refinanciar',85);
          modal_height_auto('modal_creditogarveh_refinanciar');
          $('#modal_creditogarveh_refinanciar').modal('show');
          modal_hidden_bs_modal('modal_creditogarveh_refinanciar', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
			}
		});
	}
	else{
		$('#cmb_fil_opc_cre').val('');
		alerta_warning('Importante', 'Debe ingresar una fecha');
	}   
}

function credito_resolver_vigente(act, tip, est, id){
	console.log(act + ' // tipo: ' + tip + ' // estad: ' + est + ' // id: '+ id);
	//return false;
	$.ajax({
		type: "POST",
		url: VISTA_URL + "resolver/resolver_form.php",
		async:true,
		dataType: "html",
		data: ({
			action: act,
			cre_tip_cre_aden: tip,
			cre_tip: 3,
			cre_est: est,
			cre_id:	id
		}),
		beforeSend: function() {
//			$('#div_credito_his').dialog('open');
//			$('#msj_credito').html("Cargando...");
//			$('#msj_credito').show(100);
		},
		success: function(html){
			$('#div_resolver_form').html(html);
			modal_width_auto('modal_creditogarveh_resolver',85);
      modal_height_auto('modal_creditogarveh_resolver');
      $('#modal_creditogarveh_resolver').modal('show');
      modal_hidden_bs_modal('modal_creditogarveh_resolver', 'limpiar'); //funcion encontrada en public/js/generales.js
		},
		complete: function(){
			//credito_tabla();
		}
	});
}

function registrar_polizas(credito_id){
$.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/polizas_reg.php",
        dataType: "json",
        data: ({
            action: 'insertar_de_credito',
            credito_id: credito_id,          
            creditotipo_id: 3          
        }),
        beforeSend: function() {
          //$('#msj_clientenota').html("Guardando datos...");
          //$('#msj_clientenota').show(100);
        },
        success: function(data){
            console.log(data);
          if(data.estado>0){
          	alerta_success("EXITO",data.msj);
          }
          else
                alerta_error("ERROR",data.msj);
        },
        complete: function(data){
          console.log(data);
          if(data.statusText == "parsererror"){
            console.log(data);
            $('#msj_creditoplaca').text('ERROR: ' + data.responseText);
          }
        }
      });
    };

// JUAN : 07-08-2023

function liquidacion_formato_form(credito_id){
  var fecha_hoy = $('#txt_fecha_ref').val();

  $.confirm({
    title: 'Fecha de Liquidación',
    content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    '<label>Ingresa tu Fecha Aquí:</label>' +
    '<input type="date" class="name form-control" required />' +
    '</div>' +
    '</form>',
    buttons: {
      formSubmit: {
        text: 'Generar',
        btnClass: 'btn-blue',
        action: function () {
          var fecha_liquidacion = this.$content.find('.name').val();
          if(!fecha_liquidacion){
            $.alert('Debes ingresar una fecha valida');
            return false;
          }
          var tipo_cambio = $('#hdd_tipo_cambio_venta').val();

          $.ajax({
            type: "POST",
            url: VISTA_URL + "liquidacion/liquidacion_formato_form.php",
            async:true,
            dataType: "html",
            data: ({
              credito_id: credito_id,
              creditotipo_id: 3, //estamos en la carpeta de garveh, por ende es el 3
              fecha_liquidacion: fecha_liquidacion,
              tipo_cambio: tipo_cambio
            }),
            beforeSend: function() {
              $('#modal_mensaje').modal('show');
            },
            success: function(html){
              $('#modal_mensaje').hide('show');
              $('#div_modal_liquidacion').html(html);
              modal_width_auto('modal_liquidacion_formato',92);
              modal_height_auto('modal_liquidacion_formato');
              $('#modal_liquidacion_formato').modal('show');
              modal_hidden_bs_modal('modal_liquidacion_formato', 'limpiar'); //funcion encontrada en public/js/generales.js
            },
            complete: function(){
              //credito_tabla();
            }
          });
        }
      },
      Cancelar: function () {
          //close
      },
    },
    onContentReady: function () {
      // bind to events
      var jc = this;
      this.$content.find('form').on('submit', function (e) {
          // if the user submits the form by pressing enter in the field.
          e.preventDefault();
          jc.$$formSubmit.trigger('click'); // reference the button and click it
      });
    }
  });
}

//JUAN 16-09-2023

function creditolinea_form(action, creditolinea_id, creditotipo_id, credito_id) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditolinea/creditolinea_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action:	action,
      creditolinea_id: creditolinea_id,
      creditotipo_id: creditotipo_id,
      credito_id: credito_id,
      modulo: 'creditogarveh'
		}),
		beforeSend: function() {
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_creditolinea_form').html(data);
        // modal_width_auto('modal_creditogarveh_placa',80);
        // modal_height_auto('modal_creditogarveh_placa');
        $('#modal_creditolinea_form').modal('show');
        modal_hidden_bs_modal('modal_creditolinea_form', 'limpiar'); //funcion encontrada en public/js/generales.js
      }		
		}
	});
}

// GERSON (11-11-23)
function generarSimulador(proceso_id, usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/generar_simulador.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_generar_simulador').html(data);
      $('#modal_generar_simulador').modal('show');
      modal_height_auto('modal_generar_simulador'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_generar_simulador', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

// GERSON (17-02-24)
function verChequesGenerados(usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarveh/ver_cheques_generados.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_cheques_generados').html(data);
      $('#modal_cheques_generados').modal('show');
      modal_height_auto('modal_cheques_generados'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_cheques_generados', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function obtener_valor_seguro_gps(){

  var cuota = $("#simu_cuota").val();

  if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else{

    var numero_cuotas = parseInt(cuota);
    if(numero_cuotas <= 0)
      return false;

    $.ajax({
      type: "POST",
      url: VISTA_URL + "segurogps/segurogps_controller.php",
      async: true,
      dataType: "json",
      data: {
        action: "valores",
        numero_cuotas: numero_cuotas
      },
      beforeSend: function () {
      },
      success: function (data) {
        console.log(data);
        $('#hdd_seguro_porcentaje').val(data.valor_seguro);
        $('#hdd_gps_precio').val(data.valor_gps);
        $('#span_seguro_gps').val(data.texto);
        calcularSimulacion();
      },
      complete: function (data) {},
    });

  }
  
}

function guardarSimulador(id, value, tipo_dato, vista) {
  if(vista === 'procesos'){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
        action: 'guardar_campo_simulador',
        proceso_id: $("#hdd_simu_proceso_id").val(),
        id: id, 
        value: value,
        tipo_dato: tipo_dato,
      }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado == 1){
            notificacion_success(data.mensaje, 3000);
          }else{
            notificacion_warning(data.mensaje, 3000);
          }
        },
        complete: function (data) {
        }
    });
  }
}

function calcularSimulacion(){

  var chk_gps = 0;
  if($('input[name="chk_gps"]:checked').val()){
    chk_gps = 1;
  }

  var fecha = $("#simu_fecha").val();
  var moneda = $("#simu_moneda").val();
  var cambio = $("#simu_cambio").val();
  var cuota = $("#simu_cuota").val();
  var interes = $("#simu_interes").val();
  var costo = $("#simu_costo").val();
  var inicial = $("#simu_inicial").val();
  var valor = $("#simu_valor").val();

  var vista = $("#hdd_vista").val();
  if(vista==='procesos'){
    if(inicial=='' || parseFloat(inicial)==0){
      notificacion_warning("Ingrese el monto de inicial.", 3000);
    }
  }

  if(cambio=='' || parseFloat(cambio)==0){
    notificacion_warning("Ingrese el tipo de cambio.", 3000);
  }else if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else if(interes=='' || parseFloat(interes)==0){
    notificacion_warning("Ingrese el monto de interés.", 3000);
  }else if(costo=='' || parseFloat(costo)==0){
    notificacion_warning("Ingrese el costo de vehículo.", 3000);
  }else if(valor=='' || parseFloat(valor)==0){
    notificacion_warning("Ingrese el valor del préstamo.", 3000);
  }else{

    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async:true,
      dataType: "html",                      
      data: ({
        action: 'detalle_simulacion',
        proceso_id: parseInt($('#hdd_simu_proceso_id').val()),
        usuario_id: parseInt($('#hdd_simu_usuario_id').val()),
        fecha: fecha,
        moneda: moneda,
        cambio: cambio,
        cuota: cuota,
        interes: interes,
        costo: costo,
        inicial: inicial,
        valor: valor,
        chk_gps: chk_gps,
        seguro_porcentaje: $('#hdd_seguro_porcentaje').val(),
        gps_precio: $('#hdd_gps_precio').val(),
        span_seguro_gps: $('#span_seguro_gps').val(),
      }),
      beforeSend: function() {
      },
      success: function(html){
        $('#div_simulacion').html(html);  
      }
    });

  }
  
}

function generarSimuladorExcelPDF(proceso_id, usuario_id, tipo) {

  var chk_gps = 0;
  if($('input[name="chk_gps"]:checked').val()){
    chk_gps = 1;
  }

  var fecha = $("#simu_fecha").val();
  var moneda = $("#simu_moneda").val();
  var cambio = $("#simu_cambio").val();
  var cuota = $("#simu_cuota").val();
  var interes = $("#simu_interes").val();
  var costo = $("#simu_costo").val();
  var inicial = $("#simu_inicial").val();
  var valor = $("#simu_valor").val();

  var seguro_porcentaje = $('#hdd_seguro_porcentaje').val();
  var gps_precio        = $('#hdd_gps_precio').val();
  var span_seguro_gps   = $('#span_seguro_gps').val();

  var vista = $("#hdd_vista").val();
  if(vista==='procesos'){
    if(inicial=='' || parseFloat(inicial)==0){
      notificacion_warning("Ingrese el monto de inicial.", 3000);
    }
  }

  if(cambio=='' || parseFloat(cambio)==0){
    notificacion_warning("Ingrese el tipo de cambio.", 3000);
  }else if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else if(interes=='' || parseFloat(interes)==0){
    notificacion_warning("Ingrese el monto de interés.", 3000);
  }else if(costo=='' || parseFloat(costo)==0){
    notificacion_warning("Ingrese el costo de vehículo.", 3000);
  }else if(valor=='' || parseFloat(valor)==0){
    notificacion_warning("Ingrese el valor del préstamo.", 3000);
  }else{

    //window.open("http://www.ipdnsac.com/ipdnsac/vista/cuenta/cuenta_reporte_excel.php?fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&cuenta_tipo=" + cuenta_tipo + "&sede=" + sede);
    if(tipo=='excel'){
      window.open(VISTA_URL + "proceso/generar_simulador_excel.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&fecha="+fecha+"&moneda="+moneda+"&cambio="+cambio+"&cuota="+cuota+"&interes="+interes+"&costo="+costo+"&inicial="+inicial+"&valor="+valor+"&seguro_porcentaje="+seguro_porcentaje+"&gps_precio="+gps_precio+"&span_seguro_gps="+span_seguro_gps+"&chk_gps="+chk_gps);
    }else{
	    //window.open("vista/inventarioproducto/pdf_inventarioproducto_tabla.php?inventario_id="+inventario_id,"_blank");

      window.open("vista/proceso/generar_simulador_pdf.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&fecha="+fecha+"&moneda="+moneda+"&cambio="+cambio+"&cuota="+cuota+"&interes="+interes+"&costo="+costo+"&inicial="+inicial+"&valor="+valor+"&seguro_porcentaje="+seguro_porcentaje+"&gps_precio="+gps_precio+"&span_seguro_gps="+span_seguro_gps+"&chk_gps="+chk_gps,"_blank");
    }

  }
}

function solo_numero(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}
//

// GERSON (04-0824)
function credito_acciones(cre_id){
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarveh/creditogarveh_seguimiento.php",
      async:true,
      dataType: "html",                      
      data: ({
          credito_id:	cre_id
      }),
      beforeSend: function() {
      },
      success: function(data){
          if(data != 'sin_datos'){
            $('#div_credito_seguimiento').html(data);
            modal_width_auto('modal_creditogarveh_seguimiento',80);
            modal_height_auto('modal_creditogarveh_seguimiento');
            $('#modal_creditogarveh_seguimiento').modal('show');
            modal_hidden_bs_modal('modal_creditogarveh_seguimiento', 'limpiar'); //funcion encontrada en public/js/generales.js

            //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

          }	
      }
  });
}

// ----------------- CAMBIOS 15-10-2024 ----------------- //

function creditogarveh_modal_titulos_form(creditogarveh_id,usuario_act,tb_credito_subgar,tb_credito_cus,estado) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarveh/creditogarveh_titulos_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: 'L', // PUEDE SER: L, I, M , E
        creditogarveh_id: creditogarveh_id,
        vista: 'creditogarveh',
        tb_credito_subgar: tb_credito_subgar,
        tb_credito_cus: tb_credito_cus,
        estado: estado
      }),
      beforeSend: function () {
        // console.log(':: beforeSend :: abriendo creditogarveh_titulos_form.php');
        // $('#h3_modal_title').text('Cargando Formulario');
        // $('#modal_mensaje').modal('show');
      },
      success: function (data) {
        // console.log(':: success :: proceso de carga terminado');
        $('#div_modal_titulos_form').html(data);
        $('#modal_titulos_form').modal('show');
        //desabilitar elementos del form si es L (LEER)
        // if (usuario_act == 'L'){
        //     form_desabilitar_elementos('form_credgarv_titulos');
        // } 
        modal_hidden_bs_modal('modal_titulos_form', 'limpiar');
        modal_width_auto('modal_titulos_form',98);
        modal_height_auto('modal_titulos_form');
      },
      complete: function (data) {
        // console.log(':: complete ::');
        // $('#modal_mensaje').modal('hide');
      },
      error: function (data) {
        // console.log('error: '+data);
        alerta_warning("error",''+data.responseText);
        $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
        $('#overlay_modal_mensaje').removeClass('overlay').empty();
        $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);        
      }
  });
}

function controlseguro_form(action, poliza_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "controlseguro/controlseguro_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: action,
          poliza_id: poliza_id,
          vista: 'creditogarveh'
      }),
      beforeSend: function (data) {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
          $('#div_controlseguro_form').html(data);
          $('#modal_registro_controlseguro').modal('show');
          modal_height_auto('modal_registro_controlseguro');
          modal_hidden_bs_modal('modal_registro_controlseguro', 'limpiar');
          modal_width_auto('modal_registro_controlseguro', 80);
          if( $.trim(action) === 'Leer' ){ form_desabilitar_elementos('form_controlseguro'); }
          $('#modal_mensaje').modal('hide');
      },
      complete: function (data) {
          //console.log(data);
      }
  });
}