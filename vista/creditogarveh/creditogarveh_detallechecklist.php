<?php 
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../tipogarvdocpersdetalle/Tipogarvdocpersdetalle.class.php');
    $oTipogarvdocpersdetalle = new Tipogarvdocpersdetalle();
    require_once('../pdfgarv/Pdfgarv.class.php');
    $oPdfgarv = new Pdfgarv();
    require_once('../historial/Historial.class.php');
    $oHistorial = new Historial();
    require_once('../proceso/Proceso.class.php');
    $oProceso = new Proceso();
  
    $cliente_tip = $_POST['tipopersona_id'];
    $cre_subgar = $_POST['cre_subgar'];
    $cre_cus = $_POST['cre_cus'];
    $cre_tip = $_POST['cre_tip'];
    $creditogarveh_id = $_POST['creditoid'];
    $cli_id = $_POST['clienteid'];
    $proceso_id = intval($_POST['proceso_id']);

    $tipopersona_id = $cliente_tip; // USO DIRECTO PARA TIPO PERSONA
    if($cre_subgar == 'PRE-CONSTITUCION'){
        $cgarvtipo_id = 1;
    }
    if($cre_subgar == 'REGULAR'){
        if($cre_cus=='1'){
            $cgarvtipo_id = 3;
        }
        if($cre_cus=='2'){
            $cgarvtipo_id = 2; 
        }
    }
    if($cre_subgar == 'GARMOB COMPRA TERCERO'){
        if($cre_cus=='1'){
            $cgarvtipo_id = 5;
        }
        if($cre_cus=='2'){
            $cgarvtipo_id = 4; 
        }
    }
    if($cre_subgar == 'GARMOB IPDN VENTA'){
        $cgarvtipo_id = 7;
    }
    if($cre_subgar == 'GARMOB IPDN VENTA CON RD'){
        $cgarvtipo_id = 8;
    }
    if($cre_tip == 2 || $cre_subgar == 'ADENDA'){
        $cgarvtipo_id = 6; 
    }
   
    $listado ='';

    if($proceso_id > 0) {

        if($cgarvtipo_id == 6) {
            // para adenda
            $proceso_fase = $oProceso->mostrarProcesoFase($proceso_id, 3);  // Obtener fase 3
        }else{
            $proceso_fase = $oProceso->mostrarProcesoFase($proceso_id, 4);  // Obtener fase 4
        }
        if($proceso_fase['estado'] == 1){
            $items = $oProceso->obtenerItemsPorProcesoFase($proceso_fase['data']['tb_proceso_fase_id']);  // Obtener items de fase 4

            //$pdf = '';
            
            if($items['estado'] == 1){

                foreach ($items['data'] as $key => $item) {

                    //var_dump($item['tb_multimedia_id']);
                    //if($item['tb_multimedia_exo'] == null){ // quiere decir que no hay ningun registro subido, ni si quiera con xac=0
                    if(is_null($item['tb_multimedia_id'])){
                        // insertar
                        $color = 'red'; $action='new';
                        $pdf = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="" title="Subir documento">'
                            . '<i class="fa fa-file-pdf-o"></i>'
                            . '</a>'
                            . ' <a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="" title="Exonerar">'
                            . '<i class="fa fa-check"></i>'
                            . '</a>';
                    }else{
                        
                        if($item['tb_multimedia_exo'] > 0){ // 1=exonerado
                                    
                            $color = 'green';
                            $pdf = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick=vercredit_pdf_form_proceso('.$item['tb_proceso_id'].','.$item['tb_item_id'].') title="Ver pdf">'
                                    . '<i class="fa fa-file-pdf-o"></i>'
                                    . '</a>';
                                    $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                    $multi_url      = $items_fase['data']['tb_multimedia_url'];
                            $pdf .= '  <a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="hist_doc_form_proceso('.$item['tb_proceso_id'].','.$item['tb_item_id'].')" title="Historial y Comentarios">'
                                    . '<i class="fa fa-history"></i>'
                                    . '</a>';
                        }else{

                            if(intval($item['tb_multimedia_est']) == 2){ // con archivo

                                $items_fase = $oProceso->obtenerContenidoPorProcesoFaseItem($item['tb_proceso_id'], $item['tb_item_id']); 

                                if($items_fase['estado']==1){

                                    $color = 'green';

                                    //$color = 'red'; $action='mod'.$cont.'-'.$result1['data']['tb_pdfgarv_id'];
                                    $pdf = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick=vercredit_pdf_form_proceso('.$item['tb_proceso_id'].','.$item['tb_item_id'].') title="Ver pdf">'
                                    . '<i class="fa fa-file-pdf-o"></i>'
                                    . '</a>';
                                    $multimedia_id  = $items_fase['data']['tb_multimedia_id'];
                                    $multi_url      = $items_fase['data']['tb_multimedia_url'];

                                }
                                
                            }
                            $pdf .= '  <a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="hist_doc_form_proceso('.$item['tb_proceso_id'].','.$item['tb_item_id'].')" title="Historial y Comentarios">'
                            . '<i class="fa fa-history"></i>'
                            . '</a>';
                        }
                        
                        

                    }

                    

                    $listado .=   '<div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="checkbox-inline" style="padding:6px">
                                <span class="description-percentage text-'.$color.'">
                                    <i class="fa fa-caret-up"></i>
                                </span>
                                <b>'. $item['tb_item_des'] .'</b>
                            </label>
                        </div>
                        <div class="col-md-6">
                            '.$pdf.'
                        </div>
                    </div>
                    </div>';

                }

            }

        }

        


    }else{

        $result = $oTipogarvdocpersdetalle->mostrarDocsObligatorios($cgarvtipo_id, $tipopersona_id);
        if($result['estado']==1){
            foreach ($result['data'] as $key => $value) {
                $action='';
                $result1 = $oPdfgarv->mostrarPorDocumento1($creditogarveh_id, $value['tipogarvdocpersdetalle_id']);
                if($result1['estado'] != 1){ // quiere decir que no hay ningun registro subido, ni si quiera con xac=0
                        // insertar
                        $color = 'red'; $action='new';
                        $pdf = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="credito_pdf_form1('.$value['tipogarvdocpersdetalle_id'].",'".$value['cgarvdoc_nom']. "', '".$action."'". ')" title="Subir documento">'
                            . '<i class="fa fa-file-pdf-o"></i>'
                            . '</a>'
                            . ' <a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="exonerar_pdf_form1('.$value['tipogarvdocpersdetalle_id'].", '".$value['cgarvdoc_nom']. "', '".$action."'".')" title="Exonerar">'
                            . '<i class="fa fa-check"></i>'
                            . '</a>';
                }
                else{ //hay registro, ya sea tipo 1: subida de doc, o tipo 2: exoneracion, cualquiera de estos dos estados puede tener xac=0 o xac=1
                    //si el xac =1 -> se muestra en azul, se puede ver, y eliminar(dentro del form vercredit_pdf_form, realiza el cambio de xac)
                    if(intval($result1['data']['tb_pdfgarv_xac']) == 1){
                        $color = 'green';
                        $pdf = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="vercredit_pdf_form('.$result1['data']['tb_pdfgarv_id'].')" title="Ver pdf">'
                                . '<i class="fa fa-file-pdf-o"></i>'
                                . '</a>';
                    }
                    else{  // si el xac=0 -> aparece en naranja y editaremos el registro = modificar(id_reg, 'actualizardoc_subir') o modificar(id_reg, 'actualizardoc_exonerar')
                        $result2 = $oHistorial->filtrar_historial_por('pdfgarv',$result1['data']['tb_pdfgarv_id'])['data'];
                        $cont = 1;
                        foreach($result2 as $k => $v){
                            $contador = substr(trim($v['tb_hist_det']),3,3);
                            if($contador == 'act' || $contador == 'exo'){
                                $cont++;
                            }
                        }
                        $color = 'red'; $action='mod'.$cont.'-'.$result1['data']['tb_pdfgarv_id'];
                        $pdf = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="credito_pdf_form1('.$value['tipogarvdocpersdetalle_id'].",'".$value['cgarvdoc_nom']. "', '".$action."'".')" title="Actualizar documento">'
                            . '<i class="fa fa-file-pdf-o"></i>'
                            . '</a>'
                            . ' <a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="exonerar_pdf_form1('.$value['tipogarvdocpersdetalle_id'].",'".$value['cgarvdoc_nom']. "', '".$action."'".')" title="Exonerar">'
                            . '<i class="fa fa-check"></i>'
                            . '</a>';
                    }
                    $pdf .= '  <a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="hist_doc_form('.$result1['data']['tb_pdfgarv_id'].')" title="Historial y Comentarios">'
                            . '<i class="fa fa-history"></i>'
                            . '</a>';
                }
                
                $listado .=   '<div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="checkbox-inline" style="padding:6px">
                                <span class="description-percentage text-'.$color.'">
                                    <i class="fa fa-caret-up"></i>
                                </span>
                                <b>'. $value['cgarvdoc_nom'] .'</b>
                            </label>
                        </div>
                        <div class="col-md-6">
                            '.$pdf.'
                        </div>
                    </div>
                    </div>';
            }
        }

    }
   
   echo $listado;
    
    ?>