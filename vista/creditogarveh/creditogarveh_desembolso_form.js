$(document).ready(function () {

  $('#for_credes').validate({
    submitHandler: function () {
      var max_desem = Number($('#hdd_credes_mon').val());
      var desem = $('#txt_credes_mon').val();
      var reten = $('#txt_credes_ret').val();
      var res = 0;
      var flag_cheque = $('#hdd_flag_cheque').val();
      res = desem + reten;

      if (res > max_desem) {
        alerta_warning('Importante', 'No puede desembolsar un monto mayor al máximo: ' + res + ' // ' + max_desem);
        return false;
      }
      if (desem == 0 && reten == 0) {
        alerta_warning('Importante', 'No se puede desembolsar un monto 0');
        return false;
      }
      //console.log('MAX:'+max_desem+' DESEM:'+desem+' RETEN:'+reten+' RES:'+res);

      // GERSON 10-11-23
      var valor = $('input:checkbox[name=chequeSeleccionados]:checked').val();
      var datax = $('#for_credes').serializeArray();
		  datax.push({name: "chequeSelect",value:valor});

      if(flag_cheque === ''){ // transferencia, efectivo, depostio

        $.ajax({
          type: "POST",
          url: VISTA_URL + "creditogarveh/credito_desembolso_reg.php",
          async: true,
          dataType: "json",
          //data: $("#for_credes").serialize(),
          data: datax,
          beforeSend: function () {
            $('#msj_desembolso').show(400);
            $('#btn_guardar_desembolso').prop('disabled', true);
          },
          success: function (data) {
            console.log(data);
            if (parseInt(data.estado) > 0) {
  
              if(parseInt(data.estado) == 2){
  
                $('#btn_guardar_desembolso').prop('disabled', false);
                alerta_error('Error', data.mensaje);
  
              }else{
  
                $('#creditogarveh_desembolso_form').removeClass('callout-info').addClass('callout-success');
                $('#creditogarveh_desembolso_form').html(data.mensaje);
    
                setTimeout(function () {
                  if($("#hdd_vista").val() === 'credito_tabla'){
                    creditogarveh_tabla();
                  }
                  $('#modal_creditogarveh_desembolso').modal('hide');
                  }, 1000
                );
  
              }
  
            }
            else {
              alerta_error('Error', data.mensaje);
            }
          },
          complete: function (data) {
             
          },
          error: function (data) {
            console.log(data);
            alerta_error('Error', data.responseText);
          }
        });

      }else{ // cheque

        if(valor === undefined){
          alerta_warning('Importante', 'Debes seleccionar un cheque válido.');
        }else{
  
          $.ajax({
            type: "POST",
            url: VISTA_URL + "creditogarveh/credito_desembolso_reg.php",
            async: true,
            dataType: "json",
            //data: $("#for_credes").serialize(),
            data: datax,
            beforeSend: function () {
              $('#msj_desembolso').show(400);
              $('#btn_guardar_desembolso').prop('disabled', true);
            },
            success: function (data) {
              console.log(data);
              if (parseInt(data.estado) > 0) {
    
                if(parseInt(data.estado) == 2){
    
                  $('#btn_guardar_desembolso').prop('disabled', false);
                  alerta_error('Error', data.mensaje);
    
                }else{
    
                  $('#creditogarveh_desembolso_form').removeClass('callout-info').addClass('callout-success');
                  $('#creditogarveh_desembolso_form').html(data.mensaje);
      
                  setTimeout(function () {
                    if($("#hdd_vista").val() === 'credito_tabla'){
                      creditogarveh_tabla();
                    }
                    $('#modal_creditogarveh_desembolso').modal('hide');
                    }, 1000
                  );
    
                }
    
              }
              else {
                alerta_error('Error', data.mensaje);
              }
            },
            complete: function (data) {
               
            },
            error: function (data) {
              console.log(data);
              alerta_error('Error', data.responseText);
            }
          });
  
        }

      }
      
      
    },
    rules: {
      txt_cuotatipo_nom: {
        required: true,
        minlength: 2
      },
      txt_finalidad: {
        required: true,
        minlength: 5
      },
      cmb_cheque_motivo: {
        required: true
      }
    },
    messages: {
      txt_cuotatipo_nom: {
        required: "Ingrese un nombre para el Tipo de Crédito",
        minlength: "Como mínimo el nombre debe tener 2 caracteres"
      },
      txt_finalidad: {
        required: "Ingrese una descripción para el Tipo de Crédito",
        minlength: "La descripción debe tener como mínimo 5 caracteres"
      },
      cmb_cheque_motivo: {
        required: "Debes seleccionar un motivo para el desembolso"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });

  //tipo_cronograma();

  var cretip_id = $("#hdd_cre_tip").val();
  var credes_mon = $("#hdd_credes_mon").val();

  $('.moneda3').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: credes_mon
  });

  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '999999.99'
  });

  $('#cmb_mon_id_des').change(function (event) {
    cambio_moneda_desem();
  });

  $('#txt_credes_mon, #txt_credes_tipcam').change(function (event) {
    calculo_desembolso_diferente_moneda();
  });

  $('#txt_credes_moncam').change(function (event) {
    calculo_reverso_diferente_moneda();
  });

  $("#txt_credes_fec").change(function () {
    $("#txt_credes_fecfac").val($(this).val()).change();
  });

  $("#txt_credes_fecfac, #txt_credes_fecpro").change(function () {
    credito_cronograma();
  });

  $('#txt_credes_ret').change(function (event) {
    var max_desem = Number($('#hdd_credes_mon').val());
    var reten = Number($('#txt_credes_ret').val());
    console.log(max_desem + " / " + reten);
    var res = 0;
    res = max_desem - reten;
    console.log(res);
    if(Number($('#cmb_mon_id_des').val()) != "" ){
        $('#txt_credes_mon').val(0);
        $("#txt_credes_moncam").autoNumeric('set', 0);
    }
    else{
      $('#txt_credes_mon').val(res.toFixed(2));
    }
  });
    
  $('#cmb_credes_mot').change(function (event) {
    //var valdolar = $("#txt_credes_mon").val();
    //var valsol = $("#txt_credes_moncam").val();
    //console.log(valdolar + ' / '+ valsol);
    var codigo = $(this).val();
    if(codigo != ""){
      $("#txt_credes_ret").removeAttr("readonly");
      $("#txt_credes_mon").attr("readonly","readonly");
      //$("#txt_credes_mon").val(0);
      //$("#txt_credes_moncam").val(0);
    }
    else{
      $("#txt_credes_ret").attr("readonly","readonly");
      $("#txt_credes_mon").removeAttr("readonly");
      $("#txt_credes_ret").val(0);
      //$("#txt_credes_mon").val(valdolar);
      //$("#txt_credes_moncam").val(valsol);
    }
  });

  let Checked = null;
  if($("#hdd_flag_cheque").val() != null || $("#hdd_flag_cheque").val() != ''){
    for (let CheckBox of document.getElementsByClassName('only-one')){
      CheckBox.onclick = function(){
        if(Checked!=null){
          Checked.checked = false;
          Checked = CheckBox;
        }
        Checked = CheckBox;
      }
    }
  }

});

function tipo_cronograma() {
    //si el tipo es 0, entonces no se ha guardado el cronograma
    var tipo = parseInt($('#hdd_cuo_guardadas').val());
    console.log(tipo);
    if (tipo == 0)
      credito_cronograma();
    else
      cuota_tabla();
}

function credito_cronograma() {
    //verificamos si el cronograma ya fue guarado para que se ejecute esta funcion
    var cuo = parseInt($('#hdd_cuo_guardadas').val());
    if (parseInt(cuo) > 0)
        return false;

    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/creditogarveh_cronograma.php",
        async: true,
        dataType: "html",
        data: ({
            cre_idori: $('#hdd_cre_id_ori').val(),
            cre_preaco: $('#hdd_credes_mon').val(),
            cre_int: $('#hdd_cre_int').val(),
            cre_numcuo: $('#hdd_num_cuo').val(),
            cre_numcuo_res: $('#hdd_cre_cuo_res').val(), //cuotas por pagar, sirve para calcular el interes de la CUOTA BALON
            mon_id: $('#hdd_mon_id').val(),
            cre_fecfac: $("#txt_credes_fecfac").val(),
            cre_feccre: '<?php echo mostrarFecha($cre_feccre) ?>',
            cre_fecpro: $('#txt_credes_fecpro').val(),
            cre_fecdes: $("#txt_credes_fec").val(),
            cuosubper_id: $('#hdd_cuosubper').val(),
            cuotip_id: $('#hdd_cuotip').val(),
            tip_ade: $('#hdd_tip_ade').val()
        }),
        beforeSend: function () {
            //$('#div_credito_cronograma_desem').html('<span>Cargando...</span>');
        },
        success: function (html) {
            $('#div_credito_cronograma_desem').html(html);
        },
        complete: function (data) {
          if (data.statusText != 'success')
            console.log(data);
        }
    });
}

function cuota_tabla() {
    console.log('del credito id: ' + $('#hdd_cre_id').val());
    $cre_id = $('#hdd_cre_id').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuota/cuota_tabla_garveh.php",
        async: true,
        dataType: "html",
        data: ({
            cre_id: $cre_id
        }),
        beforeSend: function () {
            $('#div_cuota_tabla_desem').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_cuota_tabla_desem').html(html);
        },
        complete: function (data) {
            if (data.statusText != 'success')
                //console.log(data.responseText);
                $('#div_cuota_tabla_desem').removeClass("ui-state-disabled");
        }
    });
}

function cambio_moneda_desem() {
  var moneda_original_id = $('#hdd_mon_id').val(); //moneda original del crédito
  var moneda_seleccion_id = $('#cmb_mon_id_des').val(); //moneda seleccionada por el usuario
  var tipo_cambio_credito = Number($('#hdd_credito_tc').val()); //tipo de cambio registrado en el crédito original
  var tipo_cambio_dia_creado = Number($('#hdd_tipcam').val()); //tc del día en que fue creado el crédito

  $('#txt_credes_tipcam').val(0);

  if(tipo_cambio_credito > 0)
    $('#txt_credes_tipcam').val(tipo_cambio_credito); //asignamos el tipo de cambio del crédito al desembolso
  else if(tipo_cambio_dia_creado > 1)
    $('#txt_credes_tipcam').val(tipo_cambio_dia_creado); //tc del día en que se creó el crédito

  if(parseInt(moneda_original_id) != parseInt(moneda_seleccion_id)){
    calculo_desembolso_diferente_moneda();
  }
  //si vuelve a elegir la misma oneda, todo regresa a como es ORGINALMENTE
  if (parseInt(moneda_original_id) == parseInt(moneda_seleccion_id)) {
    $('#txt_credes_moncam').autoNumeric('set', 0);
    $('#td_mon_cam').text('Monto Cambio Moneda :');
  }
}

function calculo_desembolso_diferente_moneda(){
  var moneda_original_id = $('#hdd_mon_id').val(); //moneda original del crédito
  var moneda_seleccion_id = $('#cmb_mon_id_des').val(); //moneda seleccionada por el usuario
  var valor_tipo_cambio = Number($('#txt_credes_tipcam').val());  
  var monto_desembolsar = Number($('#txt_credes_mon').val().replace(/[^0-9\.]+/g, ""));
  var result = 0;

  if(valor_tipo_cambio <= 0 || monto_desembolsar <= 0){
    alerta_warning('Importante', 'Debes asignar un tipo de cambio para desembolsar en diferente moneda y un valor mayor que 0 para desembolsar');
    return false;
  }

  if (parseInt(moneda_original_id) == 2 && parseInt(moneda_seleccion_id) == 1) {
    //el credito en dolares se da en soles
    result = monto_desembolsar * parseFloat(valor_tipo_cambio).toFixed(3);
    $('#td_mon_cam').text('Monto Cambio en S/.');
    $('#txt_credes_moncam').autoNumeric('set', result.toFixed(2));
  } 
  else {
    //el credito en soles se da en dolares
    result = monto_desembolsar / parseFloat(valor_tipo_cambio).toFixed(3);
    $('#td_mon_cam').text('Monto Cambio en US$.');
    $('#txt_credes_moncam').autoNumeric('set', result.toFixed(2));
  }
}

function calculo_reverso_diferente_moneda(){
  var moneda_original_id = $('#hdd_mon_id').val(); //moneda original del crédito
  var moneda_seleccion_id = $('#cmb_mon_id_des').val(); //moneda seleccionada por el usuario
  var valor_tipo_cambio = Number($('#txt_credes_tipcam').val());  
  var monto_desembolsar = Number($('#txt_credes_moncam').val().replace(/[^0-9\.]+/g, ""));
  var result = 0;

  if(valor_tipo_cambio <= 0 || monto_desembolsar <= 0){
    alerta_warning('Importante', 'Debes asignar un tipo de cambio para desembolsar en diferente moneda y un valor mayor que 0 para desembolsar');
    return false;
  }

  if (parseInt(moneda_original_id) == 2 && parseInt(moneda_seleccion_id) == 1) {
    //el credito en dolares se da en soles
    result = monto_desembolsar / parseFloat(valor_tipo_cambio).toFixed(3);
    $('#td_mon_cam').text('Monto Cambio en S/.');
    $('#txt_credes_mon').autoNumeric('set', result.toFixed(2));
  } 
  else {
    //el credito en soles se da en dolares
    result = monto_desembolsar * parseFloat(valor_tipo_cambio).toFixed(3);
    $('#td_mon_cam').text('Monto Cambio en US$.');
    $('#txt_credes_mon').autoNumeric('set', result.toFixed(2));
  }
}

// GERSON (09-11-23)
function verMovimientosCheque(chequedetalle_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/ver_cheque_retiro.php",
    async: true,
    dataType: "html",
    data: ({
      chequedetalle_id: chequedetalle_id
    }),
    beforeSend: function() {
    },
    success: function(data){
      $('#div_modal_cheque_retiro').html(data);
      $('#modal_cheque_retiro').modal('show');
      modal_height_auto('modal_cheque_retiro'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_cheque_retiro', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      //$('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function verMovimientosOtro(proceso_id, tipo){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/ver_otro_retiro.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      tipo: tipo
    }),
    beforeSend: function() {
    },
    success: function(data){
      $('#div_modal_otro_retiro').html(data);
      $('#modal_otro_retiro').modal('show');
      modal_height_auto('modal_otro_retiro'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_otro_retiro', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      //$('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}
//