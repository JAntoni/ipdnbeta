<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ("../../core/usuario_sesion.php");

require_once ("Creditogarveh.class.php");
$oCreditogarveh = new Creditogarveh();
require_once ("../creditoinicial/Creditoinicial.class.php");
$oCreditoinicial = new Creditoinicial();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../notaria/Notaria.class.php");
$oNotaria = new Notaria();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
/* GERSON (21-10-23) */
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
/*  */

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
$usuario_id = intval($_SESSION['usuario_id']);

if($_POST['action_credito']=="I")
{
  if(!empty($_POST['txt_cre_linapr']))
  {
    $xac=1;
    $est=1;
    /* GERSON (12-02-24) */
    $proceso_id = 0;
    if(isset($_POST['hdd_proceso'])){
        $proceso_id = intval($_POST['hdd_proceso']);
    }
    $usuario_aprueba = NULL;
    $fecha_aprueba = NULL;
    if($proceso_id>0){ // si viene de procesos ya está aprobado por comité
      $est=1; // Inicia el crédito ya aprobado
      $usuario_aprueba = 11; // usuario que aprobará por defecto será Engel
      $fecha_aprueba = date('Y-m-d H:i:s'); // fecha de aprobacion que es el mismo dia que se crea
    }
    /*  */
    $mon_id=$_POST['cmb_mon_id'];
    $cuot_tip = intval($_POST['cmb_cuotip_id']); // 4 cuota fija, 3 cuota libre
    $cre_tip = intval($_POST['cmb_credito_tip']); // 1 COMPRA VENTA, 4 MOBILIRIA

    $cre_valtas = moneda_mysql($_POST['txt_cre_valtas']);
    $cre_valrea = moneda_mysql($_POST['txt_cre_valrea']);
    $cre_valgra = moneda_mysql($_POST['txt_cre_valgra']);
    $cre_preaco = moneda_mysql($_POST['txt_cre_preaco']);
    $cre_vent = moneda_mysql($_POST['txt_cre_vent']);

    $moneda_id = intval($_POST['cmb_mon_id']);
    $cambio = 1;

    $dts = $oMonedacambio->consultar(fecha_mysql(date('d-m-Y')));
      if($dts['estado']==1){
        $cambio = floatval($dts['data']['tb_monedacambio_val']);
      }
    $dts = null;

    if(empty($cambio)){
      $data['cre_id'] = 0;
      $data['cre_msj'] = 'Registre el tipo de cambio del día por favor. Del día: '.date('d-m-Y');
      echo json_encode($data); exit();
    }

    if($cre_tip == 4){
      if($moneda_id == 1){
        $prestar = moneda_mysql($cre_preaco / $cambio);

        if($prestar > $cre_valrea){
          $data['cre_id'] = 0;
          $data['cre_msj'] = 'No puedes prestar más que el valor de Realización, Préstamo: '.$prestar.', Realización: '.$cre_valrea;
          echo json_encode($data); exit();
        }
      }

      if($moneda_id == 2 && $cre_preaco > $cre_valrea){
        $data['cre_id'] = 0;
        $data['cre_msj'] = 'No puedes prestar más que el valor de Realización, Préstamo: '.$cre_preaco.', Realización: '.$cre_valrea;
        echo json_encode($data); exit();
      }
    }

    //con custodia = 1, sin custodia = 2
    //con custodia ingresa a else, datos vacios, sin custodia guarda los datos entra al if. Sin custodia solo va con cuota fija, fija es 4, libre 3
    if($_POST['cmb_custodia_id'] == "2"){//sin custodia
      $gps = "";
      $str = "";
      $soa = "";
      $gas = "";
      $gpspre = "";
      $strpre = "";
      $soapre = "";
      $gaspre = "";
      $cus = 2;

      //$cuot_tip = 4; //solo con cuota fija y no libre
    }else{
      $gps = "";
      $str = "";
      $soa = "";
      $gas = "";
      $gpspre = "";
      $strpre = "";
      $soapre = "";
      $gaspre = "";
      $cus = 1;
    }

    $subper = $_POST['cmb_cuosubper_id'];
    if($_POST['cmb_cuotip_id'] == 3) //si es 3 solo será de tipo mensual
      $subper = 1;

    $empresa_id = intval($_SESSION['empresa_id']);

    $oCreditogarveh->credito_usureg = $_SESSION['usuario_id'];
    $oCreditogarveh->credito_cus = intval($_POST['cmb_custodia_id']);
    $oCreditogarveh->cuotatipo_id = $cuot_tip;
    $oCreditogarveh->cuotasubperiodo_id = $subper;
    $oCreditogarveh->cliente_id = intval($_POST['hdd_cre_cli_id']);
    $oCreditogarveh->moneda_id = $mon_id;
    $oCreditogarveh->credito_tipcam = moneda_mysql($_POST['txt_cre_tipcam']); //REVISAR BIEN EL TEMA DEL TIPO DE CAMBIO Y SOLES Y DOLARES
    $oCreditogarveh->representante_id = intval($_POST['cmb_rep_id']);
    $oCreditogarveh->cuentadeposito_id = intval($_POST['cmb_cuedep_id']);
    $oCreditogarveh->vehiculomarca_id = intval($_POST['cmb_vehmar_id_2']);
    $oCreditogarveh->vehiculomodelo_id = intval($_POST['cmb_vehmod_id_2']);
    $oCreditogarveh->vehiculoclase_id = intval($_POST['cmb_vehcla_id_2']);
    $oCreditogarveh->vehiculotipo_id = intval($_POST['cmb_vehtip_id_2']);
    $oCreditogarveh->credito_vehpla = mb_strtoupper($_POST['txt_veh_pla'], 'UTF-8');
    $oCreditogarveh->credito_vehsermot = mb_strtoupper($_POST['txt_veh_mot'], 'UTF-8');
    $oCreditogarveh->credito_vehsercha = mb_strtoupper($_POST['txt_veh_cha'], 'UTF-8');
    $oCreditogarveh->credito_vehano = intval($_POST['txt_veh_fab']);
    $oCreditogarveh->credito_vehcol = mb_strtoupper($_POST['txt_veh_col'], 'UTF-8');
    $oCreditogarveh->credito_vehnumpas = intval($_POST['txt_veh_numpas']);
    $oCreditogarveh->credito_vehnumasi = intval($_POST['txt_veh_numasi']);
    $oCreditogarveh->credito_vehkil = intval($_POST['txt_veh_kil']);
    $oCreditogarveh->credito_vehest = mb_strtoupper($_POST['txt_veh_est'], 'UTF-8');
    $oCreditogarveh->credito_vehent = mb_strtoupper($_POST['txt_veh_ent'], 'UTF-8');
    $oCreditogarveh->credito_gps = $gps;
    $oCreditogarveh->credito_gpspre = moneda_mysql($gpspre);
    $oCreditogarveh->credito_str = $str;
    $oCreditogarveh->credito_strpre = moneda_mysql($strpre);
    $oCreditogarveh->credito_soa = $soa;
    $oCreditogarveh->credito_soapre = moneda_mysql($soapre);
    $oCreditogarveh->credito_gas = $gas;
    $oCreditogarveh->credito_gaspre = moneda_mysql($gaspre);
    $oCreditogarveh->credito_otrpre = moneda_mysql($_POST['txt_otr_pre']);
    $oCreditogarveh->credito_vehpre = moneda_mysql($_POST['txt_veh_pre']);
    $oCreditogarveh->credito_ini = moneda_mysql($_POST['txt_cre_ini']);
    $oCreditogarveh->credito_preaco = moneda_mysql($_POST['txt_cre_preaco']);
    $oCreditogarveh->credito_int = moneda_mysql($_POST['txt_cre_int']);
    $oCreditogarveh->credito_numcuo = $_POST['txt_cre_numcuo'];
    $oCreditogarveh->credito_linapr = moneda_mysql($_POST['txt_cre_linapr']);
    $oCreditogarveh->credito_numcuomax = $_POST['txt_cre_numcuomax'];
    $oCreditogarveh->credito_feccre = fecha_mysql($_POST['txt_cre_feccre']);
    $oCreditogarveh->credito_fecdes = fecha_mysql($_POST['txt_cre_fecdes']);
    $oCreditogarveh->credito_fecfac = fecha_mysql($_POST['txt_cre_fecfac']);
    $oCreditogarveh->credito_fecpro = fecha_mysql($_POST['txt_cre_fecpro']);
    $oCreditogarveh->credito_fecent = fecha_mysql($_POST['txt_cre_fecent']);
    $oCreditogarveh->credito_pla = intval($pla);
    //$oCreditogarveh->credito_comdes = $_POST['txt_cre_comdes'];
    $oCreditogarveh->credito_comdes = mb_strtoupper($_POST['txt_cre_comdes'], 'UTF-8'); //REVISAR EN BLANCO
    $oCreditogarveh->credito_obs = mb_strtoupper($_POST['txt_cre_obs'], 'UTF-8');
    $oCreditogarveh->credito_webref = intval($webref);
    $oCreditogarveh->credito_est = $est;
    $oCreditogarveh->credito_parele = $_POST['txt_veh_parele'];
    $oCreditogarveh->credito_solreg = $_POST['txt_veh_solreg'];
    $oCreditogarveh->credito_fecsol = fecha_mysql($_POST['txt_veh_fecsol']);
    $oCreditogarveh->credito_tipus = '';
    $oCreditogarveh->credito_carg = mb_strtoupper($_POST['txt_cre_carg'], 'UTF-8');
    $oCreditogarveh->credito_fecgps = fecha_mysql($_POST['txt_cre_fecgps']);
    $oCreditogarveh->credito_fecstr = fecha_mysql($_POST['txt_cre_fecstr']);
    $oCreditogarveh->credito_fecsoat = fecha_mysql($_POST['txt_cre_fecsoat']);
    $oCreditogarveh->credito_fecvenimp = fecha_mysql($_POST['txt_cre_fecvenimp']);
    $oCreditogarveh->credito_tip = $cre_tip;
    $oCreditogarveh->credito_valtas = $cre_valtas;
    $oCreditogarveh->credito_valrea = $cre_valrea;
    $oCreditogarveh->credito_valgra = $cre_valgra;
    $oCreditogarveh->credito_vehmode = $_POST['txt_veh_mode'];
    $oCreditogarveh->credito_vehcate = $_POST['txt_veh_cate'];
    $oCreditogarveh->credito_vehcomb = $_POST['txt_veh_comb'];
    $oCreditogarveh->credito_vent = $cre_vent;
    $oCreditogarveh->empresa_id = $empresa_id;
    
    $dts = $oCreditogarveh->insertar();
    if(intval($dts['estado']) == 1){
      $cre_id = $dts['credito_id'];
    }
    $dts = null;

    /* GERSON (12-02-24) */
    if($proceso_id>0){ // si viene de procesos ya está aprobado por comité

      // Se quitó la función el 20/05/24

      /* $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_apr', $fecha_aprueba, 'STR');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_usuapr', $usuario_aprueba, 'INT');

      //registramos el historial para el credito
      $his = '<span style="color: #03959D;">Crédito aprobado por: MIEMBROS DE COMITÉ</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      //$oCreditogarveh->registro_historial($_POST['cre_id'], $his);
      $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_aprueba, $his);

      $line_det = '<b>MIEMBROS DE COMITÉ</b> han aprobado el Crédito GARVEH de número: CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
      $oLineaTiempo->tb_usuario_id = $usuario_aprueba;
      $oLineaTiempo->tb_lineatiempo_det = $line_det;
      $oLineaTiempo->insertar(); */
    }
    /*  */

    //? EVALUAMOS SI SE HA REGISTRADO UN GPS PARA EL CREDITO, Y ASÍ PODER ACTIVAR EL XAC DEL GPS EN LA BD
    $poliza_id = intval($_POST['hdd_gps_simple_id']);

    if($poliza_id > 0){
      $oPoliza->modificar_campo($poliza_id, 'tb_poliza_xac', 1, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante
      $oPoliza->modificar_campo($poliza_id, 'tb_credito_id', $cre_id, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante
    }

    /*
    El credito tiene nuevos cambios, ahora hay 2 formas más, se considera como TRANSFERENCIA, COMPRA VENTA, Y FORMA REGULAR
    VAMOS A GUARDAR:
    1. GUARDAMOS LA SUBA GARANTIA: 1 REGULAR, 2 TRANSFERENCIA, 3 COMPRA VENTA
    1. SI EL CREDITO ES TRANSFERENCIA: GUARDAMOS EL ID DEL TRANSFERENTE
    2. SI EL CREDITO ES COMPRA VENTA SOLO GUARDAMOS INICIAL
    3. PARA AMBOS CASOS REGISTRAMOS LA INICIAL, PARA ELLO GUARDAMOS: MONTO DE INICIAL, ID DE LA TABLA (INGRESO, DEPOSITO), TIPO DE INICIAL (EFECTIVO O DEPOSITO)
    */
    //?VALORES DEL % DEL SEGURO Y PRECIO DEL GPS POR TODO EL TIEMPO DEL CRÉDITO
    $valor_seguro = formato_moneda($_POST['hdd_seguro_porcentaje']);
    $valor_gps = formato_moneda($_POST['hdd_gps_precio']);
    $estado_gps = intval($_POST['che_gps']); // si es 0 no se eligió GPS para el crédito

    $credito_uniq = trim($_POST['hdd_credito_uniq']);
    $credito_subgar = intval($_POST['cmb_credito_subgar']);
    $credito_ini = moneda_mysql($_POST['txt_credito_ini']); //monto incial
    $credito_tipini = intval($_POST['hdd_credito_tipini']); //tipo de inciial: efectivo, deposito (tabla ingreso, deposito)
    $credito_idini = intval($_POST['hdd_credito_idini']); //id de la tabla donde está la inicial, ingreso, deposito
    $transferente_id = intval($_POST['hdd_transferente_id']);
    $credito_tc = $_POST['txt_cre_tc'];
    
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_tc', $credito_tc, 'STR');
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_subgar', $credito_subgar, 'STR');

    //?REGISTRAMOS LOS VALORES DEL % DEL SEGURO Y EL VALOR DEL GPS
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_preseguro', $valor_seguro, 'STR');
    if($estado_gps > 0)
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_pregps', $valor_gps, 'STR');

    if($credito_subgar == 2 || $credito_subgar == 3 || $credito_subgar == 4){ //subgarantia de transferencia
      $oCreditogarveh->modificar_campo($cre_id, 'tb_transferente_id', $transferente_id, 'INT');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_ini', $credito_ini, 'STR');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_idini', $credito_idini, 'INT');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_tipini', $credito_tipini, 'INT');
      $oCreditoinicial->actualizar_credito_uniq($credito_uniq, $cre_id);
    }
    //? GUARDAMOS LOS VALORES DEL CHEQUE A GENERAR
    $credito_monedache = $_POST['txt_credito_monedache'];
    $cre_tc_2 = $_POST['txt_cre_tc_2'];
    $credito_montoche = moneda_mysql($_POST['txt_credito_montoche']);
    
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_monedache', $credito_monedache, 'INT');
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_tc', $cre_tc_2, 'STR');
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_montoche', $credito_montoche, 'STR');

    $zona_id = intval($_POST['cmb_zona_id']);
    $oCreditogarveh->modificar_campo($cre_id, 'tb_zonaregistral_id', $zona_id, 'INT');
    $notaria = intval($_POST['cmb_credito_not']);
    
    $resultf = $oNotaria->mostrarUno($notaria);
    if($resultf['estado']==1){
        $notaria_nom = $resultf['data']['tb_notaria_nombre'];
    }
    $oCreditogarveh->modificar_campo($cre_id, 'tb_notaria_id', $notaria, 'INT');
    //$notaria = 'VALDIVIA DEXTRE';
    //if($cre_not == 2) $notaria = 'VERA MENDEZ';
    //if($cre_not == 3) $notaria = 'SANTA CRUZ';
    //registramos el historial para el credito guardado
    $his = '<span>Crédito creado por: <b>'.$_SESSION['usuario_nom'].'</b>, enviado a la <b>NOTARIA '.$notaria_nom.'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCreditogarveh->registro_historial($cre_id, $his);
    $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);

    //$inicial = formato_moneda($_POST['txt_cre_valtas'] * $credito_tc) - formato_moneda($_POST['txt_veh_pre']);
    //$oCreditogarveh->registro_historial($cre_id, $his);
    if($credito_ini > 0){
      $his = '<span>El cliente dio una inicial de <b>'.formato_moneda($credito_ini).'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);
    }
    
    $line_det = '<b>'.$_SESSION['usuario_nom'].'</b> ha registrado un nuevo Crédito GARVEH de número: CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
    //$oLineaTiempo->insertar($_SESSION['usuario_id'], $line_det);
    $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det = $line_det;
    $oLineaTiempo->insertar();
    
    $cuotip_id = $_POST['cmb_cuotip_id']; //tipo de cuota 4 fija, 3 libre
    $fec_desembolso = $_POST['txt_cre_fecdes'];
    $fec_desem = $_POST['txt_cre_fecfac']; //fecha de desembolso
    $fec_prorra = $_POST['txt_cre_fecpro'];//fecha de prorrateo
    $cuosubper_id = $_POST['cmb_cuosubper_id']; //sub perio, semanal, quincena, mes

    $mon_id = $_POST['cmb_mon_id'];
    $C = moneda_mysql($_POST['txt_cre_preaco']);
    $i = moneda_mysql($_POST['txt_cre_int']);
    $n = ($cuot_tip == 4)? $_POST['txt_cre_numcuo'] : $_POST['txt_cre_numcuomax'];
    
    if($mon_id == 1)
      $valor_gps = formato_moneda($valor_gps * $cambio); // el gps tiene precio en dolares si el cronograma es en soles se genera el tipo de cambio
    
    $gps_mensual = 0;
    $seguro_mensual = 0;

    if(floatval($valor_seguro) > 0)
      $i += $valor_seguro;
    if(intval($estado_gps) <= 0)
      $valor_gps = 0;
    if(floatval($valor_gps) > 0 && $n > 0)
      $gps_mensual = formato_moneda($valor_gps / $n);

    //? ACTUALIZAMOS EL VALOR NUEVO DEL INTERÉS AL CRÉDITO
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_int', $i, 'INT');

    if($cuotip_id == 3) // si es 4 es fija, 3 es libre
      $n = 1; // si es libre solo será una cuota

    if($cuosubper_id == 1){
      $cols = 1;
      $name_per = "MENSUAL";
    }
    if($cuosubper_id == 2){
      $cols = 2;
      $name_per = "QUINCENAL";
    }
    if($cuosubper_id == 3){
      $cols = 4;
      $name_per = "SEMANAL";
    }
    //para la formula del prorrateo, se toma la fecha del prorrateo y se cuenta cuántos días tiene el mes de la fecha del prorrateo, ejem: prorrateo 10/12/2016, el mes 12 tiene 31 días, para saber eso usamos la funcion de mktime
    //formula prorrateo = (interes de la primera cuota / total días del mes prorrateo) * dias prorrateo
    list($dia,$mes,$anio) = explode('-',$fec_prorra);
    $dias_mes = date('t', mktime(0, 0, 0, $mes, 1, $anio));
    $fec_desem1 = new DateTime($fec_desembolso);
    $fec_prorra1 = new DateTime($fec_prorra);
    $dias_prorra = $fec_desem1->diff($fec_prorra1)->format("%a");

    $R = $C / $n;
    if($i != 0){
      $uno = $i / 100;
      $dos = $uno + 1;
      $tres = pow($dos,$n);
      $cuatroA = ($C * $uno) * $tres;
      $cuatroB = $tres - 1;
      $R = $cuatroA / $cuatroB;
      $r_sum = $R*$n;
    }

    list($day, $month, $year) = explode('-', $fec_desem);
    
    for ($j=1; $j <= $n; $j++){ 
      if($j>1){
        $C = $C - $amo;
        $int = $C*($i/100);
        $amo = $R - $int;
        if($cuotip_id == 3)
          $amo = 0;
      }
      else{
        $int = $C*($i/100);
        $amo = $R - $int;
        if($cuotip_id == 3)
          $amo = 0;
      }

      $seguro_mensual = $C*($valor_seguro/100);

      //fecha facturacion
      $month = $month + 1;
      if($month == '13'){
        $month = 1;
        $year = $year + 1;
      }

      $fecha=validar_fecha_facturacion($day,$month,$year);

      $int_pro = 0;
      if($j == 1){
        $int_pro = ($int / $dias_mes) * $dias_prorra;
        $int += $int_pro;
        // $monto_pro = $int_pro;
        // $pro_div_cuo = $int_pro / $n;
        // el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
        // $pro_div_subcuo = $int_pro / ($n * $cols);
      }

      $xac=1;
      $cretip_id=3;//credito tipo 1=menor 2=asive 3=garve, 4 hipo
      $est=1;
      $persubcuo=4;
      $l = 3;
      if($cuosubper_id == 2){
        $persubcuo = 2;
        $l = 2;
      }
      else{
        if($cuosubper_id == 1){
          $persubcuo = 1;
          $l = 0;
        }
      }

      $oCuota->credito_id = $cre_id;
      $oCuota->creditotipo_id = $cretip_id;
      $oCuota->moneda_id = $mon_id;
      $oCuota->cuota_num = $j;
      $oCuota->cuota_fec = fecha_mysql($fecha);
      $oCuota->cuota_cap = $C;
      $oCuota->cuota_amo = $amo;
      $oCuota->cuota_int = $int;
      $oCuota->cuota_cuo = ($R + $int_pro + $gps_mensual);
      $oCuota->cuota_pro = $int_pro;
      $oCuota->cuota_persubcuo = $persubcuo;
      $oCuota->cuota_est = $est;
      $oCuota->cuota_acupag = 0;
      $oCuota->cuota_interes = $i;
      $oCuota->cuota_preseguro = $seguro_mensual; 
      $oCuota->cuota_pregps = $gps_mensual;

      $result = $oCuota->insertar();
      if(intval($result['estado']) == 1){
        $cuo_id = $result['cuota_id'];
      }
      $result = null;
      
      for ($k=0; $k < $persubcuo; $k++) {
        list($day,$mon,$year) = explode('-',$fecha);
        $fecha_new=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*$l),$year));

        $oCuotadetalle->tb_cuota_id = $cuo_id;
        $oCuotadetalle->tb_moneda_id = $mon_id;
        $oCuotadetalle->tb_cuotadetalle_num = $k+1;
        $oCuotadetalle->tb_cuotadetalle_fec = fecha_mysql($fecha_new);
        $oCuotadetalle->tb_cuotadetalle_cuo = ($R / $persubcuo + $int_pro + $gps_mensual);
        $oCuotadetalle->tb_cuotadetalle_est = $est;
        $oCuotadetalle->tb_cuotadetalle_acupag = 0;

        $result = $oCuotadetalle->insertar();
        
        if($cuosubper_id == 2)
          $l = 0;
        else{
          if($cuosubper_id != 1)
            $l--;
        }
      }
    }

    $data['cre_id']=$cre_id;
    $data['cus']=$cus;
    $data['cre_msj']='Se registró correctamente:'.$n;
  }
  else
  {
    $data['cre_msj']='Intentelo nuevamente.';
  }
  echo json_encode($data);
}

if($_POST['action_credito']=="M"){
  if(!empty($_POST['hdd_cre_id'])){
    $ver = $oCreditogarveh->mostrarUno($_POST['hdd_cre_id']);
    if($ver['estado']==1){
        $estado = $ver['data']['tb_credito_est'];
    }

    if($estado<2){
    $est=1;
    $mon_id=intval($_POST['cmb_mon_id']);
    $cre_id = intval($_POST['hdd_cre_id']);
    $cuot_tip = intval($_POST['cmb_cuotip_id']);

    $hdd_tipo = intval($_POST['hdd_credito_tip']);
    if($hdd_tipo == 2)
      $cre_tip = 2;
    else
      $cre_tip = intval($_POST['cmb_credito_tip']);
    
    $cre_valtas = moneda_mysql($_POST['txt_cre_valtas']);
    $cre_valrea = moneda_mysql($_POST['txt_cre_valrea']);
    $cre_valgra = moneda_mysql($_POST['txt_cre_valgra']);
    $cre_vent = moneda_mysql($_POST['txt_cre_vent']);
    
    if($_POST['cmb_custodia_id'] == "2"){
      $gps = $_POST['cmb_gps_id'];
      $str = $_POST['cmb_str_id'];
      $soa = $_POST['cmb_soa_id'];
      $gas = $_POST['cmb_gas_id'];
      $gpspre = $_POST['txt_gps_pre'];
      $strpre = $_POST['txt_str_pre'];
      $soapre = $_POST['txt_soa_pre'];
      $gaspre = $_POST['txt_gas_pre'];
      //$cuot_tip = 4; //solo con cuota fija y no libre
    }else{
      $gps = "";
      $str = "";
      $soa = "";
      $gas = "";
      $gpspre = "";
      $strpre = "";
      $soapre = "";
      $gaspre = "";
    }

    $subper = $_POST['cmb_cuosubper_id'];
    if($_POST['cmb_cuotip_id'] == 3) //si es 3 solo será de tipo mensual
      $subper = 1;

    $repre_id = $_POST['cmb_rep_id']; //id del representante
    
    $oCreditogarveh->credito_id = $_POST['hdd_cre_id'];
    $oCreditogarveh->credito_usumod = $_SESSION['usuario_id'];
    $oCreditogarveh->credito_cus = intval($_POST['cmb_custodia_id']);
    $oCreditogarveh->cuotatipo_id = $cuot_tip;
    $oCreditogarveh->cuotasubperiodo_id = $subper;
    $oCreditogarveh->cliente_id = intval($_POST['hdd_cre_cli_id']);
    $oCreditogarveh->moneda_id = $mon_id;
    $oCreditogarveh->credito_tipcam = moneda_mysql($_POST['txt_cre_tipcam']);
    $oCreditogarveh->representante_id = intval($_POST['cmb_rep_id']);
    $oCreditogarveh->cuentadeposito_id = intval($_POST['cmb_cuedep_id']);
    $oCreditogarveh->vehiculomarca_id = intval($_POST['cmb_vehmar_id_2']);
    $oCreditogarveh->vehiculomodelo_id = intval($_POST['cmb_vehmod_id_2']);
    $oCreditogarveh->vehiculoclase_id = intval($_POST['cmb_vehcla_id_2']);
    $oCreditogarveh->vehiculotipo_id = intval($_POST['cmb_vehtip_id_2']);
    $oCreditogarveh->credito_vehpla = mb_strtoupper($_POST['txt_veh_pla'], 'UTF-8');
    $oCreditogarveh->credito_vehsermot = mb_strtoupper($_POST['txt_veh_mot'], 'UTF-8');
    $oCreditogarveh->credito_vehsercha = mb_strtoupper($_POST['txt_veh_cha'], 'UTF-8');
    $oCreditogarveh->credito_vehano = intval($_POST['txt_veh_ano']);
    $oCreditogarveh->credito_vehcol = mb_strtoupper($_POST['txt_veh_col'], 'UTF-8');
    $oCreditogarveh->credito_vehnumpas = intval($_POST['txt_veh_numpas']);
    $oCreditogarveh->credito_vehnumasi = intval($_POST['txt_veh_numasi']);
    $oCreditogarveh->credito_vehkil = intval($_POST['txt_veh_kil']);
    $oCreditogarveh->credito_vehest = $_POST['txt_veh_est'];
    $oCreditogarveh->credito_vehent = $_POST['txt_veh_ent'];
    $oCreditogarveh->credito_gps = $gps;
    $oCreditogarveh->credito_gpspre = moneda_mysql($gpspre);
    $oCreditogarveh->credito_str = $str;
    $oCreditogarveh->credito_strpre = moneda_mysql($strpre);
    $oCreditogarveh->credito_soa = $soa;
    $oCreditogarveh->credito_soapre = moneda_mysql($soapre);
    $oCreditogarveh->credito_gas = $gas;
    $oCreditogarveh->credito_gaspre = moneda_mysql($gaspre);
    $oCreditogarveh->credito_otrpre = moneda_mysql($_POST['txt_otr_pre']);
    $oCreditogarveh->credito_vehpre = moneda_mysql($_POST['txt_veh_pre']);
    $oCreditogarveh->credito_ini = moneda_mysql($_POST['txt_cre_ini']);
    $oCreditogarveh->credito_preaco = moneda_mysql($_POST['txt_cre_preaco']);
    $oCreditogarveh->credito_int = moneda_mysql($_POST['txt_cre_int']);
    $oCreditogarveh->credito_numcuo = $_POST['txt_cre_numcuo'];
    $oCreditogarveh->credito_linapr = moneda_mysql($_POST['txt_cre_linapr']);
    $oCreditogarveh->credito_numcuomax = $_POST['txt_cre_numcuomax'];
    $oCreditogarveh->credito_feccre = fecha_mysql($_POST['txt_cre_feccre']);
    $oCreditogarveh->credito_fecdes = fecha_mysql($_POST['txt_cre_fecdes']);
    $oCreditogarveh->credito_fecfac = fecha_mysql($_POST['txt_cre_fecfac']);
    $oCreditogarveh->credito_fecpro = fecha_mysql($_POST['txt_cre_fecpro']);
    $oCreditogarveh->credito_fecent = fecha_mysql($_POST['txt_cre_fecent']);
    $oCreditogarveh->credito_pla = intval($pla);
    $oCreditogarveh->credito_comdes = $_POST['txt_cre_comdes'];
    $oCreditogarveh->credito_obs = $_POST['txt_cre_obs'];
    $oCreditogarveh->credito_webref = intval($webref);
    $oCreditogarveh->credito_parele = $_POST['txt_veh_parele'];
    $oCreditogarveh->credito_solreg = $_POST['txt_veh_solreg'];
    $oCreditogarveh->credito_fecsol = fecha_mysql($_POST['txt_veh_fecsol']);
    $oCreditogarveh->credito_tipus = '';
    $oCreditogarveh->credito_carg = $_POST['txt_cre_carg'];
    $oCreditogarveh->credito_fecgps = fecha_mysql($_POST['txt_cre_fecgps']);
    $oCreditogarveh->credito_fecstr = fecha_mysql($_POST['txt_cre_fecstr']);
    $oCreditogarveh->credito_fecsoat = fecha_mysql($_POST['txt_cre_fecsoat']);
    $oCreditogarveh->credito_fecvenimp = fecha_mysql($_POST['txt_cre_fecvenimp']);
    $oCreditogarveh->credito_tip = $cre_tip;
    $oCreditogarveh->credito_valtas = $cre_valtas;
    $oCreditogarveh->credito_valrea = $cre_valrea;
    $oCreditogarveh->credito_valgra = $cre_valgra;
    $oCreditogarveh->credito_vehmode = $_POST['txt_veh_mode'];
    $oCreditogarveh->credito_vehcate = $_POST['txt_veh_cate'];
    $oCreditogarveh->credito_vehcomb = $_POST['txt_veh_comb'];
    $oCreditogarveh->credito_vent = $cre_vent;
    
    $dts = $oCreditogarveh->modificar();

    /*
    El credito tiene nuevos cambios, ahora hay 2 formas más, se considera como TRANSFERENCIA, COMPRA VENTA, Y FORMA REGULAR
    VAMOS A GUARDAR:
    1. GUARDAMOS LA SUBA GARANTIA: 1 REGULAR, 2 TRANSFERENCIA, 3 COMPRA VENTA
    1. SI EL CREDITO ES TRANSFERENCIA: GUARDAMOS EL ID DEL TRANSFERENTE
    2. SI EL CREDITO ES COMPRA VENTA SOLO GUARDAMOS INICIAL
    3. PARA AMBOS CASOS REGISTRAMOS LA INICIAL, PARA ELLO GUARDAMOS: MONTO DE INICIAL, ID DE LA TABLA (INGRESO, DEPOSITO), TIPO DE INICIAL (EFECTIVO O DEPOSITO)
    */

    $credito_subgar = intval($_POST['cmb_credito_subgar']);
    $credito_ini = moneda_mysql($_POST['txt_credito_ini']); //monto incial
    $credito_tipini = intval($_POST['hdd_credito_tipini']); //tipo de inciial: efectivo, deposito (tabla ingreso, deposito)
    $credito_idini = intval($_POST['hdd_credito_idini']); //id de la tabla donde está la inicial, ingreso, deposito
    $transferente_id = intval($_POST['hdd_transferente_id']);
    $credito_tc = $_POST['txt_cre_tc'];
    
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_tc', $credito_tc,'STR');
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_subgar', $credito_subgar,'STR');
    if($credito_subgar == 2 || $credito_subgar == 3 || $credito_subgar == 4){ //subgarantia de transferencia
      $oCreditogarveh->modificar_campo($cre_id, 'tb_transferente_id', $transferente_id,'INT');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_ini', $credito_ini,'STR');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_idini', $credito_idini,'INT');
      $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_tipini', $credito_tipini,'INT');
    }

    $zona_id = intval($_POST['cmb_zona_id']);
    $oCreditogarveh->modificar_campo($cre_id, 'tb_zonaregistral_id', $zona_id,'INT');
    $notaria = intval($_POST['cmb_credito_not']);
    $resultf = $oNotaria->mostrarUno($notaria);
    if($resultf['estado']==1){
        $notaria_nom = $resultf['data']['tb_notaria_nombre'];
    }
    //$notaria = 'VALDIVIA DEXTRE';
    //if($cre_not == 2) $notaria = 'VERA MENDEZ';
    //if($cre_not == 3) $notaria = 'SANTA CRUZ';
    $oCreditogarveh->modificar_campo($cre_id, 'tb_notaria_id', $notaria, 'INT');
    $data['cre_not'] = $notaria_nom;
    
    //registramos el historial para el credito editado
    $his = '<span style="color: brown;">Crédito editado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCreditogarveh->registro_historial($cre_id, $his);
    $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);

    $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha editado los datos del Crédito GARVEH de número: CGV-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
    $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det = $line_det;
    $oLineaTiempo->insertar();

    $cli_id_ori = $_POST['hdd_cre_cli_id_ori']; //este es el id del cliente original
    $cli_id_nuevo = $_POST['hdd_cre_cli_id']; //esete id sería diferente al original si se cambia de cliente
    
    if($cli_id_nuevo != $cli_id_ori){
      $dts = $oCliente->mostrarUno($cli_id_ori); //datos del cliente original
        if(intval($dts['estado']) == 1){
            $cli_ori_nom = $dts['data']['tb_cliente_nom'].' '.$dts['data']['tb_cliente_ape'];
        }
        $dts = null;

      $oClientecaja->cambio_de_cliente($cli_id_ori,$cli_id_nuevo);
      $oCuotapago->cambio_de_cliente($cli_id_ori,$cli_id_nuevo);
      $oIngreso->cambio_de_cliente($cli_id_ori,$cli_id_nuevo);

      //registramos el historial para el cambio de cliente
      $his = '<span style="color: blue;">Crédito editado por: <b>'.$_SESSION['usuario_nom'].'</b>. Anteriormente el crédito pertenecía al señor(a): <b>'.$cli_ori_nom.'</b>, ahora pasa a nombre del señor(a): <b>'.$_POST['txt_ven_cli_nom'].'</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      //$oCreditogarveh->registro_historial($cre_id, $his);
      $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);
    }

    $data['cre_id'] = $cre_id;
    $data['cre_msj'] = 'Se modificó correctamente. '. $notaria;
    }
    else{
        $data['cre_id'] = 0;
        $data['cre_msj'] =' NO SE PUEDE MODIFICAR UN CREDITO APROBADO';
    }
  }
  else{
    $data['cre_id'] = 0;
    $data['cre_msj'] =' Intentelo nuevamente.';
  }
  

  echo json_encode($data);
}

if($_POST['action']=="historial"){

  $result = $oCreditogarveh->mostrarUno($_POST['cre_id']);

  if($result['estado']==1){
      $his = $result['data']['tb_credito_his'];
  }
  $result=null;
  echo'<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditogarveh_historial">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                      </button>
                      <h4 class="modal-title" style="font-family: cambria;"><b>Historial de Garantía Vehicular</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12" style="font-family: cambria">
                                            ' . $his . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
            </div>';
}
if($_POST['action']=="eliminar"){
  if(!empty($_POST['cre_id']))
  {

    $proceso = $oProceso->mostrarProcesoXCredito($_POST['cre_id']);

    $result = $oCreditogarveh->modificar_campo($_POST['cre_id'],'tb_credito_xac',0,'INT');
    if($result){

        /* GERSON (21-10-23) */
        if ($proceso['estado'] == 1) {
          $oProceso->modificar_campo_proceso($proceso['data']['tb_proceso_id'], 'tb_credito_id', NULL, 'INT');
          //$oProceso->modificar_campo_proceso($proceso['data']['tb_proceso_id'], 'tb_proceso_fecreg', $proceso['data']['tb_proceso_fecreg'], 'STR'); // existe un pequeño error que al modificar cualquier campo se modifica tmb el fecreg sin razon
        }
        /*  */
        $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha eliminado el Crédito GARVEH de número: CGV-'.str_pad($_POST['cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
        $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
        $oLineaTiempo->tb_lineatiempo_det = $line_det;
        $result2 = $oLineaTiempo->insertar();
        echo $result2['estado'];
    }
  }
  else
  {
    echo 'Intentelo nuevamente.';
  }
}
if($_POST['action']=="asesor"){
  $asesor_id = $_POST['asesor_id'];
  $cre_id = $_POST['cre_id'];

  $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_usureg', $asesor_id,'INT');
  if($oCreditogarveh){
     echo 1; 
  }
}
if($_POST['action']=="placa"){
  $credito_id = intval($_POST['hdd_credito_id']);
  $veh_pla = $_POST['txt_veh_pla'];
  $veh_parele = $_POST['txt_veh_parele'];
  $veh_solreg = $_POST['txt_veh_solreg'];
  $veh_fecsol = fecha_mysql($_POST['txt_veh_fecsol']);

  $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_vehpla', $veh_pla, 'STR');
  $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_parele', $veh_parele, 'STR');
  $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_solreg', $veh_solreg, 'STR');
  $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_fecsol', $veh_fecsol, 'STR');

  $his = '<span>Crédito modificado para agregar Placa y datos de Boletín, por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';
  //$oCreditogarveh->registro_historial($credito_id, $his);
  $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $his);
  echo 'exito';
}

if($_POST['action']=="verificacion_titulo"){
  $credito_id = intval($_POST['hdd_credito_id']);
  $obs = $_POST['txt_veri_observaciones'];
  $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_vertit', 1, 'INT'); //verificacion completa

  $his = '<span>El colaborador: <b>'.$_SESSION['usuario_nom'].'</b> indica que los datos del Crédito están correctos. '.trim($obs).' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
  //$oCreditogarveh->registro_historial($credito_id, $his);
  $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $his);

  $fecha_verificacion = date("Y-m-d H:i:s");
  $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_fecveri', $fecha_verificacion, 'STR'); //fecha de verificacion

  echo "exito";
}
if($_POST['action']=="documentos"){
  $credito_id = intval($_POST['credito_id']);

  // Validamos si existe un nro. titulo registrado para dicho credito
  $resultado01 = $oCreditogarveh->validar_registro_nrotitulo($credito_id);
  $resultado02 = $oCreditogarveh->validar_registro_ep($credito_id);

  if(intval($resultado01['estado']) == 0)
  {
    echo 'Proceso detenido, no se pudo actualizar el crédito a vigente, porque aun falta registrar el número de título';
    exit();
  }
  elseif($resultado02['data']['tb_credito_numescri'] == NULL || ! isset($resultado02['data']))
  {
    echo 'Proceso detenido, no se pudo actualizar el crédito a vigente, porque aun falta registrar el número de escritura pública';
    exit();
  }
  else
  {
    $oCreditogarveh->modificar_campo($credito_id, 'tb_credito_est', 3, 'INT'); //cuando los documentos están completos pasa A N° TITULO
    $oCreditogarveh->modificar_campo($credito_id,'tb_credito_fecvig', date('Y-m-d'), 'STR');
    $his = '<span>El colaborador: <b>'.$_SESSION['usuario_nom'].'</b> indica que los documentos del Crédito están completos. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCreditogarveh->registro_historial($credito_id, $his);
    $oCreditolinea->insertar($creditotipo_id, $credito_id, $usuario_id, $his);
  
    echo 'exito';
  }
}
if($_POST['action']=="aprobar")
{
  if(!empty($_POST['cre_id']))
  {
      $est=2;
      $oCreditogarveh->aprobar($_POST['cre_id'],$_SESSION['usuario_id'],$est);

      //registramos el historial para el credito
      $his = '<span style="color: #03959D;">Crédito aprobado por: <b>'.$_SESSION['usuario_nom'].'</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      //$oCreditogarveh->registro_historial($_POST['cre_id'], $his);
      $oCreditolinea->insertar($creditotipo_id, $_POST['cre_id'], $usuario_id, $his);

      $line_det = '<b>'.$_SESSION['usuario_nom'].$_SESSION['usuario_ape'].'</b> ha aprobado el Crédito GARVEH de número: CGV-'.str_pad($_POST['cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
      $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
      $oLineaTiempo->tb_lineatiempo_det = $line_det;
      $oLineaTiempo->insertar();

      $data['estado'] = 1;
      $data['credito_id'] = $_POST['cre_id'];
      $data['msj'] = 'exito';
      $data['custodia'] = $_POST['custodia'];
      echo json_encode($data);
    //}
  }
  else
  {
    $data['estado'] = 0;
    $data['credito_id'] = 0;
    $data['msj'] = 'error';
    echo json_encode($data);
  }
}

//paralizarrr el creditooo
if($_POST['action']=="paralizar"){
  parse_str($_POST["formData"], $datos);

  if(!empty($datos['hdd_cre_id'])){
    $est = 4;
    $oCreditogarveh->aprobar($datos['hdd_cre_id'],$_SESSION['usuario_id'],$est);

    //registramos el historial para el credito
    $his = '<span style="color: #024E0C;">Crédito Paralizado por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: '.$datos['txt_cre_det'].' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCreditogarveh->registro_historial($datos['hdd_cre_id'], $his);
    $oCreditolinea->insertar($creditotipo_id, $datos['hdd_cre_id'], $usuario_id, $his);

    $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha paralizado el Crédito GARVEH de número: CGV-'.str_pad($datos['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
    $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det = $line_det;
    $oLineaTiempo->insertar();

    //guardamos finalidad de la paralización
    $hoy = date('d-m-Y');
    $oCreditogarveh->detalle_finalidad($datos['hdd_cre_id'], $datos['txt_cre_det'], fecha_mysql($hoy));
    
    echo 'Se ha paralizado el crédito.';
  }
  else{
    echo 'Intentelo nuevamente.';
  }
}

if($_POST['action']=="resolver"){
  parse_str($_POST["formData"], $datos);
  
  if(!empty($datos['hdd_cre_id'])){
    $nombre = $_FILES['archivo']['name'];
    $tipo = trim($_FILES['archivo']['type']);
    $tamanio = intval($_FILES['archivo']['size']);
    $ruta = $_FILES['archivo']['tmp_name'];
    $destino = str_replace(' ', '', '../../public/pdf/creditogarveh/'.$datos['hdd_cre_id'].'-'.$nombre);
    

    $nombre_img = $_FILES['imagen']['name'];
    $tipo_img = $_FILES['imagen']['type'];
    $tamanio_img = $_FILES['imagen']['size'];
    $ruta_img = $_FILES['imagen']['tmp_name'];
    $destino_img = str_replace(' ', '', '../../public/images/creditogarveh/'.$datos['hdd_cre_id'].'-'.$nombre_img);

    /////echo $nombre_img.' // tipo: '.$tipo_img.' // tam: '.$tamanio_img.' // ruta: '.$ruta_img; exit();
    $arr_files = array('image/png', 'image/jpeg', 'image/jpg');

    $bandera = false;
    //echo $nombre.' // tipo: '.$tipo.' // tam: '.$tamanio.' // ruta: '.$ruta;
    if($tipo === 'application/pdf' || $tamanio < 2097152){
        //echo '$nombre='.$nombre.' $tipo='.$tipo.' $tamanio='.$tamanio.' $ruta='.$ruta.' $destino='.$destino. 'gettype($tamanio)='.gettype($tamanio).' gettype($tipo)='.gettype($tipo); exit();
      if(file_exists($destino))
        unlink($destino);
      if(move_uploaded_file($ruta, $destino))
        $bandera = true;
    }
    
    if($nombre_img != ""){
      if(in_array($tipo_img, $arr_files) && $tamanio_img <= 2097152){
        if(file_exists($destino_img)) 
          unlink($destino_img);
        if(move_uploaded_file($ruta_img, $destino_img))
          $bandera = true;
      }
    }

    if($bandera){

      $est=8;
      $oCreditogarveh->aprobar($datos['hdd_cre_id'],$_SESSION['usuario_id'],$est);

      $monto_resuelto = $datos['hdd_mon_result']; //el credito se resuelve con este monto capital
      $oCreditogarveh->modificar_campo($datos['hdd_cre_id'],'tb_credito_monres', moneda_mysql($monto_resuelto),'STR');

      $fec_resol = date('d-m-Y'); //fecha de resolución del credito
      $oCreditogarveh->modificar_campo($datos['hdd_cre_id'],'tb_credito_fecresol', fecha_mysql($fec_resol),'STR');
      
      //listamos las adendas del crédipo para resolverlas
      $result = $oCreditogarveh->listar_adendas_credito($datos['hdd_cre_id']);
      if($result['estado']==1){
          foreach ($result['data'] as $key => $value) {
            $aden_id = $value['tb_credito_id'];
            $est=8;
            $oCreditogarveh->aprobar($aden_id, $_SESSION['usuario_id'], $est);

            //registramos el historial para el credito
            $his = '<span style="color: #F56B02;">Adenda Resuelta por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: ha sido resuelta ya que su saldo capital se ha sumado al crédito matríz resuelto, crédito ID. CGV-'.$datos['hdd_cre_id'].' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
            //$oCreditogarveh->registro_historial($aden_id, $his);
            $oCreditolinea->insertar($creditotipo_id, $aden_id, $usuario_id, $his);

            $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha resuelto una Adenda del Crédito GARVEH de número: CGV-'.str_pad($datos['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'. La Adenda tiene como número: A-CGV-'.str_pad($aden_id, 4, "0", STR_PAD_LEFT).'. && <b>'.date('d-m-Y h:i a').'</b>';
            $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
            $oLineaTiempo->tb_lineatiempo_det = $line_det;
            $oLineaTiempo->insertar();
          }
      }
      $result = null;

      $moneda = $datos['hdd_moneda']; // en soles o dorales S/. US$

      //registramos el historial para el credito
      $his = '<span style="color: #F56B02;">Crédito Resuelto por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: '.$datos['txt_cre_det'].', se rosolvió con un monto capital de: '.$moneda.' '.formato_moneda($monto_resuelto).'. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      //$oCreditogarveh->registro_historial($datos['hdd_cre_id'], $his);
      $oCreditolinea->insertar($creditotipo_id, $datos['hdd_cre_id'], $usuario_id, $his);

      $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha resuelto el Crédito GARVEH de número: CGV-'.str_pad($datos['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
      $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
      $oLineaTiempo->tb_lineatiempo_det = $line_det;
      $oLineaTiempo->insertar();

      //guardamos el link del archivo pdf
      if($nombre != ""){
        $his = '<span>Se adjuntó el documento de Resolución.| <b>'.date('d-m-Y h:i a').'</b></span><br>';
        $oCreditogarveh->guardar_link_pdf($datos['hdd_cre_id'], $destino, $his);
      }

      //guaramos la imagen si es que se ha subido
      if($nombre_img != ""){
        $his = '<span>Se adjuntó una imagen para la Resolución.| <b>'.date('d-m-Y h:i a').'</b></span><br>';
        $oCreditogarveh->modificar_campo($datos['hdd_cre_id'],'tb_credito_img', $destino_img,'STR');
        //$oCreditogarveh->registro_historial($datos['hdd_cre_id'], $his);
        $oCreditolinea->insertar($creditotipo_id, $datos['hdd_cre_id'], $usuario_id, $his);
      }

      $cre_tip = $datos['hdd_cre_tip'];

      if($cre_tip == 3){
        $result = $oAcuerdopago->mostrar_por_creditonuevo($datos['hdd_cre_id'], 3); //pametro 3 ya que es tipo credito garveh
        if($result['estado']==1){
          foreach ($result['data'] as $key => $value) {
            $acu_id = $value['tb_acuerdopago_id'];
            $cre_id_ori = $value['tb_credito_id'];
          }
        }
        $result = null;

        $oAcuerdopago->resolver_acuerdopago($_SESSION['usuario_id'], $acu_id, 2); //2 es resuelto en acuerdo de pago
      }

      echo 'exito';
    }
    else
      echo 'Suba un PDF que no exceda 2MB o una imagen para poder resolver: '.$tipo.' '.$tamanio;
  }
  else{
    echo 'Intentelo nuevamente.';
  }
}

if($_POST['action']=="vigente"){
  parse_str($_POST["formData"], $datos);
  
  if(!empty($datos['hdd_cre_id'])){
      $cre_tip1 = $datos['hdd_cre_tip'];
      $est=3; //estado 3, regresa a desembolsado
      if($cre_tip1 != 3){
        $oCreditogarveh->aprobar($datos['hdd_cre_id'],$_SESSION['usuario_id'],$est);
        //registramos el historial para el credito
        $his = '<span style="color: #06B5B8;">Crédito regresado a vigente por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: '.$datos['txt_cre_det'].' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
        //$oCreditogarveh->registro_historial($datos['hdd_cre_id'], $his);
        $oCreditolinea->insertar($creditotipo_id, $datos['hdd_cre_id'], $usuario_id, $his);

        $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha regresado a estado vigente al Crédito GARVEH de número: CGV-'.str_pad($datos['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';

        $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
        $oLineaTiempo->tb_lineatiempo_det = $line_det;
        $oLineaTiempo->insertar();
        echo 'En vigente correctamente.';
      }
      //REGRESAR AL ESTADO ANTERIOR AL ACUERDO DE PAGO
      if($cre_tip1 == 3){
        //verificamos que no tenga un acuerdopago activo, si hay uno entonces no se podrá activar otro, este credito es el nuevo, sacamos el original
        $result = $oAcuerdopago->mostrar_por_creditonuevo($datos['hdd_cre_id'], 3); //pametro 3 ya que es tipo credito garveh
          if($result['estado']==1){
            foreach ($result['data'] as $key => $value) {
                $acu_id = $value['tb_acuerdopago_id'];
                $cre_id_ori = $value['tb_credito_id'];
            }
          }
        $result = null;

        $result = $oAcuerdopago->listar_acuerdopago_credito_xac($cre_id_ori, 3, 0); //pametro 3 ya que es tipo credito garveh y cero AP activo
          if($result['estado']==1){
              $num_activos = count($result['data']);
          }
        $result = null;

        if($num_activos == 0){
          $oCreditogarveh->aprobar($datos['hdd_cre_id'],$_SESSION['usuario_id'],$est);

          //registramos el historial para el credito
          $his = '<span style="color: #06B5B8;">Crédito regresado a vigente por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: '.$datos['txt_cre_det'].' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
          //$oCreditogarveh->registro_historial($datos['hdd_cre_id'], $his);
          $oCreditolinea->insertar($creditotipo_id, $datos['hdd_cre_id'], $usuario_id, $his);

          $oAcuerdopago->resolver_acuerdopago($_SESSION['usuario_id'], $acu_id, 0); //0 regresa a activo el AP
          echo 'En vigente correctamente.';
        }
        else
          echo 'Ya hay otro AP en estado Vigente.';
      }
  }
  else{
    echo 'Intentelo nuevamente.';
  }
}
//daniel odar 17-09-24
if($_POST['action']=="modificar_campo"){
  $tabla = $_POST['tabla'];
  $tabla_id = $_POST['tabla_id'];
  $columna = $_POST['columna'];
  $valor = $_POST['valor'];
  $tipo_dato = $_POST['tipo_dato'];

  $data['estado'] = 0;
  $data['mensaje'] = "Error al actualizar";

  if ($tabla == 'tb_creditogarveh') {
    $result = $oCreditogarveh->modificar_campo($tabla_id, $columna, $valor, $tipo_dato);

    if ($result == 1) {
      $data['estado'] = 1;
      $data['mensaje'] = "Se actualizó el campo";

      if ($columna == 'tb_credito_suministro') {
        $data['num_suministro'] = $valor;
      }
    }
  }

  echo json_encode($data);
}
?>
