<?php
require_once('../../core/usuario_sesion.php');
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();

$cuotip_id = $_POST['cuotip_id'];
$C = moneda_mysql($_POST['cre_preaco']);
$i = moneda_mysql($_POST['cre_int']);
$tip_ade = $_POST['tip_ade'];
$cre_id_ori = $_POST['cre_idori'];
$n = $_POST['cre_numcuo'];
$moneda = $_POST['mon_id'];
$tipo_cambio = floatval($_POST['tipo_cambio']);

$valor_seguro = floatval($_POST['valor_seguro']);
$valor_gps = $_POST['valor_gps'];
$estado_gps = $_POST['estado_gps'];

if($moneda == 1)
  $valor_gps = formato_moneda($valor_gps * $tipo_cambio); // el gps tiene precio en dolares si el cronograma es en soles se genera el tipo de cambio

$gps_mensual = 0;

if($valor_seguro > 0)
  $i += $valor_seguro;
if(intval($estado_gps) <= 0)
  $valor_gps = 0;
if(floatval($valor_gps) > 0 && $n > 0)
  $gps_mensual = formato_moneda($valor_gps / $n);

if ($tip_ade == 4)
  $n = $_POST['cre_numcuo_res']; // si es CUOTA BALON, se suma todas las cuotas por pagar para calcular el interes y generar una sola cuota

if ($cuotip_id == 3) // si es 4 es fija, 3 es libre
  $n = 1; // si es libre solo será una cuota

//parametro 2 ya que debe buscar las cuotas de un credito ASIVEH
if ($tip_ade == 4) {
  $dts = $oCuota->ultima_cuota_por_credito($cre_id_ori, 3);
  if ($dts['estado'] == 1) {
    $cuo_fec = mostrar_fecha($dts['data']['tb_cuota_fec']);
  }
  $dts = null;
}

$fec_desem = $_POST['cre_fecdes']; //fecha de desembolso
$fec_prorra = $_POST['cre_fecpro']; //fecha de prorrateo
$cuosubper_id = $_POST['cuosubper_id']; //sub perio, semanal, quincena, mes
$cols = 1;
if ($cuosubper_id == 1) {
  $cols = 1;
  $name_per = "MENSUAL";
}
if ($cuosubper_id == 2) {
  $cols = 2;
  $name_per = "QUINCENAL";
}
if ($cuosubper_id == 3) {
  $cols = 4;
  $name_per = "SEMANAL";
}
//para la formula del prorrateo, se toma la fecha del prorrateo y se cuenta cuántos días tiene el mes de la fecha del prorrateo, ejem: prorrateo 10/12/2016, el mes 12 tiene 31 días, para saber eso usamos la funcion de mktime
//formula prorrateo = (interes de la primera cuota / total días del mes prorrateo) * dias prorrateo
list($dia, $mes, $anio) = explode('-', $fec_prorra);
$dias_mes = date('t', mktime(0, 0, 0, $mes, 1, $anio));
$fec_desem1 = new DateTime($fec_desem);
$fec_prorra1 = new DateTime($fec_prorra);
$dias_prorra = $fec_desem1->diff($fec_prorra1)->format("%a");

if (intval($n) <= 0)
  $n = 1;

$R = $C / $n;
if ($i != 0) {
  $uno = $i / 100;
  $dos = $uno + 1;
  $tres = pow($dos, $n);
  $cuatroA = ($C * $uno) * $tres;
  $cuatroB = $tres - 1;
  $R = $cuatroA / $cuatroB;
  $r_sum = $R * $n;
}
$fecha_factu = $_POST['cre_fecfac'];
list($day, $month, $year) = explode('-', $fecha_factu);


if (is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R) && $tip_ade != 4) {
?>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h4>Cronograma de Crédito <?php echo $R . ' | valor Seguro: ' . $valor_seguro . ' | valor gps: ' . $valor_gps.' | cambio: '.$tipo_cambio; ?></h4>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_credito_cronograma"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table style="width:100%" class="table table-bordered table-hover">
            <thead>
              <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                <th id="filacabecera">N°</th>
                <th id="filacabecera" colspan="<?php echo $cols ?>">FECHAS</th>
                <th id="filacabecera">CAPITAL</th>
                <th id="filacabecera">AMORTIZACIÓN</th>
                <th id="filacabecera">INTERÉS</th>
                <th id="filacabecera">PRORRATEO</th>
                <th id="filacabecera">CUOTA</th>
                <th id="filacabecera">GPS</th>
                <th id="filacabecera"><?php echo $name_per; ?></th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($moneda == '1') {
                $moneda = 'S/. ';
              } else {
                $moneda = 'US$. ';
              }
              for ($j = 1; $j <= $n; $j++) {
                if ($j > 1) {
                  $C = $C - $amo;

                  if ($i > 0)
                    $int = $C * ($i / 100);
                  else
                    $int = 0;

                  $amo = $R - $int;
                  if ($cuotip_id == 3)
                    $amo = 0;
                } else {
                  if ($i > 0)
                    $int = $C * ($i / 100);
                  else
                    $int = 0;

                  $amo = $R - $int;
                  if ($cuotip_id == 3)
                    $amo = 0;
                }
                //fecha facturacion
                $month = $month + 1;
                if ($month == '13') {
                  $month = 1;
                  $year = $year + 1;
                }
                $fecha = validar_fecha_facturacion($day, $month, $year);
                if ($j == 1) {
                  if ($int > 0) {
                    $int_pro = ($int / $dias_mes) * $dias_prorra;
                    $pro_div_cuo = $int_pro / $n;
                    //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                    $pro_div_subcuo = $int_pro / ($n * $cols);
                  } else {
                    $int_pro = 0;
                    $pro_div_cuo = 0;
                    //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                    $pro_div_subcuo = 0;
                  }
                } else
                  $int_pro = 0;

                $valor_cuota = formato_moneda($R / $cols);
                
              ?>
                <tr class="filaTabla">
                  <td id="fila">
                    <?php echo $j; ?>
                  </td>
                  <?php
                  list($day, $mon, $year) = explode('-', $fecha);
                  if ($cuosubper_id == 3) {
                    $fecha_1 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 3), $year));
                    $fecha_2 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 2), $year));
                    $fecha_3 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 1), $year));
                    $fecha_4 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 0), $year));
                    echo '<td align="center">' . $fecha_1 . '</td>
                                <td align="center">' . $fecha_2 . '</td>
                                <td align="center">' . $fecha_3 . '</td>
                                <td align="center"><strong>' . $fecha_4 . '</strong></td>';
                  }
                  if ($cuosubper_id == 2) {
                    $fecha_1 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 2), $year));
                    $fecha_2 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 0), $year));
                    echo '<td align="center">' . $fecha_1 . '</td>
                                <td align="center"><strong>' . $fecha_2 . '</strong></td>';
                  }
                  if ($cuosubper_id == 1) {
                    $fecha_1 = date('d-m-Y', mktime(0, 0, 0, $mon, $day - (7 * 0), $year));
                    echo '<td align="center"><strong>' . $fecha_1 . '</strong></td>';
                  }
                  ?>
                  <td id="fila">
                    <?php echo $moneda . formato_moneda($C); ?>
                  </td>
                  <td id="fila">
                    <?php echo $moneda . formato_moneda($amo); ?>
                  </td>
                  <td id="fila">
                    <?php echo $moneda . formato_moneda($int); ?>
                  </td>
                  <td id="fila">
                    <?php echo '<input type="hidden" id="hdd_mon_pro_' . $j . '" value="' . formato_moneda($pro_div_cuo) . '">' ?>
                    <?php if ($j == 1) {
                      echo $moneda . formato_moneda($int_pro);
                    } else {
                      echo $moneda . '0.00';
                    }; ?>
                  </td>
                  <td id="fila">
                    <?php if ($j == 1) {
                      echo $moneda . formato_moneda($R + $int_pro);
                    } else {
                      echo $moneda . formato_moneda($R);
                    }; ?>
                  </td>
                  <td id="fila">
                    <?php echo $gps_mensual;?>
                  </td>
                  <td id="fila">
                    <?php if ($j == 1) {
                      echo $moneda . formato_moneda($R / $cols + $int_pro + $gps_mensual);
                    } else {
                      echo $moneda . formato_moneda($R / $cols + $gps_mensual);
                    }; ?>
                  </td>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php
}
if (is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R) && $tip_ade == 4) {
?>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h4>Cronograma de Crédito 2222<?php echo $R; ?></h4>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_credito_cronograma"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table style="width:100%" class="table table-bordered table-hover">
            <thead>
              <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                <th align="center">N°</th>
                <th align="center" colspan="<?php echo $cols ?>">FECHAS</th>
                <th align="center">CAPITAL</th>
                <th align="center">AMORTIZACIÓN</th>
                <th align="center">INTERÉS</th>
                <th align="center">PRORRATEO</th>
                <th align="center">CUOTA</th>
                <th align="center"><?php echo $name_per; ?></th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($moneda == '1') {
                $moneda = 'S/. ';
              } else {
                $moneda = 'US$. ';
              }
              $sum_int = 0;
              $sum_amo = 0;
              for ($j = 1; $j <= $n; $j++) {
                if ($j > 1) {
                  $C = $C - $amo;

                  if ($i > 0)
                    $int = $C * ($i / 100);
                  else
                    $int = 0;

                  $amo = $R - $int;
                } else {
                  if ($i > 0)
                    $int = $C * ($i / 100);
                  else
                    $int = 0;
                  $amo = $R - $int;
                }
                //fecha facturacion
                $month = $month + 1;
                if ($month == '13') {
                  $month = 1;
                  $year = $year + 1;
                }
                $fecha = validar_fecha_facturacion($day, $month, $year);
                if ($j == 1) {
                  if ($int > 0) {
                    $int_pro = ($int / $dias_mes) * $dias_prorra;
                    $pro_div_cuo = $int_pro / $n;
                    //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                    $pro_div_subcuo = $int_pro / ($n * $cols);
                  } else {
                    $int_pro = 0;
                    $pro_div_cuo = 0;
                    //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                    $pro_div_subcuo = 0;
                  }
                } else
                  $int_pro = 0;

                $sum_int += $int;
                $sum_amo += $amo;
              }
              $cre_preaco = moneda_mysql($_POST['cre_preaco']);
              ?>
              <tr class="filaTabla">
                <td id="fila">1</td>
                <td id="fila"><?php echo $cuo_fec; ?></td>
                <td id="fila">
                  <?php echo $moneda . formato_moneda($cre_preaco); ?>
                </td>
                <td id="fila">
                  <?php echo $moneda . formato_moneda($sum_amo); ?>
                </td>
                <td id="fila">
                  <?php echo $moneda . formato_moneda($sum_int); ?>
                </td>
                <td id="fila">
                  <?php echo '<input type="hidden" id="hdd_mon_pro_1" value="0">' ?>
                  <?php echo $moneda . '0.00'; ?>
                </td>
                <td id="fila">
                  <?php echo $moneda . formato_moneda($cre_preaco + $sum_int); ?>
                </td>
                <td id="fila">
                  <?php echo $moneda . formato_moneda($cre_preaco + $sum_int); ?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>