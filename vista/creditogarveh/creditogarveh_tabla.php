<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}

require_once('Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();
require_once('../controlseguro/Poliza.class.php');
$oPoliza = new Poliza();
/*require_once('../caja/Caja.class.php');
  $oCaja = new Caja();
  require_once('../moneda/Moneda.class.php');
  $oMoneda = new Moneda();*/

$fecha1a = $_POST['fecha1'];
$fecha2a = $_POST['fecha2'];
$estado = $_POST['estado'];
$tipogarveh = $_POST['tipogarveh'];
$cliente_id = $_POST['cliente_id'];
$fecha1 = date("Y-m-d", strtotime($fecha1a));
$fecha2 = date("Y-m-d", strtotime($fecha2a));
$fecha_hoy = date('d-m-Y');

$tr = '';
//EJECUTA LA FUNCION 
$result = $oCreditogarveh->filtro_creditos2($fecha1, $fecha2, $estado, $tipogarveh, $cliente_id);
  //SI ENCONTRÓ RESULTADOS
  if ($result['estado'] == 1) {
    //RECORRE LOS RESULTADOS ALMACENADOS EN DATA Y CADA LLAVE LA ASIGNA A VALUE
    foreach ($result['data'] as $key => $value) {
      $cuo_tipo = ' FIJA';
      if ($value['tb_cuotatipo_id'] == 3) {
        $cuo_tipo = ' LIBRE';
      }
      if ($value['tb_credito_tip'] == 1) $tip = "GARVEH";
      if ($value['tb_credito_tip'] == 2) $tip = "ADENDA";
      if ($value['tb_credito_tip'] == 3) $tip = "ACUERDO PAGO";
      if ($value['tb_credito_tip'] == 4) $tip = "GAR MOBILIARIA";

      if ($value['tb_credito_tip2'] == 1) $tip2 = " CREDITO REGULAR";
      if ($value['tb_credito_tip2'] == 2) $tip2 = " CREDITO ESPECÍFICO";
      if ($value['tb_credito_tip2'] == 3) $tip2 = " REPROGRAMADO";
      if ($value['tb_credito_tip2'] == 4) $tip2 = " CUOTA BALON";
      if ($value['tb_credito_tip2'] == 5) $tip2 = " REFINANCIADO AMORTIZADO";
      if ($value['tb_credito_tip2'] == 6) $tip2 = " REFINANCIADO";

      if ($value['tb_cliente_tip'] == 1) {
        $cliente = $value['tb_cliente_nom'] . ' | ' . $value['tb_cliente_doc'];
      }
      if ($value['tb_cliente_tip'] == 2) {
        $cliente = $value['tb_cliente_emprs'] . ' | ' . $value['tb_cliente_empruc'];
      }

      if ($value['tb_moneda_id'] == 1) $moneda = "S/.";
      if ($value['tb_moneda_id'] == 2) $moneda = "US$";

      if ($value['tb_cuotatipo_id'] == 3) {
        $cuotas = $value['tb_credito_numcuomax'];
      } else {
        $cuotas = $value['tb_credito_numcuo'];
      }
      $aprobar =  '';
      $desembolsar =  '';
      $test =  '';
      if (/* $value['tb_credito_est'] == 9 && */ ($_SESSION['usuariogrupo_id'] == 1 || $_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6 || $_SESSION['usuariogrupo_id'] == 7)) {
        $test =  '<a class="btn btn-info" href="javascript:void(0)" onClick="credito_acciones(' . $value['tb_credito_id'] . ')">Seguimiento</a>';
      }

      if ($value['tb_credito_est'] == 1 && ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuarioperfil_id'] == 6) ) { // GERENCIA | SUPERVISOR
        //SE AGREGO UN NUEVO VALOR PARAMETRO DENTRO DE LA LLAMADA A LA FUNCION APROBAR
        $aprobar =  '<a class="btn btn-default" href="javascript:void(0)" onClick="credito_aprobar(' . $value['tb_credito_id'] . ',' . $value['tb_credito_cus'] . ')">Apr Legal</a>';
      }
      if ($value['tb_credito_est'] == 2) {
        if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 3) {
          $desembolsar =  '<a class="btn btn-default" href="javascript:void(0)" onClick="credito_desembolso_form(\'insertar\',' . $value['tb_credito_id'] . ')">Desembolsar</a>';
        }
      }
      $titulo = '';
      $documentos =  '';
      $placa =  '';
      $verificacion =  '';
      $eliminar = '';
      $comentar = '';
      $multiple = '';
      $refinanciar = '';
      $DatosPoliza_validar_placa = 0;
      $btnpoliza = '';

      if( $value['tb_credito_subgar'] == 'PRE-CONSTITUCION' )
      {
        $validationParam_tb_credito_subgar = 3;
        $validationParam_tb_credito_cus    = 0;
      }
      elseif( $value['tb_credito_subgar'] == 'REGULAR' ) // CONSTITUCION
      {
        $validationParam_tb_credito_subgar = 2;
        $validationParam_tb_credito_cus    = $value['tb_credito_cus'];
      }
      elseif( substr($value['tb_credito_subgar'],0,6) === 'GARMOB' )
      {
        $validationParam_tb_credito_subgar = $value['tb_credito_subgar'];
        $validationParam_tb_credito_cus    = -1;
      }
      else
      {
        $validationParam_tb_credito_subgar = 1;
        $validationParam_tb_credito_cus    = 1;
      }

      switch ($value['tb_credito_est']) {
          case 1:
            $validarestado = 'PENDIENTE';
            break;
          case 2:
            $validarestado = 'APROBADO';
            break;
          case 3:
            $validarestado = 'VIGENTE';
            break;
          case 4:
            $validarestado = 'PARALIZADO';
            break;
          case 5:
            $validarestado = 'REFINANCIADO AMORTIZADO';
            break;
          case 6:
            $validarestado = 'REFINANCIADO SIN AMORTIZADO';
            break;
          case 7:
            $validarestado = 'LIQUIDADO';
            break;
          case 8:
            $validarestado = 'RESUELTO';
            break;
          case 9:
            $validarestado = 'TITULO';
            break;
          case 10:
            $validarestado = 'DOCUMENTOS';
            break;
          default:
            $validarestado = 'SIN ESTADO, REPORTELO';
            break;
      }

      if (($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuarioperfil_id'] == 2 || $_SESSION['usuarioperfil_id'] == 5 || $_SESSION['usuarioperfil_id'] == 6 || $_SESSION['usuario_id'] == 81 || $_SESSION['usuario_id'] == 86 || $_SESSION['usuario_id'] == 85 )) { // G: GERENCIA | P: VENDEDOR | P: CONTABILIDAD | P. SUPERVISOR | NUEVO USUARIO 86 :: MARILIN LISBETH MOLINA AQUINO  |  MARIA FERNANDA ALCANTARA SOTO
        $titulo =  '<button type="button" class="btn btn-primary btn-xs" onClick=\'creditogarveh_modal_titulos_form('.$value['tb_credito_id'].',"L","'.$validationParam_tb_credito_subgar.'",'.$validationParam_tb_credito_cus.','.$value['tb_credito_est'].')\'>Titulos</button>';
      }
      if ($value['tb_credito_est'] == 10 && ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6)) {
        $documentos =  '<a class="btn btn-default" href="javascript:void(0)" onClick="credito_documentos(' . $value['tb_credito_id'] . ')">Documentos</a>';
      }
      // if (trim($value['tb_credito_vehpla']) == '' ) { // && trim($value['tb_credito_subgar']) == 'PRE-CONSTITUCION') {
      //   $placa =  '<a class="btn btn-default" href="javascript:void(0)" onClick="placa_form(' . $value['tb_credito_id'] . ')">Placa</a>';
      // }
      if (trim($value['tb_credito_vehpla']) != '' && $value['tb_credito_vertit'] == 0 && $value['tb_credito_est'] == 3 && ($value['tb_credito_tip'] == 1 || $value['tb_credito_tip'] == 4)) {
        $verificacion =  '<a class="btn btn-default" href="javascript:void(0)" onClick="verificacion_titulos_form(' . $value['tb_credito_id'] . ')">Verificación</a>';
      }

      if ($_SESSION['usuariogrupo_id'] == 2) {
        $eliminar = '<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="eliminar_credito(' . $value['tb_credito_id'] . ',' . $value['tb_credito_est'] . ')" title="ELIMINAR"><i class="fa fa-trash"></i></a>';
      }

      if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6) {
        $comentar = '<a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="cliente_nota(' . $value['tb_credito_id'] . ')"><i class="fa fa-commenting" title="COMENTAR"></i></a>';
      }

      if ($value['tb_credito_tip'] == 2 && $value['tb_credito_est'] == 3) {
        $multiple = '<input type="checkbox" name="che_multiple" class="che_multiple" data-credito="' . $value['tb_credito_id'] . '" data-cliente="' . $value['tb_cliente_id'] . '">';
      }

      if($fecha_hoy >= '29-11-2023' && ($value['tb_credito_tip2'] == 5 || $value['tb_credito_tip2'] == 6) && $value['tb_credito_doc_ref'] == '')
        $refinanciar = '<span class="badge bg-red">Subir Doc Refinanciado</span>';
      
      /*$result1 = $oCaja->mostrarUno($value['tb_caja_id']);
        if($result1['estado']==1){
          $caja = $result1['data']['tb_caja_nom'];
        }
        $result2 = $oMoneda->mostrarUno($value['tb_moneda_id']);
        if($result2['estado']==1){
          $moneda = $result2['data']['tb_moneda_nom'];
      }*/

      $consultarDatosPoliza = $oCreditogarveh->validar_poliza($value['tb_credito_id']);
        if($consultarDatosPoliza['estado'] == 1)
        {
          $btnpoliza =  '<button type="button" class="btn btn-github btn-xs" onClick=\'controlseguro_form("Leer",'.$consultarDatosPoliza['data']['tb_poliza_id'].')\'>'.$consultarDatosPoliza['data']['tb_poliza_aseg'].'</button>';
        }
      $consultarDatosPoliza = null;

      //ASIGNA LOS VALORES POR FILA EN LA COLUMNA CORRESPONDIENTE 
      $tr .= '<tr class="tabla_filaTabla">';
        $tr .= '
          <td id="tabla_fila" style="vertical-align: middle; border-left: 1px solid #135896;">' . $value['tb_credito_id'] . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . mostrar_fecha($value['tb_credito_fecdes']) . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $value['tb_credito_subgar']. '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $tip . ' ' . $tip2 . ' ' . $cuo_tipo . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $value['tb_credito_vehpla'] . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $value['tb_vehiculomarca_nom'] . ' / ' . $value['tb_vehiculomodelo_nom'] . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $cliente . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $moneda . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . mostrar_moneda($value['tb_credito_preaco']) . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $value['tb_credito_int'] . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $cuotas . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $value['tb_usuario_nom'] . ' ' . $value['tb_usuario_ape'] . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $value['tb_representante_nom'] . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $validarestado . '</td>
          <td id="tabla_fila" style="vertical-align: middle;">' . $aprobar . $desembolsar . $documentos . /* $placa .*/ $verificacion . $refinanciar /* . $test */ .'</td>
          <td id="tabla_fila" style="vertical-align: middle; width: 11%;">
            <a class="btn btn-facebook btn-xs" href="javascript:void(0)" onClick="creditogarveh_pagos(' . $value['tb_credito_id'] . ')" title="HISTORIAL PAGOS"><i class="fa fa-money"></i></a>
            <a class="btn btn-success btn-xs" href="javascript:void(0)" onClick="creditogarveh_form(\'M\',' . $value['tb_credito_id'] . ')" title="EDITAR CREDITO"><i class="fa fa-edit"></i></a>
            ' . $eliminar . ' 
            ' . $comentar . '
            <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="credito_his(' . $value['tb_credito_id'] . ')" title="HISTORIAL">His</a>
            ' . $titulo . '
            ' . $btnpoliza . '
          </td>
          <td id="tabla_fila" style="vertical-align: middle;">
            <input type="radio" name="ra_estado" id="ra_estado" data-est="' . $value['tb_credito_est'] . '" data-cuo_tip="' . $value['tb_cuotatipo_id'] . '" data-tip="' . $value['tb_credito_tip'] . '" value="' . $value['tb_credito_id'] . '">
            ' . $multiple . '
          </td>
        ';
      $tr .= '</tr>';
    }
  }
$result = null;
?>
<!-- CREA LA CABECERA DE LA TABLA CON LAS COLUMNAS QUE SE MOSTRARÁt -->
<table id="tbl_creditogarveh" class="table table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th id="tabla_cabecera">ID</th>
      <th id="tabla_cabecera">FECHA</th>
      <th id="tabla_cabecera">TIPO</th>
      <th id="tabla_cabecera">SUB TIPO</th>
      <th id="tabla_cabecera">PLACA</th>
      <th id="tabla_cabecera">MARCA / MODELO</th>
      <th id="tabla_cabecera">CLIENTE</th>
      <th id="tabla_cabecera">MON</th>
      <th id="tabla_cabecera">MONTO</th>
      <th id="tabla_cabecera">INT(%)</th>
      <th id="tabla_cabecera">CUOTAS</th>
      <th id="tabla_cabecera">ASESOR</th>
      <th id="tabla_cabecera">REPRESENTANTE</th>
      <th id="tabla_cabecera">ESTADO</th>
      <th id="tabla_cabecera">ACCIONES</th>
      <th id="tabla_cabecera">OPCIONES</th>
      <th id="tabla_cabecera">ADD</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr; ?>
  </tbody>
</table>