$(document).ready(function() {
    $('#datetimepicker3' ).datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  
    $("#datetimepicker3").on("change", function (e) {
    var endVal = $('#txt_veh_fecsol').val();
    $('#datetimepicker3').data('datepicker').setEndDate(endVal);
  });
});

function guardarplaca(){
$.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/credito_reg.php",
        dataType: "html",
        data: $('#frm_creditoplaca').serialize(),
        beforeSend: function() {
          //$('#msj_clientenota').html("Guardando datos...");
          //$('#msj_clientenota').show(100);
        },
        success: function(data){
            console.log(data);
          if(data == 'exito'){
          	alerta_success("EXITO","Los campos restantes del crédito han sido ingresados");
                $('#modal_creditogarveh_placa').modal('hide');
                creditogarveh_tabla();
          }
          else
            console.log(data);
        },
        complete: function(data){
          console.log(data);
          if(data.statusText == "parsererror"){
            console.log(data);
            $('#msj_creditoplaca').text('ERROR: ' + data.responseText);
          }
        }
      });
    };
