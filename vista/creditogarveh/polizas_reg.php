<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once ('../cliente/Cliente.class.php');
$oCliente= new cliente();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();
require_once '../vehiculoadicional/Vehiculoadicional.class.php';
$oVehiculoadicional = new Vehiculoadicional();
require_once '../creditogarveh/Creditogarveh.class.php';
$oCreditogarveh = new Creditogarveh();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];

$dts = $oCreditogarveh->mostrarUno($_POST['credito_id']);
    if ($dts['estado'] == 1){
        $cli_nom = $dts['data']['tb_cliente_nom'];
        $cli_ape = $dts['data']['tb_cliente_ape'];
        $cliente_id = $dts['data']['tb_cliente_id'];
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $credito_id = $_POST['credito_id'];
        $creditotipo_id = $_POST['creditotipo_id']; 
        $poliza_categoria = 1; // 1 es nuevo
        //$poliza_aseg = mb_strtoupper($_POST['txt_cliente_aseg'], 'UTF-8'); 
        $gps_id = intval($dts['data']['tb_credito_gps']);
        $str_id = intval($dts['data']['tb_credito_str']);
        $rgps = $oVehiculoadicional->mostrarProveedor($gps_id);
        if($rgps['estado']==1){
            $provgps = $rgps['data']['tb_proveedor_nom'];
            //$moneda_id = intval($rgps['data']['tb_moneda_id']);
            $data['proveedor_gps'] = $provgps;
            $data['gps_id'] = $gps_id;
        }
        $rgps = null;
        
        $rstr = $oVehiculoadicional->mostrarProveedor($str_id);
        if($rstr['estado']==1){
            $provstr = $rstr['data']['tb_proveedor_nom'];
            //$moneda_id = intval($rstr['data']['tb_moneda_id']);
            $data['proveedor_seguro'] = $provstr;
            $data['str_id'] = $str_id;
        }
        $rstr=null;
        $poliza_tipo = $dts['data']['tb_credito_tipus']; //tipo de uso 1 taxi, 2 uso particular
        $vehiculomarca_id = $dts['data']['tb_vehiculomarca_id'];
        $vehiculomodelo_id = $dts['data']['tb_vehiculomodelo_id'];
        $vehiculo_tasacion = moneda_mysql($dts['data']['tb_credito_valtas']);
        $data['tasacion'] = $vehiculo_tasacion;
        $asesor_id = intval($dts['data']['tb_credito_usureg']);
        $usuario_id=$_SESSION['usuario_id'];
    }

$tip="Particular";
if($poliza_tip==2){
   $tip="Taxi";
}
$hoy = date('Y-m-d');

$oPoliza->setPoliza_num(0);
$oPoliza->setCliente_id($cliente_id);
$oPoliza->setPoliza_tip($poliza_tipo);
$oPoliza->setCredito_id($credito_id);
$oPoliza->setCreditotipo_id($creditotipo_id);
$oPoliza->setPoliza_categoria($poliza_categoria);
$oPoliza->setUsuario_id($usuario_id);
$oPoliza->setVehiculomarca_id($vehiculomarca_id);
$oPoliza->setVehiculomodelo_id($vehiculomodelo_id);
$oPoliza->setVehiculo_tasacion($vehiculo_tasacion);
$oPoliza->setMoneda_id(2); //codigo 2 de moneda es en dolares y tanto gps como str es en dolares
$oPoliza->setAsesor_id($asesor_id);
$oPoliza->setPoliza_fecini(fecha_mysql($hoy));
$oPoliza->setPoliza_fecfin(fecha_mysql($hoy));

$data['estado'] = 0;
$data['msj'] = 'Error al registrar';


if($action == 'insertar_de_credito'){
    $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha registrado una Poliza con el número'
            . '<b style="color: #0000cc"> 0 </b> de tipo <b style="color: #0000cc">SEGURO</b> '.$estado.' para el cliente <b style="color: #0000cc">'.$cli_nom.' '.$cli_ape."</b>"
            . ' con DNI <b style="color: #0000cc">'.$cli_doc.'</b> para el tipo de crédito <b style="color: #0000cc">Garantia Vehicular</b> con la Aseguradora <b style="color: #0000cc">'.$provstr.'</b>'
            . ' para uso del Vehiculo tipo <b style="color: #0000cc">'.$tip.'</b> ';
    
    $oPoliza->setPoliza_his($poliza_his);
    $oPoliza->setPoliza_aseg($provstr); //
    $oPoliza->setPoliza_tipo(1); //
//||$poliza_categoria>0||$poliza_categoria>0

    $result=$oPoliza->insertarautomatico(); 
        
    if(intval($result['estado']) == 1){
        $polizaseguro_id = $result['poliza_id'];
        $data['polizaseguro_id'] = $polizaseguro_id;
    }
    $result = null;

    $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha registrado una Poliza '
            . 'de tipo <b style="color: #0000cc">GPS</b> de categoria <b style="color: #0000cc"> Nuevo </b> para el cliente <b style="color: #0000cc">'.$cli_nom.' '.$cli_ape."</b>"
            . ' con DNI <b style="color: #0000cc">'.$cli_doc.'</b> para el tipo de crédito <b style="color: #0000cc">Garantia Vehicular</b> con el proveedor <b style="color: #0000cc">'.$provgps.'</b>'
            . ' para uso del Vehiculo tipo <b style="color: #0000cc">'.$tip.'</b> ';
    
    $oPoliza->setPoliza_his($poliza_his);
    $oPoliza->setPoliza_aseg($provgps); //
    $oPoliza->setPoliza_tipo(2); //
    
    $result1=$oPoliza->insertarautomatico(); 
        
    if(intval($result1['estado']) == 1){
        $polizagps_id = $result1['poliza_id'];
        $data['polizagps_id'] = $polizagps_id;
    }
    $result1 = null;
    
    $data['estado'] = 1;
    $data['msj'] = 'Registro de Póliza Correctamente';

	echo json_encode($data);
//    }
//    else {
//        $data['estado'] = 0;
//        $data['msj'] = 'El Número de Poliza ' . $poliza_num . ' ya se encuentra Registrado en el Sistema por favor Verifique';
//        echo json_encode($data);
//    }
}
?>