
<div class="row">

    <!--DATOS DEL CLIENTE-->
    <div class="col-md-12">
        <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">DATOS DEL CLIENTE <?php echo $opc_modificar;
        echo ($cre_tip == 2) ? ' <b style="font-family: cambria;font-weight: bold;color: red"> ESTO ES UNA ADENDA</b>' : ''; ?></label>
        <div class="box box-primary">
            <div class="box-header" style="font-family: cambria;">

                <div class="row">
                    <div class="col-md-4">
                        <label>RUC/DNI:</label>
                        <div class="input-group">
                            <input type="text" id="txt_ven_cli_doc" name="txt_ven_cli_doc" class="form-control input-sm" value="<?php echo $cli_doc ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button" onclick="cliente_form2('M', 0)" title="MODIFICAR CLIENTE">
                                    <span class="fa fa-pencil icon"></span>
                                </button>
                            </span>
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button" onclick="cliente_form2('I', 0)" title="AGREGAR CLIENTE">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <label for="txt_ven_cli_nom">CLIENTE:</label><br/>
                        <input type="text" id="txt_ven_cli_nom" name="txt_ven_cli_nom" class="form-control input-sm mayus"  value="<?php echo $cli_nom ?>">
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-4">
                        <label for="txt_ven_cli_ape">FECHA DE NACIMIENTO:</label><br/>
                        <input type="text" id="txt_ven_cli_fecnac" name="txt_ven_cli_fecnac" class="form-control input-sm" value="<?php echo $cli_fecnac ?>" disabled="disabled"/>
                    </div>
                    <div class="col-md-8">
                        <label for="txt_ven_cli_ape">EMAIL:</label><br/>
                        <input type="text" id="txt_ven_cli_ema" name="txt_ven_cli_ema" class="form-control input-sm mayus" value="<?php echo $cli_ema ?>" disabled="disabled"/>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-4">
                        <label for="txt_ven_cli_ape">TELÉFONO:</label><br/>
                        <input type="text" id="txt_ven_cli_tel" name="txt_ven_cli_tel" class="form-control input-sm" value="<?php echo $cli_tel ?>" disabled="disabled"/>
                        <small class="form-text text-muted small_tel" style="margin-top: 0; color: blue;"><b><?php echo ($cliente_protel)? $cliente_protel : 'Sin Propietario';?></b></small>
                    </div> 
                    <div class="col-md-4">
                        <label for="txt_ven_cli_ape">CELULAR:</label><br/>
                        <input type="text" id="txt_ven_cli_cel" name="txt_ven_cli_cel" class="form-control input-sm" value="<?php echo $cli_cel ?>" disabled="disabled"/>
                        <small class="form-text text-muted small_tel" style="margin-top: 0; color: blue;"><b><?php echo ($cliente_procel)? $cliente_procel : 'Sin Propietario';?></b></small>
                    </div> 
                    <div class="col-md-4">
                        <label for="txt_ven_cli_ape">TELÉFONO REF.:</label><br/>
                        <input type="text" id="txt_ven_cli_telref" name="txt_ven_cli_telref" class="form-control input-sm" value="<?php echo $cli_telref ?>" disabled="disabled"/>
                        <small class="form-text text-muted small_tel" style="margin-top: 0; color: blue;"><b><?php echo ($cliente_protelref)? $cliente_protelref : 'Sin Propietario';?></b></small>
                    </div> 
                </div>
                <p>
                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_ven_cli_ape">DIRECCIÓN :</label><br/>
                        <input type="text" id="txt_ven_cli_dir" name="txt_ven_cli_dir" class="form-control input-sm mayus" value="<?php echo $cli_dir ?>" disabled="disabled"/>
                    </div>
                </div>
                <!-- GERSON (14-02-24) -->
                <p>
                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_ven_cli_ape">OCUPACIÓN :</label><br/>
                        <input type="text" id="txt_ven_cli_ocu" name="txt_ven_cli_ocu" class="form-control input-sm mayus" value="<?php echo $cli_ocu ?>" disabled="disabled"/>
                    </div>
                </div>
                <!--  -->
            </div>
        </div>
    </div>
    <!--FIN DATOS DEL CLIENTE-->

</div>