<?php

require_once('../../core/usuario_sesion.php');
//require_once('../../core/core.php');
require_once('Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
  $hdd_form = $_POST['hdd_form'];
  $creditotipo_id = 3;
  $credito_id = $_POST['credito_id'];
  $result=$oCreditogarveh->mostrarUno($credito_id);
  if($result['estado']==1){

	$reg=mostrar_fecha_hora($result['data']['tb_credito_reg']);
	$mod=mostrar_fecha_hora($result['data']['tb_credito_mod']);
	$apr=mostrar_fecha_hora($result['data']['tb_credito_apr']);

	$usureg=$result['data']['tb_credito_usureg'];
	$usumod=$result['data']['tb_credito_usumod'];
	$usuapr=$result['data']['tb_credito_usuapr'];

        $cuotip_id=$result['data']['tb_cuotatipo_id']; //3 cuota libre, 4 cuota fija
        
	$mon_id=$result['data']['tb_moneda_id'];
	$cuedep_id = $result['data']['tb_cuentadeposito_id'];
	
	$cre_tipcam=$result['data']['tb_credito_tipcam'];
	$cre_preaco = $result['data']['tb_credito_preaco'];
	$cre_int = $result['data']['tb_credito_int'];
	$cre_numcuo = $result['data']['tb_credito_numcuo'];
	$cre_linapr = $result['data']['tb_credito_linapr'];

	$cre_ini = $result['data']['tb_credito_ini'];

	$cre_feccre= mostrar_fecha($result['data']['tb_credito_feccre']);
	$cre_fecdes= mostrar_fecha($result['data']['tb_credito_fecdes']);
	$cre_fecfac=mostrar_fecha($result['data']['tb_credito_fecfac']);
	$cre_fecpro=mostrar_fecha($result['data']['tb_credito_fecpro']);
	$cre_fecent=mostrar_fecha($result['data']['tb_credito_fecent']);
	
	$cre_comdes = $result['data']['tb_credito_comdes'];
	$cre_obs = $result['data']['tb_credito_obs'];
	
	$cre_est= $result['data']['tb_credito_est'];
  $cre_tip = $result['data']['tb_credito_tip']; //el tipo de credito, 1 es el credito normal, 2 es adenda regular y 3 AP
  $cre_idori = $result['data']['tb_credito_idori']; //si es una adenda, aquí se guarda el id del credito padre

	$cli_id=$result['data']['tb_cliente_id'];
	$cli_doc = $result['data']['tb_cliente_doc'];
	$cli_nom=$result['data']['tb_cliente_nom'];
	$cli_ape=$result['data']['tb_cliente_ape'];
	$cli_dir=$result['data']['tb_cliente_dir'];
	$cli_ema=$result['data']['tb_cliente_ema'];
	$cli_fecnac=$result['data']['tb_cliente_fecnac'];
	$cli_tel=$result['data']['tb_cliente_tel'];
	$cli_cel=$result['data']['tb_cliente_cel'];
	$cli_telref=$result['data']['tb_cliente_telref'];
}
$result = "";
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_pagos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">X</span>
        </button>
        <h4 class="modal-title">Historial de Pagos</h4>
      </div>
      <form id="for_historial_acu">
        <input type="hidden" name="hdd_his_cre_id" id="hdd_his_cre_id" value="<?php echo $credito_id; ?>">
        <input type="hidden" name="hdd_his_mon_acu" id="hdd_his_mon_acu" value="0">
        <input type="hidden" name="hdd_his_cli_id" id="hdd_his_cli_id" value="<?php echo $cli_id; ?>">
        <input type="hidden" name="hdd_his_mone_id" id="hdd_his_mone_id" value="<?php echo $mon_id; ?>">
        <input type="hidden" name="hdd_cre_tip" id="hdd_cre_tip" value="3">
        <input type="hidden" name="hdd_cuotip_id" id="hdd_cuotip_id" value="<?php echo $cuotip_id;?>">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="panel panel-info">
              <div class="panel-body">
                  <button type="button" class="btn btn-info" onclick="cuotapago_pagos_tabla()" title="HISTORIAL PAGOS">Historial</button>
                <button type="button" class="btn btn-info" onclick="morapago_pagos_tabla()" title="HISTORIAL MORAS">Moras</button>
                <a class="btn btn-info" href="javascript:imprimir_js('div_modal_creditogarveh_pagos')" >Imprimir</a>

                <?php if($cre_tip != 2 && $cre_tip != 3): ?>
                  <button type="button" class="btn btn-info" onClick="adenda_form('adenda',<?php echo $credito_id;?>)" title="CREAR ADENDA">Adenda</button>
                  <?php if($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6): ?>
                    <button type="button" class="btn btn-info" onclick="acuerdopago_form('insertar',0)" title="CREAR ACUERDO DE PAGO">Acuerdo de Pago</button>
                  <?php endif; ?>
                  <button type="button" class="btn btn-info" onclick="acuerdopago_tabla()" title="HISTORIAL ACUERDO DE PAGO">Acuerdo de Pago tabla</button>
                <?php endif;?>

                <?php if($cre_est == 4): ?>
                  <button type="button" class="btn btn-info" onclick="adenda_reprogramacion('ad_repro',<?php echo $credito_id;?>)" title="REPROGRAMAR ADENDA">Reprogramación</button>
                <?php endif; ?>

                    <a class="btn btn-info" target="_blank" href="<?php echo 'vista/caja/caja_historico_credito_excel.php?cre_id='.$credito_id.'&cre_tip=3'; ?>" title="REPORTE HISTORICO">Histórico</a>


                <input type="hidden" id="hdd_credito_id" value="<?php echo $credito_id;?>">
                <input type="hidden" id="hdd_creditotipo_id" value="<?php echo $creditotipo_id;?>">
                <input type="hidden" id="hdd_form" value="<?php echo $hdd_form;?>">
              </div>
            </div>
            <div class="callout callout-info" id="creditogarveh_pagos_mensaje_tbl" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando el historial de Pagos</h4>
            </div>
            <div id="creditogarveh_pagos_tabla">
              <!-- INCLUIMOS LA TABLA DE PAGOS, PUEDE SER CUOTAS O MORAS-->
              <?php //require_once('../cuotapago/cuotapago_pagos_tabla.php');?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div id="div_modal_cuotapago_detalle"></div>
<div id="div_modal_solicitar_anulacion"></div>

<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_pagos.js?ver=4374441';?>"></script>
