<?php
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();

$cred_id = $_POST['creditoid'];
$html = '';
$res_ejecucion = $oEjecucion->listar_todos('', '', $cred_id, '', '', '');
if ($res_ejecucion['estado'] == 1) {
    //todos los require
        require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
        $oCertificado = new Certificadoregistralveh();
        require_once '../upload/Upload.class.php';
        $oUpload = new Upload();
        require_once '../cartanotarial/Cartanotarial.class.php';
        $oCarta = new Cartanotarial();
        require_once '../demanda/Demanda.class.php';
        $oDemanda = new Demanda();
        require_once '../ejecucionupload/Ejecucionfasefile.class.php';
        $oEjecucionfasefile = new Ejecucionfasefile();

        require_once '../funciones/fechas.php';
    //

    foreach ($res_ejecucion['data'] as $key => $ejecucion) {
        $listado_documentos = '';
        $ejecucion_id = $ejecucion['tb_ejecucion_id'];
        //mostrar EP
            $listado_documentos .=
            '<div class="col-md-6">'.
                '<div class="row">'.
                    '<div class="col-md-9">'.
                        '<label class="checkbox-inline" style="padding:6px">'.
                            '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>'.
                            '<b>ESCRITURA PÚBLICA</b>'.
                        '</label>'.
                    '</div>'.
                    '<div class="col-md-3">'.
                        '<a href="'.buscarUrlDocumento($ejecucion, 'escritura_publica')['url'].'" target="_blank" class="btn btn-primary btn-xs" title="Ver pdf">'.
                            '<i class="fa fa-file-pdf-o"></i>'.
                        '</a>'.
                    '</div>'.
                '</div>'.
            '</div>';
        //
        //mostrar los certificados registrales registrados
            $res_crv = $oCertificado->listar_todos('', $cred_id, $ejecucion_id, '');
            if ($res_crv['estado'] == 1) {
                foreach ($res_crv['data'] as $key => $value) {
                    $text_dado_baja = $value['tb_certificadoregveh_est'] == 0 ? ' (dado de baja por legal)' : '';

                    $listado_documentos .=
                    '<div class="col-md-6">' .
                        '<div class="row">' .
                            '<div class="col-md-9">' .
                                '<label class="checkbox-inline" style="padding:6px">' .
                                    '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .
                                    '<b>CERTIFICADO REG. VEH. DEL ' . mostrar_fecha($value['tb_certificadoregveh_fecgenerado']) . $text_dado_baja . '</b>' .
                                '</label>' .
                            '</div>' .
                            '<div class="col-md-3">' .
                                '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="verejecucion_pdf_form(' . $value['upload_id'] . ', \'ejecucion_certificado_regveh\', \'checklist\')" title="Ver pdf">' .
                                    '<i class="fa fa-file-pdf-o"></i>' .
                                '</a>' .
                            '</div>' .
                        '</div>' .
                    '</div>';
                }
            }
            unset($res_crv);
        //
        //mostrar documento liquidación credito subida por legal
            $oUpload->incluye_xac = 1;
            $res_pdf_liquidacion = $oUpload->mostrarUno_modulo($ejecucion_id, 'ejecucion_liquidacion');

            if ($res_pdf_liquidacion['estado'] == 1) {
                $listado_documentos .=
                '<div class="col-md-6">' .
                    '<div class="row">' .
                        '<div class="col-md-9">' .
                            '<label class="checkbox-inline" style="padding:6px">' .
                                '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .
                                '<b>RESOLUCION DE CREDITO (subido el '. mostrar_fecha($res_pdf_liquidacion['data']['upload_reg']).')</b>' .
                            '</label>' .
                        '</div>' .
                        '<div class="col-md-3">' .
                            '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="verejecucion_pdf_form('.$res_pdf_liquidacion['data']['upload_id'].', \'ejecucion_liquidacion\', \'checklist\')" title="Ver pdf">' .
                                '<i class="fa fa-file-pdf-o"></i>' .
                            '</a>' .
                        '</div>' .
                    '</div>' .
                '</div>';
            }
            unset($res_pdf_liquidacion);
        //
        //mostrar las cartas notariales
            $res_carta = $oCarta->listar_todos('', $ejecucion_id, '', '');
            if ($res_carta['estado'] == 1) {
                foreach ($res_carta['data'] as $key => $carta) {
                    if (!empty($carta['tb_cartanotarial_doc_scanned'])) {
                        $cli_rep = $carta['tb_cartanotarial_destinatario'] == 1 ? 'CLIENTE' : 'REPRESENTANTE';
                        $listado_documentos .=
                        '<div class="col-md-6">' .
                            '<div class="row">' .
                                '<div class="col-md-9">' .
                                    '<label class="checkbox-inline" style="padding:6px">' .
                                        '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .
                                        '<b>CARTA NOTARIAL DIRIGIDA AL '.$cli_rep.' (diligenciada el '.mostrar_fecha($carta['tb_cartanotarial_fecdiligencia']).')</b>' .
                                    '</label>' .
                                '</div>' .
                                '<div class="col-md-3">' .
                                    '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="'."verejecucion_pdf_form({$carta['tb_cartanotarial_doc_scanned']}, 'ejecucion_varios-cartascan', 'checklist')".'" title="Ver pdf">' .
                                        '<i class="fa fa-file-pdf-o"></i>' .
                                    '</a>' .
                                '</div>' .
                            '</div>' .
                        '</div>';
                    }
                }
            }
            unset($res_carta);
        //
        //mostrar pdf de pago med cautelar y derecho not, se necesita primero buscar la demanda pq ahí está el id del upload
            $res_demanda = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '');
            if ($res_demanda['estado'] == 1) {
                if (!empty($res_demanda['data'][0]['tb_demanda_medcau_upload_id'])) {
                    $listado_documentos .=
                    '<div class="col-md-6">' .
                        '<div class="row">' .
                            '<div class="col-md-9">' .
                                '<label class="checkbox-inline" style="padding:6px">' .
                                    '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .
                                    '<b>PAGO MEDIDA CAUTELAR PARA DEMANDA (subido el '. mostrar_fecha($res_demanda['data'][0]['reg_medcau']).')</b>' .
                                '</label>' .
                            '</div>' .
                            '<div class="col-md-3">' .
                                '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="verejecucion_pdf_form('.$res_demanda['data'][0]['tb_demanda_medcau_upload_id'].', \'\', \'checklist\', \'?PAGO MEDIDA CAUTELAR PARA DEMANDA\')" title="Ver pdf">' .
                                    '<i class="fa fa-file-pdf-o"></i>' .
                                '</a>' .
                            '</div>' .
                        '</div>' .
                    '</div>';
                }
                if (!empty($res_demanda['data'][0]['tb_demanda_dernot_upload_id'])) {
                    $listado_documentos .=
                    '<div class="col-md-6">' .
                        '<div class="row">' .
                            '<div class="col-md-9">' .
                                '<label class="checkbox-inline" style="padding:6px">' .
                                    '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .
                                    '<b>PAGO DERECHO NOTIFICACION PARA DEMANDA (subido el '. mostrar_fecha($res_demanda['data'][0]['reg_dernot']).')</b>' .
                                '</label>' .
                            '</div>' .
                            '<div class="col-md-3">' .
                                '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="verejecucion_pdf_form('.$res_demanda['data'][0]['tb_demanda_dernot_upload_id'].', \'\', \'checklist\', \'?PAGO DERECHO NOTIFICACION PARA DEMANDA\')" title="Ver pdf">' .
                                    '<i class="fa fa-file-pdf-o"></i>' .
                                '</a>' .
                            '</div>' .
                        '</div>' .
                    '</div>';
                }
                if (!empty($res_demanda['data'][0]['tb_demanda_doc_scanned'])) {
                    $listado_documentos .=
                    '<div class="col-md-6">' .
                        '<div class="row">' .
                            '<div class="col-md-9">' .
                                '<label class="checkbox-inline" style="padding:6px">' .
                                    '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .
                                    '<b>DEMANDA SCANEADA (presentada el '. mostrar_fecha_hora($ejecucion['tb_ejecucion_demanda_fechora_presentacion']).')</b>' .
                                '</label>' .
                            '</div>' .
                            '<div class="col-md-3">' .
                                '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="verejecucion_pdf_form('.$res_demanda['data'][0]['tb_demanda_doc_scanned'].', \'ejecucion_varios-demscan\', \'checklist\')" title="Ver pdf">' .
                                    '<i class="fa fa-file-pdf-o"></i>' .
                                '</a>' .
                            '</div>' .
                        '</div>' .
                    '</div>';
                }
            }
            unset($res_demanda);
        //
        //documentos desde la fase incautacion en adelante
            $res_fasefiledocs = $oEjecucionfasefile->listar('', '', $ejecucion_id, 'not null', '', '', '', '');
            if ($res_fasefiledocs['estado'] == 1) {
                foreach ($res_fasefiledocs['data'] as $key => $fasefiledoc) {
                    $listado_documentos .=
                    '<div class="col-md-6">' .
                        '<div class="row">' .
                            '<div class="col-md-9">' .
                                '<label class="checkbox-inline" style="padding:6px">' .
                                    '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>' .//upload_reg
                                    '<b>'.strtoupper($fasefiledoc['tb_ejecucionfasefile_archivonom']).' (subido el '. mostrar_fecha($fasefiledoc['upload_reg']).')</b>' .
                                '</label>' .
                            '</div>' .
                            '<div class="col-md-3">' .
                                '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="verejecucion_pdf_form('.$fasefiledoc['upload_id'].', \'\', \'checklist\', \'?'.strtoupper($fasefiledoc['tb_ejecucionfasefile_archivonom']).'\')" title="Ver pdf">' .
                                    '<i class="fa fa-file-pdf-o"></i>' .
                                '</a>' .
                            '</div>' .
                        '</div>' .
                    '</div>';
                }
            }
            unset($res_fasefiledocs);
        //
        
        $html .=
        '<label style="font-family: cambria;font-weight: bold;color: #003399">DOCUMENTOS DE EJECUCION ID: '.$ejecucion_id.'</label>'.
        '<div class="box box-primary">'.
            '<div class="box-body">'.
                '<div class="row">'.
                    $listado_documentos.
                '</div>'.
            '</div>'.
        '</div><br>';
    }
}
else {
    $html = '<b>No existen procesos de ejecucion legal para este crédito</b>';
}

echo $html;

function buscarUrlDocumento($ejecucion_reg, $nombre_item){
	require_once '../proceso/Proceso.class.php';
	$oProceso = new Proceso();
	require_once '../pdfgarv/Pdfgarv.class.php';
	$oPdfgarv = new Pdfgarv();

    $credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];

	//para saber si un credito viene de "procesos garveh" o de ANTES del modulo de procesos, se hace una consulta en tabla procesos
	$buscar_proceso = $oProceso->mostrarProcesoXCredito($credito_getdocs);
	$return = array();
	
	if ($buscar_proceso['estado'] == 1) {
		$proceso_id = $buscar_proceso['data']['tb_proceso_id'];

		//en la tabla tb_proc_item, columna tb_item_idx = 'escritura_publica' tiene el tb_item_id = 49.
		//Si se desea buscar id mediante consulta usar Proceso->obtenerItemPorIdx($idx)
		//encontrar el item y su url mediante Proceso->obtenerContenidoPorProcesoFaseItem

		if ($nombre_item == 'escritura_publica') {
			$res = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 49)['data'];
		}
		elseif ($nombre_item == 'bol_fac_compra') {
			$res = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, 41)['data'];
		}
		$return['tabla'] = 'tb_proc_multimedia';
		$return['url'] = $res['tb_multimedia_url'];
		$return['id_reg'] = $res['tb_multimedia_id'];
	} else {
		if ($ejecucion_reg['tb_cgarvtipo_id'] == 1) {//es PRECONSTITUCION
			if ($nombre_item == 'escritura_publica') {
				$tipogarvdoc_persdetalle_id = $ejecucion_reg['tb_cliente_tip'] == 1 ? 16 : 48;
			}
			elseif ($nombre_item == 'bol_fac_compra') {
				$tipogarvdoc_persdetalle_id = $ejecucion_reg['tb_cliente_tip'] == 1 ? 12 : 44;
			}
		} /* else para las demas tipos de credito garveh */
		elseif ($ejecucion_reg['tb_cgarvtipo_id'] == 2) {//es CONSTITUCION SIN CUSTODIA
			if ($nombre_item == 'escritura_publica') {
				$tipogarvdoc_persdetalle_id = $ejecucion_reg['tb_cliente_tip'] == 1 ? 77 : 106;
			}
		}

		//retorno
		$buscar_documento = $oPdfgarv->mostrarPorDocumento1($credito_getdocs, $tipogarvdoc_persdetalle_id);
		if ($buscar_documento['estado'] == 1) {
			$return['tabla'] = 'pdfgarv';
			$return['url'] = $buscar_documento['data']['tb_pdfgarv_ruta'];
			$return['id_reg'] = $buscar_documento['data']['tb_pdfgarv_id'];
		}
		unset($buscar_documento);
	}
	$return['estado'] = 1;
	unset($buscar_proceso);
	return $return;
}
