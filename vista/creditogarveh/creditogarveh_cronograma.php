<?php
  if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }

  require_once('Creditogarveh.class.php');
  $oCreditogarveh = new Creditogarveh();
  require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();
  /*require_once('../caja/Caja.class.php');
  $oCaja = new Caja();
  require_once('../moneda/Moneda.class.php');
  $oMoneda = new Moneda();*/

$cuotip_id = $_POST['cuotip_id'];
$C = moneda_mysql($_POST['cre_preaco']);
$i = moneda_mysql($_POST['cre_int']);
$tip_ade = $_POST['tip_ade'];
$cre_id_ori = $_POST['cre_idori'];
$n = $_POST['cre_numcuo'];

if($tip_ade == 4)
$n = $_POST['cre_numcuo_res']; // si es CUOTA BALON, se suma todas las cuotas por pagar para calcular el interes y generar una sola cuota

if($cuotip_id == 3) // si es 4 es fija, 3 es libre
$n = 1; // si es libre solo será una cuota

//parametro 2 ya que debe buscar las cuotas de un credito ASIVEH
if($tip_ade == 4){
  $result = $oCuota->ultima_cuota_por_credito($cre_id_ori, 3);
    if($result['estado']==1){
        $cuo_fec = mostrar_fecha($result['data']['tb_cuota_fec']);
    }
 $result = "";
}

$fec_desem = $_POST['cre_fecdes']; //fecha de desembolso
$fec_prorra = $_POST['cre_fecpro'];//fecha de prorrateo
$cuosubper_id = $_POST['cuosubper_id']; //sub perio, semanal, quincena, mes
if($cuosubper_id == 1){
  $cols = 1;
  $name_per = "MENSUAL";
}
if($cuosubper_id == 2){
  $cols = 2;
  $name_per = "QUINCENAL";
}
if($cuosubper_id == 3){
  $cols = 4;
  $name_per = "SEMANAL";
}
    
//para la formula del prorrateo, se toma la fecha del prorrateo y se cuenta cuántos días tiene el mes de la fecha del prorrateo, ejem: prorrateo 10/12/2016, el mes 12 tiene 31 días, para saber eso usamos la funcion de mktime
//formula prorrateo = (interes de la primera cuota / total días del mes prorrateo) * dias prorrateo
list($dia,$mes,$anio) = explode('-',$fec_prorra);
$dias_mes = date('t', mktime(0, 0, 0, $mes, 1, $anio));
$fec_desem1 = new DateTime($fec_desem);
$fec_prorra1 = new DateTime($fec_prorra);
$dias_prorra = $fec_desem1->diff($fec_prorra1)->format("%a");

$R = $C / $n;
if($i != 0){
  $uno = $i / 100;
  $dos = $uno + 1;
  $tres = pow($dos,$n);
  $cuatroA = ($C * $uno) * $tres;
  $cuatroB = $tres - 1;
  $R = $cuatroA / $cuatroB;
  $r_sum = $R*$n;
}
$fecha_factu = $_POST['cre_fecfac'];
list($day, $month, $year) = explode('-', $fecha_factu);
$moneda = $_POST['mon_id'];
//HASTA ACÁ LLEGÉ A REVISARLO
if(is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R) && $tip_ade != 4){
?>
<div class="box box-default">
    <div class="box-header">
        <h4>Cronograma de Crédito 1111<?php echo $R;?></h4>
    </div>
  <div class="box-body">
        <div class="row"> 
            <div class="col-md-12"> 
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                      <th id="filacabecera">N°</th>
                      <th id="filacabecera" colspan="<?php echo $cols?>">FECHAS</th>
                      <th id="filacabecera">CAPITAL</th>
                      <th id="filacabecera">AMORTIZACIÓN</th>
                      <th id="filacabecera">INTERÉS</th>
                      <th id="filacabecera">PRORRATEO</th>
                      <th id="filacabecera">CUOTA</th>
                      <th id="filacabecera"><?php echo $name_per; ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($moneda == '1'){
                    $moneda = 'S/. ';
                  }else{
                    $moneda = 'US$. ';
                  }
                  for ($j=1; $j <= $n; $j++)
                  { 
                    if($j>1)
                    {
                      $C = $C - $amo;
                      $int = $C*($i/100);
                      $amo = $R - $int;
                      if($cuotip_id == 3)
                        $amo = 0;
                    }
                    else
                    {
                      $int = $C*($i/100);
                      $amo = $R - $int;
                      if($cuotip_id == 3)
                        $amo = 0;
                    }
                    //fecha facturacion
                    $month = $month + 1;
                    if($month == '13'){
                      $month = 1;
                      $year = $year + 1;
                    }
                    $fecha=validar_fecha_facturacion($day,$month,$year);
                    if($j == 1){
                      $int_pro = ($int / $dias_mes) * $dias_prorra;
                      $pro_div_cuo = $int_pro / $n;
                      //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                      $pro_div_subcuo = $int_pro / ($n * $cols);
                    }
                    else
                      $int_pro = 0;
                  ?>
                    <tr class="filaTabla">
                      <td id="fila">
                        <?php echo $j;?>
                      </td>
                      <?php 
                        list($day,$mon,$year) = explode('-',$fecha);
                        if($cuosubper_id == 3){
                          $fecha_1=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*3),$year));
                          $fecha_2=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*2),$year));
                          $fecha_3=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*1),$year));
                          $fecha_4=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*0),$year));        
                          echo '<td align="center">'.$fecha_1.'</td>
                                <td align="center">'.$fecha_2.'</td>
                                <td align="center">'.$fecha_3.'</td>
                                <td align="center"><strong>'.$fecha_4.'</strong></td>';
                        }
                        if($cuosubper_id == 2){
                          $fecha_1=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*2),$year));
                          $fecha_2=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*0),$year));
                          echo '<td align="center">'.$fecha_1.'</td>
                                <td align="center"><strong>'.$fecha_2.'</strong></td>';
                        }
                        if($cuosubper_id == 1){
                          $fecha_1=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*0),$year));
                          echo '<td align="center"><strong>'.$fecha_1.'</strong></td>';
                        }
                      ?>      
                      <td id="fila">
                        <?php echo $moneda . formato_numero($C);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($amo);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($int);?>
                      </td>
                      <td id="fila">
                        <?php echo '<input type="hidden" id="hdd_mon_pro_'.$j.'" value="'.formato_numero($pro_div_cuo).'">'?>
                        <?php echo $moneda . formato_numero($pro_div_cuo);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($R + $pro_div_cuo);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($R/$cols + $pro_div_subcuo);?>
                      </td>
                    </tr>
                  <?php
                  }
                  ?>
                  </tbody>
                </table>
            </div>
        </div> 
    </div>
</div>
<?php
  }
if(is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R) && $tip_ade ==4){
?>
<div class="box box-default">
    <div class="box-header">
        <h4>Cronograma de Crédito 2222<?php echo $R;?></h4>
    </div>
    <div class="box-body">
        <div class="row"> 
            <div class="col-md-12"> 
                <table class="table table-bordered table-hover">
                  <thead>
                     <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                      <th id="filacabecera">N°</th>
                      <th id="filacabecera" colspan="<?php echo $cols?>">FECHAS</th>
                      <th id="filacabecera">CAPITAL</th>
                      <th id="filacabecera">AMORTIZACIÓN</th>
                      <th id="filacabecera">INTERÉS</th>
                      <th id="filacabecera">PRORRATEO</th>
                      <th id="filacabecera">CUOTA</th>
                      <th id="filacabecera"><?php echo $name_per; ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($moneda == '1'){
                    $moneda = 'S/. ';
                  }else{
                    $moneda = 'US$. ';
                  }
                  $sum_int = 0;
                  $sum_amo = 0;
                  for ($j=1; $j <= $n; $j++){ 
                    if($j>1){
                      $C = $C - $amo;
                      $int = $C*($i/100);
                      $amo = $R - $int;
                    }
                    else{
                      $int = $C*($i/100);
                      $amo = $R - $int;
                    }
                    //fecha facturacion
                    $month = $month + 1;
                    if($month == '13'){
                      $month = 1;
                      $year = $year + 1;
                    }
                    $fecha=validar_fecha_facturacion($day,$month,$year);
                    if($j == 1){
                      $int_pro = ($int / $dias_mes) * $dias_prorra;
                      $pro_div_cuo = $int_pro / $n;
                      //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
                      $pro_div_subcuo = $int_pro / ($n * $cols);
                    }
                    else
                      $int_pro = 0;

                    $sum_int += $int;
                    $sum_amo += $amo;
                  }
                  $cre_preaco = moneda_mysql($_POST['cre_preaco']);
                  ?>
                    <tr class="filaTabla">
                      <td id="fila">1</td>
                      <td id="fila"><?php echo $cuo_fec;?></td>  
                      <td id="fila">
                        <?php echo $moneda . formato_numero($cre_preaco);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($sum_amo);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($sum_int);?>
                      </td>
                      <td id="fila">
                        <?php echo '<input type="hidden" id="hdd_mon_pro_1" value="0">'?>
                        <?php echo $moneda . '0.00';?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($cre_preaco + $sum_int);?>
                      </td>
                      <td id="fila">
                        <?php echo $moneda . formato_numero($cre_preaco + $sum_int);?>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div> 
    </div>
</div>
<?php
}
?>
