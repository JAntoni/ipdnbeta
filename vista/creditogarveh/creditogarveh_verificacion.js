$(document).ready(function() {
    
$("#frm_creditoverificacion").validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/credito_reg.php",
        async:true,
        dataType: "text",
        data: $("#frm_creditoverificacion").serialize(),
        beforeSend: function() {
          //$('#msj_credito').html("Guardando...");
          //$('#msj_credito').show(100);
        },
        success: function(data){
                if(data == 'exito'){
                    alerta_success("EXITO","Los datos han sido verificados");
                    $('#modal_creditogarveh_verificacion').modal('hide');
                    creditogarveh_tabla();
                }
                else
                  console.log(data);
        },
        complete: function(data){
          console.log(data);
        }
      });
    },
    rules: {
        cmb_verificacion_cli_si: {
                required: true
        },
        cmb_verificacion_emp_si: {
                required: true
        },
        txt_veri_observaciones: {
                required: true
        }
    },
    messages: {
        cmb_verificacion_cli_si: {
                required: 'Ingresar si cliente correcto'
        },
        cmb_verificacion_emp_si: {
                required: 'Ingresa si Empresa correcto'
        },
        txt_veri_observaciones: {
                required: 'Observaciones'
        }
    }
	  });
});

