<?php
  $cuotas_fijas = '';
  $cuotas_libres = '';

  if($cuotatipo_id == 4)
    $cuotas_libres = 'style="display:none;"';
  if($cuotatipo_id == 3)
    $cuotas_fijas = 'style="display:none;"';

  $cre_tecm = $cre_int;
  $cre_int = $cre_int - $credito_seguro;
?>

<div class="row">
  <!--DATOS DEL CREDITO-->
  <div class="col-md-12">
    <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">
      DATOS DEL CREDITO <?php echo $opc_modificar; echo ($cre_tip == 2) ? ' <b style="font-family: cambria;font-weight: bold;color: red"> ESTO ES UNA ADENDA</b>' : ''; ?>
    </label>
    <div class="box box-primary">
      <div class="box-header" style="font-family: cambria;">
        <?php if ($aden_id > 0) : ?>
          <div class="row">
            <div class="col-md-6">
              <label>TIPO DE ADENDA :</label>
              <select name="cmb_tip_aden" id="cmb_tip_aden" class="form-control input-sm mayus">
                <option value="1">Crédito Regular</option>
                <option value="2">Crédito Específico</option>
                <option value="4">Cuota Balón</option>
              </select>
            </div>
            <div class="col-md-6" id="div_cuo_res" style="display: none;">
              <label for="txt_cre_cuo_res">CUOTAS RESTANTES :</label><br />
              <input type="text" id="txt_cre_cuo_res" class="form-control input-sm mayus" name="txt_cre_cuo_res" value="<?php echo $cuo_res ?>" readonly>
            </div>
          </div>
        <?php endif; ?>
        <div class="row">
          <div class="col-md-4">
            <label>TIPO DE CUOTA <?php echo "($cuotatipo_id)";?>:</label>
            <select name="cmb_cuotip_id" id="cmb_cuotip_id" class="form-control input-sm mayus">
              <?php require_once '../cuotatipo/cuotatipo_select.php'; ?>
            </select>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-6">
                <label for="txt_cre_perid">PERIODO :</label><br />
                <input type="text" id="txt_cre_perid" name="txt_cre_perid" class="form-control input-sm mayus" value="MENSUAL" readonly>
              </div>
              <div class="col-md-6">
                <label for="cmb_cuosubper_id">SUB-PERIODO :</label><br />
                <select name="cmb_cuosubper_id" id="cmb_cuosubper_id" class="form-control input-sm mayus">
                  <option value="1" <?php if ($cuosubper_id == '1') {
                                      echo 'selected';
                                    } ?>>MENSUAL</option>
                  <option value="2" <?php if ($cuosubper_id == '2') {
                                      echo 'selected';
                                    } ?>>QUINCENAL</option>
                  <option value="3" <?php if ($cuosubper_id == '3') {
                                      echo 'selected';
                                    } ?>>SEMANAL</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-6">
                <label for="cmb_mon_id">MONEDA :</label><br />
                <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm mayus">
                  <?php require '../moneda/moneda_select.php'; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="txt_cre_tipcam">CAMBIO :</label><br />
                <input type="text" id="txt_cre_tipcam" class="form-control input-sm mayus" name="txt_cre_tipcam" value="<?php echo $cre_tipcam ?>" readonly>
              </div>
            </div>
          </div>
        </div>
        <p>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="txt_cre_preaco" class="control-label">PRECIO ACORDADO :</label>
              <input type="text" name="txt_cre_preaco" id="txt_cre_preaco" class="form-control input-sm moneda2" value="<?php echo formato_moneda($cre_preaco) ?>" readonly>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cre_int" class="control-label">INTERÉS (%) :</label>
                  <input type="text" name="txt_cre_int" id="txt_cre_int" class="form-control input-sm porcentaje" value="<?php echo $cre_int; ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cre_tecm" class="control-label">TECM (%) :</label>
                  <input type="text" name="txt_cre_tecm" id="txt_cre_tecm" class="form-control input-sm porcentaje" readonly value="<?php echo $cre_tecm; ?>">
                </div>
              </div>
              <div class="col-md-4" id="cre_fijo" <?php echo $cuotas_fijas;?> >
                <div class="form-group">
                  <label for="txt_cre_numcuo" class="control-label">N° CUOTAS :</label>
                  <input type="text" name="txt_cre_numcuo" id="txt_cre_numcuo" class="form-control input-sm" value="<?php echo $cre_numcuo ?>">
                </div>
              </div>
              <div class="col-md-4" id="cre_libre" <?php echo $cuotas_libres;?> >
                <div class="form-group">
                  <label for="txt_cre_numcuo" class="control-label">N° CUOTAS MAX :</label>
                  <input type="text" name="txt_cre_numcuomax" id="txt_cre_numcuomax" class="form-control input-sm" value="<?php echo $cre_numcuomax ?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="txt_cre_linapr" class="control-label">LÍNEA APROBADA :</label>
              <input type="text" name="txt_cre_linapr" id="txt_cre_linapr" class="form-control input-sm moneda2" value="<?php echo $cre_linapr ?>" readonly>
            </div>
          </div>
        </div>
        <p>
        
        <div class="row" style="background-color: #f7e49f;">
          <div class="col-md-3">
            <div class="form-group">
              <label for="txt_credito_monedache" class="control-label">EL CHEQUE SERÁ EN:</label>
              <select name="txt_credito_monedache" id="txt_credito_monedache" class="form-control input-sm">
                <option value="">Selecciona...</option>
                <option value="1" <?php if(intval($credito_monedache) == 1) echo 'selected';?> >Soles S/.</option>
                <option value="2" <?php if(intval($credito_monedache) == 2) echo 'selected';?> >Dólares USD$</option>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="txt_cre_tc_2" class="control-label">TIPO DE CAMBIO PARA EL CHEQUE:</label>
              <input type="text" name="txt_cre_tc_2" id="txt_cre_tc_2" class="form-control input-sm moneda3" value="<?php echo $cre_tc_2;?>">
            </div>
          </div>
          <div class="col-md-3" id="cre_fijo">
            <div class="form-group">
              <label for="txt_credito_montoche" class="control-label">VALOR DEL CHEQUE A REALIZAR:</label>
              <input type="text" name="txt_credito_montoche" id="txt_credito_montoche" class="form-control input-sm moneda2" value="<?php echo $credito_montoche;?>">
            </div>
          </div>
          <div class="col-md-3" id="cre_fijo">
            <div class="form-group">
              <label for="txt_credito_montoche" class="control-label">CHEK PARA AGREGAR: <?php echo $credito_gps;?></label><br/>
              <label>
                AGREGAR GPS: <input class="flat-green" type="checkbox" name="che_gps" id="che_gps" value="2" <?php if(intval($credito_gps) > 0) echo 'checked="true"'; ?> />
              </label>
            </div>
            <input type="hidden" id="hdd_seguro_porcentaje" name="hdd_seguro_porcentaje"/>
            <input type="hidden" id="hdd_gps_precio" name="hdd_gps_precio"/>
            <input type="hidden" id="hdd_gps_simple_id" name="hdd_gps_simple_id" />
            <span class="badge bg-green" id="span_seguro_gps"></span>
          </div>
        </div>

        <p>
        <div class="row">
          <div class="col-md-4">
            <label for="cmb_rep_id">REPRESENTANTE:</label><br />
            <div class="input-group">
              <select name="cmb_rep_id" id="cmb_rep_id" class="form-control input-sm mayus">
                <?php require_once '../representante/representante_select.php'; ?>
              </select>
              <span class="input-group-btn">
                <button class="btn btn-primary btn-sm" type="button" onclick="representante_form3('I', 0)" title="AGREGAR ACREEDOR">
                  <span class="fa fa-plus icon"></span>
                </button>
              </span>
            </div>
          </div>
          <div class="col-md-4">
            <label for="cmb_cuedep_id">CUENTA A DEPOSITAR :</label><br />
            <div class="input-group">
              <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm mayus">
                <?php require '../cuentadeposito/cuentadeposito_garveh_select.php'; ?>
              </select>
              <span class="input-group-btn">
                <button class="btn btn-primary btn-sm" type="button" onclick="cuentadeposito_form2('I', 0)" title="AGREGAR CUENTA DEPOSITO">
                  <span class="fa fa-plus icon"></span>
                </button>
              </span>
            </div>
          </div>
          <div class="col-md-4">
            <label for="txt_ven_cli_ape">COMPROBANTE DE DESEMBOLSO :</label><br />
            <input type="text" id="txt_cre_comdes" name="txt_cre_comdes" class="form-control input-sm" value="<?php echo $cre_comdes ?>" disabled="disabled" />
          </div>
        </div>
        <p>
        <div class="row">
          <div class="col-md-12">
            <label for="txt_ven_cli_ape">OBSERVACIÓN :</label><br />
            <textarea name="txt_cre_obs" id="txt_cre_obs" class="form-control input-sm mayus" rows="2" cols="95" placeholder="Describe las observaciones..."><?php echo $cre_obs ?></textarea>
          </div>
        </div>
        <p>
          <?php if ($adenda_id > 1 || $cre_tip == 2) : ?>
        <div class="row">
          <div class="col-md-12">
            <label for="txt_ven_cli_ape">Si esta ADENDA es de un crédito Garantía Mobiliaria, llena estos datos ---------> :</label><br />
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label for="txt_cre_numescr">N° ESCRITURA :</label><br />
            <input type="text" name="txt_cre_numescr" id="txt_cre_numescr" class="form-control input-sm mayus" value="<?php echo $cre_numescr ?>">
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="">FECHA DE ESCRITURA :</label>
              <div class="input-group">
                <div class='input-group date' id='datetimepicker6'>
                  <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecescr" id="txt_cre_fecescr" value="<?php echo mostrar_fecha($cre_fecescr); ?>" readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <p>
        <?php endif; ?>
        <div class="row">
          <div class="col-md-2">
            <div class="form-group">
              <label for="">FECHA DE CRÉDITO :</label>
              <div class="input-group">
                <div class='input-group date' id='datetimepicker7'>
                  <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_feccre" id="txt_cre_feccre" value="<?php echo mostrar_fecha($cre_feccre); ?>" readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="">FCH. DESEMBOLSO :</label>
              <div class="input-group">
                <div class='input-group date' id='datetimepicker8'>
                  <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecdes" id="txt_cre_fecdes" value="<?php echo mostrar_fecha($cre_fecdes); ?>" readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="">FCH. FACTURACIÓN :</label>
              <div class="input-group">
                <div class='input-group date' id='datetimepicker9'>
                  <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecfac" id="txt_cre_fecfac" value="<?php echo mostrar_fecha($cre_fecfac); ?>" readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="">FCH. PRORRATEO :</label>
              <div class="input-group">
                <div class='input-group date' id='datetimepicker10'>
                  <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecpro" id="txt_cre_fecpro" value="<?php echo mostrar_fecha($cre_fecpro); ?>" readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="">FECHA DE ENTREGA :</label>
              <div class="input-group">
                <div class='input-group date' id='datetimepicker11'>
                  <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecent" id="txt_cre_fecent" value="<?php echo mostrar_fecha($cre_fecent); ?>" readonly />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <p>
      </div>
    </div>
  </div>
  <!--FIN DATOS DEL CLIENTE-->
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($creditogarveh_id <= 0) : ?>
      <div id="div_credito_cronograma"></div>
    <?php endif; ?>
    <div id="div_cuota_tabla"></div>
  </div>
</div>