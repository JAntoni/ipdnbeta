<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
  require_once('../creditogarveh/Creditogarveh.class.php');

  $oCreditogarveh = new Creditogarveh();

  $action          = $_POST['action'];
  $credito_id      = $_POST['cre_id'];
  $seguimiento_id  = $_POST['seguimiento_id'];
  $tipo            = $_POST['tipo']; // 1 | 2 | 3 
  $nrotitulo       = '';
  $drawButton      = '';

  $result = $oCreditogarveh->seguimiento_titulo_mostraruno($seguimiento_id);
    if($result['estado'] == 1)
    {
      $nrotitulo         = $result['data']['tb_seguimiento_nrotitulo'];
      $fecha_reg         = $result['data']['tb_seguimiento_fecreg'];
      $fecha_mod         = $result['data']['tb_seguimiento_fecmod'];
      $credestado_titulo = $result['data']['tb_credestado_titulo_id'];
      $observacion       = $result['data']['tb_seguimiento_observacion'];
      $sede              = $result['data']['sede'];
      $fecha_mod         = date("Y-m-d", strtotime($fecha_mod));
    }
  $result = null;

  $drawButton = '<button type="button" class="btn btn-primary" onclick="update('.$seguimiento_id.','.$credito_id.')">Actualizar</button>';
?>
<style>
  .form-group.has-info .form-control, .form-group.has-info .input-group-addon {
    border-color: #079adc;
    box-shadow: none;
  }
</style>
<form id="frm_titulo_update" name="frm_titulo_update" method="post" autocomplete="off">
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_form_actualizar" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title text-blue">Actualizar Datos del Título: <?php echo $nrotitulo;?></h4>
        </div>
        <div class="modal-body">
          <p><input type="hidden" class="form-control" id="txttipotitulo" name="txttipotitulo" value="<?php echo $tipo;?>"></p>
          <p><input type="hidden" class="form-control" id="hdd_sede" name="hdd_sede" value="<?php echo $sede;?>"></p>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="txtfecha">Nro. de Título:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-file-text"></i></span>
                  </div>
                  <input type="text" id="txtnrotitulo" name="txtnrotitulo" class="form-control" maxlength="20" value="<?php echo $nrotitulo;?>">
                </div>
              </div>            
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="txtfecha">Fecha:</label>
                <input type="date" id="txtfecha" name="txtfecha" class="form-control" value="<?php echo $fecha_mod;?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-info">
                <label for="cmbestado">Estado:</label>
                  <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-circle"></i></span>
                  </div>
                  <select name="cmbestado" id="cmbestado" class="form-control" required>
                    <option value="">Seleccione:</option>
                    <option value="1"<?php if($credestado_titulo==1){echo 'selected';}?>>PRESENTADO</option>
                    <option value="2"<?php if($credestado_titulo==2){echo 'selected';}?>>REINGRESADO</option>
                    <option value="3"<?php if($credestado_titulo==3){echo 'selected';}?>>APELADO</option>
                    <option value="4"<?php if($credestado_titulo==4){echo 'selected';}?>>EN PROCESO</option>
                    <option value="5"<?php if($credestado_titulo==5){echo 'selected';}?>>EN CALIFICACIÓN</option>
                    <option value="6"<?php if($credestado_titulo==6){echo 'selected';}?>>INSCRITO</option>
                    <option value="7"<?php if($credestado_titulo==7){echo 'selected';}?>>RESERVADO</option>
                    <option value="8"<?php if($credestado_titulo==8){echo 'selected';}?>>DISTRIBUIDO</option>
                    <option value="9"<?php if($credestado_titulo==9){echo 'selected';}?>>LIQUIDADO</option>
                    <option value="10"<?php if($credestado_titulo==10){echo 'selected';}?>>PRORROGADO</option>
                    <option value="11"<?php if($credestado_titulo==11){echo 'selected';}?>>OBSERVADO</option>
                    <option value="12"<?php if($credestado_titulo==12){echo 'selected';}?>>SUSPENDIDO</option>
                    <option value="13"<?php if($credestado_titulo==13){echo 'selected';}?>>TACHADO</option>
                    <option value="14"<?php if($credestado_titulo==14){echo 'selected';}?>>ANOTADO</option>
                    <option value="15"<?php if($credestado_titulo==15){echo 'selected';}?>>CANCELADO</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-info">
                <label for="cmb_sede">Sede:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-map-marker"></i></span>
                  </div>
                  <select name="cmb_sede" id="cmb_sede" class="selectpicker form-control" required>
                    <option value="">Seleccione:</option>
                    <option value="LIMA"> LIMA</option>
                    <option value="ABANCAY"> ABANCAY</option>
                    <option value="ANDAHUAYLAS"> ANDAHUAYLAS</option>
                    <option value="AREQUIPA"> AREQUIPA</option>
                    <option value="AYACUCHO"> AYACUCHO</option>
                    <option value="BAGUA"> BAGUA</option>
                    <option value="BARRANCA"> BARRANCA</option>
                    <option value="CAJAMARCA"> CAJAMARCA</option>
                    <option value="CALLAO"> CALLAO</option>
                    <option value="CAMANA"> CAMANA</option>
                    <option value="CASMA"> CASMA</option>
                    <option value="CASTILLA_APLAO"> CASTILLA _ APLAO</option>
                    <option value="CAÑETE"> CAÑETE</option>
                    <option value="CHACHAPOYAS"> CHACHAPOYAS</option>
                    <option value="CHEPEN"> CHEPEN</option>
                    <option value="CHICLAYO"> CHICLAYO</option>
                    <option value="CHIMBOTE"> CHIMBOTE</option>
                    <option value="CHINCHA"> CHINCHA</option>
                    <option value="CHOTA"> CHOTA</option>
                    <option value="CUSCO"> CUSCO</option>
                    <option value="ESPINAR"> ESPINAR</option>
                    <option value="HUACHO"> HUACHO</option>
                    <option value="HUAMACHUCO"> HUAMACHUCO</option>
                    <option value="HUANCAVELICA"> HUANCAVELICA</option>
                    <option value="HUANCAYO"> HUANCAYO</option>
                    <option value="HUANTA"> HUANTA</option>
                    <option value="HUANUCO"> HUANUCO</option>
                    <option value="HUARAL"> HUARAL</option>
                    <option value="HUARAZ"> HUARAZ</option>
                    <option value="ICA"> ICA</option>
                    <option value="ILO"> ILO</option>
                    <option value="ISLAY _ MOLLENDO"> ISLAY _ MOLLENDO</option>
                    <option value="JAEN"> JAEN</option>
                    <option value="JUANJUI"> JUANJUI</option>
                    <option value="JULIACA"> JULIACA</option>
                    <option value="LA MERCED (SELVA CENTRAL)"> LA MERCED (SELVA CENTRAL)</option>
                    <option value="MADRE DE DIOS"> MADRE DE DIOS</option>
                    <option value="MAYNAS"> MAYNAS</option>
                    <option value="MOQUEGUA"> MOQUEGUA</option>
                    <option value="MOYOBAMBA"> MOYOBAMBA</option>
                    <option value="NAZCA"> NAZCA</option>
                    <option value="OTUZCO"> OTUZCO</option>
                    <option value="PASCO"> PASCO</option>
                    <option value="PISCO"> PISCO</option>
                    <option value="PIURA"> PIURA</option>
                    <option value="PUCALLPA"> PUCALLPA</option>
                    <option value="PUNO"> PUNO</option>
                    <option value="QUILLABAMBA"> QUILLABAMBA</option>
                    <option value="SAN PEDRO"> SAN PEDRO</option>
                    <option value="SATIPO"> SATIPO</option>
                    <option value="SICUANI"> SICUANI</option>
                    <option value="SULLANA"> SULLANA</option>
                    <option value="TACNA"> TACNA</option>
                    <option value="TARAPOTO"> TARAPOTO</option>
                    <option value="TARMA"> TARMA</option>
                    <option value="TINGO MARIA"> TINGO MARIA</option>
                    <option value="TRUJILLO"> TRUJILLO</option>
                    <option value="TUMBES"> TUMBES</option>
                    <option value="YURIMAGUAS"> YURIMAGUAS</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group has-info">
              <input type="checkbox" id="ckjustif" name="ckjustif"> &nbsp; <label for="txtobservacion">Observación:</label>
                <textarea name="txtobservacion" id="txtobservacion" class="form-control" rows="4" style="resize: vertical;" readonly></textarea> <!-- < ?php echo $observacion;?> -->
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <?php echo $drawButton;?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_titulos_form_actualizar.js?ver=040220251703'; ?>"></script>
<script> var sede = $("#hdd_sede").val(); $("#cmb_sede").val(sede); </script>