<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');
class Creditogarveh extends Conexion
{
  public $credito_id;

  public $credito_usureg;
  public $credito_usumod;
  public $cliente_id;  // se usa refinanciar 
  public $credito_tip = 1; //tipo de credito es decir un subtipo, solo es cretido menor, no hay adendas ni otros // se usa refinanciar
  public $cuotatipo_id;
  public $moneda_id; // se usa refinanciar
  public $credito_tipcam;  // se usa acuerdopago
  public $credito_preaco;
  public $credito_int;
  public $credito_numcuo;
  public $credito_numcuomax;
  public $credito_linapr;
  public $representante_id;  // se usa refinanciar
  public $cuentadeposito_id;  // se usa refinanciar
  public $credito_pla = 1; //plazo retroventa,no se usa actualmente  // se usa refinanciar
  public $credito_obs;
  public $credito_feccre;
  public $credito_fecdes;
  public $credito_comdes;  // se usa refinanciar
  public $credito_his;
  public $cuotasubperiodo_id;
  public $credito_carg;
  public $credito_fecgps;
  public $credito_fecstr;
  public $credito_fecsoat;
  public $credito_fecvenimp;
  public $credito_vent;
  public $empresa_id;
  public $credito_est;
  // SIGUIENTES ATRIBUTOS SON USADOS AL REFINANCIAR UN CREDITO FUNCION registrar_refinanciado()
  public $credito_cus; // se usa acuerdopago
  public $vehiculomarca_id;
  public $vehiculomodelo_id;
  public $vehiculoclase_id;
  public $vehiculotipo_id;
  public $credito_vehpla;
  public $credito_vehsermot;
  public $credito_vehsercha;
  public $credito_vehano;
  public $credito_vehcol;
  public $credito_vehnumpas;
  public $credito_vehnumasi;
  public $credito_vehkil;
  public $credito_vehest;
  public $credito_vehent;
  public $credito_gas;
  public $credito_vehpre;
  public $credito_fecent;
  public $credito_webref;
  public $credito_parele;
  public $credito_solreg;
  public $credito_fecsol;
  public $credito_tipus;
  public $credito_valtas;
  public $credito_valrea;
  public $credito_valgra;
  public $credito_vehmode;
  public $credito_vehcate;
  public $credito_vehcomb;
  public $credito_numtit;
  public $zonaregistral_id;
  // SIGUIENTES ATRIBUTOS SON USADOS AL REGISTRAR EL ACUERDO PAGO FUNCION  registrar_acuerdopago()
  public $credito_gps;
  public $credito_gpspre;
  public $credito_str;
  public $credito_strpre;
  public $credito_soa;
  public $credito_soapre;
  public $credito_gaspre;
  public $credito_otrpre;
  public $credito_ini;
  public $credito_fecfac;
  public $credito_fecpro;
  //ATRIBUTO USADO PARA NUM_SUMINISTRO
  public $credito_suministro;


  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_creditogarveh (
                tb_credito_xac, tb_credito_reg, tb_credito_usureg,
                tb_credito_cus, tb_cuotatipo_id, tb_cuotasubperiodo_id, tb_cliente_id, tb_moneda_id, 
                tb_credito_tipcam, tb_representante_id,tb_cuentadeposito_id, tb_vehiculomarca_id, 
                tb_vehiculomodelo_id, tb_vehiculoclase_id, tb_vehiculotipo_id, tb_credito_vehpla, 
                tb_credito_vehsermot, tb_credito_vehsercha, tb_credito_vehano, tb_credito_vehcol, 
                tb_credito_vehnumpas, tb_credito_vehnumasi, tb_credito_vehkil, tb_credito_vehest, 
                tb_credito_vehent, tb_credito_gps, tb_credito_gpspre, tb_credito_str, tb_credito_strpre, 
                tb_credito_soa, tb_credito_soapre, tb_credito_gas, tb_credito_gaspre, tb_credito_otrpre, 
                tb_credito_vehpre, tb_credito_ini, tb_credito_preaco, tb_credito_int, tb_credito_numcuo, 
                tb_credito_linapr, tb_credito_numcuomax, tb_credito_feccre, tb_credito_fecdes, 
                tb_credito_fecfac, tb_credito_fecpro, tb_credito_fecent, tb_credito_pla, tb_credito_comdes, 
                tb_credito_obs, tb_credito_webref, tb_credito_est, tb_credito_parele,  tb_credito_solreg,  
                tb_credito_fecsol,tb_credito_tipus,tb_credito_carg,tb_credito_fecgps, tb_credito_fecstr, 
                tb_credito_fecsoat, tb_credito_fecvenimp, tb_credito_tip, tb_credito_valtas, tb_credito_valrea, 
                tb_credito_valgra, tb_credito_vehmode, tb_credito_vehcate, tb_credito_vehcomb, tb_credito_vent, tb_empresa_id)
                VALUES (
                1, NOW(), :credito_usureg, 
                :credito_cus, :cuotatipo_id, :cuotasubperiodo_id, :cliente_id, :moneda_id, 
                :credito_tipcam, :representante_id, :cuentadeposito_id, :vehiculomarca_id, 
                :vehiculomodelo_id, :vehiculoclase_id, :vehiculotipo_id, :credito_vehpla, 
                :credito_vehsermot, :credito_vehsercha, :credito_vehano, :credito_vehcol, 
                :credito_vehnumpas, :credito_vehnumasi, :credito_vehkil, :credito_vehest, 
                :credito_vehent,:credito_gps, :credito_gpspre, :credito_str, :credito_strpre, 
                :credito_soa, :credito_soapre, :credito_gas, :credito_gaspre, :credito_otrpre, 
                :credito_vehpre, :credito_ini, :credito_preaco, :credito_int, :credito_numcuo, 
                :credito_linapr, :credito_numcuomax, :credito_feccre, :credito_fecdes, 
                :credito_fecfac, :credito_fecpro, :credito_fecent, :credito_pla, :credito_comdes, 
                :credito_obs, :credito_webref, :credito_est, :credito_parele, :credito_solreg, 
                :credito_fecsol, :credito_tipus, :credito_carg, :credito_fecgps, :credito_fecstr, 
                :credito_fecsoat, :credito_fecvenimp, :credito_tip, :credito_valtas, :credito_valrea, 
                :credito_valgra, :credito_vehmode, :credito_vehcate, :credito_vehcomb, :credito_vent, :empresa_id)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_usureg", $this->credito_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_cus", $this->credito_cus, PDO::PARAM_INT);
      $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cuotasubperiodo_id", $this->cuotasubperiodo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_tipcam", $this->credito_tipcam, PDO::PARAM_STR);
      $sentencia->bindParam(":representante_id", $this->representante_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculotipo_id", $this->vehiculotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehpla", $this->credito_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehsermot", $this->credito_vehsermot, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehsercha", $this->credito_vehsercha, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehano", $this->credito_vehano, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehcol", $this->credito_vehcol, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehnumpas", $this->credito_vehnumpas, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehnumasi", $this->credito_vehnumasi, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehkil", $this->credito_vehkil, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehest", $this->credito_vehest, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehent", $this->credito_vehent, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_gps", $this->credito_gps, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_gpspre", $this->credito_gpspre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_str", $this->credito_str, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_strpre", $this->credito_strpre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_soa", $this->credito_soa, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_soapre", $this->credito_soapre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_gas", $this->credito_gas, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_gaspre", $this->credito_gaspre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_otrpre", $this->credito_otrpre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehpre", $this->credito_vehpre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_ini", $this->credito_ini, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_int", $this->credito_int, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecfac", $this->credito_fecfac, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecpro", $this->credito_fecpro, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecent", $this->credito_fecent, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_pla", $this->credito_pla, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_obs", $this->credito_obs, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_webref", $this->credito_webref, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_est", $this->credito_est, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_parele", $this->credito_parele, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_solreg", $this->credito_solreg, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecsol", $this->credito_fecsol, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_tipus", $this->credito_tipus, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_carg", $this->credito_carg, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecgps", $this->credito_fecgps, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecstr", $this->credito_fecstr, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecsoat", $this->credito_fecsoat, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecvenimp", $this->credito_fecvenimp, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_tip", $this->credito_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_valtas", $this->credito_valtas, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_valrea", $this->credito_valrea, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_valgra", $this->credito_valgra, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehmode", $this->credito_vehmode, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehcate", $this->credito_vehcate, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehcomb", $this->credito_vehcomb, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vent", $this->credito_vent, PDO::PARAM_STR);
      $sentencia->bindParam(":empresa_id", $this->empresa_id, PDO::PARAM_INT);


      $result = $sentencia->execute();
      $credito_id = $this->dblink->lastInsertId();
      $this->dblink->commit();

      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['credito_id'] = $credito_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_creditogarveh SET 
        tb_credito_mod = NOW(), tb_credito_usumod =:credito_usumod, tb_credito_cus =:credito_cus,
        tb_cuotatipo_id =:cuotatipo_id,tb_cuotasubperiodo_id =:cuotasubperiodo_id, tb_cliente_id =:cliente_id,
        tb_moneda_id =:moneda_id, tb_credito_tipcam =:credito_tipcam, tb_representante_id =:representante_id,
        tb_cuentadeposito_id =:cuentadeposito_id, tb_vehiculomarca_id =:vehiculomarca_id, tb_vehiculomodelo_id =:vehiculomodelo_id,
        tb_vehiculoclase_id =:vehiculoclase_id, tb_vehiculotipo_id =:vehiculotipo_id, tb_credito_vehpla =:credito_vehpla,
        tb_credito_vehsermot =:credito_vehsermot, tb_credito_vehsercha =:credito_vehsercha, tb_credito_vehano =:credito_vehano,
        tb_credito_vehcol =:credito_vehcol, tb_credito_vehnumpas =:credito_vehnumpas, tb_credito_vehnumasi =:credito_vehnumasi,
        tb_credito_vehkil =:credito_vehkil, tb_credito_vehest =:credito_vehest, tb_credito_vehent =:credito_vehent,
        tb_credito_gps =:credito_gps, tb_credito_gpspre =:credito_gpspre, tb_credito_str =:credito_str,
        tb_credito_strpre =:credito_strpre, tb_credito_soa =:credito_soa, tb_credito_soapre =:credito_soapre,
        tb_credito_gas =:credito_gas, tb_credito_gaspre =:credito_gaspre, tb_credito_otrpre =:credito_otrpre,
        tb_credito_vehpre =:credito_vehpre, tb_credito_ini =:credito_ini, tb_credito_preaco =:credito_preaco,
        tb_credito_int =:credito_int, tb_credito_numcuo =:credito_numcuo, tb_credito_numcuomax =:credito_numcuomax,
        tb_credito_linapr =:credito_linapr, tb_credito_feccre =:credito_feccre, tb_credito_fecdes =:credito_fecdes,
        tb_credito_fecfac =:credito_fecfac, tb_credito_fecpro =:credito_fecpro, tb_credito_fecent =:credito_fecent,
        tb_credito_pla =:credito_pla, tb_credito_comdes =:credito_comdes, tb_credito_obs =:credito_obs,
        tb_credito_webref =:credito_webref, tb_credito_parele =:credito_parele,
        tb_credito_solreg =:credito_solreg, tb_credito_fecsol =:credito_fecsol, tb_credito_tipus =:credito_tipus,
        tb_credito_carg = :credito_carg, tb_credito_fecgps = :credito_fecgps, tb_credito_fecstr = :credito_fecstr,
        tb_credito_fecsoat = :credito_fecsoat, tb_credito_fecvenimp = :credito_fecvenimp ,
        tb_credito_tip = :credito_tip, tb_credito_valtas = :credito_valtas, tb_credito_valrea = :credito_valrea, 
        tb_credito_valgra = :credito_valgra, tb_credito_vehmode = :credito_vehmode, tb_credito_vehcate = :credito_vehcate, 
        tb_credito_vehcomb = :credito_vehcomb, tb_credito_vent = :credito_vent
        WHERE tb_credito_id =:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_usumod", $this->credito_usumod, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_cus", $this->credito_cus, PDO::PARAM_INT);
      $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cuotasubperiodo_id", $this->cuotasubperiodo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_tipcam", $this->credito_tipcam, PDO::PARAM_STR);
      $sentencia->bindParam(":representante_id", $this->representante_id, PDO::PARAM_INT);
      $sentencia->bindParam(":cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculotipo_id", $this->vehiculotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehpla", $this->credito_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehsermot", $this->credito_vehsermot, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehsercha", $this->credito_vehsercha, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehano", $this->credito_vehano, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehcol", $this->credito_vehcol, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehnumpas", $this->credito_vehnumpas, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehnumasi", $this->credito_vehnumasi, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehkil", $this->credito_vehkil, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_vehest", $this->credito_vehest, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehent", $this->credito_vehent, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_gps", $this->credito_gps, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_gpspre", $this->credito_gpspre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_str", $this->credito_str, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_strpre", $this->credito_strpre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_soa", $this->credito_soa, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_soapre", $this->credito_soapre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_gas", $this->credito_gas, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_gaspre", $this->credito_gaspre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_otrpre", $this->credito_otrpre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehpre", $this->credito_vehpre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_ini", $this->credito_ini, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_int", $this->credito_int, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecfac", $this->credito_fecfac, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecpro", $this->credito_fecpro, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecent", $this->credito_fecent, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_pla", $this->credito_pla, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_obs", $this->credito_obs, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_webref", $this->credito_webref, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_parele", $this->credito_parele, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_solreg", $this->credito_solreg, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecsol", $this->credito_fecsol, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_tipus", $this->credito_tipus, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_carg", $this->credito_carg, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecgps", $this->credito_fecgps, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecstr", $this->credito_fecstr, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecsoat", $this->credito_fecsoat, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_fecvenimp", $this->credito_fecvenimp, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_tip", $this->credito_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_valtas", $this->credito_valtas, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_valrea", $this->credito_valrea, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_valgra", $this->credito_valgra, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehmode", $this->credito_vehmode, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehcate", $this->credito_vehcate, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vehcomb", $this->credito_vehcomb, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_vent", $this->credito_vent, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function eliminar($credito_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "DELETE FROM tb_creditogarveh WHERE tb_credito_id =:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function mostrarUno($credito_id)
  {
    try {
      $sql = "SELECT * 
          FROM tb_creditogarveh cg
          INNER JOIN tb_cliente c ON cg.tb_cliente_id = c.tb_cliente_id
          INNER JOIN tb_moneda m  ON cg.tb_moneda_id = m.tb_moneda_id
          INNER JOIN tb_cuotatipo ct ON cg.tb_cuotatipo_id = ct.tb_cuotatipo_id
          INNER JOIN tb_representante r ON cg.tb_representante_id=r.tb_representante_id
          LEFT JOIN tb_vehiculomarca vm ON cg.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
          LEFT JOIN tb_vehiculomodelo vd ON cg.tb_vehiculomodelo_id=vd.tb_vehiculomodelo_id
          LEFT JOIN tb_vehiculotipo vt ON cg.tb_vehiculotipo_id=vt.tb_vehiculotipo_id
          LEFT JOIN tb_vehiculoclase vc ON cg.tb_vehiculoclase_id=vc.tb_vehiculoclase_id
          WHERE tb_credito_id=:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function listar_creditos()
  {
    try {
      $sql = "SELECT * FROM tb_creditogarveh garveh 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = garveh.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = garveh.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = garveh.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = garveh.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = garveh.tb_representante_id 
          WHERE tb_credito_xac = 1 ORDER BY tb_credito_id desc limit 25";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  //lista de creditos por estado
  function listar_creditos_estado($credito_est)
  {
    try {
      $sql = "SELECT * FROM tb_creditogarveh garveh 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = garveh.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = garveh.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = garveh.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = garveh.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = garveh.tb_representante_id 
          WHERE tb_credito_xac = 1 and tb_credito_est in($credito_est) ORDER BY tb_credito_id desc";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function filtro_creditos($filtro_fec1, $filtro_fec2)
  {
    try {
      $sql = "SELECT * FROM tb_creditogarveh garveh 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = garveh.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = garveh.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = garveh.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = garveh.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = garveh.tb_representante_id 
          WHERE tb_credito_xac = 1 and tb_credito_feccre BETWEEN :filtro_fec1 and :filtro_fec2 ORDER BY tb_credito_id desc";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro_fec1", $filtro_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":filtro_fec2", $filtro_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function modificar_campo($credito_id, $credito_columna, $credito_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($credito_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_creditogarveh SET " . $credito_columna . " =:credito_valor WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":credito_valor", $credito_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":credito_valor", $credito_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  //listado de cuotas facturadas
  function listar_creditos_cuotas_facturadas_subcredito_total($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est)
  {
    try {
      $sql = "SELECT 1 AS moneda, SUM(CASE WHEN tb_cuotatipo_id =3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) AS facturado
          FROM tb_creditogarveh cre 
          inner join tb_cuota cuo on cre.tb_credito_id = cuo.tb_credito_id 
          inner join tb_cuotadetalle cudet on cudet.tb_cuota_id = cuo.tb_cuota_id
          where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id = 3 and tb_credito_tip in($subcredito_id) and tb_cuotadetalle_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est) and tb_cuotadetalle_estap = 0 and cre.tb_moneda_id = 1
          UNION
          SELECT 2 AS moneda, SUM(CASE WHEN tb_cuotatipo_id =3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) AS facturado
          FROM tb_creditogarveh cre 
          inner join tb_cuota cuo on cre.tb_credito_id = cuo.tb_credito_id 
          inner join tb_cuotadetalle cudet on cudet.tb_cuota_id = cuo.tb_cuota_id
          where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id = 3 and tb_credito_tip in($subcredito_id) and tb_cuotadetalle_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est) and tb_cuotadetalle_estap = 0 and cre.tb_moneda_id = 2";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuota_fec1", $cuota_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":cuota_fec2", $cuota_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  //funcion para listar cuotas facturadas en detalle
  function listar_creditos_cuotas_facturadas_subcredito_detalle($subcredito_id, $cuota_fec1, $cuota_fec2, $credito_est)
  {
    try {
      $sql = "SELECT 
          cre.tb_credito_id, tb_credito_est, tb_credito_preaco, tb_credito_numcuo, tb_cuotadetalle_id,
          tb_cuotadetalle_fec, (CASE WHEN tb_cuotatipo_id =3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) AS tb_cuotadetalle_cuo, tb_cuotadetalle_est, cre.tb_moneda_id
          FROM tb_creditogarveh cre 
          inner join tb_cuota cuo on cre.tb_credito_id = cuo.tb_credito_id 
          inner join tb_cuotadetalle cudet on cudet.tb_cuota_id = cuo.tb_cuota_id
          where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id = 3 and tb_cuotadetalle_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est) and tb_credito_tip in ($subcredito_id) and tb_cuotadetalle_estap = 0";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cuota_fec1", $cuota_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":cuota_fec2", $cuota_fec2, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function filtro_creditos2($filtro_fec1, $filtro_fec2, $estado, $tipogarveh, $cliente_id)
  {
    if (intval($estado) > 0) {
      $concatenar = " AND garveh.tb_credito_est = :estado";
    } else {
      $concatenar = "";
    }

    if (intval($tipogarveh) > 0) {
      $concatenar1 = " AND garveh.tb_credito_tip = :tipogarveh"; //VER
    } else {
      $concatenar1 = "";
    }

    if (intval($cliente_id) > 0) {
      $concatenar2 = " AND cli.tb_cliente_id =:cliente_id"; //VER
    } else {
      $concatenar2 = "";
    }
    try {
      $sql = "SELECT * FROM tb_creditogarveh garveh 
          LEFT JOIN tb_cliente cli on cli.tb_cliente_id = garveh.tb_cliente_id 
          LEFT JOIN tb_moneda mo on mo.tb_moneda_id = garveh.tb_moneda_id 
          LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = garveh.tb_cuotatipo_id 
          LEFT JOIN tb_usuario usu on usu.tb_usuario_id = garveh.tb_credito_usureg
          LEFT JOIN tb_representante rep on rep.tb_representante_id = garveh.tb_representante_id 
          LEFT JOIN tb_vehiculomarca vm on garveh.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
          LEFT JOIN tb_vehiculomodelo vmo on garveh.tb_vehiculomodelo_id = vmo.tb_vehiculomodelo_id
          WHERE tb_credito_xac = 1 and date(tb_credito_reg) BETWEEN :filtro_fec1 and :filtro_fec2 " . $concatenar . $concatenar1 . $concatenar2 . "
          ORDER BY garveh.tb_credito_id desc";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro_fec1", $filtro_fec1, PDO::PARAM_STR);
      $sentencia->bindParam(":filtro_fec2", $filtro_fec2, PDO::PARAM_STR);
      if (intval($estado) > 0) {
        $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);
      }
      if (intval($tipogarveh) > 0) {
        $sentencia->bindParam(":tipogarveh", $tipogarveh, PDO::PARAM_INT);
      }
      if (intval($cliente_id) > 0) {
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      }

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function registro_historial($credito_id, $his)
  {
    $this->dblink->beginTransaction();

    try {

      $sql = "UPDATE tb_creditogarveh 
                    SET tb_credito_his = CONCAT(tb_credito_his, :his) 
                    WHERE tb_credito_id = :credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":his", $his, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function aprobar($credito_id, $usuario_id, $estado)
  {
    $this->dblink->beginTransaction();

    try {

      $sql = "UPDATE tb_creditogarveh 
                    SET tb_credito_apr = NOW(), tb_credito_usuapr = :usuario_id, tb_credito_est = :estado
                    WHERE tb_credito_id = :credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":estado", $estado, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function actualizar_monto_prorrateo($credito_id, $monto_pro)
  {
    $this->dblink->beginTransaction();

    try {

      $sql = "UPDATE tb_creditogarveh 
                    SET tb_credito_monpro = :monto
                    WHERE tb_credito_id = :credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":monto", $monto_pro, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_adendas_credito($cre_id)
  {
    try {
      $sql = "SELECT * FROM tb_creditogarveh where tb_credito_xac =1 and tb_credito_est IN (3, 4) and tb_credito_tip = 2 and tb_credito_idori =:cre_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $retorno["data2"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
        $retorno["data2"] = null;
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function registrar_refinanciado($cre_id, $fec_gps, $fec_str, $fec_soat, $fec_iv, $tip_ref, $subper, $tipcam, $preaco, $amor, $int, $numcuo, $linapr, $obs, $feccre, $fecdes, $fecfac, $fecpro, $usureg, $cuotip_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_creditogarveh(
                tb_credito_xac, tb_credito_reg, tb_credito_usureg, 
                tb_credito_cus, tb_cuotatipo_id, tb_cuotasubperiodo_id, 
                tb_cliente_id, tb_moneda_id, tb_credito_tipcam, 
                tb_representante_id, tb_cuentadeposito_id, tb_vehiculomarca_id, 
                tb_vehiculomodelo_id, tb_vehiculoclase_id, tb_vehiculotipo_id, 
                tb_credito_vehpla, tb_credito_vehsermot, tb_credito_vehsercha, 
                tb_credito_vehano, tb_credito_vehcol, tb_credito_vehnumpas, 
                tb_credito_vehnumasi, tb_credito_vehkil, tb_credito_vehest, 
                tb_credito_vehent, tb_credito_gps, tb_credito_gpspre, 
                tb_credito_str, tb_credito_strpre, tb_credito_soa, 
                tb_credito_soapre, tb_credito_gas, tb_credito_gaspre, 
                tb_credito_otrpre, tb_credito_fecvenimp, tb_credito_vehpre, 
                tb_credito_ini, tb_credito_preaco, tb_credito_int, 
                tb_credito_numcuo, tb_credito_numcuomax, tb_credito_linapr, 
                tb_credito_feccre, tb_credito_fecdes, tb_credito_fecfac, 
                tb_credito_fecpro, tb_credito_fecent, tb_credito_pla, 
                tb_credito_comdes, tb_credito_obs, tb_credito_webref, 
                tb_credito_est, tb_credito_parele, tb_credito_solreg, 
                tb_credito_fecsol, tb_credito_tipus, tb_credito_tip, 
                tb_credito_tip2, tb_credito_valtas, tb_credito_valrea, 
                tb_credito_valgra, tb_credito_vehmode, tb_credito_vehcate, 
                tb_credito_vehcomb, tb_credito_numtit, tb_zonaregistral_id,
                tb_credito_idori) 
                VALUES(1, NOW(), $usureg, :tb_credito_cus, 
                $cuotip_id, $subper, :tb_cliente_id, :tb_moneda_id, 
                $tipcam, :tb_representante_id, :tb_cuentadeposito_id, 
                :tb_vehiculomarca_id, :tb_vehiculomodelo_id, 
                :tb_vehiculoclase_id, :tb_vehiculotipo_id, 
                :tb_credito_vehpla, :tb_credito_vehsermot, 
                :tb_credito_vehsercha, :tb_credito_vehano, 
                :tb_credito_vehcol, :tb_credito_vehnumpas, 
                :tb_credito_vehnumasi, :tb_credito_vehkil, 
                :tb_credito_vehest, :tb_credito_vehent, '$fec_gps', 
                0, '$fec_str', 0, '$fec_soat', 0, :tb_credito_gas, 0, 
                0, '$fec_iv', :tb_credito_vehpre, $amor, $preaco, $int, 
                $numcuo, $numcuo, $linapr, '$feccre', '$fecdes', '$fecfac', 
                '$fecpro', :tb_credito_fecent, :tb_credito_pla, 
                :tb_credito_comdes, '$obs', :tb_credito_webref, 
                3, :tb_credito_parele, :tb_credito_solreg, 
                :tb_credito_fecsol, :tb_credito_tipus, 
                :tb_credito_tip, $tip_ref, :tb_credito_valtas, 
                :tb_credito_valrea, :tb_credito_valgra, 
                :tb_credito_vehmode, :tb_credito_vehcate, 
                :tb_credito_vehcomb, :tb_credito_numtit, 
                :tb_zonaregistral_id, $cre_id)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_credito_cus", $this->credito_cus, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_cliente_id", $this->cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_moneda_id", $this->moneda_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_representante_id", $this->representante_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vehiculotipo_id", $this->vehiculotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_vehpla", $this->credito_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehsermot", $this->credito_vehsermot, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehsercha", $this->credito_vehsercha, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehano", $this->credito_vehano, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_vehcol", $this->credito_vehcol, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehnumpas", $this->credito_vehnumpas, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_vehnumasi", $this->credito_vehnumasi, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_vehkil", $this->credito_vehkil, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_vehest", $this->credito_vehest, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehent", $this->credito_vehent, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_gas", $this->credito_gas, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_vehpre", $this->credito_vehpre, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_fecent", $this->credito_fecent, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_pla", $this->credito_pla, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_webref", $this->credito_webref, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_parele", $this->credito_parele, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_solreg", $this->credito_solreg, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_fecsol", $this->credito_fecsol, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_tipus", $this->credito_tipus, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_tip", $this->credito_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_credito_valtas", $this->credito_valtas, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_valrea", $this->credito_valrea, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_valgra", $this->credito_valgra, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehmode", $this->credito_vehmode, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehcate", $this->credito_vehcate, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_vehcomb", $this->credito_vehcomb, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credito_numtit", $this->credito_numtit, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_zonaregistral_id", $this->zonaregistral_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $credito_id = $this->dblink->lastInsertId();
      $this->dblink->commit();


      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['credito_id'] = $credito_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function detalle_finalidad($cre_id, $fina_par, $fec_par)
  {
    $this->dblink->beginTransaction();

    try {

      $sql = "UPDATE tb_creditogarveh 
                    set tb_credito_finpar =:fina_par, tb_credito_fecpar =:fec_par where tb_credito_id =:cre_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fina_par", $fina_par, PDO::PARAM_STR);
      $sentencia->bindParam(":fec_par", $fec_par, PDO::PARAM_STR);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function guardar_link_pdf($cre_id, $link, $his)
  {
    $this->dblink->beginTransaction();

    try {

      $sql = "UPDATE tb_creditogarveh 
                    set tb_credito_his = CONCAT(tb_credito_his, :his), 
                    tb_credito_file =:link where tb_credito_id = :cre_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":link", $link, PDO::PARAM_STR);
      $sentencia->bindParam(":his", $his, PDO::PARAM_STR);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  //    function registrar_acuerdopago($cre_id, $cre_tip, $subper, $mon_acu, $int, $num_cuo, $usureg){
  //      $this->dblink->beginTransaction();
  //      try {
  //        $sql = "INSERT INTO tb_creditogarveh(tb_credito_xac, tb_credito_reg, tb_credito_usureg, tb_credito_cus, tb_cuotatipo_id, 
  //            tb_cuotasubperiodo_id, tb_cliente_id, tb_moneda_id, tb_credito_tipcam, tb_representante_id, tb_cuentadeposito_id, 
  //            tb_vehiculomarca_id, tb_vehiculomodelo_id, tb_vehiculoclase_id, tb_vehiculotipo_id, tb_credito_vehpla,tb_credito_vehsermot, 
  //            tb_credito_vehsercha, tb_credito_vehano, tb_credito_vehcol, tb_credito_vehnumpas, tb_credito_vehnumasi, tb_credito_vehkil, 
  //            tb_credito_vehest, tb_credito_vehent, tb_credito_gps, tb_credito_gpspre, tb_credito_str, tb_credito_strpre, tb_credito_soa, 
  //            tb_credito_soapre, tb_credito_gas, tb_credito_gaspre, tb_credito_otrpre, tb_credito_vehpre, tb_credito_ini, tb_credito_preaco, 
  //            tb_credito_int, tb_credito_numcuo, tb_credito_numcuomax, tb_credito_linapr, tb_credito_feccre, tb_credito_fecdes, 
  //            tb_credito_fecfac, tb_credito_fecpro, tb_credito_fecent, tb_credito_pla, tb_credito_comdes, tb_credito_obs, tb_credito_webref, 
  //            tb_credito_est, tb_credito_parele, tb_credito_solreg, tb_credito_fecsol, tb_credito_tip, tb_credito_idori) 
  //            VALUES( 1, NOW(), :usureg, :tb_credito_cus, 4, :subper, :tb_cliente_id, :tb_moneda_id, :tb_credito_tipcam, 
  //            :tb_representante_id, :tb_cuentadeposito_id, :tb_vehiculomarca_id, :tb_vehiculomodelo_id,:tb_vehiculoclase_id, 
  //            :tb_vehiculotipo_id, :tb_credito_vehpla, :tb_credito_vehsermot, :tb_credito_vehsercha, :tb_credito_vehano, 
  //            :tb_credito_vehcol, :tb_credito_vehnumpas, :tb_credito_vehnumasi, :tb_credito_vehkil, :tb_credito_vehest, 
  //            :tb_credito_vehent, :tb_credito_gps, :tb_credito_gpspre, :tb_credito_str, :tb_credito_strpre, :tb_credito_soa, 
  //            :tb_credito_soapre, :tb_credito_gas, :tb_credito_gaspre, :tb_credito_otrpre, :tb_credito_vehpre, :tb_credito_ini, 
  //            :mon_acu, :int, $num_cuo, $num_cuo, :tb_credito_linapr, :tb_credito_feccre, :tb_credito_fecdes, :tb_credito_fecfac, 
  //            :tb_credito_fecpro, :tb_credito_fecent, :tb_credito_pla, :tb_credito_comdes,:tb_credito_obs, :tb_credito_webref, 3, 
  //            :tb_credito_parele, :tb_credito_solreg, :tb_credito_fecsol, :cre_tip, :cre_id) ";
  //
  //          $sentencia->bindParam(":credito_usureg", $this->credito_usureg, PDO::PARAM_INT);

  //        $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":credito_preaco", $this->credito_preaco, PDO::PARAM_STR);
  //        $sentencia->bindParam(":credito_numcuo", $this->credito_numcuo, PDO::PARAM_INT);
  //        $sentencia->bindParam(":credito_numcuomax", $this->credito_numcuomax, PDO::PARAM_INT);
  //        $sentencia->bindParam(":credito_his", $this->credito_his, PDO::PARAM_STR);

  //        $sentencia = $this->dblink->prepare($sql);
  //        $sentencia->bindParam(":tb_credito_cus", $this->credito_cus, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_cliente_id", $this->cliente_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_moneda_id", $this->moneda_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_tipcam", $this->credito_tipcam, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_representante_id", $this->representante_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_cuentadeposito_id", $this->cuentadeposito_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_vehiculotipo_id", $this->vehiculotipo_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_vehpla", $this->credito_vehpla, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_vehsermot", $this->credito_vehsermot, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_vehsercha", $this->credito_vehsercha, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_vehano", $this->credito_vehano, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_vehcol", $this->credito_vehcol, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_vehnumpas", $this->credito_vehnumpas, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_vehnumasi", $this->credito_vehnumasi, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_vehkil", $this->credito_vehkil, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_vehest", $this->credito_vehest, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_vehent", $this->credito_vehent, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_gps", $this->credito_gps, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_gpspre", $this->credito_gpspre, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_str", $this->credito_str, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_strpre", $this->credito_strpre, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_soa", $this->credito_soa, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_soapre", $this->credito_soapre, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_gas", $this->credito_gas, PDO::PARAM_INT); 
  //        $sentencia->bindParam(":tb_credito_gaspre", $this->credito_gaspre, PDO::PARAM_STR); 
  //        $sentencia->bindParam(":tb_credito_otrpre", $this->credito_otrpre, PDO::PARAM_STR); 
  //        $sentencia->bindParam(":tb_credito_vehpre", $this->credito_vehpre, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_ini", $this->credito_ini, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_linapr", $this->credito_linapr, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_feccre", $this->credito_feccre, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_fecdes", $this->credito_fecdes, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_fecfac", $this->credito_fecfac, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_fecpro", $this->credito_fecpro, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_fecent", $this->credito_fecent, PDO::PARAM_STR); 
  //        $sentencia->bindParam(":tb_credito_pla", $this->credito_pla, PDO::PARAM_INT);
  //        $sentencia->bindParam(":tb_credito_comdes", $this->credito_comdes, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_obs", $this->credito_obs, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_webref", $this->credito_webref, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_parele", $this->credito_parele, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_solreg", $this->credito_solreg, PDO::PARAM_STR);
  //        $sentencia->bindParam(":tb_credito_fecsol", $this->credito_fecsol, PDO::PARAM_STR);
  //        $sentencia->bindParam(":int", $int, PDO::PARAM_STR);
  //        $sentencia->bindParam(":mon_acu", $mon_acu, PDO::PARAM_STR);
  //        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
  //        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
  //        $sentencia->bindParam(":usureg", $usureg, PDO::PARAM_INT);
  //        $sentencia->bindParam(":subper", $subper, PDO::PARAM_INT);
  //        $result = $sentencia->execute();
  //        $this->dblink->commit();
  //
  //        $resultado = $sentencia->fetch();
  //
  //        $data['estado'] = $result; //si es correcto el ingreso retorna 1
  //        $data['credito_id'] = $resultado['credito_id'];
  //        return $data;
  //
  //      } catch (Exception $e) {
  //        $this->dblink->rollBack();
  //        throw $e;
  //      }
  //    }

  function registrar_acuerdopago($cre_id, $cre_tip, $subper, $mon_acu, $int, $num_cuo, $usureg)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_creditogarveh(tb_credito_xac, tb_credito_reg, tb_credito_usureg, tb_credito_cus, 
               tb_cuotatipo_id, tb_cuotasubperiodo_id, tb_cliente_id, tb_moneda_id, tb_credito_tipcam, tb_representante_id, 
               tb_cuentadeposito_id, tb_vehiculomarca_id, tb_vehiculomodelo_id, tb_vehiculoclase_id, tb_vehiculotipo_id, 
               tb_credito_vehpla, tb_credito_vehsermot, tb_credito_vehsercha, tb_credito_vehano, tb_credito_vehcol, tb_credito_vehnumpas, 
               tb_credito_vehnumasi, tb_credito_vehkil, tb_credito_vehest, tb_credito_vehent, tb_credito_gps, tb_credito_gpspre, 
               tb_credito_str, tb_credito_strpre, tb_credito_soa, tb_credito_soapre, tb_credito_gas, tb_credito_gaspre, tb_credito_otrpre, 
               tb_credito_vehpre, tb_credito_ini, tb_credito_preaco, tb_credito_int, tb_credito_numcuo, tb_credito_numcuomax, 
               tb_credito_linapr, tb_credito_feccre, tb_credito_fecdes, tb_credito_fecfac, tb_credito_fecpro, tb_credito_fecent, 
               tb_credito_pla, tb_credito_comdes, tb_credito_obs, tb_credito_webref, tb_credito_est, tb_credito_parele, tb_credito_solreg, 
               tb_credito_fecsol, tb_credito_tip, tb_credito_idori) 
               SELECT 1, NOW(), $usureg, cre.tb_credito_cus, 4, $subper, 
               cre.tb_cliente_id, cre.tb_moneda_id, cre.tb_credito_tipcam, cre.tb_representante_id, cre.tb_cuentadeposito_id, 
               cre.tb_vehiculomarca_id, cre.tb_vehiculomodelo_id, IFNULL(cre.tb_vehiculoclase_id,0), IFNULL(cre.tb_vehiculotipo_id,0), cre.tb_credito_vehpla, 
               cre.tb_credito_vehsermot, cre.tb_credito_vehsercha, cre.tb_credito_vehano, cre.tb_credito_vehcol, cre.tb_credito_vehnumpas, 
               cre.tb_credito_vehnumasi, cre.tb_credito_vehkil, cre.tb_credito_vehest, cre.tb_credito_vehent, cre.tb_credito_gps, 
               cre.tb_credito_gpspre, cre.tb_credito_str, cre.tb_credito_strpre, cre.tb_credito_soa, cre.tb_credito_soapre, cre.tb_credito_gas, 
               cre.tb_credito_gaspre, cre.tb_credito_otrpre, cre.tb_credito_vehpre, cre.tb_credito_ini, $mon_acu, $int, $num_cuo, $num_cuo, 
               cre.tb_credito_linapr, cre.tb_credito_feccre, cre.tb_credito_fecdes, cre.tb_credito_fecfac, cre.tb_credito_fecpro, 
               cre.tb_credito_fecent, cre.tb_credito_pla, cre.tb_credito_comdes,cre.tb_credito_obs, cre.tb_credito_webref, 3, 
               cre.tb_credito_parele, cre.tb_credito_solreg, cre.tb_credito_fecsol, $cre_tip, $cre_id FROM tb_creditogarveh cre where 
               cre.tb_credito_id =$cre_id";

      $sentencia = $this->dblink->prepare($sql);
      $result = $sentencia->execute();
      $credito_id = $this->dblink->lastInsertId();
      $this->dblink->commit();


      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['credito_id'] = $credito_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function registrar_adenda($cre_id, $tip_aden, $cuot_tip, $mon_id, $cambio, $preaco, $int, $numcuo, $numcuomax, $linapr, $obs, $feccre, $fecdes, $fecfac, $fecpro, $usureg, $subper, $cre_tip)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_creditogarveh(
                tb_credito_xac, tb_credito_reg, tb_credito_usureg, tb_credito_cus, tb_cuotatipo_id, tb_cuotasubperiodo_id, 
                tb_cliente_id, tb_moneda_id, tb_credito_tipcam, tb_representante_id, tb_cuentadeposito_id, tb_vehiculomarca_id, 
                tb_vehiculomodelo_id, tb_vehiculoclase_id, tb_vehiculotipo_id, tb_credito_vehpla, tb_credito_vehsermot, 
                tb_credito_vehsercha, tb_credito_vehano, tb_credito_vehcol, tb_credito_vehnumpas, tb_credito_vehnumasi, 
                tb_credito_vehkil, tb_credito_vehest, tb_credito_vehent, tb_credito_gps, tb_credito_gpspre, tb_credito_str, 
                tb_credito_strpre, tb_credito_soa, tb_credito_soapre, tb_credito_gas, tb_credito_gaspre, tb_credito_otrpre, 
                tb_credito_vehpre, tb_credito_ini, tb_credito_preaco, tb_credito_int, tb_credito_numcuo, tb_credito_numcuomax, 
                tb_credito_linapr, tb_credito_feccre, tb_credito_fecdes, tb_credito_fecfac, tb_credito_fecpro, tb_credito_fecent, 
                tb_credito_pla, tb_credito_comdes, tb_credito_obs, tb_credito_webref, tb_credito_est, tb_credito_parele, 
                tb_credito_solreg, tb_credito_fecsol, tb_credito_tip, tb_credito_tip2, tb_credito_idori,tb_zonaregistral_id, 
                tb_credito_valtas, tb_credito_valrea, tb_credito_valgra, tb_credito_tipus, tb_credito_vehmode, tb_credito_vehcomb, 
                tb_credito_vehcate) 
                SELECT 1, NOW(), $usureg, cre.tb_credito_cus, $cuot_tip, '$subper', cre.tb_cliente_id, $mon_id, $cambio, 
                cre.tb_representante_id, cre.tb_cuentadeposito_id, cre.tb_vehiculomarca_id, cre.tb_vehiculomodelo_id, 
                cre.tb_vehiculoclase_id, cre.tb_vehiculotipo_id, cre.tb_credito_vehpla, cre.tb_credito_vehsermot, 
                cre.tb_credito_vehsercha, cre.tb_credito_vehano, cre.tb_credito_vehcol, cre.tb_credito_vehnumpas, 
                cre.tb_credito_vehnumasi, cre.tb_credito_vehkil, cre.tb_credito_vehest, cre.tb_credito_vehent, 
                cre.tb_credito_gps, cre.tb_credito_gpspre, cre.tb_credito_str, cre.tb_credito_strpre, cre.tb_credito_soa, 
                cre.tb_credito_soapre, cre.tb_credito_gas, cre.tb_credito_gaspre, cre.tb_credito_otrpre, cre.tb_credito_vehpre, 
                cre.tb_credito_ini, $preaco, $int, $numcuo, $numcuomax, $linapr, '$feccre', '$fecdes', '$fecfac', '$fecpro', 
                cre.tb_credito_fecent, cre.tb_credito_pla, cre.tb_credito_comdes, '$obs', cre.tb_credito_webref, 1, 
                cre.tb_credito_parele, cre.tb_credito_solreg, cre.tb_credito_fecsol, $cre_tip, $tip_aden, $cre_id,
                cre.tb_zonaregistral_id, cre.tb_credito_valtas, cre.tb_credito_valrea, cre.tb_credito_valgra, 
                cre.tb_credito_tipus, cre.tb_credito_vehmode, cre.tb_credito_vehcomb, cre.tb_credito_vehcate 
                FROM tb_creditogarveh cre where cre.tb_credito_id =$cre_id";
      $sentencia = $this->dblink->prepare($sql);
      $result = $sentencia->execute();
      $credito_id = $this->dblink->lastInsertId();
      $this->dblink->commit();


      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['credito_id'] = $credito_id;
      return $data;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_adendas_garmob_credito($cre_id, $adenda_id)
  {
    try {
      $sql = "SELECT * FROM tb_creditogarveh 
                where tb_credito_xac =1 
                and tb_credito_est IN (3, 4, 7) 
                and tb_credito_tip = 2 
                and tb_credito_idori =:cre_id 
                and tb_credito_numescr != 0 
                and tb_credito_fecescr != '0000-00-00' 
                and tb_credito_id !=:adenda_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->bindParam(":adenda_id", $adenda_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function datos_vehiculo_por_cliente($cliente_id)
  {
    try {
      $sql = "SELECT * 
        FROM tb_creditogarveh cg
        INNER JOIN tb_cliente c ON cg.tb_cliente_id = c.tb_cliente_id
        LEFT JOIN tb_vehiculomarca vm ON cg.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
        LEFT JOIN tb_vehiculomodelo vd ON cg.tb_vehiculomodelo_id=vd.tb_vehiculomodelo_id
        LEFT JOIN tb_vehiculotipo vt ON cg.tb_vehiculotipo_id=vt.tb_vehiculotipo_id
        LEFT JOIN tb_vehiculoclase vc ON cg.tb_vehiculoclase_id=vc.tb_vehiculoclase_id
        WHERE cg.tb_cliente_id =:cliente_id AND cg.tb_vehiculomarca_id > 0 AND cg.tb_vehiculomodelo_id > 0 AND cg.tb_vehiculoclase_id > 0 LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }


  // ------------------------------------------------------------------------ //

  function proceso_titulos_listar($tb_credito_id, $tipo, $estadotitulo, $cgarvtipo_id)
  {
    try {
      $sql = "SELECT s.*, ct.tb_creditotipo_nom, cgvt.cgarvtipo_id, cgvt.cgarvtipo_nombre, cet.tb_credestado_titulo_descripcion, CONCAT(u.tb_usuario_nom,' ',u.tb_usuario_ape) as usuario, credgarv.tb_credito_vehpla,
          cli.tb_cliente_id, cli.tb_cliente_tip, cli.tb_cliente_nom, cli.tb_cliente_doc, cli.tb_cliente_emprs, cli.tb_cliente_empruc, DATEDIFF (CURRENT_DATE, tb_seguimiento_fecmod) AS diferencia_dias
        FROM tb_seguimiento s 
        INNER JOIN tb_creditotipo ct ON (s.tb_creditotipo_id = ct.tb_creditotipo_id)
        INNER JOIN cgarvtipo cgvt ON (s.tb_cgarvtipo_id = cgvt.cgarvtipo_id)
        INNER JOIN tb_credestado_titulo cet ON (s.tb_credestado_titulo_id = cet.tb_credestado_titulo_id)
        INNER JOIN tb_usuario u ON (s.tb_seguimiento_usureg = u.tb_usuario_id)
        INNER JOIN tb_creditogarveh credgarv ON (s.tb_credito_id = credgarv.tb_credito_id)
        LEFT JOIN tb_cliente cli ON (cli.tb_cliente_id = credgarv.tb_cliente_id)
        WHERE 
          s.tb_seguimiento_xac = 1
        AND 
          (case :tb_credito_id
            when 0 then
              1=1
            else
              s.tb_credito_id = :tb_credito_id 
          end)          
        AND 
          (case :p_tb_seguimiento_tipo
            when 0 then
              1=1
            else
              s.tb_seguimiento_tipo = :p_tb_seguimiento_tipo
          end)
        AND 
          (case :p_cgarvtipo_id
            when 0 then
              1=1
            else
              s.tb_cgarvtipo_id = :p_cgarvtipo_id
          end)
        AND 
          (case :p_estado_titulo
            when -1 then
              cet.tb_credestado_titulo_id != 6
            when 0 then
              1=1
            else
              cet.tb_credestado_titulo_id = :p_estado_titulo
          end)
        ORDER BY tb_seguimiento_fecreg ASC
          ";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":p_tb_seguimiento_tipo", $tipo, PDO::PARAM_INT);
      $sentencia->bindParam(":p_estado_titulo", $estadotitulo, PDO::PARAM_INT);
      $sentencia->bindParam(":p_cgarvtipo_id", $cgarvtipo_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function seguimiento_titulo_mostraruno($seg_id)
  {
    try {
      $sql = "SELECT * FROM tb_seguimiento where tb_seguimiento_xac = 1 and tb_seguimiento_id = :seg_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":seg_id", $seg_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function seguimiento_titulo_actualizar($tb_seguimiento_id,$tb_seguimiento_nrotitulo,$tb_seguimiento_fecmod,$tb_credestado_titulo_id,$tb_seguimiento_sede,$checkobservacion,$txtobservacion)
  {
    $this->dblink->beginTransaction();

    try {
      $sql = "UPDATE tb_seguimiento SET tb_seguimiento_nrotitulo = :tb_seguimiento_nrotitulo, tb_seguimiento_fecmod = :tb_seguimiento_fecmod, tb_credestado_titulo_id = :tb_credestado_titulo_id, sede = :p_sede";
              if($checkobservacion == 'si') { $sql .= ", tb_seguimiento_observacion = CONCAT(tb_seguimiento_observacion,:tb_credestado_titulo_observ)"; }
      $sql .= " WHERE tb_seguimiento_id = :tb_seguimiento_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_seguimiento_nrotitulo", $tb_seguimiento_nrotitulo, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_seguimiento_fecmod", $tb_seguimiento_fecmod, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_credestado_titulo_id", $tb_credestado_titulo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":p_sede", $tb_seguimiento_sede, PDO::PARAM_STR);
      if($checkobservacion == 'si') { $sentencia->bindParam(":tb_credestado_titulo_observ", $txtobservacion, PDO::PARAM_STR); }
      $sentencia->bindParam(":tb_seguimiento_id", $tb_seguimiento_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function seguimiento_titulo_modificar_observacion($seguimiento_id,$observacion)
  {
    $this->dblink->beginTransaction();

    try {
      $sql = "UPDATE tb_seguimiento SET tb_seguimiento_observacion = CONCAT(tb_seguimiento_observacion,:tb_credestado_titulo_observ) WHERE tb_seguimiento_id = :tb_seguimiento_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_credestado_titulo_observ", $observacion, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_seguimiento_id", $seguimiento_id, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function seguimiento_titulo_registrar($nroTitulo,$anho,$credito_id,$creditotipo_id,$tipo,$usuario_id,$justif,$fecha,$cgarvtipo_id,$cmb_sede){
    $this->dblink->beginTransaction();
    
    try
    {
      $sql = "select coalesce((select max(tb_seguimiento_id)+1 from tb_seguimiento),1) as newcod";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();
      $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
      $nuevoCodigo = $resultado["newcod"];
          
      $sql = "INSERT INTO tb_seguimiento(
        tb_seguimiento_id,
        tb_seguimiento_nrotitulo,
        tb_seguimiento_anhotitulo,
        tb_credito_id,
        tb_creditotipo_id,
        tb_cgarvtipo_id,
        tb_credestado_titulo_id,
        tb_seguimiento_observacion,
        tb_seguimiento_justificacion,
        tb_seguimiento_tipo,
        tb_seguimiento_fecreg,
        tb_seguimiento_fecmod,
        tb_seguimiento_usureg,
        tb_seguimiento_usumod,
        sede,
        tb_seguimiento_xac
      )
      VALUES (?,?,?,?,?,?,1,' ',?,?,?,?,?,null,?,1)";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(1, $nuevoCodigo, PDO::PARAM_INT);
      $sentencia->bindParam(2, $nroTitulo, PDO::PARAM_STR);
      $sentencia->bindParam(3, $anho, PDO::PARAM_INT);
      $sentencia->bindParam(4, $credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(5, $creditotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(6, $cgarvtipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(7, $justif, PDO::PARAM_STR);
      $sentencia->bindParam(8, $tipo, PDO::PARAM_INT);
      $sentencia->bindParam(9, $fecha, PDO::PARAM_STR);
      $sentencia->bindParam(10, $fecha, PDO::PARAM_STR);
      $sentencia->bindParam(11, $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(12, $cmb_sede, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    }
    catch (Exception $e) 
    {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function proceso_titulos_listar_documentos_pdf($seguimiento_id)
  {
    try {
      $sql = "SELECT * FROM filestorage WHERE modulo_subnom = :seguimiento_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":seguimiento_id", $seguimiento_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function proceso_titulos_cargar_combo_estado()
  {
    try {
      $sql = "SELECT tb_credestado_titulo_id, tb_credestado_titulo_descripcion FROM tb_credestado_titulo ORDER BY 1 ASC";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay registros";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function validar_poliza($cre_id)
  {
    try {
      $sql = "SELECT tb_poliza_id, tb_credito_id, tb_poliza_aseg FROM tb_poliza WHERE tb_credito_id = :cre_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de Polizas registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_creditos_select($cliente_id){
    try {
      $sql = "SELECT * FROM tb_creditogarveh garveh 
              WHERE tb_credito_xac = 1 AND tb_cliente_id = :cliente_id
              AND tb_credito_est in (3,4)
              ORDER BY tb_credito_id desc";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  /* GERSON (30-01-25) */
  function listar_adendas_credito_matriz($cliente_id, $placa, $nro_motor, $nro_serie)
  {
    try {
      $sql = "SELECT * 
              FROM tb_creditogarveh 
              WHERE tb_credito_xac = 1 
              AND tb_credito_est IN (3, 4) 
              AND tb_credito_tip = 2 
              AND tb_cliente_id =:cliente_id 
              AND (tb_credito_vehpla =:placa 
                  OR tb_credito_vehsermot =:nro_motor 
                  OR tb_credito_vehsercha =:nro_serie)";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":placa", $placa, PDO::PARAM_STR);
      $sentencia->bindParam(":nro_motor", $nro_motor, PDO::PARAM_STR);
      $sentencia->bindParam(":nro_serie", $nro_serie, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  function validar_registro_nrotitulo($credito_id)
  {
    try
    {
      $sql = "SELECT tb_seguimiento_id,tb_seguimiento_nrotitulo,tb_credito_id FROM tb_seguimiento WHERE tb_credito_id = ?";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(1, $credito_id, PDO::PARAM_STR);
      $sentencia->execute();
      if ($sentencia->rowCount() > 0) {
        $retorno["estado"] = 1;
        $sentencia->closeCursor();
      } else {
        $retorno["estado"] = 0;
      }
      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function validar_registro_ep($credito_id)
  {
    try
    {
      $sql = "SELECT tb_credito_id, tb_credito_est, tb_credito_fecescri,tb_credito_numescri FROM tb_creditogarveh WHERE tb_credito_id = ?";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(1, $credito_id, PDO::PARAM_STR);
      $sentencia->execute();
      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"]  = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"]    = $resultado;
        $sentencia->closeCursor();
      } else {
        $retorno["estado"]  = 0;
        $retorno["mensaje"] = "No hay resultados";
        $retorno["data"]    = "";
      }
      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
?>