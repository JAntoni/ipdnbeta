<?php
require_once('../../core/usuario_sesion.php');

require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once ("../externo/Externo.class.php");
$oExterno = new Externo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$fecha1 = fecha_mysql($_POST['txt_inicial_fec1']);
$fecha2 = fecha_mysql($_POST['txt_inicial_fec2']);
$moneda_id = intval($_POST['cmb_inicial_mon_id']);
$cuenta_id = intval($_POST['cmb_inicial_cue_id']);
$inicial_tipo = intval($_POST['cmb_tipo_ini']); //puede ser 1 efectivo en caja, 2 mediante deposito

$moneda_nom = 'S/';
if($moneda_id == 2){
	$moneda_nom = 'US$';
	$cuedep_id = '2,4';
}
if($moneda_id == 1){
	$cuedep_id = '1,3';
}

if($inicial_tipo == 1):

	$dts = $oIngreso->listar_ingresos($fecha1, $fecha2, 1,'', $moneda_id, '', '', '', '', $cuenta_id, '','' );
	?>
	
	<table cellspacing="1" id="tabla_inicial" class="table table-bordered table-hover">
		<thead>
      <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      	<th id="filacabecera"></th>
      	<th id="filacabecera">ID</th>
      	<th id="filacabecera">FECHA</th>
      	<th id="filacabecera">TIP CAM</th>
      	<th id="filacabecera">MONTO</th>
      	<th id="filacabecera">CLIENTE</th>
      	<th id="filacabecera">DETALLE</th>
    	</tr>
    </thead>
    <tbody>
			<?php 
                            if($dts['estado'] == 1){    
				foreach ($dts['data'] as $key => $dt) {
					$tipo_cambio = '';
					$dts2 = $oMonedacambio->consultar($dt['tb_ingreso_fec']);
						if($dts2['estado'] == 1){   
							$tipo_cambio = $dts2['data']['tb_monedacambio_val'];
						}
					$dts2 = NULL;

					$bandera = '';
					if(floatval($tipo_cambio) <= 0){
						$bandera = '<span style="color: red;">REGISTRE</span>';
					}
					?>
					<tr class="filaTabla">
						<td id="fila" style="border-left: 1px solid #135896;"><input type="radio" name="ra_inicial_id" class="ra_inicial_id" value="<?php echo $dt['tb_ingreso_id'];?>"></td>
						<td id="fila"><?php echo $dt['tb_ingreso_id'];?></td>
                                                <td id="fila"><?php echo mostrar_fecha($dt['tb_ingreso_fec']);?></td>
						<td id="fila"><?php echo $tipo_cambio.''.$bandera;?></td>
						<td id="fila"><?php echo $moneda_nom.' '.formato_moneda($dt['tb_ingreso_imp']);?></td>
						<td id="fila"><?php echo $dt['tb_cliente_nom'];?></td>
						<td id="fila"><?php echo $dt['tb_ingreso_det'];?></td>

						<input type="hidden" <?php echo 'id="hdd_fec_'.$dt['tb_ingreso_id'].'" value="'.mostrar_fecha($dt['tb_ingreso_fec']).'"';?>>
						<input type="hidden" <?php echo 'id="hdd_camb_'.$dt['tb_ingreso_id'].'" value="'.$tipo_cambio.'"';?>>
						<input type="hidden" <?php echo 'id="hdd_imp_'.$dt['tb_ingreso_id'].'" value="'.$dt['tb_ingreso_imp'].'"';?>>
						<input type="hidden" <?php echo 'id="hdd_det_'.$dt['tb_ingreso_id'].'" value="'.$dt['tb_ingreso_det'].'"';?>>
					</tr>
					<?php
				}
                            }
                            $dts = NULL;
			?>
    </tbody>
	</table>
<?php endif;?>

<?php if($inicial_tipo == 2):
	$dts = $oDeposito->mostrar_fecha_deposito($fecha1, $fecha2, $cuedep_id);
	?>
	<table cellspacing="1" id="tabla_inicial" class="table table-bordered table-hover">
		<thead>
      <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      	<th id="filacabecera"></th>
      	<th id="filacabecera">ID</th>
      	<th id="filacabecera">FECHA</th>
      	<th id="filacabecera">TIP CAM</th>
      	<th id="filacabecera">MONTO</th>
      	<th id="filacabecera">CLIENTE</th>
      	<th id="filacabecera">DETALLE</th>
    	</tr>
    </thead>
    <tbody>
			<?php 
                            if($dts['estado'] == 1){    
				foreach ($dts['data'] as $key => $dt) {
					$tipo_cambio = '';
					$dts2 = $oMonedacambio->consultar($dt['tb_deposito_fec']);
						if($dts2['estado'] == 1){ 
							$tipo_cambio = $dts2['data']['tb_monedacambio_val'];
						}
					$dts2 = NULL;

					$bandera = '';
					if(floatval($tipo_cambio) <= 0){
						$bandera = '<span style="color: red;">REGISTRE</span>';
					}
					$deposito_det = $dt['tb_deposito_des'].' | N°: '.$dt['tb_deposito_num'];
					?>
					<tr class="filaTabla">
						<td id="fila" style="border-left: 1px solid #135896;"><input type="radio" name="ra_inicial_id" class="ra_inicial_id" value="<?php echo $dt['tb_deposito_id'];?>"></td>
						<td id="fila"><?php echo $dt['tb_deposito_id'];?></td>
						<td id="fila"><?php echo mostrar_fecha($dt['tb_deposito_fec']);?></td>
						<td id="fila"><?php echo $tipo_cambio.''.$bandera;?></td>
						<td id="fila"><?php echo $moneda_nom.' '.formato_moneda($dt['tb_deposito_mon']);?></td>
						<td id="fila"><?php echo $dt['tb_cuentadeposito_nom'];?></td>
						<td id="fila"><?php echo $deposito_det;?></td>

						<input type="hidden" <?php echo 'id="hdd_fec_'.$dt['tb_deposito_id'].'" value="'.mostrar_fecha($dt['tb_deposito_fec']).'"';?>>
						<input type="hidden" <?php echo 'id="hdd_camb_'.$dt['tb_deposito_id'].'" value="'.$tipo_cambio.'"';?>>
						<input type="hidden" <?php echo 'id="hdd_imp_'.$dt['tb_deposito_id'].'" value="'.$dt['tb_deposito_mon'].'"';?>>
						<input type="hidden" <?php echo 'id="hdd_det_'.$dt['tb_deposito_id'].'" value="'.$deposito_det.'"';?>>
					</tr>
					<?php
				}
                            }
                            $dts = NULL;
			?>
    </tbody>
	</table>
<?php endif;?>

<?php if($inicial_tipo == 3):
	$dts = $oExterno->mostrarTodos($fecha1, $fecha2);
	?>
	<table cellspacing="1" id="tabla_inicial" class="table table-bordered table-hover">
		<thead>
      <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      	<th id="filacabecera"></th>
      	<th id="filacabecera">ID</th>
      	<th id="filacabecera">FECHA</th>
      	<th id="filacabecera">TIP CAM</th>
      	<th id="filacabecera">MONTO</th>
      	<th id="filacabecera">CLIENTE</th>
      	<th id="filacabecera">DETALLE</th>
    	</tr>
    </thead>
    <tbody>
			<?php 
                            if($dts['estado'] == 1){ 
				foreach ($dts['data'] as $key => $dt) {
					$tipo_cambio = '';
					/*$dts2 = $oMonedacambio->consultar($dt['tb_externo_fec']);
						if(mysql_num_rows($dts2) > 0){
							$dt2 = mysql_fetch_array($dts2);
							$tipo_cambio = $dt2['tb_monedacambio_val'];
						}
					$dts2 = NULL;*/
					$tipo_cambio = $dt['tb_externo_tipcam'];
					$bandera = '';
					if(floatval($tipo_cambio) <= 0){
						$bandera = '<span style="color: red;">REGISTRE</span>';
					}
					$externo_det = $dt['tb_externo_des'];
					$forma = 'INICIAL EN EFECTIVO';
					if(intval($dt['tb_externo_tip']) == 2){
						$forma = 'INICIAL EN DEPÓSITO';
						$externo_det = $dt['tb_externo_des'].', BANCO: '.$dt['tb_externo_ban'].' | N°: '.$dt['tb_externo_num'];
					}
					?>
					<tr class="filaTabla">
						<td id="fila" style="border-left: 1px solid #135896;"><input type="radio" name="ra_inicial_id" class="ra_inicial_id" value="<?php echo $dt['tb_externo_id'];?>"></td>
						<td id="fila"><?php echo $dt['tb_externo_id'];?></td>
						<td id="fila"><?php echo mostrar_fecha($dt['tb_externo_fec']);?></td>
						<td id="fila"><?php echo $tipo_cambio.''.$bandera;?></td>
						<td id="fila"><?php echo $moneda_nom.' '.formato_moneda($dt['tb_externo_mon']);?></td>
						<td id="fila"><?php echo $forma;?></td>
						<td id="fila"><?php echo $externo_det;?></td>

						<input type="hidden" <?php echo 'id="hdd_fec_'.$dt['tb_externo_id'].'" value="'.mostrar_fecha($dt['tb_externo_fec']).'"';?>>
						<input type="hidden" <?php echo 'id="hdd_camb_'.$dt['tb_externo_id'].'" value="'.$tipo_cambio.'"';?>>
						<input type="hidden" <?php echo 'id="hdd_imp_'.$dt['tb_externo_id'].'" value="'.$dt['tb_externo_mon'].'"';?>>
						<input type="hidden" <?php echo 'id="hdd_det_'.$dt['tb_externo_id'].'" value="'.$externo_det.'"';?>>
					</tr>
					<?php
				}
                            }
                            $dts = NULL;
			?>
    </tbody>
	</table>
<?php endif;?>

<script type="text/javascript" src="<?php echo 'vista/creditogarveh/credito_inicial_tabla.js'?>"></script>
