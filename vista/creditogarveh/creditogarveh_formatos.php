<?php 
  $url_ipdnsac = 'https://www.ipdnsac.com/app/modulos/creditogarveh/';
  // $cre_tip => 1 compra venta, 2 adenda, 4 grantia mobiliaria
  // $cre_subgar => REGULAR, GARMOB COMPRA TERCERO, GARMOB IPDN VENTA, GARMOB IPDN VENTA CON RD, PRE-CONSTITUCION
  // $cre_cus => 1 con custodia, 2 sin custodia
?>
<?php if ($_POST['action'] == 'M' || $_POST['action'] == 'L'): ?>
  <div class="row">
    <!--DATOS DE FORMATOS-->
    <div class="col-md-12">
      <h4><?php echo 'Este crédito es: '.$cre_subgar;?></h4>
      <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> IMPRIMIR FORMATOS </label>
      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
            <!-- DOCUMENTOS PARA CREDITO GARANTIA MOBILIARIA -->
            <?php if($cre_tip == 4):?>
              <!-- CUANDO EL CREDITO ES REGULAR con CUSTODIA-->
              <?php if($cre_subgar == 'REGULAR' && $cre_cus == 1):?>
                <a class="btn btn-warning btn-sm"  target="_blank"  href="<?php echo 'vista/doc_creditogarveh/doc_gar_mobiliaria_concus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> BETA MOB CON C.</a>
                <a class="btn btn-danger btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_gar_mobiliaria_concus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> MOB CON C.</a>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_mob_concus_acta.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> ACTA ENTREGA</a>
              <?php endif;?>

              <!-- CUANDO EL CREDITO ES REGULAR sin CUSTODIA-->
              <?php if($cre_subgar == 'REGULAR' && $cre_cus == 2):?>
                <a class="btn btn-warning btn-sm"  target="_blank"  href="<?php echo 'vista/doc_creditogarveh/doc_gar_mobiliaria_sincus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> BETA MOB SIN C.</a>
                <a class="btn btn-danger btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_gar_mobiliaria_sincus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> MOB SIN C.</a>
              <?php endif;?>

              <!-- CUANDO EL CREDITO ES: GARMOB COMPRA TERCERO-->
              <?php if($cre_subgar == 'GARMOB COMPRA TERCERO' || $cre_subgar == 'GARMOB IPDN VENTA' || $cre_subgar == 'GARMOB IPDN VENTA CON RD'):?>
                <a class="btn btn-warning btn-sm"  target="_blank"  href="<?php echo 'vista/doc_creditogarveh/doc_gar_mobiliaria_sincus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> BETA MOB SIN C.</a>
                <a class="btn btn-danger btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_gar_mobiliaria_sincus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i><?php echo ' '.$cre_subgar;?></a>
              <?php endif;?>
              
              <?php if($cre_subgar == 'GARMOB IPDN VENTA CON RD'):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_gar_mobiliaria_reconocimiento.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> RECONOCIMIENTO D.</a>
              <?php endif;?>

              <!-- CUANDO EL CREDITO ES: PRE-CONSTITUCION-->
              <?php if($cre_subgar == 'PRE-CONSTITUCION'):
                $fec_nuevo_cont = '07-08-2020'; //desde esta fecha parte los nuevos contratos de PRE-
                ?>
                <!-- PRE CONSTITUCIONES ANTIGUAS AL 07-08-2020-->
                <?php if(strtotime($cre_feccre) < strtotime($fec_nuevo_cont)): ?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_preconstitucion.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> PRE-CONSTITUCION</a>
                <?php endif;?>

                <!-- PRE CONSTITUCIONES MAYORES AL 07-08-2020-->
                <?php if(strtotime($cre_feccre) > strtotime($fec_nuevo_cont)): ?>
                  <a class="btn btn-warning btn-sm"  target="_blank"  href="<?php echo 'vista/doc_creditogarveh/doc_preconstitucion.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> BETA PRE-CONSTITUCION</a>
                  <a class="btn btn-danger btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_preconstitucion_nuevo.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> PRE-CONSTITUCION</a>
                <?php endif;?>
              <?php endif;?>

            <?php endif;?>
            
            <!-- CONTRATO PARA LAS ADENDAS-->
            <?php if($cre_tip == 2):?>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_adenda_garmob.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> ADENDA GarMob</a>
            <?php endif;?>

            <!-- CONTRATOS IGUALES PARA TODOS-->
            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_simulador.php?d1='.$creditogarveh_id;?>"><i class="fa fa-print"></i> CRONOGRAMA</a>
            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_mobiliaria_anexo02.php?d1='.$creditogarveh_id;?>"><i class="fa fa-print"></i> HOJA RESUMEN</a>
            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_anexo_05.php?d1='.$creditogarveh_id;?>"><i class="fa fa-print"></i> PROTOCOLO ROBO</a>
            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'vista/doc_creditogarveh/doc_letras_cambio.php?d1='.$creditogarveh_id;?>"><i class="fa fa-print"></i> LETRAS DE CAMBIO</a>

            <?php if($cre_subgar == 'PRE-CONSTITUCION'):?>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_poder_llave.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> PODER PARA LLAVE</a>
            <?php endif;?>

            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_poder_general.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> PODER PARA MUNI</a>
            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'pagare_gar_mob.pdf';?>"><i class="fa fa-print"></i> PAGARÉ</a>
            <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_cronograma_resolver.php?d1='.$creditogarveh_id.'&fecha=no';?>"><i class="fa fa-print"></i> RESOLVER CREDITO</a>
            
            <?php if($credito_tip2 == 5 || $credito_tip2 == 6):?>
              <a class="btn btn-success btn-sm"  target="_blank"  href="<?php echo $url_ipdnsac.'doc_amortizacion_mutuo.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> FORMATO AMORTIZACION</a>
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($_POST['action'] == 'M'): ?>
  <div class="row">
    <!--DATOS DE ADJUNTOS-->
    <div class="col-md-12">
      <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> ADJUNTOS </label>
      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
            <a class="btn btn-primary btn-sm" href="javascript:void(0)" onClick="credito_file_form();"><i class="fa fa-plus"></i> Subir Imagen</a>
            <div id="div_credito_file_tabla"></div>	
          </div>
        </div>
      </div>
    </div>
      <!--FIN DATOS DEL CLIENTE-->
  </div>
<?php endif; ?>
