<?php
  $credito_id = $_POST['credito_id'];
  $hoy = date('d-m-Y');

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_placa" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Datos Restantes del Crédito</h4>
      </div>
      <form id="frm_creditoplaca">
        <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
	<input type="hidden" name="action" value="placa">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_veh_pla" class="control-label">Placa :</label>
                              <input type="text" name="txt_veh_pla" id="txt_veh_pla" class="form-control input-sm mayus" value="<?php echo $veh_pla;?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_veh_parele" class="control-label">Partida Electronica :</label>
                              <input type="text" name="txt_veh_parele" id="txt_veh_parele" class="form-control input-sm mayus" value="<?php echo $veh_parele;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_veh_solreg" class="control-label">Solicitud Registral :</label>
                              <input type="text" name="txt_veh_solreg" id="txt_veh_solreg" class="form-control input-sm mayus" value="<?php echo $veh_solreg;?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_veh_solreg" class="control-label">Fec. Solicitud Reg :</label>
                              <div class='input-group date' id='datetimepicker3'>
                                  <input type='text' class="form-control" name="txt_veh_fecsol" id="txt_veh_fecsol" value="<?php echo $hoy;?>"/>
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" id="btn_guardar_creditogarveh" onclick="guardarplaca()">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_creditoplaca" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_placa.js?ver=437433';?>"></script>
