<?php 
  $fecha = new DateTime();
  
  $cajacambio_fec1 = date('01-01-Y');
  $cajacambio_fec2 = date('d-m-Y');
  $usuario_columna = 'tb_empresa_id';
  $empresa_id = $_SESSION['empresa_id'];
  $usuario_valor = $empresa_id;
  $param_tip = 'INT';
?>
<form id="form_creditogarveh_filtro" role="form">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Cliente : </label>
                <input type="text" id="txt_creditogarveh_fil_cli_nom"  class="form-control input-sm mayus" placeholder="ingrese cliente a buscar" size=45>
                <input type="hidden" id="hdd_creditogarveh_fil_cli_id">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Fecha :</label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo $cajacambio_fec1; ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo $cajacambio_fec2; ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Estado :</label>
                <br>
                <select class="form-control input-sm" name="cbo_estado" id="cbo_estado">
                    <option value="">- </option>
                    <option value="1">PENDIENTE</option>
                    <option value="2">APROBADO</option>
                    <option value="3">VIGENTE</option>
                    <option value="4">PARALIZADO</option>
                    <option value="7">LIQUIDADO</option>
                    <option value="8">RESUELTO</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Tipo :</label>
                <br>
                <select class="form-control input-sm" name="cbo_tipogarveh" id="cbo_tipogarveh">
                    <option value="">- </option>
                    <option value="1">COMPRA VENTA</option>
                    <option value="4">GARANTIA MOB</option>
                    <option value="2">ADENDAS</option>
                    <option value="3">ACUERDO DE PAGO</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-4">
            <?php if($_SESSION['usuariogrupo_id'] == 2): ?>
                <div class="form-group">
                    <label for="">Opciones credito :</label>
                    <br>
                    <select class="form-control input-sm" name="cmb_fil_opc_cre" id="cmb_fil_opc_cre">
                        <option value="">-</option>
                        <option value="3">Vigente</option>
                        <option value="4">Paralizar Crédito</option>
                        <option value="5">Refinanciar / Liquidar</option>
                        <!--option value="6">Refinanciado sin Amortizar</option>
                        <option value="7">Liquidado</option-->
                        <option value="8">Resolver Crédito</option>
                        <option value="9">Renovar Cuotas</option>
                        <option value="10">Refinanciar Múltiple</option>
                        <option value="11"><b>FORMATO DE LIQUIDACION</b></option>
                    </select>
                </div>
            <?php endif;?>
            <?php if($_SESSION['usuariogrupo_id'] != 2): ?>
                <div class="form-group">
                    <label for="">Opciones credito :</label>
                    <br>
                    <select class="form-control input-sm" name="cmb_fil_opc_cre" id="cmb_fil_opc_cre">
                        <option value="0">Selecciona aquí...</option>
                        <option value="11"><b>FORMATO DE LIQUIDACION</b></option>
                    </select>
                </div>
            <?php endif;?> 
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="">Sede :</label>
                <br>
                <select class="form-control input-sm" name="cmb_sede_id" id="cmb_sede_id">
                    <?php require_once('vista/empresa/empresa_select.php');?>
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="cmb_usuario_id">Cambiar asesor :</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                    </span>
                    <select id="cmb_usuario_id" name="cmb_usuario_id" class="selectpicker form-control" data-live-search="true" data-max-options="1" data-size="12">
                        <?php require_once('vista/usuario/usuario_select.php');?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</form>