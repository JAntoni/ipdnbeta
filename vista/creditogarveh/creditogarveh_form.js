$(document).ready(function () {
  $("#btn_guardar_creditogarveh").css("visibility", "hidden");
  var action = $("#action_credito").val();
  var adenda = $("#hdd_aden_id").val();
  
  console.log('cambis 1111 14-09');

  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });

  $('#che_gps').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-yellow'
  });

  $('#che_gps').on('ifChanged', function(event){
    // Obtener el estado actual del checkbox
    var isChecked = $(this).prop('checked');
    credito_cronograma();

    if (isChecked) {
      gps_form_simple();
    }
  });


  $(".opcional").hide();
  $("#cmb_cuotip_id, #txt_cre_numcuo, #cmb_cuosubper_id, #txt_cre_int").change(
    function (e) {
      creditotipo();
    }
  );
  $("#cmb_credito_tip").change(function (e) {
    datos_mobiliaria();

  });
  $("#cmb_rep_id").change(function (e) {
    datos_mobiliaria();
  });

  $("#cmb_vehcla_id_2").change(function () {
    cargar_tipo($(this).val());
  });
  $("#cmb_vehmar_id_2").change(function () {
    cargar_modelo($(this).val());
  });

  $("#cmb_mon_id").change(function (e) {
    //monedacambio_boton();
    cambio_moneda($(this).val());
  });

  $(
    "#panel1, #panel2, #panel3, #panel4, #panel5, #panel6, #panel7, #panel8, #panel10"
  ).on("click", function () {
    $captura = $(this).text();
    if ($captura == "CRÉDITO") {
      $("#btn_guardar_creditogarveh").css("visibility", "visible");
    }
    if ($captura != "CRÉDITO") {
      //console.log("NO");
      $("#btn_guardar_creditogarveh").css("visibility", "hidden");
    }

    if ($captura == "CHECKLIST") {
      cargardocumentosnecesarios();
    }

    if ($captura == "DOCS POST CIERRE") {
      cargar_docpostcierre();
      listar_docs_post_cierre();
    }
  });

  $("#txt_cre_valtas").change(function (e) {
    var cre_valtas = Number($("#txt_cre_valtas").autoNumeric("get"));
    var tip_cam = Number($("#txt_cre_tipcam").val());
    var rea = parseFloat((cre_valtas * 90) / 100);
    //console.log(cre_valtas);
    //console.log(tip_cam);
    //console.log(rea);
    //rea = parseFloat(rea).toFixed(2);
    $("#txt_cre_valrea").autoNumeric("set", rea.toFixed(2));
    $("#txt_cre_valgra").autoNumeric("set", rea.toFixed(2));
  });
  $("#txt_cre_valtas, #txt_cre_tc").change(function (e) {
    var cre_valtas = Number($("#txt_cre_valtas").autoNumeric("get"));
    var tip_cam = Number($("#txt_cre_tc").val());
    var cmbforma = parseInt($("#cmb_credito_subgar").val());
    var vehsoles = parseFloat(tip_cam * cre_valtas);
    if (cmbforma == 4) {
      $("#txt_veh_pre").autoNumeric("set", vehsoles.toFixed(2));
      $("#txt_cre_tc_2").autoNumeric("set", tip_cam);
    }
  });

  $("#txt_cre_fecdes").change(function () {
    cambio_moneda($("#cmb_mon_id").val());
  });

  $("#cmb_tip_aden").change(function (event) {
    var tip = $(this).val();
    if (tip == 4) {
      $("#div_cuo_res").show();
      $("#txt_cre_numcuo").val(1);
    } else $("#div_cuo_res").hide();

    credito_calculo();
  });

  $("#txt_cre_preaco").change(function (event) {
    credito_calculo();
  });
  
  // 22-09-2023 ANTONIO
  $("#txt_credito_monedache, #txt_cre_tc_2").change(function (event) {
    var monedades_id = parseInt($('#txt_credito_monedache').val());
    var cre_tc_2 = Number($("#txt_cre_tc_2").autoNumeric("get"));
    var moneda_principal_id = parseInt($('#cmb_mon_id').val());
    var monto_prestar = Number($("#txt_cre_preaco").autoNumeric("get"));
    var monto_cambio = monto_prestar;

    if(moneda_principal_id == 1 && monedades_id == 2){
      //? EL CRONOGRAMA SERÁ EN SOLES, PERO EL CHEQUE DE GERENCIA SERÁ EN DOLARES
      monto_cambio = monto_prestar / cre_tc_2;
    }
    if(moneda_principal_id == 2 && monedades_id == 1){
      //* EL CRONOGRAMA SERÁ EN DOLARES, PERO EL CHEQUE DE GERENCIA SERÁ EN SOLES
      monto_cambio = monto_prestar * cre_tc_2;
    }
    console.log('moneda des: ' + monedades_id + ' / tc2: ' + cre_tc_2 + ' // mon_pri: ' + moneda_principal_id + ' // cambio: ' + monto_cambio)
    $('#txt_credito_montoche').autoNumeric('set', monto_cambio.toFixed(2))
  });

  if (adenda > 0) {
    cambio_moneda($("#cmb_mon_id").val());
  }

  if (action == "I") {
    $("#td_cambio").hide();
    $("#txt_ven_cli_nom").focus();
    $("#cmb_cuotip_id").val(3);
    //$("#cmb_custodia_id").val(1);
    //console.log(action);
    tipo_credito(); //para identificar si es adenda o no
    creditotipo(); //para mostrar el tipo de subperiodo y num cuotas

    datos_mobiliaria(); // CARGAR INPUTS DE TASACIÓN

    $("#cmb_credito_subgar").change();
    //        garvehtipo();
    credito_calculo();

    //monedacambio_boton();
    cambio_moneda($("#cmb_mon_id").val());

    $("#cmb_mon_id").change(function (e) {
      cargar_gps($("#hdd_gps_id").val(), "3");
      cargar_str($("#hdd_str_id").val(), "2");
      cargar_soat($("#hdd_soa_id").val(), "1");
      cargar_gas($("#hdd_gas_id").val(), "4");
    });

    
    $(
      "#txt_otr_pre,#txt_cre_ini,#txt_veh_pre,#txt_cre_preaco, #txt_cre_int, #txt_cre_numcuo, #txt_cre_numcuomax, #cmb_mon_id, #txt_cre_fecfac, #txt_cre_fecpro, #txt_cre_fecdes"
    ).change(function (e) {
      precioacordado_calculo();
    });

    $("#txt_ven_cli_nom").change(function () {
      $(this).val($(this).val().toUpperCase());
    });
    $("#txt_veh_mot, #txt_veh_cha, #txt_veh_pla, #txt_veh_col").change(
      function () {
        $(this).val($(this).val().toUpperCase());
      }
    );
  }

  if (action == "M" || action == "L") {
    credito_file_tabla();
    var valor = $("#cmb_credito_subgar").val();
    if (valor == 4) {
      $("#td_cambio").show();
    } else {
      $("#td_cambio").hide();
    }
    //---------------------------------------------------------

    $(
      "#txt_otr_pre,#txt_cre_ini,#txt_veh_pre,#txt_cre_preaco, #txt_cre_int, #txt_cre_numcuo, #txt_cre_numcuomax, #cmb_mon_id"
    ).change(function (e) {
      precioacordado_calculo();
    });
    //---------------------------------------------------------
    //credito_file_tabla();
    if ($("#txt_cre_fecfac").val() == "") {
      credito_calculo();
    } else {
      cuota_tabla();
    }
  }

  //AUTOCOMNPLETE PARA CLIENTE
  $("#txt_ven_cli_doc").focus();
  $("#txt_ven_cli_doc").autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        { term: request.term }, //
        response
      );
    },
    select: function (event, ui) {
      $("#hdd_cre_cli_id").val(ui.item.cliente_id);
      $("#hdd_cre_cli_id_ori").val(ui.item.cliente_id);
      $("#txt_ven_cli_doc").val(ui.item.cliente_doc);
      $("#txt_ven_cli_nom").val(ui.item.cliente_nom);
      $("#txt_ven_cli_dir").val(ui.item.cliente_dir);
      $("#txt_ven_cli_fecnac").val(ui.item.cliente_fecnac);
      $("#txt_ven_cli_ema").val(ui.item.cliente_ema);
      $("#txt_ven_cli_tel").val(ui.item.cliente_tel);
      $("#txt_ven_cli_cel").val(ui.item.cliente_cel);
      $("#txt_ven_cli_telref").val(ui.item.cliente_telref);
      $("#hdd_ven_cli_tip").val(ui.item.cliente_tip);
      //vencimiento_tabla();
      event.preventDefault();
      $("#txt_ven_cli_nom").focus();
    },
  });

  $("#txt_ven_cli_nom").autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        { term: request.term }, //
        response
      );
    },
    select: function (event, ui) {
      $("#hdd_cre_cli_id").val(ui.item.cliente_id);
      $("#hdd_cre_cli_id_ori").val(ui.item.cliente_id);
      $("#txt_ven_cli_doc").val(ui.item.cliente_doc);
      $("#txt_ven_cli_nom").val(ui.item.cliente_nom);
      $("#txt_ven_cli_dir").val(ui.item.cliente_dir);
      $("#txt_ven_cli_fecnac").val(ui.item.cliente_fecnac);
      $("#txt_ven_cli_ema").val(ui.item.cliente_ema);
      $("#txt_ven_cli_tel").val(ui.item.cliente_tel);
      $("#txt_ven_cli_cel").val(ui.item.cliente_cel);
      $("#txt_ven_cli_telref").val(ui.item.cliente_telref);
      $("#hdd_ven_cli_tip").val(ui.item.cliente_tip);
      //vencimiento_tabla();
      event.preventDefault();
      $("#txt_ven_cli_nom").focus();
    },
  });

  //AUTOCOMPLETE PARA TRANSFERENTE
  $("#txt_transferente_fil_nom").focus();
  $("#txt_transferente_fil_doc").autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "transferente/transferente_autocomplete.php",
        { term: request.term }, //
        response
      );
    },
    select: function (event, ui) {
      //$('#hdd_cre_cli_id').val(ui.item.cliente_id);
      //$('#hdd_cre_cli_id_ori').val(ui.item.cliente_id);
      $("#txt_transferente_fil_doc").val(ui.item.transferente_doc);
      $("#txt_transferente_fil_nom").val(ui.item.transferente_nom);
      $("#hdd_transferente_id").val(ui.item.transferente_id);

      //vencimiento_tabla();
      event.preventDefault();
      $("#txt_transferente_fil_nom").focus();
    },
  });

  $("#txt_transferente_fil_nom").autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "transferente/transferente_autocomplete.php",
        { term: request.term }, //
        response
      );
    },
    select: function (event, ui) {
      //$('#hdd_cre_cli_id').val(ui.item.cliente_id);
      //$('#hdd_cre_cli_id_ori').val(ui.item.cliente_id);
      $("#txt_transferente_fil_doc").val(ui.item.transferente_doc);
      $("#txt_transferente_fil_nom").val(ui.item.transferente_nom);

      //vencimiento_tabla();
      event.preventDefault();
      $("#txt_transferente_fil_nom").focus();
    },
  });

  $("#cmb_credito_subgar").change(function (event) {
    var subgar = $(this).val();
    //console.log(subgar);
    if (subgar == 2 || subgar == 4) {
      $("#fil_transferente").show(400);
      $("#fil_inicial").show(400);
      $(".opcional").hide(100);
    } else if (subgar == 5) {
      $("#fil_transferente").show(400);
      $("#fil_inicial").show(400);
      var gar_mobiliaria = parseInt($("#cmb_credito_tip").val());
      //console.log(gar_mobiliaria);
      if (gar_mobiliaria == 1) {
        $(".opcional").hide(100);
      }
      if (gar_mobiliaria == 4) {
        $(".opcional").show();
      }
    } else if (subgar == 3) {
      $(".opcional").hide(100);
      $("#fil_inicial").show(400);
      $("#fil_transferente").hide(400);
    } else {
      $("#fil_transferente").hide(400);
      $("#fil_inicial").hide(400);
      $(".opcional").hide(100);
    }

    if (subgar == 4) {
      $("#cmb_custodia_id").val(2);
      $("#cmb_custodia_id").change();

      $("#cmb_zona_id").val(1);
      $("#label_valor").text("Valor Adquisición USD$:");
      $("#tr_cargas").hide(400);
      $("#cmb_cuotip_id").val(4);
      $("#cmb_cuotip_id").change();

      $("#tbl_partidas").hide(400);
      $("#cmb_rep_id").val(8);
      $("#td_cambio").show();
      
    } else {
      $("#td_cambio").hide();
      $("#cmb_custodia_id").val(1);
      $("#cmb_custodia_id").change();

      //$('#cmb_zona_id').val(null);
      $("#label_valor").text("Valor Tasación USD$:");
      $("#tr_cargas").show(400);

      $("#cmb_cuotip_id").val(3);
      $("#cmb_cuotip_id").change();

      $("#tbl_partidas").show(400);
      
      //$('#cmb_rep_id').val(null);
    }
  });

  $(
    "#datetimepicker1, #datetimepicker2, #datetimepicker3, #datetimepicker4,#datetimepicker5, #datetimepicker6, #datetimepicker7, #datetimepicker8,#datetimepicker9, #datetimepicker10, #datetimepicker11"
  ).datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //        endDate: new Date()
  });
  $("#datetimepicker11").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate: new Date()
  });

  $(".moneda2").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "999999.99",
  });
  $(".moneda3").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.000",
    vMax: "999999.999",
  });

  //  $('#txt_cre_linapr').autoNumeric({
  //    aSep: ',',
  //    aDec: '.',
  //    //aSign: 'S/. ',
  //    //pSign: 's',
  //    vMin: '0.00',
  //    vMax: '99999999.99'
  //  });
  $(".porcentaje").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "99.99",
  });

  $("#for_cre").validate({
    submitHandler: function () {
      var poliza_id = parseInt($('#hdd_gps_simple_id').val()) || 0;
      console.log('poliza id: ' + poliza_id);

      if ($('#che_gps').is(':checked') && poliza_id <= 0){
        alerta_warning('IMPORTANTE', 'Has seleccionado el GPS para agregarlo al crédito pero aún no has llenado los datos, debes llenar los datos del GPS antes de guardar el crédito');

        return false;
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/credito_reg.php",
        async: true,
        dataType: "json",
        data: $("#for_cre").serialize(),
        beforeSend: function () {
          //alert($("#for_cre").serialize());
          $("#msj_credito").html("Guardando...");
          $("#msj_credito").show(100);
        },
        success: function (data) {
          //console.log(data);
          if (parseInt(data.cre_id) > 0) {
            if ($("#hdd_vista").val() == "credito_tabla") {
              //              registrar_poliza(data.cre_id); SE REGISTRAN AL APROBAR EL CREDITO
              if($("#hdd_proceso").val() > 0){
                actualizarProceso($("#hdd_proceso").val(), data.cre_id);
              }else{
                creditogarveh_tabla();
              }
            }
            alerta_success("EXITO", data.cre_msj);
            $("#modal_registro_creditogarveh").modal("hide");
            $("#msj_credito").html(data.cre_msj);
            if (parseInt(data.cus) == 1) {
              //console.log("NO HAY POLIZAS");
            } else {
              controlseguro_form("insertar", data.cre_id);
            }
          } else {
            $("#msj_credito_form").show();
            $("#msj_credito_form").html("AVISO IMPORTANTE: " + data.cre_msj);
            alerta_warning(data.cre_msj);
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function(data){
          alerta_error('ERROR', 'Existe un error al guardar el crédito, consultar con sistemas')
          console.log(data)
        }
      });
    },
    rules: {
      cmb_cuotip_id: {
        required: true,
      },
      cmb_mon_id: {
        required: true,
      },
      cmb_cuedep_id: {
        required: true,
      },
      cmb_rep_id: {
        required: true,
      },
      txt_cre_tipcam: {
        required: true,
      },
      txt_cre_preaco: {
        required: true,
      },
      txt_cre_int: {
        required: true,
      },
      txt_cre_ini: {
        required: true,
      },
      txt_cre_numcuo: {
        required: true,
      },
      txt_cre_numcuomax: {
        required: true,
      },
      txt_cre_linapr: {
        required: true,
      },
      txt_cre_feccre: {
        required: true,
      },
      txt_veh_pre:{
        required: true
      },
      txt_credito_monedache: {
        required: true
      },
      txt_cre_tc_2: {
        required: true
      },
      txt_credito_montoche: {
        required: true
      }
    },
    messages: {
      cmb_cuotip_id: {
        required: "*",
      },
      cmb_mon_id: {
        required: "*",
      },
      cmb_cuedep_id: {
        required: "*",
      },
      cmb_rep_id: {
        required: "Elige un Acreedor",
      },
      txt_cre_tipcam: {
        required: "*",
      },
      txt_cre_preaco: {
        required: "*",
      },
      txt_cre_int: {
        required: "*",
      },
      txt_cre_ini: {
        required: "*",
      },
      txt_cre_numcuo: {
        required: "*",
      },
      txt_cre_numcuomax: {
        required: "*",
      },
      txt_cre_linapr: {
        required: "*",
      },
      txt_cre_feccre: {
        required: "*",
      },
      txt_veh_pre:{
        required: "Ingresa el valor del vehículo o valor de préstamo"
      },
      txt_credito_monedache: {
        required: "Selecciona Moneda de Cheque"
      },
      txt_cre_tc_2: {
        required: "Ingresa tipo cambio de cheque"
      },
      txt_credito_montoche: {
        required: "Monto del cheque no debe ser vacío"
      }
    },
  });
  $.fn.modal.Constructor.prototype.enforceFocus = function () {};
});

function obtener_valor_seguro_gps(){
  var numero_cuotas = parseInt($('#txt_cre_numcuo').val());
  if(numero_cuotas <= 0)
    return false;

  $.ajax({
    type: "POST",
    url: VISTA_URL + "segurogps/segurogps_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "valores",
      numero_cuotas: numero_cuotas
    },
    beforeSend: function () {
    },
    success: function (data) {
      console.log(data);
      $('#hdd_seguro_porcentaje').val(data.valor_seguro);
      $('#hdd_gps_precio').val(data.valor_gps);
      $('#span_seguro_gps').text(data.texto);
      credito_cronograma();
    },
    complete: function (data) {},
  });
}

function credito_calculo() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/credito_calculo.php",
    async: true,
    dataType: "json",
    data: {
      action: "cuotas",
      adenda: $("#hdd_aden_id").val(),
      veh_pre: $("#txt_veh_pre").val(),
      cre_ini: $("#txt_cre_ini").val(),
      cre_preaco: $("#txt_cre_preaco").val(),
      cre_int: $("#txt_cre_int").val(),
      cre_numcuo: $("#txt_cre_numcuo").val(),
      cre_numcuo_res: $("#txt_cre_cuo_res").val(), //cuotas que faltan por pagar, para cuota valor sirve para calcular el interés de esas cuotas
      tip_ade: $("#cmb_tip_aden").val(),
    },
    beforeSend: function () {
      //$('#txt_cre_linapr').html('Cargando...');
    },
    success: function (data) {
      $("#txt_cre_linapr").val(data.credito_resultado);
      //credito_cronograma();
      obtener_valor_seguro_gps()
    },
    complete: function (data) {},
  });
}

function precioacordado_calculo() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/credito_calculo.php",
    async: true,
    dataType: "json",
    data: {
      action: "suma",
      veh_pre: $("#txt_veh_pre").val(),
      gps_pre: parseInt($("#txt_gps_pre").val()),
      str_pre: parseInt($("#txt_str_pre").val()),
      soa_pre: parseInt($("#txt_soa_pre").val()),
      gas_pre: parseInt($("#txt_gas_pre").val()),
      otr_pre: $("#txt_otr_pre").val(),
      cre_ini: $("#txt_credito_ini").val(),
    },
    beforeSend: function () {
      //$('#txt_cre_preaco').html('Cargando...');
    },
    success: function (data) {
      $("#txt_cre_preaco").val(data.credito_resultado);
    },
    complete: function (data) {
      credito_calculo();
    },
  });
}

function cuota_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cuota/cuota_tabla_garveh.php",
    async: true,
    dataType: "html",
    data: {
      cre_id: $("#hdd_cre_id").val(),
    },
    beforeSend: function () {
      //$('#div_cuota_tabla').addClass("ui-state-disabled");
    },
    success: function (html) {
      ////console.log(html);
      $("#div_cuota_tabla").html(html);
    },
    complete: function () {
      $("#div_cuota_tabla").removeClass("ui-state-disabled");
    },
  });
}

function creditotipo() {
  var action = $("#action_credito").val();
  var adenda = $("#hdd_aden_id").val();
  if ($("#cmb_cuotip_id").val() == "3") {
    //libre
    $("#cmb_cuosubper_id").val(1);
    $("#cmb_cuosubper_id").attr("readonly", true);

    $("#cre_fijo").hide();
    $("#cre_libre").show();
    if (action == "I" || adenda > 0) {
      $("#txt_cre_numcuo").val("1");
      $("#txt_cre_numcuomax").val("");
    }
  }
  if ($("#cmb_cuotip_id").val() == "4") {
    //fijo
    $("#cmb_cuosubper_id").attr("readonly", false);

    $("#cre_fijo").show();
    $("#cre_libre").hide();
    if (action == "I" || adenda > 0) {
      $("#txt_cre_numcuomax").val($("#txt_cre_numcuo").val());
    }
  }
  credito_calculo();
  $("#txt_cre_pla").val($("#txt_cre_numcuo").val());
}

//para ocultar o mostrar datos cuando sea una adenda
function tipo_credito() {
  var action = $("#action_credito").val();
  if (action == "adenda") {
    $(".fiel_credito").hide();
    $("#txt_cre_preaco").attr("readonly", false);
  } else {
    $(".fiel_credito").show();
    $("#txt_cre_preaco").attr("readonly", true);
  }

  var cre_est = $("#hdd_cre_est").val();

  if (parseFloat(cre_est) > 1) $("#btn_guar_garveh").hide();
  else $("#btn_guar_garveh").show();
}

function garvehtipo() {
  //console.log($('#cmb_custodia_id').val());
  //    if ($('#cmb_custodia_id').val() == 2)//sin custodia
  //    {
  //        $('.con_custodia').show();
  //
  //        //$('.con_custodia').show();
  //        //$('#cmb_cuotip_id').val(4);
  //        //$('#cmb_cuotip_id').attr('disabled',true);
  //        creditotipo();
  //    } else {
  //        //$('#cmb_cuotip_id').attr('disabled',false);
  //        $('.con_custodia').hide();
  //    }
}

function datos_mobiliaria() {
  var gar_mobiliaria = parseInt($("#cmb_credito_tip").val());

  if (gar_mobiliaria == 1) {
    //es una GARANTÍA NORMAL, COMPRAVENTA
    $(".gar_mobiliaria").hide(300);
    $("#txt_cre_valtas").val("0");
    $("#txt_cre_valrea").val("0");
    $("#txt_cre_valgra").val("0");
  } else {
    //es una garantía con MINUTA es 4
    $(".gar_mobiliaria").show(300);
  }
}

function cargar_tipo(vehiculoclase_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculotipo/vehiculotipo_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculoclase_id: vehiculoclase_id,
    },
    beforeSend: function () {
      $("#cmb_vehtip_id_2").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_vehtip_id_2").html(html);
    },
  });
}

function cargar_modelo(marca_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculomarca_id: marca_id,
    },
    beforeSend: function () {
      $("#cmb_vehmod_id_2").selectpicker('destroy');
      $("#cmb_vehmod_id_2").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_vehmod_id_2").html(html);
      $("#cmb_vehmod_id_2").selectpicker({
        liveSearch: true,
        maxOptions: 1,
        language: "ES",
      });
    },
  });
}

// function cargar_cuentadeposito(representante_id)
// {
// $.ajax({
//     type: "POST",
//     url: VISTA_URL + "cuentadeposito/cuentadeposito_select.php",
//     async: true,
//     dataType: "html",
//     data: ({
//         representante_id: representante_id
//     }),
//     beforeSend: function () {
//         $('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
//     },
//     success: function (html) {
//         $('#cmb_cuedep_id').html(html);
//     }
// });
// }

function cambio_moneda(monid) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "/monedacambio/monedacambio_reg.php",
    async: true,
    dataType: "json",
    data: {
      mon_id: monid,
      action: "obtener_dato",
      fecha: $("#txt_cre_fecdes").val(),
    },
    beforeSend: function () {
      //$("#cbo_cancha_id2").val("<option>' cargando '</option>");
    },
    success: function (data) {
      $("#txt_cre_tipcam").val(data.moncam_val);
    },
    complete: function (data) {},
  });
}

function cargar_gps(ids, proveedortipo_id) {
  //console.log($('#cmb_mon_id').val());
  //console.log(proveedortipo_id);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculoadicional/vehiculoadicional_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculoadicional_id: ids,
      protip_id: proveedortipo_id,
      mon_id: $("#cmb_mon_id").val(),
    },
    beforeSend: function () {
      $("#cmb_gps_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_gps_id").html(html);
    },
  });
}
function cargar_str(ids, proveedortipo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculoadicional/vehiculoadicional_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculoadicional_id: ids,
      protip_id: proveedortipo_id,
      mon_id: $("#cmb_mon_id").val(),
    },
    beforeSend: function () {
      $("#cmb_str_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_str_id").html(html);
    },
  });
}
function cargar_soat(ids, proveedortipo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculoadicional/vehiculoadicional_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculoadicional_id: ids,
      protip_id: proveedortipo_id,
      mon_id: $("#cmb_mon_id").val(),
    },
    beforeSend: function () {
      $("#cmb_soa_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_soa_id").html(html);
    },
  });
}
function cargar_gas(ids, proveedortipo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculoadicional/vehiculoadicional_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculoadicional_id: ids,
      protip_id: proveedortipo_id,
      mon_id: $("#cmb_mon_id").val(),
    },
    beforeSend: function () {
      $("#cmb_gas_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_gas_id").html(html);
    },
  });
}

function credito_inicial_form() {
  var pre_veh = Number($("#txt_veh_pre").autoNumeric("get"));
  //console.log(pre_veh);
  if (pre_veh <= 0) {
    alert("Ingrese primero el precio del vehículo por favor.");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/credito_inicial_form.php",
    async: true,
    dataType: "html",
    data: {},
    beforeSend: function () {
      //$('#modal_creditogarveh_anular_acuerdopago').modal('show');
    },
    success: function (data) {
      ////console.log(data);
      $("#div_modal_creditogarveh_inicial").html(data);
      $("#modal_creditogarveh_inicial").modal("show");
    },
    complete: function (data) {
      ////console.log(data);
    },
  });
}

function guardar_inicial(
  inicial_id,
  metodo,
  inicial_valido,
  fecha_inicial,
  detalle
) {
  var inicial_actual = Number($("#txt_credito_ini").autoNumeric("get"));
  inicial_actual = inicial_actual + inicial_valido;
  inicial_actual = parseFloat(inicial_actual);
  $("#txt_credito_ini").val(0.0);

  $("#hdd_credito_idini").val(inicial_id);
  $("#hdd_credito_tipini").val(metodo); // 1 efectivo en caja, 2 deposito
  $("#txt_credito_ini").autoNumeric("set", inicial_actual.toFixed(2));
  $("#txt_inicial_fec").val(fecha_inicial);
  $("#txt_inicial_det").text(detalle);
  ////console.log('desde creditoform / ' + fecha_inicial+' // '+detalle+' // id: '+inicial_id);
  if (metodo == 1) $("#txt_inicial_met").val("EFECTIVO");
  else $("#txt_inicial_met").val("DEPOSITO");

  $("#div_inicial_form").dialog("close");
  precioacordado_calculo();
}

function credito_inicial_reset() {
  //console.log($('#hdd_credito_uniq').val());
  var pre_veh = Number($("#txt_veh_pre").autoNumeric("get"));
  if (pre_veh <= 0) {
    alert("Ingrese primero el precio del vehículo por favor.");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditoinicial/creditoinicial_reg.php",
    async: true,
    dataType: "json",
    data: {
      action: "reset",
      credito_uniq: $("#hdd_credito_uniq").val(),
    },
    beforeSend: function () {},
    success: function (data) {
      if (parseInt(data.estado) > 0) {
        $("#txt_credito_ini").val("0.00");
        $("#hdd_credito_idini").val("");
        $("#hdd_credito_tipini").val(""); // 1 efectivo en caja, 2 deposito
        $("#txt_inicial_fec").val("");
        $("#txt_inicial_det").text("");

        precioacordado_calculo();
      } else {
        alert("Hay un error al resetear las iniciales " + data);
        //console.log(data)
      }
    },
  });
}

function credito_cronograma() {
  var v_pre = $("#txt_veh_pre").val();
  var v_preaco = $("#txt_cre_preaco").val();
  var act = $("#action_credito").val();
  var tipo_cambio = parseFloat($('#hdd_tipo_cambio_venta').val());
  var valor_seguro = parseFloat($('#hdd_seguro_porcentaje').val());
  var cre_interes = parseFloat($("#txt_cre_int").val());
  var tecm = valor_seguro + cre_interes;
  $('#txt_cre_tecm').autoNumeric("set", tecm);

  if(tipo_cambio <= 0){
    alerta_warning('IMPORTANTE', 'Registre el tipo de cambio para poder generar el cronograma');
    return false;
  }

  if (act == "ad_regular") {
    v_pre = 0;
    v_preaco = $("#txt_cre_preaco_ad").val();
  }
  if ($("#txt_cre_fecfac").val() == "" || $("#txt_cre_fecpro").val() == "") {
    //alert('Ingrese fecha de desembolso y prorrateo');
    //console.log('no está entrando al cronograma');
    return false;
  }

  var estado_gps = 0;

  if ($('#che_gps').is(':checked'))
    estado_gps = $('#che_gps').val();
  
    console.log('el estado del gps es: ' + estado_gps)

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/credito_cronograma.php",
    async: true,
    dataType: "html",
    data: {
      veh_pre: v_pre,
      cre_ini: $("#txt_cre_ini").val(),
      cre_id: $("#hdd_cre_id").val(),
      fec_desem: $("#txt_cre_fecdes").val(),
      fec_prorra: $("#txt_cre_fecpro").val(),
      cre_preaco: v_preaco,
      cre_int: $("#txt_cre_int").val(),
      cre_numcuo: $("#txt_cre_numcuo").val(),
      cre_numcuo_res: $("#txt_cre_cuo_res").val(), //son la cantidad de cuotas restantes que faltan por pagar, sirve para calcular la CUOTA BALON
      cre_linapr: $("#hdd_cre_linapr").val(),
      mon_id: $("#cmb_mon_id").val(),
      cre_fecdes: $("#txt_cre_fecdes").val(),
      cre_fecfac: $("#txt_cre_fecfac").val(),
      cre_fecpro: $("#txt_cre_fecpro").val(),
      cre_per: $("#cmb_cre_subper").val(),
      tip_ade: $("#cmb_cre_tipade").val(),
      cuosubper_id: $("#cmb_cuosubper_id").val(),
      valor_seguro: $('#hdd_seguro_porcentaje').val(),
      valor_gps: $('#hdd_gps_precio').val(),
      estado_gps: estado_gps,
      tipo_cambio: tipo_cambio
    },
    beforeSend: function () {
      //$('#div_credito_cronograma').html('<span>Cargando...</span>');
    },
    success: function (html) {
      $("#div_credito_cronograma").html(html);

      monto_pro = Number($("#hdd_mon_pro_1").val());
      /*var linapr = Number($('#txt_cre_linapr').val().replace(/[^0-9\.]+/g,""));
      
      suma_lin_pro = parseFloat(mon_pro + linapr).toFixed(2);
      //console.log('prorra: '+mon_pro+' / linapr: '+linapr+' / suma: '+suma_lin_pro);*/
    },
    complete: function () {
      //credito_calculo();
    },
  });
}

function credito_file_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehfile/credito_file_tabla.php",
    async: false,
    dataType: "html",
    data: {
      cre_id: $("#hdd_cre_id").val(),
    },
    beforeSend: function () {
      //alert($("#hdd_catimg_id").val());
      //$('#div_credito_file_tabla').addClass("ui-state-disabled");
    },
    success: function (html) {
      $("#div_credito_file_tabla").html(html);
    },
    complete: function () {
      $("#div_credito_file_tabla").removeClass("ui-state-disabled");
    },
  });
}

function credito_file_form() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehfile/credito_file_form.php",
    async: true,
    dataType: "html",
    data: {
      cre_id: $("#hdd_cre_id").val(),
    },
    beforeSend: function () {
      //$('#msj_crefil').hide();
      //$('#div_credito_file').dialog("open");
      //$('#div_credito_file').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
    success: function (html) {
      $("#div_credito_file").html(html);
      $("#modal_creditogarvehfile_form").modal("show");
    },
  });
}

function credito_file_eliminar(id) {
  if (confirm("Realmente desea eliminar archivo?")) {
    $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarvehfile/credito_file_controller.php",
      async: true,
      dataType: "json",
      data: {
        action: "eliminar",
        crefil_id: id,
      },
      beforeSend: function () {
        //$('#msj_crefil').html("Cargando...");
        //$('#msj_crefil').show(100);
      },
      success: function (data) {
        //console.log(data);
        if (data.estado > 0) {
          alerta_success("EXITO", "ARCHIVO ELIMINADO CON EXITO");
          credito_file_tabla();
        }
      },
      complete: function () {},
    });
  }
}

function cliente_form2(usuario_act, cliente_id) {
  var credito_cliente_id = Number($("#hdd_cre_cli_id").val());
  console.log(credito_cliente_id);

  if (parseInt(credito_cliente_id) <= 0 && usuario_act == "M") {
    alerta_warning("Información", "Debes buscar por documento o nombres");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      cliente_id: credito_cliente_id,
      vista: "creditogarveh",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cliente_form").html(data);
        $("#modal_registro_cliente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cliente"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cliente", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un ancho automatico al modal, al abrirlo
        modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "cliente";
        var div = "div_modal_cliente_form";
        permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {
      console.log("Aqui listo22");
    },
    error: function (data) {
      alerta_error("ERROR", data.responseText);
      console.log(data);
    },
  });
}

function buscar_idcliente() {
  var doc = $("#txt_ven_cli_doc").val();
  //console.log(doc);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cliente/cliente_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "buscar",
      doc: doc,
    },
    beforeSend: function () {
      //$('#msj_crefil').html("Cargando...");
      //$('#msj_crefil').show(100);
    },
    success: function (data) {
      //console.log(data);
      if (data.estado > 0) {
        cliente_form2("M", data.id_cliente);
      }
    },
    complete: function () {},
  });
}

function llenar_campos_cliente(data) {
  //console.log(data);
  $("#hdd_cre_cli_id").val(data.cliente_id);
  $("#txt_ven_cli_doc").val(data.cliente_doc);
  $("#txt_ven_cli_nom").val(data.cliente_nom);
  $("#txt_ven_cli_fecnac").val(data.cliente_tel);
  $("#txt_ven_cli_ema").val(data.cliente_cel);
  $("#txt_ven_cli_tel").val(data.cliente_telref);
  $("#txt_ven_cli_cel").val(data.cliente_telref);
  $("#txt_ven_cli_telref").val(data.cliente_telref);
  $("#txt_ven_cli_dir").val(data.cliente_telref);
}

function transferente_form(usuario_act, transferente_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "transferente/transferente_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      transferente_id: transferente_id,
      vista: "creditogarveh",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      ////console.log(data);
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_transferente_form").html(data);
        $("#modal_registro_transferente").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_transferente"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_transferente", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "transferente";
        var div = "div_modal_transferente_form";
        permiso_solicitud(usuario_act, transferente_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function controlseguro_form(action, credito_id, poliza_tipo) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "controlseguro/controlseguro_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      credito_id: credito_id,
      vista: "creditogarveh",
      poliza_tipo: parseInt(poliza_tipo),
    },
    beforeSend: function (data) {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#div_controlseguro_form").html(data);
      $("#modal_registro_controlseguro").modal("show");
      modal_height_auto("modal_registro_controlseguro");
      modal_hidden_bs_modal("modal_registro_controlseguro", "limpiar");
      modal_width_auto("modal_registro_controlseguro", 80);
      $("#modal_mensaje").modal("hide");
    },
    complete: function (data) {
      ////console.log(data);
    },
  });
}

function buscar_idtransferente() {
  var doc = $("#txt_transferente_fil_doc").val();
  //console.log(doc);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "transferente/transferente_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "buscar",
      doc: doc,
    },
    beforeSend: function () {
      //$('#msj_crefil').html("Cargando...");
      //$('#msj_crefil').show(100);
    },
    success: function (data) {
      //console.log(data);
      if (data.estado > 0) {
        transferente_form("M", data.id_transferente);
      }
    },
    complete: function () {},
  });
}

function verificar(tipo, credito_id) {
  /* FUNCION QUE VERIFICA EXISTENCIA DE POLIZAS PERO SOLO PARA CREDITOS NUEVOS*/
  $.ajax({
    type: "POST",
    url: VISTA_URL + "controlseguro/verificar_polizas.php",
    async: true,
    dataType: "json",
    data: {
      tipo: tipo,
      credito_id: credito_id,
    },
    beforeSend: function () {
      //$('#msj_crefil').html("Cargando...");
      //$('#msj_crefil').show(100);
    },
    success: function (data) {
      //console.log(data);
      if (data.estado > 0) {
        if (data.seguro > 0 && data.gps == 0) {
          controlseguro_form("insertar", data.credito_id, data.seguro);
        }
        if (data.gps > 0 && data.seguro == 0) {
          controlseguro_form("insertar", data.credito_id, data.gps);
        }
        if (data.gps > 0 && data.seguro > 0) {
          creditogarveh_tabla();
        }
      }
    },
    complete: function () {},
  });
}

function llenar_campos_transferente(data) {
  //console.log(data);
  $("#hdd_transferente_id").val(data.transferente_id);
  $("#txt_transferente_fil_doc").val(data.transferente_doc);
  $("#txt_transferente_fil_nom").val(data.transferente_nombre);
}

function cuentadeposito_form2(usuario_act, cuentadeposito_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cuentadeposito/cuentadeposito_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: "creditogarveh",
      cuentadeposito_id: cuentadeposito_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_cuentadeposito_form").html(data);
        $("#modal_registro_cuentadeposito").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cuentadeposito"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_cuentadeposito", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "cuentadeposito";
        var div = "div_modal_cuentadeposito_form";
        permiso_solicitud(usuario_act, cuentadeposito_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function representante_form3(usuario_act, representante_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "representante/representante_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: "creditogarveh", //
      representante_id: representante_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_representante_form").html(data);
        $("#modal_registro_representante").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_representante"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_representante", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "representante";
        var div = "div_modal_representante_form";
        permiso_solicitud(usuario_act, representante_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function cargar_representante() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "representante/representante_select.php",
    async: true,
    dataType: "html",
    data: {},
    beforeSend: function () {
      $("#cmb_rep_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_rep_id").html(html);
    },
  });
}
function cargar_cuentadeposito() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cuentadeposito/cuentadeposito_select.php",
    async: true,
    dataType: "html",
    data: {},
    beforeSend: function () {
      $("#cmb_cuedep_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_cuedep_id").html(html);
    },
  });
}

function vehiculomarca_form(usuario_act, vehiculomarca_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculomarca/vehiculomarca_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      modulo: "creditogarveh",
      vehiculomarca_id: vehiculomarca_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_vehiculomarca_form").html(data);
        $("#modal_registro_vehiculomarca").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_vehiculomarca"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_vehiculomarca", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "vehiculomarca";
        var div = "div_modal_vehiculomarca_form";
        permiso_solicitud(usuario_act, vehiculomarca_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function cargar_vehiculomarca(marca_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculomarca/vehiculomarca_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculomarca_id: marca_id,
    },
    beforeSend: function () {
      $("#cmb_vehmar_id_2").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_vehmar_id_2").html(html);
    },
  });
}

function vehiculoclase_form(usuario_act, vehiculoclase_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculoclase/vehiculoclase_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      modulo: "creditogarveh",
      vehiculoclase_id: vehiculoclase_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_vehiculoclase_form").html(data);
        $("#modal_registro_vehiculoclase").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_vehiculoclase"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_vehiculoclase", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "vehiculoclase";
        var div = "div_modal_vehiculoclase_form";
        permiso_solicitud(usuario_act, vehiculoclase_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function cargar_vehiculoclase(vehiculoclase_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculoclase/vehiculoclase_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculoclase_id: vehiculoclase_id,
    },
    beforeSend: function () {
      $("#cmb_vehcla_id_2").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_vehcla_id_2").html(html);
    },
  });
}

function vehiculomodelo_form(usuario_act, vehiculomodelo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculomodelo/vehiculomodelo_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      modulo: "creditogarveh",
      vehiculomodelo_id: vehiculomodelo_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_vehiculomodelo_form").html(data);
        $("#modal_registro_vehiculomodelo").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_vehiculomodelo"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_vehiculomodelo", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "vehiculomodelo";
        var div = "div_modal_vehiculomodelo_form";
        permiso_solicitud(usuario_act, vehiculomodelo_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}
function cargar_vehiculomodelo(vehiculomodelo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculomodelo_id: vehiculomodelo_id,
    },
    beforeSend: function () {
      $("#cmb_vehmod_id_2").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_vehmod_id_2").html(html);
    },
  });
}
function vehiculotipo_form(usuario_act, vehiculotipo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculotipo/vehiculotipo_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      modulo: "creditogarveh",
      vehiculotipo_id: vehiculotipo_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_vehiculotipo_form").html(data);
        $("#modal_registro_vehiculotipo").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_vehiculotipo"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_vehiculotipo", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "vehiculotipo";
        var div = "div_modal_vehiculotipo_form";
        permiso_solicitud(usuario_act, vehiculotipo_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}
function cargar_vehiculotipo(vehiculotipo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vehiculotipo/vehiculotipo_select.php",
    async: true,
    dataType: "html",
    data: {
      vehiculotipo_id: vehiculotipo_id,
    },
    beforeSend: function () {
      $("#cmb_vehtip_id_2").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_vehtip_id_2").html(html);
    },
  });
}

function credito_pdf_form(documentodetalle_id, cgarvdoc_nom) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehpdf/credito_pdf_form.php",
    async: true,
    dataType: "html",
    data: {
      cre_id: $("#hdd_cre_id").val(),
      cli_id: $("#hdd_cre_cli_id").val(),
      documentodetalle_id: documentodetalle_id,
      cgarvdoc_nom: cgarvdoc_nom,
    },
    beforeSend: function () {
      //$('#msj_crefil').hide();
      //$('#div_credito_file').dialog("open");
      //$('#div_credito_file').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
    success: function (html) {
      $("#div_credito_file").html(html);
      $("#modal_creditogarvehpdf_form").modal("show");
    },
  });
}

function credito_pdf_form1(documentodetalle_id, cgarvdoc_nom, pdf_is) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehpdf/credito_pdf_form.php",
    async: true,
    dataType: "html",
    data: {
      cre_id: $("#hdd_cre_id").val(),
      cli_id: $("#hdd_cre_cli_id").val(),
      documentodetalle_id: documentodetalle_id,
      cgarvdoc_nom: cgarvdoc_nom,
      pdf_is: pdf_is, //mod o new
    },
    beforeSend: function () {},
    success: function (html) {
      $("#div_credito_file").html(html);
      $("#modal_creditogarvehpdf_form").modal("show");
    },
  });
}

function hist_doc_form(tb_pdfgarv_id) {
  //$('.modal-backdrop').remove();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pdfgarv/doc_pdfgarv_reg.php",
    async: true,
    dataType: "html",
    data: {
      tb_pdfgarv_id: tb_pdfgarv_id,
    },
    beforeSend: function () {},
    success: function (data) {
      //console.log(data);
      $("#div_historial_doc_pdf").html(data);
      $("#modal_hist_doc_pdf_timeline").modal("show");
      modal_height_auto("modal_hist_doc_pdf_timeline"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_hist_doc_pdf_timeline", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function () {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data.responseText);
    },
  });
}

/* GERSON (26-01-24) */
function hist_doc_form_proceso(proceso_id, item_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "pdfgarv/doc_pdfgarv_reg.php",
    async: true,
    dataType: "html",
    data: {
      tb_pdfgarv_id: 0,
      proceso_id: proceso_id,
      item_id: item_id
    },
    beforeSend: function () {},
    success: function (data) {
      //console.log(data);
      $("#div_historial_doc_pdf").html(data);
      $("#modal_hist_doc_pdf_timeline").modal("show");
      modal_height_auto("modal_hist_doc_pdf_timeline"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_hist_doc_pdf_timeline", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function () {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data.responseText);
    },
  });
}
/*  */

function vercredit_pdf_form(pdfgarv_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehpdf/ver_creditopdf_form.php",
    async: true,
    dataType: "html",
    data: {
      pdfgarv_id: pdfgarv_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#div_credito_pdf_ver").html(data);
      $("#modal_creditopdf_ver").modal("show");
      modal_height_auto("modal_creditopdf_ver"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_creditopdf_ver", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      //console.log(data.responseText);
    },
  });
}

/* GERSON (24-01-24) */
function vercredit_pdf_form_proceso(proceso_id, item_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehpdf/ver_creditopdf_form.php",
    async: true,
    dataType: "html",
    data: {
      pdfgarv_id: 0,
      proceso_id: proceso_id,
      item_id: item_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#div_credito_pdf_ver").html(data);
      $("#modal_creditopdf_ver").modal("show");
      modal_height_auto("modal_creditopdf_ver"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_creditopdf_ver", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      //console.log(data.responseText);
    },
  });
}
/*  */

function carousel(modulo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "templates/carousel.php",
    async: true,
    dataType: "html",
    data: {
      modulo_nom: "creditogarvehfile",
      modulo_id: modulo_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Galería");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      $("#div_modal_carousel").html(data);
      $("#modal_carousel_galeria").modal("show");

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_carousel_galeria"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_carousel_galeria", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      //console.log(data.responseText);
    },
  });
}

function exonerar_pdf_form(tipogarvdocpersdetalle_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehpdf/exonerar_creditogarvehpdf.php",
    async: true,
    dataType: "html",
    data: {
      tipogarvdocpersdetalle_id: tipogarvdocpersdetalle_id,
      credito_id: $("#hdd_creditoid").val(),
      cliente_id: $("#hdd_clienteid").val(),
    },
    beforeSend: function () {
      //$('#div_verificacion_titulo_form').dialog("open");
      //$('#div_verificacion_titulo_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_exonerar_creditogarvehpdf").html(data);
        modal_width_auto("modal_creditogarveh_exonerar", 80);
        modal_height_auto("modal_creditogarveh_exonerar");
        $("#modal_creditogarveh_exonerar").modal("show");

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      }
    },
  });
}

function exonerar_pdf_form1(documentodetalle_id, cgarvdoc_nom, pdf_is) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarvehpdf/exonerar_creditogarvehpdf.php",
    async: true,
    dataType: "html",
    data: {
      tipogarvdocpersdetalle_id: documentodetalle_id,
      credito_id: $("#hdd_creditoid").val(),
      cliente_id: $("#hdd_clienteid").val(),
      cgarvdoc_nom: cgarvdoc_nom,
      pdf_is: pdf_is, //mod o new
    },
    beforeSend: function () {},
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_exonerar_creditogarvehpdf").html(data);
        modal_width_auto("modal_creditogarveh_exonerar", 80);
        modal_height_auto("modal_creditogarveh_exonerar");
        $("#modal_creditogarveh_exonerar").modal("show");
      }
    },
  });
}

function cargardocumentosnecesarios() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_detallechecklist.php",
    async: true,
    dataType: "html",
    data: {
      tipopersona_id: $("#hdd_tipopersona").val(),
      cre_subgar: $("#hdd_cre_subgar").val(),
      cre_cus: $("#hdd_cre_cus").val(),
      cre_tip: $("#hdd_cre_tip2").val(),
      creditoid: $("#hdd_creditoid").val(),
      clienteid: $("#hdd_clienteid").val(),
      proceso_id: $("#hdd_proceso_id").val(),
    },
    beforeSend: function () {
      $("#cheklist_mensaje").show(200);
    },
    success: function (data) {
      //console.log(data);
      $("#cheklist_mensaje").hide(200);
      $(".pdf").html(data);
      setTimeout(function () {
        if ($('#action_credito').val() == 'M' && parseInt($('input[name*="hdd_credito_tip"]').val()) != 2 && $('#hdd_credito_suministro').val() == '') {
          $('b:contains("DOMICILIARIA")').css({"color":"red"});
        }
      }, 300);
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    },
  });
}

/* GERSON (20-10-23) */
function actualizarProceso(proceso_id, credito_id) {
	$.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async: false,
    dataType: "json",
    data: ({
    action: 'insertar_credito_proceso',
    proceso_id: proceso_id,
    credito_id: credito_id
  }),
      beforeSend: function () {
      },
      success: function (data) {
        notificacion_success(data.mensaje, 3000);
      },
      complete: function (data) {
      }
  });
}

// JUAN 22-11-2023
function gps_form_simple() {
  var cliente_id = parseInt($('#hdd_cre_cli_id').val());
  var gps_precio = parseInt($('#hdd_gps_precio').val());
  var num_cuotas = parseInt($('#txt_cre_numcuo').val());

  if(cliente_id > 0 && gps_precio > 0 && num_cuotas > 0){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "controlseguro/gps_form_simple.php",
      async: true,
      dataType: "html",
      data: {
        cliente_id: cliente_id,
        gps_precio: gps_precio,
        num_cuotas: num_cuotas,
        poliza_id: $('#hdd_gps_simple_id').val()
      },
      beforeSend: function () {},
      success: function (data) {
        if (data != "sin_datos") {
          $("#div_modal_gps_simple").html(data);
          $("#modal_gps_simple").modal("show");
          //funcion js para limbiar el modal al cerrarlo
          modal_hidden_bs_modal("modal_gps_simple", "limpiar"); //funcion encontrada en public/js/generales.js
        }
      },
    });
  }
  else
    alerta_warning('IMPORTANTE', 'Hay algunos datos que debes llenar en el crédito antes de registrar el GPS: cliente ID: ' + cliente_id + ', Precio GPS: ' + gps_precio + ', Cuotas: ' + num_cuotas)
}

// JUAN 22-11-2023 -> FUNCION GENERAL PARA TODOS LOS FORMULARIOS DONDE SE USE EL REGISTRO DE GPS
function get_poliza_id(data) {
  var poliza_id = parseInt(data.poliza_id); //viene desde el registro del formulario de gps simple
  $('#hdd_gps_simple_id').val(poliza_id);
  console.log(data)
}

function upload_form_docs(credito_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_form_docs.php",
    async: true,
    dataType: "html",
    data: {
      action: 'I', // PUEDE SER: L, I, M , E
      upload_id: 0,
      modulo_nom: 'refinanciar', //nombre de la tabla a relacionar
      modulo_id: credito_id, //aun no se guarda este modulo
      //modulo_id: 0, //aun no se guarda este modulo
      upload_uniq: '' //ID temporal
    },
    beforeSend: function () {
    },
    success: function (html) {
      $("#div_credito_file").html(html);
      $("#modal_upload_docs").modal("show");

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_upload_docs'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_upload_docs', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
  });
}

function historial_form(tabla_nom, tabla_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "historial/historial_form.php",
    async: true,
    dataType: "html",
    data: ({
      tabla_nom: tabla_nom,
      tabla_id: tabla_id
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      $('#div_historial_cambios_legal').html(html);
      $('#modal_' + tabla_nom + '_historial_form').modal('show');
      $('#modal_mensaje').modal('hide');

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal('modal_' + tabla_nom + '_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_' + tabla_nom + '_historial_form'); //funcion encontrada en public/js/generales.js
    }
  });
}

function verejecucion_pdf_form(uploadid, modulo_nom, vista, archivo_nom) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "ejecucionupload/ver_pdfuploaded_form.php",
    async: true,
    dataType: "html",
    data: {
      upload_id: uploadid,
      modulo_nom: modulo_nom,
      vista: vista,
      archivo_nom: archivo_nom
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#div_modal_ejecucion_verpdf_form").html(data);
      $("#modal_ejecucion_pdf_ver").modal("show");
      modal_height_auto("modal_ejecucion_pdf_ver"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("modal_ejecucion_pdf_ver", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {
      $("#modal_mensaje").modal("hide");
    },
    error: function (data) {
      alerta_error("Error", "ERROR!:" + data.responseText); //en generales.js
    },
  });
}

function cargar_docpostcierre() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_docpostcierre_detalle.php",
    async: true,
    dataType: "html",
    data: {
      creditoid: $("#hdd_cre_id").val()
    },
    beforeSend: function () {
      $("#postcierre_mensaje1").show(200);
    },
    success: function (data) {
      $("#postcierre_mensaje1").hide(200);
      $("#ejecucion_result").html(data);
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

/* daniel odar 16-09-24 */
function actualizar_suministro() {
  $.confirm({
    title: "ACTUALIZAR CAMPO",
    content:
      '<form class="form_suministro">' +
      '<div class="form-group">' +
      "<label for='sumin'>Ingrese numero suministro:</label>" +
      '<input id="sumin" name="sumin" type="text" class="suministro_num form-control input-shadow input-sm" required />' +
      "</div>" +
      "</form>"
    ,
    type: 'blue',
    escapeKey: 'close',
    backgroundDismiss: true,
    columnClass: 'small',
    buttons: {
      guardar: {
        text: 'Guardar',
        btnClass: 'btn-blue',
        action: function () {
          var num_suministro = this.$content.find('.suministro_num').val();

          if (!num_suministro) {
            $.alert('<b>Ingrese numero de suministro</b>');
            return false;
          }
          $.ajax({
            type: 'POST',
            url: VISTA_URL + 'creditogarveh/credito_reg.php',
            async: true,
            dataType: 'json',
            data: ({
              action: 'modificar_campo',
              tabla: 'tb_creditogarveh',
              tabla_id: $('#hdd_cre_id').val(),
              columna: 'tb_credito_suministro',
              valor: num_suministro,
              tipo_dato: 'STR',
            }),
            success: function (data) {
              $.alert(`<b>${data.mensaje}</b>`);
              if (data.estado == 1) {
                $('#hdd_credito_suministro').val(data.num_suministro);
                $('#lbl_num_suministro').css({ 'color': 'orange' });
                $('#lbl_num_suministro').html(`NUMERO SUMINISTRO: ${data.num_suministro}. <a onclick="actualizar_suministro()" style="cursor: pointer;">CLICK AQUÍ SI DESEA ACTUALIZAR</a>`);
                cargardocumentosnecesarios();
              }
            },
            complete: function(data) {
              console.log(data);
            }
          });
        }
      },
      cancelar: function () {
      }
    }
  });
}

/* daniel odar 31-01-25 */
function filestorage_form() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "filestorage/filestorage_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: "I", //insertar
      public_carpeta: 'pdf',
      modulo_nom: 'creditogarveh',
      modulo_id: $('#hdd_cre_id').val(),
      modulo_subnom: 'post_cierre',
      filestorage_uniq: '',
      filestorage_des: ''
    }),
    beforeSend: function () {
    },
    success: function (data) {
      if (data != 'sin_datos') {
        $('#div_filestorage_form').html(data);
        $('#modal_registro_filestorage').modal('show');
        modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
    },
    error: function (data) {
    }
  });
}
function listar_docs_post_cierre() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "filestorage/filestorage_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: 'creditogarveh',
      modulo_id: $('#hdd_cre_id').val(),
      modulo_subnom: 'post_cierre'
    }),
    beforeSend: function () {
      $("#postcierre_mensaje2").show(200);
    },
    success: function (data) {
      $("#postcierre_mensaje2").hide(200);
      if (data != '') {
        $('#result_otros_doc').html(data);
      }
    },
    complete: function (data) {
    },
    error: function (data) {
      alert(data.responseText);
    }
  });
}
function filestorage_eliminar(filestorage_id) {
  Swal.fire({
    title: '¿DESEA ELIMINAR EL ARCHIVO?',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class ="fa fa-home"></i> Confirmo!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> No',
    confirmButtonColor: '#3b5998',
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "filestorage/filestorage_controller.php",
        async: true,
        dataType: "json",
        data: ({
          action: 'eliminar',
          hdd_filestorage_id: filestorage_id,
        }),
        beforeSend: function () {
        },
        success: function (data) {
          if (parseInt(data.estado) == 1) {
            notificacion_success(data.mensaje, 4000)
            $('#tr_' + filestorage_id).hide();
          }
          else
            alerta_error('IMPORTANTE', data.mensaje)
        },
        complete: function (data) {
        },
        error: function (data) {
        }
      });
    } else if (result.isDenied) {

    }
  });
}