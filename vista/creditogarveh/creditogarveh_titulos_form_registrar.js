$(document).ready(function() {
  console.log('cambios 04/10/2024');
  
  validarJustificacion();

  $("#ckjustif").on("change", function (e) {
    validarJustificacion();
  });

  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });

});

function validarJustificacion(){
  var miCheckbox = document.getElementById('ckjustif');
  if (miCheckbox.checked) {
    $("#txtobservacion").removeAttr("readonly","readonly");
  } else {
    $("#txtobservacion").attr("readonly","readonly");
  }
}

function addnew(seguimiento_id,credito_id,cgarvtipo_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_titulos_controller.php",
		async:true,
		dataType: "json",
		data: ({
			action: 'registrar',
			seguimiento_id: seguimiento_id,
			credito_id: credito_id,
			cgarvtipo_id: cgarvtipo_id,
      p_array_datos: $("#frm_titulo_register").serialize()
		}),
		beforeSend: function() {
			// console.log('cargando controller...');
    },
		success: function(data){
      // console.log(data)
      if(parseInt(data.estado) == 1){
        $('#modal_creditogarveh_form_actualizar').modal('hide');
        creditogarveh_modal_titulos_listar_registros();
        alerta_success('Correcto', data.mensaje);
      }
      else{
        alerta_warning('Alerta', data.mensaje); //en generales.js
      }
		},
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      // console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
	});
}