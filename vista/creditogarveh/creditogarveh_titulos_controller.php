<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../creditogarveh/Creditogarveh.class.php');
    require_once('../creditolinea/Creditolinea.class.php');

    $oCreditogarveh = new Creditogarveh();
    $oCreditolinea = new Creditolinea();

    $action           = $_POST['action'];
    $seguimiento_id   = $_POST['seguimiento_id'];
    $credito_id       = $_POST['credito_id'];
    parse_str($_POST["p_array_datos"], $datosFrm);
    $txtnrotitulo     = $datosFrm["txtnrotitulo"];
    $txtfecha         = $datosFrm["txtfecha"];
    $cmbestado        = $datosFrm["cmbestado"];
    $cmb_sede         = $datosFrm["cmb_sede"];
    $txtobservacion   = $datosFrm["txtobservacion"];
    $txtobservacionHist = $txtobservacion;
    $extracanho       = explode("-", $txtnrotitulo);
    $creditotipo_id   = 3; // CREDITO GARVEH
    $tipo             = intval($datosFrm["txttipotitulo"]) ; // GARANTIA | PORDER | INMATRICULACION
    $usuario_id       = $_SESSION['usuario_id'];
    $data = array();

    if( !isset($extracanho[1]) ){
        $data['estado']  = 0;
        $data['mensaje'] = 'El formato de Nro. de título es incorrecto, falta indicar el año. <br>Formato adecuado: <br> <b>_ _ _ _ _ _ _ _ - _ _ _ _</b> <br> <b>N° de Título &nbsp;&nbsp;&nbsp;&nbsp; Año</b>';
        echo json_encode($data);
        exit();
    }
    else
    {
        $anho = $extracanho[1];
        $validar_anho = date("Y");
        $extrac_validar_anho = trim(substr(intval($validar_anho),0,2));

        if( empty($txtnrotitulo) )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'Debe ingresar el Nro. de título.';
        }
        elseif( empty($txtfecha) )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'Debe ingresar fecha.';
        }
        elseif( empty($cmbestado) )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'Debe seleccionar el estado PRESENTADO.';
        }
        elseif( empty($cmb_sede) )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'Debe seleccionar el la sede.';
        }
        elseif( strlen($anho) != 4 )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'El año ingresado: '.$anho. ', es incorrecto.';
        }
        elseif( $anho > $validar_anho )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'El año ingresado: '.$anho. ' es incorrecto,<br>no puede ser mayor al año actual: '.$validar_anho;
        }
        elseif( substr(intval($anho),0,2) != $extrac_validar_anho )
        {
            $data['estado']  = 0;
            $data['mensaje'] = 'El año ingresado: '.$anho. ', es incorrecto.<br>Año actual: '.$validar_anho;
        }
        else
        {
            $txtfecha = $datosFrm["txtfecha"].' '.date("H:i:s");
            if( empty($txtobservacion) ) { $txtobservacion = ' '; } else { $txtobservacion = '<li>'.$txtobservacion.'</li>'; }
            
            if( $action == "registrar" )
            {
                $cgarvtipo_id = $_POST['cgarvtipo_id'];
                $color = "";
                $descripEstado = "";
                
                // - - - - - REGISTRAR NUEVO EN EL HISTORIAL - - - - - //
                    $oCreditolinea->tb_creditotipo_id      = $creditotipo_id;
                    $oCreditolinea->tb_credito_id          = $credito_id;
                    $oCreditolinea->tb_usuario_id          = $usuario_id;
                    $oCreditolinea->tb_creditolinea_det    = '<span style="color: gray;"> Nro. de Titulo: '.$txtnrotitulo.' <br> Fecha: '.mostrar_fecha_hora($txtfecha).' &nbsp; &nbsp; Estado: &nbsp; <span class="padsquare rounded" style="background-color: #25dbcf;">&nbsp; &nbsp; &nbsp;</span> &nbsp; PRESENTADO <br> Observación: <br> '.$txtobservacionHist.'</span>';
                    $oCreditolinea->tb_creditolinea_xac    = 1;
                    $oCreditolinea->tb_creditolinea_subnom = $txtnrotitulo;
                    $oCreditolinea->insertar_nuevo();
                // - - - - - REGISTRAR NUEVO EN EL HISTORIAL - - - - - //
    
                if( $oCreditogarveh->seguimiento_titulo_registrar($txtnrotitulo,$anho,$credito_id,$creditotipo_id,$tipo,$usuario_id,$txtobservacion,$txtfecha,$cgarvtipo_id,$cmb_sede) == true )
                {
                    $data['estado']  = 1;
                    $data['mensaje'] = 'Nuevo titulo registrado correctamente';
                }
                else
                {
                    $data['estado']  = 0;
                    $data['mensaje'] = 'Se presentó un inconveniente al momento de realizar el proceso de registro: <br> Error SQL: Creditogarveh.class :: seguimiento_titulo_registrar() :: linea: 1279';
                }
            }
            elseif( $action == "actualizar" )
            {
                $checkobservacion = $_POST['p_checkobservacion'];                
                $result = $oCreditogarveh->seguimiento_titulo_mostraruno($seguimiento_id);
                    if($result['estado'] == 1)
                    {
                        $data_nrotitulo               = $result['data']['tb_seguimiento_nrotitulo'];
                        $data_credito_id              = $result['data']['tb_credito_id'];
                        $data_creditotipo_id          = $result['data']['tb_creditotipo_id'];
                        $data_cgarvtipo_id            = $result['data']['tb_cgarvtipo_id'];
                        $data_sede                    = $result['data']['sede'];
                        $data_credestado_titulo_id    = $result['data']['tb_credestado_titulo_id'];
                        $data_seguimiento_observacion = $result['data']['tb_seguimiento_observacion'];
                        $data_fecha_reg               = $result['data']['tb_seguimiento_fecreg'];
                        $data_fecha_mod               = $result['data']['tb_seguimiento_fecmod'];
                    }
                    else
                    {
                        $data['estado']  = 0;
                        $data['mensaje'] = 'No se retornó resultados para el ID de seguimiento: '.$seguimiento_id;
                        echo json_encode($data);
                        exit();
                    }
                $result = null;

                if( $oCreditogarveh->seguimiento_titulo_actualizar($seguimiento_id,$txtnrotitulo,$txtfecha,$cmbestado,$cmb_sede,$checkobservacion,$txtobservacion) == false )
                {
                    $data['estado'] = 0;
                    $data['mensaje'] = 'Se presentó un inconveniente al momento de realizar el proceso de seguimiento: Actualizar. <br> Error SQL: Creditogarveh.class :: seguimiento_titulo_actualizar() :: linea: 1193';
                }
                else
                {
                    $data['estado']  = 1;
                    $data['mensaje'] = 'Datos actualizados correctamente';
                }
    
                switch($data_credestado_titulo_id) {
                    case 1:
                    $color = '25dbcf';
                    $descripEstado = "PRESENTADO";
                    break;
                    case 2:
                    $color = '2057AC';
                    $descripEstado = "REINGRESADO";
                    break;
                    case 3:
                    $color = 'F38C05';
                    $descripEstado = "APELADO";
                    break;
                    case 4:
                    $color = 'B4B4B4';
                    $descripEstado = "EN PROCESO";
                    break;
                    case 5:
                    $color = '662485';
                    $descripEstado = "EN CALIFICACIÓN";
                    break;
                    case 6:
                    $color = '84BC18';
                    $descripEstado = "INSCRITO";
                    break;
                    case 7:
                    $color = '575755';
                    $descripEstado = "RESERVADO";
                    break;
                    case 8:
                    $color = 'EF1B52';
                    $descripEstado = "DISTRIBUIDO";
                    break;
                    case 9:
                    $color = '006632';
                    $descripEstado = "LIQUIDADO";
                    break;
                    case 10:
                    $color = '81D1FF';
                    $descripEstado = "PRORROGADO";
                    break;
                    case 11:
                    $color = 'FC0002';
                    $descripEstado = "OBSERVADO";
                    break;
                    case 12:
                    $color = '95141E';
                    $descripEstado = "SUSPENDIDO";
                    break;
                    case 13:
                    $color = '020202';
                    $descripEstado = "TACHADO";
                    break;
                    case 14:
                    $color = '6BA7CF';
                    $descripEstado = "ANOTADO";
                    break;
                    case 15:
                    $color = 'CD3C40';
                    $descripEstado = "CANCELADO";
                    break;
                    default:
                    $color = 'ffffff';
                    $descripEstado = "-";
                    break;
                }
    
                // - - - - - REGISTRAR ANTERIOR EN EL HISTORIAL - - - - - //
                    $oCreditolinea->tb_creditotipo_id      = $creditotipo_id;
                    $oCreditolinea->tb_credito_id          = $credito_id;
                    $oCreditolinea->tb_usuario_id          = $usuario_id;
                    $oCreditolinea->tb_creditolinea_det    = '<span style="color: gray;"> Sede: '.$data_sede.' <br> Nro. de Titulo: '.$data_nrotitulo.' <br> Fecha: '.mostrar_fecha_hora($data_fecha_mod).' &nbsp; &nbsp; Estado: &nbsp; <span class="padsquare rounded" style="background-color: #'.$color.';">&nbsp; &nbsp; &nbsp;</span> &nbsp; '.$descripEstado.' <br> Observación: <br> '.$data_seguimiento_observacion.'</span>';
                    $oCreditolinea->tb_creditolinea_xac    = 1;
                    $oCreditolinea->tb_creditolinea_subnom = $data_nrotitulo;
                    $oCreditolinea->insertar_nuevo();
                // - - - - - REGISTRAR ANTERIOR EN EL HISTORIAL - - - - - //
    
                switch($cmbestado) {
                    case 1:
                    $color = '25dbcf';
                    $descripEstado = "PRESENTADO";
                    break;
                    case 2:
                    $color = '2057AC';
                    $descripEstado = "REINGRESADO";
                    break;
                    case 3:
                    $color = 'F38C05';
                    $descripEstado = "APELADO";
                    break;
                    case 4:
                    $color = 'B4B4B4';
                    $descripEstado = "EN PROCESO";
                    break;
                    case 5:
                    $color = '662485';
                    $descripEstado = "EN CALIFICACIÓN";
                    break;
                    case 6:
                    $color = '84BC18';
                    $descripEstado = "INSCRITO";
                    break;
                    case 7:
                    $color = '575755';
                    $descripEstado = "RESERVADO";
                    break;
                    case 8:
                    $color = 'EF1B52';
                    $descripEstado = "DISTRIBUIDO";
                    break;
                    case 9:
                    $color = '006632';
                    $descripEstado = "LIQUIDADO";
                    break;
                    case 10:
                    $color = '81D1FF';
                    $descripEstado = "PRORROGADO";
                    break;
                    case 11:
                    $color = 'FC0002';
                    $descripEstado = "OBSERVADO";
                    break;
                    case 12:
                    $color = '95141E';
                    $descripEstado = "SUSPENDIDO";
                    break;
                    case 13:
                    $color = '020202';
                    $descripEstado = "TACHADO";
                    break;
                    case 14:
                    $color = '6BA7CF';
                    $descripEstado = "ANOTADO";
                    break;
                    case 15:
                    $color = 'CD3C40';
                    $descripEstado = "CANCELADO";
                    break;
                    default:
                    $color = 'ffffff';
                    $descripEstado = "-";
                    break;
                }
    
                // ------- PROCEDIMIENTO PARA RETORNAR EL ID Y OCULTARLO EN EL HISTORIAL Y REEMPLAZARLO POR EL NUEVO ------- //
    
                    $consultarDatosCreditoLinea = $oCreditolinea->consultar_proximo();
                    $DatosCreditoLinea_proximoCodigo = $consultarDatosCreditoLinea["newcod"];
                    $oCreditolinea->refrescar_historial_antiguo_por_nuevo($DatosCreditoLinea_proximoCodigo);
                
                // ------- PROCEDIMIENTO PARA RETORNAR EL ID Y OCULTARLO EN EL HISTORIAL Y REEMPLAZARLO POR EL NUEVO ------- //
    
                // - - - - - REGISTRAR NUEVO EN EL HISTORIAL - - - - - //
                    $oCreditolinea->tb_creditotipo_id      = $creditotipo_id;
                    $oCreditolinea->tb_credito_id          = $credito_id;
                    $oCreditolinea->tb_usuario_id          = $usuario_id;
                    $oCreditolinea->tb_creditolinea_det    = '<span style="color: gray;"> Sede: '.$cmb_sede.' <br> Nro. de Titulo: '.$txtnrotitulo.' <br> Fecha: '.mostrar_fecha_hora($txtfecha).' &nbsp; &nbsp; Estado: &nbsp; <span class="padsquare rounded" style="background-color: #'.$color.';">&nbsp; &nbsp; &nbsp;</span> &nbsp; '.$descripEstado.' <br> Observación: <br> '.$txtobservacionHist.'</span>';
                    $oCreditolinea->tb_creditolinea_xac    = 1;
                    $oCreditolinea->tb_creditolinea_subnom = $txtnrotitulo;
                    $oCreditolinea->insertar_nuevo();
                // - - - - - REGISTRAR NUEVO EN EL HISTORIAL - - - - - //
            }
            else
            {
                $data['estado']  = 0;
                $data['mensaje'] = 'Tipo de operación inválida, recargue la página.';
            }
        }    
        echo json_encode($data);
    }
?>