<?php

require_once('../../core/usuario_sesion.php');

require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();

require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();

require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();

require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();
require_once('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

require_once('../transferente/Transferente.class.php');
$oTransferente = new Transferente();
require_once('../externo/Externo.class.php');
$oExterno = new Externo();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
$creditogarveh_id = $_POST['cre_id'];
$cuentadeposito_id = 3;
// GERSON (30-10-23)
$proceso_id = 0;
$cliente_id = 0;
if(isset($_POST['proceso_id'])){ // si la creacion de adenda viene desde PROCESOS
  $proceso_id = intval($_POST['proceso_id']);
  if($proceso_id > 0){
    $proceso = $oProceso->mostrarUno($proceso_id);
  }
}
//

if ($_POST['action'] == 'adenda') {
  $cre_feccre = date('d-m-Y');
  $cre_fecdes = date('d-m-Y');
  $cre_fecfac = date('d-m-Y');
  $cre_fecpro = date('d-m-Y');

  $dts = $oCreditogarveh->mostrarUno($creditogarveh_id);
  if ($dts['estado'] != 1) {
    $mensaje = 'No se ha encontrado ningún registro para el creditogarveh seleccionado, problemas con la adenda.';
    $bandera = 4;
  } else {
    $reg = mostrar_fecha_hora($dts['data']['tb_credito_reg']);
    $mod = mostrar_fecha_hora($dts['data']['tb_credito_mod']);
    $apr = mostrar_fecha_hora($dts['data']['tb_credito_apr']);
    $cuotatipo_id = $dts['data']['tb_cuotatipo_id'];
    $cuosubper_id = $dts['data']['tb_cuotasubperiodo_id'];
    $moneda_id = $dts['data']['tb_moneda_id'];
    $cuentadeposito_id = $dts['data']['tb_cuentadeposito_id'];
    $representante_id = $dts['data']['tb_representante_id'];
    $cliente_id = $dts['data']['tb_cliente_id'];
  }
  $dts = null;

  //listamos las cuotas que faltan por pagar
  $dts1 = $oCuota->listar_cuotas_impagas($creditogarveh_id, 3); //parametro 3 de garveh
  if ($dts1['estado'] == 1) {
    $cuo_res = count($dts1['data']); //cuotas restantes por pagar
  }
  $dts1 = null;
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_adenda_form" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">ADENDA DE <b>GAR-MOB</b> <?php echo '$cuo_res=' . $cuo_res . ' || $cuotatipo_id=' . $cuotatipo_id; ?></h4>
      </div>
      <form id="form_adenda">
        <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $creditogarveh_id; ?>">
        <input type="hidden" name="hdd_proceso_id" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
        <input type="hidden" name="hdd_his_cli_id" id="hdd_his_cli_id" value="<?php echo $cliente_id; ?>">
        <input type="hidden" name="action_credito" id="action_credito" value="<?php echo $_POST['action']; ?>">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                  <div class="box box-primary">

                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-4">
                          <label>TIPO DE ADENDA :</label>
                          <select name="cmb_tip_aden" id="cmb_tip_aden" class="form-control input-sm mayus">
                            <option value="1">Crédito Regular</option>
                            <option value="2">Crédito Específico</option>
                            <option value="4">Cuota Balón</option>
                          </select>
                        </div>
                        <div class="col-md-4" id="div_cuo_res" style="display: none;">
                          <label for="txt_cre_cuo_res">CUOTAS RESTANTES :</label><br />
                          <input type="text" id="txt_cre_cuo_res" class="form-control input-sm mayus" name="txt_cre_cuo_res" value="<?php echo $cuo_res ?>" readonly>
                        </div>
                      </div>
                      <p>
                      <div class="row">
                        <div class="col-md-4">
                          <label>TIPO DE CUOTA :</label>
                          <select name="cmb_cuotip_id" id="cmb_cuotip_id" class="form-control input-sm mayus">
                            <?php require_once '../cuotatipo/cuotatipo_select.php'; ?>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <div class="row">
                            <div class="col-md-6">
                              <label for="txt_cre_perid">PERIODO :</label><br />
                              <input type="text" id="txt_cre_perid" name="txt_cre_perid" class="form-control input-sm mayus" value="MENSUAL" readonly>
                            </div>
                            <div class="col-md-6">
                              <label for="cmb_cuosubper_id">SUB-PERIODO :</label><br />
                              <select name="cmb_cuosubper_id" id="cmb_cuosubper_id" class="form-control input-sm mayus">
                                <option value="1" <?php if ($cuosubper_id == '1') {
                                                    echo 'selected';
                                                  } ?>>MENSUAL</option>
                                <option value="2" <?php if ($cuosubper_id == '2') {
                                                    echo 'selected';
                                                  } ?>>QUINCENAL</option>
                                <option value="3" <?php if ($cuosubper_id == '3') {
                                                    echo 'selected';
                                                  } ?>>SEMANAL</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="row">
                            <div class="col-md-6">
                              <label for="cmb_mon_id">MONEDA :</label><br />
                              <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm mayus">
                                <?php require '../moneda/moneda_select.php'; ?>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <label for="txt_cre_tipcam">CAMBIO :</label><br />
                              <input type="text" id="txt_cre_tipcam" class="form-control input-sm mayus" name="txt_cre_tipcam" value="<?php echo $cre_tipcam ?>" readonly>
                            </div>
                          </div>
                        </div>
                      </div>
                      <p>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="txt_cre_preaco" class="control-label">PRECIO ACORDADO :</label>
                            <input type="text" name="txt_cre_preaco" id="txt_cre_preaco" class="form-control input-sm moneda2" value="<?php echo formato_moneda($cre_preaco) ?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="txt_cre_int" class="control-label">INTERÉS (%) :</label>
                                <input type="text" name="txt_cre_int" id="txt_cre_int" class="form-control input-sm porcentaje" value="<?php echo $cre_int ?>">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="txt_cre_tcem" class="control-label">TCEM (%) :</label>
                                <input type="text" name="txt_cre_tcem" id="txt_cre_tcem" class="form-control input-sm porcentaje" value="<?php echo $cre_tcem ?>" readonly>
                              </div>
                            </div>
                            <div class="col-md-4" id="cre_fija">
                              <div class="form-group">
                                <label for="txt_cre_numcuo" class="control-label">N° CUOTAS :</label>
                                <input type="text" name="txt_cre_numcuo" id="txt_cre_numcuo" class="form-control input-sm" value="<?php echo $cre_numcuo ?>">
                              </div>
                            </div>
                            <div class="col-md-4" id="cre_libre">
                              <div class="form-group">
                                <label for="txt_cre_numcuo" class="control-label">N° CUOTAS MAX :</label>
                                <input type="text" name="txt_cre_numcuomax" id="txt_cre_numcuomax" class="form-control input-sm" value="<?php echo $cre_numcuomax ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="txt_cre_linapr" class="control-label">LÍNEA APROBADA :</label>
                            <input type="text" name="txt_cre_linapr" id="txt_cre_linapr" class="form-control input-sm moneda2" value="<?php echo $cre_linapr ?>" readonly>
                          </div>
                        </div>
                      </div>
                      <p>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="" class="control-label">CHEK PARA AGREGAR ADICIONALES: <?php echo $credito_gps;?></label><br/>
                            <label>
                              AGREGAR GPS: <input class="flat-green" type="checkbox" name="che_gps" id="che_gps" value="2" <?php if(intval($credito_gps) > 0) echo 'checked="true"'; ?> />
                            </label>
                          </div>
                          <input type="hidden" id="hdd_seguro_porcentaje" name="hdd_seguro_porcentaje"/>
                          <input type="hidden" id="hdd_gps_precio" name="hdd_gps_precio"/>
                          <input type="hidden" id="hdd_gps_simple_id" name="hdd_gps_simple_id" />
                          <span class="badge bg-green" id="span_seguro_gps"></span>
                        </div>
                        <div class="col-md-4">
                          <label for="cmb_cuedep_id">CUENTA A DEPOSITAR :</label><br />
                          <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm mayus">
                            <?php //$tipocuenta = 1 
                            ?>
                            <?php require '../cuentadeposito/cuentadeposito_garveh_select.php'; ?>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <label for="txt_ven_cli_ape">REPRESENTANTE :</label><br />
                          <select name="cmb_rep_id" id="cmb_rep_id" class="form-control input-sm mayus">
                            <?php require_once '../representante/representante_select.php'; ?>
                          </select>
                        </div>
                        
                      </div>
                      <p>
                      <div class="row">
                        <div class="col-md-12">
                          <label for="txt_ven_cli_ape">OBSERVACIÓN :</label><br />
                          <textarea name="txt_cre_obs" id="txt_cre_obs" class="form-control input-sm" rows="2" cols="95" placeholder="Describe las observaciones..."><?php echo $cre_obs ?></textarea>
                        </div>
                      </div>
                      <p>
                        <?php if ($_POST['action'] == 'adenda' || $cre_tip == 2) : ?>
                      <div class="row">
                        <div class="col-md-12">
                          <label for="txt_ven_cli_ape">Si esta ADENDA es de un crédito Garantía Mobiliaria, llena estos datos :</label><br />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <label for="txt_cre_numescr">N° ESCRITURA :</label><br />
                          <input type="text" name="txt_cre_numescr" id="txt_cre_numescr" class="form-control input-sm mayus" value="<?php echo $cre_numescr ?>">
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="">FECHA DE ESCRITURA :</label>
                            <div class="input-group">
                              <div class='input-group date' id='datetimepickera1'>
                                <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecescr" id="txt_cre_fecescr" value="<?php echo mostrar_fecha($cre_fecescr); ?>" readonly />
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <p>
                      <?php endif; ?>
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="">FECHA DE CRÉDITO :</label>
                            <div class="input-group">
                              <div class='input-group date' id='datetimepickera2'>
                                <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_feccre" id="txt_cre_feccre" value="<?php echo mostrar_fecha($cre_feccre); ?>" readonly />
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="">FCH. DESEMBOLSO :</label>
                            <div class="input-group">
                              <div class='input-group date' id='datetimepickera3'>
                                <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecdes" id="txt_cre_fecdes" value="<?php echo mostrar_fecha($cre_fecdes); ?>" readonly />
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="">FCH. FACTURACIÓN :</label>
                            <div class="input-group">
                              <div class='input-group date' id='datetimepickera4'>
                                <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecfac" id="txt_cre_fecfac" value="<?php echo mostrar_fecha($cre_fecfac); ?>" readonly />
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="">FCH. PRORRATEO :</label>
                            <div class="input-group">
                              <div class='input-group date' id='datetimepickera5'>
                                <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecpro" id="txt_cre_fecpro" value="<?php echo mostrar_fecha($cre_fecpro); ?>" readonly />
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="div_credito_cronograma"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardar_adenda">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_creditoverificacion" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/adenda_form.js?ver=51112221'; ?>"></script>