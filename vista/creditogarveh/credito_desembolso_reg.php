<?php
require_once ("../../core/usuario_sesion.php");
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

require_once ("../creditogarveh/Creditogarveh.class.php");
$oCreditogarveh = new Creditogarveh();
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ('../historial/Historial.class.php');
$oHistorial = new Historial();

$creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
$usuario_id = intval($_SESSION['usuario_id']);

if($_POST['action_desembolso'] == "insertar"){

  // GERSON (09-11-23)
  $flag_proceso = $_POST['hdd_flag_proceso'];
  $proceso_id = $_POST['hdd_proceso_id'];
  $flag_cheque = $_POST['hdd_flag_cheque'];
  $banca = $_POST['hdd_banca'];
  $banca_id = $_POST['hdd_banca_id'];

  
  /* if($flag_cheque){
    $chequeedetalle_id = intval($_POST['chequeSelect']);
    if($chequeedetalle_id <= 0){
      $data['checked'] = $chequeedetalle_id;
      $data['mensaje'] = 'Debe seleccionar un cheque válido.';
      $data['estado'] = 2;
      echo json_encode($data);
      exit();
    }
  } */

  // GERSON (13-11-23)
  if($flag_proceso){

    if($banca == 'cheque'){ // Registrar retiro de cheque
      
      if($flag_cheque){
        $chequeedetalle_id = intval($_POST['chequeSelect']);
        if($chequeedetalle_id <= 0){
          $data['checked'] = $chequeedetalle_id;
          $data['mensaje'] = 'Debe seleccionar un cheque válido.';
          $data['estado'] = 2;
          echo json_encode($data);
          exit();
        }
      }

    }elseif($banca == 'otro'){ // Registrar retiro de efectivo, depostio o transferencia

      if($banca_id == 2){ // TRANSFERENCIA

        $nro_operacion = '';
        if(isset($_POST['txt_nro_operacion_t'])){
            $nro_operacion = $_POST['txt_nro_operacion_t'];
        }
        $bancat = '';
        if(isset($_POST['txt_banca_t'])){
            $bancat = $_POST['txt_banca_t'];
        }
        $cta_origen = '';
        if(isset($_POST['txt_cta_origen_t'])){
            $cta_origen = $_POST['txt_cta_origen_t'];
        }
        $cta_destino = '';
        if(isset($_POST['txt_cta_destino_t'])){
            $cta_destino = $_POST['txt_cta_destino_t'];
        }

        if($nro_operacion == ''){
          $data['mensaje'] = 'Debe ingresar un número de operación válida.';
        }elseif($bancat == ''){
          $data['mensaje'] = 'Debe ingresar un banco válido.';
        }elseif($cta_origen == ''){
          $data['mensaje'] = 'Debe ingresar una cuenta de origen válida.';
        }elseif($cta_destino == ''){
          $data['mensaje'] = 'Debe ingresar una cuenta de destino válida.';
        }
        if($nro_operacion == '' || $bancat == '' || $cta_origen == '' || $cta_destino == ''){
          $data['estado'] = 2;
          echo json_encode($data);
          exit();
        }

      }elseif($banca_id == 3){ // DEPÓSITO

        $nro_operacion = '';
        if(isset($_POST['txt_nro_operacion_d'])){
            $nro_operacion = $_POST['txt_nro_operacion_d'];
        }
        $bancad = '';
        if(isset($_POST['txt_banca_d'])){
            $bancad = $_POST['txt_banca_d'];
        }
        $caja_id = 0;
        if(isset($_POST['cmb_caja_id_d'])){
            $caja_id = $_POST['cmb_caja_id_d'];
        }
        $cta_destino = '';
        if(isset($_POST['txt_cta_destino_d'])){
            $cta_destino = $_POST['txt_cta_destino_d'];
        }

        if($nro_operacion == ''){
          $data['mensaje'] = 'Debe ingresar un número de operación válida.';
        }elseif($bancad == ''){
          $data['mensaje'] = 'Debe ingresar un banco válido.';
        }elseif($caja_id <= 0){
          $data['mensaje'] = 'Debe seleccionar una caja válida.';
        }elseif($cta_destino == ''){
          $data['mensaje'] = 'Debe ingresar una cuenta de destino válida.';
        }
        if($nro_operacion == '' || $bancad == '' || $caja_id <= 0 || $cta_destino == ''){
          $data['estado'] = 2;
          echo json_encode($data);
          exit();
        }


      }elseif($banca_id == 4){ // EFECTIVO

        $caja_id = 0;
        if(isset($_POST['cmb_caja_id_e'])){
            $caja_id = intval($_POST['cmb_caja_id_e']);
        }

        if($caja_id <= 0){
          $data['mensaje'] = 'Debe seleccionar una caja válida.';
          $data['estado'] = 2;
          echo json_encode($data);
          exit();
        }

      }
   
    }
  }
  
  //

  if(!empty($_POST['hdd_cre_id'])){
    $codigo ='CGV-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT);
    $tip_ade = $_POST['hdd_tip_ade'];
    $cre_idori = $_POST['hdd_cre_id_ori'];

    $result = $oCreditogarveh->mostrarUno($_POST['hdd_cre_id']);
      if($result['estado']==1){
        $cli_id=$result['data']['tb_cliente_id'];
        $cli_doc = $result['data']['tb_cliente_doc'];
        $cli_nom=$result['data']['tb_cliente_nom'];
        $cli_ape=$result['data']['tb_cliente_ape'];
        $cli_dir=$result['data']['tb_cliente_dir'];
        $cli_ema=$result['data']['tb_cliente_ema'];
        $cli_fecnac=$result['data']['tb_cliente_fecnac'];
        $cli_tel=$result['data']['tb_cliente_tel'];
        $cli_cel=$result['data']['tb_cliente_cel'];
        $cli_telref=$result['data']['tb_cliente_telref'];

        $cre_tip = intval($result['data']['tb_credito_tip']); //1 general, 2 adenda, 3 ap, 4 minuta
        $mon_id=$result['data']['tb_moneda_id'];
        $credito_preaco = $result['data']['tb_credito_preaco'];
      }
    $result = "";

    //monto desembolsado, si son la misma moneda del credito con el seleccionado, el monto es cre_desm si son diferentes el monto es moncam
    $monto_desem = moneda_mysql($_POST['txt_credes_mon']);
    $mon_reten = moneda_mysql($_POST['txt_credes_ret']);
    $tipo_cambio = moneda_mysql($_POST['txt_credes_tipcam']);
    //$maximo_desembolsar = moneda_mysql($_POST['hdd_credes_mon']);

    //si ya tiene un desembolso previo no se cambia los datos del credito
    $mod_id = 53; // el modulo para garveh es 53 en egreso
    $est = 1;
    $total_desembolsado = 0;

    //la consulta de egreso se tiene que ser por los dos tipos de moneda, 1
    $result1 = $oEgreso->mostrar_por_modulo($mod_id,$_POST['hdd_cre_id'],1,$est);
      if($result1['estado']==1){
        foreach ($result1['data'] as $key => $value) {
          if($mon_id == 1)
            $total_desembolsado += $value['tb_egreso_imp'];

          if($mon_id == 2)
            $total_desembolsado += ($value['tb_egreso_imp'] / $value['tb_egreso_tipcam']);
        }
      }
    $result1 = "";

    //la consulta de egreso se tiene que ser por los dos tipos de moneda, 2
    $result1 = $oEgreso->mostrar_por_modulo($mod_id,$_POST['hdd_cre_id'],2,$est);
      if($result1['estado']==1){
        foreach ($result1['data'] as $key => $value) {      
          if($mon_id == 1)
            $total_desembolsado += ($value['tb_egreso_imp'] * $value['tb_egreso_tipcam']);

          if($mon_id == 2)
            $total_desembolsado += $value['tb_egreso_imp'];
        } 
      }
    $result1 = "";
    
    $maximo_desembolsar = formato_moneda($credito_preaco - $total_desembolsado);
    $total_egreso = $monto_desem + $mon_reten;
    $egreso_final = $monto_desem + $mon_reten;

    $deseo_desembolsar = 0; //para retenciones en diferentes monedas

    if($mon_reten > 0 && $monto_desem > 0){
      $data['mensaje'] = 'Si desea hacer una retención, no puede hacer desembolso, por favor corregir.';
      $data['estado'] = 0;
      echo json_encode($data);
      exit();
    }

    //EN CASO DE QUE SE REQUIERA EGRESAR UN MONTO CON DIFERENTE MONEDA AL CREDITO
    if($_POST['hdd_mon_id'] != $_POST['cmb_mon_id_des']){
      $total_egreso = formato_numero($_POST['txt_credes_moncam']);
      if($mon_reten > 0){
        $total_egreso = $mon_reten;
        
        if($_POST['hdd_mon_id'] == 1 && $_POST['cmb_mon_id_des'] == 2){
          $deseo_desembolsar = formato_moneda($mon_reten * $tipo_cambio);
        }
        if($_POST['hdd_mon_id'] == 2 && $_POST['cmb_mon_id_des'] == 1){
          $deseo_desembolsar = formato_moneda($mon_reten / $tipo_cambio);
        }
        if($deseo_desembolsar == 0 || $deseo_desembolsar > $maximo_desembolsar){
          $data['mensaje'] = 'Usted desea desembolsar: '.formato_moneda($deseo_desembolsar).', lo cual no es menor al máximo a desembolsar de: '.formato_moneda($maximo_desembolsar);
          $data['estado'] = 0;
          echo json_encode($data);
          exit();
        }
      }
      $moneda_dif = '. SE HIZO CON DIFERENTE MONEDA, EL CAMBIO FUE: '.$_POST['txt_credes_moncam'];
    }
    //$data['ver']= '$credito_preaco = '.$value['tb_egreso_imp'].' $total_desembolsado = '.$total_desembolsado.'';
    if(formato_moneda($_POST['txt_credes_mon']) > $maximo_desembolsar){
      $data['mensaje'] = 'Usted desea desembolsarx: '.formato_moneda($total_egreso).', lo cual no es menor al máximo a desembolsar de: '.formato_moneda($maximo_desembolsar);
      $data['estado'] = 0;
      echo json_encode($data);
      exit();
    }
        
    //EGRESO
    $doc_id = 9;
    $det = trim("DESEMBOLSO: $codigo CLIENTE: $cli_nom $cli_ape").". Detalle: ".$_POST['txt_finalidad'];
    $mod_id = 53;//EL MODULO DE GARVEH ES 53
    $numdoc = $mod_id.'-'.$_POST['hdd_cre_id'];
    $est = 1;
    $cue_id=19; //cuenta_id creditos
    $pro_id = 1;//proveedor
    $caj_id = 1;//caja
    $subcue_id = 178; // DESEMBOLSO CHEQUE TOTAL / CLIENTE 
    $cheque_motivo = intval($_POST['cmb_cheque_motivo']); //motivo de retencion: 1 entrega total, 2 desembolso tramites
    if($cheque_motivo == 2)
      $subcue_id = 180; // DESEMBOLSO CHEQUE GASTOS / PARCIALES 

    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_usumod= $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = fecha_mysql($_POST['txt_credes_fec']);
    $oEgreso->documento_id = $doc_id;
    $oEgreso->egreso_numdoc = $numdoc;
    $oEgreso->egreso_det = $det;
    $oEgreso->egreso_imp = $total_egreso;
    $oEgreso->egreso_tipcam = $_POST['txt_credes_tipcam'];
    $oEgreso->egreso_est = $est;
    $oEgreso->cuenta_id = $cue_id;
    $oEgreso->subcuenta_id = $subcue_id;
    $oEgreso->proveedor_id = $pro_id;
    $oEgreso->cliente_id = 0;
    $oEgreso->usuario_id = 0;
    $oEgreso->caja_id = $caj_id;
    $oEgreso->moneda_id = $_POST['cmb_mon_id_des'];
    $oEgreso->modulo_id = $mod_id;
    $oEgreso->egreso_modide = $_POST['hdd_cre_id'];
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0;
    
    $result2 = $oEgreso->insertar();
      if(intval($result2['estado']) == 1){
        $estado = $result2['egreso_id'];
      }
    $result2 = "";
    $hoy = date("Y-m-d H:i:s"); 
    //$data['ver'] = $hoy;
    $numdoc=$numdoc.'-'.$estado;
    $oEgreso->modificar_campo($estado,'tb_egreso_numdoc',$numdoc,'STR');
    $oEgreso->modificar_campo($estado,'tb_egreso_usumod',$_SESSION['usuario_id'],'INT');    
    $oEgreso->modificar_campo($estado,'tb_egreso_fecmod',$hoy,'STR'); 
    
    //si se ingresa un monto para retener, ese monto lo ingresamos a CAJA SEGUROS
    if($mon_reten > 0){
      $cue_id = 29; //CUENTA SEGUROS
      $subcue_id = 97; //SUBCUENTA ADICIONALES Y SEGUROS
      $mod_id = 53; //INGRESO POR SEGUROS DE GARVEH
      $ing_det = 'INGRESO POR SEGUROS DE GARVEH, ES DECIR, MONTO RETENIDO PARA PAGO FRACCIONADO O TOTAL DE ADICIONALES Y SEGUROS.';

      $numdoc = $mod_id.'-'.$_POST['hdd_cre_id'];

      $oIngreso->ingreso_usureg = $_SESSION['usuario_id'];
      $oIngreso->ingreso_usumod = $_SESSION['usuario_id'];
      $oIngreso->ingreso_fec = fecha_mysql($_POST['txt_credes_fec']);
      $oIngreso->documento_id = 8; 
      $oIngreso->ingreso_numdoc = $numdoc;
      $oIngreso->ingreso_det = $ing_det; 
      $oIngreso->ingreso_imp = $mon_reten;  
      $oIngreso->cuenta_id = $cue_id;
      $oIngreso->subcuenta_id = $subcue_id; 
      $oIngreso->cliente_id = $cli_id; 
      $oIngreso->caja_id = 15; 
      $oIngreso->moneda_id = $_POST['cmb_mon_id_des'];
      //valores que pueden ser cambiantes según requerimiento de ingreso
      $oIngreso->modulo_id = $mod_id; 
      $oIngreso->ingreso_modide = $_POST['hdd_cre_id'];
      $oIngreso->empresa_id = $_SESSION['empresa_id'];
      $oIngreso->ingreso_fecdep = ''; 
      $oIngreso->ingreso_numope = ''; 
      $oIngreso->ingreso_mondep = 0; 
      $oIngreso->ingreso_comi = 0; 
      $oIngreso->cuentadeposito_id = 0; 
      $oIngreso->banco_id = 0; 
      $oIngreso->ingreso_ap = 0; 
      $oIngreso->ingreso_detex = '';
      
      $result2 = $oIngreso->insertar();
      if(intval($result2['estado']) == 1){
          $ing_id = $result2['ingreso_id'];
      }
      $result2 = "";
      
      $numdoc=$numdoc.'-'.$ing_id;
      $oIngreso->modificar_campo($ing_id,'tb_ingreso_numdoc',$numdoc,'STR');
      $ingreso_mot = intval($_POST['cmb_credes_mot']);
      $oIngreso->modificar_campo($ing_id,'tb_ingreso_mot',$ingreso_mot,'STR');
      $oIngreso->modificar_campo($ing_id,'tb_ingreso_usumod',$_SESSION['usuario_id'],'INT');    
      $oIngreso->modificar_campo($ing_id,'tb_ingreso_fecmod', $hoy,'STR');
    }

    if($total_desembolsado == 0){
      $oCreditogarveh->modificar_campo($_POST['hdd_cre_id'],'tb_credito_comdes',$numdoc,'STR');
      $oCreditogarveh->modificar_campo($_POST['hdd_cre_id'],'tb_credito_mod', $hoy,'STR');
      $oCreditogarveh->modificar_campo($_POST['hdd_cre_id'],'tb_credito_usumod', $_SESSION['usuario_id'],'INT');
    }
    
    $moneda = 'S/.';
    if($_POST['hdd_mon_id'] == 2)
      $moneda = 'US$.';

    if($_POST['hdd_mon_id'] != $_POST['cmb_mon_id_des'] && $mon_reten > 0){
      $total_egreso = moneda_mysql($deseo_desembolsar);
      $egreso_final = moneda_mysql($deseo_desembolsar);
      $mon_selec = 'S/.';
      if($_POST['cmb_mon_id_des'] == 2)
        $mon_selec = 'US$.';
      $moneda_dif = 'La retención se está haciendo en otra moneda equivalente a: '.$mon_selec.' '.$_POST['txt_credes_ret'];
    }
    
    $finalidad = $_POST['txt_finalidad'];
    if($finalidad == ""){
        $finalidad = "No especificado";
    }
    
    //registramos el historial para el credito
    $his = '<span style="color: green;">Se ha desembolsado '.$moneda.' '. formato_moneda($egreso_final).'. Finalidad: '.$finalidad.'.'.$moneda_dif.'. Desembolsado por: <b>'.$_SESSION['usuario_nom'].'</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCreditogarveh->registro_historial($_POST['hdd_cre_id'], $his);
    $oCreditolinea->insertar($creditotipo_id, $_POST['hdd_cre_id'], $usuario_id, $his);

    $line_det = '<b>'.$_SESSION['usuario_nom'].$_SESSION['usuario_ape'].'</b> ha hecho un desembolso para el Crédito GARVEH de número: CGV-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'. El desembolso fue: '.$moneda.' '. formato_moneda($total_egreso).'. Finalidad:'.$_POST['txt_finalidad'].'. && <b>'.date('d-m-Y h:i a').'</b>';
    $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
    $oLineaTiempo->tb_lineatiempo_det = $line_det;
    $oLineaTiempo->insertar();

    //cambio de estado
    if(moneda_mysql($_POST['txt_credes_mon']) >= moneda_mysql($_POST['hdd_credes_mon']) || moneda_mysql($_POST['txt_credes_ret']) >= moneda_mysql($_POST['hdd_credes_mon']) ){
      //AUN NO PASARÁ A DESEMBOLSADO, ESPERAR PARA INGRESAR NÚMERO DE TÍTULO
      if($cre_tip == 2){
        $oCreditogarveh->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est',3,'INT'); //adenda pasa directo a VIGENTE
        $oCreditogarveh->modificar_campo($_POST['hdd_cre_id'], 'tb_credito_fecvig', date('Y-m-d'),'STR'); //PAGA DIRECTO COMISION
      }
      else
        $oCreditogarveh->modificar_campo($_POST['hdd_cre_id'],'tb_credito_est',10,'INT'); //CONFIRMACION DE DOCUMENTOS
      //$oCredito->modificar_campo($_POST['hdd_cre_id'], 'fecvig', date('Y-m-d')); AÚN NO SE PAGA COMISIÓN

      $his = '<span style="color: green;">Último desembolso que cambia al crédito a estado PENDIENTE DE APROBACIÓN DE DOCUMENTOS, desembolsado por: <b>'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      //$oCreditogarveh->registro_historial($_POST['hdd_cre_id'], $his);
      $oCreditolinea->insertar($creditotipo_id, $_POST['hdd_cre_id'], $usuario_id, $his);
    }
    
    $cuo_guardadas = $_POST['hdd_cuo_guardadas'];


    // GERSON 06-11-23
    $proceso_fase_item_id = 0;
    if(isset($_POST['hdd_proceso_fase_item_id'])){
        $proceso_fase_item_id = $_POST['hdd_proceso_fase_item_id'];
    }
    if($flag_proceso){

      if($banca == 'cheque'){ // Registrar retiro de cheque
        if($flag_cheque){

          $chequeedetalle_id = intval($_POST['chequeSelect']);

          if($chequeedetalle_id > 0){

            $egreso_id = intval($estado);

            if($oProceso->insertarRetiro($chequeedetalle_id, $egreso_id, $usuario_id)){
              $usuario_ie = $oUsuario->mostrarUno($usuario_id);
              if($usuario_id>0){
                $oHistorial->setTbHistUsureg($usuario_id);
                $oHistorial->setTbHistNomTabla("tb_proc_chequeretiro");
                $oHistorial->setTbHistRegmodid($proceso_fase_item_id);
                $oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado un desembolso <b>'.date('d-m-Y h:i a').'</b>');
                $oHistorial->insertar();
              }
            }

          }else{
            $data['checked'] = $chequeedetalle_id;
            $data['mensaje'] = 'Debe seleccionar un cheque válido.';
            $data['estado'] = 2;
            echo json_encode($data);
            exit();
          }
          
        }else{
          $data['mensaje'] = 'No se encuentran cheques registrados.';
          $data['estado'] = 3;
          echo json_encode($data);
          exit();
        }

      }elseif($banca == 'otro'){ // Registrar retiro de efectivo, depostio o transferencia

        $egreso_id = intval($estado);

        if($banca_id == 2){ // TRANSFERENCIA

          $nro_operacion = '';
          if(isset($_POST['txt_nro_operacion_t'])){
              $nro_operacion = $_POST['txt_nro_operacion_t'];
          }
          $fecha = '';
          if(isset($_POST['txt_fecha_t'])){
              $fecha = fecha_mysql($_POST['txt_fecha_t']);
          }
          $banca = '';
          if(isset($_POST['txt_banca_t'])){
              $banca = $_POST['txt_banca_t'];
          }
          $cta_origen = '';
          if(isset($_POST['txt_cta_origen_t'])){
              $cta_origen = $_POST['txt_cta_origen_t'];
          }
          $cta_destino = '';
          if(isset($_POST['txt_cta_destino_t'])){
              $cta_destino = $_POST['txt_cta_destino_t'];
          }

          if($oProceso->insertarRetiroDeposito($proceso_id, $egreso_id, 'transferencia', $fecha, $nro_operacion, $banca, null, $cta_origen, $cta_destino)){

            $usuario_ie = $oUsuario->mostrarUno($usuario_id);
            if($usuario_id>0){
              $oHistorial->setTbHistUsureg($usuario_id);
              $oHistorial->setTbHistNomTabla("tb_proc_chequeretiro");
              $oHistorial->setTbHistRegmodid($proceso_fase_item_id);
              $oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado un desembolso <b>'.date('d-m-Y h:i a').'</b>');
              $oHistorial->insertar();
            }
          }

        }elseif($banca_id == 3){ // DEPÓSITO

          $nro_operacion = '';
          if(isset($_POST['txt_nro_operacion_d'])){
              $nro_operacion = $_POST['txt_nro_operacion_d'];
          }
          $fecha = '';
          if(isset($_POST['txt_fecha_d'])){
              $fecha = fecha_mysql($_POST['txt_fecha_d']);
          }
          $banca = '';
          if(isset($_POST['txt_banca_d'])){
              $banca = $_POST['txt_banca_d'];
          }
          $caja_id = 0;
          if(isset($_POST['cmb_caja_id_d'])){
              $caja_id = $_POST['cmb_caja_id_d'];
          }
          $cta_destino = '';
          if(isset($_POST['txt_cta_destino_d'])){
              $cta_destino = $_POST['txt_cta_destino_d'];
          }

          if($oProceso->insertarRetiroDeposito($proceso_id, $egreso_id, 'deposito', $fecha, $nro_operacion, $banca, $caja_id, null, $cta_destino)){
            $usuario_ie = $oUsuario->mostrarUno($usuario_id);
            if($usuario_id>0){
              $oHistorial->setTbHistUsureg($usuario_id);
              $oHistorial->setTbHistNomTabla("tb_proc_chequeretiro");
              $oHistorial->setTbHistRegmodid($proceso_fase_item_id);
              $oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado un desembolso <b>'.date('d-m-Y h:i a').'</b>');
              $oHistorial->insertar();
            }
          }

        }elseif($banca_id == 4){ // EFECTIVO

          $fecha = '';
          if(isset($_POST['txt_fecha_e'])){
              $fecha = fecha_mysql($_POST['txt_fecha_e']);
          }
          $caja_id = 0;
          if(isset($_POST['cmb_caja_id_e'])){
              $caja_id = $_POST['cmb_caja_id_e'];
          }

          if($oProceso->insertarRetiroDeposito($proceso_id, $egreso_id, 'efectivo', $fecha, null, null, $caja_id, null, null)){
            $usuario_ie = $oUsuario->mostrarUno($usuario_id);
            if($usuario_id>0){
              $oHistorial->setTbHistUsureg($usuario_id);
              $oHistorial->setTbHistNomTabla("tb_proc_chequeretiro");
              $oHistorial->setTbHistRegmodid($proceso_fase_item_id);
              $oHistorial->setTbHistDet('El usuario <b>'.$usuario_ie['data']['tb_usuario_nom'].' '.$usuario_ie['data']['tb_usuario_ape'].'</b> ha realizado un desembolso <b>'.date('d-m-Y h:i a').'</b>');
              $oHistorial->insertar();
            }
          }

        }
        /* $data['mensaje'] = 'OTRO METODO DE DESEMBOLSO.';
        $data['estado'] = 3;
        echo json_encode($data);
        exit(); */
      }
    }
    //


    $data['mensaje'] = 'Se registró desembolso correctamente. | '.$total_egreso.' >= '.moneda_mysql($_POST['hdd_credes_mon']);

    $data['estado'] = $estado;
    $data['estado'] = 1;
  }
  else
  {
    $data['mensaje']='Intentelo nuevamente.';
  }
  
  echo json_encode($data);
}
?>
