<?php
require_once ("../funciones/funciones.php");

if($_POST['action']=="cuotas")
{

	$inicial = moneda_mysql($_POST['cre_ini']);
	$vehiculo = moneda_mysql($_POST['veh_pre']);
	$tip_ade = intval($_POST['tip_ade']);

	$C = moneda_mysql($_POST['cre_preaco']);
	$i = moneda_mysql($_POST['cre_int']);
	$n = intval($_POST['cre_numcuo']);
	if($tip_ade == 4)
  	$n = intval($_POST['cre_numcuo_res']); // si es CUOTA BALON, se suma todas las cuotas por pagar para calcular el interes y generar una sola cuota

  if($_POST['adenda'] > 0)
    $vehiculo = 1;

	if(is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($vehiculo) && $i > 0 && intval($n) > 0){
		$uno = $i / 100;
		$dos = $uno + 1;
		$tres = pow($dos,$n);
		$cuatroA = ($C * $uno) * $tres;
		$cuatroB = $tres - 1;
		$r = $cuatroA / $cuatroB;

		$sum = $r*$n;

		$data['credito_resultado'] = formato_moneda($sum);
		$data['credito_real'] = $r;
	}else{
		$data['credito_resultado'] = "";
	}

	echo json_encode($data);

}

if($_POST['action']=="suma")
{

	$veh = moneda_mysql($_POST['veh_pre']);
	$gps = moneda_mysql($_POST['gps_pre']);
	$str = moneda_mysql($_POST['str_pre']);
	$soa = moneda_mysql($_POST['soa_pre']);
	$gas = moneda_mysql($_POST['gas_pre']);
	$otr = moneda_mysql($_POST['otr_pre']);
	$ini = moneda_mysql($_POST['cre_ini']);

	$sum = $veh + $gps + $str + $soa + $gas + $otr - $ini;
	
	$data['credito_resultado'] = formato_moneda($sum);

	
	echo json_encode($data);

}

?>