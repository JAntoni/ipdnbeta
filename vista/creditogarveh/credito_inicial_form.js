$(document).ready(function () {
    $('#datetimepicker111, #datetimepicker222').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $("#datetimepicker111").on("change", function (e) {
        var startVal = $('#txt_inicial_fec1').val();
        $('#datetimepicker222').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker222").on("change", function (e) {
        var endVal = $('#txt_inicial_fec2').val();
        $('#datetimepicker111').data('datepicker').setEndDate(endVal);
    });
    

    /*$('#cmb_credito_moneda').change(function (event) {
     var val = $(this).val();
     if (val == 1 || val == 2) {
     $('#btn_buscar_inicial').attr('disabled', false);
     $('#btn_buscar_inicial').removeClass('ui-button-disabled');
     $('#btn_buscar_inicial').removeClass('ui-state-disabled');
     } else {
     $('#btn_buscar_inicial').attr('disabled', true);
     $('#btn_buscar_inicial').addClass('ui-button-disabled');
     $('#btn_buscar_inicial').addClass('ui-state-disabled');
     }
     });*/

    $('#cmb_inicial_mon_id, #cmb_tipo_ini').change(function (event) {
        credito_inicial_tabla();
    });

    $('#cmb_credito_moneda').change(function (event) {
        var mon_select = parseInt($(this).val());

        if (mon_select == 1 || mon_select == 2) {
            cambio_moneda(mon_select);
        }
    });

});

function credito_inicial_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/credito_inicial_tabla.php",
        async: true,
        dataType: "html",
        data: $('#form_inicial').serialize(),
        beforeSend: function () {
            //$('#div_credito_inicial_tabla').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#hdd_incial_id_select').val(0)
            $('#txt_inicial_utilizar').val('0.00');
            $('#div_credito_inicial_tabla').html(html);
        }
    });
}

function guardar_creditoinicial() {
    var INICIAL_ID = parseInt($('#hdd_incial_id_select').val());
    var id_uniq = $('#hdd_credito_uniq').val();
    var credito_id = $('#hdd_cre_id').val();

    if (INICIAL_ID > 0) {
        var metodo = $('#cmb_tipo_ini').val(); // 1 efectivo en caja, 2 deposito en banco
        var inicial_valido = parseFloat($('#txt_inicial_utilizar').val());
        var fecha_inicial = $('#hdd_fec_' + INICIAL_ID).val(); //despues de validar la moneda original
        var detalle = $('#hdd_det_' + INICIAL_ID).val();
        //para la creacion del crédito debemos guardar ID DE INGRESO O DEPOSITO, METODO (TIPO CAJA O DEPOSITO), inicial, fecha, detalle
        $.ajax({
            type: "POST",
            url: VISTA_URL + "creditoinicial/creditoinicial_reg.php",
            async: true,
            dataType: "json",
            data: ({
                action: 'insertar',
                creditotipo_id: 3, //tipo 3 por CREDITO GARVEH 
                creditoinicial_tipini: metodo,
                creditoinicial_idini: INICIAL_ID,
                credito_id: credito_id, //AUN NO SABEMOS 
                creditoinicial_uniq: id_uniq
            }),
            beforeSend: function () {
                //$('#msj_inicial').show(400);
                //$('#msj_inicial').html('Guardando');
            },
            success: function (data) {
                console.log(data);
                if (parseInt(data.estado) == 1) {
                    guardar_inicial(INICIAL_ID, metodo, inicial_valido, fecha_inicial, detalle);
                    $('#div_modal_creditogarveh_inicial').html('');
                    $('#modal_creditogarveh_inicial').modal('hide');
                } else
                    $('#msj_inicial').html(data);
            }
        });

    } else {
        console.log('no tiene datos ' + INICIAL_ID);
    }
}