<?php
  $credito_id = $_POST['credito_id'];

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_seguimiento" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
          <h4 class="modal-title">Seguimiento de <b>GAR-MOB</b></h4>
      </div>
      <form id="frm_creditoverificacion">
        <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
	      <input type="hidden" name="action" value="verificacion_titulo">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">

              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="font-family: cambria;font-weight: bold">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" id="panel1">VERIFICACIÓN</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" id="panel2">PLACA</a></li>
                  <li class="" id="opciontransf"><a href="#tab_3" data-toggle="tab" aria-expanded="false" id="panel3">TÍTULO</a></li>
                  <li class="" id="opcioninicial"><a href="#tab_4" data-toggle="tab" aria-expanded="false" id="panel4">OTROS</a></li>
                  <li class="pull-right"><a href="javascript:void(0)" class="text-muted"></a></li>
                </ul>
                <div class="tab-content">
                  <?php //echo ($cre_est > 1 || $cre_tip == 2)? 'disabled="true"':'';
                  ?>
                  <div class="tab-pane active" id="tab_1">
                    <?php include_once 'creditogarveh_seguimiento_verificacion.php'; ?>
                  </div>
                  <div class="tab-pane" id="tab_2">
                    <?php include_once 'creditogarveh_seguimiento_placa.php'; ?>
                  </div>
                  <div class="tab-pane" id="tab_3">
                    <?php include_once 'creditogarveh_seguimiento_titulo.php'; ?>
                  </div>
                  <div class="tab-pane" id="tab_4">
                    <?php include_once 'creditogarveh_seguimiento_otro.php'; ?>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_creditoverificacion" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_verificacion.js?ver=437433';?>"></script>
