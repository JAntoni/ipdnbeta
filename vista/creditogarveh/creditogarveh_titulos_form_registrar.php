<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../../core/usuario_sesion.php');
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../creditogarveh/Creditogarveh.class.php');

$oCreditogarveh = new Creditogarveh();

$action          = $_POST['action'];
$credito_id      = $_POST['cre_id'];
$seguimiento_id  = $_POST['seguimiento_id']; // 0
$tipo            = $_POST['tipo'];  // 1 | 2 | 3 
$cgarvtipo_id    = $_POST['cgarvtipo_id'];
$nrotitulo       = '';
$drawButton      = '';

$drawButton = '<button type="button" class="btn btn-success" onclick="addnew(' . $seguimiento_id . ',' . $credito_id . ',' . $cgarvtipo_id . ')">Registrar</button>';
?>
<form id="frm_titulo_register" name="frm_titulo_register" method="post" autocomplete="off">
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_form_actualizar" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title text-success">Registrar Datos del Título</h4>
        </div>
        <div class="modal-body">
          <p><input type="hidden" class="form-control" id="txttipotitulo" name="txttipotitulo" value="<?php echo $tipo; ?>"></p>
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label for="txtnrotitulo">Nro. de Título:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-file-text"></i></span>
                  </div>
                  <input type="text" id="txtnrotitulo" name="txtnrotitulo" class="form-control" maxlength="20" required>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label for="txtfecha">Fecha:</label>
                <input type="date" id="txtfecha" name="txtfecha" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label for="cmbestado">Estado:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-circle"></i></span>
                  </div>
                  <select name="cmbestado" id="cmbestado" class="form-control" required>
                    <option value="">Seleccione:</option>
                    <option value="1" selected>PRESENTADO</option>
                    <option value="2">REINGRESADO</option>
                    <option value="3">APELADO</option>
                    <option value="4">EN PROCESO</option>
                    <option value="5">EN CALIFICACIÓN</option>
                    <option value="6">INSCRITO</option>
                    <option value="7">RESERVADO</option>
                    <option value="8">DISTRIBUIDO</option>
                    <option value="9">LIQUIDADO</option>
                    <option value="10">PRORROGADO</option>
                    <option value="11">OBSERVADO</option>
                    <option value="12">SUSPENDIDO</option>
                    <option value="13">TACHADO</option>
                    <option value="14">ANOTADO</option>
                    <option value="15">CANCELADO</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label for="cmb_sede">Sede:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <span><i class="fa fa-map-marker"></i></span>
                  </div>
                  <select name="cmb_sede" id="cmb_sede" class="selectpicker form-control" required>
                    <option value="">Seleccione:</option>
                    <option value="LIMA"> LIMA</option>
                    <option value="ABANCAY"> ABANCAY</option>
                    <option value="ANDAHUAYLAS"> ANDAHUAYLAS</option>
                    <option value="AREQUIPA"> AREQUIPA</option>
                    <option value="AYACUCHO"> AYACUCHO</option>
                    <option value="BAGUA"> BAGUA</option>
                    <option value="BARRANCA"> BARRANCA</option>
                    <option value="CAJAMARCA"> CAJAMARCA</option>
                    <option value="CALLAO"> CALLAO</option>
                    <option value="CAMANA"> CAMANA</option>
                    <option value="CASMA"> CASMA</option>
                    <option value="CASTILLA _ APLAO"> CASTILLA _ APLAO</option>
                    <option value="CAÑETE"> CAÑETE</option>
                    <option value="CHACHAPOYAS"> CHACHAPOYAS</option>
                    <option value="CHEPEN"> CHEPEN</option>
                    <option value="CHICLAYO"> CHICLAYO</option>
                    <option value="CHIMBOTE"> CHIMBOTE</option>
                    <option value="CHINCHA"> CHINCHA</option>
                    <option value="CHOTA"> CHOTA</option>
                    <option value="CUSCO"> CUSCO</option>
                    <option value="ESPINAR"> ESPINAR</option>
                    <option value="HUACHO"> HUACHO</option>
                    <option value="HUAMACHUCO"> HUAMACHUCO</option>
                    <option value="HUANCAVELICA"> HUANCAVELICA</option>
                    <option value="HUANCAYO"> HUANCAYO</option>
                    <option value="HUANTA"> HUANTA</option>
                    <option value="HUANUCO"> HUANUCO</option>
                    <option value="HUARAL"> HUARAL</option>
                    <option value="HUARAZ"> HUARAZ</option>
                    <option value="ICA"> ICA</option>
                    <option value="ILO"> ILO</option>
                    <option value="ISLAY _ MOLLENDO"> ISLAY _ MOLLENDO</option>
                    <option value="JAEN"> JAEN</option>
                    <option value="JUANJUI"> JUANJUI</option>
                    <option value="JULIACA"> JULIACA</option>
                    <option value="LA MERCED (SELVA CENTRAL)"> LA MERCED (SELVA CENTRAL)</option>
                    <option value="MADRE DE DIOS"> MADRE DE DIOS</option>
                    <option value="MAYNAS"> MAYNAS</option>
                    <option value="MOQUEGUA"> MOQUEGUA</option>
                    <option value="MOYOBAMBA"> MOYOBAMBA</option>
                    <option value="NAZCA"> NAZCA</option>
                    <option value="OTUZCO"> OTUZCO</option>
                    <option value="PASCO"> PASCO</option>
                    <option value="PISCO"> PISCO</option>
                    <option value="PIURA"> PIURA</option>
                    <option value="PUCALLPA"> PUCALLPA</option>
                    <option value="PUNO"> PUNO</option>
                    <option value="QUILLABAMBA"> QUILLABAMBA</option>
                    <option value="SAN PEDRO"> SAN PEDRO</option>
                    <option value="SATIPO"> SATIPO</option>
                    <option value="SICUANI"> SICUANI</option>
                    <option value="SULLANA"> SULLANA</option>
                    <option value="TACNA"> TACNA</option>
                    <option value="TARAPOTO"> TARAPOTO</option>
                    <option value="TARMA"> TARMA</option>
                    <option value="TINGO MARIA"> TINGO MARIA</option>
                    <option value="TRUJILLO"> TRUJILLO</option>
                    <option value="TUMBES"> TUMBES</option>
                    <option value="YURIMAGUAS"> YURIMAGUAS</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <input type="checkbox" id="ckjustif" name="ckjustif"> &nbsp; <label for="txtobservacion">Observación:</label>
                <textarea class="form-control" name="txtobservacion" id="txtobservacion" rows="4" style="resize: vertical;"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <?php echo $drawButton; ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_titulos_form_registrar.js?ver=22102024'; ?>"></script>