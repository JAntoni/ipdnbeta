<?php
require_once('../../core/usuario_sesion.php');
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

require_once("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();

$usuario_action     = $_POST['action'];
$credito_id         = $_POST['creditogarveh_id'];
$creditotipo_id     = 3;
$vista              = $_POST['vista'];
$tb_credito_subgar  = $_POST['tb_credito_subgar'];
$tb_credito_cus     = $_POST['tb_credito_cus'];
$estado             = $_POST['estado'];

$hide02 = '';
$hide03 = '';
$hideLevantamiento = 'hide';
$titledescripción = '';
$cgarvtipo_id = 0;
$view_btnagregar = 'hide';

if ($tb_credito_subgar == 3 && $tb_credito_cus == 0) { // VISUALIZAR LOS 3
    $hide02 = '';
    $hide03 = '';
    $cgarvtipo_id = 1;
    $titledescripción = 'PRE-CONSTITUCIÓN';
} elseif ($tb_credito_subgar == 2 && $tb_credito_cus == 1) { // VISUALIZAR 1 | CON CUSTODIA :: SOLO GARANTIA
    $hide02 = 'hide';
    $hide03 = 'hide';
    $cgarvtipo_id = 3;
    $titledescripción = 'REGULAR O CONSITUCIÓN CON CUSTODIA';
} elseif ($tb_credito_subgar == 2 && $tb_credito_cus == 2) { // GARATÍA Y PODER
    $hide02 = '';
    $hide03 = 'hide';
    $cgarvtipo_id = 2;
    $titledescripción = 'REGULAR O CONSTITUCIÓN SIN CUSTODIA';
} elseif (is_numeric($tb_credito_subgar) == false && $tb_credito_cus == -1) { // GARATÍA Y PODER - GARMOB
    $hide02 = '';
    $hide03 = 'hide';
    $cgarvtipo_id = 5;
    $titledescripción = $tb_credito_subgar;
} else // otro sólo mostrar garantia
{
    $hide02 = 'hide';
    $hide03 = 'hide';
    $titledescripción = '-';
}

if ($estado == 7) {
    $hideLevantamiento = '';
    $titledescripción = '';
}

if (($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuarioperfil_id'] == 5 || $_SESSION['usuarioperfil_id'] == 6 || $_SESSION['usuario_id'] == 81 || $_SESSION['usuario_id'] == 86 || $_SESSION['usuario_id'] == 85)) { // G: GERENCIA | P: CONTABILIDAD | P. SUPERVISOR | NUEVO USUARIO 86 :: MARILIN LISBETH MOLINA AQUINO | MARIA FERNANDA ALCANTARA SOTO
    $view_btnagregar = '';
}

$consultarDatosCredGarveh = $oCredito->mostrarUno($credito_id);
$DatosCredGarveh_tb_credito_numtit   = '';
$DatosCredGarveh_tb_credito_fecescri = '';
$DatosCredGarveh_tb_credito_numescri = '';
$DatosCredGarveh_tb_credito_fecvig   = '';
$DatosCredGarveh_tb_credito_selected = '';
$DatosCredGarveh_tb_credito_observacion = 'Ninguna';
if(isset($consultarDatosCredGarveh['data']['tb_credito_numescri']) || $consultarDatosCredGarveh['data']['tb_credito_numescri'] != null )
{
    $DatosCredGarveh_tb_credito_numtit   = $consultarDatosCredGarveh['data']['tb_credito_numtit'];
    $DatosCredGarveh_tb_credito_fecescri = $consultarDatosCredGarveh['data']['tb_credito_fecescri'];
    $DatosCredGarveh_tb_credito_numescri = $consultarDatosCredGarveh['data']['tb_credito_numescri'];
    $DatosCredGarveh_tb_credito_fecvig   = $consultarDatosCredGarveh['data']['tb_credito_fecvig'];
    $DatosCredGarveh_tb_credito_selected = 'selected';
    $DatosCredGarveh_tb_credito_observacion = 'La observación se visualizará en el historial.';
}
?>
<style>
    .btn-new-update {
        padding: 5px;
        border-radius: 5px;
        background-color: #75A9F9;
        color: #fff;
    }

    .btn-new-historial {
        padding: 5px;
        border-radius: 5px;
        background-color: #00A4D3;
        color: #fff;
    }

    .shadow-now {
        transition: 1s;
    }

    .shadow-now:hover {
        transition: 1s;
        cursor: pointer;
        display: block;
        box-shadow: rgb(14 30 37 / 12%) 3px 2px 0px -2px, rgb(14 30 37 / 32%) 2px 2px 17px 3px;
        margin-bottom: 5px;
    }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_titulos_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-center"><?php echo $titledescripción; ?></h4>
            </div>
            <form id="form_credgarv_titulos">
                <p>
                    <input type="hidden" id="hdd_action" name="hdd_action" value="<?php echo $usuario_action; ?>">
                    <input type="hidden" id="hdd_creditogarveh_id" name="hdd_creditogarveh_id" value="<?php echo $credito_id; ?>">
                    <input type="hidden" id="hdd_vista" name="hdd_vista" value="<?php echo $vista; ?>">
                    <input type="hidden" id="hdd_cgarvtipo_id" name="hdd_cgarvtipo_id" value="<?php echo $cgarvtipo_id; ?>">
                    <input type="hidden" id="hdd_estado" name="hdd_estado" value="<?php echo $estado; ?>">
                </p>
                <div class="modal-body">
                    <div class="row">
                        <!-- GERSON (21-12-24) -->
                        <div class="col-md-12" style="text-align: right; padding-bottom: 10px;">
                            <?php //if($ejecucion_activa){ ?>
                            <button name="btn_volver_tabla" id="btn_volver_tabla" type="button" class="btn btn-sm btn-warning" onclick="registrar_fecha_ejecucion(0, 0, <?php echo $credito_id; ?>, 'credito')"><i class="fa fa-calendar"></i> Fecha acta incautación</button>
                            <?php //} ?>
                            <button name="btn_incautacion" id="btn_incautacion" type="button" class="btn btn-sm bg-purple" onclick="ver_gastos_ejecucion(0, 0, <?php echo $credito_id; ?>, 'credito')"><i class="fa fa-money"></i> Gastos incautación</button>
                        </div>
                        <!--  -->
                    </div>

                    <div class="row mb-4">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="panel panel-default shadow-now">
                                <div class="panel-header p-4">
                                    <h5 class="panel-title">Leyenda</h5>
                                    <div class="pull-right"><a href="https://sigueloplus.sunarp.gob.pe/siguelo/" target="_blank" class="btn btn-success btn-sm mb-4"><i class="fa fa-file-text fa-fw"></i> SIGUELO </a></div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #0b9c92; background-color: #25dbcf;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Presentado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #0e397d; background-color: #2057AC;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Reingresado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #d10d40; background-color: #EF1B52;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Distribuido</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #003d1e; background-color: #006632;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Liquidado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #ff5e00; background-color: #F38C05;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Apelado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #757575; background-color: #B4B4B4;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">En Proceso</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #468bb3; background-color: #81D1FF;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Prorrogado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #990001; background-color: #FC0002;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Observado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #3d0d54; background-color: #662485;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">En Calificación</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #5e8a08; background-color: #84BC18;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Inscrito</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #54070d; background-color: #95141E;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Suspendido</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #000000; background-color: #020202;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Tachado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #333332; background-color: #575755;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Reservado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #34607d; background-color: #6BA7CF;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Anotado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <table class="table datatable">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 3px 10px;">
                                                        <div class="square" style="width:15px; height: 13px; border:1px solid #801d20; background-color: #CD3C40;"></div>
                                                    </td>
                                                    <td style="padding: 3px 10px;">Cancelado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="container-fluid mt-4 shadow-now" style="background-color: #BFD8FF; border-radius: 8px; border: 1px solid #789acf; padding: 20px;">
                                        <div class="text-center mt-4 mb-4">
                                            <h4><b>TÍTULO DE GARANTÍA</b></h4>
                                        </div> <br>
                                        <div class="row mt-4 mb-4">
                                            <div class="col-lg-4"> <br>
                                                <span>DATOS ACTUALIZADOS</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <button type="button" class="btn btn-default pull-right rounded <?php echo $view_btnagregar; ?>" onclick="agregar(<?php echo $credito_id; ?>,1, <?php echo $cgarvtipo_id; ?>)"> <img src="./public/images/creditogarveh/plus-circle.png" alt="icon-plus" width="25">&nbsp; Agregar </button>
                                            </div>
                                            <div class="col-lg-12">
                                                <div id="div-lista-registros-garantia"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <!-- id="fragmentEP" -->
                                    <div class="container-fluid rounded shadow-now p-4 mt-4" style="background-color: #fffded; border: 1px solid #ffdd00;">
                                        <p><input type="hidden" id="hdd_cre_tip" name="hdd_cre_tip" value="<?php echo $creditotipo_id;?>"></p>
                                        <p><input type="hidden" id="hdd_cre_id" name="hdd_cre_id" value="<?php echo $credito_id;?>"></p>

                                        <div class="text-center mt-4 mb-4">
                                            <h4><b>DATOS DE LA ESCRITURA PÚBLICA</b></h4>
                                        </div> <br>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_credito_numtit" class="control-label">Número de Título:</label>
                                                    <div class='input-group'>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-file-text"></span>
                                                        </span>
                                                        <input type="text" class="form-control input-sm" id="txt_credito_numtit" name="txt_credito_numtit" required autocomplete="off" value="<?php echo $DatosCredGarveh_tb_credito_numtit;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_credito_fecescri" class="control-label">Fecha de Escritura:</label>
                                                    <div class="input-group">
                                                        <div class='input-group date' id='datetimepicker5'>
                                                            <input type='text' class="form-control input-sm" id="txt_credito_fecescri" name="txt_credito_fecescri" required autocomplete="off" value="<?php echo $DatosCredGarveh_tb_credito_fecescri;?>"/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_credito_numescri" class="control-label">Número de Escritura:</label>
                                                    <div class='input-group'>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-file-text"></span>
                                                        </span>
                                                        <input type="text" class="form-control input-sm" id="txt_credito_numescri" name="txt_credito_numescri" required autocomplete="off" value="<?php echo $DatosCredGarveh_tb_credito_numescri;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-left mt-4 mb-4">
                                            <h4><b>VERIFICACIÓN</b></h4>
                                        </div> <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cmb_verificacion_cli_si" class="control-label">¿Datos Cliente Correcto? </label>
                                                    <select id="cmb_verificacion_cli_si" name="cmb_verificacion_cli_si" class="form-control input-sm mayus">
                                                        <option value="">Seleccione...</option>
                                                        <option value="1" <?php echo $DatosCredGarveh_tb_credito_selected;?>>SI</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cmb_verificacion_emp_si" class="control-label">¿Datos Acreedor correcto? </label>
                                                    <select id="cmb_verificacion_emp_si" name="cmb_verificacion_emp_si" class="form-control input-sm mayus">
                                                        <option value="">Seleccione...</option>
                                                        <option value="1" <?php echo $DatosCredGarveh_tb_credito_selected;?>>SI</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txt_veri_observaciones" class="control-label">Observaciones :</label>
                                                    <div class='input-group'>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-list"></span>
                                                        </span>
                                                        <textarea name="txt_veri_observaciones" id="txt_veri_observaciones" class="form-control input-sm rows=" cols="70" style="resize: vertical;"><?php echo $DatosCredGarveh_tb_credito_observacion;?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <!-- <button type="button" class="btn btn-default btn-sm" id="closefragment">Cerrar</button> -->
                                            <button type="button" class="btn btn-warning btn-sm" id="btn_guardartitulo"><i class="fa fa-refresh fa-fw"></i> Actualizar</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 <?php echo $hide02; ?>">
                                    <div class="container-fluid mt-4 shadow-now" style="background-color: #D9F6FF; border-radius: 8px; border: 1px solid #71c4de; padding: 20px;">
                                        <div class="text-center mt-4 mb-4">
                                            <h4><b>TÍTULO DE PODER</b></h4>
                                        </div> <br>
                                        <div class="row mt-4 mb-4">
                                            <div class="col-lg-4"> <br>
                                                <span>DATOS ACTUALIZADOS</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <button type="button" class="btn btn-default pull-right rounded <?php echo $view_btnagregar; ?>" onclick="agregar(<?php echo $credito_id; ?>,2, <?php echo $cgarvtipo_id; ?>)"> <img src="./public/images/creditogarveh/plus-circle.png" alt="icon-plus" width="25">&nbsp; Agregar </button>
                                            </div>
                                            <div class="col-lg-12">
                                                <div id="div-lista-registros-poder"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 <?php echo $hide03; ?>">
                                    <div class="container-fluid mt-4 shadow-now" style="border-radius: 8px; border: 1px solid #dbdbdb; padding: 20px;">
                                        <div class="text-center mt-4 mb-4">
                                            <h4><b>TÍTULO DE INMATICAULACIÓM</b></h4>
                                        </div> <br>
                                        <?php if ($tb_credito_subgar == 3) {
                                            echo '<p class="text-center"><a class="btn btn-default" href="javascript:void(0)" onclick="placa_form(' . $credito_id . ')">Placa</a></p>';
                                        } ?>
                                        <div class="row mt-4 mb-4">
                                            <div class="col-lg-4"> <br>
                                                <span>DATOS ACTUALIZADOS</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <button type="button" class="btn btn-default pull-right rounded <?php echo $view_btnagregar; ?>" onclick="agregar(<?php echo $credito_id; ?>,3, <?php echo $cgarvtipo_id; ?>)"> <img src="./public/images/creditogarveh/plus-circle.png" alt="icon-plus" width="25">&nbsp; Agregar </button>
                                            </div>
                                            <div class="col-lg-12">
                                                <div id="div-lista-registros-inmatric"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 <?php echo $hideLevantamiento; ?>">
                                    <div class="container-fluid mt-4 shadow-now" style="background-color: #b9f8fa; border-radius: 8px; border: 1px solid #2be7ed; padding: 20px;">
                                        <div class="text-center mt-4 mb-4">
                                            <h4><b>LEVANTAMIENTO</b></h4>
                                        </div> <br>
                                        <div class="row mt-4 mb-4">
                                            <div class="col-lg-4"> <br>
                                                <span>DATOS ACTUALIZADOS</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <button type="button" class="btn btn-default pull-right rounded <?php echo $view_btnagregar; ?>" onclick="agregar(<?php echo $credito_id; ?>,4, <?php echo $cgarvtipo_id; ?>)"> <img src="./public/images/creditogarveh/plus-circle.png" alt="icon-plus" width="25">&nbsp; Agregar </button>
                                            </div>
                                            <div class="col-lg-12">
                                                <div id="div-lista-registros-levantamiento"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="div_creditogarveh_his_titulo"></div>
<div id="div_creditogarveh_view_frm_act_titulo"></div>
<div id="div_filestorage_form"></div>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_titulos_form.js?ver=030220251550'; ?>"></script>