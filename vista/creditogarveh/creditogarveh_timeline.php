<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../creditolinea/Creditolinea.class.php');
  $oCreditolinea = new Creditolinea();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $titulo = 'Historial de Gestión del Crédito';
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
  $timeline = '';

  $result = $oCreditolinea->listar_creditolineas(3, $_POST['cre_id']);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $timeline .= '
          <li>
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($value['tb_creditolinea_reg']).'</span>
              
              <div class="user-block separador">
                <img class="img-circle img-bordered-sm" src="'.$value['tb_usuario_fot'].'" alt="Usuario imagen">
                <span class="username"><a href="#">'.$value['tb_usuario_nom'].'</a></span>
                <span class="description">'.$value['tb_creditolinea_det'].'</span>';

        if($usuariogrupo_id == 2){
          $timeline .= '
            <a class="btn btn-danger btn-xs" onclick="creditolinea_form(\'eliminar\', '.$value['tb_creditolinea_id'].', 3, '.$value['tb_credito_id'].')"><i class="fa fa-trash"></i></a>
            <a class="btn btn-warning btn-xs" onclick="creditolinea_form(\'modificar\', '.$value['tb_creditolinea_id'].', 3, '.$value['tb_credito_id'].')"><i class="fa fa-edit"></i></a>';
        }
        $timeline .= '
              </div>
            </div>
          </li>
        ';
      }
    }
  $result = NULL;
?>
<style>
  .separador {
    padding: 10px;
    display: block;
    box-shadow: rgb(14 30 37 / 12%) 3px 2px 0px -2px, rgb(14 30 37 / 32%) 2px 2px 17px 3px;
    margin-bottom: 0px;
  }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_timeline" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      
      <div class="modal-body">
        <ul class="timeline">
          <?php echo $timeline;?>
        </ul>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-info" onclick="creditolinea_form(<?php echo "'insertar', 0, 3, ".$_POST['cre_id'];?>)">Nuevo</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>

    </div>
  </div>
</div>