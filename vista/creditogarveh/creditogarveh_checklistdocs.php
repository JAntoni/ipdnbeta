<?php
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

$proceso_id = 0;
$proceso = $oProceso->mostrarProcesoXCredito($creditogarveh_id);
if($proceso['estado'] == 1){
  $proceso_id = $proceso['data']['tb_proceso_id'];
}

?>
<?php if ($_POST['action'] == 'M' && $cre_tip != 2) {//solo aparece cuando no es adenda
  $color = 'color: orange;';
  $texto = 'NUMERO SUMINISTRO: '.$credito_suministro;
  if (empty($credito_suministro)) {
    $color = 'color: red;';
    $texto = 'FALTA ACTUALIZAR EL NUMERO SUMINISTRO';
  }
  ?>
  <div class="row">
    <input type="hidden" id="hdd_credito_suministro" name="hdd_credito_suministro" value="<?php echo $credito_suministro;?>">
    <div class="col-md-12">
      <label id="lbl_num_suministro" name="lbl_num_suministro" class="control-label" style="font-family: cambria;font-weight: bold;<?php echo $color;?>"><?php echo $texto;?>. <a onclick="actualizar_suministro()" >CLICK AQUÍ SI DESEA ACTUALIZAR</a></label>
    </div>
  </div>
<?php } ?>
<?php if ($_POST['action'] == 'M' && $credito_tip2 != 5 && $credito_tip2 != 6) : ?>
  <div class="row">
    <div class="col-md-12 callout callout-info" id="cheklist_mensaje" style="display: none;">
      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando checklist para el Crédito...</h4>
    </div>
    <!--DATOS DE FORMATOS-->
    <div class="col-md-12">
      <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> DOCUMENTOS NECESARIOS </label>
      <div class="box box-primary">
        <div class="box-body">
          <input type="hidden" name="hdd_tipopersona" id="hdd_tipopersona" value="<?php echo $cliente_tip; ?>">
          <input type="hidden" name="hdd_garvehtipo" id="hdd_garvehtipo" value="<?php echo $cgarvtipo_id; ?>">
          <input type="hidden" name="hdd_creditoid" id="hdd_creditoid" value="<?php echo $creditogarveh_id; ?>">
          <input type="hidden" name="hdd_clienteid" id="hdd_clienteid" value="<?php echo $cli_id; ?>">
          <input type="hidden" name="hdd_cre_subgar" id="hdd_cre_subgar" value="<?php echo $cre_subgar; ?>">
          <input type="hidden" name="hdd_cre_cus" id="hdd_cre_cus" value="<?php echo $cre_cus; ?>">
          <input type="hidden" name="hdd_cre_tip2" id="hdd_cre_tip2" value="<?php echo $cre_tip; ?>">
          <input type="hidden" name="hdd_proceso_id" id="hdd_proceso_id" value="<?php echo $proceso_id; ?>">
          <div class="row pdf">
            <?php //echo $listado 
            ?>
          </div>
        </div>
      </div>
    </div>
    <!--FIN DATOS DEL CLIENTE-->
  </div>
<?php endif; ?>

<?php if ($credito_tip2 == 5 || $credito_tip2 == 6) : ?>
  <div class="row">
    <div class="col-md-12">
      <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">FORMATO DE REFINANCIACIÓN DE CRÉDITOS</label>
      <div class="box box-primary">
        <div class="box-body">
          <div class="col-md-6">
            <a class="btn btn-success btn-sm" href="javascript:void(0)" onClick="upload_form_docs(<?php echo $creditogarveh_id;?>);"><i class="fa fa-plus"></i> Subir Documento Refinanciado</a>
          </div>
          <div class="col-md-6">
            <?php if ($credito_doc_ref != '') : ?>
              <a class="btn btn-danger btn-sm" href="<?php echo $credito_doc_ref;?>" target="_blank">
                <i class="fa fa-file-pdf-o"></i> Ver Documento
              </a>
            <?php endif; ?>

            <?php if ($credito_doc_ref == '') : ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4>
                  <i class="icon fa fa-ban"></i> Debes subir el documento del proceso de Refinaciado
                </h4>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!--FIN DATOS DEL CLIENTE-->
  </div>
<?php endif; ?>