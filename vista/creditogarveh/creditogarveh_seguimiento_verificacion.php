<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

$lista_comite = $oProceso->listar_comite_todos();
?>
<div class="">
  
  <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
  <input type="hidden" name="action" value="seguimiento_verificacion">

  <div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
          <?php
            if($lista_comite['estado'] == 1){
              foreach ($lista_comite['data'] as $key => $value) {
          ?>
                <div class="col-md-9">
                  <div class="form-group">
                    <?php echo $value['usuario']; ?>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <!-- <label>Verificado: </label> -->
                    <div class="">
                      <label class="radio-inline" style="padding-left: 0px;">
                        <input type="radio" name="com_verificado<?php echo $value['tb_usuario_id']; ?>" id="com_verificado<?php echo $value['tb_usuario_id']; ?>" value="s" class="flat-green" <?php /* if($verificado=='s') echo 'checked' */ ?>> SI
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="com_verificado<?php echo $value['tb_usuario_id']; ?>" id="com_verificado<?php echo $value['tb_usuario_id']; ?>" value="n" class="flat-green" <?php /* if($verificado=='n') echo 'checked' */ ?>> NO
                      </label>
                    </div>
                  </div>
                </div>
          <?php
              }
            }
          ?>
            
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 10px;">
              <label for="txt_veri_observaciones" class="control-label" >DOCUMENTOS</label>
              <div class="col-md-12">
                <input type="file" class="form-control input-sm" name="dj_file" id="dj_file" accept="application/pdf">
              </div>
            </div>
            <div class="col-md-12 p-3">
              <label for="txt_veri_observaciones" class="control-label" >OBSERVACIÓN</label>
              <div class="col-md-12 form-group">
                <textarea class="form-control input-sm" rows="4" name="txt_veri_observaciones" id="txt_veri_observaciones" placeholder="" style="resize: none;"></textarea>
              </div>
            </div>
        </div>
    </div>
  </div>
</div>
        
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_verificacion.js?ver=437433';?>"></script>
<script type="text/javascript">
$(document).ready(function(){

  $('input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

})
</script>
