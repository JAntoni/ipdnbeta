<?php

  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $credito_garveh_id = $creditogarveh_id;

  $proceso = $oProceso->mostrarProcesoXCredito($credito_garveh_id);

  if($proceso['estado'] == 1){

    $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso['data']['tb_proceso_id']);

    $comentario = '';
    if($precalificacion['estado'] == 1){
        $comentario = $precalificacion['data']['tb_precalificacion_observacion'];
    }

    // item 5: HOJA PRE CALIFICACION
    // item 6: FORMATO INGRESOS Y GASTOS

    $item_precalificacion = $oProceso->mostrarDocXItem($proceso['data']['tb_proceso_id'], 5);
    $item_ingresos_gastos = $oProceso->mostrarDocXItem($proceso['data']['tb_proceso_id'], 6);


?>
  <div class="row" id="fil_comite">

    <h4><strong>COMENTARIOS</strong></h4>
    <div class="col-md-12">

      <?php if($comentario != '' || $comentario != null){ ?>
        <p class="h5"><?php echo $comentario; ?></p>
      <?php } ?>
      <hr>

    </div>


    <h4><strong>ARCHIVOS</strong></h4>

    <?php if($item_precalificacion['estado'] == 0 && $item_ingresos_gastos['estado'] == 0) { ?>

      <div class="col-md-12">
        <h5>No se encuentra archivos subidos.</h5>
      </div>

    <?php }else{ ?>

      <?php if($item_precalificacion['estado'] == 1) { ?>
        <div class="col-md-6">
          <p class="message" style="font-family:cambria">
            <a href="<?php echo $item_precalificacion['data']['tb_multimedia_url'] ?>" class="name h5"  target="_blank">HOJA DE PRE CALIFICACIÓN <i class="fa fa-file-pdf-o" title="VER"></i> 
            </a>
          </p>
        </div>
      <?php } ?>

      <?php if($item_ingresos_gastos['estado'] == 1) { ?>
        <div class="col-md-6">
          <p class="message" style="font-family:cambria">
            <a href="<?php echo $item_ingresos_gastos['data']['tb_multimedia_url'] ?>" class="name h5"  target="_blank">FORMATO DE INGRESOS Y GASTOS <i class="fa fa-file-excel-o" title="VER"></i> 
            </a>
          </p>
        </div>
      <?php } ?>

    <?php } ?>

  </div>

<?php }else{ ?>

  <div class="row" id="fil_comite">

    <div class="col-md-12">
      <h4>No se encuentra un Proceso asociado a este Crédito.</h4>
    </div>

  </div>

<?php } ?>