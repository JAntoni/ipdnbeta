<?php
require_once('../../core/usuario_sesion.php');
require_once ("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
$fecha1 = date('d-m-Y');
$fecha2 = date('d-m-Y');
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_inicial" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Registro de <b>INICIAL</b></h4>
            </div>
            <form id="form_inicial">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <div id="msj_inicial" style="font-size: 16px;"></div>
                                    <div class="row"> 
                                        <div class="col-md-5 col-lg-5">
                                            <label for="cmb_mon_id2">ESTE CRÉDITO SERÁ EN :</label><br/>
                                            <select name="cmb_credito_moneda" id="cmb_credito_moneda" class="form-control input-sm mayus">
                                                <option value="">Seleccione...</option>
                                                <option value="1">Soles</option>
                                                <option value="2">Dólares</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row"> 
                                        <div class="col-md-4 col-lg-4">
                                            <label for="txt_fechas">FECHAS :</label><br/>
                                            <div class="input-group">
                                                <div class='input-group date' id='datetimepicker111'>
                                                    <input type='text' class="form-control input-sm" name="txt_inicial_fec1" id="txt_inicial_fec1" value="<?php echo $fecha1; ?>"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                                <span class="input-group-addon">-</span>
                                                <div class='input-group date' id='datetimepicker222'>
                                                    <input type='text' class="form-control input-sm" name="txt_inicial_fec2" id="txt_inicial_fec2" value="<?php echo $fecha2; ?>"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2">
                                            <label for="cmb_inicial_mon_id">MONEDA :</label><br/>
                                            <select name="cmb_inicial_mon_id" id="cmb_inicial_mon_id" class="form-control input-sm mayus">
                                                <?php require '../moneda/moneda_select.php'; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-lg-3">
                                            <label for="cmb_inicial_cue_id">CUENTA A DEPOSITAR :</label><br/>
                                            <select name="cmb_inicial_cue_id" id="cmb_inicial_cue_id" class="form-control input-sm mayus">
                                                <?php $tipocuenta = 1; $cuenta_id=4 ?>
                                                <?php require '../cuenta/cuenta_select.php'; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-lg-2">
                                            <label for="cmb_tipo_ini">TIPO :</label><br/>
                                            <select name="cmb_tipo_ini" id="cmb_tipo_ini" class="form-control input-sm mayus">
                                                <option value="1">Caja</option>
                                                <option value="2">Depósito</option>
                                                <option value="3">Externo</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1 col-lg-1" style="padding-top: 20px">
                                            <div class="form-group">
                                                <a class="btn btn-primary btn-sm" title="Buscar" onclick="credito_inicial_tabla()"><i class="fa fa-search"></i> </a>
                                                </div> 
                                        </div>
                                    </div>
                                    <div id="div_credito_inicial_tabla"></div>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <label for="txt_monto_total" class="control-label" >FINALMENTE SE UTILIZARÁ :</label>
                                            <input type="text" name="txt_inicial_utilizar" id="txt_inicial_utilizar" class="form-control input-sm" readonly>
                                            <input type="hidden" id = "hdd_incial_id_select">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btn_guardar_creditogarveh" onclick="guardar_creditoinicial()">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/creditogarveh/credito_inicial_form.js'?>"></script>



