<!--<div class="box box-primary">
    <div class="box-header">-->
<div class="col-md-12">
  <!-- Custom Tabs -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs" style="font-family: cambria;font-weight: bold">
      <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" id="panel1">CLIENTE</a></li>
      <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" id="panel2">VEHÍCULO</a></li>
      <li class="" id="opciontransf"><a href="#tab_3" data-toggle="tab" aria-expanded="false" id="panel3">TRANSFERENTE</a></li>
      <li class="" id="opcioninicial"><a href="#tab_4" data-toggle="tab" aria-expanded="false" id="panel4">INICIAL</a></li>
      <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false" id="panel5">CRÉDITO</a></li>
      <li class=""><a href="#tab_6" data-toggle="tab" aria-expanded="false" id="panel6">HISTORIAL</a></li>
      <li class=""><a href="#tab_7" data-toggle="tab" aria-expanded="false" id="panel7">FORMATOS</a></li>
      <?php if ($_POST['action'] == 'M' || $_POST['action'] == 'L') { ?>
        <li class=""><a href="#tab_8" data-toggle="tab" aria-expanded="false" id="panel8">CHECKLIST</a></li>
        <li class=""><a href="#tab_9" data-toggle="tab" aria-expanded="false" id="panel8">COMITÉ</a></li>
        <li class=""><a href="#tab_10" data-toggle="tab" aria-expanded="false" id="panel10">DOCS POST CIERRE</a></li>
      <?php } ?>
      <li class="pull-right"><a href="javascript:void(0)" class="text-muted"></a></li>
    </ul>
    <div class="tab-content">
      <?php //echo ($cre_est > 1 || $cre_tip == 2)? 'disabled="true"':'';
      ?>
      <div class="tab-pane active" id="tab_1">
        <?php include_once 'creditogarveh_cliente.php'; ?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2">
        <?php include_once 'creditogarveh_vehiculo.php'; ?>
      </div>
      <div class="tab-pane" id="tab_3">
        <?php include_once 'creditogarveh_transferente.php'; ?>
      </div>
      <div class="tab-pane" id="tab_4">
        <?php include_once 'creditogarveh_inicial.php'; ?>
      </div>
      <div class="tab-pane" id="tab_5">
        <?php include_once 'creditogarveh_credito.php'; ?>
      </div>
      <div class="tab-pane" id="tab_6">
        <?php include_once 'creditogarveh_historial.php'; ?>
      </div>
      <div class="tab-pane" id="tab_7">
        <?php include_once 'creditogarveh_formatos.php'; ?>
      </div>
      <div class="tab-pane" id="tab_8">
        <?php include_once 'creditogarveh_checklistdocs.php'; ?>
      </div>
      <div class="tab-pane" id="tab_9">
        <?php include_once 'creditogarveh_comite.php'; ?>
      </div>
      <div class="tab-pane" id="tab_10">
        <?php require_once 'creditogarveh_docpostcierre.php'; ?>
      </div>
    </div>
    <!-- /.tab-content -->
  </div>
  <!-- nav-tabs-custom -->
</div>

<!--    </div>
</div>-->