<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}

require_once('Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

$credito_id = $_POST['credito_id'];
$tipo = $_POST['tipo'];
// $fecha1a = $_POST['fecha1'];
// $fecha2a = $_POST['fecha2'];
// $tipogarveh = $_POST['tipogarveh'];
// $cliente_id = $_POST['cliente_id'];
// $fecha1 = date("Y-m-d", strtotime($fecha1a));
// $fecha2 = date("Y-m-d", strtotime($fecha2a));
// 
// $fecha_hoy = date('d-m-Y');

$view_btnactualizar = 'hide';
$view_btnpdf = 'hide';

$borderInmatric = '';
if ($tipo == 3) {
  $borderInmatric = 'border: 1px solid #dbdbdb';
}

$color = '';
$col = '';
$justif = '';
$result = $oCreditogarveh->proceso_titulos_listar($credito_id, $tipo, 0, 0);

if (($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuarioperfil_id'] == 5 || $_SESSION['usuarioperfil_id'] == 6 || $_SESSION['usuario_id'] == 81 || $_SESSION['usuario_id'] == 86 || $_SESSION['usuario_id'] == 85 )) { // G: GERENCIA | P: CONTABILIDAD | P. SUPERVISOR | NUEVO USUARIO 86 :: MARILIN LISBETH MOLINA AQUINO | MARIA FERNANDA ALCANTARA SOTO
  $view_btnactualizar = '';
  $view_btnpdf = '';
}

if ($result['estado'] == 1) {

  $col .= '<div class="row mt-4 mb-0">';
  foreach ($result['data'] as $key => $value) {

    // if( $value['tb_credestado_titulo_id'] == 1 )
    // {
    //   $newfecha = date("d-m-Y", strtotime($value['tb_seguimiento_fecreg']));
    //   $newhora =  date('h:i a', strtotime($value['tb_seguimiento_fecreg']));
    // }
    // else
    // {
    $newfecha = date("d-m-Y", strtotime($value['tb_seguimiento_fecmod']));
    $newhora =  date('h:i a', strtotime($value['tb_seguimiento_fecmod']));
    // }

    switch ($value['tb_credestado_titulo_id']) {
      case 1:
        $color = '25dbcf';
        break;
      case 2:
        $color = '2057AC';
        break;
      case 3:
        $color = 'F38C05';
        break;
      case 4:
        $color = 'B4B4B4';
        break;
      case 5:
        $color = '662485';
        break;
      case 6:
        $color = '84BC18';
        break;
      case 7:
        $color = '575755';
        break;
      case 8:
        $color = 'EF1B52';
        break;
      case 9:
        $color = '006632';
        break;
      case 10:
        $color = '81D1FF';
        break;
      case 11:
        $color = 'FC0002';
        break;
      case 12:
        $color = '95141E';
        break;
      case 13:
        $color = '020202';
        break;
      case 14:
        $color = '6BA7CF';
        break;
      case 15:
        $color = 'CD3C40';
        break;
      default:
        $color = 'ffffff';
        break;
    }
    
    $col .=
      '<div class="col-xs-12">
          <div class="container-fluid rounded p-4 mt-4" style="background-color: #fff; ' . $borderInmatric . '">
            <div id="spnadjpdf'.$tipo.'"></div>
            <div class="row mt-4 mb-4">
              <div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">
                <label class="mt-4">Sede: </label> &nbsp; <span> ' . $value['sede'] . ' </span>  <br>
                <label class="mt-0">Nro. de Titulo: </label> &nbsp; <span> ' . $value['tb_seguimiento_nrotitulo'] . ' </span>
              </div>
              <div class="col-lg-7 col-md-7 col-sm-9 col-xs-12 text-right">
                <button type="button" class="btn btn-new-update rounded btn-sm '.$view_btnactualizar.'" onclick=\'actualizar(' . $value['tb_credito_id'] . ',' . $value['tb_seguimiento_id'] . ')\'><img src="./public/images/creditogarveh/update.png" alt="icon-plus" width="15">&nbsp; Actualizar</button> &nbsp;
                <button type="button" class="btn btn-new-historial rounded btn-sm" onclick=\'viewHistorial(' . $value['tb_credito_id'] . ',"' . $value['tb_seguimiento_nrotitulo'] . '",' . $value['tb_seguimiento_id'] . ')\'><i class="fa fa-clock-o fa-fw"></i> Historial</button>
              </div>
            </div>
            <div class="row mt-4 mb-4">
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="container-fluid rounded p-4" style="background-color: #'.$color.';">
                  &nbsp;
                </div>
              </div>
              <div class="col-lg-4 col-md-3 col-sm-4 col-xs-10 tex-left"> <br>
                <span class="mt-4">'.$value['tb_credestado_titulo_descripcion'].'</span>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 text-right">
                <label class="mt-4">Fecha: </label> &nbsp; <span> ' . $newfecha . ' | ' . $newhora . ' </span>
              </div>
            </div>
            <div class="row mt-4 mb-4">
              <div class="col-lg-12">
                <label>Observación:</label> <br>
                <ul> ' . $value['tb_seguimiento_observacion'] . ' </ul>
              </div>
            </div>
            <div class="row mt-4 mb-4">
              <div class="col-lg-12">
                <label>Documentos adjuntos:</label>
                <button type="button" class="btn btn-danger rounded btn-sm pull-right '.$view_btnpdf.'" onclick="filestorage_form_pdf(' . $tipo . ',' . $credito_id . ',' . $value['tb_seguimiento_id'] . ')"> <i class="fa fa-plus-circle fa-fw"></i> PDF</button>
              </div>
            </div>
          <div class="row mt-4 mb-4">';
            $fragmentDocument = $oCreditogarveh->proceso_titulos_listar_documentos_pdf($value['tb_seguimiento_id']);
            if ($fragmentDocument['estado'] == 1) {
              foreach ($fragmentDocument['data'] as $key => $valueDocs) {
                $rutaArchivo = $valueDocs['filestorage_url'];
                $col .= '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center"> <a href="' . $rutaArchivo . '" target="_blank" style="font-size: 25px;" class="text-red"> <i class="fa fa-file-text"></i> </a> </div>';
              }
            }
  $col .= '</div>
        </div>
      </div>';
  }
  $col .= '</div>';
}
$result = null;

echo $col;
?>