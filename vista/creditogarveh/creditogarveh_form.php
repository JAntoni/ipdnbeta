<?php
require_once('../../core/usuario_sesion.php');
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();
require_once('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();
require_once('../transferente/Transferente.class.php');
$oTransferente = new Transferente();
require_once('../externo/Externo.class.php');
$oExterno = new Externo();
/* GERSON (20-10-23) */
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
/*  */
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'creditogarveh';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$creditogarveh_id = $_POST['creditogarveh_id'];
$proceso_id = intval($_POST['proceso_id']);
if($proceso_id == 0){
    $proc = $oProceso->mostrarProcesoXCredito($creditogarveh_id);
    if($proc['estado'] == 1){
        $proceso_id = intval($proc['data']['tb_proceso_id']);
    }
}

$action = $_POST['action'];
$total_desem = 0;

$id_uniq = uniqid();

$titulo = '';
if ($usuario_action == 'L') {
$titulo = 'Credito Garveh Registrado';
}
if ($usuario_action == 'I') {
$titulo = 'Registrar Credito Garveh';
} elseif ($usuario_action == 'M') {
$titulo = 'Editar Credito Garveh';
} elseif ($usuario_action == 'E') {
$titulo = 'Eliminar Credito Garveh';
} else {
$titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en creditogarveh
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';
$ocupacion = '';

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'creditogarveh';
    $modulo_id = $creditogarveh_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;
    //$bandera = 1;
    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if ($result['estado'] == 1) {
        $bandera = 1; // si tiene el permiso para la accion que desea hacer
        } else {
        $result = NULL;
        echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
        exit();
        }
    $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del creditogarveh por su ID
    if (intval($creditogarveh_id) > 0) {
    $dts = $oCreditogarveh->mostrarUno($creditogarveh_id);
    if ($dts['estado'] != 1) {
    $mensaje = 'No se ha encontrado ningún registro para el creditogarveh seleccionado, inténtelo nuevamente.';
    $bandera = 4;
    } else {
    //            $creditogarveh_nom = $dts['data']['tb_creditogarveh_nom'];
    //            $creditogarveh_des = $dts['data']['tb_creditogarveh_des'];
    $reg = mostrar_fecha_hora($dts['data']['tb_credito_reg']);
    $mod = mostrar_fecha_hora($dts['data']['tb_credito_mod']);
    $apr = mostrar_fecha_hora($dts['data']['tb_credito_apr']);
    $usureg = intval($dts['data']['tb_credito_usureg']);
    $usumod = intval($dts['data']['tb_credito_usumod']);
    $usuapr = intval($dts['data']['tb_credito_usuapr']);
    $cre_cus = $dts['data']['tb_credito_cus'];
    $cuotatipo_id = $dts['data']['tb_cuotatipo_id'];
    $cuosubper_id = $dts['data']['tb_cuotasubperiodo_id'];
    $moneda_id = $dts['data']['tb_moneda_id'];
    $cuentadeposito_id = $dts['data']['tb_cuentadeposito_id'];
    $representante_id = $dts['data']['tb_representante_id'];
    $cre_tipcam = $dts['data']['tb_credito_tipcam'];
    $cre_preaco = $dts['data']['tb_credito_preaco'];
    $cre_int = $dts['data']['tb_credito_int'];
    $cre_numcuo = intval($dts['data']['tb_credito_numcuo']);
    $cre_linapr = intval($dts['data']['tb_credito_linapr']);
    $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
    $cre_feccre = mostrar_fecha($dts['data']['tb_credito_feccre']);
    $cre_fecdes = mostrar_fecha($dts['data']['tb_credito_fecdes']);
    $cre_fecfac = mostrar_fecha($dts['data']['tb_credito_fecfac']);
    $cre_fecpro = mostrar_fecha($dts['data']['tb_credito_fecpro']);
    $cre_fecent = mostrar_fecha($dts['data']['tb_credito_fecent']);
    $cre_comdes = $dts['data']['tb_credito_comdes'];
    $cre_obs = $dts['data']['tb_credito_obs'];
    $cre_est = $dts['data']['tb_credito_est'];
    $cre_cargas = $dts['data']['tb_credito_carg']; //si tuviera cargas el vehículo
    $cre_fecgps = mostrar_fecha($dts['data']['tb_credito_fecgps']);
    $cre_fecstr = mostrar_fecha($dts['data']['tb_credito_fecstr']);
    $cre_fecsoat = mostrar_fecha($dts['data']['tb_credito_fecsoat']);
    $cre_fecvenimp = mostrar_fecha($dts['data']['tb_credito_fecvenimp']); //fecha de vencimiento de impuesto vehicular
    $cre_tip = $dts['data']['tb_credito_tip']; // tipo del credito 1 es credito normal, 2 es una adenda, 3 ACUERDO PAGO, 4 garantía MINUTA
    if($cre_fecgps=='0000-00-00'){
    $cre_fecgps = '';
    }
    if($cre_fecstr=='31-12-1969'){
    $cre_fecstr = '';
    }
    if($cre_fecsoat=='31-12-1969'){
    $cre_fecsoat = '';
    }
    if($cre_fecvenimp=='31-12-1969'){
    $cre_fecvenimp = '';
    }

    $cli_id = $dts['data']['tb_cliente_id'];
    $cli_doc = $dts['data']['tb_cliente_doc'];
    $cli_nom = $dts['data']['tb_cliente_nom'];
    $cli_ape = $dts['data']['tb_cliente_ape'];
    $cli_dir = $dts['data']['tb_cliente_dir'];
    $cli_ema = $dts['data']['tb_cliente_ema'];
    $cli_fecnac = $dts['data']['tb_cliente_fecnac'];
    $cli_tel = $dts['data']['tb_cliente_tel'];
    $cli_cel = $dts['data']['tb_cliente_cel'];
    $cli_telref = $dts['data']['tb_cliente_telref'];
    $cliente_protel = $dts['data']['tb_cliente_protel'];
    $cliente_procel = $dts['data']['tb_cliente_procel'];
    $cliente_protelref = $dts['data']['tb_cliente_protelref'];
    $cliente_tip = intval($dts['data']['tb_cliente_tip']);

    /* Jalar ocupacion en el modificar desde la vista de credito garveh */
    $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
    if($precalificacion['estado']==1){
        $ocupacion = $precalificacion['data']['tb_precalificacion_ocupacion'];
    }
    $cli_ocu = $ocupacion;
    /*  */

    $veh_tipus = $dts['data']['tb_credito_tipus']; //tipo de uso 1 taxi, 2 uso particular
    $veh_pre = $dts['data']['tb_credito_vehpre'];
    $vehiculomarca_id = $dts['data']['tb_vehiculomarca_id'];
    $vehiculomodelo_id = $dts['data']['tb_vehiculomodelo_id'];
    $vehiculoclase_id = $dts['data']['tb_vehiculoclase_id'];
    $vehiculotipo_id = $dts['data']['tb_vehiculotipo_id'];
    $veh_pla = $dts['data']['tb_credito_vehpla'];
    $veh_mot = $dts['data']['tb_credito_vehsermot'];
    $veh_cha = $dts['data']['tb_credito_vehsercha'];
    $veh_ano = $dts['data']['tb_credito_vehano'];
    $veh_col = $dts['data']['tb_credito_vehcol'];
    $veh_numpas = $dts['data']['tb_credito_vehnumpas'];
    $veh_numasi = $dts['data']['tb_credito_vehnumasi'];
    $veh_kil = $dts['data']['tb_credito_vehkil'];
    $gps_id = intval($dts['data']['tb_credito_gps']);
    $str_id = intval($dts['data']['tb_credito_str']);
    $soa_id = intval($dts['data']['tb_credito_soa']);
    $gas_id = intval($dts['data']['tb_credito_gas']);
    $gps_pre = $dts['data']['tb_credito_gpspre'];
    $str_pre = $dts['data']['tb_credito_strpre'];
    $soa_pre = $dts['data']['tb_credito_soapre'];
    $gas_pre = $dts['data']['tb_credito_gaspre'];
    $otr_pre = $dts['data']['tb_credito_otrpre'];
    $veh_est = $dts['data']['tb_credito_vehest'];
    $veh_ent = $dts['data']['tb_credito_vehent'];
    $veh_parele = $dts['data']['tb_credito_parele'];
    $veh_solreg = $dts['data']['tb_credito_solreg'];
    $veh_fecsol = mostrar_fecha($dts['data']['tb_credito_fecsol']);

    $veh_mode = $dts['data']['tb_credito_vehmode'];
    $veh_cate = $dts['data']['tb_credito_vehcate'];
    $veh_comb = $dts['data']['tb_credito_vehcomb'];
    $cre_valtas = $dts['data']['tb_credito_valtas'];
    $cre_valrea = $dts['data']['tb_credito_valrea'];
    $cre_valgra = $dts['data']['tb_credito_valgra'];
    $cre_vent = $dts['data']['tb_credito_vent']; //VALOR DE VENTA EN NOTARÍA

    $zona_id = $dts['data']['tb_zonaregistral_id']; //id de la zona registral

    $cre_tc = $dts['data']['tb_credito_tc']; //TIPO DE CAMBIO QUE SE LE DIO AL CLIENTE
    $cre_subgar = $dts['data']['tb_credito_subgar']; // 1 regular, 2 transferencia, 3 compra venta, 4 pre constitucion
    $cre_ini = $dts['data']['tb_credito_ini'];
    $cre_tipini = intval($dts['data']['tb_credito_tipini']); //1 tabla de ingreso, 2 tabla de deposito
    $cre_idini = intval($dts['data']['tb_credito_idini']);
    $transferente_id = intval($dts['data']['tb_transferente_id']);
    $notaria_id = $dts['data']['tb_notaria_id'];
    $cre_numescr = $dts['data']['tb_credito_numescr'];
    $cre_fecescr = $dts['data']['tb_credito_fecescr'];
    $credito_monedache = $dts['data']['tb_credito_monedache'];
    $cre_tc_2 = $dts['data']['tb_credito_tc'];
    $credito_montoche = $dts['data']['tb_credito_montoche'];
    $credito_seguro = $dts['data']['tb_credito_preseguro'];
    $credito_gps = $dts['data']['tb_credito_pregps'];

    $credito_tip2 = $dts['data']['tb_credito_tip2'];
    $credito_doc_ref = $dts['data']['tb_credito_doc_ref'];
    $credito_suministro = $dts['data']['tb_credito_suministro'];

    $titulo .= ' // '.$credito_monedache.' // '.$cre_tc_2 .' // '.$credito_montoche;
    if ($usureg > 0) {
    $rws = $oUsuario->mostrarUno($usureg);
    if ($rws['estado'] == 1) {
    $usuregnom = $rws['data']['tb_usuario_nom'] . " " . $rws['data']['tb_usuario_ape'];
    }
    $rws = null;
    }
    if ($usumod > 0) {
    $rws = $oUsuario->mostrarUno($usumod);
    if ($rws['estado'] == 1) {
    $usumodnom = $rws['data']['tb_usuario_nom'] . " " . $rws['data']['tb_usuario_ape'];
    }
    $rws = null;
    }
    if ($usuapr > 0) {
    $mos_usuapr = 1;
    $rws = $oUsuario->mostrarUno($usuapr);
    if ($rws['estado'] == 1) {
    $usuaprnom = $rws['data']['tb_usuario_nom'] . " " . $rws['data']['tb_usuario_ape'];
    }
    $rws = null;
    }
    
    
    }
    $dts = null;

    //VERIFICAMOS SI YA TIENE UN CRONOGRAMA
    $dts = $oCuota->filtrar(3, $_POST['cre_id']); //parametro 3 de garveh
    if ($dts['estado'] != 1) {
    $cronograma = count($dts['data']); //si ya tiene cronograma generado
    }
    $dts = null;

    $mod_id = 53; // el modulo para garveh es 53 en egreso
    $est = 1;
    /*
      $dts1=$oEgreso->mostrar_por_modulo($mod_id,$_POST['cre_id'],$mon_id,$est);
      $num_rows= mysql_num_rows($dts1);
      while($dt1 = mysql_fetch_array($dts1))
      {
      $total_desem+=$dt1['tb_egreso_imp'];
      }
      mysql_free_result($dts1); */

    $opc_modificar = '<strong style="color: green;">No se puede modificar los datos. Crédito aprobado.</strong>';
    //if($total_desem == 0)
    //$opc_modificar = '';

    if ($cre_est == 1)
    $opc_modificar = '';

    $transferente_doc = '';
    $transferente_nom = '';
    $inicial_fec = '';
    $inicial_met = '';
    $inicial_det = '';

        if ($cre_subgar != 'REGULAR') {
        $dts = $oTransferente->mostrarUno($transferente_id);
        if ($dts['estado'] == 1) {
        $transferente_doc = $dts['data']['tb_transferente_doc'];
        $transferente_nom = $dts['data']['tb_transferente_nom'];
        }
        $dts = NULL;

        if ($cre_tipini == 1) { //tabla ingreso
        $inicial_met = 'EFECTIVO';
        $dts = $oIngreso->mostrarUno($cre_idini);
        if ($dts['estado'] == 1) {
        $inicial_fec = mostrar_fecha($dts['data']['tb_ingreso_fec']);
        $inicial_det = $dts['data']['tb_ingreso_det'];
        }
        $dts = NULL;
        }
        if ($cre_tipini == 2) { //tabla deposito
        $inicial_met = 'DEPOSITO';
        $dts = $oDeposito->mostrarUno($cre_idini);
        if ($dts['estado'] == 1) {
        $inicial_fec = mostrar_fecha($dts['data']['tb_deposito_fec']);
        $inicial_det = $dts['data']['tb_deposito_des'] . ' | N°: ' . $dt['tb_deposito_num'];
        }
        $dts = NULL;
        }

            if ($cre_tipini == 3) { //tabla tb_externo
                $inicial_met = 'EXTERNO';
                $dts = $oExterno->mostrarUno($cre_idini);
                if ($dts['estado'] == 1) {
                    $inicial_fec = mostrar_fecha($dts['data']['tb_externo_fec']);
                    $externo_tip = intval($dts['data']['tb_externo_tip']);
                    $inicial_det = $dts['data']['tb_externo_des'] . '. Depositado en Banco: ' . $dts['data']['tb_externo_ban'] . ' | N°: ' . $dts['data']['tb_externo_num'];
                    if ($externo_tip == 1)
                        $inicial_det = $dts['data']['tb_externo_des'];
                }
                $dts = NULL;
            }
        }
    }
    else{
    $representante_id = '';
    $cre_feccre = date('d-m-Y');
    $cre_fecdes = date('d-m-Y');
    $cre_fecfac = date('d-m-Y');
    $cre_fecpro = date('d-m-Y');
    //$cre_fecdes=date('d-m-Y');
    $cuotatipo_id = 3;
    $moneda_id = 1;
    $cuentadeposito_id = 11;

    /* GERSON (20-10-23) */
    // Cuando se crea un nuevo credito desde procesos
    if($proceso_id>0){
        
        $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);
        if($precalificacion['estado']==1){
            $ocupacion = $precalificacion['data']['tb_precalificacion_ocupacion'];
        }

        $proceso = $oProceso->mostrarUno($proceso_id);

        if($proceso['estado']==1){

            // cliente
            $cliente = $oCliente->mostrarUno($proceso['data']['tb_cliente_id']);
            if($cliente['estado']==1){
                $cli_id         = $cliente['data']['tb_cliente_id'];
                $cli_doc        = $cliente['data']['tb_cliente_doc'];
                $cli_nom        = $cliente['data']['tb_cliente_nom'];
                $cli_ape        = "";
                $cli_dir        = $cliente['data']['tb_cliente_dir'];
                $cli_ema        = $cliente['data']['tb_cliente_ema'];
                $cli_fecnac     = $cliente['data']['tb_cliente_fecnac'];
                $cli_tel        = $cliente['data']['tb_cliente_tel'];
                $cli_cel        = $cliente['data']['tb_cliente_cel'];
                $cli_telref     = $cliente['data']['tb_cliente_telref'];
                $cliente_tip    = intval($cliente['data']['tb_cliente_tip']);
            }
            $cli_ocu        = $ocupacion;

            if($precalificacion['estado']==1){

                if($precalificacion['data']['tb_precalificacion_moneda_credito']=='sol'){
                    $moneda_id          = 1;
                }elseif($precalificacion['data']['tb_precalificacion_moneda_credito']=='dolar'){
                    $moneda_id          = 2;
                }
                $cuentadeposito_id  = "";
                $representante_id   = "";
                $cre_tipcam         = floatval($proceso['data']['tb_proceso_tip_cambio']);
                $cre_int            = $proceso['data']['tb_proceso_tasa'];
                $cre_numcuo         = intval($proceso['data']['tb_proceso_plazo']);
                $cre_numcuomax      = intval($proceso['data']['tb_proceso_plazo']);

                // vehiculo
                $veh_tipus          = ""; //tipo de uso 1 taxi, 2 uso particular
                $veh_pre            = $precalificacion['data']['tb_precalificacion_monto'];
                $vehiculomarca_id   = $precalificacion['data']['tb_precalificacion_marca'];
                $vehiculomodelo_id  = $precalificacion['data']['tb_precalificacion_modelo'];
                $vehiculoclase_id   = "";
                $vehiculotipo_id    = "";
                $veh_pla            = "";
                $veh_mot            = "";
                $veh_cha            = "";
                $veh_ano            = "";
                $veh_col            = "";
                $veh_numpas         = "";
                $veh_numasi         = "";
                $veh_kil            = "";
                $gps_id             = "";
                $str_id             = "";
                $soa_id             = "";
                $gas_id             = "";
                $gps_pre            = "";
                $str_pre            = "";
                $soa_pre            = "";
                $gas_pre            = "";
                $otr_pre            = "";
                $veh_est            = "";
                $veh_ent            = "";
                $veh_parele         = "";
                $veh_solreg         = "";
                $veh_fecsol         = "";

                // credito
                $cre_tc             = $proceso['data']['tb_proceso_tip_cambio']; //TIPO DE CAMBIO QUE SE LE DIO AL CLIENTE
                if($proceso['data']['tb_cgarvtipo_id']==1){ // preconstitucion
                    $cre_subgar     = 4; // 1 regular, 2 transferencia, 3 compra venta, 4 pre constitucion
                }else{
                    $cre_subgar     = ""; // 1 regular, 2 transferencia, 3 compra venta, 4 pre constitucion
                }
                $cre_ini            = $proceso['data']['tb_proceso_inicial'];
                $cre_tipini         = ""; //1 tabla de ingreso, 2 tabla de deposito
                $cre_idini          = "";
                $transferente_id    = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
                $notaria_id         = "";
                $cre_numescr        = "";
                $cre_fecescr        = "";
                $credito_monedache  = "";
                $cre_tc_2           = "";
                $credito_montoche   = "";

                if ($cre_subgar != 'REGULAR') {
                    $dts = $oTransferente->mostrarUno($transferente_id);
                    if ($dts['estado'] == 1) {
                    $transferente_doc = $dts['data']['tb_transferente_doc'];
                    $transferente_nom = $dts['data']['tb_transferente_nom'];
                    }
                    $dts = NULL;
                }
            }

            

        }
        
    }
    /*  */
    }

    
    $style_trans = 'style="display: none;"';
    $style_ini = 'style="display: none;"';
    if($cre_subgar == 'GARMOB COMPRA TERCERO' || $cre_subgar == 'PRE-CONSTITUCION'){
    $style_trans = 'style="display: block;"';
    $style_ini = 'style="display: block;"';
    }
    if($cre_subgar == 'GARMOB IPDN VENTA')
    $style_ini = 'style="display: block;"';
} else {
    $mensaje = $result['mensaje'];
    $bandera = 4;
    $result = NULL;
}
?>
<?php if ($bandera == 1): ?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditogarveh" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <!--<h4 class="modal-title"><?php //echo $titulo . ' // $direc=' . $direc . ' $usuarioperfil_id=' . $usuarioperfil_id . ' $usuario_action=' . $usuario_action . ' $creditogarveh_id=' . $creditogarveh_id. ' $cre_est='.$cre_est; ?></h4>-->
                <?php if($usuario_action=='L'){?>
                <h4 class="modal-title"><?php echo 'CREDITO INGRESADO POR : '.$usuregnom; } else{?></h4>
                <h4 class="modal-title"><?php echo $titulo; }?></h4>
            </div>
            <form id="for_cre">
                <input name="action_credito" id="action_credito" type="hidden" value="<?php echo $_POST['action'] ?>">
                <input type="hidden" id="hdd_cre_cli_id" name="hdd_cre_cli_id" value="<?php echo $cli_id ?>">
                <input type="hidden" id="hdd_cre_cli_id_ori" name="hdd_cre_cli_id_ori" value="<?php echo $cli_id ?>">
                <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $creditogarveh_id ?>">
                <!--<input name="hdd_aden_id" id="hdd_aden_id" type="hidden" value="<?php echo $adenda_id ?>">-->
                <input type="hidden" name="hdd_cre_est" id="hdd_cre_est" value="<?php echo $cre_est; ?>">
                <input type="hidden" id="hdd_vista" name="hdd_vista" value="<?php echo $_POST['vista']; ?>">
                <input type="hidden" id="hdd_proceso" name="hdd_proceso" value="<?php echo $proceso_id; ?>">
                <input type="hidden" name="hdd_credito_tip" value="<?php echo intval($cre_tip); ?>">
                <input type="hidden" name="hdd_style_trans" id="hdd_style_trans" value="<?php echo $style_trans; ?>">
                <input type="hidden" name="hdd_style_ini" id="hdd_style_ini" value="<?php echo $style_ini; ?>">

                <!-- DATOS DE TRANSFERENTE E INICIAL -->
                <input type="hidden" name="hdd_transferente_id" id="hdd_transferente_id" value="<?php echo $transferente_id; ?>">
                <input type="hidden" name="hdd_credito_idini" id="hdd_credito_idini" value="<?php echo $cre_idini; ?>">
                <input type="hidden" name="hdd_credito_tipini" id="hdd_credito_tipini" value="<?php echo $cre_tipini; ?>">
                <input type="hidden" name="hdd_credito_uniq" id="hdd_credito_uniq" value="<?php echo $id_uniq; ?>">
                
                <!-- GPS, STR, SOAT, GAS -->
                <input type="hidden" name="hdd_gps_id" id="hdd_marca_id" value="<?php echo $vehiculomarca_id; ?>">
                <input type="hidden" name="hdd_str_id" id="hdd_str_id" value="<?php echo $str_id; ?>">
                <input type="hidden" name="hdd_soa_id" id="hdd_soa_id" value="<?php echo $soa_id; ?>">
                <input type="hidden" name="hdd_gas_id" id="hdd_gas_id" value="<?php echo $gas_id; ?>">
                
                <div class="modal-body">
                    <?php include 'creditogarveh_form_vista.php'; ?>
                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($action == 'eliminar'): ?>
                      <div class="callout callout-warning">
                          <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Crédito?</h4>
                      </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="creditogarveh_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if ($usuario_action == 'I' || $usuario_action == 'M'):?>
                        <button type="hidden" class="btn btn-info" id="btn_guardar_creditogarveh" <?php if($cre_est>1){echo 'disabled';}else{echo '';} ?>>Guardar</button>
                        <?php endif; ?>
                        <?php if ($usuario_action == 'E'): ?>
                        <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Aceptar</button>
                            <?php endif; ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if ($bandera == 4): ?>
<div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_creditogarveh">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Mensaje Importante</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <p><?php echo $mensaje .' id =' .$creditogarveh_id; ?></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_form.js?ver=1122335'; ?>"></script>

