<?php if ($_POST['action'] == 'M'): ?>
    <div class="row">
        <!--DATOS DE FORMATOS-->
        <div class="col-md-12">
            <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> IMPRIMIR FORMATOS </label>
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <?php if($cre_tip == 2 && intval($cre_numescr) != 0):?>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_adenda_garmob.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> Adenda GarMob</a>
            <?php endif?>
            <!-- FORMATOS PARA CREDITO CON CUSTODIA-->
            <?php if($cre_cus==1):?>

              <?php if($cre_tip != 4 && $cre_tip != 2):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_concus_contrato_fija_y_libre.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Contrato</a>
              <?php endif?>

              <?php if($cre_tip == 4):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_gar_mobiliaria_concus.php?d1='.$creditogarveh_id.'&d2=word';?>"><i class="fa fa-print"></i> MOB WORD</a>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_gar_mobiliaria_concus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> MOB PDF</a>
              <?php endif?>
              <a class="btn btn-primary btn-sm" target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_simulador.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Simulador</a>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_mobiliaria_anexo02.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Hoja Resumen</a>
              <?php if($cre_tip != 4):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_concus_anexo06.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Anexo 06</a>
              <?php endif?>

              <!-- CONTRATO PARA CUOTAS LIBRES-->
              <?php if($cuotip_id==3):?>
                <!--DOCUMENTO DE CRONOGRAMA DE PAGOS, SOLO SI YA SE HIZO EL PRIMER DESEMBOLSO -->
                <?php if($cronograma > 0):?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo07_cronograma_libres.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Cronograma Libres</a>
                <?php endif?>
              <?php endif?>
              
              <!-- CONTRATO PARA CUOTAS FIJAS-->
              <?php if($cuotip_id==4 && $cre_tip != 4 && $cre_tip != 2):?>
                <!--DOCUMENTO DE CRONOGRAMA DE PAGOS, SOLO SI YA SE HIZO EL PRIMER DESEMBOLSO -->
                <?php if($cronograma > 0):?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo07_cronograma_fijas.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Cronograma Fijas</a>
                <?php endif?>
              <?php endif?>
            <?php endif?>
            
            <!-- FORMATOS PARA CREDITO SIN CUSTODIA-->
            <?php if($cre_cus==2):?>
              
              <!--CONTRATO PARA USO DE TAXI -->
              <?php if(($veh_tipus == 1 || $veh_tipus == 3) && $cre_tip != 4 && intval($cre_numescr) == 0):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_sincus_uso_taxi.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Contrato Uso Taxi/Urb</a>
              <?php endif?>

              <!--CONTRATO PARA USO PARTICULAR -->
              <?php if($veh_tipus == 2 && $cre_tip != 4 && intval($cre_numescr) == 0):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_sincus_uso_particular.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> Contrato Uso Particular</a>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_sincus_uso_particular.php?d1='.$creditogarveh_id.'&d2=word';?>"><i class="fa fa-print"></i> Contrato Word</a>
              <?php endif?>

              <?php if($cre_tip == 4 && $cre_subgar != 'PRE-CONSTITUCION'):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_gar_mobiliaria_sincus.php?d1='.$creditogarveh_id.'&d2=word';?>"><i class="fa fa-print"></i> MOB WORD</a>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_gar_mobiliaria_sincus.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> MOB PDF</a>
              <?php endif?>
              <?php if($cre_tip == 4 && $cre_subgar == 'GARMOB IPDN VENTA CON RD'):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_gar_mobiliaria_reconocimiento.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> RECONOCIMIENTO D.</a>
              <?php endif?>
              <?php if($cre_tip == 4 && $cre_subgar == 'PRE-CONSTITUCION'): 
                  $fec_nuevo_cont = '07-08-2020'; //desde esta fecha parte los nuevos contratos de PRE-
                ?>
                <?php if(strtotime($cre_feccre) < strtotime($fec_nuevo_cont)): ?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_preconstitucion.php?d1='.$creditogarveh_id.'&d2=word';?>"><i class="fa fa-print"></i> MOB WORD</a>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_preconstitucion.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> MOB PDF</a>
                <?php endif?>

                <?php if(strtotime($cre_feccre) > strtotime($fec_nuevo_cont)): ?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_preconstitucion_nuevo.php?d1='.$creditogarveh_id.'&d2=word';?>"><i class="fa fa-print"></i> MOB WORD</a>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_preconstitucion_nuevo.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> MOB PDF</a>
                <?php endif?>

              <?php endif?>
              <a class="btn btn-primary btn-sm" target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_simulador.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Simulador</a>

              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_mobiliaria_anexo02.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Hoja Resumen</a>

              <?php if($cre_tip != 4):?>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo_03.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Anexo 03</a>
                <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo_04.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Anexo 04</a>
              <?php endif?>

              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo_05.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Anexo 05</a>

              <!--DOCUMENTO DE CRONOGRAMA DE PAGOS, SOLO SI YA SE HIZO EL DESEMBOLSO -->
              <?php if($cronograma > 0):?>
                <?php if($cuotip_id == 4 && $cre_tip != 4):?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo07_cronograma_fijas.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Cronograma Fijas</a>
                <?php endif?>
                <?php if($cuotip_id == 3):?>
                  <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo07_cronograma_libres.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Cronograma Libres</a>
                <?php endif?>
              <?php endif?>
            <?php endif?>

            <!-- FORMATOS IGUALES PARA TODOS-->
            <?php 
              if($cre_tip == 4)
                echo '<a class="btn btn-primary btn-sm"  target="_blank"  href="https://www.ipdnsac.com/app/modulos/creditogarveh/doc_mob_concus_acta.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id.'"><i class="fa fa-print"></i> Acta</a>';
              
              if($dt['tb_credito_est'] == 8 && !empty($dt['tb_credito_file']))
                echo '<a class="btn btn-primary btn-sm"  target="_blank"  href="'.$dt['tb_credito_file'].'"><i class="fa fa-print"></i> Resolución</a>';
              if($dt['tb_credito_est'] == 8 && !empty($dt['tb_credito_img']))
                echo '<a class="btn btn-primary btn-sm"  target="_blank" title="Imagen" href="'.$dt['tb_credito_img'].'"><i class="fa fa-print"></i> Img Resolución</a>';
              if($cuotip_id == 3 && $dt['tb_credito_numcuomax'] > 12)
                echo '<a class="btn btn-primary btn-sm"  target="_blank"  href="https://www.ipdnsac.com/app/modulos/creditogarveh/doc_anexo07_cronograma_libres.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id.'&renov=1"><i class="fa fa-print"></i> Cuotas Renov.</a>';
               ?>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_letras.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id;?>"><i class="fa fa-print"></i> Letras de Cambio</a>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_poder_llave.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> Poder y Declaracion</a>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_poder_general.php?d1='.$creditogarveh_id.'&d2=pdf';?>"><i class="fa fa-print"></i> Poder General</a>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="https://www.ipdnsac.com/app/modulos/creditogarveh/pagare_gar_mob.pdf"><i class="fa fa-print"></i> Pagaré</a>
              <a class="btn btn-primary btn-sm"  target="_blank"  href="<?php echo 'https://www.ipdnsac.com/app/modulos/creditogarveh/doc_cronograma_resolver.php?d1='.$creditogarveh_id.'&d2='.$creditogarveh_id.'&fecha=no';?>"><i class="fa fa-print"></i> Resolver</a>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DATOS DEL CLIENTE-->
    </div>
<?php endif; ?>

<?php if ($_POST['action'] == 'M'): ?>
    <div class="row">
        <!--DATOS DE ADJUNTOS-->
        <div class="col-md-12">
            <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399"> ADJUNTOS </label>
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <a class="btn btn-primary btn-sm" href="javascript:void(0)" onClick="credito_file_form();"><i class="fa fa-plus"></i> Subir Imagen</a>

                        <div id="div_credito_file_tabla"></div>	
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DATOS DEL CLIENTE-->
    </div>
<?php endif; ?>