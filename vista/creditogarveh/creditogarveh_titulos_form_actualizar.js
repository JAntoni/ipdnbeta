$(document).ready(function() {
  console.log('cambios 04/10/2024');
  $(".selectpicker").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });

  $("#ckjustif").on("change", function (e) {
    validarJustificacion();
  });
});

function validarJustificacion(){
  var miCheckbox = document.getElementById('ckjustif');
  if (miCheckbox.checked) {
    $("#txtobservacion").removeAttr("readonly","readonly");
  } else {
    $("#txtobservacion").attr("readonly","readonly");
  }
}

function update(seguimiento_id,credito_id){
	$.confirm({
    title: "Advertencia",
    content: "¿Está seguro que desea guardar los cambios?",
    type: "orange",
    escapeKey: "close",
    backgroundDismiss: false,
    columnClass: "small",
    buttons: {
      formSubmit: {
        text: "Aceptar",
        btnClass: "btn-orange",
        action: function () {
          var editObservacion = 'no';
          var miCheckbox = document.getElementById('ckjustif');
          if (miCheckbox.checked) { editObservacion = 'si'; } else { editObservacion = 'no'; }
          $.ajax({
            type: "POST",
            url: VISTA_URL + "creditogarveh/creditogarveh_titulos_controller.php",
            async:true,
            dataType: "json",
            data: ({
              action: 'actualizar',
              seguimiento_id: seguimiento_id,
              credito_id: credito_id,
              p_array_datos: $("#frm_titulo_update").serialize(),
              p_checkobservacion: editObservacion
            }),
            beforeSend: function() {
              console.log('cargando controller...');
            },
            success: function(data){
              console.log(data)
              if(parseInt(data.estado) == 1){
                $('#modal_creditogarveh_form_actualizar').modal('hide');
                swal_success('Correcto', data.mensaje, 5000);
                creditogarveh_modal_titulos_listar_registros();
              }
              else{
                swal_warning('Advertencia', data.mensaje, 5000); //en generales.js
              }
            },
            complete: function (data) {
              console.log(data);
            },
            error: function (data) {
              console.log(data)
              $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
              $('#overlay_modal_mensaje').removeClass('overlay').empty();
              $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
              swal_warning('Advertencia', data.responseText, 10000);
            }
          });
        },
      },
      Cancelar: function () {
        //close
      },
    },
    onContentReady: function () {
      // bind to events
      var jc = this;
      this.$content.find("form").on("submit", function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger("click"); // reference the button and click it
      });
    },
  });
}