$(document).ready(function() {
  console.log('cambios 01/10/2024');
  validarFragmentGarantia(); // $("#fragmentEP").hide();
  creditogarveh_modal_titulos_listar_registros();

  $('#datetimepicker5' ).datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

});

$("#txtnrotitulo").keypress(function(evento){
  if (evento.which === 13){
    evento.preventDefault(); //ignore el evento
    $("#txtfecha").focus();
  }
});

function creditogarveh_modal_titulos_listar_registros() {

  var credito_id = $("#hdd_creditogarveh_id").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_titulos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: credito_id,
      tipo: 1
    }),
    beforeSend: function (data) {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      // console.log(html);
      $('#modal_mensaje').modal('hide');
      $('#div-lista-registros-garantia').html(html);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      // console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
  });

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_titulos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: credito_id,
      tipo: 2
    }),
    beforeSend: function (data) {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      // console.log(html);
      $('#modal_mensaje').modal('hide');
      $('#div-lista-registros-poder').html(html);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      // console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
  });

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_titulos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: credito_id,
      tipo: 3
    }),
    beforeSend: function (data) {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      // console.log(html);
      $('#modal_mensaje').modal('hide');
      $('#div-lista-registros-inmatric').html(html);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      // console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
  });

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/creditogarveh_titulos_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: credito_id,
      tipo: 4
    }),
    beforeSend: function (data) {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      // console.log(html);
      $('#modal_mensaje').modal('hide');
      $('#div-lista-registros-levantamiento').html(html);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      // console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
  });
  
}

function viewHistorial(credito_id,nrotitulo,seguimiento_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_timeline_titulos.php",
		async:true,
		dataType: "html",
		data: ({
			action: 'historial',
			cre_id:	credito_id,
			nrotitulo: nrotitulo,
			seguimiento_id: seguimiento_id
		}),
		beforeSend: function() {
			// console.log('cargando historial...');
    },
		success: function(html){
      // console.log(html);
			$('#div_creditogarveh_his_titulo').html(html);
      $('#modal_creditogarveh_timeline_titulo').modal('show');
      modal_hidden_bs_modal('modal_creditogarveh_timeline_titulo', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_creditogarveh_timeline_titulo'); //funcion encontrada en public/js/generales.js
		},
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
	});
}

function actualizar(credito_id,seguimiento_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_titulos_form_actualizar.php",
		async:true,
		dataType: "html",
		data: ({
			action: 'actualizar',
			cre_id:	credito_id,
			seguimiento_id: seguimiento_id,
      tipo: 0
		}),
		beforeSend: function() {
			// console.log('cargando historial...');
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      // console.log(html);
      $('#modal_mensaje').modal('hide');
			$('#div_creditogarveh_view_frm_act_titulo').html(html);
      $('#modal_creditogarveh_form_actualizar').modal('show');

      modal_hidden_bs_modal('modal_creditogarveh_form_actualizar', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_creditogarveh_form_actualizar'); //funcion encontrada en public/js/generales.js
		},
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
	});
}

function agregar(credito_id,tipo,cgarvtipo_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_titulos_form_registrar.php",
		async:true,
		dataType: "html",
		data: ({
			action: 'agregar',
			cre_id:	credito_id,
      seguimiento_id: 0,
			tipo: tipo,
			cgarvtipo_id: cgarvtipo_id
		}),
		beforeSend: function() {
			console.log('cargando creditogarveh_titulos_form_registrar.php ...');
      // $('#modal_mensaje').modal('show');
    },
		success: function(html){
      console.log(html);
      // $('#modal_mensaje').modal('hide');
			$('#div_creditogarveh_view_frm_act_titulo').html(html);
      $('#modal_creditogarveh_form_actualizar').modal('show');
      modal_hidden_bs_modal('modal_creditogarveh_form_actualizar', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_creditogarveh_form_actualizar'); //funcion encontrada en public/js/generales.js
		},
    complete: function (data) {
      console.log(data);
    },
    error: function (data) {
      console.log(data)
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      alerta_warning('Alerta', data.responseText);
    }
	});
}

function filestorage_form_pdf(tipo_titulo,credito_id,seguimiento_id){
  $.ajax({
      type: "POST",
      url: VISTA_URL + "filestorage/filestorage_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: "I", //insertar
        public_carpeta: 'pdf',
        modulo_nom: 'cgvtitulos',
        modulo_id: credito_id,
        modulo_subnom: seguimiento_id,
        filestorage_uniq: '',
        filestorage_des: tipo_titulo
      }),
      beforeSend: function () {
        // console.log('cargando mald del file storage');
      },
      success: function (data) {
        // console.log(data)
        if (data != 'sin_datos') {
          $('#div_filestorage_form').html(data);
          $('#modal_registro_filestorage').modal('show');
          modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
      },
      complete: function (data) {
      },
      error: function (data) {
        alerta_warning('Alerta', data.responseText);
      }
  });
}

function refreshSpanPDF(){
  var hdd_filestorage_des = $('#hdd_filestorage_des').val();
  $('#spnadjpdf'+hdd_filestorage_des).addClass('p-4 text-center');
  $('#spnadjpdf'+hdd_filestorage_des).empty().append('Archivo PDF adjuntado correctamente.');
  setTimeout(() => {
    $('#spnadjpdf'+hdd_filestorage_des).removeClass('p-4 text-center');
    $('#spnadjpdf'+hdd_filestorage_des).empty();
  }, 3500);
}

// function viewfragmentEP(credito_id,creditotipo_id,nrotitulo){
//   $("#fragmentEP").show(300);
//   $("#hdd_cre_id").val(''+credito_id);
//   $("#hdd_cre_tip").val(''+creditotipo_id);
//   $("#txt_credito_numtit").val(''+nrotitulo);
// }

$("#btn_guardartitulo").click(function() {
  registrarEP();
});

// $("#closefragment").click(function() {
//   $("#txt_credito_numtit").val('');
//   $("#fragmentEP").hide(300);
// });

function validarFragmentGarantia(){
  setTimeout(() => {
    if($('#div-lista-registros-garantia').html()) {
      $('#txt_credito_numtit').removeAttr('disabled', 'disabled');
      $('#txt_credito_fecescri').removeAttr('disabled', 'disabled');
      $('#txt_credito_numescri').removeAttr('disabled', 'disabled');
      $('#cmb_verificacion_cli_si').removeAttr('disabled', 'disabled');
      $('#cmb_verificacion_emp_si').removeAttr('disabled', 'disabled');
      $('#txt_veri_observaciones').removeAttr('disabled', 'disabled');
      $('#btn_guardartitulo').removeAttr('disabled', 'disabled');
    } else {    
      $('#txt_credito_numtit').attr('disabled', 'disabled');
      $('#txt_credito_fecescri').attr('disabled', 'disabled');
      $('#txt_credito_numescri').attr('disabled', 'disabled');
      $('#cmb_verificacion_cli_si').attr('disabled', 'disabled');
      $('#cmb_verificacion_emp_si').attr('disabled', 'disabled');
      $('#txt_veri_observaciones').attr('disabled', 'disabled');
      $('#btn_guardartitulo').attr('disabled', 'disabled');      
    }
  }, 800);
}

function registrarEP() {
  if( $.trim($("#txt_credito_numtit").val()) == "" )
  {
    alerta_warning("Advertencia","Debe ingresar el número de título");
    return 0;
  }
  else if( $.trim($("#txt_credito_fecescri").val()) == "" )
  {
    alerta_warning("Advertencia","Debe ingresar fecha de la escritura pública");
    return 0;
  }
  else if( $.trim($("#txt_credito_numescri").val()) == "" )
  {
    alerta_warning("Advertencia","Debe ingresar el número de escritura pública");
    return 0;
  }
  else
  {
    $.confirm({
      title: "Advertencia",
      content:'¿Está seguro que desea registrar los datos? <br> Al confirmar, estaría guardando los datos de la escritura pública y confirmando que los datos del título están correctos',
      type: 'blue',
      escapeKey: 'close',
      backgroundDismiss: true,
      columnClass: 'small',
      buttons: {
        guardar: {
          text: 'Si, confirmar',
          btnClass: 'btn-blue',
          action: function () {
            $.ajax({
              type: "POST",
              url: VISTA_URL + "titulo/titulo_reg.php",
              async:true,
              dataType: "json",
              data: ({
                hdd_cre_tip: $('#hdd_cre_tip').val(),
                hdd_cre_id:	$('#hdd_cre_id').val(),
                txt_credito_fecescri: $('#txt_credito_fecescri').val(),
                txt_credito_numtit: $('#txt_credito_numtit').val(),
                txt_credito_numescri: $('#txt_credito_numescri').val(),
                txt_veri_observaciones: $('#txt_veri_observaciones').val(),
              }),
              beforeSend: function() {
                //$('#msj_titulo').html("Guardando...");
                //$('#msj_titulo').show(100);
              },
              success: function(data){
                if(parseInt(data.estado) > 0){ swal_success("Correcto",data.titulo_msj,5000); } else { swal_warning("Advertencia",data,5000); }
              },
              complete: function(data){
                // console.log(data);
              }
            });          
          }
        },
        cancelar: function () {
        }
      }
    });
  }
}

/* GERSON (22-12-24) */
function ver_gastos_ejecucion(ejecucion_id, ejecucion_fase, credito_id, origen){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"ejecucion/ejecucion_gastos.php",
		async:true,
		dataType: "html",                      
		data: ({
			ejecucion_id: ejecucion_id,
			ejecucion_fase: ejecucion_fase,
      credito_id: credito_id,
      origen: origen
		}),
      beforeSend: function() {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
		success: function(html){
      $('#div_modal_gastos_ejecucion').html(html);		
      $('#modal_gastos_ejecucion').modal('show');
      $('#modal_mensaje').modal('hide');
    
      modal_hidden_bs_modal('modal_gastos_ejecucion', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_gastos_ejecucion'); //funcion encontrada en public/js/generales.js
		},
      complete: function (html) {

      }
	});
}

function registrar_fecha_ejecucion(ejecucion_id, ejecucion_fase, credito_id, origen){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"ejecucion/ejecucion_gastos_fecha.php",
		async:true,
		dataType: "html",                      
		data: ({
			ejecucion_id: ejecucion_id,
			ejecucion_fase: ejecucion_fase,
      credito_id: credito_id,
      origen: origen
		}),
      beforeSend: function() {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
		success: function(html){
      $('#div_modal_gastos_ejecucion_fecha').html(html);		
      $('#modal_gastos_ejecucion_fecha').modal('show');
      $('#modal_mensaje').modal('hide');
    
      modal_hidden_bs_modal('modal_gastos_ejecucion_fecha', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_gastos_ejecucion_fecha'); //funcion encontrada en public/js/generales.js
		},
      complete: function (html) {

      }
	});
}
/*  */