<?php
  session_name("ipdnsac");
  session_start();
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 admin, 3 ventas

  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'creditogarveh/Creditogarveh.class.php');
    require_once(VISTA_URL.'cuota/Cuota.class.php');
    require_once(VISTA_URL.'cuota/Cuotadetalle.class.php');
    require_once(VISTA_URL.'cuotapago/Cuotapago.class.php');
    require_once(VISTA_URL.'ingreso/Ingreso.class.php');
    require_once(VISTA_URL.'clientecaja/Clientecaja.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../creditogarveh/Creditogarveh.class.php');
    require_once('../cuota/Cuota.class.php');
    require_once('../cuota/Cuotadetalle.class.php');
    require_once('../cuotapago/Cuotapago.class.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../clientecaja/Clientecaja.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }
  $oCredito = new Creditogarveh();
  $oCuota = new Cuota();
  $oCuotadetalle = new Cuotadetalle();
  $oIngreso = new Ingreso();
  $oCuotapago = new Cuotapago();
  $oClientecaja = new Clientecaja();
  $clicaj = 0;
  $credito_id = (isset($_POST['credito_id']))? intval($_POST['credito_id']) : $credito_id;

  $dts=$oCredito->mostrarUno($credito_id);
  if($dts['estado'] == 1){
     $mon_id=$dts['data']['tb_moneda_id'];
     $cli_id=$dts['data']['tb_cliente_id'];
  }
  $creditotipo_id = (isset($_POST['creditotipo_id']))? intval($_POST['creditotipo_id']) : $creditotipo_id;
  $credito_tabla = '';

  if($creditotipo_id == 1) $credito_tabla = 'tb_creditomenor';
  if($creditotipo_id == 2) $credito_tabla = 'tb_creditoasiveh';
  if($creditotipo_id == 3) $credito_tabla = 'tb_creditogarveh';
  if($creditotipo_id == 4) $credito_tabla = 'tb_creditohipo';

  $dts1 = $oClientecaja->filtrar_por_credito($cli_id,$mon_id,$credito_id, $credito_tabla);
    $cli_caja_ad = 0;
    if($dts1['estado'] == 1){
    foreach ($dts1['data'] as $key => $dt1)
        {
          if($dt1['tb_clientecaja_tip']==1)
          {
            $cli_caja_ad = $cli_caja_ad + $dt1['tb_clientecaja_mon'];
          }
          if($dt1['tb_clientecaja_tip']==2)
          {
            $cli_caja_ad = $cli_caja_ad - $dt1['tb_clientecaja_mon'];
          }
        }
    }
      
  function estado_cuota($cuota_est){
    $estado = '<span class="badge bg-orange">POR COBRAR</span>';
    if($cuota_est == 2)
      $estado = '<span class="badge bg-green">CANCELADO</span>';
    if($cuota_est == 3)
      $estado = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
    return $estado;
  }

?>
<script type="text/javascript">
  $('.chk_acu').change(function() {
    //replace(/[^0-9\.]+/g,"")
    var acuerdo = Number($('#hdd_his_mon_acu').val()); //input general que suma todos los montos
    var monto = Number($(this).attr('monto')); //monto de la cuota detalle
    console.log("acuerdo ="+ acuerdo + " monto ="+monto);
    if($(this).is(":checked")){
        acuerdo = acuerdo + monto;
        $('#hdd_his_mon_acu').val(parseFloat(acuerdo).toFixed(2));
        console.log(acuerdo);  
    }
    else{
      acuerdo = acuerdo - monto;
      $('#hdd_his_mon_acu').val(parseFloat(acuerdo).toFixed(2));
      console.log(acuerdo);      
    }
  });
</script>
<table id="tbl_cuota" class="table table-bordered">
  <thead>
    <tr>
      <th>N°</th>
      <th>Fecha</th>
      <th>Cuota</th>
      <th>Estado</th>
      <th>Mora</th>
      <th>Detalle</th>
      <th>Pagos</th>
      <th>Saldo</th>
      <th>Deuda</th>
      <th>Cliente Caja</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $deuda_total = 0; $clientecaja_cuota = 0;
    $result = $oCuota->cuotas_credito($credito_id, $creditotipo_id, $credito_tabla);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr class="odd">
            <td><?php echo '<b>'.$value['tb_cuota_num'].'/'.$value['tb_credito_numcuomax'].'</b>';?></td>
            <td><?php echo '<b>'.mostrar_fecha($value['tb_cuota_fec']);?></td>
            <td><?php echo '<b>'.$value['tb_moneda_nom'].' '.mostrar_moneda($value['tb_cuota_cuo']).'</b>';?></td>
            <td><?php echo '<b>'.estado_cuota($value['tb_cuota_est']).'</b>';?></td>
            <td align="left" nowrap colspan="7"></td>
            <td> 
          </tr>
          <?php
          $result4=$oCuotadetalle->filtrar($value['tb_cuota_id']);
          if($result4['estado'] == 1){
              foreach ($result4['data'] as $key => $value4){
                $estado="";
                if($value4['tb_cuotadetalle_est']==1)$estado = 'POR COBRAR';
                if($value4['tb_cuotadetalle_est']==2)$estado = 'CANCELADA';
                if($value4['tb_cuotadetalle_est']==3)$estado = 'PAGO PARCIAL';
                $vencida = 1;
                if(strtotime($fecha_hoy) >= strtotime($value4['tb_cuotadetalle_fec']) && $estado != 'CANCELADA'){
                  $vencida = 0;
                }
              ?>
              <tr class="even">
                <td align="center" nowrap><?php echo $value4['tb_cuotadetalle_num']."/".$value['tb_cuota_persubcuo']?></td>
                <td align="center" nowrap <?php if($vencida == 0) echo 'style="color: red;"';?>><?php echo $value4['tb_cuotadetalle_fec'];?></td>
                <td align="right" nowrap>
                  <?php 
                    echo $value4['tb_moneda_nom']." ". mostrar_moneda($value4['tb_cuotadetalle_cuo']);
                  ?>      
                </td>
                <td align="center"><?php echo $estado;?></td>
                <td align="center" nowrap>
                  <?php 
                    echo $value4['tb_moneda_nom']." ".mostrar_moneda($value4['tb_cuotadetalle_mor']);
                    if($value4['tb_cuotadetalle_moraut']==1)echo '<br>(Aut)';
                  ?>
                </td>
                <td align="center" nowrap>
                <?php
                    $est = $value4['tb_cuotadetalle_est']; //si está pagado o no
                    $estado_ap = $value4['tb_cuotadetalle_estap'];//si es 1 es xq se paga con AP
                    $acupag = $value4['tb_cuotadetalle_acupag'];//si es una cuota de Acuerdopago
                    
                  $pagos_cuota = 0; 
                  $saldo_cuota = 0;

                  $modulo_id = 2; $cuotapago_modid = $value4['tb_cuotadetalle_id'];
                  
                    $result2 = $oCuotapago->mostrar_cuotapagos_modulo($modulo_id, $cuotapago_modid);
                    if($result2['estado'] == 1){
                      $numero_pago = 1;
                      foreach ($result2['data'] as $key => $value2){ 
                        ?>
                          <table class="table" style="border: 2px solid #333; margin-bottom: 0px;">
                            <tr>
                              <th width="40%"><?php 
                                echo '<a href="javascript:void(0)" onclick="cuotapago_imppos_datos3('.$value2['tb_cuotapago_id'].')" title="IMPRIMIR TICKET">Pago '.$numero_pago.'</a>';
                                //echo '<a href="javascript:void(0)" onclick="cuotapago_imppos_datos('.$value2['tb_cuotapago_id'].')">Pago '.$numero_pago.'</a>';
                                /* GERSON (12-01-25) */
                                if($_SESSION['usuario_id']==2 || $_SESSION['usuario_id']==11 || $_SESSION['usuario_id']==61){ // Usuarios
                                  echo '<a href="javascript:void(0)" onclick="cuotapago_anular_garveh('.$value2['tb_cuotapago_id'].')" style="margin-left: 12%;" title="ANULAR PAGO">Anular</a>';
                                }else{
                                  echo '<a href="javascript:void(0)" onclick="solicitar_anular('.$value2['tb_cuotapago_id'].','.$credito_id.','.$creditotipo_id.')" style="margin-left: 12%;">Solic. Anular</a>';
                                }
                                /*  */
                                ?>
                              </th>
                              <th width="30%"><?php
                                echo '<a href="javascript:void(0)" onclick="cuotapago_detalle('.$value2['tb_cuotapago_id'].')" title="HISTORIAL INGRESOS">Ingreso</a>';?>
                              </th>
                              <th width="30%">Caj Cli</th>
                            </tr>
                            <tr>
                              <td><?php echo mostrar_fecha($value2['tb_cuotapago_fec']).' | '.$value2['tb_moneda_nom'].' '.mostrar_moneda($value2['tb_cuotapago_mon']);?></td>
                              <td>
                                <?php
                                  $modulo_id = 30; $ingreso_modide = $value2['tb_cuotapago_id'];
                                  $result3 = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
                                    if($result3['estado'] == 1){
                                    foreach ($result3['data'] as $key => $value3){
                                      echo '<dd>'.$value3['tb_moneda_nom'].' '.mostrar_moneda($value3['tb_ingreso_imp']).'</dd>';
                                      $pagos_cuota += formato_numero($value3['tb_ingreso_imp']);
                                    }
                                    }
                                    else
                                    echo mostrar_moneda(0);
                                  $result3 = NULL;
                                ?>
                              </td>
                              <td>
                                <?php
                                  $modulo_id = 30; $clientecaja_modid = $value2['tb_cuotapago_id'];
                                  $result5 = $oClientecaja->mostrar_clientecaja_modulo($modulo_id, $clientecaja_modid);
                                    if($result5['estado'] == 1){
                                    foreach ($result5['data'] as $key => $value5){
                                      if(intval($value5['tb_clientecaja_tip']) == 1){
                                        echo '<dd>+ '.$value5['tb_moneda_nom'].' '.mostrar_moneda($value5['tb_clientecaja_mon']).'</dd>';
                                        $clicaj = $clicaj + $value5['tb_clientecaja_mon'];
                                      }
                                      if(intval($value5['tb_clientecaja_tip']) == 2){
                                        echo '<dd>- '.$value5['tb_moneda_nom'].' '.mostrar_moneda($value5['tb_clientecaja_mon']).'</dd>';
                                        $clicaj = $clicaj - $value5['tb_clientecaja_mon'];
                                      }
                                    }
                                    }
                                    else
                                    echo $value2['tb_moneda_nom'].' '.mostrar_moneda(0);
                                  $result3 = NULL;
                                ?>
                              </td>
                            </tr>
                          </table>
                        <?php
                        $numero_pago ++;
                          //REVISAR PORQUE SE SETEA EN EL ULTIMO//$info_clicaja = $value4['tb_moneda_nom'] . ' ' . formato_money($clientecaja_cuota).'<br> AD('.formato_money($clientecaja_mas).')<br>= <b>'.formato_money(abs($clientecaja_mas + $clientecaja_cuota)).'</b>';
                      }
                    }
                    $result2 = NULL;
                  
                  //$clientecaja_cuota = $clientecaja_mas - $clientecaja_menos;
                  if($estado_ap == 1 && $est == 3){
                    echo '<strong>EL SALDO SE PAGARÁ POR ACUERDO DE PAGO</strong>';
                  }
                  if($estado_ap == 1 && $est == 1){
                    echo '<strong>ESTA CUOTA SE PAGARÁ POR ACUERDO DE PAGO</strong>';
                  }

                  $saldo_cuota = formato_numero($value['tb_cuota_cuo']) - formato_numero($pagos_cuota);
                  $deuda_total += formato_numero($saldo_cuota);
                  $monto_acuerdo = $saldo_cuota;
                  $info_clicaja = $value2['tb_moneda_nom'].' '. formato_numero($clicaj) .'<br> AD('.formato_numero($cli_caja_ad).')<br>= <b>'.formato_numero(abs($cli_caja_ad + $clicaj)).'</b>';
                  if(intval($cuotip_id) == 3){
                      $deuda_total = $saldo_cuota;
                      //si es libre entonces el monto para sumar a un AP debe ser el interés de la cuota menos los pagos
                      $monto_acuerdo = formato_moneda($value['tb_cuota_int'] - $pagos);
                    }        
                ?>
            </td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($pagos_cuota); ?></td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($saldo_cuota); ?></td>
            <td><?php echo $value2['tb_moneda_nom'].' '.mostrar_moneda($deuda_total); ?></td>
            <td><?php echo $value2['tb_moneda_nom'].' '.$info_clicaja; ?></td>
            <td><?php 
                    $hoy_time = new DateTime($fecha_hoy);
                 $fecha_ven_time = new DateTime($value2['tb_cuotadetalle_fec']);
                  //resta entre fechas
                  $diff = $fecha_ven_time->diff($hoy_time)->format("%a");
                  if(($vencida == 0 || $est ==3) || ($vencida == 1 && $diff <= 30 && $est != 2)){
                    if($estado_ap != 1 && $acupag != 1)
                      echo '
                      <input type="checkbox" class="chk_acu" name="chk_cre_acuerdo[]" monto="'.$monto_acuerdo.'" value="'.$value4['tb_cuotadetalle_id'].'" >';
                  }
            ?></td>    
          </tr>
          <?php
           }
          }
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>