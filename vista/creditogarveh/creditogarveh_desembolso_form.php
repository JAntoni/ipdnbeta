<?php
require_once('../../core/usuario_sesion.php');
require_once ("Creditogarveh.class.php");
$oCreditogarveh = new Creditogarveh();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../transferente/Transferente.class.php');
$oTransferente = new Transferente();
require_once('../empresa/Empresa.class.php');
$oEmpresa = new Empresa();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$credito_id = $_POST['cre_id'];

if($_POST['action'] == "insertar"){
    $today = date('d-m-Y');
    $result = $oCreditogarveh->mostrarUno($credito_id);
      if($result['estado'] == 1){
        $cuotip_id = $result['data']['tb_cuotatipo_id'];
        $cuosubper_id = $result['data']['tb_cuotasubperiodo_id'];
        $mon_id = $result['data']['tb_moneda_id'];
        $cre_tipcam = $result['data']['tb_credito_tipcam'];
        $cre_tip = $result['data']['tb_credito_tip']; //tipo 1 general, 2 adenda, 3 ap, 4 MINUTA
        $cre_tip2 = $result['data']['tb_credito_tip2']; //tipo 2 del crédito, 4 CUOTA BALON
        $cre_preaco = $result['data']['tb_credito_preaco'];
        $cre_int = $result['data']['tb_credito_int'];
        $cre_int_porc = $result['data']['tb_cuota_interes'];
        $cre_numcuo = $result['data']['tb_credito_numcuo'];
        $cre_numcuomax = $result['data']['tb_credito_numcuomax'];
        $cre_linapr = $result['data']['tb_credito_linapr'];
        $cre_idori = $result['data']['tb_credito_idori']; //id del crédito matríz si este crédito es una ADENDA
        $cuedep_id = $result['data']['tb_cuentadeposito_id'];
        $rep_id = $result['data']['tb_representante_id'];
        $cre_comdes = $result['data']['tb_credito_comdes'];
        $cre_obs = $result['data']['tb_credito_obs'];
        $cargas = $result['data']['tb_credito_carg']; //cargas que tanga el inmueble
        $cre_feccre = mostrar_fecha($result['data']['tb_credito_feccre']);
        $cre_fecdes = mostrar_fecha($result['data']['tb_credito_fecdes']);
        $cre_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
        $cre_fecpro = mostrar_fecha($result['data']['tb_credito_fecpro']);
        $cli_id = $result['data']['tb_cliente_id'];
        $credito_tc = $result['data']['tb_credito_tc']; //tipo de cambio guardado para el crédito
      }
    $result = NULL;

    $condicion = 0;
    $credes_fec = $cre_fecdes;
    $monto_total = $cre_preaco;
    
    //tipo de cuota: 4-> fija o 3 ->libre, y num cuotas
    $num_cuo = $cre_numcuo;
    $tipo_cuo = 'FIJA';
    if($cuotip_id == 3){
      $tipo_cuo = 'LIBRE';
      $num_cuo = $cre_numcuomax;
    }
    
    //periodo de pago
    $tip_periodo = 'MENSUAL';
    if($cuosubper_id == 2)
        $tip_periodo = 'QUINCENAL';
    if($cuosubper_id == 3)
        $tip_periodo = 'SEMANAL';

    $mod_id=53; // el modulo para garveh es 53 en egreso
    $est=1;
    $total_desembolsado = 0;
    $des = '';
    //la consulta de egreso se tiene que ser por los dos tipos de moneda, 1
    $result1=$oEgreso->mostrar_por_modulo($mod_id,$_POST['cre_id'],1,$est);
    if($result1['estado']==1){
        foreach ($result1['data'] as $key => $value) {
            if($mon_id == 1)
            $total_desembolsado+=$value['tb_egreso_imp'];

          if($mon_id == 2)
            $total_desembolsado+= ($value['tb_egreso_imp'] / $value['tb_egreso_tipcam']);
          
            $des .= ' ||soles '.$value['tb_egreso_imp'].' -- '.$value['tb_egreso_tipcam'].' ()';
        }
    }

    $result1 = "";

    //la consulta de egreso se tiene que ser por los dos tipos de moneda, 2
    $result1=$oEgreso->mostrar_por_modulo($mod_id,$_POST['cre_id'],2,$est);
    if($result1['estado']==1){
        foreach ($result1['data'] as $key => $value) {
            if($mon_id == 1)
            $total_desembolsado+= ($value['tb_egreso_imp'] * $value['tb_egreso_tipcam']);

          if($mon_id == 2)
            $total_desembolsado+=$value['tb_egreso_imp'];
          

          $des .= ' ||dolares '.$value['tb_egreso_imp'].' -- '.$value['tb_egreso_tipcam'].' ()';
        }
    }
    $result1 = "";
    
    $monto_desembolsar = $monto_total - $total_desembolsado;
    $credes_mon= formato_numero($monto_desembolsar); //VER 
    $monto_desembolsar = formato_numero($monto_desembolsar); //VER
    
    //listamos las cuotas que faltan por pagar, si es una adenda CUOTA BALON necesitamos el id del crédito matriz
    $cuo_res = 0;
    if($cre_tip2 == 4){
      $result2 = $oCuota->listar_cuotas_impagas($cre_idori, 3); //parametro 3 de garveh
        if($result2['estado']==1){
             $cuo_res = count($result2['data']);
        }
      $result2 = "";
      $fechared = "readonly";
      $fechafact = "readonly";
      $fechapro = "readonly";
    }
    else{
        $fechared = "";
        $fechafact = "";
        $fechapro = "";
    }
    
    //verificamos si este crédito ya tiene guardado su cronograma, para no volverlo a guardar
    $result3 = $oCuota->filtrar(3,$_POST['cre_id']);
    if($result3['estado']==1){
        $num_rows_cuo = count($result3['data']);
    }
    $result3 = "";

    $generado = '<strong style="color: green;margin-left:20px;">Este cronograma ya ha sido guardada</strong>';
    if($num_rows_cuo == 0)
      $generado = '<strong style="color: red;margin-left:20px">Este cronograma aun no ha sido guardada</strong>';

    //INGRESO TOTAL EN CAJA SEGUROS, MONTOS RETENIDOS
    $mod_id = 52; //INGRESO POR SEGUROS DE ASIVEH
    $modide = $credito_id;
    $est = 1; // estado del ingreso, 1 vigente
    $caj_id = 15; //ID DE LA CAJA SEGUROS ES 15

    $mon_rete = 0;
    $result4 = $oIngreso->mostrar_por_modulo_cajaid($mod_id,$modide,$mon_id,$est,$caj_id);
    if($result4['estado']==1){
        foreach ($result4['data'] as $key => $value) {
            $mon_rete += $value['tb_ingreso_imp'];
        }
    }
    $result4 = "";
}
$moneda = 'S/.';
if($mon_id == 2)
$moneda = 'US$';

// GERSON (30-10-23)
$banca = '';
if(isset($_POST['banca'])){ // si el desembolso se hace desde PROCESOS
  $banca = $_POST['banca'];
}

$proceso_fase_item_id = 0;
if(isset($_POST['proceso_fase_item_id'])){
    $proceso_fase_item_id = $_POST['proceso_fase_item_id'];
}

$proceso = $oProceso->mostrarProcesoXCredito($credito_id);
$flag_proceso = false;

$proceso_id = 0;
$banca_id = 0;

if($proceso['estado'] == 1){

  $flag_proceso = true;

  $tipo_garantia_id = intval($proceso['data']['tb_cgarvtipo_id']);
  $proceso_id = $proceso['data']['tb_proceso_id'];
  $banca_id = intval($proceso['data']['tb_proceso_banca']);

  if($banca == ''){ // si el desembolso se hace desde listado GARVEH
    $proceso_banca = intval($proceso['data']['tb_proceso_banca']);
    if($proceso_banca == 1){
      $banca = 'cheque';
    }elseif($proceso_banca == 2 || $proceso_banca == 3 || $proceso_banca == 4){
      $banca = 'otro';
    }
    
  }

  $cheque_c = $oProceso->mostrarCabeceraChequeXProceso($proceso_id);
  $cheques = $oProceso->mostrarChequeXProceso($proceso_id);
  $precalificacion = $oProceso->mostrarUnoPrecalificacion($proceso_id);

  $fecha = '';
  $detalle = '';
  $banco = 0;
  $moneda_cheque = '';
  $cheque_aprobado = 0;
  $usuario_aprobado = 0;
  $fecha_aprobado = '';
  $cheque_id = 0;

  $flag_cheque = false;

  if( $cheque_c['estado'] == 1){
    $flag_cheque = true;
    $cheque_id = intval($cheque_c['data']['tb_cheque_id']);
    $fecha = date("d/m/Y, h:i a", strtotime($cheque_c['data']['tb_cheque_fecreg']));
    $detalle = $cheque_c['data']['tb_cheque_det'];
    $banco = intval($cheque_c['data']['tb_cheque_banco']);
    $moneda_cheque = $cheque_c['data']['tb_cheque_moneda'];
    $cheque_aprobado = intval($cheque_c['data']['tb_cheque_aprobado']);
    $usuario_aprobado = intval($cheque_c['data']['tb_cheque_usu_aprob']);
    $fecha_aprobado = date("d/m/Y, h:i a", strtotime($cheque_c['data']['tb_cheque_fec_aprob']));
  }else{
    $fecha = date("d/m/Y, h:i a");
  }
}
//
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_desembolso" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
          <h4 class="modal-title">Verifica los Datos de <b>GAR-MOB</b></h4>
      </div>

      <form id="for_credes">
        <div id="msj_desembolso" style="font-size: 16px;"></div>

        <input name="action_desembolso" id="action_desembolso" type="hidden" value="<?php echo $_POST['action']?>">
        <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $_POST['cre_id'] ?>">
        <input name="hdd_cre_id_ori" id="hdd_cre_id_ori" type="hidden" value="<?php echo $cre_idori;?>">
        <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $mon_id ?>">
        <input name="hdd_credes_mon" id="hdd_credes_mon" type="hidden" value="<?php echo $monto_desembolsar;?>">
        <input type="hidden" id="hdd_tipcam" value="<?php echo $cre_tipcam;?>">

        <input type="hidden" name="hdd_cuotip" id="hdd_cuotip" value="<?php echo $cuotip_id?>">
        <input type="hidden" name="hdd_num_cuo" id="hdd_num_cuo" value="<?php echo $cre_numcuo?>">
        <input type="hidden" name="hdd_cre_int" id="hdd_cre_int" value="<?php echo $cre_int ?>">
        <input type="hidden" name="hdd_cre_int_porc" id="hdd_cre_int_porc" value="<?php echo $cre_int_porc ?>">
        <input type="hidden" name="hdd_cuosubper" id="hdd_cuosubper" value="<?php echo $cuosubper_id ?>">

        <input type="hidden" name="hdd_cuo_guardadas" id="hdd_cuo_guardadas" value="<?php echo $num_rows_cuo;?>">
        <input type="hidden" name="hdd_cre_tip" id="hdd_cre_tip" value="<?php echo $cre_tip;?>">
        <input type="hidden" name="hdd_tip_ade" id="hdd_tip_ade" value="<?php echo $cre_tip2;?>">
        <input type="hidden" name="hdd_cre_cuo_res" id="hdd_cre_cuo_res" value="<?php echo $cuo_res;?>">
        <input type="hidden" name="hdd_cli_id" id="hdd_cli_id" value="<?php echo $cli_id;?>">
        <input type="hidden" name="hdd_credito_tc" id="hdd_credito_tc" value="<?php echo $credito_tc;?>">

        <!-- GERSON (07-11-23) -->
        <input type="hidden" name="hdd_flag_proceso" id="hdd_flag_proceso" value="<?php echo $flag_proceso;?>">
        <input type="hidden" name="hdd_proceso_id" id="hdd_proceso_id" value="<?php echo $proceso_id;?>">
        <input type="hidden" name="hdd_flag_cheque" id="hdd_flag_cheque" value="<?php echo $flag_cheque;?>">
        <input type="hidden" name="hdd_banca" id="hdd_banca" value="<?php echo $banca;?>">
        <input type="hidden" name="hdd_banca_id" id="hdd_banca_id" value="<?php echo $banca_id;?>">
        <input type="hidden" name="hdd_proceso_fase_item_id" id="hdd_proceso_fase_item_id" value="<?php echo $proceso_fase_item_id;?>">
        <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $_POST['vista'];?>">
        <!--  -->

        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-heading">
                  <b>Datos del Desembolso <?php echo $des;?></b>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_periodo" class="control-label">Periodo :</label>
                        <input type="text" name="txt_periodo" id="txt_periodo" class="form-control input-sm mayus" value="<?php echo $tip_periodo;?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="row">
                        <div class="col-md-8">
                          <div class="form-group">
                            <label for="cmb_mon_id_des" class="control-label">Tipo Moneda :</label>
                            <select id="cmb_mon_id_des" name="cmb_mon_id_des"  class="form-control input-sm mayus">
                              <option value="1" <?php echo ($mon_id == 1)? 'selected':'';?>>S/.</option>
                              <option value="2" <?php echo ($mon_id == 2)? 'selected':'';?>>US$.</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="txt_credes_tipcam" class="control-label">Cambio :</label> <br>
                            <input type="text" class="form-control input-sm mayus" id="txt_credes_tipcam" name="txt_credes_tipcam" size="5" value="<?php echo $cre_tipcam;?>">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_tip_cuota" class="control-label">Tipo de Cuota :</label>
                        <input type="text" name="txt_tip_cuota" id="txt_tip_cuota" class="form-control input-sm mayus" value="<?php echo $tipo_cuo;?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_nrocuotas" class="control-label">Número de Cuotas :</label>
                        <input type="text" name="txt_nrocuotas" id="txt_nrocuotas" class="form-control input-sm mayus" value="<?php echo $num_cuo;?>" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_monto_total" class="control-label" >Monto Total :</label>
                        <input type="text" name="txt_monto_total" id="txt_monto_total" class="form-control input-sm mayus" value="<?php echo $moneda.' '. mostrar_moneda($monto_total);?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_monto_desembolsado" class="control-label" >Monto Desembolsado :</label>
                        <input type="text" name="txt_monto_desembolsado" id="txt_monto_desembolsado" class="form-control input-sm mayus" value="<?php echo $moneda.' '. mostrar_moneda($total_desembolsado);?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_maximo_desembolsar" class="control-label" >Máximo a Desembolsar :</label>
                        <input type="text" name="txt_maximo_desembolsar" id="txt_maximo_desembolsar" class="form-control input-sm mayus" value="<?php echo $moneda.' '. mostrar_moneda($monto_desembolsar);?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_fecha_credito" class="control-label" >Fecha de Crédito :</label>
                        <input type="text" name="txt_fecha_credito" id="txt_fecha_credito" class="form-control input-sm mayus" value="<?php echo $cre_feccre;?>" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="txt_credes_fec" class="control-label">Fecha Desembolso :</label>
                        <input name="txt_credes_fec" class="form-control input-sm" id="txt_credes_fec" type="text" value="<?php echo date('d-m-Y');?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="txt_credes_mon" class="control-label" >Monto :</label>
                        <input type="text" name="txt_credes_mon" id="txt_credes_mon" class="moneda3 form-control input-sm mayus" value="<?php echo $credes_mon;?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="txt_credes_moncam" class="control-label" id="td_mon_cam">Monto Cambio Moneda :</label>
                        <input type="text" name="txt_credes_moncam" id="txt_credes_moncam" class="form-control input-sm moneda" value="0.00">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cmb_credes_mot" class="control-label">Motivo retención :</label>
                        <select id="cmb_credes_mot" name="cmb_credes_mot"  class="form-control input-sm mayus">
                          <option value="">Sin retención</option>
                          <option value="1">Pagos de Trámites</option>
                          <option value="2">Pagos de Seguros</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_credes_ret" class="control-label" >Retener Caja Seguros :</label>
                        <input type="text" name="txt_credes_ret" id="txt_credes_ret" class="moneda3 form-control input-sm mayus" <?php echo $condicion==0 ? 'readonly' : '';?>>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="txt_credes_fecfac" class="control-label">Fecha Facturación :</label>
                        <input name="txt_credes_fecfac" class="form-control input-sm" id="txt_credes_fecfac" type="text" value="<?php echo $cre_fecfac;?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cmb_cheque_motivo" class="control-label">Motivo Desembolso:</label>
                        <select id="cmb_cheque_motivo" name="cmb_cheque_motivo"  class="form-control input-sm mayus">
                          <option value="">Selecciona Motivo</option>
                          <option value="1">Desembolso Entrega Cheque Total</option> <!-- subcuenta: 178-->
                          <option value="2">Desembolso Trámites / Gastos</option> <!-- subcuenta: 180-->
                        </select>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-8">
                      <label for="txt_finalidad" class="control-label" >Finalidad del Desembolso :</label>
                      <div class="form-group">
                        <textarea name="txt_finalidad" id="txt_finalidad" rows="3" cols="130" class="form form-control mayus"></textarea>
                      </div>
                    </div>
                  </div>

                  <!-- GERSON (30-10-23) -->
                  <?php if($proceso['estado'] == 1){ ?>

                    <?php if($banca == 'cheque'){ // CHEQUES ?>

                      <?php if($banca_id == 1){ // CHEQUES ?>

                        <?php if($cheque_c['estado'] == 1){ ?>
                          <div class="row">
                            <div class="col-md-12">
                              
                              <div class="col-md-3"></div>
                              <div align="center" class="col-md-6" style="overflow-x: auto;">
                                <table class="table table-bordered table-responsive">
                                  <tbody>
                                    <?php 

                                      if($tipo_garantia_id == 1){ // PRE CONSTITUCION

                                        $transferente_id = 0;
                                        $transferente = '';
                                        if($precalificacion['estado']==1){
                                          $transferente_id = intval($precalificacion['data']['tb_precalificacion_proveedor_id']);
                                          $transferente = $oTransferente->mostrarUno($transferente_id);
                                          if($transferente['estado']==1){
                                            $transferente = $transferente['data']['tb_transferente_nom'];
                                          }
                                        } 
                          
                                        $beneficiarios = $transferente;
                                        
                                      }elseif($tipo_garantia_id == 2 || $tipo_garantia_id == 3 || $tipo_garantia_id == 6){ // CONSTITUCIÓN SIN Y CON CUSTODIA O ADENDA
                          
                                        $beneficiarios = $cliente_nom;
                                        $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
                                        if($participantes['estado']==1){
                                          foreach ($participantes['data'] as $key => $p) {
                                            $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'];
                                          }
                                        }
                          
                                      }elseif($tipo_garantia_id == 4 || $tipo_garantia_id == 5) { // TERCERO SIN Y CON CUSTODIA
                          
                                        $participantes = $oProceso->mostrarParticipanteXProceso($proceso_id);
                                        $beneficiarios = '';
                                        if($participantes['estado']==1){
                                          foreach ($participantes['data'] as $key => $p) {
                                            if($key == 0){
                                              $beneficiarios .= $p['tb_participante_nom'];
                                            }else{
                                              $beneficiarios .= ' '.$conector.' '.$p['tb_participante_nom'];
                                            }
                                          }
                                        }
                          
                                      }

                                      $i = 0;
                                      foreach ($cheques['data'] as $key => $value) { 

                                        $moneda = "";
                                        if($value['tb_cheque_moneda']=='sol'){
                                          $moneda = "S/ ";
                                        }else{
                                          $moneda = "US$ ";
                                        }

                                        $tipo = "";
                                        if($value['tb_chequedetalle_tipo']=='nego'){
                                          $tipo = "Negociable";
                                        }else{
                                          $tipo = "No Negociable";
                                        }

                                        $num_cheque = "";
                                        $sede = "";
                                        $uso = "";
                                        $fecha_subida = "";

                                        $num_cheque = $value['tb_chequedetalle_nro_cheque'];
                                        $sede = intval($value['tb_empresa_id']);
                                        $uso = intval($value['tb_chequedetalle_uso']);
                                        $fecha_subida = mostrar_fecha($value['tb_chequedetalle_fecsub']);

                                        $empresa = $oEmpresa->mostrarUno($sede);

                                        // 1=bcp, bbva=2
                                        $img_cheque = "";
                                        $banco_nom = '';
                                        $empresa_nom = '';

                                        if($empresa['estado'] == 1){
                                          $empresa_nom = $empresa['data']['tb_empresa_nomcom'];
                                        }

                                        if($banco==1){
                                          $banco_nom = 'BANCO DE CRÉDITO DEL PERÚ';
                                          if($moneda_cheque=='sol'){
                                            if($value['tb_chequedetalle_tipo']=='nego'){
                                              $img_cheque = "public/images/cheques/bcp_sol.png";
                                            }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                                              $img_cheque = "public/images/cheques/bcp_sol_ng.png";
                                            }
                                          }elseif($moneda_cheque=='dolar'){
                                            if($value['tb_chequedetalle_tipo']=='nego'){
                                              $img_cheque = "public/images/cheques/bcp_dolar.png";
                                            }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                                              $img_cheque = "public/images/cheques/bcp_dolar_ng.png";
                                            }
                                          }
                                        }elseif($banco==2){
                                          $banco_nom = 'BBVA CONTINENTAL';
                                          if($moneda_cheque=='sol'){
                                            if($value['tb_chequedetalle_tipo']=='nego'){
                                              $img_cheque = "public/images/cheques/bbva_sol.png";
                                            }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                                              $img_cheque = "public/images/cheques/bbva_sol_ng.png";
                                            }
                                          }elseif($moneda_cheque=='dolar'){
                                            if($value['tb_chequedetalle_tipo']=='nego'){
                                              $img_cheque = "public/images/cheques/bbva_dolar.png";
                                            }elseif($value['tb_chequedetalle_tipo']=='no_nego'){
                                              $img_cheque = "public/images/cheques/bbva_dolar_ng.png";
                                            }
                                          }
                                        }

                                        $chequesretiro = $oProceso->movimientosChequeXChequedetalle(intval($value['tb_chequedetalle_id']));
                                        $suma_retiro = 0.00;
                                        if($chequesretiro['estado'] == 1){
                                          foreach ($chequesretiro['data'] as $key => $v) {
                                            $egreso = $oProceso->mostrarEgresoXCheque(intval($v['tb_egreso_id']));
                                            if($egreso['estado'] == 1){
                                              if($mon_id == 2 && intval($egreso['data']['tb_moneda_id']) == 1){ // soles
                                                $suma_retiro += formato_numero($egreso['data']['tb_egreso_imp'] / $egreso['data']['tb_egreso_tipcam']);
                                              }elseif($mon_id == 1 && intval($egreso['data']['tb_moneda_id']) == 2){ // dolares
                                                $suma_retiro += formato_numero($egreso['data']['tb_egreso_imp']) * formato_numero($egreso['data']['tb_egreso_tipcam']);
                                              }
                                              else{
                                                $suma_retiro += formato_numero($egreso['data']['tb_egreso_imp']);
                                              }

                                              // if(intval($egreso['data']['tb_moneda_id']) == 1){ // soles
                                              //   $suma_retiro = $suma_retiro + formato_numero($egreso['data']['tb_egreso_imp']);
                                              // }elseif(intval($egreso['data']['tb_moneda_id']) == 2){ // dolares
                                              //   $suma_retiro = $suma_retiro + (formato_numero($egreso['data']['tb_egreso_imp']) * formato_numero($egreso['data']['tb_egreso_tipcam']));
                                              // }
                                            }
                                          }
                                        }

                                        $nuevo_saldo = 0.00;
                                        $nuevo_saldo = formato_numero($value['tb_chequedetalle_monto']) - $suma_retiro;

                                        $bloqueo_check = '';
                                        $color_cheque = '';
                                        if($nuevo_saldo==0){
                                          $bloqueo_check = 'disabled';
                                          $color_cheque = 'background-color: #FFE7E6;';
                                        }
                                    ?>
                                    <tr style="<?php echo $color_cheque; ?>">
                                      <td style="text-align: center; vertical-align: middle;"><input class="only-one" <?php echo $bloqueo_check; ?> name="chequeSeleccionados" id="chequeSeleccionado<?php echo $i; ?>" type="checkbox" value="<?php echo $value['tb_chequedetalle_id']; ?>"></td>
                                      <td>
                                        <ul>
                                          <li><?php echo '<strong>Beneficiarios(as):</strong> '.$beneficiarios; ?></li>
                                          <!-- <li><?php echo '<strong>Banco:</strong> '.$banco_nom ?></li> -->
                                          <li><?php echo '<strong>N° Cheque:</strong> '.$value['tb_chequedetalle_nro_cheque'] ?></li>
                                          <!-- <li><?php echo '<strong>Monto:</strong> '.$moneda.''.number_format($value['tb_chequedetalle_monto'], 2) ?></li> -->
                                          <li><?php echo '<strong>Monto:</strong> '.$moneda.''.number_format($value['tb_chequedetalle_monto'], 2) ?></li>
                                          <li><?php echo '<strong>Tipo:</strong> '.$tipo ?></li>
                                          <li><?php if($uso==1){ echo "<strong>Uso:</strong> Desembolso Total"; }else{ echo "<strong>Uso:</strong> Retención Total"; } ?></li>
                                          <li><?php echo '<strong>Registrada en Sede:</strong> '.$empresa_nom ?></li>
                                        </ul>
                                      </td>
                                      <td>
                                        <div class='row'>	
                                          <div align="center" class="col-md-12">
                                            <img class="" style="width: 240px;" src="<?php echo $img_cheque ?>" alt="Cheque">
                                          </div>
                                        </div>
                                      </td>
                                      <td style="text-align: center; vertical-align: middle;">
                                        <?php //echo '<strong>SALDO</strong><br>'.$moneda.'<input class="form-control input-sm" type="text" id="saldo'.$i.'" name="saldo'.$i.'" value="'.number_format($value['tb_chequedetalle_monto'], 2).'">' ?>
                                        <?php echo '<strong>SALDO</strong><br>'.$moneda.' '.number_format($nuevo_saldo, 2) ?>
                                      </td>
                                      <td style="text-align: center; vertical-align: middle;">
                                        <a class="btn btn-info btn-xs" title="Ver Detalle" onclick="verMovimientosCheque(<?php echo $value['tb_chequedetalle_id']; ?>)"><i class="fa fa-eye"></i></a>
                                      </td>
                                    </tr>
                                    <?php $i++; } ?>

                                  </tbody>
                                </table>
                              </div>
                              <div class="col-md-3"></div>

                            </div>
                          </div>

                        <?php }else{ ?>
                          <!-- <h2>NO HAY CHEQUES</h2> -->
                        <?php } ?>

                      <?php } ?>

                    <?php }elseif($banca == 'otro'){ // EFECTIVO, TRANSFERENCIA O DEPÓSITO ?>

                      <?php if($banca_id == 2){ // TRANSFERENCIA ?>
                        
                        <div class="row">
                          <div class="col-md-12">
                            
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                              <div class="col-md-12">
                                <div class="col-md-11">
                                  <h3>TRANSFERENCIA</h3>
                                </div>
                                <div class="col-md-1" style="margin-top: 20px;">
                                  <a class="btn btn-info btn-xs" title="Ver Detalle" onclick="verMovimientosOtro(<?php echo $proceso_id; ?>, 'transferencia')"><i class="fa fa-eye"></i></a>
                                </div>
                              </div>
                              <div class="col-md-8">
                                <div class="form-group">
                                  <label for="txt_nro_operacion_t" class="control-label">N° Operación :</label>
                                  <input name="txt_nro_operacion_t" class="form-control input-sm" id="txt_nro_operacion_t" type="text" placeholder="Ingrese N° Operación" autocomplete="false">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="txt_fecha_t" class="control-label">Fecha :</label>
                                  <input name="txt_fecha_t" class="form-control input-sm" id="txt_fecha_t" type="text" value="<?php echo date('d-m-Y') ?>" readonly>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="txt_banca_t" class="control-label">Banco :</label>
                                  <input name="txt_banca_t" class="form-control input-sm" id="txt_banca_t" type="text" placeholder="Ingrese Banco" autocomplete="false">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_cta_origen_t" class="control-label">Cuenta Origen :</label>
                                  <input name="txt_cta_origen_t" class="form-control input-sm" id="txt_cta_origen_t" type="text" placeholder="Ingrese Cuenta Origen" autocomplete="false">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_cta_destino_t" class="control-label">Cuenta Destino :</label>
                                  <input name="txt_cta_destino_t" class="form-control input-sm" id="txt_cta_destino_t" type="text" placeholder="Ingrese Cuenta Destino" autocomplete="false">
                                </div>
                              </div>

                            </div>
                            <div class="col-md-3"></div>

                          </div>
                        </div>

                      <?php }elseif($banca_id == 3){ // DEPÓSITO ?>
  
                        <div class="row">
                          <div class="col-md-12">
                            
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                              <div class="col-md-12">
                                <div class="col-md-11">
                                  <h3>DEPÓSITO</h3>
                                </div>
                                <div class="col-md-1" style="margin-top: 20px;">
                                  <a class="btn btn-info btn-xs" title="Ver Detalle" onclick="verMovimientosOtro(<?php echo $proceso_id; ?>, 'deposito')"><i class="fa fa-eye"></i></a>
                                </div>
                              </div>
                              <div class="col-md-8">
                                <div class="form-group">
                                  <label for="txt_nro_operacion_d" class="control-label">N° Operación :</label>
                                  <input name="txt_nro_operacion_d" class="form-control input-sm" id="txt_nro_operacion_d" type="text" placeholder="Ingrese N° Operación" autocomplete="false">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="txt_fecha_d" class="control-label">Fecha :</label>
                                  <input name="txt_fecha_d" class="form-control input-sm" id="txt_fecha_d" type="text" value="<?php echo date('d-m-Y') ?>" readonly>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="txt_banca_d" class="control-label">Banco :</label>
                                  <input name="txt_banca_d" class="form-control input-sm" id="txt_banca_d" type="text" placeholder="Ingrese Banco" autocomplete="false">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="cmb_caja_id_d" class="control-label">Caja Origen :</label>
                                  <select class="form-control input-sm" id="cmb_caja_id_d" name="cmb_caja_id_d">
                                    <?php include '../caja/caja_select.php';?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_cta_destino_d" class="control-label">Cuenta Destino :</label>
                                  <input name="txt_cta_destino_d" class="form-control input-sm" id="txt_cta_destino_d" type="text" placeholder="Ingrese Cuenta Destino" autocomplete="false">
                                </div>
                              </div>

                            </div>
                            <div class="col-md-3"></div>

                          </div>
                        </div>

                      <?php }elseif($banca_id == 4){ // EFECTIVO ?>
                        
                        <div class="row">
                          <div class="col-md-12">
                            
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                              <div class="col-md-12">
                                <div class="col-md-11">
                                  <h3>EFECTIVO</h3>
                                </div>
                                <div class="col-md-1" style="margin-top: 20px;">
                                  <a class="btn btn-info btn-xs" title="Ver Detalle" onclick="verMovimientosOtro(<?php echo $proceso_id; ?>, 'efectivo')"><i class="fa fa-eye"></i></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="cmb_caja_id_e" class="control-label">Caja Origen :</label>
                                  <select class="form-control input-sm" id="cmb_caja_id_e" name="cmb_caja_id_e">
                                    <?php include '../caja/caja_select.php';?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="txt_fecha_e" class="control-label">Fecha :</label>
                                  <input name="txt_fecha_e" class="form-control input-sm" id="txt_fecha_e" type="text" value="<?php echo date('d-m-Y') ?>" readonly>
                                </div>
                              </div>

                            </div>
                            <div class="col-md-3"></div>

                          </div>
                        </div>
                       
                      <?php } ?>

                    <?php } ?>

                  <?php } ?>
                  <!--  -->

                  <!--- MESAJES DE GUARDADO -->
                  <div class="callout callout-info" id="creditogarveh_desembolso_form" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- <div class="row">
          <div class="col-md-12 as">
            <?php echo $generado?>
          </div>
        </div>
        <br>  
        <div id="div_credito_cronograma_desem" class="col-md-12"></div>
        <div id="div_cuota_tabla_desem" class="col-md-12"></div> -->

        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardar_desembolso">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
  
    </div>
  </div>
</div>

<div id="div_modal_cheque_retiro">
  <!-- INCLUIMOS EL MODAL PARA VISUALIZAZR RETIROS DED CHEQUE -->
</div>
<div id="div_modal_otro_retiro">
  <!-- INCLUIMOS EL MODAL PARA VISUALIZAZR RETIROS DED CHEQUE -->
</div>

<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_desembolso_form.js?ver=16112023'?>"></script>