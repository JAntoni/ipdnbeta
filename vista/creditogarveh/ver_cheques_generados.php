<?php
	session_name("ipdnsac");
  session_start();
	require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();

  require_once('../funciones/funciones.php');
	require_once ("../funciones/fechas.php");

  $usuario_id = intval($_POST['usuario_id']);

  
?>
<div class="modal fade in" tabindex="-1" role="dialog" id="modal_cheques_generados" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <input type="hidden" id="hdd_cheque_usuario_id" value="<?php echo $usuario_id; ?>">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Cheques</h4>
      </div>
      <div class="modal-body">

        <div class="row">

          <div class="col-md-6">
            <div class="form-group">
                <label for="">Fecha :</label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="fecha_ini_cheque" id="fecha_ini_cheque" value="<?php echo date('01-m-Y'); ?>" onchange="mostrarListadoCheques()" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="fecha_fin_cheque" id="fecha_fin_cheque" value="<?php echo date('d-m-Y'); ?>" onchange="mostrarListadoCheques()" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                  <label for="">Moneda :</label>
                  <br>
                  <select class="form-control input-sm" name="cbo_moneda_cheque" id="cbo_moneda_cheque" onchange="mostrarListadoCheques()" >
                      <option value="">TODOS</option>
                      <option value="sol">Soles</option>
                      <option value="dolar">Dolares</option>
                  </select>
              </div>
          </div>

        </div>

        <br>

        <div id="div_cheques" class="row">


        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<script>
  $('#fecha_ini_cheque').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
  });
  $('#fecha_fin_cheque').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
  });

  mostrarListadoCheques();

  function mostrarListadoCheques(){

  var fecha_ini = $("#fecha_ini_cheque").val();
  var fecha_fin = $("#fecha_fin_cheque").val();
  var moneda = $("#cbo_moneda_cheque").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "proceso/proceso_controller.php",
    async:true,
    dataType: "json",                      
    data: ({
      action: 'lista_cheques',
      usuario_id: parseInt($('#hdd_cheque_usuario_id').val()),
      fecha_ini: fecha_ini,
      fecha_fin: fecha_fin,
      moneda: moneda,
    }),
    beforeSend: function() {
    },
    success: function(data){
      //console.log(data);
      $('#div_cheques').html(data.html);  
    }
  });

  }

  function verMovimientosCheque(chequedetalle_id){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"proceso/ver_cheque_retiro.php",
      async: true,
      dataType: "html",
      data: ({
        chequedetalle_id: chequedetalle_id
      }),
      beforeSend: function() {
      },
      success: function(data){
        $('#div_modal_cheque_retiro').html(data);
        $('#modal_cheque_retiro').modal('show');
        modal_height_auto('modal_cheque_retiro'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_cheque_retiro', 'limpiar'); //funcion encontrada en public/js/generales.js
      },
      complete: function(data){
        //$('#modal_mensaje').modal('hide');
      },
      error: function(data){
        alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      }
    });
  }

  function documento_desembolso(id_egr, empresa_id){
      var codigo = id_egr;
      window.open("http://ipdnsac.com/ipdnsac/vista/egreso/egreso_ticket.php?egreso_id=" + codigo + "&empresa_id=" + empresa_id);
      //window.open("http://localhost/ipdnsac/vista/egreso/egreso_ticket.php?egreso_id="+codigo+"&empresa_id="+empresa_id);
  }

</script>