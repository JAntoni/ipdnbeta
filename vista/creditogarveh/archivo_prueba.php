<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

//echo 'hola';

class Inserciones_en_bd extends Conexion{
    public function prueba(){
        try {
            $sql = "SELECT * FROM pdfgarv where tb_pdfgarv_id < 2826";// es el registro a partir del cual se ha registrado correctamente

            $sentencia = $this->dblink->prepare($sql);
            //$sentencia->bindParam(":creditogarvehfile_id", $creditogarvehfile_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function id_creditos_con_pdf_registrado(){
        try {
            $sql = "select DISTINCT p.tb_tb_credito_id from pdfgarv p INNER JOIN tb_creditogarveh c on p.tb_tb_credito_id = c.tb_credito_id";
            $sentencia = $this->dblink->prepare($sql);
            //$sentencia->bindParam(":creditogarvehfile_id", $creditogarvehfile_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function id_tipogarpers_por_credito($cred_id){
        try {
            $sql = "select DISTINCT p.tb_tipogarvdocpersdetalle_id from pdfgarv p INNER JOIN tb_creditogarveh c on p.tb_tb_credito_id = c.tb_credito_id
            where p.tb_tb_credito_id= :tb_credito_id ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $cred_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function buscar_usuario_por_nom($nom){
        try {
            $sql = "select tb_usuario_id from tb_usuario u where tb_usuario_nom= :user_nom";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":user_nom", $nom, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    function mostrarUno($pdfgarv_id){
        try {
          $sql = "SELECT * FROM pdfgarv p
                  INNER JOIN tipogarvdocpersdetalle t ON p.tb_tipogarvdocpersdetalle_id=t.tipogarvdocpersdetalle_id
                  INNER JOIN cgarvtipo gt ON gt.cgarvtipo_id=t.cgarvtipo_id
                  INNER JOIN tipopersona tp ON tp.tipopersona_id=t.tipopersona_id
                  INNER JOIN cgarvdoc gd ON gd.cgarvdoc_id=t.cgarvdoc_id
                  WHERE tb_pdfgarv_id=:pdfgarv_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":pdfgarv_id", $pdfgarv_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
      }
}

require_once(VISTA_URL.'pdfgarv/Pdfgarv.class.php');
$oPdfgarv = new Pdfgarv();

$insert = array();
$oInsercion= new Inserciones_en_bd();
$resultado = $oInsercion->id_creditos_con_pdf_registrado();

//se debe identificar los registros por credito y tipo doc, y una vez obtenido ese filtro se inserta los historiales para el ultimo registro
$i=0;
foreach($resultado['data'] as $k => $v){
    $result1 = $oInsercion->id_tipogarpers_por_credito($v['tb_tb_credito_id']);

    if($result1['estado']==1){
        foreach($result1['data'] as $k1 => $v1){
            $insert[3] = $oPdfgarv->mostrarPorDocumento1($v['tb_tb_credito_id'], $v1['tb_tipogarvdocpersdetalle_id'])['data']['tb_pdfgarv_id'];
            //de ese registro es donde tomaremos el regmodid del historial.
            
            //ahora armemos el sql para cada historial
            $setof_rowstoinsert = $oPdfgarv->mostrarRegistrosPorCred_Doc($v['tb_tb_credito_id'],$v1['tb_tipogarvdocpersdetalle_id']);//lista de registros, del mas antiguo al mas nuevo
            
            if($setof_rowstoinsert['estado']==1){
                foreach($setof_rowstoinsert['data'] as $k2 => $v2){
                    $det_para_extraer_usuario_y_fecha = $v2['tb_pdfgarv_his'];
                    $nombre_user= explode('b>',$det_para_extraer_usuario_y_fecha);
        
                    if(substr($det_para_extraer_usuario_y_fecha,0,1) == '<'){
                        $fec_hora_para_hist_det = substr($nombre_user[3],0,-2);
                        $coment_exoneracion = substr($nombre_user[5],0,-2);
                    }
                    else{
                        $fec_hora_para_hist_det = substr($nombre_user[2],3);
                    }
                    $nombre_doc = $oInsercion->mostrarUno($v2['tb_pdfgarv_id']);//tb_tipogarvdocpersdetalle_id
                    if($nombre_doc['estado']==1){
                        $nombre_doc = $nombre_doc['data']['cgarvdoc_nom'];
                        if($v2['tb_pdfgarv_estado']==1){//subida
                            $insert[4]= 'Ha subido el archivo "'.$nombre_doc.'" del crédito '. $v['tb_tb_credito_id'] .'. ';
                            if($v2['tb_pdfgarv_xac']==0){
                                $insert[4].= 'El archivo fue <b>eliminado</b>. El archivo eliminado está <a href="'.$v2['tb_pdfgarv_ruta'].'" target="_blank">AQUI</a>';
                            }
                        }
                        else{//exoneracion
                            $insert[4]= 'Ha exonerado el documento "'.$nombre_doc.'" en el crédito '. $v['tb_tb_credito_id'] .'. '. explode(' ',substr($nombre_user[1],0,-2))[0].' ingresó el motivo: '.$coment_exoneracion.'. ';
                            if($v2['tb_pdfgarv_xac']==0){
                                $insert[4].= 'La exoneracion fue <b>eliminada</b>.';
                            }
                        }
                        $insert[4].= ' | <b>'.$fec_hora_para_hist_det.'</b>';
            
                        $nombre_user= substr($nombre_user[1],0,-2);
                        $user_id = $oInsercion->buscar_usuario_por_nom($nombre_user);
                        $insert[0] = intval($user_id['data'][0]['tb_usuario_id']);
                        
                        $fec_para_bd= explode(' ',$fec_hora_para_hist_det);
                        $fecha = $fec_para_bd[0];//dd-mm-yyyy
                        $invert = explode("-",$fecha);
                        $fecha_invert = $invert[2]."-".$invert[1]."-".$invert[0];//Esta es la fecha a insertar en la bd. YYYY-mm-dd
                        
                        $insert[1] = date("Y-m-d H:i:s", strtotime($fecha_invert.' '.$fec_para_bd[1].' '.$fec_para_bd[2]));
                        $insert[2] = 'pdfgarv';
                        $html_sql="INSERT INTO tb_hist(tb_hist_usureg, tb_hist_fecreg, tb_hist_nom_tabla, tb_hist_regmodid, tb_hist_det)
                        VALUES (".$insert[0].", '".$insert[1]."', '".$insert[2]."', ".$insert[3].", '".$insert[4]."');";
                        
                        echo htmlentities($html_sql, ENT_QUOTES).' <br>' ;
                        $i++;
                        //if($i==6){exit();}
                    }                    
                }
            }
            else{
                echo 'ocurrio un error al mostrar los registros de pdf por credito '.$v['tb_tb_credito_id'].' y tipogardoc '.$v1['tb_tipogarvdocpersdetalle_id'].'<br>';
            }            
        }
    }
    else{
        echo 'no hay tipogarvdocpersdetalle para el crédito '.$v['tb_tb_credito_id'].'<br>';
    }
}
//echo '<br>'.$i.' inserciones pendientes';
?>
