
function cuotapago_imppos_datos3(cuotapago_id) {
    var codigo = cuotapago_id;
    window.open("http://ipdnsac.com/ipdnsac/vista/creditogarveh/creditogarveh_ticket.php?cuotapago_id=" + codigo);
    //window.open("http://localhost/ipdnsac/vista/creditogarveh/creditogarveh_ticket.php?cuotapago_id="+codigo);
}

function cuotapago_imppos_datos(cuotapago_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿Desea imprimir voucher?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "cuotapago/cuotapago_imppos_datos.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        cuotapago_id: cuotapago_id
                    }),
                    beforeSend: function () {
                        $('#h3_modal_title').text('Obteniendo datos...');
                        $('#modal_mensaje').modal('show');
                    },
                    success: function (data) {
                        cuotapago_imppos_ticket(data);
                    },
                    complete: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
//            console.log(data.responseText);
                    }
                });
            },
            no: function () {}
        }
    });
}
function cuotapago_imppos_ticket(datos) {
    $.ajax({
        type: "POST",
        //url: VISTA_URL + "cuotapago/cuotapago_imppos_ticket.php",
        url: "https://www.ipdnsac.com/app/modulos/cuotapago/cuotapago_imppos_ticket.php",
        async: true,
        dataType: "html",
        data: datos,
        beforeSend: function () {
            //$('#h3_modal_title').text('Imprimiendo...');
        },
        success: function (html) {
            //$('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}
function cuotapago_anular_garveh(cuotapago_id) {
    $.confirm({
        icon: 'fa fa-remove',
        title: 'Anular',
        content: '¿Desea anular este pago?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "cuotapago/cuotapago_controller_garveh.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: "anular",
                        cuotapago_id: cuotapago_id
                    }),
                    beforeSend: function () {
                        $('#h3_modal_title').text('Anulando...');
                        $('#modal_mensaje').modal('show');
                        $('#body_modal_mensaje').html('Espere');
                    },
                    success: function (data) {
                        if (parseInt(data.estado) == 1) {
                            alerta_success('Exito', data.mensaje);
                            cuotapago_pagos_tabla();

                            var hdd_form = $('#hdd_form').val();
                            if (hdd_form == 'vencimiento') {
                                vencimiento_tabla();
                            }
                        } else {
                            alerta_warning('Alerta', data.mensaje); //en generales.js
                        }
                    },
                    complete: function (data) {
                        $('#modal_mensaje').modal('hide');
                    },
                    error: function (data) {
                        alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
                        //console.log(data.responseText);
                    }
                });
            },
            no: function () {}
        }
    });
}
function cuotapago_detalle(cuotapago_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuotapago/cuotapago_detalle.php",
        async: true,
        dataType: "html",
        data: ({
            cuotapago_id: cuotapago_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Obteniendo datos...');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_modal_cuotapago_detalle').html(data);
            $('#modal_cuotapago_detalle').modal('show');

            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_cuotapago_detalle'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_cuotapago_detalle', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            $('#modal_mensaje').modal('hide');
        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            //console.log(data.responseText);
        }
    });
}
function cuotapago_pagos_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/credito_historial_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            credito_id: $('#hdd_credito_id').val(),
            creditotipo_id: $('#hdd_creditotipo_id').val()
        }),
        beforeSend: function () {
            $('#creditogarveh_pagos_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#creditogarveh_pagos_mensaje_tbl').hide(300);
            $('#creditogarveh_pagos_tabla').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            //console.log(data.responseText);
        }
    });
}

function morapago_pagos_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "morapago/morapago_pagos_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            credito_id: $('#hdd_credito_id').val(),
            creditotipo_id: $('#hdd_creditotipo_id').val()
        }),
        beforeSend: function () {
            $('#creditogarveh_pagos_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#creditogarveh_pagos_mensaje_tbl').hide(300);
            $('#creditogarveh_pagos_tabla').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            //console.log(data.responseText);
        }
    });
}

function acuerdopago_form(act, id) {
    var acuerdo = Number($('#hdd_his_mon_acu').val()); //monto total para el acuerdo 
    console.log("acuerdo = " + acuerdo + " act = " + act + " id = " + id);
    if ((isNaN(acuerdo) || acuerdo <= 0) && act != 'editar') {
        alert('Selecione las cuotas para generar un acuerdo');
        return false;
    }

    $.ajax({
        type: "POST", 
        url: VISTA_URL + "acuerdopago/acuerdopago_form.php", 
        async: true,
        dataType: "html",
        data: $('#for_historial_acu').serialize() + "&action=" + act + "&acu_id=" + id, 
        beforeSend: function () {
            //$('#div_acuerdopago_form').dialog('open'); 
        },
        success: function (html) {
            $('#div_acuerdopago_form').html(html); 
            $('#modal_creditogarveh_acuerdo').modal('show'); 
        },
        complete: function () {
        }
    });
}
function acuerdopago_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "acuerdopago/acuerdopago_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            cre_id: $("#hdd_his_cre_id").val(),
            cre_tip: 3 //credito de tipo 3: garveh
        }),
        beforeSend: function () {
//      $('#msj_credito_historial').html('Cargando acuerdos...');
//      $('#msj_credito_historial').show();
        },
        success: function (data) {
            $('#creditogarveh_pagos_mensaje_tbl').hide(300);
            $('#creditogarveh_pagos_tabla').html(data);
        },
        complete: function () {
        }
    });
}

function acuerdopago_aprobar(acuerdo_id, cre_tip) {
    $.confirm({
        icon: 'fa fa-exclamation',
        title: 'Aprobar',
        content: '¿Está seguro de APROBAR EL ACUERDO DE PAGO?',
        type: 'red',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                $.ajax({
                    type: "POST",
                    url: VISTA_URL +"acuerdopago/acuerdopago_reg.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        hdd_action: 'aprobar',
                        acu_id: acuerdo_id,
                        cre_tip: cre_tip
                    }),
                    beforeSend: function () {
            //            $('#msj_credito_historial').html('Aprobando acuerdo de pago...');
            //            $('#msj_credito_historial').show();
                    },
                    success: function (data) {
                        console.log(data);
                        if (parseInt(data.estado) == 1) {
                            alerta_success("EXITO",data.msj);
                            //$('#msj_credito_historial').html(data.msj);
                            acuerdopago_tabla();
                        }
                    },
                    complete: function (data) {
                        console.log(data);
            //            if (data.statusText != "success") {
            //                alerta_warning('Reporte este error a sistemas: Error al anular acuerdo de pago');
            //            }
                    }
                });
            },
            no: function () {}
        }
    });

    
}

function acuerdopago_anular_form(acu){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "acuerdopago/acuerdopago_anular_form.php",
    async:true,
    dataType:"html",                   
    data:({
      acu_id: acu,
      cre_tip: 3 //tipo d credito 3: garveh
    }),
    beforeSend: function() {
      $('#modal_creditogarveh_anular_acuerdopago').modal('show');
    },
    success: function(data){
      //console.log(data);  
      $('#div_acuerdopago_anular_form').html(data);  
      $('#modal_anular_acuerdopago').modal('show');
    },
    complete: function(data){     
      //console.log(data);
    }
  });
}

function adenda_form(action, cre_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/adenda_form.php",
    async:true,
    dataType:"html",                   
    data:({
      action: action,
      cre_id: cre_id //tipo d credito 3: garveh
    }),
    beforeSend: function() {
      //$('#modal_creditogarveh_anular_acuerdopago').modal('show');
    },
    success: function(data){
      
      $('#div_modal_adenda_form').html(data);  
      $('#modal_adenda_form').modal('show');
      modal_height_auto('modal_adenda_form');
    },
    complete: function(data){     
      //console.log(data);
    }
  });
}

function acuerdopago_pagos(acu,ins){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "acuerdopago/acuerdopago_pagos.php",
    async:true,
    dataType:"html",                   
    data:({
      acu_id: acu,
      num_ins: ins,
      cre_tip: 3 //tipo de credito 3 garveh
    }),
    beforeSend: function() {
      //$('#div_acuerdopago_pagos').dialog('open');
        },
    success: function(data){
      $('#div_acuerdopago_pagos_form').html(data);  
      $('#modal_acuerdopago_pagos').modal('show');
    },
    complete: function(){     
    }
  });
}

function acuerdopago_aplicar(acu){
    $.confirm({
        icon: 'fa fa-exclamation',
        title: 'Aprobar',
        content: '¿Está seguro de APLICAR EL ACUERDO DE PAGO?',
        type: 'red',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "acuerdopago/acuerdopago_aplicar.php",
                    async:true,
                    dataType:"json",                   
                    data:({
                      acu_id: acu,
                      cre_tip: 3 // tipo de credito 3: garveh
                    }),
                    beforeSend: function() {
                //      $('#msj_credito_historial').html('Guardando información...');
                //      $('#msj_credito_historial').show();
                    },
                    success: function(data){
                        console.log(data);
                      if(parseInt(data.estado) > 0){
                        //$('#msj_credito_historial').html(data.msj);
                        alerta_success("EXITO",data.msj);
                        acuerdopago_tabla();
                      }
                      else
                        //$('#msj_credito_historial').html('Error repórtelo: ' + data);
                                alerta_error("ERROR","Reportar :" + data.msj);
                    },
                    complete: function(data){
                        console.log(data);
                      if(data.statusText != "success"){
                        $('#msj_credito_historial').html('Error repórtelo: ' + data.responseText);
                      }
                    }
                  });
            },
            no: function () {}
        }
    });
}

function imprimir_js(nombre) {
    
	var ficha = document.getElementById(nombre);

	var ventimp = window.open(' ', 'popimpr');
	//ventimp.document.write('<link rel="stylesheet" href="../../public/js/tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />'); 
	//ventimp.document.write('<link href="../../public/css/miestilo.css" rel="stylesheet" type="text/css">');
	ventimp.document.write( ficha.innerHTML );
        console.log(ficha);
	ventimp.document.close();
	ventimp.print( );
	ventimp.close();
}

/* GERSON (12-01-25) */
function solicitar_anular(cuotapago_id, credito_id, creditotipo_id){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"permisoanulacion/solicitar_anulacion_form.php",
      async: true,
      dataType: "html",
      data: ({
        modid: cuotapago_id,
        origen: 2, // 1=cuota(cmenor), 2=cuotadetalle(cgarveh), 3=ingreso, 4=egreso
        credito_id: credito_id,
        creditotipo_id: creditotipo_id
      }),
      beforeSend: function() {
        $('#h3_modal_title').text('Obteniendo datos...');
        $('#modal_mensaje').modal('show');
      },
      success: function(data){
        $('#div_modal_solicitar_anulacion').html(data);
        $('#modal_solicitar_anulacion').modal('show');
  
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_solicitar_anulacion'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_solicitar_anulacion', 'limpiar'); //funcion encontrada en public/js/generales.js
      },
      complete: function(data){
        $('#modal_mensaje').modal('hide');
      },
      error: function(data){
        alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
        console.log(data.responseText);
      }
    });
}
/*  */

$(document).ready(function () {
    //cuotapago_pagos_tabla();

    console.log('historial pagos 11111')
});