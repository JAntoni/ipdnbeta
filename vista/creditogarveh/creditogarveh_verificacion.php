<?php
  $credito_id = $_POST['credito_id'];

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_verificacion" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
          <h4 class="modal-title">Verifica los Datos de <b>GAR-MOB</b></h4>
      </div>
      <form id="frm_creditoverificacion">
        <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
	<input type="hidden" name="action" value="verificacion_titulo">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="cmb_verificacion_cli_si" class="control-label">¿Datos Cliente Correcto? </label>
                                <select id="cmb_verificacion_cli_si" name="cmb_verificacion_cli_si"  class="form-control input-sm mayus"> 
                                        <option value="">Seleccione...</option>
                                        <option value="1">SI</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="cmb_verificacion_emp_si" class="control-label">¿Datos Acreedor correcto? </label>
                                <select id="cmb_verificacion_emp_si" name="cmb_verificacion_emp_si"  class="form-control input-sm mayus">
                                        <option value="">Seleccione...</option>
                                        <option value="1">SI</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="txt_veri_observaciones" class="control-label" >Observaciones :</label>
                            <div class="form-group">
                              <textarea name="txt_veri_observaciones" id="txt_veri_observaciones" rows="4" cols="70">Ninguna</textarea>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_creditoverificacion" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditogarveh/creditogarveh_verificacion.js?ver=437433';?>"></script>
