<?php if($creditogarveh_id>0):?>
<div class="row">
    <!--DATOS DEL CLIENTE-->
    <div class="col-md-12">
        <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">HISTORIAL <?php echo $opc_modificar;
        echo ($cre_tip == 2) ? ' <b style="font-family: cambria;font-weight: bold;color: red"> ESTO ES UNA ADENDA</b>' : ''; ?></label>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="his1">REGISTRADO POR :</label><br/>
                        <input type="text" id="his1" name="his1" class="form-control input-sm" value="<?php echo $usuregnom; ?>" readonly/>
                    </div> 
                    <div class="col-md-6">
                        <label for="f1">FECHA :</label><br/>
                        <input type="text" id="f1" name="f1" class="form-control input-sm" value="<?php echo mostrar_fecha($reg); ?>" readonly/>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="his2">MODIFICADO POR :</label><br/>
                        <input type="text" id="his2" name="his2" class="form-control input-sm" value="<?php echo $usumodnom; ?>" readonly/>
                    </div> 
                    <div class="col-md-6">
                        <label for="f2">FECHA :</label><br/>
                        <input type="text" id="f2" name="f2" class="form-control input-sm" value="<?php echo mostrar_fecha($mod); ?>" readonly/>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="his3">APROBADO POR :</label><br/>
                        <input type="text" id="his3" name="his3" class="form-control input-sm" value="<?php if($usuapr==0){echo "SIN APROBAR";}else{echo $usuaprnom;} ?>" readonly/>
                    </div> 
                    <div class="col-md-6">
                        <label for="f3">FECHA :</label><br/>
                        <input type="text" id="f3" name="f3" class="form-control input-sm" value="<?php echo mostrar_fecha($apr); ?>" readonly/>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!--FIN DATOS DEL CLIENTE-->
</div>
 <?php endif;?>