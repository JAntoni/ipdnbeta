<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <?php $link_ipnd1 = 'http://ipdnsac.com/app/modulos/creditogarveh/'; ?>
                <?php $link_ipnd2 = '../ipdnsac/procesos/'; ?>
                <?php if ($_SESSION['usuariogrupo_id'] == 2 ) { echo '<a class="btn btn-primary btn-sm" href="javascript:void(0)" title="NUEVO CREDITO" onClick="creditogarveh_form(\'I\', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</a>'; } ?>
                <a class="btn btn-success btn-sm" onclick="generarSimulador(0, <?php echo $_SESSION['usuario_id'] ?>)" title="Simulador"><i class="fa fa-calendar"></i> SIMULACIÓN</a>
                <a class="btn bg-purple btn-sm" href="#" onclick="verChequesGenerados(<?php echo $_SESSION['usuario_id'] ?>)" title="Simulador"><i class="fa fa-calendar"></i> CHEQUES</a>
                <a class="btn btn-info btn-sm" target="_blank" href="<?php echo $link_ipnd1 . 'doc_procesos_preconstitucion.php'; ?>" title="Pre-Cons"><i class="fa fa-print"></i> PROCESO: PRE-CONS</a>
                <a class="btn btn-info btn-sm" target="_blank" href="<?php echo $link_ipnd1 . 'doc_procesos_constitucion.php'; ?>" title="Const"><i class="fa fa-print"></i> PROCESO: CONSTITUCION</a>
                <a class="btn btn-info btn-sm" target="_blank" href="<?php echo $link_ipnd1 . 'doc_procesos_tercero.php'; ?>" title="Compra Tercero"><i class="fa fa-print"></i> PROCESO: COMPRA TERCERO</a>
                <a class="btn btn-info btn-sm" target="_blank" href="<?php echo $link_ipnd1 . 'doc_requisitos.php'; ?>" title="Requisitos"><i class="fa fa-print"></i> RESUMEN REQUISITOS</a>
                <a class="btn btn-info btn-sm" target="_blank" href="<?php echo $link_ipnd1 . 'MODIFICACIÓN_DE_ADENDA_GARMOB.pdf'; ?>" title="Adenda"><i class="fa fa-print"></i> ADENDA</a>
                <hr>
                <!--<button class="btn btn-primary btn-sm" onclick="creditogarveh_form('I', 0)"><i class="fa fa-plus"></i> Nueva Garantia</button> -->
                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtro de Garantia Vehicular</label>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php require_once('creditogarveh_filtro.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="creditogarveh_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_creditogarveh_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('creditogarveh_tabla.php'); 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="txt_fecha_ref" value="<?php echo date("d-m-Y"); ?>">
            <div id="div_modal_creditogarveh_form"></div>
            <div id="div_credito_pdf_ver"></div>
            <div id="div_exonerar_creditogarvehpdf"></div>
            <div id="div_modal_cuentadeposito_form"></div>
            <div id="div_modal_representante_form"></div>
            <div id="div_modal_carousel"></div>
            <div id="div_controlseguro_form"></div>
            <div id="div_credito_file"></div>
            <div id="div_modal_creditogarveh_inicial"></div>
            <div id="div_credito_desembolso_form"></div>
            <div id="div_modal_creditogarveh_pagos"></div>
            <div id="div_modal_adenda_form"></div>
            <div id="div_creditogarveh_his"></div>
            <div id="div_refinanciar_form"></div>
            <div id="div_renovar_cuotas"></div>
            <div id="div_clientenota_form"></div>
            <div id="div_titulo_form"></div>
            
            <div id="div_verificacion_titulo_form"></div>
            <div id="div_credito_seguimiento"></div>
            <div id="div_amortizar_form"></div>
            <div id="div_resolver_form"></div>
            <div id="div_acuerdopago_form"></div>
            <div id="div_acuerdopago_anular_form"></div>
            <div id="div_acuerdopago_pagos_form"></div>
            <div id="div_modal_cliente_form"></div>
            <div id="div_modal_transferente_form"></div>
            <div id="div_modal_vehiculomarca_form"></div>
            <div id="div_modal_vehiculoclase_form"></div>
            <div id="div_modal_vehiculomodelo_form"></div>
            <div id="div_modal_vehiculotipo_form"></div>
            <div id="div_historial_doc_pdf"></div>

            <div id="div_modal_liquidacion"></div>

            <div id="div_creditolinea_form"></div>

            <div id="div_modal_generar_simulador">
                <!-- INCLUIMOS EL MODAL PARA GENERAR SIMULACION DE CUOTAS -->
            </div>

            <div id="div_modal_cheques_generados">
                <!-- INCLUIMOS EL MODAL PARA VISUALIZAR LOS CHEQUES -->
            </div>

            <div id="div_modal_cheque_retiro">
                <!-- INCLUIMOS EL MODAL PARA VISUALIZAZR RETIROS DED CHEQUE -->
            </div>

            <div id="div_modal_gps_simple"></div>
            <div id="div_historial_cambios_legal"></div>
            <div id="div_modal_ejecucion_verpdf_form"></div>

            <div id="div_modal_titulos_form"></div>

            <div id="div_placa_form"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->

            <!-- GERSON (22-12-24) -->
            <div id="div_modal_gastos_ejecucion"></div>
            <div id="div_modal_gastos_ejecucion_fecha"></div>
            <!--  -->

            <!-- daniel odar 31-01-25 -->
            <div id="div_filestorage_form"></div>
            
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>