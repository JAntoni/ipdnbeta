
<div class="row" id="fil_transferente" <?php echo $style_trans;?>>
    <!--DATOS DEL CLIENTE-->
    <div class="col-md-12">
        <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003399">DATOS DEL TRANSFERENTE O CONCESIONARIO <?php echo $opc_modificar;
        echo ($cre_tip == 2) ? ' <b style="font-family: cambria;font-weight: bold;color: red"> ESTO ES UNA ADENDA</b>' : ''; ?></label>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-11">
                       <p style="color: green;">- Las concesionarias se registran en el menú de Transferente y Concesionaria en el menú: Mantenimiento > Operaciones > Transferente y Concesionarias.<br>
                           - Los Depósitos o Cheques que IPDN emita, se registran en el menú: Mantenimiento > Operaciones > Cheques y Depósitos, ahí agregas el ID de este Crédito.<br>
                           - Las iniciales que hace el cliente sea Cheque o Depósito se hace en el menú: Mantenimiento > Operaciones > Cuenta Externos.</p>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-4">
                        <label for="txt_transferente_fil_doc">DNI/RUC :</label>
                        <div class="input-group">
                            <input type="text" id="txt_transferente_fil_doc" name="txt_transferente_fil_doc" class="form-control input-sm" value="<?php echo $transferente_doc ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button" onclick="buscar_idtransferente()" title="MODIFICAR TRANSFERENTE">
                                    <span class="fa fa-pencil icon"></span>
                                </button>
                            </span>
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button" onclick="transferente_form('I', 0)" title="AGREGAR TRANSFERENTE">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div> 
                    <div class="col-md-8">
                        <label for="txt_transferente_fil_nom">TRANSFERENTE / CONCESIONARIO :</label><br/>
                        <input type="text" id="txt_transferente_fil_nom" name="txt_transferente_fil_nom" class="form-control input-sm" value="<?php echo $transferente_nom ?>"/>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!--FIN DATOS DEL CLIENTE-->

</div>