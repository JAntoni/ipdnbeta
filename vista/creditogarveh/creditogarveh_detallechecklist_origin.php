<?php 
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  
    $cliente_tip = $_POST['tipopersona_id'];
    $cre_subgar = $_POST['cre_subgar'];
    $cre_cus = $_POST['cre_cus'];
    $cre_tip = $_POST['cre_tip'];
    $creditogarveh_id = $_POST['creditoid'];
    $cli_id = $_POST['clienteid'];
  
  require_once('../tipogarvdocpersdetalle/Tipogarvdocpersdetalle.class.php');
  $oTipogarvdocpersdetalle = new Tipogarvdocpersdetalle();
  
  require_once('../pdfgarv/Pdfgarv.class.php');
  $oPdfgarv = new Pdfgarv();
  
    $tipopersona_id = $cliente_tip; // USO DIRECTO PARA TIPO PERSONA
    if($cre_subgar == 'PRE-CONSTITUCION'){
        $cgarvtipo_id = 1;
    }
    if($cre_subgar == 'REGULAR'){
        if($cre_cus=='1'){
            $cgarvtipo_id = 3;
        }
        if($cre_cus=='2'){
            $cgarvtipo_id = 2; 
        }
    }
    if($cre_subgar == 'GARMOB COMPRA TERCERO'){
        if($cre_cus=='1'){
            $cgarvtipo_id = 5;
        }
        if($cre_cus=='2'){
            $cgarvtipo_id = 4; 
        }
    }
    if($cre_subgar == 'GARMOB IPDN VENTA'){
        $cgarvtipo_id = 7;
    }
    if($cre_subgar == 'GARMOB IPDN VENTA CON RD'){
        $cgarvtipo_id = 8;
    }
    if($cre_tip == 2){
        $cgarvtipo_id = 6; 
    }
   
   $listado ='';
   $result = $oTipogarvdocpersdetalle->mostrarDocsObligatorios($cgarvtipo_id, $tipopersona_id);
   if($result['estado']==1){
       foreach ($result['data'] as $key => $value) {
           
           $result1 = $oPdfgarv->mostrarPorDocumento($creditogarveh_id, $cli_id, $value['tipogarvdocpersdetalle_id']);
           if($result1['estado']==1){
               $color = 'green';
               /*$pdf = '<a href="'.$result1['data']['tb_pdfgarv_ruta'].'" target="_blank" class="btn btn-primary btn-xs">
                    <i class="fa fa-file-pdf-o" title="Ver Contratos"></i>
                    </a>';*/
               $pdf = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="vercredit_pdf_form('.$result1['data']['tb_pdfgarv_id'].')" title="Ver pdf">'
                       . '<i class="fa fa-file-pdf-o"></i>'
                       . '</a>';
           }
           if($result1['estado']==0){
               $color = 'red';
               $pdf = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="credito_pdf_form('.$value['tipogarvdocpersdetalle_id'].",'".$value['cgarvdoc_nom']. "'". ')" tittle="SUBIR DOCUMENTO">'
                       . '<i class="fa fa-file-pdf-o"></i>'
                       . '</a>'
                       . ' <a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="exonerar_pdf_form('.$value['tipogarvdocpersdetalle_id'].')" tittle="EXONERAR">'
                       . '<i class="fa fa-check"></i>'
                       . '</a>';
           }
           
        $listado .=   '<div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <label class="checkbox-inline" style="padding:6px">
                        <span class="description-percentage text-'.$color.'">
                            <i class="fa fa-caret-up"></i>
                        </span>
                        <b>'. $value['cgarvdoc_nom'] .'</b>
                    </label>
                </div>
                <div class="col-md-6">
                    '.$pdf.'
                </div>
            </div>
            </div>';
       }
   }
   echo $listado;
    
    ?>