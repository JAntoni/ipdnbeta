<?php
require_once ("../../core/usuario_sesion.php");

require_once ("Creditogarveh.class.php");
$oCreditogarveh = new Creditogarveh();
require_once ("../creditoinicial/Creditoinicial.class.php");
$oCreditoinicial = new Creditoinicial();

require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../proceso/Proceso.class.php");
$oProceso = new Proceso();

$creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
$usuario_id = intval($_SESSION['usuario_id']);

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");


if($_POST['action_credito']=="adenda"){
  $cre_id = $_POST['hdd_credito_id']; //del credito original haremos una adenda
  $proceso_id = intval($_POST['hdd_proceso_id']); //del credito original haremos una adenda
  $tip_aden = $_POST['cmb_tip_aden']; //tipo de adenda
  $cuot_tip = intval($_POST['cmb_cuotip_id']);
  $mon_id = $_POST['cmb_mon_id'];
  $cambio = $_POST['txt_cre_tipcam'];
  $preaco = $_POST['txt_cre_preaco']; 
  $int = $_POST['txt_cre_int'];
  $numcuo = $_POST['txt_cre_numcuo'];
  if($cuot_tip == 3){
      $numcuomax = intval($_POST['txt_cre_numcuomax']); 
  }
  if($cuot_tip == 4){
      $numcuomax = intval($_POST['txt_cre_numcuo']); 
  }
  //$numcuomax = intval($_POST['txt_cre_numcuomax']); 
  $linapr = $_POST['txt_cre_linapr']; 
  $obs = $_POST['txt_cre_obs']; 
  $feccre = $_POST['txt_cre_feccre']; 
  $fecdes = $_POST['txt_cre_fecdes']; 
  $fecfac = $_POST['txt_cre_fecfac']; 
  $fecpro = $_POST['txt_cre_fecpro']; 
  $usureg = $_SESSION['usuario_id'];
  $empresa_id = $_SESSION['empresa_id'];
  
  $cre_tip = 2; //esta ves será de tipo 2 por ser una adenda regular
  //es un credito nuevo basado en un credito anterior
  $subper = $_POST['cmb_cuosubper_id'];
  if($cuot_tip == 3) //si es 3 solo será de tipo mensual
    $subper = 1;

  $cambio = 1;

  $dts = $oMonedacambio->consultar(fecha_mysql(date('d-m-Y')));
    if($dts['estado']==1){
      $cambio = floatval($dts['data']['tb_monedacambio_val']);
    }
  $dts = null;

  $result = $oCreditogarveh->registrar_adenda($cre_id, $tip_aden, $cuot_tip,$mon_id,moneda_mysql($cambio),moneda_mysql($preaco),$int,$numcuo,$numcuomax,moneda_mysql($linapr),$obs,fecha_mysql($feccre),fecha_mysql($fecdes),fecha_mysql($fecfac),fecha_mysql($fecpro),$usureg,$subper,$cre_tip);
    if(intval($result['estado']) == 1){
        $cre_id_new = $result['credito_id'];
        if($proceso_id > 0){ // ADENDA
          $oProceso->modificar_campo_proceso($proceso_id, 'tb_credito_id', $cre_id_new, 'INT');
        }
    }
  $result = null;
  
  //? EVALUAMOS SI SE HA REGISTRADO UN GPS PARA EL CREDITO, Y ASÍ PODER ACTIVAR EL XAC DEL GPS EN LA BD
  $poliza_id = intval($_POST['hdd_gps_simple_id']);

  if($poliza_id > 0){
    $oPoliza->modificar_campo($poliza_id, 'tb_poliza_xac', 1, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante
    $oPoliza->modificar_campo($poliza_id, 'tb_credito_id', $cre_id_new, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante
  }

  $num_escr = intval($_POST['txt_cre_numescr']);
  $fec_escr = fecha_mysql($_POST['txt_cre_fecescr']);
  $oCreditogarveh->modificar_campo($cre_id_new, 'tb_credito_numescr', $num_escr, 'INT'); //agregar el numero de escritura pública
  $oCreditogarveh->modificar_campo($cre_id_new, 'tb_credito_fecescr', $fec_escr, 'STR'); //agregar la fecha de la escritura pública
  $oCreditogarveh->modificar_campo($cre_id_new, 'tb_empresa_id', $empresa_id, 'INT'); //agregar el ID de la SEDE a la adenda registrada
  
  //registramos el historial para el credito
  $his = '<span style="color: grey;">Se ha creado una adenda de este crédito <b>(Adenda ID: '.$cre_id_new.')</b>, creado por: <b>'.$_SESSION['usuario_nom'].'</b>. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
  //$oCreditogarveh->registro_historial($cre_id, $his);
  $oCreditolinea->insertar($creditotipo_id, $cre_id_new, $usuario_id, $his);

  //registramos el historial para el credito
  $his = '<span>Adenda creada por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: esta adenda ha sido creado en base al crédito matríz de ID: CGV-'.$cre_id.' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
  //$oCreditogarveh->registro_historial($cre_id_new, $his);
  $oCreditolinea->insertar($creditotipo_id, $cre_id_new, $usuario_id, $his);

  $line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha registrado una Adenda para el Crédito GARVEH de número: CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'. La Adenda tiene como número: A-CGV-'.str_pad($cre_id_new, 4, "0", STR_PAD_LEFT).'. && <b>'.date('d-m-Y h:i a').'</b>';
  $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
  $oLineaTiempo->tb_lineatiempo_det = $line_det;
  $oLineaTiempo->insertar();
  
  //?VALORES DEL % DEL SEGURO Y PRECIO DEL GPS POR TODO EL TIEMPO DEL CRÉDITO
  $valor_seguro = formato_moneda($_POST['hdd_seguro_porcentaje']);
  $valor_gps = formato_moneda($_POST['hdd_gps_precio']);
  $estado_gps = intval($_POST['che_gps']); // si es 0 no se eligió GPS para el crédito

  //?REGISTRAMOS LOS VALORES DEL % DEL SEGURO Y EL VALOR DEL GPS
  $oCreditogarveh->modificar_campo($cre_id_new, 'tb_credito_preseguro', $valor_seguro, 'STR');
  if($estado_gps > 0)
    $oCreditogarveh->modificar_campo($cre_id_new, 'tb_credito_pregps', $valor_gps, 'STR');

    $mon_id = $_POST['cmb_mon_id'];

    $cuotip_id = $_POST['cmb_cuotip_id']; //tipo de cuota 4 fija, 3 libre
    $fec_desembolso = $_POST['txt_cre_fecdes'];
    $fec_desem = $_POST['txt_cre_fecfac']; //fecha de desembolso
    $fec_prorra = $_POST['txt_cre_fecpro'];//fecha de prorrateo
    $cuosubper_id = $_POST['cmb_cuosubper_id']; //sub perio, semanal, quincena, mes

    $C = moneda_mysql($_POST['txt_cre_preaco']);
    $i = moneda_mysql($_POST['txt_cre_int']);
    $n = ($cuot_tip == 4)? $_POST['txt_cre_numcuo'] : $_POST['txt_cre_numcuomax'];
    
    if($mon_id == 1)
      $valor_gps = formato_moneda($valor_gps * $cambio); // el gps tiene precio en dolares si el cronograma es en soles se genera el tipo de cambio

    $gps_mensual = 0;
    $seguro_mensual = 0;

    if(floatval($valor_seguro) > 0)
      $i += $valor_seguro;
    if(intval($estado_gps) <= 0)
      $valor_gps = 0;
    if(floatval($valor_gps) > 0 && $n > 0)
      $gps_mensual = formato_moneda($valor_gps / $n);

    //? ACTUALIZAMOS EL VALOR NUEVO DEL INTERÉS AL CRÉDITO
    $oCreditogarveh->modificar_campo($cre_id_new, 'tb_credito_int', $i, 'INT');

    if($cuotip_id == 3) // si es 4 es fija, 3 es libre
      $n = 1; // si es libre solo será una cuota

    if($cuosubper_id == 1){
      $cols = 1;
      $name_per = "MENSUAL";
    }
    if($cuosubper_id == 2){
      $cols = 2;
      $name_per = "QUINCENAL";
    }
    if($cuosubper_id == 3){
      $cols = 4;
      $name_per = "SEMANAL";
    }
    //para la formula del prorrateo, se toma la fecha del prorrateo y se cuenta cuántos días tiene el mes de la fecha del prorrateo, ejem: prorrateo 10/12/2016, el mes 12 tiene 31 días, para saber eso usamos la funcion de mktime
    //formula prorrateo = (interes de la primera cuota / total días del mes prorrateo) * dias prorrateo
    list($dia,$mes,$anio) = explode('-',$fec_prorra);
    $dias_mes = date('t', mktime(0, 0, 0, $mes, 1, $anio));
    $fec_desem1 = new DateTime($fec_desembolso);
    $fec_prorra1 = new DateTime($fec_prorra);
    $dias_prorra = $fec_desem1->diff($fec_prorra1)->format("%a");

    $R = $C / $n;
    if($i != 0){
      $uno = $i / 100;
      $dos = $uno + 1;
      $tres = pow($dos,$n);
      $cuatroA = ($C * $uno) * $tres;
      $cuatroB = $tres - 1;
      $R = $cuatroA / $cuatroB;
      $r_sum = $R*$n;
    }

    list($day, $month, $year) = explode('-', $fec_desem);

    for ($j=1; $j <= $n; $j++){ 
      if($j>1){
        $C = $C - $amo;
        $int = $C*($i/100);
        $amo = $R - $int;
        if($cuotip_id == 3)
          $amo = 0;
      }
      else{
        $int = $C*($i/100);
        $amo = $R - $int;
        if($cuotip_id == 3)
          $amo = 0;
      }

      $seguro_mensual = $C*($valor_seguro/100);

      //fecha facturacion
      $month = $month + 1;
      if($month == '13'){
        $month = 1;
        $year = $year + 1;
      }

      $fecha=validar_fecha_facturacion($day,$month,$year);

      if($j == 1){
        $int_pro = ($int / $dias_mes) * $dias_prorra;
        $monto_pro = $int_pro;
        $pro_div_cuo = $int_pro / $n;
        //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
        $pro_div_subcuo = $int_pro / ($n * $cols);
      }
      else
        $int_pro = 0;

      $xac=1;
      $cretip_id=3;//credito tipo 1=menor 2=asive 3=garve, 4 hipo
      $est=1;
      $persubcuo=4;
      $l = 3;
      if($cuosubper_id == 2){
        $persubcuo = 2;
        $l = 2;
      }else{
        if($cuosubper_id == 1){
          $persubcuo = 1;
          $l = 0;
        }
      }
//      $oCuota->insertar(
//        $xac,
//        $cre_id,
//        $cretip_id,
//        $mon_id,
//        $j,
//        fecha_mysql($fecha), 
//        $C, 
//        $amo, 
//        $int, 
//        $R + $pro_div_cuo, 
//        $pro_div_cuo,
//        $persubcuo, 
//        $est
//      );
//
//      $dts=$oCuota->ultimoInsert();
//        $dt = mysql_fetch_array($dts);
//        $cuo_id=$dt['last_insert_id()'];
//      mysql_free_result($dts);

        $oCuota->credito_id = $cre_id_new;
        $oCuota->creditotipo_id = $cretip_id;
        $oCuota->moneda_id = $mon_id;
        $oCuota->cuota_num = $j;
        $oCuota->cuota_fec = fecha_mysql($fecha);
        $oCuota->cuota_cap = $C;
        $oCuota->cuota_amo = $amo;
        $oCuota->cuota_int = $int;
        $oCuota->cuota_cuo = ($R + $pro_div_cuo + $gps_mensual);
        $oCuota->cuota_pro = $pro_div_cuo;
        $oCuota->cuota_persubcuo = $persubcuo;
        $oCuota->cuota_est = $est;
        $oCuota->cuota_acupag = 0;
        $oCuota->cuota_interes = $i;
        $oCuota->cuota_preseguro = $seguro_mensual; 
        $oCuota->cuota_pregps = $gps_mensual;

        $result = $oCuota->insertar();
        if(intval($result['estado']) == 1){
            $cuo_id = $result['cuota_id'];
        }
        $result = null;
      
      for ($k=0; $k < $persubcuo; $k++) {
        list($day,$mon,$year) = explode('-',$fecha);
        $fecha_new=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*$l),$year));
//        $oCuotadetalle->insertar(
//          $xac,
//          $cuo_id,
//          $mon_id,
//          $k+1,
//          fecha_mysql($fecha_new), 
//          ($R/$persubcuo)+$pro_div_subcuo, 
//          $est
//        );
        $oCuotadetalle->tb_cuota_id = $cuo_id;
        $oCuotadetalle->tb_moneda_id = $mon_id;
        $oCuotadetalle->tb_cuotadetalle_num = $k+1;
        $oCuotadetalle->tb_cuotadetalle_fec = fecha_mysql($fecha_new);
        $oCuotadetalle->tb_cuotadetalle_cuo = ($R/$persubcuo + $pro_div_subcuo + $gps_mensual);
        $oCuotadetalle->tb_cuotadetalle_est = $est;
        $oCuotadetalle->tb_cuotadetalle_acupag = 0;

        $result = $oCuotadetalle->insertar();
        
        if($cuosubper_id == 2)
          $l = 0;
        else{
          if($cuosubper_id != 1)
            $l--;
        }
      }
    }
//
//    $data['cre_id']=$cre_id;
//    $data['cre_msj']='Se registró correctamente:'.$n;

  
  //la generacion de cuotas se debe hacer al momento del desembolso
  $data['cre_id'] = $cre_id_new;
  $data['cre_msj'] = 'La Adenda Regular se registró correctamente.'.$n;

  echo json_encode($data);
}

?>
