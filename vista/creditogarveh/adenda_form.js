$(document).ready(function () {
  console.log('cambios adenda 5555');

  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-yellow'
  });

  $('#che_gps').on('ifChanged', function(event){
    // Obtener el estado actual del checkbox
    var isChecked = $(this).prop('checked');
    credito_cronograma();

    if (isChecked) {
      gps_form_simple();
    }
  });

  creditotipo();
  cambio_moneda($('#cmb_mon_id').val());
  
  $('#cmb_tip_aden').change(function (event) {
      var tip = $(this).val();
      if (tip == 4) {
          $('#div_cuo_res').show();
          $('#txt_cre_numcuo').val(1);
      } else
          $('#div_cuo_res').hide();

      credito_calculo();
  }); 
  
  
  $('#cmb_cuotip_id, #txt_cre_numcuo, #cmb_cuosubper_id, #txt_cre_int').change(function (e) {
      creditotipo();
  });
  
  $('#cmb_mon_id').change(function (e) {
      //monedacambio_boton();
      cambio_moneda($(this).val());
  });
  
  $('#txt_cre_preaco').change(function (event) {
      credito_calculo();
  });
  
  $('.moneda2').autoNumeric({
      aSep: ',',
      aDec: '.',
      //aSign: 'S/. ',
      //pSign: 's',
      vMin: '0.00',
      vMax: '999999.99'
  });

  $(".porcentaje").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "99.99",
  });
    
  $('#datetimepickera2, #datetimepickera4').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy",
      startDate: "-0d"
      //endDate: new Date()
  });

  $('#datetimepickera1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d",
    endDate: new Date()
  });

  $('#datetimepickera5, #datetimepickera3').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy",
      startDate: "-0d"
      //endDate: new Date()
  });
  $('#datetimepickera6').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy",
      startDate: new Date()
      //endDate: new Date()
  });
    
  $("#datetimepickera3").on('changeDate', function () {
    console.log('Se ejecuta aqi');
    cambio_moneda($('#cmb_mon_id').val());
  });
  
  $("#datetimepickera4, #datetimepickera5").on('changeDate', function () {
    credito_calculo();
  });

  $("#form_adenda").validate({
    submitHandler: function() {
      var poliza_id = parseInt($('#hdd_gps_simple_id').val()) || 0;
      console.log('poliza id: ' + poliza_id);

      if ($('#che_gps').is(':checked') && poliza_id <= 0){
        alerta_warning('IMPORTANTE', 'Has seleccionado el GPS para agregarlo al crédito pero aún no has llenado los datos, debes llenar los datos del GPS antes de guardar el crédito');

        return false;
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL +"creditogarveh/adenda_reg.php",
        async:true,
        dataType: "json",
        data: $("#form_adenda").serialize(),
        beforeSend: function() {
          //alert($("#for_cre").serialize());
          //$('#msj_credito').html("Guardando...");
          //$('#msj_credito').show(100);
        },
        success: function(data){
          //console.log(data);
          if(parseInt(data.cre_id) > 0){
            alerta_success("EXITO", "ADENDA REGISTRADA");
            $("#modal_adenda_form" ).modal( "hide" );
            $('#msj_credito').html(data.cre_msj);
          }
          else{
            $('#msj_credito_form').show();
            $('#msj_credito_form').html('AVISO IMPORTANTE: '+data.cre_msj);
            alerta_error("ERROR", "HA SUCEDIDO UN ERROR :"+data.cre_msj);
          }
        },
        complete: function(data){
          console.log(data);
        }
      });
    },
    rules: {
      cmb_cuotip_id: {
        required: true
      },
      cmb_mon_id: {
        required: true
      },
      cmb_cuedep_id: {
        required: true
      },
      cmb_rep_id: {
        required: true
      },
      txt_cre_tipcam: {
        required: true
      },
      txt_cre_preaco: {
        required: true
      },
      txt_cre_int: {
        required: true
      },
      txt_cre_numcuo: {
        required: true
      },
      txt_cre_numcuomax: {
        required: true
      },
      txt_cre_linapr: {
        required: true
      },
      txt_cre_feccre: {
        required: true,
      }
    },
    messages: {
      cmb_cuotip_id: {
        required: '*'
      },
      cmb_mon_id: {
        required: '*'
      },
      cmb_cuedep_id: {
        required: '*'
      },
      cmb_rep_id: {
        required: '*'
      },
      txt_cre_tipcam: {
        required: '*'
      },
      txt_cre_preaco: {
        required: '*'
      },
      txt_cre_int: {
        required: '*'
      },
      txt_cre_numcuo: {
        required: '*'
      },
      txt_cre_numcuomax: {
        required: '*'
      },
      txt_cre_linapr: {
        required: '*'
      },
      txt_cre_feccre: {
        required: '*'
      }
    }
  });

});


function cambio_moneda(monid) {

    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_reg.php",
        async: true,
        dataType: "json",
        data: {
            mon_id: monid,
            action: "obtener_dato",
            fecha: $('#txt_cre_fecdes').val()
        },
        beforeSend: function () {
            //$("#cbo_cancha_id2").val("<option>' cargando '</option>");
        },
        success: function (data) {

            $('#txt_cre_tipcam').val(data.moncam_val);
        },
        complete: function (data) {

        }
    });
}

function creditotipo() {
    var action = $("#action_credito").val();
    //var adenda = $("#hdd_aden_id").val();
    if ($('#cmb_cuotip_id').val() == '3')//libre
    {
        $('#cmb_cuosubper_id').val(1);
        $('#cmb_cuosubper_id').attr('readonly', true);

        $('#cre_fijo').hide();
        $('#cre_libre').show();
        if (action == "I" || action == "adenda") {
            $('#txt_cre_numcuo').val('1');
            $('#txt_cre_numcuomax').val('');
        }
    }
    if ($('#cmb_cuotip_id').val() == '4')//fijo
    {
        $('#cmb_cuosubper_id').attr('readonly', false);

        $('#cre_fijo').show();
        $('#cre_libre').hide();
        if (action == "I" || action == "adenda") {
            $('#txt_cre_numcuomax').val($('#txt_cre_numcuo').val());
        }
    }
    credito_calculo();
    $('#txt_cre_pla').val($('#txt_cre_numcuo').val());
}

function credito_calculo()
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/credito_calculo.php",
        async: true,
        dataType: "json",
        data: ({
            action: "cuotas",
            adenda: $('#hdd_aden_id').val(),
            veh_pre: $('#txt_veh_pre').val(),
            cre_ini: $('#txt_cre_ini').val(),
            cre_preaco: $('#txt_cre_preaco').val(),
            cre_int: $('#txt_cre_int').val(),
            cre_numcuo: $('#txt_cre_numcuo').val(),
            cre_numcuo_res: $('#txt_cre_cuo_res').val(), //cuotas que faltan por pagar, para cuota valor sirve para calcular el interés de esas cuotas
            tip_ade: $('#cmb_tip_aden').val()
        }),
        beforeSend: function () {
            //$('#txt_cre_linapr').html('Cargando...');
        },
        success: function (data) {
          $('#txt_cre_linapr').val(data.credito_resultado);
          //credito_cronograma();
          obtener_valor_seguro_gps();
        },
        complete: function (data) {

        }
    });
}

function credito_cronograma(){ 
  var v_pre = $('#txt_veh_pre').val();
  var v_preaco = $('#txt_cre_preaco').val();
  var act = $('#action_credito').val();
  var tipo_cambio = parseFloat($('#hdd_tipo_cambio_venta').val());
  var valor_seguro = parseFloat($('#hdd_seguro_porcentaje').val());
  var cre_interes = parseFloat($("#txt_cre_int").val());
  var tecm = valor_seguro + cre_interes;
  $('#txt_cre_tcem').autoNumeric("set", tecm);

  if(tipo_cambio <= 0){
    alerta_warning('IMPORTANTE', 'Registre el tipo de cambio para poder generar el cronograma');
    return false;
  }

  if(act == 'ad_regular'){
    v_pre = 0;
    v_preaco = $('#txt_cre_preaco_ad').val();
  }
  if($('#txt_cre_fecfac').val() == '' || $('#txt_cre_fecpro').val() == ''){
    //alert('Ingrese fecha de desembolso y prorrateo');
    console.log('no está entrando al cronograma');
    return false;
  }

  var estado_gps = 0;

  if ($('#che_gps').is(':checked'))
    estado_gps = $('#che_gps').val();
  
    console.log('el estado del gps es: ' + estado_gps)

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditogarveh/credito_cronograma.php",
    async:true,
    dataType: "html",
    data: ({
      veh_pre:  v_pre,
      cre_ini:  $('#txt_cre_ini').val(),
      cre_id: $('#hdd_cre_id').val(),
      fec_desem: $('#txt_cre_fecdes').val(),
      fec_prorra: $('#txt_cre_fecpro').val(),
      cre_preaco: v_preaco,
      cre_int: $('#txt_cre_int').val(),
      cre_numcuo: $('#txt_cre_numcuo').val(),
      cre_numcuo_res: $('#txt_cre_cuo_res').val(), //son la cantidad de cuotas restantes que faltan por pagar, sirve para calcular la CUOTA BALON
      cre_linapr: $('#hdd_cre_linapr').val(),
      mon_id: $('#cmb_mon_id').val(),
      cre_fecdes: $('#txt_cre_fecdes').val(),
      cre_fecfac: $('#txt_cre_fecfac').val(),
      cre_fecpro: $('#txt_cre_fecpro').val(),
      cre_per: $('#cmb_cre_subper').val(),
      tip_ade: $('#cmb_cre_tipade').val(),
      cuosubper_id: $('#cmb_cuosubper_id').val(),
      valor_seguro: $('#hdd_seguro_porcentaje').val(),
      valor_gps: $('#hdd_gps_precio').val(),
      estado_gps: estado_gps,
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function(){
      //$('#div_credito_cronograma').html('<span>Cargando...</span>');
    },
    success: function(html){
      $('#div_credito_cronograma').html(html);

      monto_pro = Number($('#hdd_mon_pro_1').val());
      /*var linapr = Number($('#txt_cre_linapr').val().replace(/[^0-9\.]+/g,""));
      
      suma_lin_pro = parseFloat(mon_pro + linapr).toFixed(2);
      console.log('prorra: '+mon_pro+' / linapr: '+linapr+' / suma: '+suma_lin_pro);*/
    },
    complete: function(){
      //credito_calculo();
    }
  });
}

function obtener_valor_seguro_gps(){
  var numero_cuotas = parseInt($('#txt_cre_numcuo').val());
  if(numero_cuotas <= 0)
    return false;

  $.ajax({
    type: "POST",
    url: VISTA_URL + "segurogps/segurogps_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "valores",
      numero_cuotas: numero_cuotas
    },
    beforeSend: function () {
    },
    success: function (data) {
      console.log(data);
      $('#hdd_seguro_porcentaje').val(data.valor_seguro);
      $('#hdd_gps_precio').val(data.valor_gps);
      $('#span_seguro_gps').text(data.texto);
      credito_cronograma();
    },
    complete: function (data) {},
  });
}
// JUAN 22-11-2023
function gps_form_simple() {
  var cliente_id = parseInt($('#hdd_his_cli_id').val());
  var gps_precio = parseInt($('#hdd_gps_precio').val());
  var num_cuotas = parseInt($('#txt_cre_numcuo').val());

  if(cliente_id > 0 && gps_precio > 0 && num_cuotas > 0){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "controlseguro/gps_form_simple.php",
      async: true,
      dataType: "html",
      data: {
        cliente_id: cliente_id,
        gps_precio: gps_precio,
        num_cuotas: num_cuotas,
        poliza_id: $('#hdd_gps_simple_id').val()
      },
      beforeSend: function () {},
      success: function (data) {
        if (data != "sin_datos") {
          $("#div_modal_gps_simple").html(data);
          $("#modal_gps_simple").modal("show");
          //funcion js para limbiar el modal al cerrarlo
          modal_hidden_bs_modal("modal_gps_simple", "limpiar"); //funcion encontrada en public/js/generales.js
        }
      },
    });
  }
  else
    alerta_warning('IMPORTANTE', 'Hay algunos datos que debes llenar en el crédito antes de registrar el GPS: cliente ID: ' + cliente_id + ', Precio GPS: ' + gps_precio + ', Cuotas: ' + num_cuotas)
}

// JUAN 22-11-2023 -> FUNCION GENERAL PARA TODOS LOS FORMULARIOS DONDE SE USE EL REGISTRO DE GPS
function get_poliza_id(data) {
  var poliza_id = parseInt(data.poliza_id); //viene desde el registro del formulario de gps simple
  $('#hdd_gps_simple_id').val(poliza_id);
  console.log(data)
}