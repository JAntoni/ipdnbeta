<?php 
  $fecha = new DateTime();

  $externo_fec1 = date('d-m-Y');
  $externo_fec2 = $externo_fec1;
?>


<div id="form_externo_filtro" class="form-inline" role="form">
    <button class="btn btn-primary btn-sm" onclick="externo_form('I',0)"><i class="fa fa-plus"></i> Nuevo</button>
  <div class="form-group">
    <div class='input-group date' id='datetimepicker1'>
      <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $externo_fec1;?>"/>
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
  </div>
  <div class="form-group">
    <div class='input-group date' id='datetimepicker2'>
      <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $externo_fec2;?>"/>
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
  </div>
    <div class="form-group" style="margin-left:9px">
        <a href="javascript:void(0)" onClick="externo_tabla()" class="btn btn-warning btn-sm">Buscar</a>
  </div>

</div>