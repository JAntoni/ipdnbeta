<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Externo.class.php');
  $oExterno = new Externo();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $fecha1 = fecha_mysql($_POST['fecha1']);
  $fecha2 = fecha_mysql($_POST['fecha2']);

  $result = $oExterno->mostrarTodos($fecha1,$fecha2);

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
        $moneda = ($value['tb_moneda_id']==1)? 'S/.' : 'US$';
        if($_SESSION['usuariogrupo_id'] == 2){
            $boton = '<a class="btn btn-danger btn-xs" title="Eliminar" onClick="externo_form(\'E\',' . $value['tb_externo_id'] . ')">Eliminar</a>';
        }
        else{
            $boton = '<a class="btn btn-succes btn-xs">Registrado</a>';;
        }
      $tr .='<tr class="filaTabla">';
        $tr.='
          <td id="fila" style="border-left: 1px solid #135896;">'.$value['tb_externo_id'].'</td>
          <td  id="fila">'. mostrar_fecha($value['tb_externo_fec']).'</td>
          <td  id="fila">'.$moneda.'</td>
          <td  id="fila">'. mostrar_moneda($value['tb_externo_mon']).'</td>
          <td  id="fila">'. mostrar_moneda($value['tb_externo_num']).'</td>
          <td  id="fila">'. mostrar_moneda($value['tb_externo_ban']).'</td>
          <td  id="fila">'. mostrar_moneda($value['tb_externo_des']).'</td>
          <td  id="fila">'.$boton.'</td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    $tr = $result['mensaje'];
    $result = null;
    //exit();
  }

?>
<table id="tbl_externos" class="table table-bordered table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th>ID</th>
      <th>FECHA</th>
      <th>MONEDA</th>
      <th>MONTO INICIAL</th>
      <th>N° OPERACIÓN</th>
      <th>BANCO</th>
      <th>DESCRIPCIÓN</th>
      <th>OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
