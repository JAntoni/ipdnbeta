<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../externo/Externo.class.php');
  $oExterno = new Externo();
  require_once('../transferente/Transferente.class.php');
  $oTransferente = new Transferente();
  require_once('../funciones/funciones.php');

  $direc = 'externo';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $externo_id = $_POST['externo_id'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'Externo Registrado';
}
if ($usuario_action == 'I') {
    $titulo = 'Registrar Externo';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Externo';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Externo';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en externo
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'externo'; $modulo_id = $externo_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del externo por su ID
    if(intval($externo_id) > 0){
      $result = $oExterno->mostrarUno($externo_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el externo seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $externo_tip = $result['data']['tb_externo_tip'];
          $externo_tipcam = $result['data']['tb_externo_tipcam'];
          $externo_fec = $result['data']['tb_externo_fec'];
          $externo_mon = $result['data']['tb_externo_mon'];
          $externo_num = $result['data']['tb_externo_num'];
          $externo_ban = $result['data']['tb_externo_ban'];
          $externo_des = $result['data']['tb_externo_des'];
          $transferente_id = $result['data']['tb_transferente_id'];
          $moneda_id = $result['data']['tb_moneda_id'];
          $result1 = $oTransferente->mostrarUno($transferente_id);
          if($result1['estado'] == 1){
              $transferente_nom = $result1['data']['tb_transferente_nom'];
              $transferente_doc = $result1['data']['tb_transferente_doc'];
          }
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_externo" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_externo" method="post">
          <input type="hidden" name="action"  id="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_externo_id" value="<?php echo $externo_id;?>">
          <input type="hidden" name="hdd_externo_tip" value="<?php echo $externo_tip;?>">
          <input type="hidden" name="hdd_transferente_id" id="hdd_transferente_id">
          
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="txt_monedacambio" class="control-label">Tipo Cambio :</label>
                              <input type="text" name="txt_monedacambio" id="txt_monedacambio" class="form-control input-sm mayus" value="<?php echo $externo_tipcam;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="cmb_externo_tip" class="control-label">Tipo de Inicial :</label>
                              <select name="cmb_externo_tip" id="cmb_externo_tip" class="form-control input-sm mayus">
                                <option value="1" <?php if($externo_tip == 1) echo 'selected'?>>En Efectivo</option>
                                <option value="2"<?php if($externo_tip == 2) echo 'selected'?>>En Depósito</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="cmb_moneda_id" class="control-label">Moneda :</label>
                              <select name="cmb_moneda_id" id="cmb_moneda_id" class="form-control input-sm mayus">
                                <option value="1" <?php if($moneda_id == 1) echo 'selected'?>>Soles</option>
                                <option value="2" <?php if($moneda_id == 2) echo 'selected'?>>Dolares</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_externo_fec" class="control-label">Inicial Fecha :</label>
                              <input name="txt_externo_fec" type="text" id="txt_externo_fec" class="form-control input-sm" value="<?php echo $externo_fec;?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_externo_mon" class="control-label">Inicial Monto :</label>
                              <input name="txt_externo_mon" type="text" id="txt_externo_mon" class="form-control input-sm moneda6" value="<?php echo $externo_mon;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row banco">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_externo_num" class="control-label">Depósito N° Ope :</label>
                              <input name="txt_externo_num" type="text" id="txt_externo_num" class="form-control input-sm mayus" value="<?php echo $externo_num;?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_externo_ban" class="control-label">Banco :</label>
                              <input name="txt_externo_ban" type="text" id="txt_externo_ban" class="form-control input-sm mayus" value="<?php echo $externo_ban;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_transferente_fil_nom" class="control-label">Nombre Transferente :</label>
                              <input name="txt_transferente_fil_nom" type="text" id="txt_transferente_fil_nom" class="form-control input-sm mayus" value="<?php echo $transferente_nom;?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_transferente_fil_doc" class="control-label">Doc Transferente :</label>
                              <input name="txt_transferente_fil_doc" type="text" id="txt_transferente_fil_doc" class="form-control input-sm mayus" value="<?php echo $transferente_doc;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_externo_des" class="control-label">Detalle :</label>
                              <textarea name="txt_externo_des" id="txt_externo_des" cols="75" class="form-control input-sm mayus"><?php echo $externo_des;?></textarea>
                            </div>
                        </div>
                    </div>
                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if($action == 'eliminar'):?>
                      <div class="callout callout-warning">
                        <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Externo?</h4>
                      </div>
                    <?php endif;?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="externo_mensaje" style="display: none;">
                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_externo">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_externo">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_externo">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/externo/externo_form.js';?>"></script>
