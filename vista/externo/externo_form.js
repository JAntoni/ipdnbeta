
$(document).ready(function(){
    var action = $("#action").val();
    if(action == 'eliminar'){
        $('#cmb_externo_tip').change();
    }
    else{
    cambio_moneda();
    }
  $('#txt_externo_fec').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  
  $('.moneda6').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '1.00',
    vMax: '9999999.00'
  });
  
  $('#cmb_moneda_id,#txt_externo_fec').change(function(event) {
cambio_moneda();
    });
    $('#cmb_externo_tip').change(function(event) {
            var tipo = parseInt($(this).val());
            if(tipo == 1){
                    $('.banco').hide();
            }
            if(tipo == 2){
                    $('.banco').show();
            }
    });
    
    
    
    //AUTOCOMPLETE PARA TRANSFERENTE
    $("#txt_transferente_fil_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "transferente/transferente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            //$('#hdd_cre_cli_id').val(ui.item.cliente_id);
            $('#hdd_transferente_id').val(ui.item.transferente_id);
            $("#txt_transferente_fil_doc").val(ui.item.transferente_doc);
            $("#txt_transferente_fil_nom").val(ui.item.transferente_nom);

            //vencimiento_tabla();
            event.preventDefault();
            $('#txt_transferente_fil_nom').focus();
        }
    });

    $("#txt_transferente_fil_doc").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "transferente/transferente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            //$('#hdd_cre_cli_id').val(ui.item.cliente_id);
            //$('#hdd_cre_cli_id_ori').val(ui.item.cliente_id);
            $('#hdd_transferente_id').val(ui.item.transferente_id);
            $("#txt_transferente_fil_doc").val(ui.item.transferente_doc);
            $("#txt_transferente_fil_nom").val(ui.item.transferente_nom);

            //vencimiento_tabla();
            event.preventDefault();
            $('#txt_transferente_fil_nom').focus();
        }
    });
    
  $('#form_externo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"externo/externo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_externo").serialize(),
				beforeSend: function() {
					$('#externo_mensaje').show(400);
					$('#btn_guardar_externo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#externo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#externo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		externo_tabla();
		      		$('#modal_registro_externo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#externo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#externo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_externo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#externo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#externo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_externo_fec: {
				required: true
			},
			txt_externo_mon: {
				required: true
			},
			txt_monedacambio: {
				required: true
			}
		},
		messages: {
			txt_externo_fec: {
				required: "Ingrese fecha"
			},
			txt_externo_mon: {
				required: "Ingrese monto"
			},
			txt_monedacambio: {
				required: "Ingrese moneda cambio"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});

function cambio_moneda(){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "monedacambio/monedacambio_reg.php",
      async:false,
      dataType: "json",
      data: ({
        action: "obtener_dato",
        fecha:  $('#txt_externo_fec').val(),
        mon_id:  $('#cmb_moneda_id').val(),
      }),
      beforeSend: function(){
      },
      success: function(data){
          //console.log(data);
        $('#txt_monedacambio').val(data.moncam_val); 
      },
      complete: function(){     
        // if($("#txt_cre_tipcam").val()!=""){
        //  $('#btn_cmb_amor_mon_id_2').hide();
        // }
      }
    });   
}

