<?php
error_reporting(1);
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Externo extends Conexion{
      private $externo_id;
      private $externo_tip;
      private $moneda_id;
      private $externo_tipcam;
      private $externo_fec;
      private $externo_mon;
      private $externo_num;
      private $externo_ban;
      private $transferente_id;
      private $externo_des;
      
    public function getExterno_id() {
        return $this->externo_id;
    }
    
    public function setExterno_id($externo_id){
        $this->externo_id = $externo_id;
    }
      
    public function getExterno_tip() {
        return $this->externo_tip;
    }
    
    public function setExterno_tip($externo_tip){
        $this->externo_tip = $externo_tip;
    }
      
    public function getMoneda_id() {
        return $this->moneda_id;
    }
    
    public function setMoneda_id($moneda_id){
        $this->moneda_id = $moneda_id;
    }
    
    public function getExterno_tipcam() {
        return $this->externo_tipcam;
    }
    
    public function setExterno_tipcam($externo_tipcam){
        $this->externo_tipcam = $externo_tipcam;
    }
    
    public function getExterno_fec() {
        return $this->externo_fec;
    }
    
    public function setExterno_fec($externo_fec){
        $this->externo_fec = $externo_fec;
    }
    
    public function getExterno_mon() {
        return $this->externo_mon;
    }
    
    public function setExterno_mon($externo_mon){
        $this->externo_mon = $externo_mon;
    }
    
    public function getExterno_num() {
        return $this->externo_num;
    }
    
    public function setExterno_num($externo_num){
        $this->externo_num = $externo_num;
    }
    
    public function getExterno_ban() {
        return $this->externo_ban;
    }
    
    public function setExterno_ban($externo_ban){
        $this->externo_ban = $externo_ban;
    }
    
    public function getTransferente_id() {
        return $this->transferente_id;
    }
    
    public function setTransferente_id($transferente_id){
        $this->transferente_id = $transferente_id;
    }
    
    public function getExterno_des() {
        return $this->externo_des;
    }
    
    public function setExterno_des($externo_des){
        $this->externo_des = $externo_des;
    }
   
    function insertar($externo_nom, $externo_dni, $externo_dir, $externo_den){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_externo
            (tb_externo_tip, tb_moneda_id, tb_externo_tipcam, tb_externo_fec, tb_externo_mon, 
            tb_externo_num,tb_externo_ban, tb_transferente_id, tb_externo_des)
          VALUES (:externo_tip, :moneda_id, :externo_tipcam, :externo_fec, :externo_mon, 
          :externo_num, :externo_ban, :transferente_id, :externo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":externo_tip", $this->getExterno_tip(), PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->getMoneda_id(), PDO::PARAM_INT);
        $sentencia->bindParam(":externo_tipcam", $this->getExterno_tipcam(), PDO::PARAM_STR);
        $sentencia->bindParam(":externo_fec", $this->getExterno_fec(), PDO::PARAM_STR);
        $sentencia->bindParam(":externo_mon", $this->getExterno_mon(), PDO::PARAM_STR);
        $sentencia->bindParam(":externo_num", $this->getExterno_num(), PDO::PARAM_STR);
        $sentencia->bindParam(":externo_ban", $this->getExterno_ban(), PDO::PARAM_STR);
        $sentencia->bindParam(":transferente_id", $this->getTransferente_id(), PDO::PARAM_INT);
        $sentencia->bindParam(":externo_des", $this->getExterno_des(), PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
//    function modificar($texterno, $externo_nom, $externo_dni, $externo_dir, $externo_den){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "UPDATE tb_externo SET tb_externo_nom =:externo_nom, tb_externo_dni =:externo_dni, tb_externo_dir =:externo_dir, tb_externo_den =:externo_den WHERE tb_texterno =:texterno";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":texterno", $texterno, PDO::PARAM_INT);
//        $sentencia->bindParam(":externo_nom", $externo_nom, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_dni", $externo_dni, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_dir", $externo_dir, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_den", $externo_den, PDO::PARAM_STR);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
    function eliminar($externo_id){
      $this->dblink->beginTransaction();
      try {
        //$sql = "UPDATE tb_externo SET tb_externo_xac=0 WHERE tb_texterno =:externo_id";
        $sql = "DELETE FROM tb_externo WHERE tb_externo_id =:externo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":externo_id", $externo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($externo){
      try {
        $sql = "SELECT * FROM tb_externo WHERE tb_externo_id=:externo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":externo", $externo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos($fecha1, $fecha2){
      try {
        $sql = "SELECT * FROM tb_externo WHERE tb_externo_fec BETWEEN :fecha1 AND :fecha2";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
