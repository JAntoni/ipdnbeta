function externo_form(usuario_act, externo_id){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"externo/externo_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      externo_id: externo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_externo_form').html(data);
        $('#modal_registro_externo').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_externo'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_externo', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'externo';
        var div = 'div_modal_externo_form';
        permiso_solicitud(usuario_act, externo_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      alerta_error('EROR',data.responseText);
      console.log(data.responseText);
    }
  });
}
function externo_tabla() {
    var fecha1 = $('#txt_filtro_fec1').val();
    var fecha2 = $('#txt_filtro_fec2').val();
       //alert(fecha1+' '+fecha2+' '+caja_id+' '+moneda_id);
  $.ajax({
    type: "POST",
    url: VISTA_URL + "externo/externo_tabla.php",
    async: true,
    dataType: "html",
    data: ({
        fecha1: fecha1,
        fecha2: fecha2
    }),
    beforeSend: function () {
      $("#externo_mensaje_tbl").show(300);
    },
    success: function (data) {
      //INSERTA MEDIANTE HTML LA DATA QUE REGRESA DE CLIENTE_TABLA.PHP
      $("#div_externo_tabla").html(data);
      $("#externo_mensaje_tbl").hide(300);
    },
    complete: function (data) {
      // console.log(data); MUESTRA LA DATA QUE DEVUELVE ACA PUEDO VER ERROR
    },
    error: function (data) {
        console.log(data);
      $("#externo_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DEL CLIENTE: " + data.responseText
      );
    },
  });
}

$(document).ready(function() {
  console.log('Perfil menu');

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  
  externo_tabla();


  $("#txt_filtro_fec1,#txt_filtro_fec2").change(function (){
            externo_tabla();
    });
});