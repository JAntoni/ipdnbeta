<?php

	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../externo/Externo.class.php');
        $oExterno = new Externo();
        require_once('../funciones/funciones.php');
        require_once('../funciones/fechas.php');
        
 	$action = $_POST['action'];
        $externo_tip = intval($_POST['cmb_externo_tip']);
        $externo_fec = fecha_mysql($_POST['txt_externo_fec']);
        $moneda_id = intval($_POST['cmb_moneda_id']);
        $externo_tipcam = $_POST['txt_monedacambio']; 
        $externo_mon = moneda_mysql($_POST['txt_externo_mon']);
        $externo_num = $_POST['txt_externo_num'];
        $externo_ban = mb_strtoupper($_POST['txt_externo_ban'], 'UTF-8');
        $externo_des = $_POST['txt_externo_des'];
        $transferente_id = intval($_POST['hdd_transferente_id']);
        
        $oExterno->setExterno_tip($externo_tip);
        $oExterno->setMoneda_id($moneda_id);
        $oExterno->setExterno_tipcam($externo_tipcam);
        $oExterno->setExterno_mon($externo_mon);
        $oExterno->setExterno_fec($externo_fec);
        $oExterno->setExterno_num($externo_num);
        $oExterno->setExterno_ban($externo_ban);
        $oExterno->setTransferente_id($transferente_id);
        $oExterno->setExterno_des($externo_des);
        
 	if($_POST['action'] == 'insertar'){
            $data['estado'] = 0;
            $data['mensaje'] = 'ERROR EN EL REGISTRO';
            $oExterno->insertar();

            $data['estado'] = 1;
            $data['mensaje'] = 'Depósito Registrado Correctamente';

            echo json_encode($data);
          }

          elseif($_POST['action'] == 'eliminar'){
                  $externo_id = $_POST['hdd_externo_id'];
            $data['estado'] = 0;
            $data['mensaje'] = 'ERROR EN EL REGISTRO'. $externo_id;
            
            $oExterno->eliminar($externo_id);
            
            $data['estado'] = 1;
            $data['mensaje'] = 'Deposito eliminado';
            echo json_encode($data);
          }

          else{
                  $data['estado'] = 0;
                  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

                  echo json_encode($data);
          }
  
?>