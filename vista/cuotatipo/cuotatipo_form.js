
$(document).ready(function(){
  $('#form_cuotatipo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"cuotatipo/cuotatipo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cuotatipo").serialize(),
				beforeSend: function() {
					$('#cuotatipo_mensaje').show(400);
					$('#btn_guardar_cuotatipo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#cuotatipo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#cuotatipo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		cuotatipo_tabla();
		      		$('#modal_registro_cuotatipo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#cuotatipo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#cuotatipo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_cuotatipo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#cuotatipo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#cuotatipo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_cuotatipo_nom: {
				required: true,
				minlength: 2
			},
			txt_cuotatipo_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_cuotatipo_nom: {
				required: "Ingrese un nombre para el Tipo de Crédito",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_cuotatipo_des: {
				required: "Ingrese una descripción para el Tipo de Crédito",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
