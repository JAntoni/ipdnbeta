<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cuotatipo/Cuotatipo.class.php');
  $oCuotatipo = new Cuotatipo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$cuotatipo_nom = mb_strtoupper($_POST['txt_cuotatipo_nom'], 'UTF-8');
 		$cuotatipo_des = $_POST['txt_cuotatipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Cuota Tipo.';
 		if($oCuotatipo->insertar($cuotatipo_nom, $cuotatipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cuota Tipo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$cuotatipo_id = intval($_POST['hdd_cuotatipo_id']);
 		$cuotatipo_nom = mb_strtoupper($_POST['txt_cuotatipo_nom'], 'UTF-8');
 		$cuotatipo_des = $_POST['txt_cuotatipo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Cuota Tipo.';

 		if($oCuotatipo->modificar($cuotatipo_id, $cuotatipo_nom, $cuotatipo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cuota Tipo modificado correctamente. '.$cuotatipo_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$cuotatipo_id = intval($_POST['hdd_cuotatipo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Cuota Tipo.';

 		if($oCuotatipo->eliminar($cuotatipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cuota Tipo eliminado correctamente. '.$cuotatipo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>