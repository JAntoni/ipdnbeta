<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Cuotatipo.class.php');
  $oCuotatipo = new Cuotatipo();

  $result = $oCuotatipo->listar_cuotatipos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_cuotatipo_id'].'</td>
          <td>'.utf8_encode($value['tb_cuotatipo_nom']).'</td>
          <td>'.$value['tb_cuotatipo_des'].'</td>
          <td>'.$value['tb_creditotipo_nom'].'</td>
          <td align="center">
            <div class="btn-group">
              <a class="btn btn-info btn-xs" title="Ver" onclick="cuotatipo_form(\'L\','.$value['tb_cutotipo_id'].')"><i class="fa fa-eye"></i></a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="cuotatipo_form(\'M\','.$value['tb_cuotatipo_id'].')"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cuotatipo_form(\'E\','.$value['tb_cuotatipo_id'].')"><i class="fa fa-trash"></i></a>
              <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
            </div>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_cuotatipos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
       <th>Tipo de Crédito</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
