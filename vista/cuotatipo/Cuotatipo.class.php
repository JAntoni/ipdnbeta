<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cuotatipo extends Conexion{

    function insertar($cuotatipo_nom, $cuotatipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuotatipo(tb_cuotatipo_xac, tb_cuotatipo_nom, tb_cuotatipo_des)
          VALUES (1, :cuotatipo_nom, :cuotatipo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotatipo_nom", $cuotatipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cuotatipo_des", $cuotatipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($cuotatipo_id, $cuotatipo_nom, $cuotatipo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuotatipo SET tb_cuotatipo_nom =:cuotatipo_nom, tb_cuotatipo_des =:cuotatipo_des WHERE tb_cuotatipo_id =:cuotatipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotatipo_id", $cuotatipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotatipo_nom", $cuotatipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cuotatipo_des", $cuotatipo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cuotatipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cuotatipo WHERE tb_cuotatipo_id =:cuotatipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotatipo_id", $cuotatipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cuotatipo_id){
      try {
        $sql = "SELECT * FROM tb_cuotatipo WHERE tb_cuotatipo_id =:cuotatipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotatipo_id", $cuotatipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cuotatipos(){
      try {
        $sql = "SELECT * FROM tb_cuotatipo c INNER JOIN tb_creditotipo a ON c.tb_cuotatipo_id = a.tb_creditotipo_id ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cuotatipos_creditotipo($creditotipo_id){
      try {
        $sql = "SELECT * FROM tb_cuotatipo";
        if(intval($creditotipo_id) == 1)
          $sql .= " WHERE tb_creditotipo_id = 1";
        else
          $sql .= " WHERE tb_creditotipo_id != 1";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
