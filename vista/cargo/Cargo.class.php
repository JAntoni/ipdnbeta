<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cargo extends Conexion{

    function insertar($cargo_nom, $cargo_des, $area_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cargo(tb_cargo_xac, tb_cargo_nom, tb_cargo_des, tb_area_id)
          VALUES (1, :cargo_nom, :cargo_des, :area_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cargo_nom", $cargo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cargo_des", $cargo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":area_id", $area_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($cargo_id, $cargo_nom, $cargo_des, $area_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cargo SET tb_cargo_nom =:cargo_nom, tb_cargo_des =:cargo_des, tb_area_id =:area_id WHERE tb_cargo_id =:cargo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cargo_nom", $cargo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cargo_des", $cargo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":area_id", $area_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cargo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cargo WHERE tb_cargo_id =:cargo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cargo_id){
      try {
        $sql = "SELECT * FROM tb_cargo cargo INNER JOIN tb_area area ON cargo.tb_area_id = area.tb_area_id WHERE tb_cargo_id =:cargo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cargos(){
      try {
        $sql = "SELECT * FROM tb_cargo cargo INNER JOIN tb_area area ON cargo.tb_area_id = area.tb_area_id ORDER BY tb_cargo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
