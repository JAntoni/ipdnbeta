
$(document).ready(function(){
  $('#form_cargo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"cargo/cargo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cargo").serialize(),
				beforeSend: function() {
					$('#cargo_mensaje').show(400);
					$('#btn_guardar_cargo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#cargo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#cargo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		cargo_tabla();
		      		$('#modal_registro_cargo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#cargo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#cargo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_cargo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#cargo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#cargo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_cargo_nom: {
				required: true,
				minlength: 2
			},
			txt_cargo_des: {
				required: true,
				minlength: 5
			},
			cmb_area_id: {
				required: true,
				min: 1
			}
		},
		messages: {
			txt_cargo_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_cargo_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			},
			cmb_area_id: {
				required: "Seleccione una Area",
				min: "Elija una Area por favor"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
