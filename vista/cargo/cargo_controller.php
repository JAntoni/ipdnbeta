<?php
	require_once '../../core/usuario_sesion.php';
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cargo/Cargo.class.php');
  $oCargo = new Cargo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$cargo_nom = mb_strtoupper($_POST['txt_cargo_nom'], 'UTF-8');
 		$cargo_des = $_POST['txt_cargo_des'];
 		$area_id = $_POST['cmb_area_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Usuario Grupo.';
 		if($oCargo->insertar($cargo_nom, $cargo_des, $area_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$cargo_id = intval($_POST['hdd_cargo_id']);
 		$cargo_nom = mb_strtoupper($_POST['txt_cargo_nom'], 'UTF-8');
 		$cargo_des = $_POST['txt_cargo_des'];
 		$area_id = $_POST['cmb_area_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Usuario Grupo.';

 		if($oCargo->modificar($cargo_id, $cargo_nom, $cargo_des, $area_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo modificado correctamente. '.$cargo_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$cargo_id = intval($_POST['hdd_cargo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Usuario Grupo.';

 		if($oCargo->eliminar($cargo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo eliminado correctamente. '.$cargo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>