<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Cargo.class.php');
  $oCargo = new Cargo();

  $result = $oCargo->listar_cargos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_cargo_id'].'</td>
          <td>'.$value['tb_cargo_nom'].'</td>
          <td>'.$value['tb_cargo_des'].'</td>
          <td>'.$value['tb_area_nom'].'</td>
          <td align="center">';

            if($value['tb_cargo_id'] != 1 && $value['tb_cargo_id'] != 8 && $value['tb_cargo_id'] != 11){
              $tr.='
                <a class="btn btn-info btn-xs" title="Ver" onclick="cargo_form(\'L\','.$value['tb_cargo_id'].')"><i class="fa fa-eye"></i> Ver</a>
                <a class="btn btn-warning btn-xs" title="Editar" onclick="cargo_form(\'M\','.$value['tb_cargo_id'].')"><i class="fa fa-edit"></i> Editar</a>
                <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cargo_form(\'E\','.$value['tb_cargo_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
                <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>';
            }
        $tr.='
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_cargos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Area</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
