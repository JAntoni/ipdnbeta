<?php
require_once('Falla.class.php');
$oFalla = new Falla();

$articulo_id = (empty($articulo_id))? intval($_POST['articulo_id']) : intval($articulo_id);
if(empty($_POST["falla_id"]) && empty($falla_id)){
	$falla_id = 0;
}
elseif(!empty($falla_id)){
	$falla_id = intval($falla_id);
}
else{
	$falla_id = intval($_POST["falla_id"]);
}


$option = '';

$parametros[0]['column_name'] = 'tb_articulo_id';
$parametros[0]['param0'] = $articulo_id;
$parametros[0]['datatype'] = 'INT';

$parametros[1]['column_name'] = 'tb_falla_xac';
$parametros[1]['param1'] = 1;
$parametros[1]['datatype'] = 'INT';

$otros_param["orden"]["column_name"] = "f.tb_falla_nombre";
$otros_param["orden"]["value"] = "ASC"; //fecha de registro desde el más actual, hasta el más antiguo

//PRIMER NIVEL
$option .= '<option value="-2" style="font-weight: bold;">..SELECCIONE..</option>';
$option .= '<option value="0" style="font-weight: bold;">AÑADIR FALLA</option>';
$result = $oFalla->listar_todos($parametros, $otros_param);
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($falla_id == $value['tb_falla_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_falla_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_falla_nombre'] . '</option>';
	}
}
$result = NULL;

//FIN PRIMER NIVEL
echo $option;
?>