<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Falla extends Conexion{
    public $tb_falla_id;
    public $tb_articulo_id;
    public $tb_falla_nombre;
    public $tb_falla_fecreg;
    public $tb_falla_usureg;
    public $tb_falla_fecmod;
    public $tb_falla_usumod;
    public $tb_falla_xac;

    public function listar_todos($array, $otros_param){
        //array: bidimensional con formato $column_name, $param.$i, $datatype
        //otros_param: arreglo para funciones diversas como el order by, limit, etc. los keys se definen de acuerdo al uso (ejm: ordenar: ["orden"]["column_name"], ["orden"]["value"])
        try {
            $sql = "SELECT f.* FROM tb_falla f";
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i])) {
                    $sql .= ($i > 0) ? " AND " : " WHERE ";

                    if (!empty($array[$i]["param$i"]) && empty($array[$i]["column_name"]) && empty($array[$i]["datatype"])) {
                        $sql .= " {$array[$i]["param$i"]}";
                    } else {
                        if (stripos($array[$i]["column_name"], 'fec') !== FALSE) { //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
                            $sql .= " DATE_FORMAT(f.{$array[$i]["column_name"]}, '%Y-%m-%d')";
                        } else { // de lo contrario solo se coloca el nombre de la columna a filtrar
                            $sql .= "f.{$array[$i]["column_name"]}";
                        }
                        $sql .= " = :param$i";
                    }
                }
            }
            if(!empty($otros_param["orden"])){
                $sql .= " ORDER BY {$otros_param["orden"]["column_name"]} {$otros_param["orden"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]["column_name"]) && !empty($array[$i]["datatype"])) {
                    $_PARAM = strtoupper($array[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
                    $sentencia->bindParam(":param$i", $array[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_falla (tb_articulo_id, tb_falla_nombre, tb_falla_usureg, tb_falla_usumod)
                    VALUES(:param0, :param1, :param2, :param3);";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_articulo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param1", $this->tb_falla_nombre, PDO::PARAM_STR);
            $sentencia->bindParam(":param2", $this->tb_falla_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param3", $this->tb_falla_usumod, PDO::PARAM_INT);
  
            $resultado['estado'] = $sentencia->execute();
  
            if($resultado['estado'] == 1){
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            }
            else{
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
}
