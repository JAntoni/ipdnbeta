
$(document).ready(function(){

	$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#form_perfilmenu').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"perfilmenu/perfilmenu_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_perfilmenu").serialize(),
				beforeSend: function() {
					$('#perfilmenu_mensaje').show(400);
					$('#btn_guardar_perfilmenu').prop('disabled', 'disabled');
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#perfilmenu_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#perfilmenu_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		perfilmenu_tabla();
		      		$('#modal_registro_perfilmenu').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#perfilmenu_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#perfilmenu_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_perfilmenu').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#perfilmenu_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#perfilmenu_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
		},
		messages: {
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
