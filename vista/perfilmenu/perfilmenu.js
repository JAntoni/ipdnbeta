var datatable_global;

function perfilmenu_form(usuario_act, perfilmenu_id, menu_id){ 
  if(parseInt(perfilmenu_id) == 0)
    usuario_act = 'I';

  var usuarioperfil_id = $('#cmb_usuarioperfil_id').val();

  $.ajax({
		type: "POST",
		url: VISTA_URL+"perfilmenu/perfilmenu_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      perfilmenu_id: perfilmenu_id,
      tb_usuarioperfil_id: usuarioperfil_id,
      menu_id: menu_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_perfilmenu_form').html(data);
      	$('#modal_registro_perfilmenu').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_perfilmenu'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_perfilmenu', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'perfilmenu';
      	var div = 'div_modal_perfilmenu_form';
      	permiso_solicitud(usuario_act, perfilmenu_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function perfilmenu_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"perfilmenu/perfilmenu_tabla.php",
    async: true,
    dataType: "html",
    data: $('#form_perfilmenu_filtro').serialize(),
    beforeSend: function() {
      $('#perfilmenu_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_perfilmenu_tabla').html(data);
      $('#perfilmenu_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#perfilmenu_mensaje_tbl').html('ERROR AL LISTAR LOS MENÚS: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_perfilmenus').DataTable({
    "pageLength": 25,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [0,1,2,3,4,5,6,7], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}

function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

$(document).ready(function() {
	jQuery('#wpf-loader-two').delay(200).fadeOut('slow');
	
	$('#cmb_usuarioperfil_id').change(function(event) {
		perfilmenu_tabla();
	});
});