<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();

  $action = $_POST['action'];

  if($action == 'insertar'){
    $perfilmenu_per = '';
    $usuarioperfil_id = intval($_POST['hdd_usuarioperfil_id']);
    $menu_id = intval($_POST['hdd_menu_id']);
    $perfilmenu_xac = intval($_POST['rad_perfilmenu_xac']);
    $array_permisos = $_POST['che_perfilmenu_per'];
    

    if(count($array_permisos) > 0){
      $perfilmenu_per = implode(',', $array_permisos);
      $perfilmenu_per = str_replace(',', '-', $perfilmenu_per);
    }

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar los permisos. ';

    if($usuarioperfil_id > 0 && $menu_id > 0){
      if($oPerfilMenu->insertar($perfilmenu_per, $usuarioperfil_id, $menu_id, $perfilmenu_xac)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Permisos para el Perfil Menú registrado correctamente.';
      }
    }
    else
      $data['mensaje'] = 'No se ha identificado los IDS del UsuarioPerfil o del Menú';

    echo json_encode($data);
  }

  elseif($action == 'modificar'){
    $perfilmenu_id = intval($_POST['hdd_perfilmenu_id']);
    $perfilmenu_per = '';
    $usuarioperfil_id = intval($_POST['hdd_usuarioperfil_id']);
    $menu_id = intval($_POST['hdd_menu_id']);
    $perfilmenu_xac = intval($_POST['rad_perfilmenu_xac']);
    $array_permisos = $_POST['che_perfilmenu_per'];

    if(count($array_permisos) > 0){
      $perfilmenu_per = implode(',', $array_permisos);
      $perfilmenu_per = str_replace(',', '-', $perfilmenu_per);
    }

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar los permisos.';
    
    if($perfilmenu_id > 0 && $usuarioperfil_id > 0 && $menu_id > 0){
      if($oPerfilMenu->modificar($perfilmenu_id, $perfilmenu_per, $usuarioperfil_id, $menu_id, $perfilmenu_xac)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Permisos para el Perfil Menú registrado correctamente.';
      }
    }
    else
      $data['mensaje'] = 'No se ha identificado los IDS de: PerfilMenu, UsuarioPerfil o del Menú';

    echo json_encode($data);
  }

  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'Action no identificado para: '.$action;

    echo json_encode($data);
  }
  
?>