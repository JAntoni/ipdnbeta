<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class PerfilMenu extends Conexion {

    function insertar($perfilmenu_per, $usuarioperfil_id, $menu_id, $perfilmenu_xac) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_perfilmenu (tb_perfilmenu_per, tb_usuarioperfil_id, tb_menu_id, tb_perfilmenu_xac) VALUES (:perfilmenu_per, :usuarioperfil_id, :menu_id, :perfilmenu_xac)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":perfilmenu_per", $perfilmenu_per, PDO::PARAM_STR);
            $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
            $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);
            $sentencia->bindParam(":perfilmenu_xac", $perfilmenu_xac, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar($perfilmenu_id, $perfilmenu_per, $usuarioperfil_id, $menu_id, $perfilmenu_xac) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_perfilmenu SET tb_perfilmenu_per =:perfilmenu_per, tb_usuarioperfil_id =:usuarioperfil_id, tb_menu_id =:menu_id, tb_perfilmenu_xac =:perfilmenu_xac WHERE tb_perfilmenu_id =:perfilmenu_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":perfilmenu_id", $perfilmenu_id, PDO::PARAM_INT);
            $sentencia->bindParam(":perfilmenu_per", $perfilmenu_per, PDO::PARAM_STR);
            $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
            $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);
            $sentencia->bindParam(":perfilmenu_xac", $perfilmenu_xac, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($perfilmenu_id) {
        try {
            $sql = "SELECT * FROM tb_perfilmenu per WHERE tb_perfilmenu_id =:perfilmenu_id";

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindParam(":perfilmenu_id", $perfilmenu_id, PDO::PARAM_INT);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se ha encontrado registros para la consulta";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function accesso_directorio_perfilmenu($direc, $usuarioperfil_id) {
        try {
            $sql = "SELECT * FROM tb_perfilmenu per INNER JOIN tb_menu me on me.tb_menu_id = per.tb_menu_id where tb_menu_dir =:direc and tb_usuarioperfil_id =:usuarioperfil_id and tb_perfilmenu_xac=1";

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindParam(":direc", $direc, PDO::PARAM_STR);
            $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "El usuario no tiene acceso a este directorio";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function retorna_menu_perfilmenu($usuarioperfil_id, $menu_idpadre) {
        try {
            $sql = "SELECT 
                        per.*,me.*, COUNT(me2.tb_menu_idp) as cant_submenu 
                    FROM tb_perfilmenu per 
                            INNER JOIN tb_menu me on me.tb_menu_id = per.tb_menu_id 
                            left join tb_menu me2 on me2.tb_menu_idp = me.tb_menu_id 
                where tb_usuarioperfil_id =:usuarioperfil_id and me.tb_menu_idp =:menu_idpadre and me.tb_menu_mos = 1 and tb_perfilmenu_xac=1 group by me.tb_menu_id order by me.tb_menu_ord";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
            $sentencia->bindParam(":menu_idpadre", $menu_idpadre, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "El usuario no tiene un menu asignado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function permiso_usuarioperfil_menu($usuarioperfil_id, $menu_id) {
        try {
            $sql = "SELECT * FROM tb_perfilmenu per WHERE tb_usuarioperfil_id =:usuarioperfil_id and tb_menu_id =:menu_id";

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
            $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se ha encontrado registros para la consulta";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}

?>
