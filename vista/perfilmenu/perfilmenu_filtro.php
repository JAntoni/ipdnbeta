<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-info">
		  <div class="panel-body">
		    <form id="form_perfilmenu_filtro" class="form-inline" role="form">
          <div class="form-group">
            <label for="cmb_usuarioperfil_id">Perfil:</label>
            <select id="cmb_usuarioperfil_id" name="cmb_usuarioperfil_id" class="form-control input-sm">
              <?php require_once(VISTA_URL.'usuarioperfil/usuarioperfil_select.php');?>
            </select>                               
          </div>
        </form>
		  </div>
		</div>
	</div>
</div>