<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../funciones/funciones.php');

  $direc = 'perfilmenu';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];

  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $perfilmenu_id = $_POST['perfilmenu_id'];
  $menu_id = $_POST['menu_id'];
  $tb_usuarioperfil_id = $_POST['tb_usuarioperfil_id'];

  $titulo = 'Desconocido';
  if($usuario_action == 'M')
    $titulo = 'Editar Permisos para el Perfil';
  if($usuario_action == 'I')
    $titulo = 'Registrar Permisos para el Perfil';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en perfilmenu
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'perfilmenu'; $modulo_id = $perfilmenu_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del perfilmenu por su ID
    $habilitado = '';
    $denegado = 'checked';
    $leer = '';
    $insertar = '';
    $modificar = '';
    $eliminar = '';

    if(intval($perfilmenu_id) > 0){
      $result = $oPerfilMenu->mostrarUno($perfilmenu_id);
        if($result['estado'] == 1){
          if($result['data']['tb_perfilmenu_xac'] == 1){
            $habilitado = 'checked';
            $denegado = '';
          }

          $perfilmenu_per = $result['data']['tb_perfilmenu_per']; //permisos de tipo L-I-M-E
          $array_permisos = explode('-', $perfilmenu_per);

          if(count($array_permisos)){
            if(in_array("L", $array_permisos))
              $leer = 'checked';
            if(in_array("I", $array_permisos))
              $insertar = 'checked';
            if(in_array("M", $array_permisos))
              $modificar = 'checked';
            if(in_array("E", $array_permisos))
              $eliminar = 'checked';
          }
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_perfilmenu" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_perfilmenu" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_perfilmenu_id" value="<?php echo $perfilmenu_id;?>">
          <input type="hidden" name="hdd_usuarioperfil_id" value="<?php echo $tb_usuarioperfil_id;?>">
          <input type="hidden" name="hdd_menu_id" value="<?php echo $menu_id;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="rad_perfilmenu_xac" class="control-label">Permiso</label>
              <div class="radio">
                <label><input type="radio" name="rad_perfilmenu_xac" class="flat-green" value="1" <?php echo $habilitado; ?>> Habilitado</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="rad_perfilmenu_xac" class="flat-green" value="0" <?php echo $denegado; ?>> Denegado</label>
              </div>
            </div>
            <div class="form-group">
              <label for="che_perfilmenu_per" class="control-label">Opciones</label>
              <div class="checkbox">
                <label><input type="checkbox" name="che_perfilmenu_per[]" class="flat-green" value="L" <?php echo $leer; ?>> Leer</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="che_perfilmenu_per[]" class="flat-green" value="I" <?php echo $insertar; ?>> Insetar</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="che_perfilmenu_per[]" class="flat-green" value="M" <?php echo $modificar; ?>> Modificar</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="che_perfilmenu_per[]" class="flat-green" value="E" <?php echo $eliminar; ?>> Eliminar</label>
              </div>
            </div>

            <?php if($action == 'eliminar'):?>
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> ¿Está seguro de que desea eliminar este Tipo de Crédito?</h4>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="perfilmenu_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Guardando Permisos de Perfil...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_perfilmenu">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_perfilmenu">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_perfilmenu">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/perfilmenu/perfilmenu_form.js';?>"></script>
