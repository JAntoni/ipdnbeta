<?php
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'cuota/Cuota.class.php');
    require_once(VISTA_URL.'reportecreditomenor/ReporteCreditomenor.class.php');
    require_once(VISTA_URL.'creditomenor/Creditomenor.class.php');
    require_once(VISTA_URL.'ingreso/Ingreso.class.php');
    require_once(VISTA_URL.'ventagarantia/Ventagarantia.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    require_once(VISTA_URL.'funciones/funciones.php');
  }
  else{
    require_once('../cuota/Cuota.class.php');
    require_once('../reportecreditomenor/ReporteCreditomenor.class.php');
    require_once('../creditomenor/Creditomenor.class.php');
    require_once('../ingreso/Ingreso.class.php');
    require_once('../ventagarantia/Ventagarantia.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
  }

  $oCuota = new Cuota();
  $oReporte = new ReporteCreditomenor();
  $oCredito = new Creditomenor();
  $oIngreso = new Ingreso();
  $oVenta = new Ventagarantia();

  $action = (isset($_POST['action']))? $_POST['action'] : $action;

  $tabla_credito = 'tb_creditomenor';
  $creditotipo_id = 1;
  $cuota_fec1 = (isset($_POST['cuota_fec1']))? fecha_mysql($_POST['cuota_fec1']) : fecha_mysql($cuota_fec1);
  $cuota_fec2 = (isset($_POST['cuota_fec2']))? fecha_mysql($_POST['cuota_fec2']) : fecha_mysql($cuota_fec2);
  $credito_est = '3,4,7';
  $moneda_id = 1;

  //tabla de facturado de credito menor
  $tabla_facturado = ''; $total_cuota_fac = 0; $total_mora_fac = 0; $total_remate = 0;

  //tabla de cobrado de credito menor
  $tabla_cobrado = ''; $total_cuota_cob = 0; $total_mora_cob = 0; $total_venta = 0; $neto_cuotas = 0; $neto_moras = 0;

  //tabla de clasificación de clientes
  $cliente_int = 0; $cliente_amor = 0; $cliente_liq = 0; $cliente_pier = 0;

  if($action == 'facturado'){
    //consultamos las cuotas facturadas en el rango de fechas seleccionadas
    $result = $oCuota->listar_cuotas_facturadas_tipocredito($tabla_credito, $creditotipo_id, $cuota_fec1, $cuota_fec2, $credito_est);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          if(intval($value['tb_credito_est']) == 4 || intval($value['tb_credito_est']) == 7){
            $total_cuota_fac += floatval($value['tb_cuota_int']) + floatval($value['tb_cuota_cap']);
          }
          else{
            if($value['tb_cuotatipo_id'] == 1)
              $total_cuota_fac += floatval($value['tb_cuota_int']);
            else
              $total_cuota_fac += floatval($value['tb_cuota_cuo']);
          }
          $total_mora_fac += floatval($value['tb_cuota_mor']);
        }
      }
    $result = NULL;

    //consultamos solo las cuotas vencidas impagas, para calcular su mora
    $fecha_filtro_time = new DateTime($cuota_fec2);
    
    $result = $oCuota->listar_cuotas_impagas_fecha($tabla_credito, $creditotipo_id, $cuota_fec2, $credito_est);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $cuota_fec_time = new DateTime(mostrar_fecha($value['tb_cuota_fec']));
          $diff = 3;//$fecha_ven_time->diff($hoy_time)->format("%a");

          $total_mora_fac += intval($diff)*5; //5 soles por día de mora
        }
      }
    $result = NULL;

    //consultamos los créditos que pasaron a remate en la fecha del rango seleccionado
    $credito_fecrem1 = $cuota_fec1; $credito_fecrem2 = $cuota_fec2; $credito_est = '5'; //credito solo en estado de remate
    $result = $oCredito->creditomenor_garantias_remate($credito_fecrem1, $credito_fecrem2, $credito_est);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $total_remate += formato_numero(floatval($value['tb_garantia_val']) * 2.5);
        }
      }
    $result = NULL;

    //consultamos las ventas que se hicieron para cliente particular o colaborador, jalamos el precio como facturado
    $ventagarantia_fec1 = $cuota_fec1; $ventagarantia_fec2 = $cuota_fec2;
    $result = $oVenta->listar_ventagarantias_fecha($ventagarantia_fec1, $ventagarantia_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $total_remate += formato_numero($value['venta_prec']);
          $_SESSION['total_remate'] = $total_remate;
        }
      }
    $result = NULL;

    $tabla_facturado = '
      <tr>
        <td>'.mostrar_moneda($total_cuota_fac).'</td>
        <td>'.mostrar_moneda($total_mora_fac).'</td>
        <td>'.mostrar_moneda($total_remate).'</td>
      </tr>';
  }
  if($action == 'cobrado'){
    $result = $oCuota->listar_cuotas_facturadas_tipocredito($tabla_credito, $creditotipo_id, $cuota_fec1, $cuota_fec2, $credito_est);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          //importe total de la cuota facturada
          $cuota_cap = floatval($value['tb_cuota_cap']);
          $cuota_int = floatval($value['tb_cuota_int']);
          $cuota_porc = formato_numero(floatval($value['tb_cuota_cap']) * 10/100 + $cuota_int);

          $cuota_id = $value['tb_cuota_id']; $tipo_cuota = 1;
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota, $moneda_id);
            if($result2['estado'] == 1){
              $importe_cuota = floatval($result2['data']['importe_total']);
              if($importe_cuota > 0){
                if($importe_cuota <= $cuota_int || ($importe_cuota > $cuota_int && $importe_cuota < $cuota_porc))
                  $cliente_int ++;
                if($importe_cuota >= $cuota_porc && $importe_cuota < $cuota_cap)
                  $cliente_amor ++;
                if($importe_cuota >= $cuota_cap)
                  $cliente_liq ++;
              }
              $_SESSION['cliente_int'] = $cliente_int;
              $_SESSION['cliente_amor'] = $cliente_amor;
              $_SESSION['cliente_liq'] = $cliente_liq;
              $total_cuota_cob += floatval($value2['importe_total']);
            }
          $result2 = NULL;

          if(floatval($value['tb_cuota_mor']) > 0){
            //importe total de la mora en las cuotas facturada
            $result2 = $oIngreso->mostrar_importe_total_mora($cuota_id, $tipo_cuota, $moneda_id);
              if($result2['estado'] == 1){
                foreach ($result2['data'] as $key => $value2) {
                  $total_mora_cob += floatval($value2['importe_total']);
                }
              }
            $result2 = NULL;
          }
        }
      }
    $result = NULL;

    //importe total de ventas por remate de productos
    $modulo_id = 90; //modulo de ingreso por REMATE, venta de garantías
    $ingreso_fec1 = $cuota_fec1; $ingreso_fec2 = $cuota_fec2;
    $result = $oIngreso->mostrar_importe_total_modulo_fecha($modulo_id, $moneda_id, $ingreso_fec1, $ingreso_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $total_venta = floatval($value['importe_total']);
        }
      }
    $result = NULL;

    //importe total de cuotas cobradas de credito menor
    $cuenta_id = 1; //CUENTA DE CANCELACION DE CUOTAS
    $subcuenta_id = 2; //SUBCUENTA DE CREDITO-MENOR
    $ingreso_fec1 = $cuota_fec1; $ingreso_fec2 = $cuota_fec2;
    $result = $oIngreso->mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $ingreso_fec1, $ingreso_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $neto_cuotas = floatval($value['importe_total']);
        }
      }
    $result = NULL;

    //importe total de MORAS COBRADAS
    $cuenta_id = 26; //CUENTA DE CANCELACION DE PAGO DE MORAS
    $subcuenta_id = 67; //SUBCUENTA DE PAGO DE MORAS DE CREDITO-MENOR
    $ingreso_fec1 = $cuota_fec1; $ingreso_fec2 = $cuota_fec2;
    $result = $oIngreso->mostrar_importe_total_cuenta_subcuenta_fecha($cuenta_id, $subcuenta_id, $moneda_id, $ingreso_fec1, $ingreso_fec2);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $neto_moras = floatval($value['importe_total']);
        }
      }
    $result = NULL;

    $por_vender = $_SESSION['total_remate'] - $total_venta;
    $tabla_cobrado = '
      <tr>
        <td>'.mostrar_moneda($total_cuota_cob).'</td>
        <td class="success">'.mostrar_moneda($neto_cuotas).'</td>
        <td>'.mostrar_moneda($total_mora_cob).'</td>
        <td class="success">'.mostrar_moneda($neto_moras).'</td>
        <td>'.mostrar_moneda($total_venta).'</td>
        <td>'.mostrar_moneda($por_vender).'</td>
      </tr>';
  }

  //creditos que pasaron a remate
  $credito_est = '5,6'; $array_cliente_pierde = array();
  $result = $oCredito->creditomenor_garantias_remate($cuota_fec1, $cuota_fec2, $credito_est);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        //guardamos la cantidad de clientes que pierden su producto
        array_push($array_cliente_pierde, $value['tb_cliente_id']);
      }
      //contamos la cantidad de clientes que pierden sus productos en el mes elegido
      $array_cliente_pierde = array_unique($array_cliente_pierde);
      $_SESSION['cliente_pier'] = count($array_cliente_pierde);
    }
  $result = NULL;
?>

<?php if($action == 'facturado'):?>
  <!-- TABLA DE CUOTAS FACTURADAS -->
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Facturado</th>
        <th>Moras</th>
        <th>Facturado Remate</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $tabla_facturado;?>
    </tbody>
  </table>
<?php endif;?>

<?php if($action == 'cobrado'):?>
  <!-- TABLA DE CUOTAS COBRADAS -->
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Facturado</th>
        <th class="success">Total Cobrado</th>
        <th>Moras</th>
        <th class="success">Total Moras</th>
        <th>Ventas Remate</th>
        <th>Por Vender</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $tabla_cobrado;?>
    </tbody>
  </table>
  <input type="hidden" id="hdd_cliente_int" value="<?php echo $_SESSION['cliente_int'];?>">
  <input type="hidden" id="hdd_cliente_amor" value="<?php echo $_SESSION['cliente_amor'];?>">
  <input type="hidden" id="hdd_cliente_liq" value="<?php echo $_SESSION['cliente_liq'];?>">
<?php endif;?>


<input type="hidden" id="hdd_cliente_pier" value="<?php echo $_SESSION['cliente_pier'];?>">