<?php
	
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<?php require_once('reportecreditomenor_filtro.php');?>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="reportecreditomenor_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
        </div>
				<div class="row">
					<!-- DETALLE DE CREDITO MENOR-->
					<div class="col-sm-6">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">FACTURADO CREDITO MENOR</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="table-responsive" id="div_facturado_tabla">
									<?php 
										$action = 'facturado';
										require('reportecreditomenor_tabla.php');
									?>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">COBRADO CREDITO MENOR</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="table-responsive" id="div_cobrado_tabla">
									<?php 
										$action = 'cobrado';
										require('reportecreditomenor_tabla.php');
									?>
								</div>
							</div>
						</div>
					</div>
					<!-- CLASIFICACION DE CLIENTES-->
					<div class="col-sm-12">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Clasificación de clientes</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="table-responsive" id="div_clasificar_tabla">
									<table class="table table-bordered">
								    <thead>
								      <tr>
								        <th>Cliente Interés</th>
								        <th>Cliente Amortiza</th>
								        <th>Cliente Liquida</th>
								        <th>Cliente Pierde</th>
								      </tr>
								    </thead>
								    <tbody>
								    	<!-- Variables definidas en la tabla reportecreditomenor_tabla -->
								      <tr>
								        <td id="td_cliente_int"><?php echo $_SESSION['cliente_int'];?></td>
								        <td id="td_cliente_amor"><?php echo $_SESSION['cliente_amor'];?></td>
								        <td id="td_cliente_liq"><?php echo $_SESSION['cliente_liq'];?></td>
								        <td id="td_cliente_pier"><?php echo $_SESSION['cliente_pier'];?></td>
								      </tr>
								    </tbody>
								  </table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
