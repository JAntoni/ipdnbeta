
function facturado_creditomenor_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"reportecreditomenor/reportecreditomenor_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      action: 'facturado',
      cuota_fec1: $('#txt_filtro_fec1').val(),
      cuota_fec2: $('#txt_filtro_fec2').val()
    }),
    beforeSend: function() {
      $('#reportecreditomenor_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_facturado_tabla').html(data);
      $('#reportecreditomenor_mensaje_tbl').hide(300);
      cobrado_creditomenor_tabla();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#reportecreditomenor_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function cobrado_creditomenor_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"reportecreditomenor/reportecreditomenor_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      action: 'cobrado',
      cuota_fec1: $('#txt_filtro_fec1').val(),
      cuota_fec2: $('#txt_filtro_fec2').val()
    }),
    beforeSend: function() {
      $('#reportecreditomenor_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_cobrado_tabla').html(data);
      $('#reportecreditomenor_mensaje_tbl').hide(300);

      $('#td_cliente_int').text($('#hdd_cliente_int').val());
      $('#td_cliente_amor').text($('#hdd_cliente_amor').val());
      $('#td_cliente_liq').text($('#hdd_cliente_liq').val());
      $('#td_cliente_pier').text($('#hdd_cliente_pier').val());
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#reportecreditomenor_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

$(document).ready(function() {
  console.log('Perfil menu');

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
});