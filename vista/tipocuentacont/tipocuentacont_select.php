<?php
require_once('Tipocuentacont.class.php');
$oTipocuentacont = new Tipocuentacont();

if(empty($_POST["tipocuentacont_id"]) && empty($tipocuentacont_id)){
	$tipocuentacont_id = 0;
}
elseif(!empty($tipocuentacont_id)){
	$tipocuentacont_id = intval($tipocuentacont_id);
}
else{
	$tipocuentacont_id = intval($_POST["tipocuentacont_id"]);
}

$option = '<option value="0" style="font-weight: bold;"></option>';

//PRIMER NIVEL
$result = $oTipocuentacont->listar();
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($tipocuentacont_id == $value['tb_tipocuentacont_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_tipocuentacont_id'] . '" style="font-weight: bold;" ' . $selected . '>'. $value['tb_tipocuentacont_id'].' - '. $value['tb_tipocuentacont_nom'] . '</option>';
	}
}
$result = NULL;

//FIN PRIMER NIVEL
echo $option;
?>