<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Tipocuentacont extends Conexion{
    public $tb_tipocuentacont_id;
    public $tb_tipocuentacont_nom;
    public $tb_tipocuentacont_cod;
    public $tb_tipocuentacont_xac;
    
    public function listar(){
        try {
            $sql = "SELECT * FROM tb_tipocuentacont;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }
}
