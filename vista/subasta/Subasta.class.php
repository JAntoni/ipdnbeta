<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Subasta extends Conexion
{
  public $subasta_id;
  public $garantia_id;
  public $subasta_monbase;
  public $subasta_fecini;
  public $subasta_fecfin;
  public $subasta_est = 0; // 0 esta activa y 1 finalizado, por defecto estará activa
  public $subasta_mongana = 0; //monto que gana la subasta por fecto en 0
  public $subasta_usugana = 0; // usuario que gana la subasta por fecto en 0
  public $subasta_usureg;

  public $subastadetalle_monto = 0;
  public $subastadetalle_fec;

  // Insertar una nueva subasta
  function insertar_subasta()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_subasta (tb_garantia_id, tb_subasta_monbase, tb_subasta_fecini, tb_subasta_fecfin, tb_subasta_usureg) 
                    VALUES (:garantia_id, :subasta_monbase, :subasta_fecini, :subasta_fecfin, :subasta_usureg)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $this->garantia_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subasta_monbase", $this->subasta_monbase, PDO::PARAM_STR);
      $sentencia->bindParam(":subasta_fecini", $this->subasta_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":subasta_fecfin", $this->subasta_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":subasta_usureg", $this->subasta_usureg, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; // Retorna 1 si es exitoso
    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  // Actualizar subasta
  function modificar_subasta()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_subasta SET 
          tb_subasta_monbase =:subasta_monbase, 
          tb_subasta_fecini =:subasta_fecini, 
          tb_subasta_fecfin =:subasta_fecfin
        WHERE tb_subasta_id = :subasta_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":subasta_id", $this->subasta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":subasta_monbase", $this->subasta_monbase, PDO::PARAM_STR);
      $sentencia->bindParam(":subasta_fecini", $this->subasta_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":subasta_fecfin", $this->subasta_fecfin, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }

  // Insertar oferta
  function insertar_oferta()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_subastadetalle (tb_subasta_id, tb_usuario_id, tb_subastadetalle_monto, tb_subastadetalle_fec) 
              VALUES (:subasta_id, :usuario_id, :subastadetalle_monto, :subastadetalle_fec)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":subasta_id", $this->subasta_id, PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_id", $this->subasta_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":subastadetalle_monto", $this->subastadetalle_monto, PDO::PARAM_STR);
      $sentencia->bindParam(":subastadetalle_fec", $this->subastadetalle_fec, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: ' . $e->getMessage();
    }
  }
  function mostrarUno($subasta_id){
    try {
      $sql = "SELECT * FROM tb_subasta WHERE tb_subasta_id =:subasta_id AND tb_subasta_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":subasta_id", $subasta_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  // Listar ofertas por subasta
  function listar_ofertas($subasta_id){
    try {
      $sql = "SELECT det.*, tb_usuario_nom FROM tb_subastadetalle det 
              INNER JOIN tb_usuario usu ON usu.tb_usuario_id = det.tb_usuario_id
              WHERE det.tb_subasta_id = :subasta_id ORDER BY tb_subastadetalle_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":subasta_id", $subasta_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "Éxito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor();
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay ofertas registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      echo 'Error!: ' . $e->getMessage();
    }
  }
  function mostrar_uno_por_garantia($garantia_id){
    try {
      $sql = "SELECT * FROM tb_subasta WHERE tb_garantia_id =:garantia_id AND tb_subasta_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function obtener_maximo_oferta($subasta_id){
    try {
      $sql = "SELECT tb_usuario_id, MAX(tb_subastadetalle_monto) AS monto_ofertado FROM tb_subastadetalle
              WHERE tb_subasta_id =:subasta_id
              GROUP BY tb_usuario_id
              ORDER BY monto_ofertado DESC
              LIMIT 1;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":subasta_id", $subasta_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_campo($subasta_id, $subasta_columna, $subasta_valor, $param_tip){
    $this->dblink->beginTransaction();
    try {
      if (!empty($subasta_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_subasta SET " . $subasta_columna . " = :subasta_valor WHERE tb_subasta_id = :subasta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":subasta_id", $subasta_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":subasta_valor", $subasta_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":subasta_valor", $subasta_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el egreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }  

  function mostrar_ganador_por_garantia($garantia_id){
    try {
      $sql = "SELECT suba.*, usu.tb_usuario_nom, usu.tb_usuario_ape FROM tb_subasta suba 
              INNER JOIN tb_usuario usu ON usu.tb_usuario_id = suba.tb_subasta_usugana 
              WHERE suba.tb_garantia_id = :garantia_id limit 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
