function guardar_subasta() {
  var action = $('#action').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "subasta/subasta_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: action,
      subasta_id: $('#hdd_subasta_id').val(),
      garantia_id: $('#hdd_garantia_id').val(),
      subasta_monbase: $('#txt_subasta_monbase').val(),
      subasta_fecini: $('#txt_subasta_fecini').val(),
      subasta_fecfin: $('#txt_subasta_fecfin').val()
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      if(parseInt(data.estado) == 1){
        notificacion_success(data.mensaje)
        if(action == 'insertar')
          $('#btn_guardar_subasta').hide();
      }
      else
        alerta_error("Error", data.mensaje);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alerta_error("Error", data.responseText);
      console.log(data);
    },
  });
}
function guardar_oferta() {
  var monto = parseInt($('#txt_subastadetalle_monto').val());
  if(isNaN(monto) || monto <= 0) {
    alerta_error("Error", "Debe ingresar un monto válido.");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "subasta/subasta_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: 'insertar_oferta',
      subasta_id: $('#hdd_subasta_id').val(),
      monto_base: $('#txt_subasta_monbase').val(),
      subastadetalle_monto: $('#txt_subastadetalle_monto').val()
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      if(parseInt(data.estado) == 1){
        notificacion_success(data.mensaje)
        listar_ofertas();
      }
      else
        swal_warning("Importante", data.mensaje, 10000);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alerta_error("Error", data.responseText);
      console.log(data);
    },
  });
}

function guardar_ganador() {
  var usuario_ganador = parseInt($('#hdd_usuario_ganador').val());
  if(isNaN(usuario_ganador) || usuario_ganador <= 0) {
    alerta_error("Alerta", "Aún no existe un usuario ganador");
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "subasta/subasta_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: 'guardar_ganador',
      subasta_id: $('#hdd_subasta_id').val(),
      garantia_id: $('#hdd_garantia_id').val()
    },
    beforeSend: function () {
    },
    success: function (data) {
      if(parseInt(data.estado) == 1){
        notificacion_success(data.mensaje)
      }
      else
        swal_warning("Importante", data.mensaje, 10000);
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alerta_error("Error", data.responseText);
      console.log(data);
    },
  });
}
function listar_ofertas() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "subasta/subastadetalle_tabla.php",
    async: true,
    dataType: "html",
    data: {
      subasta_id: $('#hdd_subasta_id').val()
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      $('.lista_ofertas').html(data)
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alerta_error("Error", data.responseText);
      console.log(data);
    },
  });
}
$(document).ready(function () {
  console.log('lets go 88')
  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '999999999.99'
  });

  $('.fecha_subasta').timepicker({
    format: 'YYYY-MM-DD HH:mm', // Formato de fecha y hora
    showClose: true,           // Botón para cerrar
    showClear: true,           // Botón para limpiar
    sideBySide: true,          // Fecha y hora en paralelo
    locale: 'es'               // Cambia a español
  });

  listar_ofertas();
});