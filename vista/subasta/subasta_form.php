<?php
require_once('../../core/usuario_sesion.php');
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../subasta/Subasta.class.php');
$oSubasta = new Subasta();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$usuario_sesion_id = $_SESSION['usuario_id'];
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 gerencia, 3 ventas
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$garantia_id = $_POST['garantia_id'];
$vista = $_POST['vista'];
$bandera = 1;

$titulo = '';
if ($usuario_action == 'I')
  $titulo = 'Subasta';
else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//? vamos a verificar si ya existe una subasta registrada previa
$subasta_id = 0;
$subasta_monbase = 0;
$subasta_fecini = '';
$subasta_fecfin = '';

$monto_ofertado = 0;
$maximo_ofertado = 'Sin Ofertas aun';
$usuario_ganador = 0;
$bandera_oferta = 0; //? si estaba bandera se mantiene en 0 no se muestra el input para ofertar
$mensaje_bandera = '<h5><b>Sin Subasta</b></h5>';

$action = 'insertar';
$boton_guardar = 'Guardar Subasta';
$result = $oSubasta->mostrar_uno_por_garantia($garantia_id);
  if($result['estado'] == 1){
    $subasta_id = $result['data']['tb_subasta_id'];
    $subasta_monbase = $result['data']['tb_subasta_monbase'];
    $subasta_fecini = $result['data']['tb_subasta_fecini'];
    $subasta_fecfin = $result['data']['tb_subasta_fecfin'];
    $subasta_est = intval($result['data']['tb_subasta_est']);

    $action = 'modificar';
    $boton_guardar = 'Modificar Subasta';

    $result = $oSubasta->obtener_maximo_oferta($subasta_id);
      if($result['estado'] == 1){
        if(intval($result['data']['monto_ofertado']) > 0){
          $monto_ofertado = formato_numero($result['data']['monto_ofertado']);
          $maximo_ofertado = 'S/. '.mostrar_moneda($result['data']['monto_ofertado']);
          $usuario_ganador = $result['data']['tb_usuario_id'];
        }
      }
    
    $bandera_oferta = 1; //? 1 se muestra input para ofertar
    $mensaje_bandera = '';

    $fecha_hora_hoy = date("d-m-Y H:i:00");

    $timestamp_mysql = strtotime($subasta_fecfin);
    $timestamp_actual = strtotime($fecha_hora_hoy);

    if ($timestamp_actual > $timestamp_mysql || $subasta_est == 1) {
      $bandera_oferta = 0; // NO SE MUESTRA
      $mensaje_bandera = '<h5 style="color: red;"><b>La Subasta Finalizó</b></h5>';
    }
  }
$result = NULL;

$hora_servidor = date('Y-m-d H:i:s');
?>
<style>
  .panel {
    border-radius: 8px;
    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
  }

  .panel-heading {
    font-size: 18px;
    font-weight: bold;
    text-align: center;
  }

  .btn-ofertar {
    background-color: #5cb85c;
    color: white;
  }

  .btn-ofertar:hover {
    background-color: #4cae4c;
  }

  .offer-list {
    max-height: 300px;
    overflow-y: auto;
  }
</style>
<?php if ($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_subasta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo; ?></h4>
        </div>

        <input type="hidden" id="garantia_vista" value="<?php echo $vista; ?>">
        <input type="hidden" name="action" id="action" value="<?php echo $action;?>" >
        <input type="hidden" name="hdd_subasta_id" id="hdd_subasta_id" value="<?php echo $subasta_id; ?>">
        <input type="hidden" name="hdd_garantia_id" id="hdd_garantia_id" value="<?php echo $garantia_id; ?>">

        <input type="hidden" id="hdd_usuario_ganador" value="<?php echo $usuario_ganador;?>">
        <input type="hidden" id="hdd_monto_ofertado" value="<?php echo $monto_ofertado;?>">

        <div class="modal-body">
          <div class="col-md-12">
            <?php if($usuario_sesion_id == 2 || $usuario_sesion_id == 11):?>
              <!-- Panel para el Administrador -->
              <div class="panel panel-primary shadow">
                <div class="panel-heading">Configuración de la Subasta</div>
                <div class="panel-body">
                  <div class="form-group">
                    <label for="txt_subasta_monbase">Monto Base</label>
                    <input type="text" class="form-control moneda" id="txt_subasta_monbase" id="txt_subasta_monbase" placeholder="Ingrese el monto base" value="<?php echo $subasta_monbase;?>">
                  </div>
                  <div class="form-group">
                    <label for="txt_subasta_fecini">Fecha y Hora de Inicio</label>
                    <input type="datetime-local" class="form-control" id="txt_subasta_fecini" name="txt_subasta_fecini" value="<?php echo $subasta_fecini;?>">
                  </div>
                  <div class="form-group">
                    <label for="txt_subasta_fecfin">Fecha y Hora de Fin</label>
                    <input type="datetime-local" class="form-control" id="txt_subasta_fecfin" name="txt_subasta_fecfin" value="<?php echo $subasta_fecfin;?>">
                  </div>
                  <button type="button" class="btn btn-primary btn-block" id="btn_guardar_subasta" onclick="guardar_subasta()"><?php echo $boton_guardar;?></button>
                </div>
              </div>
            <?php endif;?>
            
            <div class="panel panel-primary shadow">
              
                <div class="box panel-primary collapsed-box">
                  <div class="box-header with-border panel-heading">
                    <h3 class="box-title" style="font-weight: bold;">Reglas de la Subasta, al participar aceptas estas reglas</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                  <div class="box-body" style="display: none;font-size: 14pt;">
                    <p>1.	Todos los interesados deberán ofertar a través del sistema en el proceso de SUBASTA para convertirse en PARTICIPANTES, la oferta mínima está regulada por SISTEMA en + S/10.00, la oferta máxima no está limitada o determinada.</p>
                    <p>2.	La SUBASTA tendrá siempre fecha y hora establecida tanto de inicio como de fin. El sistema permitirá ingresar hasta la última oferta colocando en orden cronológico cada oferta, siendo la última y más alta la ganadora, siendo hasta el término de la fecha y hora determinada por sistema el punto de cierre de la misma.</p>
                    <p>3.	El responsable de moderar la SUBASTA (HGALVEZ) podrá determinar en función a sus CRITERIOS si una determinada SUBASTA amerita ser renovada, por lo que podrá extender la duración de la misma por los plazos que crea convenientes hasta finalizar la venta.</p>
                    <p>4.	Toda SUBASTA empieza con productos PREVIAMENTE REVISADOS, cada PARTICIPANTE en caso de requerirlo, debe haber realizado DE MANERA PREVIA la revisión del producto a adquirir, pudiendo solicitar a la persona responsable de la revisión (Adriana en caso de mall o Engel en caso de boulevard) las fotos, consultas y detalles de la pieza a adquirir ya que una vez realizada LA SUBASTA, esta es definitiva, por lo que el PARTICIPANTE deberá proceder OBLIGATORIAMENTE a la compra, no pudiendo DESISTIR de la misma.</p>
                    <p>5.	El PARTICIPANTE ganador de la subasta será el único usuario que podrá comprar EL PRODUCTO por sistema una vez finalizada LA SUBASTA; no podrá cederse esta condición a terceros dado que el sistema sólo le validará el precio final al usuario ganador.</p>
                  </div>
                </div>
              
            </div>

            

            <?php if($subasta_fecfin != ''):?>
              <div class="panel panel-primary shadow">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-md-6" id="reloj">Cargando...</div>
                    <div class="col-md-6">Subasta Finaliza: <?php echo mostrar_fecha_hora_segundo($subasta_fecfin);?></div>
                  </div>
                </div>
              </div>
            <?php endif;?>

            <!-- Panel para el Público -->
            <div class="panel panel-success shadow">
              <div class="panel-heading">Realizar una Oferta</div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-3">
                    <?php echo '<h5><b>MONTO BASE: S/. '.mostrar_moneda($subasta_monbase).'</b></h5>'?>
                  </div>
                  <div class="col-md-4">
                    <?php echo '<h5><b>ÚLTIMA OFERTA: '.$maximo_ofertado.'</b></h5>'?>
                  </div>

                  <div class="col-md-5 form-inline">
                    <?php if($bandera_oferta == 1):?>
                      <div class="form-group">
                        <input type="text" class="form-control moneda" id="txt_subastadetalle_monto" placeholder="Ingrese su oferta">
                      </div>
                      <button type="button" class="btn btn-ofertar" onclick="guardar_oferta()">Ofertar</button>
                    <?php endif;?>
                    <?php 
                      if($bandera_oferta == 0)
                        echo $mensaje_bandera;
                    ?>
                  </div>
                </div>
              </div>
            </div>

            <!-- Panel de Ofertas Realizadas -->
            <div class="panel panel-info shadow">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-9">
                  Ofertas Realizadas
                </div>
                <div class="col-xs-3 text-right">
                  <?php if(($usuario_sesion_id == 2 || $usuario_sesion_id == 11) && $subasta_est == 0):?>
                    <button type="button" class="btn bg-black" onclick="guardar_ganador()">Aceptar Oferta y Guardar</button>
                  <?php endif;?>
                </div>
              </div>
            </div>

              <div class="panel-body">
                <div class="offer-list lista_ofertas">
                  <!-- AQUI SE LISTA TODAS LAS OFERTAS REALIZADAS-->
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="button" class="btn btn-default modal-close" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_garantia_resumen">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/subasta/subasta_form.js?ver=1122'; ?>"></script>
<script>
  var intervalId;
  $(document).ready(function () {
      // Hora inicial del servidor pasada desde PHP
      let horaServidor = new Date("<?php echo $hora_servidor; ?>");
      
      // Función para actualizar el reloj cada segundo
      function actualizarReloj() {
          // Incrementar un segundo
          
          horaServidor.setSeconds(horaServidor.getSeconds() + 1);

          // Obtener las horas, minutos y segundos en formato de 24 horas
          const horas24 = horaServidor.getHours();
          const minutos = horaServidor.getMinutes().toString().padStart(2, '0');
          const segundos = horaServidor.getSeconds().toString().padStart(2, '0');

          // Convertir a formato de 12 horas
          const horas12 = horas24 % 12 || 12; // Ajustar 0 a 12
          const periodo = horas24 >= 12 ? 'PM' : 'AM';

          // Construir la hora en formato 12 horas
          const horaFormateada = horas12 + ":" + minutos + ":" + segundos + " " + periodo;
          console.log('hora servidor: ' + horaFormateada);
          // Actualizar el contenido del div con el reloj
          $('#reloj').text('Reloj: '+ horaFormateada);
      }

      // Ejecutar la función inmediatamente y luego cada segundo
      actualizarReloj();
      intervalId = setInterval(actualizarReloj, 1000);
  });
  $('.modal-close').on('click', function() {
    console.log('cerrando el interval');
    clearInterval(intervalId)
  });
</script>