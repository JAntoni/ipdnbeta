<?php
require_once('../../core/usuario_sesion.php');

require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../subasta/Subasta.class.php');
$oSubasta = new Subasta();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$usuario_id = intval($_SESSION['usuario_id']);
$action = $_POST['action'];

$oSubasta->subasta_id = intval($_POST['subasta_id']);
$oSubasta->garantia_id = intval($_POST['garantia_id']);
$oSubasta->subasta_monbase = moneda_mysql($_POST['subasta_monbase']);
$oSubasta->subasta_fecini = fecha_hora_mysql($_POST['subasta_fecini']);
$oSubasta->subasta_fecfin = fecha_hora_mysql($_POST['subasta_fecfin']);
$oSubasta->subasta_usureg = $usuario_id;

if ($action == 'insertar') {
  
  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al guardar la Subasta, ID Garantia: ' . $oSubasta->subasta_fecini;

  if($oSubasta->insertar_subasta()){
    $oGarantia->modificar_campo($oSubasta->garantia_id, 'tb_garantia_subasta', 1, 'INT'); //? se actualiza la garantía a subastado
    $data['estado'] = 1;
    $data['mensaje'] = 'Subasta registrada correctamente';
  }

  echo json_encode($data);
}

elseif ($action == 'modificar') {
  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al MODIFICAR la Subasta, ID Garantia: ' . $oSubasta->subasta_fecini;

  if($oSubasta->modificar_subasta()){
    $data['estado'] = 1;
    $data['mensaje'] = 'Subasta MODIFICADA correctamente';
  }

  echo json_encode($data);

} 
elseif ($action == 'eliminar') {
  echo json_encode($data);
}
elseif ($action == 'insertar_oferta') {
  //? EL ID de la subasta ya se obtiene al principio de los if
  $monto_ingresado = moneda_mysql($_POST['subastadetalle_monto']);

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al guardar la Oferta: ' . $oSubasta->subastadetalle_monto;

  //? antes de insertar vamos a obtener el máximo ofertado hasta el momento, solo se puede ofertar de 10 en 10, pero no el mismo monto
  $monto_ofertado = moneda_mysql($_POST['monto_base']); // el valor mínimo a ofertar es el base + 10 

  $result = $oSubasta->obtener_maximo_oferta($oSubasta->subasta_id);
    if($result['estado'] == 1){
      if(moneda_mysql($result['data']['monto_ofertado']) > 0)
        $monto_ofertado = moneda_mysql($result['data']['monto_ofertado']);
    }
  $result = NULL;
  
  $minimo_ofertar = $monto_ofertado + 10;

  if($monto_ingresado < $minimo_ofertar){
    $data['estado'] = 0;
    $data['mensaje'] = 'No puedes ofertar lo mismo o menor a la última Oferta, Mínimo a ofertar es: S/. '.mostrar_moneda($minimo_ofertar);
    echo json_encode($data);
    exit();
  }

  $fecha_hora_limite = NULL;

  $result = $oSubasta->mostrarUno($oSubasta->subasta_id);
    if($result['estado'] == 1){
      $fecha_hora_limite = new DateTime($result['data']['tb_subasta_fecfin']);
      $fecha_hora_limite->modify('+59 seconds'); // Sumamos 59 segundos
    }
  $result = NULL;
  
  $fecha_hora_actual = new DateTime(); // Obtiene la fecha y hora actual del servidor
  $hora_actual = $fecha_hora_actual->format('Y-m-d H:i:s');
  $hora_limite = $fecha_hora_limite->format('Y-m-d H:i:s');

  //? la oferta guarda: subasta_id, usuario_id, subastadetalle monto y la fecha de oferta en automático
  if($fecha_hora_actual <= $fecha_hora_limite){
    $oSubasta->subastadetalle_monto = $monto_ingresado;
    $oSubasta->subastadetalle_fec = $hora_actual;
    $oSubasta->insertar_oferta();

    $data['estado'] = 1;
    $data['mensaje'] = 'Oferta registrada correctamente. FECHA LÍMITE: '.$hora_limite.' / FECHA AHORA: '.$hora_actual;
  }
  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'La Subasta ya ha finalizado, no puedes ofertar más. FECHA LÍMITE: '.$hora_limite.' / FECHA AHORA: '.$hora_actual;
  }

  echo json_encode($data);
}
elseif ($action == 'guardar_ganador') {
  $subasta_id = intval($_POST['subasta_id']);
  $garantia_id = intval($_POST['garantia_id']);

  $monto_ofertado = formato_numero($result['data']['monto_ofertado']);
  $usuario_ganador = $result['data']['tb_usuario_id'];

  $result = $oSubasta->obtener_maximo_oferta($subasta_id);
    if($result['estado'] == 1){
      if(intval($result['data']['monto_ofertado']) > 0){
        $monto_ofertado = formato_numero($result['data']['monto_ofertado']);
        $usuario_ganador = $result['data']['tb_usuario_id'];
      }
    }
  $result = NULL;
  
  if($monto_ofertado > 0 && $usuario_ganador > 0){
    //? 1. PRIMERO VAMOS A GUARDAR EL USUARIO GANADOR Y MONTO MAXIMO OFERTADO EN LA TABLA SUBASTA
    $oSubasta->modificar_campo($subasta_id, 'tb_subasta_est', 1, 'INT'); //? la subasta pasa a finalizado estado 1
    $oSubasta->modificar_campo($subasta_id, 'tb_subasta_mongana', $monto_ofertado, 'STR');
    $oSubasta->modificar_campo($subasta_id, 'tb_subasta_usugana', $usuario_ganador, 'INT');

    //? 2. FINALMENTE FIJAMOS EL PRECIO DE LA GARANTÍA PARA QUE APAREZCA EL BOTON VENDER
    $oGarantia->modificar_campo($garantia_id, 'tb_garantia_prefij', $monto_ofertado, 'STR');

    $data['estado'] = 1;
    $data['mensaje'] = 'Datos de oferta y usuario ganador guardados correctamente';

    echo json_encode($data);
  }
  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'El valor ofertado es 0 o no existe el usuario ganador: ofertado '.$monto_ofertado.' | usuario_id '.$usuario_ganador;
    echo json_encode($data);
  }

}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
