<?php
require_once('../subasta/Subasta.class.php');
$oSubasta = new Subasta();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$subasta_id = intval($_POST['subasta_id']);
$lista = '';

$result = $oSubasta->listar_ofertas($subasta_id);
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $lista .= '
      <li class="list-group-item">
        <strong style="font-size: 12pt;">Usuario: '.$value['tb_usuario_nom'].'</strong>
        <span class="pull-right" style="font-size: 12pt;font-weight:bold;">S/. '.mostrar_moneda($value['tb_subastadetalle_monto']).' | '.mostrar_fecha_hora_segundo($value['tb_subastadetalle_fec']).'</span>
      </li>
    ';
  }
}
$result = NULL;
?>
<ul class="list-group">
  <?php echo $lista;?>
</ul>