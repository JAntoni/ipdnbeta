function cambiar_asesor(){
  var asesor_new = parseInt($('#cmb_asesor_new').val());
  if(asesor_new == 0)
    return false;
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cambioasesor/cambioasesor_controller.php",
    async: true,
    dataType: "JSON",
    data: $('#form_asesor').serialize(),
    beforeSend: function() {
      $('#h3_modal_title').text('Cambio de asesor');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      if(parseInt(data.estado) == 1){
        if(parseInt($('#hdd_creditotipo_id').val()) == 1)
          creditomenor_tabla();
        if(parseInt($('#hdd_creditotipo_id').val()) == 2)
          creditoasiveh_tabla();
        if(parseInt($('#hdd_creditotipo_id').val()) == 3)
          creditogarveh_tabla();
        if(parseInt($('#hdd_creditotipo_id').val()) == 4)
          creditohipo_tabla();
        $('#modal_cambio_asesor').modal('hide');
      }
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}