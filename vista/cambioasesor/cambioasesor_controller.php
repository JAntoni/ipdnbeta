<?php
	session_name("ipdnsac");
  session_start();
  
  require_once('../creditomenor/Creditomenor.class.php');
  //require_once('../creditoasiveh/Creditoasiveh.class.php');
  //require_once('../creditogarveh/Creditogarveh.class.php');
  //require_once('../creditohipo/Creditohipo.class.php');
  require_once('../funciones/funciones.php');

  $credito_id = intval($_POST['hdd_credito_id']);
  $creditotipo_id = intval($_POST['hdd_creditotipo_id']);
  $asesor_new = intval($_POST['cmb_asesor_new']);

  if(intval($creditotipo_id) == 1) $oCredito = new Creditomenor();
  if(intval($creditotipo_id) == 2) $oCredito = new Creditoasiveh();
  if(intval($creditotipo_id) == 3) $oCredito = new Creditogarveh();
  if(intval($creditotipo_id) == 4) $oCredito = new Creditohipo();

  $oCredito->modificar_campo($credito_id, 'tb_credito_usureg', $asesor_new, 'INT');

  $data['estado'] = 1;
  $data['mensaje'] = 'Cambio de asesor guardado correctamente.';
  echo json_encode($data);
?>