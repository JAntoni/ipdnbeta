<?php
  session_name("ipdnsac");
  session_start();
  
  require_once('../creditomenor/Creditomenor.class.php');
  //require_once('../creditoasiveh/Creditoasiveh.class.php');
  //require_once('../creditogarveh/Creditogarveh.class.php');
  //require_once('../creditohipo/Creditohipo.class.php');
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../funciones/funciones.php');

  $usuariogrupo_id = $_SESSION['usuariogrupo_id'];
  $usuario_action = $_POST['action']; //tipo de accion L(leer), I(insertar), M(modificar), E(eliminar)
  $credito_id = $_POST['credito_id'];
  $creditotipo_id = $_POST['creditotipo_id'];

  if(intval($creditotipo_id) == 1) $oCredito = new Creditomenor();
  if(intval($creditotipo_id) == 2) $oCredito = new Creditoasiveh();
  if(intval($creditotipo_id) == 3) $oCredito = new Creditogarveh();
  if(intval($creditotipo_id) == 4) $oCredito = new Creditohipo();

  $titulo = '';
  if($usuario_action == 'M')
    $titulo = 'Cambiar asesor del Crédito';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $result = $oCredito->mostrarUno($credito_id);
    if($result['estado'] == 1){
      $usuario_id = $result['data']['tb_usuario_id'];
      $usuario_nombre = $result['data']['usuario_nombre'];
    }
  $result = NULL;
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_cambio_asesor" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      <form id="form_asesor" method="post">
        <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id;?>">
        <input type="hidden" name="hdd_creditotipo_id" id="hdd_creditotipo_id" value="<?php echo $creditotipo_id;?>">
        
        <div class="modal-body">
          <?php if($usuariogrupo_id == 2):?>
            <div class="form-group">
              <label for="txt_asesor_nom" class="control-label">Asesor actual</label>
              <input type="text" class="form-control input-sm mayus" value="<?php echo $usuario_nombre;?>" readonly>
            </div>
            <div class="form-group">
              <label for="txt_asesor_des" class="control-label">Cambiar a</label>
              <select class="form-control input-sm" name="cmb_asesor_new" id="cmb_asesor_new">
                <?php
                  $usuario_columna = 'tb_usuario_mos'; $usuario_valor = 1; $param_tip = 'INT';
                  require_once('../usuario/usuario_select.php');
                ?>
              </select>
            </div>
          <?php endif;?>
          <?php if($usuariogrupo_id != 2):?>
            <div class="callout callout-info" id="asesor_mensaje">
              <h4><i class="fa fa-warning"></i> Solicte con administración para el cambio de asesor por favor.</h4>
            </div>
          <?php endif;?>
          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="asesor_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <?php if($usuario_action == 'M' && $usuariogrupo_id == 2): ?>
              <button type="button" class="btn btn-info" onclick="cambiar_asesor()">Guardar</button>
            <?php endif; ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cambioasesor/cambioasesor_form.js';?>"></script>

