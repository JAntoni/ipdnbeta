<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Vehiculoclase.class.php');
  $oVehiculoclase = new Vehiculoclase();

  $result = $oVehiculoclase->listar_vehiculoclases();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_vehiculoclase_id'].'</td>
          <td>'.$value['tb_vehiculoclase_nom'].'</td>
          <td>'.$value['tb_vehiculoclase_des'].'</td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="vehiculoclase_form(\'M\','.$value['tb_vehiculoclase_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="vehiculoclase_form(\'E\','.$value['tb_vehiculoclase_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
            <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_vehiculoclases" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
