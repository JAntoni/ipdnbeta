<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculoclase/Vehiculoclase.class.php');
  $oVehiculoclase = new Vehiculoclase();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$vehiculoclase_nom = mb_strtoupper($_POST['txt_vehiculoclase_nom'], 'UTF-8');
 		$vehiculoclase_des = $_POST['txt_vehiculoclase_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Clase de Vehículo.';
                $result = $oVehiculoclase->insertar($vehiculoclase_nom, $vehiculoclase_des);
                
 		if($result['estado']){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Marca registrado correctamente.';
 			$data['clase_id'] = $result['vehiculoclase_id'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$vehiculoclase_id = intval($_POST['hdd_vehiculoclase_id']);
 		$vehiculoclase_nom = mb_strtoupper($_POST['txt_vehiculoclase_nom'], 'UTF-8');
 		$vehiculoclase_des = $_POST['txt_vehiculoclase_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Clase de Vehículo.';

 		if($oVehiculoclase->modificar($vehiculoclase_id, $vehiculoclase_nom, $vehiculoclase_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Clase de Vehículo modificado correctamente. ';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculoclase_id = intval($_POST['hdd_vehiculoclase_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Clase de Vehículo.';

 		if($oVehiculoclase->eliminar($vehiculoclase_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Clase de Vehículo eliminado correctamente. '.$vehiculoclase_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>