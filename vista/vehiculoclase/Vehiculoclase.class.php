<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculoclase extends Conexion{

    function insertar($vehiculoclase_nom, $vehiculoclase_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_vehiculoclase(tb_vehiculoclase_xac, tb_vehiculoclase_nom, tb_vehiculoclase_des)
          VALUES (1, :vehiculoclase_nom, :vehiculoclase_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoclase_nom", $vehiculoclase_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoclase_des", $vehiculoclase_des, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $vehiculoclase_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result;
        $data['vehiculoclase_id'] = $vehiculoclase_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($vehiculoclase_id, $vehiculoclase_nom, $vehiculoclase_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculoclase SET tb_vehiculoclase_nom =:vehiculoclase_nom, tb_vehiculoclase_des =:vehiculoclase_des WHERE tb_vehiculoclase_id =:vehiculoclase_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoclase_id", $vehiculoclase_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoclase_nom", $vehiculoclase_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoclase_des", $vehiculoclase_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculoclase_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_vehiculoclase WHERE tb_vehiculoclase_id =:vehiculoclase_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoclase_id", $vehiculoclase_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculoclase_id){
      try {
        $sql = "SELECT * FROM tb_vehiculoclase WHERE tb_vehiculoclase_id =:vehiculoclase_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoclase_id", $vehiculoclase_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculoclases(){
      try {
        $sql = "SELECT * FROM tb_vehiculoclase";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
