
$(document).ready(function(){
  $('#form_vehiculoclase').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculoclase/vehiculoclase_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculoclase").serialize(),
				beforeSend: function() {
					$('#vehiculoclase_mensaje').show(400);
					$('#btn_guardar_vehiculoclase').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculoclase_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculoclase_mensaje').html(data.mensaje);
                        $('#modal_registro_vehiculoclase').modal('hide');
                        var modulo = $("#hdd_modulo").val();
                        if(modulo = 'creditogarveh'){
                            cargar_vehiculoclase(data.clase_id);
                        }
                        if(modulo != 'creditogarveh'){
		      	setTimeout(function(){ 
		      		vehiculoclase_tabla();
		      		 }, 1000
		      	);
                        }
					}
					else{
		      	$('#vehiculoclase_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculoclase_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculoclase').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculoclase_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculoclase_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_vehiculoclase_nom: {
				required: true,
				minlength: 1
			},
			txt_vehiculoclase_des: {
				required: true,
				minlength: 1
			}
		},
		messages: {
			txt_vehiculoclase_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_vehiculoclase_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
