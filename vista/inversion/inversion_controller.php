<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../inversion/Inversion.class.php');
  $oInversion = new Inversion();
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$inversion_nom = mb_strtoupper($_POST['txt_inversion_nom'], 'UTF-8');
		$mediocom_id =	intval($_POST['cmb_mediocom_id']);
		$creditotipo_id =	intval($_POST['cmb_creditotipo_id']);
		$inversion_fec1 =	fecha_mysql($_POST['txt_inversion_fec1']);
		$inversion_fec2 =	fecha_mysql($_POST['txt_inversion_fec2']);
		$inversion_can =	moneda_mysql($_POST['txt_inversion_can']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Inversion.';
 		if($oInversion->insertar($inversion_nom, $mediocom_id, $creditotipo_id, $inversion_fec1, $inversion_fec2, $inversion_can)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Inversion registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$inversion_id = intval($_POST['hdd_inversion_id']);
 		$inversion_nom = mb_strtoupper($_POST['txt_inversion_nom'], 'UTF-8');
 		$mediocom_id =	intval($_POST['cmb_mediocom_id']);
		$creditotipo_id =	intval($_POST['cmb_creditotipo_id']);
		$inversion_fec1 =	fecha_mysql($_POST['txt_inversion_fec1']);
		$inversion_fec2 =	fecha_mysql($_POST['txt_inversion_fec2']);
		$inversion_can =	moneda_mysql($_POST['txt_inversion_can']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Inversion.';

 		if($oInversion->modificar($inversion_id, $inversion_nom, $mediocom_id, $creditotipo_id, $inversion_fec1, $inversion_fec2, $inversion_can)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Inversion modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$inversion_id = intval($_POST['hdd_inversion_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Inversion.';

 		if($oInversion->eliminar($inversion_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Inversion eliminada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>