
$(document).ready(function(){
	$('#txt_inversion_can').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999.00'
	});
	$('#datetimepicker1, #datetimepicker2').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		//endDate : new Date()
	});

	$("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_inversion_fec1').val();
   	$('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_inversion_fec2').val();
   	$('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  
  $('#cmb_inversion_tip').change(function(event) {
  	$('#cmb_creditotipo_id').val(0);
  	
  	if($(this).val() == 3)
  		$('#group_creditotipo').show(300);
  	else
  		$('#group_creditotipo').hide(300);
  });

  $('#form_inversion').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"inversion/inversion_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_inversion").serialize(),
				beforeSend: function() {
					$('#inversion_mensaje').show(400);
					$('#btn_guardar_inversion').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#inversion_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#inversion_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		inversion_tabla();
		      		$('#modal_registro_inversion').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#inversion_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#inversion_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_inversion').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#inversion_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#inversion_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_inversion_nom: {
				required: true,
				minlength: 2
			},
			cmb_mediocom_id: {
				required: true,
				min: 1
			},
			cmb_creditotipo_id: {
				min: 1
			},
			txt_inversion_fec1: {
				required: true
			},
			txt_inversion_fec2: {
				required: true
			},
			txt_inversion_can: {
				required: true
			}
		},
		messages: {
			txt_inversion_nom: {
				required: "Ingrese un nombre para la Inversión",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			cmb_mediocom_id: {
				required: "Seleccione el Medio de Comunicación",
				min: "Seleccione el Medio de Comunicación"
			},
			cmb_creditotipo_id: {
				min: "Seleccione el tipo de Crédito por favor"
			},
			txt_inversion_fec1: {
				required: "Ingrese una fecha de Inicio"
			},
			txt_inversion_fec2: {
				required: "Ingrese una fecha de Fin"
			},
			txt_inversion_can: {
				required: "Ingre una cantidad por favor"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
