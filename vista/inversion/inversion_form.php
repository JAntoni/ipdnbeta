<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../inversion/inversion.class.php');
  $oinversion = new inversion();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'inversion';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $inversion_id = $_POST['inversion_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Inversion Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Inversion';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Inversion';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Inversion';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en inversion
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'inversion'; $modulo_id = $inversion_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del inversion por su ID
    if(intval($inversion_id) > 0){
      $result = $oinversion->mostrarUno($inversion_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el inversion seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $inversion_nom = $result['data']['tb_inversion_nom'];
          $mediocom_id = $result['data']['tb_mediocom_id'];
          $creditotipo_id = $result['data']['tb_creditotipo_id'];
          $inversion_fec1 = mostrar_fecha($result['data']['tb_inversion_fec1']);
          $inversion_fec2 = mostrar_fecha($result['data']['tb_inversion_fec2']);
          $inversion_can = $result['data']['tb_inversion_can'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_inversion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_inversion" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_inversion_id" value="<?php echo $inversion_id;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_inversion_nom" class="control-label">Nombre de Inversion</label>
              <input type="text" name="txt_inversion_nom" id="txt_inversion_nom" class="form-control input-sm mayus" value="<?php echo $inversion_nom;?>">
            </div>
            <div class="form-group">
              <label for="cmb_mediocom_id" class="control-label">Medio de Comunicación</label>
              <select name="cmb_mediocom_id" id="cmb_mediocom_id" class="form-control input-sm">
                <?php require_once('../mediocom/mediocom_select.php');?>
              </select>
            </div>
            <div class="form-group">
              <label for="cmb_creditotipo_id" class="control-label">Tipo de Crédito</label>
              <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm">
                <?php require_once('../creditotipo/creditotipo_select.php');?>
              </select>
            </div>
            <div class="form-group">
              <label for="txt_inversion_fec1" class="control-label">Fecha Inicio</label>
              <div class='input-group date' id='datetimepicker1'>
                <input type='text' class="form-control" name="txt_inversion_fec1" id="txt_inversion_fec1" value="<?php echo $inversion_fec1;?>"/>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="txt_inversion_fec2" class="control-label">Fecha Fin</label>
              <div class='input-group date' id='datetimepicker2'>
                <input type='text' class="form-control" name="txt_inversion_fec2" id="txt_inversion_fec2" value="<?php echo $inversion_fec2;?>"/>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="txt_inversion_can" class="control-label">Monto Invertido</label>
              <input type="text" name="txt_inversion_can" id="txt_inversion_can" class="form-control input-sm" value="<?php echo $inversion_can;?>">
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Inversion?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="inversion_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_inversion">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_inversion">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_inversion">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/inversion/inversion_form.js';?>"></script>
