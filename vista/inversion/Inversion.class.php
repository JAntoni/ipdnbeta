<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Inversion extends Conexion{

    function insertar($inversion_nom, $mediocom_id, $creditotipo_id, $inversion_fec1, $inversion_fec2, $inversion_can){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_inversion(tb_inversion_nom, tb_mediocom_id, tb_creditotipo_id, tb_inversion_fec1, tb_inversion_fec2, tb_inversion_can)
          VALUES (:inversion_nom, :mediocom_id, :creditotipo_id, :inversion_fec1, :inversion_fec2, :inversion_can)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":inversion_nom", $inversion_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":mediocom_id", $mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":inversion_fec1", $inversion_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":inversion_fec2", $inversion_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":inversion_can", $inversion_can, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($inversion_id, $inversion_nom, $mediocom_id, $creditotipo_id, $inversion_fec1, $inversion_fec2, $inversion_can){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_inversion SET tb_inversion_nom =:inversion_nom, tb_mediocom_id =:mediocom_id, tb_creditotipo_id =:creditotipo_id, tb_inversion_fec1 =:inversion_fec1, tb_inversion_fec2 =:inversion_fec2, tb_inversion_can =:inversion_can WHERE tb_inversion_id =:inversion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":inversion_id", $inversion_id, PDO::PARAM_INT);
        $sentencia->bindParam(":inversion_nom", $inversion_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":mediocom_id", $mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":inversion_fec1", $inversion_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":inversion_fec2", $inversion_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":inversion_can", $inversion_can, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($inversion_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_inversion WHERE tb_inversion_id =:inversion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":inversion_id", $inversion_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($inversion_id){
      try {
        $sql = "SELECT * FROM tb_inversion inver LEFT JOIN tb_mediocom medio on medio.tb_mediocom_id = inver.tb_mediocom_id LEFT JOIN tb_creditotipo cretip on cretip.tb_creditotipo_id = inver.tb_creditotipo_id WHERE tb_inversion_id =:inversion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":inversion_id", $inversion_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_inversiones($inversion_mes, $inversion_anio){
      try {
        $inversion_mes = intval($inversion_mes);
        $inversion_anio = intval($inversion_anio);

        $where_mes = '';
        $where_anio = '';
        if($inversion_mes > 0)
          $where_mes = ' AND MONTH(tb_inversion_fec1) =:inversion_mes';
        if($inversion_anio > 0)
          $where_anio = ' AND YEAR(tb_inversion_fec1) =:inversion_anio';

        $sql = "SELECT * FROM tb_inversion inver LEFT JOIN tb_mediocom medio on medio.tb_mediocom_id = inver.tb_mediocom_id LEFT JOIN tb_creditotipo cretip on cretip.tb_creditotipo_id = inver.tb_creditotipo_id WHERE 1=1".$where_mes.''.$where_anio;

        $sentencia = $this->dblink->prepare($sql);
        if($inversion_mes > 0)
          $sentencia->bindParam(":inversion_mes", $inversion_mes, PDO::PARAM_INT);
        if($inversion_anio > 0)
          $sentencia->bindParam(":inversion_anio", $inversion_anio, PDO::PARAM_INT);
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
