<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'inversion/Inversion.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    require_once(VISTA_URL.'funciones/funciones.php');
  }
  else{
    require_once('../inversion/Inversion.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
  }
  $oInversion = new Inversion();

  $inversion_mes = (empty($inversion_mes))? $_POST['inversion_mes'] : $inversion_mes;
  $inversion_anio = (empty($inversion_anio))? $_POST['inversion_anio'] : $inversion_anio;

?>
<table id="tbl_inversion" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Inversion</th> 
      <th>Medio Comunicación</th>
      <th>Tipo Crédito</th>
      <th>Fecha Inicio</th>
      <th>Fecha Fin</th>
      <th>Monto</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oInversion->listar_inversiones($inversion_mes, $inversion_anio);
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_inversion_id'];?></td>
            <td><?php echo $value['tb_inversion_nom'];?></td>
            <td><?php echo $value['tb_mediocom_nom'];?></td>
            <td><?php echo $value['tb_creditotipo_nom'];?></td>
            <td><?php echo mostrar_fecha($value['tb_inversion_fec1']); ?></td>
            <td><?php echo mostrar_fecha($value['tb_inversion_fec2']); ?></td>
            <td><?php echo mostrar_moneda($value['tb_inversion_can']); ?></td>
            <td align="center">
              <a class="btn btn-warning btn-xs" title="Editar" onclick="inversion_form(<?php echo "'M', ".$value['tb_inversion_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="inversion_form(<?php echo "'E', ".$value['tb_inversion_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
