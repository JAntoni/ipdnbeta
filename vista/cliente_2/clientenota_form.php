<?php
require_once('../../core/usuario_sesion.php');

require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();

$cre_id = $_POST['cre_id'];
$cre_tip = $_POST['cre_tip']; //3 credito garveh, 4 credito hipo
$cod = '';
$usuario_action = 'I';

if($cre_tip == 3){
	require_once ("../creditogarveh/Creditogarveh.class.php");
	$oCredito = new Creditogarveh();
	$cod = 'CGV';
}


$result = $oCredito->mostrarUno($cre_id);
if($result['estado']==1){
    $cli_id = $result['data']['tb_cliente_id']; //2139
    
    $result1 = $oCliente->mostrarUno($cli_id);
    if($result1['estado']==1){
        $cli_nom = $result1['data']['tb_cliente_nom'];
    }
    
    $cre_cod = $cod.'-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    $cre_subtip = 0;
    if($cre_tip == 2)
            $cre_subtip = $result['data']['tb_credito_tip1']; // en asiveh la columna se llama tip1
    else
            $cre_subtip = $result['data']['tb_credito_tip']; //2
}

$result = "";

$tr = '';
$result2 = $oCliente->listar_nota_cliente_credito1($cli_id, $cre_tip, $cre_subtip);
if($result2['estado']==1){
    foreach ($result2['data'] as $key => $value) {
        if($value['tb_notacliente_mos'] == 0){
            $mostrar = '<button type="button" id="btn_nota_'.$value['tb_notacliente_id'].'" class="btn btn-default" onclick="vista_nota('.$value['tb_notacliente_id'].',1)" style="color: green;border-radius:4px"><i class="fa fa-plus"></i> Mostrar</button>';
        }
        else{
            $mostrar = '<button type="button" id="btn_nota_'.$value['tb_notacliente_id'].'" class="btn btn-default" onclick="vista_nota('.$value['tb_notacliente_id'].',0)" style="color: red;border-radius:4px"><i class="fa fa-plus"></i> Ocultar</button>';
        }
        
    $tr .='<table>';
    $tr .='<tr>';
    $tr.='
        <td style="padding-rigth:5px">'.$value['tb_notacliente_det'].'</td>
        <td style="padding-left:5px">'.$mostrar.'</td>
        ';
    $tr.='</tr>'; 
    $tr.='</table><hr>'; 
    

    }
}

                              $result2 = "";

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_cliente_nota" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="padding: 2px">Nota Para el Crédito del Cliente</h4>
            </div>
            <form id="frm_clientenota">
                <input type="hidden" name="action" value="insertar">
                <input type="hidden" name="hdd_cli_id" value="<?php echo $cli_id;?>">
                <input type="hidden" name="hdd_cre_tip" value="<?php echo $cre_tip;?>">
                <input type="hidden" name="hdd_cre_subtip" value="<?php echo $cre_subtip;?>">
                <input type="hidden" name="hdd_cre_id" value="<?php echo $cre_id;?>">
                <div class="modal-body">
                    <label style="padding-right: 4px;font-weight: normal;">Credito : <?php echo '<b>'.$cre_cod.'</b>';?></label><br>
                    <label style="padding-right: 4px;font-weight: normal;">Cliente : <?php echo '<b>'.$cli_nom.'</b>';?></label>
                    <hr>
                    <div id="div_comentarios" style="max-height: 150px;overflow-y: auto;">
                            <?php echo $tr;?>
                    </div>
                    <br>
                    <strong>Ingrese Nota:</strong><br><br>
                    <textarea name="txt_clientenota_det" id="txt_clientenota_det" rows="2" cols="72"></textarea>
                    <br>
                    <strong>Mostrar Nota: </strong><input type="checkbox" name="che_nota_mos" value="1" checked="true">
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <input type="button" class="btn btn-info" id="btn_guardar_creditogarveh" onclick="guardarnota()" value="Guardar">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
            <div id="msj_clientenota" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
        </div>
    </div>
</div>

