<?php

require_once('../../core/usuario_sesion.php');
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../funciones/fechas.php');
require_once ('../referidos/Referidos.class.php');
$oReferido=new Referidos();

$action = (empty($_POST['action'])) ? $_GET['action'] : $_POST['action'];
// VALIDAR QUE SI ES JURIDICO Q OCULTE LOS COMBOS DE UBIGEO ACTUAL
$ubigeo_cod = '';
if (intval($_POST['cmb_ubigeo_coddis']) > 0)
    $ubigeo_cod = $_POST['cmb_ubigeo_coddep'] . $_POST['cmb_ubigeo_codpro'] . $_POST['cmb_ubigeo_coddis'];

$oCliente->cliente_tip = intval($_POST['txt_cliente_tip']);
$oCliente->cliente_emp = intval($_POST['che_cliente_emp']);
$oCliente->cliente_ref = intval($_POST['che_cliente_ref']);
$oCliente->cliente_seg = intval($_POST['che_cliente_seg']);
$oCliente->cliente_vis = intval($_POST['che_cliente_vis']);
$oCliente->cliente_des = intval($_POST['che_cliente_des']);

$oCliente->cliente_empruc = $_POST['txt_cliente_empruc'];
$oCliente->cliente_emprs = mb_strtoupper($_POST['txt_cliente_emprs'], 'UTF-8');
$oCliente->cliente_empdir = $_POST['txt_cliente_empdir'];

$oCliente->cliente_doc = $_POST['txt_cliente_doc'];
$oCliente->cliente_nom = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
$oCliente->cliente_con = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
$oCliente->cliente_tel = $_POST['txt_cliente_tel'];
$oCliente->cliente_cel = $_POST['txt_cliente_cel'];
$oCliente->cliente_telref = $_POST['txt_cliente_telref'];
$oCliente->cliente_protel = $_POST['txt_cliente_protel'];
$oCliente->cliente_procel = $_POST['txt_cliente_procel'];
$oCliente->cliente_protelref = $_POST['txt_cliente_protelref'];

$oCliente->cliente_ema = $_POST['txt_cliente_ema'];
$oCliente->cliente_dir = $_POST['txt_cliente_dir'];
$oCliente->cliente_fecnac = fecha_mysql($_POST['txt_cliente_fecnac']);
$oCliente->cliente_estciv = intval($_POST['cmb_cliente_estciv']);

$oCliente->ubigeo_cod = $ubigeo_cod;
$oCliente->mediocom_id = intval($_POST['cmb_mediocom_id']);
$oCliente->creditotipo_id = intval($_POST['cmb_creditotipo_id']);

$oCliente->zona_id = intval($_POST['cmb_zona_id']);
$oCliente->cliente_numpar = $_POST['txt_cliente_numpar'];
$oCliente->cliente_bien = intval($_POST['cmb_cliente_bien']);
$oCliente->cliente_firm = intval($_POST['cmb_cliente_firm']);

$oCliente->usuario_id = intval($_SESSION['usuario_id']);

if ($action == 'insertar') {
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar al Cliente.';

    $cliente_doc = $_POST['txt_cliente_doc'];
    $cliente_id = 0;
    if (trim($cliente_doc) != '') {
        $result = $oCliente->verificar_documento_cliente($cliente_doc, $cliente_id);
        $cliente_existe = $result['estado'];
        $result = NULL;
    }

    if ($cliente_existe == 1) {
        $data['mensaje'] = 'El documento del Cliente ya ha sido registrado / ' . $cliente_doc;
    } else {
        $result = $oCliente->insertar(); //devuelve un array, estado e ID de Cliente
        $cliente_id = $result['cliente_id'];
        $oCliente->modificar_campo($cliente_id, 'tb_cliente_sexo', intval($_POST['txt_cliente_sexo']), 'INT');
        
        if ($result['estado'] == 1) {

            $array_cliente_id2 = $_POST['txt_cliente_id2'];
            $array_clientedetalle_tip = $_POST['txt_clientedetalle_tip'];

            if (count($array_cliente_id2) == count($array_clientedetalle_tip) && count($array_cliente_id2) > 0) {

                $oCliente->eliminar_sociedad_cliente($cliente_id);

                for ($i = 0; $i < count($array_cliente_id2); $i++) {

                    $result2 = $oCliente->verificar_sociedad_cliente($cliente_id, $array_cliente_id2[$i]);
                    $estado_detalle = intval($result2['estado']);
                    $result2 = NULL;

                    if ($estado_detalle == 0)
                        $oCliente->insertar_clientedetalle($cliente_id, $array_cliente_id2[$i], $array_clientedetalle_tip[$i]);
                }
            }

            $data['estado'] = 1;
            $data['mensaje'] = 'Cliente registrado correctamente.';
        }
        $result = NULL;
        $data['cliente_id'] = $cliente_id;
        $data['cliente_doc'] = $_POST['txt_cliente_doc'];
        $data['cliente_nom'] = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
        $data['cliente_tel'] = $_POST['txt_cliente_tel'];
        $data['cliente_cel'] = $_POST['txt_cliente_cel'];
        $data['cliente_telref'] = $_POST['txt_cliente_telref'];
        
        if(intval($_POST['che_cliente_ref'])>0){
            if($_SESSION['usuariogrupo_id']==3){
            $oReferido->tb_cliente_id=$cliente_id;
            $oReferido->tb_usuario_id=$_SESSION['usuario_id'];
            $oReferido->registrar_tarea();
            
            }
        }
        $oCliente->modificar_campo($cliente_id, 'tb_cliente_int',intval($_POST['cmb_creditotipo_id']),'INT');
    }

    echo json_encode($data);
} elseif ($action == 'modificar') {
    $cliente_id = intval($_POST['hdd_cliente_id']);
    $oCliente->cliente_id = $cliente_id;

    $cliente_doc = $_POST['txt_cliente_doc'];
    if (trim($cliente_doc) != '') {
        $result = $oCliente->verificar_documento_cliente($cliente_doc, $cliente_id);
        $cliente_existe = $result['estado'];
        $result = NULL;
    }

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar al Cliente.';

    if ($oCliente->modificar()) {

        $array_cliente_id2 = $_POST['txt_cliente_id2'];
        $array_clientedetalle_tip = $_POST['txt_clientedetalle_tip'];

        if (count($array_cliente_id2) == count($array_clientedetalle_tip) && count($array_cliente_id2) > 0) {

            $oCliente->eliminar_sociedad_cliente($cliente_id);

            for ($i = 0; $i < count($array_cliente_id2); $i++) {

                $result2 = $oCliente->verificar_sociedad_cliente($cliente_id, $array_cliente_id2[$i]);
                $estado_detalle = intval($result2['estado']);
                $result2 = NULL;

                if ($estado_detalle == 0)
                    $oCliente->insertar_clientedetalle($cliente_id, $array_cliente_id2[$i], $array_clientedetalle_tip[$i]);
            }
        }

        $data['estado'] = 1;
        $data['mensaje'] = 'Cliente modificado correctamente.';
        $data['cliente_id'] = $cliente_id;
        $data['cliente_doc'] = $_POST['txt_cliente_doc'];
        $data['cliente_nom'] = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
        $data['cliente_tel'] = $_POST['txt_cliente_tel'];
        $data['cliente_cel'] = $_POST['txt_cliente_cel'];
        $data['cliente_telref'] = $_POST['txt_cliente_telref'];
    }
    $oCliente->modificar_campo($cliente_id, 'tb_cliente_int',intval($_POST['cmb_creditotipo_id']),'INT');
    echo json_encode($data);
} elseif ($action == 'eliminar') {
    $cliente_id = intval($_POST['hdd_cliente_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar al cliente.';

    if ($oCliente->eliminar($cliente_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Cliente eliminado correctamente. ' . $cliente_des;
    }

    echo json_encode($data);
} elseif ($action == 'clientedetalle') {
    
} elseif ($action == 'buscar') {
    $cliente_doc = intval($_POST['doc']);
    $result = $oCliente->mostrarPorDni($cliente_doc);
    $data['estado'] = 0;
    $data['mensaje'] = 'No se encuentra un cliente con ese documento.';
    if($result['estado']){
        $id_cliente = $result['data']['tb_cliente_id'];
        $data['estado'] = 1;
        $data['id_cliente'] = $id_cliente;
        $data['mensaje'] = 'Cliente encontrado correctamente. ' . $id_cliente;
    }
    echo json_encode($data);
}else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>
