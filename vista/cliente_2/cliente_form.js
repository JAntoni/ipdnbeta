function ubigeo_provincia_select(ubigeo_coddep) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep
        }),
        beforeSend: function () {
            $('#cmb_ubigeo_codpro').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_ubigeo_codpro').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}
function ubigeo_distrito_select(ubigeo_coddep, ubigeo_codpro) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep,
            ubigeo_codpro: ubigeo_codpro
        }),
        beforeSend: function () {
            $('#cmb_ubigeo_coddis').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_ubigeo_coddis').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}
function cliente_mostrar_ocultar() {
    var cliente_tip = $('input[name="txt_cliente_tip"]:checked').val();
    var cliente_estciv = $('#cmb_cliente_estciv').val();
    var vista = $('#cliente_vista').val();

    if (parseInt(cliente_tip) == 2){
        $('.datos_juridica').show();
        if(vista == 'creditogarveh'){
            // ANTHONY *
            console.log("VRGA");
        $('.datos_natural').hide();
        }
        else{
            $('.datos_natural').show();
        }
    }else
        $('.datos_juridica').hide();
    if (parseInt(cliente_estciv) == 2)
        $('#group_casado').show();
    
    
}
$(document).ready(function () {
    cliente_mostrar_ocultar();

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $('#datetimepicker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //endDate : new Date()
    });

    $('input[name="txt_cliente_tip"]').on('ifChecked', function (event) {
        if (parseInt($(this).val()) == 2)
            $('.datos_juridica').show(300);
        else
            $('.datos_juridica').hide(300);
    });

    $('#che_cliente_emp').on('ifChecked ifUnchecked', function (event) {
        if ($(this).is(':checked'))
            $('#che_cliente_des').iCheck('uncheck'); //check
    });

    $('#che_cliente_des').on('ifChecked ifUnchecked', function (event) {
        if ($(this).is(':checked'))
            $('#che_cliente_emp').iCheck('uncheck'); //check
    });

    $('#cmb_ubigeo_coddep').change(function (event) {
        var codigo_dep = $(this).val();
        if (parseInt(codigo_dep) == 0) {
            $('#cmb_ubigeo_codpro').html('');
            $('#cmb_ubigeo_coddis').html('');
        } else {
            ubigeo_provincia_select(codigo_dep);
            $('#cmb_ubigeo_coddis').html('');
        }
    });

    $('#cmb_ubigeo_codpro').change(function (event) {
        var codigo_dep = $('#cmb_ubigeo_coddep').val();
        var codigo_pro = $(this).val();

        if (parseInt(codigo_pro) == 0)
            $('#cmb_ubigeo_coddis').html('');
        else
            ubigeo_distrito_select(codigo_dep, codigo_pro);
    });

    $('#cmb_cliente_estciv').change(function (event) {
        var estado_civil = $(this).val();
        if (parseInt(estado_civil) == 2)
            $('#group_casado').show(300);
        else
            $('#group_casado').hide(300);

        $('#cmb_cliente_bien').val(0);
        $('#cmb_cliente_firm').val(0);
    });

    $("#txt_cliente_fil_nom").autocomplete({
        minLength: 3,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term, cliente_emp: 1},
                    response
                    );
        },
        select: function (event, ui) {
            $('#txt_cliente_fil_id').val(ui.item.cliente_id);
        }
    });

    $('.btn_eliminar_soc').click(function (event) {
        event.preventDefault();
        $(this).closest('section').remove();
    });

    $('#btn_sociedad_add').click(function (event) {
        event.preventDefault();
        var cliente_nom = $('#txt_cliente_fil_nom').val();
        var cliente_id = $('#txt_cliente_fil_id').val();
        var cliente_tipsoc = $('#cmb_cliente_tipsoc').val();
        var sociedad_nom = $('#cmb_cliente_tipsoc').find('option:selected').text();

        if (!cliente_id || parseInt(cliente_tipsoc) == 0) {
            alert('Revisar: ' + cliente_id + ', ' + cliente_tipsoc);
            return false;
        }

        var section = $('<section class="row" style="margin-bottom: 10px;"></section>');

        section.append('<div class="col-md-6"> <input type="text"  class="form-control input-sm mayus" value="' + cliente_nom + '" disabled>');
        section.append('<input type="hidden" name="txt_cliente_id2[]" value="' + cliente_id + '"></div>');

        section.append('<div class="col-md-4"><label class="control-label">' + sociedad_nom + '</label>');
        section.append('<input type="hidden" name="txt_clientedetalle_tip[]" value="' + cliente_tipsoc + '"></div>');

        section.append('<div class="col-md-2"><a href="" class="btn btn-danger btn-sm btn_eliminar_soc">Eliminar</a></div>');

        $('#div_sociedad_list').append(section);
        //$('#div_sociedad_list').append('<br>');

        $('#txt_cliente_fil_nom').val('');
        $('#txt_cliente_fil_id').val('');
        $('#cmb_cliente_tipsoc').val(0);

        section.find('.btn_eliminar_soc').click(function (event) {
            event.preventDefault();
            $(this).closest('section').remove();
        });
    });

    $('#form_cliente').validate({
        submitHandler: function () {

            if (!$('#che_cliente_emp').is(":checked") && !$('#che_cliente_ref').is(":checked")) {
                alert('Debe seleccionar al menos un estado para el cliente');
                return false;
            }

            $.ajax({
                type: "POST",
                url: VISTA_URL + "cliente/cliente_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_cliente").serialize(),
                beforeSend: function () {
                    $('#cliente_mensaje').show(400);
                    $('#btn_guardar_cliente').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#cliente_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#cliente_mensaje').html(data.mensaje);
                        var vista = $('#cliente_vista').val();
                        if (vista == 'cliente')
                            cliente_tabla();
                        if (vista == 'credito') {
                            llenar_datos_cliente(data);
                        }
                        if (vista == 'creditogarveh') {
                            llenar_campos_cliente(data);
                        }
                        if (vista == 'pago_banco') {
                            llenar_datos_cliente_banco(data);
                        }
                        if (vista == 'cliente_referido') {
                            referidos_tabla();
                        }
                        setTimeout(function () {
                            $('#modal_registro_cliente').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#cliente_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#cliente_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_cliente').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#cliente_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#cliente_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_cliente_tip: {
                required: true
            },
            txt_cliente_empruc: {
                digits: true,
                minlength: 11,
                maxlength: 11
            },
            txt_cliente_doc: {
                required: function () {
                    if ($('#che_cliente_emp').is(":checked")) {
                        return true;
                    } else {
                        return false;
                    }
                },
                digits: true,
                minlength: 8,
                maxlength: 11
            },
            txt_cliente_nom: {
                required: true,
                minlength: 2
            },
            txt_cliente_tel: {
                required: function () {
                    var cliente_cel = $('#txt_cliente_cel').val().trim();
                    if (cliente_cel)
                        return false;
                    else
                        return true;
                },
                digits: true,
                minlength: 6,
                maxlength: 9
            },
            txt_cliente_cel: {
                required: function () {
                    var cliente_tel = $('#txt_cliente_tel').val().trim();
                    if (cliente_tel)
                        return false;
                    else
                        return true;
                },
                digits: true,
                minlength: 6,
                maxlength: 9
            },
            txt_cliente_ema: {
                email: true
            },
            txt_cliente_dir: {
                required: function () {
                    if ($('#che_cliente_emp').is(":checked")) {
                        return true;
                    } else {
                        return false;
                    }
                },
                minlength: 5
            },
            txt_cliente_fecnac: {
                required: function () {
                    if ($('#che_cliente_emp').is(":checked")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            cmb_ubigeo_coddis: {
                required: function () {
                    if ($('#che_cliente_emp').is(":checked")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            cmb_mediocom_id: {
                min: 1
            }
        },
        messages: {
            txt_cliente_tip: {
                required: "Seleccione el tipo, Natural o Jurídica"
            },
            txt_cliente_emprs: {
                digits: "Solo números por favor",
                minlength: "El RUC como mínimo debe tener 11 números",
                maxlength: "El RUC como máximo debe tener 11 números"
            },
            txt_cliente_doc: {
                required: "Ingrese el número de documento del Cliente",
                digits: "Solo números por favor",
                minlength: "El documento como mínimo debe tener 8 números",
                maxlength: "El documento como máximo debe tener 11 números"
            },
            txt_cliente_nom: {
                required: "Ingrese un nombre para el Cliente",
                minlength: "Como mínimo el nombre debe tener 2 caracteres"
            },
            txt_cliente_tel: {
                required: "Ingrese el número de teléfono del Cliente",
                digits: "Solo números por favor",
                minlength: "El teléfono como mínimo debe tener 6 números",
                maxlength: "El teléfono como máximo debe tener 9 números"
            },
            txt_cliente_cel: {
                required: "Ingrese el número de celular del Cliente",
                digits: "Solo números por favor",
                minlength: "El celular como mínimo debe tener 6 números",
                maxlength: "El celular como máximo debe tener 9 números"
            },
            txt_cliente_ema: {
                email: "Ingrese un Email válido por favor."
            },
            txt_cliente_dir: {
                required: "Ingrese una dirección para el Cliente",
                minlength: "La dirección debe tener como mínimo 5 caracteres"
            },
            txt_cliente_fecnac: {
                required: "Ingrese la fecha de nacimiento del Cliente"
            },
            cmb_ubigeo_coddis: {
                required: "Elija un distrito para el Cliente"
            },
            cmb_mediocom_id: {
                min: "Elija el medio de comunicación del Cliente"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});
