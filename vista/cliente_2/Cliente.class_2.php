<?php
  if(defined('APP_URL')){
  require_once(APP_URL.'datos/conexion.php');}
  else{
  require_once('../../datos/conexion.php');}
  class Cliente extends Conexion{
    public $cliente_id;
    public $cliente_tip;
    public $cliente_emp;
    public $cliente_ref;
    public $cliente_seg;
    public $cliente_vis;
    public $cliente_des;

    public $cliente_empruc; //RUC
    public $cliente_emprs; //razón social
    public $cliente_empdir; //dirección de la empresa

    public $cliente_doc;
    public $cliente_nom;
    public $cliente_con;
    public $cliente_tel;
    public $cliente_cel;
    public $cliente_telref;
    public $cliente_protel;
    public $cliente_procel;
    public $cliente_protelref;

    public $cliente_ema;
    public $cliente_dir;
    public $cliente_fecnac;
    public $cliente_estciv;

    public $ubigeo_cod;
    public $mediocom_id;
    public $creditotipo_id;

    public $zona_id;
    public $cliente_numpar;
    public $cliente_bien;
    public $cliente_firm;

    public $usuario_id;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cliente (
                            tb_cliente_xac,
                            tb_cliente_tip, tb_cliente_emp, tb_cliente_ref,
                            tb_cliente_seg, tb_cliente_vis, tb_cliente_des,
                            tb_cliente_empruc, tb_cliente_emprs, tb_cliente_empdir,
                            tb_cliente_doc, tb_cliente_nom, tb_cliente_con,
                            tb_cliente_tel, tb_cliente_cel, tb_cliente_telref, 
                            tb_cliente_protel, tb_cliente_procel, tb_cliente_protelref, 
                            tb_cliente_ema, tb_cliente_dir, tb_cliente_fecnac,
                            tb_cliente_estciv, tb_ubigeo_cod, tb_mediocom_id,
                            tb_creditotipo_id, tb_zona_id, tb_cliente_numpar, 
                            tb_cliente_bien, tb_cliente_firm, tb_cliente_usureg)
                    VALUES (1,
                            :cliente_tip, :cliente_emp, :cliente_ref,
                            :cliente_seg, :cliente_vis, :cliente_des,
                            :cliente_empruc, :cliente_emprs, :cliente_empdir,
                            :cliente_doc, :cliente_nom, :cliente_con,
                            :cliente_tel, :cliente_cel, :cliente_telref,
                            :cliente_protel, :cliente_procel, :cliente_protelref,
                            :cliente_ema, :cliente_dir, :cliente_fecnac,
                            :cliente_estciv, :ubigeo_cod, :mediocom_id,
                            :creditotipo_id, :zona_id, :cliente_numpar,
                            :cliente_bien, :cliente_firm, :cliente_usureg)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_tip", $this->cliente_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_emp", $this->cliente_emp, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_ref", $this->cliente_ref, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_seg", $this->cliente_seg, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_vis", $this->cliente_vis, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_des", $this->cliente_des, PDO::PARAM_INT);

        $sentencia->bindParam(":cliente_empruc", $this->cliente_empruc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_emprs", $this->cliente_emprs, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empdir", $this->cliente_empdir, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_doc", $this->cliente_doc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_nom", $this->cliente_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_con", $this->cliente_con, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_tel", $this->cliente_tel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_cel", $this->cliente_cel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_telref", $this->cliente_telref, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_protel", $this->cliente_protel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_procel", $this->cliente_procel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_protelref", $this->cliente_protelref, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_ema", $this->cliente_ema, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_dir", $this->cliente_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_fecnac", $this->cliente_fecnac, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_estciv", $this->cliente_estciv, PDO::PARAM_INT);

        $sentencia->bindParam(":ubigeo_cod", $this->ubigeo_cod, PDO::PARAM_STR);
        $sentencia->bindParam(":mediocom_id", $this->mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);

        $sentencia->bindParam(":zona_id", $this->zona_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_numpar", $this->cliente_numpar, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_bien", $this->cliente_bien, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_firm", $this->cliente_firm, PDO::PARAM_INT);

        $sentencia->bindParam(":cliente_usureg", $this->usuario_id, PDO::PARAM_INT);
        $result = $sentencia->execute();
        $cliente_id = $this->dblink->lastInsertId();
        
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['cliente_id'] = $cliente_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_cliente(
          :cliente_id,
          :cliente_tip, :cliente_emp, :cliente_ref,
          :cliente_seg, :cliente_vis, :cliente_des,
          :cliente_empruc, :cliente_emprs, :cliente_empdir,
          :cliente_doc, :cliente_nom, :cliente_con,
          :cliente_tel, :cliente_cel, :cliente_telref,
          :cliente_protel, :cliente_procel, :cliente_protelref,
          :cliente_ema, :cliente_dir, :cliente_fecnac,
          :cliente_estciv, :ubigeo_cod, :mediocom_id,
          :creditotipo_id, :zona_id, :cliente_numpar,
          :cliente_bien, :cliente_firm, :cliente_usumod
        )";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_tip", $this->cliente_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_emp", $this->cliente_emp, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_ref", $this->cliente_ref, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_seg", $this->cliente_seg, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_vis", $this->cliente_vis, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_des", $this->cliente_des, PDO::PARAM_INT);

        $sentencia->bindParam(":cliente_empruc", $this->cliente_empruc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_emprs", $this->cliente_emprs, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_empdir", $this->cliente_empdir, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_doc", $this->cliente_doc, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_nom", $this->cliente_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_con", $this->cliente_con, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_tel", $this->cliente_tel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_cel", $this->cliente_cel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_telref", $this->cliente_telref, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_protel", $this->cliente_protel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_procel", $this->cliente_procel, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_protelref", $this->cliente_protelref, PDO::PARAM_STR);

        $sentencia->bindParam(":cliente_ema", $this->cliente_ema, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_dir", $this->cliente_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_fecnac", $this->cliente_fecnac, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_estciv", $this->cliente_estciv, PDO::PARAM_INT);

        $sentencia->bindParam(":ubigeo_cod", $this->ubigeo_cod, PDO::PARAM_STR);
        $sentencia->bindParam(":mediocom_id", $this->mediocom_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);

        $sentencia->bindParam(":zona_id", $this->zona_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_numpar", $this->cliente_numpar, PDO::PARAM_STR);
        $sentencia->bindParam(":cliente_bien", $this->cliente_bien, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_firm", $this->cliente_firm, PDO::PARAM_INT);

        $sentencia->bindParam(":cliente_usumod", $this->usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cliente_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cliente WHERE tb_cliente_id =:cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function insertar_nota_cliente($cli_id, $usu_id, $cre_tip, $cre_subtip, $cre_id, $nota_det, $gc){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_notacliente(
                                            tb_cliente_id, 
                                            tb_usuario_id, 
                                            tb_creditotipo_id, 
                                            tb_subcredito_id, 
                                            tb_credito_id, 
                                            tb_notacliente_det, 
                                            tb_notacliente_gc) 
                                    VALUES (
                                            :tb_cliente_id, 
                                            :tb_usuario_id, 
                                            :tb_creditotipo_id, 
                                            :tb_subcredito_id, 
                                            :tb_credito_id, 
                                            :tb_notacliente_det, 
                                            :tb_notacliente_gc)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_subcredito_id", $cre_subtip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_notacliente_det", $nota_det, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_notacliente_gc", $gc, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cliente_id){
      try {
        $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_id =:cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_clientes(){
      try {
        $sql = "SELECT * FROM tb_cliente ORDER BY tb_cliente_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function verificar_documento_cliente($cliente_doc, $cliente_id){
      try {
        if(intval($cliente_id) > 0){
          //cliente está siendo editado
          $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_doc !='' AND tb_cliente_doc =:cliente_doc AND tb_cliente_id !=:cliente_id AND tb_cliente_xac = 1";
        }
        else{
          //cliente a registrar es nuevo
          $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_doc !='' AND tb_cliente_doc =:cliente_doc AND tb_cliente_xac = 1";
        }

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_doc", $cliente_doc, PDO::PARAM_STR);
        if(intval($cliente_id) > 0)
          $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function insertar_clientedetalle($cliente_id1, $cliente_id2, $clientedetalle_tip){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_clientedetalle(tb_cliente_id1, tb_cliente_id2, tb_clientedetalle_tip)
            VALUES(:cliente_id1, :cliente_id2, :clientedetalle_tip)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id2", $cliente_id2, PDO::PARAM_INT);
        $sentencia->bindParam(":clientedetalle_tip", $clientedetalle_tip, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar_sociedad_cliente($cliente_id1){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_clientedetalle WHERE tb_cliente_id1 =:cliente_id1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function verificar_sociedad_cliente($cliente_id1, $cliente_id2){
      try {
        $sql = "SELECT * FROM tb_clientedetalle where tb_cliente_id1 =:cliente_id1 and tb_cliente_id2 =:cliente_id2";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id2", $cliente_id2, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_sociedad_cliente($cliente_id1){
      try {
        $sql = "SELECT clidet.*,(case when tb_clientedetalle_tip = 1 then 'Sociedad Conyugal' when tb_clientedetalle_tip = 2 then 'Co-Propietario' else 'Apoderado' end) as tipo,tb_cliente_nom FROM tb_clientedetalle clidet inner join tb_cliente cli on cli.tb_cliente_id = clidet.tb_cliente_id2 where tb_cliente_id1 =:cliente_id1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id1", $cliente_id1, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function cliente_autocomplete($dato, $cliente_emp){
      try {

        $filtro = "%".$dato."%";
        $cliente_empresa = '';
        if(intval($cliente_emp) == 1)
          $cliente_empresa = 'AND tb_cliente_emp = 1';

        $sql = "SELECT * FROM tb_cliente WHERE tb_cliente_xac = 1 AND (tb_cliente_nom LIKE :filtro OR tb_cliente_doc LIKE :filtro) ".$cliente_empresa." GROUP BY CONCAT(tb_cliente_nom,'-',tb_cliente_emprs) LIMIT 0,12";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay clientes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function listar_nota_cliente_credito($cli_id, $cre_tip,$cre_subtip){
      try {
        $sql = "SELECT * FROM tb_notacliente WHERE tb_cliente_id =$cli_id AND tb_creditotipo_id =$cre_tip AND tb_subcredito_id IN($cre_subtip) AND tb_notacliente_mos = 1 AND tb_notacliente_gc = 0 ORDER BY tb_notacliente_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotapago_modid", $cuotapago_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
  }

?>
