<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../cliente/Cliente.class.php');
  $oCliente = new Cliente();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'cliente';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cliente_id = $_POST['cliente_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Cliente Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Cliente';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Cliente';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Cliente';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cliente
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cliente'; $modulo_id = $cliente_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cliente por su ID
    $cliente_tip = 0;

    if(intval($cliente_id) > 0){
      $result = $oCliente->mostrarUno($cliente_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el cliente seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $cliente_tip = intval($result['data']['tb_cliente_tip']);
          $cliente_emp = intval($result['data']['tb_cliente_emp']);
          $cliente_ref = intval($result['data']['tb_cliente_ref']);
          $cliente_seg = intval($result['data']['tb_cliente_seg']);
          $cliente_vis = intval($result['data']['tb_cliente_vis']);
          $cliente_des = intval($result['data']['tb_cliente_des']);

          $cliente_empruc = $result['data']['tb_cliente_empruc'];
          $cliente_emprs = $result['data']['tb_cliente_emprs'];
          $cliente_empdir = $result['data']['tb_cliente_empdir'];

          $cliente_doc = $result['data']['tb_cliente_doc'];
          $cliente_nom = $result['data']['tb_cliente_nom'];
          $cliente_con = $result['data']['tb_cliente_con'];
          $cliente_tel = $result['data']['tb_cliente_tel'];
          $cliente_cel = $result['data']['tb_cliente_cel'];
          $cliente_telref = $result['data']['tb_cliente_telref'];
          $cliente_protel = $result['data']['tb_cliente_protel'];
          $cliente_procel = $result['data']['tb_cliente_procel'];
          $cliente_protelref = $result['data']['tb_cliente_protelref'];

          $cliente_ema = $result['data']['tb_cliente_ema'];
          $cliente_dir = $result['data']['tb_cliente_dir'];
          $cliente_fecnac = mostrar_fecha($result['data']['tb_cliente_fecnac']);
          $cliente_estciv = $result['data']['tb_cliente_estciv'];

          $ubigeo_cod = $result['data']['tb_ubigeo_cod'];
          $mediocom_id = $result['data']['tb_mediocom_id'];
          $creditotipo_id = $result['data']['tb_creditotipo_id'];

          $zona_id = $result['data']['tb_zona_id'];
          $cliente_numpar = $result['data']['tb_cliente_numpar'];
          $cliente_bien = $result['data']['tb_cliente_bien'];
          $cliente_firm = $result['data']['tb_cliente_firm'];
          $cliente_sexo = intval($result['data']['tb_cliente_sexo']);

          $ubigeo_coddep = substr($ubigeo_cod, 0, 2);
          $ubigeo_codpro = substr($ubigeo_cod, 2, 2);
          $ubigeo_coddis = substr($ubigeo_cod, 4, 2);
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cliente" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cliente" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" id="cliente_vista" name="cliente_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <div class="row">
              <!-- GRUPO 1 DATOS DEL CLIENTE-->
              <div class="col-md-6">
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Datos del Cliente
                  </div>
                  <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="txt_cliente_tip" class="control-label">Persona</label>
                                <br>
                                <label class="radio-inline" style="padding-left: 0px;">
                                  <input type="radio" name="txt_cliente_tip" class="flat-green" value="1" <?php if($cliente_tip == 1 || $cliente_tip == 0) echo 'checked';?> > Natural
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="txt_cliente_tip" class="flat-green" value="2" <?php if($cliente_tip == 2) echo 'checked';?> > Jurídica
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label for="txt_cliente_sexo" class="control-label">Sexo</label>
                                <br>
                                <label class="radio-inline" style="padding-left: 0px;">
                                  <input type="radio" name="txt_cliente_sexo" class="flat-green" value="1" <?php if($cliente_sexo == 1) echo 'checked';?> > Masculino
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="txt_cliente_sexo" class="flat-green" value="2" <?php if($cliente_sexo == 2) echo 'checked';?> > Femenino
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="che_cliente_emp" class="control-label">Estados del Cliente</label>
                      <br>
                      <label class="checkbox-inline" style="padding-left: 0px;">
                        <input type="checkbox" name="che_cliente_emp" id="che_cliente_emp" value="1" class="flat-green" <?php if($cliente_emp==1) echo "checked";?> > Empresa
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="che_cliente_ref" id="che_cliente_ref" value="1" class="flat-green" <?php if($cliente_ref==1) echo "checked";?> > Referido
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="che_cliente_seg" id="che_cliente_seg" value="1" class="flat-green" <?php if($cliente_seg==1) echo "checked";?> > Seguimiento
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="che_cliente_vis" id="che_cliente_vis" value="1" class="flat-green" <?php if($cliente_vis==1) echo "checked";?> > Visitante
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="che_cliente_des" id="che_cliente_des" value="1" class="flat-green" <?php if($cliente_des==1) echo "checked";?> > Descartado
                      </label>
                    </div>
                    <!-- DATOS PARA PERSONA JURIDICA-->
                    <div class="form-group datos_juridica">
                      <label for="txt_cliente_doc" class="control-label">RUC de la Empresa</label>
                      <input type="text" name="txt_cliente_empruc" id="txt_cliente_empruc" class="form-control input-sm" value="<?php echo $cliente_empruc;?>">
                    </div>
                    <div class="form-group datos_juridica">
                      <label for="txt_cliente_doc" class="control-label">Razón Social</label>
                      <input type="text" name="txt_cliente_emprs" id="txt_cliente_emprs" class="form-control input-sm mayus" value="<?php echo $cliente_emprs;?>">
                    </div>
                    <?php 
                        if($vista=='creditogarveh'){
                             // ANTHONY *
                    ?>
                    <div class="form-group datos_juridica">
                      <label for="txt_cli_empact" class="control-label">Actividad de la Empresa</label>
                     <input type="text" name="txt_cli_empact" id="txt_cli_empact" class="form-control input-sm" value="<?php echo $cliente_empact;?>">
<!--                       <textarea class="form-control input-sm" name="txt_cli_empact" id="txt_cli_empact" rows="2" cols="70">Ninguna</textarea>-->
                    </div>
                     <p>
                    <div class="form-group datos_juridica">
                      <label for="cmb_cliente_empger" class="control-label">Tipo de Gerente</label>
                      <select name="cmb_cliente_empger" id="cmb_cliente_empger" class="form-control input-sm">
                        <option value="0"></option>
                        <option value="Gerente General" <?php if($cliente_empger == 'Gerente General') echo 'selected';?> >Gerente General</option>
                        <option value="Titular Gerente" <?php if($cliente_empger == 'Titular Gerente') echo 'selected';?> >Titular Gerente</option>
                      </select>
                    </div>
                    <p>
                    <div class=row">
                        <div class="form-group datos_juridica">
                         <div class="col-md-4">
                            <label for="cmb_ubigeo_coddep2" class="control-label">Departamento</label>
                            <select name="cmb_ubigeo_coddep2" id="cmb_ubigeo_coddep2" class="form-control input-sm">
                              <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_codpro2" class="control-label">Provincia</label>
                            <select name="cmb_ubigeo_codpro2" id="cmb_ubigeo_codpro2" class="form-control input-sm">
                              <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddis2" class="control-label">Distrito</label>
                            <select name="cmb_ubigeo_coddis2" id="cmb_ubigeo_coddis2" class="form-control input-sm">
                              <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                            </select>
                          </div>
                       </div>
                    </div>
                    <?php 
                        }
                    ?>
                    <div class="form-group datos_juridica">
                      <label for="txt_cli_empdir" class="control-label">Dirección de la Empresa</label>
                      <input type="text" name="txt_cliente_empdir" id="txt_cliente_empdir" class="form-control input-sm" value="<?php echo $cliente_empdir;?>">
                    </div>
                    <!-- FIN DATOS DE PERSONA JURIDICA-->
                    <div class="form-group">
                      <label for="txt_cliente_doc" class="control-label">DNI del Cliente</label>
                      <input type="text" name="txt_cliente_doc" id="txt_cliente_doc" class="form-control input-sm" value="<?php echo $cliente_doc;?>">
                    </div>
                    <div class="form-group">
                      <label for="txt_cliente_nom" class="control-label">Nombres del Cliente</label>
                      <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm mayus" value="<?php echo $cliente_nom;?>">
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="txt_cliente_tel" class="control-label">Teléfono</label>
                          <input type="text" name="txt_cliente_tel" id="txt_cliente_tel" class="form-control input-sm" value="<?php echo $cliente_tel;?>">
                        </div>
                        <div class="form-group">
                          <label for="txt_cliente_cel" class="control-label">Celular</label>
                          <input type="text" name="txt_cliente_cel" id="txt_cliente_cel" class="form-control input-sm" value="<?php echo $cliente_cel;?>">
                        </div>
                        <div class="form-group">
                          <label for="txt_cliente_telref" class="control-label">Telefono Referencia</label>
                          <input type="text" name="txt_cliente_telref" id="txt_cliente_telref" class="form-control input-sm" value="<?php echo $cliente_telref;?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="txt_cliente_protel" class="control-label">Propietario</label>
                          <input type="text" name="txt_cliente_protel" id="txt_cliente_protel" class="form-control input-sm" value="<?php echo $cliente_protel;?>">
                        </div>
                        <div class="form-group">
                          <label for="txt_cliente_procel" class="control-label">Propietario</label>
                          <input type="text" name="txt_cliente_procel" id="txt_cliente_procel" class="form-control input-sm" value="<?php echo $cliente_procel;?>">
                        </div>
                        <div class="form-group">
                          <label for="txt_cliente_protelref" class="control-label">Propietario</label>
                          <input type="text" name="txt_cliente_protelref" id="txt_cliente_protelref" class="form-control input-sm" value="<?php echo $cliente_protelref;?>">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="txt_cliente_ema" class="control-label">Email del Cliente</label>
                      <input type="text" name="txt_cliente_ema" id="txt_cliente_ema" class="form-control input-sm" value="<?php echo $cliente_ema;?>">
                    </div>
                    <div class="form-group">
                      <label for="txt_cliente_dir" class="control-label">Dirección del Cliente</label>
                      <input type="text" name="txt_cliente_dir" id="txt_cliente_dir" class="form-control input-sm" value="<?php echo $cliente_dir;?>">
                    </div>
                    <div class="form-group">
                      <label for="txt_cliente_fecnac" class="control-label">Fecha de Nacimiento</label>
                      <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" name="txt_cliente_fecnac" id="txt_cliente_fecnac" value="<?php echo $cliente_fecnac;?>"/>
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="cmb_cliente_estciv" class="control-label">Estado Civil</label>
                      <select name="cmb_cliente_estciv" id="cmb_cliente_estciv" class="form-control input-sm">
                        <option value="0"></option>
                        <option value="1" <?php if($cliente_estciv == 1) echo "selected";?> >Soltero</option>
                        <option value="2" <?php if($cliente_estciv == 2) echo "selected";?> >Casado</option>
                        <option value="3" <?php if($cliente_estciv == 3) echo "selected";?> >Divorciado</option>
                        <option value="4" <?php if($cliente_estciv == 4) echo "selected";?> >Viudo</option>
                      </select>
                    </div>
                     <div class=row">
                     <!--<div class=row"> // ANTHONY *-->
                        <div class="form-group datos_natural">
                         <div class="col-md-4">
                      <label for="cmb_ubigeo_coddep" class="control-label">Departamento</label>
                      <select name="cmb_ubigeo_coddep" id="cmb_ubigeo_coddep" class="form-control input-sm">
                        <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="cmb_ubigeo_codpro" class="control-label">Provincia</label>
                      <select name="cmb_ubigeo_codpro" id="cmb_ubigeo_codpro" class="form-control input-sm">
                        <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="cmb_ubigeo_coddis" class="control-label">Distrito</label>
                      <select name="cmb_ubigeo_coddis" id="cmb_ubigeo_coddis" class="form-control input-sm">
                        <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- GRUPO 2 DEL FORMULARIO-->
              <div class="col-md-6">
                <!-- MEDIO COMUNICACION E INTERESES-->
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Medios e Intereses
                  </div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="cmb_mediocom_id" class="control-label">Medio de Comununicación</label>
                      <select name="cmb_mediocom_id" id="cmb_mediocom_id" class="form-control input-sm">
                        <?php require_once('../mediocom/mediocom_select.php');?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="cmb_creditotipo_id" class="control-label">Crédito de Interés</label>
                      <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm">
                        <?php require_once('../creditotipo/creditotipo_select.php');?>
                      </select>
                    </div>
                  </div>
                </div>
                <br>
                <!-- SOCIEDAD DEL CLIENTE-->
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Sociedad del Cliente
                  </div>
                  <div class="panel-body">
                    <div class="row" id="group_casado" style="display: none;">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="cmb_cliente_bien" class="control-label">Tipo de Bienes</label>
                          <select name="cmb_cliente_bien" id="cmb_cliente_bien" class="form-control input-sm">
                            <option value="0"></option>
                            <option value="1" <?php if($cliente_bien == 1) echo "selected";?> >Separacion de Bienes</option>
                            <option value="2" <?php if($cliente_bien == 2) echo "selected";?> >Sin Separación de Bienes</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="cmb_cliente_firm" class="control-label">Forma de Firma</label>
                          <select name="cmb_cliente_firm" id="cmb_cliente_firm" class="form-control input-sm">
                            <option value="0"></option>
                            <option value="1" <?php if($cliente_firm == 1) echo "selected";?>>Individual</option>
                            <option value="2" <?php if($cliente_firm == 2) echo "selected";?>>Con Cónyuge</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="cmb_zona_id" class="control-label">Zona Registral</label>
                          <select name="cmb_zona_id" id="cmb_zona_id" class="form-control input-sm">
                            <?php require_once('../zona/zona_select.php');?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="txt_cliente_numpar" class="control-label">N° de Partida</label>
                          <input type="text" name="txt_cliente_numpar" id="txt_cliente_numpar" class="form-control input-sm mayus" value="<?php echo $cliente_numpar;?>">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="txt_cliente_fil_nom" class="control-label">Busque un Cliente</label>
                          <input type="text" name="txt_cliente_fil_nom" id="txt_cliente_fil_nom" class="form-control input-sm mayus">
                          <input type="hidden" id="txt_cliente_fil_id">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cmb_cliente_tipsoc" class="control-label">Sociedad</label>
                          <select name="cmb_cliente_tipsoc" id="cmb_cliente_tipsoc" class="form-control input-sm">
                            <option value="0"></option>
                            <option value="1">Sociedad Conyugal</option>
                            <option value="2">Co-Propietario</option>
                            <option value="3">Apoderado</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="cmb_ubigeo_coddep" class="control-label">Agregar</label>
                          <a href="" id="btn_sociedad_add" class="btn btn-info btn-sm">Add</a>
                        </div>
                      </div>
                    </div>
                    <div id="div_sociedad_list">
                      <?php
                        if($action == 'modificar'){
                          $result = $oCliente->listar_sociedad_cliente($cliente_id);
                            if($result['estado'] == 1){
                              foreach ($result['data'] as $key => $value) {
                                echo '
                                  <section class="row" style="margin-bottom: 10px;">
                                    <div class="col-md-6">
                                      <input type="text"  class="form-control input-sm mayus" value="'.$value['tb_cliente_nom'].'" disabled>
                                      <input type="hidden" name="txt_cliente_id2[]" value="'.$value['tb_cliente_id2'].'">
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label">'.$value['tipo'].'</label>
                                      <input type="hidden" name="txt_clientedetalle_tip[]" value="'.$value['tb_clientedetalle_tip'].'">
                                    </div>
                                    <div class="col-md-2">
                                      <a href="" class="btn btn-danger btn-sm btn_eliminar_soc">Eliminar</a>
                                    </div>
                                  </section>';
                              }
                            }
                          $result = NULL;
                        }
                      ?>
                    </div>
                  </div>
                </div>
                <br>
                <?php 
                    if($vista=='creditogarveh'){
                        // ANTHONY *
                ?>
                <div class="panel panel-info">
                  <div class="panel-heading">
                    Nuevas Direcciones
                  </div>
                  <div class="panel-body">
                      <div class=row">
                          <div class="form-group">
                         <div class="col-md-4">
                            <label for="cmb_ubigeo_coddep2" class="control-label">Departamento</label>
                            <select name="cmb_ubigeo_coddep2" id="cmb_ubigeo_coddep2" class="form-control input-sm">
                              <?php require('../ubigeo/ubigeo_departamento_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_codpro2" class="control-label">Provincia</label>
                            <select name="cmb_ubigeo_codpro2" id="cmb_ubigeo_codpro2" class="form-control input-sm">
                              <?php require('../ubigeo/ubigeo_provincia_select.php');?>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <label for="cmb_ubigeo_coddis2" class="control-label">Distrito</label>
                            <select name="cmb_ubigeo_coddis2" id="cmb_ubigeo_coddis2" class="form-control input-sm">
                              <?php require('../ubigeo/ubigeo_distrito_select.php');?>
                            </select>
                          </div>
                       </div>
                          </div>
                      <div class=row">
                        <div class=col-md-12">
                          <label for="txt_cli_dir2" class="control-label">Direccion Cobranza</label>
                          <div class="form-group">
                            <input type="text" id="txt_cli_dir2" name="txt_cli_dir2" class="form-control input-sm mayus" value="<?php echo $cli_direc_cobra ?>"/>
                          </div>
                        </div>
                     </div>
                      <div class=row">
                        <div class=col-md-12">
                          <label for="txt_cli_ref2" class="control-label">Referencia Cobranza</label>
                          <div class="form-group">
                            <textarea class="form-control input-sm" name="txt_cli_ref2" id="txt_cli_ref2" rows="2" cols="70">Ninguna</textarea>
                          </div>
                        </div>
                     </div>
                  </div>
                </div>
                <?php 
                    } 
                ?>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cliente?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cliente_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cliente">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cliente">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cliente/cliente_form.js?ver=1111';?>"></script>
