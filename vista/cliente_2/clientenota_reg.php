<?php
session_start();
require_once ("../../core/usuario_sesion.php");
require_once ("../cliente/Cliente.class.php");
require_once ("../usuario/Usuario.class.php");

$oCliente = new Cliente();
$oUsuario = new Usuario();

$hora = date('d-m-Y h:i:s a');
$cli_id = $_POST['hdd_cli_id'];
$cre_tip = $_POST['hdd_cre_tip']; //3 credito garveh, 4 credito hipo
$cre_subtip = $_POST['hdd_cre_subtip'];
$cre_id = $_POST['hdd_cre_id'];
$nota_det = $_POST['txt_clientenota_det'];
$che_mos = 1;//(empty($_POST['che_nota_mos']))? 0 : $_POST['che_nota_mos'];
$gc = 0; // SI SE REGISTRA DESDE AQUÍ, NO SE MUESTRA EN EL HISTORIAL DE GC DEL CLIENTE

$action = $_POST['action'];

if($action == 'insertar'){
	if(!empty($nota_det)){
        /*$result = $oCliente->mostrarUno($cliente_id);
        if($result['estado']==1){
            $nombre_cliente = $result['data']['tb_cliente_nom'];
        }
        $result1 = $oUsuario->mostrarUno($_SESSION['usuario_id']);
        if($result1['estado']==1){
            $nombre_usuario = $result1['data']['tb_usuario_nom'];
        }
        
        $nota = 'Nota GC para: <b>'.$nombre_cliente.':</b><b style="color: green;">'.$nombre_usuario.': '.$nota_det.' | '.$hora.'</b><br> | Nota de la cuota de fecha: <b>20-03-2022</b>, del Crédito: <b>CGV-0'.$cre_id.'</b>';
        */
	$oCliente->insertar_nota_cliente($cli_id, $_SESSION['usuario_id'], $cre_tip, $cre_subtip, $cre_id, $nota_det, $gc);
	//$oCliente->mostrar_notas_cliente($cli_id, $cre_tip, $cre_subtip, $che_mos);
	//el mostrar o ocultar se hace de manera manual
        // Nota GC para: <b>DIAZ VELA CARLOS ALBERTO:</b> <b style="color: green;">CARLOS: timbra, se insiste | 29-03-2022 09:43 am</b><br> 
        // | Nota de la cuota de fecha: <b>20-03-2022</b>, del Crédito: <b>CGV-0768</b>
	echo 'exito';}
}

if ($action == 'estado') {
	$nota_id = $_POST['nota_id'];
	$estado = $_POST['estado'];

	$oCliente->estado_nota_cliente($nota_id, $estado);
	echo 'exito';
}

?>