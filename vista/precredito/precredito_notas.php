<?php
require_once("../precredito/Precredito.class.php");
$oPcr = new Precredito();

$pcr_id = intval($_GET['pcr_id']);

$where[0]['column_name'] = 'tb_precred_id';
$where[0]['param0'] = $pcr_id;
$where[0]['datatype'] = 'INT';

$join[0]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
$join[0]['tipo_union'] = 'LEFT';
$join[0]['tabla_alias'] = 'tb_usuario us';
$join[0]['columna_enlace'] = 'pcr.tb_precred_usureg';
$join[0]['alias_columnaPK'] = 'us.tb_usuario_id';

$pcr_actual = $oPcr->listar_todos($where, $join, array())['data'][0];

$notas_anteriores = empty($pcr_actual['tb_precred_notas']) ? 'no hay notas anteriores' : $pcr_actual['tb_precred_notas'];

?>

<form action="" class="formName">
    <b>Notas anteriores</b>
    <p></p>
    <input type="hidden" name="hdd_precredito_reg" id="hdd_precredito_reg" value='<?php echo json_encode($pcr_actual);?>'>
    <?php echo $notas_anteriores; ?>
    <p></p>
    <br>

    <div class="form-group">
        <label>Ingrese nueva nota:</label>
        <textarea type="text" name="txt_nuevanota" id="txt_nuevanota" class="form-control input-sm input-shadow mayus"></textarea>
    </div>
</form>
