$(document).ready(function () {
	disabled($('#form_precredito').find($('.disabled')));

    $('#txt_precred_coment').blur(function(){
        $(this).val(removeAccents($(this).val()));
    });
    $('#div_creditos_anteriores').hide();

    $('#txt_precred_producto').blur(function(){
        $(this).val(removeAccents($(this).val()));
    });

	$('input[id*="txt_precli_"]').not('#txt_precli_numcel').autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                VISTA_URL + "precliente/precliente_autocomplete.php",
                { term: request.term }, //
                response
            );
        },
        select: function (event, ui) {
            $('#hdd_precli_id').val(ui.item.precliente_id);
            $('#hdd_cli_id').val(ui.item.cliente_id);
			$('#txt_precli_nrodoc').val(ui.item.precliente_numdoc);
			$('#txt_precli_nombres').val(ui.item.precliente_nombres);
			$('#txt_precli_numcel').val(ui.item.precliente_numcel);
            if (parseInt(ui.item.mediocom_id) > 0) {
                $('#cbo_mediocom_id').val(ui.item.mediocom_id).change();
            }
			event.preventDefault();
            $('#precliente_selected_mensaje').html('Usted ha elegido al cliente: ' + ui.item.value);
            $('#hdd_precli_reg').val(JSON.stringify(ui.item));
            $('#div_creditos_anteriores').show(100);
        }
    });
    
    $('#txt_precli_nrodoc, #txt_precli_nombres').keyup(function(e){
        if(e.keyCode === 8 && $(this).val().length == 0) {
            $('#hdd_precli_id').val('');
            $('#hdd_cli_id').val('');
            $('#txt_precli_nrodoc').val('');
            $('#txt_precli_nombres').val('');
            $('#txt_precli_numcel').val('');
            $('#cbo_mediocom_id').val(0).change();
            $('#precliente_selected_mensaje').html('');
            $('#hdd_precli_reg').val('');
            $('#div_creditos_anteriores').hide(100);
            $('#2da_parte').css('display', 'none');
        }
    });

    $('#txt_precli_nrodoc').blur(function(){
        if (!solo_numeros($(this).val())){
            alertas_error('VERIFIQUE', 'Documento deben ser solo números', 'small');
        }
    });
	
	$('#cbo_creditotipo_id').change(function(){
		var cred_tip = $(this).val();
		if (cred_tip > 0) {
			if ($('#action_precredito').val() == 'insertar' || $('#action_precredito').val() == 'modificar') {
                $('#cbo_articulo_id').prop('disabled', false);
            }
			completar_cbo_articulo(cred_tip);
            //MOSTRAR OCULTAR URL, DIV DE GALERIA
            if (parseInt(cred_tip) == 3) {
                $('#url_cred_menor').hide(100);
            } else {
                $('#url_cred_menor').show(100);
            }
		}
        cargar_estados_precred(cred_tip);
	});
    $('#cbo_precred_modoatencion').change(function(){
        ajustar_combos();
    });

    $('#cbo_articulo_id').change(function(){
        $('#hdd_articulo_id').val($(this).val());
        if (parseInt($(this).val()) > 0) {
            $('#cbo_articulo_id-error').css('display', 'none');
        } else {
            $('#cbo_articulo_id-error').show();
        }
    });

    $('.moneda').focus(function(){
		formato_moneda(this);
	});

    $('.interes').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0.00',
        vMax: '20.00'
    });

    $('#cbo_cgarvtipo_id option[value=0], #cbo_mediocom_id option[value=0]').text('Seleccione').css('font-weight', 'bold');

    $('#cbo_mediocom_id').change(function(){
        var hdd_precli  = $('#hdd_precli_id').val();
        var hdd_cli     = $('#hdd_cli_id').val();
        if (parseInt($(this).val()) > 0 && $('#txt_precli_nrodoc').val().length > 0 && hdd_precli === '' && hdd_cli === '') {
            $('#precliente_selected_mensaje').html('Usted está añadiendo un nuevo cliente');
            mostrar_2da_parte();
        }
    });

	$('#form_precredito').validate({
        submitHandler: function(event) {
            enviar_form_precred();
        },
        rules: {
            txt_precli_nrodoc: {
                required: true
            },
			txt_precli_nombres: {
				required: true
			},
            cbo_mediocom_id: {
                min: 1
            },
			cbo_creditotipo_id: {
				min: 1
			},
			cbo_articulo_id: {
				min: 1
			},
            cbo_cgarvtipo_id: {
                min: 1
            },
            txt_precred_producto: {
                required: true
            },
            txt_precred_monto: {
                required: true
            },
            txt_precred_tasacionurl: {
                required: true
            },
            txt_interes: {
                required: true
            },
            cbo_precred_modoatencion: {
                min: 1
            },
            cbo_inconclusomotivo_id: {
                min: 1
            },
            cbo_estadoprecred_id: {
                min: 1
            }
        },
        messages: {
            txt_precli_nrodoc: {
                required: 'N° documento*'
            },
			txt_precli_nombres: {
				required: 'Nombres Cliente*'
			},
            cbo_mediocom_id: {
                min: 'Elija medio comunicación*'
            },
			cbo_creditotipo_id: {
				min: 'Elija tipo crédito*'
			},
			cbo_articulo_id: {
				min: 'Elija Tipo garantía*'
			},
            cbo_cgarvtipo_id: {
                min: 'Elija tipo CGV*'
            },
            txt_precred_producto: {
                required: 'Registre producto*'
            },
            txt_precred_monto: {
                required: 'Monto*'
            },
            txt_precred_tasacionurl: {
                required: 'URL tasación*'
            },
            txt_interes: {
                required: 'Interés*'
            },
            cbo_precred_modoatencion: {
                min: 'Elija modo atención*'
            },
            cbo_inconclusomotivo_id: {
                min: 'Elija motivo no concluso*'
            },
            cbo_estadoprecred_id: {
                min: 'Elija estado*'
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    if ($('#action_precredito').val() == 'leer' || $('#action_precredito').val() == 'eliminar'){
        disabled($('#form_precredito').find("select, input, textarea"));
    } else {
        $('#cbo_inconclusomotivo_id option[value=5]').hide();
    }
    setTimeout(function(){
        completar_cbo_articulo($('#cbo_creditotipo_id option:selected').val())
    }, 150);

    $('#cbo_inconclusomotivo_id').change(function(){
        if($(this).val() === '6'){ //motivo inconcluso es 'otros'
            if ($('#txt_precred_coment').val().length > 0 && $('#txt_precred_coment').val().toLowerCase().indexOf('motivo') < 0) {
                if ($('#txt_precred_coment').val().charAt($('#txt_precred_coment').val().length - 1) !== '.'){
                    $('#txt_precred_coment').val($('#txt_precred_coment').val() + '. ');
                }
                $('#txt_precred_coment').val($('#txt_precred_coment').val() + 'MOTIVO CREDITO NO CONCLUIDO: ');
            } else if ($('#txt_precred_coment').val().length == 0){
                $('#txt_precred_coment').val('MOTIVO CREDITO NO CONCLUIDO: ');
            }
        }
    });

    $('#cbo_estadoprecred_id').change(function(){
        setTimeout(function () {
            var estado_id = parseInt($('#cbo_estadoprecred_id').val());
            if (estado_id == 5 || estado_id == 10) {
                $('#div_inconclusomotivo').css('display', 'inline');
            } else {
                $('#div_inconclusomotivo').css('display', 'none');
                $('#cbo_inconclusomotivo_id').val(0).change();
            }
        }, 50);
    });
    $('#cbo_creditotipo_id').change();
    $('#cbo_estadoprecred_id').change();

    //modificacion 19-10-23
    var concluido = parseInt($('#hdd_concluido').val());
    if (concluido == 1) {
        $.confirm({
            title: '¿Cambiar estado a concluido?',
            content: ``,
            type: 'green',
            escapeKey: 'close',
            backgroundDismiss: true,
            columnClass: 'medium',
            buttons: {
                confirmar: {
                    btnClass: 'btn-green',
                    action: function() {
                        $('#cbo_estadoprecred_id option:eq(4)').prop('selected', 'true').change();
                        $('#action_precredito').val('modificar');
                        enviar_form_precred();
                    }
                },
                cerrar: function () {
                }
            }
        });
    }
});

function completar_cbo_articulo(cretip_id) {
    if (cretip_id == 3) {
        $('#div_col_articulo').css('display', 'none');
        $('#div_col_cgarvtipo').css('display', 'inline');
        $('#cbo_articulo_id').val(-2).change();
    } else {
        $('#div_col_cgarvtipo').css('display', 'none');
        $('#div_col_articulo').css('display', 'inline');
        $('#cbo_cgarvtipo_id').val(0).change();
    }
	$.ajax({
        type: "POST",
        url: VISTA_URL + "articulo/articulo_select.php",
        async: true,
        dataType: "html",
        data: ({
            creditotipo_id: cretip_id,
            articulo_id: $('#hdd_articulo_id').val()
        }),
        beforeSend: function () {
            $('#cbo_articulo_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cbo_articulo_id').html(data);
            $('#cbo_articulo_id option[value=0]').remove();
            if (cretip_id == 3) {
                $('#cbo_articulo_id').val(74).change();
                $('#cbo_cgarvtipo_id').focus();
            }
			//aquí hacer el select2
			$('#cbo_articulo_id').select2({
				dropdownParent: $('#modal_precredito_form')
            });
            $($('#form_precredito').find('span.select2-selection')).addClass('input-shadow').css('height', '30px');
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function serializar_form_pcr() {
	var extra = ``;

    var elementos_disabled = $('#form_precredito').find('input:disabled, select:disabled, textarea:disabled').removeAttr('disabled');
    var form_serializado = $('#form_precredito').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

function ajustar_combos() {
    $('#cbo_estadoprecred_id option').show();
    var cred_tip = parseInt($('#cbo_creditotipo_id').val());
    var modo_atencion = parseInt($('#cbo_precred_modoatencion').val());
    var precred_est_orig = parseInt($('#hdd_estadoprecred_orig').val());
    precred_est_orig = isNaN(precred_est_orig) ? 0 : precred_est_orig;

    if (precred_est_orig == 0) {
        var precred_est_recomen;
        if (modo_atencion == 1) {
            precred_est_recomen = cred_tip == 1 ? 3 : 8;
            $('#cbo_estadoprecred_id option:eq(1), #cbo_estadoprecred_id option:eq(2)').css('display', 'none');
            if (cred_tip == 3)
                $('#cbo_estadoprecred_id option:eq(4)').css('display', 'none');
        } else {
            precred_est_recomen = cred_tip == 1 ? 1 : 6;
            $('#cbo_estadoprecred_id option:eq(2), #cbo_estadoprecred_id option:eq(3), #cbo_estadoprecred_id option:eq(4)').css('display', 'none');
        }
        $('#cbo_estadoprecred_id').val(precred_est_recomen).change();
    } else {
        if (precred_est_orig == 6) {
            $('#cbo_estadoprecred_id option:eq(4)').css('display', 'none');
        } else if (precred_est_orig == 2 || precred_est_orig == 7) {
            $('#cbo_estadoprecred_id option:eq(1)').css('display', 'none');
            if (cred_tip == 3)
                $('#cbo_estadoprecred_id option:eq(4)').css('display', 'none');
        } else if (precred_est_orig == 3 || precred_est_orig == 8) {
            $('#cbo_estadoprecred_id option:eq(1), #cbo_estadoprecred_id option:eq(2)').css('display', 'none');
        } else if (precred_est_orig == 4 || precred_est_orig == 9) {
            $('#cbo_estadoprecred_id option:eq(1), #cbo_estadoprecred_id option:eq(2), #cbo_estadoprecred_id option:eq(3), #cbo_estadoprecred_id option:eq(5)').css('display', 'none');
        } else if (precred_est_orig == 5 || precred_est_orig == 10){
            $('#cbo_estadoprecred_id option:eq(1)').css('display', 'none');
            if (cred_tip == 3)
                $('#cbo_estadoprecred_id option:eq(4)').css('display', 'none');
        }
    }
}

function cargar_estados_precred(cred_tip) {
    var estado_original = $('#hdd_estadoprecred_orig').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "precredito/estadoprecred_select.php",
        async: true,
        dataType: "html",
        data: ({
            tipocredito_id: cred_tip,
            estadoprecred_id: estado_original
        }),
        beforeSend: function () {
            $('#cbo_estadoprecred_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cbo_estadoprecred_id').html(data);
            ajustar_combos();
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            close: function () {
            }
        }
    });
}

function creditos_anteriores(){
    var precli_id = $('#hdd_precli_id').val();
    var cli_id = $('#hdd_cli_id').val();
    var action = $('#action_precredito').val();
    var pcr_id =$('#hdd_precredito_id').val();
    
    $.ajax({
        type: "POST",
        url: VISTA_URL + "precliente/precliente_creditosanteriores.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            precli_id: precli_id,
            cli_id: cli_id,
            pcr_id: pcr_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_creditosanteriores_form').html(html);
            $('#modal_creditosanteriores_form').modal('show');
            $('#modal_mensaje').modal('hide');

            modal_width_auto('modal_creditosanteriores_form', 42);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_creditosanteriores_form'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_creditosanteriores_form', 'limpiar');
        }
    });
}

function garantia_form(usuario_act, garantia_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantia/garantia_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            garantia_id: garantia_id,
            vista: 'garantia'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_garantia_form').html(data);
                $('#modal_registro_garantia').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_garantia'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_garantia'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_garantia', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else {
                //llamar al formulario de solicitar permiso
                var modulo = 'garantia';
                var div = 'div_modal_garantia_form';
                permiso_solicitud(usuario_act, garantia_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

function enviar_form_precred() {
    if (!solo_numeros($('#txt_precli_nrodoc').val())){
        alertas_error('VERIFIQUE', 'Documento deben ser solo números', 'small');
        return;
    }
    
    $.ajax({
        type: "POST",
        url: VISTA_URL + "precredito/precredito_controller.php",
        async: true,
        dataType: "json",
        data: serializar_form_pcr(),
        beforeSend: function () {
            $('#precredito_mensaje').show(400);
            $('#btn_guardar_precredito').prop('disabled', true);
        },
        success: function (data) {
            if (parseInt(data.estado) > 0) {
                $('#precredito_mensaje').removeClass('callout-info').addClass('callout-success');
                $('#precredito_mensaje').html(`<h4>${data.mensaje}</h4>`);
                if ($.inArray($('#action_precredito').val(), ['insertar', 'modificar']) > -1) {
                    disabled($('#form_precredito').find("button[class*='btn-sm'], select, input, textarea"));
                }

                setTimeout(function () {
                    $('#modal_precredito_form').modal('hide');
                    if (data.estado == 1) {
                        precredito_contenido();
                    }
                    if (data.form_cm == 1) {
                        var pcr_id = data.pcr_id;
                        creditomenor_form('I', pcr_id, 0);
                    }
                    if (data.form_garveh == 1) {
                        //var pcr_id = data.pcr_id;
                        //creditomenor_form('I', pcr_id, 0);
                        window.location='./proceso';
                        generarProceso(data.creditotipo_id, data.cgarvtipo_id, data.precliente_id, data.credito_matriz_id, data.precred_id);
                    }
                }, 2800);
            }
            else {
                console.log(data);
                $('#precredito_mensaje').removeClass('callout-info').addClass('callout-warning');
                $('#precredito_mensaje').html('<h4>Alerta: ' + data.mensaje +'<h4>');
                $('#btn_guardar_precredito').prop('disabled', false);
            }
        },
        complete: function(data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data);
            $('#precredito_mensaje').removeClass('callout-info').addClass('callout-danger')
            $('#precredito_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
        }
    });
}

function mostrar_2da_parte() {
    $('#2da_parte').css('display', 'inline');
    setTimeout(function(){
        completar_cbo_articulo($('#cbo_creditotipo_id option:selected').val());
    }, 450);
    $('#btn_guardar_precredito').css('display', 'inline');
}

function creditomenor_pagos(creditomenor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditomenor/creditomenor_pagos.php",
        async: true,
        dataType: "html",
        data: ({
            credito_id: creditomenor_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_creditomenor_pagos').html(data);
                $('#modal_creditomenor_pagos').modal('show');

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_creditomenor_pagos', 95);
                modal_height_auto('modal_creditomenor_pagos'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_creditomenor_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

/* GERSON (21-11-23) */
function generarProceso(creditotipo_id, cgarvtipo_id, precliente_id, credito_matriz_id, precred_id) {

    if(creditotipo_id=='0' || creditotipo_id==''){
      Swal.fire(
        '¡Aviso!',
        'Seleccione un tipo de crédito válido.',
        'warning'
      );
    }else{
      if(creditotipo_id==1){ // CREDITO MENOR 
        // aún no hay proceso
        Swal.fire(
          '¡Aviso!',
          'Se está trabajando en los procesos.',
          'info'
        );
      }else if(creditotipo_id==2){ // CRÉDITO ASCVEH
        // aún no hay proceso
        Swal.fire(
          '¡Aviso!',
          'Se está trabajando en los procesos.',
          'info'
        );
      }else if(creditotipo_id==3){ // CRÉDITO GARVEH
  
        if(cgarvtipo_id=='0' || cgarvtipo_id==''){
          Swal.fire(
            '¡Aviso!',
            'Seleccione un tipo de garantía válido.',
            'warning'
          );
        }else{
          $.ajax({
            type: "POST",
            url: VISTA_URL + "proceso/proceso_controller.php",
            async: false,
            dataType: "json",
            data: ({
                action: 'insertar',
                creditotipo_id: creditotipo_id, 
                cgarvtipo_id: cgarvtipo_id,
                precliente_id: precliente_id,
                credito_matriz_id: credito_matriz_id,
                precred_id: precred_id
            }),
              beforeSend: function () {
              },
              success: function (data) {
                Swal.fire(
                  'Excelente!',
                  data.mensaje,
                  'success'
                );
                verFaseProceso('R', data.proceso_id); // R=Register
              },
              complete: function (data) {
              }
          });
        }
        
      }else if(creditotipo_id==4){ // CREDITO HIPOTECARIO
        // aún no hay proceso
        Swal.fire(
          '¡Aviso!',
          'Se está trabajando en los procesos.',
          'info'
        );
      }else if(creditotipo_id==5){ // ADENDAS
        // aún no hay proceso
        Swal.fire(
          '¡Aviso!',
          'Se está trabajando en los procesos.',
          'info'
        );
      }
      
    }
    
}

function verFaseProceso(action, proceso_id, usuario_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_vista_fase.php",
        async: true,
        dataType: "html",
        data: ({
          proceso_id: proceso_id,
          usuario_id: usuario_id
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_proceso_fase').html(html);
        },
        complete: function (html) {
        verFases();
        detalleParticipante();
  
        }
    });
}

// uso para volver a la lista de procesos
function verFases() {
    $("#vista_fase").removeClass("hidden");
    $("#vista_nuevo").addClass("hidden");
    $("#vista_main").addClass("hidden");
    //$('#div_proceso_tabla').html('');
}

function detalleParticipante(){
  
	$.ajax({
		type: "POST",
		url: VISTA_URL + "proceso/proceso_controller.php",
		async:true,
		dataType: "html",                      
		data: ({
		  action: 'detalle_participante_cliente',
		  proceso_id: parseInt($('#hdd_participante_proceso_id').val()),
		}),
		beforeSend: function() {
		},
		success: function(html){

			$('#listaParticipante').html(html);  
			
		}
	});
  
}

/*  */
/* function abrir_proceso_garveh(creditotipo, cgarvtipo, precliente_id) {
    window.location='./proceso';
    generarProceso(creditotipo, cgarvtipo, precliente_id);
} */

/* gerson (13-03-24) */
function detectarAdenda(garvtipo_id) {
    id = parseInt(garvtipo_id);
    precli_id = $('#hdd_precli_id').val();
    cli_id = $('#hdd_cli_id').val();

    if(id==6){
        $('#div_col_credito').removeClass('hidden');
        $('#div_col_tipoadenda').removeClass('hidden');
        completar_cbo_credito_matriz(precli_id, cli_id);
    }else{
        $('#div_col_credito').addClass('hidden');
        $('#div_col_tipoadenda').addClass('hidden');
    }
}

function completar_cbo_credito_matriz(precli_id, cli_id) {
	$.ajax({
        type: "POST",
        url: VISTA_URL + "precredito/precredito_controller.php",
        async: true,
        dataType: "html",
        data: ({
            action_precredito: 'combo_credito_matriz',
            precli_id: precli_id,
            cli_id: cli_id
        }),
        success: function (data) {
            $('#div_col_credito').html(data);
            /* $('#cbo_articulo_id option[value=0]').remove();
            if (cretip_id == 3) {
                $('#cbo_articulo_id').val(74).change();
                $('#cbo_cgarvtipo_id').focus();
            } */
			/* $('#cbo_articulo_id').select2({
				dropdownParent: $('#modal_precredito_form')
            });
            $($('#form_precredito').find('span.select2-selection')).addClass('input-shadow').css('height', '30px'); */
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}
/*  */
