<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Precredito extends Conexion{
    public $precred_id;
    public $precliente_id;
    public $empresa_id;
    public $articulo_id;
    public $creditotipo_id;
    public $cgarvtipo_id;
    public $credito_id;
    public $estadoprecred_id;
    public $precred_modoatencion;
    public $precred_producto;
    public $moneda_id;
    public $precred_monto;
    public $inconclusomotivo_id;
    public $precred_coment;
    public $precred_notas;
    public $precred_fecreg;
    public $precred_usureg;
    public $precred_fecmod;
    public $precred_usumod;
    public $precred_xac;
    public $precred_tipo_adenda;
    public $creditomatriz_id;
    public $proceso_id;
    public $precred_int;
    public $precred_urltasacion;

    public function listar_todos($where, $join, $otros){
        try {
            $sql = "SELECT pcr.*";
            if (!empty($join)) {
                for ($j = 0; $j < count($join); $j++) {
                    $sql .= ",\n" . $join[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_precred pcr\n";
            if (!empty($join)) {
                for ($k = 0; $k < count($join); $k++) {
                    $sql .= $join[$k]['tipo_union'] . " JOIN " . $join[$k]['tabla_alias'] . " ON ";
                    if (!empty($join[$k]['columna_enlace']) && !empty($join[$k]['alias_columnaPK'])) {
                        $sql .= $join[$k]['columna_enlace'] . " = " . $join[$k]['alias_columnaPK'] . "\n";
                    } else {
                        $sql .= $join[$k]['on'] . "\n";
                    }
                }
            }
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i])) {
                    $sql .= ($i > 0) ? "\n{$where[$i]["conector"]} " : "WHERE ";

                    if (!empty($where[$i]["column_name"])) {
                        if (stripos($where[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$where[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$where[$i]["column_name"]}";
                        }

                        if (!empty($where[$i]["datatype"])) {
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " " . $where[$i]["param$i"];
                        }
                    } else {
                        $sql .= $where[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros["group"])) {
                $sql .= "\nGROUP BY {$otros["group"]["column_name"]}";
            }
            if (!empty($otros["orden"])) {
                $sql .= "\nORDER BY {$otros["orden"]["column_name"]} {$otros["orden"]["value"]}";
            }
            if (!empty($otros["limit"])) {
                $sql .= "\nLIMIT {$otros["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i]["column_name"]) && !empty($where[$i]["datatype"])) {
                    $_PARAM = strtoupper($where[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $where[$i]["param$i"], $_PARAM);
                }
            }
            $retorno["sql"] = $sql;
            $sentencia->execute();
            $retorno["row_count"] = $sentencia->rowCount();
            
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } 
            else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $column_opc = $this->creditotipo_id == 3 ? ",\ncgarvtipo_id" : '';
            $param_opc = $this->creditotipo_id == 3 ? ",\n:param_opc1" : '';
            $column_opc .= in_array($this->estadoprecred_id, array(5, 10)) ? ",\ntb_inconclusomotivo_id" : '';
            $param_opc .= in_array($this->estadoprecred_id, array(5, 10)) ? ",\n:param_opc2" : '';
            $estado_precred_orig = $this->creditotipo_id == 1 ? 1 : 6;
            $column_opc .= !empty($this->precred_urltasacion) ? ",\ntb_precred_urltasacion" : '';
            $param_opc .= !empty($this->precred_urltasacion) ? ",\n:param_opc3" : '';

            $sql = "INSERT INTO tb_precred (
                tb_precliente_id,
                tb_empresa_id,
                tb_articulo_id,
                tb_creditotipo_id,
                tb_estadoprecred_id,
                tb_precred_modoatencion,
                tb_precred_producto,
                tb_moneda_id,
                tb_precred_monto,
                tb_precred_coment,
                tb_precred_int,
                tb_precred_usureg,
                tb_precred_usumod $column_opc)
              VALUES (
                :param0,
                :param1,
                :param2,
                :param3,
                $estado_precred_orig,
                :param5,
                :param6,
                :param7,
                :param8,
                :param9,
                :param10,
                :param11,
                :param12 $param_opc);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->precliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param1", $this->empresa_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param2", $this->articulo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param3", $this->creditotipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param5", $this->precred_modoatencion, PDO::PARAM_INT);
            $sentencia->bindParam(":param6", $this->precred_producto, PDO::PARAM_STR);
            $sentencia->bindParam(":param7", $this->moneda_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param8", $this->precred_monto, PDO::PARAM_STR);
            $sentencia->bindParam(":param9", $this->precred_coment, PDO::PARAM_STR);
            $sentencia->bindParam(":param10", $this->precred_int, PDO::PARAM_STR);
            $sentencia->bindParam(":param11", $this->precred_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param12", $this->precred_usumod, PDO::PARAM_INT);
            if ($this->creditotipo_id == 3) {
                $sentencia->bindParam(":param_opc1", $this->cgarvtipo_id, PDO::PARAM_INT);
            }
            if (in_array($this->estadoprecred_id, array(5, 10))) {
                $sentencia->bindParam(':param_opc2', $this->inconclusomotivo_id, PDO::PARAM_INT);
            }
            if (!empty($this->precred_urltasacion)) {
                $sentencia->bindParam(':param_opc3', $this->precred_urltasacion, PDO::PARAM_STR);
            }

            $resultado['estado'] = $sentencia->execute();
            $resultado['sql'] = $sql;

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Solicitud pre-credito registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    public function modificar(){
        $this->dblink->beginTransaction();
        try {
            $modif_opc = $this->creditotipo_id == 3 ? ",\ncgarvtipo_id = :param_opc1" : '';
            $modif_opc .= in_array($this->estadoprecred_id, array(5, 10)) ? ",\ntb_inconclusomotivo_id = :param_opc2" : '';
            $modif_opc .= !empty($this->precred_urltasacion) ? ",\ntb_precred_urltasacion = :param_opc3" : '';

            $sql = "UPDATE tb_precred
              SET
                tb_precliente_id = :param0,
                tb_estadoprecred_id = :param1,
                tb_articulo_id = :param2,
                tb_precred_producto = :param3,
                tb_precred_monto = :param4,
                tb_precred_coment = :param5,
                tb_moneda_id = :param6,
                tb_precred_int = :param7 $modif_opc
              WHERE
                tb_precred_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->precliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->estadoprecred_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->articulo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param3', $this->precred_producto, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->precred_monto, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->precred_coment, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->moneda_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param7', $this->precred_int, PDO::PARAM_STR);
            $sentencia->bindParam(':paramx', $this->precred_id, PDO::PARAM_INT);
            if ($this->creditotipo_id == 3) {
                $sentencia->bindParam(':param_opc1', $this->cgarvtipo_id, PDO::PARAM_INT);
            }
            if (in_array($this->estadoprecred_id, array(5, 10))) {
                $sentencia->bindParam(':param_opc2', $this->inconclusomotivo_id, PDO::PARAM_INT);
            }
            if (!empty($this->precred_urltasacion)) {
                $sentencia->bindParam(':param_opc3', $this->precred_urltasacion, PDO::PARAM_STR);
            }

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_campo($precred_id, $precred_columna, $precred_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($precred_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_precred SET " . $precred_columna . " = :precred_valor, tb_precred_usumod = {$_SESSION['usuario_id']} WHERE tb_precred_id = :precred_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":precred_id", $precred_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":precred_valor", $precred_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":precred_valor", $precred_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_estadosprecred($nombre_columna, $valor, $tipo_dato) {
        try {
            if (!empty($valor)) {
                $sql = "SELECT * FROM tb_estadoprecred WHERE $nombre_columna = :param0 ORDER BY tb_estadoprecred_orden ASC;";

                $sentencia = $this->dblink->prepare($sql);
                if ($tipo_dato == 'INT')
                    $sentencia->bindParam(':param0', $valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(':param0', $valor, PDO::PARAM_STR);
                $sentencia->execute();
                
                if ($sentencia->rowCount() > 0) {
                    $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                    $retorno["estado"] = 1;
                    $retorno["mensaje"] = "exito";
                    $retorno["data"] = $resultado;
                    $sentencia->closeCursor(); //para libera memoria de la consulta
                } else {
                    $retorno["estado"] = 0;
                    $retorno["mensaje"] = "No hay registros";
                    $retorno["data"] = "";
                }
                return $retorno;
            }
        } catch (Exception $th) {
            throw $th;
        }
    }

    /* GERSON (13-01-24) */
    function obtener_precredito_activo_x_cliente($cliente_id, $tipo_credito) {
        try {
            $sql = "SELECT pcre.*, pcli.tb_cliente_id
                FROM tb_precred pcre
                INNER JOIN tb_precliente pcli ON pcre.tb_precliente_id = pcli.tb_precliente_id
                WHERE pcre.tb_precred_xac = 1
                AND pcli.tb_cliente_id = :cliente_id 
                AND pcre.tb_creditotipo_id = :tipo_credito;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':tipo_credito', $tipo_credito, PDO::PARAM_INT);
            $sentencia->execute();
            
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;

        } catch (Exception $th) {
            throw $th;
        }
    }

    function obtener_precredito_activo_x_precliente($precliente_id, $tipo_credito) {
        try {
            $sql = "SELECT *
                FROM tb_precred
                WHERE tb_precred_xac = 1
                AND tb_precliente_id = :precliente_id 
                AND tb_creditotipo_id = :tipo_credito;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':precliente_id', $precliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':tipo_credito', $tipo_credito, PDO::PARAM_INT);
            $sentencia->execute();
            
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;

        } catch (Exception $th) {
            throw $th;
        }
    }
    /*  */

    /* gerson (13-03-24) */
    function obtener_credito_x_cliente($cliente_id) {
        try {
            $sql = "SELECT *
                FROM tb_creditogarveh cre
                LEFT JOIN tb_moneda mo on mo.tb_moneda_id = cre.tb_moneda_id 
                LEFT JOIN tb_vehiculomarca vm on cre.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
                LEFT JOIN tb_vehiculomodelo vmo on cre.tb_vehiculomodelo_id = vmo.tb_vehiculomodelo_id
                WHERE cre.tb_credito_xac = 1
                AND cre.tb_cliente_id = :cliente_id
                AND cre.tb_credito_tip IN(1,3,4)
                ORDER BY cre.tb_credito_fecdes DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();
            
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;

        } catch (Exception $th) {
            throw $th;
        }
    }

    /* gerson (15-06-24) */
    function mostrarUno($precred_id) {
        try {
            $sql = "SELECT * FROM tb_precred WHERE tb_precred_id =:precred_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":precred_id", $precred_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos del Pre Crédito";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */

    /* ANTHONY */
    function mostrarPreCreXProceso($proceso_id) {
        try {
            $sql = "SELECT * FROM tb_precred WHERE tb_proceso_id =:proceso_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proceso_id", $proceso_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontró datos del Pre Crédito";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */
}
?>
