<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Créditos</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<label>Filtros</label>
						<div class="panel panel-danger" id="div_filtros_precredito">
							<div class="panel-body">
								<?php include VISTA_URL . 'precredito/precredito_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="div_contenido" class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="precredito_mensaje_contenido" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>


				<div id="filtro_fases" style="display: none;">
					<h4 style="padding-top: 0px; margin-top: 0px; padding-bottom:2px; margin-bottom:2px;">VISTA DE FASES</h4>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6"><b>- Filtro de tiempo aplicable solo a estado "CONCLUIDO" y "RECHAZADO"</b></div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-3" style="width: 20%;"><b>- Fase rechazado visible:</b></div>
										<div class="col-md-2 checkbox" style="padding: 0px; margin: 0px; width: 10%;">
											<input type="checkbox" style="margin: 1.5px 0px 0px -12px; " id="che_fase_rechazado" name="che_fase_rechazado" value="1" checked="true">&nbsp;<b> RECHAZADO</b>
										</div>
									</div>
								</div>
							</div>
							<?php if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) { ?>
								<div class="row">
									<div class="col-md-7">
										<b>- <a onclick="reasignar_grupo()" style="cursor: pointer; font-size: 13.5px;">Click aquí para reasignar el grupo de precréditos seleccionados a otro asesor</a></b>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>

				<div id="contenido">
				</div>
			</div>
			<div id="div_modal_precredito_form"></div>
			<div id="div_tasacionsug_form"></div>
			<div id="div_modal_garantia_resumen"></div>
			<div id="div_modal_creditosanteriores_form"></div>
			<div id="div_modal_creditomenor_form"></div>
			<div id="div_modal_garantia_form"></div>
			<div id="div_modal_creditomenor_pagos"></div>
			<div id="div_modal_upload_form"></div>
			<div id="div_modal_carousel"></div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
