var datatable_global_pcr;
var vista_box = parseInt($('#hdd_vista').val());

$(document)
.ready(function () {
    $('#click').click();
    $('#cbo_fil_asesor option[value=0]').text('Seleccione asesor').change();

    if (vista_box == 1) {
        $('#btn_cambiar_vista').html('<i class="fa fa-eye icon"></i> Ver fases');
    } else {
        //ocultar los demás options
        var user_actual = $('#hdd_usuario_id').val();
        $(`#cbo_fil_asesor option[value!='${user_actual}']`).hide();
        $('#btn_cambiar_vista').html('<i class="fa fa-eye icon"></i> Ver lista');
    }
    $('.select2').select2({
        dropdownParent: $('#form_precredito_filtro')
    });
    $('#btn_buscar').click();

    $('#btn_cambiar_vista').on('click', function(){
        if (vista_box == 1) {
            vista_box = 2;
            $('#btn_cambiar_vista').html('<i class="fa fa-eye icon"></i> Ver lista');
        } else {
            vista_box = 1;
            $('#btn_cambiar_vista').html('<i class="fa fa-eye icon"></i> Ver fases');
        }
        $('#btn_buscar').click();
    });

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });

    $('#cbo_fil_cretip option:eq(2), #cbo_fil_cretip option:eq(4), #cbo_fil_cretip option:eq(5)').css('display', 'none');
    $('#cbo_fil_cretip option[value=0]').text('Todos');

    $("#txt_fil_precliente_nom")
    .autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                VISTA_URL + "precliente/precliente_autocomplete.php",
                { term: request.term },
                response
            );
        },
        select: function (event, ui) {
            event.preventDefault();
            $("#hdd_fil_precliente_id").val(ui.item.precliente_id);
            $('#txt_fil_precliente_nom').val(ui.item.precliente_nombres);
            precredito_contenido();
        }
    })
    .keyup(function () {
        if ($('#txt_fil_precliente_nom').val() === '') {
            $('#hdd_fil_precliente_id').val('');
            precredito_contenido();
        }
    });

    $('#cbo_fil_sede, #cbo_fil_cretip').on('change', function(){
        precredito_contenido();
    });

    $("#che_fase_rechazado").change(function () {
        if (this.checked) {
            $('div[id*="fase_"]').not('#fase_rec').css('width', '20%');

            $("#fase_rec").show(100);
        }
        else {
            $('div[id*="fase_"]').not('#fase_rec').css('width', '25%');

            $("#fase_rec").hide(100);
        }
    });
})
.on('focus', '.select2.select2-container', function (e) {
    var isOriginalEvent = e.originalEvent // don't re-open on closing focus event

    if (isOriginalEvent) {
        $(this).siblings('select:enabled').select2('open');
    }
});

function precredito_contenido() {
    var destino = 'precredito/precredito_tabla.php';
    if (vista_box == 2) {
        destino = 'precredito/precredito_fases.php';
    }
    $.ajax({ 
        type: "POST",
        url: VISTA_URL + destino,
        async: true,
        dataType: "html",
        data: $('#form_precredito_filtro').serialize(),
        beforeSend: function () {
            $('#precredito_mensaje_contenido').show(300);
        },
        success: function (data) {
            $('#contenido').html(data);
            $('#precredito_mensaje_contenido').hide(300);

            if (vista_box == 1) {
                estilos_datatable_precredito();
                $('#filtro_fases').hide(50);
            } else {
                $('#filtro_fases').show(50);
                $("#che_fase_rechazado").prop('checked', 'true').change();

                checkbox_reasignar();
            }
        },
        error: function (data) {
            $('#precredito_mensaje_contenido').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

function estilos_datatable_precredito() {
    datatable_global_pcr = $('#tbl_precreditos').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [4, 5, 6, 7, 8, 9], orderable: false}
        ]
    });
    datatable_texto_filtrar_precredito();
}

function datatable_texto_filtrar_precredito() {
    $('input[aria-controls*="tbl_precreditos"]')
    .attr('id', 'txt_datatable_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global_pcr.search(text_fil).draw();
    }
};

function precredito_form(action, precred_id, concluido){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "precredito/precredito_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            precred_id: precred_id,
            concluido: concluido
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_precredito_form').html(html);
            $('#modal_precredito_form').modal('show');
            $('#modal_mensaje').modal('hide');

            modal_width_auto('modal_precredito_form', 45);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_precredito_form'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_precredito_form', 'limpiar');
        }
    });
}

function formato_num(obj){
    $(obj).autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '99999999'
    });
}

function eliminar(precred_id) {
    $.confirm({
        title: '¡Cuidado!',
        content: `<b>¿Desea eliminar esta solicitud de precrédito?</b>`,
        type: 'red',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            Confirmar: {
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'precredito/precredito_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action_precredito: 'modificar_campo',
                            columna: 'tb_precred_xac',
                            hdd_precredito_id: precred_id,
                            valor: 0,
                            tipo_dato: 'INT'
                        }),
                        success: function (data) {
                            $.alert(data.mensaje);
                            if (parseInt(data.estado) == 1) {
                                eliminarProceso(data.proceso_id);
                                precredito_contenido();
                            }
                        },
                    });
                }
            },
            cancelar: function () {
            }
        }
    });
}

function creditomenor_form(usuario_act, precredito_id, creditomenor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditomenor/creditomenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditomenor_id: 0,
            vista: 'precredito',
            pcr_id: precredito_id,
            creditomenor_id: creditomenor_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_creditomenor_form').html(data);
                $('#modal_registro_creditomenor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_registro_creditomenor', 95);
                modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else {
                //llamar al formulario de solicitar permiso
                var modulo = 'creditomenor';
                var div = 'div_modal_creditomenor_form';
                permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        error: function (data) {
            alerta_error('Error', data.responseText)
            console.log(data);
        }
    });
}

function tasacionsug_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "tasacionsug/tasacionsug_form.php",
        async: true,
        dataType: "html",
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_tasacionsug_form').html(html);
            $('#modal_tasacionsug_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_width_auto('modal_tasacionsug_form', 60);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_tasacionsug_form'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_tasacionsug_form', 'limpiar');
        }
    });
}

function reasignar(pcr_id) {
    $.confirm({
        title: 'Cambiar asesor',
        content: 'url:' + VISTA_URL + `precredito/precredito_cambioasesor.php?pcr_id=${pcr_id}`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    var asesor_n = this.$content.find('.asesor_n').val();
                    var asesor_n_nombre = this.$content.find('.asesor_n option:selected').text();
                    var asesor_a = this.$content.find('#hdd_asesor_a').val();
                    var pcr_reg = this.$content.find('#hdd_precredito_reg').val();
                    if(asesor_n < 1){
                        $.alert('<b>Elija asesor a asignar</b>');
                        return false;
                    } else if (asesor_a == asesor_n) {
                        $.alert('<b>Elija un asesor diferente al actual</b>');
                        return false;
                    }                    
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'precredito/precredito_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action_precredito: 'modificar_campo',
                            hdd_precredito_id: pcr_id,
                            hdd_precredito_reg: pcr_reg,
                            columna: 'tb_precred_usureg',
                            valor: asesor_n,
                            tipo_dato: 'INT',
                            asesornuevo_nombre: asesor_n_nombre
                        }),
                        success: function (data) {
                            $.alert(`<b>${data.mensaje}</b>`);
                            if (parseInt(data.estado) == 1) {
                                precredito_contenido();
                            }
                        },
                    });
                }
            },
            cancelar: function () {
            }
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

function notas_precred(pcr_id) {
    $.confirm({
        title: 'Notas de precredito id: '+pcr_id,
        content: 'url:' + VISTA_URL + `precredito/precredito_notas.php?pcr_id=${pcr_id}`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Guardar',
                btnClass: 'btn-blue',
                action: function () {
                    var nueva_nota = this.$content.find('#txt_nuevanota').val();
                    var pcr_reg = this.$content.find('#hdd_precredito_reg').val();
                    if(!nueva_nota){
                        $.alert('<b>Ingrese una nota</b>');
                        return false;
                    }
                    $.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'precredito/precredito_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action_precredito: 'modificar_campo',
                            hdd_precredito_id: pcr_id,
                            hdd_precredito_reg: pcr_reg,
                            columna: 'tb_precred_notas',
                            valor: nueva_nota,
                            tipo_dato: 'STR'
                        }),
                        success: function (data) {
                            $.alert(`<b>${data.mensaje}</b>`);
                        },
                    });
                }
            },
            cancelar: function () {
            }
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

function historial_n(pcr_id){
    $.confirm({
        title: 'Historial del precredito id: '+pcr_id,
        content: 'url:' + VISTA_URL + `precredito/precredito_historial.php?pcr_id=${pcr_id}`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'large',
        buttons: {
            close: function () {
            }
        }
    });
}

/*se usa desde form credito menor */
function carousel(modulo_nom, modulo_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "templates/carousel.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: modulo_nom,
            modulo_id: modulo_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Galería');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            $('#div_modal_carousel').html(data);
            $('#modal_carousel_galeria').modal('show');

            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
/*  */

function f_x() {
    $('#che_fase_rechazado').change(function () {
        if (this.checked) {
            $('#fase_rec').show(100);
        }
        else {
            $('#fase_rec').hide(100);
        }
    });
}

function checkbox_reasignar(){
    $('input[type="checkbox"][id*="che_pcr_"]').iCheck({
        checkboxClass: `icheckbox_flat-green`,
        radioClass: `iradio_flat-green`
    });
}

function reasignar_grupo() {
    var id_fallo = '';
    var box_seleccionados = $('#contenido').find('input[type="checkbox"][id*="che_pcr_"]:checked');
    if (box_seleccionados.length == 0) {
        alertas_error('Error', 'No seleccionó ningún precrédito', 'small');
        return;
    }
    $.confirm({
        title: 'Cambiar asesor',
        content: `url:${VISTA_URL}precredito/precredito_cambioasesor.php`,
        type: 'blue',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: 'small',
        buttons: {
            Confirmar: {
                btnClass: 'btn-blue',
                action: function () {
                    var asesor_n = this.$content.find('.asesor_n').val();
                    var asesor_n_nombre = this.$content.find('.asesor_n option:selected').text();
                    if (parseInt(asesor_n) === 0) {
                        alertas_error('Error', 'elija un asesor', 'small');
                        return;
                    }
                    for (let i = 0; i < box_seleccionados.length; i++) {
                        var pcr_id = box_seleccionados[i].id.split('_')[2];
                        var pcr_reg = $('#contenido').find(`#hdd_precredreg_${pcr_id}`).val();
                        
                        $.ajax({
                            type: 'POST',
                            url: VISTA_URL + 'precredito/precredito_controller.php',
                            async: true,
                            dataType: 'json',
                            data: ({
                                action_precredito: 'modificar_campo',
                                columna: 'tb_precred_usureg',
                                hdd_precredito_id: pcr_id,
                                hdd_precredito_reg: pcr_reg,
                                valor: asesor_n,
                                tipo_dato: 'INT',
                                asesornuevo_nombre: asesor_n_nombre
                            }),
                            success: function (data) {
                                if (parseInt(data.estado) != 1) {
                                    id_fallo += "<br>- fallo en id "+ data.pcr_id;
                                }
                            },
                            complete: function (data) {
                                console.table(data.responseText);
                            }
                        });
                    }
                    $.alert("Éxito en la operación"+ id_fallo);
                    precredito_contenido();
                }
            },
            cancelar: function () {
            }
        }
    });
}


function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            close: function () {
            }
        }
    });
}

/* GERSON (15-06-24) */
function eliminarProceso(proceso_id) {

    $.ajax({
        type: "POST",
        url: VISTA_URL + "proceso/proceso_controller.php",
        async: false,
        dataType: "json",
        data: ({
        action: 'eliminar_proceso',
        proceso_id: proceso_id
        }),
        beforeSend: function () {
        },
        success: function (data) {
            if(data.estado==1){
                console.log(data.mensaje);
            }
        },
        complete: function (data) {
        }
    });
  
}
/*  */