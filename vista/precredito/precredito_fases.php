<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../precredito/Precredito.class.php');
$oPrecredito = new Precredito();
require_once('../historial_nuevo/Historial_nuevo.class.php');
$oHist_n = new Historial_nuevo();

$modo_atencion = array(1 => 'PRESENCIAL', 2 => 'VIRTUAL');
$cretip_nom = array(1 => 'CM', 3 => 'CGV');
$moneda_nom = array(1 => 'S/.', 2 => 'US$');
$fecha_hoy = date('Y-m-d');

$precliente_id = intval($_POST['hdd_fil_precliente_id']);
$asesor_id  = intval($_POST['cbo_fil_asesor']);
$fecha_ini  = fecha_mysql($_POST['txt_fil_fec1']);
$fecha_fin  = fecha_mysql($_POST['txt_fil_fec2']);
$empresa    = $_POST['cbo_fil_sede'];
$cretip_id  = $_POST['cbo_fil_cretip'];

//datos del where
$where[0]['column_name'] = 'tb_precred_xac';
$where[0]['param0'] = 1;
$where[0]['datatype'] = 'INT';

$where[1]['conector'] = 'AND';
$where[1]['column_name'] = 'pcr.tb_precliente_id';
$where[1]['param1'] = $precliente_id;
$where[1]['datatype'] = 'INT';

$where[2]['conector'] = 'AND';
$where[2]['column_name'] = 'pcr.tb_precred_usureg';
$where[2]['param2'] = $asesor_id;
$where[2]['datatype'] = 'INT';

$where[3]['conector'] = 'AND';
$where[3]['column_name'] = 'pcr.tb_precred_fecreg';
$where[3]['param3'] = !empty($fecha_ini) && !empty($fecha_fin) ? "BETWEEN '$fecha_ini' and '$fecha_fin'" : '';

$where[4]['conector'] = 'AND';
$where[4]['column_name'] = 'pcr.tb_empresa_id';
$where[4]['param4'] = $empresa;
$where[4]['datatype'] = 'INT';

$where[5]['conector'] = 'AND';
$where[5]['column_name'] = 'pcr.tb_creditotipo_id';
$where[5]['param5'] = $cretip_id;
$where[5]['datatype'] = 'INT';

$where[6]['conector'] = 'AND';
$where[6]['column_name'] = 'pcr.tb_estadoprecred_id';
$where[6]['param6'] = "IN (4, 5, 9, 10)";
//
for ($i = 0; $i < count($where); $i++) {
    if (empty($where[$i]["param$i"])) {
        $where[$i] = array();
    }
}
//datos del join
    $join[0]['alias_columnasparaver'] = 'im.tb_inconclusomotivo_nom';
    $join[0]['tipo_union'] = 'LEFT';
    $join[0]['tabla_alias'] = 'tb_inconclusomotivo im';
    $join[0]['columna_enlace'] = 'pcr.tb_inconclusomotivo_id';
    $join[0]['alias_columnaPK'] = 'im.tb_inconclusomotivo_id';

    $join[1]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
    $join[1]['tipo_union'] = 'LEFT';
    $join[1]['tabla_alias'] = 'tb_usuario us';
    $join[1]['columna_enlace'] = 'pcr.tb_precred_usureg';
    $join[1]['alias_columnaPK'] = 'us.tb_usuario_id';

    $join[2]['alias_columnasparaver'] = 'tb_empresa_nomcom';
    $join[2]['tipo_union'] = 'LEFT';
    $join[2]['tabla_alias'] = 'tb_empresa em';
    $join[2]['columna_enlace'] = 'pcr.tb_empresa_id';
    $join[2]['alias_columnaPK'] = 'em.tb_empresa_id';

    $join[3]['alias_columnasparaver'] = 'tb_precliente_nombres';
    $join[3]['tipo_union'] = 'LEFT';
    $join[3]['tabla_alias'] = 'tb_precliente pcl';
    $join[3]['columna_enlace'] = 'pcr.tb_precliente_id';
    $join[3]['alias_columnaPK'] = 'pcl.tb_precliente_id';

    $join[4]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
    $join[4]['tipo_union'] = 'LEFT';
    $join[4]['tabla_alias'] = 'tb_moneda mon';
    $join[4]['columna_enlace'] = 'pcr.tb_moneda_id';
    $join[4]['alias_columnaPK'] = 'mon.tb_moneda_id';

    $join[5]['alias_columnasparaver'] = 'epcr.tb_estadoprecred_nom';
    $join[5]['tipo_union'] = 'LEFT';
    $join[5]['tabla_alias'] = 'tb_estadoprecred epcr';
    $join[5]['columna_enlace'] = 'pcr.tb_estadoprecred_id';
    $join[5]['alias_columnaPK'] = 'epcr.tb_estadoprecred_id';
//
//datos extra
$otros['orden']['column_name'] = 'pcr.tb_precred_fecmod';
$otros['orden']['value'] = 'DESC';
//
$result = $oPrecredito->listar_todos($where, $join, $otros);
$where[3] = array();
$where[6]['param6'] = "NOT IN (4, 5, 9, 10)";
$result1 = $oPrecredito->listar_todos($where, $join, $otros);
unset($where, $join, $otros);

$lista = array('int' => '', 'seg' => '', 'vis' => '', 'con' => '', 'rec' => '');

if ($result1['estado'] == 1) {
    foreach ($result1['data'] as $key => $pcr) {
        $btn_opc = $li_opc = $checkbox_reasignar = $pcr_reg_hidden = $extension_interes = '';

        //calculo de días en gestión
            $wh[0]['column_name'] = 'hn.tb_histnuevo_tablanom';
            $wh[0]['param0'] = 'tb_precred';
            $wh[0]['datatype'] = 'STR';

            $wh[1]['conector'] = 'AND';
            $wh[1]['column_name'] = 'hn.tb_histnuevo_columnamod';
            $wh[1]['param1'] = 'tb_estadoprecred_id';
            $wh[1]['datatype'] = 'STR';

            $wh[2]['conector'] = 'AND';
            $wh[2]['column_name'] = 'hn.tb_histnuevo_regmodid';
            $wh[2]['param2'] = $pcr['tb_precred_id'];
            $wh[2]['datatype'] = 'INT';

            $otros['orden']['column_name'] = 'tb_histnuevo_fecreg';
            $otros['orden']['value'] = 'DESC';

            $otros['limit']['value'] = '1';

            $consulta = $oHist_n->listar_todos($wh, array(), $otros);
            unset($wh, $otros);
            if ($consulta['estado'] == 1) {
                $dias = restaFechas(fecha_mysql($consulta['data'][0]['tb_histnuevo_fecreg']), $fecha_hoy);
            } else {
                $dias = restaFechas(fecha_mysql($pcr['tb_precred_fecreg']), $fecha_hoy);
            }
        //

        if (($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) && in_array(intval($pcr['tb_estadoprecred_id']), array(1, 6))) { //usuarios admin o gestor de cobranza(nayra)
            $checkbox_reasignar = '<input type="checkbox" style="margin: 1.5px 0px 0px -12px;" id="che_pcr_'. $pcr['tb_precred_id'] .'" name="che_pcr_'. $pcr['tb_precred_id'] .'" value="1">&nbsp;';
            $pcr_reg_hidden = '<input type="hidden" name="hdd_precredreg_'. $pcr['tb_precred_id'] .'" id="hdd_precredreg_'. $pcr['tb_precred_id'] .'" value=\''. json_encode($pcr) .'\'>';
        }

        if (!empty($pcr['tb_precred_int'])) {
            $extension_interes = ' al '.$pcr['tb_precred_int'].'%';
        }

        $solic_precred =
            '<li class="item">' .
                $pcr_reg_hidden.
                "<div class='col-md-10' style='padding: 0px'>". $checkbox_reasignar .'<b> ID '. $pcr['tb_precred_id'] .'</b><br>'.
                    "<i class=\"fa fa-user\"></i> {$pcr['tb_precliente_nombres']}<br>" .
                    "<i class=\"fa fa-cube\"></i> {$cretip_nom[intval($pcr['tb_creditotipo_id'])]}, {$pcr['tb_precred_producto']}<br>" .
                    "<i class=\"fa fa-money\"></i> {$moneda_nom[$pcr['tb_moneda_id']]} " . mostrar_moneda($pcr['tb_precred_monto']) . $extension_interes. "<br>" .
                    "<i class=\"fa fa-black-tie\"></i> {$pcr['asesor']}<br>" .
                    '<i class="fa fa-calendar"></i> '. mostrar_fecha($consulta['data'][0]['tb_histnuevo_fecreg']) .
                '</div>' .
                '<div class="col-md-2" style="padding: 0px 5px">' .
                    '<button class="btn bg-light-blue btn-xs btn-block" onclick="precredito_form(\'L\', ' . $pcr['tb_precred_id'] . ')" title="Ver"><i class="fa fa-eye"></i></button>';
        if ($pcr['tb_precred_usureg'] == $_SESSION['usuario_id'] || $_SESSION['usuario_id'] == 32 || $_SESSION['usuarioperfil_id'] == 1) {
            $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="notas_precred(' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-newspaper-o"></i> Notas</b></a></li>';
            if (empty($pcr['tb_credito_id'])) {
                //aqui se puede modificar y reasignar
                $btn_opc .= '<button class="btn btn-warning btn-xs btn-block" onclick="precredito_form(\'M\', ' . $pcr['tb_precred_id'] . ')" title="Modificar"><i class="fa fa-edit"></i></button>';
                if (intval($pcr['tb_creditotipo_id']) == 1 || in_array(intval($pcr['tb_estadoprecred_id']), array(7, 8))) {
                    $btn_opc .= '<button class="btn btn-success btn-xs btn-block" onclick="precredito_form(\'L\', ' . $pcr['tb_precred_id'] . ', 1)" title="Crédito completo"><i class="fa fa-check"></i></button>';
                }
                if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
                    $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="reasignar(' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-exchange"></i> Reasignar</b></a></li>';
                }
            }
            //$li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick=""><b><i class="fa fa-history"></i> Historial</b></a></li>';
            if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
                $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="eliminar(' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-trash"></i> Eliminar</b></a></li>';
            }

            $solic_precred .=
                $btn_opc .
                '<div class="btn-group btn-block">' .
                    '<button type="button" class="btn bg-navy btn-block btn-xs dropdown-toggle" data-toggle="dropdown" title="Más opciones"><span class="caret"></span></button>' .
                    '<ul class="dropdown-menu dropdown-menu-right">' .
                        $li_opc .
                    '</ul>' .
                '</div>';
        }
        $solic_precred .=
            "</div></row>" .
            '<div class="row">' .
                '<div class="col-md-6">'.
                '</div>'.
                '<div class="col-md-6">' .
                    '<a onclick="historial_n(' . $pcr['tb_precred_id'] . ')" class="pull-right" style="color: blue; cursor: pointer; font-weight: bold;">Días en gestión: ' . $dias . '</a>' .
                '</div>' .
            '</div>' .
            "</li>";

        if (in_array(intval($pcr['tb_estadoprecred_id']), array(1, 6))) {
            $lista['int'] .= $solic_precred;
        } elseif (in_array(intval($pcr['tb_estadoprecred_id']), array(2, 7))) {
            $lista['seg'] .= $solic_precred;
        } else {
            /* if (in_array(intval($pcr['tb_estadoprecred_id']), array(3, 8))) */
            $lista['vis'] .= $solic_precred;
        }
    }
}

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $pcr) {
        $estado_motivo = $cretip_nom[intval($pcr['tb_creditotipo_id'])];
        $motivo_incon = $btn_opc = $li_opc = $extension_interes = '';
        if (in_array(intval($pcr['tb_estadoprecred_id']), array(4, 9))) {
            if (!empty($pcr['tb_credito_id'])) {
                $estado_motivo .= '-' . $pcr['tb_credito_id'];
            } else {
                $estado_motivo .= ' <font color="red">(registro cred. pend.)</font>';
            }
        } elseif (in_array(intval($pcr['tb_estadoprecred_id']), array(5, 10))) {
            $motivo_incon = "<i class=\"fa fa-info\"></i> Motivo: {$pcr['tb_inconclusomotivo_nom']}<br>";
        }

        //calculo de días en gestión
            $wh[0]['column_name'] = 'hn.tb_histnuevo_tablanom';
            $wh[0]['param0'] = 'tb_precred';
            $wh[0]['datatype'] = 'STR';

            $wh[1]['conector'] = 'AND';
            $wh[1]['column_name'] = 'hn.tb_histnuevo_columnamod';
            $wh[1]['param1'] = 'tb_estadoprecred_id';
            $wh[1]['datatype'] = 'STR';

            $wh[2]['conector'] = 'AND';
            $wh[2]['column_name'] = 'hn.tb_histnuevo_regmodid';
            $wh[2]['param2'] = $pcr['tb_precred_id'];
            $wh[2]['datatype'] = 'INT';

            $otros['orden']['column_name'] = 'tb_histnuevo_fecreg';
            $otros['orden']['value'] = 'DESC';

            $otros['limit']['value'] = '1';

            $consulta = $oHist_n->listar_todos($wh, array(), $otros);
            unset($wh, $otros);
        //

        if (!empty($pcr['tb_precred_int'])) {
            $extension_interes = ' al '.$pcr['tb_precred_int'].'%';
        }

        $solic_precred =
            '<li class="item">' .
            "<div class='col-md-10' style='padding: 0px'><b>ID {$pcr['tb_precred_id']}</b><br>" .
            "<i class=\"fa fa-user\"></i> {$pcr['tb_precliente_nombres']}<br>" .
            "<i class=\"fa fa-cube\"></i> $estado_motivo, {$pcr['tb_precred_producto']}<br>" .
            "<i class=\"fa fa-money\"></i> {$moneda_nom[$pcr['tb_moneda_id']]} " . mostrar_moneda($pcr['tb_precred_monto']) .$extension_interes. "<br>" .
            $motivo_incon .
            "<i class=\"fa fa-black-tie\"></i> {$pcr['asesor']}<br>" .
            '<i class="fa fa-calendar"></i> '. mostrar_fecha($consulta['data'][0]['tb_histnuevo_fecreg']) .
            '</div>' .
            '<div class="col-md-2" style="padding: 0px 5px">' .
            '<button class="btn bg-light-blue btn-xs btn-block" onclick="precredito_form(\'L\', ' . $pcr['tb_precred_id'] . ')"><i class="fa fa-eye" title="Ver"></i></button>';
        if ($pcr['tb_precred_usureg'] == $_SESSION['usuario_id'] || $_SESSION['usuario_id'] == 32 || $_SESSION['usuarioperfil_id'] == 1) {
            $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="notas_precred(' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-newspaper-o"></i> Notas</b></a></li>';
            if (empty($pcr['tb_credito_id'])) {
                //aqui se puede modificar y reasignar
                if (!in_array(intval($pcr['tb_estadoprecred_id']), array(4, 9))) {
                    $btn_opc .= '<button class="btn btn-warning btn-xs btn-block" onclick="precredito_form(\'M\', ' . $pcr['tb_precred_id'] . ')" title="Modificar"><i class="fa fa-edit"></i></button>';
                } else {
                    if ($pcr['tb_creditotipo_id'] == 1) {
                        $btn_opc .= '<button class="btn btn-success btn-xs btn-block" onclick="creditomenor_form(\'I\', ' . $pcr['tb_precred_id'] . ')" title="Crédito completo"><i class="fa fa-check"></i></button>';
                    } else {
                        //ya no es necesario añadir boton de generar proceso garveh
                    }
                    $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="precredito_form(\'M\', ' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-edit"></i> Modificar</b></a></li>';
                }
                if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
                    $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="reasignar(' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-exchange"></i> Reasignar</b></a></li>';
                }
            }
            //$li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="historial_n('.$pcr['tb_precred_id'].')"><b><i class="fa fa-history"></i> Historial</b></a></li>';
            if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
                $li_opc .= '<li><a class="btn" href="javascript:void(0)" onclick="eliminar(' . $pcr['tb_precred_id'] . ')"><b><i class="fa fa-trash"></i> Eliminar</b></a></li>';
            }

            $solic_precred .=
                $btn_opc .
                '<div class="btn-group btn-block">' .
                '<button type="button" class="btn bg-navy btn-block btn-xs dropdown-toggle" data-toggle="dropdown" title="Más opciones"><span class="caret"></span></button>' .
                '<ul class="dropdown-menu dropdown-menu-right">' .
                $li_opc .
                '</ul>' .
                '</div>';
        }
        $solic_precred .=
            "</div>" .
            '<div class="row">' .
                '<div class="col-md-6">'.
                '</div>'.
                '<div class="col-md-6">' .
                    '<a onclick="historial_n(' . $pcr['tb_precred_id'] . ')" class="pull-right" style="color: blue; cursor: pointer; font-weight: bold;">Historial</a>' .
                '</div>' .
            '</div>' .
            "</li>";

        if (in_array(intval($pcr['tb_estadoprecred_id']), array(4, 9))) {
            $lista['con'] .= $solic_precred;
        } else {
            $lista['rec'] .= $solic_precred;
        }
    }
    $result = null;
}

if (empty($lista['int'])) {
    $lista['int'] = 'no hay pre-creditos en esta fase';
}
if (empty($lista['seg'])) {
    $lista['seg'] = 'no hay pre-creditos en esta fase';
}
if (empty($lista['vis'])) {
    $lista['vis'] = 'no hay pre-creditos en esta fase';
}
if (empty($lista['con'])) {
    $lista['con'] = 'no hay pre-creditos en esta fase';
}
if (empty($lista['rec'])) {
    $lista['rec'] = 'no hay pre-creditos en esta fase';
}

unset($result, $result1);
?>


<p></p>
<div class="row">
    <div id="fase_int" class="col-md-2" style="width: 20%;">
        <div class="box box-info shadow" style="padding: 5px 10px;">
            <div class="box-header with-border" style="padding: 5px 3px;">
                <h3 class="box-title" style="font-size: 16px;">INTERESADOS</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" style="padding-top: 0px;" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" style="padding: 5px 5px;">
                <ul style="padding: 0px;" class="products-list product-list-in-box"><?php echo $lista['int']; ?></ul>
            </div>
        </div>
    </div>

    <div id="fase_seg" class="col-md-2" style="width: 20%;">
        <div class="box box-warning shadow" style="padding: 5px 10px;">
            <div class="box-header with-border" style="padding: 5px 3px;">
                <h3 class="box-title" style="font-size: 16px;">SEGUIMIENTO</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" style="padding-top: 0px;" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" style="padding: 5px 5px;">
                <ul style="padding: 0px;" class="products-list product-list-in-box"><?php echo $lista['seg']; ?></ul>
            </div>
        </div>
    </div>

    <div id="fase_vis" class="col-md-2" style="width: 20%;">
        <div class="box box-primary shadow" style="padding: 5px 10px;">
            <div class="box-header with-border" style="padding: 5px 3px;">
                <h3 class="box-title" style="font-size: 16px;">VISITA</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" style="padding-top: 0px;" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" style="padding: 5px 5px;">
                <ul style="padding: 0px;" class="products-list product-list-in-box"><?php echo $lista['vis']; ?></ul>
            </div>
        </div>
    </div>

    <div id="fase_con" class="col-md-2" style="width: 20%;">
        <div class="box box-success shadow" style="padding: 5px 10px;">
            <div class="box-header with-border" style="padding: 5px 3px;">
                <h3 class="box-title" style="font-size: 16px;">CONCLUIDO</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" style="padding-top: 0px;" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" style="padding: 5px 5px;">
                <ul style="padding: 0px;" class="products-list product-list-in-box"><?php echo $lista['con']; ?></ul>
            </div>
        </div>
    </div>

    <div id="fase_rec" class="col-md-2" style="width: 20%;">
        <div class="box box-danger shadow" style="padding: 5px 10px;">
            <div class="box-header with-border" style="padding: 5px 3px;">
                <h3 class="box-title" style="font-size: 16px;">RECHAZADO</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" style="padding-top: 0px;" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" style="padding: 5px 5px;">
                <ul style="padding: 0px;" class="products-list product-list-in-box"><?php echo $lista['rec']; ?></ul>
            </div>
        </div>
    </div>
</div>
