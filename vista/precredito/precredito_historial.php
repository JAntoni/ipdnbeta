<?php
require_once('../historial_nuevo/Historial_nuevo.class.php');
$oHist_n = new Historial_nuevo();
require_once('../precredito/Precredito.class.php');
$oPcr = new Precredito();
require_once('../funciones/fechas.php');

$pcr_id = $_GET['pcr_id'];

$wh[0]['column_name'] = 'pcr.tb_precred_id';
$wh[0]['param0'] = $pcr_id;
$wh[0]['datatype'] = 'INT';

$join[0]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor, us.tb_usuario_fot";
$join[0]['tipo_union'] = 'LEFT';
$join[0]['tabla_alias'] = 'tb_usuario us';
$join[0]['columna_enlace'] = 'pcr.tb_precred_usureg';
$join[0]['alias_columnaPK'] = 'us.tb_usuario_id';

$pcr_reg = $oPcr->listar_todos($wh, $join, array())['data'][0];

$lista = '';

//datos
    $wh[0]['column_name'] = 'hn.tb_histnuevo_tablanom';
    $wh[0]['param0'] = 'tb_precred';
    $wh[0]['datatype'] = 'STR';

    $wh[1]['conector'] = 'AND';
    $wh[1]['column_name'] = 'hn.tb_histnuevo_regmodid';
    $wh[1]['param1'] = $pcr_id;
    $wh[1]['datatype'] = 'INT';

    $wh[2]['conector'] = 'AND';
    $wh[2]['param2'] = "(hn.tb_histnuevo_det IS NOT NULL OR hn.tb_histnuevo_det <> '')";

    $wh[3]['conector'] = 'AND';
    $wh[3]['column_name'] = 'hn.tb_histnuevo_xac';
    $wh[3]['param3'] = 1;
    $wh[3]['datatype'] = 'INT';

    $join[0]['columna_enlace'] = 'hn.tb_histnuevo_usureg';

    $join[1]['alias_columnasparaver'] = 'epcr.tb_estadoprecred_nom as estado1';
    $join[1]['tipo_union'] = 'LEFT';
    $join[1]['tabla_alias'] = 'tb_estadoprecred epcr';
    $join[1]['columna_enlace'] = 'hn.tb_histnuevo_valorant';
    $join[1]['alias_columnaPK'] = 'epcr.tb_estadoprecred_id';

    $join[2]['alias_columnasparaver'] = 'epcr1.tb_estadoprecred_nom as estado2';
    $join[2]['tipo_union'] = 'LEFT';
    $join[2]['tabla_alias'] = 'tb_estadoprecred epcr1';
    $join[2]['columna_enlace'] = 'hn.tb_histnuevo_valornuevo';
    $join[2]['alias_columnaPK'] = 'epcr1.tb_estadoprecred_id';

    $otros['orden']['column_name'] = 'hn.tb_histnuevo_fecreg';
    $otros['orden']['value'] = 'ASC';
//fin de datos
$resultado = $oHist_n->listar_todos($wh, $join, $otros);
unset($wh, $join, $otros);
//$lista = $resultado['sql'];

if ($resultado['estado'] == 1) {
    foreach ($resultado['data'] as $historiales => $hist) {
        $lista .= 
        '<li>'.
            '<i class="fa fa-envelope bg-blue"></i>'.
			'<div class="timeline-item">'.
                '<span class="time"><i class="fa fa-clock-o"></i> '. mostrar_fecha_hora($hist['tb_histnuevo_fecreg']) . '</span>'.
                '<div class="user-block">'.
                    '<img class="img-circle img-bordered-sm" src="' . $hist['tb_usuario_fot'] . '" alt="user image">'.
                    '<span class="username">'.
                        '<a href="#">'. $hist['asesor'] .'</a>'.
                    '</span>'.
                    '<span class="description">'. $hist['tb_histnuevo_det'] .'</span><br>'.
                '</div>'.
            '</div>'.
		'</li>';
    }
}
?>

<br>
<ul class="timeline">
    <?php echo $lista; ?>
</ul>
