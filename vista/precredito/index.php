<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
	echo 'Terminó la sesión';
	exit();
}
?>
<!DOCTYPE html>
<html>

<head>
	<!-- TODOS LOS ESTILOS-->
	<?php include VISTA_URL . 'templates/head.php'; ?>
	<title><?php echo ucwords(mb_strtolower($menu_tit)); ?></title>
	<style>
		.select2-container--default .select2-selection--single {
			padding: 8px 4px;
			/* necesario con select2 */
		}

		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 20px;
			/* necesario con select2 */
		}

		.select2.narrow {
			width: 250px;
			/* opcional para definir un ancho especifico del select2 */
		}

		.product-list-in-box>.item {
			border-bottom: 2px solid #5A7D6F;
			/* para dar color a la linea de separacion */
		}
	</style>
</head>

<body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>

	<div class="wrapper">
		<!-- INCLUIR EL HEADER-->
		<?php include VISTA_URL . 'templates/header.php'; ?>
		<!-- INCLUIR ASIDE, MENU LATERAL -->
		<?php include VISTA_URL . 'templates/aside.php'; ?>
		<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
		<?php include 'precredito_vista.php'; ?>
		<!-- INCLUIR FOOTER-->
		<?php include VISTA_URL . 'templates/footer.php'; ?>
		<div class="control-sidebar-bg"></div>
	</div>

	<!-- TODOS LOS SCRIPTS-->
	<?php include VISTA_URL . 'templates/script.php'; ?>

	<script type="text/javascript" src="<?php echo VISTA_URL . 'precredito/precredito.js?ver=15062024';?>"></script>
</body>

</html>
