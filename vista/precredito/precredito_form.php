<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../precredito/Precredito.class.php');
$oPrecredito = new Precredito();
require_once('../inconclusomotivo/Inconclusomotivo.class.php');
$oInconclusoMot = new Inconclusomotivo();
$listo = 1;

$usuario_action = $_POST['action'];
$titulo = $ocultar_2da_parte = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar precredito';
    $ocultar_2da_parte = 'display: none;';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar precredito';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar precredito';
} elseif ($usuario_action == 'L') {
    $titulo = 'Precredito registrado';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action = devuelve_nombre_usuario_action($usuario_action);

$precredito_id = intval($_POST["precred_id"]);
$fecha_precredito = date('Y-m-d');

if ($usuario_action != 'I') {
    $titulo .= ": ID $precredito_id";
}

if ($listo == 1) {
    $tipocredito_id = 1; //default cre menor elegido
    $modo_atencion[1] = 'selected'; //default atencion presencial $modo_atencion[1] elegido
    $estadoprecred_id = 3; //default estado 3 = visita

    if (!empty($precredito_id)) {
        $where[0]['column_name'] = 'tb_precred_id';
        $where[0]['param0'] = $precredito_id;
        $where[0]['datatype'] = 'INT';

        $inner[0]['alias_columnasparaver'] = 'pcl.tb_precliente_numdoc, pcl.tb_precliente_nombres, pcl.tb_precliente_numcel, pcl.tb_mediocom_id, pcl.tb_precliente_tipodoc, pcl.tb_cliente_id';
        $inner[0]['tipo_union'] = 'LEFT';
        $inner[0]['tabla_alias'] = 'tb_precliente pcl';
        $inner[0]['columna_enlace'] = 'pcr.tb_precliente_id';
        $inner[0]['alias_columnaPK'] = 'pcl.tb_precliente_id';

        $inner[1]['alias_columnasparaver'] = 'mc.tb_mediocom_nom';
        $inner[1]['tipo_union'] = 'LEFT';
        $inner[1]['tabla_alias'] = 'tb_mediocom mc';
        $inner[1]['columna_enlace'] = 'pcl.tb_mediocom_id';
        $inner[1]['alias_columnaPK'] = 'mc.tb_mediocom_id';

        $res = $oPrecredito->listar_todos($where, $inner, array());

        if ($res['estado'] == 1) {
            $precredito = $res['data'][0];
            $precli['precliente_id'] = $precredito['tb_precliente_id'];
            $precli['cliente_id'] = $precredito['tb_cliente_id'];
            $precli['precliente_tipodoc'] = $precredito['tb_precliente_tipodoc'];
            $precli['precliente_numdoc'] = $precredito['tb_precliente_numdoc'];
            $precli['precliente_nombres'] = $precredito['tb_precliente_nombres'];
            $precli['precliente_numcel'] = $precredito['tb_precliente_numcel'];
            $precli['mediocom_id'] = $mediocom_id = $precredito['tb_mediocom_id'];
            $precli['mediocom_nom'] = $precredito['tb_mediocom_nom'];
            $tipocredito_id = $precredito['tb_creditotipo_id'];
            $tipcre_selected[$tipocredito_id] = 'selected';
            $articulo_id = $precredito['tb_articulo_id'];
            $cgarvtipo_id = $precredito['cgarvtipo_id'];
            $producto_nom = $precredito['tb_precred_producto'];
            $moneda_id = $precredito['tb_moneda_id'];
            $precred_monto = mostrar_moneda($precredito['tb_precred_monto']);
            $precred_int = $precredito['tb_precred_int'];
            $precred_url = $precredito['tb_precred_urltasacion'];
            $precred_coment = $precredito['tb_precred_coment'];
            $modo_atencion[1] = $precredito['tb_precred_modoatencion'] == 1 ? 'selected' : '';
            $modo_atencion[2] = $precredito['tb_precred_modoatencion'] == 2 ? 'selected' : '';
            $estadoprecred_id = $precredito['tb_estadoprecred_id'];
            $inconclusomotivo_id = $precredito['tb_inconclusomotivo_id'];
            /* gerson (15-03-24) */
            $tipo_adenda = isset($precredito['tb_precred_tipo_adenda'] ) ? $precredito['tb_precred_tipo_adenda'] : '';
            $creditomatriz_id = isset($precredito['tb_creditomatriz_id'] ) ? $precredito['tb_creditomatriz_id'] : 0;
            /*  */
        }
        unset($res);
    }
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_precredito_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_precredito" method="post">
                    <div class="modal-body">
                        <style>
                            .form-group {
                                padding-bottom: 4px;
                                margin-bottom: 4px;
                            }
                            .uploadifive-queue-item {
                                padding: 5px 10px 0px 10px !important;
                            }
                        </style>
                        <?php if ($action == 'insertar' || $action == 'modificar')
                            echo '<label style="color: green; font-size: 11.5px;">- Para añadir nuevo cliente borre el documento y escriba el nuevo dni.<br>' .
                                '- Revisar tasación de producto en el sistema <a onclick="return tasacionsug_form();" style="cursor: pointer;">AQUI</a></label>';
                        ?>
                        <input type="hidden" name="action_precredito" id="action_precredito" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_precredito_id" id="hdd_precredito_id" value="<?php echo $precredito_id; ?>">
                        <input type="hidden" name="hdd_precredito_reg" id="hdd_precredito_reg" value='<?php echo json_encode($precredito); ?>'>
                        <input type="hidden" name="hdd_concluido" id="hdd_concluido" value="<?php echo $_POST['concluido']; ?>">

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <input type="hidden" name="hdd_precli_id" id="hdd_precli_id" value="<?php echo $precli['precliente_id']; ?>">
                                <input type="hidden" name="hdd_cli_id" id="hdd_cli_id" value="<?php echo $precli['cliente_id']; ?>">
                                <input type="hidden" name="hdd_precli_reg" id="hdd_precli_reg" value='<?php echo json_encode($precli) ?>'>

                                <div class="col-md-4">
                                    <label for="txt_precli_nrodoc">N° Documento:</label>
                                    <input type="text" name="txt_precli_nrodoc" id="txt_precli_nrodoc" class="input-sm form-control mayus input-shadow" placeholder="N° Documento" value="<?php echo $precli['precliente_numdoc']; ?>">
                                </div>

                                <div class="col-md-8">
                                    <label for="txt_precli_nombres">Nombres:</label>
                                    <input type="text" name="txt_precli_nombres" id="txt_precli_nombres" class="input-sm form-control mayus input-shadow" placeholder="Nombres del cliente" value="<?php echo $precli['precliente_nombres']; ?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="txt_precli_numcel">Telf/Cel (opc):</label>
                                    <input type="text" name="txt_precli_numcel" id="txt_precli_numcel" class="input-sm form-control mayus input-shadow" placeholder="numero cel" value="<?php echo $precli['precliente_numcel']; ?>">
                                </div>

                                <div class="col-md-4">
                                    <label for="cbo_mediocom_id">Medio de comunicacion:</label>
                                    <select name="cbo_mediocom_id" id="cbo_mediocom_id" class="form-control input-sm input-shadow">
                                        <?php require_once('../mediocom/mediocom_select.php'); ?>
                                    </select>
                                </div>

                                <div class="col-md-11">
                                    <span id="precliente_selected_mensaje" style="font-weight: bold; color: red;"><?php echo $action == 'modificar' ? "Usted está modificando al cliente: {$precli['precliente_numdoc']} - {$precli['precliente_nombres']} - {$precli['precliente_numcel']} - {$precli['mediocom_nom']}" : ''; ?></span>
                                </div>
                                <div class="col-md-11" id="div_creditos_anteriores">
                                    <span style="font-weight: bold; color: blue;"><a onclick="return creditos_anteriores()" style="cursor: pointer;">OBLIGATORIO: Ver créditos de este cliente</a></span>
                                </div>
                            </div>

                            <div id="2da_parte" style="<?php echo $ocultar_2da_parte; ?>"> <!-- esto se muestra despues de la revision de creditos anteriores -->
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="cbo_creditotipo_id">Tipo credito:</label>
                                        <select name="cbo_creditotipo_id" id="cbo_creditotipo_id" class="form-control input-sm input-shadow<?php echo $action == 'modificar' ? ' disabled' : ''; ?>">
                                            <option value="1" style="font-weight: bold;" <?php echo $tipcre_selected[1]; ?>>C. MENOR</option>
                                            <option value="3" style="font-weight: bold;" <?php echo $tipcre_selected[3]; ?>>C. GARVEH</option>
                                        </select>
                                    </div>

                                    <div id="div_col_articulo" class="col-md-9 form-group">
                                        <input type="hidden" name="hdd_articulo_id" id="hdd_articulo_id" value="<?php echo $articulo_id; ?>">
                                        <label for="cbo_articulo_id">Tipo garantía:</label>
                                        <select name="cbo_articulo_id" id="cbo_articulo_id" class="form-control input-sm">
                                            <!-- aqui traer el select de articulo -->
                                        </select>
                                    </div>

                                    <div id="div_col_cgarvtipo" class="col-md-9 form-group">
                                        <label for="cbo_cgarvtipo_id">CGV. Tipo:</label>
                                        <select name="cbo_cgarvtipo_id" id="cbo_cgarvtipo_id" onchange="detectarAdenda(this.value)" class="form-control input-sm input-shadow mayus">
                                            <?php require_once('../proceso/cgarvtipo_select.php'); ?>
                                        </select>
                                    </div>
                                    <!-- gerson (13-03-24) -->
                                    <div id="div_col_tipoadenda" class="col-md-3 form-group <?php if($usuario_action != 'I' && $cgarvtipo_id == 6){ echo ''; }else{ echo 'hidden'; } ?>">
                                        <label for="cbo_tipoadenda">Tipo Adenda:</label>
                                        <select name="cbo_tipoadenda" id="cbo_tipoadenda" class="form-control input-sm input-shadow mayus">
                                            <option value="regular">REGULAR</option>
                                            <option value="otro">OTRO</option>
                                        </select>
                                        <input type="hidden" name="hdd_tipoadenda" id="hdd_tipoadenda" value="<?php echo $tipo_adenda; ?>">
                                    </div>
                                    <div id="div_col_credito" class="col-md-9 form-group hidden">
                                        <!-- <label for="cbo_credito_id">Crédito Matriz:</label> -->
                                        <!-- <select name="cbo_credito_id" id="cbo_credito_id" class="form-control input-sm input-shadow mayus">
                                            <?php //require_once('../proceso/cgarvtipo_select.php'); ?>
                                        </select> -->
                                    </div>
                                    <div id="div_col_credito" class="col-md-3 form-group <?php if($usuario_action != 'I' && $cgarvtipo_id == 6){ echo ''; }else{ echo 'hidden'; } ?>">
                                        <label for="cbo_credito">Crédito Matriz:</label>
                                        <label for="cbo_credito_valor"><?php echo $creditomatriz_id; ?></label>
                                        <input type="hidden" name="hdd_credito_valor" id="hdd_credito_valor" value="<?php echo $creditomatriz_id; ?>">
                                    </div>
                                    <!--  -->
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-9">
                                        <label for="txt_precred_producto">Producto:</label>
                                        <input type="text" name="txt_precred_producto" id="txt_precred_producto" class="form-control input-sm mayus input-shadow" placeholder="descripción del producto" value="<?php echo $producto_nom; ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <label>Monto aprox:</label>
                                        <div class="input-group">
                                            <select name="cbo_moneda_pagar" id="cbo_moneda_pagar" class="form-control input-sm mayus input-shadow" style="width: 39%; padding: 5px 5px;">
                                                <?php require_once('../moneda/moneda_select.php'); ?>
                                            </select>
                                            <input type="text" name="txt_precred_monto" id="txt_precred_monto" class="form-control input-sm moneda input-shadow" style="width: 61%; padding: 5px 5px;" placeholder="0.00" value="<?php echo $precred_monto; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div id="url_cred_menor" class="col-md-9">
                                        <label for="txt_precred_tasacionurl">Url tasación:</label>
                                        <input type="text" name="txt_precred_tasacionurl" id="txt_precred_tasacionurl" class="form-control input-sm input-shadow" placeholder="URL TASACIÓN" value="<?php echo $precred_url; ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txt_interes">Interés (%):</label>
                                        <input type="text" name="txt_interes" id="txt_interes" class="form-control input-sm input-shadow interes" placeholder="0.0%" value="<?php echo $precred_int; ?>">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="cbo_precred_modoatencion">Modo de atención:</label>
                                        <select name="cbo_precred_modoatencion" id="cbo_precred_modoatencion" class="form-control input-sm input-shadow<?php echo $precredito['tb_estadoprecred_id'] > 0 ? ' disabled' : ''; ?>">
                                            <option value="0" style="font-weight: bold;">(vacío)</option>
                                            <option value="1" style="font-weight: bold;" <?php echo $modo_atencion[1]; ?>>PRESENCIAL</option>
                                            <option value="2" style="font-weight: bold;" <?php echo $modo_atencion[2]; ?>>VIRTUAL</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <input type="hidden" id="hdd_estadoprecred_orig" name="hdd_estadoprecred_orig" value="<?php echo $precredito['tb_estadoprecred_id']; ?>">
                                        <label for="cbo_estadoprecred_id">Estado del precredito:</label>
                                        <select name="cbo_estadoprecred_id" id="cbo_estadoprecred_id" class="form-control input-sm input-shadow mayus">
                                            <?php //require_once('../precredito/estadoprecred_select.php'); 
                                            ?>
                                        </select>
                                    </div>

                                    <div id="div_inconclusomotivo" class="col-md-6">
                                        <label for="cbo_inconclusomotivo_id">Motivo no concluido:</label>
                                        <select name="cbo_inconclusomotivo_id" id="cbo_inconclusomotivo_id" class="form-control input-sm input-shadow">
                                            <?php require_once('../inconclusomotivo/inconclusomotivo_select.php'); ?>
                                        </select>
                                    </div>

                                    <?php if (!in_array(intval($estadoprecred_id), array(4, 5, 9, 10)) && !empty($inconclusomotivo_id)) {
                                        $wh[0]['column_name'] = 'im.tb_inconclusomotivo_id';
                                        $wh[0]['param0'] = $inconclusomotivo_id;
                                        $wh[0]['datatype'] = 'INT';
                                        $res = $oInconclusoMot->listar_todos($wh, array(), array());
                                        $motivo_anterior = $res['data'][0]['tb_inconclusomotivo_nom'];
                                    ?>
                                        <div class="col-md-11">
                                            <span id="" style="font-weight: bold; color: red;">Anteriormente rechazó por motivo: <?php echo $motivo_anterior; ?></span>
                                        </div>
                                    <?php
                                        unset($wh, $res, $motivo_anterior);
                                    } ?>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label for="txt_precred_coment">Comentario (opc):</label>
                                        <textarea class="form-control mayus input-sm input-shadow" name="txt_precred_coment" id="txt_precred_coment" rows="2" cols="80" placeholder="comentarios"><?php echo $precred_coment; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="precredito_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_precredito" style="<?php echo $ocultar_2da_parte; ?>">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_precredito">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/precredito/precredito_form.js?ver=112233'; ?>"></script>
