<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../precredito/Precredito.class.php');
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
$oPrecredito = new Precredito();

$modo_atencion = array(1 => 'PRESENCIAL', 2 => 'VIRTUAL');

$precliente_id = intval($_POST['hdd_fil_precliente_id']);
$asesor_id  = intval($_POST['cbo_fil_asesor']);
$fecha_ini  = fecha_mysql($_POST['txt_fil_fec1']);
$fecha_fin  = fecha_mysql($_POST['txt_fil_fec2']);
$empresa    = $_POST['cbo_fil_sede'];
$cretip_id  = $_POST['cbo_fil_cretip'];

//datos del where
    $where[0]['column_name'] = 'tb_precred_xac';
    $where[0]['param0'] = 1;
    $where[0]['datatype'] = 'INT';

    $where[1]['conector'] = 'AND';
    $where[1]['column_name'] = 'pcr.tb_precliente_id';
    $where[1]['param1'] = $precliente_id;
    $where[1]['datatype'] = 'INT';

    $where[2]['conector'] = 'AND';
    $where[2]['column_name'] = 'pcr.tb_precred_usureg';
    $where[2]['param2'] = $asesor_id;
    $where[2]['datatype'] = 'INT';

    $where[3]['conector'] = 'AND';
    $where[3]['column_name'] = 'pcr.tb_precred_fecreg';
    $where[3]['param3'] = !empty($fecha_ini) && !empty($fecha_fin) ? "BETWEEN '$fecha_ini' and '$fecha_fin'" : '';

    $where[4]['conector'] = 'AND';
    $where[4]['column_name'] = 'pcr.tb_empresa_id';
    $where[4]['param4'] = $empresa;
    $where[4]['datatype'] = 'INT';

    $where[5]['conector'] = 'AND';
    $where[5]['column_name'] = 'pcr.tb_creditotipo_id';
    $where[5]['param5'] = $cretip_id;
    $where[5]['datatype'] = 'INT';
//
for ($i = 0; $i < count($where); $i++) {
    if (empty($where[$i]["param$i"])) {
        $where[$i] = array();
    }
}
//datos del join
    $join[0]['alias_columnasparaver'] = 'im.tb_inconclusomotivo_nom';
    $join[0]['tipo_union'] = 'LEFT';
    $join[0]['tabla_alias'] = 'tb_inconclusomotivo im';
    $join[0]['columna_enlace'] = 'pcr.tb_inconclusomotivo_id';
    $join[0]['alias_columnaPK'] = 'im.tb_inconclusomotivo_id';

    $join[1]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
    $join[1]['tipo_union'] = 'LEFT';
    $join[1]['tabla_alias'] = 'tb_usuario us';
    $join[1]['columna_enlace'] = 'pcr.tb_precred_usureg';
    $join[1]['alias_columnaPK'] = 'us.tb_usuario_id';

    $join[2]['alias_columnasparaver'] = 'tb_empresa_nomcom';
    $join[2]['tipo_union'] = 'LEFT';
    $join[2]['tabla_alias'] = 'tb_empresa em';
    $join[2]['columna_enlace'] = 'pcr.tb_empresa_id';
    $join[2]['alias_columnaPK'] = 'em.tb_empresa_id';

    $join[3]['alias_columnasparaver'] = 'tb_precliente_nombres';
    $join[3]['tipo_union'] = 'LEFT';
    $join[3]['tabla_alias'] = 'tb_precliente pcl';
    $join[3]['columna_enlace'] = 'pcr.tb_precliente_id';
    $join[3]['alias_columnaPK'] = 'pcl.tb_precliente_id';

    $join[4]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
    $join[4]['tipo_union'] = 'LEFT';
    $join[4]['tabla_alias'] = 'tb_moneda mon';
    $join[4]['columna_enlace'] = 'pcr.tb_moneda_id';
    $join[4]['alias_columnaPK'] = 'mon.tb_moneda_id';

    $join[5]['alias_columnasparaver'] = 'epcr.tb_estadoprecred_nom';
    $join[5]['tipo_union'] = 'LEFT';
    $join[5]['tabla_alias'] = 'tb_estadoprecred epcr';
    $join[5]['columna_enlace'] = 'pcr.tb_estadoprecred_id';
    $join[5]['alias_columnaPK'] = 'epcr.tb_estadoprecred_id';
//
//datos extra
    $otros['orden']['column_name'] = 'pcr.tb_precred_fecreg';
    $otros['orden']['value'] = 'DESC';
//
$result = $oPrecredito->listar_todos($where, $join, $otros);

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $estado_motivo = $value['tb_estadoprecred_nom'];
        if (in_array(intval($value['tb_estadoprecred_id']), array(4, 9))) {
            if (!empty($value['tb_credito_id'])) {
                $cretip_nom = intval($value['tb_creditotipo_id']) == 1 ? 'CM' : 'CGV';
                $estado_motivo .= ' en '.$cretip_nom.'-'.$value['tb_credito_id'];
            } else {
                $estado_motivo .= ' <font color="red">(pendiente registrar crédito)</font>';
            }
        } elseif (in_array(intval($value['tb_estadoprecred_id']), array(5, 10))) {
            $estado_motivo .= ' - '.$value['tb_inconclusomotivo_nom'];
        }
        $tr .= "<tr>";
        $tr .=
            '<td id="tabla_fila" align="center">' . $value['tb_precred_id'] . '</td>
                <td id="tabla_fila">' . mostrar_fecha($value['tb_precred_fecreg']) . '</td>
                <td id="tabla_fila">' . $value['asesor'] . '</td>
                <td id="tabla_fila">' . substr($value['tb_empresa_nomcom'], 7) . '</td>
                <td id="tabla_fila">' . $value['tb_precliente_nombres'] . '</td>
                <td id="tabla_fila">' . strtoupper($value['tb_precred_producto']) . '</td>
                <td id="tabla_fila" align="center">'. $value['tb_moneda_nom'] .' '. mostrar_moneda($value['tb_precred_monto']) . '</td>
                <td id="tabla_fila" align="center">'. $value['tb_precred_int'] . '</td>
                <td id="tabla_fila">' . $modo_atencion[$value['tb_precred_modoatencion']] . '</td>
                <td id="tabla_fila">' . $estado_motivo . '</td>
                <td id="tabla_fila" align="center">
                    <a class="btn btn-info btn-xs" title="Ver" onclick="precredito_form(\'L\', ' . $value['tb_precred_id'] . ')"><i class="fa fa-eye"></i></a>';
        if ($value['tb_precred_usureg'] == $_SESSION['usuario_id'] || $_SESSION['usuario_id'] == 32 || $_SESSION['usuarioperfil_id'] == 1) {
            if (empty($value['tb_credito_id'])) {
                $tr .= ' <a class="btn btn-warning btn-xs" title="Modificar" onclick="precredito_form(\'M\', ' . $value['tb_precred_id'] . ')"><i class="fa fa-edit"></i></a>';
                if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
                    $tr .= ' <button class="btn btn-primary btn-xs" title="Reasignar" onclick=\'reasignar('. $value['tb_precred_id'] .')\'><i class="fa fa-exchange icon"></i></button>';
                }
                if ($value['tb_creditotipo_id'] == 1 && $value['tb_estadoprecred_id'] == 4) {
                    $tr .= ' <a class="btn btn-success btn-xs" title="Crédito completo" onclick="creditomenor_form(\'I\', '. $value['tb_precred_id'] .')"><i class="fa fa-check"></i></a>';
                }
            }
            $tr .= ' <a class="btn bg-navy btn-xs" title="Notas" onclick="notas_precred('.$value['tb_precred_id'].')"><i class="fa fa-newspaper-o"></i></a>';
            //$tr .= ' <a class="btn bg-teal btn-xs" title="Historial" onclick="historial_n('.$value['tb_precred_id'].')"><i class="fa fa-history"></i></a>';
            if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32){
                $tr .= ' <a class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminar(' . $value['tb_precred_id'] . ')"><i class="fa fa-trash"></i></a>';
            }
        }
        $tr .=  '</td>';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
unset($result);
?>

<input type="hidden" id="hdd_datatable_fil">
<div class="col-sm-12">
    <div id="div_precredito_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
        <table id="tbl_precreditos" class="table table-hover" style="word-wrap:break-word;">
            <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila" style="width: 2%;">Id</th>
                    <th id="tabla_cabecera_fila" style="width: 7%;">Fec. registro</th>
                    <th id="tabla_cabecera_fila" style="width: 12%;">Asesor</th>
                    <th id="tabla_cabecera_fila" style="width: 14;">Sede</th>
                    <th id="tabla_cabecera_fila" style="width: 15%;">Cliente interesado</th>
                    <th id="tabla_cabecera_fila" style="width: 15%;">Producto</th>
                    <th id="tabla_cabecera_fila" style="width: 6%;">Monto</th>
                    <th id="tabla_cabecera_fila" style="width: 5%;">% Int</th>
                    <th id="tabla_cabecera_fila" style="width: 7%;">Modo atención</th>
                    <th id="tabla_cabecera_fila" style="width: 14%;">Estado - Motivo inconcluso</th>
                    <th id="tabla_cabecera_fila" style="width: 8%;">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $tr; ?>
            </tbody>
        </table>
    </div>
</div>
