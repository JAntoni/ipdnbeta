<?php
require_once('../precredito/Precredito.class.php');
$oPrecredito = new Precredito();

if(!empty($_POST['tipocredito_id'])){
	$tipocredito_id = intval($_POST['tipocredito_id']);	
}
elseif (!empty($tipocredito_id)) {
	$tipocredito_id = intval($tipocredito_id);
}

if(!empty($_POST['estadoprecred_id'])){
	$estadoprecred_id = intval($_POST['estadoprecred_id']);	
}
elseif (!empty($estadoprecred_id)) {
	$estadoprecred_id = intval($estadoprecred_id);
}

$option = "<option value=\"0\" style=\"font-weight: bold;\">Seleccione</option>";

//PRIMER NIVEL
$result = $oPrecredito->listar_estadosprecred('tb_creditotipo_id', $tipocredito_id, 'INT');
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($estadoprecred_id == $value['tb_estadoprecred_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_estadoprecred_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_estadoprecred_nom'] . '</option>';
	}
}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>