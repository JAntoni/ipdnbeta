<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../precredito/Precredito.class.php');
$oPrecred = new Precredito();
require_once('../precliente/Precliente.class.php');
$oPrecliente = new Precliente();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../historial_nuevo/Historial_nuevo.class.php');
$oHistnuevo = new Historial_nuevo();
require_once('../upload/Upload.class.php');
$oUpload = new Upload();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action_precredito'];
$precredito_id = intval($_POST['hdd_precredito_id']);
$pcr_reg_origin = (array) json_decode($_POST['hdd_precredito_reg']);

$fecha_hora = date("d-m-Y h:i a");
$data['estado'] = 0;
$data['mensaje'] = "";
$data['pcr_id'] = $precredito_id;

if ($action == 'modificar_campo') {
	$columna = $_POST['columna'];
	$valor = $_POST['valor'];
	$tipo_dato = $_POST['tipo_dato'];

	if ($columna == 'tb_precred_notas') {
		$valor = strtoupper(trim($valor));
		$valor = $pcr_reg_origin['tb_precred_notas']."<span>- <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>: $valor | $fecha_hora</span><br>";
	}
	
	$res = $oPrecred->modificar_campo($precredito_id, $columna, $valor, $tipo_dato);

	if ($res == 1) {
		$data['estado'] = 1;
		$data['mensaje'] = "Éxito en la operación";

		//al eliminar el precredito, tambien se elimina su historial
		if ($columna == 'tb_precred_xac' && intval($valor) == 0) {
			$wh[0]['column_name'] = 'hn.tb_histnuevo_regmodid';
			$wh[0]['param0'] = $precredito_id;
			$wh[0]['datatype'] = 'INT';

			$r = $oHistnuevo->listar_todos($wh, array(), array());

			if ($r['estado'] == 1) {
				foreach ($r['data'] as $key => $v) {
					$oHistnuevo->modificar_campo($v['tb_histnuevo_id'], 'tb_histnuevo_xac', 0, 'INT');
				}
			}
			unset($wh, $r);

			// eliminar el proceso, pasar el proceso_id
			$precred = $oPrecred->mostrarUno($precredito_id);
			if($precred['estado'] == 1){
				$data['proceso_id'] = intval($precred['data']['tb_proceso_id']);
			}else{
				$data['proceso_id'] = 0;
			}
		}
		//al cambiar asesor tambien se debe guardar un historial
		elseif ($columna == 'tb_precred_usureg') {
			$asesor_nuevo_nombre = $_POST['asesornuevo_nombre'];
			//insertar historial del estado del precred
			$oHistnuevo->tablanom = 'tb_precred';
			$oHistnuevo->regmodid = $precredito_id;
			$oHistnuevo->columnamod = 'tb_precred_usureg';
			$oHistnuevo->usureg = $_SESSION['usuario_id'];
			$oHistnuevo->valorant = $pcr_reg_origin['tb_precred_usureg'];
			$oHistnuevo->valornuevo = $valor;
			$oHistnuevo->det = "Cambió el asesor de {$pcr_reg_origin['asesor']} => <b>$asesor_nuevo_nombre</b>";
			$oHistnuevo->insertar();
		}
		
	} else {
		$data['mensaje'] = "Error";
	}
	echo json_encode($data);
	unset($res);
	exit();
}

/* gerson (13-03-24) */
if($action == 'combo_credito_matriz'){
	$precliente_id	= intval($_POST['precli_id']);
	$cliente_id		= intval($_POST['cli_id']);

	if($cliente_id > 0){

		$creditos = $oPrecred->obtener_credito_x_cliente($cliente_id);

		if($creditos['estado'] == 1){

			$html .= '<label for="cbo_credito_id">Crédito Matriz:</label>
				<select id="cmb_creditomatriz_id" name="cmb_creditomatriz_id" class="form-control input-sm" data-live-search="true" data-max-options="1">
					<option value="">- Seleccione un crédito -</option>';
					foreach ($creditos['data'] as $key => $value) {
						$tip2 = '';
						$fecha = '';
						$moneda = '';
						$monto = 0.00;

						if ($value['tb_credito_tip'] == 1) $tip = "GARVEH";
						if ($value['tb_credito_tip'] == 2) $tip = "ADENDA";
						if ($value['tb_credito_tip'] == 3) $tip = "ACUERDO PAGO";
						if ($value['tb_credito_tip'] == 4) $tip = "GAR MOBILIARIA";
						if ($value['tb_credito_tip2'] == 1) $tip2 = " CREDITO REGULAR";
						if ($value['tb_credito_tip2'] == 2) $tip2 = " CREDITO ESPECÍFICO";
						if ($value['tb_credito_tip2'] == 3) $tip2 = " REPROGRAMADO";
						if ($value['tb_credito_tip2'] == 4) $tip2 = " CUOTA BALON";
						if ($value['tb_credito_tip2'] == 5) $tip2 = " REFINANCIADO AMORTIZADO";
						if ($value['tb_credito_tip2'] == 6) $tip2 = " REFINANCIADO";

						$fecha = mostrar_fecha($value['tb_credito_fecdes']);

						if ($value['tb_moneda_id'] == 1) $moneda = "S/.";
      					if ($value['tb_moneda_id'] == 2) $moneda = "US$";

						$monto = mostrar_moneda($value['tb_credito_preaco']);

						$html .= '<option value="'.$value['tb_credito_id'].'">'.$fecha.' | ('.$tip.' - '.$tip2.') ('.$value['tb_vehiculomarca_nom'] . ' / ' . $value['tb_vehiculomodelo_nom'].') | '.$moneda.' '.$monto.'</option>';
					}
			$html .= '</select>';

		}else{
			$html .= '<label for="cbo_credito_id">Crédito Matriz:</label>
				<select id="cmb_creditomatriz_id" name="cmb_creditomatriz_id" class="form-control input-sm" data-live-search="true" data-max-options="1">
					<option value="0">No cuenta con créditos a seleccionar</option>
				</select>';
		}

	}else{
		$html .= '<label for="cbo_credito_id">Crédito Matriz:</label>
				<select id="cmb_creditomatriz_id" name="cmb_creditomatriz_id" class="form-control input-sm" data-live-search="true" data-max-options="1">
					<option value="0">No cuenta con créditos a seleccionar</option>
				</select>';
	}

	echo $html;
	exit();
}
/*  */

$precliente_id	= intval($_POST['hdd_precli_id']);
$cliente_id		= $_POST['hdd_cli_id'];
$nrodocumento	= trim(strtoupper($_POST['txt_precli_nrodoc']));
$mediocom_id	= $_POST['cbo_mediocom_id'];
$tipo_doc		= strlen($nrodocumento) > 8 ? '4' : '1';
$nombres		= trim(strtoupper($_POST['txt_precli_nombres']));
$telefono		= trim($_POST['txt_precli_numcel']);
$empresa_id		= $_SESSION['empresa_id'];
$articulo_id	= $_POST['cbo_articulo_id'];
$creditotipo_id	= $_POST['cbo_creditotipo_id'];
$cgarvtipo_id	= $_POST['cbo_cgarvtipo_id'];
$modo_atencion	= $_POST['cbo_precred_modoatencion'];
$producto_nom	= trim(strtoupper($_POST['txt_precred_producto']));
$moneda_id		= $_POST['cbo_moneda_pagar'];
$monto			= moneda_mysql($_POST['txt_precred_monto']);
$interes		= $_POST['txt_interes'];
$url_tasacion	= trim($_POST['txt_precred_tasacionurl']);
$estado_precred = $_POST['cbo_estadoprecred_id'];
$motivo_inconcluso = intval($_POST['cbo_inconclusomotivo_id']);
$comentario		= trim(strtoupper($_POST['txt_precred_coment']));

/* gerson (15-03-24) */
if($action == 'insertar'){
	$tipo_adenda = (isset($_POST['cbo_tipoadenda'])) ? $_POST['cbo_tipoadenda'] : '';
	$credito_matriz_id = (isset($_POST['cmb_creditomatriz_id'])) ? $_POST['cmb_creditomatriz_id'] : 0;
}else{
	$tipo_adenda = (isset($_POST['hdd_tipoadenda'])) ? $_POST['hdd_tipoadenda'] : '';
	$credito_matriz_id = (isset($_POST['hdd_credito_valor'])) ? $_POST['hdd_credito_valor'] : 0;
}
/*  */

/* gerson (09-05-24) */
if (intval($creditotipo_id) == 3 && intval($cgarvtipo_id) == 6) {
	if(intval($credito_matriz_id) == 0){
		$data['mensaje'] = 'SI DESEA REGISTRAR UNA ADENDA, DEBE SELECCIONAR UN CREDITO MATRIZ';
		echo json_encode($data); exit();
	}
}
/*  */

$oPrecred->precliente_id = $precliente_id;
$oPrecred->empresa_id = $empresa_id;
$oPrecred->articulo_id = $articulo_id;
$oPrecred->creditotipo_id = $creditotipo_id;
$oPrecred->estadoprecred_id = $estado_precred;
$oPrecred->precred_modoatencion = $modo_atencion;
$oPrecred->precred_producto = $producto_nom;
$oPrecred->moneda_id = $moneda_id;
$oPrecred->precred_monto = $monto;
$oPrecred->precred_int = $interes;
$oPrecred->precred_urltasacion = $url_tasacion;
$oPrecred->inconclusomotivo_id = $motivo_inconcluso;
$oPrecred->precred_coment = $comentario;
$oPrecred->precred_usumod = $_SESSION['usuario_id'];
$oPrecred->precred_tipo_adenda = $tipo_adenda;
$oPrecred->creditomatriz_id = $credito_matriz_id;
$oPrecred->proceso_id = null;

//al insertar o modificar se verifica si el dni está en un registro de la bd con id diferente
	$where[0]['column_name'] = 'pcl.tb_precliente_numdoc';
	$where[0]['param0'] = $nrodocumento;
	$where[0]['datatype'] = 'STR';

	$where[1]['conector'] = 'AND';
	$where[1]['column_name'] = 'pcl.tb_precliente_id';
	$where[1]['param1'] = "<> $precliente_id";
	$res = $oPrecliente->listar_todos($where, array(), array());
	$dni_repetido = boolval($res['estado'] == 1);
	unset($where, $res);
//
if (!$dni_repetido && empty($cliente_id)) { //no está repetido en precliente. ahora revisar en tb_cliente
	$res = $oCliente->mostrarPorDni($nrodocumento);
	$dni_repetido = boolval($res['estado'] == 1);
}

if ($dni_repetido) {
	$data['mensaje'] = 'EL DNI ESTÁ REGISTRADO EN OTRO CLIENTE. REALICE LA BUSQUEDA Y ELIJA';
	echo json_encode($data); exit();
}

//buscar si existe un precred con xac: 1, que tenga a el id de ese precli, y que no tenga estado 4,5,9,10
	$wh[0]['column_name'] = 'pcr.tb_precred_xac';
	$wh[0]['param0'] = 1;
	$wh[0]['datatype'] = 'INT';

	$wh[1]['conector'] = 'AND';
	$wh[1]['column_name'] = 'pcr.tb_precliente_id';
	$wh[1]['param1'] = $precliente_id;
	$wh[1]['datatype'] = 'INT';

	$wh[2]['conector'] = 'AND';
	$wh[2]['column_name'] = 'pcr.tb_estadoprecred_id';
	$wh[2]['param2'] = 'NOT IN(4, 5, 9, 10)';

	if($action == 'modificar') {
		$wh[3]['conector'] = 'AND';
		$wh[3]['column_name'] = 'pcr.tb_precred_id';
		$wh[3]['param3'] = "<> $precredito_id";
	}

	$join[0]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
    $join[0]['tipo_union'] = 'LEFT';
    $join[0]['tabla_alias'] = 'tb_usuario us';
    $join[0]['columna_enlace'] = 'pcr.tb_precred_usureg';
    $join[0]['alias_columnaPK'] = 'us.tb_usuario_id';

	$join[1]['alias_columnasparaver'] = 'epcr.tb_estadoprecred_nom';
    $join[1]['tipo_union'] = 'LEFT';
    $join[1]['tabla_alias'] = 'tb_estadoprecred epcr';
    $join[1]['columna_enlace'] = 'pcr.tb_estadoprecred_id';
    $join[1]['alias_columnaPK'] = 'epcr.tb_estadoprecred_id';

	$res = $oPrecred->listar_todos($wh, $join, array());
	if ($res['estado'] == 1) {
		$sede = array(1 => 'BOULEVARD', 2=> 'MALL AVENTURA');
		$moneda = array(1 => 'S/.', 2=> 'US$');
		$data['mensaje'] = "No se puede $action la solicitud de precredito con este cliente, dado que ya ";
		$data['mensaje'] .= 'tiene otras solicitudes en proceso. Solicitudes con ID: <br>';
		$n = 0;
		foreach ($res['data'] as $key => $pcr) {
			$data['mensaje'] .= $n>0 ? '<br>' : '';
			$data['mensaje'] .= "• {$pcr['tb_precred_id']}. Atendió el asesor {$pcr['asesor']}, el día ". mostrar_fecha_hora($pcr['tb_precred_fecreg'])." en sede {$sede[$pcr['tb_empresa_id']]}, ";
			$data['mensaje'] .= " {$pcr['tb_precred_producto']}, {$moneda[$pcr['tb_moneda_id']]} ". mostrar_moneda($pcr['tb_precred_monto']).", con estado {$pcr['tb_estadoprecred_nom']}";
			$n++;
		}
		unset($n);
		echo json_encode($data);
		exit();
	}
	unset($res, $wh);
//

//al insertar o modificar se verifica si hay cambios en el precliente
	$pcl_reg_origin = (array) json_decode($_POST['hdd_precli_reg']);
	$pcl_igual[0] = boolval($nrodocumento == $pcl_reg_origin['precliente_numdoc']);
	$pcl_igual[1] = boolval($nombres == $pcl_reg_origin['precliente_nombres']);
	$pcl_igual[2] = boolval($telefono == $pcl_reg_origin['precliente_numcel']);
	$pcl_igual[3] = boolval($tipo_doc == $pcl_reg_origin['precliente_tipodoc']);
	$pcl_igual[4] = boolval($mediocom_id == $pcl_reg_origin['mediocom_id']);
//

//ESTADOS DEL PRECREDITO
$estadoprecred_nombre = array(1 => 'INTERESADO', 2 => 'SEGUIMIENTO', 3 => 'VISITA', 4 => 'CONCLUIDO', 5 => 'RECHAZADO', 
	6 => 'INTERESADO', 7 => 'SEGUIMIENTO', 8 => 'VISITA', 9 => 'CONCLUIDO', 10 => 'RECHAZADO');
//

if ($action == 'insertar') {
	$oPrecred->precred_usureg = $_SESSION['usuario_id'];
	if (intval($creditotipo_id) == 3) {
		$oPrecred->cgarvtipo_id = $cgarvtipo_id;
	}

	if (intval($modo_atencion) == 1 && in_array(intval($estado_precred), array(3, 8))) {
		$estados_falt = intval($estado_precred) == 3 ? array(1, 3) : array(6, 8);
	}
	elseif (intval($modo_atencion) == 1 && intval($estado_precred) == 4) {
		$estados_falt = array(1, 3, 4);
	}
	elseif (!in_array(intval($estado_precred), array(1, 6))) {//está rechazado
		if (intval($modo_atencion) == 1) {
			$estados_falt = intval($estado_precred) == 5 ? array(1, 3, 5) : array(6, 8, 10);
		} else {
			$estados_falt = intval($estado_precred) == 5 ? array(1, 5) : array(6, 10);
		}
	}

	if (empty($precliente_id)) {
		$oPrecliente->precliente_usumod = $_SESSION['usuario_id'];
		$oPrecliente->precliente_usureg = $_SESSION['usuario_id'];
		$oPrecliente->precliente_numdoc = $nrodocumento;
		$oPrecliente->precliente_nombres = $nombres;
		$oPrecliente->precliente_numcel = $telefono;
		$oPrecliente->precliente_tipodoc = $tipo_doc;
		$oPrecliente->mediocom_id = $mediocom_id;

		if (!empty($cliente_id)) {
			$oPrecliente->cliente_id = $cliente_id;

			if (!$pcl_igual[0] && empty($pcl_reg_origin['precliente_numdoc'])) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_doc', $nrodocumento, 'STR');
			}
			if (!$pcl_igual[1]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_nom', $nombres, 'STR');
			}
			if (!$pcl_igual[2]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');
			}
		}

		//insertar cliente
		$res = $oPrecliente->insertar();

		if ($res['estado'] == 1) {
			$oPrecred->precliente_id = $res['nuevo'];
			$data['mensaje'] = empty($cliente_id) ? '● Se registró nuevo cliente. <br>' : '';
		} else {
			$data['mensaje'] = "● Error en el registro de precliente";
			echo json_encode($data);
			exit();
		}
		unset($res);
	} else {
		if (!empty($cliente_id)) {
			if (!$pcl_igual[1]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_nom', $nombres, 'STR');
			}
			if (!$pcl_igual[2]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');
			}
		}

		if (!$pcl_igual[0]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_numdoc', $nrodocumento, 'STR');
		}
		if (!$pcl_igual[1]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_nombres', $nombres, 'STR');
		}
		if (!$pcl_igual[2]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_numcel', $telefono, 'STR');
		}
		if (!$pcl_igual[3]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_tipodoc', $tipo_doc, 'STR');
		}
		if (!$pcl_igual[4]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_mediocom_id', $mediocom_id, 'INT');
		}
	}

	$res = $oPrecred->insertar();
	
	/* gerson (15-03-24) */
	if($cgarvtipo_id == 6){ // si es adenda
		$oPrecred->modificar_campo($res['nuevo'], 'tb_precred_tipo_adenda', $tipo_adenda, 'STR');
		$oPrecred->modificar_campo($res['nuevo'], 'tb_creditomatriz_id', $credito_matriz_id, 'INT');
	}
	/*  */

	$oHistnuevo->tablanom = 'tb_precred';
	$oHistnuevo->regmodid = $res['nuevo'];
	$oHistnuevo->columnamod = 'tb_estadoprecred_id';
	$oHistnuevo->usureg = $_SESSION['usuario_id'];
	$oHistnuevo->valorant = null;
	$oHistnuevo->valornuevo = intval($creditotipo_id) == 3 ? 6 : 1;
	$oHistnuevo->det = "Registró el precredito, con estado de <b>INTERESADO</b>";
	$oHistnuevo->insertar();

	if ($res['estado'] == 1) {
		$data['estado'] = 1;
		$data['mensaje'] .= "● ".$res['mensaje'] . ". ID: " . $res['nuevo'];
		$data['pcr_id'] = $res['nuevo'];

		if (!empty($estados_falt)) {
			for ($i = 1; $i < count($estados_falt); $i++) {
				$oPrecred->modificar_campo($res['nuevo'], 'tb_estadoprecred_id', $estados_falt[$i], 'INT');
				
				$oHistnuevo->valorant = $estados_falt[$i-1];
				$oHistnuevo->valornuevo = $estados_falt[$i];
				$oHistnuevo->det = "Cambió el estado del precredito de {$estadoprecred_nombre[$estados_falt[$i-1]]} => <b>{$estadoprecred_nombre[$estados_falt[$i]]}</b>";
				$oHistnuevo->insertar();
			}
		}

		if ($estado_precred == 4) {
			$data['form_cm'] = 1;
			$data['form_garveh'] = 0;
		} elseif ($estado_precred == 9) {
			//aqui colocar si hay una redireccion a procesos
			$data['form_cm'] = 0;
			$data['form_garveh'] = 1;
			$data['creditotipo_id'] = $creditotipo_id;
			$data['cgarvtipo_id'] = $cgarvtipo_id;
			$data['precliente_id'] = $precliente_id;
			/* gerson (16-03-24) */
			if($cgarvtipo_id == 6){ // si es adenda
				$data['credito_matriz_id'] = $credito_matriz_id;
			}else{
				$data['credito_matriz_id'] = 0;
			}
			/* gerson (15-06-24) */
			// si es credito garveh se envía el precred_id para actualizar el proceso_id
			$data['precred_id'] = $res['nuevo'];
			/*  */
		}
	} else {
		$data['data'] = $res;
	}
	unset($res);
}
elseif($action == 'modificar'){
	if (intval($creditotipo_id) == 3) {
		$oPrecred->cgarvtipo_id = $cgarvtipo_id;
	}

	if (empty($precliente_id)) {
		$oPrecliente->precliente_usumod = $_SESSION['usuario_id'];
		$oPrecliente->precliente_usureg = $_SESSION['usuario_id'];
		$oPrecliente->precliente_numdoc = $nrodocumento;
		$oPrecliente->precliente_nombres = $nombres;
		$oPrecliente->precliente_numcel = $telefono;
		$oPrecliente->precliente_tipodoc = $tipo_doc;
		$oPrecliente->mediocom_id = $mediocom_id;

		if (!empty($cliente_id)) {
			$oPrecliente->cliente_id = $cliente_id;

			if (!$pcl_igual[0] && empty($pcl_reg_origin['precliente_numdoc'])) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_doc', $nrodocumento, 'STR');
			}
			if (!$pcl_igual[1]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_nom', $nombres, 'STR');
			}
			if (!$pcl_igual[2]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');
			}
		}

		//insertar cliente
		$res = $oPrecliente->insertar();

		if ($res['estado'] == 1) {
			$oPrecred->precliente_id = $res['nuevo'];
			$data['mensaje'] = empty($cliente_id) ? '● Se registró nuevo cliente. <br>' : '';
		} else {
			$data['mensaje'] = "● Error en el registro de precliente";
			echo json_encode($data);
			exit();
		}
		unset($res);
	} else {
		if (!empty($cliente_id)) {
			if (!$pcl_igual[1]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_nom', $nombres, 'STR');
			}
			if (!$pcl_igual[2]) {
				$oCliente->modificar_campo($cliente_id, 'tb_cliente_tel', $telefono, 'STR');
			}
		}

		$precliente_modificado = false;
		if (!$pcl_igual[0]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_numdoc', $nrodocumento, 'STR');
			$precliente_modificado = true;
		}
		if (!$pcl_igual[1]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_nombres', $nombres, 'STR');
			$precliente_modificado = true;
		}
		if (!$pcl_igual[2]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_numcel', $telefono, 'STR');
			$precliente_modificado = true;
		}
		if (!$pcl_igual[3]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_precliente_tipodoc', $tipo_doc, 'STR');
			$precliente_modificado = true;
		}
		if (!$pcl_igual[4]) {
			$oPrecliente->modificar_campo($precliente_id, 'tb_mediocom_id', $mediocom_id, 'INT');
			$precliente_modificado = true;
		}
		if ($precliente_modificado) {
			$data['mensaje'] = '● Se modificó al cliente. <br>';
		}
	}

	$igual[0] = boolval($oPrecred->precliente_id == $pcr_reg_origin['tb_precliente_id']);
	$igual[1] = boolval($oPrecred->creditotipo_id == $pcr_reg_origin['tb_creditotipo_id']);
	$igual[2] = boolval($oPrecred->articulo_id == $pcr_reg_origin['tb_articulo_id']);
	$igual[3] = boolval($oPrecred->precred_producto == $pcr_reg_origin['tb_precred_producto']);
	$igual[4] = boolval($oPrecred->precred_monto == $pcr_reg_origin['tb_precred_monto']);
	$igual[5] = boolval($oPrecred->precred_coment == $pcr_reg_origin['tb_precred_coment']);
	$igual[6] = boolval($oPrecred->precred_modoatencion == $pcr_reg_origin['tb_precred_modoatencion']);
	$igual[7] = boolval($oPrecred->inconclusomotivo_id == $pcr_reg_origin['tb_inconclusomotivo_id']);
	$igual[8] = boolval($oPrecred->cgarvtipo_id == $pcr_reg_origin['cgarvtipo_id']);
	$igual[9] = boolval($oPrecred->moneda_id == $pcr_reg_origin['tb_moneda_id']);
	$igual[10] = boolval($oPrecred->estadoprecred_id == $pcr_reg_origin['tb_estadoprecred_id']);
	$igual[11] = boolval($oPrecred->precred_int == $pcr_reg_origin['tb_precred_int']);
	$igual[12] = boolval($oPrecred->precred_urltasacion == $pcr_reg_origin['tb_precred_urltasacion']);

	$son_iguales = true;
	$i = 0;
	while ($i < count($igual)) {
		if (!$igual[$i]) {
			$son_iguales = false;
		}
		$i++;
	}
	$i = 0;

	if ($son_iguales) {
		$data['estado'] = 2;
		$data['mensaje'] .= "● No se realizó ninguna modificacion en solicitud pre-credito";
	}
	else {
		$oPrecred->precred_id = $precredito_id;

		if (in_array($estado_precred, array(4, 9)) && in_array($pcr_reg_origin['tb_estadoprecred_id'], array(1, 2, 5, 7, 10))) {
			if ($estado_precred == 4) {
				$estados_falt = array($pcr_reg_origin['tb_estadoprecred_id'], 3, 4);
			} else {
				$estados_falt = array($pcr_reg_origin['tb_estadoprecred_id'], 8, 9);
			}
			$oPrecred->estadoprecred_id = $estados_falt[1];
		}

		$res = $oPrecred->modificar();
		if ($res == 1) {
			$data['estado'] = 1;
			$data['mensaje'] .= '● Solicitud pre-credito modificado correctamente';

			if (!$igual[10]) {
				//insertar historial del estado del precred
				$oHistnuevo->tablanom = 'tb_precred';
				$oHistnuevo->regmodid = $precredito_id;
				$oHistnuevo->columnamod = 'tb_estadoprecred_id';
				$oHistnuevo->usureg = $_SESSION['usuario_id'];
				$oHistnuevo->valorant = $pcr_reg_origin['tb_estadoprecred_id'];
				$oHistnuevo->valornuevo = $oPrecred->estadoprecred_id;
				$oHistnuevo->det = "Cambió el estado del precredito de {$estadoprecred_nombre[$pcr_reg_origin['tb_estadoprecred_id']]} => <b>{$estadoprecred_nombre[$oPrecred->estadoprecred_id]}</b>";
				$oHistnuevo->insertar();

				if (!empty($estados_falt)) {
					$oPrecred->modificar_campo($precredito_id, 'tb_estadoprecred_id', $estados_falt[2], 'INT');

					$oHistnuevo->valorant = $estados_falt[1];
					$oHistnuevo->valornuevo = $estados_falt[2];
					$oHistnuevo->det = "Cambió el estado del precredito de {$estadoprecred_nombre[$estados_falt[1]]} => <b>{$estadoprecred_nombre[$estados_falt[2]]}</b>";
					$oHistnuevo->insertar();
				}

				if ($estado_precred == 4) {
					$data['form_cm'] = 1;
					$data['form_garveh'] = 0;
				} elseif ($estado_precred == 9) {
					//aqui colocar si hay una redireccion a procesos
					$data['form_cm'] = 0;
					$data['form_garveh'] = 1;
					$data['creditotipo_id'] = intval($pcr_reg_origin['tb_creditotipo_id']);
					$data['cgarvtipo_id'] = intval($pcr_reg_origin['cgarvtipo_id']);
					$data['precliente_id'] = intval($pcr_reg_origin['tb_precliente_id']);
					/* gerson (16-03-24) */
					if($cgarvtipo_id == 6){ // si es adenda
						$data['credito_matriz_id'] = intval($pcr_reg_origin['tb_creditomatriz_id']);
					}else{
						$data['credito_matriz_id'] = 0;
					}
					// si es credito garveh se envía el precred_id para actualizar el proceso_id
					$data['precred_id'] = $precredito_id;
					/*  */
				}
			}
		}
		unset($res);
	}
}


echo json_encode($data);
//$data['mensaje'] = var_export($estados_falt);//$oPrecred
//echo json_encode($data); exit();
