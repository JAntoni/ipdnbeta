<?php
$usuario_id = $_SESSION['usuario_id'];
if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
    $usuario_id = '';
}
$filtro_fec1 = date('01-m-Y');
$filtro_fec2 = date('d-m-Y');
$vista = 2;
if ($_SESSION['usuarioperfil_id'] == 1 || $_SESSION['usuario_id'] == 32) {
    $vista = 1;
}
?>
<form id="form_precredito_filtro" role="form" class="form-inline">
    <div class="form-group" style="padding-right: 15px;">
        <button type="button" class="btn btn-primary btn-sm" onclick="precredito_form('I',0)">
            <i class="fa fa-plus"></i> Nuevo
        </button>
    </div>

    <div class="form-group" style="padding-right: 12px;">
        <label for="txt_fil_precliente_nom" style="padding-right: 2px;">Cliente:</label>
        <input type="text" name="txt_fil_precliente_nom" id="txt_fil_precliente_nom" placeholder="Ingrese N° Documento o Nombre de Cliente" class="form-control input-sm ui-autocomplete-input input-shadow" autocomplete="off" size="40" value>
        <input type="hidden" name="hdd_fil_precliente_id" id="hdd_fil_precliente_id">
    </div>

    <div class="form-group" style="padding-right: 12px;">
    <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">
        <label for="cbo_fil_asesor" style="padding-right: 2px;">Asesor:</label>
        <select name="cbo_fil_asesor" id="cbo_fil_asesor" class="wrap form-control mayus input-shadow input-sm"><!-- select2 narrow , data-select2-id="select2-data-cbo_fil_asesor" -->
            <?php require_once(VISTA_URL . 'usuario/usuario_select.php'); ?>
        </select>
    </div>

    <div class="form-group" style="padding-right: 12px;">
        <label style="padding-right: 2px;">Del:</label>
        <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control input-sm input-shadow" name="txt_fil_fec1" id="txt_fil_fec1" style="width: 80px;" value="<?php echo $filtro_fec1; ?>"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
        <label style="padding: 1px;">al:</label>
        <div class='input-group date' id='datetimepicker2'>
            <input type='text' class="form-control input-sm input-shadow" name="txt_fil_fec2" id="txt_fil_fec2" style="width: 80px;" value="<?php echo $filtro_fec2; ?>"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>

    <div class="form-group" style="padding-right: 12px;">
        <label for="cbo_fil_sede" style="padding-right: 2px;">Sede:</label>
        <select name="cbo_fil_sede" id="cbo_fil_sede" class="form-control mayus input-shadow input-sm" style="width: 150px;">
            <?php require_once(VISTA_URL . 'empresa/empresa_select.php'); ?>
        </select>
    </div>

    <div class="form-group" style="padding-right: 12px;">
        <label for="cbo_fil_cretip" style="padding-right: 2px;">Tipo credito:</label>
        <select name="cbo_fil_cretip" id="cbo_fil_cretip" class="form-control mayus input-shadow input-sm" style="width: 150px;">
            <?php require_once(VISTA_URL . 'creditotipo/creditotipo_select.php'); ?>
        </select>
    </div>

    <!-- botones -->
    <button type="button" id="btn_buscar" class="btn btn-info btn-sm" onclick="precredito_contenido()" title="Buscar">
        <i class="fa fa-search icon"></i>
    </button>

    <div class="form-group" style="padding-left: 5px;">
        <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista;?>">
        <button id="btn_cambiar_vista" type="button" class="btn btn-github btn-sm" title="Cambiar vista"><i class="fa fa-exchange icon"></i></button>
    </div>
</form>
