<?php
if (!empty($_GET['pcr_id'])) {
    require_once("../precredito/Precredito.class.php");
    $oPcr = new Precredito();

    $pcr_id = intval($_GET['pcr_id']);

    $where[0]['column_name'] = 'tb_precred_id';
    $where[0]['param0'] = $pcr_id;
    $where[0]['datatype'] = 'INT';

    $join[0]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as asesor";
    $join[0]['tipo_union'] = 'LEFT';
    $join[0]['tabla_alias'] = 'tb_usuario us';
    $join[0]['columna_enlace'] = 'pcr.tb_precred_usureg';
    $join[0]['alias_columnaPK'] = 'us.tb_usuario_id';

    $pcr_actual = $oPcr->listar_todos($where, $join, array())['data'][0];

    $asesor_actual_nom = $pcr_actual['asesor'];
    $asesor_actual_id = $pcr_actual['tb_precred_usureg'];
}
?>

<form action="" class="formName">
    Está cambiando el asesor responsable
    <?php if (!empty($_GET['pcr_id'])) {
        echo ' de esta solicitud precredito<p></p>'; ?>
        <input type="hidden" name="hdd_precredito_reg" id="hdd_precredito_reg" value='<?php echo json_encode($pcr_actual);?>'>
        <div class="form-group">
            <label>Asesor (actual):</label>
            <input type="hidden" name="hdd_asesor_a" id="hdd_asesor_a" value="<?php echo $asesor_actual_id; ?>">
            <input type="text" class="form-control input-sm input-shadow" style="cursor: text;" disabled value="<?php echo $asesor_actual_nom; ?>"/>
        </div>
    <?php } else {
        echo ' de este grupo de precréditos<p></p>';
    } ?>

    <div class="form-group">
        <label>Asignar a:</label>
        <select type="text" class="form-control input-sm input-shadow asesor_n">
            <?php require_once('../usuario/usuario_select.php'); ?>
        </select>
    </div>
</form>
