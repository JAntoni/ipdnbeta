<?php
  //require_once('../../core/usuario_sesion.php');
  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');


  $fecha_hoy = date('Y-m-d');

  $mes = $_POST['reporte_mes'];
  $anio = $_POST['reporte_anio'];
  $empresa_id = intval($_POST['empresa_id']);

  $garveh_pre = 0;
  $garveh_const_s = 0;
  $garveh_const_c = 0;
  $garveh_compra_s = 0;
  $garveh_compra_c = 0;
  $garveh_adenda = 0;

  $garveh_concluido_pre = 0;
  $garveh_concluido_const_s = 0;
  $garveh_concluido_const_c = 0;
  $garveh_concluido_compra_s = 0;
  $garveh_concluido_compra_c = 0;
  $garveh_concluido_adenda = 0;

  $result = $oProceso->reporte_proceso_general($mes, $anio, $empresa_id);
    if($result['estado'] == 1){
      $return['estado'] = 1;
      
      $data .= '
        <div class="table-responsive">
          <table id="tbl_proceso_reporte1" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th style="background-color: #3c8dbc; color: #fff;">TIPO DE GARANTÍA</th>
                <th style="background-color: #3c8dbc; color: #fff;">ATENCIONES</th>
                <th style="background-color: #3c8dbc; color: #fff;">PROCESOS CREADOS</th>
                <th style="background-color: #3c8dbc; color: #fff;">OPERACIONES CONCLUIDAS</th>
                <th style="background-color: #3c8dbc; color: #fff;">ATENCIONES PERDIDAS</th>
                <th style="background-color: #3c8dbc; color: #fff;">PROCESOS PERDIDOS</th>
              </tr>
            </thead>
            <tbody id="lista_creditos">
      ';

      // precreditos
      $result1 = $oProceso->lista_precredito($mes, $anio, $empresa_id);
      $precred_pre = 0;
      $precred_const_s = 0;
      $precred_const_c = 0;
      $precred_compra_s = 0;
      $precred_compra_c = 0;
      $precred_adenda = 0;
      foreach ($result1['data'] as $key => $value) {
        $precred_pre = $value['cant_pre'];
        $precred_const_s = $value['cant_const_s'];
        $precred_const_c = $value['cant_const_c'];
        $precred_compra_s = $value['cant_compra_s'];
        $precred_compra_c = $value['cant_compra_c'];
        $precred_adenda = $value['cant_adenda'];
      }
      //


      // procesos
      foreach ($result['data'] as $key => $value) {
        
        $class_concluido = 'class="success"';
        $class_perdido = 'class="danger"';

        $garveh_pre = $value['cant_pre'];
        $garveh_const_s = $value['cant_const_s'];
        $garveh_const_c = $value['cant_const_c'];
        $garveh_compra_s = $value['cant_compra_s'];
        $garveh_compra_c = $value['cant_compra_c'];
        $garveh_adenda = $value['cant_adenda'];

        $garveh_concluido_pre = $value['concluido_pre'];
        $garveh_concluido_const_s = $value['concluido_const_s'];
        $garveh_concluido_const_c = $value['concluido_const_c'];
        $garveh_concluido_compra_s = $value['concluido_compra_s'];
        $garveh_concluido_compra_c = $value['concluido_compra_c'];
        $garveh_concluido_adenda = $value['concluido_adenda'];
      }
      //

      $data .= '
          <tr>
            <td>PRECONSTITUCIÓN</td>
            <td>'.$precred_pre.'</td>
            <td>'.$garveh_pre.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_pre.'</td>
            <td '.$class_perdido.'>'.($precred_pre - $garveh_pre).'</td>
            <td '.$class_perdido.'>'.($garveh_pre - $garveh_concluido_pre).'</td>
          </tr>
          <tr>
            <td>CONSTITUCION SIN CUSTODIA</td>
            <td>'.$precred_const_s.'</td>
            <td>'.$garveh_const_s.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_const_s.'</td>
            <td '.$class_perdido.'>'.($precred_const_s - $garveh_const_s).'</td>
            <td '.$class_perdido.'>'.($garveh_const_s - $garveh_concluido_const_s).'</td>
          </tr>
          <tr>
            <td>CONSTITUCION CON CUSTODIA</td>
            <td>'.$precred_const_c.'</td>
            <td>'.$garveh_const_c.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_const_c.'</td>
            <td '.$class_perdido.'>'.($precred_const_c - $garveh_const_c).'</td>
            <td '.$class_perdido.'>'.($garveh_const_c - $garveh_concluido_const_c).'</td>
          </tr>
          <tr>
            <td>CONSTITUCION - COMPRAVENTA A UN TERCERO SIN CUSTODIA</td>
            <td>'.$precred_compra_s.'</td>
            <td>'.$garveh_compra_s.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_compra_s.'</td>
            <td '.$class_perdido.'>'.($precred_compra_s - $garveh_compra_s).'</td>
            <td '.$class_perdido.'>'.($garveh_compra_s - $garveh_concluido_compra_s).'</td>
          </tr>
          <tr>
            <td>CONSTITUCION - COMPRAVENTA A UN TERCERO CON CUSTODIA</td>
            <td>'.$precred_compra_c.'</td>
            <td>'.$garveh_compra_c.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_compra_c.'</td>
            <td '.$class_perdido.'>'.($precred_compra_c - $garveh_compra_c).'</td>
            <td '.$class_perdido.'>'.($garveh_compra_c - $garveh_concluido_compra_c).'</td>
          </tr>
          <tr>
            <td>ADENDA</td>
            <td>'.$precred_adenda.'</td>
            <td>'.$garveh_adenda.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_adenda.'</td>
            <td '.$class_perdido.'>'.($precred_adenda - $garveh_adenda).'</td>
            <td '.$class_perdido.'>'.($garveh_adenda - $garveh_concluido_adenda).'</td>
          </tr>';

      $data .= '
          </tbody>
          <tfooter>
            <tr>
              <th></th>
              <th style="background-color: #3c8dbc; color: #fff;">'.($precred_pre + $precred_const_s + $precred_const_c + $precred_compra_s + $precred_compra_c + $precred_adenda).'</th>
              <th style="background-color: #3c8dbc; color: #fff;">'.($garveh_pre + $garveh_const_s + $garveh_const_c + $garveh_compra_s + $garveh_compra_c + $garveh_adenda).'</th>
              <th style="background-color: #3c8dbc; color: #fff;">'.($garveh_concluido_pre + $garveh_concluido_const_s + $garveh_concluido_const_c + $garveh_concluido_compra_s + $garveh_concluido_compra_c + $garveh_concluido_adenda).'</th>
              <th style="background-color: #3c8dbc; color: #fff;">'.(($precred_const_s - $garveh_const_s) + ($precred_const_s - $garveh_const_s) + $garveh_concluido_const_c + $garveh_concluido_compra_s + $garveh_concluido_compra_c + $garveh_concluido_adenda).'</th>
              <th style="background-color: #3c8dbc; color: #fff;">'.(($garveh_pre - $garveh_concluido_pre) + ($garveh_const_s - $garveh_concluido_const_s) + ($garveh_const_c - $garveh_concluido_const_c) + ($garveh_compra_s - $garveh_concluido_compra_s) + ($garveh_compra_c - $garveh_concluido_compra_c) + ($garveh_adenda - $garveh_concluido_adenda)).'</th>
            </tr>
          </tfooter>
        </table>
      </div>
      ';

    }else{

      $return['estado'] = 0;
      $data .= '
        <div class="table-responsive">
          <table id="tbl_proceso_reporte" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>TIPO DE GARANTÍA</th>
                <th>ATENCIONES</th>
                <th>PROCESOS CREADOS</th>
                <th>OPERACIONES CONCLUIDAS</th>
                <th>ATENCIONES PERDIDAS</th>
                <th>PROCESOS PERDIDOS</th>
              </tr>
            </thead>
            <tbody id="lista_creditos">
              <tr>
                <td colspan="23">'.$result['mensaje'].'</td>
              </tr>
            </tbody>
          </table>
        </div>
      ';
      
    }
  $result = NULL;
  
  $return['tabla'] = $data;
  $return['inputs'] = $inputs;
  $return['reporte'] = 1;
  echo json_encode($return);
?>