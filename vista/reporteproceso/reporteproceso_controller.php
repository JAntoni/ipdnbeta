<?php

    require_once('../../core/usuario_sesion.php');
    require_once('../usuario/Usuario.class.php');
    $oUsuario = new Usuario();
    
    $action = $_POST['action'];

    if($action == 'obtener_colaborador_sede'){
        $sede_id = intval($_POST['sede_id']);

        $data = '';

        $colaborares_option = '<option value="0">TODOS</option>';
        $result = $oUsuario->listar_trabajadores_sede($sede_id);
        if($result['estado'] == 1){
            foreach ($result['data'] as $key => $value) {
                $colaborares_option .= '<option value="'.$value['tb_usuario_id'].'">'.$value['tb_usuario_nom'].' '.$value['tb_usuario_ape'].'</option>';
            }
            $data = $colaborares_option;
        }
        $result = NULL;

        echo $data;
    }

?>