<?php  
  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");
  /* require_once('Chart.class.php');
  $oChart = new Chart(); */
  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();

  $mes = $_POST['mes'];
  $anio = $_POST['anio'];
  $empresa_id = intval($_POST['empresa_id']);
  
  $result = $oProceso->reporte_proceso_general($mes,$anio,$empresa_id);

  $data = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {

      $garveh_concluido_pre = $value['concluido_pre'];
      $garveh_concluido_const_s = $value['concluido_const_s'];
      $garveh_concluido_const_c = $value['concluido_const_c'];
      $garveh_concluido_compra_s = $value['concluido_compra_s'];
      $garveh_concluido_compra_c = $value['concluido_compra_c'];
      $garveh_concluido_adenda = $value['concluido_adenda'];

      $data .= '
          <tr>
            <td>PRECONSTITUCIÓN</td>
            <td>'.$garveh_concluido_pre.'</td>
          </tr>
          <tr>
            <td>CONSTITUCION SIN CUSTODIA</td>
            <td>'.$garveh_concluido_const_s.'</td>
          </tr>
          <tr>
            <td>CONSTITUCION CON CUSTODIA</td>
            <td>'.$garveh_concluido_const_c.'</td>
          </tr>
          <tr>
            <td>CONSTITUCION - COMPRAVENTA A UN TERCERO SIN CUSTODIA</td>
            <td>'.$garveh_concluido_compra_s.'</td>
          </tr>
          <tr>
            <td>CONSTITUCION - COMPRAVENTA A UN TERCERO CON CUSTODIA</td>
            <td>'.$garveh_concluido_compra_c.'</td>
          </tr>
          <tr>
            <td>ADENDA</td>
            <td>'.$garveh_concluido_adenda.'</td>
          </tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_piecharts" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>TIPO GARANTÍA</th>
      <th>VENTAS</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $data;?>
  </tbody>
</table>
