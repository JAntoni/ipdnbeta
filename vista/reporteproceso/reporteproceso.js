var datatable_global;
var array_deudadiaria;
var arrayfinal = [];
google.charts.load('current', {'packages':['corechart']});
google.charts.load('current', {'packages':['corechart', 'bar']});

function reporteSeleccionado() {
    let reporte_tipo = parseInt($('#reporte_tipo').val());
    if(reporte_tipo == 1){
        $('#div_colaborador').addClass('hidden');
    }else if(reporte_tipo == 2){
        $('#div_colaborador').removeClass('hidden');
    }else{

    }
}

function cargarColaboradores() {
    let sede_id = $('#empresa_id').val();
    $.ajax({
		type: "POST",
		url: VISTA_URL+"reporteproceso/reporteproceso_controller.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'obtener_colaborador_sede',
            sede_id: sede_id
        }),
        success: function(data){
            if(data != ''){
                $('#colaborador_id').html(data);
            }
        },
        complete: function(data){
                
        },
        error: function(data){
            console.log(data);
        }
	});
}

function temp_reporte_lista(){
    let reporte_tipo = parseInt($('#reporte_tipo').val());
    let url = '';
    if(reporte_tipo == 1){
        url = VISTA_URL+"reporteproceso/reporteproceso1_tabla.php";
    }else if(reporte_tipo == 2){
        url = VISTA_URL+"reporteproceso/reporteproceso2_tabla.php";
    }
    $.ajax({
      type: "POST",
      url: url,
      async: true,
      dataType: "JSON",
      data: $('#form_fil_reporteproceso').serialize(),
      beforeSend: function() {
        $('#reporteproceso_mensaje_tbl').show(300);
        $('#span_cargando').text('Cargando Tabla Proceso...');
        $('#div_tabla_contenido').html('');
      },
      success: function(data){
        if(parseInt(data.estado) == 1)
          //datatable_global.destroy();
        $('#reporteproceso_mensaje_tbl').hide(300);
        
        $('#div_tabla_contenido').html(data.tabla);
  
        if(parseInt(data.estado) == 1){
          
          estilos_datatable();
  
          /* $('input[type="checkbox"].flat-yellow, input[type="radio"].flat-green').iCheck({
            checkboxClass: 'icheckbox_flat-yellow',
            radioClass: 'iradio_flat-yellow'
          }); */
        }

        if(parseInt(data.reporte) == 1){
          piechart_tabla_reporte1()
          cargar_datachart1()
        }
      },
      complete: function(data){
        //console.log(data)
      },
      error: function(data){
        console.log(data);
        $('#reporteproceso_mensaje_tbl').html('ERROR AL CARGAR DATOS DE PROCESO: ' + data.responseText);
      }
    });
}

function estilos_datatable(){
    datatable_global = $('#tbl_proceso_reporte1, #tbl_proceso_reporte2').DataTable({
      "pageLength": 50,
      "responsive": true,
      "bScrollCollapse": true,
      "bPaginate": false,
      "bJQueryUI": true,
      "scrollX": true,
      scrollY: 700,
      fixedColumns: {
        left: 1
      },
      //"scrollY": "80vh",
      //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
      dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
      buttons: [
        { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
        { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
        { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
        { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
        { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
      ],
      "language": {
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "Ninguna coincidencia para la búsquedad",
        "info": "Mostrado _END_ registros",
        "infoEmpty": "Ningún registro disponible",
        "infoFiltered": "(filtrado de _MAX_ registros totales)",
        "search": "Buscar"
      },
      order: [],
      columnDefs: [
        { targets: "no-sort", orderable: false}
      ]
      /*drawCallback: function () {
        var sum_desembolsado = $('#tbl_comportamiento').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
        var sum_capital_rest_sol = $('#tbl_comportamiento').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles
  
        $('#total').html(sum);
      }*/
    });

}

function piechart_tabla_reporte1(){
  var mes = $('#reporte_mes').val();
  var anio = $('#reporte_anio').val();
  var empresa_id = $('#empresa_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL+"reporteproceso/piechart_tabla.php",
    async: true,
    dataType: "html",
    data: ({
        mes: mes,
        anio: anio,
        empresa_id: empresa_id
    }),
    beforeSend: function() {
      $('#piechart_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_piechart_tabla_reporte1').html(data);
      drawChart();
      $('#piechart_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#piechart_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function cargar_datachart1(){
  var mes = $('#reporte_mes').val();
  var anio = $('#reporte_anio').val();
  var empresa_id = $('#empresa_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL+"reporteproceso/chart_controller.php",
    async:true,
    dataType: "json",                      
    data: ({
        mes: mes,
        anio: anio,
        empresa_id: empresa_id,
        grafico:"piechart"
    }),
    success: function(data){
        var arraydatos = [];
      for(let i=0; i<data.length;i++) {
          let creditos = parseInt(data[i][1]);
          arraydatos.push([data[i][0],creditos]);
      }
      arraydatos.unshift(['Tipo Garantía','Ventas']);
      arrayfinal = arraydatos;
      console.log(arraydatos);

      google.charts.setOnLoadCallback(drawChart);
    }
  });
  piechart_tabla_reporte1();
}

function drawChart() {

  var data = google.visualization.arrayToDataTable(arrayfinal);

  console.log(data);

  var options = {
    title: 'VENTAS POR GARANTÍA',
    pieSliceText: 'value', 
     is3D: false
  };

  var chart = new google.visualization.PieChart(document.getElementById('piechart'));

  chart.draw(data, options);
}