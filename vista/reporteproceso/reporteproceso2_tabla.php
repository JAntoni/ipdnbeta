<?php
  //require_once('../../core/usuario_sesion.php');
  require_once('../proceso/Proceso.class.php');
  $oProceso = new Proceso();
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');


  $fecha_hoy = date('Y-m-d');

  $mes = $_POST['reporte_mes'];
  $anio = $_POST['reporte_anio'];
  $empresa_id = intval($_POST['empresa_id']);

  $SUMA_TOTAL_DESEMBOLSADO = 0;
  $SUMA_TOTAL_CAPITAL_RECUPERAR = 0;
  $SUMA_TOTAL_FACTURADO = 0;
  $SUMA_TOTAL_COBRADO = 0;
  $SUMA_TOTAL_POR_COBRAR = 0;
  $DEUDA_ACUMULADA = 0;

  $garveh_pre = 0;
  $garveh_const_s = 0;
  $garveh_const_c = 0;
  $garveh_compra_s = 0;
  $garveh_compra_c = 0;
  $garveh_adenda = 0;

  $garveh_concluido_pre = 0;
  $garveh_concluido_const_s = 0;
  $garveh_concluido_const_c = 0;
  $garveh_concluido_compra_s = 0;
  $garveh_concluido_compra_c = 0;
  $garveh_concluido_adenda = 0;

  $result = $oProceso->reporte_proceso_general($mes, $anio, $empresa_id);
    if($result['estado'] == 1){
      $return['estado'] = 1;
      
      $data .= '
        <div class="table-responsive">
          <table id="tbl_proceso_reporte2" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th style="background-color: #3c8dbc; color: #fff;">#</th>
                <th style="background-color: #3c8dbc; color: #fff;">ASESOR</th>
                <th style="background-color: #3c8dbc; color: #fff;">ATENCIONES</th>
                <th style="background-color: #3c8dbc; color: #fff;">PROCESOS CREADOS</th>
                <th style="background-color: #3c8dbc; color: #fff;">FINALIZADOS EN VENTA</th>
                <th style="background-color: #3c8dbc; color: #fff;">MONTO VENTA SOLES (S/)</th>
                <th style="background-color: #3c8dbc; color: #fff;">MONTO VENTA DÓLARES ($)</th>
                <th style="background-color: #3c8dbc; color: #fff;">GARANTÍA MÁS VENDIDA</th>
              </tr>
            </thead>
            <tbody id="lista_creditos">
      ';

      // precreditos
      $result1 = $oProceso->lista_precredito($mes, $anio, $empresa_id);
      $precred_pre = 0;
      $precred_const_s = 0;
      $precred_const_c = 0;
      $precred_compra_s = 0;
      $precred_compra_c = 0;
      $precred_adenda = 0;
      foreach ($result1['data'] as $key => $value) {
        $precred_pre = $value['cant_pre'];
        $precred_const_s = $value['cant_const_s'];
        $precred_const_c = $value['cant_const_c'];
        $precred_compra_s = $value['cant_compra_s'];
        $precred_compra_c = $value['cant_compra_c'];
        $precred_adenda = $value['cant_adenda'];
      }
      //


      // procesos
      foreach ($result['data'] as $key => $value) {
        
        $class_concluido = 'class="success"';
        $class_perdido = 'class="danger"';

        /* $class_td = 'class="success"';
        $class_atraso = 'class="success"';
        $class_inactivo = 'class="success"';
        $class_porcentaje = 'class="danger"';

        $cuota_estado = 'Pendiente';
        if($value['tb_cuotadetalle_est'] == 2)
          $cuota_estado = 'Cancelado';
        if($value['tb_cuotadetalle_est'] == 3)
          $cuota_estado = 'Pago Parcial';

        if(strtotime($fecha_hoy) > strtotime($value['cuota_fecha'])){ //todo codigo aquí dentro es para cuando la cuota está vencida
          $estado_credito = 'Vencido';
          $class_td = 'class="danger"';
        }
        if(intval($value['dias_atraso']) > 20 && intval($value['dias_atraso']) <= 30)
          $class_atraso = 'class="warning"';
        if(intval($value['dias_atraso']) > 30)
          $class_atraso = 'class="danger"';

        if(intval($value['inactividad']) > 20 && intval($value['inactividad']) <= 30)
          $class_inactivo = 'class="warning"';
        if(intval($value['inactividad']) > 30)
          $class_inactivo = 'class="danger"';

        if(intval($value['ultimo_pago']) >= 50 && intval($value['ultimo_pago']) < 80)
          $class_porcentaje = 'class="warning"';
        if(intval($value['ultimo_pago']) >= 80)
          $class_porcentaje = 'class="success"';
        
        //vamos a obtener sus opciones asignadas
        $ultimopago_fecha = $value['ultimopago_fecha'];

        $opcionesgc_nom = $value['tb_opcionesgc_nom'];
        $resultado_nom = $value['resultado_nom'];
        $comentario_des = $value['comentario_des'];
        $fecha_pdp = $value['opciongc_fechapdp']; // esta fecha es la promesa de pago, mediante esta fecha evaluamos si cumplió o no
        if($value['tb_opcionesgc_id'] == 1 || $value['tb_opcionesgc_id'] == 4)
          $opcionesgc_nom = $value['tb_opcionesgc_nom'].' <b>'.mostrar_fecha($fecha_pdp).'</b>';

        //definimos el estado que tendrá la columna resultado que depende de la opción asignada
        if($value['tb_opcionesgc_id'] == 1){ //la columna de RESULTADO netamente depende de los compromisos de pago, cualquiero otra opción esta debe estar varcía
          //fechas que necesitamos: fecha hoy, fecha de ultimo pago, fecha compromiso de pago
          $fecha_hoy_menos1 = date("Y-m-d", strtotime($fecha_hoy."- 1 days")); //obtenemos fecha de ayer para comparar si hubo un pago ayer con el compromiso
          if($ultimopago_fecha == $fecha_pdp){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'SI CUMPLIO', 'STR');
            $resultado_nom = 'SI CUMPLIÓ';
          }
          if(strtotime($fecha_pdp) > strtotime($fecha_hoy_menos1)){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'ESPERANDO PDP', 'STR');
            $resultado_nom = 'ESPERANDO PDP';
          }
          if(strtotime($fecha_pdp) < strtotime($fecha_hoy) && strtotime($ultimopago_fecha) < strtotime($fecha_pdp)){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'NO CUMPLIO', 'STR');
            $resultado_nom = 'NO CUMPLIÓ';
          }
          if(strtotime($ultimopago_fecha) > strtotime($fecha_pdp)){
            $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', 'ASIGNAR NUEVO PDP', 'STR');
            $resultado_nom = 'ASIGNAR NUEVO PDP';
          }
        }
        else{
          $oAsignar->modificar_campo_asignar_opciongc($value['credito_id'], $value['cuotadetalle_id'], 'resultado_nom', '', 'STR');
        }

        $DEUDA_ACUMULADA += $value['deuda_acumulada']; */
        
        $garveh_pre = $value['cant_pre'];
        $garveh_const_s = $value['cant_const_s'];
        $garveh_const_c = $value['cant_const_c'];
        $garveh_compra_s = $value['cant_compra_s'];
        $garveh_compra_c = $value['cant_compra_c'];
        $garveh_adenda = $value['cant_adenda'];

        $garveh_concluido_pre = $value['concluido_pre'];
        $garveh_concluido_const_s = $value['concluido_const_s'];
        $garveh_concluido_const_c = $value['concluido_const_c'];
        $garveh_concluido_compra_s = $value['concluido_compra_s'];
        $garveh_concluido_compra_c = $value['concluido_compra_c'];
        $garveh_concluido_adenda = $value['concluido_adenda'];
      }
      //

      $data .= '
          <tr>
            <td>PRECONSTITUCIÓN</td>
            <td>'.$precred_pre.'</td>
            <td>'.$garveh_pre.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_pre.'</td>
            <td '.$class_perdido.'>'.($precred_pre - $garveh_pre).'</td>
            <td '.$class_perdido.'>'.($garveh_pre - $garveh_concluido_pre).'</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>CONSTITUCION SIN CUSTODIA</td>
            <td>'.$precred_const_s.'</td>
            <td>'.$garveh_const_s.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_const_s.'</td>
            <td '.$class_perdido.'>'.($precred_const_s - $garveh_const_s).'</td>
            <td '.$class_perdido.'>'.($garveh_const_s - $garveh_concluido_const_s).'</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>CONSTITUCION CON CUSTODIA</td>
            <td>'.$precred_const_c.'</td>
            <td>'.$garveh_const_c.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_const_c.'</td>
            <td '.$class_perdido.'>'.($precred_const_c - $garveh_const_c).'</td>
            <td '.$class_perdido.'>'.($garveh_const_c - $garveh_concluido_const_c).'</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>CONSTITUCION - COMPRAVENTA A UN TERCERO SIN CUSTODIA</td>
            <td>'.$precred_compra_s.'</td>
            <td>'.$garveh_compra_s.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_compra_s.'</td>
            <td '.$class_perdido.'>'.($precred_compra_s - $garveh_compra_s).'</td>
            <td '.$class_perdido.'>'.($garveh_compra_s - $garveh_concluido_compra_s).'</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>CONSTITUCION - COMPRAVENTA A UN TERCERO CON CUSTODIA</td>
            <td>'.$precred_compra_c.'</td>
            <td>'.$garveh_compra_c.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_compra_c.'</td>
            <td '.$class_perdido.'>'.($precred_compra_c - $garveh_compra_c).'</td>
            <td '.$class_perdido.'>'.($garveh_compra_c - $garveh_concluido_compra_c).'</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>ADENDA</td>
            <td>'.$precred_adenda.'</td>
            <td>'.$garveh_adenda.'</td>
            <td '.$class_concluido.'>'.$garveh_concluido_adenda.'</td>
            <td '.$class_perdido.'>'.($precred_adenda - $garveh_adenda).'</td>
            <td '.$class_perdido.'>'.($garveh_adenda - $garveh_concluido_adenda).'</td>
            <td></td>
            <td></td>
          </tr>';


      $data .= '
          </tbody>
        </table>
      </div>
      ';

    }else{

      $return['estado'] = 0;
      $data .= '
        <div class="table-responsive">
          <table id="tbl_proceso_reporte" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>TIPO DE GARANTÍA</th>
                <th>ATENCIONES</th>
                <th>PROCESOS CREADOS</th>
                <th>OPERACIONES CONCLUIDAS</th>
                <th>ATENCIONES PERDIDAS</th>
                <th>PROCESOS PERDIDOS</th>
              </tr>
            </thead>
            <tbody id="lista_creditos">
              <tr>
                <td colspan="23">'.$result['mensaje'].'</td>
              </tr>
            </tbody>
          </table>
        </div>
      ';
      
    }
  $result = NULL;
  
  $alerta_deudadiaria = '';
  
  $inputs['suma_total_desembolsado'] = mostrar_moneda($SUMA_TOTAL_DESEMBOLSADO);
  $inputs['suma_total_capital_recuperar'] = mostrar_moneda($SUMA_TOTAL_CAPITAL_RECUPERAR);
  $inputs['suma_capital_recuperado'] = mostrar_moneda($SUMA_CAPITAL_RECUPERADO);

  $inputs['suma_total_facturado'] = mostrar_moneda($SUMA_TOTAL_FACTURADO);
  $inputs['suma_total_cobrado'] = mostrar_moneda($SUMA_TOTAL_COBRADO);
  $inputs['suma_total_por_cobrar'] = mostrar_moneda($SUMA_TOTAL_POR_COBRAR);
  $inputs['alerta_deudadiaria'] = $alerta_deudadiaria;
  $inputs['deuda_acumulada'] = $DEUDA_ACUMULADA;

  $return['tabla'] = $data;
  $return['inputs'] = $inputs;
  echo json_encode($return);
?>