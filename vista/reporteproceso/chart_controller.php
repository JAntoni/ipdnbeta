<?php
	session_name("ipdnsac");
        session_start();

 	/* require_once('Chart.class.php');
        $oChart = new Chart(); */
        require_once('../proceso/Proceso.class.php');
        $oProceso = new Proceso();
        require_once ("../funciones/funciones.php");
        require_once ("../funciones/fechas.php");

        $mes = $_POST['mes'];
        $anio = $_POST['anio'];
        $empresa_id= intval($_POST['empresa_id']);
        $grafico = $_POST['grafico'];
        
        if($grafico=="piechart"){
            $array = array();
            $result = $oProceso->reporte_proceso_general($mes,$anio,$empresa_id);
            if($result['estado'] == 1){
                 foreach ($result['data'] as $key => $value) {
                     
                     $garveh_concluido_pre = intval($value['concluido_pre']);
                     $garveh_concluido_const_s = intval($value['concluido_const_s']);
                     $garveh_concluido_const_c = intval($value['concluido_const_c']);
                     $garveh_concluido_compra_s = intval($value['concluido_compra_s']);
                     $garveh_concluido_compra_c = intval($value['concluido_compra_c']);
                     $garveh_concluido_adenda = intval($value['concluido_adenda']);
                     if($garveh_concluido_pre > 0){
                        $array_temporal = array();
                        array_push($array_temporal,'PRECONSTITUCIÓN');
                        array_push($array_temporal,$garveh_concluido_pre);
                        array_push($array,$array_temporal);
                     }
                     if($garveh_concluido_const_s > 0){
                        $array_temporal = array();
                        array_push($array_temporal,'CONSTITUCION SIN CUSTODIA');
                        array_push($array_temporal,$garveh_concluido_const_s);
                        array_push($array,$array_temporal);
                     }
                     if($garveh_concluido_const_c > 0){
                        $array_temporal = array();
                        array_push($array_temporal,'CONSTITUCION CON CUSTODIA');
                        array_push($array_temporal,$garveh_concluido_const_c);
                        array_push($array,$array_temporal);
                     }
                     if($garveh_concluido_compra_s > 0){
                        $array_temporal = array();
                        array_push($array_temporal,'COMPRAVENTA A UN TERCERO SIN CUSTODIA');
                        array_push($array_temporal,$garveh_concluido_compra_s);
                        array_push($array,$array_temporal);
                     }
                     if($garveh_concluido_compra_c > 0){
                        $array_temporal = array();
                        array_push($array_temporal,'COMPRAVENTA A UN TERCERO CON CUSTODIA');
                        array_push($array_temporal,$garveh_concluido_compra_c);
                        array_push($array,$array_temporal);
                     }
                     if($garveh_concluido_adenda > 0){
                        $array_temporal = array();
                        array_push($array_temporal,'ADENDA');
                        array_push($array_temporal,$garveh_concluido_adenda);
                        array_push($array,$array_temporal);
                     }

                 }

                echo json_encode($array);
            }
            $result="";
        }
       /*  elseif($grafico=="chart_div"){
            $array = array();
            $result = $oChart->listarreportegeneral($sede,$fecha1,$fecha2);
            if($result['estado'] == 1){
                 foreach ($result['data'] as $key => $value) {
                     $array_temporal = array();
                     array_push($array_temporal,$value['Vendedor']);
                     array_push($array_temporal,$value['creditos']);
                     array_push($array_temporal,$value['monto']);
                     array_push($array,$array_temporal);
                 }

                echo json_encode($array);
            }
            $result="";
        }
        elseif($grafico=="chart_divaño"){
            $array = array();
            $result = $oChart->listarreporteporaño($sede, $año);
            if($result['estado'] == 1){
                 foreach ($result['data'] as $key => $value) {
                     if($value['tb_empresa_id']==1){
                         $sede = 'B';
                     }
                     else{
                         $sede = 'M';
                     }
                     $array_temporal = array();
                     array_push($array_temporal,$value['nombremes']);
                     array_push($array_temporal,$value['creditos']);
                     array_push($array_temporal,$value['monto']);
                     array_push($array,$array_temporal);
                 }

                echo json_encode($array);
            }
            $result="";
        } */
        
      
?>