<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<?php require_once('reporteproceso_filtro.php');?>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="reporteproceso_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla Proceso...</span></h4>
				</div>
				
				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success class_comportamiento">
					<div class="box-header with-border">
						<h3 class="box-title">REPORTE</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div id="div_tabla_contenido" class="box-body">
						
					</div>
				</div>

				<?php 
					$fecha = new DateTime();
					$fecha->modify('last day of this month');

					$chart_fec1 = date('01-m-Y');
					$chart_fec2 = $fecha->format('d-m-Y');
				?>

				<!-- GRÁFICO DEUDA ACUMULADA POR DIA -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">GRÁFICA</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="piechart"></div>
							</div>
							<div class="col-md-6">
								<div id="div_piechart_tabla_reporte1" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
								
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="box-body">
						<div id="chart-deudadiaria" style="height: 400px;">
							
						</div>
					</div> -->
				</div>

				

				<!-- REPORTE DE ASISTENCIA VEHICULAR-->
				<!-- <div id="div_asiveh_tabla">
				</div> -->

				<!-- REPORTE FACTURADO GARANTIA VEHICULAR-->
				<!-- <div id="div_garveh_tabla">
				</div> -->

				<!-- REPORTE FACTURADO GARANTIA HIPOTECARIO-->
				<!-- <div id="div_hipo_tabla">
				</div> -->
				
			</div>
			<!-- <div id="div_modal_opcionesgc_form">
			</div>

			<div id="div_modal_opcionesgc_asignar_form">
			</div>

			<div id="div_modal_opcionesgc_timeline">
			</div>

			<div id="div_modal_cliente_contacto">
			</div> -->

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
