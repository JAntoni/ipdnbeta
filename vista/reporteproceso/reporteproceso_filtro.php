<?php 
  $fecha = new DateTime();
  $fecha->modify('last day of this month');

  $cuota_fec1 = date('01-m-Y');
  $cuota_fec2 = $fecha->format('d-m-Y');

  /* require_once('core/usuario_sesion.php');
  require_once('vista/usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('vista/usuarioperfil/UsuarioPerfil.class.php');
  $oUsuarioPerfil = new UsuarioPerfil();
  require_once('Personal.class.php');
  $oPersonal = new Personal(); */

  require_once('vista/funciones/funciones.php');
  require_once('vista/funciones/fechas.php');

  $usu_id = $_SESSION['usuario_id'];
  $hoy = date('Y-m-d');
  $mes_actual = date('m');
  $anio_actual = date('Y');

?>

<style>
  .label-filter{
    padding-left: 1%;
    padding-right: 1%;
    font-size: 12pt;
  }
  .select-filter{
    width: 10%;
  }
</style>
  
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info" style="margin-bottom: -12px !Important;">
        <div class="box-body shadow">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-primary">
                <div class="panel-body">
                  <form id="form_fil_reporteproceso" class="form-inline" role="form" method="POST">

                    <div class="py-2">
                      <label for="reporte_tipo" class="control-label label-filter">Opción de Reporte:</label>
                      <div class="form-group select-filter">
                        <select id="reporte_tipo" name="reporte_tipo" class="selectpicker form-control input-sm col-12" onchange="reporteSeleccionado()" data-live-search="true" data-max-options="1" data-size="12">
                          <option value="1">REPORTE GENERAL DE PROCESOS GARVEH</option>
                          <option value="2">REPORTE PRODUCTIVIDAD DE ASESORES</option>
                        </select>
                      </div>
                    </div>

                    <br>

                    <div id="filtro_div" class="py-1" style="">
                      <div class="form-group">
                        <label class="control-label">Mes:</label>
                        <select class="form-control form-control-sm input-sm" name="reporte_mes" id="reporte_mes">
                          <?php echo devuelve_option_nombre_meses($mes_actual);?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Año:</label>
                        <select class="form-control form-control-sm input-sm" name="reporte_anio" id="reporte_anio">
                          <?php echo devuelve_option_anios_garveh(2020, $anio_actual, 4);?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Sede:</label>
                        <select class="form-control form-control-sm input-sm" name="empresa_id" id="empresa_id" onchange="cargarColaboradores()">
                          <option value="0">TODAS</option>
                          <!-- <option value="1">IPDN - BOULEVARD</option>
                          <option value="2">IPDN - MALL AVENTURA</option> -->
                        </select>
                      </div>
                      <div id="div_colaborador" class="form-group hidden">
                        <label class="control-label">Colaborador:</label>
                        <select class="form-control form-control-sm input-sm" name="colaborador_id" id="colaborador_id">
                          <option value="0">TODOS</option>
                        </select>
                      </div>

                      <button type="button" class="btn btn-info" onclick="temp_reporte_lista()"><i class="fa fa-search"></i> Buscar</button>

                    </div>

                  </form>
                </div>
              </div>
            </div>
    
          </div>
        </div>
      </div>
    </div>
    
  </div>


