<?php
if (defined('APP_URL'))    
require_once(APP_URL . 'datos/conexion.php');
else   
require_once('../../datos/conexion.php');
class Almacen extends Conexion {    
    public $tb_almacen_id;    
    public $tb_almacen_nom;    
    public $tb_almacen_ven;    
    public $tb_empresa_id;    
    function insertar(){      
        $this->dblink->beginTransaction();      
        try {        
            $sql = "INSERT tb_almacen (                                
            tb_almacen_nom,                                
            tb_almacen_ven,                                
            tb_empresa_id)            		
            VALUES (                                
            :tb_almacen_nom,                                
            :tb_almacen_ven,                                
            :tb_empresa_id);";         
    $sentencia = $this->dblink->prepare($sql);        
    $sentencia->bindParam(":tb_almacen_nom", $this->tb_almacen_nom, PDO::PARAM_STR);        
    $sentencia->bindParam(":tb_almacen_ven", $this->tb_almacen_ven, PDO::PARAM_INT);        
    $sentencia->bindParam(":tb_empresa_id", $this->tb_empresa_id, PDO::PARAM_INT);        
    
    $result = $sentencia->execute();        
    $this->dblink->commit();        
    return $result; 
    //si es correcto el ingreso retorna 1      
        } catch (Exception $e) {        
            $this->dblink->rollBack();        
            throw $e;      
        }    
    }    
    function modificar(){      
        $this->dblink->beginTransaction();      
        try {        
            $sql = "UPDATE tb_almacen SET                                      
            tb_almacen_nom = :tb_almacen_nom,                                    
            tb_almacen_ven =  :tb_almacen_ven                            
            WHERE                                     
            tb_almacen_id =:tb_almacen_id";         
            $sentencia = $this->dblink->prepare($sql);        
            $sentencia->bindParam(":tb_almacen_nom", $this->tb_almacen_nom, PDO::PARAM_STR);        
            $sentencia->bindParam(":tb_almacen_ven", $this->tb_almacen_ven, PDO::PARAM_INT);        
            $sentencia->bindParam(":tb_empresa_id", $this->tb_almacen_id, PDO::PARAM_INT);        
            $result = $sentencia->execute();        $this->dblink->commit();        
            
            return $result; 
            //si es correcto el ingreso retorna 1      
        } catch (Exception $e) {        
            $this->dblink->rollBack();        
            throw $e;      
        }    
    }                
    
    function mostrarTodos($emp_id){            
        try {              
            $sql="SELECT                             
            *                     
            FROM                             
            tb_almacen                    
            WHERE                             
            tb_empresa_id = :tb_empresa_id                            
            ORDER BY tb_almacen_nom";              
            $sentencia = $this->dblink->prepare($sql);              
            $sentencia->bindParam(":tb_empresa_id", $emp_id, PDO::PARAM_INT);              
            $sentencia->execute();              
            if ($sentencia->rowCount() > 0) {                
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);                
                $retorno["estado"] = 1;                
                $retorno["mensaje"] = "exito";                
                $retorno["data"] = $resultado;                
                $sentencia->closeCursor(); 
                //para libera memoria de la consulta              
            }              
            else{                
                $retorno["estado"] = 0;               
                $retorno["mensaje"] = "No hay tipos de crédito registrados";                
                $retorno["data"] = "";              
            }              
            return $retorno;            
        } catch (Exception $e) {              
            throw $e;            
        }       
    }                
    
    function mostrar_por_empresa($emp_id){            
        try {                
            $sql="SELECT                             
            *                     
            FROM                            
             tb_almacen                    
            WHERE                             
             tb_empresa_id = :tb_empresa_id                            
            ORDER BY tb_almacen_id";              
            $sentencia = $this->dblink->prepare($sql);              
            $sentencia->bindParam(":tb_empresa_id", $emp_id, PDO::PARAM_INT);              
            $sentencia->execute();              
            
            if ($sentencia->rowCount() > 0) {                
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);                
                $retorno["estado"] = 1;                
                $retorno["mensaje"] = "exito";                
                $retorno["data"] = $resultado;                
                
                $sentencia->closeCursor(); 
                //para libera memoria de la consulta              
            }              
            else{                
                $retorno["estado"] = 0;                
                $retorno["mensaje"] = "No hay tipos de crédito registrados";                
                $retorno["data"] = "";              
            }              
            
        return $retorno;            
        } catch (Exception $e) {              
            throw $e;            
        }        
    }        
    
    function mostrar_para_venta($emp_id){           
        try {                
            $sql="SELECT *                     
            FROM tb_almacen                    
            WHERE tb_almacen_ven=1 AND tb_empresa_id = :tb_empresa_id                    
            ORDER BY tb_almacen_id";              
            
            $sentencia = $this->dblink->prepare($sql);              
            $sentencia->bindParam(":tb_empresa_id", $emp_id, PDO::PARAM_INT);              
            $sentencia->execute();              
            if ($sentencia->rowCount() > 0) {                
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);                
                $retorno["estado"] = 1;                
                $retorno["mensaje"] = "exito";                
                $retorno["data"] = $resultado;                
                $sentencia->closeCursor(); 
                //para libera memoria de la consulta              
            }else{                
                $retorno["estado"] = 0;                
                $retorno["mensaje"] = "No hay tipos de crédito registrados";                
                $retorno["data"] = "";              
            }              
            return $retorno;            
        } catch (Exception $e) {              
            throw $e;            
        }        
    }                
    
    function mostrarUno($id){            
        try {              
            $sql="SELECT                             
            *                     
            FROM                             
            tb_almacen                    
            WHERE                             
            tb_almacen_id=:tb_almacen_id ";              
            $sentencia = $this->dblink->prepare($sql);              
            $sentencia->bindParam(":tb_almacen_id", $id, PDO::PARAM_INT);              
            $sentencia->execute();              
            
            if ($sentencia->rowCount() > 0) {                
                $resultado = $sentencia->fetch();                
                $retorno["estado"] = 1;                
                $retorno["mensaje"] = "exito";                
                $retorno["data"] = $resultado;                
                $sentencia->closeCursor(); 
                //para libera memoria de la consulta              
            }              
            else{                
                $retorno["estado"] = 0;                
                $retorno["mensaje"] = "No hay tipos de crédito registrados";                
                $retorno["data"] = "";              
            }              
            return $retorno;            
        } catch (Exception $e) {              
            throw $e;            
        }       
    }                
    
    function verifica_almacen_tabla($id,$tabla){            
        try {              
            $sql = "SELECT * 		
            FROM  $tabla 		
            WHERE tb_almacen_id =$id";             
            $sentencia = $this->dblink->prepare($sql);              
            $sentencia->bindParam(":tb_almacen_id", $id, PDO::PARAM_INT);              
            $sentencia->execute();              
            if ($sentencia->rowCount() > 0) {                
                $resultado = $sentencia->fetch();                
                $retorno["estado"] = 1;                
                $retorno["mensaje"] = "exito";                
                $retorno["data"] = $resultado;                
                $sentencia->closeCursor(); 
                //para libera memoria de la consulta              
            }              
            else{                
                $retorno["estado"] = 0;                
                $retorno["mensaje"] = "No hay tipos de crédito registrados";                
                $retorno["data"] = "";              
            }              
            return $retorno;            
        } catch (Exception $e) {              
            throw $e;            
        }        
    }               
    
    function eliminar($id){            
        $this->dblink->beginTransaction();            
        try {              
            $sql="DELETE FROM tb_almacen WHERE tb_almacen_id=$id";              
            $sentencia = $this->dblink->prepare($sql);             
            $sentencia->bindParam(":tb_almacen_id", $id, PDO::PARAM_INT);              
            $result = $sentencia->execute();              
            $this->dblink->commit();              
            return $result; 
            //si es correcto retorna 1            
        } catch (Exception $e) {              
            $this->dblink->rollBack();              
            throw $e;            
        }        
    }
    
}
?>