<?php

require_once('Almacen.class.php');
$oAlmacen = new Almacen();
$option = '<option value="0">Todos</option>';

$almacen_id = (isset($_POST['almacen_id']))? intval($_POST['almacen_id']) : intval($almacen_id); //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

//PRIMER NIVEL
$result = $oAlmacen->mostrarTodos();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($almacen_id == $value['tb_almacen_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_almacen_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_almacen_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>