<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../tarifacochera/TarifaCochera.class.php');
  $oTarifaCochera = new TarifaCochera();

  require_once '../funciones/funciones.php';
  require_once '../funciones/fechas.php';
  
  $action = $_POST['action'];

  $oTarifaCochera->tarifacochera_fecinicio = fecha_mysql($_POST['tarifacochera_fecinicio']);
  $oTarifaCochera->tarifacochera_fecfinal = fecha_mysql($_POST['tarifacochera_fecfinal']);
  $oTarifaCochera->tarifacochera_criteriohora = intval($_POST['tarifacochera_criteriohora']);
  $oTarifaCochera->cochera_id = intval($_POST['hdd_cochera_id']);

  if($action == 'insertar'){
    
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Usuario Perfil.';

    if($oTarifaCochera->insertar()){
      $data['estado'] = 1;
      $data['mensaje'] = 'Usuario Perfil registrado correctamente.';
    }

    echo json_encode($data);
  }
  if($action == 'tabla'){
    $td_tarifacochera = '';
    $result = $oTarifaCochera->listar_tarifacochera();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $td_tarifacochera .= '
            <tr id="tabla_cabecera_fila">
              <td id="tabla_fila">'.$value['tb_tarifacochera_id'].'</td>
              <td id="tabla_fila">'.$value['tb_tarifacochera_fecinicio'].'</td>
              <td id="tabla_fila">'.$value['tb_tarifacochera_fecfinal'].'</td>
              <td id="tabla_fila">'.$value['tb_tarifacochera_criteriohora'].'</td>
              <td id="tabla_fila" align="center">
                <a class="btn btn-warning btn-xs" title="Asignar" onclick="cochera_tarifadetalle(\'I\',0,'.$value['tb_tarifacochera_id'].')"><i class="fa fa-plus"></i> Asignar</a>
              </td>
            </tr>';
        }
      }
    $result = NULL;
    
    $tabla = '
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">FECHA DE INICIO</th>
        <th id="tabla_cabecera_fila">FECHA DE TERMINO</th>
        <th id="tabla_cabecera_fila">CRITERIO DE HORA</th>
        <th id="tabla_cabecera_fila">AGREGAR DETALLE</th>
      </tr>' . $td_tarifacochera;

    echo $tabla;
  }
?>