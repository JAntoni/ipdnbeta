<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class TarifaCochera extends Conexion{

    public $tarifacochera_fecinicio; 
    public $tarifacochera_fecfinal; 
    public $tarifacochera_criteriohora; 
    public $cochera_id;
    

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tarifacochera(
                tb_tarifacochera_fecinicio, 
                tb_tarifacochera_fecfinal, 
                tb_tarifacochera_criteriohora, 
                tb_cochera_id)
                VALUES (
                :tarifacochera_fecinicio, 
                :tarifacochera_fecfinal, 
                :tarifacochera_criteriohora, 
                :cochera_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifacochera_fecinicio", $this->tarifacochera_fecinicio, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifacochera_fecfinal", $this->tarifacochera_fecfinal, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifacochera_criteriohora", $this->tarifacochera_criteriohora, PDO::PARAM_STR);
        $sentencia->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }

    /*function modificar($opcionesgc_id, $opcionesgc_nom, $opcionesgc_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_opcionesgc SET tb_opcionesgc_nom =:opcionesgc_nom, tb_opcionesgc_des =:opcionesgc_des WHERE tb_opcionesgc_id =:opcionesgc_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_id", $opcionesgc_id, PDO::PARAM_INT);
        $sentencia->bindParam(":opcionesgc_nom", $opcionesgc_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":opcionesgc_des", $opcionesgc_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }*/

    /*function mostrarUno($opcionesgc_id){
      try {
        $sql = "SELECT * FROM tb_opcionesgc WHERE tb_opcionesgc_id =:opcionesgc_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_id", $opcionesgc_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }*/
    
    function listar_tarifacochera(){
      try {
        $sql = "SELECT * FROM tb_tarifacochera WHERE tb_tarifacochera_xac = 1 AND  tb_cochera_id = :cochera_id" ;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cochera_id", $this->cochera_id, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>