<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../tarifacochera/TarifaCochera.class.php');
  $oTarifaCochera = new TarifaCochera();
  require_once('../funciones/funciones.php');

  $direc = 'tarifacochera';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $tarifacochera_id = $_POST['tarifacochera_id'];
  $cochera_id = $_POST['cochera_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Tarifa Registrada';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Tarifa ';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Tarifa ';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Tarifa ';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_tarifacochera" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo;?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="directorio" value="tarifacochera">
        <label for="txt_filtro" class="control-label">Registro de nueva Tarifa</label>
        <div class="panel panel-primary shadow-simple">
          <div class="panel-body">
            <form id="form_tarifacochera" class="form-inline" role="form">
                <input type="hidden" name="hdd_cochera_id" id='hdd_cochera_id' value="<?php echo $cochera_id;?>">
                  <div class="panel-body">
                    <div class="row">
                      <div class="form-group col-md-6">
                        <label for="txt_tarifacochera_fecinicio" class="control-label" style="margin-bottom: 5px">Fecha de Inicio</label>
                        <input type='date' class="form-control input-sm" name="txt_tarifacochera_fecinicio" id="txt_tarifacochera_fecinicio" style="width: 100%;" autocomplete="off" placeholder="Nombre de Opción"/>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="txt_tarifacochera_fecfinal" class="control-label" style="margin-bottom: 5px">Fecha de Culminación</label>
                        <input type='date' class="form-control input-sm" name="txt_tarifacochera_fecfinal" id="txt_tarifacochera_fecfinal" style="width: 100%;" autocomplete="off" placeholder="Breve descripción"/>
                      </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                      <div class="form-group col-md-6">
                        <label for="txt_tarifacochera_criteriohora" class="control-label" style="margin-bottom: 5px">Criterio de Hora</label>
                        <input type='number' class="form-control input-sm" name="txt_tarifacochera_criteriohora" id="txt_tarifacochera_criteriohora" style="width: 100%;" autocomplete="off" placeholder="Criterio de Hora"/>
                      </div>
                      <div class="form-group col-md-6">
                        <br>
                        <button type="button" class="btn btn-primary btn-sm" onclick="registrar_tarifacochera()"><i class="fa fa-save"></i></button>
                      </div>
                    </div>
                  </div>
            </form>
          </div>
        </div>
        <p></p>
        <hr>
        <label for="txt_filtro" class="control-label">Lista de Tarifas Registradas</label>
        <div class="panel panel-primary shadow-simple">
          <div class="panel-body">
            <table class="table table-bordered">
              <tbody id="table_tarifacochera"></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/tarifacochera/tarifacochera_form.js?ver=2';?>"></script>