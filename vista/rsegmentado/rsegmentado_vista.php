<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-cogs"></i>Mantenimiento</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-body">
				<!--- MESAJES DE GUARDADO --> 
				<div class="callout callout-info" id="reversiones_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row mt-4">
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="lblfecha">Fecha :</label>
							<div class="input-group">
								<div class='input-group date' id='datetimepicker1'>
									<input type='text' class="form-control input-sm" name="txt_fil_fec1" id="txt_fil_fec1" value="<?php echo date('01-m-Y'); ?>"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<span class="input-group-addon">-</span>
								<div class='input-group date' id='datetimepicker2'>
									<input type='text' class="form-control input-sm" name="txt_fil_fec2" id="txt_fil_fec2" value="<?php echo date('d-m-Y'); ?>"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="div_reversiones_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<!-- < ?php require_once('reversiones_tabla.php');?> -->
						</div>
					</div>
				</div>
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
