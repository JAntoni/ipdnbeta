<?php 
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  require_once('../reversiones/Reversiones.class.php');
  require_once ("../cliente/Cliente.class.php");

  $oReversiones = new Reversiones();
  $oCliente = new Cliente();

  $tipo_cambio  = $_POST['tipo_cambio'];
  $fecha01      = fecha_mysql($_POST['fecha01']);
  $fecha02      = fecha_mysql($_POST['fecha02']);
  
  $result = $oReversiones->lista_reversiones_segmentado($fecha01,$fecha02,0,0,0);
    if($result['estado'] == 1){
      $tipocred = ''; $estadocred = ''; $dscmoneda = ''; $estado_cuota = ''; $tipo_cuota = ''; $tr01 = ''; $tr02 = ''; $tr03 = ''; $tr04 = '';      
      $mostrar_monto_pagado_por_cuota01 = 0; $mostrar_monto_pagado_por_cuota02 = 0; $mostrar_monto_pagado_por_cuota03 = 0; $mostrar_monto_pagado_por_cuota04 = 0;      
      $cont_tramo_01_cuota_01 = 0; $cont_tramo_01_cuota_02 = 0; $cont_tramo_01_cuota_03 = 0;
      $contArray00    = -1;      $contArray01    = -1;      $contArray02    = -1;      $contArray03    = -1;      $contArray04    = -1;
      $array_tramo_00 = array(); $array_tramo_01 = array(); $array_tramo_02 = array(); $array_tramo_03 = array(); $array_tramo_04 = array();      
      $sum_monto_cobrado01   = 0; $sum_monto_cobrado02   = 0; $sum_monto_cobrado03   = 0; $sum_monto_cobrado04   = 0; $sum_monto_cobrado_total   = 0;
      $sum_monto_facturado01 = 0; $sum_monto_facturado02 = 0; $sum_monto_facturado03 = 0; $sum_monto_facturado04 = 0; $sum_monto_facturado_total = 0;
      $sum_tramo_01_pendiente_sol = 0.00; $sum_tramo_01_pendiente_dol = 0.00; $sum_tramo_01_pago_parcial_sol = 0.00; $sum_tramo_01_pago_parcial_dol = 0.00; $sum_tramo_01_pagado_sol = 0.00; $sum_tramo_01_pagado_dol = 0.00;
      $sum_tramo_02_pendiente_sol = 0.00; $sum_tramo_02_pendiente_dol = 0.00; $sum_tramo_02_pago_parcial_sol = 0.00; $sum_tramo_02_pago_parcial_dol = 0.00; $sum_tramo_02_pagado_sol = 0.00; $sum_tramo_02_pagado_dol = 0.00;
      $sum_tramo_03_pendiente_sol = 0.00; $sum_tramo_03_pendiente_dol = 0.00; $sum_tramo_03_pago_parcial_sol = 0.00; $sum_tramo_03_pago_parcial_dol = 0.00; $sum_tramo_03_pagado_sol = 0.00; $sum_tramo_03_pagado_dol = 0.00;
      $sum_tramo_04_pendiente_sol = 0.00; $sum_tramo_04_pendiente_dol = 0.00; $sum_tramo_04_pago_parcial_sol = 0.00; $sum_tramo_04_pago_parcial_dol = 0.00; $sum_tramo_04_pagado_sol = 0.00; $sum_tramo_04_pagado_dol = 0.00;

      // if( substr($col10,5,2) == substr($fecha01,5,2) ) // SI PERTENCE AL MISMO MES, ES VIGENTE

      foreach ($result['data'] as $key => $value) {
        $col01 = mostrar_fecha($value['tb_credito_feccre']);
        $col02 = $value['tb_cliente_doc'];
        $col03 = $value['tb_cliente_nom'];
        $col04 = intval($value['tb_credito_numcuo']);
        $col05 = intval($value['tb_credito_id']);
        $col06 = intval($value['tb_creditotipo_id']);
        $col07 = intval($value['tb_credito_est']);
        $col08 = intval($value['tb_moneda_id']);
        $col09 = intval($value['tb_cuota_est']);
        $col10 = fecha_mysql($value['tb_cuota_fec']);
        $col11 = formato_numero($value['tb_credito_preaco']);
        $col12 = formato_numero($value['tb_cuota_cap']);
        $col13 = formato_numero($value['tb_cuota_amo']);
        $col14 = formato_numero($value['tb_cuota_int']);
        $col15 = formato_numero($value['tb_cuota_cuo']);
        $col16 = intval($value['tb_cuotatipo_id']);
        $col17 = formato_numero($value['cuota_real']);
        $col18 = intval($value['tb_cuota_num']);
        $col19 = intval($value['tb_cuota_id']);

        if( $col06 == 1 ) {

          $tipocred = 'CRED. MENOR'; 

          if( $col16 == 1 ){ $tipo_cuota = 'CUOTA LIBRE'; } elseif( $col16 == 2 ){ $tipo_cuota = 'CUOTA FIJA'; } else { $tipo_cuota = '<span class="badge bg-red">¡Reportar!</span>'; }
          
          switch ($col07) {
            case 1:
              $estadocred = 'PENDIENTE';
            break;
            case 2:
              $estadocred = 'APROBADO';
            break;
            case 3:
              $estadocred = 'VIGENTE';
            break;
            case 4:
              $estadocred = 'LIQUIDADO';
            break;
            case 5:
              $estadocred = 'REMATE';
            break;
            case 6:
              $estadocred = 'VENDIDO';
            break;
            case 7:
              $estadocred = 'LIQUIDADO PENDIENTE DE ENVIO';
            break;
            default:
              $estadocred = '<span class="badge bg-red">¡Reportar!</span>';
          }
        }
        else {
          switch ($col07) {
            case 1:
              $estadocred = 'PENDIENTE';
            break;
            case 2:
              $estadocred = 'APROBADO';
            break;
            case 3:
              $estadocred = 'VIGENTE';
            break;
            case 4:
              $estadocred = 'PARALIZADO';
            break;
            case 5:
              $estadocred = 'REFINANCIADO AMORT.';
            break;
            case 6:
              $estadocred = 'REFINANCIADO SIN AMORT.';
            break;
            case 7:
              $estadocred = 'LIQUIDADO';
            break;
            case 8:
              $estadocred = 'RESUELTO';
            break;
            case 9:
              $estadocred = 'TITULO';
            break;
            case 10:
              $estadocred = 'DOCUMENTOS';
            break;
            default:
              $estadocred = '<span class="badge bg-red">¡Reportar!</span>';
          }

          switch ($col06) {
            case 2:
              $tipocred = 'CRED. ASCVEH';
            break;
            case 3:
              $tipocred = 'CRED. GARVEH';
            break;
            case 4:
              $tipocred = 'CRED. HIPOTECARIO';
            break;
            case 5:
              $tipocred = 'ADENDA';
            break;
            default:
              $tipocred = '<span class="badge bg-red">¡Reportar!</span>';
          }

          if( $col16 == 3 ){ $tipo_cuota = 'CUOTA LIBRE'; } elseif( $col16 == 4 ){ $tipo_cuota = 'CUOTA FIJA'; } else { $tipo_cuota = '<span class="badge bg-red">¡Reportar!</span>'; }
        }        
        
        if( $col18 >= 1 && $col18 <= 3 )
        {
          $contArray01++;

          if($col18 == 1) { $cont_tramo_01_cuota_01++; } elseif($col18 == 2) { $cont_tramo_01_cuota_02++; } elseif($col18 == 3) { $cont_tramo_01_cuota_03++; } else { $cont_tramo_01_cuota_01+0; $cont_tramo_01_cuota_02+0; $cont_tramo_01_cuota_03+0; }

          if($col09==1)
          { 
            $estado_cuota = '<span class="badge bg-red">PENDIENTE</span>';
            $mostrar_monto_pagado_por_cuota01  = formato_numero(0);
            if($col08==1){
              $dscmoneda                       = 'S/';
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += $col17;
              $sum_tramo_01_pendiente_sol    += $col17;
            }
            elseif($col08==2){
              $dscmoneda                       = 'US$';
              $sum_monto_cobrado01            += 0;
              $sum_monto_facturado01          += ($tipo_cambio * $col17);
              $sum_tramo_01_pendiente_dol     += $col17;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += 0;
              $sum_tramo_01_pendiente_sol    += 0;
              $sum_tramo_01_pendiente_dol    += 0;
            }
          }
          elseif($col09==2)
          { 
            $estado_cuota = '<span class="badge bg-green">PAGADO</span>';
            $mostrar_monto_pagado_por_cuota01  = $col17;
            if($col08==1){
              $dscmoneda                       = 'S/';
              $sum_tramo_01_pagado_sol        += $mostrar_monto_pagado_por_cuota01;
              $sum_monto_cobrado01            += $mostrar_monto_pagado_por_cuota01;
              $sum_monto_facturado01          += $mostrar_monto_pagado_por_cuota01;
            }
            elseif($col08==2){
              $dscmoneda                       = 'US$';              
              $sum_monto_cobrado01            += ($tipo_cambio * $mostrar_monto_pagado_por_cuota01);
              $sum_monto_facturado01          += ($tipo_cambio * $mostrar_monto_pagado_por_cuota01);
              $sum_tramo_01_pagado_dol        += $mostrar_monto_pagado_por_cuota01;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += 0;
              $sum_tramo_01_pagado_sol       += 0;
              $sum_tramo_01_pagado_dol       += 0;
            }
          }
          elseif($col09==3)
          { 
            $estado_cuota = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
            $mostrar_monto_pagado_por_cuota01    = $col17;
            if($col08==1){
              $dscmoneda                         = 'S/';
              $sum_tramo_01_pago_parcial_sol    += $mostrar_monto_pagado_por_cuota01;
              $sum_monto_cobrado01              += $mostrar_monto_pagado_por_cuota01;
              $sum_monto_facturado01            += $mostrar_monto_pagado_por_cuota01;
            }
            elseif($col08==2){
              $dscmoneda                         = 'US$';
              $sum_monto_cobrado01              += ($tipo_cambio * $mostrar_monto_pagado_por_cuota01);
              $sum_monto_facturado01            += ($tipo_cambio * $mostrar_monto_pagado_por_cuota01);
              $sum_tramo_01_pago_parcial_dol    += $mostrar_monto_pagado_por_cuota01;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado01              += 0;
              $sum_monto_facturado01            += 0;
              $sum_tramo_01_pago_parcial_sol    += 0;
              $sum_tramo_01_pago_parcial_dol    += 0;
            }
          } else {
            $sum_monto_cobrado01                += 0;
            $sum_monto_facturado01              += 0;
            $sum_tramo_01_pendiente_sol         += 0;
            $sum_tramo_01_pendiente_dol         += 0;
            $sum_tramo_01_pagado_sol            += 0;
            $sum_tramo_01_pagado_dol            += 0;
            $sum_tramo_01_pago_parcial_sol      += 0;
            $sum_tramo_01_pago_parcial_dol      += 0;
            $mostrar_monto_pagado_por_cuota01    = formato_numero(0);
            $dscmoneda                           = '<span class="badge bg-red">¡Moneda no identificada!</span>';
            $estado_cuota                        = '<span class="badge bg-red">Estado no identificado :: '.$col09.' ::</span>';
          }
          
          $array_tramo_01[$contArray01]['col01'] = $col01;
          $array_tramo_01[$contArray01]['col02'] = $col02;
          $array_tramo_01[$contArray01]['col03'] = $col03;
          $array_tramo_01[$contArray01]['col04'] = $col04;
          $array_tramo_01[$contArray01]['col05'] = $col05;
          $array_tramo_01[$contArray01]['col06'] = $tipocred;
          $array_tramo_01[$contArray01]['col07'] = $estadocred;
          $array_tramo_01[$contArray01]['col08'] = $dscmoneda;
          $array_tramo_01[$contArray01]['col09'] = $estado_cuota;
          $array_tramo_01[$contArray01]['col10'] = $col10;
          $array_tramo_01[$contArray01]['col11'] = $col11;
          $array_tramo_01[$contArray01]['col12'] = $col12;
          $array_tramo_01[$contArray01]['col13'] = $col13;
          $array_tramo_01[$contArray01]['col14'] = $col14;
          $array_tramo_01[$contArray01]['col15'] = $col15;
          $array_tramo_01[$contArray01]['col16'] = $tipo_cuota;
          $array_tramo_01[$contArray01]['col17'] = $col17;
          $array_tramo_01[$contArray01]['col18'] = $col18;
          $array_tramo_01[$contArray01]['col19'] = $col19;
          $array_tramo_01[$contArray01]['col20'] = $mostrar_monto_pagado_por_cuota01;

        }
        elseif( $col18 >= 4 && $col18 <= 9 )
        {
          $contArray02++;

          if($col09==1){ 
            $estado_cuota = '<span class="badge bg-red">PENDIENTE</span>';
            $mostrar_monto_pagado_por_cuota02 = formato_numero(0);
            if($col08==1){
              $dscmoneda                      = 'S/';
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += $col17;
              $sum_tramo_02_pendiente_sol    += $col17;
            }
            elseif($col08==2){
              $dscmoneda                      = 'US$';
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += ($tipo_cambio * $col17);
              $sum_tramo_02_pendiente_dol    += $col17;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += 0;
              $sum_tramo_02_pendiente_sol    += 0;
              $sum_tramo_02_pendiente_dol    += 0;
            }
          } elseif($col09==2){ 
            $estado_cuota = '<span class="badge bg-green">PAGADO</span>'; 
            $mostrar_monto_pagado_por_cuota02 = $col17;
            if($col08==1){
              $dscmoneda                      = 'S/';
              $sum_monto_cobrado02           += $mostrar_monto_pagado_por_cuota02;
              $sum_monto_facturado02         += $mostrar_monto_pagado_por_cuota02;
              $sum_tramo_02_pagado_sol       += $mostrar_monto_pagado_por_cuota02;
            }
            elseif($col08==2){
              $dscmoneda                      = 'US$';
              $sum_monto_cobrado02           += ($tipo_cambio * $mostrar_monto_pagado_por_cuota02);
              $sum_monto_facturado02         += ($tipo_cambio * $mostrar_monto_pagado_por_cuota02);
              $sum_tramo_02_pagado_dol       += $mostrar_monto_pagado_por_cuota02;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += 0;
              $sum_tramo_02_pagado_sol       += 0;
              $sum_tramo_02_pagado_dol       += 0;
            }
          } elseif($col09==3){ 
            $estado_cuota = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
            $mostrar_monto_pagado_por_cuota02 = $col17;
            if($col08==1){
              $dscmoneda                      = 'S/';
              $sum_monto_cobrado02           += $mostrar_monto_pagado_por_cuota02;
              $sum_monto_facturado02         += $mostrar_monto_pagado_por_cuota02;
              $sum_tramo_02_pago_parcial_sol += $mostrar_monto_pagado_por_cuota02;
            }
            elseif($col08==2){
              $dscmoneda                      = 'US$';
              $sum_monto_cobrado02           += ($tipo_cambio * $mostrar_monto_pagado_por_cuota02);
              $sum_monto_facturado02         += ($tipo_cambio * $mostrar_monto_pagado_por_cuota02);
              $sum_tramo_02_pago_parcial_dol += $mostrar_monto_pagado_por_cuota02;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += 0;
              $sum_tramo_02_pago_parcial_sol += 0;
              $sum_tramo_02_pago_parcial_dol += 0;
            }
          } else {
            $sum_monto_cobrado02             += 0;
            $sum_monto_facturado02           += 0;
            $sum_tramo_02_pendiente_sol      += 0;
            $sum_tramo_02_pendiente_dol      += 0;
            $sum_tramo_02_pagado_sol         += 0;
            $sum_tramo_02_pagado_dol         += 0;
            $sum_tramo_02_pago_parcial_sol   += 0;
            $sum_tramo_02_pago_parcial_dol   += 0;
            $mostrar_monto_pagado_por_cuota02 = formato_numero(0);
            $dscmoneda                        = '<span class="badge bg-red">¡Moneda no identificada!</span>';
            $estado_cuota                     = '<span class="badge bg-red">Estado no identificado :: '.$col09.' ::</span>';
          }

          $array_tramo_02[$contArray02]['col01'] = $col01;
          $array_tramo_02[$contArray02]['col02'] = $col02;
          $array_tramo_02[$contArray02]['col03'] = $col03;
          $array_tramo_02[$contArray02]['col04'] = $col04;
          $array_tramo_02[$contArray02]['col05'] = $col05;
          $array_tramo_02[$contArray02]['col06'] = $tipocred;
          $array_tramo_02[$contArray02]['col07'] = $estadocred;
          $array_tramo_02[$contArray02]['col08'] = $dscmoneda;
          $array_tramo_02[$contArray02]['col09'] = $estado_cuota;
          $array_tramo_02[$contArray02]['col10'] = $col10;
          $array_tramo_02[$contArray02]['col11'] = $col11;
          $array_tramo_02[$contArray02]['col12'] = $col12;
          $array_tramo_02[$contArray02]['col13'] = $col13;
          $array_tramo_02[$contArray02]['col14'] = $col14;
          $array_tramo_02[$contArray02]['col15'] = $col15;
          $array_tramo_02[$contArray02]['col16'] = $tipo_cuota;
          $array_tramo_02[$contArray02]['col17'] = $col17;
          $array_tramo_02[$contArray02]['col18'] = $col18;
          $array_tramo_02[$contArray02]['col19'] = $col19;
          $array_tramo_02[$contArray02]['col20'] = $mostrar_monto_pagado_por_cuota02;
        }
        elseif( $col18 >= 10 && $col18 <= 18 )
        {
          $contArray03++;

          if($col09==1){ 
            $estado_cuota = '<span class="badge bg-red">PENDIENTE</span>';
            $mostrar_monto_pagado_por_cuota03 = formato_numero(0);
            if($col08==1){
              $dscmoneda                       = 'S/';
              $sum_monto_cobrado03            += 0;
              $sum_monto_facturado03          += $col17;
              $sum_tramo_03_pendiente_sol     += $col17;
            }
            elseif($col08==2){
              $dscmoneda                       = 'US$';
              $sum_monto_cobrado03            += 0;
              $sum_monto_facturado03          += ($tipo_cambio * $col17);
              $sum_tramo_03_pendiente_dol     += $col17;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += 0;
              $sum_tramo_03_pendiente_sol    += 0;
              $sum_tramo_03_pendiente_dol    += 0;
            }
          } elseif($col09==2){ 
            $estado_cuota = '<span class="badge bg-green">PAGADO</span>'; 
            $mostrar_monto_pagado_por_cuota03  = $col17;
            if($col08==1){
              $dscmoneda                       = 'S/';
              $sum_monto_cobrado03            += $mostrar_monto_pagado_por_cuota03;
              $sum_monto_facturado03          += $mostrar_monto_pagado_por_cuota03;
              $sum_tramo_03_pagado_sol        += $mostrar_monto_pagado_por_cuota03;
            }
            elseif($col08==2){
              $dscmoneda                       = 'US$';
              $sum_monto_cobrado03            += ($tipo_cambio * $mostrar_monto_pagado_por_cuota03);
              $sum_monto_facturado03          += ($tipo_cambio * $mostrar_monto_pagado_por_cuota03);
              $sum_tramo_03_pagado_dol        += $mostrar_monto_pagado_por_cuota03;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += 0;
              $sum_tramo_03_pagado_sol       += 0;
              $sum_tramo_03_pagado_dol       += 0;
            }
          } elseif($col09==3){ 
            $estado_cuota = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
            $mostrar_monto_pagado_por_cuota03  = $col17;
            if($col08==1){
              $dscmoneda                       = 'S/';
              $sum_monto_cobrado03            += $mostrar_monto_pagado_por_cuota03;
              $sum_monto_facturado03          += $mostrar_monto_pagado_por_cuota03;
              $sum_tramo_03_pago_parcial_sol  += $mostrar_monto_pagado_por_cuota03;
            }
            elseif($col08==2){
              $dscmoneda                       = 'US$';
              $sum_monto_cobrado03            += ($tipo_cambio * $mostrar_monto_pagado_por_cuota03);
              $sum_monto_facturado03          += ($tipo_cambio * $mostrar_monto_pagado_por_cuota03);
              $sum_tramo_03_pago_parcial_dol  += $mostrar_monto_pagado_por_cuota03;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado03            += 0;
              $sum_monto_facturado03          += 0;
              $sum_tramo_03_pago_parcial_sol  += 0;
              $sum_tramo_03_pago_parcial_dol  += 0;
            }
          } else {
            $sum_monto_cobrado03              += 0;
            $sum_monto_facturado03            += 0;
            $sum_tramo_03_pendiente_sol       += 0;
            $sum_tramo_03_pendiente_dol       += 0;
            $sum_tramo_03_pagado_sol          += 0;
            $sum_tramo_03_pagado_dol          += 0;
            $sum_tramo_03_pago_parcial_sol    += 0;
            $sum_tramo_03_pago_parcial_dol    += 0;
            $mostrar_monto_pagado_por_cuota03  = formato_numero(0);
            $dscmoneda                         = '<span class="badge bg-red">¡Moneda no identificada!</span>';
            $estado_cuota                      = '<span class="badge bg-red">Estado no identificado :: '.$col09.' ::</span>';
          }

          $array_tramo_03[$contArray03]['col01'] = $col01;
          $array_tramo_03[$contArray03]['col02'] = $col02;
          $array_tramo_03[$contArray03]['col03'] = $col03;
          $array_tramo_03[$contArray03]['col04'] = $col04;
          $array_tramo_03[$contArray03]['col05'] = $col05;
          $array_tramo_03[$contArray03]['col06'] = $tipocred;
          $array_tramo_03[$contArray03]['col07'] = $estadocred;
          $array_tramo_03[$contArray03]['col08'] = $dscmoneda;
          $array_tramo_03[$contArray03]['col09'] = $estado_cuota;
          $array_tramo_03[$contArray03]['col10'] = $col10;
          $array_tramo_03[$contArray03]['col11'] = $col11;
          $array_tramo_03[$contArray03]['col12'] = $col12;
          $array_tramo_03[$contArray03]['col13'] = $col13;
          $array_tramo_03[$contArray03]['col14'] = $col14;
          $array_tramo_03[$contArray03]['col15'] = $col15;
          $array_tramo_03[$contArray03]['col16'] = $tipo_cuota;
          $array_tramo_03[$contArray03]['col17'] = $col17;
          $array_tramo_03[$contArray03]['col18'] = $col18;
          $array_tramo_03[$contArray03]['col19'] = $col19;
          $array_tramo_03[$contArray03]['col20'] = $mostrar_monto_pagado_por_cuota03;
        }
        elseif( $col18 >= 19 && $col18 <= 72 )
        {
          $contArray04++;

          if($col09==1){ 
            $estado_cuota = '<span class="badge bg-red">PENDIENTE</span>';
            $mostrar_monto_pagado_por_cuota04 = formato_numero(0);
            if($col08==1){
              $dscmoneda                      = 'S/';
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += $col17;
              $sum_tramo_04_pendiente_sol    += $col17;
            }
            elseif($col08==2){
              $dscmoneda                      = 'US$';
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += ($tipo_cambio * $col17);
              $sum_tramo_04_pendiente_dol    += $col17;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_tramo_04_pendiente_sol    += 0;
              $sum_tramo_04_pendiente_dol    += 0;
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += 0;
            }
          } elseif($col09==2){ 
            $estado_cuota = '<span class="badge bg-green">PAGADO</span>'; 
            $mostrar_monto_pagado_por_cuota04 = $col17;
            if($col08==1){
              $dscmoneda                      = 'S/';
              $sum_monto_cobrado04           += $mostrar_monto_pagado_por_cuota04;
              $sum_monto_facturado04         += $mostrar_monto_pagado_por_cuota04;
              $sum_tramo_04_pagado_sol       += $mostrar_monto_pagado_por_cuota04;
            }
            elseif($col08==2){
              $dscmoneda                      = 'US$';
              $sum_monto_cobrado04           += ($tipo_cambio * $mostrar_monto_pagado_por_cuota04);
              $sum_monto_facturado04         += ($tipo_cambio * $mostrar_monto_pagado_por_cuota04);
              $sum_tramo_04_pagado_dol       += $mostrar_monto_pagado_por_cuota04;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += 0;
              $sum_tramo_04_pagado_sol       += 0;
              $sum_tramo_04_pagado_dol       += 0;
            }
          } elseif($col09==3){ 
            $estado_cuota = '<span class="badge bg-aqua">PAGO PARCIAL</span>';
            $mostrar_monto_pagado_por_cuota04 = $col17;
            if($col08==1){
              $dscmoneda                      = 'S/';
              $sum_monto_cobrado04           += $mostrar_monto_pagado_por_cuota04;
              $sum_monto_facturado04         += $mostrar_monto_pagado_por_cuota04;
              $sum_tramo_04_pago_parcial_sol += $mostrar_monto_pagado_por_cuota04;
            }
            elseif($col08==2){
              $dscmoneda                      = 'US$';
              $sum_monto_cobrado04           += ($tipo_cambio * $mostrar_monto_pagado_por_cuota04);
              $sum_monto_facturado04         += ($tipo_cambio * $mostrar_monto_pagado_por_cuota04);
              $sum_tramo_04_pago_parcial_dol += $mostrar_monto_pagado_por_cuota04;
            }
            else{
              $dscmoneda = '<span class="badge bg-red">Reportar</span>';
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += 0;
              $sum_tramo_04_pago_parcial_sol += 0;
              $sum_tramo_04_pago_parcial_dol += 0;
            }
          } else {
            $sum_monto_cobrado04              += 0;
            $sum_monto_facturado04            += 0;
            $sum_tramo_04_pendiente_sol       += 0;
            $sum_tramo_04_pendiente_dol       += 0;
            $sum_tramo_04_pagado_sol          += 0;
            $sum_tramo_04_pagado_dol          += 0;
            $sum_tramo_04_pago_parcial_sol    += 0;
            $sum_tramo_04_pago_parcial_dol    += 0;
            $mostrar_monto_pagado_por_cuota04  = formato_numero(0);
            $dscmoneda                         = '<span class="badge bg-red">¡Moneda no identificada!</span>';
            $estado_cuota                      = '<span class="badge bg-red">Estado no identificado :: '.$col09.' ::</span>';
          }

          $array_tramo_04[$contArray04]['col01'] = $col01;
          $array_tramo_04[$contArray04]['col02'] = $col02;
          $array_tramo_04[$contArray04]['col03'] = $col03;
          $array_tramo_04[$contArray04]['col04'] = $col04;
          $array_tramo_04[$contArray04]['col05'] = $col05;
          $array_tramo_04[$contArray04]['col06'] = $tipocred;
          $array_tramo_04[$contArray04]['col07'] = $estadocred;
          $array_tramo_04[$contArray04]['col08'] = $dscmoneda;
          $array_tramo_04[$contArray04]['col09'] = $estado_cuota;
          $array_tramo_04[$contArray04]['col10'] = $col10;
          $array_tramo_04[$contArray04]['col11'] = $col11;
          $array_tramo_04[$contArray04]['col12'] = $col12;
          $array_tramo_04[$contArray04]['col13'] = $col13;
          $array_tramo_04[$contArray04]['col14'] = $col14;
          $array_tramo_04[$contArray04]['col15'] = $col15;
          $array_tramo_04[$contArray04]['col16'] = $tipo_cuota;
          $array_tramo_04[$contArray04]['col17'] = $col17;
          $array_tramo_04[$contArray04]['col18'] = $col18;
          $array_tramo_04[$contArray04]['col19'] = $col19;
          $array_tramo_04[$contArray04]['col20'] = $mostrar_monto_pagado_por_cuota04;
        }
        else
        {
          $sum_monto_cobrado01           += 0;
          $sum_monto_facturado01         += 0;          
          $sum_tramo_01_pendiente_sol    += 0;
          $sum_tramo_01_pendiente_dol    += 0;
          $sum_tramo_01_pagado_sol       += 0;
          $sum_tramo_01_pagado_dol       += 0;
          $sum_tramo_01_pago_parcial_sol += 0;
          $sum_tramo_01_pago_parcial_dol += 0;
          
          $sum_monto_cobrado02           += 0;
          $sum_monto_facturado02         += 0;
          $sum_tramo_02_pendiente_sol    += 0;
          $sum_tramo_02_pendiente_dol    += 0;
          $sum_tramo_02_pagado_sol       += 0;
          $sum_tramo_02_pagado_dol       += 0;
          $sum_tramo_02_pago_parcial_sol += 0;
          $sum_tramo_02_pago_parcial_dol += 0;
          
          $sum_monto_cobrado03           += 0;
          $sum_monto_facturado03         += 0;
          $sum_tramo_03_pendiente_sol    += 0;
          $sum_tramo_03_pendiente_dol    += 0;
          $sum_tramo_03_pagado_sol       += 0;
          $sum_tramo_03_pagado_dol       += 0;
          $sum_tramo_03_pago_parcial_sol += 0;
          $sum_tramo_03_pago_parcial_dol += 0;
          
          $sum_monto_cobrado04           += 0;
          $sum_monto_facturado04         += 0;
          $sum_tramo_04_pendiente_sol    += 0;
          $sum_tramo_04_pendiente_dol    += 0;
          $sum_tramo_04_pagado_sol       += 0;
          $sum_tramo_04_pagado_dol       += 0;
          $sum_tramo_04_pago_parcial_sol += 0;
          $sum_tramo_04_pago_parcial_dol += 0;
        }
      }

      $arrayList01 = json_encode($array_tramo_01);
      $arrayList02 = json_encode($array_tramo_02);
      $arrayList03 = json_encode($array_tramo_03);
      $arrayList04 = json_encode($array_tramo_04);

      foreach (json_decode($arrayList01) as $key => $value01) {
        $tr01 .='<tr>';
          $tr01.='
            <td style="vertical-align: middle;" class="text-center">'.$value01->col02.'</td>
            <td style="vertical-align: middle;" class="text-left">'  .$value01->col03.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col05.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col01.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col06.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col07.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col08.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value01->col11).'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col04.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col16.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col18.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col09.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value01->col10.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value01->col12).'</td>
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value01->col13) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value01->col14) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value01->col15) </td> -->
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value01->col17).'</td>
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value01->col20).'</td>
          ';
        $tr01.='</tr>';
      }

      foreach (json_decode($arrayList02) as $key => $value02) {
        $tr02 .='<tr>';
          $tr02.='
            <td style="vertical-align: middle;" class="text-center">'.$value02->col02.'</td>
            <td style="vertical-align: middle;" class="text-left">'  .$value02->col03.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col05.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col01.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col06.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col07.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col08.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value02->col11).'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col04.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col16.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col18.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col09.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value02->col10.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value02->col12).'</td>
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value02->col13) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value02->col14) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value02->col15) </td> -->
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value02->col17).'</td>
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value02->col20).'</td>
          ';
        $tr02.='</tr>';
      }

      foreach (json_decode($arrayList03) as $key => $value03) {
        $tr03 .='<tr>';
          $tr03.='
            <td style="vertical-align: middle;" class="text-center">'.$value03->col02.'</td>
            <td style="vertical-align: middle;" class="text-left">'  .$value03->col03.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col05.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col01.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col06.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col07.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col08.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value03->col11).'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col04.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col16.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col18.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col09.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value03->col10.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value03->col12).'</td>
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value03->col13) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value03->col14) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value03->col15) </td> -->
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value03->col17).'</td>
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value03->col20).'</td>
          ';
        $tr03.='</tr>';
      }

      foreach (json_decode($arrayList04) as $key => $value04) {
        $tr04 .='<tr>';
          $tr04.='
            <td style="vertical-align: middle;" class="text-center">'.$value04->col02.'</td>
            <td style="vertical-align: middle;" class="text-left">'  .$value04->col03.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col05.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col01.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col06.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col07.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col08.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value04->col11).'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col04.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col16.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col18.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col09.'</td>
            <td style="vertical-align: middle;" class="text-center">'.$value04->col10.'</td>
            <td style="vertical-align: middle;" class="text-center">'.mostrar_moneda($value04->col12).'</td>
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value04->col13) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value04->col14) </td> -->
            <!-- <td style="vertical-align: middle;" class="text-center"> mostrar_moneda($value04->col15) </td> -->
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value04->col17).'</td>
            <td style="vertical-align: middle;" class="text-right">' .mostrar_moneda($value04->col20).'</td>
          ';
        $tr04.='</tr>';
      } 

      $sum_monto_cobrado_total   = formato_numero(($sum_monto_cobrado01 + $sum_monto_cobrado02 + $sum_monto_cobrado03 + $sum_monto_cobrado04));
      $sum_monto_facturado_total = formato_numero(($sum_monto_facturado01 + $sum_monto_facturado02 + $sum_monto_facturado03 + $sum_monto_facturado04));
      
      $total_tramo_01_pendiente_en_soles   = formato_numero(($tipo_cambio * $sum_tramo_01_pendiente_dol)    + $sum_tramo_01_pendiente_sol);
      $total_tramo_01_pagoparcial_en_soles = formato_numero(($tipo_cambio * $sum_tramo_01_pago_parcial_dol) + $sum_tramo_01_pago_parcial_sol);
      $total_tramo_01_pagado_en_soles      = formato_numero(($tipo_cambio * $sum_tramo_01_pagado_dol)       + $sum_tramo_01_pagado_sol);

      $total_tramo_02_pendiente_en_soles   = formato_numero(($tipo_cambio * $sum_tramo_02_pendiente_dol)    + $sum_tramo_02_pendiente_sol);
      $total_tramo_02_pagoparcial_en_soles = formato_numero(($tipo_cambio * $sum_tramo_02_pago_parcial_dol) + $sum_tramo_02_pago_parcial_sol);
      $total_tramo_02_pagado_en_soles      = formato_numero(($tipo_cambio * $sum_tramo_02_pagado_dol)       + $sum_tramo_02_pagado_sol);

      $total_tramo_03_pendiente_en_soles   = formato_numero(($tipo_cambio * $sum_tramo_03_pendiente_dol)    + $sum_tramo_03_pendiente_sol);
      $total_tramo_03_pagoparcial_en_soles = formato_numero(($tipo_cambio * $sum_tramo_03_pago_parcial_dol) + $sum_tramo_03_pago_parcial_sol);
      $total_tramo_03_pagado_en_soles      = formato_numero(($tipo_cambio * $sum_tramo_03_pagado_dol)       + $sum_tramo_03_pagado_sol);

      $total_tramo_04_pendiente_en_soles   = formato_numero(($tipo_cambio * $sum_tramo_04_pendiente_dol)    + $sum_tramo_04_pendiente_sol);
      $total_tramo_04_pagoparcial_en_soles = formato_numero(($tipo_cambio * $sum_tramo_04_pago_parcial_dol) + $sum_tramo_04_pago_parcial_sol);
      $total_tramo_04_pagado_en_soles      = formato_numero(($tipo_cambio * $sum_tramo_04_pagado_dol)       + $sum_tramo_04_pagado_sol);

      $porcent_tramo01 = 0.00; $porcent_tramo02 = 0.00; $porcent_tramo03 = 0.00; $porcent_tramo04 = 0.00;

      $color_porcent_tramo01 = ''; $porcent_tramo01 = 0.00;
      if( $sum_monto_cobrado01 > 0 )
        $porcent_tramo01 = formato_numero(( $sum_monto_cobrado01 / $sum_monto_facturado01 ) * 100);
      else
        $porcent_tramo01 = formato_numero(0);

      if($porcent_tramo01 >= 90.00) { $color_porcent_tramo01 = 'bg-green'; } elseif($porcent_tramo01 >= 60.00 && $porcent_tramo01 <= 89.99) { $color_porcent_tramo01 = 'bg-yellow'; } else { $color_porcent_tramo01 = 'bg-red'; }

      $color_porcent_tramo02 = ''; $porcent_tramo02 = 0.00;
      if( $sum_monto_cobrado02 > 0 )
        $porcent_tramo02 = formato_numero(( $sum_monto_cobrado02 / $sum_monto_facturado02 ) * 100);
      else
        $porcent_tramo02 = formato_numero(0);

      if($porcent_tramo02 >= 90.00) { $color_porcent_tramo02 = 'bg-green'; } elseif($porcent_tramo02 >= 60.00 && $porcent_tramo02 <= 89.99) { $color_porcent_tramo02 = 'bg-yellow'; } else { $color_porcent_tramo02 = 'bg-red'; }
      
      $color_porcent_tramo03 = ''; $porcent_tramo03 = 0.00;
      if( $sum_monto_cobrado03 > 0 )
        $porcent_tramo03 = formato_numero(( $sum_monto_cobrado03 / $sum_monto_facturado03 ) * 100);
      else
        $porcent_tramo03 = formato_numero(0);

      if($porcent_tramo03 >= 90.00) { $color_porcent_tramo03 = 'bg-green'; } elseif($porcent_tramo03 >= 60.00 && $porcent_tramo03 <= 89.99) { $color_porcent_tramo03 = 'bg-yellow'; } else { $color_porcent_tramo03 = 'bg-red'; }

      $color_porcent_tramo04 = ''; $porcent_tramo04 = 0.00;
      if( $sum_monto_cobrado04 > 0 )
        $porcent_tramo04 = formato_numero(( $sum_monto_cobrado04 / $sum_monto_facturado04 ) * 100);
      else
        $porcent_tramo04 = formato_numero(0);

      if($porcent_tramo04 >= 90.00) { $color_porcent_tramo04 = 'bg-green'; } elseif($porcent_tramo04 >= 60.00 && $porcent_tramo04 <= 89.99) { $color_porcent_tramo04 = 'bg-yellow'; } else { $color_porcent_tramo04 = 'bg-red'; }

    }
  $result = null;

  echo 
  '
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs" style="font-family: cambria;font-weight: bold">
      <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" id="panel1">Antigüedad de 1 - 3 meses</a></li>
      <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" id="panel2">Antigüedad de 4 - 9 meses</a></li>
      <li class="" id="opciontransf"><a href="#tab_3" data-toggle="tab" aria-expanded="false" id="panel3">Antigüedad de 10 - 18 meses</a></li>
      <li class="" id="opcioninicial"><a href="#tab_4" data-toggle="tab" aria-expanded="false" id="panel4">Antigüedad de 19 meses a más</a></li>
      <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false" id="panel5">RESUMEN </a></li>
    </ul>
    <div class="tab-content">
    
      <div class="tab-pane active" id="tab_1">
        <div class="container-fluid">
          <div class="row mt-4">
            <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12"></div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h4 class="box-title text-yellow">Conteo de cuotas</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <ul>
                        <li class="mt-4"> N° de Cuotas 01 = <span class="text-yellow"> '.$cont_tramo_01_cuota_01.'</span></li>
                        <li class="mt-4"> N° de Cuotas 02 = <span class="text-yellow"> '.$cont_tramo_01_cuota_02.'</span></li>
                        <li class="mt-4"> N° de Cuotas 03 = <span class="text-yellow"> '.$cont_tramo_01_cuota_03.'</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h4 class="box-title text-red">Pendiente</h4> <strong class="text-red pull-right" style="font-size: 18px;">S/ '.mostrar_moneda($total_tramo_01_pendiente_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_01_pendiente_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_01_pendiente_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h4 class="box-title text-aqua">Pago Parcial</h4> <strong class="text-aqua pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_01_pagoparcial_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_01_pago_parcial_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_01_pago_parcial_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h4 class="box-title text-green">Pagado</h4> <strong class="text-green pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_01_pagado_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_01_pagado_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_01_pagado_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-8 col-sm-12 col-xs-12">
              <div class="box box-primary">
                <div class="box-header bg-primary with-border">
                  <h4 class="box-title" style="color: white;">Total</h4>
                </div>
                <div class="box-body">
                  <div class="row">                    
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                      <ul>
                        <li class="mt-4"> <span class="badge bg-navy">FACTURADO:</span> <h4 class="pull-right">S/ '.mostrar_moneda($total_tramo_01_pendiente_en_soles+$total_tramo_01_pagoparcial_en_soles+$total_tramo_01_pagado_en_soles).'</h4></li>  <br><br>
                        <li> <span class="badge bg-teal">COBRADO:</span> <h4 class="pull-right"> S/ '.mostrar_moneda($total_tramo_01_pagoparcial_en_soles+$total_tramo_01_pagado_en_soles).'</h4></li>
                      </ul>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                      <div class="p-4 '.$color_porcent_tramo01.' shadow-simple" style="border-radius: 50%; height: 100px;">
                        <div class="text-center">
                          <h3><strong>'.$porcent_tramo01.'%</strong></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="tbl_rsegmentado01" class="table table-borderless table-striped">
              <thead>
                <tr>
                  <th style="vertical-align: middle;" class="text-center">DNI/RUC</th>
                  <th style="vertical-align: middle;" class="text-center">CLIENTE</th>
                  <th style="vertical-align: middle;" class="text-center">ID CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO DEL CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">MONEDA</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PREACO</th>
                  <th style="vertical-align: middle;" class="text-center">TOTAL CUOTAS</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">NÚMERO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">CAP</th>
                  <!-- <th style="vertical-align: middle;" class="text-center">AMO</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">INT</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">CUO</th> -->
                  <th style="vertical-align: middle;" class="text-center">CUOTA REAL</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PAGADO</th>
                </tr>
              </thead>
              <tbody>
                '.$tr01.'
              </tbody>
              <tfoot>
                <tr><td colspan="15" class="text-right">Total Cobrado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_cobrado01).'</td></tr>
                <tr><td colspan="15" class="text-right">Total Facturado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_facturado01).'</td></tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="tab_2">
        <div class="container-fluid">
          <div class="row mt-4">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h4 class="box-title text-red">Pendiente</h4> <strong class="text-red pull-right" style="font-size: 18px;">S/ '.mostrar_moneda($total_tramo_02_pendiente_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_02_pendiente_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_02_pendiente_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h4 class="box-title text-aqua">Pago Parcial</h4> <strong class="text-aqua pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_02_pagoparcial_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_02_pago_parcial_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_02_pago_parcial_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h4 class="box-title text-green">Pagado</h4> <strong class="text-green pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_02_pagado_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_02_pagado_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_02_pagado_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-6 col-xs-12">
              <div class="box box-primary">
                <div class="box-header bg-primary with-border">
                  <h4 class="box-title" style="color: white;">Total</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                      <ul>
                        <li class="mt-4"> <span class="badge bg-navy">FACTURADO:</span> <h4 class="pull-right"> S/ '.mostrar_moneda($total_tramo_02_pendiente_en_soles+$total_tramo_02_pagoparcial_en_soles+$total_tramo_02_pagado_en_soles).'</h4></li>  <br><br>
                        <li> <span class="badge bg-teal">COBRADO:</span> <h4 class="pull-right"> S/ '.mostrar_moneda($total_tramo_02_pagoparcial_en_soles+$total_tramo_02_pagado_en_soles).'</h4></li>
                      </ul>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                      <div class="p-4 '.$color_porcent_tramo02.' shadow-simple" style="border-radius: 50%; height: 100px;">
                        <div class="text-center">
                          <h3><strong>'.$porcent_tramo02.'%</strong></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="tbl_rsegmentado02" class="table table-borderless table-striped">
              <thead>
                <tr>
                  <th style="vertical-align: middle;" class="text-center">DNI/RUC</th>
                  <th style="vertical-align: middle;" class="text-center">CLIENTE</th>
                  <th style="vertical-align: middle;" class="text-center">ID CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO DEL CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">MONEDA</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PREACO</th>
                  <th style="vertical-align: middle;" class="text-center">TOTAL CUOTAS</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">NÚMERO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">CAP</th>
                  <!-- <th style="vertical-align: middle;" class="text-center">AMO</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">INT</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">CUO</th> -->
                  <th style="vertical-align: middle;" class="text-center">CUOTA REAL</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PAGADO</th>
                </tr>
              </thead>
              <tbody>
                '.$tr02.'
              </tbody>
              <tfoot>
                <tr><td colspan="15" class="text-right">Total Cobrado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_cobrado02).'</td></tr>
                <tr><td colspan="15" class="text-right">Total Facturado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_facturado02).'</td></tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="tab_3">
        <div class="container-fluid">
          <div class="row mt-4">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h4 class="box-title text-red">Pendiente</h4> <strong class="text-red pull-right" style="font-size: 18px;">S/ '.mostrar_moneda($total_tramo_03_pendiente_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_03_pendiente_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_03_pendiente_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h4 class="box-title text-aqua">Pago Parcial</h4> <strong class="text-aqua pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_03_pagoparcial_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_03_pago_parcial_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_03_pago_parcial_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h4 class="box-title text-green">Pagado</h4> <strong class="text-green pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_03_pagado_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_03_pagado_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_03_pagado_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-6 col-xs-12">
              <div class="box box-primary">
                <div class="box-header bg-primary with-border">
                  <h4 class="box-title" style="color: white;">Total</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                      <ul>
                        <li class="mt-4"> <span class="badge bg-navy">FACTURADO:</span> <h4 class="pull-right"> S/ '.mostrar_moneda($total_tramo_03_pendiente_en_soles+$total_tramo_03_pagoparcial_en_soles+$total_tramo_03_pagado_en_soles).'</h4></li>  <br><br>
                        <li> <span class="badge bg-teal">COBRADO:</span> <h4 class="pull-right">   S/ '.mostrar_moneda($total_tramo_03_pagoparcial_en_soles+$total_tramo_03_pagado_en_soles).'</h4></li>
                      </ul>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                      <div class="p-4 '.$color_porcent_tramo03.' shadow-simple" style="border-radius: 50%; height: 100px;">
                        <div class="text-center">
                          <h3><strong>'.$porcent_tramo03.'%</strong></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="tbl_rsegmentado03" class="table table-borderless table-striped">
              <thead>
                <tr>
                  <th style="vertical-align: middle;" class="text-center">DNI/RUC</th>
                  <th style="vertical-align: middle;" class="text-center">CLIENTE</th>
                  <th style="vertical-align: middle;" class="text-center">ID CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO DEL CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">MONEDA</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PREACO</th>
                  <th style="vertical-align: middle;" class="text-center">TOTAL CUOTAS</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">NÚMERO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">CAP</th>
                  <!-- <th style="vertical-align: middle;" class="text-center">AMO</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">INT</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">CUO</th> -->
                  <th style="vertical-align: middle;" class="text-center">CUOTA REAL</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PAGADO</th>
                </tr>
              </thead>
              <tbody>
                '.$tr03.'
              </tbody>
              <tfoot>
                <tr><td colspan="15" class="text-right">Total Cobrado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_cobrado03).'</td></tr>
                <tr><td colspan="15" class="text-right">Total Facturado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_facturado03).'</td></tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="tab_4">
        <div class="container-fluid">
          <div class="row mt-4">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h4 class="box-title text-red">Pendiente</h4> <strong class="text-red pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_04_pendiente_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_04_pendiente_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_04_pendiente_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h4 class="box-title text-aqua">Pago Parcial</h4> <strong class="text-aqua pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_04_pagoparcial_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_04_pago_parcial_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_04_pago_parcial_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h4 class="box-title text-green">Pagado</h4> <strong class="text-green pull-right" style="font-size: 18px;"> S/ '.mostrar_moneda($total_tramo_04_pagado_en_soles).'</strong>
                </div>
                <div class="box-body">
                  <ul>
                    <li class="mt-4">SOLES: <h4 class="pull-right">S/ '.mostrar_moneda($sum_tramo_04_pagado_sol).'</h4></li>  <br><br>
                    <li>DÓLARES<h4 class="pull-right">US $'.mostrar_moneda($sum_tramo_04_pagado_dol).'</h4></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-6 col-xs-12">
              <div class="box box-primary">
                <div class="box-header bg-primary with-border">
                  <h4 class="box-title" style="color: white;">Total</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                      <ul>
                        <li class="mt-4"><span class="badge bg-navy">FACTURADO:</span> <h4 class="pull-right"> S/ '.mostrar_moneda($total_tramo_04_pendiente_en_soles+$total_tramo_04_pagoparcial_en_soles+$total_tramo_04_pagado_en_soles).'</h4></li>  <br><br>
                        <li> <span class="badge bg-teal">COBRADO:</span> <h4 class="pull-right"> S/ '.mostrar_moneda($total_tramo_04_pagoparcial_en_soles+$total_tramo_04_pagado_en_soles).'</h4></li>
                      </ul>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                      <div class="p-4 '.$color_porcent_tramo04.' shadow-simple" style="border-radius: 50%; height: 100px;">
                        <div class="text-center">
                          <h3><strong>'.$porcent_tramo04.'%</strong></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="tbl_rsegmentado04" class="table table-borderless table-striped">
              <thead>
                <tr>
                  <th style="vertical-align: middle;" class="text-center">DNI/RUC</th>
                  <th style="vertical-align: middle;" class="text-center">CLIENTE</th>
                  <th style="vertical-align: middle;" class="text-center">ID CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO DE CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO DEL CRÉD.</th>
                  <th style="vertical-align: middle;" class="text-center">MONEDA</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PREACO</th>
                  <th style="vertical-align: middle;" class="text-center">TOTAL CUOTAS</th>
                  <th style="vertical-align: middle;" class="text-center">TIPO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">NÚMERO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">ESTADO <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">FECHA <br>DE CUOTA</th>
                  <th style="vertical-align: middle;" class="text-center">CAP</th>
                  <!-- <th style="vertical-align: middle;" class="text-center">AMO</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">INT</th> -->
                  <!-- <th style="vertical-align: middle;" class="text-center">CUO</th> -->
                  <th style="vertical-align: middle;" class="text-center">CUOTA REAL</th>
                  <th style="vertical-align: middle;" class="text-center">MONTO PAGADO</th>
                </tr>
              </thead>
              <tbody>
                '.$tr04.'
              </tbody>
              <tfoot>
                <tr><td colspan="15" class="text-right">Total Cobrado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_cobrado04).'</td></tr>
                <tr><td colspan="15" class="text-right">Total Facturado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_facturado04).'</td></tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="tab_5">
        <div class="container-fluid">
          <div class="row mt-4">
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h4 class="box-title text-red">Total Pendiente</h4>
                </div>
                <div class="box-body text-center">
                  <h4 class="text-red">S/ '.mostrar_moneda($total_tramo_00_pendiente_en_soles + $total_tramo_01_pendiente_en_soles + $total_tramo_02_pendiente_en_soles + $total_tramo_03_pendiente_en_soles + $total_tramo_04_pendiente_en_soles).'</h4>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h4 class="box-title text-aqua">Total Pago Parcial</h4>
                </div>
                <div class="box-body text-center">
                  <h4 class="text-aqua">S/ '.mostrar_moneda($total_tramo_00_pagoparcial_en_soles + $total_tramo_01_pagoparcial_en_soles + $total_tramo_02_pagoparcial_en_soles + $total_tramo_03_pagoparcial_en_soles + $total_tramo_04_pagoparcial_en_soles).'</h4>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h4 class="box-title text-green">Total Pagado</h4>
                </div>
                <div class="box-body text-center">
                  <h4 class="text-green">S/ '.mostrar_moneda($total_tramo_00_pagado_en_soles + $total_tramo_01_pagado_en_soles + $total_tramo_02_pagado_en_soles + $total_tramo_03_pagado_en_soles + $total_tramo_04_pagado_en_soles).'</h4>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-6 col-xs-12">
              <div class="box box-default">
                <div class="box-header bg-teal with-border">
                  <span class="box-title" style="color: white;">Total Cobrado</span>
                </div>
                <div class="box-body text-center">
                  <h4>S/ '.mostrar_moneda(( $total_tramo_00_pagoparcial_en_soles+$total_tramo_00_pagado_en_soles) + (($total_tramo_01_pagoparcial_en_soles+$total_tramo_01_pagado_en_soles)+($total_tramo_02_pagoparcial_en_soles+$total_tramo_02_pagado_en_soles)+($total_tramo_03_pagoparcial_en_soles+$total_tramo_03_pagado_en_soles)+($total_tramo_04_pagoparcial_en_soles+$total_tramo_04_pagado_en_soles))).'</h4>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-6 col-xs-12">
              <div class="box box-default">
                <div class="box-header bg-navy with-border">
                  <span class="box-title" style="color: white;">Total Facturado</span>
                </div>
                <div class="box-body text-center">
                  <h4>S/ '.mostrar_moneda(($total_tramo_00_pendiente_en_soles+$total_tramo_00_pagoparcial_en_soles+$total_tramo_00_pagado_en_soles) + (($total_tramo_01_pendiente_en_soles+$total_tramo_01_pagoparcial_en_soles+$total_tramo_01_pagado_en_soles)+($total_tramo_02_pendiente_en_soles+$total_tramo_02_pagoparcial_en_soles+$total_tramo_02_pagado_en_soles)+($total_tramo_03_pendiente_en_soles+$total_tramo_03_pagoparcial_en_soles+$total_tramo_03_pagado_en_soles)+($total_tramo_04_pendiente_en_soles+$total_tramo_04_pagoparcial_en_soles+$total_tramo_04_pagado_en_soles))).'</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-4">
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="tbl_rsegmentado05" class="table table-borderless table-striped">
                  <thead>
                    <tr>
                      <th style="vertical-align: middle;" class="text-center">DNI/RUC</th>
                      <th style="vertical-align: middle;" class="text-center">CLIENTE</th>
                      <th style="vertical-align: middle;" class="text-center">ID CRÉD.</th>
                      <th style="vertical-align: middle;" class="text-center">FECHA DE CRÉD.</th>
                      <th style="vertical-align: middle;" class="text-center">TIPO DE CRÉD.</th>
                      <th style="vertical-align: middle;" class="text-center">ESTADO DEL CRÉD.</th>
                      <th style="vertical-align: middle;" class="text-center">MONEDA</th>
                      <th style="vertical-align: middle;" class="text-center">MONTO PREACO</th>
                      <th style="vertical-align: middle;" class="text-center">TOTAL CUOTAS</th>
                      <th style="vertical-align: middle;" class="text-center">TIPO <br>DE CUOTA</th>
                      <th style="vertical-align: middle;" class="text-center">NÚMERO <br>DE CUOTA</th>
                      <th style="vertical-align: middle;" class="text-center">ESTADO <br>DE CUOTA</th>
                      <th style="vertical-align: middle;" class="text-center">FECHA <br>DE CUOTA</th>
                      <th style="vertical-align: middle;" class="text-center">CAP</th>
                      <!-- <th style="vertical-align: middle;" class="text-center">AMO</th> -->
                      <!-- <th style="vertical-align: middle;" class="text-center">INT</th> -->
                      <!-- <th style="vertical-align: middle;" class="text-center">CUO</th> -->
                      <th style="vertical-align: middle;" class="text-center">CUOTA REAL</th>
                      <th style="vertical-align: middle;" class="text-center">MONTO PAGADO</th>
                    </tr>
                  </thead>
                  <tbody>
                    '.$tr01.'
                    '.$tr02.'
                    '.$tr03.'
                    '.$tr04.'
                  </tbody>
                  <tfoot>
                    <tr><td colspan="15" class="text-right">Total Cobrado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_cobrado_total).'</td></tr>
                    <tr><td colspan="15" class="text-right">Total Facrturado</td><td colspan="1"  class="text-right">'.mostrar_moneda($sum_monto_facturado_total).'</td></tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  '
  ;
?>