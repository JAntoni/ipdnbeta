function listar(){
  // var credito_tipo = $("#cmb_tipo_creditos").val();
  var fecha01 = $("#txt_fil_fec1").val();
  var fecha02 = $("#txt_fil_fec2").val();
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val());  
  $.ajax({
    type: "POST",
    url: VISTA_URL+"rsegmentado/rsegmentado_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      // credito_tipo: 0,
      fecha01: fecha01,
      fecha02: fecha02,
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function(data) {
      $('#reversiones_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_reversiones_tabla').html(data);
      $('#reversiones_mensaje_tbl').hide(300);
    },
    complete: function(data){
      estilos_datatable_this('tbl_rsegmentado00');
      estilos_datatable_this('tbl_rsegmentado01');
      estilos_datatable_this('tbl_rsegmentado02');
      estilos_datatable_this('tbl_rsegmentado03');
      estilos_datatable_this('tbl_rsegmentado04');
      estilos_datatable_this('tbl_rsegmentado05');
    },
    error: function(error){
      console.log(error);
      $('#reversiones_mensaje_tbl').html('ERROR: ' + error.responseText);
    }
  });
}

function estilos_datatable_this(tabla) {
  datatable_global = $("#"+tabla).DataTable({
    pageLength: 25,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    order: [[10, "asc"]],
    columnDefs: [{ targets: "no-sort", orderable: false }],
  });

  datatable_texto_filtrar();
}

function validarTazaCambio(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php
  if(tipo_cambio <= 0){
    alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
    return false;
  }
}

$('#txt_fil_fec1, #txt_fil_fec2').change(function () {
  listar();
});

$(document).ready(function() {
  $("#datetimepicker1, #datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy"
  });
  validarTazaCambio();
  listar();
});