<?php
	session_name("ipdnsac");
	session_start();
	if(!isset($_SESSION['usuario_id'])){
		echo 'Terminó la sesión';
		exit();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php require_once(VISTA_URL.'templates/head.php'); ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
	</head>

	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>

		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php require_once(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php require_once(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php require_once('rsegmentado_vista.php'); ?>
			<!-- INCLUIR FOOTER-->
				<?php require_once(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php require_once(VISTA_URL.'templates/script.php'); ?>

		<script type="text/javascript" src="<?php echo VISTA_URL.'rsegmentado/rsegmentado.js?ver=160120251752';?>"></script>
	</body>
</html>