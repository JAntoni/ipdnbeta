<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Impresora extends Conexion{

    function insertar($impresora_nom, $impresora_tip, $creditotipo_id, $impresora_fec1, $impresora_fec2, $impresora_can, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_impresora(tb_impresora_nom, tb_impresora_tip, tb_creditotipo_id, tb_impresora_fec1, tb_impresora_fec2, tb_impresora_can, tb_usuario_id)
          VALUES (:impresora_nom, :impresora_tip, :creditotipo_id, :impresora_fec1, :impresora_fec2, :impresora_can, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":impresora_nom", $impresora_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":impresora_tip", $impresora_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":impresora_fec1", $impresora_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":impresora_fec2", $impresora_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":impresora_can", $impresora_can, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($impresora_id, $impresora_nom, $impresora_tip, $creditotipo_id, $impresora_fec1, $impresora_fec2, $impresora_can, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_impresora SET tb_impresora_nom =:impresora_nom, tb_impresora_tip =:impresora_tip, tb_creditotipo_id =:creditotipo_id, tb_impresora_fec1 =:impresora_fec1, tb_impresora_fec2 =:impresora_fec2, tb_impresora_can =:impresora_can, tb_usuario_id =:usuario_id WHERE tb_impresora_id =:impresora_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":impresora_id", $impresora_id, PDO::PARAM_INT);
        $sentencia->bindParam(":impresora_nom", $impresora_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":impresora_tip", $impresora_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":impresora_fec1", $impresora_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":impresora_fec2", $impresora_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":impresora_can", $impresora_can, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($impresora_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_impresora WHERE tb_impresora_id =:impresora_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":impresora_id", $impresora_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($impresora_id){
      try {
        $sql = "SELECT * FROM tb_impresora WHERE tb_impresora_id =:impresora_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":impresora_id", $impresora_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_impresoras($impresora_mes, $impresora_anio){
      try {
        $impresora_mes = intval($impresora_mes);
        $impresora_anio = intval($impresora_anio);

        $where_mes = '';
        $where_anio = '';
        if($impresora_mes > 0)
          $where_mes = ' AND MONTH(tb_impresora_fec1) =:impresora_mes';
        if($impresora_anio > 0)
          $where_anio = ' AND YEAR(tb_impresora_fec1) =:impresora_anio';

        $sql = "SELECT * FROM tb_impresora me left join tb_creditotipo cretip on cretip.tb_creditotipo_id = me.tb_creditotipo_id left join tb_usuario usu on usu.tb_usuario_id = me.tb_usuario_id WHERE 1=1".$where_mes.''.$where_anio;

        $sentencia = $this->dblink->prepare($sql);
        if($impresora_mes > 0)
          $sentencia->bindParam(":impresora_mes", $impresora_mes, PDO::PARAM_INT);
        if($impresora_anio > 0)
          $sentencia->bindParam(":impresora_anio", $impresora_anio, PDO::PARAM_INT);
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
