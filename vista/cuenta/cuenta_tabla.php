<?php
if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}

require_once('Cuenta.class.php');
$oCuenta = new Cuenta();

$fecha1= fecha_mysql($_POST['fecha1']);
$fecha2=fecha_mysql($_POST['fecha2']);
$cuenta= intval($_POST['cuenta_tipo']);
$sede=   intval($_POST['sede']);

?>
<table id="tbl_cuentas" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
            <th id="tabla_cabecera_fila">CÓDIGO</th>
            <th id="tabla_cabecera_fila">MONTO SOLES</th>
            <th id="tabla_cabecera_fila">MONTO DOLARES</th>
            <th id="tabla_cabecera_fila">OPCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //PRIMER NIVEL
        $result = $oCuenta->listar_cuentas($cuenta);

        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value):
                
                if($cuenta==1){
                    $result1=$oCuenta->listar_ingresos($fecha1, $fecha2, $value['tb_cuenta_id'],0,$sede);
                        if($result1['estado'] == 1){
                            foreach ($result1['data'] as $key => $value1){
                                if($value1['tb_moneda_id']==1){
                                    $sumasoles+=$value1['tb_ingreso_imp'];
                                }
                                else{
                                    $sumadolares+=$value1['tb_ingreso_imp'];
                                }
                                
                            }
                        }
                        $result1=NULL;
                }   
                if($cuenta==2){
                    $result1=$oCuenta->listar_egresos($fecha1, $fecha2, $value['tb_cuenta_id'],0,$sede);
                        if($result1['estado'] == 1){
                            foreach ($result1['data'] as $key => $value1){
                                if($value1['tb_moneda_id']==1){
                                    $sumasoles+=$value1['tb_egreso_imp'];
                                }
                                else{
                                    $sumadolares+=$value1['tb_egreso_imp'];
                                }
                                
                            }
                        }
                        $result1=NULL;
                }   
                    
                ?>
                <tr class="success" id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['tb_cuenta_id']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_cuenta_des']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_cuenta_cod']; ?></td>
                    <td id="tabla_fila" align="center" style="font-weight: bold;font-size: 15px;color: #330033"><?php echo 'S/ '. mostrar_moneda($sumasoles)?></td>
                    <td id="tabla_fila" align="center" style="font-weight: bold;font-size: 15px;color: #330033"><?php echo ' USS/ '.mostrar_moneda($sumadolares)?></td>
                    <td id="tabla_fila"align="center">
                        <a class="btn btn-primary btn-xs" title="Insertar" onclick="cuenta_form(<?php echo "'I'," . $value['tb_cuenta_id'] . ",0,'subcuenta'"; ?>)"><i class="fa fa-plus"></i> Subcuenta</a>
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="cuenta_form(<?php echo "'M'," . $value['tb_cuenta_id'] . ",0,'cuenta'"; ?>)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cuenta_form(<?php echo "'E'," . $value['tb_cuenta_id'] . ",0,'cuenta'"; ?>)"><i class="fa fa-trash"></i></a>
                        <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>Info</a>
                    </td>
                </tr>
                <?php
                $sumasoles=0;
                $sumadolares=0;
                //SEGUNDO NIVEL
                $cuenta_id = $value['tb_cuenta_id'];
                $result2 = $oCuenta->listar_subcuentas($cuenta_id);
                if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key => $value):
                        
                        if($cuenta==1){
                            $result1=$oCuenta->listar_ingresos($fecha1, $fecha2, $value['tb_cuenta_id'], $value['tb_subcuenta_id'],$sede);
                                if($result1['estado'] == 1){
                                    foreach ($result1['data'] as $key => $value1){
                                        if($value1['tb_moneda_id']==1){
                                            $sumasoles2+=$value1['tb_ingreso_imp'];
                                        }
                                        else{
                                            $sumadolares2+=$value1['tb_ingreso_imp'];
                                        }
                                    }
                                }
                                $result1=NULL;
                        }
                        if($cuenta==2){
                            $result1=$oCuenta->listar_egresos($fecha1, $fecha2, $value['tb_cuenta_id'], $value['tb_subcuenta_id'],$sede);
                                if($result1['estado'] == 1){
                                    foreach ($result1['data'] as $key => $value1){
                                        if($value1['tb_moneda_id']==1){
                                            $sumasoles2+=$value1['tb_egreso_imp'];
                                        }
                                        else{
                                            $sumadolares2+=$value1['tb_egreso_imp'];
                                        }
                                    }
                                }
                                $result1=NULL;
                        }
                        ?>
                        <tr>
                            <td id="tabla_fila" align="center"><div class="col-md-offset-1"><?php echo $value['tb_subcuenta_id']; ?></div></td>
                            <td id="tabla_fila">
                                <div class="col-md-offset-1">
                                    <span class="fa fa-fw fa-chevron-circle-right"></span><?php echo $value['tb_subcuenta_des']; ?>
                                    <?php
                                    if ($value['tb_subcuenta_mos'] == 0)
                                        echo '<span class="badge bg-yellow pull-right">No se muestra en reportes</span>';
                                    ?>
                                </div>

                            </td>
                            <td id="tabla_fila" align="center"><div class="col-md-offset-1"><?php echo $value['tb_subcuenta_cod']; ?></div></td>
                            <td id="tabla_fila" align="center"><div class="col-md-offset-1"><?php echo 'S/ '. mostrar_moneda($sumasoles2)?></div></td>
                            <td id="tabla_fila" align="center"><div class="col-md-offset-1"><?php echo 'USS/ '. mostrar_moneda($sumadolares2)?></div></td>
                            <td id="tabla_fila" align="center">
                                <a class="btn btn-warning btn-xs" title="Editar" onclick="cuenta_form(<?php echo "'M'," . $cuenta_id . "," . $value['tb_subcuenta_id'] . ",'subcuenta'"; ?>)"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cuenta_form(<?php echo "'E'," . $cuenta_id . "," . $value['tb_subcuenta_id'] . ",'subcuenta'"; ?>)"><i class="fa fa-trash"></i></a>
                                <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>Info</a>
                            </td>
                        </tr>
                        
                        <?php
                        $sumasoles2=0;
                        $sumadolares2=0;
                    endforeach;
                }
                $result2 = NULL;
                //FIN PRIMER NIVEL
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
</table>
