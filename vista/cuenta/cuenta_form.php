<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilmenu = new Perfilmenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('Cuenta.class.php');
  $oCuenta = new Cuenta();
  require_once('../funciones/funciones.php');

  $direc = 'cuenta';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cuenta_id = $_POST['cuenta_id'];
  $subcuenta_id = $_POST['subcuenta_id'];
  $tabla = $_POST['tabla']; //cuenta y subcuenta

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = ucwords($tabla).' Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar '.ucwords($tabla);
  elseif($usuario_action == 'M')
    $titulo = 'Editar '.ucwords($tabla);
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar '.ucwords($tabla);
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cuenta
  $result = $oPerfilmenu->accesso_directorio_Perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cuenta'; $modulo_id = $cuenta_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cuenta por su ID
    if(intval($cuenta_id) > 0){
      $result = $oCuenta->mostrarUno($cuenta_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el cuenta seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $cuenta_tip = $result['data']['tb_cuenta_tip'];
          $cuenta_des = $result['data']['tb_cuenta_des'];
          $cuenta_cod = $result['data']['tb_cuenta_cod'];
          $cuenta_padre_des = $result['data']['tb_cuenta_des'];

          if($tabla == 'subcuenta'){
            $cuenta_des = "";
            $cuenta_cod = "";
          }
        }
      $result = NULL;
    }
    if(intval($subcuenta_id) > 0){
      $result = $oCuenta->mostrarUnoSubcuenta($subcuenta_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el cuenta seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $cuenta_padre_des = $result['data']['tb_cuenta_des'];
          $cuenta_des = $result['data']['tb_subcuenta_des'];
          $cuenta_cod = $result['data']['tb_subcuenta_cod'];
          $subcuenta_mos = $result['data']['tb_subcuenta_mos'];
        }
      $result = NULL;
    }
// echo 'hasta aki estamos entrandop normal al sistema';exit();
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cuenta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cuenta" method="post">
          <input type="hidden" id="action" name="action" value="<?php echo $action;?>">
          <input type="hidden" id="hdd_cuenta_id" name="hdd_cuenta_id" value="<?php echo $cuenta_id;?>">
          <input type="hidden" id="hdd_subcuenta_id" name="hdd_subcuenta_id" value="<?php echo $subcuenta_id;?>">
          <input type="hidden" id="hdd_tabla" name="hdd_tabla" value="<?php echo $tabla;?>">
          
          <div class="modal-body">
            <?php if($tabla == 'cuenta'):?>
              <div class="form-group">
                <label for="cmb_cuenta_tip" class="control-label">Tipo Operación</label>
                <select name="cmb_cuenta_tip" id="cmb_cuenta_tip" class="form-control input-sm">
                  <option value=""></option>
                  <option value="1" <?php if($cuenta_tip == 1) echo 'selected';?> >Ingreso</option>
                  <option value="2" <?php if($cuenta_tip == 2) echo 'selected';?> >Egreso</option>
                </select>
              </div>
            <?php endif;?>
            <?php if($tabla == 'subcuenta'):?>
              <div class="form-group">
                <label for="" class="control-label">Cuenta</label>
                <input type="text" class="form-control input-sm" value="<?php echo $cuenta_padre_des;?>" readonly>
              </div>
            <?php endif;?>
            <div class="form-group">
              <label for="txt_cuenta_des" class="control-label">Descripción</label>
              <input type="text" name="txt_cuenta_des" id="txt_cuenta_des" class="form-control input-sm mayus" value="<?php echo $cuenta_des;?>">
            </div>
            <div class="form-group">
              <label for="txt_cuenta_cod" class="control-label">Código de Cuenta</label>
              <input type="text" name="txt_cuenta_cod" id="txt_cuenta_cod" class="form-control input-sm mayus" value="<?php echo $cuenta_cod;?>">
            </div>
            <?php if($tabla == 'subcuenta'):?>
              <div class="form-group">
                <label for="txt_cuenta_cod" class="control-label">Mostrar en Reporte</label>
                <br>
                <label class="radio-inline" style="padding-left: 0px;">
                  <input type="radio" name="txt_subcuenta_mos" id="txt_subcuenta_mos" value="1" class="flat-green" <?php if($subcuenta_mos ==1) echo "checked";?> > SI
                </label>
                <label class="radio-inline" style="padding-left: 0px;">
                  <input type="radio" name="txt_subcuenta_mos" id="txt_subcuenta_mos" value="0" class="flat-green" <?php if($subcuenta_mos ==0) echo "checked";?> > NO
                </label>
              </div>
            <?php endif;?>

            <!--- MESAJES DE ALERTA AL ELIMINAR -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> <?php echo '¿Está seguro de que desea eliminar esta '.ucwords($tabla).'?';?></h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cuenta_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cuenta">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cuenta">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cuenta">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cuenta/cuenta_form.js';?>"></script>
