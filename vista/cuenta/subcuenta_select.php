<?php
require_once('Cuenta.class.php');
$oCuenta = new Cuenta();

//esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$cuenta_id = (empty($cuenta_id))? intval($_POST['cuenta_id']) : intval($cuenta_id);
$subcuenta_id = (empty($subcuenta_id))? intval($_POST['subcuenta_id']) : intval($subcuenta_id);

$option = '<option value="0">Seleccione..</option>';

if(intval($cuenta_id) > 0){
	$result = $oCuenta->listar_subcuentas($cuenta_id);
		if($result['estado'] == 1)
		{
		  foreach ($result['data'] as $key => $value)
		  {
		  	$selected = '';
		  	if($subcuenta_id == $value['tb_subcuenta_id'])
		  		$selected = 'selected';

		    $option .= '<option value="'.$value['tb_subcuenta_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_subcuenta_des'].'</option>';
		  }
		}
	$result = NULL;
}

echo $option;
?>