<?php

require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../cuenta/Cuenta.class.php');
$oCuenta = new Cuenta();

$action = $_POST['action'];
$tabla = $_POST['hdd_tabla']; //cuenta y subcuenta

if ($action == 'insertar') {
    $cuenta_id = intval($_POST['hdd_cuenta_id']); //id para subcuenta
    $cuenta_tip = intval($_POST['cmb_cuenta_tip']);
    $cuenta_des = strtoupper($_POST['txt_cuenta_des']);
    $cuenta_cod = strtoupper($_POST['txt_cuenta_cod']);
    $subcuenta_mos = intval($_POST['txt_subcuenta_mos']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar la ' . ucwords($tabla);

    if ($tabla == 'cuenta') {
        if ($oCuenta->insertar($cuenta_tip, $cuenta_des, $cuenta_cod)) {
            $data['estado'] = 1;
            $data['mensaje'] = ucwords($tabla) . ' registrada correctamente.';
        }
    } elseif ($tabla == 'subcuenta') {
        if ($oCuenta->insertar_subcuenta($cuenta_id, $cuenta_des, $cuenta_cod, $subcuenta_mos)) {
            $data['estado'] = 1;
            $data['mensaje'] = ucwords($tabla) . ' registrada correctamente.';
        }
    } else
        $data['mensaje'] = 'No existe el tipo de tabla que se envia como parametro (cuenta, subcuenta): ' . $tabla;

    echo json_encode($data);
} elseif ($action == 'modificar') {
    $cuenta_id = intval($_POST['hdd_cuenta_id']); //id para subcuenta
    $subcuenta_id = intval($_POST['hdd_subcuenta_id']); //id para subcuenta
    $cuenta_tip = intval($_POST['cmb_cuenta_tip']);
    $cuenta_des = strtoupper($_POST['txt_cuenta_des']);
    $cuenta_cod = strtoupper($_POST['txt_cuenta_cod']);
    $subcuenta_mos = intval($_POST['txt_subcuenta_mos']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar la ' . ucwords($tabla);

    if ($tabla == 'cuenta') {
        if ($oCuenta->modificar($cuenta_id, $cuenta_tip, $cuenta_des, $cuenta_cod)) {
            $data['estado'] = 1;
            $data['mensaje'] = ucwords($tabla) . ' modificada correctamente.';
        }
    } elseif ($tabla == 'subcuenta') {
        if ($oCuenta->modificar_subcuenta($subcuenta_id, $cuenta_id, $cuenta_des, $cuenta_cod, $subcuenta_mos)) {
            $data['estado'] = 1;
            $data['mensaje'] = ucwords($tabla) . ' modificada correctamente.';
        }
    } else
        $data['mensaje'] = 'No existe el tipo de tabla que se envia como parametro (cuenta, subcuenta): ' . $tabla;

    echo json_encode($data);
} elseif ($action == 'eliminar') {
    $cuenta_id = intval($_POST['hdd_cuenta_id']);
    $subcuenta_id = intval($_POST['hdd_subcuenta_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar la ' . ucwords($tabla);

    if ($tabla == 'cuenta') {
        if ($oCuenta->eliminar($cuenta_id)) {
            $data['estado'] = 1;
            $data['mensaje'] = ucwords($tabla) . ' eliminada correctamente.';
        }
    } elseif ($tabla == 'subcuenta') {
        if ($oCuenta->eliminar_subcuenta($subcuenta_id)) {
            $data['estado'] = 1;
            $data['mensaje'] = ucwords($tabla) . ' eliminada correctamente.';
        }
    }

    echo json_encode($data);
} else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>