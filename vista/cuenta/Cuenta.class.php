<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cuenta extends Conexion{

    function insertar($cuenta_tip, $cuenta_des, $cuenta_cod){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuenta(tb_cuenta_xac, tb_cuenta_tip, tb_cuenta_des, tb_cuenta_cod) VALUES (1, :cuenta_tip, :cuenta_des, :cuenta_cod)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuenta_tip", $cuenta_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cuenta_des", $cuenta_des, PDO::PARAM_STR);
        $sentencia->bindParam(":cuenta_cod", $cuenta_cod, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($cuenta_id, $cuenta_tip, $cuenta_des, $cuenta_cod){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuenta SET tb_cuenta_tip =:cuenta_tip, tb_cuenta_des =:cuenta_des, tb_cuenta_cod =:cuenta_cod WHERE tb_cuenta_id =:cuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuenta_tip", $cuenta_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cuenta_des", $cuenta_des, PDO::PARAM_STR);
        $sentencia->bindParam(":cuenta_cod", $cuenta_cod, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($cuenta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cuenta WHERE tb_cuenta_id =:cuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($cuenta_id){
      try {
        $sql = "SELECT * FROM tb_cuenta WHERE tb_cuenta_id =:cuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cuentas($tipocuenta){
      try {
        $sql = "SELECT * FROM tb_cuenta WHERE tb_cuenta_xac =1 AND tb_cuenta_tip=:tipocuenta ORDER BY tb_cuenta_des";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tipocuenta", $tipocuenta, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "El usuario no tiene un cuenta asignado";
          $retorno["data"] = "";
        }

        return $retorno;

      } catch (Exception $e) {
        throw $e;
      }
    }
    //*************************** FUNCIONES PARA SUB CUENTA *****************************************
    function insertar_subcuenta($cuenta_id, $subcuenta_des, $subcuenta_cod, $subcuenta_mos){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_subcuenta(tb_subcuenta_xac, tb_cuenta_id, tb_subcuenta_des, tb_subcuenta_cod, tb_subcuenta_mos) VALUES (1, :cuenta_id, :subcuenta_des, :subcuenta_cod, :subcuenta_mos)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":subcuenta_des", $subcuenta_des, PDO::PARAM_STR);
        $sentencia->bindParam(":subcuenta_cod", $subcuenta_cod, PDO::PARAM_STR);
        $sentencia->bindParam(":subcuenta_mos", $subcuenta_mos, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_subcuenta($subcuenta_id, $cuenta_id, $subcuenta_des, $subcuenta_cod, $subcuenta_mos){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_subcuenta SET tb_cuenta_id =:cuenta_id, tb_subcuenta_des =:subcuenta_des, tb_subcuenta_cod =:subcuenta_cod, tb_subcuenta_mos =:subcuenta_mos WHERE tb_subcuenta_id =:subcuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":subcuenta_id", $subcuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":subcuenta_des", $subcuenta_des, PDO::PARAM_STR);
        $sentencia->bindParam(":subcuenta_cod", $subcuenta_cod, PDO::PARAM_STR);
        $sentencia->bindParam(":subcuenta_mos", $subcuenta_mos, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar_subcuenta($subcuenta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_subcuenta WHERE tb_subcuenta_id =:subcuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":subcuenta_id", $subcuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUnoSubcuenta($subcuenta_id){
      try {
        $sql = "SELECT * FROM tb_subcuenta subc INNER JOIN tb_cuenta cu on cu.tb_cuenta_id=subc.tb_cuenta_id WHERE tb_subcuenta_id =:subcuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":subcuenta_id", $subcuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_subcuentas($cuenta_id){
      try {
        $sql = "SELECT * FROM tb_subcuenta WHERE tb_cuenta_id =:cuenta_id AND tb_subcuenta_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuenta_id", $cuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "El usuario no tiene un cuenta asignado";
          $retorno["data"] = "";
        }

        return $retorno;

      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    
    
    
    
    
    function listar_ingresos($fec1, $fec2,$tb_cuenta_id,$tb_subcuenta_id,$sede) {
        if(intval($tb_subcuenta_id)>0){
            $tb_subcuenta=" AND tb_subcuenta_id=:tb_subcuenta_id";
        }
        try {
            $sql = "SELECT 
                            * 
                    FROM 
                        tb_ingreso 
                    WHERE 
                        tb_ingreso_xac=1 AND tb_cuenta_id=:tb_cuenta_id
                        AND tb_ingreso_fec BETWEEN :fec1 AND :fec2 AND tb_empresa_id=:tb_empresa_id ".$tb_subcuenta;

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_cuenta_id", $tb_cuenta_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_empresa_id", $sede, PDO::PARAM_INT);
            if(intval($tb_subcuenta_id)>0){
            $sentencia->bindParam(":tb_subcuenta_id", $tb_subcuenta_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    
    function listar_egresos($fec1, $fec2,$tb_cuenta_id,$tb_subcuenta_id,$sede) {
        if(intval($tb_subcuenta_id)>0){
            $tb_subcuenta=" AND tb_subcuenta_id=:tb_subcuenta_id";
        }
      try {
        $sql = "SELECT 
                        * 
                FROM 
                        tb_egreso 
                WHERE 
                        tb_egreso_xac=1 AND tb_cuenta_id=:tb_cuenta_id
                        AND tb_egreso_fec BETWEEN :fec1 AND :fec2 AND tb_empresa_id=:tb_empresa_id ".$tb_subcuenta;
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fec1", $fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":fec2", $fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuenta_id", $tb_cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_empresa_id", $sede, PDO::PARAM_INT);
        if(intval($tb_subcuenta_id)>0){
            $sentencia->bindParam(":tb_subcuenta_id", $tb_subcuenta_id, PDO::PARAM_INT);
        }
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Egresos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
    
    
    
    
    
  }

?>
