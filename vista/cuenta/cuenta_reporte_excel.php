<?php
//echo 'hasta aki estoy entrando normal '; exit();
//require_once('../../core/usuario_sesion.php');
//error_reporting(0);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';

require_once('../cuenta/Cuenta.class.php');
$oCuenta = new Cuenta();
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");




$fecha1= fecha_mysql($_GET['fecha1']);
$fecha2=fecha_mysql($_GET['fecha2']);
$cuenta= intval($_GET['cuenta_tipo']);
$sede=   intval($_GET['sede']);
//echo 'fecha 1= '.$fecha1.' fecha 2= '.$fecha2.' $cuenta ='.$cuenta.' sede ='.$sede; exit();


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("cristhian.cyga@gmail.com")
	->setLastModifiedBy("cristhian.cyga@gmail.com")
	->setTitle("Reporte De Montos de Cuentas")
	->setSubject("monto de cuentas")
	->setDescription("Reporte generado por cristhian.cyga@gmail.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
    'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '135896')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);
$estiloTituloFilas2 = array(
    'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '135825')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

  $nombre_archivo = "IPDNSAC - ".mostrar_fecha($_GET['fecha1']);

  $c = 1;
  $titulo = "FORMATO DE MONTOS DE CUENTAS";
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:E$c");
  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("A$c:E$c")->applyFromArray($estiloTituloColumnas);
//  $objPHPExcel->getActiveSheet()->setCellValue("D1", mostrar_fecha($fecha1));

  $c = 2;
//  $nombre_fecha = "FORMATO DE APERTURA Y CIERRE DE CAJA DE LA FECHA: ".mostrar_fecha($fecha1)." HASTA ".mostrar_fecha($fecha2);
//  $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $nombre_fecha);

  $c = $c + 1;

  $titulosColumnas = array(
    'ID',
    'DESCRIPCIÓN',
    'CÓDIGO',
    'MONTO SOLES',
    'MONTO DÓLARES'
  );

  $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
  ;

  $objPHPExcel->getActiveSheet()->getStyle("A$c:E$c")->applyFromArray($estiloTituloColumnas);

$c = $c+1;  
$total_ingresos = 0;
//echo 'sfahsahfsa == '.$result['estado'];          exit();
$result = $oCuenta->listar_cuentas($cuenta);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
          
          
          if($cuenta==1){
                    $result1=$oCuenta->listar_ingresos($fecha1, $fecha2, $value['tb_cuenta_id'],0,$sede);
                        if($result1['estado'] == 1){
                            foreach ($result1['data'] as $key => $value1){
                                if($value1['tb_moneda_id']==1){
                                    $sumasoles+=$value1['tb_ingreso_imp'];
                                }
                                else{
                                    $sumadolares+=$value1['tb_ingreso_imp'];
                                }
                                
                            }
                        }
                        $result1=NULL;
                }   
                if($cuenta==2){
                    $result1=$oCuenta->listar_egresos($fecha1, $fecha2, $value['tb_cuenta_id'],0,$sede);
                        if($result1['estado'] == 1){
                            foreach ($result1['data'] as $key => $value1){
                                if($value1['tb_moneda_id']==1){
                                    $sumasoles+=$value1['tb_egreso_imp'];
                                }
                                else{
                                    $sumadolares+=$value1['tb_egreso_imp'];
                                }
                                
                            }
                        }
                        $result1=NULL;
                }  
          

        $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas2);
        $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas2);
        $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas2);
        $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas2);
        $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas2);
        
        $objPHPExcel->getActiveSheet()
          ->setCellValue('A'.$c, $value['tb_cuenta_id'])
          ->setCellValue('B'.$c, $value['tb_cuenta_des'])
          ->setCellValue('C'.$c, $value['tb_cuenta_cod'])
          ->setCellValue('D'.$c, 'S/ '. mostrar_moneda($sumasoles))
          ->setCellValue('E'.$c, 'USS/ '.mostrar_moneda($sumadolares))
        ;
//        $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//        $objPHPExcel->getActiveSheet()->getStyle('I'.$c)->getNumberFormat()->setFormatCode('00000000');
        $objPHPExcel->getActiveSheet(0)->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $objPHPExcel->getActiveSheet(0)->getStyle('E'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $sumasoles=0;
        $sumadolares=0;
        $c++;
        
        
        
        $cuenta_id = $value['tb_cuenta_id'];
        $result2 = $oCuenta->listar_subcuentas($cuenta_id);
            if ($result2['estado'] == 1) {

            foreach ($result2['data'] as $key => $value){
                
                
                
                if($cuenta==1){
                            $result1=$oCuenta->listar_ingresos($fecha1, $fecha2, $value['tb_cuenta_id'], $value['tb_subcuenta_id'],$sede);
                                if($result1['estado'] == 1){
                                    foreach ($result1['data'] as $key => $value1){
                                        if($value1['tb_moneda_id']==1){
                                            $sumasoles2+=$value1['tb_ingreso_imp'];
                                        }
                                        else{
                                            $sumadolares2+=$value1['tb_ingreso_imp'];
                                        }
                                    }
                                }
                                $result1=NULL;
                        }
                        if($cuenta==2){
                            $result1=$oCuenta->listar_egresos($fecha1, $fecha2, $value['tb_cuenta_id'], $value['tb_subcuenta_id'],$sede);
                                if($result1['estado'] == 1){
                                    foreach ($result1['data'] as $key => $value1){
                                        if($value1['tb_moneda_id']==1){
                                            $sumasoles2+=$value1['tb_egreso_imp'];
                                        }
                                        else{
                                            $sumadolares2+=$value1['tb_egreso_imp'];
                                        }
                                    }
                                }
                                $result1=NULL;
                        }
                
                


                    $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
                    $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
                    $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);
                    $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas);
                    $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas);

                    $objPHPExcel->getActiveSheet()
                      ->setCellValue('A'.$c, $value['tb_subcuenta_id'])
                      ->setCellValue('B'.$c, $value['tb_subcuenta_des'])
                      ->setCellValue('C'.$c, $value['tb_subcuenta_cod'])
                      ->setCellValue('D'.$c, 'S/ '. mostrar_moneda($sumasoles2))
                      ->setCellValue('E'.$c, 'USS/ '. mostrar_moneda($sumadolares2))
                    ;
                    $objPHPExcel->getActiveSheet(0)->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                    $objPHPExcel->getActiveSheet(0)->getStyle('E'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$c++;
$sumasoles2=0;
                        $sumadolares2=0;


            }
            }
        
      }
    }
  $result = NULL;

  //for($z = 'A'; $z <= 'O'; $z++){
    //$objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
  //}
  

  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
  

  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('MONTO DE CUENTAS POR MES');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0*/
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=".$nombre_archivo.".csv");
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
