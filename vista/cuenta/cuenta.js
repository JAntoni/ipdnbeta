var datatable_global;

function cuenta_form(usuario_act, cuenta_id, subcuenta_id, tabla) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuenta/cuenta_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            cuenta_id: cuenta_id,
            subcuenta_id: subcuenta_id,
            tabla: tabla //cuenta y subcuenta
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_cuenta_form').html(data);
                $('#modal_registro_cuenta').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_cuenta'); //funcion encontrada en public/js/generales.js

                modal_hidden_bs_modal('modal_registro_cuenta', 'limpiar'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_cuenta'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'cuenta';
                var div = 'div_modal_cuenta_form';
                permiso_solicitud(usuario_act, cuenta_id, modulo, div); //funcion ubicada en public/js/generales.js
            }
        },
        complete: function (data) {
        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

function cuenta_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuenta/cuenta_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fecha1:$("#txt_filtro_fec1").val(),
            fecha2:$("#txt_filtro_fec2").val(),
            cuenta_tipo:$("#cmb_cuenta_tip").val(),
            sede:$("#cbm_empresa_id").val()
        }),
        beforeSend: function () {
            $('#cuenta_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_cuenta_tabla').html(data);
            $('#cuenta_mensaje_tbl').hide(300);
            estilos_datatable();
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#cuenta_mensaje_tbl').html('ERROR AL LISTAR LOS MENÚS: ' + data.responseText);
        }
    });
}
function estilos_datatable() {
    datatable_global = $('#tbl_cuentas').DataTable({
        "pageLength": 25,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Página mostrada _PAGE_ de _PAGES_",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [1, 2, 3], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function caja_reporte(){
    
var fecha1=$("#txt_filtro_fec1").val();
var fecha2=$("#txt_filtro_fec2").val();
var cuenta_tipo=$("#cmb_cuenta_tip").val();
var sede=$("#cbm_empresa_id").val();


//  window.open("https://ipdsac.ipdnsac/ipdnsac/vista/flujocaja/reporte1_excel.php");
  window.open("http://www.ipdnsac.com/ipdnsac/vista/cuenta/cuenta_reporte_excel.php?fecha1="+fecha1+"&fecha2="+fecha2+"&cuenta_tipo="+cuenta_tipo+"&sede="+sede);
//  window.open("https://localhost/ipdnsac/vista/cuenta/cuenta_reporte_excel.php?fecha1="+fecha1+"&fecha2="+fecha2+"&cuenta_tipo="+cuenta_tipo+"&sede="+sede);
//  window.open("https://localhost/ipdnsac/vista/flujocaja/reporte1_excel.php?fecha1="+fecha1+"&fecha2="+fecha2+"&empre_id="+emp_id+"&caj_id="+caj_id+"&mon_id="+mon_id+"&cli_id="+cli_id+"&usuario_id="+usuario_id);
}
$(document).ready(function () {

 $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    cuenta_tabla();
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    cuenta_tabla();
  });


$("#cmb_cuenta_tip,#cbm_empresa_id").on("change", function (e) {
    cuenta_tabla();
  });

    cuenta_tabla();
});