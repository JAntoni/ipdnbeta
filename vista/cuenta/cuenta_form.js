
function cuenta_idpadre_seleccionado(){
	var action = $('#action').val();
	var cuenta_idp = $('#hdd_cuenta_idp').val();
	if(action && cuenta_idp && (action == 'modificar' || action == 'leer')){
		
	}
}
$(document).ready(function(){

	$('input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#cmb_cuenta_id').selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: 'ES'
  });

  $('#txt_cuenta_dir').keypress(function(e) {
  	if (e.keyCode === 0 || e.keyCode === 32) {
    	return false;
  	}
  });
  
  console.log('cambio 222');

  $('#form_cuenta').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"cuenta/cuenta_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cuenta").serialize(),
				beforeSend: function() {
					$('#cuenta_mensaje').show(400);
					$('#btn_guardar_cuenta').prop('disabled', true);
				},
				success: function(data){
					console.log(data);
					if(parseInt(data.estado) > 0){
						$('#cuenta_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#cuenta_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		cuenta_tabla();
		      		$('#modal_registro_cuenta').modal('hide');
		      		}, 1000
		      	);
					}
					else{
		      	$('#cuenta_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#cuenta_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_cuenta').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#cuenta_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#cuenta_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
	      	$('#btn_guardar_cuenta').prop('disabled', false);
				}
			});
	  },
	  rules: {
			cmb_cuenta_tip: {
				required: function(){
					var tabla = $('#hdd_tabla').val();
					if(tabla == 'cuenta')
						return true
					else
						return false;
				}
			},
			txt_cuenta_des: {
				required: true,
				minlength: 3
			}
		},
		messages: {
			cmb_cuenta_tip: {
				required: "Ingrese el tipo de operación para la Cuenta"
			},
			txt_cuenta_des: {
				required: "Ingrese una descripción de la Cuenta o Subcuenta",
				minlength: "La descripción debe tener como mínimo 3 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
