<?php
  require_once('../../core/usuario_sesion.php');
  require_once("../rendicioncuentas/RendicionCuentas.class.php");
  $oRendicionCuentas = new RendicionCuentas();
  require_once('../gasto/Gasto.class.php');
  $oGasto = new Gasto();
  require_once("../rendicioncuentasdetalle/RendicionCuentasDetalle.class.php");
  $oRendicionCuentasDetalle = new RendicionCuentasDetalle();
  require_once('../ingreso/Ingreso.class.php');
  $oIngreso = new Ingreso();

  $action = $_POST['action'];
  if($action == 'cerrar'){
    $data['estado'] = 0;
    $data['mensaje'] = 'Exite un error al cerrar la rendicion';

    $rendicioncuentasid = $_POST['rendicioncuentasid'];
    $cerrar = $oRendicionCuentas->modificar_campo_nuevo($rendicioncuentasid, intval($_SESSION['usuario_id']), 'estado', 2);
    if($cerrar){
        $data['estado'] = 1;
        $data['mensaje'] = 'Rendicion Cerrada';
    }
    echo json_encode($data);
  }
  elseif($action == 'eliminar'){
    $data['estado'] = 0;
    $data['mensaje'] = 'Exite un error al eliminar la rendicion';

    $rendicioncuentasid = $_POST['rendicioncuentasid'];

    $cuenta = $oRendicionCuentas->mostrarUno($rendicioncuentasid);
    if($cuenta['estado'] == 1){
        $gasto_id = $cuenta['data']['tb_gasto_id'] ;
    }

    $result = $oRendicionCuentas->eliminar($rendicioncuentasid);
    if($result){
      $data['estado'] = 1;
      $data['mensaje'] = 'Rendicion Cuenta Eliminada. ';
      if($oRendicionCuentasDetalle->eliminarxrendicion($rendicioncuentasid)){
        $data['mensaje'] .= ' Detalles eliminados.';
        if($oGasto->modificar_campo($gasto_id, "tb_gasto_xac", 0, "INT")){
          $data['mensaje'] .= ' Gasto eliminado.';
          if($oRendicionCuentasDetalle->eliminaringresosxrendicion($rendicioncuentasid)){
            $data['mensaje'] .= ' RendicionCuentasIngresos eliminado.';

            $cuenta = $oRendicionCuentasDetalle->mostrarUnoIngresoxRendicion($rendicioncuentasid);
            if($cuenta['estado'] == 1){
                $ingreso_id = $cuenta['data']['tb_ingreso_id'];
                $result1 = $oIngreso->eliminar_2($ingreso_id);
                if($result1 == 1){
                  $data['mensaje'] .= ' Ingreso eliminado.';
                }
                $result1 = null;
            }
          }
        }
      }
    }
    $result = null;
    echo json_encode($data);
  }
?>