<form id="form_rendicioncuenta_filtro" role="form">
    <div class="row">
        <input type="hidden" name="fecha_hoy" id="fecha_hoy" value="<?php echo date('d-m-Y'); ?>">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="form-group">
            <label for="txt_filtro_fec1" class="control-label">Fecha Inicio :</label>
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo date('d-m-Y'); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="form-group">
            <label for="txt_filtro_fec2" class="control-label">Fecha Fin :</label>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo date('d-m-Y'); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-8">
            <div class="form-group">
                <label for="cmb_proveedor_id" class="control-label">Proveedor</label>
                <select name="cmb_proveedor_id" id="cmb_proveedor_id" class="selectpicker form-control input-sm" data-live-search="true" data-max-options="1">
                  <?php require_once 'vista/proveedor/proveedor_select.php';?>
                </select>
              </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-8">
            <div class="form-group">
                <label for="cmb_estado_id" class="control-label">Estado</label>
                <select name="cmb_estado_id" id="cmb_estado_id" class="selectpicker form-control input-sm" data-live-search="true" data-max-options="1">
                    <option value="0">[SELECCIONE]</option>
                    <option value="1">PENDIENTE</option>
                    <option value="2">RESUELTO</option>
                </select>
              </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-8">
            <label for="cmb_estado_id" class="control-label" style="color: white; display: block;">AA</label>
            <button type="button" class="btn btn-info btn-sm" onclick="rendicioncuentas_tabla()"><i class="fa fa-search"></i> Buscar</button>
            <button type="button" class="btn btn-danger btn-sm" onclick="limpiar_filtro()"><i class="fa fa-trash"></i> Limpiar</button>
        </div>
    </div>
</form> 