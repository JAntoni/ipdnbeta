$(document).ready(function() {
    rendicioncuentadetalle_tabla()

    $("#rendicioncuenta_detalle").hide()

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });

    $("#for_rendicioncuentadetalle").validate({
        submitHandler: function() {
            //console.log("AAAAAAAAAAAA");
			$.ajax({
				type: "POST",
				url: VISTA_URL+"rendicioncuentasdetalle/rendicioncuentasdetalle_controller.php",
				async:true,
				dataType: "json",
				data: $('#for_rendicioncuentadetalle').serialize(),
				beforeSend: function() {
					$('#retencion_mensaje').html("Guardando...");
					$('#retencion_mensaje').show(100);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						alerta_success("EXITO", data.mensaje)
                        limpiar_form()
						rendicioncuentadetalle_tabla();
						//$("#modal_registro_retencion" ).modal( "hide" );
					}else{
                        console.log(data);
                        
                        alerta_error("ERROR", data.mensaje)
                        //limpiar_form()
                    }				
				},
				complete: function(data){
					/*if(data.statusText != "success"){
						alerta_error("ERROR", data.mensaje)
					}*/
                    console.log(data);
                    
				}
			});
		},
		rules: {
			cmb_mon_id: {
				required: true
			},
			txt_importe: {
				required: true
			},
			txt_detalle: {
				required: true
			},
		},
		messages: {
			cmb_mon_id: {
				required: 'Seleccione Moneda'
			},
			txt_importe: {
				required: 'Ingrese Importe'
			},
			txt_detalle: {
				required: 'Ingrese Comentario'
			},
		}
	});
});

function agregar_detalle(){
    $("#rendicioncuenta_detalle").show()
}

function filestorage_form(rendicioncuentasdetalleid){
    var filestorage_rendicioncuentasdetalleID = rendicioncuentasdetalleid
    //console.log('Enviando el parametro credito id:'+filestorage_rendicioncuentasdetalleID)
    $.ajax({
        type: "POST",
        url: VISTA_URL + "filestorage/filestorage_form2.php",
        async: true,
        dataType: "html",
        data: ({
          action: "I", //insertar
          public_carpeta: 'pdf',
          modulo_nom: 'rendicioncuentadetalle',
          modulo_id: filestorage_rendicioncuentasdetalleID,
          modulo_subnom: 'rendicioncuentadetalle_pdf',
          filestorage_uniq: '',
          filestorage_des: 'pdf-rendicioncuentadetalle-'+filestorage_rendicioncuentasdetalleID
        }),
        beforeSend: function () { 
          
        },
        success: function (data) {
          //console.log(data)
          if (data != 'sin_datos') {
            $('#div_filestorage_form').html(data);
            $('#modal_registro_filestorage').modal('show');
            modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
          }
        },
        complete: function (data) {
        },
        error: function (data) {
          
        }
    });
}

function previewFile(url) {
    // Obtener la URL del input
    //const fileUrl = document.getElementById("fileUrl").value;
    let tipo = 0;
    let arr_url_img = [];
    let url_pdf = "";
    const fileUrl = "http://192.168.101.75/ipdnsac/"+url;

    // Validar si es imagen o PDF
    if (fileUrl.endsWith(".jpg") || fileUrl.endsWith(".jpeg") || fileUrl.endsWith(".png") || fileUrl.endsWith(".gif")) {
        // Mostrar imagen
        tipo = 1;
        arr_url_img.push(fileUrl);
    } else if (fileUrl.endsWith(".pdf")) {
        // Mostrar PDF usando <iframe>
        tipo = 2;
        url_pdf = fileUrl;
    }
    console.log(arr_url_img);

    if(tipo == 0){
        alerta_warning("ALERTA", "EL FORMATO NO ES ACEPTABLE")
    }else{
        $.ajax({
            type: "POST",
            url: VISTA_URL + "filestorage/filestorage_archivo.php",
            async: false,
            dataType: "html",
            data: {
                tipo: tipo,
                arr_url_img: JSON.stringify(arr_url_img),
                url_pdf: url_pdf
            },
            beforeSend: function () {
                $('#div_filestorage_modal').html('<option value="">Cargando...</option>');
                $('#modal_filestorage_archivo').modal('show');
            },
            success: function (data) {
                $('#div_filestorage_modal').html(data);
                $('#modal_filestorage_archivo').modal('show');
            },
            complete: function (data) {
                //console.log(data);
            },
            error: function (data) {
                //$('#ejecucion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
                alerta_error("ERROR", data.responseText)
            }
        });
    }
}

function rendicioncuentadetalle_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "rendicioncuentasdetalle/rendicioncuentasdetalle_tabla.php",
        async: false,
        dataType: "html",
        data: {
            rendicioncuentas_id: $("#hdd_rendicioncuentas_id").val()
        },
        beforeSend: function () {
            $('#div_rendicioncuentasdetalle_tabla').html('<option value="">Cargando...</option>');
        },
        success: function (data) {
            $('#div_rendicioncuentasdetalle_tabla').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            //$('#ejecucion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
            alerta_error("ERROR", data.responseText)
        }
    });
}

function limpiar_form(){
    $("#txt_importe").val("")
    $("#txt_detalle").val("")
    $("#rendicioncuenta_detalle").hide()
}

function ver_archivo(url){
    //var filestorage_rendicioncuentasdetalleID = rendicioncuentasdetalleid
    //console.log('Enviando el parametro credito id:'+filestorage_rendicioncuentasdetalleID)
    $.ajax({
        type: "POST",
        url: VISTA_URL + "filestorage/filestorage_form2.php",
        async: true,
        dataType: "html",
        data: ({
          action: "I", //insertar
          public_carpeta: 'pdf',
          modulo_nom: 'rendicioncuentadetalle',
          modulo_id: filestorage_rendicioncuentasdetalleID,
          modulo_subnom: 'rendicioncuentadetalle_pdf',
          filestorage_uniq: '',
          filestorage_des: 'pdf-rendicioncuentadetalle-'+filestorage_rendicioncuentasdetalleID
        }),
        beforeSend: function () { 
          
        },
        success: function (data) {
          //console.log(data)
          if (data != 'sin_datos') {
            $('#div_filestorage_form').html(data);
            $('#modal_registro_filestorage').modal('show');
            modal_hidden_bs_modal('modal_registro_filestorage', 'limpiar'); //funcion encontrada en public/js/generales.js
          }
        },
        complete: function (data) {
        },
        error: function (data) {
          
        }
    });
}

function ingreso_form(usuario_act, ingreso_id){ 
    $.ajax({
        type: "POST",
        url: VISTA_URL+"ingreso/ingreso_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            ingreso_id: ingreso_id,
            vista: 'rendicioncuentas',
            cuenta_id: 2,
            subcuenta_id: 9,
            ingreso_imp: $("#txt_monto_vuelto").val(),
            moneda_egreso_id: $("#hdd_moneda_egreso_id").val(),
            egreso_numdoc: $("#hdd_egreso_numdoc").val(),
            rendicioncuentas_id: $("#hdd_rendicioncuentas_id").val(),
        }),
          beforeSend: function() {
        $('#h3_modal_title').text('Cargando Formulario');
        $('#modal_mensaje').modal('show');
          },
          success: function(data){
        $('#modal_mensaje').modal('hide');
        if(data != 'sin_datos'){
            $('#div_ingreso_modal').html(data);
            $('#modal_registro_ingreso').modal('show');
  
          //desabilitar elementos del form si es L (LEER)
          if(usuario_act == 'L' || usuario_act == 'E')
            form_desabilitar_elementos('form_ingreso'); //funcion encontrada en public/js/generales.js
          
          //funcion js para agregar un largo automatico al modal, al abrirlo
          modal_height_auto('modal_registro_ingreso'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_registro_ingreso', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
        else{
            //llamar al formulario de solicitar permiso
            var modulo = 'ingreso';
            var div = 'div_ingreso_modal';
            permiso_solicitud(usuario_act, ingreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
        }
          },
          complete: function(data){
              
          },
          error: function(data){
        $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
        $('#overlay_modal_mensaje').removeClass('overlay').empty();
        $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
              console.log(data.responseText);
          }
      });
}

function cerrar_rendicion(rendicioncuentas_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL+"rendicioncuentas/rendicioncuentas_controller.php",
        async:true,
        dataType: "json",
        data: ({
            action: "cerrar",
            rendicioncuentasid: rendicioncuentas_id
        }),
        beforeSend: function() {
            $('#retencion_mensaje').html("Guardando...");
            $('#retencion_mensaje').show(100);
        },
        success: function(data){
            if(parseInt(data.estado) > 0){
                alerta_success("EXITO", data.mensaje)
                rendicioncuentas_tabla();
                $('#modal_registro_rendicioncuentas').modal('hide');
                //$("#modal_registro_retencion" ).modal( "hide" );
            }else{
                console.log(data);
                
                alerta_error("ERROR", data.mensaje)
                //limpiar_form()
            }				
        },
        complete: function(data){
            /*if(data.statusText != "success"){
                alerta_error("ERROR", data.mensaje)
            }*/
            console.log(data);
            
        }
    });
}