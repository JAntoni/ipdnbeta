<?php
	
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<span><b>Filtros</b></span>
						<div class="panel panel-primary" id="div_filtros_cgv">
							<div class="panel-body">
								<?php include VISTA_URL . 'rendicioncuentas/rendicioncuentas_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="rendicioncuentas_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
        </div>

				<div class="row">
					<div class="col-sm-12">
						<div id="div_rendicioncuentas_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php //require_once('rendicioncuentas_tabla.php');?>
						</div>
					</div>
				</div>
			</div>
			<div id="div_modal_rendicioncuentas_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<div id="div_modal_gasto_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<div id="div_modal_proveedor_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO/EDICION DE PROVEEDOR-->
			</div>
			<div id="div_filestorage_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO/EDICION DE PROVEEDOR-->
			</div>
			<div id="div_filestorage_modal">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO/EDICION DE PROVEEDOR-->
			</div>
			<div id="div_ingreso_modal">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO/EDICION DE PROVEEDOR-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
