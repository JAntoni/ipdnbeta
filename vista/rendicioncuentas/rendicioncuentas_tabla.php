<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('RendicionCuentas.class.php');
  $oRendicionCuentas = new RendicionCuentas();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $filtro_fec1 = fecha_mysql($_POST['txt_filtro_fec1']);
  $filtro_fec2 = fecha_mysql($_POST['txt_filtro_fec2']);
  $proveedor_id = $_POST['cmb_proveedor_id'];
  $estado_id = $_POST['cmb_estado_id'];

  function estado_rendicion($crendicion_est){
    $estado = '<span class="badge bg-orange">PENDIENTE</span>';
    if($crendicion_est == 2)
      $estado = '<span class="badge bg-green">RENDIDO</span>';
    return $estado;
  }

  $result = $oRendicionCuentas->listar_rendicioncuentas($filtro_fec1, $filtro_fec2, $proveedor_id, $estado_id);
  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $btngasto = '';
      //$gasto = intval($value['tb_gasto_opcion'])==0?"NO":"SI";
      $gasto_id = intval($value['tb_gasto_opcion'])==0?'':$value['tb_gasto_id'];
      if(intval($value['tb_rendicioncuenta_estado'])==1){
        if(intval($value['tb_gasto_opcion'])==0){
          $btngasto = '<a class="btn btn-success btn-xs" title="GASTO" onclick="form_gasto(\'I\', 0, '.$value['tb_rendicioncuenta_id'].')"><i class="fa fa-money"></i> GV</a>';
        }else{
          $btngasto = '<a class="btn btn-primary btn-xs" title="GASTO" onclick="form_gasto(\'I\', 0, '.$value['tb_rendicioncuenta_id'].')"><i class="fa fa-info"></i> GV</a>';
        }
      }
      
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_rendicioncuenta_id'].'</td>
          <td>'.$value['tb_rendicioncuenta_fec'].'</td>
          <td>'.$value['tb_egreso_numdoc'].'</td>
          <td>'.mostrar_moneda($value['tb_egreso_imp']).'</td>
          <td>'.$value['tb_proveedor_doc'].' - '.$value['tb_proveedor_nom'].'</td>
          <td>'.$gasto_id.'</td>
          <td>'.estado_rendicion($value['tb_rendicioncuenta_estado']).'</td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="rendicioncuentas_form(\'M\','.$value['tb_rendicioncuenta_id'].')"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="rendicioncuentas_delete('.$value['tb_rendicioncuenta_id'].')"><i class="fa fa-trash"></i></a>
            '.$btngasto.'
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    $tr ='<tr><td colspan="8">'.$result['mensaje'].'</td></tr>';
    //exit();
  }

?>
<table id="tbl_rendicioncuentas" class="table table-bordered table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila">Fecha Creacion</th>
      <th id="tabla_cabecera_fila">Egreso</th>
      <th id="tabla_cabecera_fila">Importe</th>
      <th id="tabla_cabecera_fila">Proveedor</th>
      <th id="tabla_cabecera_fila">Gasto ID</th>
      <th id="tabla_cabecera_fila">Estado</th>
      <th id="tabla_cabecera_fila">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>

