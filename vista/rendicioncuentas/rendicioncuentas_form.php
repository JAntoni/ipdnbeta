<?php
require_once('../../core/usuario_sesion.php');

require_once ("./RendicionCuentas.class.php");
$oRendicionCuentas = new RendicionCuentas();

/*
    require_once ("./RendicionCuentasDetalle.class.php");
    $oRendicionCuentasDetalle = new RendicionCuentasDetalle();
*/

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$rendicioncuentas_id = $_POST['rendicioncuentas_id'];

$cuenta = $oRendicionCuentas->mostrarUno($rendicioncuentas_id);
if($cuenta['estado'] == 1){
    $moneda_egreso_id = $cuenta['data']['tb_rendicioncuenta_detalle_moneda'] ;
    $egreso_numdoc = $cuenta['data']['tb_egreso_numdoc'];
    $estado = $cuenta['data']['tb_rendicioncuenta_estado'];
}
/*
    $rendicioncuentas_id = intval($_POST['rendicioncuentas_id']);
    $registros = '';
    $total = 0;
    /** LISTADO DE DETALLES **
    $result = $oRendicionCuentasDetalle->listar_rendicioncuenta_detalles($rendicioncuentas_id);
    if($result['estado']==1){
        foreach ($result['data'] as $key => $value) {
            $total += $value['tb_rendicioncuenta_detalle_importe'];
            $estado = '';
            if($value['tb_rendicioncuenta_detalle_estado']==1){
                $estado = '<span class="badge bg-orange">POR COBRAR</span>';
            }elseif($value['tb_rendicioncuenta_detalle_estado']==2){
                $estado = '<span class="badge bg-green">CANCELADO</span>';
            }

            $registros = '
            <tr id="tabla_fila" style="height: 20px">
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$value['tb_rendicioncuenta_detalle_id'].'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$value['tb_rendicioncuenta_detalle_importe'].'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$value['tb_rendicioncuenta_detalle_detalle'].'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>'.$estado.'</b></td>
                <td id="tabla_fila" align="center" style="height: 22px"><b>BOTON</b></td>
            </tr>';
        }
    }
*/
/** LISTADO DE DETALLES **/

//verificar si es una liquidación anticipada
$hoy = date('d--m-Y');
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_rendicioncuentas" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;font-size: 15px">RENDICION DE CUENTA</h4>
            </div>
                <div class="modal-body">
                
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label style="font-family: cambria;color: #006633;">FORMULARIO RENDICION CUENTA DETALLE</label>
                                    <?php if(intval($estado) == 1){?>
                                        <button type="button" class="btn btn-success btn-sm pull-right" onclick="agregar_detalle()">
                                            <i class="fa fa-plus"></i> AGREGAR
                                        </button>
                                    <?php } ?>
                                    <p>
                                    <p>
                                    <div id="rendicioncuenta_detalle" class="row m-3">
                                        <form id="for_rendicioncuentadetalle" method="post">
                                            <input type="hidden" id="hdd_usuariogrupo" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">
                                            <input name="hdd_rendicioncuentas_id" id="hdd_rendicioncuentas_id" type="hidden" value="<?php echo $_POST['rendicioncuentas_id'] ?>">
                                            <input name="hdd_moneda_egreso_id" id="hdd_moneda_egreso_id" type="hidden" value="<?php echo $moneda_egreso_id ?>">
                                            <input name="hdd_egreso_numdoc" id="hdd_egreso_numdoc" type="hidden" value="<?php echo $egreso_numdoc ?>">
                                            <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $_POST['vista'] ?>"> 
                                            <input type="hidden" name="action" id="action" value="insertar"> 
                                            <input type="hidden" name="hdd_filestorageid" id="hdd_filestorageid" value=""> 
                                            <input type="hidden" name="hdd_url" id="hdd_url" value=""> 
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Moneda</label>
                                                            <select name="cmb_mon_id" id="cmb_mon_id" class="form-control input-sm">
                                                                <?php include '../moneda/moneda_select.php';?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="txt_importe" class="control-label">IMPORTE</label>
                                                            <input type="text" name="txt_importe" id="txt_importe" class="form-control input-sm moneda" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_detalle" class="control-label">Comentario:</label>
                                                            <textarea name="txt_detalle" id="txt_detalle" rows="3" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row modal-footer">
                                                    <div class="f1-buttons">
                                                        <button type="submit" class="btn btn-info" id="btn_guardar_gasto">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <p>
                                    <p>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- RENDICION CUENTA DETALLE -->
                                            <div id="div_rendicioncuentasdetalle_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                                
                                            </div>
                                            <!-- RENDICION CUENTA DETALLE -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- RENDICION CUENTA DETALLE -->
                                            <div id="div_filestorage_url" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                            </div>
                                            <!-- RENDICION CUENTA DETALLE -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/rendicioncuentas/rendicioncuentas_form.js?ver=02'; ?>"></script>