function rendicioncuentas_form(usuario_act, rendicioncuentas_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"rendicioncuentas/rendicioncuentas_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      rendicioncuentas_id: rendicioncuentas_id,
      vista: "rendicioncuentas"
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_rendicioncuentas_form').html(data);
      	$('#modal_registro_rendicioncuentas').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_rendicioncuentas'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_rendicioncuentas', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'rendicioncuentas';
      	var div = 'div_modal_rendicioncuentas';
      	permiso_solicitud(usuario_act, rendicioncuentas_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function rendicioncuentas_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"rendicioncuentas/rendicioncuentas_tabla.php",
    async: true,
    dataType: "html",
    data: $('#form_rendicioncuenta_filtro').serialize(),
    beforeSend: function() {
      $('#rendicioncuentas_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_rendicioncuentas_tabla').html(data);
      $('#rendicioncuentas_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#rendicioncuentas_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function form_gasto(action, gasto_id, rendicion_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/gasto_retencion_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      gasto_id: gasto_id,
      rendicion_id: rendicion_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_gasto_form").html(html);
      $("#modal_gasto_form").modal("show");
      $("#modal_mensaje").modal("hide");

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal("modal_gasto_form", "limpiar"); //funcion encontrada en public/js/generales.js
      modal_width_auto("modal_gasto_form", 40);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_gasto_form"); //funcion encontrada en public/js/generales.js
    },
  });
}

function rendicioncuentas_delete(rendicioncuentas_id){
  Swal.fire({
    title: '¿DESEA ELIMINAR LA RENDICION DE CUENTA?',
    icon: 'warning',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> SI',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-trash"></i> NO',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#d6181b',
  }).then((result) => {
    if (result.isConfirmed) {
      eliminar_rendicioncuenta(rendicioncuentas_id);
    } else if (result.isDenied) {
      return false
    }
  });
}

function limpiar_filtro(){
  $("#txt_filtro_fec1").val($("#fecha_hoy").val())
  $("#txt_filtro_fec2").val($("#fecha_hoy").val())
  $('#cmb_proveedor_id').val(null).trigger('change');
  $('#cmb_estado_id').val(null).trigger('change');
}

function mensaje_gasto(){
  alerta_warning("YA HAY UN GASTO REGISTRADO")
}

function eliminar_rendicioncuenta(rendicioncuentas_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "rendicioncuentas/rendicioncuentas_controller.php",
    async: true,
    dataType: "JSON",
    data: {
      action: "eliminar",
      rendicioncuentasid: rendicioncuentas_id
    },
    beforeSend: function () {},
    success: function (data) {
      console.log(data)
      if (parseInt(data.estado) == 1) {
        alerta_success("EXITO", data.mensaje)
        rendicioncuentas_tabla()
      }else{
        alerta_error("ERROR", data.mensaje)
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("ERROR", data.responseText);
    },
  });
}

$(document).ready(function() {
  console.log('rendicioncuentas');
  //rendicioncuentas_tabla()
  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
});