<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class RendicionCuentas extends Conexion{

    public $egreso_id;
    public $gasto_id;
    public $gasto_opcion;
    public $rendicioncuenta_estado;
    public $rendicioncuenta_fec;
    public $rendicioncuenta_usureg;
    public $rendicioncuenta_usumod;
    public $rendicioncuenta_xac;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_rendicioncuenta(
                    tb_egreso_id, 
                    tb_rendicioncuenta_estado,
                    tb_rendicioncuenta_fec,
                    tb_rendicioncuenta_usureg,
                    tb_rendicioncuenta_xac)
          VALUES (
                    :egreso_id, 
                    :rendicioncuenta_estado,
                    :rendicioncuenta_fec,
                    :rendicioncuenta_usureg,
                    :rendicioncuenta_xac)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_id", $this->egreso_id, PDO::PARAM_INT);
        $sentencia->bindParam(":rendicioncuenta_estado", $this->rendicioncuenta_estado, PDO::PARAM_INT);
        $sentencia->bindParam(":rendicioncuenta_fec", $this->rendicioncuenta_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":rendicioncuenta_usureg", $this->rendicioncuenta_usureg, PDO::PARAM_INT);
        $sentencia->bindParam(":rendicioncuenta_xac", $this->rendicioncuenta_xac, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function eliminar($rendicioncuenta_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_rendicioncuenta SET tb_gasto_opcion = 0 WHERE tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUno($rendicioncuenta_id){
      try {
        $sql = "SELECT rc.*, e.tb_egreso_numdoc, e.tb_egreso_imp, c.* 
                FROM tb_rendicioncuenta rc
                INNER JOIN tb_egreso e ON rc.tb_egreso_id = e.tb_egreso_id
                LEFT JOIN tb_cliente c ON e.tb_cliente_id = c.tb_cliente_id
                WHERE tb_rendicioncuenta_xac = 1 AND tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No existe esa rendicion";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }


    function listar_rendicioncuentas($rendicion_fec1, $rendicion_fec2, $proveedor_id, $estado_id){
      try {
        $proveedor = '';
        $estado = '';
        if (intval($proveedor_id) > 0)
        $proveedor = " AND e.tb_proveedor_id = $proveedor_id";
        if (intval($estado_id) > 0)
        $estado = " AND rc.tb_rendicioncuenta_estado = $estado_id";


        $sql = "SELECT rc.*, p.tb_proveedor_doc, p.tb_proveedor_nom, e.tb_egreso_numdoc, e.tb_egreso_imp 
          FROM tb_rendicioncuenta rc
          INNER JOIN tb_egreso e ON rc.tb_egreso_id = e.tb_egreso_id
          LEFT JOIN tb_proveedor p ON e.tb_proveedor_id = p.tb_proveedor_id
          WHERE tb_rendicioncuenta_xac = 1 AND (rc.tb_rendicioncuenta_fec BETWEEN :rendicion_fec1 AND :rendicion_fec2)". $proveedor . $estado;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicion_fec1", $rendicion_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":rendicion_fec2", $rendicion_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay rendiciones";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }

    function agregar_gasto($rendicioncuenta_id, $gasto_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_rendicioncuenta 
                SET 
                    tb_gasto_id=:gasto_id, 
                    tb_gasto_opcion=1 
                WHERE tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gasto_id", $gasto_id, PDO::PARAM_INT);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function modificar_campo_nuevo($rendicioncuenta_id, $usumod, $campo, $rendicioncuenta_valor)
    {
      $this->dblink->beginTransaction();
      try {


        $sql = "UPDATE tb_rendicioncuenta SET
                                          tb_rendicioncuenta_fecmod = NOW( ),
                                          tb_rendicioncuenta_usumod =:tb_rendicioncuenta_usumod,
                                          tb_rendicioncuenta_$campo = :campo 
                                  WHERE 
                                          tb_rendicioncuenta_id =:rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_rendicioncuenta_usumod", $usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":campo", $rendicioncuenta_valor, PDO::PARAM_STR);
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el rendicioncuenta retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function listar_gv_rendicioncuenta($rendicioncuenta_id){
      try {
        $sql = "SELECT g.*
        FROM tb_rendicioncuenta rc
        INNER JOIN tb_gasto g ON rc.tb_gasto_id = g.tb_gasto_id
        WHERE tb_rendicioncuenta_xac = 1 AND rc.tb_rendicioncuenta_id = :rendicioncuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":rendicioncuenta_id", $rendicioncuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay rendiciones";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarxEgreso($egreso_id){
      try {
        $sql = "SELECT tb_rendicioncuenta_id
                FROM tb_rendicioncuenta rc
                INNER JOIN tb_egreso e ON rc.tb_egreso_id = e.tb_egreso_id
                WHERE tb_rendicioncuenta_xac = 1 AND e.tb_egreso_id =:egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":egreso_id", $egreso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No existe esa rendicion";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
