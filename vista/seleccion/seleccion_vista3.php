<?php

	require_once('core/core.php');

?>

<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title><?php echo NOMBRE_SISTEMA?></title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.6 -->

  <!--<link rel="stylesheet" href="static/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">-->

  <!-- Font Awesome -->

  <link href="static/css/waves.css" rel="stylesheet">

	<!-- Ionicons -->

	<link rel="stylesheet" href="static/css/font-awesome/css/font-awesome.min.css">

	<link rel="stylesheet" href="static/css/ionicons.min.css">

	<!-- Theme style -->

        <link rel="stylesheet" href="static/css/AdminLTE.css">

	<link href="static/css/waves.css" rel="stylesheet">

	<link rel="stylesheet" href="public/css/generales.css">

	<link rel="shortcut icon" type="image/x-icon" href="public/images/logopres.ico" />

</head>

<style>

	.fondo {

		background: url('public/images/fondo1.jpg') no-repeat center center fixed;

		-webkit-background-size: cover;

		-moz-background-size: cover;

		-o-background-size: cover;

		background-size: cover;

	}

</style>



<body class="fondo login-page">

<div class="login-box" >



    <div class="card card-outline card-primary">

	      <div class="card-header text-center">

	          <a href="#" class="h1"><b>SELECCIONAR UNA SEDE</b></a>

	      </div>

	      <div class="card-body">

	          

	          <form name="form">

	             <div class="form-group has-feedback" id="seleccion">

				    <?php require_once("seleccion_lista.php") ?>

		        </div>

	          </form>

	      </div>

	  </div>

  </div>

  

<script type="text/javascript">

	VISTA_URL = 'vista/';

	function redirigir(codigo,nombre){

		  $.ajax({

	          method: "POST",

	          url: VISTA_URL+'seleccion/seleccion_asignar.php',

	          data: {

	              "id": codigo,
	              "sucursal_nombre": nombre  

	            }

	      })

	      .done(function( html ) {

	            //window.location="./principal"; 

	            $(location).attr('href','principal'); 

	      }); 



}

</script>

<!--MODAL PARA LOS MENSAJES -->

<?php include(VISTA_URL.'templates/modal_mensaje.php');?>



<!-- jQuery 2.2.3 -->

<script src="static/js/jquery/jquery-3.2.1.min.js"></script>

<script src="static/js/jquery/jquery.validate.min.js"></script>

<!-- Bootstrap 3.3.6 -->

<script src="static/js/bootstrap/bootstrap.min.js"></script>

<!-- SCRIPT PROPIOS-->

<!--<script src="public/js/generales.js"></script>-->

<script type="text/javascript" src="static/js/waves.js"></script>

<script src="static/js/admin.js"></script>

</body>

</html>

