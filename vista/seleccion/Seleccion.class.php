<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Seleccion extends Conexion {

    
    function consultarTiendasUsuario(){
      try {
        $sql = "SELECT * from tb_empresa e inner join tb_usuarioempresa ue on e.tb_empresa_id=ue.tb_empresa_id WHERE ue.tb_usuario_id = :usuario_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $_SESSION['usuario_id'], PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay empresas registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }

    
  }
?>
