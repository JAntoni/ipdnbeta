<?php
	require_once('core/core.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEMA PRESTAMOS DEL NORTE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="static/css/bootstrap/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="static/css/waves.css" rel="stylesheet">
	<!-- Ionicons -->
	<link rel="stylesheet" href="static/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="static/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="static/css/AdminLTE.min.css">
	<link rel="stylesheet" href="static/css/AdminLTE.custom.css">
	<link href="static/css/waves.css" rel="stylesheet">
	<link rel="stylesheet" href="public/css/generales.css">
	<link rel="shortcut icon" type="image/x-icon" href="public/images/logopres.ico" />
</head>
<style>
	.fondo {
		background: url('public/images/fondo1.jpg') no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
</style>

<body class="fondo">
<div class="login-box" >

	  <div class="" id="" style="height: 50px">
	  	<div class="box box-info box-solid" id="box_modal_mensaje" style="display: none;margin-bottom: 0px">
			  <div class="box-header" style="padding: 2px">
			    <div class="box-body" id="body_modal_mensaje">
			    			<i class="icon fa fa-info"></i>&nbsp; Por favor espere unos segundos...
			  	</div>
		  	</div>
	  	</div>
	  </div>

	<div class="register-logo" style="margin-bottom: 0px;">
	   <p style="color: black; -webkit-text-stroke: 1px white;"><b>SELECCIONAR SEDE</b></p>
	 </div>
  <!-- /.login -->
  <div class="login-box-body" style="background: #fff; min-height: 50px;box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);position: relative;margin-bottom: 30px;">

    <!-- /.form -->
    <form method="post" name="form" id="form_login" >
		<!--p class="login-box-msg" style="font-weight: bolder;">PLATAFORMA DE VENTAS <br> USUARIO: <?php //echo $_SESSION['usuario_nom']?></p-->

    <h4 class="login-box-msg"><b>SEDES</b></h4>
    
    <?php require_once("seleccion_lista.php") ?>

<!--		  <div class="form-group has-feedback">
				<input type="text"  name="txt_usuario_usu" id="txt_usuario_usu" class="form-control" placeholder="Nombre de usuario">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
		  </div>
		  <div class="form-group has-feedback">
				<input type="password" name="txt_usuario_pass" id="txt_usuario_pass" class="form-control" placeholder="Contraseña">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		  </div>
		  <div class="row">-->

			 <!-- /.col -->
      </div>
    </form>

 </div>
     <div style="    color: #f4f4f4; font-family: verdana;font-size: 7pt;margin: 10px 0 0;    text-align: center;">
     Chiclayo. <br>Copyright ©   2019 <a href="https://www.fibranet.com" target="_blank
     " style="color: white;">Fribranet</a></div><br>



  </div>
  <!-- /.login-box-body -->
<!-- /.login-box -->

<!--MODAL PARA LOS MENSAJES -->
<?php include(VISTA_URL.'templates/modal_mensaje.php');?>

<!-- jQuery 2.2.3 -->
<script src="static/js/jquery/jquery-3.2.1.min.js"></script>
<script src="static/js/jquery/jquery.validate.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="static/js/bootstrap/bootstrap.min.js"></script>
<!-- SCRIPT PROPIOS-->
<!--<script src="public/js/generales.js"></script>-->
<script src="<?php echo VISTA_URL.'login/login.js?ver=13131313131';?>"></script>
<script type="text/javascript" src="static/js/waves.js"></script>
<script src="static/js/admin.js"></script>
<script type="text/javascript">

	VISTA_URL = 'vista/';

	function redirigir(codigo,nombre){

		  $.ajax({

	          method: "POST",

	          url: VISTA_URL+'seleccion/seleccion_asignar.php',

	          data: {

	              "id": codigo,
	              "sucursal_nombre": nombre  

	            }

	      })

	      .done(function( html ) {

	            //window.location="./principal"; 

	            $(location).attr('href','principal'); 

	      }); 



}

</script>
</body>
</html>
