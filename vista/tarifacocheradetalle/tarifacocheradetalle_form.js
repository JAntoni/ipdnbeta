function tarifa_mostrar_ocultar() {
  var tarifa_num = $('select[name="cmb_tarifacocheradetalle_turno"]').val(); // Obtener el valor seleccionado del combo box
  
  if (parseInt(tarifa_num) == 1) {
    $('.tarifaHora1').show(); 
    $('.tarifaHora2').hide(); 
  } else  {
    $('.tarifaHora1').show(); 
    $('.tarifaHora2').show(); 
  } 
}

function registrar_tarifacocheradetalle(){
  var tarifacocheradetalle_tam = $('#txt_tarifacocheradetalle_tam option:selected').val();
  var tarifacocheradetalle_turno = $('#cmb_tarifacocheradetalle_turno option:selected').val();
  var tarifacocheradetalle_precio = $('#txt_tarifacocheradetalle_precio').val().trim();
  var tarifacocheradetalle_horarioinicio = $('#txt_tarifacocheradetalle_horarioinicio1').val().trim();
  var tarifacocheradetalle_horariofin = $('#txt_tarifacocheradetalle_horafintarifa1').val().trim();
  var tarifacocheradetalle_horarioinicio2 = $('#txt_tarifacocheradetalle_horarioinicio2').val().trim();
  var tarifacocheradetalle_horariofin2 = $('#txt_tarifacocheradetalle_horafintarifa2').val().trim();
  var hdd_tarifacochera_id= $('#hdd_tarifacochera_id').val();

  

  

  
  if(!tarifacocheradetalle_tam || !tarifacocheradetalle_turno || !tarifacocheradetalle_precio || !tarifacocheradetalle_horarioinicio || !tarifacocheradetalle_horariofin){
    alerta_warning('AVISO', 'Debes llenar todos los campos');
    return false;
  }

  $.ajax({
		type: "POST",
		url: VISTA_URL+"tarifacocheradetalle/tarifacocheradetalle_controller.php",
    async: true,
		dataType: "json",
		data: ({
    action: 'insertar',
    tarifacocheradetalle_tam: tarifacocheradetalle_tam,
    tarifacocheradetalle_turno: tarifacocheradetalle_turno,
    tarifacocheradetalle_precio: tarifacocheradetalle_precio,
    tarifacocheradetalle_horarioinicio:tarifacocheradetalle_horarioinicio,
    tarifacocheradetalle_horariofin:tarifacocheradetalle_horariofin, 
    tarifacocheradetalle_horarioinicio2:tarifacocheradetalle_horarioinicio2, 
    tarifacocheradetalle_horariofin2:tarifacocheradetalle_horariofin2, 
    hdd_tarifacochera_id: hdd_tarifacochera_id,
   
}),


		beforeSend: function() {
      
		},
		success: function(data){
      if(parseInt(data.estado) == 1){
        notificacion_success('Detalle de Tarifa registrado')
        listar_tarifacocheradetalle();

        $('#txt_tarifacocheradetalle_tam').val('');
        $('#txt_tarifacocheradetalle_turno').val('');
        $('#txt_tarifacocheradetalle_precio').val('');
        $('#txt_tarifacocheradetalle_horarioinicio1').val('');
        $('#txt_tarifacocheradetalle_horafintarifa1').val('');
        $('#txt_tarifacocheradetalle_horarioinicio2').val('');
        $('#txt_tarifacocheradetalle_horafintarifa2').val('');
        $('#hdd_cochera_id').val('');
      }
      else{
        console.log(data)
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function listar_tarifacocheradetalle(){
  var hdd_tarifacochera_id= $('#hdd_tarifacochera_id').val();
  
  $.ajax({
		type: "POST",
		url: VISTA_URL+"tarifacocheradetalle/tarifacocheradetalle_controller.php",
    async: true,
		dataType: "html",
		data: ({
      action: 'tabla',
      hdd_tarifacochera_id: hdd_tarifacochera_id,
    }),
		beforeSend: function() {
		},
		success: function(data){
      $('#table_tarifacocheradetalle').html(data)
		},
		complete: function(data){
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}


function opcionesgc_contacto(){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"reportecreditomejorado/opcionesgc_contacto.php",
    async: true,
		dataType: "html",
		data: ({
      cliente_id: $('#hdd_cliente_id').val()
    }),
		beforeSend: function() {
      
		},
		success: function(data){
      if(data != 'sin_datos'){
        console.log('entra suuu')
      	$('#div_modal_cliente_contacto').html(data);
      	$('#modal_cliente_contacto').modal('show');
      	modal_hidden_bs_modal('modal_cliente_contacto', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function actualizar_numeros(){
  var celular = $('#txt_numero_cel').val();
  var telefono = $('#txt_numero_tel').val();
  var referencial = $('#txt_numero_telref').val();
  var cliente_id = $('#hdd_cliente_id').val();

  if(!celular || !telefono || !referencial || !cliente_id){
    alerta_error('IMPORTANTE', 'No puedes ingresar valores vacíos / ' + cliente_id);
    return false;
  }

  $.confirm({
    icon: 'fa fa-pencil',
    title: 'Actualizar',
    content: '¿Está seguro de actualizar los números del cliente?',
    type: 'blue',
    theme: 'material', // 'material', 'bootstrap'
    typeAnimated: true,
    buttons: {
      si: function () {
        $.ajax({
          type: "POST",
          url: VISTA_URL + "reportecreditomejorado/opcionesgc_asignar_controller.php",
          async: true,
          dataType: "json",
          data: ({
            cliente_id: cliente_id,
            cliente_cel: celular,
            cliente_tel: telefono,
            cliente_telref: referencial,
            action: 'actualizar_numeros'
          }),
          beforeSend: function () {
          },
          success: function (data) {
            swal_success("SISTEMA", data.mensaje, 2000);
          },
          complete: function (data) {
            if (data.status != 200)
              console.log(data);
          }
        });
      },
      no: function () {}
    }
  });
}

$(document).ready(function(){
  $('#datetimepicker3').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    startDate: "-0d"
    //endDate : new Date()
  });

  $('select[name="cmb_tarifacocheradetalle_turno"]').on('change', function() {
    tarifa_mostrar_ocultar(); // Llamar a la función cuando cambie el valor del combo
  });

  listar_tarifacocheradetalle();

  $('#cmb_opcionesgc_id').change(function(event){
    var opcionesgc_id = parseInt($(this).val());
    var opcionesgc_nom = $('#cmb_opcionesgc_id :selected').text();
    $('#hdd_opcionesgc_nom').val(opcionesgc_nom);

    if(opcionesgc_id == 1 || opcionesgc_id == 4)
      $('.fecha_pdp').show();
    else
      $('.fecha_pdp').hide();
  });

  directorio = $('#directorio').val(); //opcionesgc, asignaropciongc
  if(directorio == "opcionesgc")
    listar_opcionesgc();

  console.log('corre ya 444: ' + directorio)

  $('#form_opcionesgc_asignar').validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "reportecreditomejorado/opcionesgc_asignar_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_opcionesgc_asignar").serialize(),
        beforeSend: function () {
          
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {
            swal_success('Genial!', data.mensaje, 3000)
            $('#modal_registro_asignaropciongc').modal('hide');

            //retorna los valores de gestión ingresados, para asiganarlo a los td
            $.each(data.cuotadetalle_id, function(index, value) {
              $('#td_opciones_' + value).text(data.td_opciones);
              $('#td_comentario_' + value).text(data.td_comentario);
            });

            console.log(data)
          } 
          else {
            alerta_warning('AVISO', 'Por favor tener en cuenta: ' + data.mensaje)
          }
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      cmb_opcionesgc_id: {
        required: true,
        min: 1
      }
    },
    messages: {
      cmb_opcionesgc_id: {
        required: "Selecciona una opción de gestión para guardar",
        min: "Selecciona una opción de gestión para guardar"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });
  
  // Aquí llamamos a la función para mostrar/ocultar las tarifas cuando se carga el documento
  tarifa_mostrar_ocultar();
});
