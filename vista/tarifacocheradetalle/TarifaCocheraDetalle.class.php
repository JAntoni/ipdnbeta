<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class TarifaCocheraDetalle extends Conexion{

    public $tarifacocheradetalle_tam; 
    public $tarifacocheradetalle_turno; 
    public $tarifacocheradetalle_precio; 
    public $tarifacocheradetalle_horarioinicio;
    public $tarifacocheradetalle_horariofin;
    public $tarifacochera_id;
    public $tarifacocheradetalle_horarioinicio2;
    public $tarifacocheradetalle_horariofin2;
    

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tarifacocheradetalle(
                tb_tarifacocheradetalle_tamano, 
                tb_tarifacocheradetalle_turno,
                tb_tarifacocheradetalle_horarioinicio,
                tb_tarifacocheradetalle_horariofin, 
                tb_tarifacocheradetalle_precio,
                tb_tarifacocheradetalle_horarioinicio2,
                tb_tarifacocheradetalle_horariofin2,
                tb_tarifacochera_id)
                VALUES (
                :tarifacocheradetalle_tam, 
                :tarifacocheradetalle_turno,
                :tarifacocheradetalle_horarioinicio, 
                :tarifacocheradetalle_horariofin,
                :tarifacocheradetalle_precio,
                :tarifacocheradetalle_horarioinicio2,
                :tarifacocheradetalle_horariofin2, 
                :tarifacochera_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifacocheradetalle_tam", $this->tarifacocheradetalle_tam, PDO::PARAM_INT);
        $sentencia->bindParam(":tarifacocheradetalle_turno", $this->tarifacocheradetalle_turno, PDO::PARAM_INT);


        $sentencia->bindParam(":tarifacocheradetalle_horarioinicio", $this->tarifacocheradetalle_horarioinicio, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifacocheradetalle_horariofin", $this->tarifacocheradetalle_horariofin, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifacocheradetalle_horarioinicio2", $this->tarifacocheradetalle_horarioinicio2, PDO::PARAM_STR);
        $sentencia->bindParam(":tarifacocheradetalle_horariofin2", $this->tarifacocheradetalle_horariofin2, PDO::PARAM_STR);

        $sentencia->bindParam(":tarifacocheradetalle_precio", $this->tarifacocheradetalle_precio, PDO::PARAM_INT);
        $sentencia->bindParam(":tarifacochera_id", $this->tarifacochera_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }

    /*function modificar($opcionesgc_id, $opcionesgc_nom, $opcionesgc_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_opcionesgc SET tb_opcionesgc_nom =:opcionesgc_nom, tb_opcionesgc_des =:opcionesgc_des WHERE tb_opcionesgc_id =:opcionesgc_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_id", $opcionesgc_id, PDO::PARAM_INT);
        $sentencia->bindParam(":opcionesgc_nom", $opcionesgc_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":opcionesgc_des", $opcionesgc_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }*/

    /*function mostrarUno($opcionesgc_id){
      try {
        $sql = "SELECT * FROM tb_opcionesgc WHERE tb_opcionesgc_id =:opcionesgc_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":opcionesgc_id", $opcionesgc_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }*/
    
    function listar_tarifacocheradetalle(){
      try {
        $sql = "SELECT 
                  tcd.*, 
                CASE
                  WHEN tcd.tb_tarifacocheradetalle_tamano= 1 THEN 'Pequeño'
                  WHEN tcd.tb_tarifacocheradetalle_tamano= 2 THEN 'Mediano'
                  WHEN tcd.tb_tarifacocheradetalle_tamano= 3 THEN 'Grande'
                END AS tamano
                FROM 
                  tb_tarifacocheradetalle tcd
                WHERE 
                  tcd.tb_tarifacocheradetalle_xac = 1 AND  tcd.tb_tarifacochera_id = :tarifacochera_id" ;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tarifacochera_id", $this->tarifacochera_id, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay detalle de tarifas registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>