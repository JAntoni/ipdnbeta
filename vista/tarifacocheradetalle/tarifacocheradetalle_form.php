<?php
require_once('../../core/usuario_sesion.php');
require_once('../tarifacochera/TarifaCochera.class.php');
$oTarifaCochera = new TarifaCochera();
require_once('../funciones/funciones.php');

$direc = 'tarifacochera';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$tarifacocheradetalle_id = $_POST['tarifacocheradetalle_id'];
$tarifacochera_id = $_POST['tarifacochera_id'];

$titulo = '';
if ($usuario_action == 'L')
  $titulo = 'Detalle de Tarifa Registrado';
elseif ($usuario_action == 'I')
  $titulo = 'Registrar Detalle de Tarifa ';
elseif ($usuario_action == 'M')
  $titulo = 'Editar Detalle de Tarifa ';
elseif ($usuario_action == 'E')
  $titulo = 'Eliminar Detalle de Tarifa ';
else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_tarifacocheradetalle" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo $titulo; ?></h4>
      </div>

      <div class="modal-body">
        <input type="hidden" id="directorio" value="tarifacocheradetalle">

        <label for="txt_filtro" class="control-label">Registro de Detalles</label>
        <div class="panel panel-primary shadow-simple">
          <div class="panel-body">
            <form id="form_tarifacocheradetalle" class="form-inline" role="form">
              <input type="hidden" name="hdd_tarifacochera_id" id='hdd_tarifacochera_id' value="<?php echo $tarifacochera_id; ?>">
              <div class="panel-body">
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="txt_tarifacocheradetalle_tam" class="control-label" style="margin-bottom: 5px">Tamaño de Vehículo</label>
                    <select id="txt_tarifacocheradetalle_tam" name="txt_tarifacocheradetalle_tam" class="form-control input-sm" style="display: block; width: 100%;">
                      <option>Seleccione...</option>
                      <option value="1" <?php if ($tarifacocheradetalle_tam == 1) echo 'selected'; ?>>Pequeño</option>
                      <option value="2" <?php if ($tarifacocheradetalle_tam == 2) echo 'selected'; ?>>Mediano</option>
                      <option value="3" <?php if ($tarifacocheradetalle_tam == 3) echo 'selected'; ?>>Grande</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="txt_tarifacocheradetalle_precio" class="control-label" style="margin-bottom: 5px">Precio de Tarifa</label>
                    <input type='number' class="form-control input-sm" name="txt_tarifacocheradetalle_precio" id="txt_tarifacocheradetalle_precio" style="width: 100%;" autocomplete="off" placeholder="Precio" />
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="form-gro
                  up col-md-6">
                    <label for="cmb_tarifacocheradetalle_turno" class="control-label" style="margin-bottom: 5px">Número de Turnos</label>
                    <select id="cmb_tarifacocheradetalle_turno" name="cmb_tarifacocheradetalle_turno" class="form-control input-sm" style="display: block; width: 100%;">
                      <option value="1" <?php if ($tarifacocheradetalle_turno == 1) echo 'selected'; ?>>1</option>
                      <option value="2" <?php if ($tarifacocheradetalle_turno == 2) echo 'selected'; ?>>2</option>
                    </select>
                  </div>

                </div>
                <br>
                <div class="row">
                  <div class="form-group tarifaHora1 col-md-12">
                    <div class="form-group col-md-6" style="margin-bottom: 12px">
                      <label for="txt_tarifacocheradetalle_horarioinicio1" class="control-label">Hora Inicio Tarifa 1: </label>
                      <input type="time" class="form-control input-sm" name="txt_tarifacocheradetalle_horarioinicio1" id="txt_tarifacocheradetalle_horarioinicio1" style="width: 100%;" autocomplete="off" value="<?php echo $horaIniciotarifa1; ?>">
                    </div>
                    <div class="form-group col-md-6" style="margin-bottom: 12px">
                      <label for="txt_tarifacocheradetalle_horafintarifa1" class="control-label">Hora Fin Tarifa 1: </label>
                      <input type="time" class="form-control input-sm" name="txt_tarifacocheradetalle_horafintarifa1" id="txt_tarifacocheradetalle_horafintarifa1" style="width: 100%;" autocomplete="off" value="<?php echo $horaSalidatarifa1; ?>">
                    </div>
                  </div>

                  <div class="form-group tarifaHora2 col-md-12">
                    <div class="form-group col-md-6" style="margin-bottom: 12px">
                      <label for="txt_tarifacocheradetalle_horarioinicio2" class="control-label">Hora Inicio Tarifa 2: </label>
                      <input type="time" class="form-control input-sm" name="txt_tarifacocheradetalle_horarioinicio2" id="txt_tarifacocheradetalle_horarioinicio2" style="width: 100%;" autocomplete="off" value="<?php echo $horaIniciotarifa2; ?>">
                    </div>
                    <div class="form-group col-md-6" style="margin-bottom: 12px">
                      <label for="txt_tarifacocheradetalle_horafintarifa2" class="control-label">Hora Fin Tarifa 2: </label>
                      <input type="time" class="form-control input-sm" name="txt_tarifacocheradetalle_horafintarifa2" id="txt_tarifacocheradetalle_horafintarifa2" style="width: 100%;" autocomplete="off" value="<?php echo $horaSalidatarifa2; ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <button type="button" class="btn btn-primary btn-sm" onclick="registrar_tarifacocheradetalle()"><i class="fa fa-save"></i></button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <p></p>
        <hr>
        <label for="txt_filtro" class="control-label">Lista de Detalles de la Tarifa</label>
        <div class="panel panel-primary shadow-simple">
          <div class="panel-body">
            <table class="table table-bordered">
              <tbody id="table_tarifacocheradetalle"></tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/tarifacocheradetalle/tarifacocheradetalle_form.js?ver=2'; ?>"></script>