<?php
require_once('../../core/usuario_sesion.php');
require_once('../tarifacocheradetalle/TarifaCocheraDetalle.class.php');
$oTarifaCocheraDetalle = new TarifaCocheraDetalle();

require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';

$action = $_POST['action'];

$oTarifaCocheraDetalle->tarifacocheradetalle_tam = intval($_POST['tarifacocheradetalle_tam']);
$oTarifaCocheraDetalle->tarifacocheradetalle_turno = intval($_POST['tarifacocheradetalle_turno']);
$oTarifaCocheraDetalle->tarifacocheradetalle_horarioinicio = string_hora($_POST['tarifacocheradetalle_horarioinicio']);
$oTarifaCocheraDetalle->tarifacocheradetalle_horariofin = string_hora($_POST['tarifacocheradetalle_horariofin']);
$oTarifaCocheraDetalle->tarifacocheradetalle_horarioinicio2 = string_hora($_POST['tarifacocheradetalle_horarioinicio2']);
$oTarifaCocheraDetalle->tarifacocheradetalle_horariofin2 = string_hora($_POST['tarifacocheradetalle_horariofin2']);
$oTarifaCocheraDetalle->tarifacocheradetalle_precio = $_POST['tarifacocheradetalle_precio'];
$oTarifaCocheraDetalle->tarifacochera_id = intval($_POST['hdd_tarifacochera_id']);

if ($action == 'insertar') {

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al guardar Tarifa Detalle.';

  if ($oTarifaCocheraDetalle->insertar()) {
    $data['estado'] = 1;
    $data['mensaje'] = 'Detalle de Tarifa registrado correctamente.';
  }

  echo json_encode($data);
}
if ($action == 'tabla') {
  $td_tarifacocheradetalle = '';
  $result = $oTarifaCocheraDetalle->listar_tarifacocheradetalle();
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $td_tarifacocheradetalle .= '
            <tr id="tabla_cabecera_fila">
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_id'] . '</td>
              <td id="tabla_fila">' . $value['tamano'] . '</td>
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_precio'] . '</td>
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_horarioinicio'] . '</td>
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_horariofin'] . '</td>
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_horarioinicio2'] . '</td>
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_horariofin2'] . '</td>
              <td id="tabla_fila">' . $value['tb_tarifacocheradetalle_turno'] . '</td>
            </tr>';
    }
  }
  $result = NULL;

  $tabla = '
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">TAMAÑO DEL VEHICULO</th>
        <th id="tabla_cabecera_fila">PRECIO</th>
        <th id="tabla_cabecera_fila">HORA INICIO TURNO 1</th>
        <th id="tabla_cabecera_fila">HORA FIN TURNO 1</th>
        <th id="tabla_cabecera_fila">HORA INICIO TURNO 2</th>
        <th id="tabla_cabecera_fila">HORA FIN TURNO 2</th>
        <th id="tabla_cabecera_fila">NUMERO DE TURNOS</th>
      </tr>' . $td_tarifacocheradetalle;

  echo $tabla;
}
