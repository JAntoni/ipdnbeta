<?php
  if(defined('APP_URL')){
		require_once(APP_URL.'core/usuario_sesion.php');
                require_once(VISTA_URL.'funciones/funciones.php');
                require_once(VISTA_URL.'funciones/fechas.php');
	}
	else{
		require_once('../../core/usuario_sesion.php');
                require_once('../funciones/funciones.php');
                require_once('../funciones/fechas.php');
	}
  
  
  
  require_once('Moneda.class.php');
  $oMoneda = new Moneda();

  $result = $oMoneda->listar_monedas();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_moneda_id'].'</td>
          <td>'.$value['tb_moneda_nom'].'</td>
          <td>'.$value['tb_moneda_des'].'</td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="moneda_form(\'M\','.$value['tb_moneda_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="moneda_form(\'E\','.$value['tb_moneda_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_monedas" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
