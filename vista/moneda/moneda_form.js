
$(document).ready(function(){
  $('#form_moneda').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"moneda/moneda_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_moneda").serialize(),
				beforeSend: function() {
					$('#moneda_mensaje').show(400);
					$('#btn_guardar_moneda').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#moneda_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#moneda_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		moneda_tabla();
		      		$('#modal_registro_moneda').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#moneda_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#moneda_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_moneda').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#moneda_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#moneda_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_moneda_nom: {
				required: true,
				minlength: 2
			},
			txt_moneda_des: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			txt_moneda_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_moneda_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 2 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
