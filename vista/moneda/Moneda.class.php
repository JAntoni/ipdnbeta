<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Moneda extends Conexion{

    function insertar($moneda_nom, $moneda_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_moneda(tb_moneda_xac, tb_moneda_nom, tb_moneda_des)
          VALUES (1, :moneda_nom, :moneda_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":moneda_nom", $moneda_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_des", $moneda_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($moneda_id, $moneda_nom, $moneda_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_moneda SET tb_moneda_nom =:moneda_nom, tb_moneda_des =:moneda_des WHERE tb_moneda_id =:moneda_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_nom", $moneda_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_des", $moneda_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($moneda_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_moneda WHERE tb_moneda_id =:moneda_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($moneda_id){
      try {
        $sql = "SELECT * FROM tb_moneda WHERE tb_moneda_id =:moneda_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_monedas(){
      try {
        $sql = "SELECT * FROM tb_moneda";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
