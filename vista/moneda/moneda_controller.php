<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../moneda/Moneda.class.php');
  $oMoneda = new Moneda();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$moneda_nom = mb_strtoupper($_POST['txt_moneda_nom'], 'UTF-8');
 		$moneda_des = $_POST['txt_moneda_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Moneda.';
 		if($oMoneda->insertar($moneda_nom, $moneda_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Moneda registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$moneda_id = intval($_POST['hdd_moneda_id']);
 		$moneda_nom = mb_strtoupper($_POST['txt_moneda_nom'], 'UTF-8');
 		$moneda_des = $_POST['txt_moneda_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el moneda.';

 		if($oMoneda->modificar($moneda_id, $moneda_nom, $moneda_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Moneda modificado correctamente. ';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$moneda_id = intval($_POST['hdd_moneda_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el moneda.';

 		if($oMoneda->eliminar($moneda_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Moneda eliminado correctamente. '.$moneda_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>