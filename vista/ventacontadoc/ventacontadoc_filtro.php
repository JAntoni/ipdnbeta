<form id="form_creditomenor_filtro" class="form-inline" role="form">
    <div class="form-group">
        <label for="txt_fil_cliente_nom">Cliente: </label>
        <input type="text" name="txt_fil_cliente_nom" id="txt_fil_cliente_nom" placeholder="Escribe para buscar cliente" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="40" value>
        <input type="hidden" name="hdd_fil_cliente_id" id="hdd_fil_cliente_id">
    </div>
    &nbsp;&nbsp;
    <div class="form-group">
        <label for="txt_fil_doc_cod">Tipo Doc.: </label>
        <input type="text" name="txt_fil_doc_cod" id="txt_fil_doc_cod" class="form-control input-sm ui-autocomplete-input" placeholder="Tipo de documento" autocomplete="off" size="37">
        <input type="hidden" name="hdd_fil_doc_cod" id="hdd_fil_doc_cod">
    </div>
    &nbsp;&nbsp;
    <div class="form-group">
        <label for="cmb_periodo_id">Periodo Contable: </label>
        <select name="cmb_periodo_id" id="cmb_periodo_id" class="form-control input-sm">
            <?php include VISTA_URL . 'mesconta/mesconta_select.php' ?>
        </select>
    </div>
    &nbsp;&nbsp;
    <div class="form-group">
        <label for="cmb_doc_activos">Ver: </label>
        <select name="cmb_doc_activos" id="cmb_doc_activos" class="form-control input-sm">
            <option value="0">Todos</option>
            <option value="1" style="font-weight: bold;">Activos</option>
            <option value="2" style="font-weight: bold;">Eliminados</option>
        </select>
    </div>
    &nbsp;&nbsp;
    <div class="form-group">
        <label for="cmb_verificar_docs">Docs: </label>
        <select name="cmb_verificar_docs" id="cmb_verificar_docs" class="form-control input-sm">
            <option value="0" selected>Todos</option>
            <option value="1" style="font-weight: bold;">Falta numeracion Sunat</option>
        </select>
    </div>
    &nbsp;&nbsp;
    <div class="form-group">
        <label for="cmb_datos">Informe: </label>
        <select name="cmb_datos" id="cmb_datos" class="form-control input-sm">
            <option value="0" style="font-weight: bold;">Interno</option>
            <option value="1" style="font-weight: bold;">Externo</option>
        </select>
    </div>
</form>