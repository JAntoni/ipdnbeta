<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
	echo 'Terminó la sesión';
	exit();
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<!-- TODOS LOS ESTILOS-->
	<?php include(VISTA_URL . 'templates/head.php'); ?>
	<title><?php echo ucwords(mb_strtolower($menu_tit)); ?></title>
	<style type="text/css">
		.box.box-primary.box-solid>.box-body>.row {
			margin-right: -15px;
			margin-left: 0px;
		}
	</style>
</head>

<body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>
	<div class="wrapper">
		<!-- INCLUIR EL HEADER-->
		<?php include(VISTA_URL . 'templates/header.php'); ?>
		<!-- INCLUIR ASIDE, MENU LATERAL -->
		<?php include(VISTA_URL . 'templates/aside.php'); ?>
		<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
		<?php include('ventacontadoc_vista.php'); ?>
		<!-- INCLUIR FOOTER-->
		<?php include(VISTA_URL . 'templates/footer.php'); ?>
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- TODOS LOS SCRIPTS-->
	<?php include(VISTA_URL . 'templates/script.php'); ?>
	<script type="text/javascript" src="<?php echo VISTA_URL . 'ventacontadoc/ventacontadoc.js?VER=8096471'; ?>"></script>
</body>

</html>