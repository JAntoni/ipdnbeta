var cantidad_clicks = 0;
var opciones_del_select_empresa = null;

$(document).ready(function(){
	$('#form_ventacontadoc').find('input[type="text"], select, textarea').addClass("input-sm");
	$('label').css('font-size', 11.5);
	
	opciones_del_select_empresa = $("select[id*='cbo_sede'] option[value='0']").text("");
	opciones_del_select_empresa = opciones_del_select_empresa[0] .parentElement .innerHTML; //[0] .parentElement .innerHTML

	cambiar_lbl_importe_total(1);

	$('.cant-dias').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '365'
    });

    $('.moneda').focus(function(){
		formato_moneda(this);
	});

	$('.porc-igv').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '100'
    });

	$('.cred-id').autoNumeric({
		aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '999999999'
	});

	$('#txt_doc_condicion').val('CONTADO');
	disabled( $('#form_ventacontadoc').find($('.disabled')) );
	
	/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
	$('#txt_dias_venc')
	.keyup(function(event){
		validar_fecha_venc(this.value);
	})
	.blur(function(){
		if( $('#ventacontadoc_emi_picker').val() == ''){
			$('#ventacontadoc_emi_picker').focus();
		}
	}); */

	$('#txt_porc_igv')
	.keyup(function(){
		calcular_montos();
	});

	/* $('#txt_monto_base_red, #txt_monto_igv_red')
	.keyup(function(){
		if( $('#che_afecta').prop('checked') ){
			calcular_montos_base_igv(1);
			console.log('pasapor aqui');
		}
	}); */

	//COMPLETAR FILAS PARA DOCUMENTOS QUE TIENEN MÁS DE 3 FILAS
	/* array_detalles = JSON.parse($('#hdd_ventacontadetalle_array').val());
	if($('#action').val() != 'insertar'){
		empresa_0 = parseInt(array_detalles[0]['tb_empresa_idfk']);
		$('#cbo_sede_0').val(empresa_0);
	}
	else{
		$('#txt_detalleorden_0').val(1); 
		$('#txt_detalleorden_1').val(2);
		$('#txt_detalleorden_2').val(3);
	}
	contador = 0;
	if(array_detalles !== null && array_detalles.length > 3){
		while(contador < array_detalles.length - 3){
			nuevo_detalle(0);
			contador++;
			$(`#txt_detalleid_${contador}`).val(array_detalles[contador]['tb_ventacontadetalle_id']);
			$(`#txt_detalleorden_${contador}`).val(array_detalles[contador]['tb_ventacontadetalle_orden']);//tb_ventacontadetalle_orden
			$(`#txt_codcuentacont_${contador}`).val(array_detalles[contador]['tb_cuentaconta_cod']);
			$(`#txt_glosa_${contador}`).val(array_detalles[contador]['tb_ventacontadetalle_glosa']);
			$(`#txt_monto_${contador}`).val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(parseFloat(array_detalles[contador]['tb_ventacontadetalle_monto'])));
			$(`#cbo_sede_${contador}`).val(parseInt(array_detalles[contador]['tb_empresa_idfk']));
		}
	} */

	$('input[name*="che_afecta"]').on('ifChanged',function(){
		if (this.checked) {
			$('#txt_porc_igv').val(18);
			$('.es_afecta').show(200);
			$('#txt_porc_igv').focus();
			cambiar_labels('lbl-afecta', 'in');
		} else {
			$('.es_afecta').hide(200);
			$('#txt_porc_igv').val('');
			$('#che_detr').iCheck('uncheck');
			cambiar_labels('lbl-afecta', 'afecta');
			this.focus();
		}
		//if( $('#txt_importe_total').val() !== '' ){
		if( $('input [class*="para_calcular"]').val() !== '' ){
			calcular_montos();
		}
	});

	$('input[name*="che_detr"]').on('ifChanged',function(){
		if (this.checked) {
			$('.es_detr').show(200);
			$('#txt_const_detraccion').focus();
		} else {
			$('.es_detr').hide(200);
			limpiar_detraccion();
			this.focus();
		}
	});

	//autocompletar del cliente
	$("#txt_cliente_doc,#txt_cliente_nom").autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
					VISTA_URL + "cliente/cliente_autocomplete.php",
					{term: request.term}, //
					response
					);
		},
		select: function (event, ui) {
			$('#hdd_cliente_id').val(ui.item.cliente_id);
			$('#txt_cliente_doc').val(ui.item.cliente_doc);
			$('#txt_cliente_nom').val(ui.item.cliente_nom);
			event.preventDefault();
			disabled($('input[id*="txt_cliente"]'));
			disabled($('#btn_agr'));

			//var inputs = $(this).closest('form').find(':focusable'); ///inputs.eq(inputs.index(this) + 8).focus();
			if( $('#txt_doc_cod').prop("disabled") == true){
				$('#cmb_mesconta_id').focus();
			}
			else{
				$('#txt_doc_cod').focus();
			}
		}
	});

	//autocompletar del tipo de documento
	$("#txt_doc_cod,#txt_doc_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "documentocontable/documentocontable_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            $('#txt_doc_cod').val(ui.item.tb_documentocontable_cod);
            $('#txt_doc_nom').val(ui.item.tb_documentocontable_desc);
            event.preventDefault();

			disabled($('#txt_doc_cod, #txt_doc_nom'));

			if( $('#txt_cliente_doc').prop('disabled') == true ){
				$('#cmb_mesconta_id').focus();
			}
			else{
				$('#txt_cliente_doc').focus();
			}
			if($('#txt_doc_cod').val()=='07'){
				$('.documento_ref').show(200);
			}
        }
    });

	//autocompletar del documento referido en las NC
	$("#txt_documento_ref").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "ventacontadoc/ventacontadoc_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
			$('#txt_docrefidfk').val(ui.item.tb_ventacontadoc_id);
			$('#txt_docref_clienteid').val(ui.item.tb_cliente_idfk);
            $('#txt_documento_ref').val(ui.item.tb_ventacontadoc_serie + '-' +ui.item.tb_ventacontadoc_numero);
            $('#txt_docref_fechaemision').val(ui.item.tb_ventacontadoc_fechaemision);
            event.preventDefault();

			$('#txt_motivo_nc').focus();
        }
    });

	//autocompletar de las cuentas contables
	$('input[id*="txt_codcuentacont"]').focus(function(){
		autocompletar_cuenta_contable(this);
	});

	$('#ventacontadoc_emi_pickeri, #ventacontadoc_emi_pickere')
	.datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });
	
	$('#ventacontadoc_emi_pickere').on('changeDate', function(e) {
		verificar_antiguedad_anio_mes();
		//validar_fecha_venc($('#txt_dias_venc').val()); //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
		monedacambioconta_tipo_cambio();
	});

	$('#ventacontadoc_detraccion_picker').datepicker({
		language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
	});

	/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
	$('#ventacontadoc_venc_picker').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
		minDate: null,
		maxDate: null
    }); */

	monedacambioconta_tipo_cambio();

	$('#form_ventacontadoc').validate({
		submitHandler: function (form) {
			var prohibido = verificar_antiguedad_anio_mes();
			if (prohibido == 1) {
				return;
			}

			//comprobar que monto_base (mon_original // soles) + monto_igv (mon_original // soles) = monto_total (mon_original // soles)
			//verificacion en moneda_original
			var nombre_moneda = $('#cmb_moneda_id option:selected').text(); // en soles: S/.

			var monto_base = quitar_comas_miles($('#txt_monto_base_red').val());
			var monto_igv = quitar_comas_miles($('#txt_monto_igv_red').val());
			var monto_total = quitar_comas_miles($('#txt_importe_total').val());

			var monto_base_sol = 0;
			var monto_igv_sol = 0;
			var monto_total_sol = 0;

			if (nombre_moneda !== 'S/.') {
				monto_base_sol = quitar_comas_miles($('#txt_monto_base_sol_red').val());
				monto_igv_sol = quitar_comas_miles($('#txt_monto_igv_sol_red').val());
				monto_total_sol = quitar_comas_miles($('#txt_importe_total_sol_red').val());
			}

			suma_base_igv = quitar_comas_miles(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base + monto_igv));
			suma_base_igv_sol = quitar_comas_miles(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base_sol + monto_igv_sol));

			if ((suma_base_igv != monto_total) || (suma_base_igv_sol != monto_total_sol)) {
				var lbl_monto_base = $('#lbl_monto_base_ori').text().slice(0, -7).toLowerCase();
				var lbl_monto_igv = $('#lbl_monto_igv_ori').text().slice(0, -7).toLowerCase();
				var lbl_monto_total = $('#lbl_monto_total_ori').text().slice(0, -7).toLowerCase();
				mensaje = `La suma de ${lbl_monto_base} y ${lbl_monto_igv} no coincide con el ${lbl_monto_total}. En el resumen columna: `;

				if ((suma_base_igv != monto_total) && (suma_base_igv_sol != monto_total_sol)) {
					mensaje += 'US$ y S/.';
				}
				else if (suma_base_igv !== monto_total) {
					mensaje += nombre_moneda;
				}
				else {
					mensaje += 'S/.';
				}

				setTimeout(function () {
					//notificacion_warning(data.mensaje, 7000);
					swal_warning('VERIFIQUE', mensaje,2000);
				}, 500
				);
				return;
			}

			var cont = 0;
			while (cont < $('#cantidad_filas_detalle').val()) {
				$('#txt_sedenombre_' + cont).val($('#cbo_sede_' + cont + ' option:selected').text());
				cont++;
			}
			var elementos_disabled = $('#form_ventacontadoc').find('input:disabled, select:disabled').removeAttr('disabled');
			var form_serializado = $('#form_ventacontadoc').serialize();
			elementos_disabled.attr('disabled', 'disabled');
			//console.log( form_serializado ); //return;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "ventacontadoc/ventacontadoc_controller.php",
				async: true,
				dataType: "json",
				data: form_serializado,
				beforeSend: function () {
					$('#ventacontadoc_mensaje').show(400);
					$('#btn_guardar_ventacontadoc').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0 && parseInt(data.estado) != 3) {
						$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-success')
						$('#ventacontadoc_mensaje').html('<h4>' + data.mensaje + '</h4>');
						setTimeout(function () {
							if (parseInt(data.estado) != 2) {
								ventacontadoc_tabla();
							}
							$('#modal_registro_ventacontadoc').modal('hide');
						}, 1000
						);
					}
					else {
						$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-warning')
						$('#ventacontadoc_mensaje').html('<h4>Alerta: ' + data.mensaje + '</h4>');
						$('#btn_guardar_ventacontadoc').prop('disabled', false);
					}
				},
				complete: function (data) {
				},
				error: function (data) {
					console.log(data);
					$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#ventacontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
		},
		rules: {
			txt_cliente_doc: {
				required: true,
				minlength: 8,
				maxlength: 11
			},
			txt_doc_cod: {
				required: true,
				minlength: 2
			},
			txt_doc_serie: {
				required: true,
				minlength: 3,
				maxlength: 4
			},
			txt_doc_numero: {
				required: true
			},
			ventacontadoc_emi_pickeri: {
				required: true
			},
			/* //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
			txt_dias_venc: {
				required: true
			}, */
			cmb_mesconta_id: {
				required: true,
				min: 1
			},
			txt_porc_igv: {
				required: function () {
					return $('#che_afecta').prop('checked');
				}
			},
			txt_importe_total: {
				required: true
			},
			txt_monto_base_red: {
				required: true
			},
			txt_monto_igv_red: {
				required: true
			},
			txt_importe_total_sol_red: {
				required: function () {
					return (parseInt($('#cmb_moneda_id').val()) == 2) ? true : false;
				}
			},
			txt_monto_base_sol_red: {
				required: function () {
					return (parseInt($('#cmb_moneda_id').val()) == 2) ? true : false;
				}
			},
			txt_monto_igv_sol_red: {
				required: function () {
					return (parseInt($('#cmb_moneda_id').val()) == 2) ? true : false;
				}
			},
			txt_documento_ref: {
				required: function () {
					return !$('.documento_ref').is(':hidden');
				}
			},
			txt_credito_id: {
				required: function () {
					return $('#che_anexa_credito').prop('checked');
				}
			},
			cmb_creditotipo_id: {
				required: true,
				min: 1
			}
		},
		messages: {
			txt_cliente_doc: {
				required: "Ingrese el cliente",
				minlength: "El RUC/DNI debe tener al menos 8 caracteres"
			},
			txt_doc_cod: {
				required: "Ingrese el tipo de documento",
				minlength: "El código del documento debe tener 2 caracteres"
			},
			txt_doc_serie: {
				required: "Ingrese la serie",
				minlength: "Ingrese al menos 3 dígitos"
			},
			txt_doc_numero: {
				required: "Ingrese el numero de documento"
			},
			ventacontadoc_emi_pickeri: {
				required: "Seleccione la fecha de emision"
			},
			/* //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
			txt_dias_venc: {
				required: "Ingrese cant. dias para vencimiento"
			}, */
			cmb_mesconta_id: {
				required: "Seleccione el periodo contable",
				min: "Seleccione el periodo contable"
			},
			txt_porc_igv: {
				required: "Ingrese el % IGV"
			},
			txt_importe_total: {
				required: "Ingrese importe total"
			},
			txt_monto_base_red: {
				required: "Ingrese monto base con redondeo"
			},
			txt_monto_igv_red: {
				required: "Ingrese monto igv con redondeo"
			},
			txt_importe_total_sol_red: {
				required: "Ingrese importe total soles redondeo"
			},
			txt_monto_base_sol_red: {
				required: "Ingrese monto base soles redondeo"
			},
			txt_monto_igv_sol_red: {
				required: "Ingrese monto igv soles redondeo"
			},
			txt_documento_ref: {
				required: "Ingrese el documento origen"
			},
			txt_credito_id: {
				required: "Ingrese el id del credito a anexar"
			},
			cmb_creditotipo_id: {
				required: "Seleccione el tipo de credito",
				min: "Seleccione el tipo de credito"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			}
			else if (element.parent("div.input-group").length == 1) {
				error.insertAfter(element.parent("div.input-group")[0])
			}
			else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	// jQuery button click event to remove a row.
	$('#tbody')
	.on('click', '.remove', function () {
		filas_detalle =  parseInt($('#cantidad_filas_detalle').val()) - 1;
		$('#cantidad_filas_detalle').val(filas_detalle);
		var child = $(this).closest('tr').nextAll();

		// Iterating across all the rows obtained to change the index
		child.each(function () {
			// Getting <tr> id.
			var id = $(this).attr('id');

			// Gets the row number from <tr> id.
			var dig = parseInt(id.slice(-1));

			// Modifying row id.
			$(this).attr('id', `tabla_cabecera_fila_${dig - 1}`);

			$(this).children('input').each(function(){
				id_antiguo = $(this)[0].id.split('_')[2];
				$(this).attr('id', $(this)[0].id.replace( id_antiguo, (dig - 1) ) );
				$(this).attr('name', $(this)[0].id.replace( id_antiguo, (dig - 1) ) );
				if($(this)[0].id.indexOf("orden") > -1){
					$(this)[0].value --;
				}
			});

			$(this).children('td').children('input, select').each(function () {
				id_antiguo = $(this)[0].id.split('_')[2] ;
				$(this).attr('id', $(this)[0].id.replace( id_antiguo, (dig - 1) ) );
				$(this).attr('name', $(this)[0].id.replace( id_antiguo, (dig - 1) ) );
			});


		});

		// Removing the current row.
		$(this).closest('tr').remove();

		// Decreasing total number of rows by 1.
		cantidad_clicks--;
		calcular_montos();
	})
	.on('keyup', '.para_calcular', function(){
		calcular_montos();
	})
	.on('keyup', '.monto_total', function(){
		copiar_monto = quitar_comas_miles($(this)[0].value);
		$('#txt_importe_total').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(copiar_monto));
		var tipo_cambio = parseFloat($('#txt_tipocambioe').val());
		$('#txt_importe_total_sol_red').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(copiar_monto * tipo_cambio));
	});

	if($('#action').val() == 'leer' || $('#action').val() == 'eliminar'){
		disabled($('#form_ventacontadoc').find("button[class*='btn-sm']")); //attr('disabled', 'disabled') // aqui esta el error
		disabled($('#form_ventacontadoc').find("select"));
		disabled($('#form_ventacontadoc').find("input, textarea"));
		disabled($('#form_ventacontadoc').find("checkbox"));
		disabled($('#form_ventacontadoc').find("a"));
		$('#form_ventacontadoc').find("input").css("cursor", "text");
	}
	else if($('#action').val() == 'modificar'){
		disabled($('input[id*="txt_cliente"], #txt_doc_cod, #txt_doc_nom, #cmb_creditotipo_id, #txt_credito_id'));
		disabled($('#btn_agr'));
	}

});


function cliente_form(usuario_act, cliente_id){ 
	var doc_cliente_id = Number($('#hdd_cliente_id').val());
	//console.log(doc_cliente_id);
  
	if(parseInt(doc_cliente_id) <= 0 && usuario_act == 'M'){
	  alerta_warning('Información', 'Debes buscar por documento o nombres');
	  return false;
	}
  
	$.ajax({
		  type: "POST",
		  url: VISTA_URL+"cliente/cliente_form.php",
	  async: true,
		  dataType: "html",
		  data: ({
		action: usuario_act, // PUEDE SER: L, I, M , E
		cliente_id: doc_cliente_id,
		vista: 'ventacontadoc'
	  }),
		beforeSend: function () {
			$('#h3_modal_title').text('Cargando Formulario');
			$('#modal_mensaje').modal('show');
		},
		  success: function(data){
		$('#modal_mensaje').modal('hide');
		if(data != 'sin_datos'){
			$('#div_modal_cliente_form').html(data);
			$('#modal_registro_cliente').modal('show');
  
		  //desabilitar elementos del form si es L (LEER)
		  if(usuario_act == 'L' || usuario_act == 'E')
			form_desabilitar_elementos('form_cliente'); //funcion encontrada en public/js/generales.js
		  
		  //funcion js para limbiar el modal al cerrarlo
			modal_hidden_bs_modal('modal_registro_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
		  //funcion js para agregar un largo automatico al modal, al abrirlo
		  modal_height_auto('modal_registro_cliente'); //funcion encontrada en public/js/generales.js
		  //funcion js para agregar un ancho automatico al modal, al abrirlo
		  modal_width_auto('modal_registro_cliente', 75); //funcion encontrada en public/js/generales.js
		}
		else{
			//llamar al formulario de solicitar permiso
			var modulo = 'cliente';
			var div = 'div_modal_cliente_form';
			permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
		}
		  },
		  complete: function(data){
			  //console.log('Aqui listo22')
		  },
		  error: function(data){
		alerta_error('ERROR', data.responseText)
			  //console.log(data);
		  }
	  });
}

function llenar_datos_cliente(data){
	$('#hdd_cliente_id').val(data.registro['tb_cliente_id']);
	$('#txt_cliente_doc').val(data.registro['tb_cliente_doc']);
	$('#txt_cliente_nom').val(data.registro['tb_cliente_nom']);

	disabled($('input[id*="txt_cliente"]'));
	disabled($('#btn_agr'));

	$('#txt_doc_cod').focus();
	$('#txt_cliente_doc-error').hide(50);
}

function llenar_datos_monedacambiocontable(data){
	$('#hdd_monedacambiocontable_idfk').val(data.registro['tb_monedacambiocontable_id']);
	$('#txt_tipocambioe').val(data.registro['tb_monedacambiocontable_ventasunat']);

	disabled($('#txt_tipocambioe'));
	$('#span_errore').hide(200);
	//$('#txt_doc_cod').focus(); //siguiente
}

function formato_serie_contable(id_txt, txt_serie){
	//id_txt contiene el id de la caja de texto que contiene el codigo de documento
	//txt_serie es la caja de texto donde se dará el formato
	var cod_doc = $('#'+id_txt).val();
	$('#'+txt_serie.id).keydown(function(event){
		var codigo = event.which || event.keyCode;
		var text = txt_serie.value.toUpperCase();
		if(text.length +1 > 4){ // maximo de caracteres
			if(codigo !==8 && codigo !==9 && !(codigo > 36 && codigo < 41)) // 8: tecla borrar, 9: tecla TAB, 36 al 41 son flechas
				return false;
		}
		else{
			if(text.length +1 > 2 ){//despues de segundo digito solo numeros
				if ((codigo < 48 || codigo > 57) && (codigo < 96 || codigo > 105) && codigo !==8 && codigo !==9 && !(codigo > 36 && codigo < 41)) {return false;}
			}
			else if (text.length +1 < 2){ //primer digito debe ser letra
				if(!(codigo > 64 && codigo < 91) && codigo !==9 && !(codigo > 36 && codigo < 41)) {return false;}
			}
			else{
				var key = event.key;
				if(key){
					key = key.toLowerCase();
					const isLetter = (key >= "a" && key <= "z");
					const isNumber = (key >= "0" && key <= "9");
					if (!(isLetter || isNumber)) {
						return false;
					}
				}
			}
		}
	});
	$('#'+txt_serie.id).blur(function(){ // cuando pierde el foco se autocompleta
		if(txt_serie.value.length == 3){
			txt_serie.value = txt_serie.value.slice(0,1)+'0'+txt_serie.value.slice(-2);
		}
	});
}

function formato_numero_contable(txt_numero){
	$('.numero-doc').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '99999999'
    });
	$('#'+txt_numero.id).blur(function(){ // cuando pierde el foco se autocompleta
		if(txt_numero.value.length > 0){
			var i=0, completar='';
			var cantidad_falta = 8 - txt_numero.value.length;
			while(i<cantidad_falta){
				completar+='0';
				i++;
			}
			txt_numero.value = completar+txt_numero.value;
		}
		if(txt_numero.id == "txt_doc_numeroe"){
			verificar_documento_existente();
		}
	});
}

function verificar_documento_existente(){
	const action_original = $('#action').val();
	$('#action').val(action_original +'_verificar_documento');
	var elementos_disabled = $('#form_ventacontadoc').find('input:disabled, select:disabled').removeAttr('disabled');
	var form_serializado = $('#form_ventacontadoc').serialize();
	elementos_disabled.attr('disabled', 'disabled');
	$('#action').val(action_original);
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ventacontadoc/ventacontadoc_controller.php",
		async: true,
		dataType: "json",
		data: form_serializado,
		beforeSend: function () {
			$('#ventacontadoc_mensaje').show(400);
			$('#btn_guardar_ventacontadoc').prop('disabled', true);
		},
		success: function (data) {
			if (parseInt(data.estado) > 0) {
				// cambiar TOAST por SWAL
				setTimeout(function () {
					//notificacion_warning(data.mensaje, 7000);
					swal_warning('VERIFIQUE', data.mensaje,2000);
				}, 500 );
			}
		},
		complete: function (data) {
			$('#ventacontadoc_mensaje').hide(100);
			$('#btn_guardar_ventacontadoc').prop('disabled', false);
		},
		error: function (data) {
			$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
			$('#ventacontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}

//funcion para revisar si hay tc registrado en la fecha seleccionada
function monedacambioconta_tipo_cambio() {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "monedacambiocontable/monedacambiocontable_controller.php",
		async: true,
		dataType: "json",
		data: ({
			action: 'tipocambio',
			monedacambio_fec: $('#ventacontadoc_emi_pickere').val(),
			moneda_id: $('#cmb_moneda_id').val()
		}),
		beforeSend: function () {
			$('#txt_tipocambioe').val('Consultando...');
		},
		success: function (data) {
			if (parseInt(data.estado) == 1) {
				$('#txt_tipocambioe').val(data.valor_venta);
				$('#hdd_monedacambiocontable_idfk').val(data.id);
				$("#salto").remove();
				$('#span_errore').hide(200);
			} else {
				$('#txt_tipocambioe').val('');
				$('#span_errore').show(200);
				var link = ' <a href="javascript:void(0)" onclick="monedacambiocontable_form(\'I\', 0, \'' + data.fecha + '\')" style="color: black;"> Debe registrar TC</a>'
				$('#span_errore').html(link);
			}
		},
		complete: function (data) {

		},
		error: function (data) {
			$('#span_errore').show(200);
			$('#span_errore').html('ERROR AL CONSULTAR: ' + data.responseText);
		}
	});
}

function monedacambiocontable_form(usuario_act, monedacambiocontable_id, fecha) {
	//console.log(fecha);return;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambiocontable/monedacambiocontable_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            monedacambiocontable_id: monedacambiocontable_id,
            vista: 'ventacontadoc',
			fecha: fecha
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_monedacambiocontable_form').html(data);
                $('#modal_registro_monedacambiocontable').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_monedacambiocontable'); //funcion encontrada en public/js/generales.js

                modal_hidden_bs_modal('modal_registro_monedacambiocontable', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_height_auto('modal_registro_monedacambiocontable'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'monedacambiocontable';
                var div = 'div_modal_monedacambiocontable_form';
                permiso_solicitud(usuario_act, monedacambiocontable_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            //console.log(data.responseText);
        }
    });
}

function limpiar_cliente(){
	$('#hdd_cliente_id').val('');
	$('#txt_cliente_doc').val('');
	$('#txt_cliente_nom').val('');

	$('#txt_cliente_doc').prop('disabled', false);
	$('#txt_cliente_nom').prop('disabled', false);
	$('#btn_agr').removeAttr('disabled');// attr('', '')
	$('#txt_cliente_doc').focus();
	limpiar_doc_ref(0);
}

function limpiar_tipodoc(){
	$('#txt_doc_cod').val('');
	$('#txt_doc_nom').val('');
	$('txt_motivo_nc').val('');

	$('#txt_doc_cod').prop('disabled', false);
	$('#txt_doc_nom').prop('disabled', false);
	$('#txt_doc_serie').focus();
	$('#txt_doc_cod').focus();
	$('.documento_ref').hide(200);
	limpiar_doc_ref(1);
}

function limpiar_credito(){
	$("#cmb_creditotipo_id").val("0");
	$('#txt_credito_id').val('');
	$('#txt_credito_cliente').val('');
	$('#txt_credito_monto').val('');

	$('#cmb_creditotipo_id').prop('disabled', false);
	$('#txt_credito_id').prop('disabled', false);
	$('#cmb_creditotipo_id').focus();
}

function limpiar_doc_ref(num){
	$('#txt_documento_ref').val('');
	$('#txt_docref_fechaemision').val('')
	.datepicker('option', {minDate: null, maxDate: null});
	if(num == 1) { $('#txt_motivo_nc').val('') }
}

function limpiar_detraccion(){
	$('#txt_const_detraccion').val('');
	$('#ventacontadoc_detraccion_picker').val('')
	.datepicker('option', {minDate: null, maxDate: null});
}

$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
	checkboxClass: 'icheckbox_flat-green',
	radioClass: 'iradio_flat-green'
});

$('#cmb_moneda_id').change(function(){
    cambiar_lbl_importe_total(6);
	var moneda_id = parseInt($(this).val());
	if(moneda_id == 2){
		$('#box_redim').width('48.4%');
		$('div[id*="div_mon_ori_"]').removeClass('col-md-12').addClass('col-md-6');
		$('.moneda-dolar').show(200);
	}else{
		$('.moneda-dolar').hide(100);
		$('div [id*="div_mon_ori"]').removeClass('col-md-6').addClass('col-md-12');
		$('#box_redim').width('24.2%');
	}
	if ($('#ventacontadoc_emi_pickeri').val() !== ''){
		//monedacambioconta_tipo_cambio(); // se debe hacer la consulta del tipo de cambio interno
	}
	else{
		$('#txt_tipocambioi').val('');
		$('#ventacontadoc_emi_pickeri').focus();
	}
});

function cambiar_lbl_importe_total(num){
	arreglo = $('#form_ventacontadoc').find("label[class*='lbl-moneda']");
	Object.keys(arreglo).forEach(element => {
		index = parseInt(element);
		if(Number.isInteger(index)){
			const texto_actual = arreglo[index].innerHTML.slice(0,-num);
			var moneda_nom = '('+$('select[name="cmb_moneda_id"] option:selected').text() +'):';
			arreglo[index].innerHTML = texto_actual + moneda_nom;
		}
	});
}

function disabled(obj){
	Object.keys(obj).forEach(element => {
		index = parseInt(element);
		if(Number.isInteger(index)){
			//console.log(obj[index]);
			if(obj[index].type == "text" || obj[index].type == "textarea"){
				//console.log(obj[index].id);
				$('#'+obj[index].id).prop('disabled', true);
				$('#'+obj[index].id).css("cursor", "text");
			}
			else if(obj[index].type == "button"){
				$('#'+obj[index].id).attr('disabled', 'disabled');
				$('#'+obj[index].id).css("cursor", "context-menu");
			}
			else if(obj[index].type == "select-one" || obj[index].type == "checkbox"){
				$('#'+obj[index].id).prop('disabled', true);
				$('#'+obj[index].id).css("cursor", "context-menu");
			}
			else if(obj[index].tagName == "A"){
				$('#'+obj[index].id).css("pointer-events", "none");
				$('#'+obj[index].id).css("cursor", "context-menu");
			}
		}
	});
}

/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
function validar_fecha_venc(text){
	var dias_para_venc = parseInt($('#txt_dias_venc').val());
	if (text.length > 0) {
		if ($('#ventacontadoc_emi_picker').val() != '') {
			recalcular_fecha_fin($('#ventacontadoc_emi_picker').val(), dias_para_venc, $('#ventacontadoc_venc_picker'));
		}
	}
	else {
		$('#ventacontadoc_venc_picker').val('')
			.datepicker('option', { minDate: null, maxDate: null });
	}
} */

//puede ir para los generales
function recalcular_fecha_fin(sFecha, dias, objet){
	var fecha = sFecha.split("-");
	fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
	fecha = new Date(fecha);
	fecha.setDate(fecha.getDate() + dias+1);
	nueva_fecha = ((fecha.getDate() > 9) ? fecha.getDate() : ('0' + fecha.getDate())) + '-' + ((fecha.getMonth() > 8) ? (fecha.getMonth() + 1) : ('0' + (fecha.getMonth() + 1))) + '-' + fecha.getFullYear();
	objet.datepicker("setDate",nueva_fecha);
}

function cambiar_labels(nombre_clase, texto){
	texto = texto.toUpperCase();
	arreglo = $('#form_ventacontadoc').find("label[class*='"+nombre_clase+"']");
	Object.keys(arreglo).forEach(element => {
		index = parseInt(element);
		if(Number.isInteger(index)){
			texto_a_poner = texto=='IN' ? "" : "INAFECTA";
			nuevo_texto = arreglo[index].innerHTML.replace(texto, texto_a_poner, "ig") ;
			arreglo[index].innerHTML = nuevo_texto;
		}
	});
}

function quitar_comas_miles(valor){
	return parseFloat(valor.replace(/,/g, ""));
}

function calcular_montos() {
	//console.log('ejecutando: calcular_montos()');
	var monto_igv = 0, monto_total = 0, monto_base = 0;
	var porc_igv = $('#txt_porc_igv').val() == '' ? 0 : parseInt($('#txt_porc_igv').val()) / 100;
	//calcular el igv en la tabla, y calcular monto total en la tabla
	filas_articulos = $('input[id*="txt_codcuentacont"].disabled').closest('tr').prevAll(); //son articulos o servicios ventados, con los cuales se calculará el monto igv y monto total
	// Iterating across all the rows obtained to change the index
	filas_articulos.each(function () {
		//obtener la columna del monto
		monto_articulo = $(this).children('.monto').children('.moneda').val();
		monto_articulo = monto_articulo === '' ? 0 : quitar_comas_miles(monto_articulo);

		//console.log( monto_articulo + ' - tipo: ' + typeof monto_articulo );
		monto_igv += (monto_articulo * porc_igv);
		monto_base += monto_articulo;
	});

	monto_igv1 = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_igv);

	//cambiar el valor del input monto_igv en la tabla
	$('input[id*="txt_codcuentacont"].disabled').closest('tr').children('.monto').children('.moneda').val(monto_igv1);

	$('input[class*="monto_total"]').closest('tr').prevAll().each(function () {
		valor_a_sumar = $(this).children('.monto').children('.moneda').val();
		valor_a_sumar = valor_a_sumar === '' ? 0 : quitar_comas_miles(valor_a_sumar);

		monto_total += valor_a_sumar;
	});
	monto_total1 = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_total);
	monto_base1 = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base);
	//$('#txt_').val( new Intl.NumberFormat('es-PE', { style: 'currency', currency: 'PEN' }).format(parseFloat(calcular_monto_igv)) ); // more info https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat/NumberFormat#examples

	//console.log(monto_total);
	$('input[class*="monto_total"]').val(monto_total1);
	$('#txt_monto_base_red').val(monto_base1);
	$('#txt_monto_igv_red').val(monto_igv1);
	$('#txt_importe_total').val(monto_total1);

	var moneda_id = parseInt($('#cmb_moneda_id').val());
	if (moneda_id == 2) {
		if ($('#txt_tipocambioe').val() !== '') {
			var tipo_cambio = parseFloat($('#txt_tipocambioe').val());
			//console.log('monto total: ' + monto_total + ' . tipo cambio: ' + tipo_cambio );
			$('#txt_importe_total_sol_red').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_total * tipo_cambio));

			calcular_montos_base_igv();
		}
	}
	// DETRACCION
	caja_monto_total = moneda_id == 1 ? $('#txt_importe_total') : $('#txt_importe_total_sol_red');
	if (quitar_comas_miles(caja_monto_total.val()) >= 700 && $('#che_afecta').prop('checked')) {
		$('.detrac').show(200);
	}
	else {
		limpiar_detraccion();
		$('.detrac').hide(200);
	}
}

function calcular_montos_base_igv(){
	var tipo_cambio = parseFloat($('#txt_tipocambioe').val());
	var monto_base_mon_ori = quitar_comas_miles($('#txt_monto_base_red').val());
	var monto_igv_mon_ori = quitar_comas_miles($('#txt_monto_igv_red').val());

	$('#txt_monto_base_sol_red').val( new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base_mon_ori*tipo_cambio) );
	
	$('#txt_monto_igv_sol_red').val( new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_igv_mon_ori*tipo_cambio) );
}

$('#txt_credito_id').blur(function(){
	buscar_credito();
});
$('#cmb_creditotipo_id').change(function(){
	buscar_credito();
});

function buscar_credito(){
	var tipo_credito_id = parseInt( $('#cmb_creditotipo_id').val() );
	var credito_id		= parseInt( $('#txt_credito_id').val() );
	if(tipo_credito_id != 0 && !isNaN(credito_id)){
		//console.log(tipo_credito_id + ' - '+ credito_id + ' gaa');
		$.ajax({
			type: "POST",
			url: VISTA_URL + "ventacontadoc/ventacontadoc_controller.php",
			async: true,
			dataType: "json",
			data: ({
				action: 'buscar_credito',
				tipo_credito_id: tipo_credito_id,
				credito_id: credito_id
			}),
			beforeSend: function () {
				$('#ventacontadoc_mensaje').removeClass('callout-warning').addClass('callout-info').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
				$('#btn_guardar_ventacontadoc').prop('disabled', true);
			},
			success: function (data) {
				if (parseInt(data.estado) > 0) {
					setTimeout(function () {
						//acciones a realizar // tb_moneda_nom tb_credito_preaco tb_cliente_nom
						$('#ventacontadoc_mensaje').hide(100);
						$('#btn_guardar_ventacontadoc').prop('disabled', false);

						$('#txt_credito_cliente').val(data.data['tb_cliente_nom']);
						monto = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(parseFloat(data.data['tb_credito_preaco']));
						$('#txt_credito_monto').val(data.data['tb_moneda_nom'] + monto.toString() );
						
						disabled($('#cmb_creditotipo_id, #txt_credito_id'));
					}, 1000
					);
				}
				else {
					$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-warning');
					$('#ventacontadoc_mensaje').html('<h4>Alerta: '+ data.mensaje +'</h4>');
					$('#btn_guardar_ventacontadoc').prop('disabled', false);
				}
			},
			complete: function (data) {
			},
			error: function (data) {
				//console.log(data);
				$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
				$('#ventacontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
			}
		});
	}
}

$('#cmb_mesconta_id').change(function(){
	if($('#cmb_mesconta_id').val() != 0){
		validar_mes_conta();
	}
	else{
		$('#txt_ventaconta_correlativo').val('');
	}
});

function validar_mes_conta(){
	const action_original = $('#action').val();
	$('#action').val('validar_mes_conta');
	var elementos_disabled = $('#form_ventacontadoc').find('input:disabled, select:disabled').removeAttr('disabled');
	var form_serializado = $('#form_ventacontadoc').serialize();
	elementos_disabled.attr('disabled', 'disabled');
	$('#action').val(action_original);
	//console.log( form_serializado );//return;
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ventacontadoc/ventacontadoc_controller.php",
		async: true,
		dataType: "json",
		data: form_serializado,
		beforeSend: function () {
		},
		success: function (data) {
			if (parseInt(data.estado) == 3) {
				$('#txt_ventaconta_correlativo').val('');
				// cambiar TOAST por SWAL
				setTimeout(function () {
					//notificacion_warning(data.mensaje, 7000);
					swal_warning('VERIFIQUE', data.mensaje,2000);
				}, 500
				);
			}
			if (parseInt(data.estado) == 4){
				$('#txt_ventaconta_correlativo').val(data.data);
			}
		},
		complete: function (data) {
		},
		error: function (data) {
			$('#ventacontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
			$('#ventacontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}

function autocompletar_cuenta_contable(element){
		$('#'+ element.id).autocomplete({
			minLength: 1,
			source: function (request, response) {
				$.getJSON(
					VISTA_URL + "plancontable/plancontable_autocomplete.php",
					{
						term: request.term,
						nivel_inicio: 1,
						nivel_final: 6
					},
					response
				);
			},
			select: function (event, ui) {
				var dig = parseInt(this.id.split('_')[2]);
				$(`#txt_nomtablacuentacont_${dig}`).val('tb_cuentaconta' + ui.item.nivel);
				$(`#txt_regidcuentacont_${dig}`).val(ui.item.id);
				$('#'+this.id).val(ui.item.tb_cuentaconta_cod);
				//$('#hdd_fil_cuentaconta_selected').val(JSON.stringify(ui.item));
				event.preventDefault();
			}
		});
}

function formato_moneda(element){
	$('#'+ element.id).autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0.00',
        vMax: '999999999.00'
    });
}

function nuevo_detalle(num){
	cantidad_clicks++;
	filas_detalle =  parseInt($('#cantidad_filas_detalle').val()) +1;
	$('#cantidad_filas_detalle').val(filas_detalle);
	var child = $('#txt_codcuentacont_' + (cantidad_clicks - 1)).closest('tr').nextAll();

	// Iterating across all the rows obtained to change the index
	child.each(function () {
		// Getting <tr> id.
		var id = $(this).attr('id');

		// Gets the row number from <tr> id.
		var dig = parseInt(id.split('_')[3]);

		// Modifying row id.
		$(this).attr('id', `tabla_cabecera_fila_${dig + 1}`);

		$(this).children('input').each(function(){
			id_antiguo = $(this)[0].id.split('_')[2];
			$(this).attr('id', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
			$(this).attr('name', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
			if($(this)[0].id.indexOf("orden") > -1  && num > 0){
				$(this)[0].value ++;
			}
		});

		$(this).children('td').children('input, select').each(function () {
			id_antiguo = $(this)[0].id.split('_')[2];
			$(this).attr('id', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
			$(this).attr('name', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
		});

	});

	$('#tabla_cabecera_fila_' + (cantidad_clicks - 1))
		.after(`<tr id="tabla_cabecera_fila_${cantidad_clicks}">
					<input type="hidden" name="txt_detalleid_${cantidad_clicks}" id="txt_detalleid_${cantidad_clicks}">
					<input type="hidden" name="txt_detalleorden_${cantidad_clicks}" id="txt_detalleorden_${cantidad_clicks}" value="${cantidad_clicks+1}">
					<td id="tabla_fila"><input type="text" name="txt_codcuentacont_${cantidad_clicks}" id="txt_codcuentacont_${cantidad_clicks}" class="form-control input-sm" onfocus="autocompletar_cuenta_contable(this)" placeholder="COD. CTA. CONT."></td>
					<td id="tabla_fila"><input type="text" name="txt_glosa_${cantidad_clicks}" id="txt_glosa_${cantidad_clicks}" class="form-control mayus input-sm" placeholder="GLOSA"></td>
					<td id="tabla_fila" class="monto"><input type="text" name="txt_monto_${cantidad_clicks}" id="txt_monto_${cantidad_clicks}" class="form-control moneda input-sm para_calcular" onfocus="formato_moneda(this)" placeholder="0.00"></td>
					<td id="tabla_fila">
						<input type="hidden" name="txt_sedenombre_${cantidad_clicks}" id="txt_sedenombre_${cantidad_clicks}">
						<select name="cbo_sede_${cantidad_clicks}" id="cbo_sede_${cantidad_clicks}" class="form-control input-sm"> ${opciones_del_select_empresa} </select>
					</td>
					<td id="tabla_fila" class="text-center"><button name="btn_eliminar_detalle_${cantidad_clicks}" id="btn_eliminar_detalle_${cantidad_clicks}" class="btn btn-sm btn-danger remove fa fa-trash" type="button" title="ELIMINAR"></button></td>
				</tr>`);
}

function verificar_antiguedad_anio_mes(){
	var d = new Date();
	var curr_month = d.getMonth() + 1; //Months are zero based
	var curr_year = d.getFullYear();

	var d_selected = $('#ventacontadoc_emi_pickere').val();
	var mes_selecc = d_selected.split("-")[1];
	var anio_selecc = d_selected.split("-")[2];

	var resta_anios = parseInt(curr_year) - parseInt(anio_selecc);
	var resta_meses = parseInt(curr_month) - parseInt(mes_selecc);
	if(resta_anios > 1){
		swal_warning('VERIFIQUE', 'No se puede registrar documentos de antiguedad mayor a un año mes',2000);
		return 1;
	}
	else if(resta_anios == 1){
		if(resta_meses > 0){
			swal_warning('VERIFIQUE', 'No se puede registrar documentos de antiguedad mayor a un año mes',2000);
			return 1;
		}
	}
}