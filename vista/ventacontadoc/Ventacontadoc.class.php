<?php

if (defined('APP_URL')) {
    require_once (APP_URL . 'datos/conexion.php');
} else {
    require_once ('../../datos/conexion.php');
}

class Ventacontadoc extends Conexion{
    public $tb_ventacontadoc_id;
    public $tb_cliente_idfk;
    public $tb_documentocontable_codfk;
    public $tb_ventacontadoc_seriei;
    public $tb_ventacontadoc_seriee;
    public $tb_ventacontadoc_numeroi;
    public $tb_ventacontadoc_numeroe;
    public $tb_ventacontadoc_condicion;
    public $tb_ventacontadoc_fechaemisioni;
    public $tb_ventacontadoc_fechaemisione;
    public $tb_ventacontadoc_fechavencimientoi;
    public $tb_ventacontadoc_fechavencimientoe;
    public $tb_mesconta_idfk;
    public $tb_moneda_idfk;
    public $tb_monedacambio_idfk;
    public $tb_monedacambiocontable_idfk;
    public $tb_ventacontadoc_montototali;
    public $tb_ventacontadoc_montosolesi;
    public $tb_ventacontadoc_montototale;
    public $tb_ventacontadoc_montosolese;
    public $tb_ventacontadoc_afecta;
    public $tb_ventacontadoc_porcentajeigv;
    public $tb_ventacontadoc_baseinafectai;
    public $tb_ventacontadoc_baseinafectasolesi;
    public $tb_ventacontadoc_baseinafectae;
    public $tb_ventacontadoc_baseinafectasolese;
    public $tb_ventacontadoc_baseafectai;
    public $tb_ventacontadoc_baseafectasolesi;
    public $tb_ventacontadoc_baseafectae;
    public $tb_ventacontadoc_baseafectasolese;
    public $tb_ventacontadoc_montoigvi;
    public $tb_ventacontadoc_montoigvsolesi;
    public $tb_ventacontadoc_montoigve;
    public $tb_ventacontadoc_montoigvsolese;
    public $tb_ventacontadoc_docrefidfk;
    public $tb_ventacontadoc_motivonota;
    public $tb_ventacontadoc_detalle;
    public $tb_ventacontadoc_fecreg;
    public $tb_ventacontadoc_usureg;
    public $tb_ventacontadoc_fecmod;
    public $tb_ventacontadoc_usumod;
    public $tb_ventacontadoc_xac;
    public $tb_ventacontadoc_correlativo;

    public function listar_todos($array, $otros_param){
        //array: bidimensional con formato $column_name, $param.$i, $datatype
        //otros_param: arreglo para funciones diversas como el order by, limit, etc. los keys se definen de acuerdo al uso (ejm: ordenar: ["orden"]["column_name"], ["orden"]["value"])
        try {
            $sql = "SELECT
                    vcd.*,
                    mc.tb_mesconta_anio, mc.tb_mesconta_mes,
                    dc.tb_documentocontable_desc,
                    vcd1.tb_ventacontadoc_seriei as docreferido_serieint, vcd1.tb_ventacontadoc_seriee as docreferido_serieext,
                    vcd1.tb_ventacontadoc_numeroi as docreferido_numeroint, vcd1.tb_ventacontadoc_numeroe as docreferido_numeroext,
                    vcd1.tb_ventacontadoc_fechaemisioni as docreferido_emisionint, vcd1.tb_ventacontadoc_fechaemisione as docreferido_emisionext,
                    vcd1.tb_cliente_idfk as docref_clienteidfk, vcd1.tb_documentocontable_codfk as docreferido_tipodoc,
                    c.tb_cliente_doc, c.tb_cliente_nom,
                    m.tb_moneda_nom, m.tb_moneda_des, m.tb_moneda_codsunat,
                    IF(vcd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_comprasunat) AS tipocambio_comprasunat,
                    IF(vcd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_ventasunat) AS tipocambio_ventasunat,
                    IF(vcd.tb_monedacambio_idfk IS NULL, 1,	mc1.tb_monedacambio_com) AS tipocambio_compra,
                    IF(vcd.tb_monedacambio_idfk IS NULL, 1,	mc1.tb_monedacambio_val) AS tipocambio_venta
                    FROM tb_ventacontadoc vcd
                    LEFT JOIN tb_mesconta mc ON vcd.tb_mesconta_idfk = mc.tb_mesconta_id
                    INNER JOIN tb_documentocontable dc ON vcd.tb_documentocontable_codfk = dc.tb_documentocontable_cod
                    LEFT JOIN (select * from tb_ventacontadoc) as vcd1 ON vcd.tb_ventacontadoc_docrefidfk = vcd1.tb_ventacontadoc_id
                    INNER JOIN tb_cliente c ON c.tb_cliente_id = vcd.tb_cliente_idfk
                    INNER JOIN tb_moneda m ON vcd.tb_moneda_idfk = m.tb_moneda_id
                    LEFT JOIN tb_monedacambiocontable mcc ON vcd.tb_monedacambiocontable_idfk = mcc.tb_monedacambiocontable_id
                    LEFT JOIN tb_monedacambio mc1 ON vcd.tb_monedacambio_idfk = mc1.tb_monedacambio_id";
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i])) {
                    $sql .= ($i > 0) ? " AND " : " WHERE ";

                    if (!empty($array[$i]["param{$i}"]) && empty($array[$i]['column_name']) && empty($array[$i]['datatype'])) {
                        $sql .= ' ' . $array[$i]["param{$i}"];
                    } else {
                        if (stripos($array[$i]['column_name'], 'fec') !== FALSE) { //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
                            $sql .= " DATE_FORMAT(vcd." . $array[$i]['column_name'] . ", '%Y-%m-%d')";
                        } else { // de lo contrario solo se coloca el nombre de la columna a filtrar
                            $sql .= 'vcd.' . $array[$i]['column_name'];
                        }
                        $sql .= " = :param" . $i;
                    }
                }
            }
            $sql .= " ORDER BY {$otros_param["orden"]["column_name"]} {$otros_param["orden"]["value"]}";

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]['column_name']) && !empty($array[$i]['datatype'])) {
                    $_PARAM = strtoupper($array[$i]['datatype']) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
                    $sentencia->bindParam(":param" . $i, $array[$i]['param' . $i], $_PARAM);
                }
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay documentos contables de ventas registrados";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    function mostrarUno($array, $array1){
        //array: bidimensional con formato $column_name, $param.$i, $datatype
        //array1: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
        try {
            $sql = "SELECT
                    vcd.*,
                    mc.tb_mesconta_anio, mc.tb_mesconta_mes,
                    dc.tb_documentocontable_desc,
                    vcd1.tb_ventacontadoc_seriei as docreferido_serieint, vcd1.tb_ventacontadoc_seriee as docreferido_serieext,
                    vcd1.tb_ventacontadoc_numeroi as docreferido_numeroint, vcd1.tb_ventacontadoc_numeroe as docreferido_numeroext,
                    vcd1.tb_ventacontadoc_fechaemisioni as docreferido_emisionint, vcd1.tb_ventacontadoc_fechaemisione as docreferido_emisionext,
                    vcd1.tb_cliente_idfk as docref_clienteidfk, vcd1.tb_documentocontable_codfk as docreferido_tipodoc,
                    c.tb_cliente_doc, c.tb_cliente_nom,
                    m.tb_moneda_nom, m.tb_moneda_des, m.tb_moneda_codsunat,
                    IF(vcd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_comprasunat) AS tipocambio_comprasunat,
                    IF(vcd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_ventasunat) AS tipocambio_ventasunat,
                    IF(vcd.tb_monedacambio_idfk IS NULL, 1,	mc1.tb_monedacambio_com) AS tipocambio_compra,
                    IF(vcd.tb_monedacambio_idfk IS NULL, 1,	mc1.tb_monedacambio_val) AS tipocambio_venta";
            if (!empty($array1)) {
                $sql .= ",\n";
                for ($j = 0; $j < count($array1); $j++) {
                    $sql .= $array1[$j]['alias_columnasparaver'] . "\n";
                }
            }
            $sql .= " FROM tb_ventacontadoc vcd
                    LEFT JOIN tb_mesconta mc ON vcd.tb_mesconta_idfk = mc.tb_mesconta_id
                    INNER JOIN tb_documentocontable dc ON vcd.tb_documentocontable_codfk = dc.tb_documentocontable_cod
                    LEFT JOIN (select * from tb_ventacontadoc) as vcd1 ON vcd.tb_ventacontadoc_docrefidfk = vcd1.tb_ventacontadoc_id
                    INNER JOIN tb_cliente c ON c.tb_cliente_id = vcd.tb_cliente_idfk
                    INNER JOIN tb_moneda m ON vcd.tb_moneda_idfk = m.tb_moneda_id
                    LEFT JOIN tb_monedacambiocontable mcc ON vcd.tb_monedacambiocontable_idfk = mcc.tb_monedacambiocontable_id
                    LEFT JOIN tb_monedacambio mc1 ON vcd.tb_monedacambio_idfk = mc1.tb_monedacambio_id\n";
            if (!empty($array1)) {
                for ($k = 0; $k < count($array1); $k++) {
                    $sql .= $array1[$k]['tipo_union'] . " JOIN " . $array1[$k]['tabla_alias'] . " ON " . $array1[$k]['columna_enlace'] . " = " . $array1[$k]['alias_columnaPK'] . "\n";
                }
            }
            for ($i = 0; $i < count($array); $i++) {
                $sql .= ($i > 0) ? " AND " : " WHERE ";
               
                if (!empty($array[$i]["param{$i}"]) && empty($array[$i]['column_name']) && empty($array[$i]['datatype'])) {
                    $sql .= ' ' . $array[$i]["param{$i}"];
                } else {
                    if (stripos($array[$i]['column_name'], 'fec') !== FALSE) { //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
                        $sql .= " DATE_FORMAT(vcd." . $array[$i]['column_name'] . ", '%Y-%m-%d')";
                    } else { // de lo contrario solo se coloca el nombre de la columna a filtrar
                        $sql .= 'vcd.' . $array[$i]['column_name'];
                    }
                    $sql .= " = :param" . $i;
                }
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]['column_name']) && !empty($array[$i]['datatype'])) {
                    $_PARAM = strtoupper($array[$i]['datatype']) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
                    $sentencia->bindParam(":param" . $i, $array[$i]['param' . $i], $_PARAM);
                }
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No existe este registro";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function eliminar(){
        $this->dblink->beginTransaction();
        try {
          $sql = "UPDATE tb_ventacontadoc SET tb_ventacontadoc_xac = 0, tb_ventacontadoc_usumod = :tb_ventacontadoc_usumod WHERE tb_ventacontadoc_id = :tb_ventacontadoc_id;";
    
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_ventacontadoc_usumod", $this->tb_ventacontadoc_usumod, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_ventacontadoc_id", $this->tb_ventacontadoc_id, PDO::PARAM_INT);
          $resultado = $sentencia->execute(); //retorna 1 si es correcto
    
          if($resultado == 1){
            $this->dblink->commit();
          }
          return $resultado;
        } catch (Exception $th) {
          $this->dblink->rollBack();
          throw $th;
        }
      }
}

//ALT + FLECHA ARRIBA / ABAJO :  para mover la linea de codigo || SHIFT + SUPR : para eliminar la linea actual
//CTRL + L : para seleccionar la linea
// div#MyID.myClass: abreviatura emmet, para escribir un div con # para id="MyID" - con . para class="myClass"
// $retorno["mensaje"] = $sql; return $retorno; exit();
?>