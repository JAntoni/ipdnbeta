var datatable_global;

$(document).ready(function () {
    $('#cmb_doc_activos').val(1);
    //POR DEFECTO CARGAR LOS DOCUMENTOS DEL ULTIMO PERIODO CONTABLE ACTIVO
    value_a_seleccionar = $('#cmb_periodo_id')[0][1].value;
    $("#cmb_periodo_id").val(`${value_a_seleccionar}`);
    
    ventacontadoc_tabla();

    $("#txt_fil_doc_cod").autocomplete({
        minLength: 1,
        source: VISTA_URL + "documentocontable/documentocontable_autocomplete.php",
        select: function (event, ui) {
            $('#txt_fil_doc_cod').val(ui.item.tb_documentocontable_cod +'-'+ ui.item.tb_documentocontable_desc);
            $("#hdd_fil_doc_cod").val(ui.item.tb_documentocontable_cod);
            ventacontadoc_tabla();
            event.preventDefault();
            $('#txt_fil_doc_cod').focus();
        }
    });

    $("#txt_fil_cliente_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            $('#hdd_fil_cliente_id').val(ui.item.cliente_id);
            $('#txt_fil_cliente_nom').val(ui.item.cliente_nom);
            ventacontadoc_tabla();
            event.preventDefault();
            $('#txt_fil_cliente_nom').focus();
        }
    });

    $("#txt_fil_doc_cod, #txt_fil_cliente_nom").keyup(function () {
        if ($(`#`+ this.id).val() === '') {
            $(`#` + $('#'+this.id)[0].nextElementSibling.id).val('');
            ventacontadoc_tabla();
        }
    });

    $('#cmb_periodo_id, #cmb_doc_activos, #cmb_verificar_docs, #cmb_datos').change(function () {
        ventacontadoc_tabla();
    });
});

function ventacontadoc_tabla() {
    $.ajax({
      type: "POST",
      url: VISTA_URL+"ventacontadoc/ventacontadoc_tabla.php",
      async: true,
      dataType: "html",
      data: ({
        cliente_id : $("#hdd_fil_cliente_id").val(),
        doc_cod : $("#hdd_fil_doc_cod").val(),
        mesconta_id : $('#cmb_periodo_id').val(),
        doc_activos : $('#cmb_doc_activos').val(),
        verificar_docs : $('#cmb_verificar_docs').val(),
        interno_externo: $('#cmb_datos').val()
      }),
      beforeSend: function() {
        $('#ventacontadoc_mensaje_tbl').show(300);
      },
      success: function(data){
        $('#div_ventacontadoc_tabla').html(data);
        $('#ventacontadoc_mensaje_tbl').hide(300);
      },
      complete: function(data){
        estilos_datatable();
      },
      error: function(data){
        $('#ventacontadoc_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
      }
    });
}

function estilos_datatable() {
    datatable_global = $('#tbl_ventacontadocs').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [3, 4, 5, 6], orderable: false}
        ]
    });
    datatable_texto_filtrar();
}

function datatable_texto_filtrar() {
    $('.dataTables_filter input')
    .attr('id', 'txt_datatable_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function ventacontadoc_form(usuario_act, reg) {
    var ventacontadoc_id = reg.tb_ventacontadoc_id;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ventacontadoc/ventacontadoc_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            ventacontadoc_id: ventacontadoc_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_ventacontadoc_form').html(data);
                $('#modal_registro_ventacontadoc').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_ventacontadoc'); //funcion encontrada en public/js/generales.js
            }
            else {
                //llamar al formulario de solicitar permiso
                var modulo = 'ventacontadoc';
                var div = 'div_modal_ventacontadoc_form';
                permiso_solicitud(usuario_act, ventacontadoc_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
            modal_hidden_bs_modal('modal_registro_ventacontadoc', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_registro_ventacontadoc', 65);
            modal_height_auto('modal_registro_ventacontadoc');
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

function ventacontadoc_historial_form(ventacontadoc_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ventacontadoc/ventacontadoc_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
                ventacontadoc_id:ventacontadoc_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_ventacontadoc_historial_form').html(html);
            $('#div_modal_ventacontadoc_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_ventacontadoc_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_ventacontadoc_historial_form'); //funcion encontrada en public/js/generales.js

        },
        complete: function (html) {
//                    console.log(html);
        }
    });
}