<?php
require_once('../../core/usuario_sesion.php');
require_once('../ventacontadoc/Ventacontadoc.class.php');
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
$oVentacontadoc = new Ventacontadoc();

$arr[0] = 'NO';
$arr[1] = 'SI';

$verificar_docs = intval($_POST['verificar_docs']);
$sql_to_verify = "";

if ($verificar_docs == 1) {
    //1: Docs. cuya serie externa no empieza con E, o su numeracion externa está vacía, o su fecha_emision externa es vacio o null
    $sql_to_verify = "(LEFT(vcd.tb_ventacontadoc_seriee,1) <> 'E' OR LEFT(vcd.tb_ventacontadoc_seriee,1) IS NULL OR
    vcd.tb_ventacontadoc_numeroe IS NULL OR vcd.tb_ventacontadoc_numeroe = '' OR
    vcd.tb_ventacontadoc_fechaemisione IS NULL OR VCD.tb_ventacontadoc_fechaemisione = '')";
}

$value_xac = intval($_POST["doc_activos"]);
if ($value_xac == 0) {
    $value_xac = "0, 1";
} else if ($value_xac == 2) {
    $value_xac = 0;
}

//$column_name, $param.$i, $datatype
$parametros[0]['param0'] = "vcd.tb_ventacontadoc_xac IN ($value_xac)";

$parametros[1]['column_name'] = 'tb_cliente_idfk';
$parametros[1]['param1'] = intval($_POST['cliente_id']);
$parametros[1]['datatype'] = 'INT';

$parametros[2]['column_name'] = 'tb_documentocontable_codfk';
$parametros[2]['param2'] = $_POST['doc_cod'];
$parametros[2]['datatype'] = 'STR';

$parametros[3]['column_name'] = 'tb_mesconta_idfk';
$parametros[3]['param3'] = intval($_POST['mesconta_id']);
$parametros[3]['datatype'] = 'INT';

$parametros[4]['param4'] = $sql_to_verify;

for ($i = 0; $i < count($parametros); $i++) {
    if (empty($parametros[$i]['param' . $i])) {
        $parametros[$i] = array();
    }
}

$otros_param["orden"]["column_name"] = "vcd.tb_ventacontadoc_fecreg";
$otros_param["orden"]["value"] = "DESC"; //fecha de registro desde el más actual, hasta el más antiguo

$result = $oVentacontadoc->listar_todos($parametros, $otros_param);

$letra = (intval($_POST["interno_externo"]) == 0) ? "i" : "e"; //intero o externo
$sunat = (intval($_POST["interno_externo"]) == 0) ? "" : "sunat"; //intero o externo
//echo var_export($result["data"]);exit();

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $tipo_doc = (strlen($value['tb_documentocontable_desc'])>30) ? substr($value['tb_documentocontable_desc'], 0, 30)."[...]" : $value['tb_documentocontable_desc'];
        $tr .= '<tr id="tabla_cabecera_fila">';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_ventacontadoc_id'] . '</td>
          <td id="tabla_fila">'.$tipo_doc.'</td>
          <td id="tabla_fila">'.$value['tb_ventacontadoc_serie'.$letra].'-'.$value['tb_ventacontadoc_numero'.$letra].'</td>
          <td id="tabla_fila">'.$value['tb_mesconta_mes'].'-'.$value['tb_mesconta_anio'].'</td>
          <td id="tabla_fila">'. mostrar_fecha($value['tb_ventacontadoc_fechaemision'.$letra]) .'</td>
          <td id="tabla_fila">'. $value['tb_moneda_nom'] .'</td>
          <td id="tabla_fila">'. mostrar_moneda($value['tb_ventacontadoc_montototal'.$letra]) .'</td>
          <td id="tabla_fila">'. $value['tipocambio_venta'.$sunat] .'</td>
          <td id="tabla_fila">'. $arr[intval($value['tb_ventacontadoc_afecta'])] .'</td>
          <td id="tabla_fila">'.$value['tb_cliente_nom'].'</td>
          <td id="tabla_fila" align="center">
            <button class="btn btn-info btn-xs" title="Ver"' . " onclick='ventacontadoc_form(\"L\",".json_encode($value). ")'>".'<i class="fa fa-eye"></i></button>
            <button class="btn btn-warning btn-xs" title="Editar"' . " onclick='ventacontadoc_form(\"M\",".json_encode($value). ")'>".'<i class="fa fa-edit"></i></button>
            <button class="btn btn-danger btn-xs" title="Eliminar"' . " onclick='ventacontadoc_form(\"E\",".json_encode($value). ")'>".'<i class="fa fa-trash"></i></button>
            <button class="btn btn-primary btn-xs" title="Historial" onclick="ventacontadoc_historial_form(' . $value['tb_ventacontadoc_id'] . ')"><i class="fa fa-history"></i></button>
          </td>
        ';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}
?>
<table id="tbl_ventacontadocs" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID. Doc</th>
            <th id="tabla_cabecera_fila">Tipo Doc</th>
            <th id="tabla_cabecera_fila">Numeracion</th>
            <th id="tabla_cabecera_fila">Periodo Cont.</th>
            <th id="tabla_cabecera_fila">Emision</th>
            <th id="tabla_cabecera_fila">Moneda</th>
            <th id="tabla_cabecera_fila">Monto</th>
            <th id="tabla_cabecera_fila">Tipo Cambio</th>
            <th id="tabla_cabecera_fila">Afecta</th>
            <th id="tabla_cabecera_fila">Cliente</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>