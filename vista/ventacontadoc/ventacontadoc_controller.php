<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../ventacontadoc/Ventacontadoc.class.php');
$oVentacontadoc = new Ventacontadoc();
require_once('../mesconta/Mesconta.class.php');
$oMesconta = new Mesconta();
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
//require_once('../ventacontadetalle/Ventacontadetalle.class.php');

$usuario_id				= intval($_SESSION['usuario_id']);
$action					= $_POST['action'];
$ventacontadoc_id		= intval($_POST['hdd_ventacontadoc_id']);
$cliente_id				= intval($_POST['hdd_cliente_id']);
$doc_cod				= $_POST['txt_doc_cod'];
$ventacontadoc_seriei	= strtoupper($_POST['txt_doc_seriei']);
$ventacontadoc_numeroi	= $_POST['txt_doc_numeroi'];
$length_detalles		= intval($_POST['cantidad_filas_detalle']);

$oVentacontadoc->tb_ventacontadoc_usumod = $usuario_id;
$data['estado'] = 0;

if($action == 'eliminar'){
	$oVentacontadoc->tb_ventacontadoc_id = $ventacontadoc_id;
	
	$data['mensaje'] = 'Existe un error al eliminar el documento de compra contable.';

	if($oVentacontadoc->eliminar()){
		//eliminar o desactivar sus detalles
		// for($i = 0; $i < $length_detalles; $i++){
		// 	$oVentaDetalle = new Ventacontadetalle();
		// 	$oVentaDetalle->setTbVentacontadetalleId(intval($_POST['txt_detalleid_'. $i]));
		// 	$oVentaDetalle->setTbVentacontadetalleUsumod($usuario_id);
		// 	$oVentaDetalle->eliminar();
		// }

        $data['estado'] = 1;
        $data['mensaje'] = 'Se eliminó el Documento de venta y sus detalles';

        //insertar historial
        $oHist->setTbHistUsureg($usuario_id);
        $oHist->setTbHistNomTabla('tb_ventacontadoc');
        $oHist->setTbHistRegmodid($ventacontadoc_id);
        $oHist->setTbHistDet('Eliminó el documento de venta contable de numeración: '.$ventacontadoc_seriei.'-'.$ventacontadoc_numeroi);
        $oHist->insertar();
    }
}
else{
	$data['mensaje'] = 'Existe un error al '.$action.' el documento de venta.';

	///ESPACIO PARA VERIFICAR SI YA ESTA REGISTRADO EL DOCUMENTO . COMBINACION DEL CLIENTEID-DOCCOD-SERIE-NUMERO MOSTRARUNO
	$array[0]['column_name'] = 'tb_cliente_idfk';
    $array[0]['param0'] = $cliente_id;
    $array[0]['datatype'] = 'INT';

	$array[1]['column_name'] = 'tb_documentocontable_codfk';
    $array[1]['param1'] = $doc_cod;
    $array[1]['datatype'] = 'STR';

	$array[2]['column_name'] = 'tb_ventacontadoc_seriei';
    $array[2]['param2'] = $ventacontadoc_seriei;
    $array[2]['datatype'] = 'STR';

	$array[3]['column_name'] = 'tb_ventacontadoc_numeroi';
    $array[3]['param3'] = $ventacontadoc_numeroi;
    $array[3]['datatype'] = 'STR';

    $array1 = array();
	$result = $oVentacontadoc->mostrarUno($array, $array1);
	/*

	$existe = $result['estado'];
    $registro_coincide = $result['data'];
	$evaluar0 = $ventacontadoc_id == intval($registro_coincide['tb_ventacontadoc_id']);

	if(strpos($action,'_verificar_documento') && !$evaluar0){
		if($existe == 1){
			$data['estado'] = 1;
			$data['mensaje'] = 'El documento venta '.$ventacontadoc_seriei.'-'.$ventacontadoc_numeroi.' del cliente '.$registro_coincide['tb_cliente_nom'].' ya está registrado, con id: '.$registro_coincide['tb_ventacontadoc_id'];
		}
	}
	elseif($action == 'buscar_credito'){
		$tipo_credito_id = intval($_POST['tipo_credito_id']);
		$credito_id 	 = intval($_POST['credito_id']);
		$oCred = null;
	
		switch ($tipo_credito_id) {
			case 1:
				require_once('../creditomenor/Creditomenor.class.php');
				$oCred = new Creditomenor();
				break;
			case 2:
				require_once('../creditoasiveh/Creditoasiveh.class.php');
				$oCred = new Creditoasiveh();
				break;
			case 3:
				require_once('../creditogarveh/Creditogarveh.class.php');
				$oCred = new Creditogarveh();
				break;
			case 4:
				require_once('../creditohipo/Creditohipo.class.php');
				$oCred = new Creditohipo();
				break;
		}
	
		$data['estado'] = 0;
		$data['mensaje'] = 'El Credito ingresado no existe.';
		$data['data'] = '';
	
		$resultado = $oCred->mostrarUno($credito_id);
		if($resultado['estado'] == 1){
			$data['estado'] = 1;
			$data['mensaje'] = 'Credito encontrado';
			$data['data'] = $resultado['data'];
		}
	}
	//espacio para buscar mes contable aperturado o declarado
	// CODE
	else if($action == 'validar_mes_conta'){
		$mesconta_id = intval($_POST['cmb_mesconta_id']);

		// NO SE PERMITE MANIPULAR DOCUMENTOS QUE NO ESTEN EN MES CONTABLE NO APERTURADO, NI DECLARADO A SUNAT
		if (!empty($mesconta_id)){
			$arreglo[0]['column_name'] = 'tb_mesconta_id';
			$arreglo[0]['param0'] = $mesconta_id;
			$arreglo[0]['datatype'] = 'INT';

			$result = $oMesconta->mostrarUno($arreglo);

			if ($result['estado'] == 0) {
				//no está registrado el mes contable
				$data['estado'] = 3;
				$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está registrado. Verifique en modulo mes contable';
				echo json_encode($data);
				exit();
			} else {
				//está registrado. Si aperturado =0
				if (intval($result['data']['tb_mesconta_aperturado']) == 0) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está aperturado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				//sino si declarado=1
				else if (intval($result['data']['tb_mesconta_estadeclarado']) == 1) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' ya está declarado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				else if (empty($result['data']['tb_mesconta_ventacorrel'])){
					$data['estado'] = 3;
					$data['mensaje'] = 'Falta configurar el correlativo de ventas del mes ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . '. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				else{
					$data['estado'] = 4;
					$data['data'] = $result['data']['tb_mesconta_ventacorrel'];
					echo json_encode($data);
					exit();
				}
			}
		}
	}

	elseif($action == 'insertar' || $action == 'modificar'){
		$moneda_id		= intval($_POST['cmb_moneda_id']);
		$afecta			= intval($_POST['che_afecta']);
		$mesconta_id = intval($_POST['cmb_mesconta_id']);
		$correlativo = strtoupper($_POST['txt_ventaconta_correlativo']);

		if(empty($_POST['txt_tipcam_cont_tipcam']) ){
			$data['estado'] = 0;
			$data['mensaje'] = 'Verifique el registro del tipo de Cambio';
			echo json_encode($data);
			exit();
		}

		// NO SE PERMITE MANIPULAR DOCUMENTOS QUE NO ESTEN EN MES CONTABLE NO APERTURADO, NI DECLARADO A SUNAT
		if (!empty($mesconta_id)){
			$arreglo[0]['column_name'] = 'tb_mesconta_id';
			$arreglo[0]['param0'] = $mesconta_id;
			$arreglo[0]['datatype'] = 'INT';

			$result = $oMesconta->mostrarUno($arreglo);

			if ($result['estado'] == 0) {
				//no está registrado el mes contable
				$data['estado'] = 3;
				$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está registrado. Verifique en modulo mes contable';
				echo json_encode($data);
				exit();
			} else {
				//está registrado. Si aperturado =0
				if (intval($result['data']['tb_mesconta_aperturado']) == 0) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está aperturado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				//sino si declarado=1
				else if (intval($result['data']['tb_mesconta_estadeclarado']) == 1) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' .$result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' ya está declarado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				else if (empty($result['data']['tb_mesconta_ventacorrel'])){
					$data['estado'] = 3;
					$data['mensaje'] = 'Falta configurar el correlativo de ventas del mes ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . '. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
			}
		}
		else{
			$data['estado'] = 0;
			$data['mensaje'] = 'Verifique el periodo contable seleccionado';
			echo json_encode($data);
			exit();
		}

		$oVentacontadoc->setTbClienteIdfk($cliente_id);
		$oVentacontadoc->setTbVentacontadocCodfk($doc_cod);
		$fech_venc = ($doc_cod == '14') ? "" : fecha_mysql($_POST['ventacontadoc_emi_picker']);
		$oVentacontadoc->setTbVentacontadocSerie($ventacontadoc_seriei);
		$oVentacontadoc->setTbVentacontadocNumero($ventacontadoc_numeroi);
		$oVentacontadoc->setTbVentacontadocFechaemision(fecha_mysql($_POST['ventacontadoc_emi_picker']));
		$oVentacontadoc->setTbVentacontadocFechavencimiento($fech_venc);
		$oVentacontadoc->setTbVentacontadocCantdiasvenc(0);
		$oVentacontadoc->setTbMescontaIdfk($mesconta_id);
		$oVentacontadoc->setTbMonedaIdfk($moneda_id);
		$oVentacontadoc->setTbVentacontadocAfecta($afecta);
		$porc_igv = $_POST['txt_porc_igv'] == '' ? 0.0 : floatval(intval($_POST['txt_porc_igv']) / 100);
		$oVentacontadoc->setTbVentacontadocPorcentajeigv($porc_igv);

		$oVentacontadoc->setTbVentacontadocMontototal(moneda_mysql($_POST['txt_importe_total']));
		$oVentacontadoc->setTbVentacontadocMontoigv(moneda_mysql($_POST['txt_monto_igv_red']));
		$oVentacontadoc->setTbVentacontadocDetr(intval($_POST['che_detr']));
		if ($moneda_id == 2) {
			$oVentacontadoc->setTbMonedacambiocontableIdfk(intval($_POST['hdd_monedacambiocontable_idfk']));
			$oVentacontadoc->setTbVentacontadocBasesoles(moneda_mysql($_POST['txt_monto_base_sol_red']));
			$oVentacontadoc->setTbVentacontadocMontoigvsoles(moneda_mysql($_POST['txt_monto_igv_sol_red']));
			$oVentacontadoc->setTbVentacontadocMontosoles(moneda_mysql($_POST['txt_importe_total_sol_red']));
		} else {
			$oVentacontadoc->setTbVentacontadocBasesoles(moneda_mysql($_POST['txt_monto_base_red']));
			$oVentacontadoc->setTbVentacontadocMontoigvsoles(moneda_mysql($_POST['txt_monto_igv_red']));
			$oVentacontadoc->setTbVentacontadocMontosoles(moneda_mysql($_POST['txt_importe_total']));
		}
		if ($afecta == 1) {
			$oVentacontadoc->setTbVentacontadocBaseafecta(moneda_mysql($_POST['txt_monto_base_red']));
			$oVentacontadoc->setTbVentacontadocBaseinafecta(moneda_mysql(0));

			if (floatval($oVentacontadoc->getTbVentacontadocMontosoles()) >= 700 && intval($_POST['che_detr']) == 1) {
				$oVentacontadoc->setTbVentacontadocConstdetr(strtoupper($_POST['txt_const_detraccion']));
				$fecha_detr = empty($_POST['ventacontadoc_detraccion_picker']) ? '0000-00-00' : fecha_mysql($_POST['ventacontadoc_detraccion_picker']);
				$oVentacontadoc->setTbVentacontadocFechadetr($fecha_detr);
			}
		} else {
			$oVentacontadoc->setTbVentacontadocBaseafecta(moneda_mysql(0));
			$oVentacontadoc->setTbVentacontadocBaseinafecta(moneda_mysql($_POST['txt_monto_base_red']));
		}
		if ($doc_cod == '07') {
			if (intval($_POST['txt_docref_clienteid']) != $cliente_id) {
				$data['mensaje'] = '¡Verifique! El cliente del documento referido y de la nota de credito son diferentes';
				$data['data'] = '';
				echo json_encode($data);
				exit();
			} else {
				$oVentacontadoc->setTbVentacontadocDocrefidfk(intval($_POST['txt_docrefidfk']));
				$oVentacontadoc->setTbVentacontadocMotivonc(strtoupper($_POST['txt_motivo_nc']));
			}
		}
		if (intval($_POST['che_anexa_credito']) == 1) {
			$oVentacontadoc->setTbCreditotipoIdfk(intval($_POST['cmb_creditotipo_id']));
			$oVentacontadoc->setTbCreditoIdfk(intval($_POST['txt_credito_id']));
		}
		$oVentacontadoc->setTbVentacontadocDetalle(strtoupper($_POST['txt_comentario']));
		$oVentacontadoc->setTbVentacontadocCorrelativo($correlativo);

		$prox_correlativo_parte1 = substr($correlativo, 0, -4);
		$prox_correlativo_parte2 = intval(substr($correlativo, -4))+1;//devuelve 7. 17. 117. 1117
		$cantidad_falta = 4 - strlen($prox_correlativo_parte2);

		$contador_while = 0; $relleno = '';
		while($contador_while < $cantidad_falta){
			$relleno .= "0";
			$contador_while++;
		}
		$prox_correlativo = $prox_correlativo_parte1 . $relleno . $prox_correlativo_parte2;

		if ($action == 'insertar') {
			if ($existe == 0) {
				$oVentacontadoc->setTbVentacontadocUsureg($usuario_id);
				$res = $oVentacontadoc->insertar();
				
				if ($res['estado'] == 1) {
					$data['estado'] = 1;
					$data['mensaje'] = $res['mensaje'] . '. Su ID: ' . $res['nuevo'];
					$data['nuevo'] = $res['nuevo'];
					
					// aqui viene la inserción de los detalles en la nueva tabla de detalle
					for($i = 0; $i < $length_detalles; $i++){
						//uno a uno los detalles
						$oVentaDetalle = new Ventacontadetalle();
						$oVentaDetalle->setTbCuentacontaCod($_POST['txt_codcuentacont_'.$i]);
						$glosa = empty(strtoupper($_POST['txt_glosa_'.$i])) ? null : strtoupper($_POST['txt_glosa_'.$i]);			
						$oVentaDetalle->setTbVentacontadetalleGlosa($glosa);
						$oVentaDetalle->setTbVentacontadetalleMonto(moneda_mysql($_POST['txt_monto_'.$i]));
						$empresaidfk = intval($_POST['cbo_sede_'.$i]) == 0 ? null : intval($_POST['cbo_sede_'.$i]);
						$oVentaDetalle->setTbEmpresaIdfk( $empresaidfk );
						$oVentaDetalle->setTbVentacontadocIdfk(intval($data['nuevo']));
						$oVentaDetalle->setTbVentacontadetalleOrden(intval($_POST['txt_detalleorden_'.$i]));
						$oVentaDetalle->setTbVentacontadetalleUsureg($usuario_id);
						$oVentaDetalle->setTbVentacontadetalleUsumod($usuario_id);
						$oVentaDetalle->insertar();
					}
					//insertar historial
					$oHist->setTbHistUsureg($usuario_id);
					$oHist->setTbHistNomTabla('tb_ventacontadoc');
					$oHist->setTbHistRegmodid($data['nuevo']);
					$oHist->setTbHistDet('Registró el documento de Venta. ID: ' . $data['nuevo'] . '. PROVEEDOR: '. strtoupper($_POST['txt_cliente_nom']) .'. NUMERACION: ' . $ventacontadoc_seriei . '-' . $ventacontadoc_numeroi);
					$oHist->insertar();

					//ACTUALIZAR EL CORRELATIVO EN TABLA MES_CONTA
					$array_update_correlativo[0]['column_name'] = 'tb_mesconta_ventacorrel';
					$array_update_correlativo[0]['param0'] = $prox_correlativo;
					$array_update_correlativo[0]['datatype'] = 'STR';

					$array_update_correlativo[1]['column_name'] = 'tb_mesconta_id';
					$array_update_correlativo[1]['param1'] = $mesconta_id;
					$array_update_correlativo[1]['datatype'] = 'INT';

					$oMesconta->actualizar_columna($array_update_correlativo);
				}
			} else {
				$data['mensaje'] = 'El documento venta ' . $ventacontadoc_seriei . '-' . $ventacontadoc_numeroi . ' del cliente ' . $registro_coincide['tb_cliente_nom'] . ' ya está registrado, con id: ' . $registro_coincide['tb_ventacontadoc_id'];
			}
		}
		else{
			// LA ACCION ES MODIFICAR
			$registro_origin = (array) json_decode($_POST['hdd_ventacontadoc_registro']);

			$evaluar[1] = $cliente_id == intval($registro_origin["tb_cliente_idfk"]);
			$evaluar[2] = $doc_cod == $registro_origin["tb_documentocontable_codfk"];
			$evaluar[3] = $ventacontadoc_seriei == strtoupper($registro_origin["tb_ventacontadoc_serie"]);
			$evaluar[4] = $ventacontadoc_numeroi == $registro_origin["tb_ventacontadoc_numero"];
			$evaluar[5] = fecha_mysql($_POST['ventacontadoc_emi_picker']) == $registro_origin["tb_ventacontadoc_fechaemision"];
			// CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
			// $evaluar[6] = fecha_mysql($_POST['ventacontadoc_venc_picker']) == $registro_origin["tb_ventacontadoc_fechavencimiento"];
			// $evaluar[7] = intval($_POST['txt_dias_venc']) == intval($registro_origin["tb_ventacontadoc_cantdiasvenc"]);
			$evaluar[6] = $moneda_id == intval($registro_origin["tb_moneda_idfk"]);
			$evaluar[7] = $_POST['hdd_monedacambiocontable_idfk'] == $registro_origin["tb_monedacambiocontable_idfk"];
			$evaluar[8] = moneda_mysql($_POST['txt_importe_total']) == moneda_mysql($registro_origin["tb_ventacontadoc_montototal"]);
			$evaluar[9] = floatval($oVentacontadoc->getTbVentacontadocMontosoles()) == floatval($registro_origin["tb_ventacontadoc_montosoles"]);
			$evaluar[10] = intval($oVentacontadoc->getTbVentacontadocAfecta()) == intval($registro_origin["tb_ventacontadoc_afecta"]);
			$evaluar[11] = floatval($oVentacontadoc->getTbVentacontadocPorcentajeigv()) == floatval($registro_origin["tb_ventacontadoc_porcentajeigv"]);
			$evaluar[12] = floatval($oVentacontadoc->getTbVentacontadocBaseinafecta()) == floatval($registro_origin["tb_ventacontadoc_baseinafecta"]);
			$evaluar[13] = floatval($oVentacontadoc->getTbVentacontadocBaseafecta()) == floatval($registro_origin["tb_ventacontadoc_baseafecta"]);
			$evaluar[14] = floatval($oVentacontadoc->getTbVentacontadocBasesoles()) == floatval($registro_origin["tb_ventacontadoc_basesoles"]);
			$evaluar[15] = floatval($oVentacontadoc->getTbVentacontadocMontoigv()) == floatval($registro_origin["tb_ventacontadoc_montoigv"]);
			$evaluar[16] = floatval($oVentacontadoc->getTbVentacontadocMontoigvsoles()) == floatval($registro_origin["tb_ventacontadoc_montoigvsoles"]);
			$evaluar[17] = $oVentacontadoc->getTbVentacontadocDocrefidfk() == $registro_origin["tb_ventacontadoc_docrefidfk"];
			$evaluar[18] = $oVentacontadoc->getTbVentacontadocMotivonc() == $registro_origin["tb_ventacontadoc_motivonc"];
			$evaluar[19] = $oVentacontadoc->getTbVentacontadocConstdetr() == $registro_origin["tb_ventacontadoc_constdetr"];
			$evaluar[20] = $oVentacontadoc->getTbVentacontadocFechadetr() == $registro_origin["tb_ventacontadoc_fechadetr"];
			$evaluar[21] = $oVentacontadoc->getTbCreditotipoIdfk() == $registro_origin["tb_creditotipo_idfk"];
			$evaluar[22] = $oVentacontadoc->getTbCreditoIdfk() == $registro_origin["tb_credito_idfk"];
			$evaluar[23] = $oVentacontadoc->getTbVentacontadocDetalle() == strtoupper($registro_origin["tb_ventacontadoc_detalle"]);
			$evaluar[24] = $oVentacontadoc->getTbMescontaIdfk() == $registro_origin["tb_mesconta_idfk"];
			$evaluar[25] = intval($oVentacontadoc->getTbVentacontadocDetr()) == intval($registro_origin["tb_ventacontadoc_detr"]);
			$evaluar[26] = $correlativo == strtoupper($registro_origin["tb_ventacontadoc_correlativo"]);

			$son_iguales = true;
			$i = 1;
			while($i<count($evaluar)+1){
				if(!$evaluar[$i]){
					$son_iguales = false;
				}
				$i++;
			}

			$detalles_origin = (array) json_decode($_POST['hdd_ventacontadetalle_array']);
			$detalle_eliminado = array();
			$detalle_original_mod = array();
			$fila_corresponde = array();
			$evaluar_detalle = array();
			$contador = 0;
			while($contador < count($detalles_origin)){
				$detalles_origin[$contador] = (array) $detalles_origin[$contador];
				$contador++;
				$detalle_eliminado[$contador] = 1; //está eliminado
				$detalle_original_mod[$contador] = 0;
				$fila_corresponde[$contador] = null;
			}

			$mensaje = '';
			for($i = 0 ; $i < count($detalles_origin); $i++){
				$contador = 0;
				for($j = 0 ; $j < $length_detalles ; $j ++){
					if(intval($detalles_origin[$i]['tb_ventacontadetalle_id']) == intval($_POST['txt_detalleid_'.$j])){
						$detalle_eliminado[$i+1] = 0; //no se eliminó el detalle
						$fila_corresponde[$i+1] = $j; //fila donde esta el registro

						$evaluar_detalle[$i+1][0] = intval($detalles_origin[$i]['tb_cuentaconta_cod'] == $_POST['txt_codcuentacont_' . $j]);
						$evaluar_detalle[$i+1][1] = intval($detalles_origin[$i]['tb_ventacontadetalle_glosa'] == strtoupper($_POST['txt_glosa_' . $j]));
						$evaluar_detalle[$i+1][2] = intval($detalles_origin[$i]['tb_ventacontadetalle_monto'] == moneda_mysql($_POST['txt_monto_' . $j]));
						$evaluar_detalle[$i+1][3] = intval(intval($detalles_origin[$i]['tb_empresa_idfk']) == intval($_POST['cbo_sede_' . $j]));
						$evaluar_detalle[$i+1][4] = intval(intval($detalles_origin[$i]['tb_ventacontadetalle_orden']) == intval($_POST['txt_detalleorden_' . $j]));
						//guardar 1 o 0 para saber si está modificado
						//guardar el numero j para saber cual es la fila donde se modificaron los datos
						while($contador< count($evaluar_detalle[$i+1])){
							if(!$evaluar_detalle[$i+1][$contador]){
								$detalle_original_mod[$i+1] = 1;
							}
							$contador++;
						}
						break;
					}
				}
			}

			$detalles_son_iguales = true;
			if(count($detalles_origin) != $length_detalles){
				$detalles_son_iguales = false;
			}
			$i = 1;
			while($i<count($detalles_origin)+1){
				if($detalle_eliminado[$i] == 1 || $detalle_original_mod[$i] == 1){
					$detalles_son_iguales = false;
				}
				$i++;
			}
			
			if ($son_iguales && $detalles_son_iguales) {
				$data['estado'] = 2;
				$data['mensaje'] = "No se realizó ninguna modificacion";
			}
			else if ($existe == 0 || ($existe == 1 && $evaluar0)) {
				$ARRAY[0] = 'NO';
				$ARRAY[1] = 'SI';
				$moneda[1] = 'S/.';
				$moneda[2] = 'US$';
				$tipocredito[1] = 'CRÉDITO MENOR';
				$tipocredito[2] = 'CRÉDITO ASCVEH';
				$tipocredito[3] = 'CRÉDITO GARVEH';
				$tipocredito[4] = 'CREDITO HIPOTECARIO';
				$oVentacontadoc->setTbVentacontadocId($ventacontadoc_id);
				if(!$son_iguales){
					$res = $oVentacontadoc->modificar();
				}
				//ESPACIO PARA MODIFICAR LOS DETALLES
				$mensaje_detalles = '';
				if(!$detalles_son_iguales){
					//añadir todos los nuevos detalles
					for($i = 0 ; $i < $length_detalles ; $i++){
						if (intval($_POST['txt_detalleid_' . $i]) == 0) {
							//uno a uno los detalles
							$oVentaDetalle = new Ventacontadetalle();
							$oVentaDetalle->setTbCuentacontaCod($_POST['txt_codcuentacont_' . $i]);
							$glosa = empty(strtoupper($_POST['txt_glosa_' . $i])) ? null : strtoupper($_POST['txt_glosa_' . $i]);
							$oVentaDetalle->setTbVentacontadetalleGlosa($glosa);
							$oVentaDetalle->setTbVentacontadetalleMonto(moneda_mysql($_POST['txt_monto_' . $i]));
							$empresaidfk = intval($_POST['cbo_sede_' . $i]) == 0 ? null : intval($_POST['cbo_sede_' . $i]);
							$oVentaDetalle->setTbEmpresaIdfk($empresaidfk);
							$oVentaDetalle->setTbVentacontadocIdfk($ventacontadoc_id);
							$oVentaDetalle->setTbVentacontadetalleOrden(intval($_POST['txt_detalleorden_' . $i]));
							$oVentaDetalle->setTbVentacontadetalleUsureg($usuario_id);
							$oVentaDetalle->setTbVentacontadetalleUsumod($usuario_id);
							$oVentaDetalle->insertar();

							$mensaje_detalles .= '<br> - Añadió el detalle de venta nro. '. intval($_POST["txt_detalleorden_{$i}"]) . ': '. $_POST["txt_codcuentacont_{$i}"] . '. ' . strtoupper($_POST["txt_glosa_{$i}"]).'. '. $_POST["txt_monto_{$i}"] . '. '. $_POST["txt_sedenombre_{$i}"].'.';
						}
					}

					//ahora a verificar las variables $detalle_eliminado, fila_corresponde, detalle_original_mod
					for($i = 1 ; $i < count($detalles_origin)+1; $i ++){
						if($detalle_eliminado[$i]){
							$oVentaDetalle = new Ventacontadetalle();
							$oVentaDetalle->setTbVentacontadetalleUsumod($usuario_id);
							$oVentaDetalle->setTbVentacontadetalleId($detalles_origin[($i-1)]['tb_ventacontadetalle_id']);
							$oVentaDetalle->eliminar();
							$mensaje_detalles .= '<br> - Eliminó el detalle de venta: '. $detalles_origin[($i-1)]['tb_cuentaconta_cod'].'. '. $detalles_origin[($i-1)]['tb_ventacontadetalle_glosa'].'. '. $detalles_origin[($i-1)]['tb_ventacontadetalle_monto'].'.';
						}				

						if($detalle_original_mod[$i]){
							$oVentaDetalle = new Ventacontadetalle();
							$oVentaDetalle->setTbVentacontadetalleUsumod($usuario_id);
							$oVentaDetalle->setTbVentacontadetalleId(intval($_POST['txt_detalleid_'.$fila_corresponde[$i]]));
							$oVentaDetalle->setTbCuentacontaCod($_POST['txt_codcuentacont_'.$fila_corresponde[$i]]);
							$glosa = empty(strtoupper($_POST['txt_glosa_'.$fila_corresponde[$i]])) ? null : strtoupper($_POST['txt_glosa_'.$fila_corresponde[$i]]);
							$oVentaDetalle->setTbVentacontadetalleGlosa($glosa);
							$oVentaDetalle->setTbVentacontadetalleMonto(moneda_mysql($_POST['txt_monto_'.$fila_corresponde[$i]]));
							$empresaidfk = intval($_POST['cbo_sede_'.$fila_corresponde[$i]]) == 0 ? null : intval($_POST['cbo_sede_'.$fila_corresponde[$i]]);
							$oVentaDetalle->setTbEmpresaIdfk($empresaidfk);
							$oVentaDetalle->setTbVentacontadetalleOrden(intval($_POST['txt_detalleorden_'.$fila_corresponde[$i]]));
							$oVentaDetalle->modificar();
							
							if($evaluar_detalle[$i][0] != 1 || $evaluar_detalle[$i][1] != 1 || $evaluar_detalle[$i][2] != 1 || $evaluar_detalle[$i][3] != 1)
								$mensaje_detalles .= '<br> - Modificó el detalle '.intval($_POST['txt_detalleorden_'.$fila_corresponde[$i]]).':';
							if($evaluar_detalle[$i][0] != 1)
								$mensaje_detalles .= ' Cod C.C.: ' . $detalles_origin[$i-1]['tb_cuentaconta_cod'] . ' => <b>'. $_POST['txt_codcuentacont_'.$fila_corresponde[$i]].'</b>.';
							if($evaluar_detalle[$i][1] != 1)
								$mensaje_detalles .= ' Glosa: ' . $detalles_origin[$i-1]['tb_ventacontadetalle_glosa'] . ' => <b>'. strtoupper($_POST['txt_glosa_'.$fila_corresponde[$i]]).'</b>.';
							if($evaluar_detalle[$i][2] != 1)
								$mensaje_detalles .= ' Monto: ' . mostrar_moneda($detalles_origin[$i-1]['tb_ventacontadetalle_monto']) . ' => <b>'. $_POST['txt_monto_'.$fila_corresponde[$i]] .'</b>.';
							if($evaluar_detalle[$i][3] != 1)
								$mensaje_detalles .= ' Empresa: ' . $detalles_origin[$i-1]['tb_empresa_nomcom'] . ' => <b>'. strtoupper($_POST['txt_sedenombre_'.$fila_corresponde[$i]]) .'</b>.';
							// if($evaluar_detalle[$i][4] == 1)
							// 	$mensaje_detalles .= '<br>   - Nro Orden: ';
						}
					}
				}

				if ($res == 1 || !$detalles_son_iguales) {
					$data['estado'] = 1;
					$data['mensaje'] = "Se modificó correctamente";

					//insertar historial
					$oHist->setTbHistUsureg($usuario_id);
					$oHist->setTbHistNomTabla('tb_ventacontadoc');
					$oHist->setTbHistRegmodid($ventacontadoc_id);
					$mensaje = 'Modificó el documento de venta con id ' . $ventacontadoc_id . ':';
					if (!$evaluar[1])
						$mensaje .= '<br> - Cambió el cliente: ' . $registro_origin['tb_cliente_nom'] . ' - ID:'.$registro_origin['tb_cliente_idfk'] . ' => <b>' . strtoupper($_POST['txt_cliente_nom']) . ' - ID:'.$cliente_id . '</b>';
					if (!$evaluar[2])
						$mensaje .= '<br> - Cambió el tipo de documento: ' . $registro_origin['tb_documentocontable_codfk'].' - '. $registro_origin['tb_documentocontable_desc'] . ' => <b>' . $oVentacontadoc->getTbVentacontadocCodfk() .' - '. strtoupper($_POST['txt_doc_nom']) . '</b>';
					if (!$evaluar[3] || !$evaluar[4])
						$mensaje .= '<br> - Cambió la numeración: ' . $registro_origin['tb_ventacontadoc_serie'].'-'. $registro_origin['tb_ventacontadoc_numero'] . ' => <b>' . $ventacontadoc_seriei.'-'.$ventacontadoc_numeroi . '</b>';
					if (!$evaluar[5])
						$mensaje .= '<br> - Cambió la fecha emisión: ' . mostrar_fecha($registro_origin['tb_ventacontadoc_fechaemision']) . ' => <b>' . $_POST['ventacontadoc_emi_picker'] . '</b>';
					if (!$evaluar[24])
						$mensaje .= '<br> - Cambió el periodo contable: ' . $registro_origin['tb_mesconta_mes'].'-'.$registro_origin['tb_mesconta_anio'] . ' => <b>' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . '</b>';
					if (!$evaluar[26])
						$mensaje .= '<br> - Cambió el correlativo: ' . $registro_origin['tb_ventacontadoc_correlativo'] . ' => <b>' . $correlativo . '</b>';
					// CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
					// if (!$evaluar[6])
					// 	$mensaje .= '<br> - Cambió la fecha vencim.: ' . mostrar_fecha($registro_origin['tb_ventacontadoc_fechavencimiento']) . ' => <b>' . $_POST['ventacontadoc_venc_picker'] . '</b>';
					// if (!$evaluar[7])
					// 	$mensaje .= '<br> - Cambió la cantidad de días para vencimiento: ' . $registro_origin['tb_ventacontadoc_cantdiasvenc'] . ' => <b>' . $_POST['txt_dias_venc'] . '</b>';
					if (!$evaluar[6])
						$mensaje .= '<br> - Cambió la moneda: ' . $registro_origin['tb_moneda_nom'] . ' => <b>' . $moneda[$moneda_id] . '</b>';
					if (!$evaluar[7])
						$mensaje .= '<br> - Cambió el tipo cambio: ' . $registro_origin['tipocambio_ventasunat'] . ' => <b>' . $_POST['txt_tipcam_cont_tipcam'] . '</b>';
					if (!$evaluar[8])
						$mensaje .= '<br> - Cambió el monto de importe total: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_ventacontadoc_montototal']) . ' => <b>' . $moneda[$moneda_id]. $_POST['txt_importe_total'] . '</b>';
					if (!$evaluar[9])
						$mensaje .= '<br> - Cambió el monto de importe total en soles: ' . $moneda[1]. mostrar_moneda($registro_origin['tb_ventacontadoc_montosoles']) . ' => <b>' . $moneda[1].mostrar_moneda($oVentacontadoc->getTbVentacontadocMontosoles()) . '</b>';
					if (!$evaluar[10])
						$mensaje .= '<br> - Cambió el estado de Afecta: ' . $ARRAY[intval($registro_origin['tb_ventacontadoc_afecta'])] . ' => <b>' . $ARRAY[$afecta] . '</b>';
					if (!$evaluar[11])
						$mensaje .= '<br> - Cambió el porcentaje de IGV: ' . $registro_origin['tb_ventacontadoc_porcentajeigv'] . ' => <b>' . $oVentacontadoc->getTbVentacontadocPorcentajeigv() . '</b>';
					if (!$evaluar[12])
						$mensaje .= '<br> - Cambió el monto base inafecta: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_ventacontadoc_baseinafecta']) . ' => <b>' .  $moneda[$moneda_id].mostrar_moneda($oVentacontadoc->getTbVentacontadocBaseinafecta()) . '</b>';
					if (!$evaluar[13])
						$mensaje .= '<br> - Cambió el monto base afecta: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_ventacontadoc_baseafecta']) . ' => <b>' . $moneda[$moneda_id].mostrar_moneda($oVentacontadoc->getTbVentacontadocBaseafecta()) . '</b>';
					if (!$evaluar[14])
						$mensaje .= '<br> - Cambió el monto base en soles: ' . $moneda[1].mostrar_moneda($registro_origin['tb_ventacontadoc_basesoles']) . ' => <b>' . $moneda[1].mostrar_moneda($oVentacontadoc->getTbVentacontadocBasesoles()) . '</b>';
					if (!$evaluar[15])
						$mensaje .= '<br> - Cambió el monto de igv: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_ventacontadoc_montoigv']) . ' => <b>' . $moneda[$moneda_id].mostrar_moneda($oVentacontadoc->getTbVentacontadocMontoigv()) . '</b>';
					if (!$evaluar[16])
						$mensaje .= '<br> - Cambió el monto de igv en soles: ' . $moneda[1].mostrar_moneda($registro_origin['tb_ventacontadoc_montoigvsoles']) . ' => <b>' . $moneda[1].mostrar_moneda($oVentacontadoc->getTbVentacontadocMontoigvsoles()) . '</b>';
					if (!$evaluar[17])
						$mensaje .= '<br> - Cambió el documento referido en la NC: ' . $registro_origin['docreferido_serie'].'-'.$registro_origin['docreferido_numero'] . ' => <b>' . $_POST['txt_documento_ref'] . '</b>';
					if (!$evaluar[18])
						$mensaje .= '<br> - Cambió el motivo de la NC: \'' . $registro_origin['tb_ventacontadoc_motivonc'] . '\' => \'<b>' . $oVentacontadoc->getTbVentacontadocMotivonc() . '</b>\'';
					if (!$evaluar[25])
						$mensaje .= '<br> - Cambió el estado de detraccion: \'' . $ARRAY[intval($registro_origin["tb_ventacontadoc_detr"])] . '\' => \'<b>' . $ARRAY[intval($oVentacontadoc->getTbVentacontadocDetr())] . '</b>\'';
					if (!$evaluar[19])
						$mensaje .= '<br> - Cambió la constancia de detracción: ' . $registro_origin['tb_ventacontadoc_constdetr'] . ' => <b>' . $oVentacontadoc->getTbVentacontadocConstdetr() . '</b>';
					if (!$evaluar[20])
						$mensaje .= '<br> - Cambió la fecha de detracción: ' . mostrar_fecha($registro_origin['tb_ventacontadoc_fechadetr']) . ' => <b>' . $_POST['ventacontadoc_detraccion_picker'] . '</b>';
					if (!$evaluar[21])
						$mensaje .= '<br> - Cambió el tipo de credito anexado: ' . $registro_origin['tb_creditotipo_nom']  . ' => <b>' . $tipocredito[intval($_POST['cmb_creditotipo_id'])] . '</b>';
					if (!$evaluar[22])
						$mensaje .= '<br> - Cambió el credito anexado: ' . $registro_origin['tb_credito_idfk'] . ' => <b>' . $oVentacontadoc->getTbCreditoIdfk() . '</b>';
					if (!$evaluar[23])
						$mensaje .= '<br> - Cambió el comentario: \'' . $registro_origin['tb_ventacontadoc_detalle'] . '\' => \'<b>' . $oVentacontadoc->getTbVentacontadocDetalle() . '</b>\'';
					if(!$detalles_son_iguales)
						$mensaje .= $mensaje_detalles;

					$oHist->setTbHistDet($mensaje);
					$oHist->insertar();

					//ACTUALIZAR EL CORRELATIVO EN TABLA MES_CONTA
					if (!$evaluar[26]) {
						$array_update_correlativo[0]['column_name'] = 'tb_mesconta_ventacorrel';
						$array_update_correlativo[0]['param0'] = $prox_correlativo;
						$array_update_correlativo[0]['datatype'] = 'STR';

						$array_update_correlativo[1]['column_name'] = 'tb_mesconta_id';
						$array_update_correlativo[1]['param1'] = $mesconta_id;
						$array_update_correlativo[1]['datatype'] = 'INT';

						$oMesconta->actualizar_columna($array_update_correlativo);
					}
				}
			} else {
				$data['mensaje'] = 'Los datos que ingresó ya están registrados, en el documento con id: ' . intval($registro_coincide['tb_ventacontadoc_id']);
			}
		}
	}
	else {
		$data['estado'] = 0;
		$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;
	}*/
}
echo json_encode($data);

/* $data['estado'] = 0;
$data['mensaje'] = 'MIRÁA WACHO, ES MESSI: '.intval($_POST['che_detr']);
$data['data'] = intval($_POST['che_detr']);
echo json_encode($data);
exit(); */


/* $data['estado'] = 0;
$data['mensaje'] = "";
echo json_encode($data); exit(); */