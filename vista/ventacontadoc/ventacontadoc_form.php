<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
} else {
  require_once('../../core/usuario_sesion.php');
}
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../ventacontadoc/Ventacontadoc.class.php');
$oVentacontadoc = new Ventacontadoc();
/* require_once('../ventacontadetalle/Ventacontadetalle.class.php');
$oVentaDetalle = new Ventacontadetalle(); */
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'ventacontadoc';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$ventacontadoc_id = $_POST['ventacontadoc_id'];

$titulo = '';
if ($usuario_action == 'I') {
  $titulo = 'Registrar Documento de Venta';
} elseif ($usuario_action == 'M') {
  $titulo = 'Editar Documento de Venta';
} elseif ($usuario_action == 'E') {
  $titulo = 'Eliminar Documento de Venta';
} elseif ($usuario_action == 'L') {
  $titulo = 'Documento de Venta Registrado';
} else {
  $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en ventacontadoc
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'ventacontadoc';
    $modulo_id = $ventacontadoc_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }

  //si la accion es modificar o eliminar, mostramos los datos del ventacontadoc por su ID
  if (intval($ventacontadoc_id) > 0) {
    $array[0]['column_name'] = 'tb_ventacontadoc_id';
    $array[0]['param0'] = $ventacontadoc_id;
    $array[0]['datatype'] = 'INT';

    $array1 = array(); // se configura cuando se necesitan más columnas para ver

    $result = $oVentacontadoc->mostrarUno($array, $array1);
    if ($result['estado'] != 1) {
      $mensaje =  'No se ha encontrado ningún registro para el documento de venta seleccionado, inténtelo nuevamente.';
      echo $mensaje;
      $bandera = 4;
    } else {
      $result = $result['data'];

      // todos los campos que van a ser mostrados

      $ventacontadoc_id    = $result['tb_ventacontadoc_id'];
      $cliente_id         = $result['tb_cliente_idfk'];
      $cliente_doc        = $result['tb_cliente_doc'];
      $cliente_nom        = $result['tb_cliente_nom'];
      $doc_cod              = $result['tb_documentocontable_codfk'];
      $doc_nom              = $result['tb_documentocontable_desc'];
      $seriei                = $result['tb_ventacontadoc_seriei'];
      $numeroi               = $result['tb_ventacontadoc_numeroi'];
      $condicion            = $result['tb_ventacontadoc_condicion'];
      $seriee                = $result['tb_ventacontadoc_seriee'];
      $numeroe               = $result['tb_ventacontadoc_numeroe'];
      $mesconta_id          = intval($result['tb_mesconta_idfk']);
      $fecha_emisioni        = mostrar_fecha($result['tb_ventacontadoc_fechaemisioni']);
      $fecha_emisione        = mostrar_fecha($result['tb_ventacontadoc_fechaemisione']);
      //$fecha_vencim         = mostrar_fecha($result['tb_ventacontadoc_fechavencimiento']);//CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
      //$dias_para_venc       = $result['tb_ventacontadoc_cantdiasvenc'];//CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
      $monedacambio_idfk    = $result['tb_monedacambio_idfk'];
      $monedacambiocontable_idfk = $result['tb_monedacambiocontable_idfk'];
      $tipo_cambioi          = number_format($result["tipocambio_venta"], 3);
      $tipo_cambioe          = number_format(floatval($result['tipocambio_ventasunat']), 3); //tipocambio_ventasunat
      $moneda_id            = intval($result['tb_moneda_idfk']);
      $afecta               = intval($result['tb_ventacontadoc_afecta']) == 1 ? 'checked' : '';
      $porcentaje_igv       = floatval($result['tb_ventacontadoc_porcentajeigv']) * 100;
      $importe_total        = mostrar_moneda($result['tb_ventacontadoc_montototal']);
      $importe_total_soles  = mostrar_moneda($result['tb_ventacontadoc_montosoles']);
      $monto_base           = $afecta == 'checked' ? mostrar_moneda($result['tb_ventacontadoc_baseafecta']) : mostrar_moneda($result['tb_ventacontadoc_baseinafecta']);
      $monto_base_soles     = mostrar_moneda($result['tb_ventacontadoc_basesoles']);
      $monto_igv            = mostrar_moneda($result['tb_ventacontadoc_montoigv']);
      $monto_igv_soles      = mostrar_moneda($result['tb_ventacontadoc_montoigvsoles']);
      $creditotipo_id       = (empty(intval($result['tb_creditotipo_idfk']))) ? 0 : intval($result['tb_creditotipo_idfk']);
      $anexa_credito        = ($creditotipo_id != 0) ? 'checked' : '';
      $credito_id           = $anexa_credito == 'checked' ? intval($result['tb_credito_idfk']) : '--';
      $credito_cliente_nombre = $result['tb_cliente_nom'];
      $credito_monto        = $result['credito_moneda'] . mostrar_moneda($result['tb_credito_preaco']);
      $docrefidfk           = intval($result['tb_ventacontadoc_docrefidfk']);
      $docref_clienteidfk = intval($result['docref_clienteidfk']);
      $motivo_nc            = $result['tb_ventacontadoc_motivonc'];
      $detr                 = intval($result['tb_ventacontadoc_detr']) == 1 ? 'checked' : '';
      $docref_serie_numero = ($docrefidfk == 0) ? '' : $result['docreferido_serie'] . '-' . $result['docreferido_numero'];
      $docref_fechaemision  = ($docrefidfk == 0) ? '' : mostrar_fecha($result['docreferido_emision']);
      $comentario           = $result['tb_ventacontadoc_detalle'];
      $constancia_detraccion = $result['tb_ventacontadoc_constdetr'];
      $fecha_detraccion     = $result['tb_ventacontadoc_fechadetr'] == '0000-00-00' ? '' : mostrar_fecha($result['tb_ventacontadoc_fechadetr']);
      $venta_correlativo = $result['tb_ventacontadoc_correlativo'];

      //buscamos los detalles
      //$detalles = $oVentaDetalle->buscar_por_ventacontaid($ventacontadoc_id)['data'];
      $cantidad_detalles = count($detalles);
      // los detalles que no son el primero ni los dos ultimos se añaden mediante el JS
      echo var_export($result);
      //exit();
    }
  }
} else {
  $mensaje =  $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_ventacontadoc" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title mayus" style="font-size: 16px"><b><?php echo $titulo ?></b></h4>
        </div>

        <form id="form_ventacontadoc" method="post" style="font-size: 11px">

          <input type="hidden" name="action" id="action" value="<?php echo $action ?>">
          <input type="hidden" name="hdd_ventacontadoc_id" id="hdd_ventacontadoc_id" value="<?php echo $action == 'insertar' ? 0 : $ventacontadoc_id ?>">
          <input type="hidden" id="hdd_ventacontadoc_registro" name="hdd_ventacontadoc_registro" value='<?php echo json_encode($result) ?>'>

          <div class="modal-body">
            <?php if ($action == 'insertar' || $action == 'modificar') ?>
            <!-- espacio para los campos -->
            <div class="box box-primary shadow">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="hidden" id="hdd_cliente_id" name="hdd_cliente_id" value='<?php echo $cliente_id ?>'>
                      <label>RUC/DNI CLIENTE:</label>
                      <div id="cliente-grp" class="input-group">
                        <input type="text" id="txt_cliente_doc" name="txt_cliente_doc" placeholder="Escribe para buscar cliente" class="form-control mayus" value="<?php echo $cliente_doc ?>">
                        <span class="input-group-btn">
                          <button id="btn_mod" class="btn btn-primary btn-sm" type="button" onclick="cliente_form('M', 0)" title="MODIFICAR CLIENTE">
                            <span class="fa fa-pencil icon"></span>
                          </button>
                        </span>
                        <span class="input-group-btn">
                          <button id="btn_agr" class="btn btn-primary btn-sm" type="button" onclick="cliente_form('I', 0)" title="AGREGAR CLIENTE">
                            <span class="fa fa-plus icon"></span>
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="form group">
                      <label for="txt_cliente_nom">CLIENTE:</label><br>
                      <input type="text" id="txt_cliente_nom" name="txt_cliente_nom" placeholder="Escribe para buscar cliente" class="form-control mayus" value="<?php echo $cliente_nom ?>">
                    </div>
                  </div>
                  <div class="col-md-1">
                    <label for="btn-limpiar" style="color:white;">l</label>
                    <span class="input-group-btn">
                      <button id="btn-limpiar" class="btn btn-primary btn-sm" type="button" onclick="limpiar_cliente()" title="LIMPIAR DATOS DEL CLIENTE">
                        <span class="fa fa-eraser icon"></span>
                      </button>
                    </span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="txt_doc_cod">CODIGO DE TIPO DOC:</label><br>
                      <input type="text" name="txt_doc_cod" id="txt_doc_cod" placeholder="Escribe para buscar Tipo de documento" class="form-control mayus" value="<?php echo $doc_cod ?>">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="form-group">
                      <label for="txt_doc_nom">TIPO DE DOCUMENTO:</label><br>
                      <input type="text" name="txt_doc_nom" id="txt_doc_nom" placeholder="Escribe para buscar Tipo de documento" class="form-control mayus" value="<?php echo $doc_nom ?>">
                    </div>
                  </div>
                  <div class="col-md-1">
                    <label for="btn-limpiar-1" style="color:white;">l</label>
                    <span class="input-group-btn">
                      <button id="btn-limpiar-1" class="btn btn-primary btn-sm" type="button" onclick="limpiar_tipodoc()" title="LIMPIAR TIPO DE DOCUMENTO">
                        <span class="fa fa-eraser icon"></span>
                      </button>
                    </span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2" style="width: 14.3%;">
                    <div class="form-group">
                      <label for="cmb_mesconta_id">PERIODO CONTABLE:</label>
                      <select class="form-control mayus" name="cmb_mesconta_id" id="cmb_mesconta_id">
                        <?php require_once('../mesconta/mesconta_select.php') ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2" style="width: 19%;">
                    <div class="form-group">
                      <label for="txt_ventaconta_correlativo">CORRELATIVO:</label>
                      <input type="text" name="txt_ventaconta_correlativo" id="txt_ventaconta_correlativo" placeholder="CORRELATIVO" class="form-control mayus disabled" value="<?php echo $venta_correlativo ?>">
                    </div>
                  </div>
                  <div class="col-md-2" style="width: 19%;">
                    <div class="form-group">
                      <label for="txt_doc_condicion">CONDICION:</label>
                      <input type="text" name="txt_doc_condicion" id="txt_doc_condicion" class="form-control mayus disabled" value="<?php echo $condicion ?>">
                    </div>
                  </div>
                  <div class="col-md-1" style="width: 5%;"></div>
                  <div class="col-md-1">
                    <div class="form-group">
                      <label for="cmb_moneda_id" class="control-label">MONEDA:</label>
                      <select class="form-control" name="cmb_moneda_id" id="cmb_moneda_id" style="width: 65px;">
                        <?php require_once('../moneda/moneda_select.php') ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2" style="width: 12.5%">
                    <div class="form-group">
                      <label for="che_afecta" class="control-label" style="color:white;"></label>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" id="che_afecta" name="che_afecta" class="flat-green" style="position: relative;" value="1" <?php echo $afecta ?>><b>&nbsp; AFECTA IGV</b>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-1 es_afecta" <?php if ($afecta != 'checked') echo 'style="display: none;"' ?>>
                    <div class="form-group">
                      <label for="txt_porc_igv">% IGV:</label>
                      <input type="text" name="txt_porc_igv" id="txt_porc_igv" placeholder="18" class="form-control porc-igv" value="<?php echo $porcentaje_igv ?>">
                    </div>
                  </div>
                </div>

                <div class="row documento_ref" style="<?php if ($doc_cod != '07') {echo 'display: none;';} ?>">
                  <input type="hidden" name="txt_docrefidfk" id="txt_docrefidfk" value="<?php echo $docrefidfk ?>">
                  <input type="hidden" name="txt_docref_clienteid" id="txt_docref_clienteid" value="<?php echo $docref_clienteidfk ?>">
                  <div class="col-md-3">
                    <label for="txt_documento_ref">DOCUMENTO REFERIDO:</label>
                    <input type="text" name="txt_documento_ref" id="txt_documento_ref" placeholder="F001-000000001" class="form-control mayus" value="<?php echo $docref_serie_numero ?>">
                  </div>
                  <div class="col-md-4">
                    <label for="txt_docref_fechaemision">FECHA EMISION DOC. REFERIDO:</label>
                    <div class='input-group date'>
                      <input type='text' class="form-control disabled" placeholder="DD-MM-AAAA" name="txt_docref_fechaemision" id="txt_docref_fechaemision" value="<?php echo $docref_fechaemision ?>" />
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <label for="txt_motivo_nc">MOTIVO DE NC:</label>
                    <textarea class="form-control mayus" name="txt_motivo_nc" id="txt_motivo_nc"><?php echo $motivo_nc ?></textarea>
                  </div>
                </div>

                <p>

                <div class="box box-primary box-solid" style="width: 97%;">
                  <div class="box-header">
                    <h5 class="box-title">Datos control interno</h5>
                    <div class="box-tools">
                      <button type="button" class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-1">
                        <div class="form-group">
                          <label for="txt_doc_seriei">SERIE:</label>
                          <input type="text" name="txt_doc_seriei" id="txt_doc_seriei" placeholder="F001" class="form-control mayus" onfocus="formato_serie_contable('txt_doc_cod', this)" value="<?php echo $seriei; ?>">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <label for="txt_doc_numeroi">NUMERO:</label>
                        <input type="text" name="txt_doc_numeroi" id="txt_doc_numeroi" placeholder="00000001" class="form-control numero-doc" onfocus="formato_numero_contable(this)" value="<?php echo $numeroi; ?>">
                      </div>
                      <div class="col-md-2">
                        <label for="ventacontadoc_emi_pickeri">FECHA EMISION:</label>
                        <div class='input-group date'>
                          <input type='text' class="form-control" placeholder="DD-MM-AAAA" name="ventacontadoc_emi_pickeri" id="ventacontadoc_emi_pickeri" value="<?php echo $fecha_emisioni ?>" />
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-1">
                        <input type="hidden" name="hdd_monedacambio_idfk" id="hdd_monedacambio_idfk" value="<?php echo $monedacambio_idfk ?>">
                        <div class="form-group">
                          <label for="txt_tipocambioi" class="control-label">T. CAMBIO:</label>
                          <input type="text" name="txt_tipocambioi" id="txt_tipocambioi" class="form-control disabled" value="<?php echo $tipo_cambioi ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <br><br>
                        <span class="badge bg-red" id="span_errori" style="display: none;"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <br>

                <div class="box box-primary box-solid" style="width: 97%;">
                  <div class="box-header">
                    <h5 class="box-title">Datos control externo</h5>
                    <div class="box-tools">
                      <button type="button" class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-1">
                        <div class="form-group">
                          <label for="txt_doc_seriee">SERIE:</label>
                          <input type="text" name="txt_doc_seriee" id="txt_doc_seriee" placeholder="F001" class="form-control mayus" onfocus="formato_serie_contable('txt_doc_cod', this)" value="<?php echo $seriee ?>">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <label for="txt_doc_numeroe">NUMERO:</label>
                        <input type="text" name="txt_doc_numeroe" id="txt_doc_numeroe" placeholder="00000001" class="form-control numero-doc" onfocus="formato_numero_contable(this)" value="<?php echo $numeroe ?>">
                      </div>
                      <div class="col-md-2">
                        <label for="ventacontadoc_emi_pickere">FECHA EMISION:</label>
                        <div class='input-group date'>
                          <input type='text' class="form-control" placeholder="DD-MM-AAAA" name="ventacontadoc_emi_pickere" id="ventacontadoc_emi_pickere" value="<?php echo $fecha_emisione ?>" />
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-1">
                        <input type="hidden" name="hdd_monedacambiocontable_idfk" id="hdd_monedacambiocontable_idfk" value="<?php echo $monedacambiocontable_idfk; ?>">
                        <div class="form-group">
                          <label for="txt_tipocambioe" class="control-label">T. CAMBIO:</label>
                          <input type="text" name="txt_tipocambioe" id="txt_tipocambioe" class="form-control disabled" value="<?php echo $tipo_cambioe ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <br><br>
                        <span class="badge bg-red" id="span_errore" style="display: none;"></span>
                      </div>
                    </div>
                  </div>
                </div>

                <br>

                <div class="row">
                  <div class="col-md-11" style="width: 97%;">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="tbl_detalle">DETALLE DE LA COMPRA:</label>
                      </div>
                      <div class="col-md-6" align="right">
                        <a href="javascript:void(0)" id="add_row" onclick="nuevo_detalle(1)" style="font-size: 13px"><b class="mayus"><u>Añadir fila</u></b></a>
                      </div>
                    </div>
                    <input type="hidden" id="hdd_ventacontadetalle_array" name="hdd_ventacontadetalle_array" value='<?php echo json_encode($detalles) ?>'>
                    <input type="hidden" name="cantidad_filas_detalle" id="cantidad_filas_detalle" value="<?php echo 3; //$cantidad_detalles 
                                                                                                          ?>">
                    <table id="tbl_detalle" name="tbl_detalle" class="table table-hover" style="word-wrap:break-word;">
                      <thead>
                        <tr id="tabla_cabecera">
                          <th id="tabla_cabecera_fila" style="width: 15%;">Cod. Cuenta Cont.</th>
                          <th id="tabla_cabecera_fila" style="width: 50%;">Glosa</th>
                          <th id="tabla_cabecera_fila" style="width: 12%;">Monto</th>
                          <th id="tabla_cabecera_fila" style="width: 18%;">Sede</th>
                          <th id="tabla_cabecera_fila" style="width: 5%;"> </th>
                        </tr>
                      </thead>
                      <tbody id="tbody">
                        <tr id="tabla_cabecera_fila_0">
                          <input type="hidden" name="txt_detalleid_0" id="txt_detalleid_0" value="<?php echo $detalles[0]['tb_ventacontadetalle_id']; ?>">
                          <input type="hidden" name="txt_detalleorden_0" id="txt_detalleorden_0" value="<?php echo $detalles[0]['tb_ventacontadetalle_orden']; ?>">
                          <td id="tabla_fila"><input type="text" name="txt_codcuentacont_0" id="txt_codcuentacont_0" class="form-control" placeholder="COD. CTA. CONT." value="<?php echo $detalles[0]['tb_cuentaconta_cod']; ?>"></td>
                          <td id="tabla_fila"><input type="text" name="txt_glosa_0" id="txt_glosa_0" class="form-control mayus" placeholder="GLOSA" value="<?php echo $detalles[0]['tb_ventacontadetalle_glosa']; ?>"></td>
                          <td id="tabla_fila" class="monto"><input type="text" name="txt_monto_0" id="txt_monto_0" class="form-control moneda para_calcular" placeholder="0.00" value="<?php echo mostrar_moneda($detalles[0]['tb_ventacontadetalle_monto']); ?>"></td>
                          <td id="tabla_fila">
                            <input type="hidden" name="txt_sedenombre_0" id="txt_sedenombre_0">
                            <select name="cbo_sede_0" id="cbo_sede_0" class="form-control"><?php require '../empresa/empresa_select.php' ?></select>
                          </td>
                          <td id="tabla_fila"></td>
                        </tr>
                        <tr id="tabla_cabecera_fila_1" class="auto">
                          <input type="hidden" name="txt_detalleid_1" id="txt_detalleid_1" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_ventacontadetalle_id']; ?>">
                          <input type="hidden" name="txt_detalleorden_1" id="txt_detalleorden_1" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_ventacontadetalle_orden']; ?>">
                          <td id="tabla_fila"><input type="text" name="txt_codcuentacont_1" id="txt_codcuentacont_1" class="form-control disabled" value="25070302"></td>
                          <td id="tabla_fila"><input type="text" name="txt_glosa_1" id="txt_glosa_1" class="form-control mayus" placeholder="IGV" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_ventacontadetalle_glosa']; ?>"></td>
                          <td id="tabla_fila" class="monto"><input type="text" name="txt_monto_1" id="txt_monto_1" class="form-control moneda" placeholder="0.00" onfocus="formato_moneda(this)" value="<?php echo mostrar_moneda($detalles[$cantidad_detalles - 2]['tb_ventacontadetalle_monto']); ?>"></td>
                          <td id="tabla_fila">
                            <input type="hidden" name="txt_sedenombre_1" id="txt_sedenombre_1">
                            <select name="cbo_sede_1" id="cbo_sede_1" class="form-control disabled"><?php require '../empresa/empresa_select.php' ?></select>
                          </td>
                          <td id="tabla_fila" class="text-center"></td>
                        </tr>
                        <tr id="tabla_cabecera_fila_2" class="auto">
                          <input type="hidden" name="txt_detalleid_2" id="txt_detalleid_2" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_ventacontadetalle_id']; ?>">
                          <input type="hidden" name="txt_detalleorden_2" id="txt_detalleorden_2" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_ventacontadetalle_orden']; ?>">
                          <td id="tabla_fila"><input type="text" name="txt_codcuentacont_2" id="txt_codcuentacont_2" class="form-control" placeholder="COD. CTA. CONT." value="<?php echo $detalles[$cantidad_detalles - 1]['tb_cuentaconta_cod']; ?>"></td>
                          <td id="tabla_fila"><input type="text" name="txt_glosa_2" id="txt_glosa_2" class="form-control mayus" placeholder="GLOSA" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_ventacontadetalle_glosa']; ?>"></td>
                          <td id="tabla_fila" class="monto"><input type="text" name="txt_monto_2" id="txt_monto_2" class="form-control moneda monto_total" placeholder="0.00" onfocus="formato_moneda(this)" value="<?php echo mostrar_moneda($detalles[$cantidad_detalles - 1]['tb_ventacontadetalle_monto']); ?>"></td>
                          <td id="tabla_fila">
                            <input type="hidden" name="txt_sedenombre_2" id="txt_sedenombre_2">
                            <select name="cbo_sede_2" id="cbo_sede_2" class="form-control disabled"><?php require '../empresa/empresa_select.php' ?></select>
                          </td>
                          <td id="tabla_fila"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>


                <label for="box_resumen">RESUMEN:</label>
                <div id="box_resumen" class="row">
                  <div id="box_redim" class="col-md-5" style="width: <?php echo $moneda_id == 2 ? '48.8' : '24.2'; ?>%;">
                    <!-- datos en moneda original -->
                    <div class="box box-primary shadow">
                      <div class="box-header">
                        <!-- fila 1 -->
                        <div class="row">
                          <div id="div_mon_ori_1" class="col-md-<?php echo $moneda_id == 2 ? '6' : '12'; ?>">
                            <label id="lbl_monto_base_ori" class="lbl-afecta lbl-moneda" for="txt_monto_base_red">BASE <?php echo $afecta != 'checked' ? 'INAFECTA' : 'AFECTA' ?> :</label>
                            <input type="text" name="txt_monto_base_red" id="txt_monto_base_red" placeholder="" class="form-control moneda montos-mone-ori disabled" value="<?php echo $monto_base ?>">
                          </div>
                          <div class="col-md-6 moneda-dolar" <?php echo $moneda_id == 2 ? '' : 'style="display: none;"' ?>>
                            <label id="lbl_monto_base_sol" class="lbl-afecta" for="txt_monto_base_sol_red">BASE <?php echo $afecta != 'checked' ? 'INAFECTA' : 'AFECTA' ?> (S/.):</label>
                            <input type="text" name="txt_monto_base_sol_red" id="txt_monto_base_sol_red" placeholder="" class="form-control moneda" value="<?php echo $monto_base_soles ?>">
                          </div>
                        </div>
                        <br><!-- fila 2 -->
                        <div class="row">
                          <div id="div_mon_ori_2" class="col-md-<?php echo $moneda_id == 2 ? '6' : '12'; ?>">
                            <label id="lbl_monto_igv_ori" class="lbl-moneda" for="txt_monto_igv_red">MONTO IGV :</label>
                            <input type="text" name="txt_monto_igv_red" id="txt_monto_igv_red" placeholder="" class="form-control moneda montos-mone-ori disabled" value="<?php echo $monto_igv ?>">
                          </div>
                          <div class="col-md-6 moneda-dolar" <?php echo $moneda_id == 2 ? '' : 'style="display: none;"' ?>>
                            <label id="lbl_monto_igv_sol" for="txt_monto_igv_sol_red">MONTO IGV (S/.):</label>
                            <input type="text" name="txt_monto_igv_sol_red" id="txt_monto_igv_sol_red" placeholder="" class="form-control moneda" value="<?php echo $monto_igv_soles ?>">
                          </div>
                        </div>
                        <br>
                        <!-- fila 3 -->
                        <div class="row">
                          <div id="div_mon_ori_3" class="col-md-<?php echo $moneda_id == 2 ? '6' : '12'; ?>">
                            <label id="lbl_monto_total_ori" class="lbl-moneda" for="txt_importe_total">IMPORTE TOTAL :</label>
                            <input type="text" name="txt_importe_total" id="txt_importe_total" placeholder="" class="form-control moneda disabled" value="<?php echo $importe_total ?>">
                          </div>
                          <div class="col-md-6 moneda-dolar" <?php echo $moneda_id == 2 ? '' : 'style="display: none;"' ?>>
                            <label id="lbl_monto_total_sol" for="txt_importe_total_sol_red">IMPORTE TOTAL (S/.):</label>
                            <input type="text" name="txt_importe_total_sol_red" id="txt_importe_total_sol_red" placeholder="" class="form-control moneda" value="<?php echo $importe_total_soles ?>">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <!-- detraccion -->
                <div class="row detrac" style="<?php echo ($afecta == 'checked') ? '' : 'display: none;' ?>">
                  <br>
                  <div class="col-md-3">
                    <label for="che_detr" class="control-label" style="color:white;"></label>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="che_detr" name="che_detr" class="flat-green" style="position: relative;" value="1" <?php echo $detr ?>><b>&nbsp; DETRACCION</b>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3 es_detr" <?php if ($detr != 'checked') echo 'style="display: none;"' ?>>
                    <label for="txt_const_detraccion">NRO. CONSTANCIA DETRACCION:</label>
                    <input type="text" name="txt_const_detraccion" id="txt_const_detraccion" placeholder="" class="form-control mayus" value="<?php echo $constancia_detraccion ?>">
                  </div>
                  <div class="col-md-4 es_detr" <?php if ($detr != 'checked') echo 'style="display: none;"' ?>>
                    <label for="ventacontadoc_detraccion_picker">FECHA DETRACCION:</label>
                    <div class='input-group date'>
                      <input type='text' class="form-control" placeholder="DD-MM-AAAA" name="ventacontadoc_detraccion_picker" id="ventacontadoc_detraccion_picker" value="<?php echo $fecha_detraccion ?>" />
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>

              </div>

              <div class="box-footer">
                <div class="row">
                  <div class="col-md-12">
                    <label for="txt_comentario">COMENTARIO:</label>
                    <textarea class="form-control mayus" name="txt_comentario" id="txt_comentario"><?php echo $comentario ?></textarea>
                  </div>
                </div>
              </div>
            </div>



            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Documento de venta?</h4>
              </div>
            <?php endif ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="ventacontadoc_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_ventacontadoc">Guardar</button>
              <?php endif ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_ventacontadoc">Aceptar</button>
              <?php endif ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_ventacontadoc">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <?php echo $mensaje ?>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif ?>

<script type="text/javascript" src="<?php echo 'vista/ventacontadoc/ventacontadoc_form.js?ver=240423' ?>"></script>