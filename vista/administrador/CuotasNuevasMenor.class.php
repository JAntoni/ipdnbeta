<?php

  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');

class CuotasNuevasMenor extends Conexion{
	function generar_nueva_cuota(){
		require_once ("../funciones/formato.php");
		require_once ("../funciones/fechas.php");
		require_once("../cuota/Cuota.class.php");
		$oCuota = new Cuota();

		try {
			$dts = $this->listar_creditos_cuota_libre();
			$rows_libre = mysql_num_rows($dts);
			
			if($rows_libre < 1){
				$tipo = 0; //tipos: 0 no se muestra como mensaje, 1 se muestra como mensaje al iniciar sesión
				$det = 'No hay créditos MENORES con cuotas vencidas';
				$this->insertar_cronjob($det, $tipo, 2); //insertado por defecto al admin de sistemas
				echo 'SIN CUOTAS VENCIDAS EN CREDITO MENOR // ';
			}
			else{
				while ($dt = mysql_fetch_array($dts)){
					$cre_id = $dt['tb_credito_id'];
					$cli_nom = $dt['tb_cliente_nom'];
				  $cuo_id = $dt['tb_cuota_id'];
				  $cuo_num = $dt['tb_cuota_num'];
				  $cuo_max = $dt['tb_credito_numcuomax'];
				  $cre_int = $dt['tb_credito_int'];
				  $fecha = mostrarFecha($dt['tb_cuota_fec']);
				  $mon_id = $dt['tb_moneda_id'];

				  $cuota_cap =$dt['tb_cuota_cap']; //capital de la cuota
		      $cuota_int = $dt['tb_cuota_int']; //monto del interes de esa cuota
		      $cuota_cuo = $dt['tb_cuota_cuo']; //la cuota fue creada con este monto

		      //el tipo del credito es 1 de menor, eso va en el primer parametro
				  $dtsCuo = $oCuota->filtrar(1, $cre_id);
				    $num_rows_cuo = mysql_num_rows($dtsCuo);
				  mysql_free_result($dtsCuo);

				  if($num_rows_cuo >= $cuo_max){
				    //no se puede generar más cuotas ya pasó el máximo
				    //tipos: 0 no se muestra como mensaje, 1 se muestra como mensaje al iniciar sesión
						$det = '<strong style="color: red;">La cuota número: '.$cuo_num.' (ID: '.$cuo_id.') del crédito CM-'.$cre_id.' y cliente '.$cli_nom.', ya venció y no se pude crear una nueva cuota ya que ha llegado al número máximo de cuotas libres.</strong>';
						$this->insertar_cronjob($det, 1, 11);//se mostrará como mensaje a los admin	 
						$this->insertar_cronjob($det, 1, 18); //se mostrará como mensaje a los admin
						$this->insertar_cronjob($det, 1, 2); //se mostrará como mensaje a los admin
						$this->insertar_cronjob($det, 1, 16); //mensaje para vendedor Luis
						$this->insertar_cronjob($det, 1, 26); //mensaje para vendedor Angelo
				  }
				  else{
				  	//ingresos totales de la cuota
				  	$pagos_cuo = $this->retorna_importe_cuota($cuo_id);
				    
				    //generamos otra cuota, para ello obetenemos el nuevo capital que es el mismo capital de la cuota anteior, restado el monto sobrante del pago mínimo
				    $capital = $cuota_cap;

				    if($pagos_cuo >= $cuota_int){
				    	//actualizamos los estado de la cuota que ya venció, si los pagos pasan al pago minimo la cuota cambia a cancelada sino se queda con el estado anterior
				    	$oCuota->modificar_campo($cuo_id,'est','2');

				    	if($pagos_cuo > $cuota_int){
				      	$capital = $cuota_cap - ($pagos_cuo - $cuota_int);
				    	}

				      //echo $capital.' // '.$cuota_cap.' // '.$pagos_cuo; exit();
				      //return mysql_num_rows($dts).' // cre_id: '.$cre_id.' // rows cuta: '.$num_rows_cuo.'/// maximas: '.$cuo_max.' // cuota_id: '.$cuo_id.' / pagos: '.$pagos_cuo.' // interes: '.$cuota_int.' // 	capital: '.$cuota_cap.' // nuevo capital: '.$capital; exit();

				      $C = $capital;
					    $i = $cre_int;
					    $n = 1; //num cuotas

					    $uno = $i / 100;
					    $dos = $uno + 1;
					    $tres = pow($dos,$n);
					    $cuatroA = ($C * $uno) * $tres;
					    $cuatroB = $tres - 1;
					    $R = $cuatroA / $cuatroB;
					    $r_sum = $R*$n;
					        //echo $C.' // '.$uno.' // '.$dos.' // '.$tres.' // '.$cuatroA.' // '.$cuatroB; exit();
					    
					    list($day, $month, $year) = split('-', $fecha);

					    for ($j=1; $j <= $n; $j++){ 
					      if($j>1)
					      {
					        $C = $C - $amo;
					        $int = $C*($i/100);
					        $amo = 0;//$R - $int;
					      }
					      else
					      {
					        $int = $C*($i/100);
					        $amo = 0;//$R - $int;
					      } 

					      //fecha facturacion
					      $month = $month + 1;
					      if($month == '13'){
					        $month = 1;
					        $year = $year + 1;
					      }
					      $cuota_fecha = validar_fecha_facturacion($day,$month,$year);
					          //echo $C.' // '.$amo.' // '.$int.' // '.$R.' // '.fecha_mysql($cuota_fecha); exit();
					      $xac=1;
					      $cretip_id=1;//credito tipo
					      $est=1;
					      $cuo_num= $num_rows_cuo + 1;
					      $persubcuo = 1;
					      $oCuota->insertar(
					        $xac,
					        $cre_id,
					        $cretip_id,
					        $mon_id,
					        $cuo_num,
					        fecha_mysql($cuota_fecha), 
					        $C, 
					        $amo, 
					        $int, 
					        $R, 
					        $pro,
					        $persubcuo, 
					        $est
					      );

					      $dtsCuo = $oCuota->ultimoInsert();
					        $dtCuo = mysql_fetch_array($dtsCuo);
					        $cuo_id = $dtCuo['last_insert_id()'];
					      mysql_free_result($dtsCuo);

					      //automatico
					      $oCuota->modificar_campo($cuo_id,'aut','1');
					    }
					    $det = '<strong style="color: green;">Se ha creado una nueva cuota para el crédio: CM-'.$cre_id.' del cliente '.$cli_nom.'</strong>';
							$this->insertar_cronjob($det, 1, 11); //se mostrará como mensaje a los admin
							$this->insertar_cronjob($det, 1, 18); //se mostrará como mensaje a los admin
							$this->insertar_cronjob($det, 1, 2); //se mostrará como mensaje a los admin
							$this->insertar_cronjob($det, 1, 16); //mensaje para Luis
							$this->insertar_cronjob($det, 1, 26); //mensaje para vendedor Angelo
				    }
				    else{
				    	$intervalo = date_diff(date_create($fecha), date_create(date('d-m-Y')));
				    	$diff = $intervalo->format('%d');

				    	$msj = 'Lleva '.$diff.' días vecida.';
				    	if($diff == 0){
				    		$msj = 'Vence justo hoy día.';
				    	}

				    	$det = '<strong>Los montos parciales de la cuota número: '.$cuo_num.' (ID: '.$cuo_id.')  del crédito CM-'.$cre_id.' LIBRE y cliente '.$cli_nom.', no son mayores al interés acordado, por ende si no paga en 15 días pasará a remate sus garantías. '.$msj.'</strong>';
							$this->insertar_cronjob($det, 1, 11); //se mostrará como mensaje a los admin
							$this->insertar_cronjob($det, 1, 18); //se mostrará como mensaje a los admin
							$this->insertar_cronjob($det, 1, 2); //se mostrará como mensaje a los admin
							$this->insertar_cronjob($det, 1, 16); //mensaje para Luis
							$this->insertar_cronjob($det, 1, 26); //mensaje para vendedor Angelo
				    }
				  }
				}

				mysql_free_result($dts);

				$dts = $this->listar_creditos_cuota_fija();
					while ($dt = mysql_fetch_array($dts)){
						$cre_id = $dt['tb_credito_id'];
						$cli_nom = $dt['tb_cliente_nom'];
					  $cuo_id = $dt['tb_cuota_id'];
					  $cuo_num = $dt['tb_cuota_num'];
					  $fecha = mostrarFecha($dt['tb_cuota_fec']);

			      $intervalo = date_diff(date_create($fecha), date_create(date('d-m-Y')));
			    	$diff = $intervalo->format('%d');

			    	$msj = 'Lleva '.$diff.' días vecida.';
			    	if($diff == 0){
			    		$msj = 'Vence justo hoy día.';
			    	}

			    	$det = '<strong>Los montos parciales de la cuota número: '.$cuo_num.' (ID: '.$cuo_id.')  del crédito CM-'.$cre_id.' FIJA y cliente '.$cli_nom.', no son mayores al interés acordado, por ende si no paga en 15 días pasará a remate sus garantías. '.$msj.'</strong>';
						$this->insertar_cronjob($det, 1, 11); //se mostrará como mensaje a los admin
						$this->insertar_cronjob($det, 1, 18); //se mostrará como mensaje a los admin
						$this->insertar_cronjob($det, 1, 2); //se mostrará como mensaje a los admin
						$this->insertar_cronjob($det, 1, 16); //mensaje para Luis
						$this->insertar_cronjob($det, 1, 26); //mensaje para vendedor Angelo
			    }
				mysql_free_result($dts);

				echo 'Ejecutado CUOTAS DE CREDITO MENOR // ';
			}
		} catch (Exception $e) {
			echo 'Errorrrrr: '.$e;
		}
	}

        function listar_creditos_cuota_libre(){
            try {
              $sql ="SELECT * FROM tb_creditomenor cm
			INNER JOIN tb_cuota c ON cm.tb_credito_id = c.tb_credito_id
			INNER JOIN tb_cliente cli on cli.tb_cliente_id = cm.tb_cliente_id
			WHERE c.tb_creditotipo_id = 1 AND c.tb_cuota_fec < NOW() AND c.tb_cuota_est <> 2 AND cm.tb_credito_xac = 1 and tb_cuotatipo_id = 1 and cm.tb_credito_est = 3";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->execute();

              if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
              }
              else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
              }

              return $retorno;
            } catch (Exception $e) {
              throw $e;
            }

          }

        function listar_creditos_cuota_fija(){
            try {
              $sql ="SELECT * FROM tb_creditomenor cm
			INNER JOIN tb_cuota c ON cm.tb_credito_id = c.tb_credito_id
			INNER JOIN tb_cliente cli on cli.tb_cliente_id = cm.tb_cliente_id
			WHERE c.tb_creditotipo_id = 1 AND c.tb_cuota_fec < NOW() AND c.tb_cuota_est <> 2 AND cm.tb_credito_xac = 1 and tb_cuotatipo_id = 2 and cm.tb_credito_est = 3";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->execute();

              if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
              }
              else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
              }

              return $retorno;
            } catch (Exception $e) {
              throw $e;
            }

          }
        
	function retorna_importe_cuota($cuo_id){
            require_once ("../cuotapago/Cuotapago.class.php");
            $oCuotapago = new Cuotapago();
            //parametro 1 ya que es cuota, 2 cuotadetalle
            $dtsPag = $oCuotapago->mostrar_cuotatipo_ingreso(1, $cuo_id);
            $pagos_cuo = 0;
            if($dtsPag['estado']==1){
                foreach ($dtsPag['data']as $key=>$dtp) {
                  $pagos_cuo += $dtp['tb_ingreso_imp'];
                }
            }
            return $pagos_cuo;
	}
        
        function insertar_cronjob($det, $tipo, $usu_id){
            $this->dblink->beginTransaction();
            try {
              $sql = "INSERT INTO tb_cron(
                                        tb_cron_fec,
                                        tb_cron_det,
                                        tb_cron_tip, 
                                        tb_usuario_id)
                                  values(
                                        NOW(),
                                        :tb_cron_det,
                                        :tb_cron_tip, 
                                        :tb_usuario_id)";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_cron_det", $det, PDO::PARAM_STR);
              $sentencia->bindParam(":tb_cron_tip", $tipo, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);

              $result = $sentencia->execute();
              $this->dblink->commit();
              return $result; //si es correcto retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
        }
        function listar_mensajes_admins($usu_id){
            try {
                $sql = "SELECT * FROM tb_cron where tb_cron_tip =1 and tb_usuario_id =:tb_usuario_id and tb_cron_vis = 0";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
              $sentencia->execute();

              if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
              }
              else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
              }

              return $retorno;
            } catch (Exception $e) {
              throw $e;
            }

          }
          
          function  mensaje_visto($cron_id){
            $this->dblink->beginTransaction();
            try {
             $sql = "UPDATE tb_cron SET tb_cron_vis =1 where tb_cron_id =:tb_cron_id";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_cron_id", $cron_id, PDO::PARAM_INT);

              $result = $sentencia->execute();
              $this->dblink->commit();

              return $result; //si es correcto retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
          }
          
          function  mensaje_visto_todos_usuario($user_id){
            $this->dblink->beginTransaction();
            try {
             $sql = "UPDATE tb_cron SET tb_cron_vis =1 where tb_usuario_id =:tb_usuario_id";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_usuario_id", $user_id, PDO::PARAM_INT);

              $result = $sentencia->execute();
              $this->dblink->commit();

              return $result; //si es correcto retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
          }
        
}
?>