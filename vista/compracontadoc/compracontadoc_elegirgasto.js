$(document).ready(function(){
    disabled($('#txt_credito_cliente, #cbo_gastos_id, #cbo_gastopagos_id'));
    $('#cmb_creditotipo_id').focus();
	$("#cmb_creditotipo_id option[value='5']").remove(); //se quitan las adendas del combo

    $('#txt_credito_id').blur(function(){
		if($('#cmb_creditotipo_id option:selected').val() < 1) {
			$('#cmb_creditotipo_id').focus();
		} else {
			buscar_credito();
		}
		$(this).val(rellenar_num_ceros($(this).val(), 4));
    });

    $('#cmb_creditotipo_id').change(function(){
		if($('#txt_credito_id').val() !== ''){
			buscar_credito();
		}
    });
	
	$('.cred-id').autoNumeric({
		aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '999999999'
	});

	$('#cbo_gastos_id').change(function(){
		$('#hdd_gasto_id').val($('#cbo_gastos_id option:selected').val());
		if (parseInt($('#cmb_creditotipo_id').val()) == 3 && parseInt($(this).val()) > 0) {
			cargar_gastopagos($(this).val());
		}
	});

	$("#form_elegirgasto").validate({
		submitHandler: function () {
			if ($('#hdd_vista').val() == 'compracontadoc_form') {
				var gasto_id = $('#cbo_gastos_id option:selected').val();
				if (gasto_id > 0) {
					var pos = $('#hdd_posicion').val();
					var cred_tip = $('#cmb_creditotipo_id option:selected').val();
					var cred_id = $('#txt_credito_id').val();

					var array_gastos = $('#cbo_gastos_id option:selected').text().split(' | ');
					var garantia_nom = "";
					var monto_gasto = array_gastos[0];
					var proveedor_id = array_gastos[3].split(": ")[1];
					var gastopago = $('#cbo_gastopagos_id option:selected');
					if (gastopago.val() > 0){
						monto_gasto = gastopago.text().split(' | ')[0];
					}
					if(cred_tip == 1){
						garantia_nom = array_gastos[1];
					}
					
					completar_valores_gasto(cred_tip, cred_id, gasto_id, pos, proveedor_id, garantia_nom, monto_gasto, gastopago.val());
					$('#div_modal_elegirgasto_form').modal('hide');
				}
			}
		},
		rules: {
			cmb_creditotipo_id: {
				min: 1
			},
			txt_credito_id: {
				required: true
			},
			cbo_gastos_id: {
				required: function(){
					return !$('#cbo_gastos_id').prop('disabled');
				},
				min: 1
			},
			cbo_gastopagos_id: {
				required: function(){
					return !$('.row_gastopago').css('display') == 'none';
				},
				min: 1
			}
		},
		messages: {
			cmb_creditotipo_id: {
				min: "Elegir tipo de crédito"
			},
			txt_credito_id: {
				required: "Ingrese id crédito"
			},
			cbo_gastos_id: {
				required: "Elija un gasto",
				min: "Elija un gasto"
			},
			cbo_gastopagos_id: {
				required: "Elija un pago",
				min: "Elija un pago"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			}
			else if (element.parent("div.input-group").length == 1) {
				error.insertAfter(element.parent("div.input-group")[0]);
			}
			else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	$('.select2').select2({
		dropdownParent: $('#div_modal_elegirgasto_form'),
		containerCssClass: "wrap"
	});

	if ($('#action_elegirgasto').val() == 'L') {
		buscar_credito();
        disabled($('#form_elegirgasto').find("button[class*='btn-sm'], select, input"));
        $('#form_elegirgasto').find("input").css("cursor", "text");
    }
});

function limpiar_credito(){
	$("#cmb_creditotipo_id").val("0");
	$('#txt_credito_id').val('');
	$('#txt_credito_cliente').val('');

	$('#cmb_creditotipo_id').prop('disabled', false);
	$('#txt_credito_id').prop('disabled', false);
	$('#cmb_creditotipo_id').focus();
    $('#cbo_gastos_id').html('');
	$('#hdd_gasto_id').val(0);
    disabled($('#cbo_gastos_id'));
	$('#cbo_gastopagos_id').html('');
	$('.row_gastopago').hide(150);
    disabled($('#cbo_gastopagos_id'));
    $('#compracontadoc_elegirgasto_mensaje').hide(100);
}

function buscar_credito(){
	var tipo_credito_id = parseInt( $('#cmb_creditotipo_id').val() );
	var credito_id		= parseInt( $('#txt_credito_id').val() );
	if(tipo_credito_id != 0 && !isNaN(credito_id)){
		//console.log(tipo_credito_id + ' - '+ credito_id + ' gaa');
		$.ajax({
			type: "POST",
			url: VISTA_URL + "compracontadoc/compracontadoc_controller.php",
			async: true,
			dataType: "json",
			data: ({
				action: 'buscar_credito',
				tipo_credito_id: tipo_credito_id,
				credito_id: credito_id
			}),
			beforeSend: function () {
				$('#compracontadoc_elegirgasto_mensaje').removeClass('callout-warning').addClass('callout-info').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
				$('#btn_guardar_compracontadoc_elegirgasto').prop('disabled', true);
			},
			success: function (data) {
				if (parseInt(data.estado) > 0) {
					setTimeout(function () {
						//acciones a realizar // tb_moneda_nom tb_credito_preaco tb_cliente_nom
						$('#compracontadoc_elegirgasto_mensaje').hide(100);
						$('#btn_guardar_compracontadoc_elegirgasto').prop('disabled', false);

						$('#txt_credito_cliente').val(data.data['tb_cliente_nom']);
						
						disabled($('#cmb_creditotipo_id, #txt_credito_id'));
					}, 500);
                    if($('#hdd_vista').val() == 'compracontadoc_form'){
                        buscargastospor_tipocreditoid_creditoid(tipo_credito_id, credito_id);
                        $('#cbo_gastos_id').prop('disabled', false);
                    }
				}
				else {
					$('#compracontadoc_elegirgasto_mensaje').removeClass('callout-info').addClass('callout-warning');
					$('#compracontadoc_elegirgasto_mensaje').html('<h4>Alerta: '+ data.mensaje +'</h4>');
					$('#btn_guardar_compracontadoc_elegirgasto').prop('disabled', false);
				}
			},
			error: function (data) {
				//console.log(data);
				$('#compracontadoc_elegirgasto_mensaje').removeClass('callout-info').addClass('callout-danger')
				$('#compracontadoc_elegirgasto_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
			}
		});
	}
}

function buscargastospor_tipocreditoid_creditoid(tipocredito_id, credito_id) {
	var destino = (parseInt(tipocredito_id) > 1) ? "gasto" : "gastogar";
    $.ajax({
        type: "POST",
        url: VISTA_URL + `${destino}/${destino}_select.php`,
        async: true,
        dataType: "html",
        data: ({
			action: $('#action_elegirgasto').val(),
			vista: $('#hdd_vista').val(),
            creditotipo_id: tipocredito_id,
            credito_id: credito_id,
			gasto_id: $('#hdd_gasto_id').val()
        }),
        beforeSend: function () {
            $('#cbo_gastos_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cbo_gastos_id').html(data);
			if (parseInt($('#cmb_creditotipo_id').val()) == 3 && $('#action_elegirgasto').val() == 'L') {
				cargar_gastopagos($('#cbo_gastos_id option:selected').val());
			}
        },
		complete: function(){
			if ($('#action_elegirgasto').val() == 'I') {
				$('#cbo_gastos_id.select2').select2('open');
			} else {
				disabled($('#cbo_gastos_id'));
			}
		},
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function cargar_gastopagos(gasto_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + `gastopago/gastopago_select.php`,
        async: true,
        dataType: "html",
        data: ({
			action: $('#action_elegirgasto').val(),
			vista: $('#hdd_vista').val(),
			gasto_id: gasto_id,
			gastopago_id: $('#hdd_gastopago_id').val()
        }),
        beforeSend: function () {
            $('#cbo_gastopagos_id').html('<option value="0">Cargando...</option>');
        },
        success: function (data) {
			if(data.length > 1){
				$('.row_gastopago').show(150);
				$('#cbo_gastopagos_id').html(data);
				if($('#action_elegirgasto').val() == 'I'){
					$('#cbo_gastopagos_id').removeAttr('disabled');
				}
			} else {
				$('.row_gastopago').hide(150);
			}
        },
		complete: function(){
			if ($('#action_elegirgasto').val() != 'I') {
				disabled($('#cbo_gastopagos_id'));
			} else {
				//$('#cbo_gastopagos_id.select2').select2('open');
			}
		},
        error: function (data) {
            alert(data.responseText);
        }
    });
}