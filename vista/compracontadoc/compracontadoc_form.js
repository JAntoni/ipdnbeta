var cantidad_clicks = 0;
var opciones_del_select_empresa = null;
var verificar_proveedor_antesderegistrar = true;

//funcion para revisar si hay tc registrado en la fecha seleccionada
function monedacambio_tipo_cambio() {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "monedacambiocontable/monedacambiocontable_controller.php",
		async: true,
		dataType: "json",
		data: ({
			action: 'tipocambio',
			monedacambio_fec: $('#compracontadoc_emi_picker').val(),
			moneda_id: $('#cmb_moneda_id').val()
		}),
		beforeSend: function () {
			$('#txt_tipcam_cont_tipcam').val('Consultando...');
		},
		success: function (data) {
			if (parseInt(data.estado) == 1) {
				$('#txt_tipcam_cont_tipcam').val(data.valor_compra);
				$('#hdd_monedacambiocontable_idfk').val(data.id);
				$("#salto").remove();
				$('#span_error').hide(200);
			} else {
				$('#txt_tipcam_cont_tipcam').val('');
				$('#span_error').show(200);
				var link = ' <a href="javascript:void(0)" onclick="monedacambiocontable_form(\'I\', 0, \'' + data.fecha + '\')" style="color: black;"> Registrar TC</a>'
				$('#span_error').html(data.mensaje + link);
				if($('#salto').length == 0){
					$('#fila-moneda-igv').after("<br id='salto'>");
				}
			}
		},
		error: function (data) {
			$('#span_error').show(200);
			$('#span_error').html('ERROR AL CONSULTAR: ' + data.responseText);
		}
	});
}

function monedacambiocontable_form(usuario_act, monedacambiocontable_id, fecha) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambiocontable/monedacambiocontable_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            monedacambiocontable_id: monedacambiocontable_id,
            vista: 'compracontadoc',
			fecha: fecha
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_monedacambiocontable_form').html(data);
                $('#modal_registro_monedacambiocontable').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_monedacambiocontable'); //funcion encontrada en public/js/generales.js

                modal_hidden_bs_modal('modal_registro_monedacambiocontable', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_height_auto('modal_registro_monedacambiocontable'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'monedacambiocontable';
                var div = 'div_modal_monedacambiocontable_form';
                permiso_solicitud(usuario_act, monedacambiocontable_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
        }
    });
}

$(document).ready(function(){
	if($('#txt_tipcam_cont_tipcam').val() == ''){
		monedacambio_tipo_cambio();
	}
	$('#form_compracontadoc').find('input[type="text"], select, textarea').addClass("input-sm");
	$('label').css('font-size', 11.5);
	
	$("select[id*='cbo_sede_0'] option[value='0']").text("");
	opciones_del_select_empresa = $("select[id*='cbo_sede_0']")[0].innerHTML; //[0] .parentElement .innerHTML

	cambiar_lbl_importe_total(1);

	$('.cant-dias').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '365'
    });

    $('.moneda').focus(function(){
		formato_moneda(this);
	});

	$('.porc').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '100'
    });

	$('#txt_doc_condicion').val('CONTADO');
	disabled( $('#form_compracontadoc').find($('.disabled')) );
	
	/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
	$('#txt_dias_venc')
	.keyup(function(event){
		validar_fecha_venc(this.value);
	})
	.blur(function(){
		if( $('#compracontadoc_emi_picker').val() == ''){
			$('#compracontadoc_emi_picker').focus();
		}
	}); */

	$('#txt_porc_igv')
	.keyup(function(){
		calcular_montos();
	});

	/* $('#txt_monto_base_red, #txt_monto_igv_red')
	.keyup(function(){
		if( $('#che_afecta').prop('checked') ){
			calcular_montos_base_igv(1);
			console.log('pasapor aqui');
		}
	}); */

	
	//COMPLETAR FILAS PARA DOCUMENTOS QUE TIENEN MÁS DE 3 FILAS
	var array_detalles = JSON.parse($('#hdd_compracontadetalle_array').val());
	var condicion = array_detalles == null;
	var cont = 0;
	if(!condicion){
		completar_valores_gasto(
			array_detalles[0]['tipocredito_id'], array_detalles[0]['credito_id'], array_detalles[0]['gasto_id'],
			0, array_detalles[0]['proveedor_id'], array_detalles[0]['garantia_nom'], array_detalles[0]['monto_gasto'], array_detalles[0]['gastopago_id']
		);
		if(array_detalles.length > 3){
			while(cont < array_detalles.length - 3){
				nuevo_detalle(0);
				cont++;
				$(`#txt_detalleid_${cont}`).val(array_detalles[cont]['tb_compracontadetalle_id']);
				$(`#txt_detalleorden_${cont}`).val(array_detalles[cont]['tb_compracontadetalle_orden']);//tb_compracontadetalle_orden
				$(`#txt_codcuentacont_${cont}`).val(array_detalles[cont]['tb_cuentaconta_cod']);
				$(`#txt_glosa_${cont}`).val(array_detalles[cont]['tb_compracontadetalle_glosa']);
				$(`#txt_monto_${cont}`).val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(parseFloat(array_detalles[cont]['tb_compracontadetalle_monto'])));
				$(`#cbo_sede_${cont}`).val(parseInt(array_detalles[cont]['tb_empresa_idfk']));
	
				completar_valores_gasto(
					array_detalles[cont]['tipocredito_id'], array_detalles[cont]['credito_id'], array_detalles[cont]['gasto_id'],
					cont, array_detalles[cont]['proveedor_id'], array_detalles[cont]['garantia_nom'], array_detalles[cont]['monto_gasto'], array_detalles[cont]['gastopago_id']
				);
			}
		}
	}
	if($('#action').val() != 'insertar'){
		empresa_0 = parseInt(array_detalles[0]['tb_empresa_idfk']);
		$('#cbo_sede_0').val(empresa_0);
	}
	else{ //accion es insertar
		if(condicion){
			$('#txt_detalleorden_0').val(1);
			$('#txt_detalleorden_1').val(2);
			$('#txt_detalleorden_2').val(3);
		}
		else {
			empresa_0 = parseInt(array_detalles[0]['tb_empresa_idfk']);
			$('#cbo_sede_0').val(empresa_0);
			$('#txt_doc_serie').val(rellenar_serie_ceros($('#txt_doc_serie').val()));
			rellenar_numero_ceros($('#txt_doc_numero'));
			calcular_montos();
		}
	}

	$('input[name*="che_afecta"]').on('ifChanged',function(){
		if (this.checked) {
			$('#txt_porc_igv').val(18);
			$('.es_afecta').show(200);
			$('#txt_porc_igv').focus();
			cambiar_labels('lbl-afecta', 'in');
		} else {
			$('.es_afecta').hide(200);
			$('#txt_porc_igv').val('');
			$('#che_detr').iCheck('uncheck');
			cambiar_labels('lbl-afecta', 'afecta');
			this.focus();
		}
		if( $('input [class*="para_calcular"]').val() !== '' ){
			calcular_montos();
		}
	});

	$('input[name*="che_detr"]').on('ifChanged',function(){
		if (this.checked) {
			$('.es_detr').show(200);
			$('#txt_const_detraccion').focus();
		} else {
			$('.es_detr').hide(200);
			limpiar_detraccion();
			this.focus();
		}
	});

	//autocompletar del proveedor
	$("#txt_proveedor_doc,#txt_proveedor_nom").autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
					VISTA_URL + "proveedor/proveedor_autocomplete.php",
					{term: request.term}, //
					response
					);
		},
		select: function (event, ui) {
			$('#hdd_proveedor_id').val(ui.item.tb_proveedor_id);
			$('#txt_proveedor_doc').val(ui.item.tb_proveedor_doc);
			$('#txt_proveedor_nom').val(ui.item.tb_proveedor_nom);
			event.preventDefault();
			disabled($('input[id*="txt_proveedor"]'));
			disabled($('#btn_agr'));

			//var inputs = $(this).closest('form').find(':focusable'); ///inputs.eq(inputs.index(this) + 8).focus();
			if( $('#txt_doc_cod').prop("disabled") == true){
				$('#txt_doc_serie').focus();
			}
			else{
				$('#txt_doc_cod').focus();
			}
		}
	});

	//autocompletar del tipo de documento
	$("#txt_doc_cod,#txt_doc_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "documentocontable/documentocontable_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            $('#txt_doc_cod').val(ui.item.tb_documentocontable_cod);
            $('#txt_doc_nom').val(ui.item.tb_documentocontable_desc);
            event.preventDefault();

			disabled($('#txt_doc_cod, #txt_doc_nom'));

			if( $('#txt_proveedor_doc').prop('disabled') == true ){
				$('#txt_doc_serie').focus();
			}
			else{
				$('#txt_proveedor_doc').focus();
			}
			if($('#txt_doc_cod').val()=='07'){
				$('.documento_ref').show(200);
			}
			else if($('#txt_doc_cod').val()=='14'){
				$('.fecha_ven').show(200);
			}
        }
    });

	//autocompletar del documento referido en las NC
	$("#txt_documento_ref").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "compracontadoc/compracontadoc_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
			$('#txt_docrefidfk').val(ui.item.tb_compracontadoc_id);
			$('#txt_docref_proveedorid').val(ui.item.tb_proveedor_idfk);
            $('#txt_documento_ref').val(ui.item.tb_compracontadoc_serie + '-' +ui.item.tb_compracontadoc_numero);
            $('#txt_docref_fechaemision').val(ui.item.tb_compracontadoc_fechaemision);
            event.preventDefault();

			$('#txt_motivo_nc').focus();
        }
    });

	//autocompletar de las cuentas contables
	$('input[id*="txt_codcuentacont"]')
	.focus(function(){
		autocompletar_cuenta_contable(this);
	})
	.blur(function(){
		var num = $(this)[0].id.split("_")[2];
		var txt_codcuenta = $(`#txt_codcuentacont_${num}`);
		buscar_cuentaconta(txt_codcuenta, num);
	});

	$('#compracontadoc_emi_picker')
	.datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    })
	.on('changeDate', function(e) {
		verificar_antiguedad_anio_mes();
		//validar_fecha_venc($('#txt_dias_venc').val()); //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
		monedacambio_tipo_cambio();
		configurar_fecha_venc(e.date.valueOf());
		$('#compracontadoc_venc_picker').prop('disabled', false);
		var fecha_emision = new Date(obtener_newDate($("#compracontadoc_emi_picker").val())).valueOf();
		var fecha_vencimiento = new Date(obtener_newDate($("#compracontadoc_venc_picker").val())).valueOf();
		if(fecha_emision > fecha_vencimiento ){
			//configurar el texto del txt_vencimiento
			$("#compracontadoc_venc_picker").val($("#compracontadoc_emi_picker").val());
		}
	});

	// CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
	$('#compracontadoc_venc_picker').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy"
    });

	$('#compracontadoc_detraccion_picker').datepicker({
		language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
	});

	$('#form_compracontadoc').validate({
		submitHandler: function (form) {
			var prohibido = verificar_antiguedad_anio_mes();
			if (prohibido == 1) {
				return;
			}

			//comprobar que monto_base (mon_original // soles) + monto_igv (mon_original // soles) = monto_total (mon_original // soles)
			//verificacion en moneda_original
			var nombre_moneda = $('#cmb_moneda_id option:selected').text(); // en soles: S/.

			var monto_base = quitar_comas_miles($('#txt_monto_base_red').val());
			var monto_igv = quitar_comas_miles($('#txt_monto_igv_red').val());
			var monto_total = quitar_comas_miles($('#txt_importe_total').val());

			var monto_base_sol = 0;
			var monto_igv_sol = 0;
			var monto_total_sol = 0;

			if (nombre_moneda !== 'S/.') {
				monto_base_sol = quitar_comas_miles($('#txt_monto_base_sol_red').val());
				monto_igv_sol = quitar_comas_miles($('#txt_monto_igv_sol_red').val());
				monto_total_sol = quitar_comas_miles($('#txt_importe_total_sol_red').val());
			}

			var suma_base_igv = quitar_comas_miles(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base + monto_igv));
			var suma_base_igv_sol = quitar_comas_miles(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base_sol + monto_igv_sol));

			if ((suma_base_igv != monto_total) || (suma_base_igv_sol != monto_total_sol)) {
				var lbl_monto_base = $('#lbl_monto_base_ori').text().slice(0, -7).toLowerCase();
				var lbl_monto_igv = $('#lbl_monto_igv_ori').text().slice(0, -7).toLowerCase();
				var lbl_monto_total = $('#lbl_monto_total_ori').text().slice(0, -7).toLowerCase();
				var mensaje = `La suma de ${lbl_monto_base} y ${lbl_monto_igv} no coincide con el ${lbl_monto_total}. En el resumen columna: `;

				if ((suma_base_igv != monto_total) && (suma_base_igv_sol != monto_total_sol)) {
					mensaje += 'US$ y S/.';
				}
				else if (suma_base_igv !== monto_total) {
					mensaje += nombre_moneda;
				}
				else {
					mensaje += 'S/.';
				}

				setTimeout(function () {
					alertas_error('VERIFIQUE', mensaje, 'small');
				}, 500);
				return;
			}

			var cont = 0;
			while (cont < $('#cantidad_filas_detalle').val()) {
				$('#txt_sedenombre_' + cont).val($('#cbo_sede_' + cont + ' option:selected').text());
				cont++;
			}

			cont = 0;
			var error = false, detallefilas_error = '';
			if(verificar_proveedor_antesderegistrar){
				$('#tbody').find('input[id*="hdd_gastoproveedorid"]').each(function(){
					var gastoprov_id = parseInt($(this).val());
					if(gastoprov_id > 0 && parseInt($('#hdd_proveedor_id').val()) !== gastoprov_id){
						error = true;
						var sep = cont > 0 ? ", " : "";
						detallefilas_error += sep + (parseInt($(this)[0].id.split("_")[2])+1);
						cont++;
					}
				});
				if (error) {
					//notificar: hacer un OK / CANCEL -> si es cancel se hace return o exit del proceso, de lo contrario continúa al registro en controller
					Swal.fire({
						title: 'ALERTA ¿DESEA GUARDAR DE TODAS FORMAS EL DOCUMENTO?',
						text: `Proveedor del documento no coincide con proveedor de gastos. En Detalle de compra, fila(s) ${detallefilas_error}.`,
						icon: 'warning',
						showCloseButton: true,
						showCancelButton: false,
						focusConfirm: false,
						confirmButtonText: '<i class="fa fa-check"></i> SI',
						showDenyButton: true,
						denyButtonText: '<i class="fa fa-band"></i> NO',
						confirmButtonColor: '#3b5998',
						denyButtonColor: '#DD3333',
					}).then((result) => {
						if (result.isConfirmed) {
							verificar_proveedor_antesderegistrar = false;
						}
					});
				}
			}
			
			if(verificar_proveedor_antesderegistrar && error){
				return;
			}

			var elementos_disabled = $('#form_compracontadoc').find('input:disabled, select:disabled').removeAttr('disabled');
			var form_serializado = $('#form_compracontadoc').serialize();
			elementos_disabled.attr('disabled', 'disabled');
			//console.log( form_serializado ); //return;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "compracontadoc/compracontadoc_controller.php",
				async: true,
				dataType: "json",
				data: form_serializado,
				beforeSend: function () {
					$('#compracontadoc_mensaje').show(400);
					$('#btn_guardar_compracontadoc').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0 && parseInt(data.estado) != 3) {
						$('#compracontadoc_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#compracontadoc_mensaje').html('<h4>' + data.mensaje + '</h4>');
						setTimeout(function () {
							if (parseInt(data.estado) != 2) {
								compracontadoc_tabla();
								if($('#lista_gastos_registrarccd').length > 0){
									consultar_datos();
								}
							}
							$('#modal_registro_compracontadoc').modal('hide');
						}, 1000 );
					}
					else {
						$('#compracontadoc_mensaje').removeClass('callout-info').addClass('callout-warning')
						$('#compracontadoc_mensaje').html('<h4>Alerta: ' + data.mensaje + '</h4>');
						$('#btn_guardar_compracontadoc').prop('disabled', false);
					}
				},
				complete: function (data) {
				},
				error: function (data) {
					$('#compracontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#compracontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
		},
		rules: {
			txt_proveedor_doc: {
				required: function(){
					return $('#hdd_proveedor_id').val() == '';
				},
				minlength: 8,
				maxlength: 11
			},
			txt_doc_cod: {
				required: true,
				minlength: 2
			},
			txt_doc_serie: {
				required: true,
				minlength: 3,
				maxlength: 4
			},
			txt_doc_numero: {
				required: true
			},
			compracontadoc_emi_picker: {
				required: true
			},
			/* //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
			txt_dias_venc: {
				required: true
			}, */
			cmb_mesconta_id: {
				required: true,
				min: 1
			},
			txt_porc_igv: {
				required: function () {
					return $('#che_afecta').prop('checked');
				}
			},
			txt_importe_total: {
				required: true
			},
			txt_monto_base_red: {
				required: true
			},
			txt_monto_igv_red: {
				required: true
			},
			txt_importe_total_sol_red: {
				required: function () {
					return (parseInt($('#cmb_moneda_id').val()) == 2) ? true : false;
				}
			},
			txt_monto_base_sol_red: {
				required: function () {
					return (parseInt($('#cmb_moneda_id').val()) == 2) ? true : false;
				}
			},
			txt_monto_igv_sol_red: {
				required: function () {
					return (parseInt($('#cmb_moneda_id').val()) == 2) ? true : false;
				}
			},
			txt_documento_ref: {
				required: function () {
					return !$('.documento_ref').is(':hidden');
				}
			}
		},
		messages: {
			txt_proveedor_doc: {
				required: "Ingrese el proveedor",
				minlength: "El RUC/DNI debe tener al menos 8 caracteres"
			},
			txt_doc_cod: {
				required: "Ingrese el tipo de documento",
				minlength: "El código del documento debe tener 2 caracteres"
			},
			txt_doc_serie: {
				required: "Ingrese la serie",
				minlength: "Ingrese al menos 3 dígitos"
			},
			txt_doc_numero: {
				required: "Ingrese el numero de documento"
			},
			compracontadoc_emi_picker: {
				required: "Seleccione la fecha de emision"
			},
			/* //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
			txt_dias_venc: {
				required: "Ingrese cant. dias para vencimiento"
			}, */
			cmb_mesconta_id: {
				required: "Seleccione el periodo contable",
				min: "Seleccione el periodo contable"
			},
			txt_porc_igv: {
				required: "Ingrese el % IGV"
			},
			txt_importe_total: {
				required: "Ingrese importe total"
			},
			txt_monto_base_red: {
				required: "Ingrese monto base con redondeo"
			},
			txt_monto_igv_red: {
				required: "Ingrese monto igv con redondeo"
			},
			txt_importe_total_sol_red: {
				required: "Ingrese importe total soles redondeo"
			},
			txt_monto_base_sol_red: {
				required: "Ingrese monto base soles redondeo"
			},
			txt_monto_igv_sol_red: {
				required: "Ingrese monto igv soles redondeo"
			},
			txt_documento_ref: {
				required: "Ingrese el documento origen"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			}
			else if (element.parent("div.input-group").length == 1) {
				error.insertAfter(element.parent("div.input-group")[0]);
			}
			else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});
	// jQuery button click event to remove a row.
	$('#tbody')
	.on('click', '.remove', function () {
		var filas_detalle =  parseInt($('#cantidad_filas_detalle').val()) - 1;
		$('#cantidad_filas_detalle').val(filas_detalle);
		var child = $(this).closest('tr').nextAll();

		// Iterating across all the rows obtained to change the index
		child.each(function () {
			// Getting <tr> id.
			var id = $(this).attr('id');

			// Gets the row number from <tr> id.
			var dig = parseInt(id.split('_')[3]);

			// Modifying row id.
			$(this).attr('id', `tabla_cabecera_fila_${dig - 1}`);

			//todos los hijos input, select, button se cambiará su id
			$(this).find('input, select, button, i, div[id*="div_gastomensaje"]').each(function () {
				//todos los id solo deben tener tres partes, donde la parte 3 es el num de orden empezando desde cero
				id_antiguo = $(this)[0].id.split('_')[2];
				$(this).attr('id', $(this)[0].id.replace(id_antiguo, (dig - 1)));
				$(this).attr('name', $(this)[0].id.replace(id_antiguo, (dig - 1)));
				if ($(this)[0].id.indexOf("orden") > -1) {//busca la palabra orden en el id
					$(this)[0].value--;
				}
			});
		});

		// Removing the current row.
		$(this).closest('tr').remove();

		// Decreasing total number of rows by 1.
		cantidad_clicks--;
		for (let i = 0; i <= cantidad_clicks; i++) {
			if($('#tbody').find('input.para_calcular')[i].value > 0){
				calcular_montos();
				break;
			}
		}
	})
	.on('keyup', '.para_calcular', function(){
		calcular_montos();
	})
	.on('keyup', '.monto_total', function(){
		copiar_monto = quitar_comas_miles($(this)[0].value);
		$('#txt_importe_total').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(copiar_monto));
		var tipo_cambio = parseFloat($('#txt_tipcam_cont_tipcam').val());
		$('#txt_importe_total_sol_red').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(copiar_monto * tipo_cambio));
	})
	.on('keyup', '.monto_igv', function(){
		copiar_monto = quitar_comas_miles($(this)[0].value);
		$('#txt_monto_igv_red').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(copiar_monto));
		var tipo_cambio = parseFloat($('#txt_tipcam_cont_tipcam').val());
		$('#txt_monto_igv_sol_red').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(copiar_monto * tipo_cambio));

		calcular_txt_total();
	});

	if($('#action').val() == 'leer' || $('#action').val() == 'eliminar'){
		disabled($('#form_compracontadoc').find("button[class*='btn-sm']")); //attr('disabled', 'disabled') // aqui esta el error
		disabled($('#form_compracontadoc').find("select"));
		disabled($('#form_compracontadoc').find("input, textarea"));
		disabled($('#form_compracontadoc').find("checkbox"));
		disabled($('#form_compracontadoc').find("a, i.fa-trash, i.fa-plus"));
		$('#form_compracontadoc').find("input").css("cursor", "text");
	}
	else if($('#action').val() == 'modificar'){
		disabled($('input[id*="txt_proveedor"], #txt_doc_cod, #txt_doc_nom'));
		disabled($('#btn_agr'));
	}
	else if($('#action').val() == 'insertar'){
		disabled($("#compracontadoc_venc_picker"));
		if (!condicion) {
			disabled($('input[id*="txt_proveedor"], #txt_doc_cod, #txt_doc_nom'));
			disabled($('#btn_agr'));
		}
	}

	mostrar_ocultar_rowdetraccion();
	
	configurar_fecha_venc((new Date(obtener_newDate($('#compracontadoc_emi_picker').val()))).valueOf());
	if($("#fecvenc_vacio").val() == 1){
		$("#compracontadoc_venc_picker").val('').datepicker('option', { minDate: null, maxDate: null });
	}

	$('#tbody').on('click', 'i.fa-eye, i.fa-plus:not([disabled])', function(){
		var act = $(this)[0].className.indexOf('fa-plus') > 0 ? "I" : "L";
		var posicion = $(this).parents('tr').find('input[id*="txt_detalleorden"]').val() - 1;
		var tipocredito_id = 0;
		var credito_id = 0;
		var gasto_id = 0;
		var gastopago_id = 0;
		if (act == 'L') {
			tipocredito_id = $(`#hdd_tipocreditoid_${posicion}`).val();
			credito_id = $(`#hdd_creditoid_${posicion}`).val();
			gasto_id = $(`#hdd_gastoid_${posicion}`).val();
			gastopago_id = $(`#hdd_gastopagoid_${posicion}`).val();
		}
		anexar_gasto_form(act, tipocredito_id, credito_id, gasto_id, posicion, gastopago_id);
	})
	.on('click', 'i.fa-trash:not([disabled])', function(){
		var posicion = $(this).parents('tr').find('input[id*="txt_detalleorden"]').val() - 1;
		completar_valores_gasto(0, 0, 0, posicion, 0, '', '', 0);
	});
});

function obtener_newDate(param){
	var dia_selecc = param.split("-")[0];
	var mes_selecc = param.split("-")[1];
	var anio_selecc = param.split("-")[2];

	return mes_selecc+'-'+dia_selecc+'-'+anio_selecc;
}

function configurar_fecha_venc(e){
	var minDate = new Date(e);
	$('#compracontadoc_venc_picker').datepicker('setStartDate', minDate);
}

function proveedor_form(usuario_act, proveedor_id){
	var doc_proveedor_id = Number($('#hdd_proveedor_id').val());
  
	if (parseInt(doc_proveedor_id) <= 0 && usuario_act == 'M') {
		alerta_warning('Información', 'Debes buscar por documento o nombres');
		return false;
	}
  
	$.ajax({
		type: "POST",
		url: VISTA_URL + "proveedor/proveedor_form.php",
		async: true,
		dataType: "html",
		data: ({
			action: usuario_act, // PUEDE SER: L, I, M , E
			proveedor_id: doc_proveedor_id,
			vista: 'compracontadoc'
		}),
		beforeSend: function () {
			$('#h3_modal_title').text('Cargando Formulario');
			$('#modal_mensaje').modal('show');
		},
		success: function (data) {
			$('#modal_mensaje').modal('hide');
			if (data != 'sin_datos') {
				$('#div_modal_proveedor_form').html(data);
				$('#modal_registro_proveedor').modal('show');

				//desabilitar elementos del form si es L (LEER)
				if (usuario_act == 'L' || usuario_act == 'E')
					form_desabilitar_elementos('form_proveedor'); //funcion encontrada en public/js/generales.js

				//funcion js para limbiar el modal al cerrarlo
				modal_hidden_bs_modal('modal_registro_proveedor', 'limpiar'); //funcion encontrada en public/js/generales.js
				//funcion js para agregar un largo automatico al modal, al abrirlo
				modal_height_auto('modal_registro_proveedor'); //funcion encontrada en public/js/generales.js
				//funcion js para agregar un ancho automatico al modal, al abrirlo
				modal_width_auto('modal_registro_proveedor', 75); //funcion encontrada en public/js/generales.js
			}
			else {
				//llamar al formulario de solicitar permiso
				var modulo = 'proveedor';
				var div = 'div_modal_proveedor_form';
				permiso_solicitud(usuario_act, proveedor_id, modulo, div); //funcion ubicada en public/js/permiso.js
			}
		},
		error: function (data) {
			alerta_error('ERROR', data.responseText)
		}
	});
}

function llenar_datos_proveedor(data){
	$('#hdd_proveedor_id').val(data.registro['tb_proveedor_id']);
	$('#txt_proveedor_doc').val(data.registro['tb_proveedor_doc']);
	$('#txt_proveedor_nom').val(data.registro['tb_proveedor_nom']);

	disabled($('input[id*="txt_proveedor"]'));
	disabled($('#btn_agr'));

	$('#txt_doc_cod').focus();
	$('#txt_proveedor_doc-error').hide(50);
}

function llenar_datos_monedacambiocontable(data){
	$('#hdd_monedacambiocontable_idfk').val(data.registro['tb_monedacambiocontable_id']);
	$('#txt_tipcam_cont_tipcam').val(data.registro['tb_monedacambiocontable_comprasunat']);

	disabled($('#txt_tipcam_cont_tipcam'));
	$('#span_error').hide(200);
	//$('#txt_doc_cod').focus(); //siguiente
}

function formato_serie_contable(id_txt, txt_serie){
	//id_txt contiene el id de la caja de texto que contiene el codigo de documento
	//txt_serie es la caja de texto donde se dará el formato
	var cod_doc = $('#'+id_txt).val();
	$('#'+txt_serie.id).keydown(function(event){
		var codigo = event.which || event.keyCode;
		var text = txt_serie.value.toUpperCase();
		if(text.length +1 > 4){ // maximo de caracteres
			if(codigo !==8 && codigo !==9 && !(codigo > 36 && codigo < 41)) // 8: tecla borrar, 9: tecla TAB, 36 al 41 son flechas
				return false;
		}
		else{
			if(text.length +1 > 2 ){//despues de segundo digito solo numeros
				if ((codigo < 48 || codigo > 57) && (codigo < 96 || codigo > 105) && codigo !==8 && codigo !==9 && !(codigo > 36 && codigo < 41)) {return false;}
			}
			else if (text.length +1 < 2){ //primer digito debe ser letra
				if(!(codigo > 64 && codigo < 91) && codigo !==9 && !(codigo > 36 && codigo < 41)) {return false;}
			}
			else{
				var key = event.key;
				if(key){
					key = key.toLowerCase();
					const isLetter = (key >= "a" && key <= "z");
					const isNumber = (key >= "0" && key <= "9");
					if (!(isLetter || isNumber)) {
						return false;
					}
				}
			}
		}
	});
	$('#'+txt_serie.id).blur(function(){// cuando pierde el foco se autocompleta
		$(txt_serie).val(rellenar_serie_ceros($(txt_serie).val()));
	});
}

function rellenar_serie_ceros(txt_serie){
	if(txt_serie.length > 2){
		var caracter_2 = txt_serie.slice(1,2);
		if(solo_letras(caracter_2)){
			txt_serie = txt_serie.slice(0,2)+'0'+txt_serie.slice(-1);
		} else {
			txt_serie = txt_serie.slice(0,1)+'0'+txt_serie.slice(-2);
		}
	}
	return txt_serie;
}

function formato_numero_contable(txt_numero){
	$('.numero-doc').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '99999999'
    });
	$('#'+txt_numero.id).blur(function(){ // cuando pierde el foco se autocompleta
		rellenar_numero_ceros(txt_numero);
	});
}

function rellenar_numero_ceros(txt_numero){
	if($(txt_numero)[0].value.length > 0){
		var i=0, completar='';
		var cantidad_falta = 8 - $(txt_numero)[0].value.length;
		while(i<cantidad_falta){
			completar+='0';
			i++;
		}
		$(txt_numero)[0].value = completar+$(txt_numero)[0].value;
	}
	verificar_documento_existente();
}


function verificar_documento_existente(){
	const action_original = $('#action').val();
	$('#action').val(action_original +'_verificar_documento');
	var elementos_disabled = $('#form_compracontadoc').find('input:disabled, select:disabled').removeAttr('disabled');
	var form_serializado = $('#form_compracontadoc').serialize();
	elementos_disabled.attr('disabled', 'disabled');
	$('#action').val(action_original);
	$.ajax({
		type: "POST",
		url: VISTA_URL + "compracontadoc/compracontadoc_controller.php",
		async: true,
		dataType: "json",
		data: form_serializado,
		beforeSend: function () {
			$('#compracontadoc_mensaje').show(400);
			$('#btn_guardar_compracontadoc').prop('disabled', true);
		},
		success: function (data) {
			if (parseInt(data.estado) > 0) {
				// cambiar TOAST por SWAL
				setTimeout(function () {
					//notificacion_warning(data.mensaje, 7000);
					swal_warning('VERIFIQUE', data.mensaje,6000);
				}, 500
				);
			}
		},
		complete: function (data) {
			$('#compracontadoc_mensaje').hide(100);
			$('#btn_guardar_compracontadoc').prop('disabled', false);
		},
		error: function (data) {
			$('#compracontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
			$('#compracontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}

function limpiar_proveedor(){
	$('#hdd_proveedor_id').val('');
	$('#txt_proveedor_doc').val('');
	$('#txt_proveedor_nom').val('');

	$('#txt_proveedor_doc').prop('disabled', false);
	$('#txt_proveedor_nom').prop('disabled', false);
	$('#btn_agr').removeAttr('disabled');// attr('', '')
	$('#txt_proveedor_doc').focus();
	limpiar_doc_ref(0);
}

function limpiar_tipodoc(){
	$('#txt_doc_cod').val('');
	$('#txt_doc_nom').val('');
	$('txt_motivo_nc').val('');

	$('#txt_doc_cod').prop('disabled', false);
	$('#txt_doc_nom').prop('disabled', false);
	$('#txt_doc_serie').focus();
	$('#txt_doc_cod').focus();
	$('.documento_ref').hide(200);
	$('.fecha_ven').hide(200);
	limpiar_doc_ref(1);
}

function limpiar_doc_ref(num){
	$('#txt_documento_ref').val('');
	$('#txt_docref_fechaemision').val('')
	.datepicker('option', {minDate: null, maxDate: null});
	if(num == 1) { $('#txt_motivo_nc').val('') }
}

function limpiar_detraccion(){
	$('#txt_const_detraccion').val('');
	$('#compracontadoc_detraccion_picker').val('')
	.datepicker('option', {minDate: null, maxDate: null});
}

$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
	checkboxClass: 'icheckbox_flat-green',
	radioClass: 'iradio_flat-green'
});

$('#cmb_moneda_id').change(function(){
    cambiar_lbl_importe_total(6);
	var moneda_id = parseInt($(this).val());
	if(moneda_id == 2){
		$('#box_redim').width('48.4%');
		$('div[id*="div_mon_ori_"]').removeClass('col-md-12').addClass('col-md-6');
		$('.moneda-dolar').show(200);
	}else{
		$('.moneda-dolar').hide(100);
		$('div [id*="div_mon_ori"]').removeClass('col-md-6').addClass('col-md-12');
		$('#box_redim').width('24.2%');
	}
	if ($('#compracontadoc_emi_picker').val() !== ''){
		monedacambio_tipo_cambio();
	}
	else{
		$('#txt_tipcam_cont_tipcam').val('');
		$('#compracontadoc_emi_picker').focus();
	}
});

function cambiar_lbl_importe_total(num){
	arreglo = $('#form_compracontadoc').find("label[class*='lbl-moneda']");
	Object.keys(arreglo).forEach(element => {
		index = parseInt(element);
		if(Number.isInteger(index)){
			const texto_actual = arreglo[index].innerHTML.slice(0,-num);
			var moneda_nom = '('+$('select[name="cmb_moneda_id"] option:selected').text() +'):';
			arreglo[index].innerHTML = texto_actual + moneda_nom;
		}
	});
}

/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
function validar_fecha_venc(text){
	var dias_para_venc = parseInt($('#txt_dias_venc').val());
	if (text.length > 0) {
		if ($('#compracontadoc_emi_picker').val() != '') {
			recalcular_fecha_fin($('#compracontadoc_emi_picker').val(), dias_para_venc, $('#compracontadoc_venc_picker'));
		}
	}
	else {
		$('#compracontadoc_venc_picker').val('')
			.datepicker('option', { minDate: null, maxDate: null });
	}
} */

//puede ir para los generales
function recalcular_fecha_fin(sFecha, dias, objet){
	var fecha = sFecha.split("-");
	fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
	fecha = new Date(fecha);
	fecha.setDate(fecha.getDate() + dias+1);
	nueva_fecha = ((fecha.getDate() > 9) ? fecha.getDate() : ('0' + fecha.getDate())) + '-' + ((fecha.getMonth() > 8) ? (fecha.getMonth() + 1) : ('0' + (fecha.getMonth() + 1))) + '-' + fecha.getFullYear();
	objet.datepicker("setDate",nueva_fecha);
}

function cambiar_labels(nombre_clase, texto){
	texto = texto.toUpperCase();
	arreglo = $('#form_compracontadoc').find("label[class*='"+nombre_clase+"']");
	Object.keys(arreglo).forEach(element => {
		index = parseInt(element);
		if(Number.isInteger(index)){
			texto_a_poner = texto=='IN' ? "" : "INAFECTA";
			nuevo_texto = arreglo[index].innerHTML.replace(texto, texto_a_poner, "ig") ;
			arreglo[index].innerHTML = nuevo_texto;
		}
	});
}

function quitar_comas_miles(valor){
	return parseFloat(valor.replace(/,/g, ""));
}

function calcular_montos() {
	var monto_igv = 0, monto_base = 0;
	var porc_igv = $('#txt_porc_igv').val() == '' ? 0 : parseInt($('#txt_porc_igv').val()) / 100;
	//calcular el igv en la tabla, y calcular monto total en la tabla
	var filas_articulos = $('input[id*="txt_codcuentacont"].disabled').closest('tr').prevAll(); //son articulos o servicios comprados, con los cuales se calculará el monto igv y monto total
	// Iterating across all the rows obtained to change the index
	filas_articulos.each(function () {
		//obtener la columna del monto
		var monto_articulo = $(this).children('.monto').children('.moneda').val();
		monto_articulo = monto_articulo === '' ? 0 : quitar_comas_miles(monto_articulo);

		monto_igv += (monto_articulo * porc_igv);
		monto_base += monto_articulo;
	});

	var monto_igv1 = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_igv);

	//cambiar el valor del input monto_igv en la tabla
	$('input[id*="txt_codcuentacont"].disabled').closest('tr').children('.monto').children('.moneda').val(monto_igv1);
	
	var monto_base1 = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base);
	//$('#txt_').val( new Intl.NumberFormat('es-PE', { style: 'currency', currency: 'PEN' }).format(parseFloat(calcular_monto_igv)) ); // more info https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat/NumberFormat#examples
	
	$('#txt_monto_base_red').val(monto_base1);
	$('#txt_monto_igv_red').val(monto_igv1);
	
	calcular_txt_total();
	var moneda_id = parseInt($('#cmb_moneda_id').val());
	if (moneda_id == 2) {
		if ($('#txt_tipcam_cont_tipcam').val() !== '') {
			calcular_montos_base_igv();
		}
	}
	mostrar_ocultar_rowdetraccion();
}

function calcular_txt_total() {
	var monto_total = 0;
	$('input[class*="monto_total"]').closest('tr').prevAll().each(function () {
		valor_a_sumar = $(this).children('.monto').children('.moneda').val();
		valor_a_sumar = valor_a_sumar === '' ? 0 : quitar_comas_miles(valor_a_sumar);

		monto_total += valor_a_sumar;
	});
	var monto_total1 = new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_total);
	$('input[class*="monto_total"]').val(monto_total1);

	$('#txt_importe_total').val(monto_total1);
	var moneda_id = parseInt($('#cmb_moneda_id').val());
	if (moneda_id == 2) {
		if ($('#txt_tipcam_cont_tipcam').val() !== '') {
			var tipo_cambio = parseFloat($('#txt_tipcam_cont_tipcam').val());
			$('#txt_importe_total_sol_red').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_total * tipo_cambio));
		}
	}
}

function calcular_montos_base_igv(){
	var tipo_cambio = parseFloat($('#txt_tipcam_cont_tipcam').val());
	var monto_base_mon_ori = quitar_comas_miles($('#txt_monto_base_red').val());
	var monto_igv_mon_ori = quitar_comas_miles($('#txt_monto_igv_red').val());

	$('#txt_monto_base_sol_red').val( new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_base_mon_ori*tipo_cambio) );
	
	$('#txt_monto_igv_sol_red').val( new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(monto_igv_mon_ori*tipo_cambio) );
}

function mostrar_ocultar_rowdetraccion(){
	// DETRACCION
	var moneda_id = parseInt($('#cmb_moneda_id').val());
	var cod_doc = $("#txt_doc_cod").val();
	var caja_monto_total = moneda_id == 1 ? $('#txt_importe_total') : $('#txt_importe_total_sol_red');
	if ((/*(cod_doc == "AQUI COLOCAR EL COD CUENTA CONTABLE FLETE" && quitar_comas_miles(caja_monto_total.val()) >= 400) || */(quitar_comas_miles(caja_monto_total.val()) >= 700)) && $('#che_afecta').prop('checked')) {
		$('.detrac').show(200);
	}
	else {
		limpiar_detraccion();
		$('.detrac').hide(200);
	}
}

$('#cmb_mesconta_id').change(function(){
	if($('#cmb_mesconta_id').val() != 0){
		validar_mes_conta();
	}
	else{
		$('#txt_compraconta_correlativo').val('');
	}
});

function validar_mes_conta(){
	const action_original = $('#action').val();
	$('#action').val('validar_mes_conta');
	var elementos_disabled = $('#form_compracontadoc').find('input:disabled, select:disabled').removeAttr('disabled');
	var form_serializado = $('#form_compracontadoc').serialize();
	elementos_disabled.attr('disabled', 'disabled');
	$('#action').val(action_original);
	$.ajax({
		type: "POST",
		url: VISTA_URL + "compracontadoc/compracontadoc_controller.php",
		async: true,
		dataType: "json",
		data: form_serializado,
		beforeSend: function () {
		},
		success: function (data) {
			if (parseInt(data.estado) == 3) {
				$('#txt_compraconta_correlativo').val('');
				// cambiar TOAST por SWAL
				setTimeout(function () {
					//notificacion_warning(data.mensaje, 7000);
					swal_warning('VERIFIQUE', data.mensaje,6000);
				}, 500
				);
			}
			if (parseInt(data.estado) == 4){
				$('#txt_compraconta_correlativo').val(data.data);
			}
		},
		complete: function (data) {
		},
		error: function (data) {
			$('#compracontadoc_mensaje').removeClass('callout-info').addClass('callout-danger')
			$('#compracontadoc_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}

function autocompletar_cuenta_contable(element){
	$('#'+ element.id).autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
				VISTA_URL + "plancontable/plancontable_autocomplete.php",
				{ term: request.term },
				response
			);
		},
		select: function (event, ui) {
			$('#'+this.id).val(ui.item.tb_cuentaconta_cod);
			//$('#hdd_fil_cuentaconta_selected').val(JSON.stringify(ui.item));
			event.preventDefault();
		}
	});
}

function nuevo_detalle(num){
	cantidad_clicks++;
	var filas_detalle =  parseInt($('#cantidad_filas_detalle').val()) +1;
	$('#cantidad_filas_detalle').val(filas_detalle);
	var child = $('#txt_codcuentacont_' + (cantidad_clicks - 1)).closest('tr').nextAll();

	// Iterating across all the rows obtained to change the index
	child.each(function () {
		// Getting <tr> id.
		var id = $(this).attr('id');

		// Gets the row number from <tr> id.
		var dig = parseInt(id.split('_')[3]);

		// Modifying row id.
		$(this).attr('id', `tabla_cabecera_fila_${dig + 1}`);

		//todos los hijos input, select, button se cambiará su id
		$(this).find('input, select').each(function () {
			//todos los id solo deben tener tres partes, donde la parte 3 es el num de orden empezando desde cero
			var id_antiguo = $(this)[0].id.split('_')[2];
			$(this).attr('id', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
			$(this).attr('name', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
			if($(this)[0].id.indexOf("orden") > -1  && num > 0){
				$(this)[0].value ++;
			}
		});
	});

	$('#tabla_cabecera_fila_' + (cantidad_clicks - 1))
		.after(`<tr id="tabla_cabecera_fila_${cantidad_clicks}">
					<input type="hidden" name="txt_detalleid_${cantidad_clicks}" id="txt_detalleid_${cantidad_clicks}">
					<input type="hidden" name="txt_detalleorden_${cantidad_clicks}" id="txt_detalleorden_${cantidad_clicks}" value="${cantidad_clicks+1}">
					<td id="tabla_fila">
						<input type="text" name="txt_codcuentacont_${cantidad_clicks}" id="txt_codcuentacont_${cantidad_clicks}" class="form-control input-sm" onfocus="autocompletar_cuenta_contable(this)" onblur="buscar_cuentaconta($(this), ${cantidad_clicks})" placeholder="COD. CTA. CONT.">
					</td>
					<td id="tabla_fila"><input type="text" name="txt_glosa_${cantidad_clicks}" id="txt_glosa_${cantidad_clicks}" class="form-control mayus input-sm" placeholder="GLOSA"></td>
					<td id="tabla_fila" class="monto"><input type="text" name="txt_monto_${cantidad_clicks}" id="txt_monto_${cantidad_clicks}" class="form-control moneda input-sm para_calcular" onfocus="formato_moneda(this)" placeholder="0.00"></td>
					<td id="tabla_fila">
						<input type="hidden" name="txt_sedenombre_${cantidad_clicks}" id="txt_sedenombre_${cantidad_clicks}">
						<select name="cbo_sede_${cantidad_clicks}" id="cbo_sede_${cantidad_clicks}" class="form-control input-sm"> ${opciones_del_select_empresa} </select>
					</td>
					<td id="tabla_fila">
						<input type="hidden" name="hdd_tipocreditoid_${cantidad_clicks}" id="hdd_tipocreditoid_${cantidad_clicks}">
						<input type="hidden" name="hdd_creditoid_${cantidad_clicks}" id="hdd_creditoid_${cantidad_clicks}">
						<input type="hidden" name="hdd_tablagasto_${cantidad_clicks}" id="hdd_tablagasto_${cantidad_clicks}">
						<input type="hidden" name="hdd_gastoid_${cantidad_clicks}" id="hdd_gastoid_${cantidad_clicks}">
						<input type="hidden" name="hdd_gastopagoid_${cantidad_clicks}" id="hdd_gastopagoid_${cantidad_clicks}">
						<input type="hidden" name="hdd_gastoproveedorid_${cantidad_clicks}" id="hdd_gastoproveedorid_${cantidad_clicks}">
						<ul class="products-list product-list-in-box">
							<li>
								<div class="col-md-1" style="padding-left: 5px; padding-right: 7px;">
									<i class="fa fa-plus text-blue"></i><br>
									<i class="fa fa-trash text-red"></i>
								</div>
								<div id="div_gastomensaje_${cantidad_clicks}" class="col-md-11" style="padding-left: 7px; padding-right: 5px;">
								</div>
							</li>
						</ul>
					</td>
					<td id="tabla_fila" class="text-center"><button name="btn_eliminardetalle_${cantidad_clicks}" id="btn_eliminardetalle_${cantidad_clicks}" class="btn btn-sm btn-danger remove fa fa-trash" type="button" title="ELIMINAR"></button></td>
				</tr>`);
}

function verificar_antiguedad_anio_mes(){
	var d = new Date();
	var curr_month = d.getMonth() + 1; //Months are zero based
	var curr_year = d.getFullYear();

	var d_selected = $('#compracontadoc_emi_picker').val();
	var mes_selecc = d_selected.split("-")[1];
	var anio_selecc = d_selected.split("-")[2];

	var resta_anios = parseInt(curr_year) - parseInt(anio_selecc);
	var resta_meses = parseInt(curr_month) - parseInt(mes_selecc);
	if(resta_anios > 1){
		swal_warning('VERIFIQUE', "No se puede registrar documentos de antiguedad mayor a un año mes",6000);
		return 1;
	}
	else if(resta_anios == 1){
		if(resta_meses > 0){
			swal_warning('VERIFIQUE', "No se puede registrar documentos de antiguedad mayor a un año mes",6000);
			return 1;
		}
	}
}

function anexar_gasto_form(action, tipocredito_id, credito_id, gasto_id, posicion, gastopago_id) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "compracontadoc/compracontadoc_elegirgasto.php",
		async: true,
		dataType: "html",
		data: {
			action: action,
			vista: 'compracontadoc_form',
			tipocredito_id: tipocredito_id,
			credito_id: credito_id,
			gasto_id: gasto_id,
			gastopago_id: gastopago_id,
			posicion: posicion
		},
		beforeSend: function () {
			$("#h3_modal_title").text("Cargando Formulario");
			$("#modal_mensaje").modal("show");
		},
		success: function (html) {
			$("#div_modal_seleccionargasto_form").html(html);
			$("#div_modal_elegirgasto_form").modal("show");
			$("#modal_mensaje").modal("hide");

			modal_hidden_bs_modal("div_modal_elegirgasto_form", "limpiar"); //funcion encontrada en public/js/generales.js
			modal_width_auto("div_modal_elegirgasto_form", 30);
			modal_height_auto("div_modal_elegirgasto_form"); //funcion encontrada en public/js/generales.js
		}
	});
}

function completar_valores_gasto(cred_tip, cred_id, gasto_id, pos, gastoprov_id, garantia_nom, monto_gasto, gastopago_id) {
	var cred_tip_nom = [null, 'CM', 'CAV', 'CGV', 'CH'];
	var tabla_gasto = '';
	if(cred_tip > 0){
		tabla_gasto = cred_tip == 1 ? 'tb_gastogar' : 'tb_gasto';
	}
	var mensaje = '';
	if(cred_tip > 0){
		var extra = '';
		garantia_nom = garantia_nom.length > 15 ? garantia_nom.substring(0, 16) + '[...]' : garantia_nom;
		garantia_nom = garantia_nom.length > 0 ? `Garantia ${garantia_nom}. `: '';
		if(gastopago_id > 0){
			extra = '. Gastopago id:' + gastopago_id;
		}
		mensaje = `${cred_tip_nom[cred_tip]}-${cred_id}. ${garantia_nom}Gasto Id ${gasto_id}. Monto: ${monto_gasto}${extra}`;
	}

	$(`#hdd_tipocreditoid_${pos}`).val(cred_tip);
	$(`#hdd_creditoid_${pos}`).val(cred_id);
	$(`#hdd_tablagasto_${pos}`).val(tabla_gasto);
	$(`#hdd_gastoid_${pos}`).val(gasto_id);
	$(`#hdd_gastopagoid_${pos}`).val(gastopago_id);
	$(`#div_gastomensaje_${pos}`).html(mensaje);
	$(`#hdd_gastoproveedorid_${pos}`).val(gastoprov_id);

	if(gasto_id > 0){
		$(`#tabla_cabecera_fila_${pos}`).find("i.fa-plus").removeClass("fa-plus text-blue").addClass("fa-eye text-aqua");
	} else {
		$(`#tabla_cabecera_fila_${pos}`).find("i.fa-eye").removeClass("fa-eye text-aqua").addClass("fa-plus text-blue");
	}
}

function buscar_cuentaconta(caja_texto, num){
	if(caja_texto.val().length > 0){
		$.ajax({
			type: "POST",
			url: VISTA_URL + "compracontadoc/compracontadoc_controller.php",
			async: true,
			dataType: "json",
			data: {
				action: 'buscar_cuentaconta',
				cuenta_conta: caja_texto.val()
			},
			success: function (data) {
				if(data.estado == 0){
					notificacion_info(`Revise el detalle numero ${parseInt(num)+1}. ${data.mensaje}`, 4000);
				}
			}
		});
	}
}

function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            close: function () {
            }
        }
    });
}