<?php
$usuario_action = $_POST['action'];
$creditotipo_id = $_POST['tipocredito_id'];
$credito_id = !empty($_POST['credito_id']) ? $_POST['credito_id'] : '';
$gasto_id = $_POST['gasto_id'];//puede ser gastogar_id o gasto_id
$gastopago_id = $_POST['gastopago_id'];
$vista = $_POST['vista'];
$posicion = $_POST['posicion'];
?>

<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_elegirgasto_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">SELECCION DE GASTO</h4>
            </div>

            <form id="form_elegirgasto" method="post" style="font-size: 11px">
                <div class="modal-body">
                    <div class="box shadow">
                        <input type="hidden" name="action_elegirgasto" id="action_elegirgasto" value="<?php echo $usuario_action; ?>">
                        <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista; ?>">
                        <input type="hidden" name="hdd_posicion" id="hdd_posicion" value="<?php echo $posicion; ?>">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="cmb_creditotipo_id" class="control-label">Tipo de crédito:</label>
                                <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm mayus">
                                    <?php require_once('../creditotipo/creditotipo_select.php') ?>
                                </select>
                            </div>

                            <div class="col-md-5 form-group">
                                <label for="txt_credito_id">Id Crédito:</label>
                                <input type="text" name="txt_credito_id" id="txt_credito_id" placeholder="ID DEL CREDITO" class="form-control cred-id disabled input-sm" value="<?php echo $credito_id ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-9 form-group">
                                <label for="txt_credito_cliente">Cliente:</label>
                                <input type="text" name="txt_credito_cliente" id="txt_credito_cliente" placeholder="NOMBRE DEL CLIENTE" class="form-control disabled input-sm" value="<?php echo $credito_cliente_nombre ?>">
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="btn-limpiar-0" style="color:white;">l</label>
                                <span class="input-group-btn">
                                    <button id="btn-limpiar-0" class="btn btn-primary btn-sm" type="button" onclick="limpiar_credito()" title="LIMPIAR DATOS DEL CREDITO ANEXO">
                                        <span class="fa fa-eraser icon"></span>
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-11 form-group">
                                <div id="gasto_group" class="input-group">
                                    <input type="hidden" name="hdd_gasto_id" id="hdd_gasto_id" value="<?php echo $gasto_id;?>">
                                    <label for="cbo_gastos_id">Gastos de este Crédito:</label>
                                    <select name="cbo_gastos_id" id="cbo_gastos_id" class="select2 narrow wrap form-control">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row row_gastopago" hidden>
                            <div class="col-md-11 form-group">
                                <div id="gastopago_group" class="input-group">
                                    <input type="hidden" name="hdd_gastopago_id" id="hdd_gastopago_id" value="<?php echo $gastopago_id;?>">
                                    <label for="cbo_gastopagos_id">Pagos de este Gasto:</label>
                                    <select name="cbo_gastopagos_id" id="cbo_gastopagos_id" class="select2 narrow wrap form-control">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="callout callout-info" id="compracontadoc_elegirgasto_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_compracontadoc_elegirgasto">Guardar</button>
                        <?php endif ?>
                        <?php if ($usuario_action == 'E') : ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_compracontadoc_elegirgasto">Aceptar</button>
                        <?php endif ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo "vista/compracontadoc/compracontadoc_elegirgasto.js?ver=0000012"; ?>"></script>