<?php
if(defined('APP_URL'))
  require_once(APP_URL.'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Compracontadoc extends Conexion{
  private $tb_compracontadoc_id = null;//si es modificacion
  private $tb_proveedor_idfk = null;//*
  private $tb_documentocontable_codfk = null;//*
  private $tb_compracontadoc_serie = null;//*
  private $tb_compracontadoc_numero = null;//*
  private $tb_compracontadoc_condicion = null;
  private $tb_compracontadoc_fechaemision = null;//*
  private $tb_compracontadoc_fechavencimiento = null;//*
  private $tb_compracontadoc_cantdiasvenc = 0;//*
  private $tb_mesconta_idfk = null;
  private $tb_moneda_idfk = null;//*
  private $tb_monedacambiocontable_idfk = null;//* si la moneda fue dolares
  private $tb_compracontadoc_montototal = null;//*
  private $tb_compracontadoc_montosoles = null;//*
  private $tb_compracontadoc_afecta = null;//*
  private $tb_compracontadoc_porcentajeigv = null;//* si afecta es 1
  private $tb_compracontadoc_baseinafecta = null;//* si afecta es 0
  private $tb_compracontadoc_baseafecta = null;//* si afecta es 1
  private $tb_compracontadoc_basesoles = null;//*
  private $tb_compracontadoc_montoigv = null;//*
  private $tb_compracontadoc_montoigvsoles = null;//*
  private $tb_compracontadoc_docrefidfk = null;//* si el documentocontable_codfk es 07
  private $tb_compracontadoc_motivonc = null;
  private $tb_compracontadoc_detr = 0;
  private $tb_compracontadoc_constdetr = null;//* si el importe es mayor a 700 y es afecta
  private $tb_compracontadoc_fechadetr = null;//* si el importe es mayor a 700 y es afecta
  private $tb_creditotipo_idfk = null;//* si coloca anexar credito
  private $tb_credito_idfk = null;//* si coloca anexar credito
  private $tb_compracontadoc_detalle = null;
  private $tb_compracontadoc_fecreg = null;
  private $tb_compracontadoc_usureg = null;//* cuanod es insercion
  private $tb_compracontadoc_fecmod = null;
  private $tb_compracontadoc_usumod = null;//*
  private $tb_compracontadoc_xac = null;
  private $tb_compracontadoc_correlativo;

  /**
   * Get the value of tb_compracontadoc_id
   */
  public function getTbCompracontadocId()
  {
    return $this->tb_compracontadoc_id;
  }

  /**
   * Set the value of tb_compracontadoc_id
   */
  public function setTbCompracontadocId($tb_compracontadoc_id)
  {
    $this->tb_compracontadoc_id = $tb_compracontadoc_id;

    return $this;
  }

  /**
   * Get the value of tb_proveedor_idfk
   */
  public function getTbProveedorIdfk()
  {
    return $this->tb_proveedor_idfk;
  }

  /**
   * Set the value of tb_proveedor_idfk
   */
  public function setTbProveedorIdfk($tb_proveedor_idfk)
  {
    $this->tb_proveedor_idfk = $tb_proveedor_idfk;

    return $this;
  }

  /**
   * Get the value of tb_documentocontable_codfk
   */
  public function getTbCompracontadocCodfk()
  {
    return $this->tb_documentocontable_codfk;
  }

  /**
   * Set the value of tb_documentocontable_codfk
   */
  public function setTbCompracontadocCodfk($tb_documentocontable_codfk)
  {
    $this->tb_documentocontable_codfk = $tb_documentocontable_codfk;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_serie
   */
  public function getTbCompracontadocSerie()
  {
    return $this->tb_compracontadoc_serie;
  }

  /**
   * Set the value of tb_compracontadoc_serie
   */
  public function setTbCompracontadocSerie($tb_compracontadoc_serie)
  {
    $this->tb_compracontadoc_serie = $tb_compracontadoc_serie;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_numero
   */
  public function getTbCompracontadocNumero()
  {
    return $this->tb_compracontadoc_numero;
  }

  /**
   * Set the value of tb_compracontadoc_numero
   */
  public function setTbCompracontadocNumero($tb_compracontadoc_numero)
  {
    $this->tb_compracontadoc_numero = $tb_compracontadoc_numero;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_condicion
   */
  public function getTbCompracontadocCondicion()
  {
    return $this->tb_compracontadoc_condicion;
  }

  /**
   * Set the value of tb_compracontadoc_condicion
   */
  public function setTbCompracontadocCondicion($tb_compracontadoc_condicion)
  {
    $this->tb_compracontadoc_condicion = $tb_compracontadoc_condicion;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_fechaemision
   */
  public function getTbCompracontadocFechaemision()
  {
    return $this->tb_compracontadoc_fechaemision;
  }

  /**
   * Set the value of tb_compracontadoc_fechaemision
   */
  public function setTbCompracontadocFechaemision($tb_compracontadoc_fechaemision)
  {
    $this->tb_compracontadoc_fechaemision = $tb_compracontadoc_fechaemision;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_fechavencimiento
   */
  public function getTbCompracontadocFechavencimiento()
  {
    return $this->tb_compracontadoc_fechavencimiento;
  }

  /**
   * Set the value of tb_compracontadoc_fechavencimiento
   */
  public function setTbCompracontadocFechavencimiento($tb_compracontadoc_fechavencimiento)
  {
    $this->tb_compracontadoc_fechavencimiento = $tb_compracontadoc_fechavencimiento;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_cantdiasvenc
   */
  public function getTbCompracontadocCantdiasvenc()
  {
    return $this->tb_compracontadoc_cantdiasvenc;
  }

  /**
   * Set the value of tb_compracontadoc_cantdiasvenc
   */
  public function setTbCompracontadocCantdiasvenc($tb_compracontadoc_cantdiasvenc)
  {
    $this->tb_compracontadoc_cantdiasvenc = $tb_compracontadoc_cantdiasvenc;

    return $this;
  }

  /**
   * Get the value of tb_mesconta_idfk
   */
  public function getTbMescontaIdfk()
  {
    return $this->tb_mesconta_idfk;
  }

  /**
   * Set the value of tb_mesconta_idfk
   */
  public function setTbMescontaIdfk($tb_mesconta_idfk)
  {
    $this->tb_mesconta_idfk = $tb_mesconta_idfk;

    return $this;
  }

  /**
   * Get the value of tb_moneda_idfk
   */
  public function getTbMonedaIdfk()
  {
    return $this->tb_moneda_idfk;
  }

  /**
   * Set the value of tb_moneda_idfk
   */
  public function setTbMonedaIdfk($tb_moneda_idfk)
  {
    $this->tb_moneda_idfk = $tb_moneda_idfk;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_montototal
   */
  public function getTbCompracontadocMontototal()
  {
    return $this->tb_compracontadoc_montototal;
  }

  /**
   * Set the value of tb_compracontadoc_montototal
   */
  public function setTbCompracontadocMontototal($tb_compracontadoc_montototal)
  {
    $this->tb_compracontadoc_montototal = $tb_compracontadoc_montototal;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_montosoles
   */
  public function getTbCompracontadocMontosoles()
  {
    return $this->tb_compracontadoc_montosoles;
  }

  /**
   * Set the value of tb_compracontadoc_montosoles
   */
  public function setTbCompracontadocMontosoles($tb_compracontadoc_montosoles)
  {
    $this->tb_compracontadoc_montosoles = $tb_compracontadoc_montosoles;

    return $this;
  }

  /**
   * Get the value of tb_monedacambiocontable_idfk
   */
  public function getTbMonedacambiocontableIdfk()
  {
    return $this->tb_monedacambiocontable_idfk;
  }

  /**
   * Set the value of tb_monedacambiocontable_idfk
   */
  public function setTbMonedacambiocontableIdfk($tb_monedacambiocontable_idfk)
  {
    $this->tb_monedacambiocontable_idfk = $tb_monedacambiocontable_idfk;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_afecta
   */
  public function getTbCompracontadocAfecta()
  {
    return $this->tb_compracontadoc_afecta;
  }

  /**
   * Set the value of tb_compracontadoc_afecta
   */
  public function setTbCompracontadocAfecta($tb_compracontadoc_afecta)
  {
    $this->tb_compracontadoc_afecta = $tb_compracontadoc_afecta;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_baseinafecta
   */
  public function getTbCompracontadocBaseinafecta()
  {
    return $this->tb_compracontadoc_baseinafecta;
  }

  /**
   * Set the value of tb_compracontadoc_baseinafecta
   */
  public function setTbCompracontadocBaseinafecta($tb_compracontadoc_baseinafecta)
  {
    $this->tb_compracontadoc_baseinafecta = $tb_compracontadoc_baseinafecta;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_baseafecta
   */
  public function getTbCompracontadocBaseafecta()
  {
    return $this->tb_compracontadoc_baseafecta;
  }

  /**
   * Set the value of tb_compracontadoc_baseafecta
   */
  public function setTbCompracontadocBaseafecta($tb_compracontadoc_baseafecta)
  {
    $this->tb_compracontadoc_baseafecta = $tb_compracontadoc_baseafecta;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_basesoles
   */
  public function getTbCompracontadocBasesoles()
  {
    return $this->tb_compracontadoc_basesoles;
  }

  /**
   * Set the value of tb_compracontadoc_basesoles
   */
  public function setTbCompracontadocBasesoles($tb_compracontadoc_basesoles)
  {
    $this->tb_compracontadoc_basesoles = $tb_compracontadoc_basesoles;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_porcentajeigv
   */
  public function getTbCompracontadocPorcentajeigv()
  {
    return $this->tb_compracontadoc_porcentajeigv;
  }

  /**
   * Set the value of tb_compracontadoc_porcentajeigv
   */
  public function setTbCompracontadocPorcentajeigv($tb_compracontadoc_porcentajeigv)
  {
    $this->tb_compracontadoc_porcentajeigv = $tb_compracontadoc_porcentajeigv;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_montoigv
   */
  public function getTbCompracontadocMontoigv()
  {
    return $this->tb_compracontadoc_montoigv;
  }

  /**
   * Set the value of tb_compracontadoc_montoigv
   */
  public function setTbCompracontadocMontoigv($tb_compracontadoc_montoigv)
  {
    $this->tb_compracontadoc_montoigv = $tb_compracontadoc_montoigv;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_montoigvsoles
   */
  public function getTbCompracontadocMontoigvsoles()
  {
    return $this->tb_compracontadoc_montoigvsoles;
  }

  /**
   * Set the value of tb_compracontadoc_montoigvsoles
   */
  public function setTbCompracontadocMontoigvsoles($tb_compracontadoc_montoigvsoles)
  {
    $this->tb_compracontadoc_montoigvsoles = $tb_compracontadoc_montoigvsoles;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_docrefidfk
   */
  public function getTbCompracontadocDocrefidfk()
  {
    return $this->tb_compracontadoc_docrefidfk;
  }

  /**
   * Set the value of tb_compracontadoc_docrefidfk
   */
  public function setTbCompracontadocDocrefidfk($tb_compracontadoc_docrefidfk)
  {
    $this->tb_compracontadoc_docrefidfk = $tb_compracontadoc_docrefidfk;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_motivonc
   */
  public function getTbCompracontadocMotivonc()
  {
    return $this->tb_compracontadoc_motivonc;
  }

  /**
   * Set the value of tb_compracontadoc_motivonc
   */
  public function setTbCompracontadocMotivonc($tb_compracontadoc_motivonc)
  {
    $this->tb_compracontadoc_motivonc = $tb_compracontadoc_motivonc;

    return $this;
  }

  public function getTbCompracontadocDetr(){ return $this->tb_compracontadoc_detr; }
  public function setTbCompracontadocDetr($tb_compracontadoc_detr){ $this->tb_compracontadoc_detr = $tb_compracontadoc_detr; return $this; }

  /**
   * Get the value of tb_compracontadoc_constdetr
   */
  public function getTbCompracontadocConstdetr()
  {
    return $this->tb_compracontadoc_constdetr;
  }

  /**
   * Set the value of tb_compracontadoc_constdetr
   */
  public function setTbCompracontadocConstdetr($tb_compracontadoc_constdetr)
  {
    $this->tb_compracontadoc_constdetr = $tb_compracontadoc_constdetr;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_fechadetr
   */
  public function getTbCompracontadocFechadetr()
  {
    return $this->tb_compracontadoc_fechadetr;
  }

  /**
   * Set the value of tb_compracontadoc_fechadetr
   */
  public function setTbCompracontadocFechadetr($tb_compracontadoc_fechadetr)
  {
    $this->tb_compracontadoc_fechadetr = $tb_compracontadoc_fechadetr;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_detalle
   */
  public function getTbCompracontadocDetalle()
  {
    return $this->tb_compracontadoc_detalle;
  }

  /**
   * Set the value of tb_compracontadoc_detalle
   */
  public function setTbCompracontadocDetalle($tb_compracontadoc_detalle)
  {
    $this->tb_compracontadoc_detalle = $tb_compracontadoc_detalle;

    return $this;
  }

  /**
   * Get the value of tb_creditotipo_idfk
   */
  public function getTbCreditotipoIdfk()
  {
    return $this->tb_creditotipo_idfk;
  }

  /**
   * Set the value of tb_creditotipo_idfk
   */
  public function setTbCreditotipoIdfk($tb_creditotipo_idfk)
  {
    $this->tb_creditotipo_idfk = $tb_creditotipo_idfk;

    return $this;
  }

  /**
   * Get the value of tb_credito_idfk
   */
  public function getTbCreditoIdfk()
  {
    return $this->tb_credito_idfk;
  }

  /**
   * Set the value of tb_credito_idfk
   */
  public function setTbCreditoIdfk($tb_credito_idfk)
  {
    $this->tb_credito_idfk = $tb_credito_idfk;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_fecreg
   */
  public function getTbCompracontadocFecreg()
  {
    return $this->tb_compracontadoc_fecreg;
  }

  /**
   * Set the value of tb_compracontadoc_fecreg
   */
  public function setTbCompracontadocFecreg($tb_compracontadoc_fecreg)
  {
    $this->tb_compracontadoc_fecreg = $tb_compracontadoc_fecreg;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_usureg
   */
  public function getTbCompracontadocUsureg()
  {
    return $this->tb_compracontadoc_usureg;
  }

  /**
   * Set the value of tb_compracontadoc_usureg
   */
  public function setTbCompracontadocUsureg($tb_compracontadoc_usureg)
  {
    $this->tb_compracontadoc_usureg = $tb_compracontadoc_usureg;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_fecmod
   */
  public function getTbCompracontadocFecmod()
  {
    return $this->tb_compracontadoc_fecmod;
  }

  /**
   * Set the value of tb_compracontadoc_fecmod
   */
  public function setTbCompracontadocFecmod($tb_compracontadoc_fecmod)
  {
    $this->tb_compracontadoc_fecmod = $tb_compracontadoc_fecmod;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_usumod
   */
  public function getTbCompracontadocUsumod()
  {
    return $this->tb_compracontadoc_usumod;
  }

  /**
   * Set the value of tb_compracontadoc_usumod
   */
  public function setTbCompracontadocUsumod($tb_compracontadoc_usumod)
  {
    $this->tb_compracontadoc_usumod = $tb_compracontadoc_usumod;

    return $this;
  }

  /**
   * Get the value of tb_compracontadoc_xac
   */
  public function getTbCompracontadocXac()
  {
    return $this->tb_compracontadoc_xac;
  }

  /**
   * Set the value of tb_compracontadoc_xac
   */
  public function setTbCompracontadocXac($tb_compracontadoc_xac)
  {
    $this->tb_compracontadoc_xac = $tb_compracontadoc_xac;

    return $this;
  }

  public function getTbCompracontadocCorrelativo(){ return $this->tb_compracontadoc_correlativo; }
  public function setTbCompracontadocCorrelativo($tb_compracontadoc_correlativo){ $this->tb_compracontadoc_correlativo = $tb_compracontadoc_correlativo; return $this; }

  public function listar_todos($array, $order_asc_desc){
    //array: bidimensional con formato $column_name, $param.$i, $datatype
    try {
      $sql = "SELECT
                ccd.*,
                mc.tb_mesconta_anio, mc.tb_mesconta_mes,
                dc.tb_documentocontable_desc,
                ccd1.tb_compracontadoc_serie as docreferido_serie, ccd1.tb_compracontadoc_numero as docreferido_numero, ccd1.tb_compracontadoc_fechaemision as docreferido_emision, ccd1.tb_proveedor_idfk as docref_proveedoridfk, ccd1.tb_documentocontable_codfk as docreferido_tipodoc,
                p.tb_proveedor_doc, p.tb_proveedor_nom,
                m.tb_moneda_nom, m.tb_moneda_des, m.tb_moneda_codsunat,
                IF(ccd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_comprasunat) AS tipocambio_comprasunat,
                IF(ccd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_ventasunat) AS tipocambio_ventasunat,
                ct.tb_creditotipo_nom,
                u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot
              FROM
                tb_compracontadoc ccd
              LEFT JOIN tb_mesconta mc ON ccd.tb_mesconta_idfk = mc.tb_mesconta_id
              INNER JOIN tb_documentocontable dc ON ccd.tb_documentocontable_codfk = dc.tb_documentocontable_cod
              LEFT JOIN (select * from tb_compracontadoc) as ccd1 ON ccd.tb_compracontadoc_docrefidfk = ccd1.tb_compracontadoc_id
              INNER JOIN tb_proveedor p ON p.tb_proveedor_id = ccd.tb_proveedor_idfk
              INNER JOIN tb_moneda m ON ccd.tb_moneda_idfk = m.tb_moneda_id
              LEFT JOIN tb_monedacambiocontable mcc ON ccd.tb_monedacambiocontable_idfk = mcc.tb_monedacambiocontable_id
              LEFT JOIN tb_creditotipo ct ON ccd.tb_creditotipo_idfk = ct.tb_creditotipo_id
              INNER JOIN tb_usuario u ON ccd.tb_compracontadoc_usureg = u.tb_usuario_id";
      for ($i = 0; $i < count($array); $i++) {
        if(!empty($array[$i])){
          $sql .= ($i > 0) ? " AND " : " WHERE ";

          if( !empty($array[$i]["param{$i}"]) && empty($array[$i]['column_name']) && empty($array[$i]['datatype']) ){
            $sql .= ' ' . $array[$i]["param{$i}"];
          }
          else{
            if (stripos($array[$i]['column_name'], 'fec') !== FALSE) { //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
              $sql .= " DATE_FORMAT(ccd." . $array[$i]['column_name'] . ", '%Y-%m-%d')";
            }
            else { // de lo contrario solo se coloca el nombre de la columna a filtrar
              $sql .= 'ccd.' . $array[$i]['column_name'];
            }
            $sql .= " = :param" . $i;
          }
        }
      }
      $sql .= " ORDER BY ccd.tb_compracontadoc_fecreg $order_asc_desc";
              
      $sentencia = $this->dblink->prepare($sql);
      for ($i = 0; $i < count($array); $i++) {
        if(!empty($array[$i]['column_name']) && !empty($array[$i]['datatype'])){
          $_PARAM = strtoupper($array[$i]['datatype']) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
          $sentencia->bindParam(":param" . $i, $array[$i]['param' . $i], $_PARAM);
        }
      }
      $sentencia->execute();

      $retorno["sql"] = $sql;
      if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay documentos contables de compras registrados";
          $retorno["data"] = "";
      }
      return $retorno;
    } catch (Exception $th) {
        throw $th;
    }
  }

  function mostrarUno($array, $array1) {
    //array: bidimensional con formato $column_name, $param.$i, $datatype
    //array1: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
    try {
      $sql = "SELECT
                ccd.*,
                mc.tb_mesconta_anio, mc.tb_mesconta_mes,
                dc.tb_documentocontable_desc,
                ccd1.tb_compracontadoc_serie as docreferido_serie, ccd1.tb_compracontadoc_numero as docreferido_numero, ccd1.tb_compracontadoc_fechaemision as docreferido_emision, ccd1.tb_proveedor_idfk as docref_proveedoridfk,
                p.tb_proveedor_doc, p.tb_proveedor_nom,
                pt.tb_proveedortipo_nom,
                m.tb_moneda_nom, m.tb_moneda_des,
                IF(ccd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_comprasunat) AS tipocambio_comprasunat,
                IF(ccd.tb_monedacambiocontable_idfk IS NULL, 1,	mcc.tb_monedacambiocontable_ventasunat) AS tipocambio_ventasunat,
                ct.tb_creditotipo_nom,\n";
      if(!empty($array1)){
        for($j=0; $j< count($array1); $j++){
          $sql .= $array1[$j]['alias_columnasparaver']."\n";
        }
      }
      $sql .= "u.tb_usuario_nom, u.tb_usuario_ape, u.tb_usuario_fot
              FROM
                tb_compracontadoc ccd
              LEFT JOIN tb_mesconta mc ON ccd.tb_mesconta_idfk = mc.tb_mesconta_id
              INNER JOIN tb_documentocontable dc ON ccd.tb_documentocontable_codfk = dc.tb_documentocontable_cod
              LEFT JOIN 
                (select tb_compracontadoc_id, tb_compracontadoc_serie, tb_compracontadoc_numero, tb_compracontadoc_fechaemision, tb_proveedor_idfk from tb_compracontadoc) as ccd1
                ON ccd.tb_compracontadoc_docrefidfk = ccd1.tb_compracontadoc_id
              INNER JOIN tb_proveedor p ON p.tb_proveedor_id = ccd.tb_proveedor_idfk
              INNER JOIN tb_proveedortipo pt ON p.tb_proveedor_tip = pt.tb_proveedortipo_id
              INNER JOIN tb_moneda m ON ccd.tb_moneda_idfk = m.tb_moneda_id
              LEFT JOIN tb_monedacambiocontable mcc ON ccd.tb_monedacambiocontable_idfk = mcc.tb_monedacambiocontable_id
              LEFT JOIN tb_creditotipo ct ON ccd.tb_creditotipo_idfk = ct.tb_creditotipo_id\n";
      if(!empty($array1)){
        for($k=0; $k< count($array1); $k++){
          $sql .= $array1[$k]['tipo_union']. " JOIN ". $array1[$k]['tabla_alias']. " ON ". $array1[$k]['columna_enlace']. " = ". $array1[$k]['alias_columnaPK']."\n";
        }
      }
      $sql .="LEFT JOIN tb_usuario u ON ccd.tb_compracontadoc_usureg = u.tb_usuario_id
              WHERE ";
      for($i = 0; $i < count($array); $i++){
        $sql .= ($i > 0) ? " AND " : "";
        if(stripos($array[$i]['column_name'], 'fec') !== FALSE){ //si el nombre de columna incluye las letras fec, se trata de una fecha. Se da formato de fecha.
          $sql .= " DATE_FORMAT(ccd.".$array[$i]['column_name'].", '%Y-%m-%d')";
        }
        else{ // de lo contrario solo se coloca el nombre de la columna a filtrar
          $sql .= 'ccd.'.$array[$i]['column_name'];
        }
        $sql .= " = :param".$i;
      }

      $sentencia = $this->dblink->prepare($sql);
      for($i = 0; $i < count($array); $i++){
          $_PARAM = strtoupper($array[$i]['datatype'])=='INT' ? PDO::PARAM_INT : PDO::PARAM_STR; // si el tipo de dato es int, asigna PDO::PARAM_INT, de lo contrario PDO::PARAM_STR
          $sentencia->bindParam(":param".$i, $array[$i]['param'.$i], $_PARAM);
      }
      $sentencia->execute();
      
      $retorno["sql"]  = $sql;
      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No existe este registro";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
        throw $e;
    }
  }

  function eliminar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_compracontadoc SET tb_compracontadoc_xac = 0, tb_compracontadoc_usumod = :tb_compracontadoc_usumod WHERE tb_compracontadoc_id = :tb_compracontadoc_id ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_compracontadoc_usumod", $this->tb_compracontadoc_usumod, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_compracontadoc_id", $this->tb_compracontadoc_id, PDO::PARAM_INT);
      $resultado = $sentencia->execute(); //retorna 1 si es correcto

      if($resultado == 1){
        $this->dblink->commit();
      }
      return $resultado;
    } catch (Exception $th) {
      $this->dblink->rollBack();
      throw $th;
    }
  }

  function modificar(){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_compracontadoc SET
                    tb_proveedor_idfk = :param0,
                    tb_documentocontable_codfk = :param1,
                    tb_compracontadoc_serie = :param2,
                    tb_compracontadoc_numero = :param3,
                    tb_compracontadoc_fechaemision = :param4,
                    tb_compracontadoc_fechavencimiento = :param5,
                    tb_compracontadoc_cantdiasvenc = :param6,
                    tb_moneda_idfk = :param7,
                    tb_monedacambiocontable_idfk = :param8,
                    tb_compracontadoc_montototal = :param9,
                    tb_compracontadoc_montosoles = :param10,
                    tb_compracontadoc_afecta = :param11,
                    tb_compracontadoc_porcentajeigv = :param12,
                    tb_compracontadoc_baseinafecta = :param13,
                    tb_compracontadoc_baseafecta = :param14,
                    tb_compracontadoc_basesoles = :param15,
                    tb_compracontadoc_montoigv = :param16,
                    tb_compracontadoc_montoigvsoles = :param17,
                    tb_compracontadoc_docrefidfk = :param18,
                    tb_compracontadoc_constdetr = :param19,
                    tb_compracontadoc_fechadetr = :param20,
                    tb_creditotipo_idfk = :param21,
                    tb_credito_idfk = :param22,
                    tb_compracontadoc_detalle = :param23,
                    tb_compracontadoc_usumod = :param24,
                    tb_compracontadoc_motivonc = :param25,
                    tb_mesconta_idfk = :param26,
                    tb_compracontadoc_detr = :param27,
                    tb_compracontadoc_correlativo = :param28
                  WHERE
                    tb_compracontadoc_id = :param29;";
          
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(':param0', $this->tb_proveedor_idfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param1', $this->tb_documentocontable_codfk , PDO::PARAM_STR);
          $sentencia->bindParam(':param2', $this->tb_compracontadoc_serie , PDO::PARAM_STR);
          $sentencia->bindParam(':param3', $this->tb_compracontadoc_numero , PDO::PARAM_STR);
          $sentencia->bindParam(':param4', $this->tb_compracontadoc_fechaemision , PDO::PARAM_STR);
          $sentencia->bindParam(':param5', $this->tb_compracontadoc_fechavencimiento , PDO::PARAM_STR);
          $sentencia->bindParam(':param6', $this->tb_compracontadoc_cantdiasvenc , PDO::PARAM_INT);
          $sentencia->bindParam(':param7', $this->tb_moneda_idfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param8', $this->tb_monedacambiocontable_idfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param9', $this->tb_compracontadoc_montototal , PDO::PARAM_STR);
          $sentencia->bindParam(':param10', $this->tb_compracontadoc_montosoles , PDO::PARAM_STR);
          $sentencia->bindParam(':param11', $this->tb_compracontadoc_afecta , PDO::PARAM_INT);
          $sentencia->bindParam(':param12', $this->tb_compracontadoc_porcentajeigv , PDO::PARAM_STR);
          $sentencia->bindParam(':param13', $this->tb_compracontadoc_baseinafecta , PDO::PARAM_STR);
          $sentencia->bindParam(':param14', $this->tb_compracontadoc_baseafecta , PDO::PARAM_STR);
          $sentencia->bindParam(':param15', $this->tb_compracontadoc_basesoles , PDO::PARAM_STR);
          $sentencia->bindParam(':param16', $this->tb_compracontadoc_montoigv , PDO::PARAM_STR);
          $sentencia->bindParam(':param17', $this->tb_compracontadoc_montoigvsoles , PDO::PARAM_STR);
          $sentencia->bindParam(':param18', $this->tb_compracontadoc_docrefidfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param19', $this->tb_compracontadoc_constdetr , PDO::PARAM_STR);
          $sentencia->bindParam(':param20', $this->tb_compracontadoc_fechadetr , PDO::PARAM_STR);
          $sentencia->bindParam(':param21', $this->tb_creditotipo_idfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param22', $this->tb_credito_idfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param23', $this->tb_compracontadoc_detalle , PDO::PARAM_STR);
          $sentencia->bindParam(':param24', $this->tb_compracontadoc_usumod , PDO::PARAM_INT);
          $sentencia->bindParam(':param25', $this->tb_compracontadoc_motivonc , PDO::PARAM_STR);
          $sentencia->bindParam(':param26', $this->tb_mesconta_idfk , PDO::PARAM_INT);
          $sentencia->bindParam(':param27', $this->tb_compracontadoc_detr , PDO::PARAM_INT);
          $sentencia->bindParam(':param28', $this->tb_compracontadoc_correlativo , PDO::PARAM_STR);
          $sentencia->bindParam(':param29', $this->tb_compracontadoc_id , PDO::PARAM_INT);
          $resultado = $sentencia->execute();

          if ($resultado == 1) {
            $this->dblink->commit();
          }
          return $resultado;
      } catch (Exception $th) {
          $this->dblink->rollBack();
          throw $th;
      }
  }

  function insertar(){
      $this->dblink->beginTransaction();
      try {
          $sql = "INSERT INTO tb_compracontadoc (
                    tb_proveedor_idfk,
                    tb_documentocontable_codfk,
                    tb_compracontadoc_serie,
                    tb_compracontadoc_numero,
                    tb_compracontadoc_fechaemision,
                    tb_compracontadoc_fechavencimiento,
                    tb_compracontadoc_cantdiasvenc,
                    tb_moneda_idfk,
                    tb_monedacambiocontable_idfk,
                    tb_compracontadoc_montototal,
                    tb_compracontadoc_montosoles,
                    tb_compracontadoc_afecta,
                    tb_compracontadoc_porcentajeigv,
                    tb_compracontadoc_baseinafecta,
                    tb_compracontadoc_baseafecta,
                    tb_compracontadoc_basesoles,
                    tb_compracontadoc_montoigv,
                    tb_compracontadoc_montoigvsoles,
                    tb_compracontadoc_docrefidfk,
                    tb_compracontadoc_constdetr,
                    tb_compracontadoc_fechadetr,
                    tb_creditotipo_idfk,
                    tb_credito_idfk,
                    tb_compracontadoc_detalle,
                    tb_compracontadoc_usureg,
                    tb_compracontadoc_usumod,
                    tb_compracontadoc_motivonc,
                    tb_mesconta_idfk,
                    tb_compracontadoc_detr,
                    tb_compracontadoc_correlativo
                  )
                  VALUES(
                    :param0,
                    :param1,
                    :param2,
                    :param3,
                    :param4,
                    :param5,
                    :param6,
                    :param7,
                    :param8,
                    :param9,
                    :param10,
                    :param11,
                    :param12,
                    :param13,
                    :param14,
                    :param15,
                    :param16,
                    :param17,
                    :param18,
                    :param19,
                    :param20,
                    :param21,
                    :param22,
                    :param23,
                    :param24,
                    :param25,
                    :param26,
                    :param27,
                    :param28,
                    :param29
                  );";
          
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":param0", $this->tb_proveedor_idfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param1", $this->tb_documentocontable_codfk, PDO::PARAM_STR);
          $sentencia->bindParam(":param2", $this->tb_compracontadoc_serie, PDO::PARAM_STR);
          $sentencia->bindParam(":param3", $this->tb_compracontadoc_numero, PDO::PARAM_STR);
          $sentencia->bindParam(":param4", $this->tb_compracontadoc_fechaemision, PDO::PARAM_STR);
          $sentencia->bindParam(":param5", $this->tb_compracontadoc_fechavencimiento, PDO::PARAM_STR);
          $sentencia->bindParam(":param6", $this->tb_compracontadoc_cantdiasvenc, PDO::PARAM_INT);
          $sentencia->bindParam(":param7", $this->tb_moneda_idfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param8", $this->tb_monedacambiocontable_idfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param9", $this->tb_compracontadoc_montototal, PDO::PARAM_STR);
          $sentencia->bindParam(":param10", $this->tb_compracontadoc_montosoles, PDO::PARAM_STR);
          $sentencia->bindParam(":param11", $this->tb_compracontadoc_afecta, PDO::PARAM_INT);
          $sentencia->bindParam(":param12", $this->tb_compracontadoc_porcentajeigv, PDO::PARAM_STR);
          $sentencia->bindParam(":param13", $this->tb_compracontadoc_baseinafecta, PDO::PARAM_STR);
          $sentencia->bindParam(":param14", $this->tb_compracontadoc_baseafecta, PDO::PARAM_STR);
          $sentencia->bindParam(":param15", $this->tb_compracontadoc_basesoles, PDO::PARAM_STR);
          $sentencia->bindParam(":param16", $this->tb_compracontadoc_montoigv, PDO::PARAM_STR);
          $sentencia->bindParam(":param17", $this->tb_compracontadoc_montoigvsoles, PDO::PARAM_STR);
          $sentencia->bindParam(":param18", $this->tb_compracontadoc_docrefidfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param19", $this->tb_compracontadoc_constdetr, PDO::PARAM_STR);
          $sentencia->bindParam(":param20", $this->tb_compracontadoc_fechadetr, PDO::PARAM_STR);
          $sentencia->bindParam(":param21", $this->tb_creditotipo_idfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param22", $this->tb_credito_idfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param23", $this->tb_compracontadoc_detalle, PDO::PARAM_STR);
          $sentencia->bindParam(":param24", $this->tb_compracontadoc_usureg, PDO::PARAM_INT);
          $sentencia->bindParam(":param25", $this->tb_compracontadoc_usumod, PDO::PARAM_INT);
          $sentencia->bindParam(":param26", $this->tb_compracontadoc_motivonc, PDO::PARAM_STR);
          $sentencia->bindParam(":param27", $this->tb_mesconta_idfk, PDO::PARAM_INT);
          $sentencia->bindParam(":param28", $this->tb_compracontadoc_detr, PDO::PARAM_INT);
          $sentencia->bindParam(":param29", $this->tb_compracontadoc_correlativo, PDO::PARAM_STR);

          $resultado['estado'] = $sentencia->execute();
          $resultado['nuevo'] = $this->dblink->lastInsertId();

          if($resultado['estado'] == 1){
              $this->dblink->commit();
              $resultado['mensaje'] = 'Registrado correctamente';
          }
          else{
              $resultado['mensaje'] = 'No se pudo registrar';
          }
          return $resultado;
      } catch (Exception $th) {
          $this->dblink->rollBack();
          throw $th;
      }
  }

  function Compracontadoc_autocomplete($dato) {
    try {

        $filtro = "%" . $dato . "%";

        $sql = "SELECT *
                FROM tb_compracontadoc ccd
                INNER JOIN tb_proveedor p ON ccd.tb_proveedor_idfk = p.tb_proveedor_id
                WHERE CONCAT(tb_compracontadoc_serie,'-',tb_compracontadoc_numero)
                LIKE :filtro";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay Documentos contables registrados";
            $retorno["data"] = "";
        }

        return $retorno;
    } catch (Exception $e) {
        throw $e;
    }
  }

  public function buscar_por_mesconta($mesconta){
    try {
      $sql = "SELECT * from tb_compracontadoc ccd
              WHERE LEFT(ccd.tb_compracontadoc_fechaemision,7) = :mesconta ;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':mesconta', $mesconta, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay documentos contables de compras registrados con este mes contable";
        $retorno["data"] = "";
      }
      return $retorno;
    } catch (Exception $th) {
      throw $th;
    }
  }
  
}

/* $retorno["estado"] = 0;
  $retorno["mensaje"] = $sql;
  $retorno["data"] = "".$sql;
  return $retorno; exit(); */
//$retorno["mensaje"] = $sql; return $retorno; exit();
?>