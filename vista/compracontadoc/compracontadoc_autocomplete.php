<?php
  require_once('../funciones/fechas.php');
  require_once ("Compracontadoc.class.php");
  $oCompracontadoc= new Compracontadoc();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $tb_compracontadoc_id;
    var $tb_compracontadoc_serie;
    var $tb_compracontadoc_numero;
    var $tb_proveedor_idfk;
    var $tb_compracontadoc_fechaemision;


    function __construct($label, $value, $tb_compracontadoc_id, $tb_compracontadoc_serie, $tb_compracontadoc_numero, $tb_proveedor_idfk , $tb_compracontadoc_fechaemision ){
      $this->label = $label;
      $this->value = $value;
      $this->tb_compracontadoc_id = $tb_compracontadoc_id;
      $this->tb_compracontadoc_serie = $tb_compracontadoc_serie;
      $this->tb_compracontadoc_numero = $tb_compracontadoc_numero;
      $this->tb_proveedor_idfk = $tb_proveedor_idfk;
      $this->tb_compracontadoc_fechaemision = $tb_compracontadoc_fechaemision;
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $datoBuscar = mb_strtoupper($datoBuscar, 'UTF-8');

  //busco un valor aproximado al dato escrito
  $result = $oCompracontadoc->Compracontadoc_autocomplete($datoBuscar);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    foreach ($result['data'] as $key => $value) {
      array_push(
        $arrayElementos,
        new ElementoAutocompletar(
          $value["tb_compracontadoc_serie"].'-'.$value["tb_compracontadoc_numero"].', PROV: '.$value['tb_proveedor_nom'].', EMI: '. mostrar_fecha($value['tb_compracontadoc_fechaemision']),
          $value["tb_compracontadoc_serie"].'-'.$value["tb_compracontadoc_numero"].', PROV: '.$value['tb_proveedor_nom'].', EMI: '. mostrar_fecha($value['tb_compracontadoc_fechaemision']),
          $value["tb_compracontadoc_id"],
          $value["tb_compracontadoc_serie"],
          $value["tb_compracontadoc_numero"],
          $value["tb_proveedor_idfk"],
          mostrar_fecha($value["tb_compracontadoc_fechaemision"]))
      );
    }

    print_r(json_encode($arrayElementos));
  $result = NULL;
?>
