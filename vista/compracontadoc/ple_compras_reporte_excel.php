<?php
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';
require_once('../compracontadoc/Compracontadoc.class.php');
$oCCD = new Compracontadoc();
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$verificar_docs = intval($_GET['verificar_docs']);
$sql_to_verify = "";

if($verificar_docs == 1){
  //1: Docs. que tienen detraccion check, pero constancia o fecha detraccion vacio o null
  $sql_to_verify = "(ccd.tb_compracontadoc_detr = 1 AND
                      (ccd.tb_compracontadoc_fechadetr = '0000-00-00' OR ccd.tb_compracontadoc_fechadetr IS NULL
                       OR ccd.tb_compracontadoc_constdetr = '' OR ccd.tb_compracontadoc_constdetr IS NULL)
                    )";
}

//$column_name, $param.$i, $datatype
$parametros[0]['column_name'] = 'tb_compracontadoc_xac';
$parametros[0]['param0'] = 1;
$parametros[0]['datatype'] = 'INT';

$parametros[1]['column_name'] = 'tb_proveedor_idfk';
$parametros[1]['param1'] = intval($_GET['proveedor']);
$parametros[1]['datatype'] = 'INT';

$parametros[2]['column_name'] = 'tb_documentocontable_codfk';
$parametros[2]['param2'] = $_GET['tipo_doc'];
$parametros[2]['datatype'] = 'STR';

$parametros[3]['column_name'] = 'tb_mesconta_idfk';
$parametros[3]['param3'] = intval($_GET['mesconta']);
$parametros[3]['datatype'] = 'INT';

$parametros[4]['param4'] = $sql_to_verify;

for ($i = 0 ; $i< count($parametros) ; $i++){
  if(empty($parametros[$i]['param'.$i])){
    $parametros[$i] = array();
  }
}

$result = $oCCD->listar_todos($parametros, "ASC");

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("")
	->setLastModifiedBy("")
	->setTitle("Reporte De Compras PLE")
	->setSubject("formato ple compras contables")
	->setDescription("")
	->setKeywords("")
	->setCategory("reporte");

$estiloTituloColumnas = array(
    'font' => array(
    'name'  => 'calibri',
    'bold'  => false,
    'size'  => 14,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '4472C4')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => 'FFFFFF'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloColumnas2 = array(
    'font' => array(
    'name'  => 'calibri',
    'bold'  => false,
    'size'  => 8,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '4472C4')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => 'FFFFFF'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

// This function is used to format the cells of an Excel file.
// It is used in the following way:
// $objPHPExcel->getActiveSheet()->getStyle('A1:A2')->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 11));
// The first parameter is the alignment of the text in the cell.
// The second parameter is used to indicate if the cell is empty or not.
// The third parameter is the size of the font.
function estilo_filas_iz_der_ce($alineacion, $vacio, $tamaño_letra){ // alineacion: iz, der. vacio: 1, 0. tamaño_letra: 11, 8.
  $formato_alineacion = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
  if($alineacion == 'iz'){
    $formato_alineacion = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
  }
  else if($alineacion == 'der'){
    $formato_alineacion = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
  }
  if($vacio == 1){
    $color_fill = 'FFC7CE';
  }
  else{
    $color_fill = 'FFFFFF';
  }

  $array_return = array(
    'font' => array(
      'name'  => 'calibri',
      'bold'  => false,
      'size'  => $tamaño_letra,
      'color' => array(
        'rgb' => '000000'
      )
    ),
    'fill' => array(
      'type'  => PHPExcel_Style_Fill::FILL_SOLID,
      'color' => array(
        'rgb' => $color_fill)
    ),
    'borders' => array(
      'outline' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN ,
          'color' => array(
            'rgb' => 'D0D0D0'
          )
        )
    ),
    'alignment' =>  array(
      'horizontal'=> $formato_alineacion,
      'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      'wrap'      => FALSE
    )
  );
  return $array_return;
}


//$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//establecer impresion a pagina completa
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

$nombre_mes = ($_GET['mes'] == 'MES-AÑO') ? ' (todos los meses)' : ' '.$_GET['mes'];
$fecha_hora = date("YmdHis");
$nombre_archivo = "Compras PLE". $nombre_mes . ' - ' . $fecha_hora;

$c = 3;
$ctf = 4;
$titulo = "N°";
$objPHPExcel->getActiveSheet()->mergeCells("A$c:A$ctf");
$objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
$objPHPExcel->getActiveSheet()->getStyle("A$c:A$ctf")->applyFromArray($estiloTituloColumnas);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//altura
$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(45);//altura

$titulos_columnas_2filas = array(
  "PERIODO",
  "CÓDIGO ÚNICO DE LA OPERACIÓN (CUO)",
  "NÚMERO CORRELATIVO DEL (CUO)",
  "RETENCIÓN",
  "CLASIFICACIÓN DE BIENES Y SERVICIOS TABLA 30",
  "IDENTIFICACIÓN DEL CONTRATO  DE LAS SOCIEDADES",
  "ERROR TIPO 1",
  "ERROR TIPO 2",
  "ERROR TIPO 3",
  "ERROR TIPO 4",
  "COMPROBANTES CANCELADOS CON MEDIOS DE PAGO",
  "ESTADO"
);

$objPHPExcel->getActiveSheet()
  ->mergeCells("B$c:B$ctf")
  ->mergeCells("C$c:C$ctf")
  ->mergeCells("D$c:D$ctf")
  ->mergeCells("AI$c:AI$ctf")
  ->mergeCells("AJ$c:AJ$ctf")
  ->mergeCells("AK$c:AK$ctf")
  ->mergeCells("AL$c:AL$ctf")
  ->mergeCells("AM$c:AM$ctf")
  ->mergeCells("AN$c:AN$ctf")
  ->mergeCells("AO$c:AO$ctf")
  ->mergeCells("AP$c:AP$ctf")
  ->mergeCells("AQ$c:AQ$ctf");

$objPHPExcel->getActiveSheet()
  ->setCellValue('B'.$c,  $titulos_columnas_2filas[0])
  ->setCellValue('C'.$c,  $titulos_columnas_2filas[1])
  ->setCellValue('D'.$c,  $titulos_columnas_2filas[2])
  ->setCellValue('AI'.$c,  $titulos_columnas_2filas[3])
  ->setCellValue('AJ'.$c,  $titulos_columnas_2filas[4])
  ->setCellValue('AK'.$c,  $titulos_columnas_2filas[5])
  ->setCellValue('AL'.$c,  $titulos_columnas_2filas[6])
  ->setCellValue('AM'.$c,  $titulos_columnas_2filas[7])
  ->setCellValue('AN'.$c,  $titulos_columnas_2filas[8])
  ->setCellValue('AO'.$c,  $titulos_columnas_2filas[9])
  ->setCellValue('AP'.$c,  $titulos_columnas_2filas[10])
  ->setCellValue('AQ'.$c,  $titulos_columnas_2filas[11]);

$objPHPExcel->getActiveSheet()->getStyle("B$c:B$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("C$c:C$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("D$c:D$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AI$c:AI$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AJ$c:AJ$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AK$c:AK$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AL$c:AL$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AM$c:AM$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AN$c:AN$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AO$c:AO$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AP$c:AP$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AQ$c:AQ$ctf")->applyFromArray($estiloTituloColumnas2);

$titulos_2filas_combinados = array(
  "COMPROBANTE DE PAGO O DOCUMENTO",
  "PROVEEDOR",
  "IMPORTES DEL COMPROBANTE DE PAGO O DOCUMENTO",
  "MONEDA",
  "DOCUMENTO DE REFERENCIA",
  "DETRACCION"
);

$objPHPExcel->getActiveSheet()
  ->mergeCells("E$c:K$c")
  ->mergeCells("L$c:N$c")
  ->mergeCells("O$c:Y$c")
  ->mergeCells("Z$c:AA$c")
  ->mergeCells("AB$c:AF$c")
  ->mergeCells("AG$c:AH$c");

$objPHPExcel->getActiveSheet()
  ->setCellValue('E'.$c,  $titulos_2filas_combinados[0])
  ->setCellValue('L'.$c,  $titulos_2filas_combinados[1])
  ->setCellValue('O'.$c,  $titulos_2filas_combinados[2])
  ->setCellValue('Z'.$c,  $titulos_2filas_combinados[3])
  ->setCellValue('AB'.$c,  $titulos_2filas_combinados[4])
  ->setCellValue('AG'.$c,  $titulos_2filas_combinados[5]);

$objPHPExcel->getActiveSheet()->getStyle("E$c:K$c")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("L$c:N$c")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("O$c:Y$c")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("Z$c:AA$c")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AB$c:AF$c")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("AG$c:AH$c")->applyFromArray($estiloTituloColumnas2);

$objPHPExcel->getActiveSheet()->setCellValue('E'.$ctf, "FECHA DE EMISIÓN")
  ->setCellValue('F'.$ctf, "FECHA DE VENCIMIENTO")
  ->setCellValue('G'.$ctf, "TIPO                      TABLA 10")
  ->setCellValue('H'.$ctf, "SERIE")
  ->setCellValue('I'.$ctf, "AÑO DUA O DSI")
  ->setCellValue('J'.$ctf, "NÚMERO")
  ->setCellValue('K'.$ctf, "NÚMERO FINAL");

$objPHPExcel->getActiveSheet()->getStyle("E$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("F$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("G$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("H$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("I$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("J$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("K$ctf")->applyFromArray($estiloTituloColumnas2);

$objPHPExcel->getActiveSheet()->setCellValue('L'.$ctf, "TIPO                  TABLA 2")
  ->setCellValue('M'.$ctf, "NÚMERO")
  ->setCellValue('N'.$ctf, "APELLIDOS Y NOMBRES O RAZÓN SOCIAL DEL PROVEEDOR");

$objPHPExcel->getActiveSheet()->getStyle("L$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("M$ctf")->applyFromArray($estiloTituloColumnas2);
$objPHPExcel->getActiveSheet()->getStyle("N$ctf")->applyFromArray($estiloTituloColumnas2);

$columnas = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ");
for($i = 0 ; $i < count($columnas); $i++){
  $objPHPExcel->getActiveSheet()->getColumnDimension($columnas[$i])->setWidth(11.5); 
}

$objPHPExcel->getActiveSheet()->setCellValue($columnas[14].$ctf, "BASE IMPONIBLE")
  ->setCellValue($columnas[15].$ctf, "IGV")
  ->setCellValue($columnas[16].$ctf, "BASE IMPONIBLE")
  ->setCellValue($columnas[17].$ctf, "IGV")
  ->setCellValue($columnas[18].$ctf, "BASE IMPONIBLE")
  ->setCellValue($columnas[19].$ctf, "IGV")
  ->setCellValue($columnas[20].$ctf, "NO         GRAVADO")
  ->setCellValue($columnas[21].$ctf, "I.S.C")
  ->setCellValue($columnas[22].$ctf, "ICBPER")
  ->setCellValue($columnas[23].$ctf, "OTROS          CARGOS")
  ->setCellValue($columnas[24].$ctf, "IMPORTE    TOTAL")
  ->setCellValue($columnas[25].$ctf, "CÓDIGO  MONEDA          TABLA 4")
  ->setCellValue($columnas[26].$ctf, "TIPO          DE CAMBIO")
  ->setCellValue($columnas[27].$ctf, "FECHA DE EMISIÓN")
  ->setCellValue($columnas[28].$ctf, "TIPO     TABLA 10")
  ->setCellValue($columnas[29].$ctf, "SERIE")
  ->setCellValue($columnas[30].$ctf, "CÓD TABLA 11")
  ->setCellValue($columnas[31].$ctf, "NÚMERO")
  ->setCellValue($columnas[32].$ctf, "FECHA DE EMISIÓN")
  ->setCellValue($columnas[33].$ctf, "NÚMERO          DE LA CONSTANCIA");

for($i = 0; $i < 20; $i++){
  $objPHPExcel->getActiveSheet()->getStyle($columnas[14 + $i].$ctf)->applyFromArray($estiloTituloColumnas2);
}

/* $objPHPExcel->getActiveSheet(0)->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet(0)->getStyle('E'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00); */

$c = 5;
//aqui viene rellenar con cada documento de compras
if($result["estado"] == 1){
  foreach ($result["data"] as $key => $value) {
    $tipo_text = PHPExcel_Cell_DataType::TYPE_STRING;
    $tipo_numero = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00;
    $tipo_proveedor = (strlen($value["tb_proveedor_doc"])>8) ? "6" : "1";
    $estado_ple = 1;

    $fecha_emision_d_m_a = mostrar_fecha($value["tb_compracontadoc_fechaemision"]);
    $mes_emision = explode("-",$fecha_emision_d_m_a)[1];
    $anio_emision = explode("-",$fecha_emision_d_m_a)[2];

    if(intval($anio_emision) < intval($value["tb_mesconta_anio"]) || intval($mes_emision) < intval($value["tb_mesconta_mes"]) ){
      $estado_ple = 6;
    }

    $fecha_emision_d_m_a = str_replace("-", "/", $fecha_emision_d_m_a);

    $fecha_vencimiento = "";
    if($value["tb_documentocontable_codfk"] == '14'){
      $fecha_vencimiento = $value["tb_compracontadoc_fechavencimiento"] == "0000-00-00" ? "" : mostrar_fecha($value["tb_compracontadoc_fechavencimiento"]);
      $fecha_vencimiento = str_replace("-", "/", $fecha_vencimiento);
    }
    
    if($value["tb_moneda_idfk"] == "2"){
      $base_afecta = formato_moneda($value["tb_compracontadoc_baseafecta"] * $value["tipocambio_comprasunat"]);
      $monto_igv = formato_moneda($value["tb_compracontadoc_montoigv"] * $value["tipocambio_comprasunat"]);
      $base_inafecto = formato_moneda($value["tb_compracontadoc_baseinafecta"] * $value["tipocambio_comprasunat"]);
      $monto_total = formato_moneda($value["tb_compracontadoc_montototal"] * $value["tipocambio_comprasunat"]);
    }
    else{
      $base_afecta = $value["tb_compracontadoc_baseafecta"];
      $monto_igv = $value["tb_compracontadoc_montoigv"];
      $base_inafecto = $value["tb_compracontadoc_baseinafecta"];
      $monto_total = $value["tb_compracontadoc_montototal"];
    }

    $objPHPExcel->getActiveSheet()->setCellValue($columnas[0].$c, ($c - 4))
      ->setCellValue($columnas[1].$c, $value["tb_mesconta_anio"].$value["tb_mesconta_mes"]."00")
      ->setCellValue($columnas[2].$c, $value["tb_compracontadoc_correlativo"])
      ->setCellValue($columnas[3].$c, "M-MYPE")
      ->setCellValue($columnas[4].$c, $fecha_emision_d_m_a)
      ->setCellValue($columnas[5].$c, $fecha_vencimiento)
      ->setCellValue($columnas[7].$c, $value["tb_compracontadoc_serie"])
      ->setCellValue($columnas[8].$c, "")
      ->setCellValue($columnas[10].$c, "")
      ->setCellValue($columnas[11].$c, $tipo_proveedor)
      ->setCellValue($columnas[13].$c, $value["tb_proveedor_nom"])
      ->setCellValue($columnas[14].$c, $base_afecta)
      ->setCellValue($columnas[15].$c, $monto_igv)
      ->setCellValue($columnas[16].$c, "0.00")
      ->setCellValue($columnas[17].$c, "0.00")
      ->setCellValue($columnas[18].$c, "0.00")
      ->setCellValue($columnas[19].$c, "0.00")
      ->setCellValue($columnas[20].$c, $base_inafecto)
      ->setCellValue($columnas[21].$c, "0.00")
      ->setCellValue($columnas[22].$c, "0.00")
      ->setCellValue($columnas[23].$c, "0.00")
      ->setCellValue($columnas[24].$c, $monto_total)
      ->setCellValue($columnas[25].$c, $value["tb_moneda_codsunat"])
      ->setCellValue($columnas[26].$c, $value["tipocambio_comprasunat"]);
    
    if($value['tb_documentocontable_codfk'] == '07'){
      $objPHPExcel->getActiveSheet()->setCellValue($columnas[27].$c, str_replace("-", "/", mostrar_fecha($value["docreferido_emision"])))
        ->setCellValue($columnas[29].$c, $value["docreferido_serie"]);
      $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(28, $c, $value["docreferido_tipodoc"], $tipo_text)
        ->setCellValueExplicitByColumnAndRow(31, $c, $value["docreferido_numero"], $tipo_text);
    }

    $vacio_detr = 0;
    if(intval($value["tb_compracontadoc_detr"]) == 1){
      if(empty($value["tb_compracontadoc_constdetr"])){
        $vacio_detr = 1;
      }
      $fecha_detraccion = $value["tb_compracontadoc_fechadetr"] == "0000-00-00" ? "" : mostrar_fecha($value["tb_compracontadoc_fechadetr"]);
      $objPHPExcel->getActiveSheet()->setCellValue($columnas[32].$c, str_replace("-", "/", $fecha_detraccion));
      $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(33, $c, $value["tb_compracontadoc_constdetr"], $tipo_text);
    }
  
    //NUMEROS QUE NO SE VAN A OPERAR -> EN FORMATO TEXTO
    $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(6, $c, $value["tb_documentocontable_codfk"], $tipo_text)
      ->setCellValueExplicitByColumnAndRow(9, $c, $value["tb_compracontadoc_numero"], $tipo_text)
      ->setCellValueExplicitByColumnAndRow(12, $c, $value["tb_proveedor_doc"], $tipo_text)
      ->setCellValueExplicitByColumnAndRow(42, $c, $estado_ple, $tipo_text);
    
    $vacio_fec_venc = $value["tb_compracontadoc_fechavencimiento"] == '0000-00-00';
    
    $objPHPExcel->getActiveSheet()->getStyle($columnas[0].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[1].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[2].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[3].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[4].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[5].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', $vacio_fec_venc, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[6].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[7].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[8].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[9].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[10].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[11].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[12].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[13].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 8));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[14].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[15].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[16].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[17].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[18].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[19].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[20].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[21].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[22].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[23].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[24].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode($tipo_numero);
    $objPHPExcel->getActiveSheet()->getStyle($columnas[25].$c)->applyFromArray(estilo_filas_iz_der_ce('', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[26].$c)->applyFromArray(estilo_filas_iz_der_ce('der', 0, 9))->getNumberFormat()->setFormatCode('0.000');
    $objPHPExcel->getActiveSheet()->getStyle($columnas[27].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[28].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[29].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[31].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[32].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', $vacio_detr, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[33].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', $vacio_detr, 9));
    $objPHPExcel->getActiveSheet()->getStyle($columnas[42].$c)->applyFromArray(estilo_filas_iz_der_ce('iz', 0, 9));

    //echo var_export($value) . "<br>";
    $objPHPExcel->getActiveSheet()->getRowDimension("$c")->setRowHeight(15);
    $c++;
  }
}
$result = NULL;
//exit();


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Hoja1');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0*/
header("Content-type: application/vnd.ms-excel");//text/csv
header("Content-Disposition: attachment; filename=".$nombre_archivo.".xlsx");//csv
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
exit;
