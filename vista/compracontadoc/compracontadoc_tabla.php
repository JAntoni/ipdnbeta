<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL.'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}
require_once('Compracontadoc.class.php');
$oCompracontadoc = new Compracontadoc();

$arr[0] = 'NO';
$arr[1] = 'SI';

$verificar_docs = intval($_POST['verificar_docs']);
$sql_to_verify = "";

if($verificar_docs == 1){
  //1: Docs. que tienen detraccion check, pero constancia o fecha detraccion vacio o null
  $sql_to_verify = "(ccd.tb_compracontadoc_detr = 1 AND
                      (ccd.tb_compracontadoc_fechadetr = '0000-00-00' OR ccd.tb_compracontadoc_fechadetr IS NULL
                       OR ccd.tb_compracontadoc_constdetr = '' OR ccd.tb_compracontadoc_constdetr IS NULL)
                    )";
}

//$column_name, $param.$i, $datatype
$parametros[0]['column_name'] = 'tb_compracontadoc_xac';
$parametros[0]['param0'] = 1;
$parametros[0]['datatype'] = 'INT';

$parametros[1]['column_name'] = 'tb_proveedor_idfk';
$parametros[1]['param1'] = intval($_POST['proveedor_id']);
$parametros[1]['datatype'] = 'INT';

$parametros[2]['column_name'] = 'tb_documentocontable_codfk';
$parametros[2]['param2'] = $_POST['doc_cod'];
$parametros[2]['datatype'] = 'STR';

$parametros[3]['column_name'] = 'tb_mesconta_idfk';
$parametros[3]['param3'] = intval($_POST['mesconta_id']);
$parametros[3]['datatype'] = 'INT';

$parametros[4]['param4'] = $sql_to_verify;

for ($i = 0 ; $i< count($parametros) ; $i++){
  if(empty($parametros[$i]['param'.$i])){
    $parametros[$i] = array();
  }
}

$result = $oCompracontadoc->listar_todos($parametros, "DESC");

$tr = '';
if($result['estado'] == 1){
  foreach ($result['data'] as $key => $value) {
    $tipo_doc = (strlen($value['tb_documentocontable_desc'])>30) ? substr($value['tb_documentocontable_desc'], 0, 30)."[...]" : $value['tb_documentocontable_desc'];
    $tr .='<tr>';
      $tr.='
        <td id="tabla_fila">'.$value['tb_compracontadoc_id'].'</td>
        <td id="tabla_fila">'.$tipo_doc.'</td>
        <td id="tabla_fila">'.$value['tb_compracontadoc_serie'].'-'.$value['tb_compracontadoc_numero'].'</td>
        <td id="tabla_fila">'.$value['tb_mesconta_mes'].'-'.$value['tb_mesconta_anio'].'</td>
        <td id="tabla_fila">'. mostrar_fecha($value['tb_compracontadoc_fechaemision']) .'</td>
        <td id="tabla_fila">'. $value['tb_moneda_nom'] .'</td>
        <td id="tabla_fila">'. mostrar_moneda($value['tb_compracontadoc_montototal']) .'</td>
        <td id="tabla_fila">'. $value['tipocambio_comprasunat'] .'</td>
        <td id="tabla_fila">'. $arr[intval($value['tb_compracontadoc_afecta'])] .'</td>
        <td id="tabla_fila">'.$value['tb_proveedor_nom'].'</td>
        <td id="tabla_fila" align="center">
          <button class="btn btn-info btn-xs" title="Ver"' . " onclick='compracontadoc_form(\"L\",".json_encode($value). ")'>".'<i class="fa fa-eye"></i></button>
          <button class="btn btn-warning btn-xs" title="Editar"' . " onclick='compracontadoc_form(\"M\",".json_encode($value). ")'>".'<i class="fa fa-edit"></i></button>
          <button class="btn btn-danger btn-xs" title="Eliminar"' . " onclick='compracontadoc_form(\"E\",".json_encode($value). ")'>".'<i class="fa fa-trash"></i></button>
          <button class="btn btn-primary btn-xs" title="Historial" onclick="compracontadoc_historial_form(' . $value['tb_compracontadoc_id'] . ')"><i class="fa fa-history"></i></button>
        </td>';
    $tr.='</tr>';
  }
  $result = null;
}
else {
  echo $result['mensaje'];
  $result = null;
  exit();
}
?>

<table id="tbl_compracontadocs" class="table table-hover dataTable display">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID. Doc</th>
      <th id="tabla_cabecera_fila">Tipo Doc</th>
      <th id="tabla_cabecera_fila">Numeracion</th>
      <th id="tabla_cabecera_fila">Periodo Cont.</th>
      <th id="tabla_cabecera_fila">Emision</th>
      <th id="tabla_cabecera_fila">Moneda</th>
      <th id="tabla_cabecera_fila">Monto</th>
      <th id="tabla_cabecera_fila">Tipo Cambio</th>
      <th id="tabla_cabecera_fila">Afecta</th>
      <th id="tabla_cabecera_fila">Proveedor</th>
      <th id="tabla_cabecera_fila">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
