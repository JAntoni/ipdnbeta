<form id="form_creditomenor_filtro" class="form-inline" role="form">
    <div class="form-group">
        <label for="txt_fil_proveedor_nom">Proveedor: </label>
        <input type="text" name="txt_fil_proveedor_nom" id="txt_fil_proveedor_nom" placeholder="Escribe para buscar proveedor" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value>
        <input type="hidden" name="hdd_fil_proveedor_id" id="hdd_fil_proveedor_id">
    </div>
    &nbsp; &nbsp; &nbsp; &nbsp;
    <div class="form-group">
        <label for="txt_fil_doc_cod">Tipo Doc.: </label>
        <input type="text" name="txt_fil_doc_cod" id="txt_fil_doc_cod" class="form-control input-sm ui-autocomplete-input" placeholder="Tipo de documento" autocomplete="off" size="48">
        <input type="hidden" name="hdd_fil_doc_cod" id="hdd_fil_doc_cod">
    </div>
    &nbsp; &nbsp; &nbsp; &nbsp;
    <div class="form-group">
        <label for="cmb_periodo_id">Periodo Contable: </label>
        <select name="cmb_periodo_id" id="cmb_periodo_id" class="form-control input-sm">
            <?php include VISTA_URL . 'mesconta/mesconta_select.php' ?>
        </select>
    </div>
    &nbsp; &nbsp; &nbsp; &nbsp;
    <div class="form-group">
        <label for="cmb_verificar_docs">Docs: </label>
        <select name="cmb_verificar_docs" id="cmb_verificar_docs" class="form-control input-sm">
            <option value="0" selected> - </option>
            <option value="1" style="font-weight: bold;">Falta detraccion</option>
        </select>
    </div>
</form>