<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../compracontadoc/Compracontadoc.class.php');
$oCompracontadoc = new Compracontadoc();
require_once('../compracontadetalle/Compracontadetalle.class.php');
$oCompraDetalle = new Compracontadetalle();
require_once('../plancontable/Plancontable.class.php');
$oPlan = new Plancontable();
require_once('../gasto/Gasto.class.php');
$oGasto = new Gasto();
require_once('../gastogar/Gastogar.class.php');
$oGastogar = new Gastogar();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'compracontadoc';
$registro = $_POST["registro"];
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$compracontadoc_id = $registro["tb_compracontadoc_id"];
$tip_cred_id = $registro["tb_creditotipo_idfk"];

$titulo = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar Documento de Compra';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Documento de Compra';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Documento de Compra';
} elseif ($usuario_action == 'L') {
    $titulo = 'Documento de Compra Registrado';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en compracontadoc
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos))
        $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
        $usuario_id = $_SESSION['usuario_id'];
        $modulo = 'compracontadoc';
        $modulo_id = $compracontadoc_id;
        $tipo_permiso = $usuario_action;
        $estado = 1;

        $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if ($result['estado'] == 1) {
            $bandera = 1; // si tiene el permiso para la accion que desea hacer
        } else {
            $result = NULL;
            echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
            exit();
        }
        $result = NULL;
    }

    $codigo_cuentaconta_igv = '25070302';

    $array_plan[0]['column_name'] = 'cc.tb_cuentaconta_cod';
    $array_plan[0]['param0'] = $codigo_cuentaconta_igv;
    $array_plan[0]['datatype'] = 'STR';

    $array_plan[1]['conector'] = 'AND';
    $array_plan[1]['column_name'] = 'cc.tb_cuentaconta_xac_conta';
    $array_plan[1]['param1'] = 1;
    $array_plan[1]['datatype'] = 'INT';

    $otros["group"]["column_name"] = "cc.tb_cuentaconta_id";

    $cuenta_igv = $oPlan->listar_todos($array_plan, array(), $otros)["data"][0];
    $id_cuenta_igv = intval($cuenta_igv["tb_cuentaconta_id"]);

    //si la accion es modificar, mostramos los datos del compracontadoc por su ID
    if (intval($compracontadoc_id) > 0) {
        $array[0]['column_name'] = 'tb_compracontadoc_id';
        $array[0]['param0'] = $compracontadoc_id;
        $array[0]['datatype'] = 'INT';

        $array1 = array();
        if (!empty($tip_cred_id)) {
            $array1[0]['tipo_union'] = 'LEFT';
            $array1[0]['tabla_alias'] = retorna_nombre_tabla_creditotipo($tip_cred_id) . ' cred';
            $array1[0]['columna_enlace'] = 'ccd.tb_credito_idfk';
            $array1[0]['alias_columnaPK'] = 'cred.tb_credito_id';
            $array1[0]['alias_columnasparaver'] = 'cred.tb_credito_preaco, cred.tb_cliente_id,';

            $array1[1]['tipo_union'] = 'LEFT';
            $array1[1]['tabla_alias'] = 'tb_cliente cl';
            $array1[1]['columna_enlace'] = 'cred.tb_cliente_id';
            $array1[1]['alias_columnaPK'] = 'cl.tb_cliente_id';
            $array1[1]['alias_columnasparaver'] = 'cl.tb_cliente_nom,';

            $array1[2]['tipo_union'] = 'LEFT';
            $array1[2]['tabla_alias'] = 'tb_moneda m1';
            $array1[2]['columna_enlace'] = 'cred.tb_moneda_id';
            $array1[2]['alias_columnaPK'] = 'm1.tb_moneda_id';
            $array1[2]['alias_columnasparaver'] = 'm1.tb_moneda_nom as credito_moneda,';
        }

        $result = $oCompracontadoc->mostrarUno($array, $array1);
        if ($result['estado'] != 1) {
            $mensaje =  'No se ha encontrado ningún registro para el documento de compra seleccionado, inténtelo nuevamente.';
            echo $mensaje;
            $bandera = 4;
        } else {
            $result = $result['data'];

            // todos los campos que van a ser mostrados

            $compracontadoc_id    = $result['tb_compracontadoc_id'];
            $proveedor_id         = $result['tb_proveedor_idfk'];
            $proveedor_doc        = $result['tb_proveedor_doc'];
            $proveedor_nom        = $result['tb_proveedor_nom'];
            $doc_cod              = $result['tb_documentocontable_codfk'];
            $doc_nom              = $result['tb_documentocontable_desc'];
            $serie                = $result['tb_compracontadoc_serie'];
            $numero               = $result['tb_compracontadoc_numero'];
            $condicion            = $result['tb_compracontadoc_condicion'];
            $mesconta_id          = intval($result['tb_mesconta_idfk']);
            $fecha_emision        = mostrar_fecha($result['tb_compracontadoc_fechaemision']);
            $fecha_venc_vacio     = $result['tb_compracontadoc_fechavencimiento'] == "0000-00-00";
            $fecha_vencim         = $fecha_venc_vacio ? "00-00-0000" : mostrar_fecha($result['tb_compracontadoc_fechavencimiento']); //CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
            //$dias_para_venc       = $result['tb_compracontadoc_cantdiasvenc'];//CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
            $monedacambiocontable_idfk = $result['tb_monedacambiocontable_idfk'];
            $tipo_cambio          = number_format(floatval($result['tipocambio_comprasunat']), 3); //tipocambio_ventasunat
            $moneda_id            = intval($result['tb_moneda_idfk']);
            $afecta               = intval($result['tb_compracontadoc_afecta']) == 1 ? 'checked' : '';
            $porcentaje_igv       = floatval($result['tb_compracontadoc_porcentajeigv']) * 100;
            $importe_total        = mostrar_moneda($result['tb_compracontadoc_montototal']);
            $importe_total_soles  = mostrar_moneda($result['tb_compracontadoc_montosoles']);
            $monto_base           = $afecta == 'checked' ? mostrar_moneda($result['tb_compracontadoc_baseafecta']) : mostrar_moneda($result['tb_compracontadoc_baseinafecta']);
            $monto_base_soles     = mostrar_moneda($result['tb_compracontadoc_basesoles']);
            $monto_igv            = mostrar_moneda($result['tb_compracontadoc_montoigv']);
            $monto_igv_soles      = mostrar_moneda($result['tb_compracontadoc_montoigvsoles']);
            $creditotipo_id       = (empty(intval($result['tb_creditotipo_idfk']))) ? 0 : intval($result['tb_creditotipo_idfk']);
            $anexa_credito        = ($creditotipo_id != 0) ? 'checked' : '';
            $credito_id           = $anexa_credito == 'checked' ? intval($result['tb_credito_idfk']) : '--';
            $credito_cliente_nombre = $result['tb_cliente_nom'];
            $credito_monto        = $result['credito_moneda'] . mostrar_moneda($result['tb_credito_preaco']);
            $docrefidfk           = intval($result['tb_compracontadoc_docrefidfk']);
            $docref_proveedoridfk = intval($result['docref_proveedoridfk']);
            $motivo_nc            = $result['tb_compracontadoc_motivonc'];
            $detr                 = intval($result['tb_compracontadoc_detr']) == 1 ? 'checked' : '';
            $docref_serie_numero = ($docrefidfk == 0) ? '' : $result['docreferido_serie'] . '-' . $result['docreferido_numero'];
            $docref_fechaemision  = ($docrefidfk == 0) ? '' : mostrar_fecha($result['docreferido_emision']);
            $comentario           = $result['tb_compracontadoc_detalle'];
            $constancia_detraccion = $result['tb_compracontadoc_constdetr'];
            $fecha_detraccion     = $result['tb_compracontadoc_fechadetr'] == '0000-00-00' ? '' : mostrar_fecha($result['tb_compracontadoc_fechadetr']);
            $compra_correlativo = $result['tb_compracontadoc_correlativo'];

            //buscamos los detalles
            $detalles = $oCompraDetalle->buscar_por_compracontaid($compracontadoc_id)['data'];
            //los detalles que no son el primero ni los dos ultimos se añaden mediante el JS
        }
    } else {
        if (!empty($registro["tb_compracontadoc_serie"])) {
            $proveedor_id = $registro['tb_proveedor_idfk'];
            $proveedor_doc = $registro['tb_proveedor_doc'];
            $proveedor_nom = $registro['tb_proveedor_nom'];
            $doc_cod = '01'; //por defecto aparece factura
            $doc_nom = 'FACTURA'; //por defecto aparece factura
            $serie = $registro['tb_compracontadoc_serie'];
            $numero = $registro['tb_compracontadoc_numero'];
            $condicion = $registro['tb_compracontadoc_condicion'];
            $fecha_emision = mostrar_fecha($registro['tb_compracontadoc_fechaemision']);
            $moneda_id = $registro['tb_moneda_idfk'];
            $monedacambiocontable_idfk = $registro['tb_monedacambiocontable_idfk'];
            $tipo_cambio = number_format(floatval($registro['tipocambio_comprasunat']), 3);
            $anexa_credito = 'checked';
            $afecta = 'checked';
            $porcentaje_igv = 18;
            $comentario = '';
            $cantidad_detalles = 3;
            
            $detalles = $registro['detalles'];
        }
    }
    if (!empty($detalles)) {
        $cantidad_detalles = count($detalles);
        if ($cantidad_detalles > 0) {
            for ($i = 0; $i < $cantidad_detalles - 2; $i++) {
                if (!empty($detalles[$i]['tb_gastogar_idfk']) || !empty($detalles[$i]['tb_gasto_idfk'])) {
                    $tabla = empty($detalles[$i]['tb_gastogar_idfk']) ? 'tb_gasto' : 'tb_gastogar';
                    $detalles[$i]['tabla_gasto'] = $tabla;
                    if ($tabla == 'tb_gasto') {
                        $array[0]['column_name'] = 'g.tb_gasto_id';
                        $array[0]['param0'] = $detalles[$i]['tb_gasto_idfk'];
                        $array[0]['datatype'] = 'INT';

                        $inner[0]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
                        $inner[0]['tipo_union'] = 'INNER';
                        $inner[0]['tabla_alias'] = 'tb_moneda mon';
                        $inner[0]['columna_enlace'] = 'g.tb_moneda_id';
                        $inner[0]['alias_columnaPK'] = 'mon.tb_moneda_id';

                        if ($detalles[$i]['tb_gastopago_idfk'] > 0) {
                            $array[1]['conector'] = 'AND';
                            $array[1]['column_name'] = 'gp.tb_gastopago_id';
                            $array[1]['param1'] = $detalles[$i]['tb_gastopago_idfk'];
                            $array[1]['datatype'] = 'INT';

                            $inner[1]['alias_columnasparaver'] = 'gp.tb_moneda_idfk, gp.tb_gastopago_monto';
                            $inner[1]['tipo_union'] = 'LEFT';
                            $inner[1]['tabla_alias'] = 'tb_gastopago gp';
                            $inner[1]['columna_enlace'] = 'g.tb_gasto_id';
                            $inner[1]['alias_columnaPK'] = 'gp.tb_gasto_idfk';

                            $inner[2]['alias_columnasparaver'] = 'mon1.tb_moneda_nom as moneda_pago';
                            $inner[2]['tipo_union'] = 'INNER';
                            $inner[2]['tabla_alias'] = 'tb_moneda mon1';
                            $inner[2]['columna_enlace'] = 'gp.tb_moneda_idfk';
                            $inner[2]['alias_columnaPK'] = 'mon1.tb_moneda_id';
                        }

                        $res = $oGasto->listar_todos($array, $inner, array())['data'][0];
                        unset($inner);

                        $detalles[$i]['tipocredito_id'] = $res['tb_gasto_cretip'];
                        $detalles[$i]['credito_id'] = $res['tb_credito_id'];
                        $detalles[$i]['gasto_id'] = $res['tb_gasto_id'];
                        $detalles[$i]['proveedor_id'] = $res['tb_proveedor_id'];
                        $detalles[$i]['garantia_nom'] = '';
                        $detalles[$i]['monto_gasto'] = $res['tb_moneda_nom'].$res['tb_gasto_ptl'];
                        $detalles[$i]['gastopago_id'] = 0;
                        if($detalles[$i]['tb_gastopago_idfk'] > 0){
                            $detalles[$i]['monto_gasto'] = $res['moneda_pago'].$res['tb_gastopago_monto'];
                            $detalles[$i]['gastopago_id'] = $detalles[$i]['tb_gastopago_idfk'];
                        }
                    } else {
                        $array[0]['column_name'] = 'gg.tb_gastogar_id';
                        $array[0]['param0'] = $detalles[$i]['tb_gastogar_idfk'];
                        $array[0]['datatype'] = 'INT';

                        $res = $oGastogar->mostrarUno($array, array())['data'];

                        $detalles[$i]['tipocredito_id'] = $res['tb_creditotipo_idfk'];
                        $detalles[$i]['credito_id'] = $res['tb_credito_idfk'];
                        $detalles[$i]['gasto_id'] = $res['tb_gastogar_id'];
                        $detalles[$i]['proveedor_id'] = $res['tb_proveedor_idfk'];
                        $detalles[$i]['garantia_nom'] = $res['tb_garantia_pro'];
                        $detalles[$i]['monto_gasto'] = $res['tb_moneda_nom'].$res['tb_gastogar_monto'];
                        $detalles[$i]['gastopago_id'] = 0;
                    }
                }
            }
        }
    }
} else {
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_compracontadoc" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title mayus" style="font-size: 16px"><b><?php echo $titulo ?></b></h4>
                </div>

                <form id="form_compracontadoc" method="post" style="font-size: 11px">

                    <input type="hidden" name="action" id="action" value="<?php echo $action ?>">
                    <input type="hidden" name="hdd_compracontadoc_id" id="hdd_compracontadoc_id" value="<?php echo $action == 'insertar' ? 0 : $compracontadoc_id ?>">
                    <input type="hidden" id="hdd_compracontadoc_registro" name="hdd_compracontadoc_registro" value='<?php echo json_encode($result) ?>'>

                    <div class="modal-body">
                        <?php if ($action == 'insertar' || $action == 'modificar')
                            echo '<label style="color: green; font-size: 12px">- La fecha de emisión del documento debe pertenecer a un Mes contable APERTURADO y NO DECLARADO. Verifique <a href="mesconta" target="_blank">AQUI</a>.<br>' .
                                '- Verifique los montos generados por el sistema.</label>' ?>
                        <!-- espacio para los campos -->
                        <div class="box box-primary shadow">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="hidden" id="hdd_proveedor_id" name="hdd_proveedor_id" value='<?php echo $proveedor_id ?>'>
                                            <label>RUC/DNI PROVEEDOR:</label>
                                            <div id="proveedor-grp" class="input-group">
                                                <input type="text" id="txt_proveedor_doc" name="txt_proveedor_doc" placeholder="Escribe para buscar proveedor" class="form-control mayus" value="<?php echo $proveedor_doc; ?>">
                                                <span class="input-group-btn">
                                                    <button id="btn_mod" class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('M', 0)" title="MODIFICAR PROVEEDOR">
                                                        <span class="fa fa-pencil icon"></span>
                                                    </button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button id="btn_agr" class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('I', 0)" title="AGREGAR PROVEEDOR">
                                                        <span class="fa fa-plus icon"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <label for="txt_proveedor_nom">PROVEEDOR:</label><br>
                                        <input type="text" id="txt_proveedor_nom" name="txt_proveedor_nom" placeholder="Escribe para buscar proveedor" class="form-control mayus" value="<?php echo $proveedor_nom ?>">
                                    </div>
                                    <div class="col-md-1">
                                        <label for="btn-limpiar" style="color:white;">l</label>
                                        <span class="input-group-btn">
                                            <button id="btn-limpiar" class="btn btn-primary btn-sm" type="button" onclick="limpiar_proveedor()" title="LIMPIAR DATOS DEL PROVEEDOR">
                                                <span class="fa fa-eraser icon"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="txt_doc_cod">CODIGO DE TIPO DOC:</label><br>
                                        <input type="text" name="txt_doc_cod" id="txt_doc_cod" placeholder="Escribe para buscar Tipo de documento" class="form-control mayus" value="<?php echo $doc_cod ?>">
                                    </div>
                                    <div class="col-md-7">
                                        <label for="txt_doc_nom">TIPO DE DOCUMENTO:</label><br>
                                        <input type="text" name="txt_doc_nom" id="txt_doc_nom" placeholder="Escribe para buscar Tipo de documento" class="form-control mayus" value="<?php echo $doc_nom ?>">
                                    </div>
                                    <div class="col-md-1">
                                        <label for="btn-limpiar-1" style="color:white;">l</label>
                                        <span class="input-group-btn">
                                            <button id="btn-limpiar-1" class="btn btn-primary btn-sm" type="button" onclick="limpiar_tipodoc()" title="LIMPIAR TIPO DE DOCUMENTO">
                                                <span class="fa fa-eraser icon"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="txt_doc_serie">SERIE:</label>
                                        <input type="text" name="txt_doc_serie" id="txt_doc_serie" placeholder="F001" class="form-control mayus" onfocus="formato_serie_contable('txt_doc_cod', this)" value="<?php echo $serie ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_doc_numero">NUMERO:</label>
                                        <input type="text" name="txt_doc_numero" id="txt_doc_numero" placeholder="00000001" class="form-control numero-doc" onfocus="formato_numero_contable(this)" value="<?php echo $numero ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_doc_condicion">CONDICION:</label>
                                        <input type="text" name="txt_doc_condicion" id="txt_doc_condicion" class="form-control mayus disabled" value="<?php echo $condicion ?>">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="compracontadoc_emi_picker">FECHA EMISION:</label>
                                        <div class='input-group date'>
                                            <input type='text' class="form-control" placeholder="DD-MM-AAAA" name="compracontadoc_emi_picker" id="compracontadoc_emi_picker" value="<?php echo $fecha_emision ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="cmb_mesconta_id">PERIODO CONTABLE:</label>
                                        <select class="form-control mayus" name="cmb_mesconta_id" id="cmb_mesconta_id">
                                            <?php require_once('../mesconta/mesconta_select.php') ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_compraconta_correlativo">CORRELATIVO:</label>
                                        <input type="text" name="txt_compraconta_correlativo" id="txt_compraconta_correlativo" placeholder="CORRELATIVO" class="form-control mayus disabled" value="<?php echo $compra_correlativo ?>">
                                    </div>
                                </div>

                                <div class="row documento_ref" style="<?php if ($doc_cod != '07') {echo 'display: none;';} ?>">
                                    <input type="hidden" name="txt_docrefidfk" id="txt_docrefidfk" value="<?php echo $docrefidfk ?>">
                                    <input type="hidden" name="txt_docref_proveedorid" id="txt_docref_proveedorid" value="<?php echo $docref_proveedoridfk ?>">
                                    <div class="col-md-3">
                                        <label for="txt_documento_ref">DOCUMENTO REFERIDO:</label>
                                        <input type="text" name="txt_documento_ref" id="txt_documento_ref" placeholder="F001-000000001" class="form-control mayus" value="<?php echo $docref_serie_numero ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_docref_fechaemision">FECHA EMISION DOC. REFERIDO:</label>
                                        <div class='input-group date'>
                                            <input type='text' class="form-control disabled" placeholder="DD-MM-AAAA" name="txt_docref_fechaemision" id="txt_docref_fechaemision" value="<?php echo $docref_fechaemision ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_motivo_nc">MOTIVO DE NC:</label>
                                        <textarea class="form-control mayus" name="txt_motivo_nc" id="txt_motivo_nc"><?php echo $motivo_nc ?></textarea>
                                    </div>
                                </div>

                                <div class="row fecha_ven" style="<?php if ($doc_cod != '14') { echo 'display: none;'; } ?>">
                                    <input type="hidden" name="fecvenc_vacio" id="fecvenc_vacio" value="<?php echo $fecha_venc_vacio; ?>">
                                    <div class="col-md-3 form-group">
                                        <label for="compracontadoc_venc_picker">FECHA VENCIMIENTO:</label>
                                        <div class='input-group date'>
                                            <input type="text" class="form-control" placeholder="DD-MM-AAAA" name="compracontadoc_venc_picker" id="compracontadoc_venc_picker" value="<?php echo $fecha_vencim; ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="fila-moneda-igv">
                                    <div class="col-md-6">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="cmb_moneda_id" class="control-label">MONEDA:</label>
                                                <select class="form-control" name="cmb_moneda_id" id="cmb_moneda_id" style="width: 65px;">
                                                    <?php require_once('../moneda/moneda_select.php') ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <input type="hidden" name="hdd_monedacambiocontable_idfk" id="hdd_monedacambiocontable_idfk" value="<?php echo $monedacambiocontable_idfk ?>">
                                            <div class="form-group">
                                                <label for="txt_tipcam_cont_tipcam" class="control-label">T. CAMBIO:</label>
                                                <input type="text" name="txt_tipcam_cont_tipcam" id="txt_tipcam_cont_tipcam" class="form-control disabled" value="<?php echo $tipo_cambio ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="badge bg-red" id="span_error" style="display: none;"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-5">
                                            <label for="che_afecta" class="control-label" style="color:white;"></label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="che_afecta" name="che_afecta" class="flat-green" style="position: relative;" value="1" <?php echo $afecta ?>><b>&nbsp; AFECTA IGV</b>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 es_afecta" <?php if ($afecta != 'checked') echo 'style="display: none;"' ?>>
                                            <div class="form-group">
                                                <label for="txt_porc_igv">% IGV:</label>
                                                <input type="text" name="txt_porc_igv" id="txt_porc_igv" placeholder="18" class="form-control porc" value="<?php echo $porcentaje_igv ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-11" style="width: 97%;">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-weight: bold; font-size: 11.5px;">DETALLE DE LA COMPRA:</span>
                                            </div>
                                            <div class="col-md-6" align="right">
                                                <a href="javascript:void(0)" id="add_row" onclick="nuevo_detalle(1)" style="font-size: 13px"><b class="mayus"><u>Añadir fila</u></b></a>
                                            </div>
                                        </div>
                                        <input type="hidden" id="hdd_compracontadetalle_array" name="hdd_compracontadetalle_array" value='<?php echo json_encode($detalles) ?>'>
                                        <input type="hidden" name="cantidad_filas_detalle" id="cantidad_filas_detalle" value="<?php echo 3; ?>">
                                        <table id="tbl_detalle" name="tbl_detalle" class="table table-hover" style="word-wrap:break-word;">
                                            <thead>
                                                <tr id="tabla_cabecera">
                                                    <th id="tabla_cabecera_fila" style="width: 11%;">Cod. Cuenta Cont.</th>
                                                    <th id="tabla_cabecera_fila" style="width: 37%;">Glosa</th>
                                                    <th id="tabla_cabecera_fila" style="width: 10%;">Monto</th>
                                                    <th id="tabla_cabecera_fila" style="width: 16%;">Sede</th>
                                                    <th id="tabla_cabecera_fila" style="width: 22%;">Anexar Gasto</th>
                                                    <th id="tabla_cabecera_fila" style="width: 4%;"> </th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody">
                                                <tr id="tabla_cabecera_fila_0">
                                                    <input type="hidden" name="txt_detalleid_0" id="txt_detalleid_0" value="<?php echo $detalles[0]['tb_compracontadetalle_id']; ?>">
                                                    <input type="hidden" name="txt_detalleorden_0" id="txt_detalleorden_0" value="<?php echo $detalles[0]['tb_compracontadetalle_orden']; ?>">
                                                    <td id="tabla_fila">
                                                        <input type="text" name="txt_codcuentacont_0" id="txt_codcuentacont_0" class="form-control" placeholder="COD. CTA. CONT." value="<?php echo $detalles[0]['tb_cuentaconta_cod']; ?>">
                                                    </td>
                                                    <td id="tabla_fila"><input type="text" name="txt_glosa_0" id="txt_glosa_0" class="form-control mayus" placeholder="GLOSA" value="<?php echo $detalles[0]['tb_compracontadetalle_glosa']; ?>"></td>
                                                    <td id="tabla_fila" class="monto"><input type="text" name="txt_monto_0" id="txt_monto_0" class="form-control moneda para_calcular" placeholder="0.00" value="<?php echo $detalles[0]['tb_compracontadetalle_monto']; ?>"></td>
                                                    <td id="tabla_fila">
                                                        <input type="hidden" name="txt_sedenombre_0" id="txt_sedenombre_0">
                                                        <select name="cbo_sede_0" id="cbo_sede_0" class="form-control"><?php require '../empresa/empresa_select.php' ?></select>
                                                    </td>
                                                    <td id="tabla_fila">
                                                        <input type="hidden" name="hdd_tipocreditoid_0" id="hdd_tipocreditoid_0">
                                                        <input type="hidden" name="hdd_creditoid_0" id="hdd_creditoid_0">
                                                        <input type="hidden" name="hdd_tablagasto_0" id="hdd_tablagasto_0">
                                                        <input type="hidden" name="hdd_gastoid_0" id="hdd_gastoid_0">
                                                        <input type="hidden" name="hdd_gastopagoid_0" id="hdd_gastopagoid_0">
                                                        <input type="hidden" name="hdd_gastoproveedorid_0" id="hdd_gastoproveedorid_0">
                                                        <ul class="products-list product-list-in-box">
                                                            <li>
                                                                <div class="col-md-1" style="padding-left: 5px; padding-right: 7px;">
                                                                    <?php if (!empty($detalles[0]['tabla_gasto'])) {
                                                                        echo "<i class='fa fa-eye text-aqua'></i><br>";
                                                                    } else {
                                                                        echo "<i class='fa fa-plus text-blue'></i><br>";
                                                                    } ?>
                                                                    <i class="fa fa-trash text-red"></i>
                                                                </div>
                                                                <div id="div_gastomensaje_0" class="col-md-11" style="padding-left: 7px; padding-right: 5px;">
                                                                    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td id="tabla_fila"></td>
                                                </tr>
                                                
                                                <tr id="tabla_cabecera_fila_1" class="auto">
                                                    <input type="hidden" name="txt_detalleid_1" id="txt_detalleid_1" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_compracontadetalle_id']; ?>">
                                                    <input type="hidden" name="txt_detalleorden_1" id="txt_detalleorden_1" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_compracontadetalle_orden']; ?>">
                                                    <td id="tabla_fila">
                                                        <input type="text" name="txt_codcuentacont_1" id="txt_codcuentacont_1" class="form-control disabled" value="<?php echo $codigo_cuentaconta_igv; ?>">
                                                    </td>
                                                    <td id="tabla_fila"><input type="text" name="txt_glosa_1" id="txt_glosa_1" class="form-control mayus" placeholder="IGV" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_compracontadetalle_glosa']; ?>"></td>
                                                    <td id="tabla_fila" class="monto"><input type="text" name="txt_monto_1" id="txt_monto_1" class="form-control moneda monto_igv" placeholder="0.00" onfocus="formato_moneda(this)" value="<?php echo $detalles[$cantidad_detalles - 2]['tb_compracontadetalle_monto']; ?>"></td>
                                                    <td id="tabla_fila">
                                                        <input type="hidden" name="txt_sedenombre_1" id="txt_sedenombre_1">
                                                        <select name="cbo_sede_1" id="cbo_sede_1" class="form-control disabled">
                                                            <option value="0"></option>
                                                        </select>
                                                    </td>
                                                    <td id="tabla_fila"></td>
                                                    <td id="tabla_fila" class="text-center"></td>
                                                </tr>
                                                
                                                <tr id="tabla_cabecera_fila_2" class="auto">
                                                    <input type="hidden" name="txt_detalleid_2" id="txt_detalleid_2" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_compracontadetalle_id']; ?>">
                                                    <input type="hidden" name="txt_detalleorden_2" id="txt_detalleorden_2" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_compracontadetalle_orden']; ?>">
                                                    <td id="tabla_fila">
                                                        <input type="text" name="txt_codcuentacont_2" id="txt_codcuentacont_2" class="form-control" placeholder="COD. CTA. CONT." value="<?php echo $detalles[$cantidad_detalles - 1]['tb_cuentaconta_cod']; ?>">
                                                    </td>
                                                    <td id="tabla_fila"><input type="text" name="txt_glosa_2" id="txt_glosa_2" class="form-control mayus" placeholder="GLOSA" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_compracontadetalle_glosa']; ?>"></td>
                                                    <td id="tabla_fila" class="monto"><input type="text" name="txt_monto_2" id="txt_monto_2" class="form-control moneda monto_total" placeholder="0.00" onfocus="formato_moneda(this)" value="<?php echo $detalles[$cantidad_detalles - 1]['tb_compracontadetalle_monto']; ?>"></td>
                                                    <td id="tabla_fila">
                                                        <input type="hidden" name="txt_sedenombre_2" id="txt_sedenombre_2">
                                                        <select name="cbo_sede_2" id="cbo_sede_2" class="form-control disabled">
                                                            <option value="0"></option>
                                                        </select>
                                                    </td>
                                                    <td id="tabla_fila"></td>
                                                    <td id="tabla_fila"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <span style="font-weight: bold; font-size: 11.5px;">RESUMEN:</span>
                                <div id="box_resumen" class="row">
                                    <div id="box_redim" class="col-md-5" style="width: <?php echo $moneda_id == 2 ? '48.8' : '24.2'; ?>%;">
                                        <!-- datos en moneda original -->
                                        <div class="box box-primary shadow">
                                            <div class="box-header">
                                                <!-- fila 1 -->
                                                <div class="row">
                                                    <div id="div_mon_ori_1" class="col-md-<?php echo $moneda_id == 2 ? '6' : '12'; ?>">
                                                        <label id="lbl_monto_base_ori" class="lbl-afecta lbl-moneda" for="txt_monto_base_red">BASE <?php echo $afecta != 'checked' ? 'INAFECTA' : 'AFECTA' ?> :</label>
                                                        <input type="text" name="txt_monto_base_red" id="txt_monto_base_red" placeholder="" class="form-control moneda montos-mone-ori disabled" value="<?php echo $monto_base ?>">
                                                    </div>
                                                    <div class="col-md-6 moneda-dolar" <?php echo $moneda_id == 2 ? '' : 'style="display: none;"' ?>>
                                                        <label id="lbl_monto_base_sol" class="lbl-afecta" for="txt_monto_base_sol_red">BASE <?php echo $afecta != 'checked' ? 'INAFECTA' : 'AFECTA' ?> (S/.):</label>
                                                        <input type="text" name="txt_monto_base_sol_red" id="txt_monto_base_sol_red" placeholder="" class="form-control moneda" value="<?php echo $monto_base_soles ?>">
                                                    </div>
                                                </div>
                                                <br><!-- fila 2 -->
                                                <div class="row">
                                                    <div id="div_mon_ori_2" class="col-md-<?php echo $moneda_id == 2 ? '6' : '12'; ?>">
                                                        <label id="lbl_monto_igv_ori" class="lbl-moneda" for="txt_monto_igv_red">MONTO IGV :</label>
                                                        <input type="text" name="txt_monto_igv_red" id="txt_monto_igv_red" placeholder="" class="form-control moneda montos-mone-ori disabled" value="<?php echo $monto_igv ?>">
                                                    </div>
                                                    <div class="col-md-6 moneda-dolar" <?php echo $moneda_id == 2 ? '' : 'style="display: none;"' ?>>
                                                        <label id="lbl_monto_igv_sol" for="txt_monto_igv_sol_red">MONTO IGV (S/.):</label>
                                                        <input type="text" name="txt_monto_igv_sol_red" id="txt_monto_igv_sol_red" placeholder="" class="form-control moneda" value="<?php echo $monto_igv_soles ?>">
                                                    </div>
                                                </div>
                                                <br>
                                                <!-- fila 3 -->
                                                <div class="row">
                                                    <div id="div_mon_ori_3" class="col-md-<?php echo $moneda_id == 2 ? '6' : '12'; ?>">
                                                        <label id="lbl_monto_total_ori" class="lbl-moneda" for="txt_importe_total">IMPORTE TOTAL :</label>
                                                        <input type="text" name="txt_importe_total" id="txt_importe_total" placeholder="" class="form-control moneda disabled" value="<?php echo $importe_total ?>">
                                                    </div>
                                                    <div class="col-md-6 moneda-dolar" <?php echo $moneda_id == 2 ? '' : 'style="display: none;"' ?>>
                                                        <label id="lbl_monto_total_sol" for="txt_importe_total_sol_red">IMPORTE TOTAL (S/.):</label>
                                                        <input type="text" name="txt_importe_total_sol_red" id="txt_importe_total_sol_red" placeholder="" class="form-control moneda" value="<?php echo $importe_total_soles ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- detraccion -->
                                <div class="row detrac" style="<?php echo ($afecta == 'checked') ? '' : 'display: none;' ?>">
                                    <br>
                                    <div class="col-md-3">
                                        <label for="che_detr" class="control-label" style="color:white;"></label>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="che_detr" name="che_detr" class="flat-green" style="position: relative;" value="1" <?php echo $detr ?>><b>&nbsp; DETRACCION</b>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 es_detr" <?php if ($detr != 'checked') echo 'style="display: none;"' ?>>
                                        <label for="txt_const_detraccion">NRO. CONSTANCIA DETRACCION:</label>
                                        <input type="text" name="txt_const_detraccion" id="txt_const_detraccion" placeholder="" class="form-control mayus" value="<?php echo $constancia_detraccion ?>">
                                    </div>
                                    <div class="col-md-4 es_detr" <?php if ($detr != 'checked') echo 'style="display: none;"' ?>>
                                        <label for="compracontadoc_detraccion_picker">FECHA DETRACCION:</label>
                                        <div class='input-group date'>
                                            <input type='text' class="form-control" placeholder="DD-MM-AAAA" name="compracontadoc_detraccion_picker" id="compracontadoc_detraccion_picker" value="<?php echo $fecha_detraccion ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txt_comentario">COMENTARIO:</label>
                                        <textarea class="form-control mayus" name="txt_comentario" id="txt_comentario"><?php echo $comentario ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                        <?php if ($action == 'eliminar') : ?>
                            <div class="callout callout-warning">
                                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Documento de compra?</h4>
                            </div>
                        <?php endif ?>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="compracontadoc_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_compracontadoc">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_compracontadoc">Aceptar</button>
                            <?php endif ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($bandera == 4) : ?>
    <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_compracontadoc">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Mensaje Importante</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <?php echo $mensaje ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

<script type="text/javascript" src="<?php echo 'vista/compracontadoc/compracontadoc_form.js?ver=2404236'; ?>"></script>