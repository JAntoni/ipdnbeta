<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../compracontadoc/Compracontadoc.class.php');
$oCompracontadoc = new Compracontadoc();
require_once('../mesconta/Mesconta.class.php');
$oMesconta = new Mesconta();
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../plancontable/Plancontable.class.php');
$oCuentaconta = new Plancontable();
require_once('../gastogar/Gastogar.class.php');
$oGastogar = new Gastogar();
require_once('../gastopago/Gastopago.class.php');
$oGastopago = new Gastopago();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
require_once('../compracontadetalle/Compracontadetalle.class.php');

$action = $_POST['action'];
$compracontadoc_id = intval($_POST['hdd_compracontadoc_id']);
$proveedor_id	= intval($_POST['hdd_proveedor_id']);
$doc_cod		= $_POST['txt_doc_cod'];
$compracontadoc_serie = strtoupper($_POST['txt_doc_serie']);
$compracontadoc_numero = $_POST['txt_doc_numero'];
$length_detalles = intval($_POST['cantidad_filas_detalle']);
$usuario_id = intval($_SESSION['usuario_id']);
$detalles_origin = (array) json_decode($_POST['hdd_compracontadetalle_array']);
$contador = 0;
while ($contador < count($detalles_origin)) {
	$detalles_origin[$contador] = (array) $detalles_origin[$contador];
	$contador++;
}

$oCompracontadoc->setTbCompracontadocUsumod($usuario_id);
$data['estado'] = 0;

if($action == 'eliminar'){
	$oCompracontadoc->setTbCompracontadocId($compracontadoc_id);
	
	$data['mensaje'] = 'Existe un error al eliminar el documento.';

	if($oCompracontadoc->eliminar()){
		//eliminar o desactivar sus detalles
		for($i = 0; $i < $length_detalles; $i++){
			$oCompraDetalle = new Compracontadetalle();
			$oCompraDetalle->setTbCompracontadetalleId(intval($_POST['txt_detalleid_'. $i]));
			$oCompraDetalle->setTbCompracontadetalleUsumod($usuario_id);
			$oCompraDetalle->eliminar();

			if(!empty($detalles_origin[$i]['tb_gastogar_idfk'])){
				$oGastogar->modificar_campo($detalles_origin[$i]['tb_gastogar_idfk'], 'tb_gastogar_anexado', 0, 'INT');
			}
			elseif (!empty($detalles_origin[$i]['tb_gastopago_idfk'])) {
				$oGastopago->modificar_campo($detalles_origin[$i]['tb_gastopago_idfk'], 'tb_gastopago_anexado', 0, 'INT');
			}
		}

        $data['estado'] = 1;
        $data['mensaje'] = 'Se eliminó el Documento de compra y sus detalles';

        //insertar historial
        $oHist->setTbHistUsureg($usuario_id);
        $oHist->setTbHistNomTabla('tb_compracontadoc');
        $oHist->setTbHistRegmodid($compracontadoc_id);
        $oHist->setTbHistDet('Eliminó el documento de compra contable de numeración: '.$compracontadoc_serie.'-'.$compracontadoc_numero);
        $oHist->insertar();
    }
}
else{
	$data['mensaje'] = 'Existe un error al '.$action.' el documento de compra.';

	///ESPACIO PARA VERIFICAR SI YA ESTA REGISTRADO EL DOCUMENTO . COMBINACION DEL PROVEEDORID-DOCCOD-SERIE-CORRELATIVO MOSTRARUNO
	$array[0]['column_name'] = 'tb_proveedor_idfk';
    $array[0]['param0'] = $proveedor_id;
    $array[0]['datatype'] = 'INT';

	$array[1]['column_name'] = 'tb_documentocontable_codfk';
    $array[1]['param1'] = $doc_cod;
    $array[1]['datatype'] = 'STR';

	$array[2]['column_name'] = 'tb_compracontadoc_serie';
    $array[2]['param2'] = $compracontadoc_serie;
    $array[2]['datatype'] = 'STR';

	$array[3]['column_name'] = 'tb_compracontadoc_numero';
    $array[3]['param3'] = $compracontadoc_numero;
    $array[3]['datatype'] = 'STR';

	$array[4]['column_name'] = 'tb_compracontadoc_xac';
    $array[4]['param4'] = 1;
    $array[4]['datatype'] = 'INT';

    $array1 = array();
	$result = $oCompracontadoc->mostrarUno($array, $array1);

	$existe = $result['estado'];
    $registro_coincide = $result['data'];
	$evaluar0 = $compracontadoc_id == intval($registro_coincide['tb_compracontadoc_id']);
	
	if(strpos($action, '_verificar_documento') > 0 && (!$evaluar0)){
		if($existe == 1){
			$data['estado'] = 1;
			$data['mensaje'] = 'El documento compra '.$compracontadoc_serie.'-'.$compracontadoc_numero.' del proveedor '.$registro_coincide['tb_proveedor_nom'].' ya está registrado, con id: '.$registro_coincide['tb_compracontadoc_id'];
		}
	}
	elseif($action == 'buscar_credito'){
		$tipo_credito_id = intval($_POST['tipo_credito_id']);
		$credito_id 	 = intval($_POST['credito_id']);
		$oCred = null;
	
		switch ($tipo_credito_id) {
			case 1:
				require_once('../creditomenor/Creditomenor.class.php');
				$oCred = new Creditomenor();
				break;
			case 2:
				require_once('../creditoasiveh/Creditoasiveh.class.php');
				$oCred = new Creditoasiveh();
				break;
			case 3:
				require_once('../creditogarveh/Creditogarveh.class.php');
				$oCred = new Creditogarveh();
				break;
			case 4:
				require_once('../creditohipo/Creditohipo.class.php');
				$oCred = new Creditohipo();
				break;
		}
	
		$data['estado'] = 0;
		$data['mensaje'] = 'El Credito ingresado no existe.';
		$data['data'] = '';
	
		$resultado = $oCred->mostrarUno($credito_id);
		if($resultado['estado'] == 1){
			$data['estado'] = 1;
			$data['mensaje'] = 'Credito encontrado';
			$data['data'] = $resultado['data'];
		}
	}
	elseif($action == 'validar_mes_conta'){
		$mesconta_id = intval($_POST['cmb_mesconta_id']);

		// NO SE PERMITE MANIPULAR DOCUMENTOS QUE NO ESTEN EN MES CONTABLE NO APERTURADO, NI DECLARADO A SUNAT
		if (!empty($mesconta_id)){
			$arreglo[0]['column_name'] = 'tb_mesconta_id';
			$arreglo[0]['param0'] = $mesconta_id;
			$arreglo[0]['datatype'] = 'INT';

			$result = $oMesconta->mostrarUno($arreglo);

			if ($result['estado'] == 0) {
				//no está registrado el mes contable
				$data['estado'] = 3;
				$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está registrado. Verifique en modulo mes contable';
				echo json_encode($data);
				exit();
			} else {
				//está registrado. Si aperturado =0
				if (intval($result['data']['tb_mesconta_aperturado']) == 0) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está aperturado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				//sino si declarado=1
				else if (intval($result['data']['tb_mesconta_estadeclarado']) == 1) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' ya está declarado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				else if (empty($result['data']['tb_mesconta_compracorrel'])){
					$data['estado'] = 3;
					$data['mensaje'] = 'Falta configurar el correlativo de compras del mes ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . '. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				else{
					$data['estado'] = 4;
					$data['data'] = $result['data']['tb_mesconta_compracorrel'];
					echo json_encode($data);
					exit();
				}
			}
		}
	}
	elseif ($action == 'buscar_cuentaconta') {
		$arr[0]["column_name"] = "cc.tb_cuentaconta_cod";
		$arr[0]["param0"] = $_POST['cuenta_conta'];
		$arr[0]["datatype"] = "STR";

		$result = $oCuentaconta->listar_todos($arr, array(), array());
		$data['estado'] = $result['data'][0]['tb_cuentaconta_id'] > 0 ? 1 : 0;
		$data['registro'] = $data['estado'] == 1 ? $result["data"][0] : null;
		$data['mensaje'] = $data['estado'] == 0 ? "No existe la cuenta contable {$_POST['cuenta_conta']}" : '';
	}
	elseif($action == 'insertar' || $action == 'modificar'){
		$moneda_id		= intval($_POST['cmb_moneda_id']);
		$afecta			= intval($_POST['che_afecta']);
		$mesconta_id	= intval($_POST['cmb_mesconta_id']);
		$correlativo	= strtoupper($_POST['txt_compraconta_correlativo']);
		$fecha_venc		= $_POST['compracontadoc_venc_picker'] == "" ? '0000-00-00' : fecha_mysql($_POST['compracontadoc_venc_picker']);
		$fecha_venc		= ($doc_cod == '14') ? $fecha_venc : fecha_mysql($_POST['compracontadoc_emi_picker']);

		if(empty($_POST['txt_tipcam_cont_tipcam'])){
			$data['estado'] = 0;
			$data['mensaje'] = 'Verifique el registro del tipo de Cambio';
			echo json_encode($data);
			exit();
		}

		// NO SE PERMITE MANIPULAR DOCUMENTOS QUE NO ESTEN EN MES CONTABLE NO APERTURADO, NI DECLARADO A SUNAT
		if (!empty($mesconta_id)){
			$arreglo[0]['column_name'] = 'tb_mesconta_id';
			$arreglo[0]['param0'] = $mesconta_id;
			$arreglo[0]['datatype'] = 'INT';

			$result = $oMesconta->mostrarUno($arreglo);

			if ($result['estado'] == 0) {
				//no está registrado el mes contable
				$data['estado'] = 3;
				$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está registrado. Verifique en modulo mes contable';
				echo json_encode($data);
				exit();
			} else {
				//está registrado. Si aperturado =0
				if (intval($result['data']['tb_mesconta_aperturado']) == 0) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' no está aperturado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				//sino si declarado=1
				else if (intval($result['data']['tb_mesconta_estadeclarado']) == 1) {
					$data['estado'] = 3;
					$data['mensaje'] = 'No se puede operar, el mes contable ' .$result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . ' ya está declarado. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
				else if (empty($result['data']['tb_mesconta_compracorrel'])){
					$data['estado'] = 3;
					$data['mensaje'] = 'Falta configurar el correlativo de compras del mes ' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . '. Verifique en modulo mes contable';
					echo json_encode($data);
					exit();
				}
			}
		}
		else{
			$data['estado'] = 0;
			$data['mensaje'] = 'Verifique el periodo contable seleccionado';
			echo json_encode($data);
			exit();
		}

		$oCompracontadoc->setTbProveedorIdfk($proveedor_id);
		$oCompracontadoc->setTbCompracontadocCodfk($doc_cod);
		$oCompracontadoc->setTbCompracontadocSerie(strtoupper($compracontadoc_serie));
		$oCompracontadoc->setTbCompracontadocNumero($compracontadoc_numero);
		$oCompracontadoc->setTbCompracontadocFechaemision(fecha_mysql($_POST['compracontadoc_emi_picker']));
		$oCompracontadoc->setTbCompracontadocFechavencimiento($fecha_venc);
		$oCompracontadoc->setTbCompracontadocCantdiasvenc(0);
		$oCompracontadoc->setTbMescontaIdfk($mesconta_id);
		$oCompracontadoc->setTbMonedaIdfk($moneda_id);
		$oCompracontadoc->setTbCompracontadocAfecta($afecta);
		$porc_igv = $_POST['txt_porc_igv'] == '' ? 0.0 : floatval(intval($_POST['txt_porc_igv']) / 100);
		$oCompracontadoc->setTbCompracontadocPorcentajeigv($porc_igv);

		$oCompracontadoc->setTbCompracontadocMontototal(moneda_mysql($_POST['txt_importe_total']));
		$oCompracontadoc->setTbCompracontadocMontoigv(moneda_mysql($_POST['txt_monto_igv_red']));
		$oCompracontadoc->setTbCompracontadocDetr(intval($_POST['che_detr']));
		if ($moneda_id == 2) {
			$oCompracontadoc->setTbMonedacambiocontableIdfk(intval($_POST['hdd_monedacambiocontable_idfk']));
			$oCompracontadoc->setTbCompracontadocBasesoles(moneda_mysql($_POST['txt_monto_base_sol_red']));
			$oCompracontadoc->setTbCompracontadocMontoigvsoles(moneda_mysql($_POST['txt_monto_igv_sol_red']));
			$oCompracontadoc->setTbCompracontadocMontosoles(moneda_mysql($_POST['txt_importe_total_sol_red']));
		} else {
			$oCompracontadoc->setTbCompracontadocBasesoles(moneda_mysql($_POST['txt_monto_base_red']));
			$oCompracontadoc->setTbCompracontadocMontoigvsoles(moneda_mysql($_POST['txt_monto_igv_red']));
			$oCompracontadoc->setTbCompracontadocMontosoles(moneda_mysql($_POST['txt_importe_total']));
		}
		if ($afecta == 1) {
			$oCompracontadoc->setTbCompracontadocBaseafecta(moneda_mysql($_POST['txt_monto_base_red']));
			$oCompracontadoc->setTbCompracontadocBaseinafecta(moneda_mysql(0));

			if (floatval($oCompracontadoc->getTbCompracontadocMontosoles()) >= 700 && intval($_POST['che_detr']) == 1) {
				$oCompracontadoc->setTbCompracontadocConstdetr(strtoupper($_POST['txt_const_detraccion']));
				$fecha_detr = empty($_POST['compracontadoc_detraccion_picker']) ? '0000-00-00' : fecha_mysql($_POST['compracontadoc_detraccion_picker']);
				$oCompracontadoc->setTbCompracontadocFechadetr($fecha_detr);
			}
		} else {
			$oCompracontadoc->setTbCompracontadocBaseafecta(moneda_mysql(0));
			$oCompracontadoc->setTbCompracontadocBaseinafecta(moneda_mysql($_POST['txt_monto_base_red']));
		}
		if ($doc_cod == '07') {
			if (intval($_POST['txt_docref_proveedorid']) != $proveedor_id) {
				$data['mensaje'] = '¡Verifique! El proveedor del documento referido y de la nota de credito son diferentes';
				$data['data'] = '';
				echo json_encode($data);
				exit();
			} else {
				$oCompracontadoc->setTbCompracontadocDocrefidfk(intval($_POST['txt_docrefidfk']));
				$oCompracontadoc->setTbCompracontadocMotivonc(strtoupper($_POST['txt_motivo_nc']));
			}
		}
		if (intval($_POST['che_anexa_credito']) == 1) {
			$oCompracontadoc->setTbCreditotipoIdfk(intval($_POST['cmb_creditotipo_id']));
			$oCompracontadoc->setTbCreditoIdfk(intval($_POST['txt_credito_id']));
		}
		$oCompracontadoc->setTbCompracontadocDetalle(strtoupper($_POST['txt_comentario']));
		$oCompracontadoc->setTbCompracontadocCorrelativo($correlativo);

		$prox_correlativo_parte1 = substr($correlativo, 0, -4);
		$prox_correlativo_parte2 = intval(substr($correlativo, -4))+1;//devuelve 7. 17. 117. 1117
		$cantidad_falta = 4 - strlen($prox_correlativo_parte2);

		$contador_while = 0; $relleno = '';
		while($contador_while < $cantidad_falta){
			$relleno .= "0";
			$contador_while++;
		}
		$prox_correlativo = $prox_correlativo_parte1 . $relleno . $prox_correlativo_parte2;

		$todas_cuentaconta_existe = 1;
		$mensaje = '';
		for ($i = 0; $i < $length_detalles; $i++) {
			$arr[0]["column_name"] = "cc.tb_cuentaconta_cod";
			$arr[0]["param0"] = $_POST["txt_codcuentacont_$i"];
			$arr[0]["datatype"] = "STR";

			$result = $oCuentaconta->listar_todos($arr, array(), array());
			if($result['data'][0]['tb_cuentaconta_id'] <= 0){
				$todas_cuentaconta_existe = 0;
				$mensaje .= empty($mensaje) ? ($i+1) : ", " . ($i+1);
			} else {
				$_POST["txt_detallecuentacontid_$i"] = $result['data'][0]['tb_cuentaconta_id'];
			}
			$result = null;
		}
		if ($todas_cuentaconta_existe == 0) {
			$data['estado'] = 0;
			$data['mensaje'] = "No existe la cuenta contable colocada en el detalle $mensaje";
			echo json_encode($data); exit();
		}

		if ($action == 'insertar') {
			if ($existe == 0) {
				$oCompracontadoc->setTbCompracontadocUsureg($usuario_id);
				$res = $oCompracontadoc->insertar();
				
				if ($res['estado'] == 1) {
					$data['estado'] = 1;
					$data['mensaje'] = $res['mensaje'] . '. Su ID: ' . $res['nuevo'];
					$data['nuevo'] = $res['nuevo'];
					
					// aqui viene la inserción de los detalles en la nueva tabla de detalle
					for($i = 0; $i < $length_detalles; $i++){
						//uno a uno los detalles
						$oCompraDetalle = new Compracontadetalle();
						$oCompraDetalle->setTbCuentacontaCod($_POST["txt_codcuentacont_$i"]);
						$oCompraDetalle->setTbCuentacontaIdfk($_POST["txt_detallecuentacontid_$i"]);
						$glosa = empty(strtoupper($_POST['txt_glosa_'.$i])) ? null : strtoupper($_POST["txt_glosa_$i"]);			
						$oCompraDetalle->setTbCompracontadetalleGlosa($glosa);
						$oCompraDetalle->setTbCompracontadetalleMonto(moneda_mysql($_POST["txt_monto_$i"]));
						$empresaidfk = intval($_POST['cbo_sede_'.$i]) == 0 ? null : intval($_POST["cbo_sede_$i"]);
						$oCompraDetalle->setTbEmpresaIdfk( $empresaidfk );
						//añadir gastogar_idfk/gasto_idfk
						$tabla = $_POST["hdd_tablagasto_$i"];
						if(!empty($tabla)){
							if($tabla == "tb_gasto"){
								$oCompraDetalle->setTbGastoIdfk($_POST["hdd_gastoid_$i"]);
								//añadir gastopagoid
								$gastopago_id = intval($_POST["hdd_gastopagoid_$i"]);
								if($gastopago_id > 0){
									$oCompraDetalle->tb_gastopago_idfk = $gastopago_id;
								}
							} else {
								$oCompraDetalle->setTbGastogarIdfk($_POST["hdd_gastoid_$i"]);
							}
						}
						$oCompraDetalle->setTbCompracontadocIdfk(intval($data['nuevo']));
						$oCompraDetalle->setTbCompracontadetalleOrden(intval($_POST['txt_detalleorden_'.$i]));
						$oCompraDetalle->setTbCompracontadetalleUsureg($usuario_id);
						$oCompraDetalle->setTbCompracontadetalleUsumod($usuario_id);
						$oCompraDetalle->insertar();

						if($tabla == 'tb_gastogar'){
							$oGastogar->modificar_campo($_POST["hdd_gastoid_$i"], 'tb_gastogar_anexado', 1, 'INT');
						} elseif ($tabla == 'tb_gasto' && $gastopago_id > 0){
							$oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_anexado', 1, 'INT');
						}
					}
					//insertar historial
					$oHist->setTbHistUsureg($usuario_id);
					$oHist->setTbHistNomTabla('tb_compracontadoc');
					$oHist->setTbHistRegmodid($data['nuevo']);
					$oHist->setTbHistDet('Registró el documento de Compra. ID: ' . $data['nuevo'] . '. PROVEEDOR: '. strtoupper($_POST['txt_proveedor_nom']) .'. NUMERACION: ' . $compracontadoc_serie . '-' . $compracontadoc_numero);
					$oHist->insertar();

					//ACTUALIZAR EL CORRELATIVO EN TABLA MES_CONTA
					$array_update_correlativo[0]['column_name'] = 'tb_mesconta_compracorrel';
					$array_update_correlativo[0]['param0'] = $prox_correlativo;
					$array_update_correlativo[0]['datatype'] = 'STR';

					$array_update_correlativo[1]['column_name'] = 'tb_mesconta_id';
					$array_update_correlativo[1]['param1'] = $mesconta_id;
					$array_update_correlativo[1]['datatype'] = 'INT';

					$oMesconta->actualizar_columna($array_update_correlativo);
				}
			} else {
				$data['mensaje'] = 'El documento compra ' . $compracontadoc_serie . '-' . $compracontadoc_numero . ' del proveedor ' . $registro_coincide['tb_proveedor_nom'] . ' ya está registrado, con id: ' . $registro_coincide['tb_compracontadoc_id'];
			}
		}
		else{
			// LA ACCION ES MODIFICAR
			$registro_origin = (array) json_decode($_POST['hdd_compracontadoc_registro']);

			$evaluar[1] = $proveedor_id == intval($registro_origin["tb_proveedor_idfk"]);
			$evaluar[2] = $doc_cod == $registro_origin["tb_documentocontable_codfk"];
			$evaluar[3] = $compracontadoc_serie == strtoupper($registro_origin["tb_compracontadoc_serie"]);
			$evaluar[4] = $compracontadoc_numero == $registro_origin["tb_compracontadoc_numero"];
			$evaluar[5] = fecha_mysql($_POST['compracontadoc_emi_picker']) == $registro_origin["tb_compracontadoc_fechaemision"];
			/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
			$evaluar[7] = intval($_POST['txt_dias_venc']) == intval($registro_origin["tb_compracontadoc_cantdiasvenc"]); */
			$evaluar[27] = $fecha_venc == $registro_origin["tb_compracontadoc_fechavencimiento"];
			$evaluar[6] = $moneda_id == intval($registro_origin["tb_moneda_idfk"]);
			$evaluar[7] = $_POST['hdd_monedacambiocontable_idfk'] == $registro_origin["tb_monedacambiocontable_idfk"];
			$evaluar[8] = moneda_mysql($_POST['txt_importe_total']) == moneda_mysql($registro_origin["tb_compracontadoc_montototal"]);
			$evaluar[9] = floatval($oCompracontadoc->getTbCompracontadocMontosoles()) == floatval($registro_origin["tb_compracontadoc_montosoles"]);
			$evaluar[10] = intval($oCompracontadoc->getTbCompracontadocAfecta()) == intval($registro_origin["tb_compracontadoc_afecta"]);
			$evaluar[11] = floatval($oCompracontadoc->getTbCompracontadocPorcentajeigv()) == floatval($registro_origin["tb_compracontadoc_porcentajeigv"]);
			$evaluar[12] = floatval($oCompracontadoc->getTbCompracontadocBaseinafecta()) == floatval($registro_origin["tb_compracontadoc_baseinafecta"]);
			$evaluar[13] = floatval($oCompracontadoc->getTbCompracontadocBaseafecta()) == floatval($registro_origin["tb_compracontadoc_baseafecta"]);
			$evaluar[14] = floatval($oCompracontadoc->getTbCompracontadocBasesoles()) == floatval($registro_origin["tb_compracontadoc_basesoles"]);
			$evaluar[15] = floatval($oCompracontadoc->getTbCompracontadocMontoigv()) == floatval($registro_origin["tb_compracontadoc_montoigv"]);
			$evaluar[16] = floatval($oCompracontadoc->getTbCompracontadocMontoigvsoles()) == floatval($registro_origin["tb_compracontadoc_montoigvsoles"]);
			$evaluar[17] = $oCompracontadoc->getTbCompracontadocDocrefidfk() == $registro_origin["tb_compracontadoc_docrefidfk"];
			$evaluar[18] = $oCompracontadoc->getTbCompracontadocMotivonc() == $registro_origin["tb_compracontadoc_motivonc"];
			$evaluar[19] = $oCompracontadoc->getTbCompracontadocConstdetr() == $registro_origin["tb_compracontadoc_constdetr"];
			$evaluar[20] = $oCompracontadoc->getTbCompracontadocFechadetr() == $registro_origin["tb_compracontadoc_fechadetr"];
			$evaluar[21] = $oCompracontadoc->getTbCreditotipoIdfk() == $registro_origin["tb_creditotipo_idfk"];
			$evaluar[22] = $oCompracontadoc->getTbCreditoIdfk() == $registro_origin["tb_credito_idfk"];
			$evaluar[23] = $oCompracontadoc->getTbCompracontadocDetalle() == strtoupper($registro_origin["tb_compracontadoc_detalle"]);
			$evaluar[24] = $oCompracontadoc->getTbMescontaIdfk() == $registro_origin["tb_mesconta_idfk"];
			$evaluar[25] = intval($oCompracontadoc->getTbCompracontadocDetr()) == intval($registro_origin["tb_compracontadoc_detr"]);
			$evaluar[26] = $correlativo == strtoupper($registro_origin["tb_compracontadoc_correlativo"]);

			$son_iguales = true;
			$i = 1;
			while($i<count($evaluar)+1){
				if(!$evaluar[$i]){
					$son_iguales = false;
				}
				$i++;
			}
			
			$detalle_eliminado = array();
			$detalle_original_mod = array();
			$fila_corresponde = array();
			$evaluar_detalle = array();
			$contador = 0;
			while($contador < count($detalles_origin)){
				$contador++;
				$detalle_eliminado[$contador] = 1; //está eliminado
				$detalle_original_mod[$contador] = 0;
				$fila_corresponde[$contador] = null;
			}

			for($i = 0 ; $i < count($detalles_origin); $i++){
				$contador = 0;
				for($j = 0 ; $j < $length_detalles ; $j ++){
					if(intval($detalles_origin[$i]['tb_compracontadetalle_id']) == intval($_POST['txt_detalleid_'.$j])){
						$detalle_eliminado[$i+1] = 0; //no se eliminó el detalle
						$fila_corresponde[$i+1] = $j; //fila donde esta el registro

						$evaluar_detalle[$i+1][0] = intval($detalles_origin[$i]['tb_cuentaconta_cod'] == $_POST["txt_codcuentacont_$j"]);
						$evaluar_detalle[$i+1][1] = intval($detalles_origin[$i]['tb_compracontadetalle_glosa'] == strtoupper($_POST["txt_glosa_$j"]));
						$evaluar_detalle[$i+1][2] = intval($detalles_origin[$i]['tb_compracontadetalle_monto'] == moneda_mysql($_POST["txt_monto_$j"]));
						$evaluar_detalle[$i+1][3] = intval(intval($detalles_origin[$i]['tb_empresa_idfk']) == intval($_POST["cbo_sede_$j"]));
						$evaluar_detalle[$i+1][4] = intval(intval($detalles_origin[$i]['tb_compracontadetalle_orden']) == intval($_POST["txt_detalleorden_$j"]));
						$compara_tabla = $detalles_origin[$i]['tabla_gasto'] == $_POST["hdd_tablagasto_$j"];
						$compara_gastoid = intval($detalles_origin[$i]['gasto_id']) == intval($_POST["hdd_gastoid_$j"]);
						$compara_gastopago_id[$i+1] = intval($detalles_origin[$i]['tb_gastopago_idfk']) == intval($_POST["hdd_gastopagoid_$j"]);
						$evaluar_detalle[$i+1][5] = intval($compara_tabla && $compara_gastoid && $compara_gastopago_id[$i+1]);
						//guardar 1 o 0 para saber si está modificado
						//guardar el numero j para saber cual es la fila donde se modificaron los datos
						while($contador< count($evaluar_detalle[$i+1])){
							if($evaluar_detalle[$i+1][$contador] < 1){
								$detalle_original_mod[$i+1] = 1;
							}
							$contador++;
						}
						break;
					}
				}
			}

			$detalles_son_iguales = true;
			if(count($detalles_origin) != $length_detalles){
				$detalles_son_iguales = false;
			}
			$i = 1;
			while($i<count($detalles_origin)+1){
				if($detalle_eliminado[$i] == 1 || $detalle_original_mod[$i] == 1){
					$detalles_son_iguales = false;
				}
				$i++;
			}
			
			if ($son_iguales && $detalles_son_iguales) {
				$data['estado'] = 2;
				$data['mensaje'] = "No se realizó ninguna modificacion";
			}
			else if ($existe == 0 || ($existe == 1 && $evaluar0)) {
				$ARRAY = array('NO', 'SI');
				$moneda = array(null, 'S/.', 'US$');
				$tipocredito = array(null, 'CRÉDITO MENOR', 'CRÉDITO ASCVEH', 'CRÉDITO GARVEH', 'CREDITO HIPOTECARIO');
				$tipocredito1 = array(null, 'CM', 'CAV', 'CGV', 'CH');
				$oCompracontadoc->setTbCompracontadocId($compracontadoc_id);
				if(!$son_iguales){
					$res = $oCompracontadoc->modificar();
				}
				//ESPACIO PARA MODIFICAR LOS DETALLES
				$mensaje_detalles = '';
				if(!$detalles_son_iguales){
					//añadir todos los nuevos detalles
					for($i = 0 ; $i < $length_detalles ; $i++){
						if (intval($_POST["txt_detalleid_$i"]) == 0) { //ese detalle es nuevo
							//uno a uno los detalles
							$oCompraDetalle = new Compracontadetalle();
							$oCompraDetalle->setTbCuentacontaCod($_POST["txt_codcuentacont_$i"]);
							$oCompraDetalle->setTbCuentacontaIdfk($_POST["txt_detallecuentacontid_$i"]);
							$glosa = empty(strtoupper($_POST["txt_glosa_$i"])) ? null : strtoupper($_POST["txt_glosa_$i"]);
							$oCompraDetalle->setTbCompracontadetalleGlosa($glosa);
							$oCompraDetalle->setTbCompracontadetalleMonto(moneda_mysql($_POST["txt_monto_$i"]));
							$empresaidfk = intval($_POST["cbo_sede_$i"]) == 0 ? null : intval($_POST["cbo_sede_$i"]);
							$oCompraDetalle->setTbEmpresaIdfk($empresaidfk);
							//añadir gastogar_idfk/gasto_idfk
							$tabla = $_POST["hdd_tablagasto_$i"];
							$oCompraDetalle->tb_gasto_idfk = null;
							$oCompraDetalle->tb_gastogar_idfk = null;
							if (!empty($tabla)) {
								if ($tabla == "tb_gasto") {
									$oCompraDetalle->setTbGastoIdfk($_POST["hdd_gastoid_$i"]);
									//añadir gastopagoid
									$gastopago_id = intval($_POST["hdd_gastopagoid_$i"]);
									if ($gastopago_id > 0) {
										$oCompraDetalle->tb_gastopago_idfk = $gastopago_id;
										$oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_anexado', 1, 'INT');
									}
								} else {
									$oCompraDetalle->setTbGastogarIdfk($_POST["hdd_gastoid_$i"]);
									$oGastogar->modificar_campo($_POST["hdd_gastoid_$i"], 'tb_gastogar_anexado', 1, 'INT');
								}
							}
							$oCompraDetalle->setTbCompracontadocIdfk($compracontadoc_id);
							$oCompraDetalle->setTbCompracontadetalleOrden(intval($_POST["txt_detalleorden_$i"]));
							$oCompraDetalle->setTbCompracontadetalleUsureg($usuario_id);
							$oCompraDetalle->setTbCompracontadetalleUsumod($usuario_id);
							$oCompraDetalle->insertar();

							$mensaje_detalles .= '<br> - Añadió el detalle de compra nro. '. intval($_POST["txt_detalleorden_$i"]) . ': '. $_POST["txt_codcuentacont_$i"] . '. ' . strtoupper($_POST["txt_glosa_$i"]).'. Monto: '. $moneda[$moneda_id]. $_POST["txt_monto_$i"] . '. '. $_POST["txt_sedenombre_$i"].'.';
							if(!empty($tabla)){
								$mensaje_detalles .= ' Gasto anexado id: '. $_POST["hdd_gastoid_$i"] . ', del '. $tipocredito1[$_POST["hdd_tipocreditoid_$i"]].'-'.$_POST["hdd_creditoid_$i"].'.';
								if ($gastopago_id > 0) {
									$mensaje_detalles .= " Gasto pago $gastopago_id.";
								}
							}
						}
					}

					//ahora a verificar las variables $detalle_eliminado, fila_corresponde, detalle_original_mod
					for($i = 1 ; $i < count($detalles_origin)+1; $i ++){
						if($detalle_eliminado[$i]){
							$oCompraDetalle = new Compracontadetalle();
							$oCompraDetalle->setTbCompracontadetalleUsumod($usuario_id);
							$oCompraDetalle->setTbCompracontadetalleId($detalles_origin[($i-1)]['tb_compracontadetalle_id']);
							$oCompraDetalle->eliminar();
							$mensaje_detalles .= '<br> - Eliminó el detalle de compra: '. $detalles_origin[($i-1)]['tb_cuentaconta_cod'].'. '. $detalles_origin[($i-1)]['tb_compracontadetalle_glosa'].'. Monto: '. $moneda[$moneda_id].$detalles_origin[($i-1)]['tb_compracontadetalle_monto'].'.';

							if(!empty($detalles_origin[($i-1)]['tb_gastogar_idfk'])){
								$oGastogar->modificar_campo($detalles_origin[($i-1)]['tb_gastogar_idfk'], 'tb_gastogar_anexado', 0, 'INT');
							} elseif (!empty($detalles_origin[($i-1)]['tb_gastopago_idfk'])){
								$oGastopago->modificar_campo($detalles_origin[($i-1)]['tb_gastopago_idfk'], 'tb_gastopago_anexado', 0, 'INT');
							}
						}

						if($detalle_original_mod[$i]){
							$oCompraDetalle = new Compracontadetalle();
							$oCompraDetalle->setTbCompracontadetalleUsumod($usuario_id);
							$oCompraDetalle->setTbCompracontadetalleId(intval($_POST['txt_detalleid_'.$fila_corresponde[$i]]));
							$oCompraDetalle->setTbCuentacontaCod($_POST['txt_codcuentacont_'.$fila_corresponde[$i]]);
							$oCompraDetalle->setTbCuentacontaIdfk($_POST['txt_detallecuentacontid_'.$fila_corresponde[$i]]);
							$glosa = empty(strtoupper($_POST['txt_glosa_'.$fila_corresponde[$i]])) ? null : strtoupper($_POST['txt_glosa_'.$fila_corresponde[$i]]);
							$oCompraDetalle->setTbCompracontadetalleGlosa($glosa);
							$oCompraDetalle->setTbCompracontadetalleMonto(moneda_mysql($_POST['txt_monto_'.$fila_corresponde[$i]]));
							$empresaidfk = intval($_POST['cbo_sede_'.$fila_corresponde[$i]]) == 0 ? null : intval($_POST['cbo_sede_'.$fila_corresponde[$i]]);
							$oCompraDetalle->setTbEmpresaIdfk($empresaidfk);
							//añadir gastogar_idfk/gasto_idfk
							$oCompraDetalle->tb_gasto_idfk = null;
							$oCompraDetalle->tb_gastogar_idfk = null;
							$oCompraDetalle->tb_gastopago_idfk = null;
							$tabla = $_POST["hdd_tablagasto_".$fila_corresponde[$i]];
							$tabla_original = $detalles_origin[$i-1]['tabla_gasto'];
							$gastopago_id = intval($_POST["hdd_gastopagoid_".$fila_corresponde[$i]]);
							if (!empty($tabla)) {
								if ($tabla == "tb_gasto") {
									$oCompraDetalle->setTbGastoIdfk($_POST["hdd_gastoid_".$fila_corresponde[$i]]);
									
									if ($gastopago_id > 0) {
										$oCompraDetalle->tb_gastopago_idfk = $gastopago_id;
										$oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_anexado', 1, 'INT');
									}
								} else {
									$oCompraDetalle->setTbGastogarIdfk($_POST["hdd_gastoid_".$fila_corresponde[$i]]);
									$oGastogar->modificar_campo($_POST["hdd_gastoid_".$fila_corresponde[$i]], 'tb_gastogar_anexado', 1, 'INT');
								}
							}
							if(!empty($tabla_original)){
								$gastopago_id_original = $detalles_origin[$i-1]['tb_gastopago_idfk'];
								if($tabla_original == 'tb_gasto' && !empty($gastopago_id_original)){
									$oGastopago->modificar_campo($gastopago_id_original, 'tb_gastopago_anexado', 0, 'INT');
								} elseif ($tabla_original == 'tb_gastogar') {
									$oGastogar->modificar_campo($detalles_origin[($i-1)]['tb_gastogar_idfk'], 'tb_gastogar_anexado', 0, 'INT');
								}
							}
							$oCompraDetalle->setTbCompracontadetalleOrden(intval($_POST['txt_detalleorden_'.$fila_corresponde[$i]]));
							$oCompraDetalle->modificar();
							
							if($evaluar_detalle[$i][0] != 1 || $evaluar_detalle[$i][1] != 1 || $evaluar_detalle[$i][2] != 1 || $evaluar_detalle[$i][3] != 1 || $evaluar_detalle[$i][5] != 1) //[4] es el orden, no se toma en cuenta en el historial de las modificaciones
								$mensaje_detalles .= '<br> - Modificó el detalle '.intval($_POST['txt_detalleorden_'.$fila_corresponde[$i]]).':';
							if($evaluar_detalle[$i][0] != 1)
								$mensaje_detalles .= ' Cod C.C.: ' . $detalles_origin[$i-1]['tb_cuentaconta_cod'] . ' => <b>'. $_POST['txt_codcuentacont_'.$fila_corresponde[$i]].'</b>.';
							if($evaluar_detalle[$i][1] != 1)
								$mensaje_detalles .= ' Glosa: ' . $detalles_origin[$i-1]['tb_compracontadetalle_glosa'] . ' => <b>'. strtoupper($_POST['txt_glosa_'.$fila_corresponde[$i]]).'</b>.';
							if($evaluar_detalle[$i][2] != 1)
								$mensaje_detalles .= ' Monto: ' . mostrar_moneda($detalles_origin[$i-1]['tb_compracontadetalle_monto']) . ' => <b>'. $_POST['txt_monto_'.$fila_corresponde[$i]] .'</b>.';
							if($evaluar_detalle[$i][3] != 1)
								$mensaje_detalles .= ' Empresa: ' . $detalles_origin[$i-1]['tb_empresa_nomcom'] . ' => <b>'. strtoupper($_POST['txt_sedenombre_'.$fila_corresponde[$i]]) .'</b>.';
							if($evaluar_detalle[$i][5] != 1){
								$gasto_origin = !empty($detalles_origin[$i-1]['gasto_id']) ? $tipocredito1[$detalles_origin[$i-1]['tipocredito_id']].'-'. $detalles_origin[$i-1]['credito_id'] . ', Gasto id: '. $detalles_origin[$i-1]['gasto_id'] : "(vacío)";
								if(!$compara_gastopago_id[$i] && $gasto_origin != '(vacío)'){
									$gasto_origin .= !empty($detalles_origin[$i-1]['tb_gastopago_idfk']) ? ', Gasto pago id: '.$detalles_origin[$i-1]['tb_gastopago_idfk'] : '';
								}
								$gasto_modif = !empty($_POST['hdd_tablagasto_'.$fila_corresponde[$i]]) ? $tipocredito1[$_POST['hdd_tipocreditoid_'.$fila_corresponde[$i]]].'-'.$_POST['hdd_creditoid_'.$fila_corresponde[$i]].', Gasto id: '. $_POST['hdd_gastoid_'.$fila_corresponde[$i]] : "(vacío)";
								if(!$compara_gastopago_id[$i] && $gasto_modif != '(vacío)'){
									$gasto_modif .= !empty($_POST["hdd_gastopagoid_".$fila_corresponde[$i]]) ? ', Gasto pago id: '. $_POST["hdd_gastopagoid_".$fila_corresponde[$i]] : '';
								}
								$mensaje_detalles .= ' Gasto: ' . $gasto_origin .' => <b>'. $gasto_modif .'</b>.';
							}

							$oCompraDetalle = null;
						}
					}
				}

				if ($res == 1 || !$detalles_son_iguales) {
					$data['estado'] = 1;
					$data['mensaje'] = "Se modificó correctamente";

					//insertar historial
					$oHist->setTbHistUsureg($usuario_id);
					$oHist->setTbHistNomTabla('tb_compracontadoc');
					$oHist->setTbHistRegmodid($compracontadoc_id);
					$mensaje = 'Modificó el documento de compra con id ' . $compracontadoc_id . ':';
					if (!$evaluar[1])
						$mensaje .= '<br> - Cambió el proveedor: ' . $registro_origin['tb_proveedor_nom'] . ' - ID:'.$registro_origin['tb_proveedor_idfk'] . ' => <b>' . strtoupper($_POST['txt_proveedor_nom']) . ' - ID:'.$proveedor_id . '</b>';
					if (!$evaluar[2])
						$mensaje .= '<br> - Cambió el tipo de documento: ' . $registro_origin['tb_documentocontable_codfk'].' - '. $registro_origin['tb_documentocontable_desc'] . ' => <b>' . $oCompracontadoc->getTbCompracontadocCodfk() .' - '. strtoupper($_POST['txt_doc_nom']) . '</b>';
					if (!$evaluar[3] || !$evaluar[4])
						$mensaje .= '<br> - Cambió la numeración: ' . $registro_origin['tb_compracontadoc_serie'].'-'. $registro_origin['tb_compracontadoc_numero'] . ' => <b>' . $compracontadoc_serie.'-'.$compracontadoc_numero . '</b>';
					if (!$evaluar[5])
						$mensaje .= '<br> - Cambió la fecha emisión: ' . mostrar_fecha($registro_origin['tb_compracontadoc_fechaemision']) . ' => <b>' . $_POST['compracontadoc_emi_picker'] . '</b>';
					if (!$evaluar[27])
						$mensaje .= '<br> - Cambió la fecha vencimiento: ' . mostrar_fecha($registro_origin['tb_compracontadoc_fechavencimiento']) . ' => <b>' . $_POST['compracontadoc_venc_picker'] . '</b>';
					if (!$evaluar[24])
						$mensaje .= '<br> - Cambió el periodo contable: ' . $registro_origin['tb_mesconta_mes'].'-'.$registro_origin['tb_mesconta_anio'] . ' => <b>' . $result['data']['tb_mesconta_mes'].'-'.$result['data']['tb_mesconta_anio'] . '</b>';
					if (!$evaluar[26])
						$mensaje .= '<br> - Cambió el correlativo: ' . $registro_origin['tb_compracontadoc_correlativo'] . ' => <b>' . $correlativo . '</b>';
					/* // CUANDO SE CONTEMPLE CONDICION CREDITO Y LETRA SE MOSTRARÁ LA FECHA VENCIMIENTO
					if (!$evaluar[6])
						$mensaje .= '<br> - Cambió la fecha vencim.: ' . mostrar_fecha($registro_origin['tb_compracontadoc_fechavencimiento']) . ' => <b>' . $_POST['compracontadoc_venc_picker'] . '</b>';
					if (!$evaluar[7])
						$mensaje .= '<br> - Cambió la cantidad de días para vencimiento: ' . $registro_origin['tb_compracontadoc_cantdiasvenc'] . ' => <b>' . $_POST['txt_dias_venc'] . '</b>'; */
					if (!$evaluar[6])
						$mensaje .= '<br> - Cambió la moneda: ' . $registro_origin['tb_moneda_nom'] . ' => <b>' . $moneda[$moneda_id] . '</b>';
					if (!$evaluar[7])
						$mensaje .= '<br> - Cambió el tipo cambio: ' . $registro_origin['tipocambio_comprasunat'] . ' => <b>' . $_POST['txt_tipcam_cont_tipcam'] . '</b>';
					if (!$evaluar[8])
						$mensaje .= '<br> - Cambió el monto de importe total: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_compracontadoc_montototal']) . ' => <b>' . $moneda[$moneda_id]. $_POST['txt_importe_total'] . '</b>';
					if (!$evaluar[9])
						$mensaje .= '<br> - Cambió el monto de importe total en soles: ' . $moneda[1]. mostrar_moneda($registro_origin['tb_compracontadoc_montosoles']) . ' => <b>' . $moneda[1].mostrar_moneda($oCompracontadoc->getTbCompracontadocMontosoles()) . '</b>';
					if (!$evaluar[10])
						$mensaje .= '<br> - Cambió el estado de Afecta: ' . $ARRAY[intval($registro_origin['tb_compracontadoc_afecta'])] . ' => <b>' . $ARRAY[$afecta] . '</b>';
					if (!$evaluar[11])
						$mensaje .= '<br> - Cambió el porcentaje de IGV: ' . $registro_origin['tb_compracontadoc_porcentajeigv'] . ' => <b>' . $oCompracontadoc->getTbCompracontadocPorcentajeigv() . '</b>';
					if (!$evaluar[12])
						$mensaje .= '<br> - Cambió el monto base inafecta: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_compracontadoc_baseinafecta']) . ' => <b>' .  $moneda[$moneda_id].mostrar_moneda($oCompracontadoc->getTbCompracontadocBaseinafecta()) . '</b>';
					if (!$evaluar[13])
						$mensaje .= '<br> - Cambió el monto base afecta: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_compracontadoc_baseafecta']) . ' => <b>' . $moneda[$moneda_id].mostrar_moneda($oCompracontadoc->getTbCompracontadocBaseafecta()) . '</b>';
					if (!$evaluar[14])
						$mensaje .= '<br> - Cambió el monto base en soles: ' . $moneda[1].mostrar_moneda($registro_origin['tb_compracontadoc_basesoles']) . ' => <b>' . $moneda[1].mostrar_moneda($oCompracontadoc->getTbCompracontadocBasesoles()) . '</b>';
					if (!$evaluar[15])
						$mensaje .= '<br> - Cambió el monto de igv: ' . $registro_origin['tb_moneda_nom']. mostrar_moneda($registro_origin['tb_compracontadoc_montoigv']) . ' => <b>' . $moneda[$moneda_id].mostrar_moneda($oCompracontadoc->getTbCompracontadocMontoigv()) . '</b>';
					if (!$evaluar[16])
						$mensaje .= '<br> - Cambió el monto de igv en soles: ' . $moneda[1].mostrar_moneda($registro_origin['tb_compracontadoc_montoigvsoles']) . ' => <b>' . $moneda[1].mostrar_moneda($oCompracontadoc->getTbCompracontadocMontoigvsoles()) . '</b>';
					if (!$evaluar[17])
						$mensaje .= '<br> - Cambió el documento referido en la NC: ' . $registro_origin['docreferido_serie'].'-'.$registro_origin['docreferido_numero'] . ' => <b>' . $_POST['txt_documento_ref'] . '</b>';
					if (!$evaluar[18])
						$mensaje .= '<br> - Cambió el motivo de la NC: \'' . $registro_origin['tb_compracontadoc_motivonc'] . '\' => \'<b>' . $oCompracontadoc->getTbCompracontadocMotivonc() . '</b>\'';
					if (!$evaluar[25])
						$mensaje .= '<br> - Cambió el estado de detraccion: \'' . $ARRAY[intval($registro_origin["tb_compracontadoc_detr"])] . '\' => \'<b>' . $ARRAY[intval($oCompracontadoc->getTbCompracontadocDetr())] . '</b>\'';
					if (!$evaluar[19])
						$mensaje .= '<br> - Cambió la constancia de detracción: ' . $registro_origin['tb_compracontadoc_constdetr'] . ' => <b>' . $oCompracontadoc->getTbCompracontadocConstdetr() . '</b>';
					if (!$evaluar[20])
						$mensaje .= '<br> - Cambió la fecha de detracción: ' . mostrar_fecha($registro_origin['tb_compracontadoc_fechadetr']) . ' => <b>' . $_POST['compracontadoc_detraccion_picker'] . '</b>';
					if (!$evaluar[21])
						$mensaje .= '<br> - Cambió el tipo de credito anexado: ' . $registro_origin['tb_creditotipo_nom']  . ' => <b>' . $tipocredito[intval($_POST['cmb_creditotipo_id'])] . '</b>';
					if (!$evaluar[22])
						$mensaje .= '<br> - Cambió el credito anexado: ' . $registro_origin['tb_credito_idfk'] . ' => <b>' . $oCompracontadoc->getTbCreditoIdfk() . '</b>';
					if (!$evaluar[23])
						$mensaje .= '<br> - Cambió el comentario: \'' . $registro_origin['tb_compracontadoc_detalle'] . '\' => \'<b>' . $oCompracontadoc->getTbCompracontadocDetalle() . '</b>\'';
					if(!$detalles_son_iguales)
						$mensaje .= $mensaje_detalles;

					$oHist->setTbHistDet($mensaje);
					$oHist->insertar();

					//ACTUALIZAR EL CORRELATIVO EN TABLA MES_CONTA
					if (!$evaluar[26]) {
						$array_update_correlativo[0]['column_name'] = 'tb_mesconta_compracorrel';
						$array_update_correlativo[0]['param0'] = $prox_correlativo;
						$array_update_correlativo[0]['datatype'] = 'STR';

						$array_update_correlativo[1]['column_name'] = 'tb_mesconta_id';
						$array_update_correlativo[1]['param1'] = $mesconta_id;
						$array_update_correlativo[1]['datatype'] = 'INT';

						$oMesconta->actualizar_columna($array_update_correlativo);
					}
				}
			} else {
				$data['mensaje'] = 'Los datos que ingresó ya están registrados, en el documento con id: ' . intval($registro_coincide['tb_compracontadoc_id']);
			}
		}
	}
	else {
		$data['estado'] = 0;
		$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action. "$compracontadoc_id " .intval($registro_coincide['tb_compracontadoc_id']);
	}
}
echo json_encode($data);