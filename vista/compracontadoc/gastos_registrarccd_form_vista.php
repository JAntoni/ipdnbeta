<?php
require_once('../../core/usuario_sesion.php');
require_once('../monedacambiocontable/Monedacambiocontable.class.php');
$oMonedaCambioConta = new Monedacambiocontable();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../gastogar/Gastogar.class.php');
$oGastogar = new Gastogar();
require_once('../gastopago/Gastopago.class.php');
$oGastopago = new Gastopago();
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();
require_once('../monedacambio/Monedacambio.class.php');
$oMonedaCambio = new Monedacambio();
$tipocredito = array(null, 'CM', 'CAV', 'CGV', 'CH');
$data["lista"] = "";

//1. gasto gar cre men que falte registrar documento
$where[0]['column_name'] = 'tb_gastogar_xac';
$where[0]['param0'] = 1;
$where[0]['datatype'] = 'INT';

$where[1]['column_name'] = 'tb_gastogar_emitedocsunat';
$where[1]['param1'] = 1;
$where[1]['datatype'] = 'INT';

$where[2]['column_name'] = 'tb_gastogar_anexado';
$where[2]['param2'] = 0;
$where[2]['datatype'] = 'INT';

$where[3]['param3'] = "(gg.tb_gastogar_documentossunat IS NULL OR TRIM(gg.tb_gastogar_documentossunat) = '')";

$gastos_faltan_factura = $oGastogar->listar_todos($where, array());
if ($gastos_faltan_factura['cantidad'] > 0) {
    $data["lista"] .=
    '<li class="item" style="padding-right: 5px;">
        <div class="col-md-12" style="padding-right: 5px;">
            <a href="javascript:void(0)" class="product-title">GASTOS GARANTIAS CRE-MENOR PENDIENTES DE REGISTRAR FACTURA</a>
            <span style="font-size: 11px;">';
    foreach ($gastos_faltan_factura['data'] as $key => $gasto_sin_factura){
        $data['lista'] .= "<br>● GASTO (id: {$gasto_sin_factura['tb_gastogar_id']}), EN {$tipocredito[$gasto_sin_factura['tb_creditotipo_idfk']]}-{$gasto_sin_factura['tb_credito_idfk']}, ";
        $data['lista'] .= "GARANTIA ID: {$gasto_sin_factura['tb_garantia_idfk']} - {$gasto_sin_factura['tb_garantia_pro']}, MONTO: {$gasto_sin_factura['tb_moneda_nom']}{$gasto_sin_factura['tb_gastogar_monto']}";
    }
    $data["lista"] .=
            '</span>
        </div>
    </li>';
}
unset($gastos_faltan_factura, $where);

//2. gastos garv que falte registrar documento
$where[0]['column_name'] = 'gp.tb_gastopago_emitedocsunat';
$where[0]['param0'] = 1;
$where[0]['datatype'] = 'INT';

$where[1]['conector'] = 'AND';
$where[1]['column_name'] = 'gp.tb_gastopago_anexado';
$where[1]['param1'] = 0;
$where[1]['datatype'] = 'INT';

$where[2]['conector'] = 'AND';
$where[2]['param2'] = "(gp.tb_gastopago_documentossunat IS NULL OR TRIM(gp.tb_gastopago_documentossunat) = '')";

$inner[0]['alias_columnasparaver'] = 'g.tb_gasto_cretip, g.tb_credito_id';
$inner[0]['tipo_union'] = 'INNER';
$inner[0]['tabla_alias'] = 'tb_gasto g';
$inner[0]['columna_enlace'] = 'gp.tb_gasto_idfk';
$inner[0]['alias_columnaPK'] = 'g.tb_gasto_id';

$inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
$inner[1]['tipo_union'] = 'LEFT';
$inner[1]['tabla_alias'] = 'tb_moneda mon';
$inner[1]['columna_enlace'] = 'gp.tb_moneda_idfk';
$inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';

$inner[2]['alias_columnasparaver'] = 'cgv.tb_cliente_id';
$inner[2]['tipo_union'] = 'LEFT';
$inner[2]['tabla_alias'] = 'tb_creditogarveh cgv';
$inner[2]['columna_enlace'] = 'g.tb_credito_id';
$inner[2]['alias_columnaPK'] = 'cgv.tb_credito_id';

$inner[3]['alias_columnasparaver'] = 'cli.tb_cliente_doc, cli.tb_cliente_nom';
$inner[3]['tipo_union'] = 'LEFT';
$inner[3]['tabla_alias'] = 'tb_cliente cli';
$inner[3]['columna_enlace'] = 'cgv.tb_cliente_id';
$inner[3]['alias_columnaPK'] = 'cli.tb_cliente_id';

$gastogarv_falta_fact = $oGastopago->listar_todos($where, $inner, array());
if ($gastogarv_falta_fact['row_count'] > 0) {
    $data["lista"] .=
    '<li class="item" style="padding-right: 5px;">
        <div class="col-md-12" style="padding-right: 5px;">
            <a href="javascript:void(0)" class="product-title">GASTOS GARVEH PENDIENTES DE REGISTRAR FACTURA</a>
            <span style="font-size: 11px;">';
    foreach ($gastogarv_falta_fact['data'] as $key => $gastogarv) {
        $data['lista'] .= "<br>● GASTO (id: {$gastogarv['tb_gasto_idfk']}), EN {$tipocredito[$gastogarv['tb_gasto_cretip']]}-{$gastogarv['tb_credito_id']}, ";
        $data['lista'] .= "CLIENTE: {$gastogarv['tb_cliente_doc']} - {$gastogarv['tb_cliente_nom']}, MONTO: {$gastogarv['tb_moneda_nom']}{$gastogarv['tb_gastopago_monto']}";
    }
    $data["lista"] .=
            '</span>
        </div>
    </li>';
}
unset($gastogarv_falta_fact);

//3. facturas de gastos gar cre menor
$facturas_faltan_registrar = $oGastogar->listar_documentossunatdegastos();
if ($facturas_faltan_registrar['estado'] == 1) {
    foreach($facturas_faltan_registrar['data'] as $key => $factura){
        $registro['tb_compracontadoc_id'] = 0;
        $registro['tb_proveedor_idfk'] = $factura["tb_proveedor_idfk"];
        $registro['tb_proveedor_doc'] = $factura['tb_proveedor_doc'];
        $registro['tb_proveedor_nom'] = $factura['tb_proveedor_nom'];
        $registro['tb_compracontadoc_serie'] = explode("-", $factura["tb_gastogar_documentossunat"])[0];
        $registro['tb_compracontadoc_numero'] = explode("-", $factura["tb_gastogar_documentossunat"])[1];
        $registro['tb_compracontadoc_condicion'] = 'CONTADO';
        $registro['tb_compracontadoc_fechaemision'] = $factura['tb_gastogar_fecha'];
        $registro['tb_moneda_idfk'] = $factura['tb_moneda_idfk'];
        $tc_reg_val = 1;
        if ($factura['tb_moneda_idfk'] == 2) {
            $tc_reg = $oMonedaCambioConta->mostrarUno('tb_monedacambiocontable_fecha', $factura['tb_gastogar_fecha'], 'STR');
            if ($tc_reg['estado'] == 1) {
                $tc_reg_id = $tc_reg['data']['tb_monedacambiocontable_id'];
                $tc_reg_val = $tc_reg['data']['tb_monedacambiocontable_comprasunat'];
                
                $registro['tb_monedacambiocontable_idfk'] = $tc_reg_id;
            }
            unset($tc_reg);
        }
        $registro['tipocambio_comprasunat'] = $tc_reg_val;

        $data["lista"] .=
        '<li class="item" style="padding-right: 5px;">
            <div class="col-md-9" style="width: 78%; padding-right: 5px;">
                <a href="javascript:void(0)" class="product-title">'. $factura['tb_gastogar_documentossunat'] .' | '. $factura['tb_proveedor_doc'] .' - '. $factura['tb_proveedor_nom'] .'</a>
                <span style="font-size: 11px;">';
        $res = $oGastogar->listar_gastos_por_documentossunat($factura['tb_gastogar_documentossunat'], $factura['tb_proveedor_idfk']);
        $contador = 0;
        foreach ($res['data'] as $key => $gasto) {
            $data['lista'] .= "<br>● GASTO EN {$tipocredito[$gasto['tb_creditotipo_idfk']]}-{$gasto['tb_credito_idfk']}, ";
            $data['lista'] .= "GARANTIA ID: {$gasto['tb_garantia_idfk']} - {$gasto['tb_garantia_pro']}, MONTO: {$gasto['tb_moneda_nom']}{$gasto['tb_gastogar_monto']}";

            $detalles[$contador]['tb_compracontadetalle_id'] = 0;
            $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
            $detalles[$contador]['tb_compracontadetalle_monto'] = formato_numero($gasto['tb_gastogar_monto']/1.18);
            $detalles[$contador]['tb_empresa_idfk'] = $gasto['tb_empresa_idfk'];
            $detalles[$contador]['tb_gastogar_idfk'] = $gasto['tb_gastogar_id'];
            $contador++;
        }
        unset($res);
        $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
        $contador++;
        $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
        
        $registro['detalles'] = $detalles;
        unset($detalles);
        $data["lista"] .=
            '   </span>
            </div>
            <div class="col-md-3" style="width: 22%">'.
                "<p></p><button class=\"btn bg-aqua btn-xs btn-block\" onclick='compracontadoc_form(\"I\",". json_encode($registro) .")'><i class=\"fa fa-check\"></i> &nbsp; Ok</button>".
            '</div>
        </li>';
        unset($registro);
    }
}
unset($facturas_faltan_registrar);

//4. facturas de gastos garveh
$facturas_gastos_garv = $oGastopago->listar_documentossunatdegastos();
if ($facturas_gastos_garv['cantidad'] > 0) {
    foreach($facturas_gastos_garv['data'] as $key => $factura){
        $registro['tb_compracontadoc_id'] = 0;
        $registro['tb_proveedor_idfk'] = $factura["tb_proveedor_id"];
        $registro['tb_proveedor_doc'] = $factura['tb_proveedor_doc'];
        $registro['tb_proveedor_nom'] = $factura['tb_proveedor_nom'];
        $registro['tb_compracontadoc_serie'] = explode("-", $factura["tb_gastopago_documentossunat"])[0];
        $registro['tb_compracontadoc_numero'] = explode("-", $factura["tb_gastopago_documentossunat"])[1];
        $registro['tb_compracontadoc_condicion'] = 'CONTADO';
        $registro['tb_compracontadoc_fechaemision'] = $factura['tb_gastopago_fecha'];
        $tc_reg_val = 1;
        $registro['tb_moneda_idfk'] = $factura['tb_moneda_id'];
        if ($factura['tb_moneda_id'] == 2) {
            $fecha_consulta = $factura['tb_gastopago_fecha'];
            $tc_reg = $oMonedaCambioConta->mostrarUno('tb_monedacambiocontable_fecha', $fecha_consulta, 'STR');
            if ($tc_reg['estado'] == 1) {
                $tc_reg_id = $tc_reg['data']['tb_monedacambiocontable_id'];
                $tc_reg_val = $tc_reg['data']['tb_monedacambiocontable_comprasunat'];

                $registro['tb_monedacambiocontable_idfk'] = $tc_reg_id;
            } else {
                $error = true; //falta registrar tipo de cambio contable en la fecha $fecha_consulta
            }
            unset($tc_reg);
        }
        $registro['tipocambio_comprasunat'] = $tc_reg_val;

        $data["lista"] .=
        '<li class="item" style="padding-right: 5px;">
            <div class="col-md-9" style="width: 78%; padding-right: 5px;">
                <a href="javascript:void(0)" class="product-title">'. $factura['tb_gastopago_documentossunat'] .' | '. $factura['tb_proveedor_doc'] .' - '. $factura['tb_proveedor_nom'] .'</a>
                <span style="font-size: 11px;">';
        
        $where[2]['conector'] = 'AND';
        $where[2]['column_name'] = 'gp.tb_gastopago_documentossunat';
        $where[2]['param2'] = $factura['tb_gastopago_documentossunat'];
        $where[2]['datatype'] = 'STR';

        $where[3]['conector'] = 'AND';
        $where[3]['column_name'] = 'g.tb_proveedor_id';
        $where[3]['param3'] = $factura['tb_proveedor_id'];
        $where[3]['datatype'] = 'INT';

        $inner[4]['alias_columnasparaver'] = 'emp.tb_empresa_nomcom';
        $inner[4]['tipo_union'] = 'LEFT';
        $inner[4]['tabla_alias'] = 'tb_empresa emp';
        $inner[4]['columna_enlace'] = 'gp.tb_empresa_idfk';
        $inner[4]['alias_columnaPK'] = 'emp.tb_empresa_id';

        $otros["orden"]["column_name"] = 'gp.tb_gastopago_fecreg';
        $otros["orden"]["value"] = 'ASC';

        $res = $oGastopago->listar_todos($where, $inner, $otros);
        //$data['sql'] = $res['sql']; break; echo json_encode($data);exit();
        $contador = 0;
        foreach ($res['data'] as $key => $gasto) {
            $data['lista'] .= "<br>● GASTO EN {$tipocredito[$gasto['tb_gasto_cretip']]}-{$gasto['tb_credito_id']}, ";
            $data['lista'] .= "Cliente: {$gasto['tb_cliente_doc']} - {$gasto['tb_cliente_nom']}, MONTO: {$gasto['tb_moneda_nom']}{$gasto['tb_gastopago_monto']}";

            $detalles[$contador]['tb_compracontadetalle_id'] = 0;
            $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
            if ($gasto['tb_moneda_idfk'] != $registro['tb_moneda_idfk']) {
                $fecha_consulta1 = $gasto['tb_gastopago_fecha'];
                if ($gasto['tb_gastopago_formapago'] == 2) { //se toma tipo cambio de fecha deposito, de lo contrario de la fecha de gasto
                    $res1 = $oDeposito->listar_depositos_paragasto($gasto['tb_gastopago_numope']);
                    $fecha_consulta1 = $res1['data']['tb_deposito_fec'];
                }
                $tc = $oMonedaCambioConta->mostrarUno('tb_monedacambiocontable_fecha', $fecha_consulta1, 'STR');
                if ($tc['estado'] == 1) {
                    $tc_gasto = $tc['data']['tb_monedacambiocontable_comprasunat'];
                } else {
                    $tc = $oMonedaCambio->consultar($fecha_consulta1);
                    $tc_gasto = $tc['data']['tb_monedacambio_com'];
                }
                if ($registro['tb_moneda_idfk'] == 2) {
                    //entonces gasto es en soles y debemos convertirlo a dolares
                    $gasto['tb_gastopago_monto'] = formato_numero($gasto['tb_gastopago_monto']/$tc_gasto);
                } else {
                    $gasto['tb_gastopago_monto'] = formato_numero($gasto['tb_gastopago_monto'] * $tc_gasto);
                }
            }
            $detalles[$contador]['tb_compracontadetalle_monto'] = formato_numero($gasto['tb_gastopago_monto']/1.18);
            $detalles[$contador]['tb_empresa_idfk'] = $gasto['tb_empresa_idfk'];
            $detalles[$contador]['tb_gasto_idfk'] = $gasto['tb_gasto_idfk'];
            $detalles[$contador]['tb_gastopago_idfk'] = $gasto['tb_gastopago_id'];
            $contador++;
        }
        unset($res);
        $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
        $contador++;
        $detalles[$contador]['tb_compracontadetalle_orden'] = $contador+1;
        
        $registro['detalles'] = $detalles;
        unset($detalles);
        $data["lista"] .=
            '   </span>
            </div>
            <div class="col-md-3" style="width: 22%">';
        if ($error) {
            $data["lista"] .= "<span>Falta registrar tipo de cambio contable en la fecha ". mostrar_fecha($fecha_consulta). "</span>";
        } else {
            $data["lista"] .= "<p></p><button class=\"btn bg-aqua btn-xs btn-block\" onclick='compracontadoc_form(\"I\",". json_encode($registro) .")'><i class=\"fa fa-check\"></i> &nbsp; Ok</button>";
        }
        $data["lista"] .=    
            '</div>
        </li>';
        unset($registro);
    }
}
unset($facturas_gastos_garv);

if ($data['lista'] == "") {
    $data['lista'] = 'No hay facturas pendientes de registro';
}

echo json_encode($data);
?>