var datatable_global_ccdoc;

$(document).ready(function () {
    $('#click').click();
    //POR DEFECTO CARGAR LOS DOCUMENTOS DEL ULTIMO PERIODO CONTABLE ACTIVO
    value_a_seleccionar = $('#cmb_periodo_id')[0][1].value;
    $("#cmb_periodo_id").val(`${value_a_seleccionar}`);

    //code
    compracontadoc_tabla();
    estilos_datatable();
    $("#txt_fil_proveedor_nom").autocomplete({
        minLength: 1,
        source: VISTA_URL + "proveedor/proveedor_autocomplete.php",
        select: function (event, ui) {
            //$('#txt_fil_proveedor_nom').val(ui.item.tb_proveedor_nom);
            $("#hdd_fil_proveedor_id").val(ui.item.tb_proveedor_id);
            event.preventDefault();
            compracontadoc_tabla();
        }
    })
    .keyup(function () {
        if ($('#txt_fil_proveedor_nom').val() === '') {
            $('#hdd_fil_proveedor_id').val('');
            compracontadoc_tabla();
        }
    });

    $("#txt_fil_doc_cod").autocomplete({
        minLength: 1,
        source: VISTA_URL + "documentocontable/documentocontable_autocomplete.php",
        select: function (event, ui) {
            //$('#txt_fil_proveedor_nom').val(ui.item.tb_proveedor_nom);
            $("#hdd_fil_doc_cod").val(ui.item.tb_documentocontable_cod);
            event.preventDefault();
            compracontadoc_tabla();
        }
    })
    .keyup(function () {
        if ($('#txt_fil_doc_cod').val() === '') {
            $('#hdd_fil_doc_cod').val('');
            compracontadoc_tabla();
        }
    });

    $('#cmb_periodo_id, #cmb_verificar_docs').change(function () {
        compracontadoc_tabla();
    });

    mostrar_gastos_para_completar();
});

function compracontadoc_form(usuario_act, reg) {
    var compracontadoc_id = reg.tb_compracontadoc_id;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracontadoc/compracontadoc_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            registro: reg
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_compracontadoc_form').html(data);
                $('#modal_registro_compracontadoc').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_compracontadoc'); //funcion encontrada en public/js/generales.js
            }
            else {
                //llamar al formulario de solicitar permiso
                var modulo = 'compracontadoc';
                var div = 'div_modal_compracontadoc_form';
                permiso_solicitud(usuario_act, compracontadoc_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
            modal_hidden_bs_modal('modal_registro_compracontadoc', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_registro_compracontadoc', 75);
            modal_height_auto('modal_registro_compracontadoc');
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function compracontadoc_tabla() {
    var fil_proveedor_id = $("#hdd_fil_proveedor_id").val();
    var doc_cod = $("#hdd_fil_doc_cod").val();
    var mesconta_id = $('#cmb_periodo_id').val();
    var verificar_docs = $('#cmb_verificar_docs').val();
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracontadoc/compracontadoc_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            proveedor_id: fil_proveedor_id,
            doc_cod: doc_cod,
            mesconta_id: mesconta_id,
            verificar_docs: verificar_docs
        }),
        beforeSend: function () {
            $('#compracontadoc_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_compracontadoc_tabla').html(data);
            $('#compracontadoc_mensaje_tbl').hide(300);
        },
        complete: function (data) {
            estilos_datatable();
        },
        error: function (data) {
            $('#compracontadoc_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}

function estilos_datatable() {
    datatable_global_ccdoc = $('#tbl_compracontadocs').DataTable({
        "pageLength": 50,
        "responsive": true,
        /* "bScrollCollapse": true,
        "bJQueryUI": true,
        dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
        buttons: [
          { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
          { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
          { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
          { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
          { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
        ], */
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            { targets: [10], orderable: false }
        ]
    });
    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input')
        .attr('id', 'txt_datatable_fil')
        .attr('placeholder', 'escriba para buscar');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global_ccdoc.search(text_fil).draw();
    }
};

function compracontadoc_historial_form(compracontadoc_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracontadoc/compracontadoc_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
            compracontadoc_id: compracontadoc_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_compracontadoc_historial_form').html(html);
            $('#div_modal_compracontadoc_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_compracontadoc_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_compracontadoc_historial_form'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
        }
    });
}

function ple_compras_reporte() {
    var fil_proveedor_id = $("#hdd_fil_proveedor_id").val();
    var doc_cod = $("#hdd_fil_doc_cod").val();
    var mesconta_id = $('#cmb_periodo_id').val();
    var verificar_docs = $('#cmb_verificar_docs').val();
    var nombre_mes = $('#cmb_periodo_id option:selected').text();

    //window.open("http://www.ipdnsac.com/ipdnsac/vista/cuenta/cuenta_reporte_excel.php?fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&cuenta_tipo=" + cuenta_tipo + "&sede=" + sede);
    window.open(VISTA_URL + "compracontadoc/ple_compras_reporte_excel.php?proveedor=" + fil_proveedor_id + "&tipo_doc=" + doc_cod + "&mesconta=" + mesconta_id + "&verificar_docs=" + verificar_docs + `&mes=${nombre_mes}`);
}

function mostrar_gastos_para_completar() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracontadoc/gastos_registroccd_form.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_registrarccd_gastosgarantia_form').html(html);
            $('#div_modal_registrarccdgastogar_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_registrarccdgastogar_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('div_modal_registrarccdgastogar_form', 40);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_registrarccdgastogar_form'); //funcion encontrada en public/js/generales.js
        }
    });
}