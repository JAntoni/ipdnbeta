<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
?>
<!DOCTYPE html>
<html>

<head>
	<!-- TODOS LOS ESTILOS-->
	<?php include(VISTA_URL . 'templates/head.php'); ?>
	<title><?php echo ucwords(mb_strtolower($menu_tit)); ?></title>
	<style>
		.select2.narrow {
			width: 385px;
		}

		.wrap.select2-selection--single {
			height: 100%;
		}

		.select2-container--default .select2-selection--single {
			padding: 7px 0px; /* necesario con select2 */
		}

		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 22px; /* necesario con select2 */
		}
	</style>
</head>

<body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>

	<div class="wrapper">
		<!-- INCLUIR EL HEADER-->
		<?php include(VISTA_URL . 'templates/header.php'); ?>
		<!-- INCLUIR ASIDE, MENU LATERAL -->
		<?php include(VISTA_URL . 'templates/aside.php'); ?>
		<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
		<?php include('compracontadoc_vista.php');; ?>
		<!-- INCLUIR FOOTER-->
		<?php include(VISTA_URL . 'templates/footer.php'); ?>
		<div class="control-sidebar-bg"></div>
	</div>

	<!-- TODOS LOS SCRIPTS-->
	<?php include(VISTA_URL . 'templates/script.php'); ?>

	<script type="text/javascript" src="<?php echo VISTA_URL . 'compracontadoc/compracontadoc.js?ver=12345457773'; ?>"></script>
</body>

</html>