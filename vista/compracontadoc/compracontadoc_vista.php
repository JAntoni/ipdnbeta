<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Contabilidad</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-1" style="width: 1%;"></div>
					<div name="btn_nuevo" id="btn_nuevo" style="width: 6.5%;" class="col-md-1">
						<br><p></p>
						<button class="btn btn-primary btn-sm" onclick="compracontadoc_form('I',0)"><i class="fa fa-plus"></i> Nuevo</button>
					</div>
					<div class="col-md-10" style="width: 91%;">
						<strong class="control-label">&nbsp; FILTROS:</strong>
						<div class="panel panel-danger">
							<div class="panel-body">
								<?php include VISTA_URL.'compracontadoc/compracontadoc_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-1" style="width: 1%;"></div>
					<div class="col-md-1">
						<button class="btn btn-success btn-sm" type="button" onclick="ple_compras_reporte()"> <i class="fa fa-file-excel-o"></i> Exportar Excel</button>
					</div>
					<div class="col-md-1">
						<button class="btn btn-github btn-sm" type="button" onclick="mostrar_gastos_para_completar()"> <i class="fa fa-eye"></i> Ver gastos por registrar</button>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="compracontadoc_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row">
					
					<!-- Input para guardar el valor ingresado en el search de la tabla-->
                    <input type="hidden" id="hdd_datatable_fil">

					<div class="col-sm-12">
						<div id="div_compracontadoc_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php //require_once('compracontadoc_tabla.php');?>
						</div>
					</div>
				</div>
			</div>
			<div id="div_modal_registrarccd_gastosgarantia_form">
				<!-- INCLUIMOS MODAL PARA ASIGNAR PROVEEDOR A FACTURAS DE GASTOS -->
			</div>
			<div id="div_modal_compracontadoc_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL DOCUMENTO-->
			</div>
			<div id="div_modal_proveedor_form">
				<!-- INCLUIMOS EL MODAL PARA EL PROVEEDOR-->
			</div>
			<div id="div_modal_monedacambiocontable_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE TIPO CAMBIO CONTABILIDAD-->
			</div>
			<div id="div_modal_seleccionargasto_form">
				<!-- INCLUIMOS MODAL PARA SELECCIONAR UN GASTO -->
			</div>
			<div id="div_compracontadoc_historial_form">
				<!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DEL DOCUMENTO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>