<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Garantiafilecredito extends Conexion{
    public $garantiafilecredito_id;

    public $garantiafilecredito_xac = 1;
    public $garantia_id;
    public $garantiafilecredito_url = '...';
    public $garantiafilecredito_his;

    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_garantiafilecredito(tb_garantiafilecredito_xac, tb_garantia_id, tb_garantiafilecredito_url, tb_garantiafilecredito_his) 
                VALUES(:garantiafilecredito_xac, :garantia_id, :garantiafilecredito_url, :garantiafilecredito_his)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafilecredito_xac", $this->garantiafilecredito_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":garantia_id", $this->garantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiafilecredito_url", $this->garantiafilecredito_url, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_his", $this->garantiafilecredito_his, PDO::PARAM_STR);

        $result1 = $sentencia->execute();
        $this->garantiafilecredito_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = 'garantia_'.$this->garantia_id.'_garantiafilecredito_'. $this->garantiafilecredito_id .'.'. $this->fileParts['extension'];
        $garantiafilecredito_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $garantiafilecredito_url)){
          //insertamos las imágenes
          $sql = "UPDATE tb_garantiafilecredito SET tb_garantiafilecredito_url =:garantiafilecredito_url WHERE tb_garantiafilecredito_id =:garantiafilecredito_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":garantiafilecredito_id", $this->garantiafilecredito_id, PDO::PARAM_INT);
          $sentencia->bindParam(":garantiafilecredito_url", $garantiafilecredito_url, PDO::PARAM_STR);
          $result2 = $sentencia->execute();
        }
        else{
          $this->eliminar($this->garantiafilecredito_id);
        }

        if($result1 == 1)
          return 'exito';
        else
          return 'Erro al insertar en Garantia File';
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_garantiafilecredito(
                :garantiafilecredito_id,
                :garantiafilecredito_xac, :credito_id, :garantiafilecreditotipo_id,
                :garantiafilecredito_can, :garantiafilecredito_pro, :garantiafilecredito_val,
                :garantiafilecredito_valtas, :garantiafilecredito_ser, :garantiafilecredito_kil,
                :garantiafilecredito_pes, :garantiafilecredito_tas, :garantiafilecredito_det,
                :garantiafilecredito_web)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafilecredito_id", $this->garantiafilecredito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiafilecredito_xac", $this->garantiafilecredito_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiafilecreditotipo_id", $this->garantiafilecreditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiafilecredito_can", $this->garantiafilecredito_can, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiafilecredito_pro", $this->garantiafilecredito_pro, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_val", $this->garantiafilecredito_val, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_valtas", $this->garantiafilecredito_valtas, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_ser", $this->garantiafilecredito_ser, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_kil", $this->garantiafilecredito_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_pes", $this->garantiafilecredito_pes, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_tas", $this->garantiafilecredito_tas, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_det", $this->garantiafilecredito_det, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafilecredito_web", $this->garantiafilecredito_web, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($garantiafilecredito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_garantiafilecredito WHERE tb_garantiafilecredito_id =:garantiafilecredito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafilecredito_id", $garantiafilecredito_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($garantiafilecredito_id){
      try {
        $sql = "SELECT * FROM tb_garantiafilecredito WHERE tb_garantiafilecredito_id =:garantiafilecredito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafilecredito_id", $garantiafilecredito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_garantiafilecreditos($garantia_id){
      try {
        $sql = "SELECT * FROM tb_garantiafilecredito where tb_garantia_id =:garantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay imágenes subidas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
