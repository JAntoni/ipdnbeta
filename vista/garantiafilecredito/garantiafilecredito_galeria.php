<?php
	session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  

  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'garantiafilecredito/Garantiafilecredito.class.php');
    require_once(VISTA_URL.'garantia/Garantia.class.php');
  }
  else{
    require_once('../garantiafilecredito/Garantiafilecredito.class.php');
    require_once('../garantia/Garantia.class.php');
  }
  $oGarantiafile = new Garantiafilecredito();
  $oGarantia = new Garantia();
  $usuariogrupo_id = $_SESSION['usuariogrupo_id'];
//  echo 'hasta aki entrando normnal';exit();
  $garantia_id = (isset($_POST['garantia_id']))? $_POST['garantia_id'] : intval($garantia_id); //esta variable puede ser definida en un archivo en donde se haga un require_once o un include
  $result = $oGarantia->garantia_credito($garantia_id);
    if($result['estado'] == 1){
      $credito_est = intval($result['data']['tb_credito_est']); // 1 pendiente, 2 aprobado, 3 vigente
    }
  $result = NULL;
  $result = $oGarantiafile->listar_garantiafilecreditos($garantia_id);
?>
<style type="text/css">
	.garantia_img{
		cursor: pointer;
	}
</style>
<?php
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value): ?>
			<div class="col-xs-4 col-sm-4" style="text-align: center;" id="<?php echo 'garantiafilecreditoid_'.$value['tb_garantiafilecredito_id'];?>">
			  <img class="img-responsive garantia_img" src="<?php echo $value['tb_garantiafilecredito_url'];?>" alt="Photo" style="height:73px; width:120px;" onclick="carousel('garantiafilecredito', <?php echo $value['tb_garantia_id'];?>)">
        <?php if($usuariogrupo_id == 2):?>
			    <a class="users-list-name" href="javascript:void(0)" onclick="garantiafile_form_referencia('E', 0, <?php echo $value['tb_garantiafilecredito_id'];?>)">Eliminar</a><br>
        <?php endif;?>
			</div>
			<?php 
		endforeach;
	}
	else{ ?>
		<div class="alert alert-info alert-dismissible">
      <button type="button" id="btn_close_garantiafilecredito_galeria" class="close" data-dismiss="alert" aria-hidden="true">x</button>
      <h4>
        <i class="icon fa fa-info"></i>Mensaje
      </h4>
      Esta garantía no tiene fotos subidas
    </div>
    <?php
	}
?>