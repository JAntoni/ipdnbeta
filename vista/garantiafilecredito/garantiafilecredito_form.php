<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../garantiafilecredito/Garantiafilecredito.class.php');
  $oGarantiafile = new Garantiafilecredito();
  require_once('../funciones/funciones.php');

  $direc = 'garantiafilecredito';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $garantia_id = $_POST['garantia_id'];
  $garantiafilecredito_id = $_POST['garantiafilecredito_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if ($usuario_action == 'L') {
    $titulo = 'ACCION NO DISPONIBLE';
}
if ($usuario_action == 'I') {
    $titulo = 'Subir Fotos';
} elseif ($usuario_action == 'M') {
    $titulo = 'ACCION NO DISPONIBLE';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar imagen';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en garantiafilecredito
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 1; $mensaje = '';


    //si la accion es modificar, mostramos los datos del garantiafilecredito por su ID
    if(intval($garantiafilecredito_id) > 0){
      $result = $oGarantiafile->mostrarUno($garantiafilecredito_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el garantiafilecredito seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $garantiafilecredito_tip = $result['data']['tb_garantiafilecredito_tip'];
          $garantiafilecreditomarca_id = intval($result['data']['tb_garantiafilecreditomarca_id']);
        }
      $result = NULL;
    }

?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_garantiafilecredito" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_garantiafilecredito" method="post">
          <input type="hidden" id="garantiafilecredito_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" id="hdd_garantia_id" value="<?php echo $garantia_id;?>">
          <input type="hidden" name="hdd_garantiafilecredito_id" id="hdd_garantiafilecredito_id" value="<?php echo $garantiafilecredito_id;?>">

          <div class="modal-body">
            <?php if($action != 'eliminar'):?>
              <div class="row">
                <div class="col-md-12">
                  <input id="file_upload" name="file_upload" type="file" multiple="true">
                </div>
                <div class="col-md-12">
                  <div id="queue"></div>
                </div>
                <div class="col-md-12">
                  <div class="alert alert-danger alert-dismissible" id="alert_img" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>
                      <i class="icon fa fa-ban"></i> Alerta
                    </h4>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Imagen?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="garantiafilecredito_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="button" class="btn btn-info" id="btn_guardar_garantiafilecredito" onclick="javascript:$('#file_upload').uploadifive('upload')">Guardar Fotos</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_garantiafilecredito">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_cerrar_garantiafilecredito">Listo</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_garantiafilecredito">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo 'static/css/uploadifive/uploadifive.css';?>">
<script type="text/javascript" src="<?php echo 'static/js/uploadifive/jquery.uploadifive.js';?>"></script>
<script type="text/javascript" src="<?php echo 'vista/garantiafilecredito/garantiafilecredito_form.js?ver=52131645151';?>"></script>
