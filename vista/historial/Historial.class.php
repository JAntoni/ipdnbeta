<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Historial extends Conexion{
  private $tb_hist_id;
  private $tb_hist_usureg;
  private $tb_hist_fecreg;
  private $tb_hist_nom_tabla;
  private $tb_hist_regmodid;
  private $tb_hist_det;
  private $tb_hist_regmodid2;
  private $tb_hist_regmodid3;

  /**
   * Get the value of tb_hist_det
   */
  public function getTbHistDet()
  {
    return $this->tb_hist_det;
  }

  /**
   * Set the value of tb_hist_det
   */
  public function setTbHistDet($tb_hist_det)
  {
    $this->tb_hist_det = $tb_hist_det;

    return $this;
  }

  /**
   * Get the value of tb_hist_regmodid
   */
  public function getTbHistRegmodid()
  {
    return $this->tb_hist_regmodid;
  }

  /**
   * Set the value of tb_hist_regmodid
   */
  public function setTbHistRegmodid($tb_hist_regmodid)
  {
    $this->tb_hist_regmodid = $tb_hist_regmodid;

    return $this;
  }

  /**
   * Get the value of tb_hist_nom_tabla
   */
  public function getTbHistNomTabla()
  {
    return $this->tb_hist_nom_tabla;
  }

  /**
   * Set the value of tb_hist_nom_tabla
   */
  public function setTbHistNomTabla($tb_hist_nom_tabla)
  {
    $this->tb_hist_nom_tabla = $tb_hist_nom_tabla;

    return $this;
  }

  /**
   * Get the value of tb_hist_fecreg
   */
  public function getTbHistFecreg()
  {
    return $this->tb_hist_fecreg;
  }

  /**
   * Set the value of tb_hist_fecreg
   */
  public function setTbHistFecreg($tb_hist_fecreg)
  {
    $this->tb_hist_fecreg = $tb_hist_fecreg;

    return $this;
  }

  /**
   * Get the value of tb_hist_usureg
   */
  public function getTbHistUsureg()
  {
    return $this->tb_hist_usureg;
  }

  /**
   * Set the value of tb_hist_usureg
   */
  public function setTbHistUsureg($tb_hist_usureg)
  {
    $this->tb_hist_usureg = $tb_hist_usureg;

    return $this;
  }

  /**
   * Get the value of tb_hist_id
   */
  public function getTbHistId()
  {
    return $this->tb_hist_id;
  }

  /**
   * Set the value of tb_hist_id
   */
  public function setTbHistId($tb_hist_id)
  {
    $this->tb_hist_id = $tb_hist_id;

    return $this;
  }


  /* GERSON (09-01-24) */
  public function getTbHistRegmodid2()
  {
    return $this->tb_hist_regmodid2;
  }
  public function setTbHistRegmodid2($tb_hist_regmodid2)
  {
    $this->tb_hist_regmodid2 = $tb_hist_regmodid2;
    return $this;
  }

  public function getTbHistRegmodid3()
  {
    return $this->tb_hist_regmodid3;
  }
  public function setTbHistRegmodid3($tb_hist_regmodid3)
  {
    $this->tb_hist_regmodid3 = $tb_hist_regmodid3;
    return $this;
  }
  /*  */

  public function insertar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_hist (tb_hist_usureg, tb_hist_nom_tabla, tb_hist_regmodid, tb_hist_det, tb_hist_regmodid2, tb_hist_regmodid3)
              VALUES ( :tb_hist_usureg, :tb_hist_nom_tabla, :tb_hist_regmodid, :tb_hist_det, :tb_hist_regmodid2, :tb_hist_regmodid3 );";
      
      $historial_usureg =  $this->tb_hist_usureg;
      $historial_tabla =  $this->tb_hist_nom_tabla;
      $historial_regmodid =  $this->tb_hist_regmodid;
      $historial_det =  $this->tb_hist_det;
      $historial_regmodid2 =  $this->tb_hist_regmodid2;
      $historial_regmodid3 =  $this->tb_hist_regmodid3;


      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_hist_usureg", $historial_usureg,PDO::PARAM_INT);
      $sentencia->bindParam(":tb_hist_nom_tabla", $historial_tabla, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_hist_regmodid", $historial_regmodid, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_hist_det", $historial_det, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_hist_regmodid2", $historial_regmodid2, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_hist_regmodid3", $historial_regmodid3, PDO::PARAM_INT);

      $result = $sentencia->execute(); //si se ejecuta, retorna 1
      $historial_id = $this->dblink->lastInsertId(); //ultimo id insertado
      $this->dblink->commit();

      $data['estado'] = $result;
      $data['historial_id'] = $historial_id;
      return $data;
    }
    catch (Exception $th) {
      $this->dblink->rollBack();
      throw $th;
    }
  }

  public function filtrar_historial_por($nombre_tabla, $id_reg){
    try {
      $sql = "SELECT h.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM
              tb_hist h
              INNER JOIN tb_usuario ON tb_hist_usureg = tb_usuario_id
              WHERE tb_hist_nom_tabla = :nombre_tabla
              AND tb_hist_regmodid = :regmodid ORDER BY tb_hist_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":nombre_tabla", $nombre_tabla, PDO::PARAM_STR);
      $sentencia->bindParam(":regmodid", $id_reg, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  /* GERSON (08-09-23) */
  // Procesos de créditos
  public function filtrar_historial_proceso_item($nombre_tabla1, $nombre_tabla2, $nombre_tabla3, $nombre_tabla4, $nombre_tabla5, $nombre_tabla6, $id_reg){
    try {
      $sql = "SELECT h.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM
              tb_hist h
              INNER JOIN tb_usuario ON tb_hist_usureg = tb_usuario_id
              WHERE (tb_hist_nom_tabla = :nombre_tabla1 OR tb_hist_nom_tabla = :nombre_tabla2 OR tb_hist_nom_tabla = :nombre_tabla3 OR tb_hist_nom_tabla = :nombre_tabla4 OR tb_hist_nom_tabla = :nombre_tabla5 OR tb_hist_nom_tabla = :nombre_tabla6)
              AND tb_hist_regmodid = :regmodid ORDER BY tb_hist_fecreg DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":nombre_tabla1", $nombre_tabla1, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla2", $nombre_tabla2, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla3", $nombre_tabla3, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla4", $nombre_tabla4, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla5", $nombre_tabla5, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla6", $nombre_tabla6, PDO::PARAM_STR);
      $sentencia->bindParam(":regmodid", $id_reg, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  /* GERSON (09-01-24) */
  // Procesos de créditos
  public function filtrar_historial_item($nombre_tabla, $proceso_id, $item_id){
    try {
      $sql = "SELECT h.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM
              tb_hist h
              INNER JOIN tb_usuario ON tb_hist_usureg = tb_usuario_id
              WHERE tb_hist_nom_tabla = :nombre_tabla
              AND tb_hist_regmodid2 = :regmodid2 
              AND tb_hist_regmodid3 = :regmodid3 ORDER BY tb_hist_fecreg DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":nombre_tabla", $nombre_tabla, PDO::PARAM_STR);
      $sentencia->bindParam(":regmodid2", $proceso_id, PDO::PARAM_INT);
      $sentencia->bindParam(":regmodid3", $item_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  /* GERSON (13-12-23) */
  // Procesos de créditos
  public function filtrar_historial_proceso_item_new($nombre_tabla1, $nombre_tabla2, $nombre_tabla3, $nombre_tabla4, $nombre_tabla5, $id_reg){
    try {
      $sql = "SELECT h.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM
              tb_hist h
              INNER JOIN tb_usuario ON tb_hist_usureg = tb_usuario_id
              WHERE (tb_hist_nom_tabla = :nombre_tabla1 OR tb_hist_nom_tabla = :nombre_tabla2 OR tb_hist_nom_tabla = :nombre_tabla3 OR tb_hist_nom_tabla = :nombre_tabla4 OR tb_hist_nom_tabla = :nombre_tabla5)
              AND tb_hist_regmodid = :regmodid ORDER BY tb_hist_fecreg DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":nombre_tabla1", $nombre_tabla1, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla2", $nombre_tabla2, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla3", $nombre_tabla3, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla4", $nombre_tabla4, PDO::PARAM_STR);
      $sentencia->bindParam(":nombre_tabla5", $nombre_tabla5, PDO::PARAM_STR);
      $sentencia->bindParam(":regmodid", $id_reg, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */
  
  public function mostrar_ultimo_historial($nombre_tabla, $id_reg){
    try {
      $sql = "SELECT h.*, tb_usuario_nom, tb_usuario_ape, tb_usuario_fot FROM
              tb_hist h
              INNER JOIN tb_usuario ON tb_hist_usureg = tb_usuario_id
              WHERE tb_hist_nom_tabla = :nombre_tabla 
              AND tb_hist_regmodid = :regmodid ORDER BY tb_hist_fecreg DESC LIMIT 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":nombre_tabla", $nombre_tabla, PDO::PARAM_STR);
      $sentencia->bindParam(":regmodid", $id_reg, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  /* daniel odar 28-05-2024 */
  public function eliminar($historial_id, $historial_nom_tabla, $historial_regmodid, $historial_det, $historial_regmodid2, $historial_regmodid3){
    $this->dblink->beginTransaction();
    try {
      $where_opt = '';
      //configurar el where para el delete
        if (!empty($historial_id)) {
          $where_opt .= 'tb_hist_id = :param_opc0';
        }
        if (!empty($historial_nom_tabla)) {
          $where_opt .= empty($where_opt) ? "tb_hist_nom_tabla = :param_opc2" : " AND tb_hist_nom_tabla = :param_opc2";
        }
        if (!empty($historial_regmodid)) {
          $where_opt .= empty($where_opt) ? "tb_hist_regmodid = :param_opc3" : " AND tb_hist_regmodid = :param_opc3";
        }
        if (!empty($historial_det) && !empty($historial_nom_tabla) && !empty($historial_regmodid)) {
          $where_opt .= empty($where_opt) ? "LOCATE(:param_opc4, tb_hist_det) > 0" : " AND LOCATE(:param_opc4, tb_hist_det) > 0";
        }
        if (!empty($historial_regmodid2)) {
          $where_opt .= empty($where_opt) ? "tb_hist_regmodid2 = :param_opc5" : " AND tb_hist_regmodid2 = :param_opc5";
        }
        if (!empty($historial_regmodid3)) {
          $where_opt .= empty($where_opt) ? "tb_hist_regmodid3 = :param_opc6" : " AND tb_hist_regmodid3 = :param_opc6";
        }
      //
      $sql = "DELETE FROM tb_hist WHERE $where_opt";

      $sentencia = $this->dblink->prepare($sql);
      //colocar los valores
        if (!empty($historial_id)) {
          $sentencia->bindParam(":param_opc0", $historial_id, PDO::PARAM_INT);
        }
        if (!empty($historial_nom_tabla)) {
          $sentencia->bindParam(":param_opc2", $historial_nom_tabla, PDO::PARAM_STR);
        }
        if (!empty($historial_regmodid)) {
          $sentencia->bindParam(":param_opc3", $historial_regmodid, PDO::PARAM_INT);
        }
        if (!empty($historial_det)) {
          $sentencia->bindParam(":param_opc4", $historial_det, PDO::PARAM_STR);
        }
        if (!empty($historial_regmodid2)) {
          $sentencia->bindParam(":param_opc5", $historial_regmodid2, PDO::PARAM_INT);
        }
        if (!empty($historial_regmodid3)) {
          $sentencia->bindParam(":param_opc6", $historial_regmodid3, PDO::PARAM_INT);
        }
      //

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
}
?>