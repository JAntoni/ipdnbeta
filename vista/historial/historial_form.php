<?php
    require_once '../../core/usuario_sesion.php';
    require_once '../historial/Historial.class.php';
    $oHist = new Historial();
    require_once '../comentario/Comentario.class.php';
    $oComentario = new Comentario();
    require_once '../funciones/fechas.php';

    $tabla_id = $_POST['tabla_id'];
    $tabla_nom = $_POST['tabla_nom'];
    $lista = '';

    $resultado = $oHist->filtrar_historial_por($tabla_nom, $tabla_id); //arreglo que contiene estado, mensaje, data

    if ($resultado['estado'] == 1) {
        foreach ($resultado['data'] as $historiales => $hist) {
            $lista_comentarios = '';
            $result1 = $oComentario->filtrar_comentarios_por($hist['tb_hist_id'], 'tb_hist');
            if ($result1['estado'] == 1) {
                $lista_comentarios =
                '<div class="box-footer box-comments" id="div_comentarios_anteriores-'.$hist['tb_hist_id'].'">';
                    foreach ($result1['data'] as $key1 => $value1) {
                        $boton_eliminar_coment = '';
                        if (intval($_SESSION['usuarioperfil_id']) == 1) {
                            $boton_eliminar_coment = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true" title="Eliminar comentario" onclick="comentario_eliminar('.$value1['tb_coment_id'].','.$hist['tb_hist_id'].')">x</button>';
                        }

                        $lista_comentarios .=
                        '<div class="box-comment" id="com-'.$value1['tb_coment_id'].'">
                            <img class="img-circle img-sm" src="'.$value1['tb_usuario_fot'].'" alt="User Image">
                            <div class="comment-text">
                                <span class="username">'.$value1['tb_usuario_nom'].' '.$value1['tb_usuario_ape'].$boton_eliminar_coment.'</span>'
                                .$value1['tb_coment_det'].'
                                <span class="text-muted pull-right">'.date("h:i a, j-m-y", strtotime($value1['tb_coment_fecreg'])).'</span>
                            </div>
                        </div>';
                    } '</div>';
            }

            $lista .=
            '<li>
                <i class="fa fa-envelope bg-blue"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($hist['tb_hist_fecreg']).'</span>
                    <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="'.$hist['tb_usuario_fot'].'" alt="user image">
                        <span class="username"><a href="#">'.$hist['tb_usuario_nom'].' '.$hist['tb_usuario_ape'].'</a></span>
                        <span class="description">'.$hist['tb_hist_det'].'</span>
                    </div>
                    <div class="timeline-body">
                        <input type="hidden" id="cantidad_comentarios-'.$hist['tb_hist_id'].'" value="'.intval($result1['cantidad']).'">'.
                        $lista_comentarios.
                        '<div class="box-footer" id="div_comentar-'.$hist['tb_hist_id'].'">
                            <img class="img-responsive img-circle img-sm" src="'.$_SESSION['usuario_fot'].'" alt="Alt Text">
                            <div class="img-push">
                                <input type="text" class="form-control input-sm" id="inp_comentario-'.$hist['tb_hist_id'].'" placeholder="Escribe y presiona Enter para postear un comentario" onclick=action_click(this)>
                            </div>
                        </div>
                    </div>
                </div>
            </li>';
        }
    }
    else
    {
        if( (strtolower(trim($tabla_nom)) === "tb_stockunidad") ){
            require_once('../stockunidad/Stockunidad.class.php');
            $oStockUnidad = new Stockunidad();
            $consultaDatosStockUnidad = $oStockUnidad->mostrarUno($tabla_id);
                $lista = '<div class="box box-primary" id="box'.$tabla_id.'"> <div class="box-body">'.$consultaDatosStockUnidad['data']['tb_stockunidad_his'].'</div></div>';
            $consultaDatosStockUnidad = null;
        }
    }
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_<?php echo $tabla_nom?>_historial_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Historial</h4>
            </div>
            <div class="modal-body">
                <ul class="timeline">
                    <?php echo $lista; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/historial/historial_form.js?ver=6444545300434'; ?>"></script>
