<?php

function fecha_mysql($fecha) {
    if (!empty($fecha)) {
        $date_format = date('Y-m-d', strtotime($fecha));
    } else {
        $date_format = "";
    }
    return $date_format;
}

function fecha_hora_mysql($fecha) {
    if (!empty($fecha)) {
        $date_format = str_replace("T", " ", $fecha);
        $date_format .= ":00";
    } else {
        $date_format = "";
    }
    return $date_format;
}

function mostrar_fecha($fecha) {
    if (!empty($fecha)) {
        $date_format = date('d-m-Y', strtotime($fecha));
    } else {
        $date_format = "";
    }
    return $date_format;
}

function mostrar_fecha_hora($fec) {
    if (!empty($fec)) {
        if ($fec != '0000-00-00 00:00:00') {
            $fecha = date('d-m-Y h:i a', strtotime($fec));
        } else {
            $fecha = "";
        }
    } else {
        $fecha = "";
    }
    return $fecha;
}
function mostrar_fecha_hora_segundo($fec) {
    if (!empty($fec)) {
        if ($fec != '0000-00-00 00:00:00') {
            $fecha = date('d-m-Y h:i:s a', strtotime($fec));
        } else {
            $fecha = "";
        }
    } else {
        $fecha = "";
    }
    return $fecha;
}

function string_hora($hora) {
    if (!empty($hora)) {
        $time = strtotime($hora);
        $time_format = date("H:i:s", $time);
    } else
        $time_format = "";

    return $time_format;
}

function ultimoDia($mes, $ano) {
    $ultimo_dia = 28;
    while (checkdate($mes, $ultimo_dia + 1, $ano)) {
        $ultimo_dia++;
    }
    return $ultimo_dia;
}

function validar_fecha_facturacion($day, $month, $year) {
    if ($day > 28) {
        $lastday = ultimoDia($month, $year);
        if ($day > $lastday) {
            $dayNow = $lastday;
        } else {
            $dayNow = $day;
        }

        $fecha = $dayNow . "-" . $month . "-" . $year;
    } else {
        $fecha = $day . "-" . $month . "-" . $year;
    }

    $fecha = date('d-m-Y', strtotime($fecha));
    return $fecha;
}

function validar_fecha_excel($value) {
    if (substr_count($value, '-') == 2 || substr_count($value, '/') == 2) {
        if (substr_count($value, '-') == 2)
            list($dia, $mes, $anio) = explode('-', $value);
        if (substr_count($value, '/') == 2)
            list($dia, $mes, $anio) = explode('/', $value);

        if (intval($dia) > 0 && intval($mes) > 0 && intval($anio) > 0) {
            return $dia . '-' . $mes . '-' . $anio;
        } else
            return '00-00-0000';
    } else {
        return '00-00-0000';
    }
}

function restaFechas($dFecIni, $dFecFin) {
    $operacion = (strtotime($dFecFin) - strtotime($dFecIni)) / 86400;

    return $operacion;
}


function CalcularAnios($fecha_inicio, $fecha_fin){
    $nacimiento = new DateTime($fecha_inicio);
    $ahora = new DateTime($fecha_fin);
    $diferencia = $ahora->diff($nacimiento);
    return $diferencia->format("%y");
}
    
function validar_fecha_espanol($fecha){
    $valores = explode('-', $fecha);

    if(count($valores) == 3 && checkdate($valores[1], $valores[0], $valores[2]) && strlen($fecha) == 10){
        return true;
    }
    return false;
}

/* GERSON (08-05-23) */
function fechaActual($fecha){
    $valores = explode('-', $fecha);
    $dia = $valores[2];
    $mes = $valores[1];
    $anio = $valores[0];

    switch ($mes){
        case '01': $mes_final = "Enero"; break;
        case '02': $mes_final = "Febrero"; break;
        case '03': $mes_final = "Marzo"; break;
        case '04': $mes_final = "Abril"; break;
        case '05': $mes_final = "Mayo"; break;
        case '06': $mes_final = "Junio"; break;
        case '07': $mes_final = "Julio"; break;
        case '08': $mes_final = "Agosto"; break;
        case '09': $mes_final = "Septiembre"; break;
        case '10': $mes_final = "Octubre"; break;
        case '11': $mes_final = "Noviembre"; break;
        case '12': $mes_final = "Diciembre"; break;
    }

    $fecha_final = $dia.' de '.$mes_final.' del '.$anio;

    return $fecha_final;
}
/*  */

function get_nombre_dia($fecha){
    $fechats = strtotime($fecha); //pasamos a timestamp
 
    //el parametro w en la funcion date indica que queremos el dia de la semana
    //lo devuelve en numero 0 domingo, 1 lunes,....
    switch (date('w', $fechats)){
        case 0: return "Domingo"; break;
        case 1: return "Lunes"; break;
        case 2: return "Martes"; break;
        case 3: return "Miércoles"; break;
        case 4: return "Jueves"; break;
        case 5: return "Viernes"; break;
        case 6: return "Sábado"; break;
    }
}

function nombre_dia_mysql($numero){
    //mysql genera 0 para Lunes, 1 para martes, .... 6 para Domingo
    switch ($numero){
        case 0: return "Lunes"; break;
        case 1: return "Martes"; break;
        case 2: return "Miércoles"; break;
        case 3: return "Jueves"; break;
        case 4: return "Viernes"; break;
        case 5: return "Sábado"; break;
        case 6: return "Domingo"; break;
    }
}

function cronograma_fechas($fecha_inicio, $fecha_fin){
    $fechaInicio = new DateTime($fecha_inicio, new DateTimeZone('America/Lima'));
    $fechaFin = new DateTime($fecha_fin, new DateTimeZone('America/Lima'));
    
    $cronograma = array();
    $contador = 1;

    while ($fechaInicio <= $fechaFin) {
        // Verificamos si el día actual no es domingo (w = 0 para domingo, 6 para sábado).
        if ($fechaInicio->format('w') != 0) {
            $cronograma[] = $fechaInicio->format('Y-m-d');
        }
        // Incrementamos la fecha en un día.
        $fechaInicio->modify('+1 day');

        if($contador >= 50)
            break;
        
        $contador ++;
    }
    
    return $cronograma;
}

/* DANIEL ODAR 14-05-2024, mod 04-11-2024 */
function sumarDiasSemana($fecha, $dias) {
    require_once '../asistencia/Asistencia.class.php';
    $oAsistencia = new Asistencia();
    $fecha_to_mod = new DateTime($fecha);
    $diasSumados = 0;

    while ($diasSumados < ($dias + 1)) {
        // Avanza un día
        $fecha_to_mod->modify('+1 day');
        
        // Verifica si es un día de semana (de lunes a viernes)
        if ($fecha_to_mod->format('N') < 6) {
            // Verifica si es feriado
            $feriado = $oAsistencia->detectar_feriado($fecha_to_mod->format('Y-m-d'), $fecha_to_mod->format('Y'));
            
            // Si no es feriado, cuenta el día como hábil
            if ($feriado['estado'] != 1) {
                $diasSumados++;
            }
        }
    }

    // Retorna la fecha resultante en formato Y-m-d
    return $fecha_to_mod->format('Y-m-d');
}

?>