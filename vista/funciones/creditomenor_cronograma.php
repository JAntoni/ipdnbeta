<?php
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$credito_fecdes = $_POST['credito_fecdes']; //fecha de desembolso del dinero, esta fecha es clave para el cronograma
$moneda_id = intval($_POST['moneda_id']); //el tipo de moneda que le prestas: 1 es soles, 2 és dólares
$cuotatipo_id = intval($_POST['cuotatipo_id']); //2 cuota fija, 1 cuota libre

$C = moneda_mysql($_POST['credito_preaco']); //monto que le vas a prestar
$i = moneda_mysql($_POST['credito_int']); //interés que le cobrarás, número ejemplo: 2%, 3.5%, 5%, etc
$n = intval($_POST['credito_numcuo']); // el número de cuotas, o número de meses que durará el préstamo

//----------------- INICIO DE FORMULAR ES ÚNICA PAR TODO TIPO DE PRÉSTAMO -------------------//
$uno = $i / 100;
$dos = $uno + 1;
$tres = pow($dos,$n);
$cuatroA = ($C * $uno) * $tres;
$cuatroB = $tres - 1;
$R = $cuatroA / $cuatroB;
$r_sum = $R*$n;
//----------------------------- FIN FORMULA--------------------------------//

list($day, $month, $year) = split('-', $credito_fecdes);

if(is_numeric($C) && is_numeric($i) && is_numeric($n) && is_numeric($R)): ?>
	<table style="width:100%" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th align="center">N°</th>
				<th align="center">Fecha</th>
				<th align="center">Capital</th>
				<th align="center">Amortización</th>
				<th align="center">Interés</th>
				<th align="center">Cuota</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if($moneda_id == 1){
				$moneda_simbolo = 'S/. ';
			}else{
				$moneda_simbolo = 'US$. ';
			}
			for ($j = 1; $j <= $n; $j++):
				if($j > 1){
					$C = $C - $amo;
					$int = $C*($i/100);
					$amo = $R - $int;
					if($cuotatipo_id == 1)
						$amo = 0;
				}
				else{
					$int = $C*($i/100);
					$amo = $R - $int;
					if($cuotatipo_id == 1)
						$amo = 0;
				}
				$month = intval($month + 1);
				if($month == 13){
					$month = 1;
					$year = $year + 1;
				}

				$fecha_facturacion = validar_fecha_facturacion($day, $month, $year); //solo es funcion para validar fecha, puedes omitirlo
				//bajo verás esto: mostrar_moneda(), es una función personal para que te muestre así: 2,000.00, 2,255.00, todo con comas, lo puedes hacer a tu medida, lo borras nada más.
				?>
				<tr>
					<td align="center"><?php echo $j;?></td>
					<td align="center"><?php echo $fecha_facturacion;?></td>
					<td align="right"><?php echo $moneda_simbolo . mostrar_moneda($C);?></td>
					<td align="right"><?php echo $moneda_simbolo . mostrar_moneda($amo);?></td>
					<td align="right"><?php echo $moneda_simbolo . mostrar_moneda($int);?></td>
					<td align="right"><?php echo $moneda_simbolo . mostrar_moneda($R);?></td>
				</tr>
				<?php
			endfor;?>
		</tbody>
	</table>
<?php endif; ?>