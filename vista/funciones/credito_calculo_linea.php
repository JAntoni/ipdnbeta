<?php
require_once('../funciones/funciones.php');

$C = moneda_mysql($_POST['credito_preaco']);
$i = moneda_mysql($_POST['credito_int']);
$n = intval($_POST['credito_numcuo']);
$data['estado'] = 0;

if(is_numeric($C) && is_numeric($i) && is_numeric($n)){
	$uno = $i / 100; // 0.05
	$dos = $uno + 1; // 1.05
	$tres = pow($dos, $n); //función exponencial, $dos elevado a la $n (1.05^12) = 1.795856
	$cuatroA = ($C * $uno) * $tres; // (30,000 * 0.05) * 1.795856 = 2693.784
	$cuatroB = $tres - 1; // 1.795856 - 1 = 0.795856
	$r = $cuatroA / $cuatroB; // (2693.784 / 0.795856) = 3384.7630

	$sum = $r*$n; // (3384.7630 * 12) = 40,617.15

	$data['estado'] = 1;
	$data['credito_linapr'] = mostrar_moneda($sum);
	$data['credito_real'] = $r;
}else{
	$data['credito_linapr'] = "";
}

echo json_encode($data);

?>